<?php

namespace frontend\controllers;

use app\modules\city\models\City;
use app\modules\city\models\Republic;
use yii\base\Exception;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\web\Controller;

class ImportController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

//    public function actionImportRepublic()
//    {
//        $republics = (new Query())->from('import_republic')->all();
//
//        $transaction = app()->db->beginTransaction();
//        try {
//            foreach ($republics as $from) {
//                $republic = Republic::findone($from['republic_id']);
//                if (empty($republic)) {
//                    $republic              = new Republic();
//                    $republic->republic_id = $from['republic_id'];
//                }
//
//                $republic->country_id   = $from['country_id'];
//                $republic->name         = $from['name'];
//                $republic->shortname    = $from['shortname'];
//                $republic->timezone     = $from['timezone'];
//                $republic->name_en      = $from['name_en'];
//                $republic->name_az      = $from['name_az'];
//                $republic->name_de      = $from['name_de'];
//                $republic->name_sr      = $from['name_sr'];
//                $republic->shortname_en = $from['shortname_en'];
//                $republic->shortname_az = $from['shortname_az'];
//                $republic->shortname_de = $from['shortname_de'];
//                $republic->shortname_sr = $from['shortname_sr'];
//                $republic->name_uz      = $from['name_uz'];
//                $republic->shortname_uz = $from['shortname_uz'];
//
//                if (!$republic->save()) {
//                    throw new Exception(implode(', ', $republic->getFirstErrors()));
//                }
//
//                dump($republic->toArray());
//            }
//
//            $transaction->commit();
//        } catch (\Exception $ex) {
//            $transaction->rollback();
//            echo 'error: ' . $ex->getMessage();
//            die;
//        }
//    }
//
//    public function actionImportCity()
//    {
//        $cities = (new Query())->from('import_city')->all();
//
//        $transaction = app()->db->beginTransaction();
//        try {
//            foreach ($cities as $from) {
//                $city = City::findOne($from['city_id']);
//                if (empty($city)) {
//                    $city          = new City();
//                    $city->city_id = $from['city_id'];
//                }
//
//                $city->republic_id         = $from['republic_id'];
//                $city->name                = $from['name'];
//                $city->shortname           = $from['shortname'];
//                $city->lat                 = $from['lat'];
//                $city->lon                 = $from['lon'];
//                $city->fulladdress         = $from['fulladdress'];
//                $city->fulladdress_reverse = $from['fulladdress_reverse'];
//                $city->sort                = $from['sort'];
//                $city->name_az             = $from['name_az'];
//                $city->name_en             = $from['name_en'];
//                $city->name_de             = $from['name_de'];
//                $city->name_sr             = $from['name_sr'];
//                $city->fulladdress_az      = $from['fulladdress_az'];
//                $city->fulladdress_en      = $from['fulladdress_en'];
//                $city->fulladdress_de      = $from['fulladdress_de'];
//                $city->fulladdress_sr      = $from['fulladdress_sr'];
//                $city->name_ro             = $from['name_ro'];
//                $city->fulladdress_ro      = $from['fulladdress_ro'];
//                $city->search              = $from['search'];
//                $city->name_uz             = $from['name_uz'];
//                $city->fulladdress_uz      = $from['fulladdress_uz'];
//
//                if (!$city->save()) {
//                    throw new Exception(implode(', ', $city->getFirstErrors()));
//                }
//
//                dump($city->toArray());
//            }
//
//            $transaction->commit();
//        } catch (\Exception $ex) {
//            $transaction->rollback();
//            echo 'error: ' . $ex->getMessage();
//            die;
//        }
//    }
}