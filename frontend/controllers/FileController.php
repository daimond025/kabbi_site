<?php

namespace frontend\controllers;

use Exception;
use frontend\modules\tenant\models\Call;
use Imagine\Exception\InvalidArgumentException;
use Yii;
use yii\imagine\Image;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * Site controller
 */
class FileController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class'  => AccessControl::className(),
                'except' => ['show-external-file'],
                'rules'  => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        ob_get_flush();

        return true; // or false to not run the action
    }

    /**
     * Showing the photo
     *
     * @param string $dir
     * @param string $filename
     */
    public function actionShow($dir, $filename)
    {
        try {
            $this->showFile(Yii::getAlias('@web') . $dir . '/' . user()->tenant_id . '/' . $filename,
                $this->getFileFormat($filename));
        } catch (InvalidArgumentException $exc) {

        }
    }

    /**
     * Showing external file
     *
     * @param string  $filename
     * @param integer $id Tenant id
     */
    public function actionShowExternalFile($filename, $id)
    {
        try {
            $this->showFile(Yii::getAlias('@web') . app()->params['upload'] . '/' . $id . '/' . $filename,
                $this->getFileFormat($filename));
        } catch (InvalidArgumentException $exc) {

        }
    }

    private function getFileFormat($filename)
    {
        $arFile = explode('.', $filename);

        return end($arFile);
    }

    private function showFile($url, $format)
    {
        // png формат выставлен, как костыль для софтфона, т.к. он не отображает тип image/jpeg
        $image = Image::getImagine();
        $image->open($url)->show('png');
    }


    public function actionCallRecord($id)
    {
        /** @var Call $call */
        $call = $this->findCallModel($id);

        if (!$call) {
            return false;
        }

        return $call->exportRecord();
    }

    /**
     * Получить модель Call
     * @param int $id
     *
     * @return array|null|\yii\db\ActiveRecord
     */
    protected function findCallModel($id)
    {
        return Call::find()
            ->where([
                'tenant_id' => user()->tenant_id,
                'id' => $id,
            ])
            ->limit(1)
            ->one();
    }

}
