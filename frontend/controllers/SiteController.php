<?php

namespace frontend\controllers;

use app\components\behavior\ControllerBehavior;
use common\modules\order\models\Order;
use common\modules\tenant\models\DefaultSettings;
use common\modules\tenant\models\Tenant;
use common\modules\tenant\models\TenantSetting;
use common\modules\tenant\modules\tariff\models\Module;
use common\modules\tenant\modules\tariff\models\TenantTariff;
use frontend\modules\tenant\models\mobileApp\MobileApp;
use Yii;
use yii\filters\AccessControl;
use yii\base\Exception;
use yii\base\UserException;
use yii\db\Exception as DbException;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

use Sly\NotificationPusher\PushManager,
    Sly\NotificationPusher\Adapter\Gcm as GcmAdapter,
    Sly\NotificationPusher\Collection\DeviceCollection,
    Sly\NotificationPusher\Model\Device,
    Sly\NotificationPusher\Model\Message,
    Sly\NotificationPusher\Model\Push
    ;
/**
 * Class SiteController
 * @package frontend\controllers
 *
 * @mixin ControllerBehavior
 */
class SiteController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class'  => AccessControl::className(),
                'except' => ['error', 'page-info'],
                'rules'  => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'common' => [
                'class'  => ControllerBehavior::className(),
                'except' => ['forbidden', 'page-info', 'upload_client_photo', 'test'],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if ($action->id === 'upload_client_photo') {
            $this->enableCsrfValidation = false;
        }

        $this->commonBeforeAction();

        return parent::beforeAction($action);
    }

    public function actionDeleteCache()
    {
        $cache = Yii::$app->cache;
        echo $cache->flush() ? 'Good' : 'Error';
    }

    public function actionIndex()
    {
       /* $pushManager = new PushManager(PushManager::ENVIRONMENT_DEV);
        $gcmAdapter = new GcmAdapter(array(
            'apiKey' => 'AIzaSyCZWyU4wRJBuPHxOOtSV9--jQm0Hy57N7A',
        ));

        $devices = new DeviceCollection(array(
            new Device('cwxBx2hKz4Y:APA91bG6-Vzy4R68CtYXSvHfiOh6-7Ax5ZaOmU7fCesnK-G-MpU7SPnMmbDmQBQiCm8YUQCE4Z3fyJmcnS1KAE4w3jKnF--SlP4Yul7_rMCGSEIXlSN2gl4MS832bw6kRiOeA0mXWocb'),
        ));
        $params = [];
        $message = new Message('This is an example.', $params);
        $push = new Push($gcmAdapter, $devices, $message);
        $pushManager->add($push);
        $pushManager->push();

        foreach($push->getResponses() as $token => $response) {
            var_dump($response);
        }
        exit();*/


        $clientApps = MobileApp::find()
            ->where([
                'tenant_id' => user()->tenant_id,
                'active'    => 1,
            ])
            ->orderBy('sort')
            ->all();

        $driverAndroidLink = TenantSetting::getSettingValue(user()->tenant_id, TenantSetting::GOOGLE_PLAY_WORKER_APP_LINK);

        return $this->render('index', ['clientApps' => (array)$clientApps, 'driverAndroidLink' => $driverAndroidLink]);
    }

    public function actionTest()
    {

        return $this->render('test');
    }

    public function actionPageInfo()
    {
        $domain    = domainName();
        $tenant_id = $this->getTenantIdByDomain($domain);

        return TenantSetting::getSettingValue($tenant_id, DefaultSettings::SETTING_CLIENT_APP_PAGE);
    }

    public function actionError()
    {
        if (($exception = Yii::$app->getErrorHandler()->exception) === null) {
            // action has been invoked not from error handler, but by direct route, so we display '404 Not Found'
            $exception = new HttpException(404, Yii::t('yii', 'Page not found.'));
        }

        if ($exception instanceof DbException && strpos($exception->getFile(), 'redis')) {
            session()->setFlash('error', t('error', 'Temporarily does not work order creation. Soon we will fix it.'));

            return $this->goHome();
        }

        if ($exception instanceof HttpException) {
            $code = $exception->statusCode;
        } else {
            $code = $exception->getCode();
        }

        if ($exception instanceof Exception) {
            $name = $exception->getName();
        } else {
            $name = Yii::t('yii', 'Error');
        }

        if ($code) {
            $name .= " (#$code)";
        }

        if ($exception instanceof UserException) {
            $message = $exception->getMessage();
        } else {
            $message = Yii::t('yii', 'An internal server error occurred.');
        }

        if (Yii::$app->getRequest()->getIsAjax()) {
            return "$name: $message";
        } else {
            if (app()->user->isGuest) {
                $this->layout = false;
                $view         = 'guestError';
            } else {
                $view = 'error';
            }

            $lang = get('amp;lang');

            return $this->render($view, [
                'name'      => $name,
                'message'   => $message,
                'exception' => $exception,
                'lang'      => $lang
            ]);
        }
    }

    /**
     * Getting return url
     * @return mixed|string
     */
    private function getReturnUrl()
    {
        $referrer   = Yii::$app->request->cookies->getValue('referrer');
        $currentUrl = Url::to('', true);

        return empty($referrer) || $referrer == $currentUrl ? Url::home() : $referrer;
    }

    /**
     * Access forbidden
     * @return string
     */
    public function actionForbidden()
    {
        $this->layout = false;

        $tenantTariffs = TenantTariff::getTenantTariffs(time(), user()->tenant_id, app()->cache);

        $tariff = '';
        if (isset($tenantTariffs[Module::CODE_TAXI]['tariff'])) {
            $tariff = $tenantTariffs[Module::CODE_TAXI]['tariff']['name']
                . ' (' . $tenantTariffs[Module::CODE_TAXI]['name'] . ')';
        }

        return $this->render('forbidden', [
            'url'    => $this->getReturnUrl(),
            'tariff' => $tariff,
            'name'   => 'Forbidden',
        ]);
    }

    private function getTenantIdByDomain($domain)
    {
        $tenant = Tenant::find()->where(['domain' => $domain])->select(['tenant_id'])->asArray()->one();

        if (empty($tenant)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $tenant['tenant_id'];
    }

}
