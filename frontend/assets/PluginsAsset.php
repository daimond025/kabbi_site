<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class PluginsAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
    public $css = [
    ];
    public $js = [
        'js/html5.js',
        'js/socket.io.js',
        'js/jquery.colorbox-min.js',
        'js/jquery.mousewheel.min.js',
        'js/jquery.jscrollpane.min.js',
        'js/inputmask/jquery.inputmask.js',
        'js/inputmask/jquery.bind-first-0.1.min.js',
        'js/inputmask/jquery.inputmask-multi.js',
        'js/instaFilta.js',
    ];
    public $depends = [
        'yii\jui\JuiAsset',
        'frontend\assets\DatePickerLanguageAsset',
    ];

}
