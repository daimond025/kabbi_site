<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
    public $css = [
        'css/main.css',
    ];
    public $js = [
        'js/gootaxModal.js',
        'js/i18n/i18n.js',
        'js/gootaxModal.js',
        'js/jquery.jscrollpane.min.js',
        'js/functions.js',
        'js/main.js',
        'js/chat.js',
    ];
    public $depends = [
        'frontend\assets\PluginsAsset',
        'yii\web\YiiAsset'
    ];

}
