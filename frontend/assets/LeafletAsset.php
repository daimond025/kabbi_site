<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

class LeafletAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $jsOptions = ['position' => \yii\web\View::POS_END];
    public $publishOptions = [
        'forceCopy' => true
    ];
    public $css = [
        '/js/leaflet-0.7.3/leaflet.css'
    ];
    public $js = [
        '/js/leaflet-0.7.3/leaflet.js',
//        '/js/leaflet-0.7.3/tile/Google.js',
//        '/js/leaflet-0.7.3/tile/Yandex.js',
        '/js/leaflet-0.7.3/Layer.Deferred.js',
        '/js/leaflet-0.7.3/Marker.Rotate.js',
        '/js/leaflet-0.7.3/functions.js',
    ];
    public $depends = [
//        'frontend\assets\MapsAsset'
    ];

}
