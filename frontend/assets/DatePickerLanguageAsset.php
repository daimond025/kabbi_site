<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 30.03.2016
 * Time: 10:36
 */

namespace frontend\assets;


class DatePickerLanguageAsset extends \yii\jui\DatePickerLanguageAsset
{
    const ENGLISH_LANG = 'en';

    public function init()
    {
        parent::init();
        $this->language = substr(app()->language, 0, 2);

        if ($this->language === self::ENGLISH_LANG
            || !file_exists(\Yii::getAlias($this->sourcePath . "/ui/i18n/datepicker-{$this->language}.js"))) {
            $this->autoGenerate = false;
        }
    }
}