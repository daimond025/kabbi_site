$(document).ready(function () {
    var client_type = "user";
    var client_id = $('#user_id').text();
    var client_f = $('#user_f').text();
    var user_is_admin = $('#user_is_admin').text();
    var message_save_text = $('#message_save_text').text();
    var message_delete_text = $('#message_delete_text').text();
    var message_cancel_text = $('#message_cancel_text').text();
    if (typeof user_is_admin === "undefined") {
        user_is_admin = 0;
    }
    var editLink = "";
    if (parseInt(user_is_admin) === 1) {
        editLink = '<a class="message_edit"></a>';
    }
    if (typeof client_f === "undefined") {
        client_f = "";
    }
    var client_i = $('#user_i').text();
    if (typeof client_i === "undefined") {
        client_i = "";
    }
    var client_o = $('#user_o').text();
    if (typeof client_o === "undefined") {
        client_o = "";
    }
    var user_role = $('#user_role').text();
    if (typeof user_role === "undefined") {
        user_role = "";
    }
    var mySoketId = null;
    var scroll_init = false;
    var scrl, scrlapi, scrlc, scrlapic;
    var needLoadContent = true;
    var lastScrollPosition = null;
    var city_arr = $('#user_city_arr').text();
    city_arr = JSON.parse(city_arr);
    var tenant_login = $('#tenant_login').text();

    function showChatConnectError() {
        $.ajax({
                type: "POST",
                url: "/tenant/user/get-chat-connect-error",
                data: {},
                success: function (result) {
                    alert(result);
                },
                error: function () {
                }
            }
        );
    }

    $.ajax({
            type: "POST",
            url: "/tenant/user/get-chat-server",
            data: {},
            success: function (result) {
                if (typeof result === "string" && result.length) {
                    var socket = io.connect(result, {
                        'secure': true,
                        'forceNew': true,
                        "transports" : ["websocket"]
                    });
                    $('body').on('click', '.open_mes', function () {
                        if (!socket.connected) {
                            showChatConnectError();
                        }
                    });
                    chat(socket);
                }
            },
            error: function () {
                showChatConnectError();
            }
        }
    );

    function chat(socket) {
        socket.on('connect', function () {
            $('.open_mes').colorbox({
                inline: true,
                width: "820px",
                height: "660px",
                scrolling: false,
                className: "mes_box",
                opacity: 0.6,
                onComplete: function () {
                    if ($('.mst_mes').hasClass('opened')) {
                        scrlc = $('.mwc_content').jScrollPane();
                        scrl = $('.mwm_content').jScrollPane({
                            'stickToBottom': true,
                            'maintainPosition': true
                        });
                        scrlapi = scrl.data('jsp');
                        if (typeof scrlapi !== "undefined") {
                            scrlapi.scrollToBottom();
                        }
                        scroll_init = true;
                        scrl.bind(
                            'jsp-scroll-y',
                            function (event, scrollPositionY, isAtTop, isAtBottom) {
                                if (isAtTop && needLoadContent) {
                                    needLoadContent = false;
                                    loadMessagesFromHistory();
                                }

                            }
                        );
                    }
                },
                onOpen: function () {
                    $('body').addClass('x_hidden');
                    setTimeout(function () {
                        var countCityChat = $('body').find(".chat_user_city");
                        if (countCityChat.length === 1) {
                            var openedChat = $('body').find(".chat_user_city").filter(".opened_group");
                            if (openedChat.length < 1) {
                                var chatUserCity = $('body').find('.chat_user_city').first();
                                if (chatUserCity.length > 0) {
                                    $('body').find('.chat_user_city').first().trigger('click');
                                    $('body').find('.shared_chat').first().trigger('click');
                                }
                            }
                        }

                    }, 800);
                },
                onClosed: function () {
                    $('body').removeClass('x_hidden');
                }
            });
            $('body').on('click', '.open_mes', function () {
                $('.mes_tabs_header a[data-mtid="' + $(this).data('mtid') + '"]').trigger('click');
            });

            $('body').on('click', '.mes_tabs_header a', function () {
                $('.mes_tabs_header a').removeClass('opened');
                $(this).addClass('opened');
                $('.mes_tabs_content>div').hide();
                $('.mes_tabs_content #' + $(this).data('mtid')).show();
            });


            $('.pause_list label').on('click', function () {
                $('#confirm_pause').prop('disabled', false);
            });

            $('body').on('click', '.mst_mes', function () {
                if (!scroll_init) {
                    setTimeout(function () {
                        scrlc = $('.mwc_content').jScrollPane();
                        scrl = $('.mwm_content').jScrollPane();
                        scrlapi = scrl.data('jsp');
                        if (scrlapi !== undefined) {
                            scrlapi.scrollToBottom();
                        }
                        scroll_init = true;
                    }, 50);
                }
            });

            $('.mwm_content').bind(
                'jsp-scroll-y',
                function (event, scrollPositionY, isAtTop, isAtBottom) {
                    if (isAtBottom)
                        $('.mwm_footer').removeClass('mwm_shadow');
                    else
                        $('.mwm_footer').addClass('mwm_shadow');

                    if (isAtTop)
                        $('.mwm_header').removeClass('mwm_shadow');
                    else
                        $('.mwm_header').addClass('mwm_shadow');
                }
            );


            $('body').on('click', '.chat_user_city', function () {
                $('div[data-user-city="' + $(this).prop('rel') + '"]').toggle(100);
                $(this).toggleClass('opened_group');
                if (typeof scrlc !== "undefined") {
                    scrlapic = scrlc.data('jsp');
                    setTimeout(function () {
                        scrlapic.reinitialise({
                            'stickToBottom': true,
                            'maintainPosition': true,
                            'showArrows': true
                        });
                    }, 100);
                }
            });
            $('.search_chat_user').each(function () {
                var cityId = this.dataset.cityid;
                this.onkeyup = function () {
                    var filter = this.value.toUpperCase();
                    $('body').find('li').filter("[data-cityid='" + cityId + "']").each(function () {
                        var name = $(this).find('span').filter("[data-cityid='" + cityId + "']").html();
                        if (typeof name !== "undefined") {
                            if (name.toUpperCase().indexOf(filter) < 0) {
                                this.style.display = 'none';
                            }
                            else {
                                this.style.removeProperty("display");
                            }
                        }
                    });
                };
            });

        });

        socket.on('send_client_data', function (soketId) {
            mySoketId = soketId;
            var clientData = {
                'client_type': client_type,
                'client_id': client_id,
                'client_role': user_role,
                'client_f': client_f,
                'client_i': client_i,
                'client_o': client_o,
                'city_arr': city_arr,
                'tenant_login': tenant_login,
                'socket_id': soketId,
                'device': 'browser'
            };
            socket.emit('save_client_data', clientData);
            socket.emit('get_online_users', {
                "tenant_login": tenant_login,
                "city_arr": city_arr
            });
            socket.emit('get_unreaded_message', {
                'tenant_login': tenant_login,
                'client_type': 'user',
                'client_id': client_id
            });
        });
        socket.on('unreaded_message', function (msg) {
            if (msg.length > 0) {
                for (var i in msg) {
                    showNewMessage(msg[i], false);
                }
            }
        });

        function showNewMessage(msg, needShowFloatMessage) {
            var chatDiv;
            var cityId = msg.city_id;
            if (msg.receiver_type === "main_city_chat") {
                var mainChatCityId = msg.city_id;
                chatDiv = $("div[data-owner='main_city_chat']").filter("[data-ownerid=" + '' + mainChatCityId + '' + "]").filter("[data-cityid=" + '' + cityId + '' + "]");
                if (chatDiv) {
                    if (typeof chatDiv !== "undefined" && chatDiv !== null) {
                        if (chatDiv.find('li').length === 0) {
                            loadLastMessages(chatDiv);
                        }
                        else {
                            addMessage(chatDiv.find('ul'), msg);
                        }
                    }
                }
            } else if (msg.receiver_type === "user") {
                var userId;
                if (msg.sender_type === "user" && parseInt(msg.sender_id) === parseInt(client_id)) {
                    userId = msg.receiver_id;
                }
                if (msg.sender_type === "user" && parseInt(msg.sender_id) !== parseInt(client_id)) {
                    userId = msg.sender_id;
                }
                chatDiv = $("div[data-owner='user']").filter("[data-ownerid=" + '' + userId + '' + "]").filter("[data-cityid=" + '' + cityId + '' + "]");
                if (chatDiv) {
                    if (typeof chatDiv !== "undefined" && chatDiv !== null) {
                        if (isMessengerChatListOpened()) {
                            setMessageReaded('user_chat_selector',
                                msg.city_id,
                                userId,
                                'user',
                                client_id,
                                'user');
                        }
                        if (chatDiv.find('li').length === 0) {
                            loadLastMessages(chatDiv);
                        }
                        else {
                            addMessage(chatDiv.find('ul'), msg);
                        }
                    }
                }

            } else {
                var workerCallsign = msg.receiver_id;
                chatDiv = $("div[data-owner='worker']").filter("[data-ownerid=" + '' + workerCallsign + '' + "]").filter("[data-cityid=" + '' + cityId + '' + "]");
                if (chatDiv) {
                    if (typeof chatDiv !== "undefined" && chatDiv !== null) {
                        if (chatDiv.find('li').length === 0) {
                            loadLastMessages(chatDiv);
                        }
                        else {
                            addMessage(chatDiv.find('ul'), msg);
                        }
                        if (isMessengerChatListOpened()) {
                            setMessageReaded('worker_chat_selector',
                                msg.city_id,
                                workerCallsign,
                                'worker',
                                workerCallsign,
                                'worker');
                        }
                    }
                }
            }
            addMessageCounter(msg);
            if (needShowFloatMessage) {
                if (!isMessengerChatListOpened()) {
                    addFloatMessage(msg);
                }
            }

        }

        socket.on('new_message', function (msg) {
            showNewMessage(msg, true);

        });
        socket.on('last_messages', function (msg) {
            if (Array.isArray(msg)) {
                if (msg.length > 0) {
                    ChatData = [];
                    for (var i in msg) {
                        var currentMessage = msg[i];
                        //Ищем открытое окно чата
                        var messageWindow = $('*[data-type="chat_window"]').filter(function (index) {
                            return $(this).css("display") === "block";
                        });

                        if (typeof messageWindow !== 'undefined') {
                            var chatUl = messageWindow.find("ul");
                            if (chatUl.length > 0) {
                                getHistoryMessage(chatUl, currentMessage, parseInt(i), msg.length);
                            }
                        }
                    }
                }
            }
        });

        socket.on('online_users', function (msg) {
            setStatus(msg, 'online');
        });

        socket.on('user_connected', function (msg) {
            setStatus(msg, 'online');
        });
        socket.on('user_disconnected', function (msg) {
            setStatus(msg, 'offline');
        });

        function setStatus(msg, status) {
            if (msg.length > 0) {
                for (var i = 0; i < msg.length; i++) {
                    var currentOnline = msg[i];
                    var clientId = currentOnline.client_id;
                    var clientType = currentOnline.client_type;
                    var clientCity = currentOnline.client_city;
                    var onlineSpan;
                    if (clientType === "user") {
                        onlineSpan = $('*[data-type="user_chat_selector"]').filter('[data-userid="' + clientId + '"]').filter('[data-cityid="' + clientCity + '"]');
                    }
                    if (clientType === "worker") {
                        onlineSpan = $('*[data-type="worker_chat_selector"]').filter('[data-workercallsign="' + clientId + '"]').filter('[data-cityid="' + clientCity + '"]');
                    }
                    if (typeof onlineSpan !== "undefined") {
                        var onlineI = onlineSpan.siblings('i');
                        if (onlineI.length > 0) {
                            if (status === "online") {
                                if (!onlineI.hasClass('user_online')) {
                                    onlineI.addClass('user_online');
                                }
                            }
                            if (status === "offline") {
                                if (onlineI.hasClass('user_online')) {
                                    onlineI.removeClass('user_online');
                                }
                            }

                        }
                    }

                }
            }
        }


        function drawFloatMessage(result, msg) {
            var id = null;
            var url = "";
            try {
                result = JSON.parse(result);
                id = result.id;
                url = result.url;
            } catch (err) {
                console.log('Ошибка JSON.parse');
                console.log(err);

            }
            //Если это персонально сообщение (тип получателя-клиент и получатель это текущий клиент )
            if (msg.receiver_type === "user" && parseInt(msg.receiver_id) === parseInt(client_id)) {
                msg.receiver_id = msg.sender_id;
            }

            if (msg.message.length > 65) {
                msg.message = msg.message.slice(0, 65 - 3) + '...';
            }
            msg.message = urlify(msg.message);
            var html = '<div>\n\
                                <a class="messenger-control__close" data-receivertype="' + msg.receiver_type + '" data-receiverid="' + msg.receiver_id + '" data-cityid="' + msg.city_id + '"></a>\n\
                                <div class="sn_content">\n\
                                    <div class="snc_mes">\n\
                                    <i>' + url + '</i>\n\
                                    <div>\n\
                                        <p>' + msg.sender_f + ' ' + msg.sender_i + ' ' + msg.sender_o + '</p>\n\
                                    </div>\n\
                                    <p>' + msg.message + '</p>\n\
                                    </div>\n\
                                </div>\n\
                            </div>';

            if($('.js-chat-notification').hasClass('active')){
                $('.js-chat-notification').html(html).addClass('new-message');
                setTimeout(function () {
                    $('.js-chat-notification').removeClass('new-message');
                }, 100)
            } else{
                $('.js-chat-notification').html(html).addClass('active');
            }
        }

        $('body').on('click', '.pt_message', function () {
            var reseiverType = $(this).data('receivertype');
            var reseiverId = $(this).data('receiverid');
            var cityId = $(this).data('cityid');
            var reseiverSpan;
            if (reseiverType === "worker") {
                reseiverSpan = $('*[data-type="worker_chat_selector"]').filter('[data-workercallsign="' + reseiverId + '"]').filter('[data-cityid="' + cityId + '"]');
            }
            if (reseiverType === "user") {
                reseiverSpan = $('*[data-type="user_chat_selector"]').filter('[data-userid="' + reseiverId + '"]').filter('[data-cityid="' + cityId + '"]');
            }
            if (reseiverType === "main_city_chat") {
                reseiverSpan = $('*[data-type="main_chat_selector"]').filter('[data-cityid="' + reseiverId + '"]').filter('[data-cityid="' + cityId + '"]');
            }
            if (reseiverSpan.length) {
                $('.open_mes').trigger('click');
                setTimeout(function () {
                    $.colorbox.resize();
                }, 200);
                var parentLi = reseiverSpan.parents('li');
                var parentDiv = parentLi.parents('.users_spoler');
                var parentA = parentDiv.prev('.chat_user_city');
                var collection = $(".chat_user_city");
                collection.each(function () {
                    if ($(this).hasClass('opened_group')) {
                        $(this).trigger('click');
                    }
                });
                if (!parentA.hasClass('opened_group')) {
                    parentA.trigger('click');
                }
                parentLi.trigger('click');
            }
        });

        $('body').on('click', '.sn_content', function () {
            var reseiverType = $(this).prev('.messenger-control__close').data('receivertype');
            var reseiverId = $(this).prev('.messenger-control__close').data('receiverid');
            var cityId = $(this).prev('.messenger-control__close').data('cityid');
            $('.open_mes').trigger('click');
            var reseiverSpan;
            if (reseiverType === "worker") {
                reseiverSpan = $('*[data-type="worker_chat_selector"]').filter('[data-workercallsign="' + reseiverId + '"]').filter('[data-cityid="' + cityId + '"]');
            }
            if (reseiverType === "user") {
                reseiverSpan = $('*[data-type="user_chat_selector"]').filter('[data-userid="' + reseiverId + '"]').filter('[data-cityid="' + cityId + '"]');
            }
            if (reseiverType === "main_city_chat") {
                reseiverSpan = $('*[data-type="main_chat_selector"]').filter('[data-cityid="' + reseiverId + '"]').filter('[data-cityid="' + cityId + '"]');
            }
            var parentLi = reseiverSpan.parents('li');
            var parentDiv = parentLi.parents('.users_spoler');
            var parentA = parentDiv.prev('.chat_user_city');
            var collection = $(".chat_user_city");
            collection.each(function () {
                if ($(this).hasClass('opened_group')) {
                    $(this).trigger('click');
                }
            });
            if (!parentA.hasClass('opened_group')) {
                parentA.trigger('click');
            }
            parentLi.trigger('click');
            $('.js-chat-notification').removeClass('active');
        });

        function addFloatMessage(msg) {
            $.ajax({
                type: "POST",
                url: "/tenant/user/get-photo-url",
                data: {
                    sender_type: msg.sender_type,
                    sender_id: msg.sender_id
                },
                success: function (result) {
                    drawFloatMessage(result, msg);


                },
                error: function () {
                    drawFloatMessage(null, msg);
                }

            });

        }

        $('body').on('click', '.message_edit', function () {
            var parnetMessLi = $(this).closest('li');
            var liPosition = parnetMessLi.index();
            var messagesUl = parnetMessLi.closest('ul')[0];
            var needReinitialiseMessageList = false;
            if (messagesUl.childElementCount - liPosition <= 4) {
                needReinitialiseMessageList = true;
            }

            var messageP = parnetMessLi.find('p');
            if (messageP.length && !messageP.find('#delete_message').length) {
                var messageTimestamp = messageP.data("timestamp");
                var message = messageP.text();
                messageP.replaceWith('\
                    <p data-timestamp=' + messageTimestamp + '><textarea>' + message + '</textarea>\n\
                    <button id="update_message" class="button">' + message_save_text + '</button>\n\
                    <button id="delete_message" class="button red_button">' + message_delete_text + '</button>\n\
                    <a id="cancel_message" class="cancel_edit_message">' + message_cancel_text + '</a></p>\n\n\
                 ');
                if (needReinitialiseMessageList) {
                    scrlapi.reinitialise({
                        'stickToBottom': true,
                        'maintainPosition': true
                    });
                    scrlapi.scrollToBottom();
                }
                parnetMessLi.on('click', '#update_message', function () {
                    var newMessage = parnetMessLi.find('textarea').val().trim();
                    socket.emit('update_message', {
                        "new_message": newMessage,
                        "old_message": message,
                        "timestamp": messageTimestamp
                    });
                    socket.on('message_update_result', function (msg) {
                        if (msg.result === 1) {
                            parnetMessLi.find('p').replaceWith('<p data-timestamp=' + messageTimestamp + '>' + newMessage + '</p>');
                        } else {
                            messageP.replaceWith('<p data-timestamp=' + messageTimestamp + '>' + newMessage + '</p>');
                            alert("Can't update message");
                        }

                    });
                    if (needReinitialiseMessageList) {
                        scrlapi.reinitialise({
                            'stickToBottom': true,
                            'maintainPosition': true
                        });
                        scrlapi.scrollToBottom();
                    }
                });
                parnetMessLi.on('click', '#delete_message', function () {
                    socket.emit('delete_message', {
                        "message": message,
                        "timestamp": messageTimestamp
                    });
                    socket.on('message_delete_result', function (msg) {
                        if (msg.result === 1) {
                            parnetMessLi.remove();
                        } else {
                            alert("Can't delete message");
                        }

                    });
                    if (needReinitialiseMessageList) {
                        scrlapi.reinitialise({
                            'stickToBottom': true,
                            'maintainPosition': true
                        });
                        scrlapi.scrollToBottom();
                    }
                });
                parnetMessLi.on('click', '#cancel_message', function () {
                    parnetMessLi.find('p').replaceWith('<p data-timestamp=' + messageTimestamp + '>' + message + '</p>');
                    if (needReinitialiseMessageList) {
                        scrlapi.reinitialise({
                            'stickToBottom': true,
                            'maintainPosition': true
                        });
                        scrlapi.scrollToBottom();
                    }
                });
            }

        });

        $('body').on('click', '.messenger-control__close', function () {
            var curpos = parseInt($(this).attr('data-not-pos'));
            $('.js-chat-notification').removeClass('active');
            $('body').attr('data-notifications', parseInt($('body').attr('data-notifications')) - 1);
            var receiverType = $(this).data('receivertype');
            var receiverId = $(this).data('receiverid');
            var cityId = $(this).data('cityid');
            reduceCounters(receiverType, receiverId, cityId, 1);
            if (receiverType === "user") {
                setMessageReaded('user_chat_selector',
                    $(this).data('cityid'),
                    receiverId,
                    'user',
                    client_id,
                    'user');
            }
            if (receiverType === "worker") {
                setMessageReaded('worker_chat_selector',
                    $(this).data('cityid'),
                    receiverId,
                    'worker',
                    receiverId,
                    'worker');
            }

        });

        function addMessage(chatUl, msg) {
            var message_id = msg._id;
            var senderF = (typeof msg.sender_f !== "undefined") ? msg.sender_f + ' ' : "";
            var senderI = (typeof msg.sender_i[0] !== "undefined") ? msg.sender_i[0] + '. ' : "";
            var senderO = (typeof msg.sender_o[0] !== "undefined") ? msg.sender_o[0] + '.' : "";
            var senderRole = (typeof msg.sender_role !== "undefined") ? msg.sender_role : "";
            var msgString = msg.time;
            msg.message = urlify(msg.message);
            msg.time = msg.time.split(" ");
            if (typeof msg.time[1] !== "undefined") {
                msg.time = msg.time[1];
            } else {
                msg.time = msgString;
            }
            $.ajax({
                type: "POST",
                url: "/tenant/user/get-photo-url",
                data: {
                    sender_type: msg.sender_type,
                    sender_id: msg.sender_id
                },
                success: function (result) {
                    var id = null;
                    var url = "";
                    try {
                        result = JSON.parse(result);
                        id = result.id;
                        url = result.url;
                    } catch (err) {
                        console.log('Ошибка JSON.parse');
                        console.log(err);
                    }
                    var mdate = getdhm(msg.timestamp);
                    msg.time += ' ' + mdate;
                    if (msg.sender_type === "user" && parseInt(msg.sender_id) === parseInt(client_id)) {
                        chatUl.append('<li class="sub_message"><div><i><span class="avatar">' + url + '</span><span class="message_time">' + msg.time + '</span></i><div><b>' + senderRole + editLink + '</b><a href = "/lib/user/update/' + id + '">' + senderF + senderI + senderO + '</a></div><p data-timestamp="' + msg.timestamp + '">' + msg.message + '</p></div></li>');
                    } else {
                        var href;
                        if (msg.sender_type === "user") {
                            href = "/employee/user/update/" + id;
                        }
                        if (msg.sender_type === "worker") {
                            href = "/employee/worker/update/" + id;
                        }
                        chatUl.append('<li><div><i><span class="avatar">' + url + '</span><span class="message_time">' + msg.time + '</span></i><div><b>' + senderRole + editLink + '</b><a href =' + href + '>' + senderF + senderI + senderO + '</a></div><p data-timestamp="' + msg.timestamp + '">' + msg.message + '</p></div></li>');
                    }
                    if (typeof scrlapi !== "undefined") {
                        scrlapi.reinitialise({
                            'stickToBottom': true,
                            'maintainPosition': true
                        });
                        scrlapi.scrollToBottom();
                    }

                },
                error: function () {
                    if (msg.sender_type === "user" && parseInt(msg.sender_id) === parseInt(client_id)) {
                        chatUl.append('<li class="sub_message"><div><i><span class="avatar"><i></i></span><span class="message_time">' + msg.time + '</span></i><div><b>' + senderRole + editLink + '</b><a href = "#">' + senderF + senderI + senderO + '</a></div><p data-timestamp="' + msg.timestamp + '">' + msg.message + '</p></div></li>');
                    } else {
                        chatUl.append('<li><div><i><span class="avatar"><i></i></span><span class="message_time">' + msg.time + '</span></i><div><b>' + senderRole + editLink + '</b><a href = "#">' + senderF + senderI + senderO + '</a></div><p data-timestamp="' + msg.timestamp + '">' + msg.message + '</p></div></li>');
                    }
                    if (typeof scrlapi !== "undefined") {
                        scrlapi.reinitialise({
                            'stickToBottom': true,
                            'maintainPosition': true
                        });
                        scrlapi.scrollToBottom();
                    }

                }

            });
        }


        function getHistoryMessage(chatUl, msg, i, count) {
            var senderF = (typeof msg.sender_f !== "undefined") ? msg.sender_f + ' ' : "";
            var senderI = (typeof msg.sender_i[0] !== "undefined") ? msg.sender_i[0] + '. ' : "";
            var senderO = (typeof msg.sender_o[0] !== "undefined") ? msg.sender_o[0] + '.' : "";
            var senderRole = (typeof msg.sender_role !== "undefined") ? msg.sender_role : "";
            msg.message = urlify(msg.message);
            var msgString = msg.sender_time;
            msg.sender_time = msg.sender_time.split(" ");
            if (typeof msg.sender_time[1] !== "undefined") {
                msg.sender_time = msg.sender_time[1];
            } else {
                msg.sender_time = msgString;
            }
            $.ajax({
                type: "POST",
                url: "/tenant/user/get-photo-url",
                data: {
                    sender_type: msg.sender_type,
                    sender_id: msg.sender_id
                },
                success: function (result) {
                    var id = null;
                    var url = "";
                    try {
                        result = JSON.parse(result);
                        id = result.id;
                        url = result.url;
                    } catch (err) {
                        console.log('Ошибка JSON.parse');
                        console.log(err);

                    }
                    var mdate = getdhm(msg.timestamp);
                    msg.sender_time += ' ' + mdate;
                    var href;
                    if (msg.sender_type === "user") {
                        href = "/lib/user/update/" + id;
                    }
                    if (msg.sender_type === "worker") {
                        href = "/employee/worker/update/" + id;
                    }
                    var Li;
                    if (msg.sender_type === "user" && parseInt(msg.sender_id) === parseInt(client_id)) {
                        Li = '<li class="sub_message"><div><i><span class="avatar">' + url + '</span><span class="message_time">' + msg.sender_time + '</span><div></i><b>' + senderRole + editLink + '</b><a href = "' + href + '">' + senderF + senderI + senderO + '</a></div><p data-timestamp="' + msg.timestamp + '">' + msg.message + '</p></div></li>';

                    } else {
                        Li = '<li><div><i><span class="avatar">' + url + '</span><span class="message_time">' + msg.sender_time + '</span><div></i><b>' + senderRole + editLink + '</b><a href = "' + href + '">' + senderF + senderI + senderO + '</a></div><p data-timestamp="' + msg.timestamp + '">' + msg.message + '</p></div></li>';
                    }
                    ChatData[i] = Li;
                    if (i === count - 1) {
                        showLastHistory();
                    }

                },
                error: function () {
                    var Li;
                    if (msg.sender_type === "user" && parseInt(msg.sender_id) === parseInt(client_id)) {
                        Li = '<li class="sub_message"><div><i><span class="avatar"><i></i></span><span class="message_time">' + msg.sender_time + '</span></i><div><b>' + senderRole + editLink + '</b><a href = "#">' + senderF + senderI + senderO + '</a></div><p data-timestamp="' + msg.timestamp + '">' + msg.message + '</p></div></li>';
                    } else {
                        Li = '<li><div><i><span class="avatar"><i></i></span><span class="message_time">' + msg.sender_time + '</span></i><div><b>' + senderRole + editLink + '</b><a href = "#">' + senderF + senderI + senderO + '</a></div><p data-timestamp="' + msg.timestamp + '">' + msg.message + '</p></div></li>';
                    }
                    ChatData[i] = Li;
                    if (i === count - 1) {
                        showLastHistory();
                    }
                }

            });

            function showLastHistory() {
                for (var i in ChatData) {
                    var li = ChatData[i];
                    chatUl.prepend(li);
                }
                ChatData = [];

                if (needLoadContent) {
                    scrlapi.reinitialise({
                        'stickToBottom': true,
                        'maintainPosition': true
                    });
                    scrlapi.scrollToBottom();
                } else {
                    scrlapi.reinitialise({
                        'stickToBottom': true,
                        'maintainPosition': true
                    });
                    needLoadContent = true;
                }

            }
        }

        function addMessageCounter(msg) {
            var receiverType = msg.receiver_type;
            var receiverId = msg.receiver_id;
            var senderId = msg.sender_id;
            var needAdd = needAddMessageCounter(msg);
            if (needAdd) {
                $('a[data-mtid="mt02"]').each(function () {
                    if (typeof $(this) !== "undefined") {
                        var noticeMessageSpan = $(this).find('span');
                        var countNoticeMessge, countNoticeMessgeNew;
                        if (typeof noticeMessageSpan !== "undefined") {
                            countNoticeMessge = noticeMessageSpan.text();
                            countNoticeMessgeNew = null;
                            if (countNoticeMessge === "" || parseInt(countNoticeMessge) === 0 || typeof countNoticeMessge === "undefined") {
                                countNoticeMessgeNew = 1;
                            } else {
                                countNoticeMessge = noticeMessageSpan.text();
                                countNoticeMessgeNew = parseInt(countNoticeMessge) + 1;
                            }
                            //Увеличиваем счетчик у иконок чата
                            noticeMessageSpan.empty();
                            noticeMessageSpan.text(countNoticeMessgeNew);
                        }
                    }
                });
                //Увеличиваем счетчик в листе юзера чата
                addChatItemCounter(receiverType, receiverId, senderId, msg.city_id);
            }
        }

        function addChatItemCounter(receiverType, receiverId, senderId, cityId) {
            var counterItem;
            if (receiverType === "worker") {
                counterItem = $('#worker_message_counter_' + receiverId + '_' + cityId);
            } else if (receiverType === "user") {
                counterItem = $('#user_message_counter_' + senderId + '_' + cityId);
            } else {
                counterItem = $('#city_message_counter_' + receiverId + '_' + cityId);
            }
            var currentNumNew = null;
            var currentNum = counterItem.text();
            if (currentNum === "" || parseInt(currentNum) === 0 || typeof currentNum === "undefined" || currentNum < 0) {
                currentNumNew = 1;
            } else {
                currentNumNew = parseInt(currentNum) + 1;
            }
            counterItem.empty();
            counterItem.text(currentNumNew);
            counterItem.show();
        }

        function reduceCounters(receiverType, receiverId, cityId, count) {
            var counterItem;
            if (receiverType === "worker") {
                counterItem = $('#worker_message_counter_' + receiverId + '_' + cityId);
            } else if (receiverType === "user") {
                counterItem = $('#user_message_counter_' + receiverId + '_' + cityId);
            } else {
                counterItem = $('#city_message_counter_' + receiverId + '_' + cityId);
            }
            var currentNumNew = null;
            var currentNum = counterItem.text();
            if (currentNum === "" || parseInt(currentNum) === 0 || typeof currentNum === "undefined") {
                currentNumNew = 0;
            } else {
                currentNumNew = parseInt(currentNum) - count;
                if (currentNumNew < 0) {
                    currentNumNew = 0;
                }
            }
            counterItem.empty();
            counterItem.text(currentNumNew);
            if (currentNumNew === 0) {
                counterItem.hide();
            } else {
                counterItem.show();
            }

            $('a[data-mtid="mt02"]').each(function () {
                if (typeof $(this) !== "undefined") {
                    var noticeMessageSpan = $(this).find('span');
                    if (typeof noticeMessageSpan !== "undefined") {
                        var countNoticeMessge = noticeMessageSpan.text();
                        var countNoticeMessgeNew = null;
                        if (countNoticeMessge === "" || parseInt(countNoticeMessge) === 0 || typeof countNoticeMessge === "undefined") {
                            countNoticeMessgeNew = 0;
                        } else {
                            countNoticeMessge = noticeMessageSpan.text();
                            countNoticeMessgeNew = parseInt(countNoticeMessge) - count;
                            if (countNoticeMessgeNew < 0) {
                                countNoticeMessgeNew = 0;
                            }
                        }
                        //Меняем счетчик у иконок чата
                        noticeMessageSpan.empty();
                        noticeMessageSpan.text(countNoticeMessgeNew);
                    }
                }
            });

        }

        function clearChatCounter(item) {
            var counterA = item.closest('a');
            var counter = counterA.find('.new_messages');
            if (counter.css('display') === "block") {
                var currentNum = counter.text();
                counter.text('0');
                counter.css("display", "none");
                $('a[data-mtid="mt02"]').each(function () {
                    if (typeof $(this) !== "undefined") {
                        var noticeMessageSpan = $(this).find('span');
                        if (typeof noticeMessageSpan !== "undefined") {
                            var countNoticeMessge = noticeMessageSpan.text();
                            countNoticeMessge = parseInt(countNoticeMessge);
                            var countNoticeMessgeNew = countNoticeMessge - currentNum;
                            noticeMessageSpan.text(countNoticeMessgeNew);
                        }
                    }
                });
            }
        }

        function setMessageReaded(chatType, cityId, senderId, sender_type, receiver_id, receiver_type) {
            if (chatType === "user_chat_selector" || chatType === "worker_chat_selector") {
                socket.emit('message_is_readed', {
                    'tenant_login': tenant_login,
                    'city_id': cityId,
                    'sender_id': senderId,
                    'sender_type': sender_type,
                    'receiver_id': receiver_id,
                    'receiver_type': receiver_type
                });
            }
        }

        function needAddMessageCounter(msg) {
            if ($('body').hasClass('x_hidden')) {
                if (isSelfMessage(msg)) {
                    return false;
                }
                if (isMessageChatListOpened(msg)) {
                    return false;
                }
            }
            return true;
        }

        function isSelfMessage(msg) {
            if (msg.sender_type === "user" && parseInt(msg.sender_id) === parseInt(client_id)) {
                return true;
            }
            return false;
        }

        function isMessengerChatListOpened() {
            return $('.messenger_window').is(':visible');
        }

        function isMessageChatListOpened(msg) {

            //city_id/callsign/user_id
            var receiverId = msg.receiver_id;
            //main_city_chat/worker/user
            var receiverType = msg.receiver_type;
            var msgCityId = msg.city_id;
            if (receiverType === 'user' && parseInt(receiverId) === parseInt(client_id)) {
                receiverId = msg.sender_id;
            }

            //Ищем открытое окно чата
            var messageWindow = $('[data-type="chat_window"]').filter(function (index) {
                return $(this).css("display") === "block";
            });
            if (messageWindow.length !== 0) {
                //Тип сообщения main_city_chat/worker/user
                var ownerType = messageWindow.data('owner');
                //Ид обекта для оптравки city_id/worker_callsign/user_id
                var ownerId = messageWindow.data('ownerid');
                //Ид города
                var cityId = messageWindow.data('cityid');
                //Если открото то же окно в которое пришло сообщение
                if (ownerType === receiverType && parseInt(ownerId) === parseInt(receiverId) && parseInt(cityId) === parseInt(msgCityId)) {
                    return true;
                }
            }
            return false;
        }

        function deleteFloat(reseiverType, reseiverId, cityId) {
            var sameClose = $('.messenger-control__close').filter('[data-receivertype="' + reseiverType + '"]').filter('[data-receiverid="' + reseiverId + '"]').filter('[data-cityid="' + cityId + '"]');
            sameClose.each(function () {
                $(this).parents('.system_notification').animate({
                    opacity: 0
                }, 50, function () {
                    $(this).remove();
                });
            });
        }

        var userChatSelector = $("span[data-type='user_chat_selector']");
        var userChatLi = userChatSelector.parents('li');
        userChatLi.each(function (index, domElement) {
            $(this).on('click', function () {
                var userChatSelector = $(this).find("span[data-type='user_chat_selector']");
                hideChat();
                $('a').removeClass('selected');
                $(userChatSelector).closest('a').addClass('selected');
                var userId = $(userChatSelector).data("userid");
                var cityId = $(userChatSelector).data("cityid");
                showChat('user', userId, cityId);
                var chatHeader = $(userChatSelector).text();
                $('#chat_header').text(chatHeader);
                clearChatCounter($(userChatSelector));
                deleteFloat('user', userId, cityId);
                setMessageReaded('user_chat_selector',
                    $(userChatSelector).data('cityid'),
                    $(userChatSelector).data('userid'),
                    'user',
                    client_id,
                    'user');
            });

        });

        var workerChatSelector = $("span[data-type='worker_chat_selector']");
        var workerChatLi = workerChatSelector.parents('li');
        workerChatLi.each(function (index, domElement) {
            $(this).on('click', function () {
                var workerChatSelector = $(this).find("span[data-type='worker_chat_selector']");
                hideChat();
                $('a').removeClass('selected');
                $(workerChatSelector).closest('a').addClass('selected');
                var workerCallsign = $(workerChatSelector).data("workercallsign");
                var cityId = $(workerChatSelector).data("cityid");
                showChat('worker', workerCallsign, cityId);
                var chatHeader = $(workerChatSelector).text();
                $('#chat_header').text(chatHeader);
                clearChatCounter($(workerChatSelector));
                deleteFloat('worker', workerCallsign, cityId);
                setMessageReaded('user_chat_selector',
                    $(userChatSelector).data('cityid'),
                    workerCallsign,
                    'worker',
                    workerCallsign,
                    'worker');
            });
        });


        var mainChatSelector = $("span[data-type='main_chat_selector']");
        var mainChatLi = mainChatSelector.parents('li');
        mainChatLi.each(function (index, domElement) {
            $(this).on('click', function () {
                var mainChatSelector = $(this).find("span[data-type='main_chat_selector']");
                hideChat();
                $('a').removeClass('selected');
                $(mainChatSelector).closest('a').addClass('selected');
                var cityId = $(mainChatSelector).data("cityid");
                showChat('main_city_chat', cityId, cityId);
                var chatHeader = $(mainChatSelector).text();
                $('#chat_header').text(chatHeader);
                clearChatCounter($(mainChatSelector));
                deleteFloat('main_city_chat', cityId, cityId);
            });
        });

        function loadMessagesFromHistory() {
            var messageWindow = $('*[data-type="chat_window"]').filter(function (index) {
                return $(this).css("display") === "block";
            });
            if (typeof messageWindow !== 'undefined') {
                //Тип сообщения main_city_chat/worker/user
                var receiverType = messageWindow.data('owner');
                //Ид обекта для оптравки city_id/worker_callsign/user_id
                var receiverId = messageWindow.data('ownerid');
                var cityId = messageWindow.data('cityid');
                var firstLi = messageWindow.find('ul li:first').find('p');
                var lastTimestamp = firstLi.data('timestamp');
                socket.emit('get_history_messages', {
                    'receiver_type': receiverType,
                    'receiver_id': receiverId,
                    'city_id': cityId,
                    'count': 3,
                    'last_timestamp': lastTimestamp,
                    'tenant_login': tenant_login
                });
            }
        }

        var sendMessage = function () {
            var messageWindow = $('*[data-type="chat_window"]').filter(function (index) {
                return $(this).css("display") === "block";
            });
            if (typeof messageWindow !== 'undefined') {
                //Тип сообщения main_city_chat/worker/user
                var ownerType = messageWindow.data('owner');
                //Ид обекта для оптравки city_id/worker_callsign/user_id
                var ownerId = messageWindow.data('ownerid');

                var cityId = messageWindow.data('cityid');
                var messageData = $('#message_area').val();
                $('#message_area').val('');
                socket.emit('new_message', {
                    'receiver_type': ownerType,
                    'receiver_id': ownerId,
                    'city_id': cityId,
                    'message': messageData,
                    'message_time': getDateTime()
                });

            }
        };

        $('body').on('click', '.test_fitch', function () {
            if ($('#message_area').val().trim().length > 0 && $('#message_area').val().trim().length < 1000) {
                sendMessage();
                scrlapi.reinitialise({
                    'stickToBottom': true,
                    'maintainPosition': true
                });
                scrlapi.scrollToBottom();
            }

        });

        $('#message_area').keypress(function (event) {
            if (event.keyCode == 13) {
                if ($('#message_area').val().trim().length > 0 && $('#message_area').val().trim().length < 1000) {
                    event.preventDefault();
                    sendMessage();
                    scrlapi.reinitialise({
                        'stickToBottom': true,
                        'maintainPosition': true
                    });
                    scrlapi.scrollToBottom();
                }
            }
        });


        function showChat(chatType, chatOwnerId, cityId) {
            $('.mwm_header').show();
            if (chatType === "worker") {
                var chatItem = $("div").find("[data-owner='worker']").filter("[data-ownerid=" + '' + chatOwnerId + '' + "]").filter("[data-cityid=" + '' + cityId + '' + "]").css('display', 'block');
            }
            if (chatType === "user") {
                var chatItem = $("div").find("[data-owner='user']").filter("[data-ownerid=" + '' + chatOwnerId + '' + "]").filter("[data-cityid=" + '' + cityId + '' + "]").css('display', 'block');
            }
            if (chatType === "main_city_chat") {
                var chatItem = $("div").find("[data-owner='main_city_chat']").filter("[data-ownerid=" + '' + chatOwnerId + '' + "]").filter("[data-cityid=" + '' + cityId + '' + "]").css('display', 'block');
            }
            loadLastMessages(chatItem);
            $('.mwm_footer').show();
            if (typeof scrlapi !== "undefined") {
                scrlapi.reinitialise({
                    'stickToBottom': true,
                    'maintainPosition': true
                });
                scrlapi.scrollToBottom();
            }
        }

        function loadLastMessages(chatItem) {
            var needLoad = chatItem.find('ul').find('li');
            if (typeof needLoad !== "undefined") {
                if (needLoad.length === 0) {
                    socket.emit('get_last_messages', {
                        'tenant_login': tenant_login,
                        'receiver_id': chatItem.data('ownerid'),
                        'receiver_type': chatItem.data('owner'),
                        'city_id': chatItem.data('cityid'),
                        'count': 10
                    });

                }
            }
        }

        function hideChat() {
            $('.mwm_header').hide();
            $('[data-type="chat_window"]').hide();
            $('.mwm_footer').hide();
        }

        function getDateTime() {
            var now = new Date();
            var year = now.getFullYear();
            var month = now.getMonth() + 1;
            var day = now.getDate();
            var hour = now.getHours();
            var minute = now.getMinutes();
            var second = now.getSeconds();
            if (month.toString().length === 1) {
                month = '0' + month;
            }
            if (day.toString().length === 1) {
                day = '0' + day;
            }
            if (hour.toString().length === 1) {
                hour = '0' + hour;
            }
            if (minute.toString().length === 1) {
                minute = '0' + minute;
            }
            if (second.toString().length === 1) {
                second = '0' + second;
            }
            return day + '.' + month + ' ' + hour + ':' + minute;

        }

        function getdhm(timestamp) {
            timestamp = parseInt(timestamp);
            var dateT = new Date(timestamp);
            var month = dateT.getMonth();
            month = month + 1;
            month = month.toString();
            if (month.length === 1) {
                month = "0" + month;
            }
            var day = dateT.getDate();
            return day + "." + month;
        }

        function urlify(text) {
            var urlRegex = /(((https?:\/\/)|(www\.))[^\s]+)/g;
            //var urlRegex = /(https?:\/\/[^\s]+)/g;
            return text.replace(urlRegex, function (url, b, c) {
                var url2 = (c == 'www.') ? 'http://' + url : url;
                return '<a href="' + url2 + '" target="_blank">' + url + '</a>';
            });
        }

    }
});