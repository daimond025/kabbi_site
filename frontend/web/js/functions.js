function validMail(mailVal) {
    var re = /^[\w-\.]+@[\w-]+\.[a-z]{2,4}$/i;
    return re.test(mailVal);
}

function tooltipHoverInit() {
    $(document).on('mouseenter', '.tooltip', function () {
        var telem = $(this).find('span');
        $(telem).show();

        if ($(this).hasClass('order-window-tariff-description')) {
            $('.order-window-tariff-description__content').jScrollPane();
        }

        var toffset = $(telem).offset();
        if (toffset.left + $(telem).width() > ($(window).width() - 40)) {
            $(telem).addClass('right_tooltip');
        } else {
            $(telem).removeClass('right_tooltip');
        }
        $(telem).stop().animate({"opacity": 1}, 100)
    });

    $(document).on('mouseleave', '.tooltip', function () {
        if ($(this).attr('data-clicked') != '1') {
            var telem = $(this).find('span');
            $(telem).stop().animate({"opacity": 0}, 100, function () {
                $(telem).removeClass('right_tooltip');
                $(this).hide();
            });
        }
    });

    $(document).on('click', '.tooltip', function () {
        if ($(this).attr('data-clicked') != '1') {
            $(this).attr('data-clicked', '1');

            var telem = $(this).find('span');
            $(telem).show();

            if ($(this).hasClass('order-window-tariff-description')) {
                $('.order-window-tariff-description__content').jScrollPane();
            }

            var toffset = $(telem).offset();
            if (toffset.left + $(telem).width() > ($(window).width() - 40)) {
                $(telem).addClass('right_tooltip');
            } else {
                $(telem).removeClass('right_tooltip');
            }
            $(telem).stop().animate({"opacity": 1}, 100)
        } else {
            $(this).attr('data-clicked', '0');
            var telem = $(this).find('span');
            $(telem).stop().animate({"opacity": 0}, 100, function () {
                $(telem).removeClass('right_tooltip');
                $(this).hide();
            });
        }
    });

    $(document).on('click', function (e) {
        if (!$('.tooltip').is(e.target) && $('.tooltip').has(e.target).length === 0) {
            var telem = $('.tooltip').attr('data-clicked', '0').find('span');
            $(telem).stop().animate({"opacity": 0}, 100, function () {
                $(telem).removeClass('right_tooltip');
                $(this).hide();
            });
        }
    });
}

function in_array(value, array) {
    for (var i = 0; i < array.length; i++) {
        if (array[i] == value)
            return true;
    }
    return false;
}

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

//Для обновления строк таблицы
function put_search(tbody, html) {
    tbody.find('tr:gt(0)').remove();
    tbody.append(html);
}

function formatDate(date) {

    var dd = date.getDate()
    if (dd < 10)
        dd = '0' + dd;

    var mm = date.getMonth() + 1
    if (mm < 10)
        mm = '0' + mm;

    var yy = date.getFullYear();

    return yy + '-' + mm + '-' + dd;
}

function $_GET(key) {
    var s = window.location.search;
    s = s.match(new RegExp(key + '=([^&=]+)'));
    return s ? decodeURIComponent(s[1]) : false;
}

function getUrlVar() {
    var urlVar = window.location.search; // получаем параметры из урла
    var arrayVar = []; // массив для хранения переменных
    var valueAndKey = []; // массив для временного хранения значения и имени переменной
    var resultArray = []; // массив для хранения переменных
    arrayVar = (urlVar.substr(1)).split('&'); // разбираем урл на параметры
    if (arrayVar[0] == "")
        return false; // если нет переменных в урле
    for (i = 0; i < arrayVar.length; i++) { // перебираем все переменные из урла
        valueAndKey = arrayVar[i].split('='); // пишем в массив имя переменной и ее значение
        resultArray[valueAndKey[0]] = valueAndKey[1]; // пишем в итоговый массив имя переменной и ее значение
    }
    return resultArray; // возвращаем результат
}

function setAttr(prmName, val) {
    var res = '';
    var d = location.href.split("#")[0].split("?");
    var base = d[0];
    var query = d[1];
    if (query) {
        var params = query.split("&");
        for (var i = 0; i < params.length; i++) {
            var keyval = params[i].split("=");
            if (keyval[0] != prmName) {
                res += params[i] + '&';
            }
        }
    }
    res += prmName + '=' + val;
    window.location.href = base + '?' + res;
    return false;
}

function putHtmlContent(html) {
    var first_tr = $('div.tabs_content div.active .people_table tr:first');

    first_tr.nextAll('tr').remove();
    first_tr.after(html);
}

function getFilterData(data, url) {
    $.post(
        url,
        data,
        putHtmlContent
    );
}

//Функция показа стилизованного фильтра
function showStyleFilter(obj, event) {
    if (obj.parent().hasClass('disabled_select') == false) {
        event.preventDefault();
        event.stopPropagation();

        var cur_filter_css = $(obj).next('.b_sc').css('display');

        $('.c_select').hide();
        $('.b_sc').hide();
        $('.sel_active').each(function () {
            $(this).removeClass('sel_active')
        });

        if (cur_filter_css == 'block') {
            $(obj).next('.b_sc').hide();
            $('.sel_active').each(function () {
                $(this).removeClass('sel_active')
            });

            return false;
        }
        $('.sel_active').each(function () {
            $(this).removeClass('sel_active')
        });
        $(obj).addClass('sel_active');
        $(obj).next('.b_sc').show();
        $(obj).next('.b_sc').off('click').on('click', function (e) {
            if ($(this).is(':not(.js-no-stop)')) {
                e.stopPropagation()
            }
        });

        $('body').on('click', function () {
            $('.b_sc').hide();
            $('.sel_active').each(function () {
                $(this).removeClass('sel_active')
            });
        });
    }
}

//Функция создания стилизованной строки из выбранных вариантов чекбоксов
function createCheckboxStringOnClick() {
    $('.b_sc label').off('click').on('click', function () {
        var sc_c = '';
        var checkbox_cnt = 0;
        $(this).parents('.b_sc').find('label').each(function () {
            if ($(this).find('input[type="checkbox"]').prop("checked")) {
                if (checkbox_cnt == 0)
                    sc_c += $(this).text();
                else
                    sc_c += ', ' + $(this).text();

                checkbox_cnt++;
            }
        });

        if (sc_c != '')
            $(this).parents('.select_checkbox').find('.a_sc').text(sc_c);
        else
            $(this).parents('.select_checkbox').find('.a_sc').text($(this).parents('.select_checkbox').find('.a_sc').attr('rel'));
    });
}

//Востановление стрелок сортировки по умолчанию
function clearSortArrows() {
    $('div.tabs_content div.active .people_table th a span').attr('class', '');
    $('div.tabs_content div.active .people_table th.pt_fio span').addClass('pt_down');
}

function createCheckboxStringOnReady(selector) {
    selector = selector || '.b_sc ul';

    $(selector).each(function () {
        var labels = $(this).find('label');
        var sc_c = '';
        var checkbox_cnt = 0;
        labels.each(function () {
            if ($(this).find('input[type="checkbox"]').prop("checked")) {
                if (checkbox_cnt == 0)
                    sc_c += $(this).text();
                else
                    sc_c += ', ' + $(this).text();

                checkbox_cnt++;
            }
            if (sc_c != '') {
                $(this).parents('.select_checkbox').find('.a_sc').text(sc_c);
            } else {
                var $elem = $(this).parents('.select_checkbox').find('.a_sc');
                $elem.text($elem.attr('rel'));
            }
        });
    });
}

function initInstaFilta() {
    $('.select_filter').on('click', function (e) {
        e.stopPropagation()
    });
    $('.select_filter').instaFilta({
        targets: '.li_filter',
        hideEmptySections: true,
        beginsWith: false,
        caseSensitive: false,
        typeDelay: 0
    });
}

function spolerTextInit() {
    var textt = '';
    $('.clirc_text').each(function () {
        textt = $(this).parent().find('span').html().substring(0, 14) + '...»';
        $(this).html(textt);
    });
}

//Стилизованный селект
function select_init() {
    $('.default_select').each(function () {
        select_init_content(this);
    });
}

function select_init_content(select) {
    var select_obj = $(select);
    if (select_obj.next('.d_select').size() > 0)
        return true;

    var sel_con = '';
    var dis = '', classes = '';
    var fil = '', fil2 = '', fil3 = '', fil4 = '';
    var selected_flag = true;

    if (select_obj.hasClass('select_w_filter')) {
        var placeholderText;
        switch (select_obj.data('lang')) {
            case 'en':
                placeholderText = "Start typing...";
                break;
            case 'ru':
                placeholderText = "Начните вводить запрос...";
                break;
            case 'de':
                placeholderText = "Eingabe beginnen...";
                break;
            default:
                placeholderText = "Start typing...";
        }

        fil = '<div style="margin: 0 10px 5px 10px"><input type="text" class="select_filter" placeholder="' + placeholderText + '"></div>';

        fil2 = 'li_filter';
    }
    if (select.hasAttribute("disabled")) {
        dis = ' disabled_select';
    }

    if (typeof(select_obj.attr('data-additional-classes')) !== 'undefined') {
        classes = select_obj.attr('data-additional-classes');
    }

    select_obj.find('option').each(function () {
        if (this.hasAttribute("selected")) {
            fil3 = ' opt_selected';
            selected_flag = false;
        }
        else
            fil3 = '';
        if ($(this).hasClass('opt_green'))
            fil4 = ' opt_green ';
        if ($(this).hasClass('opt_red'))
            fil4 = ' opt_red ';
        if ($(this).hasClass('select_group') == false)
            sel_con += "<li class='" + fil2 + fil3 + fil4 + "'><a rel='" + $(this).val() + "'>" + $(this).html() + "</a></li>";
        else
            sel_con += "<li class='sel_group'>" + $(this).html() + "</li>";
    });
    var sel_pl = '';

    if (selected_flag) {
        sel_pl = select_obj.attr('data-placeholder');

        if (!sel_pl) {
            sel_pl = select_obj.find('option:first').text();
        }
    }
    else {
        if (select_obj.find('option[selected]').hasClass('opt_red') || select_obj.find('option[selected]').hasClass('opt_green'))
            sel_pl = '<span class="' + select_obj.find('option[selected]').attr('class') + '">' + select_obj.find('option[selected]').html() + '</span>';
        else
            sel_pl = select_obj.find('option[selected]').html();
    }

    var sel_h = 300;
    if (select.hasAttribute("data-height"))
        sel_h = select_obj.attr('data-height');
    select_obj.after('<div class="d_select' + dis + ' ' + classes + '"><a class="a_select" data-country="">' + sel_pl + '</a><div class="c_select" style="max-height:' + sel_h + 'px;">' + fil + '<ul>' + sel_con + '</ul></div></div>');
}


//Маска номера телефона
function phone_mask_init() {
    var maskList = $.masksSort($.masksLoad("/js/inputmask/phone-codes.json"), ['#'], /[0-9]|#/, "mask");
    var maskOpts = {
        inputmask: {
            definitions: {
                '#': {
                    validator: "[0-9]",
                    cardinality: 1
                }
            },
            oncomplete: function () {
                $(this).attr('is-valid', 1);
            },
            onincomplete: function () {
                $(this).attr('is-valid', null);
            },
            //clearIncomplete: true,
            showMaskOnHover: false,
            autoUnmask: true
        },
        match: /[0-9]/,
        replace: '#',
        list: maskList,
        listKey: "mask",
        onMaskChange: function (maskObj, determined) {
            if (determined) {
                var lang = $(this).data('lang');
                var hint = lang === 'ru' ? maskObj.name_ru : maskObj.name_en;
                var desc = lang === 'ru' ? maskObj.desc_ru : maskObj.desc_en;

                if (desc && desc != "") {
                    hint += " (" + desc + ")";
                }
                $(this).next('span').html(hint);
            }
            $(this).attr("placeholder", $(this).inputmask("getemptymask"));
        }
    };

    var maskOptsWoPlaceholder = {
        inputmask: {
            definitions: {
                '#': {
                    validator: "[0-9]",
                    cardinality: 1
                }
            },
            oncomplete: function () {
                $(this).attr('is-valid', 1);
            },
            onincomplete: function () {
                $(this).attr('is-valid', null);
            },
            //clearIncomplete: true,
            showMaskOnHover: false,
            autoUnmask: true
        },
        match: /[0-9]/,
        replace: '#',
        list: maskList,
        listKey: "mask",
        onMaskChange: function (maskObj, determined) {
            if (determined) {
                var lang = $(this).data('lang');
                var hint = lang === 'ru' ? maskObj.name_ru : maskObj.name_en;
                var desc = lang === 'ru' ? maskObj.desc_ru : maskObj.desc_en;

                if (desc && desc != "") {
                    hint += " (" + desc + ")";
                }
                $(this).next('span').html(hint);
            }
        }
    };

    $('.mask_phone').each(function () {
        $(this).inputmasks(maskOpts);
    });

    $('.mask_phone_wo_placeholder').each(function () {
        $(this).inputmasks(maskOptsWoPlaceholder);
    });
}

function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}

function getCookie(key) {
    var value = new RegExp(key + "=([^;]*)").exec(document.cookie);
    return value && decodeURIComponent(value[1]);
}

