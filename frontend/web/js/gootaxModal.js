(function($) {
    $.fn.gootaxModal = function(method) {

        var methods = {
            init: function (options) {
                this.each(function () {
                    var currentId = Math.round(Math.random() * (999999 - 0));
                    $.fn.gootaxModal.settings = $.extend($.fn.gootaxModal.settings, {});
                    $.fn.gootaxModal.settings[currentId] = $.extend( {
                        url: null,
                        inline: false,
                        windowStyle: '',
                        appendTo: 'body',
                        onComplete: function () {

                        },
                        onClosed: function () {

                        }
                    }, options);
                    $(this).attr('data-gootax-modal', currentId);
                    var content = '';
                    if(options.url !== null && options.inline === true){
                        var source = $(options.url);
                        content = source.html();
                        source.empty();
                    }
                    $($.fn.gootaxModal.settings[currentId].appendTo)
                        .append('<div class="gootax-modal-wrapper" data-modal-id="' + currentId + '"><div class="gootax-modal" style="' + $.fn.gootaxModal.settings[currentId].windowStyle + '"><i class="button close-modal"></i><div class="gootax-modal__content">'+content+'</div></div></div>');
                });
            },

            close: function () {
                this.each(function () {
                    var id = $(this).data('gootax-modal');
                    $('[data-modal-id="' + id + '"]').removeClass('active');
                    $.fn.gootaxModal.settings[id].onClosed();
                    setTimeout(function () {
                        $('body').removeClass('locked');
                        if($.fn.gootaxModal.settings[id].inline !== true){
                            $('[data-modal-id="' + id + '"]').find('.gootax-modal__content').empty();
                        }
                    }, 100);
                });
                return this;
            },
            open: function () {
                var id = $(this).data('gootax-modal');
                var _that = this;
                $('.gootax-modal-wrapper').removeClass('active');
                $('body').addClass('locked');
                $('[data-modal-id="' + id + '"]').addClass('active');
                if($.fn.gootaxModal.settings[id].url !== null && $.fn.gootaxModal.settings[id].inline !== true){
                    $('[data-modal-id="' + id + '"]')
                        .addClass('loading')
                        .find('.gootax-modal__content')
                        .empty()
                        .load( $.fn.gootaxModal.settings[id].url, function( response, status, xhr ) {
                            if ( status != "error" ) {
                                $('[data-modal-id="' + id + '"]').removeClass('loading');
                            } else{
                                var msg = "Sorry but there was an error: ";
                                $(this).html( msg + xhr.status + " " + xhr.statusText );
                            };
                            $.fn.gootaxModal.settings[id].onComplete();
                        });
                } else{
                    $.fn.gootaxModal.settings[id].onComplete();
                }

                return this;
            },
            settings: function (options) {
                var id = $(this).data('gootax-modal');
                $.fn.gootaxModal.settings[id] = $.extend($.fn.gootaxModal.settings[id], options);
                return this;
            },
            refresh: function () {
                var id = $(this).data('gootax-modal');
                $.fn.gootaxModal.settings[id].onClosed();
                var height = $('[data-modal-id="' + id + '"]').find('.gootax-modal__content').height();
                if($.fn.gootaxModal.settings[id].url !== null){
                    $('[data-modal-id="' + id + '"]')
                        .addClass('loading')
                        .find('.gootax-modal__content')
                        .css('height', height)
                        .empty()
                        .load( $.fn.gootaxModal.settings[id].url, function( response, status, xhr ) {
                            $('[data-modal-id="' + id + '"]')
                                .removeClass('loading')
                                .find('.gootax-modal__content')
                                .css('height', 'auto');
                            if ( status != "error" ) {
                                $.fn.gootaxModal.settings[id].onComplete();
                            } else{
                                var msg = "Sorry but there was an error: ";
                                $(this).html( msg + xhr.status + " " + xhr.statusText );
                            }
                        });
                }
                return this;
            }
        };

        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метод с именем ' +  method + ' не существует для jQuery.customSelect' );
        }

    };
    $(document).ready(function () {
        $('body').on('click', '[data-gootax-modal]', function (e) {
            e.preventDefault();
            $(this).gootaxModal('open');
        });
        $('body').on('click', '.close-modal', function () {
            var id = $(this).parents('.gootax-modal-wrapper').data('modal-id');
            $('[data-gootax-modal="'+id+'"]').gootaxModal('close');
        });
        $('body').on('keyup', function (e) {
            if (e.keyCode === 27) {
                e.preventDefault();
                e.stopPropagation();
                $('[data-gootax-modal]').gootaxModal('close');
            }
        })
    })
})(jQuery);

function gootaxConfirm(content, ok, cancel, success, error) {

    content += '<div class="confirm-buttons"><a class="button js-gootax-confirm-cancel">' + cancel + '</a><a class="button red_button js-gootax-confirm-ok">' + ok + '</a></div>';

    if(!$('#gootax-confirm').length){
        $('body').append('<div id="gootax-confirm" style="display: none;"></div>')
            .append('<div id="gootax-confirm-content" style="display: none;">' + content + '</div>');

        $('#gootax-confirm').gootaxModal({
            inline: true,
            url: '#gootax-confirm-content',
            windowStyle: 'padding: 20px; left: 50%; top: 50%; width: 400px; margin: -100px 0 0 -200px; min-height: 0; text-align: center; box-sizing: border-box;'
        });
        $('#gootax-confirm').gootaxModal('open');
    } else{
        $('#gootax-confirm-content').html(content);
        $('#gootax-confirm').gootaxModal('open');
    }

    $('body').off('click', '.js-gootax-confirm-ok').on('click', '.js-gootax-confirm-ok', function (e) {
        e.preventDefault();
        $('#gootax-confirm').gootaxModal('close');
        if(typeof(success) === 'function')
            success();
    });
    $('body').off('click', '.js-gootax-confirm-cancel').on('click', '.js-gootax-confirm-cancel', function (e) {
        e.preventDefault();
        $('#gootax-confirm').gootaxModal('close');
        if(typeof(error) === 'function')
            error();
    });
}