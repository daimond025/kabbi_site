/*global L, $*/
function init_map(center_coords, zoom, selector)
{
    var map, layerDefs, osmLayer, yandexLayer, googleLayer, currentLayer;

    $('#' + selector).empty();
    map = L.map(selector, {
        // zoomAnimation: false,
        // markerZoomAnimation: false
    });
    if (center_coords) {
        map.setView(center_coords, zoom);
    }
    map.setMaxBounds(L.latLngBounds(L.latLng(85, -180), L.latLng(-85, 180)));

    // Bugfix for zoom buttons https://github.com/shramov/leaflet-plugins/issues/62
    L.polyline([[0, 0]]).addTo(map);

    // layerDefs = {
    //     yandex: {
    //         name: 'Yandex',
    //         js: [
    //             '/js/leaflet-0.7.3/tile/Yandex.js',
    //             '//api-maps.yandex.ru/2.0/?load=package.map&lang=ru-RU'
    //         ],
    //         init: function () {
    //             return new L.Yandex();
    //         }
    //     },
    //     google: {
    //         name: 'Google',
    //         js: [
    //             '/js/leaflet-0.7.3/tile/Google.js',
    //             '//maps.google.com/maps/api/js?v=3.2&sensor=false&callback=L.Google.asyncInitialize'
    //         ],
    //         init: function () {
    //             return new L.Google('ROADMAP');
    //         }
    //     }
    // };

    osmLayer = new L.TileLayer('//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
    //yandexLayer = new L.DeferredLayer(layerDefs.yandex);
    //googleLayer = new L.DeferredLayer(layerDefs.google);
    googleLayer = L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
        maxZoom: 20,
        subdomains:['mt0','mt1','mt2','mt3']
    });
    yandexLayer = L.tileLayer('http://vec{s}.maps.yandex.net/tiles?l=map&v=17.10.19-0&z={z}&x={x}&y={y}&scale=1&lang=ru_RU',{
        maxZoom: 19,
        subdomains:['01','02','03','04'],
        attribution: '<a href="yandex.ru" target="_blank">Яндекс</a>',
        reuseTiles: true,
        updateWhenIdle: false
    });

    L.control.layers({
        'OSM': osmLayer,
        'Yandex': yandexLayer,
        'Google': googleLayer
    }).addTo(map);

    switch (getCookie('map_layer')) {
        case 'Yandex':
            currentLayer = yandexLayer;
            map.options.crs = L.CRS.EPSG3395;
            break;
        case 'Google':
            currentLayer = googleLayer;
            map.options.crs = L.CRS.EPSG3857;
            break;
        default:
            currentLayer = osmLayer;
            map.options.crs = L.CRS.EPSG3857;
    }

    map.addLayer(currentLayer);

    map.on('baselayerchange', function(e){

        switch (e.name) {
            case 'Yandex':
                map.options.crs = L.CRS.EPSG3395;
                break;
            default:
                map.options.crs = L.CRS.EPSG3857;
        }

        map._resetView(map.getCenter(), map.getZoom());

        setCookie('map_layer', e.name, {path: '/', expires: 365 * 24 * 60 * 60});
    });

    return map;
}