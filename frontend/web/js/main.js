$(document).ready(function () {
    //Активность меню
    var pathname = location.pathname;
    pathname = pathname.split('/');
    var module = pathname[1];
    var action = pathname[2];
    var main_menu_item = $('.first_nav li ul li a');
    var main_menu_item_third = $('.first_nav li ul li ul li a');
    if ($('body .tabs_links li a') != undefined && location.hash != '') {
        tabToggle($('.tabs_links li a[href=\'' + location.hash + '\']'));
    }
    else if (location.hash == '') {
        $('#t01').addClass('active');
        $('a.t01').closest("li").toggleClass('active');
    }
    main_menu_item.each(function () {
        if($(this).hasClass('has-child'))
            return;

        var href = $(this).attr('href');
        if (href.indexOf('/' + module + '/' + action) !== -1) {
            $(this).parent().addClass('active').parents('li').addClass('active').addClass('opened');
        }

    });

    main_menu_item_third.each(function () {
        var href = $(this).attr('href');
        if (href.indexOf('/' + module + '/' + action) !== -1) {
            $(this).parent().addClass('active').parents('li').addClass('active').addClass('opened').parents('li').addClass('active').addClass('opened');
        }

    });

    $('body').on('click', '.first_nav>li>a', function () {
        if (!$(this).is('[href]')) {
            var parent_li = $(this).parent();
            if (!parent_li.hasClass('opened')) {
                $('.first_nav>li.opened').removeClass('opened');
                $('.first_nav>li>ul').hide(100);
                parent_li.toggleClass('opened');
            } else {
                parent_li.toggleClass('opened');
            }
        }
    });

    $('body').on('click', '.first_nav>li>ul>li>a', function () {
        if (!$(this).is('[href]')) {
            var parent_li = $(this).parent();
            if (!parent_li.hasClass('opened')) {
                $('.first_nav>li>ul>li.opened').removeClass('opened');
                parent_li.toggleClass('opened');
            } else {
                parent_li.toggleClass('opened');
            }
        }
    });

    //--------------------------------------------------------------------------------------------------------

    $('.close_header').on('click', function () {
        $(this).parents('.may_close').slideUp();
    });
    //Кнопка добавления города
    $('#add_city_organization .select_checkbox a.a_sc').on('click', function () {
        var city_search_block = $(this).next('.b_sc');
        city_search_block.find('ul li').remove();
        city_search_block.find('input').val('');
    });

    $('.check_option label').on('click', function () {
        if ($(this).find('input[type="checkbox"]').prop("checked")) {
            $(this).parent().find('input[type="text"]').prop('disabled', false);
            $(this).parent().find('input[type="number"]').prop('disabled', false);
        }
        else {
            $(this).parent().find('input[type="text"]').prop('disabled', true);
            $(this).parent().find('input[type="number"]').prop('disabled', true);
        }
    });
    $(document).on('click', '.check_spoler label', function () {
        if ($(this).find('input[type="checkbox"]').prop("checked")) {
            $(this).parent().find('a').removeClass('locked');
        }
        else {
            $(this).parent().find('a').addClass('locked');
            var spolerid2 = '#' + $(this).parent().find('a').data('spolerid');
            $(spolerid2).css('display', 'none');
        }
    });
    //окна

    $(".add_city_organization, .send_sms_back, #do_pause, .driver_history").colorbox({inline: true, width: "300px"});
    $(document).on('click', 'a.logo', function (e) {
        e.preventDefault();
        $.colorbox({href: $(this).attr('href')});
    });

    //tabs

    var datatabsid = 0;
    $('body').on('click', '.tabs_links li a', function () {
        tabToggle($(this));
    });

    function tabToggle(tab) {
        var activeTabId = '#' + tab.attr('class');
        setCookie('tab_active_id', tab.attr('class'), '');
        if (!tab.parents('.tabs_links').attr('data-tabs-id')) {
            tab.parents('.tabs_links').attr('data-tabs-id', datatabsid);
            tab.parents('.tabs_links').next('.tabs_content:first').attr('data-tabs-content-id', datatabsid);
            datatabsid++;
        }
        var currenttabs = tab.parents('.tabs_links').attr('data-tabs-id');

        $('[data-tabs-id="' + currenttabs + '"]').find('.active').removeClass('active');
        $('[data-tabs-content-id="' + currenttabs + '"]').children('.active').removeClass('active');
        tab.parent().addClass('active');

        if ((tab.attr('data-href') || tab.attr('data-ajax')) && !$(activeTabId).hasClass('ajax_loaded')) {
            var loadType;
            if (tab.data('href') != undefined) {
                loadType = 'href';
            }
            else if (tab.data('ajax') != undefined) {
                loadType = 'ajax';
            }
            if (tab.data(loadType)) {
                $(activeTabId).addClass('active').addClass('file_loader');
                $(activeTabId).load(tab.data(loadType), function () {
                    $(activeTabId).removeClass('file_loader');
                    $(activeTabId).addClass('ajax_loaded');
                    $(activeTabId).trigger('tab-loaded');
                })
            }
        }
        else {
            $(activeTabId).addClass('active');
        }
    }

    //окно заказа
    $('#add_order .tabs_links li a').on('click', function () {
        $('.ot_edit').colorbox.resize();
    });
    //spoler

    $('section.cont').on('click', '.spoler', function () {
        if (!$(this).hasClass('locked')) {
            var spolerid = '#' + $(this).data('spolerid');
            $(spolerid).toggle();
        }
    });
    $('.remove_from_blacklist').on('click', function () {
        var compl_i = $(this).find('i');
        compl_i.css('opacity', 1);
    });
    //Стилизованный select
    select_init();

    //Создание выбранного варианта из стилизованного селекта
    $('body').on('click', '.c_select ul li a', function () {
        var a_select = $(this).parents('div.d_select').find('a.a_select');
        $(this).parents('ul').find('.opt_selected').removeClass('opt_selected');
        $(this).parent().addClass('opt_selected');
        var sel_vv = $(this).attr('rel');
        $(this).parents('.d_select').prev('.default_select').find('option').each(function () {
            if ($(this).attr('value') == sel_vv) {
                $(this).prop('selected', true);
                $(this).attr('selected', 'selected');
            }
            else {
                $(this).removeAttr('selected');
            }
        })
        var a_select_class = '';
        if ($(this).parent().hasClass('opt_green'))
            a_select_class = 'class="opt_green"';
        if ($(this).parent().hasClass('opt_red'))
            a_select_class = 'class="opt_red"';
        var a_select_html = '<span ' + a_select_class + ' >' + $(this).html() + '</span>';
        $(a_select).data('country', sel_vv).html(a_select_html);
        $(this).parents('.d_select').prev('.default_select').trigger('change');
        $('.c_select').hide();
        $('.sel_active').each(function () {
            $(this).removeClass('sel_active')
        });
    });
    $('#add_city_organization .a_sc, .context_menu_wrap .a_sc').on('click', function (event) {
        showStyleFilter($(this), event);
    });
    //Выбор города
    $('.b_sc ul').off('click').on('click', 'li a', function () {
        $(this).parents('.select_checkbox').find('.a_sc').attr('data-city', $(this).data('city')).html($(this).html());
        $(this).parents('ul').find('input').val($(this).data('city'));
        $('.b_sc').hide();
        $('.sel_active').each(function () {
            $(this).removeClass('sel_active')
        });
    });
    $('body').on('click', '.hcmr_user .a_sc, .ordered_when', function (e) {
        if ($(this).parent().hasClass('disabled_select') == false) {

            $('.c_select').hide();
            $('.b_sc').hide();
            $('.sel_active').each(function () {
                $(this).removeClass('sel_active')
            });

            var hiddenBlock = $(this).next('.b_sc');
            if (hiddenBlock.css('display') == 'block') {
                hiddenBlock.hide();
            } else {
                hiddenBlock.show();
                e.preventDefault();
                e.stopPropagation();
                hiddenBlock.on('click', function (e) {
                    e.stopPropagation()
                });
                $('body').on('click', function () {
                    $('.b_sc').hide();
                });
            }
        }
    });
    $('div.tabs_content').on('click', '.b_sc ul li label', function () {
        var sc_c = '';
        $(this).parents('ul').find('li label').each(function () {
            if ($(this).find('input[type="checkbox"]').prop("checked")) {
                sc_c += $(this).text() + ', ';
            }
        })
        if (sc_c != '') {
            $(this).parents('.select_checkbox').find('.a_sc').text(sc_c);
        }
        else
            $(this).parents('.select_checkbox').find('.a_sc').text($(this).parents('.select_checkbox').find('.a_sc').attr('rel'));
    });

    //Поиск города
    var search_val_old = '';
    $('#city_search').on('keyup', function () {

        var country_id = $('#add_city_organization').find('div.d_select a.a_select').data('country');

        if (country_id == '') {
            country_id = $('#add_city_organization').find('.default_select').val();
        }

        if (country_id != '') {
            var input_search = $(this);
            setTimeout(function () {

                var city = input_search.val();
                var onlyCities = input_search.data('only-cities');

                if (search_val_old == city)
                    return false;
                search_val_old = city;
                if (city.length >= 2) {
                    $.ajax({
                        type: "POST",
                        url: "/city/city/city-search?all_search=0",
                        data: {city: city, country: country_id, onlyCities: onlyCities},
                        dataType: "html",
                        beforeSend: function () {
                            input_search.addClass('input_preloader');
                        },
                        success: function (html) {
                            input_search.nextAll('li').remove();
                            input_search.removeClass('input_preloader').after(html);
                        }
                    });
                }

            }, 700);
        }
    });
    //Фильтр справочников

    //Фильтр чекбоксов
    $(document).on('click', 'a.select', function (event) {
        showStyleFilter($(this), event);
        createCheckboxStringOnClick();
        if ($(this).data('filter') !== 'no') {
            $('.b_sc ul li label input').off('click').on('click', function () {
                var can_filter = $(this).parents('.select_checkbox').find('a.a_sc').data('filter');
                if (can_filter == 'no') {
                    return;
                }
                var form_search = $(this).parents('form[name="user_search"]');
                var view_type = $(this).parents('div.active').data('view');
                if (typeof view_type == 'undefined')
                    view_type = null;
                clearSortArrows();
                getFilterData(form_search.serialize() + '&view=' + view_type, form_search.attr('action'));
            });
        }
    });
    // Фильтр одного чекбокса
    $('div.tabs_content').on('change', '.q_sc', function (e) {
        var input_search = $(this);
        var form_search = input_search.parents('form[name="user_search"]');
        var view_type = input_search.parents('div.active').data('view');
        $.ajax({
            type: "POST",
            url: form_search.attr('action'),
            data: form_search.serialize() + '&view=' + view_type,
            dataType: "html",
            beforeSend: function () {
                input_search.addClass('input_preloader');
            },
            success: function (html) {
                clearSortArrows();
                input_search.removeClass('input_preloader');
                putHtmlContent(html);
            }
        });
    });
    //Текстовый
    $('div.tabs_content').on('keyup', 'input[name="input_search"]', function (e) {
        var input_search = $(this);
        if (e.which != 13) {
            setTimeout(function () {

                var search = input_search.val();
                if (search_val_old == search)
                    return false;
                search_val_old = search;
                var form_search = input_search.parents('form[name="user_search"]');
                var view_type = input_search.parents('div.active').data('view');
                $.ajax({
                    type: "POST",
                    url: form_search.attr('action'),
                    data: form_search.serialize() + '&view=' + view_type,
                    dataType: "html",
                    beforeSend: function () {
                        input_search.addClass('input_preloader');
                    },
                    success: function (html) {
                        clearSortArrows();
                        input_search.removeClass('input_preloader');
                        putHtmlContent(html);
                    }
                });
            }, 700);
        }
    });
    //Сортировка
    $('div.tabs_content').on('click', '.sort th a', function (e) {
        e.preventDefault();
        var all_sort_span = $(this).parents('tr').find('th a span');
        var sort_span = $(this).find('span');
        var sort_class = sort_span.attr('class');
        var sort;
        var sort_type = sort_span.data('type');
        var form_search = $(this).parents('div.active').find('form');
        var view_type = $(this).parents('div.active').data('view');

        if (typeof view_type == 'undefined') {
            view_type = null;
        }

        all_sort_span.attr('class', '');

        if (sort_class === 'pt_down') {
            sort = 'desc';
            sort_class = 'pt_up';
        }
        else {
            sort = 'asc';
            sort_class = 'pt_down';
        }

        var action_search = form_search.attr('action');
        var url = (action_search == '') || (typeof action_search == 'undefined') ? 'search' : action_search;
        getFilterData(form_search.serialize() + '&sort_type=' + sort_type + '&sort=' + sort + '&view=' + view_type, url);
        sort_span.attr('class', sort_class);
    });
    //----------------------------------------------------------------------------------------------------------------

    //Инициализация фильтра для селекта
    initInstaFilta();

    //TICKETS PLACE PAGE ONLY
    var rating = 0;
    $('.rating li').each(function () {
        $(this).hover(function () {
            if (!$(this).hasClass('locked')) {
                rating = $(this).data('rating');
                for (var i = 1; i <= rating; i++) {
                    $('.rating li[data-rating="' + i + '"]').addClass('active');
                }
            }
            ;
        }, function () {
            if (!$(this).hasClass('locked')) {
                for (var i = 1; i <= rating; i++) {
                    $('.rating li[data-rating="' + i + '"]').removeClass('active');
                }
            }
            ;
        }).on('click', function () {
            if (!$(this).hasClass('locked')) {
                rating = $(this).data('rating');
                for (var i = 1; i <= rating; i++) {
                    $('.rating li[data-rating="' + i + '"]').addClass('active');
                    $('.rating_input input').val(rating);
                }
                ;
                $('.rating li').each(function () {
                    $(this).addClass('locked')
                });
                $('.thanks').fadeIn();
            }
        })
    })

    // /TICKETS PLACE PAGE ONLY

    //CLIENTS PLACE PAGE ONLY

    $('section.cont').on('click', '.mo_error>b', function (e) {
        if ($(this).next('.moe_tt').css('display') != 'block') {
            $('.moe_tt').hide();
            $(this).next('.moe_tt').fadeIn(80);
            e.stopPropagation();
            $('body').on('click', function () {
                $('.moe_tt').each(function () {
                    $(this).fadeOut(80);
                })
            })
        }
        else {
            $('.moe_tt').each(function () {
                $(this).fadeOut(80);
            });
        }

    });

    $(document).on('click', '.cof_left label input', function () {
        var form = $(this).parents('form');
        var cof_time_choser = form.find('input.cof_time_choser');

        if ($('#cof_time_choser').prop('checked') || cof_time_choser.prop('checked')) {
            form.find('.cof_second').removeClass('disabled_cof');
            form.find('.cof_second .disabled_select').removeClass('disabled_select');
        }
        else {
            form.find('.cof_second').addClass('disabled_cof');
            form.find('.cof_second .cof_date').addClass('disabled_select');
        }
    })

    $(".open_order, .open_order_review").colorbox({inline: true, width: "820px"});
    $('.open_order').on('click', function () {
        var sid1 = $(this).attr('href');
        $(sid1).find('.b01').click();
    })
    $('.open_order_review').on('click', function () {
        var sid = $(this).attr('href');
        $(sid).find('.b04').click();
    })
    $('.co_list .tabs_links li a').on('click', function () {
        $('.open_order, .open_order_review').colorbox.resize();
    });
    // /CLIENTS PLACE PAGE ONLY

    // Datepicker
    $('.calendar').each(function () {
        var $input = $(this).find('input');
        $(this).datepicker({
            altField: $input,
            onChangeMonthYear: function () {
                setTimeout(function () {
                    $('.add_city_organization').colorbox.resize();
                }, 20);
            }
        });
    });

    //TARIFS FOR DRIVERS ONLY
    $('.label_js_inp').on('click', function () {
        var ljsi = $(this).data('inp');
        if (!$('[data-input="' + ljsi + '"]').hasClass('active_input')) {
            $(this).parents('.row_input').first().find('.active_input').removeClass('active_input');
            $('[data-input="' + ljsi + '"]').addClass('active_input');
        }
    });

    $('body').on('click', '.check_trigger .row_label label', function () {
        if ($(this).parent().find('input[type="checkbox"]').prop("checked")) {
            $(this).parents('.check_trigger').find('.row_input').find('input').prop('disabled', false).parent().find('select').prop('disabled', false);
            $(this).parents('.check_trigger').find('.d_select').removeClass('disabled_select');
        }
        else {
            $(this).parents('.check_trigger').find('.row_input').find('input').prop('disabled', true).parent().find('select').prop('disabled', true);
            $(this).parents('.check_trigger').find('.d_select').addClass('disabled_select');
        }
    });

    //SELECT TRIGGER

    $('body').on('update', '.default_select', function () {
        $(this).next('.d_select').remove();
        select_init_content(this);
    });

    $('body').on('click', '.a_select', function (e) {
        if ($(this).parent().hasClass('disabled_select') == false) {
            if ($(this).next('.c_select').css('display') == 'block') {
                $(this).next('.c_select').hide();
                $('.b_sc').hide();
                $('.sel_active').each(function () {
                    $(this).removeClass('sel_active')
                });
            }
            else {
                $('.c_select').hide();
                $('.b_sc').hide();
                $('.sel_active').each(function () {
                    $(this).removeClass('sel_active')
                });
                $(this).addClass('sel_active');
                $(this).next('.c_select').show();
                e.preventDefault();
                e.stopPropagation();
                //$(this).next('.c_select').on('click', function (e) { e.stopPropagation() })
                $('body').on('click', function () {
                    $('.c_select').hide();
                    $('.sel_active').each(function () {
                        $(this).removeClass('sel_active')
                    });
                })
            }
        }
    });

    $('body').on('click', '.js-open-helpdesk-widget', function () {
       $('#widget_button').click();
    });

    //Получение мессаги для диспетчера
    var refreshDispatcherMessage = function () {
        setInterval(function checkDispatcherMessage() {
            $.ajax({
                url: "/tenant/user-dispetcher/dispetcher-message-refresh",
                async: true,
                cache: false,
                success: function (json) {
                    console.log(json);
                    if (json != '') {
                        if (typeof json.action !== "undefined" && json.action !== null && json.action !== "") {
                            if (json.action == "set_pause") {
                                //console.log(json);
                                if (typeof json.message !== "undefined" && json.message !== null && json.message !== "") {
                                    $('#errorBlock').empty();
                                    $('.header_notification').show();
                                    $('#errorBlock').text(json.message);
                                    $("html, body").animate({scrollTop: 0}, "slow");
                                }
                                clearInterval(dispetcherTimer);
                                $('#dispetcher_pause').replaceWith('<a id="dispetcher_pause_time" class="button green_button">00:00:00</a>');
                                showDispetcherPauseTimer('00:00:00');
                            } else if (json.action == 'set_shift') {
                                clearInterval(pause_Timer);
                                $('#dispetcher_pause_time').replaceWith('<a href="#dispetcher_p" id="dispetcher_pause" class="button green_button">Пауза</a>');
                                $('#dispetcher_work_time2').replaceWith('<span id="dispetcher_work_time" class="hcmr_time"></span>');
                                showDispetcherTimer(json.message);
                                var pause_window = $(document).find("#dispetcher_pause");
                                pause_window.colorbox({ inline: true, width: "300px" });
                            }
                        }
                    }
                }
                , dataType: "json"
            }).fail(function (xhr) {
                if (xhr.status == 403) {
                    window.location.href = '/login';
                }
            });
        }, 4000);
    };
    if ($('#dispetcher_stop').length > 0) {
        refreshDispatcherMessage();
    }

    // Работа с кнопками для диспетчера в хедере

    function getDispatcherButtons() {
        return $('#dispetcher_start_work, #dispetcher_pause, #dispetcher_pause_time, #dispetcher_stop');
    }

    // Нажатие на кнопку старта смены
    $(document).on('click', '#dispetcher_start_work', function () {
        var $buttons = getDispatcherButtons();

        if ($buttons.hasClass('loading_button')) {
            return false;
        }

        $buttons.addClass('loading_button');
        $.ajax({
            type: "POST",
            url: "/tenant/user-dispetcher/dispetcher-start-work",
            dataType: "html",
            success: function (result) {
                $buttons.removeClass('loading_button');

                $('#dispetcher_start_work').hide();
                $('#dispetcher_buttons').prepend(result);
                var dispetcher_work_time = $('#dispetcher_work_time').text();
                showDispetcherTimer(dispetcher_work_time);
                var pause_window = $(document).find("#dispetcher_pause");
                pause_window.colorbox({inline: true, width: "300px"});
                refreshDispatcherMessage();
            },
            error: function () {
                $buttons.removeClass('loading_button');
            }
        });

    });

    var pause_window = $(document).find("#dispetcher_pause");
    pause_window.colorbox({inline: true, width: "300px"});

    var dispetcher_work_time = $('#dispetcher_work_time').text();
    if (dispetcher_work_time) {
        showDispetcherTimer(dispetcher_work_time)
    }
    var dispetcherTimer;

    function showDispetcherTimer(dispetcher_work_time) {
        var array = dispetcher_work_time.split(":");
        var houers = parseInt(array[0]);
        var mins = parseInt(array[1]);
        var secs = parseInt(array[2]);
        var itr = 1;
        //Таймер
        dispetcherTimer = setInterval(function () {
            itr = itr + 1;
            var work_time = houers * 3600 + mins * 60 + secs + itr;
            var hours = Math.floor(work_time / 3600);
            work_time -= hours * 3600;
            var minutes = Math.floor(work_time / 60);
            work_time -= minutes * 60;
            var seconds = parseInt(work_time % 60, 10);
            $('#dispetcher_work_time').text((hours < 10 ? '0' + hours : hours) + ':' + (minutes < 10 ? '0' + minutes : minutes) + ':' + (seconds < 10 ? '0' + seconds : seconds));

        }, 1000);
    }


    $(document).on('click', '#confirm_pause', function () {
        var reason = $('#dispetcher_p').find("input:radio:checked");
        reason = reason.attr('id');
        $.ajax({
            type: "POST",
            url: "/tenant/user-dispetcher/dispetcher-pause-click?reason=" + reason,
            dataType: "html",
            success: function (result) {
                $("#dispetcher_pause").colorbox.close();
                clearInterval(dispetcherTimer);
                $('#dispetcher_pause').replaceWith('<a id="dispetcher_pause_time" class="button green_button">00:00:00</a>');
                showDispetcherPauseTimer('00:00:00');
            }
        });

    });


    var dispetcher_pause_time = $('#dispetcher_pause_time').text();
    if (dispetcher_pause_time) {
        showDispetcherPauseTimer(dispetcher_pause_time)
    }

    var pause_Timer;

    function showDispetcherPauseTimer(dispetcher_pause_time) {
        var array = dispetcher_pause_time.split(":");
        var houers = parseInt(array[0]);
        var mins = parseInt(array[1]);
        var secs = parseInt(array[2]);
        var itr = 1;
        //Таймер
        pause_Timer = setInterval(function () {
            itr = itr + 1;
            var work_time = houers * 3600 + mins * 60 + secs + itr;
            var hours = Math.floor(work_time / 3600);
            work_time -= hours * 3600;
            var minutes = Math.floor(work_time / 60);
            work_time -= minutes * 60;
            var seconds = parseInt(work_time % 60, 10);
            $('#dispetcher_pause_time').text((hours < 10 ? '0' + hours : hours) + ':' + (minutes < 10 ? '0' + minutes : minutes) + ':' + (seconds < 10 ? '0' + seconds : seconds));

        }, 1000);
    }

    $(document).on('click', '#dispetcher_pause_time', function () {
        var $buttons = getDispatcherButtons();

        if ($buttons.hasClass('loading_button')) {
            return false;
        }

        $buttons.addClass('loading_button');
        $.ajax({
            type: "POST",
            url: "/tenant/user-dispetcher/dispetcher-pause-click?reason=" + null,
            dataType: "html",
            success: function (result) {
                $.ajax({
                    type: "POST",
                    url: "/tenant/user-dispetcher/get-dispetcher-work-time",
                    dataType: "html",
                    success: function (result) {
                        $buttons.removeClass('loading_button');
                        clearInterval(pause_Timer);
                        $('#dispetcher_pause_time').replaceWith('<a href="#dispetcher_p" id="dispetcher_pause" class="button green_button">Пауза</a>');
                        $('#dispetcher_work_time2').replaceWith('<span id="dispetcher_work_time" class="hcmr_time"></span>');
                        showDispetcherTimer(result);
                        var pause_window = $(document).find("#dispetcher_pause");
                        pause_window.colorbox({inline: true, width: "300px"});
                    },
                    error: function () {
                        $buttons.removeClass('loading_button');
                    }
                });
            }
        });
    });

    $(document).on('click', '#dispetcher_stop', function (e) {
        var $buttons = getDispatcherButtons();

        if ($buttons.hasClass('loading_button')) {
            return false;
        }

        $buttons.addClass('loading_button');
        $.ajax({
            type: "POST",
            url: "/tenant/user-dispetcher/dispetcher-stop",
            dataType: "html",
            success: function (result) {
                $buttons.removeClass('loading_button');
                clearInterval(pause_Timer);
                clearInterval(dispetcherTimer);
                $(document).find('#dispetcher_work_time').remove();
                $(document).find('#dispetcher_work_time2').remove();
                $('#dispetcher_stop').remove();
                $(document).find('#dispetcher_pause').replaceWith('<a id="dispetcher_start_work" class="button green_button">Начать смену</a>');
                $(document).find('#dispetcher_pause_time').replaceWith('<a id="dispetcher_start_work" class="button green_button">Начать смену</a>');
            },
            error: function () {
                $buttons.removeClass('loading_button');
            }
        });
    });

    $('.pause_list label').on('click', function () {
        $('#confirm_pause').prop('disabled', false);
    });

    //HELP

    $('body').on('click', '.hw_title', function () {
        if (!$(this).hasClass('opened')) {
            $('.hw_content').show().animate({
                opacity: 1,
                right: 40
            }, 200);
            $('.help_overflow').show().animate({
                opacity: 0.6
            }, 200);
        } else {
            $('.hw_content').animate({
                opacity: 0,
                right: -50
            }, 200, function () {
                $(this).hide();
            });
            $('.help_overflow').animate({
                opacity: 0
            }, 200, function () {
                $(this).hide();
            });
        }
        $(this).toggleClass('opened');
    });

    $('body').on('click', '.help_overflow', function () {
        $('.hw_content').animate({
            opacity: 0,
            right: -50
        }, 200, function () {
            $(this).hide();
        });
        $('.help_overflow').animate({
            opacity: 0
        }, 200, function () {
            $(this).hide();
        });
        $('.hw_title').removeClass('opened');
    });

    //Первый запуск хелпера
    var tenant_helper = $('#tenant_helper');

    if (tenant_helper.size() > 0 && getCookie('tenant_helper') === null) {
        setCookie('tenant_helper', 1, {path: '/', expires: 7 * 24 * 60 * 60});
        tenant_helper.click();
    }

    tooltipHoverInit();


    //Показать/скрыть сырые данные
    $('body').on('click', '.eio_raw_toggle', function () {
        $('.eio_raw').toggle(100);
        setTimeout(function () {
            $.colorbox.resize();
        }, 100);
    });

    //Logout
    $('#logout').on('click', function () {
        var is_dispatcher = $(this).data('dispatcher');
        if (is_dispatcher) {
            try {
                window.qt_handler.unregisterFromServer();
            } catch (e) {
                console.log(e);
            }
        }
    });
});

function SetActiveTab(tab) {
    var begin = tab.href.indexOf('#');
    var end = tab.href.length;
    tab.closest('li');
    //console.log(tab.closest('li').find(tab.class).addClass('active'));
}