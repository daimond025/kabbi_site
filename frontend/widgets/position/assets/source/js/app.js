;(function () {
    'use strict';

    var app = {
        positionSectionSelector: '#entity_position',
        carClassSectionSelector: '#car_classes',
        positionClassSectionSelector: '#position_classes',
        gettingCarClassUrl: '/car/car-class/get-car-class-by-position',
        init: function () {
            this.positionChangingEvent();
            this.classChangingEvent();
        },
        getCarClasses: function (positionId, carClassSection) {
            $.getJSON(
                this.gettingCarClassUrl,
                {positionId: positionId},
                function (json) {

                    json.sort(function (a, b) {
                        if (a['sort'] > b['sort']) {
                            return 1;
                        }
                        if (a['sort'] < b['sort']) {
                            return -1;
                        }
                        return 0;
                    });

                    var $classes = $();
                    var class_select = carClassSection.find('select');

                    class_select.find('option:not(":first")').remove();

                    if (!$.isEmptyObject(json)) {
                        for (var id in json) {
                            $classes = $classes.add($('<option/>').val(json[id]['class_id']).text(json[id]['class']));
                        }

                        class_select.append($classes);
                    }

                    class_select.trigger('update');
                    carClassSection.show();
                }
            );
        },
        positionChangingEvent: function () {
            var _this = this;
            $(this.positionSectionSelector).on('change', 'select', function () {
                var positionId = $(this).val();
                var hasCar = $("[data-position='" + positionId + "']").size();
                var carClassSection = $(_this.carClassSectionSelector);
                var positionClassSection = $(_this.positionClassSectionSelector);
                var positionClassSelect = positionClassSection.find('select');

                positionClassSelect.find('option:not(":first")').remove();

                carClassSection.hide();

                if (hasCar) {
                    _this.getCarClasses(positionId, carClassSection);
                }

                $(this).trigger('positionChange', [positionId, !!hasCar]);
            });
        },
        classChangingEvent: function () {
            $(this.carClassSectionSelector).on('change', 'select', function () {
                $(this).trigger('classChange', [$(this).val()]);
            });
        }
    };

    app.init();
})();