<?php


namespace frontend\widgets\position\assets;


use yii\web\AssetBundle;

class PositionChooserAsset extends AssetBundle
{
    public $sourcePath = '@frontend/widgets/position/assets/source/';

    public $publishOptions = [
        'forceCopy' => true
    ];

    public $css = [
    ];
    public $js = [
        'js/app.js'
    ];
    public $depends = [
    ];
}