<?php


namespace frontend\widgets\position;

use frontend\widgets\position\assets\PositionChooserAsset;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\db\ActiveRecord;

class PositionChooser extends Widget
{
    public $form;
    public $model;
    public $positionMap;
    public $carPositionList;
    public $carClassMap = [];
    public $positionFieldName = 'position_id';
    public $carClassFieldName = 'class_id';
    public $positionDisabled = false;
    public $carClassDisabled = false;
    public $positionClassDisabled = false;

    public function init()
    {
        parent::init();

        $this->configValidate();

        PositionChooserAsset::register($this->getView());
    }

    public function run()
    {
        return $this->render($this->getViewName(), [
            'form'                   => $this->form,
            'model'                  => $this->model,
            'positionMap'            => $this->positionMap,
            'carClassMap'            => $this->carClassMap,
            'carPositionList'        => $this->carPositionList,
            'positionFieldName'      => $this->positionFieldName,
            'carClassFieldName'      => $this->carClassFieldName,
            'positionDisabled'       => $this->positionDisabled,
            'carClassDisabled'       => $this->carClassDisabled,
            'positionClassDisabled'  => $this->positionClassDisabled,
        ]);
    }

    private function getViewName()
    {
        return $this->form ? 'index' : 'index_read';
    }

    private function configValidate()
    {
        $error = null;

        if (!($this->model instanceof ActiveRecord)) {
            $error = 'Field "model" must be instance of';
        } elseif (empty($this->carPositionList) || !is_array($this->carPositionList)) {
            $error = 'Field "carPositionList" must be not empty array';
        }

        if (!empty($error)) {
            throw new InvalidConfigException($error);
        }
    }
}