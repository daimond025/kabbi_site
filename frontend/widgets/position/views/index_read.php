<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \yii\base\Model */
/* @var $form \yii\widgets\ActiveForm */
/* @var string $positionFieldName */
/* @var string $carClassFieldName */
/* @var string $positionClassFieldName */
/* @var array $positionMap */
/* @var array $carClassMap */
/* @var array $carPositionList */
/* @var bool $positionDisabled */
/* @var bool $carClassDisabled */
/* @var bool $positionClassDisabled */

?>
<section id="entity_position" class="row">
    <div class="row_label">
        <?= Html::activeLabel($model, $positionFieldName) ?>
    </div>
    <div class="row_input">
        <?= $positionMap[$model->$positionFieldName] ? $positionMap[$model->$positionFieldName] : null ?>
    </div>
</section>

<section id="car_classes" class="row" <? if (!in_array($model->$positionFieldName,
    $carPositionList)): ?>style="display: none;"<? endif ?>>
    <div class="row_label">
        <?= Html::activeLabel($model, $carClassFieldName) ?>
        <a data-tooltip="2" class="tooltip">?
            <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint',
                        'The given tariff will be accessible to executors with a corresponding class of a vehicle.'); ?>
                </span>
        </a>
    </div>
    <div class="row_input">
        <?= $carClassMap[$model->$carClassFieldName] ? $carClassMap[$model->$carClassFieldName] : null ?>
    </div>
</section>