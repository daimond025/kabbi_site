<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \yii\base\Model */
/* @var $form \yii\widgets\ActiveForm */
/* @var string $positionFieldName */
/* @var string $carClassFieldName */
/* @var array $positionMap */
/* @var array $carClassMap */
/* @var array $carPositionList */
/* @var bool $positionDisabled */
/* @var bool $carClassDisabled */
/* @var bool $positionClassDisabled */

?>
<section id="entity_position" class="row">
    <?= $form->field($model, $positionFieldName)->begin(); ?>
    <div class="row_label">
        <?= Html::activeLabel($model, $positionFieldName) ?>
    </div>
    <div class="row_input">
        <?= Html::activeDropDownList($model, $positionFieldName, $positionMap, [
            'prompt'   => t('employee', 'Choose the position'),
            'class'    => 'default_select',
            'disabled' => $positionDisabled ? 'disabled' : false,
        ]) ?>
        <?= Html::error($model, $positionFieldName,
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']) ?>
    </div>
    <?= $form->field($model, $positionFieldName)->end(); ?>
    <? foreach ($carPositionList as $position_id) {
        echo Html::hiddenInput('positon_has_car[]', 1, ['data-position' => $position_id]);
    } ?>
</section>

<section id="car_classes" class="row" <? if (!in_array($model->$positionFieldName,
    $carPositionList)): ?>style="display: none;"<? endif ?>>
    <?= $form->field($model, $carClassFieldName)->begin(); ?>
    <div class="row_label">
        <?= Html::activeLabel($model, $carClassFieldName) ?>
        <a data-tooltip="2" class="tooltip">?
            <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint',
                        'The given tariff will be accessible to executors with a corresponding class of a vehicle.'); ?>
                </span>
        </a>
    </div>
    <div class="row_input">
        <?= Html::activeDropDownList($model, $carClassFieldName, $carClassMap, [
            'prompt'   => t('client-bonus', 'Choose car class'),
            'class'    => 'default_select',
            'disabled' => $carClassDisabled ? 'disabled' : false,
        ]) ?>
        <?= Html::error($model, $carClassFieldName,
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']) ?>
    </div>
    <?= $form->field($model, $carClassFieldName)->end(); ?>
</section>


