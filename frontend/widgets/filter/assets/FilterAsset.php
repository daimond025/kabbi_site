<?php


namespace frontend\widgets\filter\assets;


use yii\web\AssetBundle;

class FilterAsset extends AssetBundle
{
    public $sourcePath = '@frontend/widgets/filter/assets/source/';

    public $publishOptions = [
        'forceCopy' => true
    ];

    public $css = [
        'css/main.css'
    ];
    public $js = [
        'js/app.js'
    ];
    public $depends = [
    ];
}