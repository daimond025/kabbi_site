;(function () {
    'use strict';

    var app = {
        formSelector: 'form.entity_filter',
        textFilterDelay: 700,
        init: function () {
            this.filterEvent();
        },


        checkBoxFilterEvent: function () {
            var _this = this;

            $('body').on('change', '.filter_item_checkbox input[type="checkbox"], .b_sc input[type="checkbox"]', function () {
                var form = $(this).parents('form');
                _this.pjaxReload(form);
            });
        },

        textFilterEvent: function () {
            var _this = this;
            var search_val_old = null;
            $('body').on('keyup', this.formSelector + ' input[type="text"]', function (e) {
                if (e.which != 13) {
                    var input = $(this);
                    setTimeout(function () {
                        var search = input.val();
                        if (search_val_old == search) {
                            return false;
                        }

                        search_val_old = search;

                        var form = input.parents('form');
                        _this.pjaxReload(form);
                    }, _this.textFilterDelay);
                }
            });
        },

        radioBoxFilterEvent: function () {
            var _this = this;
            $('body').on('click', '.filter input[type=radio]', function () {
                var form = $(this).parents('form');
                _this.pjaxReload(form);
            });
        },


        submitFilterEvent: function () {
            var _this = this;
            $('body').off('click', this.formSelector + ' a.button').on('click', this.formSelector + ' a.button', function () {
                var form = $(this).parents('form');
                _this.pjaxReload(form);
            });
        },


        onSubmitFilterEvent: function () {
            this.submitFilterEvent();

        },


        onChangeFilterEvent: function () {
            this.checkBoxFilterEvent();
            this.textFilterEvent();
            this.radioBoxFilterEvent();
            createCheckboxStringOnReady('.b_sc');
        },


        changeRadioBox: function (baseSelector) {
            baseSelector = baseSelector ? baseSelector + ' ' : '';

            $(baseSelector + '.filter input[type=radio]').not(':checked').parents('div').first().find('div').addClass('disabled_select');
            $(baseSelector + '.filter input[type=radio]:checked').parents('div').first().find('div').removeClass('disabled_select');
        },
        onChangeRadioBox: function () {
            var _this = this;

            $(document).on('click', '.filter input[type=radio]', function () {
                var formId = $(this).parents('form').attr('id');
                _this.changeRadioBox('#' + formId);
            });
        },

        changeDatePicker: function (baseSelector) {
            var _this = this;
            baseSelector = baseSelector ? baseSelector + ' ' : '';
            $(baseSelector + '.sdp_filter').each(function () {
                var obj = $(this);
                $(this).datepicker({
                    altField: $(this).prev('input'),
                    onSelect: function (date) {
                        $(this).parent().prev('.a_sc').html(date).removeClass('sel_active'); $(this).parent().hide();

                        var form = $(this).parents('form').first();
                        if (form.data('onchange') == '1') {
                            _this.pjaxReload(form);
                        }
                    }
                });
                if ($(this).hasClass('first_date')) {
                    $(this).datepicker("setDate", '-14');
                }
                obj.closest('.cof_date').find('.a_sc').text(obj.val());
            });
        },


        exportFilterEvent: function () {
            $(document).on('click', '.js-filter-export', function (e) {
                e.preventDefault();

                var form = $(this).parents('form').first();
                var url = $(this).attr('href');

                console.log(url + '?' + form.serialize());

                window.location.replace(url + '?' + form.serialize());
            });
        },


        filterEvent: function () {   //Проверим как запустить - по кномпке или при изменении поля

            var form = $(this.formSelector);
            if (form.data('onchange') == '1') {
                this.onChangeFilterEvent();
            }
            if (form.data('onsubmit') == '1') {
                this.onSubmitFilterEvent();
            }

            this.changeRadioBox();
            this.onChangeRadioBox();
            this.changeDatePicker();

            this.exportFilterEvent();

            $('body').off('click', this.formSelector + ' .a_sc');
            $('body').on('click', this.formSelector + ' .a_sc', function (event) {
                showStyleFilter($(this), event);
                createCheckboxStringOnClick();
            });

        },

        pjaxReload: function (form) {
            var pjaxIds = form.data('pjaxid');
            pjaxIds = pjaxIds.split(' ');
            var count = pjaxIds.length;

            var url = form.attr('action') + '?' + form.serialize();
            var timeout = 15000;

            $.each(pjaxIds, function(index, value){
                if (count > index + 1) {
                    $("#" + value).one('pjax:end', function () {
                        $.pjax.reload({
                            url: url,
                            container: "#" + pjaxIds[index+1],
                            timeout: timeout
                        });
                    });
                }
            });
            $.pjax.reload({
                url: url,
                container: "#" + pjaxIds[0],
                timeout: timeout,
                replace: false,
                push: false
            });
        }
    };

    app.init();

    window.filter = {
        radioBlockInit: function (baseSelector) {
            app.changeDatePicker(baseSelector);
            app.changeRadioBox(baseSelector);
        }
    };
})();