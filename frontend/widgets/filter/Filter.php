<?php


namespace frontend\widgets\filter;


use frontend\widgets\filter\assets\FilterAsset;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

class Filter extends Widget
{
    public $pjaxId;
    /**
     * @var Model
     */
    public $model;
    /**
     * @var string the form submission method. This should be either 'post' or 'get'. Defaults to 'post'.
     *
     * When you set this to 'get' you may see the url parameters repeated on each request.
     * This is because the default value of [[action]] is set to be the current request url and each submit
     * will add new parameters instead of replacing existing ones.
     * You may set [[action]] explicitly to avoid this:
     *
     * ```php
     * $form = ActiveForm::begin([
     *     'method' => 'get',
     *     'action' => ['controller/action'],
     * ]);
     * ```
     */
    public $method = 'get';

    /**
     * @var bool
     * Событие фильтра по кнопке
     */
    public $onSubmit = false;

    /**
     * @var bool
     * Событие фильтра при изменнении полей
     */
    public $onChange = true;

    /**
     * Опции объединяющего блока
     * @var array
     */
    public $options = [
        'class' => 'filter',
    ];

    /**
     * Опции элемента формы
     * @var array
     */
    private $itemOptions = [];

    private $contentOptions = [
        'class' => 'filter_item_1',
        'tag'   => 'div',
    ];

    private $_form;

    private $asset;


    public function init()
    {
        parent::init();

        $this->validate();

        $this->asset = FilterAsset::register($this->getView());

        ob_start();
        $this->_form = ActiveForm::begin([
            'id'          => $this->id,
            'method'      => $this->method,
            'fieldConfig' => [
                'template' => '{input}',
            ],
            'options'     => [
                'class' => 'entity_filter',
                'data'  => [
                    'pjax'     => true,
                    'pjaxId'   => !is_array($this->pjaxId) ? $this->pjaxId : implode(' ', $this->pjaxId),
                    'onSubmit' => (int)$this->onSubmit,
                    'onChange' => (int)$this->onChange,
                ],
            ],
        ]);
    }

    public function run()
    {
        $content = ob_get_clean();

        echo Html::tag('div', $content, $this->options);

        ActiveForm::end();
    }

    /**
     * Проверка на обязательные поля
     * @return bool
     * @throws InvalidConfigException
     */
    public function validate()
    {

        if (empty($this->model)) {
            throw new InvalidConfigException('Field "model" must be configured');
        }

        if (empty($this->pjaxId)) {
            throw new InvalidConfigException('Field "pjaxId" must be configured');
        }

        // Если не указано событие фильтра (все значения ложны)
        if (!$this->onSubmit && !$this->onChange) {
            throw new InvalidConfigException('Event is not selected');
        }

        return true;
    }


    /**
     * Формирует текстовое поле
     *
     * @param string $attribute
     * @param array $itemOptions - опции input'а
     * @param array $contentOptions - опции обрамляющего тега
     * @return string
     *
     * $itemOptions = [
     *      'class' => 'example_input',
     *      'placeHolder => 'Название',
     *      'style' => 'display: block'
     * ]
     *
     * $contentOptions = [
     *      'tag' => 'p',
     *      'class' => 'example_input',
     *      'style' => 'border: solid 1px black'
     * ]
     */
    public function input($attribute, $itemOptions = [], $contentOptions = [])
    {
        $itemOptions = ArrayHelper::merge($this->itemOptions, $itemOptions, [
            'data-activity' => 'active',
            'autocomplete'  => 'off',
        ]);
        $contentOptions = ArrayHelper::merge($this->contentOptions, $contentOptions);

        if (empty($itemOptions['placeholder'])) {
            $itemOptions['placeholder'] = $this->model->getAttributeLabel($attribute);
        }

        $innerHtml = Html::activeTextInput($this->model, $attribute, $itemOptions);

        return $this->content($innerHtml, $contentOptions);
    }

    /**
     * @param string $attribute
     * @param array $items
     * @param array $itemOptions
     * @param array $contentOptions
     * @return string
     */
    public function checkboxList($attribute, $items, $itemOptions = [], $contentOptions = [])
    {
        $itemOptions = ArrayHelper::merge($this->itemOptions, $itemOptions);
        $contentOptions = ArrayHelper::merge($this->contentOptions, $contentOptions);
        $label = empty($itemOptions['label']) ? $this->model->getAttributeLabel($attribute) : $itemOptions['label'];
        unset($itemOptions['label']);

        $innerForm = Html::activeCheckboxList($this->model, $attribute, $items, $itemOptions);

        $checkbox_innerHtml = Html::tag('div', $innerForm, ['class' => 'b_sc']);

        $label_innerHtml = Html::a($label, null, ['class' => 'a_sc', 'rel' => $label]);
        $unit_innerHtml = Html::tag('div', $label_innerHtml . $checkbox_innerHtml, ['class' => 'select_checkbox']);

        return $this->content($unit_innerHtml, $contentOptions);
    }

    /**
     * @param string $attribute
     * @param array $itemOptions
     * @param array $contentOptions
     * @return string
     */
    public function checkbox($attribute, $itemOptions = [], $contentOptions = [])
    {
        $itemOptions = ArrayHelper::merge($this->itemOptions, $itemOptions);
        $contentOptions = ArrayHelper::merge($this->contentOptions, $contentOptions);
        unset($itemOptions['label']);

        $innerForm = Html::activeCheckbox($this->model, $attribute, $itemOptions);
        $innerHtml = Html::tag('label', $innerForm);

        return $this->content(Html::tag('div', $innerHtml, ['class' => 'filter_item_checkbox']), $contentOptions);
    }

    /**
     * @param string $attribute
     * @param array $items
     * @param array $itemOptions
     * @param array $contentOptions
     * @return string
     */
    public function dropDownList($attribute, $items, $itemOptions = [], $contentOptions = [])
    {
        $itemOptions = ArrayHelper::merge($this->itemOptions, $itemOptions, ['class' => 'default_select']);
        $contentOptions = ArrayHelper::merge($this->contentOptions, $contentOptions);

        $innerForm = Html::activeDropDownList($this->model, $attribute, $items, $itemOptions);

        return $this->content($innerForm, $contentOptions);
    }

    /**
     * @param string $attribute
     * @param array $items
     * array [
     *  '0' => 'Now',
     *  '1' => [
     *      'label' => 'Period',
     *      'content' => '...DatePicker...',
     *  ]
     * ]
     * @param array $itemOptions
     * @param array $contentOptions
     * @return string
     */
    public function radioList($attribute, $items, $itemOptions = [], $contentOptions = [])
    {
        $itemOptions = ArrayHelper::merge($this->itemOptions, ['class' => 'mgn_item'], $itemOptions);
        $contentOptions = ArrayHelper::merge($this->contentOptions, $contentOptions);
        unset($itemOptions['label']);

        $_items = [];
        $_content = [];
        foreach ($items as $value => $item) {
            if (!is_array($item)) {
                $_items[$value] = $item;
                $_content[$value] = '';
            } else {
                $name = $item['label'] ?: $value;
                $_items[$value] = $name;
                $_content[$value] = isset($item['content']) ? $item['content'] : '';
            }
        }

        $innerHtml = Html::activeRadioList($this->model, $attribute, $_items, [
            'item' => function($index, $label, $name, $checked, $value) use ($_content, $itemOptions){
                $formInner = Html::label(Html::radio($name, $checked, ['value' => $value]) . $label) . $_content[$value];
                return Html::tag('div', $formInner, $itemOptions);
            }
        ]);

        return $this->content($innerHtml, $contentOptions);
    }

    /**
     * @param string $attributes
     * @param array $value
     * @param array $contentOptions
     * @return string
     */
    public function datePeriod($attributes, $value, $contentOptions = [])
    {
        $contentOptions = ArrayHelper::merge($this->contentOptions, $contentOptions);

        $innerHtml = $this->calendar($attributes[0], $value[0], ['class' => 'cof_date_first']) . '<hr>'
            . $this->calendar($attributes[1], $value[1], ['class' => 'cof_date_second']);
        return $this->content($innerHtml, $contentOptions);
    }

    /**
     * @param string $attribute
     * @param string $value
     * @param array $contentOptions
     * @return string
     */
    public function calendar($attribute, $value, $contentOptions = [])
    {
        $contentOptions = ArrayHelper::merge($this->contentOptions, $contentOptions);

        $label_innerHtml = Html::a($value, null,['class' => 's_datepicker a_sc select']);

        $innerForm = Html::activeTextInput($this->model, $attribute, [
            'class' => 'sdp_input',
            'value' => $value
        ]);
        $panel_innerHtml = Html::tag('div', '', ['class' => 'sdp_filter ' . $attribute]);

        $date_innerHtml = Html::tag('div', $innerForm . $panel_innerHtml, ['class' => 's_datepicker_content b_sc', 'style' => 'display: none;']);

        $innerHtml = Html::tag('div', $label_innerHtml . $date_innerHtml, ['class' => 'cof_date']);


        return $this->content($innerHtml, $contentOptions);
    }

    public function export($items, $contentOptions = [], $title='Export')
    {
        if (!is_array($items)) {
            throw new InvalidConfigException('Type $items is not array');
        }

        $contentOptions = ArrayHelper::merge($this->contentOptions, $contentOptions);

        $element = [];
        foreach ($items as $key => $item) {
            $label = $item['label'] ?: $key;
            $url = $item['url'] ?: '';
            $element[] = Html::a($label, $url, ['class' => 'js-filter-export']);
        }
        $select_innerHtml = Html::tag('div', implode(PHP_EOL, $element), ['class' => 'context_menu_content b_sc js-no-stop']);
        $link_innerHtml = Html::a(t('widgets', $title), null, ['class' => 'context_menu a_sc select']);
        $innerHtml = Html::tag('div', $link_innerHtml . $select_innerHtml,
            ['class' => 'context_menu_wrap']
        );
        return $this->content($innerHtml, $contentOptions);
    }

    /**
     * @param $label
     * @param array $itemOptions
     * @param array $contentOptions
     * @return string
     */
    public function button($label, $itemOptions = [], $contentOptions = [])
    {
        $itemOptions = ArrayHelper::merge($this->itemOptions, $itemOptions, ['class' => 'button']);
        $contentOptions = ArrayHelper::merge($this->contentOptions, $contentOptions);

//        $innerForm = Html::submitButton($label, $itemOptions);
        $innerForm = Html::a($label, null, $itemOptions);

        return $this->content($innerForm, $contentOptions);
    }

    /**
     * @param $text
     * @param array $options
     * @return string
     */
    public function content($text, $options = [])
    {

        $options = ArrayHelper::merge($this->contentOptions, $options);
        $tag = $options['tag'];
        unset($options['tag']);

        return Html::tag($tag, $text, $options);
    }

}