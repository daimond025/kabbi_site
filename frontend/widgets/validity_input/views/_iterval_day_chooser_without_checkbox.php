<div class="iterval_day_chooser">
    <label style="display: none;"><input type="radio" name="<?=$name?>" class="has_interval js-interval-chooser-radio" value="period" checked/> <?= t('app', 'Interval') ?></label>
    <label><input type="checkbox" class="js-interval-chooser" checked><?= t('app', 'Interval') ?></label>
    <div class="dowi_interval" style="display: block; padding: 0; margin-bottom: 15px;">
        <input type="time"/><span>-</span><input type="time"/>
    </div>
    <p class="error" style="color: red; margin-left: 5px; display: none"><?=t('error', 'Time intersection')?></p>
</div>