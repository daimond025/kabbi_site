<?php
use yii\helpers\Html;
use yii\helpers\Url;

?>
<section class="row choosing_date_section">
    <div class="row_label"><label><?= $label ?></label></div>
    <div class="row_input" style="position:relative;">
        <a href="#<?= $id ?>" class="plus_icon"><i></i></a>
        <div class="tags_input">
            <? if ($attr_name !== null): ?>
                <?= Html::activeHiddenInput($model, $attr_name, ['id' => 'field_' . $id]) ?>
            <? endif; ?>
            <ul>
                <?php
                if (!empty($data)):
                    foreach ($data as $date):
                        $day = explode('|', $date);
                        if (strripos($day[0], '.') == 2) {
                            $params = explode(".", $day[0]);
                            $formatedDate = $params[0] . ' ' . $genitiveMonths[$params[1]];
                        } else {
                            if (isset($days[$day[0]])) {
                                $formatedDate = $days[$day[0]];
                            } else {
                                $formatedDate = \Yii::$app->formatter->asDate(strtotime($day[0]), 'shortDate');
                            }
                        }

                        $formatedDate .= ' ' . getValue($day[1]);
                        $formatedDate = Html::encode(trim($formatedDate));

                        if (!empty($formatedDate)):
                            ?>
                            <li><?= $formatedDate ?> <a></a><input type="text"
                                                                   name="<?= $input_name . '[ActiveDate][]' ?>"
                                                                   value="<?= Html::encode($date); ?>"></li>
                        <? endif ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>
        </div>
        <? if ($show_upload_celebration): ?>
            <a href="<?= Url::to('/tariff/tariff-client/holidays') ?>" class="load_defaults_for_job"><?= t('app',
                    'Upload celebration calendar') ?></a>
        <? endif ?>
        <div class="choosing_date">
            <div id="<?= $id ?>">
                <i class="button close"></i>
<!--                <h2>--><?//= t('app', 'Add') ?><!--</h2>-->
                <div class="grid_3">
                    <div class="grid_item">
                        <select class="default_select holidays_choser">
                            <option value="week" data-holydaytype="ht01" selected><?= t('taxi_tariff', 'Every week') ?></option>
                            <option value="year" data-holydaytype="ht02"><?= t('taxi_tariff', 'Every year') ?></option>
                            <option value="once" data-holydaytype="ht03"><?= t('taxi_tariff', 'Once') ?></option>
                            <option value="everyday" data-holydaytype="ht04"><?= t('taxi_tariff', 'Everyday') ?></option>
                        </select>
                    </div>
                </div>
                <div class="ht01 activeh">
                    <ul>
                        <?php
                        foreach ($days as $key => $day):
                            ?>
                            <li>
                                <label class="day_of_week_item"><input name="day" type="checkbox"
                                                                       value="<?= $key; ?>"/><?= Html::encode($day); ?>
                                </label>
                                <?
                                if ($show_time_interval) {
                                    echo $this->render('_iterval_day_chooser', ['name' => 'dowi_' . $key . '_' . $id]);
                                }
                                ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="ht02">
                    <div class="grid_3" style="margin-top: -50px;">
                        <div class="grid_item" style="margin-left: 2%;">
                            <?= Html::dropDownList('day_numbers', null, $monthDayNumbers, [
                                'class'            => 'default_select',
                                'data-placeholder' => t('taxi_tariff', 'Day'),
                            ]) ?>
                        </div>
                        <div class="grid_item">
                            <select class="default_select" data-placeholder="<?= t('taxi_tariff', 'Month') ?>">
                                <?php
                                foreach ($months as $key => $month) {
                                    echo Html::tag('option', $month, [
                                        'value' => $key,
                                        'data'  => [
                                            'genitive-month' => mb_strtolower($genitiveMonths[$key]),
                                        ],
                                    ]);
                                };
                                ?>
                            </select>
                        </div>
                    </div>
                        <?
                        if ($show_time_interval) {
                            echo $this->render('_iterval_day_chooser', ['name' => 'doymi_' . $id]);
                        }
                        ?>
                </div>
                <div class="ht03">
                    <div style="height: 130px;">
                        <div class="js-calendar"><input type="text" style="width: 32%;"/></div>
                        <?
                        if ($show_time_interval) {
                            echo $this->render('_iterval_day_chooser', ['name' => 'doyi_' . $id]);
                        }
                        ?>
                    </div>
                </div>
                <div class="ht04">
                    <?= $this->render('_iterval_day_chooser_without_checkbox', ['name' => 'doyi_' . $id]); ?>
                </div>
                <button data-type="<?= $input_name ?>" class="add_holiday"><?= t('app', 'Add') ?></button>
            </div>
        </div>
    </div>
</section>