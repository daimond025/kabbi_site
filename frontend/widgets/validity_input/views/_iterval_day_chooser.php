<div class="iterval_day_chooser">
    <label style="display: none;"><input type="radio" name="<?=$name?>" checked="checked" class="js-interval-chooser-radio" value="all" /> <?= t('app', 'All day') ?></label>
    <label style="display: none;"><input type="radio" name="<?=$name?>" class="has_interval js-interval-chooser-radio" value="period" /> <?= t('app', 'Interval') ?></label>
    <label><input type="checkbox" class="js-interval-chooser"><?= t('app', 'Interval') ?></label>
    <div class="dowi_interval">
        <input type="time"/><span>-</span><input type="time"/>
    </div>
    <p class="error" style="color: red; margin-left: 5px; display: none"><?=t('error', 'Time intersection')?></p>
</div>