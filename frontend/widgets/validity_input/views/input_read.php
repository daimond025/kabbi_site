<?php

use yii\helpers\Html;
?>
<section class="row">
    <div class="row_label"><label><?= $label ?></label></div>
    <div class="row_input">
        <ul>
            <?php
            if (!empty($data)):
                foreach ($data as $date):
                    $day = explode('|', $date);
                    if (strripos($day[0], '.') == 2) {
                        $params = explode(".", $day[0]);
                        $formatedDate = $params[0] . ' ' . $genitiveMonths[$params[1]];
                    } else {
                        if (isset($days[$day[0]])) {
                            $formatedDate = $days[$day[0]];
                        } else {
                            $formatedDate = $day[0];
                        }
                    }

                    $formatedDate .= ' ' . getValue($day[1]);
                    ?>
                    <div class="row_input"><?= Html::encode(trim($formatedDate)); ?></div>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    </div>
</section>