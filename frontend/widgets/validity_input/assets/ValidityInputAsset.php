<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\widgets\validity_input\assets;

use yii\web\AssetBundle;

class ValidityInputAsset extends AssetBundle
{
    public $sourcePath = '@frontend/widgets/validity_input/assets/';

    public $publishOptions = [
        'forceCopy' => true
    ];

    public $css = [
        'css/widget.css',
    ];
    public $js = [
        'js/init.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}