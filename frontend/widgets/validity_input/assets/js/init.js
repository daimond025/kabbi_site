/*global jQuery*/
;(function ($) {
  'use strict';

  var app = {
    colorbox_width: 300,
    separator: '|',
    checked_inputs: null,
    init: function () {
      this.initDatepicker();
      this.periodSelectionEventListener();
      this.loadStandartHolidaysEventListener();
      this.dayOfWeekCheckingEventListener();
      this.intervalDayChoosingEventListener();
      this.addDateEventListener();
      this.removeDateEventListener();
      this.openChoosingPeriodEventListener();
      this.colorboxInit();
      this.openWindowButtonClickEventListener();
    },
    initDatepicker: function () {
      $('.js-calendar').datepicker('destroy');
      $('.js-calendar').each(function () {
        var $input = $(this).find('input');
        $(this).datepicker({
          altField: $input
          // onChangeMonthYear: function () {
          //     setTimeout(function () {
          //         $('.add_city_organization').colorbox.resize();
          //     }, 20);
          // }
        });
      });
    },
    loadStandartHolidaysEventListener: function () {
      var _this = this;
      $('body').off('click', '.load_defaults_for_job').on('click', '.load_defaults_for_job', function (e) {
        e.preventDefault();
        var a = $(this);
        $.post(
          a.attr('href'),
          {},
          function (html) {
            a.parent('div.row_input').find('.tags_input ul').append(html);
            _this.cleanError();
          }
        );
      });
    },
    cleanError: function (tags_input) {
      var tags_input = tags_input || $('div.active .tags_input');
      tags_input.parents('.input_error').removeClass('input_error');
      tags_input.find('input[type=hidden]').val(1);
    },
    periodSelectionEventListener: function () {
      $('body').off('change', '.holidays_choser').on('change', '.holidays_choser', function () {
        var hcid = '.' + $(this).find('option:selected').data('holydaytype');
        $(this).parents('.choosing_date').find('.activeh').removeClass('activeh');
        $(this).parents('.choosing_date').find(hcid).addClass('activeh');
        //$.colorbox.resize();
      });
    },
    dayOfWeekCheckingEventListener: function () {
      var _this = this;
      $('body').off('click', '.ht01 .day_of_week_item input').on('click', '.ht01 .day_of_week_item input', function () {
        _this.displayDayOfWeekPeriodBlock($(this));
      });
    },
    displayDayOfWeekPeriodBlock: function (day_of_week_checkbox_input) {
      if (day_of_week_checkbox_input.is(':checked')) {
        day_of_week_checkbox_input.parents('li').addClass('checked');
        //$.colorbox.resize();
      } else {
        var li = day_of_week_checkbox_input.parents('li');
        li.removeClass('checked');

        var iterval_day_chooser = li.find('.iterval_day_chooser');
        iterval_day_chooser.find('label:first input').click();
        iterval_day_chooser.find('.dowi_interval input').val('');
        iterval_day_chooser.find('.input_error').removeClass('input_error');
        iterval_day_chooser.find('.error').hide();
      }
    },
    intervalDayChoosingEventListener: function () {
      $('body').off('click', '.iterval_day_chooser label input[type="checkbox"]').on('click', '.iterval_day_chooser label input[type="checkbox"]', function () {
        $('.iterval_day_chooser .error').hide();
        $(this).parents('.iterval_day_chooser').find('.js-interval-chooser-radio').prop('checked', false);
        if ($(this).is(':checked')) {
          $(this).parents('.iterval_day_chooser').find('.js-interval-chooser-radio[value="period"]').prop('checked', true);
          $(this).parents('.iterval_day_chooser').find('.dowi_interval').show();
        } else {
          $(this).parents('.iterval_day_chooser').find('.js-interval-chooser-radio[value="all"]').prop('checked', true);
          $(this).parents('.iterval_day_chooser').find('.dowi_interval').hide();
        }
        //$.colorbox.resize();
      });
    },
    addDateEventListener: function () {
      var _this = this;
      $('body').off('click', '.add_holiday').on('click', '.add_holiday', function (e) {
        e.preventDefault();
        var tariff_type = $(this).data('type');
        var holiday_text = '';
        var input_val = '';
        var dates = [];
        var parent = $(this).parent();
        var error = [];
        var pushDate = function (iterval_day_chooser, input_val, holiday_text) {
          var time_period_text = '';
          var arr_time_period = null;
          iterval_day_chooser.find('.error').hide();

          if (iterval_day_chooser.find('.has_interval').prop('checked') || iterval_day_chooser.parent().hasClass('ht04')) {
            var time_interval_inputs = iterval_day_chooser.find('.dowi_interval input');
            arr_time_period = _this.getTimeInterval(time_interval_inputs);

            //Проверка валидности выбранного периода времени
            if (!_this.checkValidTimeInterval(arr_time_period, iterval_day_chooser.parent().hasClass('ht04'))) {
              time_interval_inputs.addClass('input_error');
              return false;
            }
          }

          //Проверка на наличие пересечения интервалов времени с уже имеющимися датами
          if (!_this.checkTimeIntersection(input_val, arr_time_period)) {
            iterval_day_chooser.find('.error').show();
            //$.colorbox.resize();
            return false;
          }

          console.log(iterval_day_chooser.find('.has_interval').prop('checked'));
          console.log(time_interval_inputs);
          console.log(arr_time_period);

          if (Array.isArray(arr_time_period)) {
            time_period_text = arr_time_period.join('-');
            input_val += _this.separator + time_period_text;
            holiday_text += ' ' + time_period_text;
          }

          dates.push({'input_val': input_val, 'holiday_text': holiday_text});

          return true;
        };

        parent.find('.input_error').removeClass('input_error');

        if (parent.find('.ht01').hasClass('activeh')) { //Каждую неделю
          _this.checked_inputs = parent.find('.ht01 .day_of_week_item').find('input:checkbox:checked');
          _this.checked_inputs.each(function () {
            var input_checked = $(this);
            var iterval_day_chooser = input_checked.parents('li').find('.iterval_day_chooser');
            input_val = input_checked.val();
            holiday_text = input_checked.parent().text();

            if (!pushDate(iterval_day_chooser, input_val, holiday_text)) {
              error.push(true);
              return;
            }
          });
        } else {
          var iterval_day_chooser = parent.find('.activeh .iterval_day_chooser');
          if (parent.find('.ht02').hasClass('activeh')) { //Каждый год
            parent.find('.ht02 option:selected').each(function () {
              var genitiveMonth = $(this).data('genitive-month');
              if (!genitiveMonth) {
                input_val += $(this).val();
                holiday_text += $(this).html();
              } else {
                input_val += '.' + $(this).val();
                holiday_text += ' ' + genitiveMonth;
              }
            });
          } else { //1 раз
            if (parent.find('.ht03').hasClass('activeh')) {
              var $datepicker = parent.find('.ht03').find('.js-calendar');
              var date = $datepicker.datepicker('getDate');
              holiday_text = $.datepicker.formatDate(
                $datepicker.datepicker('option', 'dateFormat'),
                date);
              input_val = $.datepicker.formatDate('dd.mm.yy', date);
            } else { // Каждый день
              var iterval_day_chooser = $('.ht04 .iterval_day_chooser');

              holiday_text = '';
              input_val = '';

              iterval_day_chooser.find('input').each(function () {
                holiday_text = holiday_text + (holiday_text === '') ? '' : ' - ' + $(this).val();
                input_val = input_val + (input_val === '') ? '' : '-' + $(this).val();
              });
            }
          }

          if (!pushDate(iterval_day_chooser, input_val, holiday_text)) {
            error.push(true);
          }
        }

        if (error.length) {
          return false;
        }

        var tags_input = $('div.active .tags_input');
        for (var i = 0; i < dates.length; i++) {
          tags_input.find('ul').append("<li>" + dates[i].holiday_text + " <a></a><input type='text' name='" + tariff_type +
            "[ActiveDate][]' value='" + dates[i].input_val + "'/></li>");
        }

        _this.cleanError(tags_input);

        $('.choosing_date').removeClass('active');
        _this.colorboxCloseEventListener();
      });
    },
    checkTimeIntersection: function (input_val, arr_time_period) {
      var choosed_tags = $('.choosing_date_section').filter('.active').find('.tags_input [value^="' + input_val + '"]');

      if (!choosed_tags.size()) {
        return true;
      }

      if (!Array.isArray(arr_time_period)) {
        return false;
      }

      var _this = this;
      var error = false;
      var choosed_hour_1 = this.getTimeByTimeString(arr_time_period[0]);
      var choosed_hour_2 = this.getTimeByTimeString(arr_time_period[1]);

      choosed_tags.each(function () {
        var input_val = $(this).val();
        var val_parts = input_val.split(_this.separator);

        if (val_parts.length > 1) {
          var item_periods = val_parts[1].split('-');
          var item_hour_1 = _this.getTimeByTimeString(item_periods[0]);
          var item_hour_2 = _this.getTimeByTimeString(item_periods[1]);

          //Проверка пересечения текущего, выбранного периода времени, с сохраненными
          if ((item_hour_1 <= choosed_hour_1 && choosed_hour_1 < item_hour_2) ||
            (item_hour_1 < choosed_hour_2 && choosed_hour_2 <= item_hour_2) ||
            (choosed_hour_1 <= item_hour_1 && item_hour_1 < choosed_hour_2) ||
            (choosed_hour_1 < item_hour_2 && item_hour_2 <= choosed_hour_2)
          ) {
            error = true;
          }
        } else {
          error = true;
        }

        if (error) {
          return false;
        }
      });

      return !error;
    },
    getTimeByTimeString: function (time) {
      var time_parts = time.split(':');
      var date = new Date();

      date.setHours(time_parts[0], time_parts[1], 0, 0);

      return date;
    },
    colorboxCloseEventListener: function () {
      var _this = this;

      var choosing_date_section = $('.choosing_date_section').filter('.active');
      //Восстанавливаем вкладку первого селекта
      if (_this.checked_inputs !== null) {
        _this.checked_inputs.each(function () {
          $(this).prop('checked', false);
          _this.displayDayOfWeekPeriodBlock($(this));
        });
      }

      //Восстанавливаем вкладку второго и третьего селекта
      var iterval_day_chooser = choosing_date_section.find('.ht02 .iterval_day_chooser, .ht03 .iterval_day_chooser');

      iterval_day_chooser.find('.input_error').removeClass('input_error');
      iterval_day_chooser.find('.error').hide();
      iterval_day_chooser.find('.js-interval-chooser').prop('checked', false);
      iterval_day_chooser.find('.dowi_interval').hide();
      iterval_day_chooser.find('.dowi_interval input').val('');
      iterval_day_chooser.find('input[value="all"]').click();

      choosing_date_section.removeClass('active');
    },
    getTimeInterval: function (jq_time_inputs) {
      var arr_time_period = [];
      jq_time_inputs.each(function () {
        arr_time_period.push($(this).val());
      });

      return arr_time_period;
    },
    checkValidTimeInterval: function (arr_time_period, every_day_mode) {
      var cur_date_1 = this.getTimeByTimeString(arr_time_period[0]);
      var cur_date_2 = this.getTimeByTimeString(arr_time_period[1]);

      if(every_day_mode){
        return (arr_time_period[0] !== arr_time_period[1]) && (arr_time_period[0] !== '') && (arr_time_period[1] !== '');
      } else{
        return (cur_date_2 - cur_date_1) > 0;
      }


    },
    removeDateEventListener: function () {
      $('body').off('click', '.tags_input ul li a').on('click', '.tags_input ul li a', function (e) {
        e.stopPropagation();
        var ul = $(this).parents('ul');
        $(this).parents('li').empty().remove();

        if (ul.find('li').size() == 0) {
          ul.prev().val('');
        }
      });
    },
    colorboxInit: function () {
      var _this = this;
      $('body').off('click', '.choosing_date_section .plus_icon').on('click', '.choosing_date_section .plus_icon', function (e) {
        e.preventDefault();
        $('.choosing_date').addClass('active');
      });

      $('body').off('click', '.choosing_date .close').on('click', '.choosing_date .close', function () {
        $('.choosing_date').removeClass('active');
        _this.colorboxCloseEventListener();
      });
      // $(".choosing_date_section .plus_icon").colorbox({
      //     inline: true,
      //     width: _this.colorbox_width + "px",
      //     onClosed: _this.colorboxCloseEventListener()
      // });
    },
    openWindowButtonClickEventListener: function () {
      $(document).off('click', '.choosing_date_section .plus_icon').on('click', '.choosing_date_section .plus_icon', function () {
        $(this).parents('.choosing_date_section').addClass('active');
      });
    },
    openChoosingPeriodEventListener: function () {
      $('body').off('click', '.tags_input').on('click', '.tags_input', function () {
        $(this).closest('.row').find('.plus_icon').click();
      });
    }
  };

  window.validyInput = app;

  app.init();
})(jQuery);