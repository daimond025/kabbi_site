<?php

namespace frontend\widgets\validity_input;

use frontend\widgets\validity_input\assets\ValidityInputAsset;
use common\helpers\DateTimeHelper;

class ValidityInput extends \yii\base\Widget
{
    const VIEW_TEMPLATE = 'input';
    const VIEW_READ_TEMPLATE = 'input_read';

    public $id;
    public $data;
    public $label;
    public $input_name;
    public $show_upload_celebration = false;
    public $show_time_interval = true;
    public $read_view = false;
    public $model = null;
    public $attr_name = null;

    public function init()
    {
        parent::init();

        ValidityInputAsset::register($this->getView());
    }

    public function run()
    {
        $view = $this->getViewTemplate();

        return $this->render($view, $this->getViewCongigByTempalate($view));
    }

    private function getViewTemplate()
    {
        return $this->read_view ? self::VIEW_READ_TEMPLATE : self::VIEW_TEMPLATE;
    }

    private function getViewCongigByTempalate($view)
    {
        if ($view === self::VIEW_TEMPLATE) {
            return [
                'id'                      => $this->id,
                'data'                    => $this->data,
                'label'                   => $this->label,
                'days'                    => DateTimeHelper::getDayOfWeekList(),
                'months'                  => DateTimeHelper::getMonthList(),
                'genitiveMonths'          => DateTimeHelper::getGenitiveMonths(),
                'monthDayNumbers'         => DateTimeHelper::getMonthDayNumbers(),
                'input_name'              => $this->input_name,
                'show_upload_celebration' => $this->show_upload_celebration,
                'show_time_interval'      => $this->show_time_interval,
                'model'                   => $this->model,
                'attr_name'               => $this->attr_name,
            ];
        }

        return [
            'data'           => $this->data,
            'label'          => $this->label,
            'days'           => DateTimeHelper::getDayOfWeekList(),
            'genitiveMonths' => DateTimeHelper::getGenitiveMonths(),
        ];
    }

}
