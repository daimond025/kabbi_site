<?php

namespace frontend\widgets\file;

class FileMultipleRead extends \yii\base\Widget
{
    public $models;
    public $attribute;


    public function run()
    {
        return $this->render('read_multiple', [
            'models'        => $this->models,
            'attribute'     => $this->attribute,
        ]);
    }
}
