<?php

namespace frontend\widgets\file;

use frontend\widgets\file\assets\FileAsset;

class File extends \yii\base\Widget
{
    /**
     * @var object yii\widgets\ActiveForm
     */
    public $form;
    public $model;
    public $attribute;
    public $behavior_name;
    public $ajaxUpload = false;
    public $uploadAction;
    public $mimeTypes = [
        'image/gif',
        'image/jpeg',
        'image/png',
        'image/tiff',
    ];

    private $_behavior;
    /**
     * @var integer Filse size (Mb)
     */
    public $max_file_size;

    public function init()
    {
        parent::init();
        FileAsset::register($this->getView());
        $this->_behavior = $this->behavior_name ? $this->model->getBehavior($this->behavior_name) : $this->model;
    }

    public function run()
    {
        return $this->render('file', [
            'form'          => $this->form,
            'model'         => $this->model,
            'attribute'     => $this->attribute,
            'behavior'      => $this->_behavior,
            'max_file_size' => $this->getMaxFileSize(),
            'ajaxUpload'    => $this->ajaxUpload,
            'uploadAction'  => $this->uploadAction,
            'mimeTypes'     => $this->mimeTypes,
        ]);
    }

    private function getMaxFileSize()
    {
        return empty($this->max_file_size) ? getUploadMaxFileSize() : $this->max_file_size * 1024 * 1024;
    }

}
