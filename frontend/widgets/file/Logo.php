<?php

namespace frontend\widgets\file;

use frontend\widgets\file\assets\LogoAsset;

class Logo extends \yii\base\Widget
{

    /**
     * @var object yii\widgets\ActiveForm
     */
    public $form;
    public $model;
    public $attribute;
    public $cropArribute = 'cropParams';
    /**
     * @var integer Filse size (Mb)
     */
    public $max_file_size;
    public $min_height = 300;
    public $min_width = 300;
    public $ajax = false;
    public $uploadAction;
    public $suggest;

    public function init()
    {
        parent::init();

        LogoAsset::register($this->getView());
    }

    public function run()
    {
        return $this->render('logo', [
            'form'          => $this->form,
            'model'         => $this->model,
            'attribute'     => $this->attribute,
            'min_height'    => $this->min_height,
            'min_width'     => $this->min_width,
            'max_file_size' => $this->getMaxFileSize(),
            'cropArribute'  => $this->cropArribute,
            'ajax'          => $this->ajax,
            'uploadAction'  => $this->uploadAction,
            'suggest'       => $this->suggest,
        ]);
    }

    private function getMaxFileSize()
    {
        return empty($this->max_file_size) ? getUploadMaxFileSize() : $this->max_file_size * 1024 * 1024;
    }

}
