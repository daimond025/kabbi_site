<?php

/* @var $models \yii\base\Model[] */

$isEmptyScan = empty($models) || (count($models) == 1 && $models[0]->isNewRecord);
?>

    <? if (!$isEmptyScan): ?>
        <div class="row_label"></div>
        <div class="row_input" style="background: none;">
                <?
                /** @var $model \yii\db\ActiveRecord */
                foreach ($models as $model):?>
                    <? if ($model->isFileExists($model->$attribute)):
                        ?>
                        <a class="input_file_logo logo cboxElement"
                           href="<?= $model->getPicturePath($model->$attribute) ?>"
                           style="border: none">
                            <div class="row_input" style="background: none"><span><img
                                            src="<?= $model->getPicturePath($model->$attribute, false) ?>"></span>
                            </div>
                        </a>
                    <? endif ?>
                <? endforeach; ?>
        </div>
    <? endif ?>
