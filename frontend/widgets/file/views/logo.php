<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var bool $ajax */
/* @var string $cropArribute */
/* @var string $attribute */
/* @var integer $max_file_size */
/* @var integer $min_width */
/* @var integer $min_height */
/* @var string $uploadAction */

$cropInputName = Html::getInputName($model, $cropArribute);
?>
<section class="row" id="logo" data-ajax="<?= (int)$ajax ?>" data-action="<?= $uploadAction ?>">
    <?= $form->field($model, $attribute)->begin(); ?>
    <div class='row_label'>
        <?php echo $suggest?>
    </div>
    <div class="row_input">
        <span class="file-input">
            <? if ($model->isFileExists($model->$attribute)):
                $logo_path = $model->getPicturePath($model->$attribute);
                ?>
                <div class="input_file_logo input_file_loaded">
                    <a class="logo" href="<?= $logo_path ?>">
                        <?= $model->getPictureHtml($model->$attribute, false) ?>
                    </a>
                </div>
            <? else: ?>
                <div class="input_file_logo"></div>
            <? endif ?>

            <div class="input_file_replace btn-file">
                <a><b><?= t('app', 'Upload photo') ?></b></a>
                <?= Html::activeFileInput($model, $attribute, [
                    'data' => [
                        'filesize' => $max_file_size,
                        'width'    => $min_width,
                        'height'   => $min_height,
                        'crop'     => $cropInputName,
                    ],
                ]) ?>
            </div>
        </span>
    </div>
    <div class="help-block" style="margin-left: 27.5%; margin-top: 10px; color: red"></div>
    <p class="logo_error" data-error="bad_size" style="color: red; margin-left: 27.5%; display: none"><?= t('app',
            'Logo size should be more than {min_height}x{min_width} px',
            ['min_height' => $min_width, 'min_width' => $min_height]); ?></p>
    <div style="display: none">
        <div id="logo_resize">
            <h3><?= t('app', 'Highlight the desired fragment') ?></h3>
            <div>
                <img id="logo_orig" src="" alt="" title="" style="max-width: 800px; max-height: 800px;"/>
                <div class="preview" style="width: <?= $min_width ?>px; height: <?= $min_height ?>px">
                    <img style="position: relative;" src="" alt="" title=""/>
                </div>
            </div>
            <button id="choose_size" style="margin-top: 10px"><?= t('app', 'Choose') ?></button>
        </div>
    </div>
    <?= $form->field($model, $attribute)->end(); ?>
    <?= Html::hiddenInput($cropInputName . '[x]', 0) ?>
    <?= Html::hiddenInput($cropInputName . '[y]', 0) ?>
    <?= Html::hiddenInput($cropInputName . '[w]', $min_width) ?>
    <?= Html::hiddenInput($cropInputName . '[h]', $min_height) ?>
</section>