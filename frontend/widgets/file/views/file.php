<?php

/* @var bool $ajaxUpload */
/* @var string $uploadAction */
/* @var integer $max_file_size */
/* @var string $attribute */
/* @var array $mimeTypes */
/* @var object $behavior */
/* @var $model \yii\base\Model */

use yii\helpers\Html;

$mimeTypesStr = '"' . implode('", "', $mimeTypes) . '"';
$this->registerJs('fileUpload.mimeTypes = [' . $mimeTypesStr . ']');
?>
<section class="row file">
    <?= $form->field($model, $attribute)->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($model, $attribute) ?></div>
    <div class="row_input">
        <? if ($behavior->isFileExists($model->$attribute)):
            $logo_path = $behavior->getPicturePath($model->$attribute);
            ?>
            <div class="input_file_logo input_file_loaded">
                <a class="logo" href="<?= $logo_path ?>">
                    <?= $behavior->getPictureHtml($model->$attribute, false) ?>
                </a>
            </div>
        <? else: ?>
            <div class="input_file_logo" style="display: none"></div>
        <? endif ?>
        <div class="input_file_replace btn-file">
            <a><b><?= t('app', 'Upload photo') ?></b></a>
            <?= Html::activeFileInput($model, $attribute, [
                'class'         => 'file_widget',
                'data-filesize' => $max_file_size,
                'data-ajax'     => (int)$ajaxUpload,
                'data-action'   => $uploadAction,
                'accept'        => 'image/*',
                'id'            => $attribute . '_' . rand(1000, 9999),
            ]) ?>
        </div>
    </div>
    <p class="js-error-size" style="color: red; margin-left: 27.5%; margin-top: 10px; display: none"><?= t('file',
            'The file size must be less than {file_size}Mb', ['file_size' => $max_file_size / (1024 * 1024)]); ?></p>
    <p class="js-error-type" style="color: red; margin-left: 27.5%; margin-top: 10px; display: none"><?= t('app',
            'The image must be one of next types: {mimeTypes}',
            ['mimeTypes' => $mimeTypesStr]); ?></p>
    <div class="help-block" style="margin-left: 27.5%; margin-top: 10px; color: red"></div>
    <?= $form->field($model, $attribute)->end(); ?>
</section>