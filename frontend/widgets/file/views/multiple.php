<?php


/* @var $model \yii\base\Model */
/* @var string $form_name */
/* @var string $label */
/* @var string $attribute */
/* @var array $models */
/* @var bool $ajaxUpload */
/* @var string $uploadAction */
/* @var string $removeAction */

$isEmptyScan = empty($models) || (count($models) == 1 && $models[0]->isNewRecord);
?>

<section class="row">
    <div class="row_label"><?= $label ?></div>
    <div class="row_input">
        <div class="input_file_replace" style="margin: 0 20px 20px 0; float: left;"><a><b><?= t('app',
                        'Upload photo') ?></b></a></div>
        <div class="loaded_files_mini" <? if ($isEmptyScan): ?>style="display: none"<? endif ?>
             data-filename="<?= $form_name . '[' . $attribute . ']' ?>" data-ajax="<?= $ajaxUpload ?>"
             data-action="<?= $uploadAction ?>" data-remove_action="<?= $removeAction ?>"
             data-max_upload_size="<?= getUploadMaxFileSize() ?>">
            <b><?= t('app', 'Uploads') ?></b>
            <? if (!$isEmptyScan): ?>
                <?
                /** @var $model \yii\db\ActiveRecord */
                foreach ($models as $model):?>
                    <div>
                        <? if ($model->isFileExists($model->$attribute)):
                            $logo_path = $model->getPicturePath($model->$attribute);
                            ?>
                            <div class="input_file_logo input_file_loaded">
                                <a class="logo" href="<?= $logo_path ?>">
                                    <?= $model->getPictureHtml($model->$attribute, false) ?>
                                </a>
                            </div>
                        <? else: ?>
                            <div class="input_file_logo"></div>
                        <? endif ?>
                        <? $primaryKey = $model->primaryKey() ?>
                        <a class="link_red" data-id="<?= $model->{$primaryKey[0]} ?>"><?= t('app', 'Remove') ?></a>
                    </div>
                <? endforeach; ?>
            <? endif ?>
        </div>
        <div class="prototype" style="display: none"><a class="link_red" data-id=""><?= t('app', 'Remove') ?></a></div>
        <div data-error="size" class="help-block" style="color: red; display: none"><?= t('file',
                'The file size must be less than {file_size}Mb', ['file_size' => getUploadMaxFileSize(true)]); ?></div>
        <div data-error="ext" class="help-block" style="color: red; display: none"><?= t('file',
                'The format of file should be jpeg or png'); ?></div>
    </div>
</section>