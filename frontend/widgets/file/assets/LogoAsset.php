<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\widgets\file\assets;

use yii\web\AssetBundle;

/**
 * author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LogoAsset extends AssetBundle
{
    public $sourcePath = '@frontend/widgets/file/assets/';

    public $publishOptions = [
        'forceCopy' => true
    ];

    public $css = [
        'css/imgareaselect-default.css'
    ];
    public $js = [
        'js/logo/jquery.imgareaselect.pack.js',
        'js/logo/main.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}