;(function () {
    'use strict';

    var app = {
        file: null,
        fileJqObject: null,
        init: function () {
            this.uploadingEventListener();
            this.removingEventListener();
            this.fileChangeEventListener();
        },
        isAjaxLoad: function () {
            return Boolean(this.fileJqObject.closest('.loaded_files_mini').data('ajax'));
        },
        uploadingEventListener: function () {
            var _this = this;
            $(document).on('click', '.input_file_replace a', function (e) {
                e.preventDefault();

                var loadedFilesBlock = $(this).parents('.row_input').find('.loaded_files_mini');
                loadedFilesBlock.show();

                var fileBlockHtml = _this.getFileBlockHtml($(this));
                loadedFilesBlock.append(fileBlockHtml);

                var file = loadedFilesBlock.find('input[type="file"]:last');
                file.click();
            });
        },
        removingEventListener: function () {
            $(document).on('click', '.loaded_files_mini a.link_red', function (e) {
                e.preventDefault();
                var link = $(this);
                var entity_id = link.data('id');

                if (entity_id) {
                    var removeUrl = link.closest('.loaded_files_mini').data('remove_action');
                    $.get(
                        removeUrl,
                        {id: entity_id},
                        function (json) {
                            if (json) {
                                link.parent().remove();
                            }
                        },
                        'json'
                    );
                }
            });
        },
        fileChangeEventListener: function () {
            var _this = this;
            $(document).on('change', '.loaded_files_mini input[type="file"]', function (evt) {
                _this.fileJqObject = $(this);
                if (window.File && window.FileReader && window.FileList) {
                    var files = evt.target.files;

                    var validate_res = _this.fileValidate(files[0], _this.fileJqObject);
                    if (!validate_res) {
                        _this.fileJqObject.parent().remove();
                        return false;
                    }
                    _this.file = files[0];
                    var reader = new FileReader();
                    // Closure to capture the file information.
                    reader.onload = (function (theFile) {
                        return function (e) {
                            if (_this.isAjaxLoad()) {
                                _this.uploadFile();
                            } else {
                                // Render thumbnail.
                                var thumb = new Image();
                                thumb.src = e.target.result;
                                thumb.onload = function () {
                                    _this.createImage(e.target.result);
                                }
                            }
                        };
                    })(files[0]);

                    // Read in the image file as a data URL.
                    reader.readAsDataURL(files[0]);
                } else {
                    alert('The File APIs are not fully supported in this browser.');
                }
            });
        },
        createImage: function (src) {
            this.fileJqObject.before('<div class="input_file_logo"><img src="' + src + '"></div>')
                .before(this.getRemoveButton(this.fileJqObject));
        },
        uploadFile: function () {
            var _this = this;
            var formData = new FormData();
            formData.append(this.fileJqObject.attr('name'), this.file);
            var url = this.fileJqObject.closest('.loaded_files_mini').data('action');

            $.ajax({
                url: url,
                data: formData,
                dataType: "json",
                type: 'post',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.src) {
                        _this.createImage(data.src.thumb);
                        var parent = _this.fileJqObject.parent();
                        parent.find('img').wrap('<a class="logo" href="' + data.src.big + '"></a>');
                        parent.find('.link_red').data('id', data.id);
                        _this.resetFileInput();
                    } else {
                        console.log(data);
                    }
                }
            });
        },
        resetFileInput: function () {
            this.fileJqObject.wrap('<form>').parent('form').trigger('reset');
            this.fileJqObject.unwrap();
        },
        fileValidate: function (file, jq_object) {
            var parent = jq_object.parents('div.row_input');
            var error_block = parent.find('.help-block');
            error_block.hide();

            if (file.type != 'image/png' && file.type != 'image/jpeg' && file.type != 'image/jpg') {
                error_block.filter('[data-error="ext"]').show();
                return false;
            }
            var max_upload_size = parent.find('.loaded_files_mini').data('max_upload_size');
            if (file.size > max_upload_size) {
                error_block.filter('[data-error="size"]').show();
                return false;
            }

            return true;
        },
        getRemoveButton: function (jq_obj) {
            return jq_obj.parents('.row_input').find('.prototype a.link_red').clone();
        },
        getFileBlockHtml: function (jq_obj) {
            var form_name = jq_obj.parents('.row_input').find('.loaded_files_mini').data('filename');
            return '<div><input hidden type="file" name="' + form_name + '"/></div>';
        }
    };

    if (!window.isLoadedWidgetMultipleUpload) {
        app.init();
        window.isLoadedWidgetMultipleUpload = true;
    }
})();