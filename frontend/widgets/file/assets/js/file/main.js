fileUpload = (function ($) {
    'use strict';

    return {
        file: null,
        fileJqObject: null,
        mimeTypes: null,
        init: function () {
            this.changeFileEvent();
        },
        isAjaxLoad: function () {
            return Boolean(this.fileJqObject.data('ajax'));
        },
        validateMimeType: function (mimeTypes, fileType) {
            for (var i = 0, len = mimeTypes.length; i < len; i++) {
                if (new RegExp(mimeTypes[i]).test(fileType)) {
                    return true;
                }
            }

            return false;
        },
        changeFileEvent: function () {
            var _this = this;
            $(document).on('change', 'input[type="file"].file_widget', function (evt) {
                _this.fileJqObject = $(this);
                // Check for the various File API support.
                if (window.File && window.FileReader && window.FileList) {
                    // Great success! All the File APIs are supported.
                    var files = evt.target.files; // FileList object

                    var max_file_size = $(this).data('filesize');
                    if (files[0].size > max_file_size) {
                        $(this).parents('.row_input').siblings('.js-error-size').show();
                        return false;
                    } else {
                        $(this).parents('.row_input').siblings('.js-error-size').hide();
                    }

                    if (!_this.validateMimeType(_this.mimeTypes, files[0].type)) {
                        $(this).parents('.row_input').siblings('.js-error-type').show();
                        return false;
                    } else {
                        $(this).parents('.row_input').siblings('.js-error-type').hide();
                    }

                    _this.file = files[0];
                    $(this).parents('div.row_input').find('.logo_error').hide();

                    var reader = new FileReader();

                    // Closure to capture the file information.
                    reader.onload = (function (theFile) {
                        return function (e) {
                            if (_this.isAjaxLoad()) {
                                _this.uploadFile();
                            } else {
                                // Render thumbnail.
                                var thumb = new Image();
                                thumb.src = e.target.result;
                                thumb.onload = function () {
                                    _this.createImage(e.target.result);
                                }
                            }
                        };
                    })(files[0]);

                    // Read in the image file as a data URL.
                    reader.readAsDataURL(files[0]);
                } else {
                    alert('The File APIs are not fully supported in this browser.');
                }
            });
        },
        createImage: function (src) {
            var img_logo_div = this.fileJqObject.parents('div.row_input').find('.input_file_logo');
            img_logo_div.html('<img src="' + src + '">').show();
        },
        uploadFile: function () {
            var _this = this;
            var formData = new FormData();
            formData.append(this.fileJqObject.attr('name'), this.file);

            $.ajax({
                url: this.fileJqObject.data('action'),
                data: formData,
                dataType: "json",
                type: 'post',
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.src) {
                        var parent = _this.fileJqObject.parents('div.row_input');
                        var imgJqObject = parent.find('.input_file_logo img');

                        if (imgJqObject.size() > 0) {
                            imgJqObject.attr('src', data.src.thumb);
                            imgJqObject.parent().attr('href', data.src.big);
                        } else {
                            _this.createImage(data.src.thumb);
                            parent.find('.input_file_logo img').wrap('<a class="logo" href="' + data.src.big + '"></a>');
                        }

                        _this.resetFileInput();
                    } else {
                        console.log(data);
                    }
                }
            });
        },
        resetFileInput: function () {
            this.fileJqObject.wrap('<form>').parent('form').trigger('reset');
            this.fileJqObject.unwrap();
        }
    };
})(jQuery);

jQuery(document).ready(function () {
    if (!window.isLoadedWidgetFileUpload) {
        fileUpload.init();
        window.isLoadedWidgetFileUpload = true;
    }
});