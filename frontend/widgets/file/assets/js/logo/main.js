;(function () {
    'use strict';

    var app = {
        uploadButonSelector: '#choose_size',
        inputFileSelector: 'input[type="file"]',
        fileSectionSelector: '#logo',
        file: null,
        cropInputName: null,
        inputFileJqObject: null,
        init: function () {
            this.setInputJqObject();
            this.setCropInputName();
            this.changeFileEvent();
            this.uploadFileEvent();
        },
        getUploadAction: function () {
            return $(this.fileSectionSelector).data('action');
        },
        isAjaxLoad: function () {
            return Boolean($(this.fileSectionSelector).data('ajax'));
        },
        setInputJqObject: function () {
            this.inputFileJqObject = $(this.fileSectionSelector).find(this.inputFileSelector);
        },
        setCropInputName: function () {
            this.cropInputName = this.inputFileJqObject.data('crop');
        },
        changeFileEvent: function () {
            var _this = this;
            this.inputFileJqObject.on('change', function (evt) {
                // Check for the various File API support.
                if (window.File && window.FileReader && window.FileList) {
                    // Great success! All the File APIs are supported.
                    var files = evt.target.files; // FileList object

                    $('.logo_error').hide();

                    var max_file_size = $(this).data('filesize');
                    var min_width = $(this).data('width');
                    var min_height = $(this).data('height');

                    if (files[0].type != 'image/png' && files[0].type != 'image/jpeg') {
                        return false;
                    }

                    if (files[0].size > max_file_size) {
                        return false;
                    }

                    var reader = new FileReader();

                    // Closure to capture the file information.
                    reader.onload = (function (theFile) {
                        return function (e) {
                            // Render thumbnail.
                            var thumb = new Image();

                            thumb.src = e.target.result;
                            thumb.onload = function () {
                                if (this.width < min_width || this.height < min_height) {
                                    return false;
                                }

                                var img_logo_div = $(_this.fileSectionSelector).find('.input_file_logo');

                                img_logo_div.html('<img src="' + e.target.result + '">');

                                var logo = $('#logo_orig');
                                var imgAreaSelect = null;

                                logo.attr('src', e.target.result);
                                logo.next('.preview').find('img').attr('src', e.target.result);
                                _this.file = files[0];

                                $.colorbox({
                                    inline: true,
                                    href: '#logo_resize',
                                    onClosed: function () {
                                        imgAreaSelect.cancelSelection();
                                    },
                                    onComplete: function () {
                                        imgAreaSelect = logo.imgAreaSelect({
                                            x1: 0,
                                            y1: 0,
                                            x2: min_width,
                                            y2: min_height,
                                            minWidth: min_width,
                                            minHeight: min_height,
                                            aspectRatio: min_width + ':' + min_height,
                                            instance: true,
                                            imageHeight: thumb.height,
                                            imageWidth: thumb.width,
                                            persistent: true,
                                            onSelectEnd: function (img, selection) {
                                                _this.setCropParams(selection.x1, selection.y1, selection.height, selection.width);
                                            },
                                            onSelectChange: function (img, selection) {
                                                var scaleX = min_width / (selection.width || 1);
                                                var scaleY = min_height / (selection.height || 1);
                                                $('.preview img').css({
                                                    width: Math.round(scaleX * thumb.width) + 'px',
                                                    height: Math.round(scaleY * thumb.height) + 'px',
                                                    marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px',
                                                    marginTop: '-' + Math.round(scaleY * selection.y1) + 'px'
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        };
                    })(files[0]);

                    // Read in the image file as a data URL.
                    reader.readAsDataURL(files[0]);
                } else {
                    alert('The File APIs are not fully supported in this browser.');
                }
            });
        },
        setCropParams: function (x, y, height, width) {
            this.getInputXJqObject().val(x);
            this.getInputYJqObject().val(y);
            this.getInputHeightJqObject().val(height);
            this.getInputWidthJqObject().val(width);
        },
        getInputXJqObject: function () {
            return $('input[name="' + this.cropInputName + '[x]"]');
        },
        getInputYJqObject: function () {
            return $('input[name="' + this.cropInputName + '[y]"]');
        },
        getInputHeightJqObject: function () {
            return $('input[name="' + this.cropInputName + '[h]"]');
        },
        getInputWidthJqObject: function () {
            return $('input[name="' + this.cropInputName + '[w]"]');
        },
        uploadFileEvent: function () {
            var _this = this;
            $(this.uploadButonSelector).on('click', function (e) {
                e.preventDefault();
                $.colorbox.close();
                if (_this.isAjaxLoad()) {
                    if (_this.file) {
                        $.ajax({
                            url: _this.getUploadAction(),
                            data: _this.getformData(),
                            dataType: "json",
                            type: 'post',
                            contentType: false,
                            processData: false,
                            success: function (data) {
                                if (data.src) {
                                    $(_this.fileSectionSelector).find('.input_file_logo img').attr('src', data.src.thumb)
                                        .wrap('<a class="logo" href="' + data.src.big + '"></a>');
                                    _this.resetFileInput();
                                } else {
                                    console.log(data);
                                }
                            }
                        });
                    }
                }
            });
        },
        getformData: function () {
            var formData = new FormData();

            formData.append($(this.inputFileSelector).attr('name'), this.file);
            formData.append(this.cropInputName + '[x]', this.getInputXJqObject().val());
            formData.append(this.cropInputName + '[y]', this.getInputYJqObject().val());
            formData.append(this.cropInputName + '[h]', this.getInputHeightJqObject().val());
            formData.append(this.cropInputName + '[w]', this.getInputWidthJqObject().val());

            return formData;
        },
        resetFileInput: function () {
            this.inputFileJqObject.wrap('<form>').parent('form').trigger('reset');
            this.inputFileJqObject.unwrap();
        }
    };

    if (!window.isLoadedWidgetLogo) {
        app.init();
        window.isLoadedWidgetLogo = true;
    }
})();