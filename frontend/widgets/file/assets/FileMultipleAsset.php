<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\widgets\file\assets;

use yii\web\AssetBundle;

/**
 * author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class FileMultipleAsset extends AssetBundle
{
    public $sourcePath = '@frontend/widgets/file/assets/';

    public $publishOptions = [
        'forceCopy' => true
    ];

    public $css = [
    ];
    public $js = [
        'js/multiple/MultipleUpload.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}