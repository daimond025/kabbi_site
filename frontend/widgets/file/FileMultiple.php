<?php

namespace frontend\widgets\file;

use frontend\widgets\file\assets\FileMultipleAsset;

class FileMultiple extends \yii\base\Widget
{
    public $models;
    public $attribute;
    public $form_name;
    public $label;
    public $ajaxUpload = false;
    public $uploadAction;
    public $removeAction;

    /**
     * @var integer Filse size (Mb)
     */
    public $max_file_size;

    public function init()
    {
        parent::init();
        FileMultipleAsset::register($this->getView());
    }

    public function run()
    {
        return $this->render('multiple', [
            'models'        => $this->models,
            'attribute'     => $this->attribute,
            'form_name'     => $this->form_name,
            'label'         => $this->label,
            'max_file_size' => $this->getMaxFileSize(),
            'ajaxUpload'    => $this->ajaxUpload,
            'uploadAction'  => $this->uploadAction,
            'removeAction'  => $this->removeAction,
        ]);
    }

    private function getMaxFileSize()
    {
        return empty($this->max_file_size) ? getUploadMaxFileSize() : $this->max_file_size * 1024 * 1024;
    }

}
