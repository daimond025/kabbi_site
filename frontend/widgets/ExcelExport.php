<?php

namespace frontend\widgets;


class ExcelExport extends \moonland\phpexcel\Excel
{
    /**
     * Data for export.
     * @var array
     */
    public $data;

    public function init()
    {
        parent::init();
        if (!empty($this->headers)) {
            $this->data = array_merge([$this->headers], $this->data);
        }
    }

    /**
     * Setting data from array
     */
    public function executeColumns(&$activeSheet = null)
    {
        if ($activeSheet == null) {
            $activeSheet = $this->activeSheet;
        }
        $row = 1;
        $char = 26;
        foreach ($this->data as $data) {
            $isPlus = false;
            $colplus = 0;
            $colnum = 1;
            foreach ($data as $cellValue) {
                $col = '';
                if ($colnum > $char) {
                    $colplus += 1;
                    $colnum = 1;
                    $isPlus = true;
                }
                if ($isPlus) {
                    $col .= chr(64 + $colplus);
                }
                $col .= chr(64 + $colnum);
                $activeSheet->setCellValue($col . $row, $cellValue);
                $colnum++;
            }
            $row++;
        }
    }

}
