<?php

namespace frontend\widgets;

use common\modules\tenant\modules\tariff\models\TenantTariff;
use common\modules\tenant\models\Tenant;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * Class TariffNotification
 * @package frontend\widgets
 */
class TariffNotification extends Widget
{
    public $daysBefore = 10;

    /**
     * Getting expiry date of current tariff
     */
    private function getExpiryDate()
    {
        $domain   = domainName();
        $tenantId = Tenant::getIdByDomain($domain);

        $tenantTariffs = array_filter(TenantTariff::getTenantTariffs(time(), $tenantId, app()->cache),
            function ($value) {
                return isset($value['tariff'])
                && isset($value['tariff']['expiry_date']) && empty($value['isNextPaid']);
            });

        $expiryDate = array_reduce($tenantTariffs, function ($prior, $value) {
            $date = $value['tariff']['expiry_date'];

            return $date < $prior || empty($prior) ? $date : $prior;
        });

        $beforeExpiryDate = strtotime('-' . ($this->daysBefore - 1) . 'day', $expiryDate);

        return time() > $beforeExpiryDate ? $expiryDate : null;
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $expiryDate = $this->getExpiryDate();

        if (!empty($expiryDate)) {
            $text = Html::tag('span', t('setting', 'Paid to'))
                . app()->formatter->asDate($expiryDate);

            return Html::a($text, ['/tenant/tariff'], [
                'class' => 'header_money',
            ]);
        }
    }
}