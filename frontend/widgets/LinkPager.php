<?php

namespace frontend\widgets;

use yii\helpers\Html;

/**
 * LinkPager displays a list of hyperlinks that lead to different pages of target.
 *
 * LinkPager works with a [[Pagination]] object which specifies the totally number
 * of pages and the current page number.
 *
 * Note that LinkPager only generates the necessary HTML markups. In order for it
 * to look like a real pager, you should provide some CSS styles for it.
 * With the default configuration, LinkPager should look good using Twitter Bootstrap CSS framework.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LinkPager extends \yii\widgets\LinkPager
{
    public $contanerId;

    /**
     * Renders the page buttons.
     * @return string the rendering result
     */
    protected function renderPageButtons()
    {
        $pageCount = $this->pagination->getPageCount();
        if ($pageCount < 2 && $this->hideOnSinglePage) {
            return '';
        }

        $buttons = [];
        $currentPage = $this->pagination->getPage();

        // first page
        $firstPageLabel = $this->firstPageLabel === true ? '1' : $this->firstPageLabel;
        if ($firstPageLabel !== false) {
            $buttons['pages'][] = $this->renderPageButton($firstPageLabel, 0, null, $currentPage <= 0, false);
        }

        // prev page
        if ($this->prevPageLabel !== false) {
            if (($page = $currentPage - 1) < 0) {
                $page = 0;
            }
            $buttons['navigation'][] = $this->renderPageButton(
                $this->prevPageLabel,
                $page,
                'p_prev',
                $currentPage <= 0,
                false
            );
        }

        // internal pages
        list($beginPage, $endPage) = $this->getPageRange();
        for ($i = $beginPage; $i <= $endPage; ++$i) {
            $disabled = $currentPage == $i ? true : false;
            $buttons['pages'][] = $this->renderPageButton($i + 1, $i, null, $disabled, $i == $currentPage);
        }

        // next page
        if ($this->nextPageLabel !== false) {
            if (($page = $currentPage + 1) >= $pageCount - 1) {
                $page = $pageCount - 1;
            }
            $buttons['navigation'][] = $this->renderPageButton(
                $this->nextPageLabel,
                $page,
                'p_next',
                $currentPage >= $pageCount - 1,
                false
            );
        }

        // last page
        $lastPageLabel = $this->lastPageLabel === true ? $pageCount : $this->lastPageLabel;
        if ($lastPageLabel !== false) {
            $buttons['pages'][] = $this->renderPageButton(
                $lastPageLabel,
                $pageCount - 1,
                null,
                $currentPage >= $pageCount - 1,
                false
            );
        }

        $contanerOptions = !empty($this->contanerId) ?
            array_merge($this->options, ['id' => $this->contanerId]) : $this->options;

        $html = Html::beginTag('div', $contanerOptions);
        $html .= Html::tag('b', t('app', 'Pages'));
        $html .= Html::tag('div', implode("\n", $buttons['pages']), ['class' => 'p_pages']);
        $html .= Html::tag('div', $buttons['navigation'][0] . $buttons['navigation'][1], ['class' => 'p_links']);
        $html .= Html::endTag('div');

        return $html;
    }

    /**
     * Renders a page button.
     * You may override this method to customize the generation of page buttons.
     * @param string $label the text label for the button
     * @param integer $page the page number
     * @param string $class the CSS class for the page button.
     * @param boolean $disabled whether this page button is disabled
     * @param boolean $active whether this page button is active
     * @return string the rendering result
     */
    protected function renderPageButton($label, $page, $class, $disabled, $active)
    {
        if ($disabled) {
            return Html::tag('span', $label, ['class' => $class]);
        }

        $linkOptions = $this->linkOptions;
        $linkOptions['data-page'] = $page;

        if (!empty($class)) {
            $label = Html::tag('span', $label, ['class' => $class]);
        }

        return Html::a($label, $this->pagination->createUrl($page), $linkOptions);
    }
}
