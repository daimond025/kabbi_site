<?php

namespace logger;

use yii\log\SyslogTarget;


/**
 * Class CustomSyslogTarget
 * @package logger
 */
class CustomSyslogTarget extends SyslogTarget
{
    /**
     * @inheritdoc
     */
    public function formatMessage($message)
    {
        list($text, $level, $category, $timestamp) = $message;

        return $text;
    }

}