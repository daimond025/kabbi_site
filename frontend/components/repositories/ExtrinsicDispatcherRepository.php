<?php

namespace frontend\components\repositories;

use app\modules\tenant\models\User;

class ExtrinsicDispatcherRepository
{

    public function getAllExtrinsicDispatchersByTenant($tenantId)
    {
        return User::find()->alias('u')
            ->distinct()
            ->leftJoin(
                'tbl_tenant_has_extrinsic_dispatcher thed',
                '`u`.`user_id` = `thed`.`user_id` AND thed.tenant_id = ' . $tenantId
            )
            ->where([
                'u.position_id' => User::POSITION_EXTRINSIC_DISPATCHER,
                'u.tenant_id'   => $tenantId,
            ])
            ->all();
    }

}
