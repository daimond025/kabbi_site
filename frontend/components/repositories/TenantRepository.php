<?php

namespace frontend\components\repositories;


use common\helpers\CacheHelper;
use common\modules\city\models\City;
use common\modules\city\models\Republic;
use common\modules\tenant\models\TenantHasCity;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

class TenantRepository
{

    public function getTenantCityListWithRepublic()
    {
        $lang_prefix = getLanguagePrefix();

        return CacheHelper::getFromCache('UserCityListWithRepublic_' . user()->tenant_id . $lang_prefix,
            function () use ($lang_prefix) {
                return City::find()
                    ->joinWith([
                        'tenantHasCities' => function (ActiveQuery $query) {
                            $query->where([
                                'block'     => 0,
                                'tenant_id' => user()->tenant_id,
                            ]);
                        },
                    ], false)
                    ->with([
                        'republic' => function ($query) {
                            $query->select(['republic_id', 'timezone']);
                        },
                    ])
                    ->select([City::tableName() . '.city_id', 'name' . $lang_prefix, 'republic_id'])
                    ->asArray()
                    ->orderBy(TenantHasCity::tableName() . '.sort')
                    ->all();
            });
    }


    public function getTenantCityMap()
    {
        $lang_prefix = getLanguagePrefix();

        $cities = TenantHasCity::find()
            ->where([
                'tenant_id' => user()->tenant_id,
                'block'     => 0,
            ])
            ->with([
                'city' => function (ActiveQuery $query) use ($lang_prefix) {
                    $query->select(['city_id', 'name' . $lang_prefix]);
                },
            ])
            ->orderBy('sort')
            ->all();

        return ArrayHelper::map($cities, 'city_id', 'city.name' . $lang_prefix);
    }


    public function getCityList()
    {
        return TenantHasCity::find()
            ->asArray()
            ->where([
                'tenant_id' => user()->tenant_id,
                'block' => 0
            ])
            ->select(TenantHasCity::tableName() . '.city_id')
            ->with([
                'city' => function($query) {
                    $query->with(['republic' => function($query) {
                        $query->select(['republic_id',
                            Republic::tableName() . '.name' . getLanguagePrefix()]);
                    }]);
                }])
            ->orderBy(TenantHasCity::tableName() . '.sort')
            ->all();
    }

}