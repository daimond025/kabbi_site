<?php

namespace frontend\components\repositories;

use common\modules\tenant\models\DefaultSettings;
use common\modules\tenant\models\TenantSetting;

class SettingRepository
{
    public function getSettingSignalNewOrder($cityId)
    {
        return TenantSetting::getSettingValue(
            user()->tenant_id,
            DefaultSettings::SETTING_SIGNAL_NEW_ORDER,
            $cityId
        );
    }
}
