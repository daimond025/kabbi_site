<?php

namespace bonusSystem\models\ar;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%uds_game_order}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string  $promo_code
 * @property string  $customer_id
 * @property string  $request_id
 * @property string  $operation_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Order   $order
 */
class UDSGameOrder extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%uds_game_order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'request_id'], 'required'],
            [['order_id', 'created_at', 'updated_at'], 'integer'],
            [['customer_id', 'promo_code', 'request_id', 'operation_id'], 'string', 'max' => 255],
            [['order_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'order_id'     => 'Order ID',
            'promo_code'   => 'Promo Code',
            'customer_id'  => 'Customer ID',
            'request_id'   => 'Request ID',
            'operation_id' => 'Operation ID',
            'created_at'   => 'Created At',
            'updated_at'   => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }
}
