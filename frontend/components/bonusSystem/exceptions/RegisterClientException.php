<?php

namespace bonusSystem\exceptions;

use yii\base\Exception;

/**
 * Class RegisterClientException
 * @package bonusSystem\exceptions
 */
class RegisterClientException extends Exception
{

}