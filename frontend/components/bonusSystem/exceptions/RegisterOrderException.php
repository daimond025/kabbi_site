<?php

namespace bonusSystem\exceptions;

use yii\base\Exception;

/**
 * Class RegisterOrderException
 * @package bonusSystem\exceptions
 */
class RegisterOrderException extends Exception
{

}