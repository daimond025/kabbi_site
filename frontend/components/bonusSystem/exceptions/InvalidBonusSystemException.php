<?php

namespace bonusSystem\exceptions;

use yii\base\Exception;

/**
 * Class InvalidBonusSystemException
 * @package bonusSystem\exceptions
 */
class InvalidBonusSystemException extends Exception
{

}