<?php

namespace frontend\components\pushApi;

use Yii;
use GuzzleHttp\Client;
use frontend\modules\notifications\components\BulkNotifyInterface;
use yii\base\Object;

class PushApi extends Object
{
    public $url;

    public function destroyAndroidClientAppSenderService($appID){
        $this->get('notification/destroy_android_client_app_sender_service', ['app_id'=>$appID]);
    }

    public function destroyIosClientAppSenderService($appID){
        $this->get('notification/destroy_ios_client_app_sender_service', ['app_id'=>$appID]);
    }

    public function destroyAndroidWorkerAppSenderService($tenantID){
        $this->get('notification/destroy_android_worker_app_sender_service', ['tenant_id'=>$tenantID]);
    }

    public function getTenantId()
    {
        return user()->tenant_id;
    }

    public function bulkNotify(BulkNotifyInterface $notify)
    {
        $params = [
            'notification_id'    => $notify->getNotificationId(),
            'sender_id'          => user()->user_id,
            'sender_role'        => user()->getUserRole(),
            'sender_last_name'   => user()->last_name,
            'sender_name'        => user()->name,
            'sender_second_name' => user()->second_name,
            'tenant_id'          => $notify->getTenantId(),
            'city_ids'           => $notify->getCityIds(),
            'client_type'        => $notify->getClientType(),
            'client_ids'         => $notify->getClientIds(),
            'title'              => $notify->getTitle(),
            'message'            => $notify->getText(),
        ];


        if (empty($params['client_ids'])) {
            return ['status' => 'error', 'message' => 'Empty value'];
        }


        $response = $this->post('notification/bulk_notify', $params);

        if ($response === null) {
            return ['status' => 'error', 'message' => 'No connection'];
        }

        return ['status' => 'ok', 'message' => ''];
    }

    protected function post($action, $params)
    {
        $options = [
            'form_params' => $params,
        ];

        return $this->send('POST', $action, $options);
    }

    protected function get($action, $params)
    {
        $options = [
            'query' => $params,
        ];

        return $this->send('GET', $action, $options);
    }

    protected function send($type = 'GET', $action, $options)
    {
        try {
            $httpClient = new Client();
            $res = $httpClient->request($type, "{$this->url}{$action}", $options);

            $response = json_decode($res->getBody(), true);

            return $response;
        } catch (\Exception $ex) {
            Yii::error($ex);

            return null;
        }
    }

}