<?php

namespace frontend\components\autocomplete;


use common\modules\tenant\models\DefaultSettings;
use common\modules\tenant\models\TenantSetting;
use Yii;
use yii\base\InvalidConfigException;

class Autocomplete extends \yii\base\Component
{
    public $format = 'gootax';
    public $url;
    public $action = 'autocomplete';
    public $type_app = 'frontend';
    public $sort = 'distance';
    public $radius = null;

    /**
     * Must be configure.
     * Examples:
     * 'apiKey' => '34324'
     * 'apiKey' => function () {
     *    return 'sdfsdff';
     * }
     * @var string|callable
     */
    private $_api_key;

    public function init()
    {
        parent::init();

        if (empty($this->_api_key)) {
            throw new InvalidConfigException('Api key hasn`t been configure');
        }
    }

    /**
     * Получаем список улиц.
     *
     * Обязательные параметры:
     *   text = запрос
     *   focus.point.lat  и focus.point.lon координаты местоположения пользователя (если не можем их определить, то
     * центр филиала) format = gootax type_app -тип приложения (client, driver, frontend) tenant_id -ид арендодатора
     * city_id - ид филиала hash - md5(urlencode(queryString) + api_key), где queryString - строка с параметрами
     * запроса, параметры отсортированы по алфавиту, api_key - ключ приложения
     *
     * Необязательные параметры на вход:
     *  size - количество, от 10 до 40, дефолт -30 - не обяз параметр
     *  radius - радиус поиска от 1 до 5000, дефолт 30 - не обяз параметр
     *  sort - тип сортировки - distance | growth  - дефолт distance - не обяз параметр
     *
     * @param string $search Строка поиска
     * @param string $cityId city id of branch
     * @param string $focus_lat Широта фокуса поиска
     * @param string $focus_lon Долгота фокуса поиска
     * @param string $lang язык
     *
     * @return mixed
     */
    public function getStreetList($search, $cityId, $focus_lat, $focus_lon, $lang = null)
    {
        $curl       = app()->curl;
        $params     = $this->getUrlParams($search, $cityId, $focus_lat, $focus_lon, $lang);
        $response   = $curl->get($this->getUrl(), $params);
        $statusCode = $response->headers['Status-Code'];

        if ($statusCode != 200) {
            $error = $curl->error();

            return json_encode(['error' => $error ? $error : $statusCode]);
        }

        return $response->body;
    }

    public function getUrl()
    {
        return $this->url . 'v1/' . $this->action;
    }

    /**
     * @param string $search Строка поиска
     * @param string $cityId City id
     * @param string $focus_lat Широта фокуса поиска
     * @param string $focus_lon Долгота фокуса поиска
     * @param string $lang язык
     *
     * @return array
     */
    private function getUrlParams($search, $cityId, $focus_lat, $focus_lon, $lang = null)
    {
        $params         = [
            'city_id'         => $cityId,
            'text'            => $search,
            'focus.point.lat' => $focus_lat,
            'focus.point.lon' => $focus_lon,
            'format'          => $this->format,
            'lang'            => $lang,
            'sort'            => $this->sort,
            'type_app'        => $this->type_app,
            'tenant_id'       => user()->tenant_id,
            'radius'          => $this->radius ? $this->radius : $this->getRadius($cityId),
        ];
        $params['hash'] = $this->getHash($params);

        return $params;
    }

    private function getRadius($cityId)
    {
        return TenantSetting::getSettingValue(
            user()->tenant_id,
            DefaultSettings::SETTING_AUTOCOMPLETE_RADIUS,
            $cityId
        );
    }

    private function getHash(array $params)
    {
        ksort($params);
        $api_key     = $this->getApiKey();
        $queryString = http_build_query($params, '', '&', PHP_QUERY_RFC3986);

        return md5($queryString . $api_key);
    }

    public function getApiKey()
    {
        return $this->_api_key;
    }

    public function setApiKey($api_key)
    {
        if (is_callable($api_key)) {
            $this->_api_key = call_user_func($api_key);
        } else {
            $this->_api_key = $api_key;
        }
    }
}