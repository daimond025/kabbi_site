<?php

namespace orderApi\interfaces\createOrder;

interface ApiOrderPassengerInterface
{
    public function getClientPassengerPhone();
    public function getClientPassengerId();

    public function getClientPassengerName();
    public function getClientPassengerSecondName();
    public function getClientPassengerLastName();
}
