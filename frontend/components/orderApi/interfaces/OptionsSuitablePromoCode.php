<?php

namespace orderApi\interfaces;

interface OptionsSuitablePromoCode
{
    public function getTenantId();
    public function getClientId();
    public function getOrderTime();
    public function getCityId();
    public function getPositionId();
    public function getCarClassId();

}
