<?php

namespace orderApi;

use frontend\modules\promocode\models\Promo;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use orderApi\exceptions\OrderApiException;
use orderApi\interfaces\createOrder\ApiOrderInterface;
use orderApi\interfaces\OptionsSuitablePromoCode;
use yii\base\Object;

/**
 * Class OrderApi
 * @package frontend\components
 */
class OrderApi extends Object
{

    const STATUS_USER_ERROR_400 = 400;
    const STATUS_USER_ERROR_422 = 422;

    const RESPONSE_STATUS_OK = 0;

    /* @var string */
    public $baseUrl;

    /* @var int */
    public $timeout = 15;

    /* @var int */
    public $connectionTimeout = 15;

    /* @var Client */
    private $httpClient;

    /**
     * @inherited
     */
    public function init()
    {
        parent::init();

        $this->httpClient = new Client([
            'base_uri'          => $this->baseUrl,
            'connectionTimeout' => $this->connectionTimeout,
            'timeout'           => $this->timeout,
        ]);
    }

    /**
     * Getting attribute information
     *
     * @param int $statusId
     *
     * @return array
     * @throws OrderApiException
     */
    public function getAttributeInformation($statusId)
    {
        try {
            $response = $this->httpClient->request('GET', "v1/orders/get-available-attributes?statusId={$statusId}");

            return json_decode($response->getBody(), true);
        } catch (\Exception $ex) {
            throw new OrderApiException('An error occurred while getting attribute information', 0, $ex);
        }
    }


    public function workerPromoRegistration($workerId)
    {
        try {
            $response = $this->httpClient->request('GET', "promocode/promo/worker_registration?worker_id=$workerId");

            return json_decode($response->getBody(), true);
        } catch (\Exception $ex) {
            throw new OrderApiException('Error worker promo registration', 0, $ex);
        }
    }


    public function generatedPromoCodes(Promo $promo)
    {
        try {
            $response = $this->httpClient->request('POST', "promocode/promo/generated_promo_codes", [
                'form_params' => [
                    'tenant_id'             => $promo->tenant_id,
                    'city_id'               => $promo->city,
                    'position_id'           => $promo->position_id,
                    'promo_id'              => $promo->promo_id,
                    'car_classes_id'        => $promo->carClasses,
                    'count_symbols_in_code' => $promo->count_symbols_in_code,
                    'symbols'               => $promo->symbols,
                    'count_codes'           => $promo->count_codes,
                    'type_id'               => $promo->type_id,
                ],
            ]);

            return json_decode($response->getBody(), true);
        } catch (\Exception $ex) {
            throw new OrderApiException('Error worker promo generation code', 0, $ex);
        }
    }

    public function findSuitablePromoCode(OptionsSuitablePromoCode $options)
    {

        try {
            $response = $this->httpClient->get('promocode/promo/find_suitable_promo_code', [
                'query' => [
                    'tenant_id'    => $options->getTenantId(),
                    'client_id'    => $options->getClientId(),
                    'order_time'   => $options->getOrderTime(),
                    'city_id'      => $options->getCityId(),
                    'position_id'  => $options->getPositionId(),
                    'car_class_id' => $options->getCarClassId(),
                ],
            ]);

            return json_decode($response->getBody(), true);
        } catch (\Exception $ex) {
            throw new OrderApiException('Error find code', 0, $ex);
        }
    }

    public function createOrder(ApiOrderInterface $order)
    {
        try {
            $response = $this->httpClient->request('POST', 'v1/order/create', [
                'form_params' => [
                    'city_id'                 => $order->getCityId(),
                    'order_now'               => $order->getOrderNow(),
                    'order_date'              => $order->getOrderDate(),
                    'order_hours'             => $order->getOrderHours(),
                    'order_minutes'           => $order->getOrderMinutes(),
                    'phone'                   => $order->getPhone(),
                    'client_id'               => $order->getClientId(),
                    'comment'                 => $order->getComment(),
                    'bonus_payment'           => $order->getBonusPayment(),
                    'payment'                 => $order->getPayment(),
                    'position_id'             => $order->getPositionId(),
                    'tariff_id'               => $order->getTariffId(),
                    'predv_price'             => $order->getPredvPrice(),
                    'predv_distance'          => $order->getPredvDistance(),
                    'predv_time'              => $order->getPredvTime(),
                    'predv_price_no_discount' => $order->getPredvPriceNoDiscount(),
                    'is_fix'                  => $order->getIsFix(),
                    'orderAction'             => $order->getOrderAction(),
                    'parking_id'              => $order->getParkingId(),
                    'company_id'              => $order->getCompanyId(),
                    'device'                  => $order->getDevice(),
                    'tenant_id'               => $order->getTenantId(),
                    'user_modifed'            => $order->getUserModifedId(),
                    'user_create'             => $order->getUserCreatedId(),
                    'call_id'                 => $order->getCallId(),
                    'worker_id'               => $order->getWorkerId(),
                    'car_id'                  => $order->getCarId(),
                    'additional_option'       => $order->getAdditionalOption(),

                    'client_name'       => $order->getClientName(),
                    'client_lastname'   => $order->getClientLastName(),
                    'client_secondname' => $order->getClientSecondName(),

                    'client_passenger_phone'      => $order->getClientPassengerPhone(),
                    'client_passenger_id'         => $order->getClientPassengerId(),
                    'client_passenger_name'       => $order->getClientPassengerName(),
                    'client_passenger_secondname' => $order->getClientPassengerSecondName(),
                    'client_passenger_lastname'   => $order->getClientPassengerLastName(),

                    'address' => $order->getAddressOriginal(),
                ],
                'headers'     => [
                    'lang' => $order->getLang(),
                ],
            ]);
            /*var_dump( (string) $response->getBody()->getContents());
            exit();*/

            return json_decode($response->getBody(), true);
        } catch (\Exception $ex) {
            $this->throwValidateErrors($ex);

            throw new OrderApiException('Error create order', 0, $ex);
       }
    }

    protected function throwValidateErrors($ex)
    {
        if ($this->isUserErrors($ex)) {
            throw $ex;
        }
    }

    protected function isUserErrors(\Exception $exception)
    {
        $isInstance = $exception instanceof ClientException;

        /** @var ClientException $exception */
        return $isInstance && in_array($exception->getResponse()->getStatusCode(), [
                self::STATUS_USER_ERROR_400,
                self::STATUS_USER_ERROR_422,
            ]);
    }
}
