<?php

namespace frontend\components\consul\exceptions;

use yii\base\Exception;

/**
 * Class ConsulServiceException
 */
class ConsulServiceException extends Exception
{

}