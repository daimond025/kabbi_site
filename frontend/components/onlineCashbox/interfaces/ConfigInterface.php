<?php

namespace frontend\components\onlineCashbox\interfaces;


interface ConfigInterface
{
    public function getCityId();
    public function getPositionId();
    public function getProfileId();
    public function getProductName();
    public function getSendingCondition();
    public function getTaxationSystem();
    public function getTax();
    public function getTenantId();
}