<?php

namespace frontend\components\onlineCashbox\interfaces;


interface ProfileModulKassaInterface extends ProfileBaseInterface
{

    public function getModulkassaEmail();
    public function getModulkassaPassword();
    public function getModulkassaRetailPointUuid();
}