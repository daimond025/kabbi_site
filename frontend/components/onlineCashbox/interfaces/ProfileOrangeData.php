<?php

namespace frontend\components\onlineCashbox\interfaces;


interface ProfileOrangeData extends ProfileBaseInterface
{

    public function getInn();
    public function getSslClientCrtPass();

    public function getSignPkey();
    public function getSslClientKey();
    public function getSslClientCrt();
    public function getSslCaCert();

    public function getPaymentTransferOperatorPhones();
    public function getPaymentAgentOperation();
    public function getPaymentAgentPhones();
    public function getPaymentOperatorPhones();
    public function getPaymentOperatorName();
    public function getPaymentOperatorAddress();
    public function getPaymentOperatorInn();
    public function getSupplierPhones();


}