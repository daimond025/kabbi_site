<?php

namespace frontend\components\onlineCashbox\interfaces;


interface ProfileBaseInterface
{
    public function getTypeId();
    public function getTenantId();
    public function getName();
    public function getDebug();
}