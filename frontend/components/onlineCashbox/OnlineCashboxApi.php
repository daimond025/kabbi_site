<?php

namespace frontend\components\onlineCashbox;


use frontend\components\onlineCashbox\interfaces\ConfigInterface;
use frontend\components\onlineCashbox\interfaces\ProfileModulKassaInterface;
use frontend\components\onlineCashbox\interfaces\ProfileOrangeData;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Response;
use yii\base\Object;

class OnlineCashboxApi extends Object
{

    public $baseUrl;
    public $timeout;
    public $connectionTimeout;

    /* @var $httpClient Client */
    protected $httpClient;

    public function init()
    {
        parent::init();

        $this->httpClient = \Yii::createObject(Client::class, [
            [
                'base_uri'          => $this->baseUrl,
                'timeout'           => $this->timeout,
                'connectionTimeout' => $this->connectionTimeout,
            ],
        ]);
    }


    public function getListTypesCashbox()
    {
        $response = $this->httpClient->get('api/v1/types');

        return json_decode($response->getBody(), true);
    }


    public function getListProfilesCashbox($tenantId)
    {
        $response = $this->httpClient->get('api/v1/profiles', [
            'query' => ['tenant_id' => $tenantId]
        ]);

        return json_decode($response->getBody(), true);
    }


    public function createProfilesModulKassa(ProfileModulKassaInterface $profileModulKassa)
    {
        $formParams = [
            'tenant_id'                    => $profileModulKassa->getTenantId(),
            'type_id'                      => $profileModulKassa->getTypeId(),
            'name'                         => $profileModulKassa->getName(),
            'debug'                        => $profileModulKassa->getDebug(),
            'modulkassa_email'             => $profileModulKassa->getModulkassaEmail(),
            'modulkassa_password'          => $profileModulKassa->getModulkassaPassword(),
            'modulkassa_retail_point_uuid' => $profileModulKassa->getModulkassaRetailPointUuid(),
        ];

        /* @var $response Response */
        $response = $this->httpClient->post('api/v1/profiles', ['json' => $formParams]);

        return json_decode($response->getBody(), true);
    }


    public function updateProfilesModulKassa(ProfileModulKassaInterface $profileModulKassa, $profileId)
    {

        $formParams = [
            'name'                         => $profileModulKassa->getName(),
            'debug'                        => $profileModulKassa->getDebug(),
            'modulkassa_email'             => $profileModulKassa->getModulkassaEmail(),
            'modulkassa_retail_point_uuid' => $profileModulKassa->getModulkassaRetailPointUuid(),
        ];

        if ($profileModulKassa->getModulkassaPassword()) {
            $formParams = array_merge(
                $formParams, ['modulkassa_password' => $profileModulKassa->getModulkassaPassword()]
            );
        }

        $response = $this->httpClient->put('api/v1/profiles/' . $profileId, ['json' => $formParams]);

        return json_decode($response->getBody(), true);
    }


    public function createProfilesOrangeData(ProfileOrangeData $profileOrangeData)
    {

        $sslClientCrtPass = $profileOrangeData->getSslClientCrtPass();
        $sslClientCrtPass = $sslClientCrtPass ? $sslClientCrtPass : null;

        $formParams = [
            'type_id'                          => $profileOrangeData->getTypeId(),
            'tenant_id'                        => $profileOrangeData->getTenantId(),
            'name'                             => $profileOrangeData->getName(),
            'debug'                            => $profileOrangeData->getDebug(),
            'inn'                              => $profileOrangeData->getInn(),
            'sign_pkey'                        => $profileOrangeData->getSignPkey(),
            'ssl_client_key'                   => $profileOrangeData->getSslClientKey(),
            'ssl_client_crt'                   => $profileOrangeData->getSslClientCrt(),
            'ssl_ca_cert'                      => $profileOrangeData->getSslCaCert(),
            'ssl_client_crt_pass'              => $sslClientCrtPass,
            'payment_transfer_operator_phones' => $profileOrangeData->getPaymentTransferOperatorPhones(),
            'payment_agent_operation'          => $profileOrangeData->getPaymentAgentOperation(),
            'payment_agent_phones'             => $profileOrangeData->getPaymentAgentPhones(),
            'payment_operator_phones'          => $profileOrangeData->getPaymentOperatorPhones(),
            'payment_operator_name'            => $profileOrangeData->getPaymentOperatorName(),
            'payment_operator_address'         => $profileOrangeData->getPaymentOperatorAddress(),
            'payment_operator_inn'             => $profileOrangeData->getPaymentOperatorInn(),
            'supplier_phones'                  => $profileOrangeData->getSupplierPhones(),
        ];

        /* @var $response Response */
        $response = $this->httpClient->post('api/v1/profiles', ['json' => $formParams]);

        return json_decode($response->getBody(), true);
    }


    public function updateProfilesOrangeData(ProfileOrangeData $profileOrangeData, $profileId)
    {
        $sslClientCrtPass = $profileOrangeData->getSslClientCrtPass();
        $sslClientCrtPass = $sslClientCrtPass ? $sslClientCrtPass : null;

        $formParams = [
            'name'                             => $profileOrangeData->getName(),
            'debug'                            => $profileOrangeData->getDebug(),
            'inn'                              => $profileOrangeData->getInn(),
            'payment_transfer_operator_phones' => $profileOrangeData->getPaymentTransferOperatorPhones(),
            'payment_agent_operation'          => $profileOrangeData->getPaymentAgentOperation(),
            'payment_agent_phones'             => $profileOrangeData->getPaymentAgentPhones(),
            'payment_operator_phones'          => $profileOrangeData->getPaymentOperatorPhones(),
            'payment_operator_name'            => $profileOrangeData->getPaymentOperatorName(),
            'payment_operator_address'         => $profileOrangeData->getPaymentOperatorAddress(),
            'payment_operator_inn'             => $profileOrangeData->getPaymentOperatorInn(),
            'supplier_phones'                  => $profileOrangeData->getSupplierPhones(),
            'ssl_client_crt_pass'              => $sslClientCrtPass,
        ];


        if ($profileOrangeData->getSignPkey()) {
            $formParams['sign_pkey'] = $profileOrangeData->getSignPkey();
        }

        if ($profileOrangeData->getSslClientKey()) {
            $formParams['ssl_client_key'] = $profileOrangeData->getSslClientKey();
        }

        if ($profileOrangeData->getSslClientCrt()) {
            $formParams['ssl_client_crt'] = $profileOrangeData->getSslClientCrt();
        }

        if ($profileOrangeData->getSslCaCert()) {
            $formParams['ssl_ca_cert'] = $profileOrangeData->getSslCaCert();
        }

        $response = $this->httpClient->put('api/v1/profiles/' . $profileId, ['json' => $formParams]);

        return json_decode($response->getBody(), true);
    }


    public function deleteProfile($profileId)
    {
        $this->httpClient->delete('api/v1/profiles/' . $profileId);
    }


    public function viewProfile($profileId)
    {
        $response = $this->httpClient->get('api/v1/profiles/' . $profileId);

        return json_decode($response->getBody(), true);
    }





    public function getListConfigCashbox($tenantId)
    {
        $response = $this->httpClient->get('api/v1/configurations', [
            'query' => ['tenant_id' => $tenantId]
        ]);

        return json_decode($response->getBody(), true);
    }


    public function createConfig(ConfigInterface $config)
    {

        $formParams = [
            'city_id'           => $config->getCityId(),
            'position_id'       => $config->getPositionId(),
            'profile_id'        => $config->getProfileId(),
            'product_name'      => $config->getProductName(),
            'sending_condition' => $config->getSendingCondition(),
            'tax'               => $config->getTax(),
            'tenant_id'         => $config->getTenantId(),
        ];

        if ($config->getTaxationSystem()) {
            $formParams = array_merge($formParams, ['taxation_system'   => $config->getTaxationSystem()]);
        }

        $response = $this->httpClient->post('api/v1/configurations', ['json' => $formParams]);

        return json_decode($response->getBody(), true);
    }


    public function updateConfig(ConfigInterface $config, $configId)
    {
        $formParams = [
            'city_id'           => $config->getCityId(),
            'position_id'       => $config->getPositionId(),
            'profile_id'        => $config->getProfileId(),
            'product_name'      => $config->getProductName(),
            'sending_condition' => $config->getSendingCondition(),
            'taxation_system'   => $config->getTaxationSystem(),
            'tax'               => $config->getTax(),
        ];

        $response = $this->httpClient->put('api/v1/configurations/' . $configId, ['json' => $formParams]);

        return json_decode($response->getBody(), true);
    }


    public function deleteConfig($configId)
    {
        $this->httpClient->delete('api/v1/configurations/' . $configId);
    }


    public function viewConfig($configId)
    {
        $response = $this->httpClient->get('api/v1/configurations/' . $configId);

        return json_decode($response->getBody(), true);
    }





}