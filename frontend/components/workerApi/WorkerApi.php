<?php

namespace frontend\components\workerApi;


use app\modules\order\models\OrderStatus;
use yii\base\Object;

/**
 * Class WorkerApi
 * @package frontend\components\workerApi
 */
class WorkerApi extends Object
{
    public $url;

    /**
     * Close active worker shift
     *
     * @param $workerLogin
     * @param $tenantLogin
     * @param $workerApiKey
     *
     * @return bool|mixed
     */
    public function workerEndWork($workerLogin, $tenantLogin, $workerApiKey)
    {
        $params = [
            'worker_login' => $workerLogin,
            'tenant_login' => $tenantLogin,
        ];
        ksort($params);
        $signature = md5(http_build_query($params) . $workerApiKey);
        $url       = $this->url . 'v3/api/worker_end_work';

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_TIMEOUT        => 15,
            CURLOPT_HTTPHEADER     => [
                //                'Content-Type: application/x-www-form-urlencode',
                'signature: ' . $signature,
            ],
            CURLOPT_POST           => 1,
            CURLOPT_POSTFIELDS     => $params,
        ]);
        $result    = curl_exec($curl);
        $errorCode = curl_errno($curl);
        curl_close($curl);

        return ($errorCode == CURLE_OK) ? json_decode($result, true) : false;
    }

    /**
     * Remove worker block
     *
     * @param $workerLogin
     * @param $tenantLogin
     * @param $typeBlock
     * @param $blockId
     * @param $workerApiKey
     *
     * @return bool|mixed
     */
    public function workerUnblock($workerLogin, $tenantLogin, $typeBlock, $blockId, $workerApiKey)
    {
        $params = [
            'tenant_login' => $tenantLogin,
            'worker_login' => $workerLogin,
            'block_id'     => $blockId,
        ];
        ksort($params);
        $signature = md5(http_build_query($params) . $workerApiKey);

        $method = $typeBlock == 'order'
            ? 'v3/api/worker_order_unblock' : 'v3/api/worker_pre_order_unblock';

        $url = $this->url . $method;

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_TIMEOUT        => 15,
            CURLOPT_HTTPHEADER     => [
                //                'Content-Type: application/x-www-form-urlencode',
                'signature: ' . $signature,
            ],
            CURLOPT_POST           => 1,
            CURLOPT_POSTFIELDS     => $params,
        ]);
        $result    = curl_exec($curl);
        $errorCode = curl_errno($curl);
        curl_close($curl);

        return ($errorCode == CURLE_OK) ? json_decode($result, true) : false;
    }


    /**
     * Set order status
     *
     * @param string      $workerLogin
     * @param string      $tenantLogin
     * @param string      $workerApiKey
     * @param int         $orderId
     * @param int         $statusId
     * @param int|null    $timeToClient
     * @param string|null $detailOrderData
     *
     * @return bool|mixed
     */
    public function setOrderStatus(
        $workerLogin,
        $tenantLogin,
        $workerApiKey,
        $orderId,
        $statusId,
        $timeToClient,
        $detailOrderData
    ) {
        $params = [
            'worker_login'  => $workerLogin,
            'tenant_login'  => $tenantLogin,
            'order_id'      => $orderId,
            'status_new_id' => $statusId,
        ];
        if (isset($timeToClient)) {
            $params['time_to_client'] = $timeToClient;
        }
        if (isset($detailOrderData)) {
            $params['detail_order_data'] = $detailOrderData;
        }
        ksort($params);

        $signature = md5(http_build_query($params) . $workerApiKey);
        $url       = $this->url . 'v3/api/set_order_status';

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_TIMEOUT        => 15,
            CURLOPT_HTTPHEADER     => [
                //                'Content-Type: application/x-www-form-urlencode',
                'signature: ' . $signature,
            ],
            CURLOPT_POST           => 1,
            CURLOPT_POSTFIELDS     => $params,
        ]);
        $result    = curl_exec($curl);
        $errorCode = curl_errno($curl);
        curl_close($curl);

        return ($errorCode == CURLE_OK) ? json_decode($result, true) : false;
    }
}