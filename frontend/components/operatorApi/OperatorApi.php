<?php

namespace operatorApi;

use GuzzleHttp\Client;
use operatorApi\exceptions\CreateUpdateEventException;
use operatorApi\exceptions\GetUpdateEventResultException;
use operatorApi\exceptions\StopOfferException;
use yii\base\Object;

/**
 * @TODO - not working now
 * Class OperatorApi
 * @package operatorApi
 */
class OperatorApi extends Object implements ApiInterface
{
    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @var string
     */
    public $baseUrl;

    /**
     * @var float
     */
    public $timeout;

    /**
     * @var float
     */
    public $connectionTimeout;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->httpClient = new Client([
            'base_uri'          => $this->baseUrl,
            'connectionTimeout' => $this->connectionTimeout,
            'timeout'           => $this->timeout,
        ]);
    }


    /**
     * @param int $accessToken
     * @param int $orderId
     * @param int $lastUpdateTime
     * @throws StopOfferException
     */
    public function stopOffer($accessToken, $orderId, $lastUpdateTime)
    {
        try {
            $this->httpClient->request(
                'POST',
                "v1/order/stop-offer/{$orderId}",
                [
                    'auth'        => [$accessToken],
                    'form_params' => [
                        'last_update_time' => $lastUpdateTime,
                    ],
                ]);
        } catch (\Exception $ex) {
            throw new StopOfferException(
                'An error occurred while stopping offer to worker', 0, $ex);
        }
    }


    /**
     * Create update event
     *
     * @param string $accessToken
     * @param int    $orderId
     * @param array  $params
     *
     * @return string
     * @throws CreateUpdateEventException
     */
    public function createUpdateEvent($accessToken, $orderId, $params)
    {
        try {
            $response = $this->httpClient->request(
                'POST',
                "v1/order/update-event/{$orderId}",
                [
                    'auth'        => [$accessToken],
                    'form_params' => $params,
                ]);
        } catch (\Exception $ex) {
            throw new CreateUpdateEventException(
                'An error occurred while creating update event', 0, $ex);
        }

        $result = json_decode($response->getBody(), true);

        return isset($result['uuid']) ? $result['uuid'] : null;
    }


    /**
     * Getting result of update event
     *
     * @param string $accessToken
     * @param string $uuid
     *
     * @return array
     * @throws GetUpdateEventResultException
     */
    public function getUpdateEventResult($accessToken, $uuid)
    {
        try {
            $response = $this->httpClient->request(
                'GET',
                'v1/order/check-update-event',
                [
                    'auth'  => [$accessToken],
                    'query' => [
                        'uuid' => $uuid,
                    ],
                ]);
        } catch (\Exception $ex) {
            throw new GetUpdateEventResultException(
                'An error occurred while checking the result of update event', $ex->getCode(), $ex);
        }

        $result = json_decode($response->getBody(), true);

        return empty($result) ? [] : $result;
    }

}