<?php

namespace operatorApi\exceptions;

use yii\base\Exception;

/**
 * Class StopOfferException
 * @package operatorApi\exceptions
 */
class StopOfferException extends Exception
{

}