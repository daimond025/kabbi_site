<?php

namespace operatorApi\exceptions;

use yii\base\Exception;

/**
 * Class QueueIsNotExistsException
 * @package operatorApi\exceptions
 */
class QueueIsNotExistsException extends Exception
{

}