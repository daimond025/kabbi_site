<?php

namespace operatorApi\exceptions;

use yii\base\Exception;

/**
 * Class CreateUpdateEventException
 * @package operatorApi\exceptions
 */
class CreateUpdateEventException extends Exception
{

}