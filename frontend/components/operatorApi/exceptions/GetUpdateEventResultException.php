<?php

namespace operatorApi\exceptions;

use yii\base\Exception;

/**
 * Class GetUpdateEventResultException
 * @package operatorApi\exceptions
 */
class GetUpdateEventResultException extends Exception
{

}