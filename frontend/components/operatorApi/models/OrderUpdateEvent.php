<?php

namespace operatorApi\models;

use operatorApi\exceptions\QueueIsNotExistsException;
use PhpAmqpLib\Exception\AMQPProtocolChannelException;
use Ramsey\Uuid\Uuid;
use Yii;
use yii\base\Object;
use yii\helpers\Json;


/**
 * Class OrderUpdateEvent
 * @package operatorApi\models
 */
class OrderUpdateEvent extends Object
{
    const COMMAND_UPDATE_ORDER_DATA = 'update_order_data';
    const COMMAND_STOP_OFFER = 'stop_offer';
    const COMMAND_TAKE_OFF_WORKER = 'remove_worker';
    const COMMAND_TAKE_OFF_COMPANY= 'remove_company';
    const COMMAND_SEND_TO_EXCHANGE= 'send_to_exchange';


    const OPERATOR_SERVICE = 'operator_service';
    const CLIENT_SERVICE = 'client_service';
    const WORKER_SERVICE = 'worker_service';

    public $requestId;
    public $service;
    public $tenantId;
    public $orderId;
    public $senderId;
    public $lastUpdateTime;
    public $command;
    public $lang;

    private $_repository;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->setRepository();
    }

    /**
     * Add order event for update.
     *
     * @param array $data
     *
     * @return string
     * @throws QueueIsNotExistsException
     */
    public function addEvent(array $data)
    {
        try {
            $this->addEventToQueue($data);
        } catch (AMQPProtocolChannelException $exc) {
            throw new QueueIsNotExistsException('Queue is not exists', 0, $exc);
        }
    }

    /**
     * Getting response of executing order event
     *
     * @param string $key
     *
     * @return mixed
     */
    public function getResponse($key)
    {
        return $this->getRepository()->get($key);
    }


    public function removeResponse($key)
    {
        return $this->getRepository()->delete($key);
    }

    /**
     * Add event to rabbit queue
     *
     * @param array $data
     *
     * @return mixed
     */
    private function addEventToQueue(array $data)
    {
        $data = $this->prepareData($data);
        return Yii::$app->amqp->send($data, 'order_' . $this->orderId, '', false);

    }

    /**
     * @return string
     */
    private function generateUuid()
    {
        return Uuid::uuid4()->toString();
    }

    /**
     * @param array $data
     *
     * @return array
     */
    private function prepareData(array $data)
    {
        $this->requestId = $this->requestId === null ? $this->generateUuid() : $this->requestId;

        return [
            'uuid'             => $this->requestId,
            'type'             => 'order_event',
            'timestamp'        => time(),
            'sender_service'   => $this->service,
            'command'          => $this->command,
            'order_id'         => $this->orderId,
            'tenant_id'        => $this->tenantId,
            'change_sender_id' => $this->senderId,
            'last_update_time' => $this->lastUpdateTime,
            'lang'             => $this->lang,
            'params'           => $data,
        ];
    }

    /**
     * @return OrderEventRepository
     */
    private function getRepository()
    {
        if (empty($this->_repository)) {
            $this->setRepository();
        }

        return $this->_repository;
    }

    private function setRepository()
    {
        $this->_repository = new OrderEventRepository();
    }

}