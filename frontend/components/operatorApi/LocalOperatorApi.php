<?php

namespace operatorApi;

use operatorApi\models\OrderUpdateEvent;
use yii\base\Object;

/**
 * Class LocalOperatorApi
 * @package operatorApi
 */
class LocalOperatorApi extends Object implements ApiInterface
{


    /**
     * Send order to exchange
     * @param $tenantId
     * @param $senderId
     * @param $orderId
     * @param $lastUpdateTime
     * @param $lang
     * @return mixed
     * @throws exceptions\QueueIsNotExistsException
     */
    public function sendToExchange($tenantId, $senderId, $orderId, $lastUpdateTime, $lang)
    {
        $requestId = null;
        $orderUpdateEvent = new OrderUpdateEvent([
            'service'        => OrderUpdateEvent::OPERATOR_SERVICE,
            'tenantId'       => $tenantId,
            'orderId'        => $orderId,
            'senderId'       => $senderId,
            'command'        => OrderUpdateEvent::COMMAND_SEND_TO_EXCHANGE,
            'lastUpdateTime' => $lastUpdateTime,
            'lang'           => $lang,
        ]);
        $orderUpdateEvent->addEvent([]);
        return $orderUpdateEvent->requestId;

    }

    /**
     * Stop offer order
     * @param int $tenantId
     * @param int $senderId
     * @param int $orderId
     * @param int $lastUpdateTime
     * @param string $lang
     * @throws exceptions\QueueIsNotExistsException
     */
    public function stopOffer($tenantId, $senderId, $orderId, $lastUpdateTime, $lang)
    {
        $orderUpdateEvent = new OrderUpdateEvent([
            'service'        => OrderUpdateEvent::OPERATOR_SERVICE,
            'tenantId'       => $tenantId,
            'orderId'        => $orderId,
            'senderId'       => $senderId,
            'command'        => OrderUpdateEvent::COMMAND_STOP_OFFER,
            'lastUpdateTime' => $lastUpdateTime,
            'lang'           => $lang,
        ]);
        $orderUpdateEvent->addEvent([]);
    }

    /**
     * Take off worker from order
     * @param int $tenantId
     * @param int $senderId
     * @param int $orderId
     * @param int $lastUpdateTime
     * @param string $lang
     * @throws exceptions\QueueIsNotExistsException
     */
    public function takeOffFromWorker($tenantId, $senderId, $orderId, $lastUpdateTime, $lang)
    {
        $requestId = null;
        $orderUpdateEvent = new OrderUpdateEvent([
            'service'        => OrderUpdateEvent::OPERATOR_SERVICE,
            'tenantId'       => $tenantId,
            'orderId'        => $orderId,
            'senderId'       => $senderId,
            'command'        => OrderUpdateEvent::COMMAND_TAKE_OFF_WORKER,
            'lastUpdateTime' => $lastUpdateTime,
            'lang'           => $lang,
        ]);
        $orderUpdateEvent->addEvent([]);
        return $orderUpdateEvent->requestId;
    }


    /**
     * Take off company from order
     * @param int $tenantId
     * @param int $senderId
     * @param int $orderId
     * @param int $lastUpdateTime
     * @param string $lang
     * @throws exceptions\QueueIsNotExistsException
     */
    public function takeOffFromCompany($tenantId, $senderId, $orderId, $lastUpdateTime, $lang)
    {
        $orderUpdateEvent = new OrderUpdateEvent([
            'service'        => OrderUpdateEvent::OPERATOR_SERVICE,
            'tenantId'       => $tenantId,
            'orderId'        => $orderId,
            'senderId'       => $senderId,
            'command'        => OrderUpdateEvent::COMMAND_TAKE_OFF_COMPANY,
            'lastUpdateTime' => $lastUpdateTime,
            'lang'           => $lang,
        ]);
        $orderUpdateEvent->addEvent([]);
    }


    /**
     * Create event of order update
     *
     * @param int    $tenantId
     * @param int    $senderId
     * @param int    $orderId
     * @param array  $params
     * @param string $lang
     *
     * @return string
     */
    public function createUpdateEvent($tenantId, $senderId, $orderId, $params, $lang)
    {
        $requestId = null;
        $fields    = $params['fields'];

        if (!empty($fields) && is_array($fields)) {
            $orderUpdateEvent = new OrderUpdateEvent([
                'service'        => OrderUpdateEvent::OPERATOR_SERVICE,
                'tenantId'       => $tenantId,
                'orderId'        => $orderId,
                'senderId'       => $senderId,
                'command'        => OrderUpdateEvent::COMMAND_UPDATE_ORDER_DATA,
                'lastUpdateTime' => isset($params['last_update_time']) ? $params['last_update_time'] : null,
                'lang'           => $lang,
            ]);

            $orderUpdateEvent->addEvent($fields);

            $requestId = $orderUpdateEvent->requestId;
        }

        return $requestId;
    }

    /**
     * Getting result of update event
     *
     * @param string $requestId
     *
     * @return array
     */
    public function getUpdateEventResult($requestId)
    {
        $orderEvent = new OrderUpdateEvent();
        $response   = $orderEvent->getResponse($requestId);

        if (!empty($response)) {
            $orderEvent->removeResponse($requestId);
        }

        $result = json_decode($response, true);

        return empty($result) ? [] : $result;
    }

    /**
     * Reject order
     *
     * @param int    $tenantId
     * @param int    $senderId
     * @param int    $orderId
     * @param int    $statusId
     * @param string $lang
     */
    public function rejectOrder($tenantId, $senderId, $orderId, $statusId, $lang)
    {
        $orderUpdateEvent = new OrderUpdateEvent([
            'service'        => OrderUpdateEvent::CLIENT_SERVICE,
            'tenantId'       => $tenantId,
            'orderId'        => $orderId,
            'senderId'       => $senderId,
            'command'        => OrderUpdateEvent::COMMAND_UPDATE_ORDER_DATA,
            'lastUpdateTime' => null,
            'lang'           => $lang,
        ]);
        $orderUpdateEvent->addEvent(['status_id' => $statusId]);

        return $orderUpdateEvent->requestId;
    }

    /**
     * Getting result of reject order
     *
     * @param string $requestId
     *
     * @return array
     */
    public function getRejectOrderResult($requestId)
    {
        $orderEvent = new OrderUpdateEvent();
        $response   = $orderEvent->getResponse($requestId);

        if (!empty($response)) {
            $orderEvent->removeResponse($requestId);
        }

        $result = json_decode($response, true);

        return empty($result) ? [] : $result;
    }

}