<?php

namespace operatorApi;

/**
 * Interface ApiInterface
 * @package operatorApi
 */
interface ApiInterface
{
    /**
     * Stop offer
     *
     * @param int    $tenantId
     * @param int    $senderId
     * @param int    $orderId
     * @param int    $lastUpdateTime
     * @param string $lang
     *
     * @return
     */
    public function stopOffer($tenantId, $senderId, $orderId, $lastUpdateTime, $lang);


    /**
     * refuse worker from offer
     *
     * @param int    $tenantId
     * @param int    $senderId
     * @param int    $orderId
     * @param int    $lastUpdateTime
     * @param string $lang
     *
     * @return
     */
    public function takeOffFromWorker($tenantId, $senderId, $orderId, $lastUpdateTime, $lang);


    /**
     * refuse company from offer
     *
     * @param int    $tenantId
     * @param int    $senderId
     * @param int    $orderId
     * @param int    $lastUpdateTime
     * @param string $lang
     *
     * @return
     */
    public function takeOffFromCompany($tenantId, $senderId, $orderId, $lastUpdateTime, $lang);

    /**
     * Send order to exchange
     * @param $tenantId
     * @param $senderId
     * @param $orderId
     * @param $lastUpdateTime
     * @param $lang
     * @return mixed
     */
    public function sendToExchange($tenantId, $senderId, $orderId, $lastUpdateTime, $lang);

    /**
     * Create event of order update
     *
     * @param int    $tenantId
     * @param int    $senderId
     * @param int    $orderId
     * @param array  $params
     * @param string $lang
     *
     * @return string
     */
    public function createUpdateEvent($tenantId, $senderId, $orderId, $params, $lang);

    /**
     * Getting result of update event
     *
     * @param string $requestId
     *
     * @return array
     */
    public function getUpdateEventResult($requestId);


    /**
     * Reject order
     *
     * @param int    $tenantId
     * @param int    $senderId
     * @param int    $orderId
     * @param int    $statusId
     * @param string $lang
     */
    public function rejectOrder($tenantId, $senderId, $orderId, $statusId, $lang);

    /**
     * Getting result of reject order
     *
     * @param string $requestId
     *
     * @return array
     */
    public function getRejectOrderResult($requestId);

}