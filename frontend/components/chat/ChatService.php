<?php

namespace chat;

use common\modules\employee\models\worker\Worker;
use common\modules\employee\models\worker\WorkerHasCity;
use yii\caching\Cache;
use yii\caching\TagDependency;
use yii\db\ActiveQuery;

/**
 * Class ChatService
 * @package chat
 */
class ChatService
{
    /**
     * Getting workers by city
     *
     * @param int $tenantId
     * @param int $cityId
     *
     * @return array
     * @throws \yii\db\Exception
     */
    public function getWorkersByCity($tenantId, $cityId)
    {
        $sql = <<<SQL
SELECT `w`.*
FROM `tbl_worker` AS `w`
INNER JOIN (
SELECT `w`.`worker_id`, `c`.`city_id`
FROM `tbl_worker` AS `w`
JOIN `tbl_worker_has_city` AS `c` USING (`worker_id`)
WHERE `w`.`tenant_id` = :tenantId
GROUP BY `w`.`worker_id`
ORDER BY `c`.`last_active` DESC, `c`.`city_id`) AS `c` USING (`worker_id`)
WHERE `w`.`tenant_id` = :tenantId
AND `w`.`block` = 0
AND `c`.`city_id` = :cityId
SQL;

        return \Yii::$app->getDb()->createCommand($sql, [
            'tenantId' => $tenantId,
            'cityId'   => $cityId,
        ])->queryAll();
    }

    /**
     * Getting worker by city from cache
     *
     * @param int   $tenantId
     * @param int   $cityId
     * @param Cache $cache
     *
     * @return array
     */
    public function getCachedWorkersByCity($tenantId, $cityId, $cache = null)
    {
        $cache    = $cache ?: app()->getCache();
        $cacheKey = "workers_by_city__{$tenantId}_{$cityId}";

        $result = $cache->get($cacheKey);
        if ($result === false) {
            $result = $this->getWorkersByCity($tenantId, $cityId);
            $cache->set($cacheKey, $result, 3600,
                new TagDependency(['tags' => [WorkerHasCity::CACHE_TAG]]));
        }

        return $result;
    }
}