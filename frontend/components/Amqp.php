<?php


namespace app\components;


use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use yii\base\Exception;
use yii\base\Object;
use yii\helpers\Json;

class Amqp extends Object
{
    const ORDER_STATISTIC_TASK = 'order_statistic';

    /**
     * @var AMQPStreamConnection
     */
    protected $ampqConnection;

    /**
     * @var AMQPChannel[]
     */
    protected $channels = [];

    /**
     * @var string
     */
    public $host = '127.0.0.1';
    /**
     * @var integer
     */
    public $port = 5672;
    /**
     * @var string
     */
    public $user = 'guest';
    /**
     * @var string
     */
    public $password = 'guest';

    /**
     * @var string
     */
    public $vhost = '/';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->ampqConnection = new AMQPStreamConnection(
            $this->host,
            $this->port,
            $this->user,
            $this->password,
            $this->vhost
        );
    }

   /* public function __destruct($config = [])
    {
        parent::__construct($config);
    }*/

    /**
     * Returns AMQP connection.
     *
     * @return AMQPStreamConnection
     */
    public function getConnection()
    {
        return $this->ampqConnection;
    }

    /**
     * Returns AMQP connection.
     *
     * @param string $channel_id
     *
     * @return AMQPChannel
     */
    public function getChannel($channel_id = null)
    {
        $index = $channel_id ?: 'default';
        if (!array_key_exists($index, $this->channels)) {
            $this->channels[$index] = $this->connection->channel($channel_id);
        }
        return $this->channels[$index];
    }

    /**
     * Returns prepaired AMQP message.
     *
     * @param string|array|object $message
     * @param array               $properties
     *
     * @return AMQPMessage
     * @throws Exception If message is empty.
     */
    protected function prepareMessage($message, $properties = null)
    {
        if (empty($message)) {
            throw new Exception('AMQP message can not be empty');
        }
        if (is_array($message) || is_object($message)) {
            $message = Json::encode($message);
        }

        return new AMQPMessage($message, $properties);
    }

    /**
     * Sends message to the exchange.
     *
     * @param string|array $message
     * @param string       $routing_key
     * @param string       $exchange
     *
     * @return void
     */
    public function send($message, $routing_key, $exchange = '', $createQueueIfNotExists = true)
    {
        $message = $this->prepareMessage($message, ['delivery_mode' => 2]);
        $channel = $this->getChannel();

        
        $passive = !$createQueueIfNotExists;
        $channel->queue_declare($routing_key, $passive, true, false, false);

        $channel->basic_publish($message, $exchange, $routing_key);
        //$channel->close();
        //$this->connection->close();
    }
}