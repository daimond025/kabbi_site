<?php

namespace frontend\components\behavior;

use yii\base\Behavior;

class CityListBehavior extends Behavior
{
    /**
     * @return array
     */
    public function getUserCityList()
    {
        return user()->getUserCityList();
    }

    /**
     * @return array
     */
    public function getUserCityIds()
    {
        return array_keys($this->getUserCityList());
    }
}
