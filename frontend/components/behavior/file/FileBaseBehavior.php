<?php

/**
 * This behavior for upload files.
 */

namespace frontend\components\behavior\file;

use Yii;
use yii\base\Behavior;
use yii\helpers\FileHelper;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use yii\helpers\Html;
use yii\base\InvalidConfigException;

abstract class FileBaseBehavior extends Behavior
{
    const THUMB_PREFIX = 'thumb_';

    //Если изображение больше, то обрезаем до..
    public $big_picture_width = 600;
    public $big_picture_height = 600;
    public $big_picture_quality = 80;
    
    // Если меньше, то увеличим
    public $small_picture_width = 300;
    public $small_picture_height = 300;
    public $small_picture_quality = 90;
    
    // размер превьюшки
    public $thumb_width = 80;
    public $thumb_height = 80;
    public $thumb_quality = 100;
    
    public $upload_dir;

    public $action = 'show';
    public $tenant_id;

    /**
     * @var array $fileField Contains model file fields
     */
    public $fileField = [];

    /**
     * @var array $_old_value Contains old file path value
     */
    protected $_old_value = [];
    protected $_upload_dir_path;
    protected $_tenant_id;

    public function init()
    {
        parent::init();
        if (empty($this->upload_dir)) {
            throw new InvalidConfigException('The "upload_dir" property must be set.');
        }
    }

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
            ActiveRecord::EVENT_AFTER_FIND    => 'afterFind',
        ];
    }

    public function beforeDelete($event)
    {
        foreach ($this->fileField as $fileField) {
            $this->cleanFiles($this->owner->$fileField);
        }
    }

    public function cleanFiles($file_name)
    {
        $this->deleteDocument($file_name);
        $this->deleteDocument(self::THUMB_PREFIX . $file_name);
    }

    public function getThumbPrefix()
    {
        return self::THUMB_PREFIX;
    }

    public function deleteDocument($file_name)
    {
        $documentPath = $this->getUploadDirPath() . $file_name;

        if (is_file($documentPath)) {
            return unlink($documentPath);
        }

        return false;
    }

    public function afterFind($event)
    {
        foreach ($this->fileField as $fileField) {
            $this->_old_value[$fileField] = $this->owner->$fileField;
        }
    }

    public function getPicturePath($filename, $origin = true)
    {
        $filename = $origin ? $filename : self::THUMB_PREFIX . $filename;
        if ($this->action == 'show-external-file') {
            return app()->urlManager->createUrl(['/file/' . $this->action, 'id' => $this->tenant_id, 'filename' => $filename]);
        }
        return app()->urlManager->createUrl(['/file/' . $this->action, 'dir' => $this->upload_dir, 'filename' => $filename]);
    }

    public function getPictureHtml($filename, $origin = true, $style = '')
    {
        return Html::img($this->getPicturePath($filename, $origin), ['style' => $style]);
    }

    public function beforeSave($event)
    {
        foreach ($this->fileField as $fileField) {
            $file = UploadedFile::getInstance($this->owner, $fileField);
            if ($file) {
                //Чистим предыдущие файлы
                if (isset($this->_old_value[$fileField])) {
                    $this->cleanFiles($this->_old_value[$fileField]);
                }

                //Получаем имя файла
                $basename = $this->getFileBaseName($file);

                //Присваеваем имя полю таблицы текущей сущности
                $this->owner->$fileField = $basename;

                //Путь до миниатюры
                $thumb_path = $this->getThumbPath($basename);
                //Путь до большой фото
                $new_file_path = $this->getBigPicturePath($basename);

                //Сохраняем в той или иной реализации
                $this->saveFile($file->tempName, $new_file_path, $thumb_path);
            }
            //Когда не было загрузки файла, оставляем то же имя файла при обновлении модели
            elseif (isset($this->_old_value[$fileField])) {
                $this->owner->$fileField = $this->_old_value[$fileField];
            }
        }
    }

    public function isFileExists($filename)
    {
        return is_file($this->getUploadDirPath() . $filename);
    }

    /**
     * Save file.
     * @param string $fileTempName File temp name.
     * @param string $new_file_path File path for saving.
     * @param string $thumb_path Thumb file path for saving.
     */
    abstract protected function saveFile($fileTempName, $new_file_path, $thumb_path);

    protected function getThumbPath($file_name)
    {
        return $this->getUploadDirPath() . self::THUMB_PREFIX . $file_name;
    }

    protected function getBigPicturePath($file_name)
    {
        return $this->getUploadDirPath() . $file_name;
    }

    protected function getFileBaseName(UploadedFile $file)
    {
        $extension = $file->getExtension();
        if (empty($extension)) {
            $extension = 'jpg';
        }
        $filename = $this->getRandomFileName($extension);

        return $filename . '.' . $extension;
    }

    protected function getUploadDirPath()
    {
        if (empty($this->_upload_dir_path)) {
            $this->setUploadDirPath();
        }

        return $this->_upload_dir_path;
    }

    protected function getTenantId()
    {
        if (empty($this->_tenant_id)) {
            $cur_user = user();
            $this->_tenant_id = is_object($cur_user) ? user()->tenant_id : $this->owner->tenant_id;
        }
        return $this->_tenant_id;
    }

    private function setUploadDirPath()
    {
        $this->_upload_dir_path = Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR .
                $this->upload_dir . DIRECTORY_SEPARATOR . $this->getTenantId() . DIRECTORY_SEPARATOR;

        if (!is_dir($this->_upload_dir_path)) {
            FileHelper::createDirectory($this->_upload_dir_path);
        }
    }

    private function getRandomFileName($extension)
    {
        do {
            $name = uniqid();
            $file = $this->setUploadDirPath() . $name . '.' . $extension;
        } while (file_exists($file));

        return $name;
    }

}
