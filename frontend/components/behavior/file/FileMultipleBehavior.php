<?php

/**
 * This behavior for multiple upload files.
 */

namespace frontend\components\behavior\file;

use yii\web\UploadedFile;

class FileMultipleBehavior extends FileBehavior
{

    public function events()
    {
        return [];
    }

    public function saveFiles()
    {
        $arFiles = [];

        foreach ($this->fileField as $fileField) {
            $files = UploadedFile::getInstances($this->owner, $fileField);
            foreach ($files as $file) {
                //Получаем имя файла
                $basename = $this->getFileBaseName($file);
                //Путь до миниатюры
                $thumb_path = $this->getThumbPath($basename);
                //Путь до большой фото
                $new_file_path = $this->getBigPicturePath($basename);
                $arFiles[] = $basename;
                //Сохраняем
                $this->saveFile($file->tempName, $new_file_path, $thumb_path);
            }
        }

        return $arFiles;
    }

}
