<?php

/**
 * This behavior for upload files.
 */

namespace frontend\components\behavior\file;

use Yii;
use yii\imagine\Image;

class FileBehavior extends FileBaseBehavior
{
    /**
     * @var integer Reduce the size of the larger side for this value.
     */
    public $max_big_picture_side;

    protected function saveFile($fileTempName, $new_file_path, $thumb_path)
    {
        $arSize = $this->getPictureSize($fileTempName);
        Image::thumbnail($fileTempName, $arSize['width'], $arSize['height'])->save($new_file_path, ['quality' => $this->big_picture_quality]);
        Image::thumbnail($fileTempName, $this->thumb_width, $this->thumb_height)->save($thumb_path, ['quality' => $this->thumb_quality]);
    }

    protected function getSizeByBigSide($file_path)
    {
        $imagine = Image::getImagine();
        $orig_size = $imagine->open($file_path)->getSize();
        $orig_size_width = $orig_size->getWidth();
        $orig_size_height = $orig_size->getHeight();

        if ($orig_size_width > $orig_size_height) {
            $arSize['width'] = $this->max_big_picture_side;
            $arSize['height'] = round($arSize['width'] / ($orig_size_width / $orig_size_height));
        } else {
            $arSize['height'] = $this->max_big_picture_side;
            $arSize['width'] = round($arSize['height'] / ($orig_size_height / $orig_size_width));
        }

        return $arSize;
    }

    protected function getPictureSize($fileTempName)
    {
        if (!empty($this->max_big_picture_side)) {
            return $this->getSizeByBigSide($fileTempName);
        }

        return [
            'width'  => $this->big_picture_width,
            'height' => $this->big_picture_height
        ];
    }

}
