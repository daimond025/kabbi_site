<?php

namespace frontend\components\behavior\file;

use yii\imagine\Image;

class CropFileBehavior extends FileBaseBehavior
{
    const MIN_CROP_WIDTH = 300;
    const MIN_CROP_HEIGHT = 300;
    const DEFAULT_CROP_X = 0;
    const DEFAULT_CROP_Y = 0;

    private $cropParams = [];

    public function setCropParams()
    {
        $cropParams = $this->owner->cropParams;

        if (!isset($cropParams['w']) || $cropParams['w'] < $this->small_picture_width) {
            $cropParams['w'] = $this->small_picture_width;
        }

        if (!isset($cropParams['h']) || $cropParams['h'] < $this->small_picture_height) {
            $cropParams['h'] = $this->small_picture_height;
        }

        if (!isset($cropParams['x'])) {
            $cropParams['x'] = self::DEFAULT_CROP_X;
        }

        if (!isset($cropParams['y'])) {
            $cropParams['y'] = self::DEFAULT_CROP_Y;
        }

        $this->cropParams = $cropParams;
    }

    public function getCropParams()
    {
        if (empty($this->cropParams)) {
            $this->setCropParams();
        }

        return $this->cropParams;
    }

    protected function saveFile($fileTempName, $new_file_path, $thumb_path)
    {
        $cropParams = $this->getCropParams();
        Image::crop($fileTempName, $cropParams['w'], $cropParams['h'],
            [$cropParams['x'], $cropParams['y']])->save($new_file_path, ['quality' => $this->small_picture_quality]);

        if ($cropParams['h'] > $this->big_picture_height) {
            $cropParams['h'] = $this->big_picture_height;
        }
        if ($cropParams['w'] > $this->big_picture_width) {
            $cropParams['w'] = $this->big_picture_width;
        }
        Image::thumbnail($new_file_path, $cropParams['w'], $cropParams['h'])->save($new_file_path,
            ['quality' => $this->small_picture_quality]);
        Image::thumbnail($new_file_path, $this->thumb_width, $this->thumb_height)->save($thumb_path,
            ['quality' => $this->thumb_quality]);
    }

}
