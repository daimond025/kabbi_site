<?php

/**
 * This behavior for common controller methods.
 */

namespace app\components\behavior;

use app\modules\tenant\models\User;
use common\modules\tenant\models\Tenant;
use common\modules\tenant\modules\tariff\models\Module;
use common\modules\tenant\modules\tariff\models\Permission;
use common\modules\tenant\modules\tariff\models\TenantTariff;
use Yii;
use yii\base\Action;
use yii\web\Cookie;

class ControllerBehavior extends \yii\base\Behavior
{
    const ACTION_FORBIDDEN = 'forbidden';

    public $except = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->except[] = self::ACTION_FORBIDDEN;

        parent::init();
    }

    /**
     * Check online user permission
     * @return mixed
     */
    private function isAllowAccess()
    {
        $domain   = domainName();
        $tenantId = Tenant::getIdByDomain($domain);
        $userId   = app()->user->id;

        $onlineUsers   = User::getOnlineUserCount($tenantId, $userId);
        $tenantTariffs = TenantTariff::getTenantTariffs(time(), $tenantId);

        $maxOnlineUsers = false;
        if (isset($tenantTariffs[Module::CODE_TAXI]['permissions'][Permission::CODE_USERS_ONLINE]['value'])) {
            $maxOnlineUsers = $tenantTariffs[Module::CODE_TAXI]['permissions'][Permission::CODE_USERS_ONLINE]['value'];
        }

        return $maxOnlineUsers === false || $onlineUsers < $maxOnlineUsers;
    }

    /**
     * Общий для все контроллеров beforeAction($action)
     */
    public function commonBeforeAction()
    {
        $actionId = isset($this->owner->action)
            ? $this->owner->action->id
            : app()->urlManager->parseRequest(app()->request)[0];

        if (in_array($actionId, $this->except)) {
            return;
        } elseif (!$this->isAllowAccess()) {
            if (!isset(app()->request->cookies['referrer'])) {
                app()->response->cookies->add(new Cookie([
                    'name'  => 'referrer',
                    'value' => app()->request->referrer,
                ]));
            }

            return app()->controller->redirect('/site/' . self::ACTION_FORBIDDEN);
        } else {
            app()->response->cookies->remove('referrer');
            if (!app()->user->isGuest) {
                app()->user->identity->updateUserActiveTime();
            }
        }
    }
}
