<?php

namespace app\components\behavior;

use common\modules\tenant\models\DefaultSettings;
use common\modules\tenant\models\TenantSetting;


class TaxiBehavior extends \yii\base\Behavior
{
    /**
     * Getting site language
     * @return string
     */
    public function getLangParam()
    {
        $langParts = explode('-', app()->language);
        $curLang   = current($langParts);

        return empty($curLang) ? 'ru' : $curLang;
    }

    /**
     * Getting geocoder type param
     * @return string
     */
    public function getGeocoderType()
    {
        $geocoderType = TenantSetting::getSettingValue(user()->tenant_id, DefaultSettings::SETTING_GEOCODER_TYPE);

        return empty($geocoderType) ? 'ru' : $geocoderType;
    }

}
