<?php

namespace frontend\components\rbac;

class DbManager extends \yii\rbac\DbManager
{
    protected $assignments = [];

    public function checkAccess($userId, $permissionName, $params = [])
    {
        if (!isset($this->assignments[$userId])) {
            $this->assignments[$userId] = $this->getAssignments($userId);
        }

        $this->loadFromCache();
        if ($this->items !== null) {
            return $this->checkAccessFromCache($userId, $permissionName, $params, $this->assignments[$userId]);
        } else {
            return $this->checkAccessRecursive($userId, $permissionName, $params, $this->assignments[$userId]);
        }
    }
}
