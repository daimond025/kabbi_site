<?php

namespace frontend\components\rbac\rules;

use yii\rbac\Rule;

class StaffCompanyEditRule extends Rule
{

    public $name = 'canStaffCompanyEdit';


    public function execute($userId, $item, $params)
    {
        if (!$params) {
            return true;
        }

        if ($params['model']->tenant_company_id === user()->tenant_company_id) {
            return true;
        }

        return false;

    }

}