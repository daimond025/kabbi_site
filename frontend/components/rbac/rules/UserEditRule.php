<?php

namespace app\components\rbac\rules;

use Yii;
use yii\rbac\Rule;
use app\modules\tenant\models\User;

/**
 * Checks if authorID matches user passed via params
 */
class UserEditRule extends Rule
{

    public $name = 'canUserEdit';
    private $_roles = [];

    /**
     * @param string|integer $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return boolean a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        $userRoles = $this->gerUserRoles($user);

        if (array_key_exists(User::ROLE_TOP_MANAGER, $userRoles)) {
            return true;
        }

        $editUserRole = $this->gerUserRoles($params['user_id']);

        if (array_key_exists(User::ROLE_TOP_MANAGER, $editUserRole) || array_key_exists(User::ROLE_BRANCH_DIRECTOR, $editUserRole)) {
            return false;
        }

        return true;
    }

    private function gerUserRoles($user)
    {
        if (!isset($this->_roles[$user])) {
            $auth = Yii::$app->authManager;
            $this->_roles[$user] = $auth->getRolesByUser($user);
        }

        return $this->_roles[$user];
    }

}
