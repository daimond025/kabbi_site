<?php

namespace app\components\rbac;

use Yii;
use app\modules\tenant\models\User;

/**
 * Класс служит только для первоначального создания
 */
class Permission extends \yii\base\Object
{

    public static function create()
    {
        $auth = Yii::$app->authManager;

        //Создадим разрешения

        $reports = $auth->createPermission('reports');
        $reports->description = 'Отчеты';
        $auth->add($reports);

        $users = $auth->createPermission('users');
        $users->description = 'Сотрудники';
        $auth->add($users);

        $clientTariff = $auth->createPermission('clientTariff');
        $clientTariff->description = 'Тарифы для клиентов';
        $auth->add($clientTariff);

        $driverTariff = $auth->createPermission('driverTariff');
        $driverTariff->description = 'Тарифы для водителей';
        $auth->add($driverTariff);

        $orders = $auth->createPermission('orders');
        $orders->description = 'Заказы';
        $auth->add($orders);

        $clients = $auth->createPermission('clients');
        $clients->description = 'Клиенты';
        $auth->add($clients);

        $organization = $auth->createPermission('organization');
        $organization->description = 'Организации';
        $auth->add($organization);

        $cars = $auth->createPermission('cars');
        $cars->description = 'Автомобили';
        $auth->add($cars);

        $drivers = $auth->createPermission('drivers');
        $drivers->description = 'Водители';
        $auth->add($drivers);
    }

    public static function addRoles()
    {
        $auth = Yii::$app->authManager;
        //Топ-менеджеры
        $topManager = $auth->createRole(User::ROLE_TOP_MANAGER);
        $topManager->description = 'Топ-менеджер';
        $auth->add($topManager);

        //Директор филиала
        $branch_director = $auth->createRole(User::ROLE_BRANCH_DIRECTOR);
        $branch_director->description = 'Директор филиала';
        $auth->add($branch_director);

        //Персонал
        $staff = $auth->createRole(User::ROLE_STAFF);
        $staff->description = 'Персонал';
        $auth->add($staff);

        //Создаем правило на редактирование пользователя
        $rule = new rules\UserEditRule();
        $auth->add($rule);

        //Получаем разрешение на редактирование пользователей
        $userPermission = $auth->getPermission('users');
        $userPermission->ruleName = $rule->name;
        $auth->update('users', $userPermission);
    }

    public static function updateAllUsersForRole()
    {
        $auth = Yii::$app->authManager;
        $users = User::find()->select(['user_id', 'position_id'])->all();
        $cnt = 0;
        foreach ($users as $user) {
            $role = $auth->getRolesByUser($user->user_id);
            if (empty($role)) {
                $userRole = User::getUserRoleByPosition($user->position_id);
                $roleObj = $auth->getRole($userRole);
                $auth->assign($roleObj, $user->user_id);
                $cnt++;
            }
        }

        echo 'Добавлено ролей: ' . $cnt;
    }

    public static function rulesInit()
    {
        self::addRoles();
        self::updateAllUsersForRole();
    }

}
