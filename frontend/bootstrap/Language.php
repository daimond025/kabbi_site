<?php

namespace frontend\bootstrap;

use Yii;
use yii\base\BootstrapInterface;

/**
 * Class Language
 * @package frontend\bootstrap
 */
class Language implements BootstrapInterface
{
    /**
     * @var array
     */
    public $supportedLanguages = [];

    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $user = user();

        if (is_object($user) && !empty($user->lang)) {
            $lang = $user->lang;
        } else {
            $lang = $this->getPreferredLanguage();
        }

        $app->language = $lang;
    }

    /**
     * Getting preferred language
     * @return string
     */
    public function getPreferredLanguage()
    {
        return Yii::$app->request->getPreferredLanguage($this->supportedLanguages);
    }
}
