<?php

namespace frontend\modules\companies\controllers;

use app\modules\tenant\models\User;
use common\modules\tenant\models\DefaultSettings;
use common\modules\tenant\models\TenantHasCity;
use common\modules\tenant\models\TenantSetting;
use frontend\modules\companies\components\repositories\TenantCompanyRepository;
use frontend\modules\companies\components\stripe\Stripe_user;
use frontend\modules\companies\components\stripe\StripeAuthClient;
use frontend\modules\companies\models\search\TenantCompanySearch;
use frontend\modules\companies\models\TenantCompany;
use frontend\modules\tenant\models\StripeRegistration;
use paymentGate\models\Disabled;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\rbac\Role;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use Yii;
use yii\web\Response;

class CompanyController extends Controller
{

    private $profileService;

    private $_stripe_receive =  'stripe-receive';

    public function init()
    {
        $this->profileService = app()->profileService;
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow'   => true,
                        'roles'   => ['top_manager',  User::ROLE_STAFF_COMPANY],
                    ],
                    [
                        'actions' => [
                            'stripe',
                            'stripe-receive'
                        ],
                        'allow'   => true,
                        'roles'   => ['top_manager', User::ROLE_STAFF_COMPANY],
                    ],
                ],
            ],
        ];
    }


    public function actionCreate()
    {
        $model = new TenantCompany();

        if ($model->load(post()) && $model->save()) {
            session()->setFlash('success', t('tenant_company', 'Company was successfully added.'));
            return $this->redirect(Url::to('/companies/company/list'));
        }

        return $this->render('create', [
            'model'            => $model,
            'useStripeConnect' => $this->getSettingUseStripeConnect(),
        ]);

    }

    public function actionList()
    {
        $user_role = current(app()->authManager->getRolesByUser(app()->user->getId()));
        $user_role_name = ($user_role instanceof Role) ? strtolower(ArrayHelper::getValue($user_role, 'name')) : null;

        $company_user = null;
        if($user_role_name ==  User::ROLE_STAFF_COMPANY){
            $company_user = isset( user()->tenant_company_id) ? user()->tenant_company_id : 0 ;
        }


        $searchModel = new TenantCompanySearch(['id_current' => $company_user]);
        return $this->render('index', [
            'dataProvider' => $searchModel->search(),
            'pjaxId'       => 'company-list-active',
        ]);

    }

    public function actionListBlocked()
    {
        $user_role = current(app()->authManager->getRolesByUser(app()->user->getId()));
        $user_role_name = ($user_role instanceof Role) ? strtolower(ArrayHelper::getValue($user_role, 'name')) : null;



        $block_show = ($user_role_name ==  User::ROLE_STAFF_COMPANY) ? 2 : 1;
        if (Yii::$app->request->isAjax) {
            $searchModel = new TenantCompanySearch(['is_block' => $block_show]);

            return $this->renderAjax('_grid', [
                'dataProvider' => $searchModel->search(),
                'pjaxId'       => 'company-list-block',
            ]);
        }

        return $this->redirect(['list', '#' => 'blocked']);

    }

    public function actionUpdate($id)
    {
        $user_role = current(app()->authManager->getRolesByUser(app()->user->getId()));
        $user_role_name = ($user_role instanceof Role) ? strtolower(ArrayHelper::getValue($user_role, 'name')) : null;

        if(($user_role_name == User::ROLE_STAFF_COMPANY) && (!user()->tenant_company_id)){
            return $this->redirect(Url::to('/companies/company/list/' . user()->tenant_company_id));
        }
        else if (($user_role_name == User::ROLE_STAFF_COMPANY) && ($id != user()->tenant_company_id)){
            return $this->redirect(Url::to('/companies/company/update/' . user()->tenant_company_id));
        }

        $model = $this->findTenantCompany($id);
        $model->scenario = TenantCompany::SCENARIO_UPDATE;

        if ($model->load(post()) && $model->save()) {
            session()->setFlash('success', t('tenant_company', 'Company was successfully updated.'));

            return $this->redirect(Url::to('/companies/company/update/' . $id));
        }

        return $this->render('update', [
            'model'            => $model,
            'useStripeConnect' => $this->getSettingUseStripeConnect(),
            'userRoleName' => $user_role_name
        ]);

    }

    public function actionStripe($id)
    {
        $model = $this->findTenantCompany($id);
        $model->scenario = TenantCompany::SCENARIO_UPDATE;


        $cities = user()->getUserCityList();
        $currentCityId = $this->profileService->getCurrentCityId(null, []);
        $paymentName = $this->profileService->getPaymentName(user()->tenant_id, $currentCityId);

        // профиль платежного
        $profile = $this->profileService->getProfile($paymentName);
        $url = Yii::$app->request->getHostInfo().Url::toRoute( $this->id .'/' . $this->_stripe_receive  );

        $stripeApi = new StripeAuthClient([
            'key'          => $profile->privateKey,
            'clientId'          => $profile->clientId,
            'redirectUri'       => $url,
        ]);

        $stripeDate = new Stripe_user();
        $stripeDate->phone_number = '465456';

        if(!isset($get['code'])){
            $stripeUrl = $stripeApi->getAuthorizationUrl();
            return app()->response->redirect($stripeUrl);
        }






        var_dump(12);
        exit();

        $provider = new \AdamPaterson\OAuth2\Client\Provider\Stripe([
            'clientId'          => 'ca_Go4GFejJREOlAPE7fFbOzQH4C4xqGjEm',
            'redirectUri'       => $url,
            'response_type'     => 'code',
        ]);

        $option = [
            'scope'        => 'read_write',
        ];



        if (!isset($_GET['code'])){
            $authUrl = $provider->getAuthorizationUrl($option);
            $_SESSION['oauth2state'] = $provider->getState();
            header('Location: '.$authUrl);
            exit;
        } /*elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {

            unset($_SESSION['oauth2state']);
            exit('Invalid state');

        }*/ else {

            $token = $provider->getAccessToken('authorization_code', [
                'code' => $_GET['code']
            ]);



            // Optional: Now you have a token you can look up a users profile data
            try {

                // We got an access token, let's now get the user's details
                $account = $provider->getResourceOwner($token);

                // Use these details to create a new profile
                printf('Hello %s!', $account->getDisplayName());

            } catch (Exception $e) {

                // Failed to get user details
                exit('Oh dear...');
            }

            // Use this to interact with an API on the users behalf
            echo $token->getToken();
        }

    }

    public function actionStripeReceive(){

        $request = app()->request->get();
        $stripe_model = new StripeRegistration;

        if($stripe_model->load($request)){
            $stripe_request = StripeRegistration::findByState($stripe_model->state);

            if($stripe_request){
                $currentCityId = $this->profileService->getCurrentCityId(null, []);
                $paymentName = $this->profileService->getPaymentName(user()->tenant_id, $currentCityId);

                // профиль платежного
                $profile = $this->profileService->getProfile($paymentName);

                $stripeApi = new StripeAuthClient([
                    'key'          => $profile->privateKey
                ]);

                $token = $stripeApi->getAccessToken('authorization_code', [
                    'code' => $_GET['code']
                ]);
                var_dump($profile);
                exit();




            }
        }

        $url = Yii::$app->request->getHostInfo().Url::toRoute( $this->id .'/list' );
        return app()->response->redirect($url);


    }

    public function actionUploadLogo($entity_id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $entity = $this->findTenantCompany($entity_id);
        $entity->load(post());

        if ($entity->save(true, ['logo'])) {
            return [
                'src' => [
                    'big'   => $entity->getPicturePath($entity->logo),
                    'thumb' => $entity->getPicturePath($entity->logo, false),
                ],
            ];
        }

        return $entity->getFirstErrors();
    }

    /**
     * @param $id
     *
     * @return TenantCompany
     * @throws NotFoundHttpException
     */
    protected function findTenantCompany($id)
    {
        $tenantCompany = TenantCompanyRepository::selfCreate()
            ->findByCurrentTenant(['tenant_company_id' => $id]);

        if (!$tenantCompany) {
            throw new NotFoundHttpException();
        }

        return array_shift($tenantCompany);
    }

    /**
     * @return bool
     */
    private function getSettingUseStripeConnect()
    {
        $cityIds = ArrayHelper::getColumn(TenantHasCity::getCityList(), 'city_id');
        foreach ($cityIds as $cityId) {
            if ((string)TenantSetting::getSettingValue(user()->tenant_id,
                    DefaultSettings::SETTING_USE_STRIPE_CONNECT, $cityId) === '1') {
                return true;
            }        }

        return false;
    }
}