<?php

/* @var $model \frontend\modules\companies\models\TenantCompany */
/* @var $useStripeConnect bool */
/* @var $userRoleName string */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\widgets\file\Logo;
use yii\helpers\Url;
use app\modules\tenant\models\User;


$users = User::find()->where(['tenant_company_id' => $model->tenant_company_id])->orderBy('last_name')->asArray()->all();

$userList = [];
if(!$model->isNewRecord){
    foreach ($users as $user){
        $userList[$user['user_id']] = $user['last_name'] . ' ' . $user['name'];
    }

}



?>

<?php $form = ActiveForm::begin([
    'id' => 'tenant-company-form',
]); ?>


<!--Фото-->
<? if ($withLogo !== false && !$model->isNewRecord): ?>
    <?= Logo::widget([
        'form'         => $form,
        'model'        => $model,
        'attribute'    => 'logo',
        'ajax'         => true,
        'uploadAction' => Url::to(['/companies/company/upload-logo/', 'entity_id' => $model->tenant_company_id]),
    ]); ?>
<? endif ?>

    <section class="row">
        <?= $form->field($model, 'name')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'name') ?></div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'name') ?>
            <?= Html::error($model, 'name',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'name')->end(); ?>
    </section>

    <section class="row">
        <?= $form->field($model, 'phone')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'phone') ?></div>
        <div style="display: inline-block; zoom: 1; width: 27%; vertical-align: top;">
            <?= Html::activeTextInput($model, 'phone', ['placeholder' => '0951123123']) ?>
            <?= Html::error($model, 'phone',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>

        <div style="display: inline-block;width: 7%; margin-right: 1%;vertical-align: top;text-align: right;">
            <?= Html::activeLabel($model, 'fax') ?></div>
        <div style="display: inline-block; zoom: 1; width: 27%; vertical-align: top;">
            <?= Html::activeTextInput($model, 'fax', ['placeholder' => '0951123123']) ?>
            <?= Html::error($model, 'fax',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'fax')->end(); ?>

    </section>

    <? if(!app()->user->can(User::ROLE_STAFF_COMPANY)):    ?>
    <section class="row">
        <?= $form->field($model, 'percent')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'percent') ?></div>
        <div  style="display: inline-block; zoom: 1; vertical-align: top; width: 27%;">
            <?= Html::activeTextInput($model, 'percent', ['placeholder' => 'Процент от 0% до 100% '])?>

            <?= Html::error($model, 'percent',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>

        </div>
        <?= $form->field($model, 'percent')->end(); ?>
    </section>
    <? endif;?>


    <section class="row">
        <?= $form->field($model, 'street')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'street') ?></div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'street', ['placeholder' => 'Musterstrabe 123']) ?>
            <?= Html::error($model, 'street',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'street')->end(); ?>
    </section>

    <section class="row">
            <?= $form->field($model, 'city')->begin(); ?>
            <div class="row_label"><?= Html::activeLabel($model, 'city') ?></div>
            <div style="display: inline-block; zoom: 1; width: 25%; vertical-align: top;">
                <?= Html::activeTextInput($model, 'city', ['placeholder' => 'Bamberg']) ?>
                <?= Html::error($model, 'city',
                    ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
            </div>

        <div style="display: inline-block;width: 9%; margin-right: 1%;vertical-align: top;text-align: right;">
            <?= Html::activeLabel($model, 'city_code') ?></div>
        <div style="display: inline-block; zoom: 1; width: 25%; vertical-align: top;">
            <?= Html::activeTextInput($model, 'city_code', ['placeholder' => '96049']) ?>
            <?= Html::error($model, 'city_code',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
            <?= $form->field($model, 'city_code')->end(); ?>

    </section>

    <section class="row">
        <?= $form->field($model, 'tax_number')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'tax_number') ?></div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'tax_number', ['placeholder' => '204/204/20008']) ?>
            <?= Html::error($model, 'tax_number',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'tax_number')->end(); ?>
    </section>

    <section class="row">
        <?= $form->field($model, 'site')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'site') ?></div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'site', ['placeholder' => 'www.muster.com']) ?>
            <?= Html::error($model, 'site',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'site')->end(); ?>
    </section>


    <section class="row">
        <?= $form->field($model, 'sort')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'sort') ?></div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'sort') ?>
            <?= Html::error($model, 'sort',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'sort')->end(); ?>
    </section>

    <? if(!$model->isNewRecord):?>
    <section class="row">
        <?= $form->field($model, 'user_contact')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'user_contact') ?></div>
        <div class="row_input">
            <?= Html::activeDropDownList(
                $model,
                'user_contact',
                $userList,
                ['class' => 'default_select']
            ) ?>
            <?= Html::error($model, 'user_contact',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'user_contact')->end(); ?>
    </section>
    <? endif;?>

    <section class="row">
        <?= $form->field($model, 'use_logo_company')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'use_logo_company') ?></div>
        <div class="row_input">
            <?= Html::activeDropDownList(
                $model,
                'use_logo_company',
                [
                    0 => t('tenant_company', 'No'),
                    1 => t('tenant_company', 'Yes')
                ],
                ['class' => 'default_select']
            ) ?>
            <?= Html::error($model, 'use_logo_company',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'use_logo_company')->end(); ?>
    </section>





<?php if ($useStripeConnect): ?>
    <section class="row">
        <?= $form->field($model, 'stripe_account')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'stripe_account') ?></div>
        <div class="row_input"  style="display: inline-block; zoom: 1; vertical-align: top; width: 27%;">
            <?
              $change_stripe_id = true;
              if(!app()->user->can(User::ROLE_STAFF_COMPANY)){+
                $change_stripe_id = true;
              }
            ?>
            <?= Html::activeTextInput($model, 'stripe_account', ['disabled' => $change_stripe_id] ) ?>

            <?= Html::a(t('tenant_company', 'Create Stripe account'), ['company/stripe/' . $model->tenant_company_id]) ?>

            <?= Html::error($model, 'stripe_account',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'stripe_account')->end(); ?>
    </section>
<?php endif; ?>


    <section class="submit_form">
        <div>
            <label><?= Html::activeCheckbox($model, 'block', ['label' => null]) ?>
                <b><?= t('tenant_company', 'Block') ?></b></label>
        </div>
        <?= Html::submitInput(t('tenant_company', 'Save')) ?>
    </section>


<?php $form = ActiveForm::end(); ?>