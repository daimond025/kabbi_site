<?php

/* @var $model \frontend\modules\companies\models\TenantCompany */

/* @var $useStripeConnect bool */

use yii\helpers\Html;

$this->title = Yii::t('tenant_company', 'Add company');

?>


<?= Html::tag('div', Html::a(t('tenant_company', 'Companies'), ['/companies/company/list']), [
    'class' => 'bread'
]); ?>

<?= Html::tag('h1', Html::encode($this->title)); ?>


<?= $this->render('_form', [
    'model'            => $model,
    'useStripeConnect' => $useStripeConnect,
]); ?>