<?php

/* @var $model \frontend\modules\companies\models\TenantCompany */

/* @var $useStripeConnect bool */
/* @var $userRoleName string */

use yii\helpers\Html;

$this->title = $model->name;

?>

<?= Html::tag('div', Html::a(t('tenant_company', 'Companies'), ['/companies/company/list']), [
    'class' => 'bread'
]); ?>

<?= Html::tag('h1', Html::encode($this->title)); ?>


<?= $this->render('_form', [
    'model'            => $model,
    'useStripeConnect' => $useStripeConnect,
    'userRoleName' => $userRoleName,
]); ?>