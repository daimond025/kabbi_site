<?php

/* @var $this \yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $pjaxId \string */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = t('tenant_company', 'Companies');

if (app()->user->can('top_manager')) {
    echo Html::a(t('app', 'Add'), ['create'], ['class' => 'button', 'style' => 'float: right; position: relative; top: 5px;']);
}
?>

<h1><?= Html::encode($this->title) ?></h1>


<section class="main_tabs">
    <div class="tabs_links">
        <ul>
            <li><a href="#active" class="t01"><?= t('app', 'Active') ?></a></li>
            <li><a href="#blocked" class="t02" data-href="<?= Url::to('list-blocked') ?>"><?= t('app', 'Blocked') ?></a>
            </li>
        </ul>
    </div>
    <div class="tabs_content">
        <div id="t01" data-view="index">
            <?= $this->render('_grid', compact('dataProvider', 'pjaxId')) ?>
        </div>
        <div id="t02" data-view="blocked">
        </div>
    </div>
</section>