<?php

/* @var $this \yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $pjaxId \string */

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\modules\companies\models\TenantCompany;
use frontend\widgets\LinkPager;
use yii\widgets\Pjax;

?>
<? Pjax::begin(['id' => $pjaxId]) ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'       => "{items}\n{pager}",
    'tableOptions' => [
        'class' => 'people_table',
    ],
    'pager'        => [
        'class'         => LinkPager::className(),
        'prevPageLabel' => t('app', 'Prev'),
        'nextPageLabel' => t('app', 'Next'),
    ],
    'columns'      => [

        [
            'attribute'      => 'name',
            'headerOptions'  => ['class' => 'pt_fio'],
            'content'        => function (TenantCompany $model) {
                $text = Html::encode($model->name);

                return Html::a($text, ['update', 'id' => $model->tenant_company_id]);
                return Html::a($text,['update', 'id' => $model->tenant_company_id], ['data-pjax' => 0]);
            },
            'contentOptions' => ['class' => 'pt_fio'],
        ],

        [
            'attribute'      => 'sort',
            'headerOptions'  => ['class' => 'pt_job'],
            'contentOptions' => ['class' => 'pt_fio'],
        ],
    ],
]);
?>
<? Pjax::end() ?>

