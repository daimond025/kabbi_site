<?php

namespace frontend\modules\companies\models\search;


use frontend\modules\companies\models\TenantCompany;
use yii\data\ActiveDataProvider;


class TenantCompanySearch extends TenantCompany
{

    public $is_block = 0;
    public $id_current = null;

    const PAGE_SIZE = 20;


    public function rules()
    {
        return [
            ['is_block', 'in', 'range' => [0, 1]],
            ['id_current', 'default', 'value' => null],
            ['id_current', 'integer'],
        ];
    }

    public function search()
    {

        $query = TenantCompany::find();
        $query->where([
            'tenant_id' => user()->tenant_id,
            'block'     => $this->is_block,
        ])->andFilterWhere([
            'tenant_company_id' => $this->id_current
        ]);

        return new ActiveDataProvider([
            'query'      => $query,
            'sort'       => [

                'defaultOrder' => [
                    'sort' => SORT_DESC,
                    'name' => SORT_ASC,
                ],

                'attributes' => [
                    'sort' => [
                      'asc' => ['sort'=>SORT_ASC],
                      'desc' => ['sort'=>SORT_DESC]
                    ],
                    'name' => [
                        'asc'  => [
                            "SUBSTRING_INDEX(name,' ',1)"                           => SORT_ASC,
                            "SUBSTRING_INDEX(SUBSTRING_INDEX(name,' ',2),' ',-1)"   => SORT_ASC,
                            "SUBSTRING_INDEX(SUBSTRING_INDEX(name,' ',3),' ',-1)+0" => SORT_ASC,
                        ],
                        'desc' => [
                            "SUBSTRING_INDEX(name,' ',1)"                           => SORT_DESC,
                            "SUBSTRING_INDEX(SUBSTRING_INDEX(name,' ',2),' ',-1)"   => SORT_DESC,
                            "SUBSTRING_INDEX(SUBSTRING_INDEX(name,' ',3),' ',-1)+0" => SORT_DESC,
                        ],

                    ],

                ],

            ],
            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);
    }

}