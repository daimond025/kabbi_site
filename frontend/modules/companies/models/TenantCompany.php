<?php

namespace frontend\modules\companies\models;

use app\models\User;
use common\modules\car\models\Car;
use common\modules\employee\models\worker\Worker;
use common\modules\employee\models\worker\WorkerTariff;
use common\modules\order\models\Order;
use common\modules\tenant\models\Tenant;
use frontend\components\behavior\file\CropFileBehavior;
use frontend\modules\companies\components\repositories\TenantCompanyRepository;
use Yii;

/**
 * This is the model class for table "{{%tenant_company}}".
 *
 * @property integer        $tenant_company_id
 * @property integer        $tenant_id
 * @property string         $name
 * @property string         $phone
 * @property integer        $sort
 * @property integer        $block
 * @property integer        $logo
 * @property integer        $use_logo_company
 * @property string         $stripe_account
 * @property string         $street
 * @property string         $city
 * @property string         $city_code
 * @property string         $tax_number
 * @property string         $fax
 * @property string         $site
 * @property integer        $user_contact
 * @property integer        $report_id
 *
 * @property Car[]          $cars
 * @property Order[]        $orders
 * @property Tenant         $tenant
 * @property User[]         $users
 * @property Worker[]       $workers
 * @property WorkerTariff[] $workerTariffs
 */
class TenantCompany extends \yii\db\ActiveRecord
{

    const SCENARIO_UPDATE = 'scenarioUpdate';
    const FIELD_NAME_MAIN_COMPANY = 'nameMainCompany';

    public $cropParams;

    public $num;

    public function behaviors()
    {
        return [
            'fileBehavior' => [
                'class'      => CropFileBehavior::className(),
                'fileField'  => ['logo'],
                'upload_dir' => app()->params['upload'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_company}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'filter', 'filter' => 'trim'],
            [['block', 'name'], 'required'],

            [['sort', 'block'], 'integer'],

            ['sort', 'default', 'value' => 0],
            ['block', 'in', 'range' => [0, 1]],
            ['use_logo_company', 'in', 'range' => [0, 1]],

            [['name', 'stripe_account'], 'string', 'max' => 255],

            [
                'name',
                function ($attribute) {
                    if (TenantCompanyRepository::selfCreate()->findByCurrentTenant(['name' => $this->$attribute])) {
                        $this->addError($attribute, t('tenant_company', '«{attribute}» must be unique.',
                            ['attribute' => $this->getAttributeLabel($attribute)]));
                    }
                },
                'except' => self::SCENARIO_UPDATE,
            ],

            [
                ['tenant_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Tenant::className(),
                'targetAttribute' => ['tenant_id' => 'tenant_id'],
            ],


            [
                ['logo'],
                'image',
                'extensions' => ['png', 'jpeg', 'jpg'],
                'maxSize'    => getUploadMaxFileSize(),
                'tooBig'     => t('file', 'The file size must be less than {file_size}Mb',
                    ['file_size' => getUploadMaxFileSize(true)]),
                'minWidth'   => 300,
                'minHeight'  => 300,
            ],

            ['phone', 'match', 'pattern' => '/^[0-9]{1,20}$/'],


            // daimond
            [['street', 'city', 'city_code', 'tax_number',   'fax',   'site' ], 'filter', 'filter' => 'trim'],
            [[ 'city' ], 'required'],


            [['percent'], 'number', 'min' => 0],
            [['percent'], 'number', 'max' => 100],

            //['city_code', 'match', 'pattern' => '/^[0-9]{1,20}$/'],
            ['fax', 'match', 'pattern' => '/^[0-9]{1,20}$/'],

            [['street', 'city', 'tax_number',  'site' ], 'string', 'max' => 128],

            ['user_contact','safe' ],
            ['report_id', 'safe' ],
            ['cropParams', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tenant_company_id' => 'Tenant Company ID',
            'tenant_id'         => 'Tenant ID',
            'name'              => t('tenant_company', 'Name'),
            'phone'             => t('tenant_company', 'Phone'),
            'sort'              => t('app', 'Sort'),
            'block'             => 'Block',
            'use_logo_company'  => t('tenant_company', 'Use the logo in the app'),
            'stripe_account'  => t('tenant_company', 'Stripe account'),

            // daimond
            'percent'             => t('tenant_company', 'Percent'),
            'street'             => t('tenant_company', 'Street'),
            'city'             => t('tenant_company', 'City'),
            'city_code'             => t('tenant_company', 'City_code'),
            'tax_number'             => t('tenant_company', 'StNr'),
            'contact_name'             => t('tenant_company', 'Contact_name'),
            'fax'             => t('tenant_company', 'Fax'),
            'site'             => t('tenant_company', 'Site'),
            'user_contact'             => t('tenant_company', 'User_contact'),
        ];
    }

    public function beforeSave($insert)
    {
        $this->tenant_id = user()->tenant_id;

        return parent::beforeSave($insert);
    }


    public function updateIdReport(TenantCompany $company){
        if(empty($company->report_id)){
            $company->report_id = 1;
        }else{
            $company->report_id += 1;
        }
        $company->save(false);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::className(), ['tenant_company_id' => 'tenant_company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['tenant_company_id' => 'tenant_company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['tenant_company_id' => 'tenant_company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkers()
    {
        return $this->hasMany(Worker::className(), ['tenant_company_id' => 'tenant_company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerTariffs()
    {
        return $this->hasMany(WorkerTariff::className(), ['tenant_company_id' => 'tenant_company_id']);
    }
}
