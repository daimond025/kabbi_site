<?php

namespace frontend\modules\companies\components\stripe;

use frontend\modules\tenant\models\StripeRegistration;
use yii\base\Component;
use yii\base\Event;


class StripeStateMessage extends Event
{
    const EVENT_PRE_STATE= 'pre_state';

    public $state = null;
}

class StripeStateSave extends  Component
{

    public  function HandlerState($event){
        $stripe_registration = new StripeRegistration();
        $stripe_registration->state = $event->state;
        $stripe_registration->user_id = user()->id;
        $stripe_registration->tenant_company_id = user()->tenant_company_id;

        $stripe_registration->save();
    }
}
