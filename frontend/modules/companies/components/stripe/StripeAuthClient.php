<?php
namespace frontend\modules\companies\components\stripe;

use AdamPaterson\OAuth2\Client\Provider\StripeResourceOwner;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Token\AccessToken;
use Psr\Http\Message\ResponseInterface;

use yii\base\Component;

class StripeAuthClient extends AbstractProvider
{
    /*
     * @public 0 - Standard account  1 -  Express account
     * */
    private $_save_sql = 0;

    /*
     * @public 0 - Standard account  1 -  Express account
     * */
    private $authUrl = 0;


    private $_stripe_state = null;


    public function __construct(array $options = [], array $collaborators = [])
    {
        $this->_stripe_state = new StripeStateSave();
        $this->_stripe_state->on(StripeStateMessage::EVENT_PRE_STATE, [$this->_stripe_state, 'HandlerState']);

        parent::__construct($options, $collaborators);
    }

    /**
     * Get authorization url to begin OAuth flow
     *
     * @return string
     */
    public function getBaseAuthorizationUrl()
    {
        if($this->authUrl == 0){
            return 'https://connect.stripe.com/oauth/authorize';
        }
        elseif($this->authUrl == 1){
            return 'https://connect.stripe.com/oauth/authorize';
        }

    }

    /**
     * Get access token url to retrieve token
     *
     * @param array $params
     *
     * @return string
     */
    public function getBaseAccessTokenUrl(array $params)
    {
        return 'https://connect.stripe.com/oauth/token';
    }

    /**
     * Get provider url to fetch user details
     *
     * @param AccessToken $token
     *
     * @return string
     */
    public function getResourceOwnerDetailsUrl(AccessToken $token)
    {
        return 'https://api.stripe.com/v1/account';
    }

    /**
     * Get deauthorization url to end OAuth flow
     *
     * @return string
     */
    public function getBaseDeauthorizationUrl()
    {
        return 'https://connect.stripe.com/oauth/deauthorize';
    }

    /**
     * Get the default scopes used by this provider.
     *
     * @return array
     */
    protected function getDefaultScopes()
    {
        return 'read_write';
    }

    /**
     * Returns authorization parameters based on provided options.
     *
     * @param  array $options
     * @return array Authorization parameters
     */
    protected function getAuthorizationParameters(array $options)
    {
        if (empty($options['state'])) {
            $options['state'] = $this->getRandomState();
        }

        if (empty($options['scope'])) {
            $options['scope'] = $this->getDefaultScopes();
        }


        $options += [
            'response_type'   => 'code'
        ];

        $this->state = $options['state'];

        if (!isset($options['redirect_uri'])) {
            $options['redirect_uri'] = $this->redirectUri;
        }

        $options['client_id'] = $this->clientId;


        if($this->_stripe_state instanceof StripeStateSave){
            $stripe_message = new StripeStateMessage();
            $stripe_message->state = $options['state'];
            $this->_stripe_state->trigger(StripeStateMessage::EVENT_PRE_STATE, $stripe_message );
        }

        return $options;
    }


    /**
     * Check a provider response for errors.
     *
     * @param ResponseInterface $response
     * @param array|string $data
     *
     * @throws IdentityProviderException
     */
    protected function checkResponse(ResponseInterface $response, $data)
    {
        if ($response->getStatusCode() >= 400) {
            throw new IdentityProviderException(
                $data['error'] ?: $response->getReasonPhrase(),
                $response->getStatusCode(),
                $response
            );
        }
    }


    /**
     * Generate a user object from a successful user details request.
     *
     * @param array $response
     * @param AccessToken $token
     *
     * @return StripeResourceOwner
     */
    protected function createResourceOwner(array $response, AccessToken $token)
    {
        return new StripeResourceOwner($response);
    }

}