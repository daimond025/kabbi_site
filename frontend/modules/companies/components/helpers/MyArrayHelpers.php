<?php

namespace frontend\modules\companies\components\helpers;


class MyArrayHelpers
{

    public static function convertValuesArrayToString($array)
    {

        if (is_array($array)) {
            return array_map(function ($value) {
                return (string)$value;
            }, $array);
        }

        return $array;
    }

}