<?php

namespace frontend\modules\companies\components\repositories;


use app\modules\tenant\models\User;
use frontend\modules\companies\models\TenantCompany;
use yii\helpers\ArrayHelper;

class TenantCompanyRepository
{
    public static function selfCreate()
    {
        return new self();
    }

    public function findByCurrentTenant($condition = [])
    {
        return TenantCompany::findAll(array_merge([
            'tenant_id' => user()->tenant_id,
        ], $condition));
    }

    public function getForForm($condition = [], $isAddNameMainCompany = true)
    {
        $isCompanyStaff = app()->user->can(User::ROLE_STAFF_COMPANY);

        if ($isCompanyStaff) {
            $condition = array_merge($condition, ['tenant_company_id' => user()->tenant_company_id]);
        }

        $res = ArrayHelper::map(
            $this->findByCurrentTenant(array_merge(['block' => 0], $condition)),
            'tenant_company_id',
            'name'
        );

        if ($isCompanyStaff) {
            return $res;
        }

        if ($isAddNameMainCompany) {
            $res = ArrayHelper::merge(
                [TenantCompany::FIELD_NAME_MAIN_COMPANY => user()->tenant->company_name],
                $res
            );
        }

        return $res;
    }

}