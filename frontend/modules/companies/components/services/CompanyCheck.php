<?php

namespace frontend\modules\companies\components\services;


use app\modules\order\models\OrderStatus;
use app\modules\tenant\models\User;
use yii\web\ForbiddenHttpException;

class CompanyCheck
{
    protected static $ignoreStatusForStaff = [1, 2, 3, 108, 109];

    public function workWithFilterMap($filterData = [])
    {
        if (app()->user->can(User::ROLE_STAFF_COMPANY)) {
            $filterData = array_merge($filterData, ['tenantCompanies' => [user()->tenant_company_id]]);
        }

        return $filterData;
    }


    public function isAccess($model)
    {
        if (app()->user->can(User::ROLE_STAFF_COMPANY)) {
            if (!app()->user->can(User::ROLE_STAFF_COMPANY, [
                'model' => $model
            ])
            ) {
                throw new ForbiddenHttpException(t('tenant_company', 'Access denied.'));
            }
        }
    }

    public function workWithCityFilterMap()
    {
        $filterData = [];
        if (app()->user->can(User::ROLE_STAFF_COMPANY)) {
            $filterData = array_keys( user()->getUserCityList());
        }

        return $filterData;
    }

    public function workFilterByStatus()
    {
        $filterData = [];
        if (app()->user->can(User::ROLE_STAFF_COMPANY)) {
            $filterData =  self::$ignoreStatusForStaff;
        }
        return $filterData;
    }

}