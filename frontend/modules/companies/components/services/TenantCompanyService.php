<?php

namespace frontend\modules\companies\components\services;


use app\modules\tenant\models\User;
use frontend\modules\companies\components\repositories\TenantCompanyRepository;
use frontend\modules\companies\models\TenantCompany;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

class TenantCompanyService
{

    protected $arrayWhereIfStaffCompany = null;

    public function __construct(array $arrayWhereIfStaffCompany = null)
    {
        $this->arrayWhereIfStaffCompany = $arrayWhereIfStaffCompany;
    }


    public function queryChange(ActiveQuery $query, $filterTenantCompanyIds, $changeField)
    {

        if (app()->user->can(User::ROLE_STAFF_COMPANY)) {
            $where = is_array($this->arrayWhereIfStaffCompany) ? $this->arrayWhereIfStaffCompany : [$changeField => user()->tenant_company_id];
            $query->andWhere($where);
        } else {

            if (is_array($filterTenantCompanyIds)
                &&
                in_array(TenantCompany::FIELD_NAME_MAIN_COMPANY, $filterTenantCompanyIds)
            ) {
                $excludedIds = self::getIdsExcludedTenantCompany($filterTenantCompanyIds);
                if ($excludedIds) {
                    $query->andWhere([
                        'or',
                        ['NOT IN', $changeField, $excludedIds],
                        ['IS', $changeField, null]
                    ]);
                }
            } else {
                $query->andFilterWhere([$changeField => $filterTenantCompanyIds]);
            }
        }
    }



    public static function getIdsExcludedTenantCompany($selectedTenantCompanyIds)
    {
        $allIdsTenantCompany = ArrayHelper::getColumn(TenantCompanyRepository::selfCreate()
            ->findByCurrentTenant(), 'tenant_company_id');

        return array_diff($allIdsTenantCompany, $selectedTenantCompanyIds);
    }
}