<?php

namespace frontend\modules\promocode\components\services;


use frontend\modules\promocode\components\repositories\relateddata\PromoActionCountRepository;

class PromoService
{

    const ACTION_COUNT_BONUS = '_bonus';
    const ACTION_COUNT_DISCOUNT = '_discount';

    public function getPromoActionCountByType($type)
    {
        $items = PromoActionCountRepository::getAllDataColumn();

        if ($type == self::ACTION_COUNT_DISCOUNT) {
            unset($items[3]);
        }

        foreach ($items as $key => $item) {
            $items[$key] = t('promo', $item . $type);
        }

        return $items;
    }


}