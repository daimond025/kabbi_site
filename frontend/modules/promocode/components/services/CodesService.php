<?php

namespace frontend\modules\promocode\components\services;


use frontend\modules\promocode\components\extensions\codes\CodesGenerator;
use frontend\modules\promocode\components\extensions\codes\controllers\PromoController;
use frontend\modules\promocode\components\extensions\codes\settings\Setting;
use frontend\modules\promocode\components\repositories\WorkerRepository;
use frontend\modules\promocode\components\services\exceptions\ExceptionCodeService;
use frontend\modules\promocode\models\Promo;
use orderApi\OrderApi;

class CodesService
{
    /* @var $orderApi OrderApi */
    protected $orderApi;

    public function __construct()
    {
        $this->orderApi = \Yii::$app->get('orderApi');
    }

    public function createCodesForModel(Promo $model)
    {
        if (!$model->promo_id && false) {
            throw new ExceptionCodeService('Violation of the order of calling methods');
        }

        switch ($model->type_id) {
            case Promo::FIELD_TYPE_PROMO_MANUALLY:
                $model->saveCodes();
                break;

            case Promo::FIELD_TYPE_PROMO_GENERATION:
            case Promo::FIELD_TYPE_PROMO_DRIVERS:
                $result = $this->orderApi->generatedPromoCodes($model);
                if ($result['code'] != OrderApi::RESPONSE_STATUS_OK) {
                    throw new ExceptionCodeService('Error save promo-code.');
                }

                break;
        }
    }

    public function updateCodesForModel(Promo $model)
    {
        if (!$model->promo_id && false) {
            throw new ExceptionCodeService('Violation of the order of calling methods');
        }

        switch ($model->type_id) {
            case Promo::FIELD_TYPE_PROMO_MANUALLY:
                $model->updateCodes();
                break;

            case Promo::FIELD_TYPE_PROMO_GENERATION:
            case Promo::FIELD_TYPE_PROMO_DRIVERS:
                $result = $this->orderApi->generatedPromoCodes($model);
                if ($result['code'] != OrderApi::RESPONSE_STATUS_OK) {
                    throw new ExceptionCodeService('Error save promo-code.');
                }

                break;
        }
    }


}