<?php

namespace frontend\modules\promocode\components\services;


use app\modules\order\models\forms\OptionsSuitableCode;
use common\modules\order\models\Order;
use frontend\modules\promocode\models\PromoClient;
use frontend\modules\promocode\models\PromoCode;
use frontend\modules\promocode\models\search\PromoCodeSearch;
use frontend\modules\promocode\models\search\PromoOrderSearch;
use orderApi\interfaces\OptionsSuitablePromoCode;
use orderApi\OrderApi;

class PromoCodesService
{

    public function findSuitablePromoCodeForPanel($post)
    {
        $options                = new OptionsSuitableCode();
        $options->client_id     = $post['Order']['client_id'];
        $options->city_id       = $post['Order']['city_id'];
        $options->position_id   = $post['Order']['position_id'];
        $options->tariff_id     = $post['Order']['tariff_id'];
        $options->order_date    = $post['Order']['order_date'];
        $options->order_hours   = $post['Order']['order_hours'];
        $options->order_minutes = $post['Order']['order_minutes'];

        if ($options->getClientId()) {
            return $this->findSuitablePromoCode($options);
        }

        return null;
    }

    public function findSuitablePromoCode(OptionsSuitablePromoCode $options)
    {
        /* @var $orderApi OrderApi */
        $orderApi = \Yii::$app->get('orderApi');

        $promoCode = $orderApi->findSuitablePromoCode($options);

        if (($promoCode['code'] == OrderApi::RESPONSE_STATUS_OK)
            && array_key_exists('code_id', $promoCode['result'])
        ) {
            $promoCodeId = $promoCode['result']['code_id'];
        } else {
            $promoCodeId = null;
        }

        return $promoCodeId;
    }

    public function getPromoCodesList($promo_id, $pagination = true, $forGeneration = false, $blocked = false)
    {
        $searchModel = new PromoCodeSearch([
            'promo_id' => $promo_id,
        ]);

        return [
            'searchModel'  => $searchModel,
            'dataProvider' => $searchModel->search($pagination, $forGeneration, $blocked),
            'pjaxId'       => 'promo_codes',
        ];
    }

    public function prepareDataCodesToExcel($promo_id, $forGeneration = false, $blocked = false)
    {
        $codesSearchModel = new PromoCodeSearch([
            'promo_id' => $promo_id,
        ]);
        /* @var $codes PromoCode[] */
        $codes = $codesSearchModel
            ->search(false, $forGeneration, $blocked)
            ->getModels();

        $exportData = [];
        foreach ($codes as $code) {
            $exportData[] = ['code' => $code->code];
        }

        $headers = [
            'code' => $codesSearchModel->getAttributeLabel('code'),
        ];

        return ['headers' => $headers, 'exportData' => $exportData];

    }

    public function prepareDataWorkerCodesToExcel($promo_id, $blocked)
    {
        $codesSearchModel = new PromoCodeSearch([
            'promo_id' => $promo_id,
        ]);
        /* @var $codes PromoCode[] */
        $codes = $codesSearchModel
            ->search(false, false, $blocked)
            ->getModels();

        $exportData = [];
        foreach ($codes as $code) {
            $exportData[] = [
                'code'   => $code->code,
                'worker' => $code->worker->getFullName()
            ];
        }

        $headers = [
            'code'   => $codesSearchModel->getAttributeLabel('code'),
            'worker' => $codesSearchModel->getAttributeLabel('worker'),
        ];

        return ['headers' => $headers, 'exportData' => $exportData];

    }

    public function prepareUsedDataCodesToExcel($promo_id)
    {
        $ordersSearchModel = new PromoOrderSearch();
        $searchParam       = ['promo_id' => $promo_id];


        /* @var $orders Order[] */
        $orders = $ordersSearchModel->search($searchParam, false)->getModels();

        $exportData = [];
        foreach ($orders as $order) {
            $exportData[] = [
                'date'   => $order->status_time,
                'status' => $this->prepareStatus($order),
                'order'  => $order->order_number,
                'client' => $this->prepareClient($order),
                'worker' => $this->prepareWorker($order),
                'code'   => $order->promoCode->code,
            ];
        }

        $headers = [
            'date'   => t('promo', 'date'),
            'status' => t('promo', 'status'),
            'order'  => t('promo', 'Order'),
            'client' => t('promo', 'Client'),
            'worker' => t('promo', 'Worker'),
            'code'   => t('promo', 'Code'),
        ];

        return ['headers' => $headers, 'exportData' => $exportData];
    }

    public function getReportPromo($promo_id)
    {
        return $this->getReport(['promo_id' => $promo_id], 'promo_report_promo');
    }

    public function getReportWorker($worker_id)
    {
        return $this->getReport(['worker_id' => $worker_id], 'promo_report_worker');
    }

    public function getReportClient($client_id)
    {
        return $this->getReport(['client_id' => $client_id], 'promo_report_client');
    }

    protected function getReport($condition, $pjaxId)
    {
        $searchModel  = new PromoOrderSearch();
        $searchParam  = $condition;
        $dataProvider = $searchModel->search($searchParam);

        return [
            'dataProvider' => $dataProvider,
            'searchModel'  => $searchModel,
            'pjaxId'       => $pjaxId,
        ];
    }

    protected function prepareStatus(Order $order)
    {
        /* @var $promoClient PromoClient */
        $promoClient = $order
            ->promoCode
            ->getPromoClient()
            ->where(['client_id' => $order->client_id])
            ->one();

        return t('promo', $promoClient->statusCode->status_label);
    }

    protected function prepareClient(Order $order)
    {
        $client = $order->client;

        if (!$client) {
            return null;
        }

        $clientString = $client->getFullName();
        $clientString .= "\n";

        foreach ($order->client->clientPhones as $phone) {
            $clientString .= $phone->value . "\n";
        }

        return $clientString;
    }

    protected function prepareWorker(Order $order)
    {
        $worker = $order->worker;

        if (!$worker) {
            return null;
        }

        $workerString = '';
        $workerString .= $worker->getFullName();
        $workerString .= "\n";
        $workerString .= $order->car->name;
        $workerString .= "\n";
        $workerString .= $order->car->gos_number;
        $workerString .= "\n";

        return $workerString;
    }
}