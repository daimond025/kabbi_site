<?php
namespace frontend\modules\promocode\components\services;

use common\modules\car\models\CarClass;
use frontend\modules\promocode\components\repositories\CarClassRepository;
use yii\helpers\ArrayHelper;

class CarClassService
{
    public function getCarClassesForTypeCar($typeId, $cityId = null)
    {
        $allCarClasses = ArrayHelper::map(
            CarClass::find()
                ->where(['type_id' => $typeId])
                ->groupBy(['sort', 'class'])
                ->all(),
            'class_id',
            function ($array) {
                return t('car', $array['class']);
            }
        );

        if ( ! is_null($cityId)) {
            $idUsedClasses = CarClassRepository::getIdUsedCarClass($cityId, $typeId);
        } else {
            $idUsedClasses = [];
        }

        $carClasses = [];
        foreach ($allCarClasses as $key => $carClass) {
            $carClasses[] = [
                t('employee', $allCarClasses[$key]),
                in_array($key, $idUsedClasses) ? 'used' : 'notUsed',
                $key
            ];
        }

        return $carClasses;
    }
}