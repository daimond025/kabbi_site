<?php

namespace frontend\modules\promocode\components\services;


use frontend\modules\promocode\models\search\PromoSearch;

class ListDataService
{
    public function getListData($pjaxId, $blocked = 0)
    {
        $cityList            = user()->getUserCityList();
        $searchModel         = new PromoSearch(['blocked' => $blocked]);
        $searchModel
            ->accessCityList = array_keys($cityList);
        $dataProvider        = $searchModel->search(\Yii::$app->request->queryParams);

        return compact('cityList', 'searchModel', 'pjaxId', 'dataProvider');
    }
}