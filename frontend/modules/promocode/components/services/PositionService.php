<?php

namespace frontend\modules\promocode\components\services;


use common\modules\employee\models\position\Position;
use frontend\modules\promocode\components\repositories\PositionRepository;

class PositionService
{

    public function getPositions($cityId, $forTypeWorker = false)
    {
        $allPositions  = PositionRepository::getPositionByCity($cityId);

        $usedPositions = $forTypeWorker ? PositionRepository::getIdUsedPositions($cityId) : [];

        $positions = [];
        foreach ($allPositions as $key => $position) {
            $positions[$key] = [
                t('employee', $allPositions[$key]),
                in_array($key, $usedPositions) ? 'used' : 'notUsed'
            ];
        }

        return $positions;
    }

}