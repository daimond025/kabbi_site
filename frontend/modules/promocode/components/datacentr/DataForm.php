<?php

namespace frontend\modules\promocode\components\datacentr;


use frontend\modules\promocode\components\repositories\CarClassRepository;
use frontend\modules\promocode\components\repositories\PositionRepository;
use frontend\modules\promocode\components\repositories\relateddata\PromoActionClientsRepository;
use frontend\modules\promocode\components\repositories\relateddata\PromoActionCountRepository;
use frontend\modules\promocode\components\repositories\relateddata\PromoActivationTypeRepository;
use frontend\modules\promocode\components\repositories\relateddata\PromoPeriodTypeRepository;
use frontend\modules\promocode\components\repositories\relateddata\PromoSymbolTypeRepository;
use frontend\modules\promocode\components\repositories\relateddata\PromoTypeRepository;
use frontend\modules\promocode\components\services\CarClassService;
use frontend\modules\promocode\components\services\PromoService;
use frontend\modules\promocode\models\Promo;

class DataForm
{
    /* @var $promoService PromoService */
    protected $promoService;

    public function __construct()
    {
        $this->promoService = \Yii::$container->get(PromoService::class);
    }

    public function getDataForForm(Promo $promo)
    {
        return [
            'cityList'        => user()->getUserCityList(),
            'typesCarClasses' => CarClassRepository::getTypesCarClasses(),
            'carClasses'      => $promo->isNewRecord
                ? [] : (new CarClassService())->getCarClassesForTypeCar($promo->position_id),
            'namePosition'    => $promo->isNewRecord ? null : PositionRepository::getNamePosition($promo->position_id),

            'relatedData' => [
                'actionClients'       => PromoActionClientsRepository::getAllDataColumn(),
                'actionCountBonus'    => $this->promoService->getPromoActionCountByType(PromoService::ACTION_COUNT_BONUS),
                'actionCountDiscount' => $this->promoService->getPromoActionCountByType(PromoService::ACTION_COUNT_DISCOUNT),
                'activationType'      => PromoActivationTypeRepository::getAllDataColumn(),
                'periodType'          => PromoPeriodTypeRepository::getAllDataColumn(),
                'type'                => PromoTypeRepository::getAllDataColumn(),
                'symbolType'          => PromoSymbolTypeRepository::getAllDataColumn(),
            ],
        ];
    }
}