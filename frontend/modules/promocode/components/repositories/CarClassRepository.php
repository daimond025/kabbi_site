<?php

namespace frontend\modules\promocode\components\repositories;

use common\modules\employee\models\position\Position;
use frontend\modules\promocode\models\Promo;
use frontend\modules\promocode\models\PromoHasCarClass;
use frontend\modules\promocode\models\PromoHasCity;
use yii\helpers\ArrayHelper;

class CarClassRepository
{
    public static function getIdUsedCarClass($cityId, $typeCarId, $conditions = [])
    {
        $carClasses = Promo::find()->alias('p')
            ->select('`phcc`.`car_class_id`')
            ->leftJoin(
                PromoHasCity::tableName() . ' `phc`',
                '`p`.`promo_id` = `phc`.`promo_id`'
            )
            ->leftJoin(
                PromoHasCarClass::tableName() . ' `phcc`',
                '`p`.`promo_id` = `phcc`.`promo_id`'
            )
            ->leftJoin(
                Position::tableName() . ' `ps`',
                '`p`.`position_id` = `ps`.`position_id`'
            )
            ->where([
                '`ps`.`transport_type_id`' => $typeCarId,
                '`p`.`tenant_id`'          => user()->tenant_id,
                '`p`.`type_id`'            => Promo::FIELD_TYPE_PROMO_DRIVERS,
                '`p`.`blocked`'            => 0,
                '`phc`.`city_id`'          => $cityId,
            ])
            ->andFilterWhere($conditions)
            ->asArray();

        return ArrayHelper::getColumn($carClasses->all(), 'car_class_id');
    }

    public static function getTypesCarClasses()
    {
        $positions = Position::find()
            ->select(['position_id', 'transport_type_id'])
            ->where(['has_car' => 1])
            ->asArray()
            ->all();

        return ArrayHelper::map($positions, 'position_id', 'transport_type_id');
    }
}