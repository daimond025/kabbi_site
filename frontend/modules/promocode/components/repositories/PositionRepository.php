<?php

namespace frontend\modules\promocode\components\repositories;


use common\modules\employee\models\position\Position;
use common\modules\tenant\models\TenantCityHasPosition;
use frontend\modules\promocode\models\Promo;
use frontend\modules\promocode\models\PromoHasCity;
use yii\helpers\ArrayHelper;

class PositionRepository
{

    public static function getPositionByCity($cityId)
    {
        $positions = TenantCityHasPosition::find()->alias('tchp')
            ->select(['`tchp`.`position_id`', '`p`.`name`'])
            ->leftJoin(
                Position::tableName() . ' `p`',
                '`tchp`.`position_id` = `p`.`position_id`'
            )
            ->where([
                'tenant_id' => user()->tenant_id,
                'city_id'   => $cityId,
            ])
            ->asArray()
            ->all();

        return ArrayHelper::map($positions, 'position_id', 'name');
    }

    public static function getIdUsedPositions($cityId, $conditions = [])
    {
        $positions = Promo::find()->alias('p')
            ->select('`p`.`position_id`')
            ->leftJoin(
                PromoHasCity::tableName() . ' `phc`',
                '`p`.`promo_id` = `phc`.`promo_id`'
            )
            ->leftJoin(
                Position::tableName() . ' `ps`',
                '`p`.`position_id` = `ps`.`position_id`'
            )
            ->where([
                '`p`.`tenant_id`'          => user()->tenant_id,
                '`phc`.`city_id`'          => $cityId,
                '`p`.`blocked`'            => 0,
                '`p`.`type_id`'            => Promo::FIELD_TYPE_PROMO_DRIVERS,
                '`ps`.`transport_type_id`' => null,
            ])
            ->andFilterWhere($conditions)
            ->asArray();

        return ArrayHelper::getColumn($positions->all(), 'position_id');
    }

    public static function getNamePosition($positionId)
    {
        $name = Position::find()
            ->select('name')
            ->where(['position_id' => $positionId])
            ->asArray()
            ->one();

        return t('employee', $name['name']);
    }


    public static function getTransportTypeId($positionId)
    {
        return Position::find()
            ->select('transport_type_id')
            ->where([
                'position_id' => $positionId
            ])
            ->scalar();
    }

}