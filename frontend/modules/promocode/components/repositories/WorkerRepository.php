<?php

namespace frontend\modules\promocode\components\repositories;


use common\modules\car\models\Car;
use common\modules\employee\models\worker\Worker;
use common\modules\employee\models\worker\WorkerHasCar;
use common\modules\employee\models\worker\WorkerHasCity;
use common\modules\employee\models\worker\WorkerHasPosition;

class WorkerRepository
{
    /**
     * @param $tenantId integer
     * @param $cityId integer
     * @param $positionId integer
     * @param $carClasses integer[]
     *
     * @return array
     */
    public static function getWorkersForPromoCodes($tenantId, $cityId, $positionId, $carClasses = null)
    {
        $query = Worker::find()
            ->alias('w')
            ->leftJoin(WorkerHasCity::tableName() . ' `whc`', '`w`.`worker_id` = `whc`.`worker_id`')
            ->leftJoin(WorkerHasPosition::tableName() . ' `whp`', '`w`.`worker_id` = `whp`.`worker_id`')
            ->where([
                '`w`.`tenant_id`'     => $tenantId,
                '`whc`.`city_id`'     => $cityId,
                '`whp`.`position_id`' => $positionId,
            ]);

        if ($carClasses) {
            $subQuery = WorkerHasCar::find()->alias('whcr')
                ->leftJoin(Car::tableName(). ' `c`', '`c`.`car_id` = `whcr`.`car_id`')
                ->groupBy(['`whcr`.`has_position_id`', '`c`.`class_id`']);

            $query
                ->select('`w`.*, `c`.`class_id` AS car_class_id')
                ->leftJoin(['whcr' => $subQuery], '`whp`.`id` = `whcr`.`has_position_id`')
                ->leftJoin(Car::tableName() . ' `c`', '`whcr`.`car_id` = `c`.`car_id`')
                ->andWhere(['`c`.`class_id`' => $carClasses]);
        }

        return $query->createCommand()->queryAll();
    }
}