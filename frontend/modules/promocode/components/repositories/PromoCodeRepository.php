<?php

namespace frontend\modules\promocode\components\repositories;


use frontend\modules\promocode\models\Promo;
use frontend\modules\promocode\models\PromoCode;

class PromoCodeRepository
{
    public static function getDuplicateCodesCurrentTenant(array &$codes)
    {
        return PromoCode::find()->alias('c')
            ->select('`c`.`code`')
            ->leftJoin(
                Promo::tableName() . ' `p`',
                '`c`.`promo_id` = `p`.`promo_id`'
            )
            ->where(['`c`.`code`' => $codes])
            ->andWhere(['`p`.`tenant_id`' => user()->tenant_id])
            ->asArray()
            ->all();
    }

    public static function getCountCodesByRegexpCurrentTenant($regexp)
    {
        return PromoCode::find()->alias('c')
            ->leftJoin(
                Promo::tableName() . ' `p`',
                '`c`.`promo_id` = `p`.`promo_id`'
            )
            ->where(['REGEXP BINARY', '`c`.`code`', $regexp])
            ->andWhere(['`p`.`tenant_id`' => user()->tenant_id])
            ->count();
    }
}