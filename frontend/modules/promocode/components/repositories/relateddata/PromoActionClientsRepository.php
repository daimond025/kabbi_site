<?php

namespace frontend\modules\promocode\components\repositories\relateddata;


use frontend\modules\promocode\models\PromoActionClients;

class PromoActionClientsRepository
{
    public static function getAllDataColumn()
    {
        $items = (PromoActionClients::find()
            ->select(['name', 'action_clients_id'])
            ->indexBy('action_clients_id')
            ->column());

        foreach ($items as $key => $item) {
            $items[$key] = t('promo', $item);
        }

        return $items;
    }
}