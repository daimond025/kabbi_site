<?php

namespace frontend\modules\promocode\components\repositories\relateddata;


use frontend\modules\promocode\models\PromoSymbolType;

class PromoSymbolTypeRepository
{
    public static function getAllDataColumn()
    {
        $items = PromoSymbolType::find()
            ->select(['name', 'symbol_type_id'])
            ->indexBy('symbol_type_id')
            ->column();


        foreach ($items as $key => $item) {
            $items[$key] = t('promo', $item);
        }

        return $items;
    }
}