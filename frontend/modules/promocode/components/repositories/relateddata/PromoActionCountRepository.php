<?php
namespace frontend\modules\promocode\components\repositories\relateddata;


use frontend\modules\promocode\models\PromoActionCount;

class PromoActionCountRepository
{
    public static function getAllDataColumn()
    {
        $items = PromoActionCount::find()
            ->select(['name', 'action_count_id'])
            ->indexBy('action_count_id')
            ->orderBy(['sort' => SORT_ASC])
            ->column();

        return $items;
    }
}