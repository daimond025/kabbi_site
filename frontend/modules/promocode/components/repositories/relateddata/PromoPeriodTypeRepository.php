<?php
namespace frontend\modules\promocode\components\repositories\relateddata;


use frontend\modules\promocode\models\PromoPeriodType;

class PromoPeriodTypeRepository
{
    public static function getAllDataColumn()
    {
        $items = PromoPeriodType::find()
            ->select(['name', 'period_type_id'])
            ->indexBy('period_type_id')
            ->column();

        foreach ($items as $key => $item) {
            $items[$key] = t('promo', $item);
        }

        return $items;
    }
}