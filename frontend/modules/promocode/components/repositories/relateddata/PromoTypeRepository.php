<?php

namespace frontend\modules\promocode\components\repositories\relateddata;



use frontend\modules\promocode\models\PromoType;

class PromoTypeRepository
{

    public static function getAllDataColumn()
    {
        $items = PromoType::find()
            ->select(['name', 'type_id'])
            ->indexBy('type_id')
            ->column();

        foreach ($items as $key => $item) {
            $items[$key] = t('promo', $item);
        }

        return $items;
    }

}