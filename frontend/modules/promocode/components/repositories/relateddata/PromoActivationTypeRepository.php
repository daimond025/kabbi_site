<?php

namespace frontend\modules\promocode\components\repositories\relateddata;


use frontend\modules\promocode\models\PromoActivationType;

class PromoActivationTypeRepository
{
    public static function getAllDataColumn()
    {
        $items = PromoActivationType::find()
            ->select(['name', 'activation_type_id'])
            ->indexBy('activation_type_id')
            ->column();

        foreach ($items as $key => $item) {
            $items[$key] = t('promo', $item);
        }

        return $items;
    }
}