<?php

namespace frontend\modules\promocode\components\repositories;


use frontend\modules\promocode\models\Promo;

class PromoRepository
{
    public static function getPromoForCurrentTenant(array $condition)
    {
        return Promo::find()
            ->where(['tenant_id' => user()->tenant_id])
            ->andwhere($condition)
            ->one();
    }


    public function getTypeIdPromo($promoId)
    {
        return Promo::find()
            ->select('type_id')
            ->where([
                'promo_id'  => $promoId,
                'tenant_id' => user()->tenant_id,
            ])
            ->scalar();
    }

    public function getIsBlockedPromo($promoId)
    {
        return Promo::find()
            ->select('blocked')
            ->where([
                'promo_id'  => $promoId,
                'tenant_id' => user()->tenant_id,
            ])
            ->scalar() == 1;
    }
}