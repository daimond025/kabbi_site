<?php

namespace frontend\modules\promocode\components\widgets;


use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class DateTime extends Widget
{
    public $date;
    public $time;
    public $flag;

    public $attrDate;
    public $attrTime;

    public $model;


    public function init()
    {
        parent::init();

        if ( ! $this->date) {
            $this->date = date('d.m.Y', time());
        } else {
            $this->flag = true;
        }

        if ( ! $this->time) {
            $this->time = date('H:i', time());
        }

        if( ! $this->model) {
            throw new InvalidConfigException('Model must be configured');
        }

        if( ! $this->attrDate) {
            throw new InvalidConfigException('AttributeDate must be configured');
        }

        if( ! $this->attrTime) {
            throw new InvalidConfigException('AttributeTime must be configured');
        }

    }

    public function run()
    {
        return Html::tag('div', $this->createDataInput() . $this->createTimeInput(), [
            'class' => 'date-time'
        ]);
    }

    protected function createTimeInput()
    {
        return Html::activeTextInput($this->model, $this->attrTime, ['type' => 'time', 'value' => $this->time, 'class' => 'time']);
    }

    protected function createDataInput()
    {

        $label_innerHtml = Html::a($this->date, null,['class' => 's_datepicker a_sc select']);



        $innerForm = Html::activeTextInput($this->model, $this->attrDate, [
            'class' => 'sdp_input',
            'value' => $this->date
        ]);


        $panel_innerHtml = Html::tag('div', '', ['class' => 'sdp_filter ' . $this->attrDate]);

        $date_innerHtml = Html::tag('div', $innerForm . $panel_innerHtml, ['class' => 's_datepicker_content b_sc', 'style' => 'display: none;']);

        $innerHtml = Html::tag('div', $label_innerHtml . $date_innerHtml, ['class' => 'cof_date']);

        return $innerHtml;
    }

}