<?php

/* @var $pjaxId string */
/* @var $cityList array */
/* @var $searchModel \frontend\modules\promocode\models\search\PromoSearch */

/* @var $dataProvider \yii\data\ActiveDataProvider */

use frontend\widgets\filter\Filter;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\helpers\Html;
use frontend\modules\promocode\models\Promo;
use yii\helpers\ArrayHelper;
use frontend\widgets\LinkPager;


$filter = Filter::begin([
    'pjaxId' => $pjaxId,
    'model'  => $searchModel,
]);

echo $filter->checkboxList('cities', $cityList, [
    'label' => t('promo', 'All cities'),
]);
Filter::end();

Pjax::begin(['id' => $pjaxId]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'       => "{items}{pager}",
    'tableOptions' => [
        'class' => 'people_table',
    ],
    'pager'        => [
        'class'         => LinkPager::className(),
        'prevPageLabel' => t('app', 'Prev'),
        'nextPageLabel' => t('app', 'Next'),
    ],
    'columns'      => [
        [
            'attribute'     => 'name',
            'label'         => t('promo', 'Name'),
            'headerOptions' => ['class' => 'pt_wor'],
            'options'       => ['class' => 'pt_fio pt_wor'],
            'content'       => function (Promo $model) {
                return Html::a(Html::encode($model->name), ['update', 'id' => $model->promo_id],
                    ['data-pjax' => 0]);
            },
        ],
        [
            'attribute'     => 'type',
            'label'         => t('promo', 'Type'),
            'headerOptions' => ['class' => 'pt_re'],
            'options'       => ['class' => 'pt_re'],
            'value'         => function (Promo $model) {
                return t('promo', $model->type->name);
            },
        ],
        [
            'attribute'     => 'cities',
            'label'         => t('promo', 'City'),
            'headerOptions' => ['class' => 'pt_re'],
            'options'       => ['class' => 'pt_re'],
            'value'         => function (Promo $model) {
                return implode(', ', ArrayHelper::map($model->promoCities, 'city_id', 'name'));
            },
        ],
    ],
]);

Pjax::end();