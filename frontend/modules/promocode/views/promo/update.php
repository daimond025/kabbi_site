<?php
/* @var $model \frontend\modules\promocode\models\Promo */

/* @var $dataForm array */

use \frontend\modules\promocode\assets\PromoAssets;
use yii\helpers\Html;
use yii\helpers\Url;

PromoAssets::register($this);
$this->title = Html::encode($model->name);
?>
<div class="bread">
    <a href="<?= Url::toRoute('/promocode/promo/list') ?>">
        <?= Yii::t('promo', 'Promo codes') ?>
    </a>
</div>
<h1 data-title="<?= $this->title ?>"><?= $this->title ?></h1>


<div class="tabs_links">
    <ul>
        <li class="">
            <a href="#promo-code" class="t01"><?= t('promo', 'Promo-code') ?></a>
        </li>
        <li class="">
            <a href="#codes" data-href="<?= Url::to(['/promocode/promo-reports/codes', 'id' => $model->promo_id]) ?>" class="t02">
                <?= t('promo', 'Codes') ?>
            </a>
        </li>
        <li class="">
            <a href="#used-promo" data-href="<?= Url::to(['/promocode/promo-reports/used-codes', 'id' => $model->promo_id]) ?>" class="t03">
                <?= t('promo', 'Used codes') ?>
            </a>
        </li>
    </ul>
</div>




<div class="tabs_content">
    <div id="t01" data-view="index">
        <?= $this->render('_form', [
            'model'    => $model,
            'dataForm' => $dataForm,
        ]); ?>
    </div>
    <div id="t02"></div>
    <div id="t03"></div>
</div>
