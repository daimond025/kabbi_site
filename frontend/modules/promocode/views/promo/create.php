<?php
/* @var $model \frontend\modules\promocode\models\Promo */
/* @var $dataForm array */
use \frontend\modules\promocode\assets\PromoAssets;
use yii\helpers\Url;

PromoAssets::register($this);
$this->title = Yii::t('promo', 'Add promotion code');
?>
    <div class="bread">
        <a href="<?= Url::toRoute('/promocode/promo/list') ?>">
            <?= Yii::t('promo', 'Promo codes') ?>
        </a>
    </div>
    <h1 data-title="<?= $this->title ?>"><?= $this->title ?></h1>



<?= $this->render('_form', [
    'model'    => $model,
    'dataForm' => $dataForm,
]); ?>