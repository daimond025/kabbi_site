<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var string $pjaxId */
/* @var $this yii\web\View */
/* @var array $cityList */
/* @var $searchModel \frontend\modules\promocode\models\search\PromoSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */

$this->title = Yii::t('promo', 'Promo codes');

if (app()->user->can('promoSystem')) {
    echo Html::a(t('app', 'Add'), ['create'],
        ['class' => 'button', 'style' => 'float: right; position: relative; top: 5px;']);
}

?>
<h1><?= Html::encode($this->title) ?></h1>
<section class="main_tabs">
    <div class="tabs_links">
        <ul>
            <li><a href="#active" class="t01"><?= t('app', 'Active') ?></a></li>
            <li><a href="#blocked" class="t02" data-href="<?= Url::to('list-blocked') ?>"><?= t('app', 'Blocked') ?></a>
            </li>
        </ul>
    </div>
    <div class="tabs_content">
        <div id="t01" data-view="">
            <?= $this->render('_grid', compact('searchModel', 'cityList', 'pjaxId', 'dataProvider')) ?>
        </div>
        <div id="t02" data-view="blocked"></div>
    </div>
</section>
