<?php
/* @var $model \frontend\modules\promocode\models\Promo */

/* @var $dataForm array */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\modules\promocode\components\widgets\DateTime;

?>

<?php $form = ActiveForm::begin([
    'id' => 'promo-form',
]); ?>

    <!--Тип реферальной системы-->
    <section class="row">
        <?= $form->field($model, 'type_id')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'type_id') ?></div>
        <div class="row_input">
            <?= Html::activeRadioList($model, 'type_id', $dataForm['relatedData']['type'], [
                'item' => function ($index, $label, $name, $checked, $value) use ($model) {

                   // if ($value == 3) return false; // @TODO Временно убираем коды для водителя

                    return Html::radio($name, $checked, [
                        'label'    => $label,
                        'value'    => $value,
                        'disabled' => $model->isNewRecord ? false : 'disabled',
                    ]);
                },
            ]) ?>
        </div>
        <?= $form->field($model, 'type_id')->end(); ?>
    </section>


    <!--Название-->
    <section class="row">
        <?= $form->field($model, 'name')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'name') ?></div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'name') ?>
            <?= Html::error($model, 'name',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'name')->end(); ?>
    </section>


    <!--Промокод-->
    <section class="row promo-type__manually">

        <?= $form->field($model, 'codes')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'codes') ?></div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'codes', [
               // 'disabled' => $model->isNewRecord ? false : 'disabled',
            ]) ?>
            <?= Html::error($model, 'codes',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'codes')->end(); ?>
    </section>

    <!--Город-->
    <section class="row" id="city_select">
        <?= $form->field($model, 'city', [
            'enableClientValidation' => false,
            'options'                => [
                'data' => [
                    'required-message' => t('promo', '«{attribute}» cannot be blank.',
                        ['attribute' => t('promo', $model->getAttributeLabel('cities'))]),
                ],
            ],
        ])->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'city') ?></div>
        <div class="row_input">

            <?= Html::activeDropDownList($model, 'city', $dataForm['cityList'], [
                'class'            => 'default_select',
                'data-placeholder' => !empty($model->city)
                    ? $model->promoCities[0]->name
                    : t('promo', 'Choose city'),
               // 'disabled'         => $model->isNewRecord ? false : 'disabled',
            ]) ?>

            <?= Html::error($model, 'city',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'city')->end(); ?>
    </section>

    <section class="row">
        <div class="row_label">
            <?= Html::activeLabel($model, 'position_id') ?></div>
        <div class="row_input">
            <div class="grid_2">

                <!--Профессия-->
                <div class="grid_item">
                    <div id="input_positions">
                        <?= $form->field($model, 'position_id', [
                            'enableClientValidation' => false,
                            'options'                => [
                                'data' => [
                                    'required-message'  => t('promo', '«{attribute}» cannot be blank.',
                                        ['attribute' => t('promo', $model->getAttributeLabel('position_id'))]),
                                    'types-car-classes' => json_encode($dataForm['typesCarClasses']),
                                    'selected-position' => $model->position_id,
                                ],
                            ],
                        ])->begin(); ?>
                        <?= Html::activeDropDownList($model, 'position_id', [], [
                            'class'            => 'default_select',
                            'data-placeholder' => Html::encode(!$model->isNewRecord
                                ? $dataForm['namePosition']
                                : t('promo', 'Choose the position')),
                           // 'disabled'         => $model->isNewRecord ? false : 'disabled',
                        ]) ?>
                        <?= Html::error($model, 'position_id',
                            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
                        <?= $form->field($model, 'position_id')->end(); ?>
                    </div>
                </div>

                <!--Классы-->
                <div class="grid_item">
                    <div>
                        <?= $form->field($model, 'carClasses', [
                            'enableClientValidation' => false,
                            'options'                => [
                                'data' => [
                                    'required-message' => t('promo', '«{attribute}» cannot be blank.',
                                        ['attribute' => $model->getAttributeLabel('carClasses')]),
                                    'placeholder'      => t('promo', 'Choose a class'),
                                    'selected-car-classes' => json_encode($model->carClasses),
                                ],
                            ],
                        ])->begin(); ?>
                        <div class="select_checkbox">
                            <a class="a_sc" rel="<?= t('promo', 'Choose a class') ?>">
                                <?= t('promo', 'Choose a class') ?>
                            </a>
                            <div class="b_sc">
                                <?
                                 $dateFormCarClass = array_unique($dataForm['carClasses']);
                                ?>
                                <?= Html::activeCheckboxList($model, 'carClasses',  $dateFormCarClass, [
                                    'tag'  => 'ul',
                                    'item' => function ($index, $label, $name, $checked, $value) use ($model) {
                                        $checked = in_array($label[2], $model->carClasses);

                                        return Html::tag('li', Html::checkbox($name, $checked, [
                                            'value'    => $label[2],
                                            'label'    => $label[0],
                                           // 'disabled' => $model->isNewRecord ? false : false,
                                        ]));
                                    },
                                ]) ?>
                            </div>
                        </div>
                        <?= Html::error($model, 'carClasses',
                            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
                        <?= $form->field($model, 'carClasses')->end(); ?>
                    </div>
                </div>
            </div>

        </div>
    </section>


    <!--Период действия-->
    <section class="row">
        <?= $form->field($model, 'period_type_id')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'period_type_id') ?></div>
        <div class="row_input">
            <?= Html::activeRadioList($model, 'period_type_id', $dataForm['relatedData']['periodType'], [
                'item' => function ($index, $label, $name, $checked, $value) {
                    return Html::radio($name, $checked, [
                        'label' => $label,
                        'value' => $value,
                        'class' => 'radio-period-sc',
                    ]);
                },
            ]) ?>
            <div class="active_input label_js_input" data-required-message="<?= t('yii', '{attribute} cannot be blank.',
                ['attribute' => 'period of time']) ?>">
                <?= DateTime::widget([
                    'model'    => $model,
                    'attrDate' => 'dateStart',
                    'attrTime' => 'timeStart',
                    'date'     => $model->dateStart,
                    'time'     => $model->timeStart,
                ]); ?>
                <hr class="delimiter-date-time">
                <?= DateTime::widget([
                    'model'    => $model,
                    'attrDate' => 'dateEnd',
                    'attrTime' => 'timeEnd',
                    'date'     => $model->dateEnd,
                    'time'     => $model->timeEnd,
                ]); ?>
            </div>
            <?= Html::error($model, 'period_end',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
            <?= Html::tag('span', '', ['class' => 'error-block-period-type', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'period_type_id')->end(); ?>
    </section>


    <!--Активация-->
    <section class="row">
        <?= $form->field($model, 'activation_type_id')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'activation_type_id') ?></div>
        <div class="row_input">
            <?= Html::activeRadioList($model, 'activation_type_id', $dataForm['relatedData']['activationType'], [
                'item' => function ($index, $label, $name, $checked, $value) use ($model) {

                    // if ($value == 2) return false; // @TODO Временно убираем скидки

                    return Html::radio($name, $checked, [
                        'label'    => $label,
                        'value'    => $value,
                        //'disabled' => $model->isNewRecord ? false : 'disabled',
                    ]);
                },
            ]) ?>

        </div>
        <?= $form->field($model, 'activation_type_id')->end(); ?>
    </section>

    <!-- Количество бонусов -->
    <section class="row">
        <?= $form->field($model, 'client_bonuses')->begin(); ?>
        <div class="row_label"></div>
        <div class="row_input">
            <div class="grid_2">
                <div class="grid_item">
                    <?= Html::activeTextInput($model, 'client_bonuses', [
                        'class'    => 'mini_input_2',
                        'style'    => 'float: left; margin-right: 6px;',
                        //'disabled' => $model->isNewRecord ? false : 'disabled',
                    ]); ?>
                    <label class="js-on-register"
                           style="float: left; line-height: 30px;"><?= $model->getAttributeLabel('client_bonuses') ?></label>
                </div>
            </div>
            <br><br><?= Html::error($model, 'client_bonuses',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'client_bonuses')->end(); ?>
    </section>

    <!-- Скидка -->
    <section class="row" style="display: none;">
        <?= $form->field($model, 'client_discount')->begin(); ?>
        <div class="row_label"></div>
        <div class="row_input">
            <div class="grid_2">
                <div class="grid_item">
                    <?= Html::activeTextInput($model, 'client_discount', [
                        'class'    => 'mini_input_2',
                        'style'    => 'float: left; margin-right: 6px;',
                      //  'disabled' => $model->isNewRecord ? false : 'disabled',
                    ]); ?>
                    <label class="js-on-register"
                           style="float: left; line-height: 30px;"><?= $model->getAttributeLabel('client_discount') ?></label>
                </div>
            </div>
            <br><br><?= Html::error($model, 'client_discount',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'client_discount')->end(); ?>
    </section>

    <!--Действует-->
    <section class="row">
        <?= $form->field($model, 'action_count_id')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'action_count_id') ?></div>
        <div class="row_input">

            <div class="grid_2">
                <div class="grid_item" id="action_count-bonus">
                    <?= Html::activeDropDownList($model, 'action_count_bonus', $dataForm['relatedData']['actionCountBonus'], [
                        'data-placeholder' => $dataForm['relatedData']['actionCount'][0],
                        'class'            => 'default_select',
                        //'disabled'         => $model->isNewRecord ? false : 'disabled',
                    ]) ?>
                </div>
                <div class="grid_item" id="action_count-discount" style="margin-left: 0; display: none;">
                    <?= Html::activeDropDownList($model, 'action_count_discount', $dataForm['relatedData']['actionCountDiscount'], [
                        'data-placeholder' => $dataForm['relatedData']['actionCount'][0],
                        'class'            => 'default_select',
                       // 'disabled'         => $model->isNewRecord ? false : 'disabled',
                    ]) ?>
                </div>
                <div class="grid_item">
                    <?= Html::activeDropDownList($model, 'action_clients_id', $dataForm['relatedData']['actionClients'],
                        [
                            'data-placeholder' => $dataForm['relatedData']['actionClients'][0],
                            'class'            => 'default_select',
                           // 'disabled'         => $model->isNewRecord ? false : 'disabled',
                        ]) ?>
                </div>
            </div>

        </div>
        <?= $form->field($model, 'action_count_id')->end(); ?>
    </section>

    <section class="row">
        <?= $form->field($model, 'action_no_other_codes')->begin(); ?>
        <div class="row_label"></div>
        <div class="row_input">
            <?= Html::activeCheckbox($model, 'action_no_other_codes', [
                //'disabled' => $model->isNewRecord ? false : 'disabled',
            ]) ?>
        </div>
        <?= $form->field($model, 'action_no_other_codes')->end(); ?>
    </section>


    <!-- Количество кодов -->
    <section class="row promo-type__generation" style="display: none">
        <?= $form->field($model, 'count_codes')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'count_codes') ?></div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'count_codes', [
                'class'    => 'mini_input_2',
                'style'    => 'float: left; margin-right: 6px;',
                //'disabled' => $model->isNewRecord ? false : 'disabled',
            ]); ?>
            <label class="js-on-register"
                   style="float: left; line-height: 30px;"><?= Html::encode(t('promo', 'pcs.')) ?></label>
            <br><br><?= Html::error($model, 'count_codes',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'count_codes')->end(); ?>
    </section>

    <!-- Количество символов в коде -->
    <section class="row promo-type__generation promo-type__drivers" style="display: none">
        <?= $form->field($model, 'count_symbols_in_code')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'count_symbols_in_code') ?></div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'count_symbols_in_code', [
                'class'    => 'mini_input_2',
                'style'    => 'float: left; margin-right: 6px;',
                //'disabled' => $model->isNewRecord ? false : 'disabled',
            ]); ?>
            <label class="js-on-register"
                   style="float: left; line-height: 30px;"><?= Html::encode(t('promo', 'pcs.')) ?></label>
            <br><br><?= Html::error($model, 'count_symbols_in_code',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'count_symbols_in_code')->end(); ?>
    </section>

    <!-- Типы символов -->
    <section class="row promo-type__generation promo-type__drivers" style="display: none">
        <?= $form->field($model, 'symbols', [
            'enableClientValidation' => false,
            'options'                => [
                'data' => [
                    'required-message' => t('promo', '«{attribute}» cannot be blank.',
                        ['attribute' => $model->getAttributeLabel('symbols')]),
                ],
            ],
        ])->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'symbols') ?></div>
        <div class="row_input">
            <?= Html::activeCheckboxList($model, 'symbols', $dataForm['relatedData']['symbolType'], [
                'tag'   => 'ul',
                'style' => 'list-style-type: none;',
                'item'  => function ($index, $label, $name, $checked, $value) use ($model) {
                    return Html::tag('li', Html::checkbox($name, $checked, [
                        'value'    => $value,
                        'label'    => Html::encode($label),
                       // 'disabled' => $model->isNewRecord ? false : 'disabled',
                    ]));
                },
            ]) ?>
            <?= Html::tag('span', '', ['class' => 'error-block-symbols', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'symbols')->end(); ?>
    </section>

    <!-- Коммисия у воркера -->
    <section class="row promo-type__drivers" style="display: none">
        <?= $form->field($model, 'worker_commission')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'worker_commission') ?></div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'worker_commission', [
                'class'    => 'mini_input_2',
                'style'    => 'float: left; margin-right: 6px;',
               // 'disabled' => $model->isNewRecord ? false : 'disabled',
            ]); ?>
            <label class="js-on-register"
                   style="float: left; line-height: 30px;"><?= Html::encode(t('promo', '% of order value')) ?></label>
            <br><br><?= Html::error($model, 'worker_commission',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'worker_commission')->end(); ?>
    </section>

    <!-- Бонусы воркеру -->
    <section class="row promo-type__drivers" style="display: none">
        <?= $form->field($model, 'worker_bonuses')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'worker_bonuses') ?></div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'worker_bonuses', [
                'class'    => 'mini_input_2',
                'style'    => 'float: left; margin-right: 6px;',
              //  'disabled' => $model->isNewRecord ? false : 'disabled',
            ]); ?>
            <label class="js-on-register"
                   style="float: left; line-height: 30px;"><?= Html::encode(t('promo', '% of order value')) ?></label>
            <br><br><?= Html::error($model, 'worker_bonuses',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'worker_bonuses')->end(); ?>

    </section>

    <section class="submit_form">
        <div>
            <label><?= Html::activeCheckbox($model, 'blocked', ['label' => null]) ?>
                <b><?= t('promo', 'Block') ?></b></label>
        </div>
        <?= Html::submitInput(t('promo', 'Save')) ?>
    </section>


<?php $form = ActiveForm::end(); ?>