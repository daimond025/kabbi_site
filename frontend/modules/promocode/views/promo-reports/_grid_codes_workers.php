<?php
/* @var $searchModel \frontend\modules\promocode\models\search\PromoCodeSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $pjaxId string */
/* @var $this yii\web\View */


use yii\grid\GridView;
use frontend\widgets\LinkPager;
use yii\widgets\Pjax;
use frontend\widgets\filter\Filter;
use frontend\modules\promocode\models\PromoCode;
use yii\helpers\Html;

$filter = Filter::begin([
    'model' => $searchModel,
    'pjaxId' => $pjaxId,
    'onChange' => true,
    'options' => [
        'style' => 'float: right; position: relative; top: 5px;'
    ]
]);
echo $filter->export([
    [
        'label' => t('client', 'Download to Excel'),
        'url' => ['/promocode/promo-reports/dump-worker-codes', 'id' => $searchModel->promo_id],
    ],
]);
Filter::end();



Pjax::begin(['id' => $pjaxId]);

echo GridView::widget([
    'layout'       => "{items}\n{pager}",
    'dataProvider' => $dataProvider,
    'tableOptions' => [
        'class' => 'promo-codes_table people_table',
    ],
    'pager'        => [
        'class'         => LinkPager::className(),
        'prevPageLabel' => t('app', 'Prev'),
        'nextPageLabel' => t('app', 'Next'),
    ],
    'columns' => [
        [
            'attribute' => 'worker_id',
            'label'     => t('promo', 'Worker'),
            'content'   => function (PromoCode $model) {
                $worker = $model->worker;
                $text = Html::a(
                    Html::encode($worker->getFullName()),
                    ['/employee/worker/update', 'id' => $worker->worker_id]
                );
                return $text;
            }
        ],
        [
            'attribute' => 'code',
            'label'     => t('promo', 'Code'),
        ],
    ],
]);

Pjax::end();
