<?php
/* @var $searchModel \frontend\modules\promocode\models\search\PromoOrderSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */

/* @var $pjaxId string */

use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
use common\modules\order\models\Order;
use frontend\widgets\LinkPager;
use yii\widgets\Pjax;
use frontend\modules\promocode\models\PromoClient;

\frontend\modules\promocode\assets\OpenOrderAssets::register($this);

$widget = Pjax::begin([
    'id' => $pjaxId,
    'enablePushState' => false
]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'       => "{items}\n{pager}",
    'tableOptions' => [
        'class' => 'people_table',
    ],
    'pager'        => [
        'class'         => LinkPager::className(),
        'prevPageLabel' => t('app', 'Prev'),
        'nextPageLabel' => t('app', 'Next'),
    ],
    'columns'      => [

        [
            'attribute' => 'status_time',
            'label'     => t('promo', 'date'),
            'format'    => ['date', 'dd.MM.Y'],
        ],
        [
            'label'     => t('promo', 'status'),
            'content' => function (Order $model) {
                /* @var $promoClient PromoClient */
                $promoClient = $model
                    ->promoCode
                    ->getPromoClient()
                    ->where(['client_id' => $model->client_id])
                    ->one();

                return t('promo', $promoClient->statusCode->status_label);
            }
        ],
        [
            'attribute' => 'order_id',
            'label'     => t('promo', 'Order'),
            'content'   => function (Order $model) {
                $text = Html::a(
                    '№ ' . $model->order_number,
                    '/order/view/' . $model->order_number,
                    [
                        'class' => 'js-order-view'
                    ]
                );
                return $text;
            },
        ],
        [
            'label'   => t('promo', 'Client'),
            'content' => function (Order $model) {
                $client = $model->client->getFullName();
                $client .= "<br>";

                foreach ($model->client->clientPhones as $phone) {
                    $client .= $phone->value . '<br>';
                }

                $client_id = $model->client->client_id;

                return Html::tag('a', $client, ['href' => Url::to("/client/base/update/$client_id")]);
            },
        ],
        [
            'attribute' => 'promoCode.code',
            'label'     => 'Код',
        ],
        [
            'attribute' => 'promoCode.promo.type.name',
            'content'   => function (Order $model) {
                return t('promo', $model->promoCode->promo->type->name);
            },
        ],

    ],
]);

Pjax::end();


