<?php
/* @var $searchModel \frontend\modules\promocode\models\search\PromoOrderSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $pjaxId string */

use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
use common\modules\order\models\Order;
use frontend\widgets\filter\Filter;
use frontend\widgets\LinkPager;
use yii\widgets\Pjax;
use frontend\modules\promocode\models\PromoClient;
use common\modules\employee\models\worker\Worker;
use common\modules\car\models\Car;

\frontend\modules\promocode\assets\OpenOrderAssets::register($this);

$filter = Filter::begin([
    'model' => $searchModel,
    'pjaxId' => $pjaxId,
    'onChange' => true,
    'options' => [
        'style' => 'float: right; position: relative; top: 5px;'
    ]
]);
echo $filter->export([
    [
        'label' => t('client', 'Download to Excel'),
        'url' => ['/promocode/promo-reports/dump-used-codes', 'id' => $searchModel->promo_id],
    ],
]);
Filter::end();

$widget = Pjax::begin([
    'id' => $pjaxId,
    'enablePushState' => false
]);



echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'       => "{items}\n{pager}",
    'tableOptions' => [
        'class' => 'people_table',
    ],
    'pager'        => [
        'class'         => LinkPager::className(),
        'prevPageLabel' => t('app', 'Prev'),
        'nextPageLabel' => t('app', 'Next'),
    ],
    'columns'      => [

        [
            'attribute' => 'status_time',
            'label'     => t('promo', 'date'),
            'format'    => ['date', 'dd.MM.Y'],
        ],
        [
            'label'     => t('promo', 'status'),
            'content' => function (Order $model) {
                /* @var $promoClient PromoClient */
                $promoClient = $model
                    ->promoCode
                    ->getPromoClient()
                    ->where(['client_id' => $model->client_id])
                    ->one();

                return t('promo', $promoClient->statusCode->status_label);
            }
        ],
        [
            'attribute' => 'order_id',
            'label'     => t('promo', 'Order'),
            'content'   => function (Order $model) {
                $text = '';
                if ($model->order_number) {
                    $text = Html::a(
                        '№ ' . $model->order_number,
                        '/order/view/' . $model->order_number,
                        [
                            'class' => 'js-order-view'
                        ]
                    );
                }
                return $text;
            },
        ],
        [
            'label'   => t('promo', 'Client'),
            'content' => function (Order $model) {
                $client = $model->client->getFullName();
                $client .= "<br>";

                foreach ($model->client->clientPhones as $phone) {
                    $client .= $phone->value . '<br>';
                }

                $client_id = $model->client->client_id;

                return Html::tag('a', $client, ['href' => Url::to("/client/base/update/$client_id")]);
            },
        ],
        [
            'label'   => t('promo', 'Worker'),
            'content' => function (Order $model) {
                $html = '';

                if ($model->worker instanceof Worker) {
                    $workerName = $model->worker->getFullName();
                    $html .= Html::tag(
                        'a', $workerName,
                        ['href' => Url::to("/employee/worker/update/{$model->worker_id}")]
                    );
                }


                if ($model->car instanceof Car) {
                    $carName = $model->car->name;

                    $html .= "<br>";
                    $html .= Html::tag(
                        'a', $carName,
                        ['href' => Url::to("/lib/car/update/{$model->car_id}")
                        ]);
                    $html .= "<br>";
                    $html .= $model->car->gos_number;
                    $html .= "<br><br>";
                }

                return $html;
            },
        ],
        [
            'attribute' => 'promoCode.code',
            'label'     => 'Код',
        ],

    ],
]);

Pjax::end();


