<?php

namespace frontend\modules\promocode\controllers;

use frontend\modules\promocode\components\datacentr\DataForm;
use frontend\modules\promocode\components\repositories\relateddata\PromoSymbolTypeRepository;
use frontend\modules\promocode\components\services\CodesService;
use frontend\modules\promocode\components\services\ListDataService;
use frontend\modules\promocode\components\services\CarClassService;
use frontend\modules\promocode\components\services\PositionService;
use yii\db\Transaction;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use frontend\modules\promocode\models\Promo;


/**
 * Class ReferralController
 * @package frontend\modules\promocode\controllers
 */
class PromoController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow'   => true,
                        'roles'   => ['promoSystem'],
                    ],
                    [
                        'actions' => ['list', 'update', 'get-classes'],
                        'allow'   => true,
                        'roles'   => ['read_promoSystem'],
                    ],
                ],
            ],
        ];
    }

    public function actionCreate()
    {

        $model = new Promo([
            'type_id'               => Promo::FIELD_TYPE_PROMO_MANUALLY,
            'period_type_id'        => Promo::FIELD_PERIOD_LIMITED,
            'action_no_other_codes' => Promo::FIELD_ACTION_NO_OTHER_CODES,
            'activation_type_id'    => Promo::FIELD_TYPE_ACTIVATION_BONUSES,
            'symbols'               => array_keys(PromoSymbolTypeRepository::getAllDataColumn()),
        ]);



        $dataForm = (new DataForm())->getDataForForm($model);


        if ($model->load(\Yii::$app->request->post())) {

            $transaction = app()->db->beginTransaction();
            try {

                if ($model->save()) {


                    $codesServices = new CodesService();
                    $codesServices->createCodesForModel($model);
                    $transaction->commit();

                    session()->setFlash('success', t('promo', 'Promo-code was successfully added.'));

                    return $this->redirect(Url::to('/promocode/promo/list'));
                } elseif ($model->hasErrors()) {
                    displayErrors([$model]);
                }
            } catch (\Exception $e) {
                $model->delete();
                \Yii::error($e->getMessage());
                session()->setFlash('error', t('error', 'Error saving. Notify to administrator, please.'));
            }
            $transaction->rollBack();
        }

        return $this->render('create', [
            'model'    => $model,
            'dataForm' => $dataForm,
        ]);

    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = Promo::SCENARIO_UPDATE;



        $dataForm = (new DataForm())->getDataForForm($model);
        if ($model->load(\Yii::$app->request->post())) {


            $transaction = app()->db->beginTransaction();
            try {
                if ($model->save()) {

                    $codesServices = new CodesService();

                    $codesServices->updateCodesForModel($model);
                    $transaction->commit();


                    session()->setFlash('success', t('promo', 'Promo-code was successfully updated.'));

                    return $this->redirect(Url::to('/promocode/promo/update/' . $id));
                } elseif ($model->hasErrors()) {
                    displayErrors([$model]);
                }
            } catch (\Exception $e) {

                \Yii::error($e->getMessage());
                session()->setFlash('error', t('error', 'Error saving. Notify to administrator, please.'));
            }
            $transaction->rollBack();
        }

        return $this->render('update', [
            'model'    => $model,
            'dataForm' => $dataForm,
        ]);
    }

    public function actionList()
    {
        return $this->render('index', (new ListDataService())->getListData('promo_codes_active'));
    }

    public function actionListBlocked()
    {
        return $this->renderAjax('_grid', (new ListDataService())->getListData('promo_codes_blocked', 1));
    }


    public function actionGetPositions($id)
    {
        $this->testAjax();
        return (new PositionService)->getPositions($id);
    }

    public function actionGetPositionsWorkers($id)
    {
        $this->testAjax();
        return (new PositionService)->getPositions($id, true);
    }

    public function actionGetCarClasses($id)
    {
        $this->testAjax();
        return (new CarClassService())->getCarClassesForTypeCar($id);
    }

    public function actionGetCarClassesWorkers($id)
    {
        $this->testAjax();
        return (new CarClassService())->getCarClassesForTypeCar($id, post('city_id'));
    }

    protected function findModel($id)
    {
        return Promo::find()
            ->where([
                'promo_id'  => $id,
                'tenant_id' => user()->tenant_id,
            ])
            ->one();
    }

    protected function testAjax()
    {
        if (!\Yii::$app->request->isAjax) {
            throw new NotFoundHttpException();
        }
        \Yii::$app->response->format = Response::FORMAT_JSON;
    }

}