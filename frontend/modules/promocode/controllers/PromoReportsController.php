<?php

namespace frontend\modules\promocode\controllers;


use frontend\modules\promocode\components\repositories\PromoRepository;
use frontend\modules\promocode\components\services\PromoCodesService;
use frontend\modules\promocode\models\Promo;
use frontend\widgets\ExcelExport;
use yii\filters\AccessControl;
use yii\web\Controller;

class PromoReportsController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow'   => true,
                        'roles'   => ['promoSystem'],
                    ],
                    [
                        'actions' => ['list', 'update', 'get-classes'],
                        'allow'   => true,
                        'roles'   => ['read_promoSystem'],
                    ],
                ],
            ],
        ];
    }

    public function actionUsedCodes($id)
    {
        if (\Yii::$app->request->isAjax) {
            return $this->renderAjax('_grid_report_promo', (new PromoCodesService())->getReportPromo($id));
        }

        return $this->redirect(['update', '#' => 'codes', 'id' => $id]);
    }

    public function actionPromoReportWorker($id)
    {
        if (\Yii::$app->request->isAjax) {
            return $this->renderAjax('_grid_report_worker', (new PromoCodesService())->getReportWorker($id));
        }

        return $this->redirect(['update', '#' => 'codes', 'id' => $id]);
    }

    public function actionPromoReportClient($id)
    {
        if (\Yii::$app->request->isAjax) {
            return $this->renderAjax('_grid_report_client', (new PromoCodesService())->getReportClient($id));
        }

        return $this->redirect(['update', '#' => 'codes', 'id' => $id]);
    }

    public function actionCodes($id)
    {
        if (\Yii::$app->request->isAjax) {
            /* @var $promoRepository PromoRepository */
            $promoRepository = \Yii::$container->get(PromoRepository::class);
            $typeId          = $promoRepository->getTypeIdPromo($id);
            $isBlocked       = $promoRepository->getIsBlockedPromo($id);

            if ($typeId == Promo::FIELD_PROMO_TYPE_DRIVERS) {
                return $this->renderAjax(
                    '_grid_codes_workers',
                    (new PromoCodesService())
                        ->getPromoCodesList($id, true, false, $isBlocked)
                );
            }

            if ($typeId == Promo::FIELD_PROMO_TYPE_GENERATION) {
                return $this->renderAjax(
                    '_grid_codes',
                    (new PromoCodesService())
                        ->getPromoCodesList($id, true, true, $isBlocked)
                );
            }

            return $this->renderAjax(
                '_grid_codes',
                (new PromoCodesService())
                    ->getPromoCodesList($id, true, false, $isBlocked)
            );
        }

        return $this->redirect(['update', '#' => 'codes', 'id' => $id]);
    }



    public function actionDumpCodes($id)
    {
        /* @var $promoRepository PromoRepository */
        $promoRepository = \Yii::$container->get(PromoRepository::class);
        $typeId          = $promoRepository->getTypeIdPromo($id);
        $isBlocked       = $promoRepository->getIsBlockedPromo($id);

        $isForGeneration = $typeId == Promo::FIELD_PROMO_TYPE_GENERATION;

        $data = (new PromoCodesService())->prepareDataCodesToExcel($id, $isForGeneration, $isBlocked);

        ExcelExport::widget([
            'data'     => $data['exportData'],
            'format'   => 'Excel5',
            'headers'  => $data['headers'],
            'fileName' => 'codes',
        ]);
    }

    public function actionDumpWorkerCodes($id)
    {
        /* @var $promoRepository PromoRepository */
        $promoRepository = \Yii::$container->get(PromoRepository::class);
        $isBlocked       = $promoRepository->getIsBlockedPromo($id);

        $data = (new PromoCodesService())->prepareDataWorkerCodesToExcel($id, $isBlocked);

        ExcelExport::widget([
            'data'     => $data['exportData'],
            'format'   => 'Excel5',
            'headers'  => $data['headers'],
            'fileName' => 'codes',
        ]);
    }

    public function actionDumpUsedCodes($id)
    {
        $data = (new PromoCodesService())->prepareUsedDataCodesToExcel($id);

        ExcelExport::widget([
            'data'     => $data['exportData'],
            'format'   => 'Excel5',
            'headers'  => $data['headers'],
            'fileName' => 'usedCodes',
        ]);
    }
}