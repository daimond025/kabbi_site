<?php
namespace frontend\modules\promocode;


use app\components\behavior\ControllerBehavior;
use yii\helpers\ArrayHelper;


/**
 * Class Module
 * @package app\modules\promocode
 *
 * @mixin ControllerBehavior
 */
class Module
    extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\promocode\controllers';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'common' => [
                'class' => ControllerBehavior::className(),
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $this->commonBeforeAction();

        return parent::beforeAction($action);
    }
}