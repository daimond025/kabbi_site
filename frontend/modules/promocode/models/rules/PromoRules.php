<?php

namespace frontend\modules\promocode\models\rules;


use frontend\modules\promocode\components\repositories\CarClassRepository;
use frontend\modules\promocode\components\repositories\PositionRepository;
use frontend\modules\promocode\components\repositories\PromoCodeRepository;
use frontend\modules\promocode\models\Promo;
use frontend\modules\promocode\models\PromoActionClients;
use frontend\modules\promocode\models\PromoActionCount;
use frontend\modules\promocode\models\PromoActivationType;
use frontend\modules\promocode\models\PromoType;

class PromoRules
{
    public function getRules(Promo $promo)
    {
        return [

            [
                'name',
                'required',
                'message' => t('promo', '«{attribute}» cannot be blank.',
                    ['attribute' => $promo->getAttributeLabel('name')])
            ],
            [
                'name',
                'unique',
                'skipOnError'     => true,
                'targetClass'     => Promo::className(),
                'targetAttribute' => ['name', 'tenant_id'],
                'filter'          => ['not', ['promo_id' => $promo->promo_id]],
                'message'         => t('promo', '«{attribute}» must be unique.',
                    ['attribute' => $promo->getAttributeLabel('name')])
            ],
            [
                'position_id',
                'required',
                'message' => t('promo', '«{attribute}» cannot be blank.',
                    ['attribute' => $promo->getAttributeLabel('position_id')]),
                'except' => Promo::SCENARIO_UPDATE
            ],
            [
                ['dateStart', 'dateEnd', 'timeStart', 'timeEnd'],
                'required',
                'when' => function (Promo $model) {
                    return $model->period_type_id == Promo::FIELD_PERIOD_LIMITED;
                },
            ],
            [
                ['client_bonuses'],
                'required',
                'when' => function (Promo $model) {
                    return $model->activation_type_id == Promo::FIELD_TYPE_ACTIVATION_BONUSES;
                },
                'whenClient' => sprintf("function (attribute, value) {
                                    return $(\"#promo-activation_type_id input[value='%s']\")[0].checked;
                                }", Promo::FIELD_TYPE_ACTIVATION_BONUSES),
                'message' => t('promo', '«{attribute}» cannot be blank.',
                    ['attribute' => $promo->getAttributeLabel('client_bonuses')]),
                'except' => Promo::SCENARIO_UPDATE
            ],
            [
                ['client_discount'],
                'required',
                'when' => function (Promo $model) {
                    return $model->activation_type_id == Promo::FIELD_TYPE_ACTIVATION_DISCOUNT;
                },
                'whenClient' => sprintf("function (attribute, value) {
                                    return $(\"#promo-activation_type_id input[value='%s']\")[0].checked;
                                }", Promo::FIELD_TYPE_ACTIVATION_DISCOUNT),
                'message' => t('promo', '«{attribute}» cannot be blank.',
                    ['attribute' => $promo->getAttributeLabel('client_discount')]),
                'except' => Promo::SCENARIO_UPDATE
            ],

            [
                'codes',
                'required',
                'when' => function (Promo $model) {
                    return $model->type_id == Promo::FIELD_PROMO_TYPE_MANUALLY;
                },
                'whenClient' => sprintf("function (attribute, value) {
                                    return $(\"#promo-type_id input[value='%s']\")[0].checked;
                                }", Promo::FIELD_PROMO_TYPE_MANUALLY),
                'message' => t('promo', '«{attribute}» cannot be blank.',
                    ['attribute' => $promo->getAttributeLabel('codes')]),
                'except' => Promo::SCENARIO_UPDATE
            ],

            [
                'codes',
                'match', 'pattern' => '#^[а-яёa-z0-9]+$#iu',
                'when' => function (Promo $model) {
                    return $model->type_id == Promo::FIELD_PROMO_TYPE_MANUALLY;
                },
                'whenClient' => sprintf("function (attribute, value) {
                                    return $(\"#promo-type_id input[value='%s']\")[0].checked;
                                }", Promo::FIELD_PROMO_TYPE_MANUALLY),
                'except' => Promo::SCENARIO_UPDATE
            ],

            [
                'codes',
                function ($attribute) use ($promo) {
                    $code = [$promo->$attribute];
                    if (PromoCodeRepository::getDuplicateCodesCurrentTenant($code)) {
                        $promo->addError($attribute, t('promo', '«{attribute}» must bee unique.',
                            ['attribute' => $promo->getAttributeLabel($attribute)]));
                    }
                },
                'when' => function (Promo $model) {
                    return $model->type_id == Promo::FIELD_PROMO_TYPE_MANUALLY;
                },
                'except' => Promo::SCENARIO_UPDATE
            ],


            [
                ['count_codes', 'count_symbols_in_code'],
                'required',
                'when' => function (Promo $model) {
                    return $model->type_id == Promo::FIELD_PROMO_TYPE_GENERATION;
                },
                'whenClient' => sprintf("function (attribute, value) {
                                    return $(\"#promo-type_id input[value='%s']\")[0].checked;
                                }", Promo::FIELD_PROMO_TYPE_GENERATION),
                'message' => t('promo', '«{attribute}» cannot be blank.'),
                'except' => Promo::SCENARIO_UPDATE
            ],

            [
                'symbols',
                'required',
                'when' => function (Promo $model) {
                    return $model->type_id != Promo::FIELD_TYPE_PROMO_MANUALLY;
                },
                'except' => Promo::SCENARIO_UPDATE
            ],

            [
                ['worker_commission', 'worker_bonuses', 'count_symbols_in_code'],
                'required',
                'when' => function (Promo $model) {
                    return $model->type_id == Promo::FIELD_PROMO_TYPE_DRIVERS;
                },
                'whenClient' => sprintf("function (attribute, value) {
                                    return $(\"#promo-type_id input[value='%s']\")[0].checked;
                                }", Promo::FIELD_PROMO_TYPE_DRIVERS),
                'message' => t('promo', '«{attribute}» cannot be blank.'),
                'except' => Promo::SCENARIO_UPDATE
            ],

            [
                ['carClasses'],
                'required',
                'when' => function (Promo $model) {
                    return in_array($model->position_id, CarClassRepository::getTypesCarClasses());
                },
                'message' => t('promo', '«{attribute}» cannot be blank.'),
                'except' => Promo::SCENARIO_UPDATE
            ],

            [
                'position_id',
                function ($attribute) use ($promo) {
                    if ($promo->type_id == Promo::FIELD_TYPE_PROMO_DRIVERS) {
                        $usedPositions = PositionRepository::getIdUsedPositions(
                            $promo->city, ['not', ['p.promo_id' => $promo->promo_id]]
                        );
                        if (in_array($promo->position_id, $usedPositions)) {
                            $promo->blocked = $promo->getOldAttribute('blocked');
                            $promo->addError(
                                $attribute,
                                t('promo', 'This profession is already used in another promotion',
                                    ['attribute' => $promo->getAttributeLabel($attribute)])
                            );
                        }
                    }
                },
            ],

            [
                'carClasses',
                function ($attribute) use ($promo) {
                    if ($promo->type_id == Promo::FIELD_TYPE_PROMO_DRIVERS) {
                        $typesPositions = CarClassRepository::getTypesCarClasses();
                        if (!array_key_exists($promo->position_id, $typesPositions)) {
                            return true;
                        }
                        $usedCarClasses = CarClassRepository::getIdUsedCarClass(
                            $promo->city,
                            $typesPositions[$promo->position_id],
                            ['not', ['p.promo_id' => $promo->promo_id]]
                        );
                        foreach ($promo->carClasses as $carClass) {
                            if (in_array($carClass, $usedCarClasses)) {
                                $promo->blocked = $promo->getOldAttribute('blocked');
                                $promo->addError(
                                    $attribute,
                                    t('promo', 'One or more classes are already used in another promotion',
                                        ['attribute' => $promo->getAttributeLabel($attribute)])
                                );
                                return false;
                            }
                        }
                    }
                },
            ],

            [['type_id', 'activation_type_id'], 'required', 'except' => Promo::SCENARIO_UPDATE],
            [
                ['action_count_bonus', 'action_count_discount'], 'safe'
            ],
            [
                ['action_count_id'],
                'filter',
                'filter' => function () use ($promo) {
                    if ($promo->activation_type_id == Promo::FIELD_TYPE_ACTIVATION_BONUSES) {
                        return $promo->action_count_bonus;
                    } elseif ($promo->activation_type_id == Promo::FIELD_TYPE_ACTIVATION_DISCOUNT) {
                        return $promo->action_count_discount;
                    }

                    return null;
                }
            ],
            [
                [
                    'city',
                    'period_type_id',
                    'action_count_id',
                    'action_clients_id',
                ],
                'required',
            ],
            [
                [
                    'type_id',
                    'position_id',
                    'period_type_id',
                    'action_count_id',
                    'action_clients_id',
                    'action_no_other_codes',
                    'activation_type_id',
                    'client_bonuses',
                    'client_discount',
                    'blocked',
                    'city',
                ],
                'integer',
            ],

            ['count_symbols_in_code', 'integer', 'max' => 30, 'min' => 6],
            ['count_codes', 'integer', 'min' => 1, 'max' => 2500],
            ['client_discount', 'integer', 'max' => 100, 'min' => 0],
            ['client_bonuses', 'integer', 'min' => 0],
            ['worker_bonuses', 'integer', 'min' => 0],
            ['worker_commission', 'integer', 'min' => 0, 'max' => 100],

            [['dateStart', 'dateEnd'], 'match', 'pattern' => '#^\d\d.\d\d.\d\d\d\d$#'],
            [['timeStart', 'timeEnd'], 'match', 'pattern' => '#^\d\d:\d\d$#'],

            [['name'], 'string', 'max' => 255],
            [['carClasses', 'symbols'], 'each', 'rule' => ['integer']],

            [
                ['action_clients_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => PromoActionClients::className(),
                'targetAttribute' => ['action_clients_id' => 'action_clients_id'],
            ],
            [
                ['action_count_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => PromoActionCount::className(),
                'targetAttribute' => ['action_count_id' => 'action_count_id'],
            ],
            [
                ['activation_type_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => PromoActivationType::className(),
                'targetAttribute' => ['activation_type_id' => 'activation_type_id'],
            ],
            [
                ['type_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => PromoType::className(),
                'targetAttribute' => ['type_id' => 'type_id'],
            ],

        ];
    }

}