<?php

namespace frontend\modules\promocode\models;

use Yii;
use app\modules\client\models\Client;

/**
 * This is the model class for table "{{%promo_client}}".
 *
 * @property integer         $client_id
 * @property integer         $code_id
 * @property integer         $status_code_id
 * @property integer         $status_time
 *
 * @property PromoCode       $promoCode
 * @property Client          $client
 * @property PromoStatusCode $statusCode
 */
class PromoClient extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%promo_client}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'code_id'], 'required'],
            [['client_id', 'code_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'client_id'     => 'Client ID',
            'promo_code_id' => 'Promo Code ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromoCode()
    {
        return $this->hasOne(PromoCode::className(), ['code_id' => 'code_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    public function getStatusCode()
    {
        return $this->hasOne(PromoStatusCode::className(), ['status_code_id' => 'status_code_id']);
    }
}
