<?php

namespace frontend\modules\promocode\models;

use common\modules\car\models\CarClass;
use common\modules\city\models\City;
use common\modules\employee\models\position\Position;
use common\modules\tenant\models\Tenant;
use frontend\modules\promocode\models\rules\PromoRules;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%promo}}".
 *
 * @property integer              $promo_id
 * @property integer              $tenant_id
 * @property integer              $type_id
 * @property integer              $position_id
 * @property string               $name
 * @property integer              $period_type_id
 * @property integer              $period_start
 * @property integer              $period_end
 * @property integer              $action_count_id
 * @property integer              $action_clients_id
 * @property integer              $action_no_other_codes
 * @property integer              $activation_type_id
 * @property integer              $client_bonuses
 * @property integer              $client_discount
 * @property integer              $worker_commission
 * @property integer              $worker_bonuses
 * @property integer              $blocked
 * @property integer              $created_at
 * @property integer              $updated_at
 * @property integer              $count_codes
 * @property integer              $count_symbols_in_code
 *
 * @property PromoActionClients   $actionClients
 * @property PromoActionCount     $actionCount
 * @property PromoActivationType  $activationType
 * @property PromoType            $type
 * @property PromoHasCity[]       $promoHasCities
 * @property PromoHasSymbolType[] $promoHasSymbolTypes
 * @property PromoCode[]          $promoCodes
 * @property PromoSymbolType[]    $symbolTypes
 * @property City[]               $promoCities
 * @property Tenant[]             $tenant
 * @property Position[]           $position
 * @property CarClass[]           $carSavedClasses
 *
 * @property integer[]            $cities
 * @property integer[]            $symbols
 * @property string[]             $codes
 * @property string[]             $carClasses
 *
 * @property integer              $city
 *
 * @property string               $dateStart
 * @property string               $dateEnd
 * @property string               $timeStart
 * @property string               $timeEnd
 */
class Promo extends \yii\db\ActiveRecord
{
    const FIELD_PROMO_TYPE_MANUALLY = 1;
    const FIELD_PROMO_TYPE_GENERATION = 2;
    const FIELD_PROMO_TYPE_DRIVERS = 3;

    const FIELD_PERIOD_NO_LIMITED = 1;
    const FIELD_PERIOD_LIMITED = 2;

    const FIELD_TYPE_ACTIVATION_BONUSES = 1;
    const FIELD_TYPE_ACTIVATION_DISCOUNT = 2;

    const FIELD_TYPE_PROMO_MANUALLY = 1;
    const FIELD_TYPE_PROMO_GENERATION = 2;
    const FIELD_TYPE_PROMO_DRIVERS = 3;

    const FIELD_ACTION_COUNT_ONCE = 1;
    const FIELD_ACTION_COUNT_EACH_ORDER = 2;
    const FIELD_ACTION_COUNT_ONCE_ACTIVATION = 3;

    const FIELD_ACTION_NO_OTHER_CODES = 1;

    const SCENARIO_UPDATE = 'scenarioUpdate';

    const FORMAT_DATE = 'd.m.Y';
    const FORMAT_TIME = 'H:i';
    const FORMAT_DATETIME = 'd.m.YH:i';

    public $action_count_bonus;
    public $action_count_discount;

    protected $_codes;
    protected $_symbols;
    protected $_classes;
    protected $_cities;

    protected $_dateStart;
    protected $_dateEnd;
    protected $_timeStart;
    protected $_timeEnd;


    public function behaviors()
    {
        return [
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    public function scenarios()
    {
        $scenarios                        = parent::scenarios();
        $scenarios[self::SCENARIO_UPDATE] = [
            'name',
            'period_type_id',
            'period_start',
            'period_end',
            'dateStart',
            'dateEnd',
            'timeEnd',
            'timeStart',
            'blocked',
            'position_id',
            'carClasses',
            'client_bonuses',
            'client_discount',
            'codes',
            'city',
            'activation_type_id',
            'action_count_discount',
            'action_clients_id',
            'action_count_bonus',
            'action_no_other_codes',

        ];

        return $scenarios;
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%promo}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return (new PromoRules())->getRules($this);
    }


    public function attributeLabels()
    {
        return [
            'promo_id'              => 'Promo ID',
            'tenant_id'             => 'Tenant ID',
            'type_id'               => t('promo', 'Type code'),
            'position_id'           => t('promo', 'Profession'),
            'name'                  => t('promo', 'Name'),
            'codes'                 => t('promo', 'Promo-code'),
            'period_type_id'        => t('promo', 'Action period'),
            'period_start'          => 'Period Start',
            'period_end'            => 'Period End',
            'action_count_id'       => t('promo', 'Action'),
            'action_clients_id'     => 'Action Clients ID',
            'action_no_other_codes' => t('promo', 'If the client did not use other promotional codes'),
            'activation_type_id'    => t('promo', 'Activation'),
            'client_bonuses'        => t('promo', 'bonuses'),
            'client_discount'       => '%',
            'worker_commission'     => t('promo', 'Commission from the executor, if he himself executed the order'),
            'worker_bonuses'        => t('promo', 'Bonus if another artist completed the order'),
            'blocked'               => 'Blocked',
            'promoCodes'            => t('promo', 'Promo-code'),
            'cities'                => t('promo', 'City where action'),
            'city'                  => t('promo', 'City where action'),
            'count_codes'           => t('promo', 'Count of codes'),
            'count_symbols_in_code' => t('promo', 'Count of characters in the code'),
            'symbols'               => t('promo', 'Characters in the code'),
            'carClasses'            => t('promo', 'Classes'),
        ];
    }

    public function afterFind()
    {
        parent::afterFind();

        $this->action_count_discount = $this->action_count_bonus = $this->action_count_id;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);


        if ($insert) {
            $this->updateCities();
            $this->updateCarClasses();
            $this->updateSymbols();
        }else{
            $this->updateCities();
            $this->updateCarClasses();
            $this->updateSymbols();
        }

    }

    public function beforeValidate()
    {

        $this->tenant_id = user()->tenant_id;

        return parent::beforeValidate();
    }

    public function beforeSave($insert)
    {
        if ($this->type_id == self::FIELD_TYPE_PROMO_MANUALLY) {
            $this->setSymbols([]);
        }

        if ($this->period_type_id == Promo::FIELD_PERIOD_LIMITED) {
            $this->period_start = $this->getUtcTimestamp($this->_dateStart . $this->_timeStart);
            $this->period_end   = $this->getUtcTimestamp($this->_dateEnd . $this->_timeEnd);
        } else {
            $this->period_start = null;
            $this->period_end   = null;
        }

        $this->action_count_id =  $this->action_count_discount ;


        return parent::beforeSave($insert);
    }


    /**
     * @return string|array
     */
    public function &getCodes()
    {
        if ($this->_codes === null) {
            if ($this->type_id == self::FIELD_TYPE_PROMO_MANUALLY) {
                $this->_codes = $this->promoCodes[0]->code;
            }
        }

        return $this->_codes;
    }

    public function setCodes(&$values)
    {
        $this->_codes =& $values;
    }


    /**
     * @deprecated
     */
    public function setCities($values)
    {
        $this->_cities = $values;
    }

    /**
     * @return array
     * @deprecated
     */
    public function getCities()
    {
        if (!$this->_cities) {

            $this->_cities = $this
                ->getPromoCities()
                ->select('city_id')
                ->column();
        }

        return $this->_cities;
    }

    public function setCity($values)
    {
        $this->_cities = [$values];
    }

    public function getCity()
    {
        if (!$this->_cities) {
            $this->_cities = $this
                ->getPromoCities()
                ->select('city_id')
                ->column();
        }

        return $this->_cities[0];
    }


    public function setCarClasses($values)
    {
        $this->_classes = $values;
    }

    public function getCarClasses()
    {
        if (!$this->_classes) {
            $this->_classes = $this
                ->getSavedCarClasses()
                ->select('class_id')
                ->column();
        }

        return $this->_classes;
    }


    public function setSymbols($values)
    {
        $this->_symbols = $values;
    }

    public function getSymbols()
    {
        if (!$this->_symbols) {
            $this->_symbols = $this
                ->getSymbolTypes()
                ->select('symbol_type_id')
                ->column();
        }

        return $this->_symbols;
    }


    public function getDateStart()
    {
        if ($this->_dateStart === null) {
            if ($this->period_start === null) {
                return date(self::FORMAT_DATE, time());
            } else {
                return $this->getTimeWithTimeOffset(self::FORMAT_DATE, $this->period_start);
            }
        }

        return $this->_dateStart;
    }

    public function setDateStart($value)
    {
        $this->_dateStart = $value;
    }


    public function getDateEnd()
    {
        if ($this->_dateEnd === null) {
            if ($this->period_end === null) {
                return date(self::FORMAT_DATE, time() + 3600 * 24 * 30);
            } else {
                return $this->getTimeWithTimeOffset(self::FORMAT_DATE, $this->period_end);
            }
        }

        return $this->_dateEnd;
    }

    public function setDateEnd($value)
    {
        $this->_dateEnd = $value;
    }


    public function getTimeStart()
    {
        if ($this->_timeStart === null) {
            if ($this->period_start === null) {
                return '00:00';
            } else {
                return $this->getTimeWithTimeOffset(self::FORMAT_TIME, $this->period_start);
            }
        }

        return $this->_timeStart;
    }

    public function setTimeStart($value)
    {
        $this->_timeStart = $value;
    }


    public function getTimeEnd()
    {
        if ($this->_timeEnd === null) {
            if ($this->period_end === null) {
                return '23:59';
            } else {
                return $this->getTimeWithTimeOffset(self::FORMAT_TIME, $this->period_end);
            }
        }

        return $this->_timeEnd;
    }

    public function setTimeEnd($value)
    {
        $this->_timeEnd = $value;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActionClients()
    {
        return $this->hasOne(PromoActionClients::className(), ['action_clients_id' => 'action_clients_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActionCount()
    {
        return $this->hasOne(PromoActionCount::className(), ['action_count_id' => 'action_count_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivationType()
    {
        return $this->hasOne(PromoActivationType::className(), ['activation_type_id' => 'activation_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(PromoType::className(), ['type_id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromoHasCities()
    {
        return $this->hasMany(PromoHasCity::className(), ['promo_id' => 'promo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromoHasSymbolTypes()
    {
        return $this->hasMany(PromoHasSymbolType::className(), ['promo_id' => 'promo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSymbolTypes()
    {
        return $this->hasMany(PromoSymbolType::className(), ['symbol_type_id' => 'symbol_type_id'])
            ->viaTable('{{%promo_has_symbol_type}}', ['promo_id' => 'promo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromoCities()
    {
        return $this->hasMany(City::className(), ['city_id' => 'city_id'])
            ->viaTable('{{%promo_has_city}}', ['promo_id' => 'promo_id']);
    }

    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    public function getPeriodType()
    {
        return $this->hasOne(PromoPeriodType::className(), ['period_type_id' => 'period_type_id']);
    }

    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }

    public function getSavedCarClasses()
    {
        return $this->hasMany(CarClass::className(), ['class_id' => 'car_class_id'])
            ->viaTable('{{%promo_has_car_class}}', ['promo_id' => 'promo_id']);
    }

    public function getPromoCodes()
    {
        return $this->hasMany(PromoCode::className(), ['promo_id' => 'promo_id']);
    }

    /**
     * @param array $workers
     *
     * @return PromoCode|PromoCode[]
     */
    public function saveCodes($workers = null)
    {
        if (is_array($this->getCodes())) {


            /* @var $codes PromoCode */
            $codes = [];
            foreach ($this->getCodes() as $key => $code) {
                $models = new PromoCode([
                    'code'         => $code,
                    'promo_id'     => $this->promo_id,
                    'worker_id'    => is_null($workers) ? null : $workers[$key]['worker_id'],
                    'car_class_id' => is_null($workers)
                        ? null
                        : array_key_exists('car_class_id', $workers[$key])
                            ? $workers[$key]['car_class_id']
                            : null,
                ]);
                $models->save();
                $codes[] = $models;
            }

            return $codes;
        } else {
            $models = new PromoCode([
                'code'     => $this->getCodes(),
                'promo_id' => $this->promo_id,
            ]);

            $models->save();

            return $models;
        }
    }
    public function updateCodes($workers = null){

        if (is_array($this->getCodes())) {


            /* @var $codes PromoCode */
            $codes = [];
            foreach ($this->getCodes() as $key => $code) {

                $PromoCode =  PromoCode::find()
                    ->where(['promo_id' => $this->promo_id ])
                    ->one();

                if(!$PromoCode){

                    $PromoCode = new PromoCode([
                      'code'         => $code,
                      'promo_id'     => $this->promo_id,
                      'worker_id'    => is_null($workers) ? null : $workers[$key]['worker_id'],
                      'car_class_id' => is_null($workers)
                        ? null
                        : array_key_exists('car_class_id', $workers[$key])
                            ? $workers[$key]['car_class_id']
                            : null,
                        ]);
                    $PromoCode->save();
                    $codes[] = $PromoCode;
                }else{
                    $PromoCode->code = $code;
                    $PromoCode->promo_id = $this->promo_id;
                    $PromoCode->worker_id = is_null($workers) ? null : $workers[$key]['worker_id'];
                    $PromoCode->car_class_id = is_null($workers)? null
                        :array_key_exists('car_class_id', $workers[$key])
                            ? $workers[$key]['car_class_id']
                            : null;

                    $PromoCode->save();
                    $codes[] = $PromoCode;
                }
            }

            return $codes;
        } else {
            $PromoCode =  PromoCode::find()
                ->where(['promo_id' => $this->promo_id ])
                ->one();
            if($PromoCode){
                $PromoCode->code = $this->getCodes();
                $PromoCode->save();
            }else{
                $PromoCode = new PromoCode([
                    'code'     => $this->getCodes(),
                    'promo_id' => $this->promo_id,
                ]);

                $PromoCode->save();
            }

            return $PromoCode;
        }

    }


    public function updateCities()
    {
        $currentCitiesIds = $this->getPromoCities()->select('city_id')->column();
        $newCitiesIds     = $this->getCities();




        $toInsert = [];
        foreach (array_filter(array_diff($newCitiesIds, $currentCitiesIds)) as $cityId) {
            $toInsert[] = ['promo_id' => $this->promo_id, 'city_id' => $cityId];
        }
        if ($toInsert) {
            PromoHasCity::getDb()
                ->createCommand()
                ->batchInsert(
                    PromoHasCity::tableName(),
                    ['promo_id', 'city_id'],
                    $toInsert
                )
                ->execute();
        }

        if ($toRemove = array_filter(array_diff($currentCitiesIds, $newCitiesIds))) {
            PromoHasCity::deleteAll(['promo_id' => $this->promo_id, 'city_id' => $toRemove]);
        }
    }


    public function updateCarClasses()
    {
        $currentClassesIds = $this->getSavedCarClasses()->select('class_id')->column();
        $newClassesIds     = $this->getCarClasses();

        $toInsert = [];
        foreach (array_filter(array_diff($newClassesIds, $currentClassesIds)) as $classId) {
            $toInsert[] = ['promo_id' => $this->promo_id, 'car_class_id' => $classId];
        }
        $toInsert = array_unique($toInsert);


        if ($toInsert) {
            PromoHasCarClass::getDb()
                ->createCommand()
                ->batchInsert(
                    PromoHasCarClass::tableName(),
                    ['promo_id', 'car_class_id'],
                    $toInsert
                )
                ->execute();
        }

        if ($toRemove = array_filter(array_diff($currentClassesIds, $newClassesIds))) {
            $toRemove = array_unique($toRemove);
            PromoHasCarClass::deleteAll(['promo_id' => $this->promo_id, 'car_class_id' => $toRemove]);
        }
    }


    public function updateSymbols()
    {
        $currentSymbolsIds = $this->getSymbolTypes()->select('symbol_type_id')->column();
        $newSymbolsIds     = $this->getSymbols();

        $toInsert = [];
        foreach (array_filter(array_diff($newSymbolsIds, $currentSymbolsIds)) as $symbolId) {
            $toInsert[] = ['promo_id' => $this->promo_id, 'symbol_type_id' => $symbolId];
        }


        if ($toInsert) {
            PromoHasSymbolType::getDb()
                ->createCommand()
                ->batchInsert(
                    PromoHasSymbolType::tableName(),
                    ['promo_id', 'symbol_type_id'],
                    $toInsert
                )
                ->execute();
        }

        if ($toRemove = array_filter(array_diff($currentSymbolsIds, $newSymbolsIds))) {
            PromoHasSymbolType::deleteAll(['promo_id' => $this->promo_id, 'symbol_type_id' => $toRemove]);
        }
    }

    protected function getTimeWithTimeOffset($format, $timestamp)
    {
        $timestamp = $timestamp + City::getTimeOffset($this->city);

        return date($format, $timestamp);
    }

    protected function getUtcTimestamp($dateValueFormat)
    {
        $date = \DateTime::createFromFormat(self::FORMAT_DATETIME, $dateValueFormat);

        return $date->format('U') - City::getTimeOffset($this->city);
    }
}
