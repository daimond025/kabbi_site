<?php

namespace frontend\modules\promocode\models;

use Yii;

/**
 * This is the model class for table "{{%promo_symbol_type}}".
 *
 * @property integer $symbol_type_id
 * @property string $name
 *
 * @property PromoHasSymbolType[] $promoHasSymbolTypes
 */
class PromoSymbolType extends \yii\db\ActiveRecord
{


    const SYMBOL_LATIN_LOWERCASE = 1;
    const SYMBOL_DIGITS = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%promo_symbol_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'symbol_type_id' => 'Symbol Type ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromoHasSymbolTypes()
    {
        return $this->hasMany(PromoHasSymbolType::className(), ['symbol_type_id' => 'symbol_type_id']);
    }
}
