<?php

namespace frontend\modules\promocode\models;

use app\modules\client\models\Client;
use common\modules\car\models\CarClass;
use common\modules\employee\models\worker\Worker;
use common\modules\order\models\Order;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%promo_code}}".
 *
 * @property integer     $code_id
 * @property integer     $promo_id
 * @property string      $code
 * @property integer     $created_at
 * @property integer     $updated_at
 * @property integer     $worker_id
 * @property integer     $car_class_id
 *
 * @property Promo       $promo
 * @property Order[]     $orders
 * @property Client      $client
 * @property Worker      $worker
 * @property CarClass    $carClass
 * @property PromoClient $promoClient
 */
class PromoCode extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%promo_code}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['promo_id', 'code'], 'required'],
            [['promo_id'], 'integer'],
            [['code'], 'string', 'max' => 255],
            [
                ['promo_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Promo::className(),
                'targetAttribute' => ['promo_id' => 'promo_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code_id'  => 'Code ID',
            'promo_id' => 'Promo ID',
            'code'     => t('promo', 'Code'),
            'worker'   => t('promo', 'Worker'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromo()
    {
        return $this->hasOne(Promo::className(), ['promo_id' => 'promo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['promo_code_id' => 'code_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasMany(Client::className(), ['client_id' => 'client_id'])
            ->viaTable(PromoClient::tableName(), ['code_id' => 'code_id']);
    }

    public function getPromoClient()
    {
        return $this->hasMany(PromoClient::className(), ['code_id' => 'code_id']);
    }

    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }

    public function getCarClass()
    {
        return $this->hasOne(CarClass::className(), ['class_id' => 'car_class_id']);
    }

}
