<?php

namespace frontend\modules\promocode\models;

use Yii;

/**
 * This is the model class for table "{{%promo_status_code}}".
 *
 * @property integer $status_code_id
 * @property string  $status_label
 */
class PromoStatusCode extends \yii\db\ActiveRecord
{

    const STATUS_NEW = 1;
    const STATUS_ACTIVATED = 2;
    const STATUS_USED = 3;
    const STATUS_EXPIRED = 4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%promo_status_code}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_label'], 'required'],
            [['status_label'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'status_code_id' => 'Status Code ID',
            'status_label'   => 'Status Label',
        ];
    }
}
