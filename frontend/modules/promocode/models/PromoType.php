<?php

namespace frontend\modules\promocode\models;

use Yii;

/**
 * This is the model class for table "{{%promo_type}}".
 *
 * @property integer $type_id
 * @property string $name
 *
 * @property Promo[] $promos
 */
class PromoType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%promo_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'name'], 'required'],
            [['type_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type_id' => 'Type ID',
            'name' => t('promo', 'Type code'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromos()
    {
        return $this->hasMany(Promo::className(), ['type_id' => 'type_id']);
    }
}
