<?php

namespace frontend\modules\promocode\models\search;


use app\modules\client\models\Client;
use common\modules\employee\models\worker\Worker;
use common\modules\order\models\Order;
use frontend\modules\promocode\models\Promo;
use frontend\modules\promocode\models\PromoClient;
use yii\data\ActiveDataProvider;

class PromoOrderSearch extends Order
{

    const PAGE_SIZE = 20;

    public $promo_id;

    public function rules()
    {
        return [
            [['promo_id', 'worker_id', 'client_id'], 'integer'],
            [
                ['promo_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Promo::className(),
                'targetAttribute' => ['promo_id' => 'promo_id'],
            ],
            [
                ['worker_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Worker::className(),
                'targetAttribute' => ['worker_id' => 'worker_id'],
            ],
            [
                ['client_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Client::className(),
                'targetAttribute' => ['client_id' => 'client_id'],
            ],
        ];
    }

    public function search($param, $pagination = true)
    {
        $this->load($param, '');

        $queryOrder = Order::find()
            ->alias('o')
            ->select([
                'o.status_time',
                'o.promo_code_id',
                'o.order_number',
                'o.client_id',
                'o.worker_id',
                'o.car_id',
            ])
            ->joinWith([
                'promoCode c',
                'promoCode.promo p',
                'promoCode.client cl',
                'promoCode.promo.type'
            ])
            ->andWhere(['`p`.`tenant_id`' => user()->tenant_id]);

        $queryClient = PromoClient::find()
            ->alias('pc')
            ->select("`pc`.`status_time`, `pc`.`code_id`, '' AS `order_number`, `pc`.`client_id`, '' AS `worker_id`, '' AS `car_id`")
            ->joinWith([
                'promoCode pcd',
                'promoCode.promo pcp'
            ], false)
            ->andWhere(['pcp.tenant_id' => user()->tenant_id]);


        if ($this->promo_id || $this->client_id) {
            $finalQuery = Order::find()
                ->from(['dummy_name' => $queryOrder->union($queryClient)])
                ->orderBy(['status_time' => SORT_DESC]);
        } else {
            $finalQuery = $queryOrder;
        }


        $dataProvider = new ActiveDataProvider([
            'query' => $finalQuery,

            'pagination' => [
                'pageSize' => $pagination ? self::PAGE_SIZE : false,
            ],

            'sort' => false

        ]);


        if (!$this->validate()) {
            return false;
        }

        $queryOrder
            ->andFilterWhere(['`c`.`promo_id`' => $this->promo_id])
            ->andFilterWhere(['`o`.`worker_id`' => $this->worker_id])
            ->andFilterWhere(['`o`.`client_id`' => $this->client_id]);

        $queryClient
            ->andFilterWhere(['pcd.promo_id' => $this->promo_id])
            ->andFilterWhere(['pc.client_id' => $this->client_id]);

        return $dataProvider;

    }

}