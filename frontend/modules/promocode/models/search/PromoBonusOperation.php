<?php

namespace frontend\modules\promocode\models\search;

use app\modules\client\models\Client;
use common\modules\employee\models\worker\Worker;
use common\modules\order\models\Order;
use Yii;

/**
 * This is the model class for table "{{%promo_bonus_operation}}".
 *
 * @property integer $bonus_operation_id
 * @property integer $order_id
 * @property integer $client_id
 * @property integer $worker_id
 * @property string  $bonus_subject
 * @property integer $completed
 * @property integer $promo_bonus
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Client  $client
 * @property Order   $order
 * @property Worker  $worker
 */
class PromoBonusOperation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%promo_bonus_operation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'bonus_subject', 'promo_bonus'], 'required'],
            [['order_id', 'client_id', 'worker_id', 'completed', 'promo_bonus', 'created_at', 'updated_at'], 'integer'],
            [['bonus_subject'], 'string'],
            [
                ['client_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Client::className(),
                'targetAttribute' => ['client_id' => 'client_id'],
            ],
            [
                ['order_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Order::className(),
                'targetAttribute' => ['order_id' => 'order_id'],
            ],
            [
                ['worker_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Worker::className(),
                'targetAttribute' => ['worker_id' => 'worker_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bonus_operation_id' => 'Bonus Operation ID',
            'order_id'           => 'Order ID',
            'client_id'          => 'Client ID',
            'worker_id'          => 'Worker ID',
            'bonus_subject'      => 'Bonus Subject',
            'completed'          => 'Completed',
            'promo_bonus'        => 'Promo Bonus',
            'created_at'         => 'Created At',
            'updated_at'         => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }
}
