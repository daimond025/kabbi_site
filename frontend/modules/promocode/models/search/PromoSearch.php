<?php

namespace frontend\modules\promocode\models\search;

use frontend\modules\promocode\models\Promo;
use frontend\modules\promocode\models\PromoHasCity;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;


/**
 * @property array $accessCityList
 */
class PromoSearch extends Promo
{

    const PAGE_SIZE = 20;

    public $blocked;

    protected $_accessCityList;

    public function rules()
    {
        return [
            ['cities', 'in', 'range' => $this->accessCityList, 'allowArray' => true],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $city_list = $this->accessCityList;
        $query     = self::find()->distinct();

        $query->where([
            'tenant_id' => user()->tenant_id,
            'blocked'   => $this->blocked,
        ]);

        $query
            ->joinWith('promoCities')
            ->andWhere([PromoHasCity::tableName() . '.city_id' => $city_list]);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'name' => [
                    'asc'     => ['name' => SORT_ASC],
                    'desc'    => ['name' => SORT_DESC],
                    'default' => SORT_ASC,
                ],
                'type' => [
                    'asc'  => ['type_id' => SORT_ASC],
                    'desc' => ['type_id' => SORT_DESC],
                ],
            ],
        ]);

        $this->load($params);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            PromoHasCity::tableName() . '.city_id' => $this->cities,
        ]);

        return $dataProvider;
    }

    public function getAccessCityList()
    {
        return $this->_accessCityList;
    }

    public function setAccessCityList($value)
    {
        $this->_accessCityList = $value;
    }

}
