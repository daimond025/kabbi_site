<?php

namespace frontend\modules\promocode\models\search;

use frontend\modules\promocode\models\Promo;
use frontend\modules\promocode\models\PromoClient;
use frontend\modules\promocode\models\PromoCode;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%promo_code}}".
 *
 * @property integer $code_id
 * @property integer $promo_id
 * @property string  $code
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $worker_id
 * @property integer $car_class_id
 *
 *
 * @property Promo   $promo
 */
class PromoCodeSearch extends PromoCode
{

    const PAGE_SIZE = 20;

    public function rules()
    {
        return [
            [['promo_id', 'worker_id'], 'integer'],
            [
                ['promo_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Promo::className(),
                'targetAttribute' => ['promo_id' => 'promo_id'],
            ],
        ];
    }

    public function search($pagination = true, $forGeneration = false, $blocked = false)
    {
        $query = PromoCode::find()->alias('c');

        $dataProvider = new ActiveDataProvider([
            'query' => $query
                ->leftJoin(
                    Promo::tableName() . ' `p`',
                    '`c`.`promo_id` = `p`.`promo_id`'
                )
                ->andWhere(['p.tenant_id' => user()->tenant_id]),

            'pagination' => [
                'pageSize' => $pagination ? self::PAGE_SIZE : false,
            ],

            'sort' => [
                'defaultOrder' => [
                    'code_id' => SORT_DESC,
                ],
            ],

        ]);

        if ($forGeneration) {
            $subQuery = PromoClient::find()->alias('cl')
                ->where('`c`.`code_id` = `cl`.`code_id`');

            $query->andWhere(['not exists', $subQuery]);
        }

        if (!$blocked) {
            $query->andWhere(['p.blocked' => 0]);
        } else {
            $query->andWhere(['p.blocked' => 1]);
        }


        if (!$this->validate()) {
            return false;
        }

        $query
            ->andFilterWhere(['c.promo_id' => $this->promo_id])
            ->andFilterWhere(['c.worker_id' => $this->worker_id]);

        return $dataProvider;

    }
}