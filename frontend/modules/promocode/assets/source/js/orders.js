//Инициализация открытия детальных строк статистики
function openDetailRowsInit()
{
    $('body').on('click', '.oor_thead', function () {
        $('[data-theadc="' + $(this).data('thead') + '"]').toggle();
    });
}

function registerOrderOpenInModalEvent(selector) {
    $(document).on('click', selector, function (e) {
        var FORM_CLASS = 'edit-order-fake',
            selector = '.' + FORM_CLASS;

        e.preventDefault();

        $orderForm = $(selector);
        if ($orderForm.length == 0) {
            $orderForm = $('<div/>').addClass(FORM_CLASS);
            $orderForm.appendTo(document.body);
            $orderForm.gootaxModal({
                onComplete: function () {
                    select_init();
                },
                onClosed: function () {
                }
            });
        }
        $orderForm.gootaxModal('settings', {url: $(this).attr('href')})
            .gootaxModal('open');
    });
}


$(function() {

    //Инициализация открытия детальных строк статистики
    openDetailRowsInit();

    //Инициализация открытия карточки заказа
    registerOrderOpenInModalEvent('.js-order-view');

    $(document).on('click', '.js-spoiler', function () {
        $(this).prevAll('.js-full').toggle();
        $(this).prevAll('.js-preview').toggle();
    });

});