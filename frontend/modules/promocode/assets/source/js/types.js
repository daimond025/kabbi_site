;(function () {
    'use strict';

    var app = {

        fieldsPromoManually: null,
        fieldsForGeneration: null,
        fieldsForDrivers: null,

        init: function () {
            this.fieldsPromoManually = $('.promo-type__manually');
            this.fieldsForGeneration = $('.promo-type__generation');
            this.fieldsForDrivers = $('.promo-type__drivers');
            this.setCurrentPromoType();
            this.activeEventForPromoType();
        },

        setCurrentPromoType: function () {
            var _this = this;
            var promoTypeRadio = $('#promo-type_id input');
            $.each(promoTypeRadio, function (key, value) {
                if (value.checked) {
                    _this.changeForm(this);
                }
            });
        },

        activeEventForPromoType: function () {
            var _this = this;
            var promoTypeRadio = $('#promo-type_id input');
            $.each(promoTypeRadio, function (key, value) {
                $(value).on('click', function () {
                    _this.changeForm(this);
                });
            });
        },


        changeTitle: function (value) {
            var name = $('#promo-type_id input[value="' + value + '"]').parent().text().trim().toLowerCase();
            var title = $('.cont h1').data('title');
            $('.cont h1').text(title + ' (' + name + ')');
        },

        changeForm: function (obj) {
            switch (obj.value) {
                case '1':
                    this.deactivateFieldsForGeneration();
                    this.deactivateFieldsForDrivers();
                    this.activateFieldManually();
                    this.changeTitle('1');
                    break;
                case '2':
                    this.deactivateFieldManually();
                    this.deactivateFieldsForDrivers();
                    this.activateFieldsForGeneration();
                    this.changeTitle('2');
                    break;
                case '3':
                    this.deactivateFieldManually();
                    this.deactivateFieldsForGeneration();
                    this.activateFieldsForDrivers();
                    this.changeTitle('3');
                    break;
            }
        },

        activateFieldManually: function () {
            this.fieldsPromoManually.css('display', 'block');
        },

        deactivateFieldManually: function () {
            this.fieldsPromoManually.css('display', 'none');
        },

        activateFieldsForGeneration: function () {
            this.fieldsForGeneration.css('display', 'block');
        },

        deactivateFieldsForGeneration: function () {
            this.fieldsForGeneration.css('display', 'none');
        },

        deactivateFieldsForDrivers: function () {
            this.fieldsForDrivers.css('display', 'none');
        },

        activateFieldsForDrivers: function () {
            this.fieldsForDrivers.css('display', 'block');
        }
    };

    $(window).on('load', function () {
        app.init();
    });

})();