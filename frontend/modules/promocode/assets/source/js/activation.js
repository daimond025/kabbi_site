;(function () {
    'use strict';

    var app = {



        init: function () {
            this.showSelectedActivation();
            this.startAuditionsEvent();
        },

        startAuditionsEvent: function () {
            var inputs = $('input[name="Promo[activation_type_id]"][type="radio"]');
            var _this = this;
            for (var i = 0; i < inputs.length; i++) {
                $(inputs[i]).on('click', function () {
                    _this.showSelectedActivation();
                });
            }
        },


        showSelectedActivation: function () {
            var inputClientBonuses = $('.field-promo-client_bonuses')[0];
            var inputClientDiscount = $('.field-promo-client_discount')[0];

            var inputActionCountBonus = $('#action_count-bonus');
            var inputActionCountDiscount = $('#action_count-discount');

            var radios = $('input[name="Promo[activation_type_id]"][type="radio"]');

            if (radios[0].checked) {
                $(inputClientBonuses).parent().css('display', 'block');
                $(inputClientDiscount).parent().css('display', 'none');

                $(inputActionCountDiscount).css('display', 'none');
                $(inputActionCountBonus).css('display', 'block');
            } else {
                $(inputClientDiscount).parent().css('display', 'block');
                $(inputClientBonuses).parent().css('display', 'none');

                $(inputActionCountDiscount).css('display', 'block');
                $(inputActionCountBonus).css('display', 'none');
            }
        }

    };

    $(window).on('load', function () {
        app.init();
    });

})();