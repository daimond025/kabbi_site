;(function () {
    'use strict';

    var app = {

        init: function () {
            this.filterEvent();
            this.switchingRadio();
            this.enableEventRadio();
        },

        enableEventRadio: function () {
            var _this = this;
            $('#promo-period_type_id input').on('click', function (event) {
                _this.switchingRadio();
            });
        },

        switchingRadio: function () {
            var value = null;
            $.each($('#promo-period_type_id input'), function () {
                if (this.checked) {
                    value = this.value;
                }
            });
            var date = $('.active_input.label_js_input .cof_date');
            var time = $('.active_input.label_js_input input[type="time"]');
            switch (value) {
                case '1':
                    date.addClass('disabled_select');
                    time.attr('disabled', '');
                    break;
                case '2':
                    date.removeClass('disabled_select');
                    time.removeAttr('disabled', '');
                    break;
            }
        },

        changeDatePicker: function () {
            $('.sdp_filter').each(function () {
                $(this).datepicker({
                    altField: $(this).prev('input'),
                    onSelect: function (date) {
                        $(this).parent().prev('.a_sc').html(date).removeClass('sel_active');
                        $(this).parent().hide();
                    }
                });
                $(this).datepicker("setDate", $(this.parentNode.parentNode).find('a.s_datepicker.a_sc.select').text());
            });
        },


        filterEvent: function () {
            this.changeDatePicker();
        }

    };

    app.init();

})();