;(function () {
    'use strict';

    var app = {

        carClassSee : true,

        placeholderCarClasses: '',
        placeholderPosition: '',
        placeholderCity: '',
        selectedCityId: '',

        init: function () {
            this.placeholderCarClasses = $('.field-promo-carclasses').data('placeholder');
            this.placeholderPosition = $('#promo-position_id').data('placeholder');
            this.placeholderCity = $('#promo-city').data('placeholder');
            this.selectedCityId = $('#promo-city option[selected]').val();

            this.startAuditionsEvent();

            if (this.isUpdate()) {
                this.forUpdatePromo();
            }


        },

        startAuditionsEvent: function () {
            var _this = this;
            $('#promo-type_id input').on('click', function () {
                _this.emptyCities();
                _this.emptyPositions();
                _this.emptyCarClasses();
            });

            $('body').on('change', '#promo-city', function () {
                _this.emptyPositions();
                _this.emptyCarClasses();
                _this.ajaxUpdatePosition(this.value);
                _this.selectedCityId = this.value;
            });

            $('body').on('change', '#promo-position_id', function () {
                _this.emptyCarClasses();
                app.carClassSee = true;
                _this.ajaxUpdateCarClasses(this.value);
            });


        },

        ajaxUpdateCarClasses: function (positionId, async = true) {
            var url;
            var typeCar = $('.field-promo-position_id').data('types-car-classes');
            if ( ! typeCar[positionId]) {
                return false;
            }
            if (this.isForWorkers()) {
                url = '/promocode/promo/get-car-classes-workers/' + typeCar[positionId];
            } else {
                url = '/promocode/promo/get-car-classes/' + typeCar[positionId];
            }
            var _this = this;

            $.ajax({
                url: url,
                type: 'POST',
                async: async,
                data: {city_id: this.selectedCityId},
                success: function (carClasses) {

                    if(app.carClassSee){
                        _this.updateCarClassesByPosition(carClasses);
                        app.carClassSee = false;
                    }


                }
            });
        },
        updateCarClassesByPosition: function (carClasses) {
            var ul = $('#promo-carclasses');
           //$('#promo-carclasses').empty();


            for (var key in carClasses) {
                if (carClasses[key][1] === 'used') {
                    $(ul).append(
                        '<li><label style="color: red"><input type="checkbox" name="Promo[carClasses][]" value="'
                        + carClasses[key][2] + '">' + carClasses[key][0] + ' (используется в другой промо акции)</label></li>'
                    );
                } else {
                    $(ul).append(
                        '<li><label><input type="checkbox" name="Promo[carClasses][]" value="'
                        + carClasses[key][2] + '">' + carClasses[key][0] + '</label></li>'
                    );
                }
            }
        },

        ajaxUpdatePosition: function (cityId, async = true) {
            var url = this.isForWorkers()
                ? '/promocode/promo/get-positions-workers/' + cityId
                : '/promocode/promo/get-positions/' + cityId;
            var _this = this;
            $.ajax({
                url: url,
                dataType: "json",
                type: 'get',
                contentType: false,
                processData: false,
                async: async,
                success: function (data) {
                    _this.updatePositionsByCity(data);

                }
            });
        },
        updatePositionsByCity: function (positions) {

            var currentPositionsLi = '';
            var currentPositionsOption = '';

            for (var positionId in positions) {
                if (positions[positionId][1] === 'used') {
                    currentPositionsLi +=
                        '<li class><a style="color: red" rel="' + positionId + '">'
                        + positions[positionId][0]
                        + ' (используется в другой промо акции)'
                        + '</a></li>';
                } else {
                    currentPositionsLi +=
                        '<li class><a rel="' + positionId + '">'
                        + positions[positionId][0]
                        + '</a></li>';
                }
                currentPositionsOption +=
                    '<option value="' + positionId + '">'
                    + positions[positionId][0]
                    + '</option>';
            }

            var input = document.getElementById('input_positions');
            $(input).find('.c_select ul').html(currentPositionsLi);
            $(input).find('select').html(currentPositionsOption);
        },

        isForWorkers: function () {
            var $radio = $('#promo-type_id input[value="3"]');
            if ($radio.length > 0) {
                return $radio[0].checked;
            }
            return false;
        },

        emptyCities: function () {
            $('.field-promo-city .a_select').text(this.placeholderCity);
        },
        emptyPositions: function () {
            $('#input_positions .c_select ul')[0].innerText = '';
            $('.field-promo-position_id .a_select').text(this.placeholderPosition);
        },
        emptyCarClasses: function () {
            $('.field-promo-carclasses .b_sc ul')[0].innerText = '';
            $('.field-promo-carclasses .a_sc').text(this.placeholderCarClasses);
        },

        forUpdatePromo: function () {
            createCheckboxStringOnReady('#promo-carclasses');
        },

        isUpdate: function () {
            if ($('.field-promo-city .disabled_select').length > 0) {
                return true;
            }
            return false;
        }

    };

    var whenErrorSave = {

        positionId: '',

        init: function () {
            this.positionId = $('.field-promo-position_id').data('selected-position');

            if (this.isErrorSave()) {
                this.uploadPosition();
                this.selectPosition();
                this.updateCarClasses();
                this.selectCarClasses();
            }
        },

        uploadPosition: function () {
            var $cityId = $('#promo-city option[selected]');
            app.ajaxUpdatePosition($cityId.val(), false);
        },

        updateCarClasses: function () {
            app.ajaxUpdateCarClasses(this.positionId, false);
        },

        selectPosition: function () {
            var options = ($('#promo-position_id option'));
            for(var i = 0; i < options.length; i++) {
                if (options[i].value == this.positionId) {
                    $('#input_positions .c_select a[rel="' + options[i].value + '"]').trigger('click');
                }
            }
        },

        selectCarClasses: function () {

            var carClassesId = $('.field-promo-carclasses').data('selected-car-classes');

            for (var key in carClassesId) {
                $('#promo-carclasses input[value="' + carClassesId[key] + '"]').trigger('click');
            }

            this.createCheckboxStringOnClick();
        },


        createCheckboxStringOnClick: function () {
            var sc_c = '';
            var checkbox_cnt = 0;
            $('#promo-carclasses label').each(function () {
                if ($(this).find('input[type="checkbox"]').prop("checked")) {
                    if (checkbox_cnt == 0) {
                        sc_c += $(this).text();
                    } else {
                        sc_c += ', ' + $(this).text();
                    }

                    checkbox_cnt++;
                }
            });

            if (sc_c != '') {
                $('.field-promo-carclasses .select_checkbox').find('.a_sc').text(sc_c);
            } else {
                $('.field-promo-carclasses .select_checkbox')
                    .find('.a_sc').text($(this).parents('.select_checkbox').find('.a_sc').attr('rel'));
            }
        },

        isErrorSave: function () {
            if (($('#promo-city option[selected]').length > 0) && !app.isUpdate()) {
                return true;
            } else {
                return false;
            }
        }

    };


    $(window).on('load', function () {
        app.init();
        whenErrorSave.init();
    });

})();