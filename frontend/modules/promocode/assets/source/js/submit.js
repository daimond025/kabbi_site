;(function () {
    'use strict';


    var app = {

        inputs: null,

        init: function () {
            this.registerCheckListEvent();
            this.registerFormSubmitEvent();
            this.emptyMessageErrorForTime();
            this.emptyMessageErrorForSymbols();
            this.emptyMessageErrorForPosition();
            this.emptyMessageErrorForCity();
        },

        emptyMessageErrorForTime: function () {
            var _this = this;
            $('#promo-form .radio-period-sc').on('click', function (event) {
                _this.setErrorText(false, $('.field-promo-period_type_id .error-block-period-type'));
            });
        },

        emptyMessageErrorForSymbols: function () {
            var _this = this;
            $('#promo-symbols input').on('click', function (event) {
                _this.setErrorText(false, $('.field-promo-symbols .error-block-symbols'));
            });
        },

        emptyMessageErrorForCity: function () {
            var _this = this;
            $('#promo-city').on('change', function (event) {
                _this.setErrorText(false, $('#city_select .field-promo-city .help-block-error'));
            });
        },

        emptyMessageErrorForPosition: function () {
            var _this = this;
            $('#input_positions select').on('change', function (event) {
                _this.setErrorText(false, $('#input_positions .help-block-error'));
                _this.setErrorText(false, $('.field-promo-carclasses .help-block-error'));
            });
        },

        registerCheckListEvent: function () {
            var _this = this;
            $('#promo-form .a_sc').on('click', function (event) {
                showStyleFilter($(this), event);
                createCheckboxStringOnClick();
                _this.setErrorText(false, $('.field-promo-city .help-block-error'));
                _this.setErrorText(false, $('.field-promo-carclasses .help-block-error'));
            });
        },

        registerFormSubmitEvent: function () {
            var _this = this;

            $('#promo-form').on('submit', function () {
                var $form = $(this);

                if (!_this.checkCarClasses() || !_this.checkPosition() || !_this.checkSymbols() || !_this.checkCity()) {

                    if (!_this.checkCity()) {
                        _this.setErrorText(
                            $form.find('#city_select .field-promo-city').data('required-message'),
                            $('#city_select .field-promo-city .help-block-error')
                        );
                    }

                    if (!_this.checkCarClasses()) {
                        _this.setErrorText(
                            $form.find('.field-promo-carclasses').data('required-message'),
                            $('.field-promo-carclasses .help-block-error')
                        );
                    }

                    if (!_this.checkPosition()) {
                        _this.setErrorText(
                            $('#input_positions .field-promo-position_id').data('required-message'),
                            $('#input_positions .help-block-error')
                        );
                    }

                    if (!_this.checkSymbols()) {
                        _this.setErrorText(
                            $('.field-promo-symbols').data('required-message'),
                            $('.field-promo-symbols .error-block-symbols')
                        );
                    }

                    return false;
                }


            });
        },

        checkCity: function () {
            var options = $('#promo-city option');
            var citiesId = [];

            $.each(options, function (index, option) {
                if ($(option).attr('selected') === 'selected') {
                    citiesId.push(option.value);
                }
            });

            if (citiesId.length > 0) {
                return true;
            }
            return false;
        },

        checkCarClasses: function () {
            var $carClasses = $('#promo-form').find('[name="Promo[carClasses][]"]')
                .filter(function (index, elem) {
                    return $(elem).prop('checked');
                });

            var carPosition = $('#input_positions .field-promo-position_id').data('types-car-classes');

            var flag = false;
            for (var key in carPosition) {
                if (carPosition[key] == this.getSelectOptionPosition().value) {
                    flag = true;
                }
            }
            if (!flag) {
                return true;
            }


            if (!$carClasses.length) {
                return false;
            }
            return true;
        },

        checkSymbols: function () {
            var typeOne = $('#promo-type_id input[value="1"]')[0];
            if (typeOne.checked) {
                return true;
            }
            var $symbols = $('#promo-symbols input');
            for (var i = 0; i < $symbols.length; i++) {
                if ($symbols[i].checked) {
                    return true;
                }
            }
            return false;
        },

        checkPosition: function () {
            if (this.isUpdate()) {
                return true;
            }
            if (this.getSelectOptionPosition()) {
                return true;
            }
            return false;
        },

        getSelectOptionPosition: function () {
            var options = $('#promo-position_id option');
            var item = false;
            $.each(options, function (index, option) {
                if ($(option).attr('selected') === 'selected') {
                    item = option;
                }
            });
            return item;
        },


        setErrorText: function (text, where) {
            var $helpBlock = where;
            $helpBlock.text(text);

            if (text) {
                $helpBlock.show();
            } else {
                $helpBlock.hide();
            }
        },

        isUpdate: function () {
            if ($('.field-promo-city .disabled_select').length > 0) {
                return true;
            }
            return false;
        }


    };


    app.init();
})();