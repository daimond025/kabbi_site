<?php
namespace frontend\modules\promocode\assets;


use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class PromoAssets
    extends AssetBundle
{
    public $sourcePath = '@frontend/modules/promocode/assets/source';

    public $publishOptions = [
        'forceCopy' => true
    ];

    public $css = [
        'css/main.css'
    ];
    public $js = [
        'js/submit.js',
        'js/types.js',
        'js/calendar.js',
        'js/worker.js',
        'js/activation.js',
    ];
    public $depends = [
        JqueryAsset::class
    ];
}