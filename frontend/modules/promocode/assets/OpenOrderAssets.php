<?php
namespace frontend\modules\promocode\assets;


use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class OpenOrderAssets
    extends AssetBundle
{
    public $sourcePath = '@frontend/modules/promocode/assets/source';

    public $publishOptions = [
        'forceCopy' => true
    ];

    public $css = [
    ];
    public $js = [
        'js/orders.js',
    ];
    public $depends = [
        JqueryAsset::class
    ];
}