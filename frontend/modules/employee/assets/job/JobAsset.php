<?php

namespace frontend\modules\employee\assets\job;

use yii\web\AssetBundle;

class JobAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/employee/assets/job/source';

    public $publishOptions = [
        'forceCopy' => true,
    ];

    public $css = [
    ];
    public $js = [
        'js/app.js',
    ];
    public $depends = [
    ];
}