;(function () {
    'use strict';

    var app = {
        init: function () {
            select_init();
            initInstaFilta();
            createCheckboxStringOnClick();
            createCheckboxStringOnReady('.b_sc div');
            this.modalInit();
            this.addPositionEvent();
            this.pjaxEndInit();
            this.changeActivePositionEvent();
            this.changePositionClassEvent();
            this.changePositionGroupEvent();
        },
        changeActivePositionEvent: function () {
            $(document).on('change', '.check_spoler [type="checkbox"]', function () {
                var worker_id = $(this).data('worker');
                var position_id = $(this).data('position');
                var data = {
                    worker_id: worker_id,
                    position_id: position_id
                };

                data.active = $(this).prop('checked') ? 1 : 0;

                $.get(
                    '/employee/position/change-activity',
                    data
                );
            });
        },
        pjaxEndInit: function () {
            $(document).on('pjax:end', function () {
                select_init();
                createCheckboxStringOnReady('.b_sc div');
            });
        },
        modalInit: function () {
            $(".add_prof").colorbox({
                inline: true,
                width: "300px",
                onClosed: function () {
                    $('#add_prof .help-block-error').empty();
                }
            });
        },
        addPositionEvent: function () {
            var addBlock = $('#add_prof');
            addBlock.find('button').on('click', function () {
                var worker_id = $(this).data('worker');
                var module_id = addBlock.find('select').val();
                if (module_id) {
                    $.post(
                        '/employee/position/link-position',
                        {worker_id: worker_id, module_id: module_id, pjax_id:'pjax-' + ($('input[data-position]').length + 1)},
                        function (json) {
                            var errorBlock = addBlock.find('.help-block-error');
                            if (json.error) {
                                var errorHtml = '';
                                for (var i = 0; i < json.error.length; i++) {
                                    errorHtml += '<p>' + json.error[i] + '</p>';
                                }
                                errorBlock.html(errorHtml);
                                $.colorbox.resize();
                            } else {
                                $('.tabs_content div.active').append(json.html);
                                errorBlock.empty();
                                $.colorbox.close();
                                select_init();
                            }
                        },
                        'json'
                    );
                }
            });
        },
        changePositionClassEvent: function () {
            var _this = this;
            $(document).on('change', 'select[name="positionClassId"]', function () {
                var select = $(this);
                var positionCheckbox = select.parents('section.spoler_content').prev('h2.check_spoler').find('input');
                var pjaxContainer = select.parents('section.spoler_content').find('[data-pjax-container]').first();
                var positionId = select.val();

                if (!positionId) {
                    return false;
                }

                $.get(
                    '/employee/position/change-position-class',
                    {
                        worker_id: positionCheckbox.data('worker'),
                        position_id: positionCheckbox.attr('data-position'),
                        new_position_id: positionId
                    },
                    function (json) {
                        if (json) {
                            positionCheckbox.attr('data-position', positionId);
                            _this._getPositionGroupList(positionId, select);
                            if (pjaxContainer.length !== 0) {
                                $.pjax.reload({
                                    container: '#' + pjaxContainer.attr('id'),
                                    url: '/employee/position/update?worker_id=' + positionCheckbox.data('worker') + '&position_id=' + positionId + '&pjax_id=' + pjaxContainer.attr('id'),
                                    type: 'POST'
                                });
                            }
                        }
                    }
                );
            });
        },
        _getPositionGroupList: function (positionId, selectJqueryObj) {
            $.get(
                '/employee/position/get-group-list',
                {position_id: positionId},
                function (json) {
                    var groupSelect = selectJqueryObj.parents('td').next('td').find('select[name="positionGroupMap"]');
                    groupSelect.find('option:not(":first")').remove();

                    if (json) {
                        var groups = $();
                        if (!$.isEmptyObject(json)) {
                            for (var groupId in json) {
                                groups = groups.add($('<option/>').val(groupId).text(json[groupId]));
                            }

                            groupSelect.append(groups);
                        }
                    }
                    groupSelect.trigger('update');
                }
            );
        },
        changePositionGroupEvent: function () {
            $(document).on('change', 'select[name="positionGroupMap"]', function () {
                var select = $(this);
                var positionId = select.parents('tr').find('select[name="positionClassId"]').val();
                var positionCheckbox = select.parents('section.spoler_content').prev('h2.check_spoler').find('input');
                var groupId = select.val();

                if (!positionId || !groupId) {
                    return false;
                }

                $.get(
                    '/employee/position/change-group',
                    {
                        worker_id: positionCheckbox.data('worker'),
                        position_id: positionId,
                        group_id: groupId
                    },
                    function (json) {
                    }
                );
            });
        }
    };

    app.init();
})();