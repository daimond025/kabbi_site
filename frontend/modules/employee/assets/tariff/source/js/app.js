;(function () {
    'use strict';

    var app = {
        init: function () {
            createCheckboxStringOnReady();
            createCheckboxStringOnClick();
            this.optionSwitchingEvent();
            this.tarifftypeChangingEvent();
            this.cityChangingEvent();
            this.optionIntervalChangingEvent();
            this.tariffPositionChangingEvent();
            this.registerTariffCommissionEvents();
            this.ndsTriggerEvent();
            this.triggerDisabled();
            this.initModal();
        },
        optionSwitchingEvent: function () {
            //Включение/выключение опций тарифа(Выходные, исключения)
            $('input.switch').on('change', function () {
                $('#' + $(this).data('type')).toggle();
            });
        },
        ndsTriggerEvent: function () {
            $('body').on('change', 'input[type="checkbox"].js-checkbox-trigger', function () {
                $(this).closest('.js-checkbox-trigger-parent').find('.js-checkbox-trigger-checked, .js-checkbox-trigger-not-checked').toggle();
            });
        },
        triggerDisabled: function () {
            $('body').on('change', '.js-trigger-disabled', function () {
                var target = $(this).closest('.worker-tariff-exception')
                    .find('[data-trigger="' + $(this).data('trigger-target') + '"]');
                if ($(target).prop('readonly')) $(target).prop('readonly', false).trigger('update');
                else $(target).prop('readonly', true).val('').trigger('update');
            });
        },
        tarifftypeChangingEvent: function () {
            $('body').on('change', '#tariff-type', function () {
                var tariff = $(this).val();
                if (tariff == 'SUBSCRIPTION') {
                    $('#tariff-action_new_shift').parents('section.row').hide();
                    $('.cost_of_subscription').show();
                    $('.cost_of_entry_per_shift').hide();
                    $('.default_label_cost').hide();
                    $('#tariff-days').parents('section.row').show();
                    $('#tariff-subscription_limit_type').parents('section.row').show();
                    $('#tariff-subscription_limit_type [type="checkbox"]').first().prop('checked');
                    $(this).parents('form').find('div.tabs_links').hide().find('a.t01').click();
                    $('#t01 .out_on_line').hide().next().show();
                    // Отключение опций тарифа (Выходные, исключения)
                    $('input.switch').attr('checked', false).each(function () {
                        $('#' + $(this).data('type')).hide();
                    });
                } else {
                    $('#tariff-action_new_shift').parents('section.row').show();
                    $('.cost_of_entry_per_shift').show();
                    $('.cost_of_subscription').hide();
                    $('.default_label_cost').hide();
                    $('#tariff-days').parents('section.row').hide();
                    $('#tariff-subscription_limit_type').parents('section.row').hide();
                    $(this).parents('form').find('div.tabs_links').show();
                    $('#t01 .out_on_line').show().next().hide();
                }
            });
        },
        updateCarOptionList: function (city_id) {
            $.get(
                '/employee/tariff/get-active-car-options',
                {city_id: city_id},
                function (json) {
                    var additional_options = $('.additional_options');
                    additional_options.find('.check_trigger').hide();

                    if ($.isEmptyObject(json)) {
                        additional_options.hide();
                    } else {
                        additional_options.show();
                        additional_options.find('.check_trigger').each(function () {
                            var option_id = $(this).data('option');
                            if (in_array(option_id, json)) {
                                $(this).show();
                            }
                        });
                    }
                },
                'json'
            );
        },
        updateCurrencySymbol: function (object, city_id) {
            $.get(
                '/city/city/get-city-currency-symbol',
                {city_id: city_id},
                function (json) {
                    var sumbol_wrap = object.parents('form').find('span.subtext');
                    sumbol_wrap.each(function () {
                        var symbol_list = $(this).find('select');
                        if (symbol_list.size() > 0) {
                            symbol_list.find("option[value='MONEY']").text(json);
                            symbol_list.trigger('update');
                        } else {
                            $(this).text(json);
                        }
                    });
                },
                'json'
            );
        },
        cityChangingEvent: function () {
            var _this = this;
            $('body').on('change', '#tariff-city_list', function () {
                var cur_element = $(this);
                var city_id = cur_element.val();
                _this.updateCurrencySymbol(cur_element, city_id);
                _this.updateCarOptionList(city_id);
                _this.updatePositionList(city_id);
            });
        },
        updatePositionList: function (cityId) {
            $.get(
                '/employee/position/get-positions',
                {city_id: cityId},
                function (json) {
                    if ($.isArray(json)) {
                        var select = $('#tariff-position_id');
                        select.find('option:not(":first")').remove();
                        var options = $();

                        for (var i = 0; i < json.length; i++) {
                            options = options.add($('<option/>').val(json[i]['position_id']).text(json[i]['name']));
                        }

                        select.append(options);

                        select.trigger('update');
                    }
                },
                'json'
            );
        },
        optionIntervalChangingEvent: function () {
            $(document).on('change', '#optiontariffcurrent-end_interval, #optiontariffholidays-end_interval, #optiontariffexceptions-end_interval', function () {
                var current_value = $(this).val();
                var prev_value = $(this).prev().val();

                if (current_value == prev_value) {
                    $(this).parent().addClass('input_error');
                }
                else {
                    $(this).parent().removeClass('input_error');
                }
            });
            $('#optiontariffcurrent-end_interval, #optiontariffholidays-end_interval, #optiontariffexceptions-end_interval').trigger('change');
        },
        tariffPositionChangingEvent: function () {
            $('#entity_position').on('positionChange', function (event, position_id, hasCar) {
                if (hasCar) {
                    $('section.additional_options').show();
                } else {
                    $('section.additional_options').hide();
                }
            });
        },
        registerTariffCommissionEvents: function () {
            $('body').on('change', '.js-interval-commission [type="radio"]', function () {
                var $this = $(this),
                    value = $(this).val(),
                    $commission = $this.closest('.js-commission');

                if (value === $commission.find('.js-interval-commission').data('united-type')) {
                    $commission.find('.js-fixed').addClass('active_rtt');
                    $commission.find('.js-interval').removeClass('active_rtt');
                } else {
                    $commission.find('.js-fixed').removeClass('active_rtt');
                    $commission.find('.js-interval').addClass('active_rtt');
                }
            });

            $('body').on('change', '.js-to', function () {
                var $this = $(this);
                $this.closest('.item').next().find('.js-from').val($this.val());
            });
        },
        initModal: function () {
            $('.js-modal-ex-hidden').gootaxModal({
                inline: false,
                onComplete: function () {
                    select_init();
                },
            });

            $('body').on('click', '.js-edit-ex', function (e) {
                e.preventDefault();
                var link = $(this).attr('href');
                $('.js-modal-ex-hidden').gootaxModal('settings', {url: link}).gootaxModal('open');
            });

            $('body').on('click', '.js-option-delete', function (e) {
                var $form = $(this).closest('form');
                e.preventDefault();
                $.ajax({
                    async:true,
                    type: "POST",
                    url: '/employee/tariff/delete-option?tariffId=' + $form.data('tariff-id')
                    + ($form.data('option-id') ? '&optionId=' + $form.data('option-id') : ''),
                    complete: function(xhr, textStatus) {
                        var url = xhr && xhr.getResponseHeader('X-Redirect');
                        if (url) {
                            window.location = url;
                        }
                    }
                });
                return false;
            })

            $('body').on('click', '.js-submit-option', function (e) {
                var $form = $(this).closest('form');

                e.preventDefault();

                if ($form.find('.js-active-date').length) {
                    var activeDate = $form
                        .find('.choosing_date_section .tags_input ul input')
                        .map(function () {
                            return $(this).val()
                        })
                        .get()
                        .join(';');

                    if (!activeDate) {
                        $form.find('.js-active-date').addClass('input_error');
                        return false;
                    } else {
                        $form.find('.js-active-date').removeClass('input_error');
                    }
                }

                $.ajax({
                    async:true,
                    type: "POST",
                    url: '/employee/tariff/update-option?tariffId=' + $form.data('tariff-id')
                    + ($form.data('option-id') ? '&optionId=' + $form.data('option-id') : ''),
                    data: $form.serialize(),
                    dataType: "json",
                    complete: function(xhr, textStatus) {
                        var url = xhr && xhr.getResponseHeader('X-Redirect');
                        if (url) {
                            window.location = url;
                        }
                    }
                });
            });
        }
    };

    app.init();
})();