<?php

namespace frontend\modules\employee\assets\tariff;

use yii\web\AssetBundle;


class TariffAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/employee/assets/tariff/source';

    public $publishOptions = [
        'forceCopy' => true
    ];

    public $css = [
    ];
    public $js = [
        'js/app.js'
    ];
    public $depends = [
    ];
}