<?php

namespace frontend\modules\employee\assets\workCar;

use yii\web\AssetBundle;


class WorkCarAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/employee/assets/workCar/source';

    public $publishOptions = [
        'forceCopy' => true
    ];

    public $css = [
    ];
    public $js = [
        'js/ui/jquery-ui.js'
    ];
    public $depends = [
    ];
}