<?php


namespace frontend\modules\employee\assets\groups;


use yii\web\AssetBundle;

class GroupAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/employee/assets/groups/source';

    public $publishOptions = [
        'forceCopy' => true,
    ];

    public $css = [
    ];
    public $js = [
        'js/app.js',
    ];
    public $depends = [
    ];
}