;(function () {
    'use strict';

    var app = {
        defaultViewOrderSelector: '#required_view_order',
        tariffListSelector: '#workergroup-tariff_list',
        cityListSelector: '#workergroup-city_list',
        classIdSelector: '#workergroup-class_id',
        carClassIdSectionSelector: '#car_classes',
        positionIdSectionSelector: '#entity_position',
        positionClassSectionSelector: '#position_classes',
        modelName: 'WorkerGroup',
        cityListChangingAction: '/employee/group/update-model-data',
        positionChangingAction: '/employee/group/changed-position',
        classListChangingAction: '/employee/group/update-changed-class',

        init: function () {
            createCheckboxStringOnReady();
            createCheckboxStringOnClick();
            this.loadingButtonEvent();
            this.cityListChangingEvent();
            this.carClassListChangingEvent();
            this.positionListChangingEvent();
        },
        _getCityList: function () {
            var city_list = [];
            city_list.push($(this.cityListSelector).val());

            return city_list;
        },
        _updateWorkerTariffList: function (tariff_map) {
            if ($.isEmptyObject(tariff_map)) {
                $(this.tariffListSelector).empty();
                return false;
            }

            var $tariffs = $();
            for (var tariff_id in tariff_map) {
                $tariffs = $tariffs
                    .add($('<label/>')
                        .text(' ' + tariff_map[tariff_id])
                        .prepend($('<input type="checkbox" checked name="' + this.modelName + '[tariff_list][]"/>')
                            .val(tariff_id)));
            }
            $(this.tariffListSelector).html($tariffs);
        },
        _updatePositionsForExtendsGroup: function (positions) {
            var $item, $label, $input, positionId;
            var $positions = $();

            for (positionId in positions) {
                $item = $('<li/>');
                $label = $('<label/>')
                    .attr('for', 'positionsForExtendsGroup_' + positionId)
                    .text(' ' + positions[positionId]);
                $input = $('<input type="checkbox" name="' + this.modelName + '[positionsForExtendsGroup][]"/>')
                    .attr('id', 'positionsForExtendsGroup_' + positionId)
                    .val(positionId);

                $positions = $positions.add(
                    $item.append([$input, $label]));
            }

            $('#positionsForExtendsGroup').closest('.row').toggle($positions.length > 0);
            $('#positionsForExtendsGroup').html($positions);
        },
        _updateClientTariffClasses: function (clientTariffClasses) {
            var $view_orders_list = $();
            var $view_orders_item, $view_orders_label, $view_orders_input;

            $(this.defaultViewOrderSelector).val(null);

            for (var class_id in clientTariffClasses) {
                $view_orders_item = $('<li/>');
                $view_orders_label = $('<label/>')
                    .attr('for', 'view_orders_list_' + class_id)
                    .text(' ' + clientTariffClasses[class_id]);
                $view_orders_input = $('<input type="checkbox" name="' + this.modelName + '[view_orders_list][]"/>')
                    .attr('id', 'view_orders_list_' + class_id)
                    .val(class_id);

                $view_orders_list = $view_orders_list.add(
                    $view_orders_item.append([$view_orders_input, $view_orders_label]));
            }

            $('#view_orders_list').closest('.row').toggle($view_orders_list.length > 0);
            $('#view_orders_list').html($view_orders_list);
        },
        _updateCarClassList: function (classMap) {
            var $classes = $();
            var class_select = $(this.classIdSelector);

            class_select.find('option:not(":first")').remove();

            if (!$.isEmptyObject(classMap)) {
                for (var class_id in classMap) {
                    $classes = $classes.add($('<option/>').val(class_id).text(classMap[class_id]));
                }

                class_select.append($classes);
            }

            class_select.trigger('update');
        },
        _ajax: function (params) {
            $.ajax({
                type: params.type,
                url: params.url,
                data: params.data,
                dataType: 'json',
                beforeSend: function () {
                    $('.submit_form input').addClass('loading_button');
                },
                success: params.success,
                complete: function () {
                    $('.submit_form input').removeClass('loading_button');
                }
            });
        },
        _defaultCheckShowingOrder: function (class_id) {
            var view_order_list = $('#view_orders_list li input');
            var required_view_order = false;
            var _this = this;
            view_order_list.each(function () {
                if ($(this).val() == class_id) {
                    $(this).prop({checked: true, 'disabled': true});
                    $(_this.defaultViewOrderSelector).val($(this).val());
                    required_view_order = true;
                } else {
                    $(this).prop({checked: false, 'disabled': false});
                }
            });

            if (!required_view_order) {
                $(this.defaultViewOrderSelector).val('');
            }
        },
        loadingButtonEvent: function () {
            $('.loading_button').click(function (e) {
                e.preventDefault();
                return false;
            });
        },
        cityListChangingEvent: function () {
            $(this.cityListSelector).on('change', function () {
                $.get(
                    '/employee/position/get-positions',
                    {city_id: $(this).val()},
                    function (json) {
                        if ($.isArray(json)) {
                            var select = $('#workergroup-position_id');
                            select.find('option:not(":first")').remove();
                            var options = $();

                            for (var i = 0; i < json.length; i++) {
                                options = options.add($('<option/>').val(json[i]['position_id']).text(json[i]['name']));
                            }

                            select.append(options);

                            select.trigger('update');
                        }
                    },
                    'json'
                );
            });
        },
        positionListChangingEvent: function () {
            var _this = this;
            $(this.positionIdSectionSelector).on('positionChange', function (event, position_id, hasCar) {
                _this._updatePositionChangedData(position_id, hasCar);
            });
        },
        _updatePositionChangedData: function (position_id, hasCar) {
            var _this = this;
            this._ajax({
                type: 'GET',
                url: this.positionChangingAction,
                data: {cityList: _this._getCityList(), positionId: position_id, hasCar: Number(hasCar)},
                success: function (json) {
                    _this._updateClientTariffClasses(json.clientTariffClasses);
                    $(_this.tariffListSelector).empty();

                    if (!$.isEmptyObject(json.workerTariffs)) {
                        _this._updateWorkerTariffList(json.workerTariffs);
                    }

                    var positions = json.positionsForExtendsGroup || [];
                    _this._updatePositionsForExtendsGroup(positions);

                    var classMap = json.classMap || [];
                    _this._updateCarClassList(classMap);
                }
            });
        },
        carClassListChangingEvent: function () {
            var _this = this;
            $(this.carClassIdSectionSelector).on('classChange', function (event, class_id) {
                _this._ajax({
                    type: 'POST',
                    url: _this.classListChangingAction,
                    data: {city_list: _this._getCityList(), class_id: class_id},
                    success: function (json) {
                        _this._updateWorkerTariffList(json);
                        _this._defaultCheckShowingOrder(class_id);
                    }
                });
            });
        }
    };

    app.init();
})();