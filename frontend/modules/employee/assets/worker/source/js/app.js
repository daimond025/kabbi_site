;(function () {
    'use strict';

    var app = {
        init: function () {
            createCheckboxStringOnReady();
            phone_mask_init();

            this.registerCheckListEvent();
            this.registerFormSubmitEvent();
            this.registerEditReviewActive();
        },

        registerCheckListEvent: function () {
            var _this = this;
            $('#worker-form .a_sc').on('click', function (event) {
                showStyleFilter($(this), event);
                createCheckboxStringOnClick();
                _this.setErrorText(false);
            });
        },

        registerFormSubmitEvent: function () {
            var _this = this;

            $('#worker-form').on('afterValidate', function () {
                var $form = $(this);

                var $cities = $form.find('[name="Worker[cityIds][]"]')
                    .filter(function (index, elem) {
                        return $(elem).prop('checked');
                    });

                if (!$cities.length) {
                    _this.setErrorText($form.find('.field-worker-cityids').data('required-message'));
                    return false;
                }
            });

            window.submitted = false;

            $('#worker-form').on('beforeSubmit', function () {
                if (window.submitted) {
                    return false
                }
                $('#worker-form').find('input[type="submit"]').attr('disabled', 'disabled');
                window.submitted = true;
            });
        },

        setErrorText: function (text) {
            var $helpBlock = $('.field-worker-cityids .help-block-error');
            $helpBlock.text(text);

            if (text) {
                $helpBlock.show();
            } else {
                $helpBlock.hide();
            }
        },


        registerEditReviewActive: function () {
            $(document).on('click', '.js-active-review', function () {
                var url = $(this).data('url');
                var _this = this;
                $.get(url).done(function (response) {
                    if (response) {
                        $.pjax.reload({
                            container: "#pjax-reviews-list",
                            url: $(_this).data('reload'),
                            push: false,
                            timeout: 5000,
                        });
                    }
                });
            });

            $(document).on('click', '.js-not-active-review', function () {
                var url = $(this).data('url');
                var _this = this;
                $.get(url).done(function (response) {
                    if (response) {
                        $.pjax.reload({
                            container: "#pjax-reviews-list",
                            url: $(_this).data('reload'),
                            push: false,
                            timeout: 5000
                        });
                    }
                });
            });
        }
    };

    app.init();
})();