<?php

namespace frontend\modules\employee\assets\worker;

use yii\web\AssetBundle;


class WorkerAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/employee/assets/worker/source';

    public $publishOptions = [
        'forceCopy' => true
    ];

    public $css = [
    ];
    public $js = [
        'js/app.js'
    ];
    public $depends = [
    ];
}