<?php


namespace frontend\modules\employee\controllers;

use common\modules\employee\components\document\DocumentLayer as DocumentLayerBase;
use common\modules\employee\components\position\PositionFieldLayer;
use common\modules\employee\components\worker\WorkerHasPositionLayer;
use common\modules\employee\components\worker\WorkerPositionFieldValueLayer;
use common\modules\employee\models\worker\WorkerHasPosition;
use frontend\modules\employee\components\document\DocumentLayer;
use frontend\modules\employee\components\groups\GroupRepository;
use frontend\modules\employee\components\groups\GroupsLayer;
use frontend\modules\employee\components\position\PositionService;
use frontend\modules\employee\components\worker\WorkerHasDocumentService;
use frontend\modules\tenant\components\services\FieldService;
use Yii;
use yii\base\Model;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;

class PositionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class'  => AccessControl::className(),
                'except' => [],
                'rules'  => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'create' => ['POST'],
//                    'update' => ['POST'],
                ],
            ],
        ];
    }

    public function actionLinkPosition()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $workerId = post('worker_id');
        $moduleId = post('module_id');
        $pjaxId   = post('pjax_id');

        if (empty($moduleId) || empty($workerId)) {
            throw new BadRequestHttpException('Empty entry params');
        }

        $error = null;
        $html = null;

        /** @var PositionService $positionService */
        $positionService = $this->module->get('position');
        $positionIds     = $positionService->getPositionIdsByModule($moduleId);

        $fieldService  = new FieldService();
        $showFieldList = $fieldService->getFields('worker');

        /** @var WorkerHasPositionLayer $workerHasPositionService */
        $workerHasPositionService = $this->module->get('workerHasPosition');
        $workerPositions          = ArrayHelper::getColumn($workerHasPositionService->getWorkerPositionsData($workerId),
            'position_id');
        $positionIds              = array_diff($positionIds, $workerPositions);

        if (!$positionIds) {
            return ['error' => [t('employee', 'The position has already added')]];
        }

        $positionId        = reset($positionIds);
        $workerHasPosition = new WorkerHasPosition(['worker_id' => $workerId, 'position_id' => $positionId]);
        if ($workerHasPosition->save()) {
            $position = $positionService->getPositionById($positionId, ['fields', 'documents']);

            //Getting document map ["code" => "AR document model"]
            $documentMap = DocumentLayer::createDocumentMapByObjects($position->documents);

            //Creating the documents. Documents must be created for possibility scan upload.
            $documents = DocumentLayerBase::createDocumentObjectsByCode(array_keys($documentMap));
            DocumentLayer::saveData($documents, $workerId, $workerHasPosition->id);
            $workerDocumentMap = ArrayHelper::map($documents, 'document_code', 'worker_document_id');

            foreach ($documentMap as $documentId => $document) {
                $name = $fieldService->getWorkerFieldByDocument($documentId);

                if (!ArrayHelper::getValue($showFieldList, "{$moduleId}.$name", true)) {
                    unset($documentMap[$documentId]);
                }
            }

            //Getting field map [field_id => ['name' => $name, 'type' => $type]]
            $fieldMap = PositionFieldLayer::getFieldMap($position->fields);

            if (!$position->has_car) {
                //Map of worker class for current position
                $positionClassMap = $positionService->getPositionMapByModule($moduleId);

                //Map of worker groups for current position
                if (!empty($positionClassMap)) {
                    $groupList = [];
                } else {
                    $groupList = $this->module->get('groups')->getRepository()->getList(['position_id' => $positionId]);
                }

                $positionGroupMap = $this->module->get('groups')->getMapGroup($groupList);
            }

            $module = $position->module;
            $moduleMap = [$module->id => t('gootax_modules', $module->name)];


            Yii::$app->get('orderApi')->workerPromoRegistration($workerId);


            $html = $this->renderAjax('index', [
                'position'            => $position,
                'workerHasPositionId' => $workerHasPosition->id,
                'documentMap'         => $documentMap,
                'fieldMap'            => $fieldMap,
                'worker_id'           => $workerId,
                'actionUrl'           => '/employee/position/update',
                'active'              => true,
                'positionClassMap'    => $positionClassMap,
                'positionGroupMap'    => $positionGroupMap,
                'workerDocumentMap'   => $workerDocumentMap,
                'scanDocumentMap'     => [],
                'moduleMap'           => $moduleMap,
                'showFieldList'       => $showFieldList,
                'pjaxId'              => $pjaxId
            ]);
        } else {
            $error = array_values($workerHasPosition->getFirstErrors());
        }

        return [
            'html'  => $html,
            'error' => $error,
        ];
    }

    /**
     * @param array $documentMap
     * @param integer $worker_id
     * @param integer $workerHasPositionId
     * @param array $fieldModels
     * @return bool
     */
    private function savePositionData($documentMap, $worker_id, $workerHasPositionId, $fieldModels)
    {
        $transaction = app()->db->beginTransaction();
        $isSaved = DocumentLayer::saveData($documentMap, $worker_id, $workerHasPositionId);
        $isSaved = WorkerPositionFieldValueLayer::saveData($fieldModels) && $isSaved;

        if ($isSaved) {
            $transaction->commit();

            return true;
        }

        $transaction->rollback();

        return false;
    }

    /**
     * @param integer $position_id
     * @param integer $worker_id
     * @return bool|string
     */
    public function actionUpdate($position_id, $worker_id, $pjax_id)
    {
        if (!Yii::$app->request->isPjax) {
            return $this->redirect(['/employee/worker/update', 'id' => $worker_id, '#' => 'job']);
        }

        /** @var WorkerHasPositionLayer $workerHasPositionService */
        $workerHasPositionService = $this->module->get('workerHasPosition');
        $workerHasPosition = $workerHasPositionService->getWorkerPositionData($worker_id, $position_id);

        if (empty($workerHasPosition)) {
            return false;
        }

        $workerHasPositionId = $workerHasPositionService->getWorkerHasPositionId(compact('position_id',
            'worker_id'));

        $post = post();
        $isSaved = false;

        //Documents
        if (!empty($workerHasPosition->documents)) {
            $codeMap = ArrayHelper::map($workerHasPosition->documents, 'id', 'document.code');
            $positionDocumentCodeList = DocumentLayer::getDocumentCodeListByPosition($position_id);

            //Unlinked some document
            $oldDocumentCodes = array_diff($codeMap, $positionDocumentCodeList);
            if (!empty($oldDocumentCodes)) {
                $workerDocuments = DocumentLayer::cleanOldDocuments($workerHasPosition->documents, $oldDocumentCodes);
                $newDocumentCodeMap = ArrayHelper::map($workerDocuments, 'id', 'document.code');
                $codeMap = array_intersect_assoc($codeMap, $newDocumentCodeMap);
            }

            $documentMap = DocumentLayer::getDocumentMapByCodeMap($codeMap, $post);

            /** @var  $workerHasDocumentService WorkerHasDocumentService */
            $workerHasDocumentService = $this->module->get('workerHasDocument');
            $workerDocumentMap = $workerHasDocumentService->getWorkerDocumentMap($workerHasPosition->documents);
            $scanDocumentMap = $workerHasDocumentService->getScanDocumentMap($workerHasPosition->documents);
        } else {
            $documents = post('Documents');
            $documentMap = $documents ? DocumentLayer::createDocumentMapByCodes($documents, $post) : [];
            $workerDocumentMap = [];
            $scanDocumentMap = [];
        }

        $isValid = Model::validateMultiple($documentMap);

        //Fields
        /** @var array $fieldMap Set of fields for current position */
        /** @var PositionService $positionService */
        $positionService = $this->module->get('position');
        $position = $positionService->getPositionById($position_id, ['fields', 'documents']);
        $fieldMap = PositionFieldLayer::getFieldMapByPosition($position);

        if (!empty($workerHasPosition->workerPositionFieldValues)) {
            $fieldModels = $workerHasPosition->workerPositionFieldValues;
        } else {
            $fieldModels = WorkerPositionFieldValueLayer::createWorkerFieldValueModels(count($fieldMap));
        }

        Model::loadMultiple($fieldModels, $post);
        $isValid = Model::validateMultiple($fieldModels) && $isValid;

        if ($isValid) {
            $isSaved = $this->savePositionData($documentMap, $worker_id, $workerHasPositionId, $fieldModels);
        }

        return $this->render('_formData', [
            'documentMap'         => $documentMap,
            'fieldMap'            => $fieldMap,
            'fieldModels'         => $fieldModels,
            'isSaved'             => $isSaved,
            'worker_id'           => $worker_id,
            'workerHasPositionId' => $workerHasPositionId,
            'workerDocumentMap'   => $workerDocumentMap,
            'scanDocumentMap'     => $scanDocumentMap,
            'position_id'         => $position_id,
            'actionUrl'           => '/employee/position/update',
            'pjaxId'              => $pjax_id,
        ]);
    }

    /**
     * @param integer $worker_id
     * @param integer $position_id
     * @param integer $active 1|0
     * @return bool
     */
    public function actionChangeActivity($worker_id, $position_id, $active)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        return $this->module->get('workerHasPosition')->changeData($worker_id, $position_id, ['active' => $active]);
    }

    public function actionChangePositionClass($worker_id, $position_id, $new_position_id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        /** @var WorkerHasPositionLayer $workerHasPositionService */
        $workerHasPositionService = $this->module->get('workerHasPosition');

        return $workerHasPositionService->changeData($worker_id, $position_id,
            ['position_id' => $new_position_id, 'group_id' => null]);
    }

    public function actionGetGroupList($position_id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        /** @var GroupsLayer $groupService */
        $groupService = $this->module->get('groups');
        /** @var GroupRepository $groupRepository */
        $groupRepository = $groupService->getRepository();

        $groupList = $groupRepository->getList(compact('position_id'));

        return $groupService->getMapGroup($groupList);
    }

    public function actionChangeGroup($worker_id, $position_id, $group_id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        return $this->module->get('workerHasPosition')->changeData($worker_id, $position_id, ['group_id' => $group_id]);
    }

    public function actionGetPositions($city_id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var PositionService $positionService */
        $positionService = $this->module->get('position');
        $positionService->setTenantId(user()->tenant_id);

        return $positionService->getPositionsByCity($city_id);
    }
}