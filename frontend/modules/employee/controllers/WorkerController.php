<?php

namespace frontend\modules\employee\controllers;

use common\modules\employee\components\document\DocumentLayer as DocumentLayerBase;
use common\modules\city\models\City;
use app\modules\tenant\models\field\FieldRecord;
use app\modules\tenant\models\User;
use common\modules\car\helpers\CarClassHelper;
use common\modules\employee\models\worker\WorkerHasCar;
use frontend\components\serviceEngine\ServiceEngine;
use common\helpers\DateTimeHelper;
use common\modules\employee\components\position\PositionFieldLayer;
use common\modules\employee\components\worker\WorkerHasPositionLayer;
use common\modules\employee\models\documents\Document;
use common\modules\employee\models\documents\WorkerInn;
use common\modules\employee\models\documents\WorkerOgrnip;
use common\modules\employee\models\documents\WorkerPassport;
use common\modules\employee\models\documents\WorkerSnils;
use common\modules\employee\models\worker\WorkerBlock;
use common\modules\employee\models\worker\WorkerHasPosition;
use common\modules\tenant\models\DefaultSettings;
use common\modules\tenant\models\TenantSetting;
use frontend\components\behavior\CityListBehavior;
use frontend\modules\car\models\Car;
use frontend\modules\car\models\CarDriverGroupHasTariff;
use frontend\modules\companies\components\repositories\TenantCompanyRepository;
use frontend\modules\companies\components\services\CompanyCheck;
use frontend\modules\employee\components\document\DocumentLayer;
use frontend\modules\employee\components\document\WorkerDocumentFactory;
use frontend\modules\employee\components\groups\GroupRepository;
use frontend\modules\employee\components\groups\GroupsLayer;
use frontend\modules\employee\components\position\PositionService;
use frontend\modules\employee\components\review\ReviewService;
use frontend\modules\employee\components\tariff\TaxiTariffService;
use frontend\modules\employee\components\worker\WorkerHasDocumentService;
use frontend\modules\employee\components\worker\WorkerService;
use frontend\modules\employee\components\worker\WorkerShiftService;
use frontend\modules\employee\helpers\WorkerGroupHelper;
use frontend\modules\employee\models\documents\WorkerDocumentScan;
use frontend\modules\employee\models\tariff\TariffHasCity;
use frontend\modules\employee\models\worker\Worker;
use frontend\modules\employee\models\worker\WorkerHasDocument;
use frontend\modules\employee\models\worker\WorkerSearch;
use frontend\modules\tenant\components\services\FieldService;
use frontend\modules\employee\models\groups\WorkerGroup;
use app\modules\tariff\models\TaxiTariff;
use frontend\modules\car\models\CarDriverGroupHasClass;
use Yii;
use yii\base\Exception;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use frontend\modules\car\models\CarSearch;

/**
 * WorkerController implements the CRUD actions for Worker model.
 * @mixin CityListBehavior
 */
class WorkerController extends Controller
{
    public $defaultAction = 'list';
    public $defaultPosition = 1;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access'   => [
                'class'  => AccessControl::className(),
                'except' => [],
                'rules'  => [
                    [
                        'actions' => [
                            'list-not-activated',
                        ],
                        'allow'   => true,
                        'roles'   => [User::ROLE_STAFF_COMPANY],
                    ],
                    [
                        'actions' => [
                            'list',
                            'list-blocked',
                            'update',
                            'upload-logo',
                            'change-password',
                            'show-reviews',
                            'list-online-json',
                        ],
                        'allow'   => true,
                        'roles'   => ['read_drivers'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['drivers'],
                    ],
                ],
            ],
            'cityList' => [
                'class' => CityListBehavior::className(),
            ],
        ];
    }

    private function getListData($searchParams, $type = Worker::ACTIVE)
    {
        $cityList = $this->getUserCityList();

        $searchModel                  = new WorkerSearch();
        $searchModel->accessCityList  = array_keys($cityList);
        $searchModel->type            = $type;
        $searchModel->needSearchByCar = true;

        /** @var PositionService $positionService */
        $positionService = $this->module->get('position');
        $positionService->setTenantId(user()->tenant_id);
        $positionList     = $positionService->getPositionMapByCity($searchModel->accessCityList);
        $groupWorkersList = WorkerGroupHelper::createGroupWorkersMap();
        $carList          = CarClassHelper::createCarsMap();

        if (!empty($searchParams['WorkerSearch']['positionList'])) {
            $searchModel->positionActivity = 1;
        }

        $dataProvider = $searchModel->search($searchParams);
        $pjaxId       = 'worker_' . $type;

        $onlineWorkers = Worker::getOnlineWorkersId();


        if (app()->user->can(User::ROLE_STAFF_COMPANY)) {
            $notActivatedWorkers = Worker::getCountNotActivatedWorkers([
                'wc.city_id'          => $searchModel->accessCityList,
                't.tenant_company_id' => user()->tenant_company_id,
            ]);
        } else {
            $notActivatedWorkers = Worker::getNotActivatedWorkers($searchModel->accessCityList);
        }

        $notActivatedWorkers = $notActivatedWorkers ?: ''; // если 0 то заменяем на пустую строку

        $companyList = TenantCompanyRepository::selfCreate()->getForForm();

        return compact('searchModel', 'dataProvider', 'cityList', 'pjaxId', 'positionList', 'onlineWorkers',
            'notActivatedWorkers', 'type', 'groupWorkersList', 'carList', 'companyList');
    }

    /**
     * Lists all Worker models.
     * @return mixed
     */
    public function actionList()
    {
        return $this->render('index', $this->getListData(Yii::$app->request->queryParams));
    }

    public function actionWorkerBondCar(){

        $workerData = $this->getListData(Yii::$app->request->queryParams);
        $workerList = [];
        $workerCityList = [];
        if(isset($workerData['dataProvider'])){
            $workerList = $workerData['dataProvider']->query->all();
        }
        if(isset($workerData['cityList'])){
            $workerCityList = $workerData['cityList'];
        }

        $this->workerBondPosition($workerList);
        $workerData = $this->correctFormatWorker($workerList);
        $workerOutPut = isset($workerData['workerList']) ? $workerData['workerList'] : [];
        $carIdsSearch = isset($workerData['workerCarIds']) ? $workerData['workerCarIds'] : [];



        $queryArCarModels = Car::find()
            ->alias('c')
            ->where(['c.tenant_id' => user()->tenant_id, 'c.active' => 1])
            ->andFilterWhere(['not in','c.car_id',$carIdsSearch])
            ->with(['city', 'class'])
            ->joinWith([
                'city' => function ($query) {
                    $query->where([City::tableName() . '.city_id' => array_keys(user()->getUserCityList())]);
                },
            ], false)
            ->orderBy('name');

        if (app()->user->can(User::ROLE_STAFF_COMPANY)) {
            $queryArCarModels->joinWith([
                'tenantCompany tc' => function (ActiveQuery $subQuery) {
                    $subQuery->andFilterWhere(['tc.tenant_company_id' => user()->tenant_company_id]);
                },
            ], false);
        }
        $arCars = $this->correctFormatCar($queryArCarModels->all());

        return $this->render('WorkerBondCar', [
            'workerList' => $workerOutPut,
            'workerCityList' => $workerCityList,
            'carList' => $arCars,

        ]);
    }
    public function correctFormatWorker($workerList){
        $workerOutPut['workerList'] = [];
        $workerOutPut['workerCarIds'] = [];

        foreach ($workerList as $worker){
            if(!$worker->worker_id) continue;
            $worker_item['worker_id'] = ($worker->worker_id);
            $worker_item['callsign'] = $worker->callsign;
            $worker_item['last_name'] = $worker->last_name;
            $worker_item['name'] = $worker->name;
            $worker_item['second_name'] = $worker->second_name;
            $worker_item['email'] = $worker->email;

            $worker_item['cars'] = [];
            foreach ($worker->cars as $workerHasCar){
                $car = $workerHasCar->car;
                if(!$car) continue;
                $workerOutPut['workerCarIds'][] = $car->car_id;
                $worker_item['cars'][] = $car;
            }
            $worker_item['cars'] = $this->correctFormatCar($worker_item['cars']);
            $workerOutPut['workerList'][] = $worker_item;
        }
        return $workerOutPut;
    }
    public function correctFormatCar($carList){

        $carFormated = [];
        foreach ($carList as $car){
            $car_item['car_id'] = $car->car_id;
            $car_item['name'] = $car->name;
            $car_item['photo'] = $car->isFileExists($car->photo) ? $car->getPictureHtml($car->photo, false) : '';
            $car_item['gos_number'] = $car->gos_number;
            $car_item['raiting'] = $car->raiting;
            $car_item['class'] = $car->class['class'];
            $car_item['class_id'] =$car->class['class_id'];
            $car_item['group_id']  =  $car->group_id;
            $car_item['year'] = $car->year;
            $carFormated[] = $car_item;
        }
        return $carFormated;
    }
    // метод привязки водителя к позиции (создание документов по умолчанию)
    public function workerBondPosition($workerList){

        $WorkerBondPosition = [];
        foreach ($workerList as $worker){
           $workerId = isset( $worker->worker_id) ? $worker->worker_id :  null ;
           $moduleId = $this->defaultPosition;

           if (is_null($workerId)) {
               continue;
           }

            /** @var PositionService $positionService */
            $positionService = $this->module->get('position');
            $positionIds     = $positionService->getPositionIdsByModule($moduleId);


            /** @var WorkerHasPositionLayer $workerHasPositionService */
            $workerHasPositionService = $this->module->get('workerHasPosition');
            $workerPositions          = ArrayHelper::getColumn($workerHasPositionService->getWorkerPositionsData($workerId),'position_id');
            $positionIds              = array_diff($positionIds, $workerPositions);

            if (!$positionIds) {
                continue;
            }

            $fieldService  = new FieldService();
            $showFieldList = $fieldService->getFields('worker');

            $positionId        = reset($positionIds);
            $workerHasPosition = new WorkerHasPosition(['worker_id' => $workerId, 'position_id' => $positionId]);
            if ($workerHasPosition->save()) {  //

                $position = $positionService->getPositionById($positionId, ['fields', 'documents']);

                //Getting document map ["code" => "AR document model"]
                $documentMap = DocumentLayer::createDocumentMapByObjects($position->documents);

                //Creating the documents. Documents must be created for possibility scan upload.
                $documents = DocumentLayerBase::createDocumentObjectsByCode(array_keys($documentMap));

                DocumentLayer::saveData($documents, $workerId, $workerHasPosition->id);
            }
        }
    }

    // метод поиска водителей
    public function actionSearchWorker(){

        $query = Yii::$app->request->queryParams;
        $type = Worker::ACTIVE;
        $cityList = $this->getUserCityList();

        $searchModel                  = new WorkerSearch();
        $searchModel->accessCityList  = array_keys($cityList);
        $searchModel->type            = $type;
        $searchModel->needSearchByCar = true;

        if (!empty($query['WorkerSearch']['positionList'])) {
            $searchModel->positionActivity = 1;
        }

        if (empty($query['WorkerSearch']['stringSearch'])) {
            $query = [];
        }
        if (!empty($query['WorkerSearch']['positionList'])) {
            $searchModel->positionActivity = 1;
        }
        $dataProvider = $searchModel->search($query);

       $worker =   $dataProvider->query->asArray()->all();

       return(json_encode( $worker));
    }

    // метод поиска машин
    public function actionSearchCar(){
        $params = $query = Yii::$app->request->queryParams;
        $params['view'] = 'index';
        $params['input_search'] = isset($params['input_search']) ? $params['input_search'] : null ;

        $car_search   = new CarSearch();
        $dataProvider = $car_search->search($params);
        $arCars       = $car_search->getGridData($dataProvider->getModels(), false, false);
        return (json_encode( $arCars));
    }

    // метод привязки водеителй к машинам
    public function actionWorkerLinkCar(){
        Yii::$app->response->format = Response::FORMAT_JSON;

        $query = Yii::$app->request->queryParams;
        $worker_id = $query['worker_id'];
        $car_id = $query['car_id'];

        /// проверка того что машина куда то привызана
        $workerCarExist =  WorkerHasCar::find()
               ->where([
                   'car_id' => $car_id,
               ])
               ->select('car_id')
               ->scalar();

        $rezultLincCar['rezult'] = 0;
        if(!$workerCarExist){
            $entity_id =  \frontend\modules\employee\models\worker\WorkerHasPosition::find()
                ->where([
                    'worker_id' => $worker_id,
                ])
                ->select('id')
                ->one();

            if(isset($entity_id->id)){
                $rezultLincCar['rezult'] = $this->module->get('workerCar')->link($entity_id->id, $car_id);
            }
        }

        // данные для машины
        $rezultLincCar['cars_date'] =   $this->getAdditionalDateLinkWork($worker_id, $car_id);

        return  $rezultLincCar;
    }
    public function getAdditionalDateLinkWork($worker_id, $car_id){

        /** @var WorkerHasPositionLayer $workerHasPositionService */
        $workerHasPositionService = $this->module->get('workerHasPosition');
        $workerPositionsData      = $workerHasPositionService->getWorkerPositionsData($worker_id);

        $groupDasta           = [];
        if (is_array($workerPositionsData)) {
            /** @var $item WorkerHasPosition */
            foreach ($workerPositionsData as $item) {
                /** @var GroupsLayer $groupsService */
                $groupsService = $this->module->get('groups');
                /** @var GroupRepository $workerGroupRepository */
                $workerGroupRepository = $groupsService->getRepository();

                //Cars
                if ($item->position->has_car) {
                    $carsData = Car::getGridData($item->cars, false,
                        false, true);

                    // данные показываем только для текущей
                    $current_car = null;
                    foreach ($carsData['CARS'] as $key => $car){
                        if($car['ID'] == $car_id ){
                            $carsData['CARS'] = [];
                            $carsData['CARS'][] = $car; $current_car = $car; break;
                        }
                    }

                    $arClass = ArrayHelper::getColumn($carsData['CARS'], 'CLASS_ID');
                    $arClass = array_unique($arClass);

                    $worker = Worker::findOne($worker_id);
                    $workerCityIds = array_intersect(
                        $worker->getCityIds(),
                        array_keys(user()->getUserCityList())
                    );

                    //Driver groups with tariffs and orders for showing
                    $workerGroups = $workerGroupRepository->getGroupsByClass($arClass, $item->position_id,
                        $workerCityIds);
                    $groupData = [];

                    $groupMapGroupedByClass = ArrayHelper::map($workerGroups, 'group_id', 'name');

                    //Worker tariff map for current car class
                    $tariffs = $this->module->get('tariff')->getRepository()
                        ->getDriverTariffs($workerCityIds, $arClass, $item->position_id);

                    // tariff_html
                    $tariffMapGroupedByClass =  ArrayHelper::map($tariffs, 'tariff_id', 'name') ;

                    /** @var TaxiTariffService $clientTariffService */
                    $clientTariffService = $this->module->get('clientTariff');

                    //Order showing map for current worker position
                    $clientTariffs = $clientTariffService->getRepository()
                        ->getTariffsGroupByClass($workerCityIds, $item->position_id);

                    // class car
                    $showingClassesMap = $clientTariffService->getShowingClassesMap($clientTariffs);

                    $groupDasta['html'] = $this->renderPartial('worker_car_item', compact('current_car',
                        'groupMapGroupedByClass', 'tariffMapGroupedByClass', 'showingClassesMap', 'worker_id'  ));

                }
            }
        }

        return $groupDasta;
    }

    public function getAdditionalDateUnlinkWork( $car_id){
         $car =  Car::find()->where(['car_id' => $car_id ])->asArray()->one();
         $rezult = '';
         if($car){
             $rezult = $this->renderPartial('car_item', compact('car'));
         }
         return $rezult;
    }

    // метод отвязки
    public function actionWorkerUnlinkCar(){

        Yii::$app->response->format = Response::FORMAT_JSON;
        $query = Yii::$app->request->queryParams;
        $worker_id= $query['worker_id'];
        $car_id = $query['car_id'];

        $workerCarExist =  WorkerHasCar::find()
            ->where([
                'car_id' => $car_id,
            ])
            ->select('car_id')
            ->scalar();

        $rezultLincCar['rezult'] = 0;
        $rezultLincCar['html'] = $this->getAdditionalDateUnlinkWork($car_id);
        if($workerCarExist){

            $entity_id =  \frontend\modules\employee\models\worker\WorkerHasPosition::find()
                ->where([
                    'worker_id' => $worker_id,
                ])
                ->select('id')
                ->one();
            if(isset($entity_id->id)) {
                $rezultLincCar['rezult'] = (int)$this->module->get('workerCar')->unlink($entity_id->id, $car_id);
            }
        }



        return $rezultLincCar;

    }

    //изменение группы водителей
    public function actionAddDriverGroup()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $query = Yii::$app->request->queryParams;
        $group_id = $query['group_id'];
        $car_id = $query['car_id'];

        $car            = Car::find()->where(['car_id' => $car_id])->select(['car_id', 'group_id'])->one();
        $json['result'] = 0;
        $json['html'] = '';

        // id водителя
        $workerCarExist =  WorkerHasCar::find()
            ->where([
                'car_id' => $car_id,
            ])
            ->one();
        $worker_id = $workerCarExist->hasPosition->worker_id;

        //  группа тарифов водителя
        $driver_group              = WorkerGroup::find()
            ->where([
                'group_id'  => $group_id,
                'tenant_id' => user()->tenant_id,
            ])
            ->with([
                'workerGroupHasTariffs',
                'workerGroupCanViewClientTariffs',
            ])
            ->select(['group_id'])
            ->one();


        if(empty($worker_id) || !$driver_group){
            return $json;
        }

        /*$cars_tariff = [];
        foreach ($driver_group->workerGroupHasTariffs as $workerGrouptariff ){
            $tariff = $workerGrouptariff->tariff;
            $cars_tariff_item['id']=$tariff->tariff_id ;
            $cars_tariff_item['name']=$tariff->name ;
            $cars_tariff[] = $cars_tariff_item;
        }
        $cars_date  = $this->getAdditionalDateLinkWork($worker_id,$car->car_id );
        $json['group_tariffs']     = $cars_tariff;
        $json['group_classes']     = $cars_date['showingClassesMap'];*/

        if (!empty($car) && !empty($group_id) && !empty($worker_id) && $car->group_id != $group_id ) {
            $car->driver_group_tariffs = ArrayHelper::getColumn($driver_group->workerGroupHasTariffs, 'tariff_id');
            $car->driver_group_classes = ArrayHelper::getColumn($driver_group->workerGroupCanViewClientTariffs, 'class_id');
            $car->group_id  = $group_id;
            $json['result'] = (int)$car->save(false, ['group_id']);
            $json['html'] =   $this->getAdditionalDateLinkWork($worker_id, $car_id);
        }
        return $json;

    }

    // метод тарифа
    public function actionAddDriverGroupTariff()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $query = Yii::$app->request->queryParams;
        $tariffs =  isset($query['tariffs']) ? $query['tariffs'] : null ;
        if(!is_null($tariffs)){
            $tariffs = explode(',',$tariffs );
        }
        $car_id = $query['car_id'];

        // id водителя
        $workerCarExist =  WorkerHasCar::find()
            ->where([
                'car_id' => $car_id,
            ])
            ->one();
        $worker_id = $workerCarExist->hasPosition->worker_id;

        $json['rezult']           = 0;
       // $json['html'] = '';
        $arTariff       = getValue($tariffs, []);
        $city_ids       = array_keys(user()->getUserCityList());


        $old_tariff_ids = TariffHasCity::find()
            ->select('tariff_id')
            ->where(['city_id' => $city_ids])
            ->column();


        if (is_array($arTariff) && !empty($car_id)) {
            try {
                $res = CarDriverGroupHasTariff::updateRows($arTariff, $car_id, $old_tariff_ids);

                if ($res || (is_null($res) && (count($arTariff) === 0) )) {
                    $json['rezult']  = 1;
                    //$json['html'] =   $this->getAdditionalDateLinkWork($worker_id, $car_id);
                }
            } catch (\yii\db\Exception $exc) {
                Yii::error($exc->getMessage());
            }
        }

        return ($json);
    }

    // метод класса машин
    public function actionAddDriverGroupClass()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $query = Yii::$app->request->queryParams;
        $json['rezult']      = 0;
        $car_id    = $query['car_id'];
        $classes =  isset($query['classes']) ? $query['classes'] : null ;
        if(!is_null($classes)){
            $classes = explode(',',$classes );
        }
        $arClasses = getValue($classes, []);
        $tenant_id = user()->tenant_id;
        $city_ids  = array_keys(user()->getUserCityList());

        $old_class_ids = TaxiTariff::find()
            ->select('class_id')
            ->joinWith('tariffHasCity')
            ->where([
                'city_id'   => $city_ids,
                'tenant_id' => $tenant_id,
            ])
            ->column();


        $car = Car::find()->where(['car_id' => post('car_id')])->one();
        if (!empty($car) && !in_array($car->class_id, $arClasses)) {
            $arClasses[] = $car->class_id;
        }

        if (is_array($arClasses) && !empty($car_id)) {
            try {
                $res = CarDriverGroupHasClass::updateRows($arClasses, $car_id, $old_class_ids);
                if ($res || (is_null($res) && count($classes) == 0)) {
                    $json['rezult'] = 1;
                }
            } catch (\yii\db\Exception $exc) {
                Yii::error($exc->getMessage());
            }
        }

        return ($json);
    }




    /**
     * Lists all Worker models.
     * @return mixed
     */
    public function actionListBlocked()
    {
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_grid', $this->getListData(Yii::$app->request->queryParams, Worker::BLOCKED));
        }

        return $this->redirect(['list', '#' => 'blocked']);
    }

    /**
     * Lists all Worker models.
     * @return mixed
     */
    public function actionListNotActivated()
    {
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_grid',
                $this->getListData(Yii::$app->request->queryParams, Worker::NOT_ACTIVATED));
        }

        return $this->redirect(['list', '#' => 'notActivated']);
    }

    public function actionBlockedWorkers($id)
    {
        if (Yii::$app->request->isAjax) {
            $queryParams                                       = Yii::$app->request->queryParams;
            $queryParams['WorkerSearch']['clientIdHasBlocked'] = $id;

            return $this->renderAjax(
                '_grid',
                $this->getListData($queryParams)
            );
        }

        return $this->redirect(['/client/base/list', '#' => 'blocking']);
    }

    /**
     * Creates a new Worker model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        $availableCities = is_array(user()->getUserCityList())
            ? array_keys(user()->getUserCityList()) : [];

        $model = new Worker([
            'tenant_id'       => user()->tenant_id,
            //'callsign' => ' ' Браузер файрефокс не правильно принимает пустые поля, заполняет их данными логин-пароль.
            // Костылочек. В валидации эти пробелы убираю
            'callsign'        => ' ',
            'availableCities' => $availableCities,
        ]);

        $fieldService  = new FieldService();
        $showFieldList = $fieldService->getFields('worker');

        $passport = WorkerDocumentFactory::createDocument(Document::PASSPORT);
        $snils    = WorkerDocumentFactory::createDocument(Document::SNILS);
        $inn      = WorkerDocumentFactory::createDocument(Document::INN);
        $ogrnip   = WorkerDocumentFactory::createDocument(Document::OGRNIP);

        if (Yii::$app->request->isPost) {

            if (app()->user->can(User::ROLE_STAFF_COMPANY)) {
                $model->tenant_company_id = user()->tenant_company_id;
            }

            $res = $this->dataSave($model, $passport, $snils, $inn, $ogrnip, true);

            if ($res) {
                session()->setFlash('success', t('app', 'It has added successfully.'));

                return $this->redirect(['update', 'id' => $model->worker_id]);
            }
        }

        $formData = $this->getFormData();

        return $this->render('create', compact('model', 'formData', 'showFieldList'));
    }

    /**
     * Saving worker and his document.
     *
     * @param Worker         $model
     * @param WorkerPassport $passport
     * @param WorkerSnils    $snils
     * @param WorkerInn      $inn
     * @param WorkerOgrnip   $ogrnip
     * @param boolean        $optionalDocs
     *
     * @return bool
     */
    private function dataSave(
        Worker $model,
        WorkerPassport $passport,
        WorkerSnils $snils,
        WorkerInn $inn,
        WorkerOgrnip $ogrnip,
        $optionalDocs = false
    ) {
        $post = Yii::$app->request->post();

        $transaction = app()->db->beginTransaction();

        try {
            if ($model->load($post) && $model->save()) {

                if ($optionalDocs || $passport->load($post)) {
                    $passport->worker_id = $model->worker_id;
                    if (!$passport->save()) {
                        $transaction->rollback();

                        return false;
                    }
                }

                if ($optionalDocs || $snils->load($post)) {
                    $snils->worker_id = $model->worker_id;
                    if (!$snils->save()) {
                        $transaction->rollback();

                        return false;
                    }
                }

                if ($optionalDocs || $inn->load($post)) {
                    $inn->worker_id = $model->worker_id;
                    if (!$inn->save()) {
                        $transaction->rollback();

                        return false;
                    }
                }

                if ($optionalDocs || $ogrnip->load($post)) {
                    $ogrnip->worker_id = $model->worker_id;
                    if (!$ogrnip->save()) {
                        $transaction->rollback();

                        return false;
                    }
                }

                $transaction->commit();

                return true;

            }
        } catch (Exception $e) {
            Yii::error($e->getMessage());
            session()->setFlash('error', t('error', 'Error saving. Notify to administrator, please.'));
        }

        $transaction->rollBack();

        return false;
    }

    /**
     * Getting data for model form.
     * @return array
     */
    protected function getFormData()
    {
        return [
            'city_list'   => user()->getUserCityList(),
            'birth_day'   => DateTimeHelper::getMonthDayNumbers(),
            'birth_month' => DateTimeHelper::getMonthList(),
            'birth_year'  => DateTimeHelper::getOldYearList(14),
            'companies'   => TenantCompanyRepository::selfCreate()->getForForm([], false),
        ];
    }

    /**
     * Updates an existing Worker model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (app()->user->can(User::ROLE_STAFF_COMPANY)) {
            if (!app()->user->can(User::ROLE_STAFF_COMPANY, ['model' => $model])) {
                throw new ForbiddenHttpException(t('tenant_company', 'Access denied.'));
            }
        }

        $passport = $model->workerHasDocuments[Document::PASSPORT]->passport ?: WorkerDocumentFactory::createDocument(Document::PASSPORT);
        $snils    = $model->workerHasDocuments[Document::SNILS]->snils ?: WorkerDocumentFactory::createDocument(Document::SNILS);
        $inn      = $model->workerHasDocuments[Document::INN]->inn ?: WorkerDocumentFactory::createDocument(Document::INN);
        $ogrnip   = $model->workerHasDocuments[Document::OGRNIP]->ogrnip ?: WorkerDocumentFactory::createDocument(Document::OGRNIP);

        $model->availableCities = is_array(user()->getUserCityList())
            ? array_keys(user()->getUserCityList()) : [];

        if (Yii::$app->request->isPost) {
            $res = $this->dataSave($model, $passport, $snils, $inn, $ogrnip);

            if ($res) {
                session()->setFlash('success', t('app', 'It has updated successfully.'));

                return $this->refresh();
            }
        }

        $formData = $this->getFormData();

        $fieldService  = new FieldService();
        $showFieldList = $fieldService->getFields('worker');

        $workerDocumentMap = $this->getWorkerDocumentMap($model->workerHasDocuments);
        $scanDocumentMap   = $this->getScanDocumentMap($model->workerHasDocuments);

        $documentMap = [];
        if ($showFieldList['worker'][FieldRecord::NAME_WORKER_PASSPORT]) {
            $documentMap[Document::PASSPORT] = $passport;
        }
        if ($showFieldList['worker'][FieldRecord::NAME_WORKER_SNILS]) {
            $documentMap[Document::SNILS] = $snils;
        }
        if ($showFieldList['worker'][FieldRecord::NAME_WORKER_INN]) {
            $documentMap[Document::INN] = $inn;
        }
        if ($showFieldList['worker'][FieldRecord::NAME_WORKER_OGRNIP]) {
            $documentMap[Document::OGRNIP] = $ogrnip;
        }

        return $this->render('update',
            compact('model', 'formData', 'workerDocumentMap', 'scanDocumentMap',
                'documentMap', 'showFieldList'));
    }

    /**
     * @param $workerHasDocuments WorkerHasDocument[]
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    private function getWorkerDocumentMap($workerHasDocuments)
    {
        /** @var  $workerHasDocumentService WorkerHasDocumentService */
        $workerHasDocumentService = $this->module->get('workerHasDocument');

        return $workerHasDocumentService->getWorkerDocumentMap($workerHasDocuments);
    }

    /**
     * @param $workerHasDocuments WorkerHasDocument[]
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    private function getScanDocumentMap($workerHasDocuments)
    {
        /** @var  $workerHasDocumentService WorkerHasDocumentService */
        $workerHasDocumentService = $this->module->get('workerHasDocument');

        return $workerHasDocumentService->getScanDocumentMap($workerHasDocuments);
    }

    /**
     * @param int $entity_id Worker id
     *
     * @return array|bool
     * @throws NotFoundHttpException
     */
    public function actionUploadLogo($entity_id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $worker = $this->findOneWorker($entity_id);
        $worker->load(post());

        if ($worker->save(true, ['photo'])) {
            return [
                'src' => [
                    'big'   => $worker->getPicturePath($worker->photo),
                    'thumb' => $worker->getPicturePath($worker->photo, false),
                ],
            ];
        }

        return $worker->getFirstErrors();
    }

    /**
     * @param integer $id Primery key
     *
     * @return WorkerDocumentScan|null
     */
    protected function getWorkerScan($id)
    {
        /** @var WorkerDocumentScan|null $res */
        $res = WorkerDocumentScan::find()
            ->where(['scan_id' => $id])
            ->joinWith([
                'workerDocument.worker' => function (ActiveQuery $query) {
                    return $query->where(['tenant_id' => user()->tenant_id]);
                },
            ], false)
            ->one();

        return $res;
    }

    /**
     * @param int $id scan_id
     *
     * @return WorkerDocumentScan|bool
     * @throws \Exception
     */
    public function actionScanRemove($id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $scan = $this->getWorkerScan($id);

        if ($scan) {
            return $scan->delete();
        }

        return $scan;
    }

    public function actionScanUpload($has_document_id = null, $entityId = null, $code = null, $multiple = false)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        if (empty($has_document_id)) {
            /** @var  $workerHasDocumentService WorkerHasDocumentService */
            $workerHasDocumentService = $this->module->get('workerHasDocument');
            $workerHasDocument        = $workerHasDocumentService->createWorkerHasDocumentByCode($entityId, $code);

            if (!is_object($workerHasDocument)) {
                return $workerHasDocument;
            }

            $has_document_id = $workerHasDocument->id;
        }

        $documentScan = $this->getDocumentScanModel($has_document_id, $multiple);

        if ($documentScan->save()) {
            return [
                'src' => [
                    'big'   => $documentScan->getPicturePath($documentScan->filename),
                    'thumb' => $documentScan->getPicturePath($documentScan->filename, false),
                ],
                'id'  => $documentScan->scan_id,
            ];
        }

        return $documentScan->getFirstErrors();
    }

    private function getDocumentScanModel($worker_document_id, $multiple = false)
    {
        if (!$multiple) {
            $documentScan = WorkerDocumentScan::findOne(['worker_document_id' => $worker_document_id]);
            if ($documentScan) {
                return $documentScan;
            }
        }

        return (new WorkerDocumentScan(['worker_document_id' => $worker_document_id]));
    }

    /**
     * @param $worker_id
     *
     * @return Worker
     * @throws NotFoundHttpException
     */
    private function findOneWorker($worker_id)
    {
        $model = Worker::findOne(['worker_id' => $worker_id, 'tenant_id' => user()->tenant_id]);

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Changing worker password
     *
     * @param integer $id Worker Id
     *
     * @return mixed
     */
    public function actionChangePassword($id)
    {
        $worker           = $this->findOneWorker($id);
        $worker->scenario = Worker::SCENARIO_CALLSIGN;

        if ($worker->load(post())) {
            if ($worker->validate(['password', 'callsign'])) {
                $worker->password = Yii::$app->security->generatePasswordHash($worker->password);
                $worker->save(false, ['password', 'callsign']);
                session()->setFlash('success', t('app', 'Data updated successfully'));
            } else {
                displayErrors([$worker]);
            }

            return $this->redirect(['update', 'id' => $id, '#' => 'callsign_and_password']);
        }

        $form = app()->user->can('drivers') ? '' : '_read';

        return $this->renderAjax('_callsign' . $form, [
            'worker' => $worker,
        ]);
    }

    /**
     * Job tab
     *
     * @param int $id Worker id
     *
     * @return mixed
     */
    public function actionJob($id)
    {
        /** @var WorkerService $workerService */
        $workerService = $this->module->get('worker');
        $workerCityIds = $workerService->getCityIds($id);


        $fieldService  = new FieldService();
        $showFieldList = $fieldService->getFields('worker');

        /** @var PositionService $positionService */
        $positionService = $this->module->get('position');
        $positionService->setTenantId(user()->tenant_id);
        $moduleMap = $positionService->getModuleMap($workerCityIds, false);


        /** @var WorkerHasPositionLayer $workerHasPositionService */
        $workerHasPositionService = $this->module->get('workerHasPosition');
        $workerPositionsData      = $workerHasPositionService->getWorkerPositionsData($id);

        $arPositionData           = [];
        if (is_array($workerPositionsData)) {
            /** @var $item WorkerHasPosition */
            foreach ($workerPositionsData as $item) {
                $arPositionData[$item->position_id]['position']            = $item->position;
                $arPositionData[$item->position_id]['workerHasPositionId'] = $item->id;
                $arPositionData[$item->position_id]['active']              = $item->active;
                $arPositionData[$item->position_id]['groupId']             = $item->group_id;

                /** @var GroupsLayer $groupsService */
                $groupsService = $this->module->get('groups');
                /** @var GroupRepository $workerGroupRepository */
                $workerGroupRepository = $groupsService->getRepository();


                //Cars
                if ($item->position->has_car) {
                    $carsData       = Car::getGridData($item->cars, false,
                        false, true);

                    $arPositionData[$item->position_id]['cars']['car_data'] = $carsData['CARS'];

                    $arClass = ArrayHelper::getColumn($carsData['CARS'], 'CLASS_ID');
                    $arClass = array_unique($arClass);

                    $worker        = Worker::findOne($id);
                    $workerCityIds = array_intersect(
                        $worker->getCityIds(),
                        array_keys(user()->getUserCityList())
                    );

                    //Driver groups with tariffs and orders for showing
                    $workerGroups = $workerGroupRepository->getGroupsByClass($arClass, $item->position_id,
                        $workerCityIds);
                    $groupData = [];

                    //Group map is grouped by class
                    $groupData['groupMapGroupedByClass'] = $groupsService->getMapGroup($workerGroups, 'class_id');


                    //Worker tariff map for current car class
                    $tariffs = $this->module->get('tariff')->getRepository()
                        ->getDriverTariffs($workerCityIds, $arClass, $item->position_id);

                    $groupData['tariffMapGroupedByClass'] = $this->module->get('tariff')->getTariffMapGroupByClass($tariffs);


                    /** @var TaxiTariffService $clientTariffService */
                    $clientTariffService = $this->module->get('clientTariff');

                    //Order showing map for current worker position
                    $clientTariffs = $clientTariffService->getRepository()
                        ->getTariffsGroupByClass($workerCityIds, $item->position_id);

                    $groupData['showingClassesMap']                           = $clientTariffService->getShowingClassesMap($clientTariffs);
                    $arPositionData[$item->position_id]['cars']['group_data'] = $groupData;
                } else {
                    $arPositionData[$item->position_id]['positionClassMap'] = $positionService
                        ->getPositionMapByModule($item->position['module_id']);

                    $groupList = $workerGroupRepository->getList(['position_id' => $item->position_id]);

                    $arPositionData[$item->position_id]['positionGroupMap'] = $groupsService->getMapGroup($groupList);
                }

                //Documents
                if (!empty($item->documents)) {
                    $workerDocuments      = $item->documents;
                    $documentCodeList     = $this->getDocumentCodeList($item->position->documents);
                    $codeMap              = ArrayHelper::map($workerDocuments, 'id', 'document.code');
                    $newDocumentCodes     = array_diff($documentCodeList, $codeMap);
                    $documents            = [];
                    $newWorkerDocumentMap = [];

                    //New document was linked to position
                    if (!empty($newDocumentCodes)) {
                        $documents = DocumentLayer::createDocumentObjectsByCode($newDocumentCodes);
                        DocumentLayer::saveData($documents, $id, $item->id);

                        $newWorkerDocumentMap = ArrayHelper::map($documents, 'document_code', 'worker_document_id');
                    }

                    //Unlinked some document
                    $oldDocumentCodes = array_diff($codeMap, $documentCodeList);
                    if (!empty($oldDocumentCodes)) {
                        $workerDocuments    = DocumentLayer::cleanOldDocuments($workerDocuments, $oldDocumentCodes);
                        $newDocumentCodeMap = ArrayHelper::map($workerDocuments, 'id', 'document.code');
                        $codeMap            = array_intersect_assoc($codeMap, $newDocumentCodeMap);
                    }

                    $documentMapByCodeMap                              = DocumentLayer::getDocumentMapByCodeMap($codeMap);
                    $arPositionData[$item->position_id]['documentMap'] = ArrayHelper::merge($documentMapByCodeMap,
                        $documents);

                    foreach ($arPositionData[$item->position_id]['documentMap'] as $documentId => $document) {
                        $name = $fieldService->getWorkerFieldByDocument($documentId);

                        if (!ArrayHelper::getValue($showFieldList, "{$item->position_id}.$name", true)) {
                            unset($arPositionData[$item->position_id]['documentMap'][$documentId]);
                        }
                    }

                    $workerDocumentMap                                       = $this->getWorkerDocumentMap($workerDocuments);
                    $arPositionData[$item->position_id]['workerDocumentMap'] = ArrayHelper::merge($workerDocumentMap,
                        $newWorkerDocumentMap);
                    $arPositionData[$item->position_id]['scanDocumentMap']   = $this->getScanDocumentMap($workerDocuments);
                    $arPositionData[$item->position_id]['scanDocumentMap']   = $this->getScanDocumentMap($workerDocuments);
                } else {
                    //Creating the documents. Documents must be created for possibility scan upload.
                    $documentCodeList = $this->getDocumentCodeList($item->position->documents);
                    $documents        = DocumentLayer::createDocumentObjectsByCode($documentCodeList);
                    DocumentLayer::saveData($documents, $id, $item->id);
                    //--------------------------------------------------------------------------
                    $arPositionData[$item->position_id]['documentMap']       = DocumentLayer::createDocumentMapByObjects($item->position->documents);
                    $arPositionData[$item->position_id]['workerDocumentMap'] = ArrayHelper::map($documents,
                        'document_code', 'worker_document_id');
                    $arPositionData[$item->position_id]['scanDocumentMap']   = [];
                }

                //Fields
                $arPositionData[$item->position_id]['fieldMap']    = PositionFieldLayer::getFieldMapByPosition($item->position);
                $arPositionData[$item->position_id]['fieldModels'] = $item->workerPositionFieldValues;
            }
        }


        return $this->renderAjax('job', [
            'moduleMap'      => $moduleMap,
            'worker_id'      => $id,
            'arPositionData' => $arPositionData,
            'actionUrl'      => '/employee/position/update',
            'showFieldList'  => $showFieldList,
        ]);
    }

    private function getDocumentCodeList($documents)
    {
        return ArrayHelper::getColumn($documents, 'code');
    }

    /**
     * Finds the Worker model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Worker the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = Worker::find()
            ->where(['worker_id' => $id, 'tenant_id' => user()->tenant_id])
            ->with([
                'workerHasDocuments' => function ($query) {
                    $query->indexBy('document_id')
                        ->with(['documentScans'])
                        ->joinWith([
                            'document' => function ($subQuery) {
                                $subQuery->where([
                                    'code' => [
                                        Document::PASSPORT,
                                        Document::SNILS,
                                        Document::INN,
                                        Document::OGRNIP,
                                    ],
                                ]);
                            },
                        ]);
                },
            ])
            ->one();

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param int      $id
     * @param boolean  $notActive
     * @param null|int $position_id PK of tbl_worker_has_position
     *
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws Exception
     */
    public function actionShowReviews($id, $notActive = false, $position_id = null)
    {
        if (!app()->request->isAjax) {
            return $this->redirect(['update', 'id' => $id, '#' => 'feedback']);
        }

        (new CompanyCheck())->isAccess(Worker::findOne(['worker_id' => $id]));

        /** @var WorkerService $workerService */
        $workerService     = $this->module->get('worker');
        $workerPositionMap = $workerService->getWorkerPositionMap($id);
        $position_id       = $position_id ?: key($workerPositionMap);

        /** @var ReviewService $reviewService */
        $reviewService      = $this->module->get('reviews');
        $clientReviews      = $reviewService->getClientReviews(user()->tenant_id, $id, $position_id, $notActive);
        $workerReviewRating = $reviewService->getWorkerReviewRating($id, $position_id);

        if (empty($workerReviewRating) && !empty($clientReviews)) {
            $workerReviewRating = $reviewService->ratingInit($clientReviews, $id, $position_id);

            if (!is_object($workerReviewRating)) {
                throw new Exception('Error of worker init rating');
            }
        }

        return $this->renderAjax('review', [
            'clientReviews'      => $clientReviews,
            'workerReviewRating' => $workerReviewRating,
            'reviewService'      => $reviewService,
            'workerPositionMap'  => $workerPositionMap,
            'position_id'        => $position_id,
            'workerId'           => $id,
            'notActive'          => (boolean)$notActive,
        ]);
    }

    /**
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function actionEndWork()
    {
        app()->response->format = \yii\web\Response::FORMAT_JSON;

        $worker  = $this->getWorker(post('workerId'));
        $shiftId = post('shiftId');

        if (empty($worker) || empty($shiftId)) {
            return false;
        }

        /* @var $api ServiceEngine */
        $api = \Yii::createObject(ServiceEngine::className());

        return $api->closeWorkerShift(
            $worker->tenant_id,
            $worker->tenant->domain,
            $worker->callsign,
            user()->user_id,
            $shiftId);
    }

    /**
     * @return mixed
     */
    public function actionUnblock()
    {
        app()->response->format = \yii\web\Response::FORMAT_JSON;

        $worker = $this->getWorker(post('workerId'));

        $workerBlock = WorkerBlock::findone(post('blockId'));

        if (empty($worker) || empty($workerBlock)) {
            return false;
        }

        return app()->workerApi->workerUnblock(
            $worker->callsign,
            $worker->tenant->domain,
            $workerBlock->type_block,
            $workerBlock->block_id,
            TenantSetting::getSettingValue($worker->tenant->tenant_id, DefaultSettings::SETTING_WORKER_API_KEY));
    }

    /**
     * @param int $worker_id
     *
     * @return array|null|ActiveRecord
     */
    private function getWorker($worker_id)
    {
        return Worker::find()
            ->alias('t')
            ->joinWith(['tenant'])
            ->where(['worker_id' => $worker_id, 't.tenant_id' => user()->tenant_id])
            ->one();
    }

    public static function actionListOnlineJson()
    {
        /**
         * @var WorkerShiftService $workerShiftService
         */
        $workerShiftService         = Yii::$app->getModule('employee')->get('workerShift');
        Yii::$app->response->format = Response::FORMAT_JSON;

        return $workerShiftService->getWorkersOnShiftJson();
    }
}
