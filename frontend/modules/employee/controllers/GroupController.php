<?php

namespace frontend\modules\employee\controllers;

use app\modules\tenant\models\User;
use frontend\components\behavior\CityListBehavior;
use frontend\modules\car\models\CarClass;
use frontend\modules\employee\components\position\PositionService;
use frontend\modules\employee\models\groups\WorkerGroup;
use frontend\modules\employee\models\groups\WorkerGroupSearch;
use frontend\modules\employee\models\tariff\Tariff;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * GroupController implements the CRUD actions for WorkerGroup model.
 *
 * @mixin CityListBehavior
 */
class GroupController extends Controller
{
    /**
     * @var PositionService
     */
    private $positionService;

    public function __construct($id, $module, PositionService $positionService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->positionService = $positionService;
    }

    public function behaviors()
    {
        return [
            'access'   => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => [User::ROLE_STAFF_COMPANY],
                    ],
                    [
                        'actions' => ['list', 'view', 'show-blocked', 'update'],
                        'allow'   => true,
                        'roles'   => ['read_drivers'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['drivers'],
                    ],
                ],
            ],
            'cityList' => [
                'class' => CityListBehavior::className(),
            ],
        ];
    }

    private function getListData($searchParams, $block = false)
    {
        $cityList     = $this->getUserCityList();
        $searchModel  = new WorkerGroupSearch(['block' => (int)$block, 'accessCityList' => $this->getUserCityIds()]);
        $positionList = $this->positionService->getPositionMapByCity($searchModel->accessCityList);
        $groupQuery   = $searchModel->search($searchParams);
        $data         = $groupQuery ? $this->module->get('groups')->getMapIndexByPosition($groupQuery->asArray()->all()) : [];
        $dataProvider = new ArrayDataProvider([
            'allModels'  => $data,
            'pagination' => false,
        ]);
        $pjaxId       = $block ? 'blocked_groups' : 'active_groups';

        return compact('searchModel', 'dataProvider', 'cityList', 'pjaxId', 'positionList');
    }

    /**
     * Lists all WorkerGroup models.
     * @return mixed
     */
    public function actionList()
    {
        return $this->render('index', $this->getListData(Yii::$app->request->queryParams));
    }

    /**
     * Lists all blocked WorkerGroup models.
     * @return mixed
     */
    public function actionListBlocked()
    {
        return $this->renderAjax('_groupList', $this->getListData(Yii::$app->request->queryParams, 1));
    }

    /**
     * Creates a new WorkerGroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new WorkerGroup();

        if ($model->load(Yii::$app->request->post())) {
            $transaction = app()->db->beginTransaction();
            try {
                if ($model->save()) {
                    $transaction->commit();
                    session()->setFlash('success', t('employee', 'Drivers group was successfully added'));

                    return $this->redirect('list');
                } else {
                    displayErrors([$model]);
                }
            } catch (Exception $ex) {
                Yii::error($ex->getMessage());
                session()->setFlash('error', t('error', 'Error saving. Notify to administrator, please.'));
            }

            $transaction->rollBack();
        }

        //Карта городов пользователя
        $city_map         = $this->getUserCityList();
        $city_list        = $this->getFilteredCityMap($model->city_list, $city_map);
        $model->city_list = $city_list;
        $this->positionService->setTenantId(user()->tenant_id);

        //Карта должностей
        $positionMap        = $this->positionService->getPositionMapByCity($model->city_list);
        $model->position_id = current(array_keys($positionMap));

        //Карта класса авто. Берем только те, которые добавлены в тарифах для исполнителей текущего города
        $tariffs   = Tariff::getDriverTariffs($city_list, null, $model->position_id);
        $class_map = $this->getCarClassMap($tariffs);

        if (empty($model->class_id)) {
            $classIds        = array_keys($class_map);
            $model->class_id = current($classIds);
        }

        //Карта тарифов для водителей первого класса
        $tariff_map = $this->getDriverTariffMap($model->class_id, $class_map, $tariffs);

        //Карта должностей у которых есть авто
        $carPositionList = $this->getPositionHasCarIds();

        if (empty($model->tariff_list)) {
            $model->tariff_list = array_keys($tariff_map);
        }

        $clientTariffClasses = $this->getClientTariffClasses($model->position_id);

        return $this->render('create', [
            'model'     => $model,
            'form_data' => compact('city_map', 'class_map', 'tariff_map', 'carPositionList',
                'positionMap', 'clientTariffClasses'),
        ]);
    }

    private function getFilteredCityMap($groupCityList, array $userCityMap)
    {
        if (empty($groupCityList)) {
            $city_keys = array_keys($userCityMap);

            return $city_keys[0];
        }

        return $groupCityList;
    }

    /**
     * Getting positions with cars
     *
     * @return array
     */
    private function getPositionHasCarIds()
    {
        /* @var $positionService PositionService */
        $positionService = $this->positionService;

        return $positionService->getPositionIdsHasCar();
    }

    /**
     * Getting position car classes
     *
     * @param int $positionId
     *
     * @return array
     */
    private function getCarClasses($positionId)
    {
        /* @var $positionService PositionService */
        $positionService = $this->positionService;
        $transportTypeId = $positionService->getPositionTransportType($positionId);
        $types           = CarClass::getCarClassMapByTransportType($transportTypeId);

        return ArrayHelper::map($types, 'class_id', 'class');
    }

    /**
     * Updates an existing WorkerGroup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        /** @var WorkerGroup $model */
        $model = WorkerGroup::find()
            ->where(['group_id' => $id, 'tenant_id' => user()->tenant_id])
            ->with(['workerGroupHasCities', 'workerGroupHasTariffs', 'workerGroupCanViewClientTariffs'])
            ->one();

        if (empty($model)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if ($model->load(Yii::$app->request->post())) {
            $transaction = app()->db->beginTransaction();
            try {
                if ($model->save()) {
                    $transaction->commit();
                    session()->setFlash('success', t('employee', 'Drivers group was successfully updated'));

                    return $this->refresh();
                } else {
                    displayErrors([$model]);
                }
            } catch (Exception $ex) {
                Yii::error($ex->getMessage());
                session()->setFlash('error', t('error', 'Error saving. Notify to administrator, please.'));
            }

            $transaction->rollBack();
        }

        $city_list                       = ArrayHelper::getColumn($model->workerGroupHasCities, 'city_id');
        $model->city_list                = $city_list[0];
        $model->tariff_list              = ArrayHelper::getColumn($model->workerGroupHasTariffs, 'tariff_id');

        //Карта городов пользователя
        $city_map  = $this->getUserCityList();
        $city_list = $this->getFilteredCityMap($model->city_list, $city_map);
        //Карта должностей
        $positionMap = $this->positionService->getPositionMapByCity($model->city_list);
        //Карта должностей у которых есть авто
        $carPositionList = $this->getPositionHasCarIds();

        $hasCar = $this->positionService->hasCar($model->position_id);
        if ($hasCar) {
            $model->view_orders_list         = ArrayHelper::getColumn($model->workerGroupCanViewClientTariffs, 'class_id');

            //Карта класса авто. Берем только те, которые добавлены в тарифах для исполнителей текущего города
            $tariffs   = Tariff::getDriverTariffs($city_list, [], $model->position_id);
            $class_map = $this->getCarClassMap($tariffs);

            //Карта тарифов водителей
            $tariff_map = $this->getDriverTariffMap($model->class_id, $class_map, $tariffs);
        } else {
            $model->positionsForExtendsGroup = ArrayHelper::getColumn($model->workerGroupCanViewClientTariffs, 'class_id');

            $class_map     = [];
            $workerTariffs = Tariff::getWorkerTariffsByPositionId($model->position_id, $city_list);
            $tariff_map    = ArrayHelper::map($workerTariffs, 'tariff_id', 'name');

            $positionsForExtendsGroup = $this->positionService->getPositionMapByModule($model->position->module_id);
            unset($positionsForExtendsGroup[$model->position_id]);
        }

        $clientTariffClasses = $this->getClientTariffClasses($model->position_id);

        return $this->render('update', [
            'model'     => $model,
            'form_data' => compact('city_map', 'class_map', 'tariff_map', 'carPositionList',
                'positionMap', 'clientTariffClasses', 'positionsForExtendsGroup'),
        ]);
    }

    private function getCarClassMap($tariffs)
    {
        $classMap = ArrayHelper::map($tariffs, 'class_id', function ($item) {
            return t('car', $item['class']['class']);
        });
        ksort($classMap);

        return $classMap;
    }

    /**
     * Getting client classes
     *
     * @param $positionId
     *
     * @return array
     */
    private function getClientTariffClasses($positionId)
    {
        $positionHasCarIds = $this->getPositionHasCarIds();

        if (in_array($positionId, $positionHasCarIds)) {
            return $this->getCarClasses($positionId);
        }

        return [];
    }

    public function actionUpdateModelData(array $cityList, $positionId)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $data['clientTariffClasses'] = $this->getClientTariffClasses($positionId);

        $tariffs          = Tariff::getDriverTariffs($cityList, [], $positionId);
        $data['classMap'] = $this->getCarClassMap($tariffs);

        return $data;
    }

    /**
     * Changing group`s position.
     *
     * @param array   $cityList
     * @param integer $positionId
     * @param bool    $hasCar
     *
     * @return bool|string
     */
    public function actionChangedPosition(array $cityList, $positionId, $hasCar)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $data['clientTariffClasses'] = $this->getClientTariffClasses($positionId);

        if ($hasCar) {
            $tariffs          = Tariff::getDriverTariffs($cityList, [], $positionId);
            $data['classMap'] = $this->getCarClassMap($tariffs);
        } else {
            $workerTariffs         = Tariff::getWorkerTariffsByPositionId($positionId, $cityList);
            $data['workerTariffs'] = ArrayHelper::map($workerTariffs, 'tariff_id', 'name');

            $position                 = $this->positionService->getPositionById($positionId);
            $positionsForExtendsGroup = $this->positionService->getPositionMapByModule($position->module_id);
            unset($positionsForExtendsGroup[$positionId]);
            $data['positionsForExtendsGroup'] = $positionsForExtendsGroup;
        }

        return $data;
    }

    /**
     * Changing car class.
     * @return bool|string
     */
    public function actionUpdateChangedClass()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        $city_post_data = post('city_list');
        $city_list      = empty($city_post_data) ? $this->getUserCityIds() : $city_post_data;
        //Карта класса авто. Берем только те, которые добавлены в тарифах для водителей текущего города и класса авто
        $tariffs = ArrayHelper::map(Tariff::getDriverTariffs($city_list, post('class_id')), 'tariff_id', 'name');

        return json_encode($tariffs);
    }

    /**
     * @param $class_id
     * @param $class_map
     * @param $tariffs
     *
     * @return array
     */
    private function getDriverTariffMap($class_id, $class_map, $tariffs)
    {
        if (empty($class_id)) {
            $classIds = array_keys($class_map);
            $class_id = current($classIds);
        }

        $tariff_map = [];
        foreach ($tariffs as $key => $tariff) {
            if ($class_id == $tariff['class']['class_id']) {
                $tariff_map[$tariff['tariff_id']] = $tariff['name'];
            }
        }

        return $tariff_map;
    }

}
