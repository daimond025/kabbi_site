<?php

namespace frontend\modules\employee\controllers;

use app\modules\tenant\models\User;
use frontend\components\behavior\CityListBehavior;
use frontend\modules\companies\components\repositories\TenantCompanyRepository;
use frontend\modules\employee\components\position\PositionService;
use frontend\modules\employee\models\tariff\OptionTariff;
use frontend\modules\employee\models\tariff\OptionTariffCurrent;
use frontend\modules\employee\models\tariff\OptionTariffExceptions;
use frontend\modules\employee\models\tariff\Tariff;
use frontend\modules\employee\models\tariff\TariffHasCity;
use frontend\modules\employee\models\tariff\TariffSearch;
use Yii;
use yii\base\InvalidParamException;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

/**
 * TariffController implements the CRUD actions for Tariff model.
 * @mixin CityListBehavior
 */
class TariffController extends Controller
{
    /**
     * @var PositionService
     */
    private $positionService;

    public function __construct($id, $module, PositionService $positionService, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->positionService = $positionService;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access'   => [
                'class'  => AccessControl::className(),
                'except' => [],
                'rules'  => [
                    [
                        'actions' => ['list', 'update', 'update-option', 'list-blocked'],
                        'allow'   => true,
                        'roles'   => ['read_driverTariff'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['driverTariff'],
                    ],
                ],
            ],
            'cityList' => [
                'class' => CityListBehavior::className(),
            ],
        ];
    }

    /**
     * @param array $searchParams
     * @param bool  $block
     *
     * @return mixed
     */
    private function getListData($searchParams, $block = false)
    {
        $cityList                    = $this->getUserCityList();
        $searchModel                 = new TariffSearch(['block' => (int)$block]);
        $searchModel->accessCityList = array_keys($cityList);
        $positionList                = $this->positionService->getPositionMapByCity($searchModel->accessCityList);
        $dataProvider                = $searchModel->search($searchParams);
        $tenantCompany               = TenantCompanyRepository::selfCreate()->getForForm();
        $pjaxId                      = $block ? 'blocked_tariffs' : 'active_tariffs';

        return compact('searchModel', 'dataProvider', 'cityList', 'pjaxId', 'positionList', 'tenantCompany');
    }

    /**
     * Lists all Worker models.
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionList()
    {
        return $this->render('index', $this->getListData(Yii::$app->request->queryParams));
    }

    /**
     * Lists all Worker models.
     * @return mixed
     */
    public function actionListBlocked()
    {
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_grid', $this->getListData(Yii::$app->request->queryParams, 1));
        }

        return $this->redirect(['list', '#' => 'blocked']);
    }

    /**
     * @return mixed
     * @throws Exception
     * @throws InvalidParamException
     */
    public function actionCreate()
    {
        $tariff = new Tariff([
            'action_new_shift'        => Tariff::ACTION_NEW_SHIFT_CONTINUE_PERIOD,
            'tenant_id'               => user()->tenant_id,
            'type'                    => Tariff::TYPE_ONCE,
            'subscription_limit_type' => Tariff::SUBSCRIPTION_LIMIT_TYPE_SHIFT,
            'period_type'             => Tariff::PERIOD_TYPE_INTERVAL,
            'period'                  => [Tariff::DEFAULT_START_INTERVAL_VALUE, Tariff::DEFAULT_END_INTERVAL_VALUE],
        ]);

        $tariff->scenario = Tariff::SCENARIO_INSERT;

        $post = post();
        if (app()->user->can('driverTariff') && $tariff->load($post)) {
            $transaction = app()->db->beginTransaction();
            try {
                if ($tariff->save()) {
                    TariffHasCity::manySave((array)$tariff->city_list, $tariff->tariff_id);

                    $currentOption = new OptionTariff([
                        'tariff_id'   => $tariff->tariff_id,
                        'tariff_type' => OptionTariff::TYPE_CURRENT,
                    ]);
                    if (!$currentOption->save()) {
                        $error = implode(';', $currentOption->getFirstErrors());
                        throw new Exception("Add option tariff error (current option). Error={$error}");
                    }

                    $transaction->commit();
                    session()->setFlash('success', t('taxi_tariff', 'The tariff successfully added'));

                    return $this->redirect(['update', 'id' => $tariff->tariff_id]);
                }
            } catch (\Exception $ex) {
                $transaction->rollBack();
                \Yii::error($ex->getMessage(), 'worker-tariff');
                session()->setFlash('error', t('taxi_tariff', 'Error saving tariff. Notify to administrator, please.'));
            }
        }

        $formData            = $tariff->getFormData();
        $tariff->city_list   = key($formData['USER_CITY_LIST']);
        $tariff->position_id = $tariff->position_id ?: key($formData['POSITION_MAP']);

        return $this->render('create', [
            'tariff'   => $tariff,
            'formData' => $tariff->getFormData(),
        ]);
    }

    /**
     * @param $id
     *
     * @return mixed
     * @throws Exception
     * @throws InvalidParamException
     */
    public function actionUpdate($id)
    {
        /* @var $tariff Tariff */
        $tariff = Tariff::find()
            ->where([
                'tariff_id' => $id,
                'tenant_id' => user()->tenant_id,
            ])
            ->one();

        if (app()->user->can(User::ROLE_STAFF_COMPANY)) {
            if (!app()->user->can(User::ROLE_STAFF_COMPANY, ['model' => $tariff])) {
                throw new ForbiddenHttpException(t('tenant_company', 'Access denied.'));
            }
        }

        if (empty($tariff)) {
            session()->addFlash(t('employee', 'Tariff not found'));
            $this->redirect(['list']);
        }

        $tariff->city_list = $tariff->cities[0]->city_id;

        $post = post();
        if (app()->user->can('driverTariff') && $tariff->load($post)) {
            $transaction = app()->db->beginTransaction();
            try {
                if ($tariff->save()) {
                    TariffHasCity::deleteAll(['tariff_id' => $tariff->tariff_id]);
                    TariffHasCity::manySave((array)$tariff->city_list, $tariff->tariff_id);

                    $transaction->commit();
                    session()->setFlash('success', t('taxi_tariff', 'The tariff successfully updated'));

                    return $this->redirect(['update', 'id' => $tariff->tariff_id]);
                }
            } catch (\Exception $ex) {
                $transaction->rollBack();
                \Yii::error($ex->getMessage(), 'worker-tariff');
                session()->setFlash('error', t('taxi_tariff', 'Error saving tariff. Notify to administrator, please.'));
            }
        }

        $currentOption = OptionTariffCurrent::findByTariff($tariff->tariff_id)->one();
        $otherOptions  = OptionTariffExceptions::findByTariff($tariff->tariff_id)
            ->orderBy([
                'sort'      => SORT_ASC,
                'option_id' => SORT_ASC,
            ])->all();

        return $this->render('update', [
            'tariff'        => $tariff,
            'formData'      => $tariff->getFormData($tariff->position_id, $tariff->city_list),
            'currentOption' => $currentOption,
            'otherOptions'  => $otherOptions,
        ]);
    }

    /**
     * @param int $tariffId
     * @param int $optionId
     *
     * @return mixed
     * @throws Exception
     * @throws InvalidParamException
     */
    public function actionUpdateOption($tariffId, $optionId = null)
    {
        /* @var $tariff Tariff */
        $tariff = Tariff::find()
            ->where([
                'tariff_id' => $tariffId,
                'tenant_id' => user()->tenant_id,
            ])
            ->one();

        if (empty($optionId)) {
            $option = new OptionTariff([
                'tariff_id'   => $tariffId,
                'tariff_type' => OptionTariff::TYPE_EXCEPTIONS,
            ]);
            $option->loadDefaultValues();
        } else {
            $option = OptionTariff::findOne($optionId);
        }

        if (empty($tariff) || empty($option)) {
            session()->addFlash(t('employee', 'Tariff not found'));

            return $this->redirect(['update', 'id' => $tariffId], 200);
        }

        $post = post();
        if (app()->user->can('driverTariff') && $option->load($post)) {
            $transaction = app()->db->beginTransaction();

            if (isset($post['option']['ActiveDate'])) {
                $option->active_date = implode(';', array_unique($post['option']['ActiveDate']));
            }

            try {
                if ($option->save()) {
                    $option->saveTariffCommission($post['commissions']);


                    $hasCar = $this->positionService->hasCar($tariff->position_id);
                    if ($hasCar) {
                        $cityId = isset($tariff->cities[0]) ? $tariff->cities[0]->city_id : null;
                        $option->saveOptions($cityId, $post['addOptions']);
                    }

                    $transaction->commit();

                    return $this->redirect(['update', 'id' => $tariffId], 200);
                } else {
                    session()->setFlash('error',
                        t('taxi_tariff', 'Error saving tariff. Notify to administrator, please.'));

                    return $this->redirect(['update', 'id' => $tariffId], 200);
                }
            } catch (\Exception $ex) {
                $transaction->rollBack();
                \Yii::error($ex->getMessage(), 'worker-tariff');
                session()->setFlash('error', t('taxi_tariff', 'Error saving tariff. Notify to administrator, please.'));
            }
        }

        $cityId   = $tariff->cities[0]->city_id;
        $formData = $tariff->getFormData($tariff->position_id, $cityId);
        $hasCar   = $this->positionService->hasCar($tariff->position_id);

        return $this->renderAjax(app()->user->can('driverTariff') ? '_optionForm' : '_optionForm_read', [
            'option'             => $option,
            'add_options'        => $formData['ADD_OPTIONS'],
            'active_add_options' => $formData['ACTIVE_ADD_OPTION_LIST'],
            'cityId'             => $cityId,
            'hasCar'             => $hasCar,
        ]);
    }


    /**
     * @param int $tariffId
     * @param int $optionId
     *
     * @return mixed
     * @throws \Exception
     */
    public function actionDeleteOption($tariffId, $optionId)
    {
        if (!app()->request->isAjax) {
            return $this->redirect(['update', 'tariffId' => $tariffId]);
        }

        /* @var $tariff Tariff */
        $tariff = Tariff::find()
            ->where([
                'tariff_id' => $tariffId,
                'tenant_id' => user()->tenant_id,
            ])
            ->one();

        $option = OptionTariff::findOne($optionId);

        if (app()->user->can('driverTariff') && !empty($tariff) && !empty($option)) {
            $option->delete();
        }


        return $this->redirect(['update', 'id' => $tariffId], 200);
    }

    /**
     * @param $city_id
     *
     * @return array|bool
     */
    public function actionGetActiveCarOptions($city_id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        return Tariff::getActiveCarOptionList($city_id);
    }
}
