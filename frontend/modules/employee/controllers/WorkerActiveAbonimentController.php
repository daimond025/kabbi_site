<?php

namespace frontend\modules\employee\controllers;


use frontend\components\behavior\CityListBehavior;
use frontend\modules\employee\models\workerActiveAboniment\entities\WorkerActiveAboniment;
use frontend\modules\employee\models\workerActiveAboniment\search\WorkerActiveAbonimentSearch;
use landing\modules\community\components\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

class WorkerActiveAbonimentController extends Controller
{

    const VIEW_ACTION_TARIFF = 'tariff';
    const VIEW_ACTION_WORKER = 'worker';

    public function behaviors()
    {
        return [
            'access'   => [
                'class'  => AccessControl::className(),
                'except' => [],
                'rules'  => [
                    [
                        'actions' => [
                            'get-active-aboniment-by-tariff',
                            'get-active-aboniment-by-worker',
                        ],
                        'allow'   => true,
                        'roles'   => ['read_driverTariff'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['driverTariff'],
                    ],
                ],
            ],
            'cityList' => [
                'class' => CityListBehavior::className(),
            ],
        ];
    }

    public function actionDeleteByTariff($id, $abonimen_id)
    {
        $this->deleteAboniment($abonimen_id);

        return $this->actionGetActiveAbonimentByTariff($id);
    }

    public function actionDeleteByWorker($id, $abonimen_id)
    {
        $this->deleteAboniment($abonimen_id);

        return $this->actionGetActiveAbonimentByWorker($id);
    }

    public function actionGetActiveAbonimentByTariff($id)
    {
        return $this->getActiveAboniments('tariff_id', $id, self::VIEW_ACTION_TARIFF);
    }


    public function actionGetActiveAbonimentByWorker($id)
    {
        return $this->getActiveAboniments('worker_id', $id, self::VIEW_ACTION_WORKER);
    }


    protected function getActiveAboniments($find, $id, $actionType)
    {

        $preparedGet = $this->getPreparedGet();

        $this->setHeaderForPjax(array_merge($preparedGet, ['id' => $id]), $actionType);

        $searchModel = new WorkerActiveAbonimentSearch();
        $dataProvider = $searchModel->search([$find => $id]);

        return $this->renderAjax('grid', [
            'dataProvider' => $dataProvider,
            'actionType'   => $actionType
        ]);
    }

    protected function setHeaderForPjax($preparedGet, $actionType)
    {
        \Yii::$app->response->headers->set(
            'X-PJAX-URL',
            Url::to(array_merge(["/employee/$actionType/update"], $preparedGet, ['#' => 'aboniments']))
        );
    }

    protected function getPreparedGet()
    {
        $get = \Yii::$app->request->get();
        unset($get['id'], $get['_pjax'], $get['abonimen_id']);

        return $get;

    }

    protected function deleteAboniment($id)
    {
        $activeAboniment = $this->getActiveAboniment($id);
        if (!$activeAboniment->delete()) {
            \Yii::error('Error delete aboniment');
            throw new ServerErrorHttpException();
        }

    }

    protected function getActiveAboniment($id)
    {
        if (!$activeAboniment = WorkerActiveAboniment::findOne($id)) {
            throw new NotFoundHttpException();
        }

        return $activeAboniment;
    }

}