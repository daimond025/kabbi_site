<?php


namespace frontend\modules\employee\controllers;

use app\modules\tariff\models\TaxiTariff;
use frontend\modules\employee\models\groups\WorkerGroup;
use frontend\modules\employee\models\tariff\Tariff;
use frontend\modules\employee\models\worker\Worker;
use frontend\modules\employee\models\worker\WorkerHasPosition;
use Yii;
use frontend\modules\car\models\Car;
use frontend\modules\car\models\CarSearch;
use landing\modules\community\components\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;

class CarController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class'  => AccessControl::className(),
                'except' => [],
                'rules'  => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $this->setViewPath('@app/modules/employee/widgets/car/views/');

        return parent::beforeAction($action);
    }


    /**
     * Search car for driver
     */
    public function actionCarSearch()
    {
        $car_search = new CarSearch();
        $post       = post();
        $button     = $post['button'];
        $form       = app()->user->can('drivers') ? '_cars' : '_cars_read';

        if ($button == 'Remove') {
            //$car_search->with_groups = true;
            $dataProvider = $car_search->search($post);

            return $this->getShowingCarsContent($car_search->entity_id, $form, $dataProvider->getModels());
        }

        $dataProvider = $car_search->search($post);
        $arCars       = $car_search->getGridData($dataProvider->getModels(), false, false);

        return $this->renderAjax($form, ['cars' => $arCars['CARS'], 'button' => $button]);
    }

    /**
     * List of all driver car.
     *
     * @param integer    $id driver_id
     * @param string     $view View name
     * @param null|array $carsModels
     *
     * @return mixed
     */
    public function actionShowCars($id, $view = 'cars', $carsModels = null)
    {
        return $this->getShowingCarsContent($id, $view, $carsModels);
    }

    /**
     * Unlink car from worker.
     *
     * @param $entity_id
     * @param $car_id
     *
     * @return int
     */
    public function actionDeleteCar($entity_id, $car_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if(empty($entity_id) || empty($car_id)) {
            return false;
        }

        return (int)$this->module->get('workerCar')->unlink($entity_id, $car_id);
    }

    /**
     * Link car to worker.
     *
     * @param integer $entity_id
     * @param integer $car_id
     * @param integer $position_id
     *
     * @return bool|mixed
     */
    public function actionAddCar($entity_id, $car_id, $position_id)
    {
       $this->module->get('workerCar')->link($entity_id, $car_id);

        return $this->getShowingCarsContent($entity_id, '_cars_table', null, $position_id);
    }

    /**
     * List of all worker cars.
     *
     * @param integer            $entity_id Worker has position id
     * @param string             $view View name
     * @param null|array         $carsModels
     * @param null|integer|array $position_id
     *
     * @return mixed
     */
    private function getShowingCarsContent($entity_id, $view = 'cars', $carsModels = null, $position_id = null)
    {
        if (empty($carsModels)) {
            $carsModels = $this->module->get('workerCar')->getCarsByEntityId($entity_id);
        }

        $arClass = ArrayHelper::getColumn($carsModels, 'class_id');
        $arClass = array_unique($arClass);
        $cars    = Car::getGridData($carsModels, false, false, true);


        $workerCityIds = $this->module->get('workerHasPosition')->getWorkerCitiesById($entity_id);

        $workerCityIds = array_intersect(
            $workerCityIds,
            array_keys(user()->getUserCityList())
        );

        //Группы водителей с тарифами и заказами для просмотра
        $workerGroupRepository = $this->module->get('groups')->getRepository();
        $workerGroups          = $workerGroupRepository->getGroupsByClass($arClass, $position_id,
            $workerCityIds);

        //Карта групп, сгруппированная по классу
        $groupMapGroupedByClass = $this->module->get('groups')
            ->getMapGroup($workerGroups, 'class_id');


        //Карта тарифов водителей для текущего класса авто
        $tariffs                 = $this->module->get('tariff')->getRepository()
            ->getDriverTariffs($workerCityIds, $arClass, $position_id);
        $tariffMapGroupedByClass = $this->module->get('tariff')->getTariffMapGroupByClass($tariffs);

        //Карта просмотра заказов для текущей должности
        $clientTariffs     = $this->module->get('clientTariff')->getRepository()
            ->getTariffsGroupByClass($workerCityIds, $position_id);
        $showingClassesMap = $this->module->get('clientTariff')->getShowingClassesMap($clientTariffs);

        Yii::$app->get('orderApi')->workerPromoRegistration(WorkerHasPosition::findOne(['id' => $entity_id])->worker_id);

        return $this->renderAjax($view, [
            'cars'                    => $cars['CARS'],
            'workerGroups'            => $workerGroups,
            'button'                  => 'Remove',
            'groupMapGroupedByClass'  => $groupMapGroupedByClass,
            'tariffMapGroupedByClass' => $tariffMapGroupedByClass,
            'showingClassesMap'       => $showingClassesMap,
        ]);
    }
}