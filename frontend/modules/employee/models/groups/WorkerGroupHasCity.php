<?php

namespace frontend\modules\employee\models\groups;

use common\modules\city\models\City;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%worker_group_has_city}}".
 *
 * @property integer $group_id
 * @property string $city_id
 *
 * @property City $city
 * @property WorkerGroup $group
 */
class WorkerGroupHasCity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_group_has_city}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'city_id'], 'required'],
            [['group_id', 'city_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'group_id' => 'Group ID',
            'city_id' => 'City ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(WorkerGroup::className(), ['group_id' => 'group_id']);
    }

    /**
     * Allow to save multiple rows
     * @param array $arCity
     * @param integer $group_id
     * @return integer number of rows affected by the execution|null
     * @throws Exception execution failed
     */
    public static function insertRows($arCity, $group_id)
    {
        $insertValue = [];
        $connection = app()->db;

        if(empty($arCity))
            $arCity = ArrayHelper::getColumn(user()->cities, 'city_id');

        foreach($arCity as $city_id)
        {
            $insertValue[] = [$group_id, $city_id];
        }

        return !empty($insertValue) ? $connection->createCommand()->batchInsert(self::tableName(), ['group_id', 'city_id'], $insertValue)->execute() : null;
    }

    /**
     * Update city of driver groups.
     * @param array $arCity
     * @param integer $group_id
     * @return integer number of rows affected by the execution.
     * @throws Exception execution failed
     */
    public static function updateRows($arCity, $group_id)
    {
        self::deleteAll(['group_id' => $group_id]);
        return self::insertRows($arCity, $group_id);
    }
}
