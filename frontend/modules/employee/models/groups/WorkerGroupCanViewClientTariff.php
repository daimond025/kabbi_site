<?php

namespace frontend\modules\employee\models\groups;

use app\modules\tariff\models\TaxiTariff;
use frontend\modules\car\models\CarClass;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%worker_group_can_view_client_tariff}}".
 *
 * @property integer $group_id
 * @property string $class_id
 *
 * @property TaxiTariff $tariff
 * @property WorkerGroup $group
 */
class WorkerGroupCanViewClientTariff extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_group_can_view_client_tariff}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'class_id'], 'required'],
            [['group_id', 'class_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'group_id' => 'Group ID',
            'class_id' => 'Tariff ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(TaxiTariff::className(), ['class_id' => 'class_id']);
    }

    public function getClass()
    {
        return $this->hasOne(CarClass::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(WorkerGroup::className(), ['group_id' => 'group_id']);
    }

    /**
     * Allow to save multiple rows
     * @param array $arTariffs
     * @param integer $group_id
     * @return integer number of rows affected by the execution|null
     * @throws Exception execution failed
     */
    public static function insertRows($arTariffs, $group_id)
    {
        $insertValue = [];
        $connection = app()->db;

        foreach($arTariffs as $class_id)
        {
            if (!empty($class_id)) {
                $insertValue[] = [$group_id, $class_id];
            }
        }

        return !empty($insertValue) ? $connection->createCommand()->batchInsert(self::tableName(), ['group_id', 'class_id'], $insertValue)->execute() : null;
    }

    /**
     * Update rows.
     * @param array $arTariffs
     * @param integer $group_id
     * @return integer number of rows affected by the execution.
     * @throws Exception execution failed
     */
    public static function updateRows($arTariffs, $group_id)
    {
        self::deleteAll(['group_id' => $group_id]);
        return self::insertRows($arTariffs, $group_id);
    }
}
