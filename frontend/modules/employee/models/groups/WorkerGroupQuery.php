<?php

namespace frontend\modules\employee\models\groups;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[WorkerGroup]].
 *
 * @see WorkerGroup
 */
class WorkerGroupQuery extends ActiveQuery
{
    public function block($block = 1)
    {
        return $this->andWhere(['block' => $block]);
    }

    public function getListByCity($cityList, $filter = [], $orderBy = 'name')
    {
        return $this->andWhere(['tenant_id' => user()->tenant_id])
            ->andFilterWhere($filter)
            ->with([
                'cities'   => function (ActiveQuery $query) {
                    $query->select(['city_id', 'name']);
                },
                'class',
                'position' => function (ActiveQuery $query) {
                    $query->select(['position_id', 'name']);
                },
            ])
            ->joinWith([
                'workerGroupHasCities' => function (ActiveQuery $query) use ($cityList) {
                    $query->where(['city_id' => $cityList]);
                },
            ], false)
            ->orderBy($orderBy);
    }
}