<?php

namespace frontend\modules\employee\models\groups;

use common\modules\city\models\City;
use common\modules\employee\models\position\Position;
use common\modules\tenant\models\Tenant;
use frontend\modules\car\models\CarClass;
use frontend\modules\employee\models\tariff\Tariff;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%worker_group}}".
 *
 * @property integer                          $group_id
 * @property string                           $name
 * @property integer                          $class_id
 * @property integer                          $block
 * @property integer                          $priority
 * @property integer                          $tenant_id
 * @property integer                          $position_id
 *
 * @property WorkerGroupCanViewClientTariff[] $workerGroupCanViewClientTariffs
 * @property WorkerGroupHasCity[]             $workerGroupHasCities
 * @property WorkerGroupHasTariff[]           $workerGroupHasTariffs
 * @property Position                         $position
 */
class WorkerGroup extends ActiveRecord
{
    public $city_list = [];
    public $tariff_list = [];
    public $view_orders_list = [];
    public $positionsForExtendsGroup = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_group}}';
    }

    public function behaviors()
    {
        return [
            [
                'class'      => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'tenant_id',
                ],
                'value'      => function ($event) {
                    return user()->tenant_id;
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'position_id'], 'required'],
            [
                ['tariff_list'],
                'required',
                'when' => function () {
                    return $this->block == 0;
                },
            ],
            [['class_id', 'block', 'position_id', 'priority'], 'integer'],
            [['block', 'priority'], 'default', 'value' => 0],
            [['city_list'], 'default', 'value' => []],
            [['name'], 'string', 'max' => 255],
            [['view_orders_list', 'positionsForExtendsGroup'], 'filter', 'filter' => 'array_filter'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'group_id'                 => 'Group ID',
            'name'                     => t('employee', 'Name'),
            'class_id'                 => t('driverTariff', 'Car class'),
            'block'                    => t('app', 'Active'),
            'city_list'                => t('employee', 'The city where the Group operates'),
            'tariff_list'              => t('employee', 'Tariffs'),
            'view_orders_list'         => t('employee', 'View orders'),
            'positionsForExtendsGroup' => t('employee', 'View orders'),
            'position_id'              => t('employee', 'Profession'),
            'priority'                 => t('employee', 'Priority at distribution of orders'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerGroupCanViewClientTariffs()
    {
        return $this->hasMany(WorkerGroupCanViewClientTariff::className(), ['group_id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShowOrders()
    {
        return $this->hasMany(CarClass::className(),
            ['class_id' => 'class_id'])->viaTable('{{%worker_group_can_view_client_tariff}}',
            ['group_id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerGroupHasCities()
    {
        return $this->hasMany(WorkerGroupHasCity::className(), ['group_id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['city_id' => 'city_id'])->viaTable('{{%worker_group_has_city}}',
            ['group_id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffs()
    {
        return $this->hasMany(Tariff::className(),
            ['tariff_id' => 'tariff_id'])->viaTable('{{%worker_group_has_tariff}}', ['group_id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClass()
    {
        return $this->hasOne(CarClass::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerGroupHasTariffs()
    {
        return $this->hasMany(WorkerGroupHasTariff::className(), ['group_id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);


        if ($insert) {
            //Запись городов группы
            WorkerGroupHasCity::insertRows($this->city_list, $this->group_id);
            //Запись тарифов водителей группы
            WorkerGroupHasTariff::insertRows($this->tariff_list, $this->group_id);
            //Запись возможных просмотров заказов группы
            if ($this->position->has_car) {
                WorkerGroupCanViewClientTariff::insertRows($this->view_orders_list, $this->group_id);
            } else {
                WorkerGroupCanViewClientTariff::insertRows($this->positionsForExtendsGroup, $this->group_id);
            }
        } else {
            //            $old_city_list = ArrayHelper::getColumn($this->driverGroupHasCities, 'city_id');
            //            if (!empty(array_diff_assoc($old_city_list, $this->city_list)) || !empty(array_diff_assoc($this->city_list, $old_city_list))) {
            //                WorkerGroupHasCity::updateRows($this->city_list, $this->group_id);
            //            }
            $old_tariff_list = ArrayHelper::getColumn($this->workerGroupHasTariffs, 'tariff_id');

            if ($this->tariff_list) {
                if (!empty(array_diff_assoc($old_tariff_list, $this->tariff_list))
                    || !empty(array_diff_assoc($this->tariff_list, $old_tariff_list))
                ) {
                    WorkerGroupHasTariff::updateRows($this->tariff_list, $this->group_id);
                }
            }

            if ($this->position->has_car) {
                $old_view_orders_list = ArrayHelper::getColumn($this->workerGroupCanViewClientTariffs, 'class_id');
                if (!empty(array_diff_assoc($old_view_orders_list, $this->view_orders_list))
                    || !empty(array_diff_assoc($this->view_orders_list, $old_view_orders_list))
                ) {
                    WorkerGroupCanViewClientTariff::updateRows($this->view_orders_list, $this->group_id);
                }
            } else {
                $oldPositionsForExtendsGroup = ArrayHelper::getColumn(
                    $this->workerGroupCanViewClientTariffs, 'class_id');
                if (!empty(array_diff($oldPositionsForExtendsGroup, $this->positionsForExtendsGroup))
                    || !empty(array_diff($this->positionsForExtendsGroup, $oldPositionsForExtendsGroup))
                ) {
                    WorkerGroupCanViewClientTariff::updateRows($this->positionsForExtendsGroup, $this->group_id);
                }
            }

        }
    }

    public static function find()
    {
        return new WorkerGroupQuery(get_called_class());
    }

    public static function getAllGroupWorkers()
    {
        return self::find()->where(['tenant_id'=>user()->tenant_id, 'block' => false])->all();
    }

}
