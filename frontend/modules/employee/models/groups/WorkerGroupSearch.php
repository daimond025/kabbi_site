<?php

namespace frontend\modules\employee\models\groups;

/**
 * WorkerGroupSearch represents the model behind the search form about `\frontend\modules\employee\models\groups\WorkerGroup`.
 */
class WorkerGroupSearch extends WorkerGroup
{
    public $accessCityList;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['block'], 'integer'],
            [['name'], 'string'],
            [['city_list'], 'in', 'range' => $this->accessCityList, 'allowArray' => true],
            [['position_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'position_id' => t('employee', 'Profession'),
            'city_list'   => t('user', 'All cities'),
            'name'        => t('employee', 'Name'),
        ];
    }

    /**
     * Getting array of instance with search query applied
     *
     * @param array $params
     *
     * @return WorkerGroupQuery|false
     */
    public function search($params)
    {
        $this->load($params);

        if (!$this->validate()) {
            return false;
        }

        $query = self::find()->getListByCity($this->getCity(), ['position_id' => $this->position_id]);
        $query->block($this->block);
        $query->andFilterWhere(['like', 'name', $this->name]);

        return $query;
    }

    private function getCity()
    {
        return $this->city_list ? $this->city_list : $this->accessCityList;
    }
}
