<?php

namespace frontend\modules\employee\models\groups;

use frontend\modules\employee\models\tariff\Tariff;
use Yii;

/**
 * This is the model class for table "{{%worker_group_has_tariff}}".
 *
 * @property integer $group_id
 * @property string $tariff_id
 *
 * @property Tariff $tariff
 * @property WorkerGroup $group
 */
class WorkerGroupHasTariff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_group_has_tariff}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'tariff_id'], 'required'],
            [['group_id', 'tariff_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'group_id' => 'Group ID',
            'tariff_id' => 'Tariff ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(Tariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(WorkerGroup::className(), ['group_id' => 'group_id']);
    }

    /**
     * Allow to save multiple rows
     * @param array $arTariffs
     * @param integer $group_id
     * @return integer number of rows affected by the execution.
     * @throws Exception execution failed
     */
    public static function insertRows($arTariffs, $group_id)
    {
        $insertValue = [];
        $connection = app()->db;

        foreach($arTariffs as $tariff_id)
        {
            if (!empty($tariff_id)) {
                $insertValue[] = [$group_id, $tariff_id];
            }
        }

        return !empty($insertValue) ? $connection->createCommand()->batchInsert(self::tableName(), ['group_id', 'tariff_id'], $insertValue)->execute() : null;
    }

    /**
     * Update rows.
     * @param array $arTariffs
     * @param integer $group_id
     * @return integer number of rows affected by the execution.
     * @throws Exception execution failed
     */
    public static function updateRows($arTariffs, $group_id)
    {
        self::deleteAll(['group_id' => $group_id]);
        return self::insertRows($arTariffs, $group_id);
    }
}
