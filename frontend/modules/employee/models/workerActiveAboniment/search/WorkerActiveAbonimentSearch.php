<?php

namespace frontend\modules\employee\models\workerActiveAboniment\search;


use frontend\modules\employee\models\workerActiveAboniment\entities\WorkerActiveAboniment;
use yii\data\ActiveDataProvider;

class WorkerActiveAbonimentSearch extends WorkerActiveAboniment
{

    const PAGE_SIZE = 20;

    public function rules()
    {
        return [
            [['tariff_id', 'worker_id'], 'integer'],
        ];
    }


    public function search($params)
    {
        $query = WorkerActiveAboniment::find()
            ->alias('waa')
            ->joinWith(['tariff t', 'worker w'], false)
            ->where(['t.tenant_id' => user()->tenant_id]);


        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'sort'       => [
                'defaultOrder' => [
                    'aboniment_id' => SORT_DESC,
                ],
                'attributes'   => [
                    'aboniment_id' => [
                        'asc'  => ['waa.aboniment_id' => SORT_ASC],
                        'desc' => ['waa.aboniment_id' => SORT_DESC]
                    ],
                    'tariff_id' => [
                        'asc'  => ['t.name' => SORT_ASC],
                        'desc' => ['t.name' => SORT_DESC]
                    ],
                    'worker_id' => [
                        'asc'  => ['w.last_name' => SORT_ASC, 'w.name' => SORT_ASC, 'w.second_name' => SORT_ASC],
                        'desc' => ['w.last_name' => SORT_DESC, 'w.name' => SORT_DESC, 'w.second_name' => SORT_DESC],
                    ],
                    'type' => [
                        'asc'  => ['waa.type' => SORT_ASC],
                        'desc' => ['waa.type' => SORT_DESC]
                    ],
                    'action_new_shift' => [
                        'asc'  => ['waa.action_new_shift' => SORT_ASC],
                        'desc' => ['waa.action_new_shift' => SORT_DESC]
                    ],
                    'subscription_limit_type' => [
                        'asc'  => ['waa.subscription_limit_type' => SORT_ASC],
                        'desc' => ['waa.subscription_limit_type' => SORT_DESC]
                    ],
                    'days' => [
                        'asc'  => ['waa.days' => SORT_ASC],
                        'desc' => ['waa.days' => SORT_DESC]
                    ],
                    'period_type' => [
                        'asc'  => ['waa.period_type' => SORT_ASC],
                        'desc' => ['waa.period_type' => SORT_DESC]
                    ],
                    'period' => [
                        'asc'  => ['waa.period' => SORT_ASC],
                        'desc' => ['waa.period' => SORT_DESC]
                    ],
                    'cost' => [
                        'asc'  => ['waa.cost' => SORT_ASC],
                        'desc' => ['waa.cost' => SORT_DESC]
                    ],
                    'count_active' => [
                        'asc'  => ['waa.count_active' => SORT_ASC],
                        'desc' => ['waa.count_active' => SORT_DESC]
                    ],
                    'expired_at' => [
                        'asc'  => ['waa.expired_at' => SORT_ASC],
                        'desc' => ['waa.expired_at' => SORT_DESC]
                    ],
                    'created_at' => [
                        'asc'  => ['waa.created_at' => SORT_ASC],
                        'desc' => ['waa.created_at' => SORT_DESC]
                    ],
                ],
            ],
            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);


        $this->load($params, '');
        if (!$this->validate()) {
            return false;
        }

        $query->andFilterWhere(['waa.worker_id' => $this->worker_id])
            ->andFilterWhere(['waa.tariff_id' => $this->tariff_id]);


        return $dataProvider;

    }

}