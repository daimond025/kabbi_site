<?php

namespace frontend\modules\employee\models\workerActiveAboniment\entities;

use common\modules\employee\models\worker\Worker;
use common\modules\employee\models\worker\WorkerTariff;
use Yii;

/**
 * This is the model class for table "{{%worker_active_aboniment}}".
 *
 * @property integer      $aboniment_id
 * @property integer      $tariff_id
 * @property integer      $count_active
 * @property integer      $worker_id
 * @property integer      $shift_valid_to
 * @property integer      $expired_at
 * @property integer      $created_at
 * @property integer      $updated_at
 *
 * @property string       $type
 * @property string       $action_new_shift
 * @property string       $period_type
 * @property string       $period
 * @property string       $cost
 * @property string       $subscription_limit_type
 * @property integer      $days
 *
 * @property WorkerTariff $tariff
 * @property Worker       $worker
 */
class WorkerActiveAboniment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_active_aboniment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id', 'worker_id'], 'required'],
            [['tariff_id', 'count_active', 'worker_id', 'shift_valid_to', 'expired_at'], 'integer'],
            [
                ['tariff_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => WorkerTariff::className(),
                'targetAttribute' => ['tariff_id' => 'tariff_id'],
            ],
            [
                ['worker_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Worker::className(),
                'targetAttribute' => ['worker_id' => 'worker_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'aboniment_id'   => Yii::t('app', 'Aboniment ID'),
            'tariff_id'      => Yii::t('driverTariff', 'Tariff'),
            'count_active'   => Yii::t('driverTariff', 'Days left'),
            'worker_id'      => Yii::t('driverTariff', 'Worker'),
            'shift_valid_to' => Yii::t('app', 'Shift Valid To'),
            'expired_at'     => Yii::t('driverTariff', 'Expires'),
            'created_at'     => Yii::t('driverTariff', 'Time of purchase'),

            'type'                    => t('driverTariff', 'Tariff type'),
            'days'                    => t('driverTariff', 'Days'),
            'subscription_limit_type' => t('driverTariff', 'How to calculate the subscription period'),
            'period_type'             => t('driverTariff', 'Work period'),
            'period'                  => t('driverTariff', 'Period'),
            'cost'                    => t('driverTariff', 'Cost'),
            'action_new_shift'        => t('driverTariff', 'When you open a new shift'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(WorkerTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }
}
