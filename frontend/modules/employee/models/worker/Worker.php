<?php

namespace frontend\modules\employee\models\worker;

use app\modules\balance\models\Account;
use app\modules\tenant\models\User;
use common\helpers\DateTimeHelper;
use common\modules\employee\models\position\Position;
use common\modules\employee\models\worker\Worker as WorkerBase;
use common\modules\employee\models\worker\WorkerHasCity;
use frontend\modules\employee\models\worker\WorkerHasPosition;
use common\modules\tenant\models\TenantHelper;
use frontend\components\behavior\file\CropFileBehavior;
use frontend\modules\client\models\ClientWorkerLike;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\caching\TagDependency;
use common\modules\employee\models\worker\WorkerHasCar;
use common\modules\employee\models\worker\WorkerShift;

/**
 * Class Worker
 * @package frontend\modules\employee\models\worker
 * @mixin CropFileBehavior
 * @property ClientWorkerLike $clientWorkerLike
 */
class Worker extends WorkerBase
{

    const STATUS_FREE = 'FREE';
    const STATUS_BLOCKED = 'BLOCKED';
    const STATUS_ON_BREAK = 'ON_BREAK';
    const STATUS_ON_ORDER = 'ON_ORDER';
    const STATUS_OFFER_ORDER = 'OFFER_ORDER';


    /**
     * Tag prefix for yii\caching\TagDependency
     * Example: "worker_123_66", where 123 is callsign and 66 is tenant_id
     */
    const TAG_DEPENDENCY_PREFIX = 'worker_';

    public $birth_day;
    public $birth_month;
    public $birth_year;
    public $password_repeat;
    public $cropParams;

    const ACTIVE = 'active';
    const BLOCKED = 'blocked';
    const NOT_ACTIVATED = 'notActivated';

    const SCENARIO_CALLSIGN = 'callsign';

    protected $_cityIds;
    public $availableCities = [];

    public function getCityIds()
    {
        return $this->_cityIds === null
            ? ArrayHelper::getColumn($this->cities, 'city_id') : $this->_cityIds;
    }

    public function setCityIds($cityIds)
    {
        $this->_cityIds = !empty($cityIds) ? (array)$cityIds : [];
    }

    /**
     * Add a comment to this line
     * Getting last active or first in list city id
     * @return int
     */
    public function getLastActiveCityId()
    {
        $cities         = $this->workerHasCities;
        $filteredCities = array_filter(empty($cities) ? [] : $cities, function ($city) {
            return !empty($city['last_active']);
        });

        $lastActiveCity = current(empty($filteredCities) ? $cities : $filteredCities);

        return isset($lastActiveCity['city_id']) ? $lastActiveCity['city_id'] : null;
    }

    public function behaviors()
    {
        return ArrayHelper::merge([
            'fileBehavior' => [
                'class'      => CropFileBehavior::className(),
                'fileField'  => ['photo'],
                'upload_dir' => app()->params['upload'],
            ],
            [
                'class'      => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'birthday',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'birthday',
                ],
                'value'      => [$this, 'getBirthday'],
            ],
        ], parent::behaviors());
    }

    public function getBirthday()
    {
        if ($this->birth_year && $this->birth_month && $this->birth_day) {
            return $this->getFormatedBirth();
        }

        return null;
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->loadBirthParams();

    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->tenant_id = User::getCurrentTenantId();
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $newCityIds    = array_diff($this->getCityIds(), ArrayHelper::getColumn($this->cities, 'city_id'));
        $insertCityIds = array_intersect($newCityIds, $this->availableCities);
        $deleteCityIds = array_diff($this->availableCities, $this->getCityIds());

        WorkerHasCity::deleteAll([
            'worker_id' => $this->worker_id,
            'city_id'   => $deleteCityIds,
        ]);
        WorkerHasCity::saveMany($this->worker_id, $insertCityIds);

        if ($insert) {
            TenantHelper::fillHelpItem(TenantHelper::WORKER_FILL);
        }
        if (!$insert) {
            TagDependency::invalidate(Yii::$app->cache, self::TAG_DEPENDENCY_PREFIX . $this->callsign . '_' . $this->tenant_id);
        }
    }

    public function rules()
    {
        return ArrayHelper::merge([
            [
                'photo',
                'image',
                'extensions' => ['png', 'jpeg', 'jpg'],
                'maxSize'    => getUploadMaxFileSize(),
                'tooBig'     => t('file', 'The file size must be less than {file_size}Mb',
                    ['file_size' => getUploadMaxFileSize(true)]),
                'minWidth'   => 300,
                'minHeight'  => 300,
            ],
            [
                ['password', 'password_repeat'],
                'required',
                'on' => self::SCENARIO_CALLSIGN,
            ],
            [['callsign'], 'integer', 'max' => 999999, 'on' => self::SCENARIO_CALLSIGN],
            ['password_repeat', 'compare', 'compareAttribute' => 'password'],
            [['birth_day', 'birth_month', 'birth_year'], 'string'],
            ['callsign', 'trim'],
            ['cityIds', 'required'],
            ['cropParams', 'safe'],
        ], parent::rules());
    }

    /**
     * Type of collaboration with the taxi company
     * @return array
     */
    public function getPartnerTypes()
    {
        return [
            'PARTNER' => t('employee', 'Parnter'),
            'STAFF'   => t('employee', 'Staff'),
        ];
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge([
            'passport_serial' => Yii::t('employee', 'Passport Serial'),
            'passport_number' => Yii::t('employee', 'Passport Number'),
            'passport_issued' => Yii::t('employee', 'Passport issued'),
            'address_registr' => Yii::t('employee', 'Address registration'),
            'address_fact'    => Yii::t('employee', 'Actual address'),
            'password_repeat' => Yii::t('app', 'Password confirm'),
            'cityIds'         => Yii::t('employee', 'City Ids'),
        ], parent::attributeLabels());
    }

    public function getFormatedBirth()
    {
        return DateTimeHelper::getFormattedDateByParts($this->birth_day, $this->birth_month, $this->birth_year);
    }

    protected function loadBirthParams()
    {
        DateTimeHelper::loadDatePartsFromDate($this->birth_day, $this->birth_month, $this->birth_year, $this->birthday);
    }

    public function getShortName()
    {
        $name = $this->last_name;

        if (!empty($this->name)) {
            $name .= ' ' . mb_substr($this->name, 0, 1, "UTF-8") . '.';
        }

        if (!empty($this->second_name)) {
            $name .= mb_substr($this->second_name, 0, 1, "UTF-8") . '.';
        }

        return $name;
    }

    public function getLogo()
    {
        return $this->isFileExists($this->photo) ? $this->getPictureHtml($this->photo, false) : '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerHasDocuments()
    {
        return $this->hasMany(WorkerHasDocument::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositions()
    {
        return $this->hasMany(Position::className(),
            ['position_id' => 'position_id'])->viaTable(WorkerHasPosition::tableName(), ['worker_id' => 'worker_id'],
            function ($query) {
                return $query->where(['active' => 1]);
            });
    }

    /**
     * Get worker accounts
     * @return ActiveQuery
     */
    public function getAccounts()
    {
        return $this->hasMany(Account::className(), [
            'owner_id'  => 'worker_id',
            'tenant_id' => 'tenant_id',
        ])->andOnCondition([
            'acc_kind_id' => Account::WORKER_KIND,
            'acc_type_id' => Account::PASSIVE_TYPE,
        ]);
    }

    /**
     * Get client account by currency_id
     *
     * @param int $currencyId
     *
     * @return Account|null
     */
    public function getAccountByCurrencyId($currencyId)
    {
        return Account::findOne([
            'owner_id'    => $this->worker_id,
            'tenant_id'   => $this->tenant_id,
            'acc_kind_id' => Account::WORKER_KIND,
            'acc_type_id' => Account::PASSIVE_TYPE,
            'currency_id' => $currencyId,
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(WorkerHasCar::className(),
            ['has_position_id' => 'id'])->viaTable(WorkerHasPosition::tableName(), ['worker_id' => 'worker_id']);
    }

    public function getClientWorkerLike()
    {
        return $this->hasMany(ClientWorkerLike::class, ['worker_id' => 'worker_id']);
    }

    public function getLastWorkerShift($worker_id){
        return WorkerShift::find()->where(['worker_id' =>$worker_id ])->orderBy(['start_work' => SORT_DESC])->one();
    }


}