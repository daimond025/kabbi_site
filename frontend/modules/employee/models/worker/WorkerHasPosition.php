<?php


namespace frontend\modules\employee\models\worker;


use common\modules\employee\models\worker\WorkerHasPosition as WorkerHasPositionBase;

class WorkerHasPosition extends WorkerHasPositionBase
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(WorkerHasDocument::className(), ['has_position_id' => 'id']);
    }
}