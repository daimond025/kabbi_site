<?php

namespace frontend\modules\employee\models\worker;

use app\modules\tenant\models\User;
use frontend\modules\companies\components\repositories\TenantCompanyRepository;
use frontend\modules\companies\components\services\TenantCompanyService;
use frontend\modules\companies\models\TenantCompany;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\db\ActiveQuery;

/**
 * WorkerSearch represents the model behind the search form about `frontend\modules\employee\models\worker\Worker`.
 *
 * @property $accessCityList
 */
class WorkerSearch extends Worker
{
    const PAGE_SIZE = 20;

    public $formName = null;
    public $stringSearch;
    public $onlyOnline;
    public $positionList;
    public $positionActivity = null;
    public $city_id;
    private $_accessCityList = [];
    public $type = 'active';
    public $needSearchByCar = false;
    public $groupWorkersList;
    public $carList;
    public $tenantCompanyIds;
    public $callNow;
    public $clientIdHasBlocked;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stringSearch'], 'string'],
            [['onlyOnline'], 'integer'],
            [['city_id'], 'in', 'range' => $this->getAccessCityList(), 'allowArray' => true],
            [
                ['tenantCompanyIds'],
                'each',
                'rule' => [
                    'match',
                    'pattern' => sprintf('/^\d+|%s$/', TenantCompany::FIELD_NAME_MAIN_COMPANY),
                ],
            ],
            [['positionList', 'groupWorkersList', 'carList'], 'safe'],
            ['clientIdHasBlocked', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'positionList'     => t('employee', 'Profession'),
            'city_id'          => t('user', 'All cities'),
            'stringSearch'     => t('employee', 'Search by name, phone, call sign or car brand, model, number'),
            'onlyOnline'       => t('employee', 'Online workers'),
            'groupWorkersList' => t('employee', 'Worker group'),
            'carList'          => t('employee', 'Car class'),
            'tenantCompanyIds' => t('tenant_company', 'Companies'),
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /** @var $query ActiveQuery */
        $query = parent::find()->alias('t');

        // add conditions that should always apply here

        $query
            ->distinct()
            ->joinWith(['workerHasCities wc', 'cars.car cc', 'workerHasPositions wp', 'clientWorkerLike like'])
            ->where([
                't.tenant_id' => user()->tenant_id,
            ])
            ->andWhere(['or', ['wc.city_id'  => $this->getAccessCityList()], ['wc.city_id' => null]]);

        switch ($this->type) {
            // Заблокированные
            case self::BLOCKED:
                $query->andWhere([
                    't.block'    => 1,
                    't.activate' => 1,
                ]);
                break;
            // Не активированные
            case self::NOT_ACTIVATED:
                $query->andWhere([
                    't.block'    => 1,
                    't.activate' => 0,
                ]);
                break;
            // Активные
            case self::ACTIVE:
                // ..и остальные
            default:
                $query->andWhere([
                    't.block' => 0,
                ]);
        }

        $query->andFilterWhere([
            't.block' => $this->block,
        ]);

        $query->with([
            'cars.car'  => function ($subQuery) {
                $subQuery->select(['car_id', 'name', 'gos_number', 'group_id']);
            },
            'positions' => function ($subQuery) {
                $subQuery->select(['position_id', 'name']);
            },
            'accounts'  => function ($subQuery) {
                $subQuery->select(['tenant_id', 'owner_id', 'balance', 'currency_id']);
            },

        ]);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'sort'       => [
                'defaultOrder' => [
                    'fullName' => SORT_ASC,
                ],
                'attributes'   => [
                    'fullName' => [
                        'asc'  => ['last_name' => SORT_ASC, 'name' => SORT_ASC, 'second_name' => SORT_ASC],
                        'desc' => ['last_name' => SORT_DESC, 'name' => SORT_DESC, 'second_name' => SORT_DESC],
                    ],
                    'callsign',
                ],
            ],
            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);

        $this->load($params, $this->formName);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->clientIdHasBlocked) {
            $query->andWhere([
                'like.client_id' => $this->clientIdHasBlocked,
            ]);
            $query->andWhere(['>', 'like.dislike_count', 0]);
        }

        if ($this->onlyOnline) {
            $query->andWhere(['t.worker_id' => Worker::getOnlineWorkersId()]);
        }

        $query->andWhere(['or', ['wc.city_id'  => $this->getSearchCityList()], ['wc.city_id' => null]]);
        $phone = strlen($this->stringSearch) > 5 ? $this->stringSearch : null;

        if (!empty($this->stringSearch)) {
            $searchQueryParams = [
                'or',
                ['like', 't.last_name', $this->stringSearch],
                ['like', 't.name', $this->stringSearch],
                ['like', 't.second_name', $this->stringSearch],
                ['like', 't.callsign', $this->stringSearch],
                ['like', 't.phone', $phone],
            ];

            if ($this->needSearchByCar) {
                $searchQueryParams[] = [
                    'exists',
                    WorkerHasPosition::find()
                        ->select('whp.position_id')
                        ->alias('whp')
                        ->innerJoinWith('cars c')
                        ->onCondition('t.worker_id=whp.worker_id')
                        ->andWhere(['whp.active' => 1])
                        ->andFilterWhere([
                            'or',
                            ['like', 'c.name', $this->stringSearch],
                            ['like', 'c.gos_number', $this->stringSearch],
                        ]),
                ];
            }

            $query->andFilterWhere($searchQueryParams);
        }


        $workersFilteredByPosition = [];

        if (!empty($this->positionList)) {
            $query->andFilterWhere([
                'wp.position_id' => $this->positionList,
                'wp.active'      => $this->positionActivity,
            ]);
            $subquery = self::find()->alias('t')->select('t.worker_id')->joinWith(['workerHasPositions wp'])->where(['wp.position_id' => $this->positionList])->asArray()->all();

            $workersFilteredByPosition = ArrayHelper::getColumn($subquery, 'worker_id');

        }
        if (!empty($this->positionList)) {
            if (!empty($this->groupWorkersList)) {
                $subquery                          = self::find()->alias('t')->joinWith([
                    'workerHasCities wc',
                    'cars.car cc',
                    'workerHasPositions wp',
                ])->select('t.worker_id')->where(['t.worker_id' => $workersFilteredByPosition])
                    ->andWhere([
                        'OR',
                        ['cc.group_id' => $this->groupWorkersList],
                        ['wp.group_id' => $this->groupWorkersList],
                    ])->asArray()->all();

                $workersFilteredByPositionAndGroup = ArrayHelper::getColumn($subquery, 'worker_id');

                $query->andWhere(
                    ['t.worker_id' => $workersFilteredByPositionAndGroup]
                );

            }
        } else {
            $query->andFilterWhere([
                'OR',
                ['cc.group_id' => $this->groupWorkersList],
                ['wp.group_id' => $this->groupWorkersList],
            ]);
        }


        $query->andFilterWhere([
            'in',
            'cc.class_id',
            $this->carList,
        ]);
        (new TenantCompanyService())
            ->queryChange($query, $this->tenantCompanyIds, 't.tenant_company_id');

        return $dataProvider;
    }

    public function getAccessCityList()
    {
        return $this->_accessCityList;
    }

    public function setAccessCityList($value)
    {
        $this->_accessCityList = $value;
    }

    public function beforeValidate()
    {
        if (app()->user->can(User::ROLE_STAFF_COMPANY)) {
            $this->tenantCompanyIds = [user()->tenant_company_id];
        }
        return parent::beforeValidate();
    }

    private function getSearchCityList()
    {
        return $this->city_id ? $this->city_id : $this->getAccessCityList();
    }

}
