<?php


namespace frontend\modules\employee\models\worker;

use common\helpers\DateTimeHelper;
use frontend\modules\employee\components\worker\WorkerShiftService;
use Yii;
use common\modules\employee\models\worker\WorkerShift as WorkerShiftBase;

class WorkerShift extends WorkerShiftBase
{
    /**
     * Количество выполненных, оплаченных заказов за смену.
     * @var integer
     */
    public $compleatedOrdersCnt = 0;
    /**
     * Количество отмененных заказов за смену.
     * @var integer
     */
    public $rejectedOrdersCnt = 0;
    /**
     * Заработанная сумма за заказы.
     * @var array
     */
    public $ordersSummaryCost;
    /**
     * Общее время пауз водителя.
     * @var string H:m:s
     */
    private $pauseTime;

    public function afterFind()
    {
        parent::afterFind();
        try {
            $this->pause_data = unserialize($this->pause_data);
        } catch (\Exception $exc) {
            Yii::error('Ошибка десериализации паузы исполнителя. Id записи - ' . $this->id, 'worker_shift');
        }
    }

    /**
     * Total worker pause time
     * @return string H:m:s
     */
    public function getPauseTime()
    {
        if (empty($this->pauseTime)) {
            $pause_sec = 0;
            if (!empty($this->pause_data)) {
                foreach ($this->pause_data as $pause) {
                    if (!empty($pause['pause_end'])) {
                        $pause_sec += $pause['pause_end'] - $pause['pause_start'];
                    } else {
                        $pause_end = empty($this->end_work) ? time() : $this->end_work;
                        $pause_sec += $pause_end - $pause['pause_start'];
                    }
                }
            }
            $this->pauseTime = DateTimeHelper::getFormatTimeFromSeconds($pause_sec);
        }

        return $this->pauseTime;
    }

    /**
     * Общее время блокировки и паузы водителя в массиве.
     * @return string H:m:s
     */
    public function getShiftList()
    {
        $shift = [];

        if (!empty($this->pause_data)) {
            foreach ($this->pause_data as $pause) {
                $pauseStart = $pause['pause_start'];
                if (!empty($pause['pause_end'])) {
                    $pauseEnd = $pause['pause_end'];
                } elseif (!empty($this->end_work)) {
                    $pauseEnd = $this->end_work;
                } else {
                    $pauseEnd = time();
                }
                $shift[] = [
                    'type'         => 'pause',
                    'start_time'   => $pauseStart,
                    'end_time'     => $pauseEnd,
                    'pause_reason' => $pause['pause_reason'],
                ];
            }
        }

        $blocks = $this->blockList;
        foreach ($blocks as $block) {
            $shift[] = [
                'type'       => 'block',
                'start_time' => $block['start_block'],
                'end_time'   => $block['end_block'],
                'type_block' => $block['type_block'],
            ];
        }

        usort($shift, function ($a, $b) {
            if ($a == $b) {
                return 0;
            }

            return ($a < $b) ? -1 : 1;
        });

        return $shift;
    }

    /**
     * Block time list
     * @return array
     */
    public function getBlockList()
    {
        /** @var WorkerShiftService $workerShiftService */
        $workerShiftService = Yii::$app->getModule('employee')->get('workerShift');

        return $workerShiftService->getBlockList($this->id);
    }

    /**
     * Total block time
     * @return string H:m:s
     */
    public function getBlockTime()
    {
        $blocks = $this->getBlockList();
        $blockTime = 0;
        foreach ($blocks as $block) {
            $blockTime += $block['end_block'] - $block['start_block'];
        }

        return DateTimeHelper::getFormatTimeFromSeconds($blockTime);
    }
}