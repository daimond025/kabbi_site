<?php

namespace frontend\modules\employee\models\worker;


use common\modules\employee\models\worker\WorkerHasDocument as WorkerHasDocumentBase;
use frontend\modules\employee\models\documents\WorkerDocumentScan;

class WorkerHasDocument extends WorkerHasDocumentBase
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentScans()
    {
        return $this->hasMany(WorkerDocumentScan::className(), ['worker_document_id' => 'id']);
    }
}