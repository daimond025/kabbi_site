<?php

namespace frontend\modules\employee\models\worker;

use common\modules\employee\models\position\Position;
use common\modules\employee\models\worker\WorkerHasPosition;
use Yii;

/**
 * This is the model class for table "{{%worker_review_rating}}".
 *
 * @property integer           $rating_id
 * @property integer           $worker_id
 * @property integer           $one
 * @property integer           $two
 * @property integer           $three
 * @property integer           $four
 * @property integer           $five
 * @property integer           $count
 * @property integer           $position_id
 *
 * @property WorkerHasPosition $workerPosition
 * @property Worker            $worker
 */
class WorkerReviewRating extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_review_rating}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['worker_id', 'position_id'], 'required'],
            [['worker_id', 'one', 'two', 'three', 'four', 'five', 'count', 'position_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rating_id'   => 'Rating ID',
            'worker_id'   => 'Worker ID',
            'one'         => 'One',
            'two'         => 'Two',
            'three'       => 'Three',
            'four'        => 'Four',
            'five'        => 'Five',
            'count'       => 'Count',
            'position_id' => 'Position ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }
}
