<?php

namespace frontend\modules\employee\models\tariff;

use yii\db\ActiveQuery;

class OptionTariffCurrent extends OptionTariff
{
    /**
     * @param $id
     * @return ActiveQuery
     */
    public static function findByTariff ($id){
        return self::find()->where([
            'tariff_id' => $id,
            'tariff_type' => self::TYPE_CURRENT
        ]);
    }
}

