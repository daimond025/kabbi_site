<?php

namespace frontend\modules\employee\models\tariff;

use Yii;

/**
 * This is the model class for table "{{%worker_option_discount}}".
 *
 * @property integer $discount_id
 * @property integer $car_option_id
 * @property integer $option_id
 * @property string $discount_line_type
 * @property string $discount_order_type
 * @property string $discount_line
 * @property string $discount_order
 *
 * @property CarOption $carOption
 */
class OptionDiscount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_option_discount}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_option_id', 'option_id'], 'required'],
            [['car_option_id', 'option_id'], 'integer'],
            [['discount_line_type', 'discount_order_type'], 'string'],
            [['discount_line', 'discount_order'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'discount_id'         => 'Discount ID',
            'car_option_id'       => 'Car Option ID',
            'option_id'           => 'Option ID',
            'discount_line_type'  => 'Discount Line Type',
            'discount_order_type' => 'Discount Order Type',
            'discount_line'       => 'Discount Line',
            'discount_order'      => 'Discount Order',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarOption()
    {
        return $this->hasOne(CarOption::className(), ['option_id' => 'car_option_id']);
    }

    /**
     * Allow to save multiple rows
     * @param integer $option_id
     * @param array $arData
     * @return null|integer Number of rows affected by the execution.
     * @throws Exception execution failed
     */
    public static function manySave($option_id, array $arData)
    {
        if (empty($arData)) {
            return null;
        }

        $insertValue = [];
        $connection = app()->db;

        foreach ($arData as $row) {
            $insertValue[] = [
                $row['id'],
                $option_id,
                $row['discount_line_type'],
                $row['discount_order_type'],
                $row['discount_line'],
                $row['discount_order'],
            ];
        }

        return $connection->createCommand()->batchInsert(self::tableName(), [
            'car_option_id',
            'option_id',
            'discount_line_type',
            'discount_order_type',
            'discount_line',
            'discount_order',
        ], $insertValue)->execute();
    }
}
