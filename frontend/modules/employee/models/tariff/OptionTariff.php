<?php

namespace frontend\modules\employee\models\tariff;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%worker_option_tariff}}".
 *
 * @property integer            $option_id
 * @property integer            $tariff_id
 * @property string             $tariff_type
 * @property string             $active_date
 * @property string             $increase_order_sum
 * @property string             $increase_sum_fix
 * @property string             $increase_sum_fix_type
 * @property string             $increase_sum_limit
 * @property int                sort
 *
 * @property Tariff             $tariff
 * @property TariffCommission[] $tariffCommissions
 */
class OptionTariff extends ActiveRecord
{
    public $arOptionDiscount = [];
    public $arCommission = [];

    const TYPE_CURRENT = 'CURRENT';
    const TYPE_EXCEPTIONS = 'EXCEPTIONS';


    private $temporaryId;

    /**
     * @return string
     */
    public function getTemporaryId()
    {
        $this->temporaryId = $this->temporaryId === null ? 'NEW_' . random_int(100, 999) : $this->temporaryId;

        return $this->temporaryId;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_option_tariff}}';
    }

    /**
     * @inheritdoc Array[period][when]
     */
    public function rules()
    {
        return [
            [['tariff_id', 'tariff_type'], 'required'],
            [['tariff_id', 'sort'], 'integer'],
            [['tariff_type'], 'string'],
            [['active_date'], 'string', 'max' => 255],
            [
                ['active_date'],
                'required',
                'when'       => function ($model) {
                    return $model->tariff_type !== self::TYPE_CURRENT;
                },
                'whenClient' => 'function(attr,value){
                    var model = attr.input.replace("-" + attr.name, "");
                     console.log(model, !$(model + "-tariff_type").prop("checked"));
                    return !$(model + "-tariff_type").prop("checked");
                }',
            ],
            [
                ['increase_order_sum', 'increase_sum_limit', 'increase_sum_fix'],
                'number',
                'min' => 0,
                'max' => 99999,
            ],
            [['increase_sum_fix_type'], 'string'],
            [['sort'], 'default', 'value' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'option_id'          => 'Option ID',
            'tariff_id'          => 'Tariff ID',
            'tariff_type'        => 'Tariff Type',
            'active_date'        => 'Active Date',
            'sort'               => t('driverTariff', 'Sort'),
            'commission'         => t('driverTariff', 'Commission'),
            'increase_order_sum' => t('driverTariff', 'Increase the value of the order to'),
            'increase_sum_fix'   => t('driverTariff', 'Fixed surcharge'),
            'increase_sum_limit' => t('driverTariff', 'For orders of less than'),
        ];
    }

    public static function getTariffTypeNameByType($tariff_type)
    {
        $arTariffTypes = [
            self::TYPE_CURRENT    => t('taxi_tariff', self::TYPE_CURRENT),
            self::TYPE_EXCEPTIONS => t('taxi_tariff', self::TYPE_EXCEPTIONS),
        ];

        return getValue($arTariffTypes[$tariff_type]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptionDiscount()
    {
        return $this->hasMany(OptionDiscount::className(), ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffCommissions()
    {
        return $this->hasMany(TariffCommission::className(), ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(Tariff::className(), ['tariff_id' => 'tariff_id']);
    }

    // Получаем список доп.опций для данного тарифа
    public function getDiscountOptionList()
    {
        // если новая запись, то возвращаем пустой массив
        if ($this->isNewRecord) {
            return [];
        }
        // ..иначе
        $dis = OptionDiscount::find()
            ->where(['option_id' => $this->option_id])
            ->asArray()
            ->all();
        $d   = [];
        foreach ($dis as $item) {
            $d[$item['car_option_id']] = $item;
        }

        return $d;
    }

    public function saveOptions($city_id, $options = [])
    {
        if (!$this->isNewRecord) {
            OptionDiscount::deleteAll(['option_id' => $this->option_id]);
        }

        if (is_array($options)) {
            $activeCarOptionList = Tariff::getActiveCarOptionList($city_id);
            $filteredCarOptions  = array_filter($options, function ($value, $key) use ($activeCarOptionList) {
                return in_array($key, $activeCarOptionList);
            }, ARRAY_FILTER_USE_BOTH);

            return OptionDiscount::manySave($this->option_id, $filteredCarOptions);
        }

        return null;
    }

    /**
     * Getting prepared fixed commission
     *
     * @param TariffCommission $commission
     *
     * @return TariffCommission
     */
    public function getPreparedFixedCommission($commission)
    {
        if ($commission === null) {
            $commission = new TariffCommission([
                'option_id' => $this->option_id,
                'kind'      => TariffCommission::KIND_FIXED,
            ]);
            $commission->loadDefaultValues();
        }
        $commission->from = 0;
        $commission->to   = null;

        return $commission;
    }

    /**
     * Getting prepared interval commissions
     *
     * @param array $groupedCommissions
     *
     * @return array
     */
    private function getPreparedIntervalCommissions($groupedCommissions)
    {
        $result = [];
        foreach (TariffCommission::KIND_ALL as $kind) {
            if ($kind === TariffCommission::KIND_FIXED) {
                continue;
            }

            $result[$kind] = [];

            $commissions     = empty($groupedCommissions[$kind]) ? [] : $groupedCommissions[$kind];
            $fixedCommission = count($commissions) > 1 || empty($commissions[0]) ? null : $commissions[0];

            $result[$kind][] = count($commissions) > 1;
            $result[$kind][] = $this->getPreparedFixedCommission($fixedCommission);

            $to                  = null;
            $intervalCommissions = [];
            for ($i = 0, $l = TariffCommission::getIntervalCommissionMaxCount(); $i < $l; $i++) {
                if (empty($commissions[$i]) || count($commissions) === 1) {
                    $commission = new TariffCommission([
                        'option_id' => $this->option_id,
                        'kind'      => $kind,
                    ]);
                    $commission->loadDefaultValues();
                    $commission->from = $to;
                } else {
                    $commission = $commissions[$i];
                }
                if ($i === 0) {
                    $commission->from = 0;
                }

                $intervalCommissions[$i] = $commission;

                $to = $commission->to;
            }
            $result[$kind][] = $intervalCommissions;
        }

        return $result;
    }

    /**
     * Getting commission data
     *
     * @return array
     */
    public function getCommissionData()
    {
        $groupedCommissions = empty($this->tariffCommissions)
            ? [] : ArrayHelper::index($this->tariffCommissions, null, 'kind');

        $fixedCommission = empty($groupedCommissions) || empty($groupedCommissions[TariffCommission::KIND_FIXED])
            ? null : current($groupedCommissions[TariffCommission::KIND_FIXED]);

        $isFixedCommission   = empty($groupedCommissions) || !empty($groupedCommissions[TariffCommission::KIND_FIXED]);
        $fixedCommission     = $this->getPreparedFixedCommission($fixedCommission);
        $intervalCommissions = $this->getPreparedIntervalCommissions($groupedCommissions);

        return [$isFixedCommission, $fixedCommission, $intervalCommissions];
    }

    /**
     * Saving tariff commission
     *
     * @param array $data
     *
     * @throws \yii\base\Exception
     */
    public function saveTariffCommission($data = [])
    {
        if (!$this->isNewRecord) {
            TariffCommission::deleteAll(['option_id' => $this->option_id]);
        }
        TariffCommission::manySave($this->option_id, $data);
    }

}
