<?php

namespace frontend\modules\employee\models\tariff;

use common\components\behaviors\ActiveRecordBehavior;
use common\modules\city\models\City;
use common\modules\employee\models\position\Position;
use common\modules\tenant\models\Tenant;
use common\modules\tenant\models\TenantHelper;
use frontend\modules\car\models\CarClass;
use frontend\modules\car\models\CarOption;
use frontend\modules\companies\components\repositories\TenantCompanyRepository;
use frontend\modules\companies\models\TenantCompany;
use frontend\modules\employee\components\position\PositionService;
use frontend\modules\employee\Module;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%worker_tariff}}".
 *
 * @property integer         $tariff_id
 * @property integer         $tenant_id
 * @property integer         $class_id
 * @property integer         $position_id
 * @property string          $name
 * @property string          $type
 * @property integer         $block
 * @property integer         $days
 * @property string          $subscription_limit_type
 * @property string          $period_type
 * @property string          $period
 * @property string          $cost
 * @property integer         $tenant_company_id
 *
 * @property OptionTariff[]  $optionTariffs
 * @property CarClass        $class
 * @property Tenant          $tenant
 * @property TariffHasCity[] $tariffHasCities
 * @property City[]          $cities
 * @property TenantCompany   $tenantCompany
 */
class Tariff extends ActiveRecord
{
    const TYPE_SUBSCRIPTION = 'SUBSCRIPTION';
    const TYPE_ONCE = 'ONCE';

    const PERIOD_TYPE_INTERVAL = 'INTERVAL';
    const PERIOD_TYPE_HOUR = 'HOURS';

    const SUBSCRIPTION_LIMIT_TYPE_SHIFT = 'SHIFT_COUNT';
    const SUBSCRIPTION_LIMIT_TYPE_DAY = 'DAY_COUNT';

    const ACTION_NEW_SHIFT_CONTINUE_PERIOD = 'CONTINUE_PERIOD';
    const ACTION_NEW_SHIFT_NEW_PERIOD = 'NEW_PERIOD';

    const MIN_DAYS = 1;
    const MIN_HOUR = 1;
    const MAX_HOUR = 24;

    const DEFAULT_START_INTERVAL_VALUE = '08:00';
    const DEFAULT_END_INTERVAL_VALUE = '20:00';
    const DEFAULT_HOUR_VALUE = 12;

    const SCENARIO_INSERT = 'scenarioInsert';

    public $city_list = [];

    public $start_interval = self::DEFAULT_START_INTERVAL_VALUE;
    public $end_interval = self::DEFAULT_END_INTERVAL_VALUE;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_tariff}}';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'common' => [
                'class' => ActiveRecordBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'position_id', 'period_type', 'period'], 'required'],
            [['tariff_id', 'tenant_id', 'class_id', 'block', 'position_id', 'tenant_company_id'], 'integer'],
            [['type'], 'string', 'on' => self::SCENARIO_INSERT],
            [['name'], 'string', 'max' => 45],
            [['description'], 'string', 'max' => 200],
            [['action_new_shift'], 'in', 'range' => [
                self::ACTION_NEW_SHIFT_CONTINUE_PERIOD,
                self::ACTION_NEW_SHIFT_NEW_PERIOD
            ]],
            [
                ['days', 'subscription_limit_type'],
                'required',
                'when'       => $daysWhen = function ($model) {
                    return $model->type === self::TYPE_SUBSCRIPTION;
                },
                'whenClient' => $daysWhenClient = 'function(attr,value){
                    var model = attr.input.replace("-" + attr.name, "");
                    return $(model + "-type").val() == "' . self::TYPE_SUBSCRIPTION . '";
                }',
            ],
            [
                ['days'],
                'integer',
                'min'        => self::MIN_DAYS,
                'when'       => $daysWhen,
                'whenClient' => $daysWhenClient,
            ],
            [
                ['class_id'],
                'required',
                'when'       => function ($model) {
                    return in_array($model->position_id,
                        Module::getInstance()->get('position')->getPositionIdsHasCar(), false);
                },
                'whenClient' => 'function(attr,value){
                    var position_id = $("#tariff-position_id").val();
                    return $("[data-position=\'" + position_id + "\']").size() > 0;
                }',
            ],
            [['cost'], 'number', 'min' => 0],
            [
                'period',
                'required',
                'when'       => $periodWhen = function ($model) {
                    return $model->period_type === self::PERIOD_TYPE_HOUR;
                },
                'whenClient' => $periodWhenClient = 'function(attr,value){
                    var model = attr.input.replace("-" + attr.name, "");
                    return $(model + "-period_type > label > [value=\"' . self::PERIOD_TYPE_HOUR . '\"]").prop("checked");
                }',
            ],
            [
                'period',
                'integer',
                'max'        => self::MAX_HOUR,
                'min'        => self::MIN_HOUR,
                'when'       => $periodWhen,
                'whenClient' => $periodWhenClient,
            ],
            [
                ['start_interval', 'end_interval'],
                'required',
                'when'       => $intervalWhen = function ($model) {
                    return $model->period_type === self::PERIOD_TYPE_INTERVAL;
                },
                'whenClient' => $intervalWhenClient = 'function(attr,value){
                    var model = attr.input.replace("-" + attr.name, "");
                    return $(model + "-period_type > label > [value=\"' . self::PERIOD_TYPE_INTERVAL . '\"]").prop("checked");
                }',
            ],
            [
                ['start_interval', 'end_interval'],
                'match',
                'pattern'    => '/^([0-1][0-9]|2[0-3]):[0-5][0-9]$/',
                'when'       => $intervalWhen,
                'whenClient' => $intervalWhenClient,
            ],
            [
                ['end_interval'],
                'compare',
                'compareAttribute' => 'start_interval',
                'operator'         => '!=',
                'when'             => $intervalWhen,
                'whenClient'       => $intervalWhenClient,
            ],
            [['cost'], 'default', 'value' => 0],
            ['type', 'default', 'value' => self::TYPE_ONCE, 'on' => self::SCENARIO_INSERT],
            ['subscription_limit_type', 'default', 'value' => self::SUBSCRIPTION_LIMIT_TYPE_SHIFT],
            ['period_type', 'default', 'value' => self::PERIOD_TYPE_INTERVAL],
            [
                'period',
                'default',
                'value' => implode('-', [self::DEFAULT_START_INTERVAL_VALUE, self::DEFAULT_END_INTERVAL_VALUE]),
            ],
            ['city_list', 'safe'],
            [
                ['tenant_company_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => TenantCompany::className(),
                'targetAttribute' => ['tenant_company_id' => 'tenant_company_id'],
            ],
            [['action_new_shift'], 'filter', 'filter' => function ($value) {
                if ($this->type == self::TYPE_SUBSCRIPTION) {
                    return null;
                }

                return $value;
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tariff_id'               => 'Tariff ID',
            'tenant_id'               => 'Tenant ID',
            'class_id'                => t('driverTariff', 'Car class'),
            'position_id'             => t('employee', 'Profession'),
            'name'                    => t('driverTariff', 'Name'),
            'type'                    => t('driverTariff', 'Tariff type'),
            'days'                    => t('driverTariff', 'Days'),
            'description'             => t('driverTariff', 'Description'),
            'city_list'               => t('taxi_tariff', 'The city where tariff will work'),
            'subscription_limit_type' => t('driverTariff', 'How to calculate the subscription period'),
            'period_type'             => t('driverTariff', 'Work period'),
            'period'                  => 'Period',
            'cost'                    => t('driverTariff', 'Cost'),
            'cost_of_entry_per_shift' => t('driverTariff', 'Cost of entry per shift'),
            'cost_of_subscription'    => t('driverTariff', 'Cost of subscription'),
            'block'                   => t('app', 'Block'),
            'tenant_company_id'       => t('tenant_company', 'Companies'),
            'action_new_shift'        => t('driverTariff', 'When you open a new shift'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrentOption()
    {
        return $this->hasOne(OptionTariff::className(),
            ['tariff_id' => 'tariff_id', OptionTariff::TYPE_CURRENT => 'tariff_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptionTariffs()
    {
        return $this->hasMany(OptionTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClass()
    {
        return $this->hasOne(CarClass::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffHasCity()
    {
        return $this->hasMany(TariffHasCity::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(),
            ['city_id' => 'city_id'])->viaTable('{{%worker_tariff_has_city}}', ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }

    public function getTenantCompany()
    {
        return $this->hasOne(TenantCompany::className(), ['tenant_company_id' => 'tenant_company_id']);
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        if ($this->period_type === self::PERIOD_TYPE_INTERVAL) {
            $array                = explode('-', $this->period);
            $this->start_interval = $array[0];
            $this->end_interval   = $array[1];
            $this->period         = self::DEFAULT_HOUR_VALUE;
        }

        parent::afterFind();
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->type === self::TYPE_ONCE) {
            $this->days                    = null;
            $this->subscription_limit_type = null;
        }

        if (!$this->position->has_car) {
            $this->class_id = null;
        }

        if ($this->period_type === self::PERIOD_TYPE_INTERVAL) {
            $this->period = $this->start_interval . '-' . $this->end_interval;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            TenantHelper::fillHelpItem(TenantHelper::WORKER_TARIFF_FILL);
        }
    }

    /**
     * @param $city_id
     *
     * @return array|ActiveRecord[]
     */
    private static function getActiveCarOptions($city_id)
    {
        return CarOption::find()
            ->joinWith([
                'tenantHasCarOptions' => function (ActiveQuery $q) use ($city_id) {
                    $q->where([
                        'is_active' => 1,
                        'tenant_id' => user()->tenant_id,
                        'city_id'   => $city_id,
                    ]);
                },
            ], false)
            ->all();
    }

    /**
     * @param $city_id
     *
     * @return array
     */
    public static function getActiveCarOptionList($city_id)
    {
        $addOptions = self::getActiveCarOptions($city_id);

        return ArrayHelper::getColumn($addOptions, 'option_id');
    }

    /**
     * @param $city_id
     *
     * @return array
     */
    public static function getActiveCarOptionMap($city_id)
    {
        $addOptions = self::getActiveCarOptions($city_id);

        return ArrayHelper::map($addOptions, 'option_id', function ($element) {
            return t('car-options', $element['name']);
        });
    }

    /**
     * @return array
     */
    public function getTariffTypes()
    {
        return [
            self::TYPE_ONCE         => t('driverTariff', 'Once'),
            self::TYPE_SUBSCRIPTION => t('driverTariff', 'Subscription'),
        ];
    }

    /**
     * Возращает массив доступных значений period для типа "количество дней"
     */
    public function getHoursList()
    {
        $h = [];
        for ($i = self::MIN_HOUR; $i <= self::MAX_HOUR; $i++) {
            $h[$i] = $i . ' ' . t('app', 'h');
        }

        return $h;
    }

    /**
     * @return array
     */
    public function getSubscriptionLimitTypeList()
    {
        return [
            self::SUBSCRIPTION_LIMIT_TYPE_SHIFT => t('driverTariff', 'Count of working shifts'),
            self::SUBSCRIPTION_LIMIT_TYPE_DAY   => t('driverTariff', 'Days count'),
        ];
    }

    /**
     * @return array
     */
    public function getPeriodTypeList()
    {
        return [
            self::PERIOD_TYPE_INTERVAL => t('driverTariff', 'Interval'),
            self::PERIOD_TYPE_HOUR     => t('driverTariff', 'Number of hours'),
        ];
    }

    /**
     * @return array
     */
    public function getActionNewShift()
    {
        return [
            self::ACTION_NEW_SHIFT_CONTINUE_PERIOD => t('driverTariff', 'Continue the incomplete period'),
            self::ACTION_NEW_SHIFT_NEW_PERIOD     => t('driverTariff', 'A new period begins'),
        ];
    }

    /**
     * @param null $position_id
     * @param null $city_id
     *
     * @return array
     */
    public function getFormData($position_id = null, $city_id = null)
    {
        /** @var PositionService $positionService */
        $positionService = Module::getInstance()->get('position');
        $positionService->setTenantId(user()->tenant_id);

        $arData = [
            'USER_CITY_LIST'    => user()->getUserCityList(),
            'TARIFF_TYPES'      => $this->getTariffTypes(),
            'ADD_OPTIONS'       => ArrayHelper::map(CarOption::find()->all(), 'option_id', function ($element) {
                return t('car-options', $element['name']);
            }),
            'CAR_POSITION_LIST' => $positionService->getPositionIdsHasCar(),
        ];

        $city_id                = $city_id ?: key($arData['USER_CITY_LIST']);
        $arData['POSITION_MAP'] = $positionService->getPositionMapByCity($city_id);

        $position_id = empty($position_id) ? key($arData['POSITION_MAP']) : $position_id;
        if (!empty($position_id)) {
            $transportTypeId          = $positionService->getPositionTransportType($position_id);
            $arData['CAR_CLASS_LIST'] = CarClass::getCarClassMapByTransportType($transportTypeId);
            $arData['CAR_CLASS_LIST'] = ArrayHelper::map($arData['CAR_CLASS_LIST'], 'class_id', 'class');
        } else {
            $arData['CAR_CLASS_LIST'] = [];
        }

        $arData['ACTIVE_ADD_OPTION_LIST'] = self::getActiveCarOptionList($city_id);


        if ($arData['TARIFF_TYPES'][$this->type] === 'Разовый') {
            $arData['LABELS']['COST'] = 'cost_of_entry_per_shift';
        } else if($arData['TARIFF_TYPES'][$this->type] === 'Абонемент'){
            $arData['LABELS']['COST'] = 'cost_of_subscription';
        }

        $arData['COMPANIES'] = TenantCompanyRepository::selfCreate()->getForForm([], false);

        return $arData;
    }

    /**
     * @param      $city_list
     * @param null $class_id
     * @param null $position_id
     *
     * @return array|ActiveRecord[]
     */
    public static function getDriverTariffs($city_list, $class_id = null, $position_id = null)
    {
        $positionIds = empty($position_id)
            ? Module::getInstance()->get('position')->getPositionIdsHasCar()
            : $position_id;

        return self::find()
            ->where([
                'tenant_id'   => user()->tenant_id,
                'block'       => 0,
                'position_id' => $positionIds,
            ])
            ->with('class')
            ->joinWith([
                'tariffHasCity' => function (ActiveQuery $query) use ($city_list) {
                    $query->where(['city_id' => $city_list]);
                },
            ], false)
            ->andFilterWhere(['class_id' => $class_id])
            ->select([self::tableName() . '.tariff_id', 'class_id', 'name'])
            ->asArray()
            ->all();
    }

    /**
     * @param int   $position_id
     * @param array $cityList
     * @param int   $block
     *
     * @return array
     */
    public static function getWorkerTariffsByPositionId($position_id, $cityList = [], $block = 0)
    {
        return self::find()
            ->where([
                'tenant_id'   => user()->tenant_id,
                'block'       => $block,
                'position_id' => $position_id,
            ])
            ->joinWith([
                'tariffHasCity' => function (ActiveQuery $query) use ($cityList) {
                    $query->andFilterWhere(['city_id' => $cityList]);
                },
            ], false)
            ->select([self::tableName() . '.tariff_id', 'name'])
            ->asArray()
            ->all();
    }
}
