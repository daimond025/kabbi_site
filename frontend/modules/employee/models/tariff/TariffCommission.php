<?php

namespace frontend\modules\employee\models\tariff;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Exception;

/**
 * This is the model class for table "tbl_tariff_commission".
 *
 * @property integer      $id
 * @property integer      $option_id
 * @property string       $type
 * @property double       $value
 * @property double       $from
 * @property double       $to
 * @property string       $kind
 * @property double       $tax
 *
 * @property OptionTariff $option
 */
class TariffCommission extends ActiveRecord
{
    const TYPE_PERCENT = 'PERCENT';
    const TYPE_MONEY = 'MONEY';

    const CLASS_UNITED = 'UNITED';
    const CLASS_EXTENDED = 'EXTENDED';

    const KIND_FIXED = 'FIXED';
    const KIND_CASH = 'CASH';
    const KIND_CARD = 'CARD';
    const KIND_PERSONAL_ACCOUNT = 'PERSONAL_ACCOUNT';
    const KIND_CORP_BALANCE = 'CORP_BALANCE';
    const KIND_BORDER = 'BORDER';

    const KIND_ALL = [
        self::KIND_FIXED,
        self::KIND_CASH,
        self::KIND_CARD,
        self::KIND_PERSONAL_ACCOUNT,
        self::KIND_CORP_BALANCE,
        self::KIND_BORDER,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_tariff_commission';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['option_id'], 'required'],
            [['option_id'], 'integer'],
            [['type', 'kind'], 'string'],
            [['value', 'from', 'to', 'tax'], 'number', 'min' => 0, 'max' => 99999],
            [['type'], 'default', 'value' => TariffCommission::TYPE_PERCENT],
            [['value'], 'default', 'value' => 0],
            [
                ['option_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => OptionTariff::className(),
                'targetAttribute' => ['option_id' => 'option_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'option_id' => 'Option ID',
            'type'      => 'Type',
            'value'     => 'Value',
            'from'      => 'From',
            'to'        => 'To',
            'kind'      => 'Kind',
            'tax'       => 'Tax',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOption()
    {
        return $this->hasOne(OptionTariff::className(), ['option_id' => 'option_id']);
    }

    /**
     * Getting interval commission count
     * @return int
     */
    public static function getIntervalCommissionMaxCount()
    {
        return \Yii::$app->getModule('employee')->params['intervalCommission.count'];
    }

    /**
     * Getting commission classes
     * @return array
     */
    public static function getCommissionClasses()
    {
        return [
            self::CLASS_UNITED   => t('commission', 'United'),
            self::CLASS_EXTENDED => t('commission', 'With types of payments and intervals'),
        ];
    }

    /**
     * Getting commission types
     *
     * @param int $cityId
     *
     * @return array
     */
    public static function getCommissionTypes($cityId)
    {
        return [
            'PERCENT' => '%',
            'MONEY'   => getCurrencySymbol($cityId),
        ];
    }

    /**
     * Saving commission
     *
     * @param int    $optionId
     * @param string $kind
     * @param float  $from
     * @param float  $to
     * @param float  $value
     * @param string $type
     * @param float  $tax
     *
     * @throws \yii\base\Exception
     */
    private static function saveCommission($optionId, $kind, $from, $to, $value, $type, $tax)
    {
        $commission = new TariffCommission([
            'option_id' => $optionId,
            'kind'      => $kind,
            'from'      => $from,
            'to'        => $to,
            'value'     => $value,
            'type'      => $type,
            'tax'       => $tax,
        ]);

        if (!empty($commission->from) || !empty($commission->to) || !empty($commission->value) || !empty($commission->tax)) {
            if (!$commission->save()) {
                throw new \yii\base\Exception(implode(', ', $commission->getFirstErrors()));
            }
        }
    }

    /**
     * Allow to save multiple rows
     *
     * @param integer $optionId
     * @param array   $data
     *
     * @return int number of rows affected by the execution.
     * @throws \yii\base\Exception
     */
    public static function manySave($optionId, array $data)
    {
        $isFixedCommission = empty($data['commission_class'])
            || $data['commission_class'] === TariffCommission::CLASS_UNITED;

        if ($isFixedCommission) {
            $commission = empty($data['kind'][TariffCommission::KIND_FIXED])
                ? [] : $data['kind'][TariffCommission::KIND_FIXED];
            $value      = isset($commission['value']) ? (float)$commission['value'] : null;
            $type       = isset($commission['type']) ? $commission['type'] : null;
            $isSetupTax = isset($commission['is_setup_tax']) ? $commission['is_setup_tax'] : null;
            $tax        = isset($commission['tax']) && $isSetupTax !== null ? (float)$commission['tax'] : null;

            self::saveCommission($optionId, TariffCommission::KIND_FIXED, 0, null, $value, $type, $tax);
        } elseif (!empty($data['kind'])) {
            foreach ($data['kind'] as $kind => $value) {
                if ($kind === TariffCommission::KIND_FIXED) {
                    continue;
                }

                $isSetupTax = isset($value['is_setup_tax']) ? $value['is_setup_tax'] : null;
                $tax        = isset($value['tax']) && $isSetupTax !== null ? (float)$value['tax'] : null;

                if (empty($value['is_interval_commission'])) {
                    $commission = empty($value[TariffCommission::CLASS_UNITED]) ? null : $value[TariffCommission::CLASS_UNITED];
                    $value      = isset($commission['value']) ? (float)$commission['value'] : null;
                    $type       = isset($commission['type']) ? $commission['type'] : null;

                    self::saveCommission($optionId, $kind, 0, null, $value, $type, $tax);
                } else {
                    $rows = empty($value[TariffCommission::CLASS_EXTENDED])
                        ? [] : $value[TariffCommission::CLASS_EXTENDED];
                    uasort($rows, function ($a, $b) {
                        return ($a['to'] === '' || (float)$a['to'] > (float)$b['to']) ? 1 : -1;
                    });

                    $from = 0;
                    foreach ($rows as $row) {
                        $to    = empty($row['to']) ? null : (float)$row['to'];
                        $value = isset($row['value']) ? (float)$row['value'] : null;
                        $type  = isset($row['type']) ? $row['type'] : null;

                        self::saveCommission($optionId, $kind, $from, $to, $value, $type, $tax);

                        $from = $to;
                        if (empty($row['to'])) {
                            break;
                        }
                    }
                }
            }
        }
    }
}
