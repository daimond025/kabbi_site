<?php

namespace frontend\modules\employee\models\tariff;


use app\modules\tenant\models\User;
use frontend\modules\companies\components\repositories\TenantCompanyRepository;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * TariffSearch represents the model behind the search form about `frontend\modules\employee\models\tariff\Tariff`.
 *
 * @property array $accessCityList
 */
class TariffSearch extends Tariff
{
    const PAGE_SIZE = 20;

    public $tenant_company_id;

    private $_accessCityList = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_list'], 'in', 'range' => $this->getAccessCityList(), 'allowArray' => true],
            [['position_id'], 'safe'],
            [
                ['tenant_company_id'],
                'in',
                'range'      => array_keys(TenantCompanyRepository::selfCreate()->getForForm()),
                'allowArray' => true,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'position_id'       => t('employee', 'Profession'),
            'city_list'         => t('user', 'All cities'),
            'name'              => t('driverTariff', 'Name'),
            'type'              => t('driverTariff', 'Tariff type'),
            'tenant_company_id' => t('tenant_company', 'Companies'),
        ];
    }

    public function beforeValidate()
    {
        if (app()->user->can(User::ROLE_STAFF_COMPANY)) {
            $this->tenant_company_id = [user()->tenant_company_id];
        }
        return parent::beforeValidate();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()->alias('t');

        $query->where([
            't.tenant_id' => user()->tenant_id,
            't.block'     => $this->block,
        ]);

        $query->with([
            'class',
            'cities' => function (ActiveQuery $subQuery) {
                $subQuery->select(['city_id', 'name' . getLanguagePrefix()]);
            },
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'sort'       => [
                'defaultOrder' => [
                    'name' => SORT_ASC,
                ],
                'attributes'   => [
                    'name',
                ],
            ],
            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');

            return $dataProvider;
        }

        $query->andFilterWhere(['t.position_id' => $this->position_id]);

        // grid filtering conditions
        $query->joinWith([
            'tariffHasCity thc' => function (ActiveQuery $subQuery) {
                $subQuery->where(['thc.city_id' => $this->getSearchCityList()]);
            },
        ], false);

        $query->joinWith([
            'tenantCompany tc' => function (ActiveQuery $subQuery) {
                $subQuery->andFilterWhere(['in', 'tc.tenant_company_id', $this->tenant_company_id]);
            },
        ], false);

        return $dataProvider;
    }

    public function getAccessCityList()
    {
        return $this->_accessCityList;
    }

    public function setAccessCityList($value)
    {
        $this->_accessCityList = $value;
    }

    private function getSearchCityList()
    {
        return !empty($this->city_list) ? $this->city_list : $this->getAccessCityList();
    }
}
