<?php

namespace frontend\modules\employee\models\tariff;

use common\modules\city\models\City;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%worker_tariff_has_city}}".
 *
 * @property integer $tariff_id
 * @property integer $city_id
 *
 * @property City    $city
 * @property Tariff  $tariff
 */
class TariffHasCity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_tariff_has_city}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id', 'city_id'], 'required'],
            [['tariff_id', 'city_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tariff_id' => 'Tariff ID',
            'city_id'   => 'City ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(Tariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * Allow to save multiple rows
     *
     * @param array   $arCity
     * @param integer $tariff_id
     *
     * @return integer number of rows affected by the execution.
     * @throws \yii\db\Exception
     */
    public static function manySave($arCity, $tariff_id)
    {
        $insertValue = [];
        $connection  = app()->db;

        if (empty($arCity)) {
            $arCity = ArrayHelper::getColumn(user()->cities, 'city_id');
        }

        foreach ($arCity as $city_id) {
            $insertValue[] = [$tariff_id, $city_id];
        }

        return $connection->createCommand()->batchInsert(self::tableName(), ['tariff_id', 'city_id'],
            $insertValue)->execute();
    }
}
