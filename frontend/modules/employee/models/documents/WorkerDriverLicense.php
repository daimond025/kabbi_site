<?php

namespace frontend\modules\employee\models\documents;

use common\helpers\DateTimeHelper;
use common\modules\employee\models\documents\WorkerDriverLicense as WorkerDriverLicenseBase;
use yii\helpers\ArrayHelper;

class WorkerDriverLicense extends WorkerDriverLicenseBase
{
    public $start_day;
    public $start_month;
    public $start_year;

    public function rules()
    {
        return ArrayHelper::merge([
            [['start_day', 'start_month', 'start_year'], 'safe'],
        ], parent::rules());
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->loadStartDateParts();
    }

    protected function loadStartDateParts()
    {
        DateTimeHelper::loadDatePartsFromDate($this->start_day, $this->start_month, $this->start_year, $this->start_date);
    }

    public function beforeSave($insert)
    {
        if ($this->start_day && $this->start_month && $this->start_year) {
            $this->start_date = $this->getFormattedDateByParts($this->start_day, $this->start_month,
                $this->start_year);
        }

        return parent::beforeSave($insert);
    }

    private function getFormattedDateByParts($day, $month, $year)
    {
        return DateTimeHelper::getFormattedDateByParts($day, $month, $year);
    }
}