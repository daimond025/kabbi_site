<?php

namespace frontend\modules\employee\models\documents;

use common\helpers\DateTimeHelper;
use common\modules\employee\models\documents\WorkerOsago as WorkerOsagoBase;
use yii\helpers\ArrayHelper;

class WorkerOsago extends WorkerOsagoBase
{
    public $start_day;
    public $start_month;
    public $start_year;
    public $end_day;
    public $end_month;
    public $end_year;

    public function rules()
    {
        return ArrayHelper::merge([
            [['start_day', 'start_month', 'start_year', 'end_day', 'end_month', 'end_year'], 'safe'],
        ], parent::rules());
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->loadDateParts($this->start_day, $this->start_month, $this->start_year, $this->start_date);
        $this->loadDateParts($this->end_day, $this->end_month, $this->end_year, $this->end_date);
    }

    /**
     * @param string $day Day part
     * @param string $month Month part
     * @param string $year Year part
     * @param string $date Date
     */
    protected function loadDateParts(&$day, &$month, &$year, $date)
    {
        DateTimeHelper::loadDatePartsFromDate($day, $month, $year, $date);
    }

    private function getFormattedDateByParts($day, $month, $year)
    {
        return DateTimeHelper::getFormattedDateByParts($day, $month, $year);
    }

    public function beforeSave($insert)
    {
        if ($this->start_day && $this->start_month && $this->start_year) {
            $this->start_date = $this->getFormattedDateByParts($this->start_day, $this->start_month,
                $this->start_year);
        }

        if ($this->end_day && $this->end_month && $this->end_year) {
            $this->end_date = $this->getFormattedDateByParts($this->end_day, $this->end_month,
                $this->end_year);
        }

        return parent::beforeSave($insert);
    }
}