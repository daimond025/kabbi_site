<?php

namespace frontend\modules\employee\models\documents;


use common\modules\employee\models\documents\WorkerDocumentScan as WorkerDocumentScanBase;
use frontend\components\behavior\file\FileBehavior;
use yii\helpers\ArrayHelper;

/**
 * Class WorkerDocumentScan
 * @package frontend\modules\employee\models\documents
 * @mixin FileBehavior
 */
class WorkerDocumentScan extends WorkerDocumentScanBase
{
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'file' => [
                'class'                => FileBehavior::className(),
                'fileField'            => ['filename'],
                'upload_dir'           => app()->params['uploadScanDir'],
                'max_big_picture_side' => 900,
            ],
        ]);
    }

    public function rules()
    {
        return [
            [['worker_document_id'], 'integer'],
        ];
    }
}