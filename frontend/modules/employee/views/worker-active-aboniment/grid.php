<?php

/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $actionType string */

use frontend\modules\employee\models\workerActiveAboniment\entities\WorkerActiveAboniment;
use frontend\modules\employee\controllers\WorkerActiveAbonimentController;
use frontend\widgets\LinkPager;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use frontend\modules\employee\models\tariff\Tariff;
use yii\helpers\Url;
use common\modules\city\models\City;

$interest = [];
$linkForDelete = '';

if ($actionType == WorkerActiveAbonimentController::VIEW_ACTION_WORKER) {

    $linkForDelete = '/employee/worker-active-aboniment/delete-by-worker';

    $interest = [
        'attribute' => 'tariff_id',
        'content'   => function (WorkerActiveAboniment $model) {
            return Html::a(
                Html::encode($model->tariff->name),
                Url::to(['/employee/tariff/update', 'id' => $model->tariff_id]), [
                    'target'    => '_blank',
                    'data-pjax' => 0,
                ]
            );
        },
    ];
} elseif ($actionType == WorkerActiveAbonimentController::VIEW_ACTION_TARIFF) {

    $linkForDelete = '/employee/worker-active-aboniment/delete-by-tariff';

    $interest = [
        'attribute' => 'worker_id',
        'content'   => function (WorkerActiveAboniment $model) {
            return Html::a(
                Html::encode($model->worker->getFullName()),
                Url::to(['/employee/worker/update', 'id' => $model->worker_id]), [
                    'target'    => '_blank',
                    'data-pjax' => 0,
                ]
            );
        },
    ];
}

?>

<? Pjax::begin() ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'       => "{items}\n{pager}",
    'tableOptions' => [
        'class' => 'people_table',
    ],
    'pager'        => [
        'class'         => LinkPager::className(),
        'prevPageLabel' => t('app', 'Prev'),
        'nextPageLabel' => t('app', 'Next'),
    ],
    'columns'      => [
        $interest,
        [
            'attribute' => 'type',
            'content'   => function (WorkerActiveAboniment $model) {
                if ($model->type == Tariff::TYPE_ONCE) {
                    return t('driverTariff', 'Once');
                } elseif ($model->type == Tariff::TYPE_SUBSCRIPTION) {
                    return t('driverTariff', 'Subscription');
                }
            },
        ],
        [
            'attribute' => 'action_new_shift',
            'content'   => function (WorkerActiveAboniment $model) {
                if ($model->action_new_shift == Tariff::ACTION_NEW_SHIFT_NEW_PERIOD) {
                    return t('driverTariff', 'A new period begins');
                } elseif ($model->action_new_shift == Tariff::ACTION_NEW_SHIFT_CONTINUE_PERIOD) {
                    return t('driverTariff', 'Continue the incomplete period');
                }
            },
        ],
        [
            'attribute' => 'subscription_limit_type',
            'content'   => function (WorkerActiveAboniment $model) {
                if ($model->subscription_limit_type == Tariff::SUBSCRIPTION_LIMIT_TYPE_DAY) {
                    return t('driverTariff', 'Days count');
                } elseif ($model->subscription_limit_type == Tariff::SUBSCRIPTION_LIMIT_TYPE_SHIFT) {
                    return t('driverTariff', 'Count of working shifts');
                }
            },
        ],
        [
            'attribute' => 'days',
            'content'   => function (WorkerActiveAboniment $model) {
                return $model->days;
            },
        ],
        [
            'attribute' => 'period_type',
            'content'   => function (WorkerActiveAboniment $model) {
                if ($model->period_type == Tariff::PERIOD_TYPE_HOUR) {
                    return t('driverTariff', 'Number of hours');
                } elseif ($model->period_type == Tariff::PERIOD_TYPE_INTERVAL) {
                    return t('driverTariff', 'Interval');
                }
            },
        ],
        [
            'attribute' => 'period',
            'content'   => function (WorkerActiveAboniment $model) {
                return $model->period;
            },
        ],
        [
            'attribute' => 'cost',
            'content'   => function (WorkerActiveAboniment $model) {
                return $model->cost;
            },
        ],
        [
            'attribute' => 'count_active',
            'content'   => function (WorkerActiveAboniment $model) {
                return $model->count_active;
            },
        ],
        [
            'attribute' => 'expired_at',
            'content'   => function (WorkerActiveAboniment $model) {
                return Yii::$app->formatter->asDatetime($model->expired_at);
            },
        ],
        [
            'attribute' => 'created_at',
            'content'   => function (WorkerActiveAboniment $model) {
                if ($model->created_at) {
                    $city = $model->tariff->cities[0];
                    return Yii::$app
                        ->formatter
                        ->asDatetime(
                            $model->created_at + City::getTimeOffset($city->city_id)
                        );

                } else {
                    return null;
                }
            },
        ],
        [
            'label'   => t('driverTariff', 'Action'),
            'content' => function (WorkerActiveAboniment $model) use ($actionType, $linkForDelete) {
                $window = '<h2>Вы уверены, что хотите удалить тариф?</h2>';

                return Html::a(
                    t('driverTariff', 'Delete'),
                    null,
                    ['onclick' => "gootaxConfirm('" . $window . "', 'Удалить', 'Отмена', function(){\$('#abon_" . $model->aboniment_id . "').click();})"]
                ) . Html::a(
                        '',
                        Url::to(
                            array_merge(
                                [$linkForDelete], get(), ['abonimen_id' => $model->aboniment_id]
                            )
                        ),
                        ['id' => 'abon_' . $model->aboniment_id, 'style' => 'display: none;']
                    );

            },
        ],
    ],
]); ?>
<? Pjax::end() ?>

