<?php

use frontend\modules\employee\models\groups\WorkerGroup;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model WorkerGroup */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Driver Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="driver-group-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->group_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->group_id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method'  => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'group_id',
            'name',
            'class',
            'active',
        ],
    ]) ?>

</div>
