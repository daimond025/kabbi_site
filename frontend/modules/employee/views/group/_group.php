<?php

/* @var $this yii\web\View */
/* @var mixed $key The key value associated with the data item. */
/* @var integer $index The zero-based index of the data item in the items array returned by the data provider. */
/* @var $widget \yii\widgets\ListView */
/* @var array $model The data item. */
/* @var array $positionList */

use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;

?>
<h3><?= Html::encode($positionList[$key]) ?></h3>
<section style="margin-bottom: 30px;">
    <?= GridView::widget([
        'dataProvider' => (
        new ArrayDataProvider([
            'allModels'  => $model,
            'pagination' => false,
            'sort'       => [
                'defaultOrder' => [
                    'cities' => SORT_ASC,
                ],
                'attributes'   => [
                    'name',
                    'class',
                    'cities',
                    'position',
                    'priority'

                ],
            ]
        ])),
        'layout'       => "{items}",
        'tableOptions' => [
            'class' => 'people_table',
        ],
        'columns'      => [
            [
                'attribute' => 'name',
                'label'     => t('employee', 'Name'),
                'content'   => function ($model) {
                    return Html::a(Html::encode($model['name']), ['/employee/group/update', 'id' => $model['group_id']],
                        ['data-pjax' => 0]);
                },
            ],
            [
                'attribute' => 'class',
                'label'     => $model[0]['class_id'] || $model[0]['position_class_id'] ? t('employee', 'Class') : null,
                'content'   => function ($model, $key, $index, $column) {
                    if (!empty($model['class_id'])) {
                        return t('car', Html::encode($model['class']['class']));
                    }

                    if (!empty($model['position_class_id'])) {
                        return t('employee', Html::encode($model['positionClass']['name']));
                    }
                }
            ],
            [
                'attribute' => 'cities',
                'label'     => t('employee', 'City'),
                'content'   => function ($model) {
                    $attribute = 'name' . getLanguagePrefix();

                    return Html::encode($model['cities'][0][$attribute]);
                },
            ],
            [
                'attribute' => 'position',
                'label'     => t('employee', 'Profession'),
                'content'   => function ($model) {
                    return t('employee', Html::encode($model['position']['name']));
                },
            ],
            [
                'attribute' => 'priority',
                'label'     => t('employee', 'Priority'),
                'content'   => function ($model) {
                    return Html::encode($model['priority']);
                },
            ],
        ],
    ]);
    ?>
</section>


