<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model \frontend\modules\employee\models\groups\WorkerGroup */
/* @var $form_data array */

$this->title = $model->name;

echo Breadcrumbs::widget([
    'links'        => [
        ['label' => t('employee', 'Worker groups'), 'url' => ['/employee/group/list']],
    ],
    'itemTemplate' => '{link}',
    'tag'          => 'div',
    'options'      => ['class' => 'bread'],
    'homeLink'     => false,
]);
?>
<h1><?= Html::encode($this->title) ?></h1>

<?php
$form = app()->user->can('drivers') ? '_form' : '_form_read';
echo $this->render($form, compact('model', 'form_data')) ?>
