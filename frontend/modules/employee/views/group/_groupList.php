<?php

/* @var string $pjaxId */
/* @var $this yii\web\View */
/* @var array $cityList */
/* @var array $positionList */
/* @var $dataProvider yii\data\ArrayDataProvider */
/* @var $searchModel \frontend\modules\employee\models\groups\WorkerGroupSearch */

use frontend\widgets\filter\Filter;
use yii\widgets\ListView;
use yii\widgets\Pjax;

$filter = Filter::begin([
    'pjaxId' => $pjaxId,
    'model'  => $searchModel,
]); ?>

<?= $filter->checkboxList('city_list', $cityList); ?>
<?= $filter->checkboxList('position_id', $positionList); ?>
<?= $filter->input('name'); ?>
<? Filter::end(); ?>


<? Pjax::begin(['id' => $pjaxId]) ?>
<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView'     => '_group',
    'emptyText'    => t('app', 'Empty'),
    'viewParams'   => [
        'positionList' => $positionList,
    ],
    'layout'       => "{items}",
]); ?>
<? Pjax::end() ?>
