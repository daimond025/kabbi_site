<?php

use frontend\modules\employee\assets\groups\GroupAsset;
use frontend\modules\employee\models\groups\WorkerGroup;
use frontend\widgets\position\PositionChooser;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model WorkerGroup */
/* @var array $form_data Form data */

GroupAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'errorCssClass'   => 'input_error',
    'successCssClass' => 'input_success',
]); ?>

    <section class="row">
        <?= $form->field($model, 'name')->begin(); ?>
        <div class="row_label">
            <?= Html::activeLabel($model, 'name') ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint',
                        'Only displayed in the system.'); ?>
                </span>
            </a>
        </div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'name') ?>
        </div>
        <?= $form->field($model, 'name')->end(); ?>
    </section>

    <section class="row">
        <?= $form->field($model, 'city_list')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'city_list') ?></div>
        <div class="row_input" style="width: 40%;">
            <? if ($model->isNewRecord): ?>
                <?= Html::activeDropDownList($model, 'city_list', $form_data['city_map'], [
                    'data-placeholder' => Html::encode(!empty($model->city_list) ? $form_data['city_map'][$model->city_list[0]] : current($form_data['city_map']) ? current($form_data['city_map']) : t('driver',
                        'There are not cities')),
                    'class'            => 'default_select',
                    'name'             => 'WorkerGroup[city_list][]',
                ]) ?>
            <? else: ?>
                <div
                        class="row_input"><?= !empty($model->city_list) ? Html::encode($form_data['city_map'][$model->city_list]) : '' ?></div>
            <? endif ?>
        </div>
        <?= $form->field($model, 'city_list')->end(); ?>
    </section>

<?= PositionChooser::widget([
    'form'                  => $form,
    'model'                 => $model,
    'carClassMap'           => $form_data['class_map'],
    'carPositionList'       => $form_data['carPositionList'],
    'positionMap'           => $form_data['positionMap'],
    'positionDisabled'      => !$model->isNewRecord,
    'carClassDisabled'      => !$model->isNewRecord,
    'positionClassDisabled' => !$model->isNewRecord,
]) ?>

    <section class="row">
        <?= $form->field($model, 'tariff_list')->begin(); ?>
        <div class="row_label">
            <?= Html::activeLabel($model, 'tariff_list') ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint',
                        'Tariffs available to the group of performers.'); ?>
                </span>
            </a>
        </div>
        <div class="row_input">
            <?= Html::activeCheckboxList($model, 'tariff_list', $form_data['tariff_map'],
                ['class' => 'checbox_list']) ?>
            <div class="help-block" style="color: red"><?= Html::error($model, 'tariff_list') ?></div>
        </div>
        <?= $form->field($model, 'tariff_list')->end(); ?>
    </section>

    <section class="row">
        <?= $form->field($model, 'priority')->begin(); ?>
        <div class="row_label">
            <?= Html::activeLabel($model, 'priority') ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint',
                        'Priority when distributing orders within the same circle. Use the numbers 1, 2, 3, etc. for the order distribution queue for groups.'); ?>
                </span>
            </a>
        </div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'priority') ?>
        </div>
        <?= $form->field($model, 'priority')->end(); ?>
    </section>

    <section class="row" style="<?= !empty($form_data['clientTariffClasses']) ? '' : 'display: none' ?>">
        <?= $form->field($model, 'view_orders_list')->begin(); ?>
        <div class="row_label">
            <?= Html::activeLabel($model, 'view_orders_list') ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint',
                        'Visibility of orders of another class for this group.'); ?>
                </span>
            </a>
        </div>
        <div class="row_input">
            <ul id="view_orders_list" class="tar_list">
                <?
                $classKeys                    = array_keys($form_data['class_map']);
                $required_view_order_class_id = $model->isNewRecord ? current($classKeys) : $model->class_id;

                if (!empty($form_data['clientTariffClasses'])):
                    foreach ($form_data['clientTariffClasses'] as $clientClassId => $clientClass):
                        $checked = '';
                        $disabled             = '';
                        if ((isset($_POST['WorkerGroup']['view_orders_list'])
                                && in_array($clientClassId, $_POST['WorkerGroup']['view_orders_list']))
                            || in_array($clientClassId, $model->view_orders_list)
                            || $required_view_order_class_id == $clientClassId
                        ) {
                            $checked = 'checked="checked"';
                            if ($required_view_order_class_id == $clientClassId) {
                                $disabled = 'disabled="disabled"';
                            }
                        }
                        ?>
                        <li>
                            <input id="view_orders_list_<?= $clientClassId ?>"
                                   name="WorkerGroup[view_orders_list][]" <?= $checked ?> <?= $disabled ?>
                                   type="checkbox"
                                   value="<?= $clientClassId ?>">
                            <label for="view_orders_list_<?= $clientClassId ?>"><?= Html::encode($clientClass); ?></label>
                        </li>
                    <? endforeach;
                endif; ?>
            </ul>
            <input id="required_view_order" type="hidden" name="WorkerGroup[view_orders_list][]"
                   value="<?= $required_view_order_class_id ?>">
        </div>
        <?= $form->field($model, 'view_orders_list')->end(); ?>
    </section>

    <section class="row" style="<?= !empty($form_data['positionsForExtendsGroup']) ? '' : 'display: none' ?>">
        <?= $form->field($model, 'positionsForExtendsGroup')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'positionsForExtendsGroup') ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint',
                        'Visibility of orders of another class for this group.'); ?>
                </span>
            </a>
        </div>
        <div class="row_input">
            <ul id="positionsForExtendsGroup" class="tar_list">
                <? if (!empty($form_data['positionsForExtendsGroup'])):
                    foreach ($form_data['positionsForExtendsGroup'] as $positionId => $positionName):
                        $checked = '';
                        $disabled = '';
                        if ((isset($_POST['WorkerGroup']['positionsForExtendsGroup'])
                                && in_array($positionId, $_POST['WorkerGroup']['positionsForExtendsGroup']))
                            || in_array($positionId, $model->positionsForExtendsGroup)
                        ) {
                            $checked = 'checked="checked"';
                        }
                        ?>
                        <li>
                            <input id="positionsForExtendsGroup_<?= $positionId ?>"
                                   name="WorkerGroup[positionsForExtendsGroup][]" <?= $checked ?> <?= $disabled ?>
                                   type="checkbox"
                                   value="<?= $positionId ?>">
                            <label for="positionsForExtendsGroup_<?= $positionId ?>"><?= Html::encode($positionName); ?></label>
                        </li>
                    <? endforeach;
                endif; ?>
            </ul>
        </div>
        <?= $form->field($model, 'positionsForExtendsGroup')->end(); ?>
    </section>

    <section class="submit_form">
        <? if (!$model->isNewRecord): ?>
            <div>
                <label>
                    <?= Html::activeCheckbox($model, 'block', ['label' => null]) ?>
                    <b><?= t('app', 'Block') ?></b>
                </label>
            </div>
        <? endif ?>
        <?= Html::submitInput(t('app', 'Save')) ?>
    </section>
<?php ActiveForm::end(); ?>