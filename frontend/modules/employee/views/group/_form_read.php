<?php

use frontend\widgets\position\PositionChooser;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \frontend\modules\employee\models\groups\WorkerGroup */
/* @var $form yii\widgets\ActiveForm */
/* @var $model \frontend\modules\employee\models\groups\WorkerGroup */
/* @var array $form_data Form data */

\frontend\modules\employee\assets\groups\GroupAsset::register($this);

?>

<div class="non-editable">
    <section class="row">
        <div class="row_label">
            <?= Html::activeLabel($model, 'name') ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint',
                        'Only displayed in the system.'); ?>
                </span>
            </a>
        </div>
        <div class="row_input">
            <div class="row_input"><?= Html::encode($model->name) ?></div>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'city_list') ?></div>
        <div class="row_input" style="width: 40%;">
            <div
                class="row_input"><?= !empty($model->city_list) ? Html::encode($form_data['city_map'][$model->city_list]) : '' ?></div>
        </div>
    </section>

    <?= PositionChooser::widget([
        'model'            => $model,
        'carClassMap'      => $form_data['class_map'],
        'carPositionList'  => $form_data['carPositionList'],
        'positionMap'      => $form_data['positionMap'],
    ]) ?>
    <section class="row">
        <div class="row_label">
            <?= Html::activeLabel($model, 'tariff_list') ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint',
                        'Tariffs available to the group of performers.'); ?>
                </span>
            </a>
        </div>
        <div class="row_input">
            <?=
            Html::activeCheckboxList($model, 'tariff_list', $form_data['tariff_map'], [
                'class' => 'checbox_list',
                'item'  => function ($index, $label, $name, $checked, $value) {
                    return '<label><input disabled = "disabled" checked name="' . $name . '" type="checkbox" value="' .
                    $value . '"> ' . Html::encode($label) . '</label>';
                },
            ])
            ?>
            <div class="help-block" style="color: red"><?= Html::error($model, 'tariff_list') ?></div>
        </div>
    </section>

    <section class="row">
        <div class="row_label">
            <?= Html::activeLabel($model, 'priority') ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint',
                        'Priority when distributing orders within the same circle. Use the numbers 1, 2, 3, etc. for the order distribution queue for groups.'); ?>
                </span>
            </a>
        </div>
        <div class="row_input">
            <div class="row_input"><?= Html::encode($model->priority) ?></div>
        </div>
    </section>

    <? if (!empty($form_data['client_tariffs'])): ?>
    <section class="row">
        <div class="row_label">
            <?= Html::activeLabel($model, 'view_orders_list') ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint',
                        'Visibility of orders of another class for this group.'); ?>
                </span>
            </a>
        </div>
        <div class="row_input">
            <ul id="view_orders_list" class="tar_list">
                <?
                $required_view_order_class_id = $model->class_id ? $model->class_id : $model->position_class_id;
                foreach ($form_data['client_tariffs'] as $clientTariffClassId => $client_tariff):
                    $checked = '';
                    $disabled = 'disabled="disabled"';
                    if (in_array($clientTariffClassId, $model->view_orders_list)
                        || $required_view_order_class_id == $clientTariffClassId
                    ) {
                        $checked = 'checked="checked"';
                    }
                    ?>
                    <li>
                        <input id="view_orders_list_<?= $clientTariffClassId ?>"
                               name="WorkerGroup[view_orders_list][]" <?= $checked ?> <?= $disabled ?> type="checkbox"
                               value="<?= $clientTariffClassId ?>">
                        <label
                            for="view_orders_list_<?= $clientTariffClassId ?>"><?= Html::encode($client_tariff); ?></label>
                    </li>
                <? endforeach; ?>
            </ul>
        </div>
    </section>
    <? endif ?>
</div>