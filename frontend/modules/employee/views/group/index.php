<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var string $pjaxId */
/* @var $this yii\web\View */
/* @var array $cityList */
/* @var array $positionList */
/* @var $dataProvider yii\data\ArrayDataProvider */
/* @var $searchModel \frontend\modules\employee\models\groups\WorkerGroupSearch */

$this->title = Yii::t('employee', 'Worker groups');
if (app()->user->can('drivers')) {
    echo Html::a(t('app', 'Add'), ['create'],
        ['class' => 'button', 'style' => 'float: right; position: relative; top: 5px;']);
}
?>
<h1><?= Html::encode($this->title) ?></h1>
<section class="main_tabs">
    <div class="tabs_links">
        <ul>
            <li><a href="#active" class="t01"><?= t('app', 'Active') ?></a></li>
            <li><a href="#blocked" class="t02" data-href="<?= Url::to('list-blocked') ?>"><?= t('app', 'Blocked') ?></a>
            </li>
        </ul>
    </div>
    <div class="tabs_content">
        <div id="t01" data-view="">
            <?= $this->render('_groupList', compact('searchModel', 'dataProvider', 'cityList', 'pjaxId', 'positionList')) ?>
        </div>
        <div id="t02" data-view="blocked"></div>
    </div>
</section>