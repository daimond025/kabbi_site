<?php
/* @var $this yii\web\View */
/* @var array $cityList */
/* @var $model \frontend\modules\employee\models\worker\WorkerSearch */

use yii\widgets\ActiveForm;

?>
<?php $form = ActiveForm::begin([
    'method'      => 'get',
    'fieldConfig' => [
        'template' => '{input}',
    ],
]); ?>
<div class="grid_3" data-activity="active">
    <div class="grid_item">
        <div class="select_checkbox">
            <a class="a_sc" rel="<?= t('user', 'All cities') ?>"><?= t('user', 'All cities') ?></a>
            <div class="b_sc">
                <?= $form->field($model, 'city_id')->checkboxList($cityList) ?>
            </div>
        </div>
    </div>
    <div class="grid_item">
        <?= $form->field($model, 'stringSearch',
            ['inputOptions' => ['placeholder' => t('employee', 'Search by name, phone or call sign')]]) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
