<?php

/* @var $this yii\web\View */
/* @var array $Cars */
/* @var array $groupMapGroupedByClass */
/* @var array $tariffMapGroupedByClass */
/* @var array $showingClassesMap */
/* @var array $current_car */
/* @var array $worker_id */



use \yii\helpers\Url;
use yii\helpers\Html;

?>

<style>
    .car_properties_item{
        display: flex;
        flex-direction: row;
        justify-content: space-evenly;
        align-items: first baseline;
    }
    }
</style>

<div id="cont-2-link-id-<?= $current_car['ID']?>" data-id="<?= $current_car['ID']?>" data-ar="<?=$worker_id?>" class="draggable cont-2-link">

    <div class="up-link_driver" id="car_name">
        <a style="color: #0e3ada" href="/lib/car/update/<?=$current_car['ID']?>">  <span> <?= $current_car['NAME']. " (" .$current_car['YEAR'] .  ") - ". $current_car['GOV_NUMBER']   ?>  </span> </a>
    </div>
    <div id="show_hide" data-id="cont-2-link-id-mal-<?= $current_car['ID']?>" class="cont-2-link-id-mal">
        <a style="float: right" id="a-btn-<?=$current_car['ID']?>" onclick="render(<?= $worker_id ?>,<?= $current_car['ID']?>)"> <?= t('reports', 'Show/Hide')?></a>
    </div>

    <div id="tariffs_config">
    <div id="content_car" class="car_properties_item">

<? $car = $current_car; ?>
    <div style="display: contents">
        <button id="btn-<?= $current_car['ID']?>" onclick="unLink(<?= $current_car['ID']?>, <?= $worker_id?>)"  type="button" data-id="<?= $worker_id?>"  data-ar="<?= $current_car['ID']?>"><?= t('reports', 'Unlink')?></button>

        <div class="select_checkbox" style="width: 250px" >
        <select  data-car="<?= $current_car['ID']?>"   onchange="changeGroup(event,<?= $current_car['ID']?>)"  class="default_select driver_group"   data-placeholder="<?= t('employee', 'Choose a group') ?>">
            <?
                foreach ($groupMapGroupedByClass as $groupId => $group) {

                    $selected = $car['GROUP_ID'] == $groupId;

                    echo Html::tag('option', Html::encode($group), [
                        'selected' => $selected,
                        'value'    => $groupId,
                    ]);
                }

            ?>
        </select>
        </div>
    </div>
    <div  <? if (!$car['GROUP_ID']): ?> class="hide driver_group_options"<? endif ?> >
        <div class="select_checkbox">
            <?
              $tariffSelect = [];
               if (isset($car['GROUP_TARIFFS'])) {
                 foreach ($tariffMapGroupedByClass as $tariffId => $tariff) {
                     $checked = !empty($car['GROUP_ID']) && in_array($tariffId, $car['GROUP_TARIFFS']);
                     if($checked){
                         $tariffSelect[] = $tariff ;
                         break;
                     }
                 }
               }
               if(count($tariffSelect) == 0){
                   $tariffSelect[] = t('employee', 'Choose a tariff');
               }
            ?>
            <a class="a_sc select" data-filter="no" style="width: 250px" id="group_tariff" rel="<?= t('employee', 'Choose a tariff') ?>">
                <?= implode(',', $tariffSelect) ?>
            </a>
            <div class="b_sc" id="group_tariff">
                <ul class="group_tariff" onchange="changeTariff(event,<?= $current_car['ID']?>)">
                    <?

                    if (isset($car['GROUP_TARIFFS'])) {
                        foreach ($tariffMapGroupedByClass as $tariffId => $tariff) {
                            $checked = !empty($car['GROUP_ID'])
                                && in_array($tariffId, $car['GROUP_TARIFFS']);

                            echo Html::tag('li', Html::tag('label',
                                Html::input('checkbox', 'tariff[]', $tariffId, [
                                    'checked' => $checked,
                                ]) . Html::encode($tariff)));
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>

        <div class="select_checkbox">
            <?
              $class_Select = [];
              if (is_array($showingClassesMap) && !empty($showingClassesMap)) {
                 foreach ($showingClassesMap as $classId => $class) {
                     $isCarClass = $classId == $car['CLASS_ID'];
                     $checked    = $isCarClass || (!empty($car['GROUP_ID']) && in_array($classId, $car['GROUP_CLASSES']));

                     if($checked)  $class_Select[] = $class;
                 }
              }
            ?>
            <a data-filter="no" data-content="partner" class="a_sc select"  id="group_class" style="width: 250px"  rel="<?= t('employee', 'Choose a class') ?>">   <?= implode(',', $class_Select) ?></a>
            <div class="b_sc" id="group_class">
                <ul class="group_class" onchange="changeClass(event,<?= $current_car['ID']?>);">
                    <?
                    if (is_array($showingClassesMap) && !empty($showingClassesMap)) {
                        foreach ($showingClassesMap as $classId => $class) {
                            $isCarClass = $classId == $car['CLASS_ID'];
                            $checked    = $isCarClass
                                || (!empty($car['GROUP_ID']) && in_array($classId, $car['GROUP_CLASSES']));

                            echo Html::tag('li', Html::tag('label',
                                Html::input('checkbox', 'class[]', $classId, [
                                    'disabled' => $isCarClass,
                                    'checked'  => $checked,
                                ]) . Html::encode($class)));
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>

     </div>
    </div>
</div>
