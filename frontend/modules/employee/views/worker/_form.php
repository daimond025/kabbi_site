<?php

use frontend\modules\employee\assets\worker\WorkerAsset;
use frontend\modules\employee\models\documents\WorkerDocumentScan;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use frontend\widgets\file\Logo;
use \frontend\modules\employee\widgets\document\Documents;
use app\modules\tenant\models\User;
use yii\helpers\ArrayHelper;
use app\modules\tenant\models\field\FieldRecord;

/* @var $this yii\web\View */
/* @var $model \common\modules\employee\models\worker\Worker */
/* @var $form yii\widgets\ActiveForm */
/* @var array $formData */
/* @var array $workerDocumentMap */
/* @var array $documentMap */
/* @var array $scanDocumentMap */
/* @var bool $isCreateForm */
/* @var array $showFieldList */

WorkerAsset::register($this);

$form = ActiveForm::begin([
    'id'                     => 'worker-form',
    'errorCssClass'          => 'input_error',
    'successCssClass'        => 'input_success',
    'enableClientValidation' => true,
    'options'                => [
        'enctype' => 'multipart/form-data',
    ],
]); ?>

<? if ($isCreateForm): ?>
    <!--Позывной-->
    <section class="row">
        <?= $form->field($model, 'callsign')->begin(); ?>
        <div class="row_label">
            <?= Html::activeLabel($model, 'callsign') ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint',
                        'The unique identifier of the worker. Used for authorization in the worker application.'); ?>
                </span>
            </a>
        </div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'callsign', ['autocomplete' => 'off']) ?>
            <?= Html::error($model, 'callsign',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'callsign')->end(); ?>
    </section>

    <!--Пароль-->
    <section class="row">
        <?= $form->field($model, 'password')->begin(); ?>
        <div class="row_label"><label><?= !$isCreateForm ? t('app', 'Сhange password') : t('app',
                    'Password') ?></label></div>
        <div class="row_input">
            <?= Html::activePasswordInput($model, 'password', ['value' => '', 'autocomplete' => 'off']) ?>
            <?= Html::error($model, 'password',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'password')->end(); ?>
    </section>

    <!--Повторить пароль-->
    <section class="row">
        <?= $form->field($model, 'password_repeat')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'password_repeat') ?></div>
        <div class="row_input">
            <?= Html::activePasswordInput($model, 'password_repeat', ['value' => '']) ?>
            <?= Html::error($model, 'password_repeat',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'password_repeat')->end(); ?>
    </section>
<? endif ?>

<!--Фото-->
<? if (!$isCreateForm): ?>
    <?= Logo::widget([
        'form'         => $form,
        'model'        => $model,
        'attribute'    => 'photo',
        'ajax'         => true,
        'uploadAction' => Url::to(['/employee/worker/upload-logo/', 'entity_id' => get('id')]),
    ]) ?>
<? endif ?>

<!--Город-->
<section class="row">
    <?= $form->field($model, 'cityIds', [
        'enableClientValidation' => false,
        'options'                => [
            'data' => [
                'required-message' => t('yii', '{attribute} cannot be blank.',
                    ['attribute' => $model->attributeLabels()['cityIds']]),
            ],
        ],
    ])->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($model, 'cityIds') ?></div>
    <div class="row_input">
        <div class="select_checkbox">
            <a class="a_sc" rel="<?= t('app', 'Choose city') ?>">
                <?= t('app', 'Choose city') ?>
            </a>
            <div class="b_sc">
                <?= Html::activeCheckboxList($model, 'cityIds', $formData['city_list'], [
                    'tag'  => 'ul',
                    'item' => function ($index, $label, $name, $checked, $value) {
                        return Html::tag('li', Html::checkbox($name, $checked, [
                            'value' => $value,
                            'label' => Html::encode($label),
                        ]));
                    },
                ]) ?>
            </div>
        </div>
        <?= Html::error($model, 'cityIds',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($model, 'cityIds')->end(); ?>
</section>


<!--Тип сотруднечества-->
<section class="row">
    <?= $form->field($model, 'partnership')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($model, 'partnership') ?></div>
    <div class="row_input">
        <?= Html::activeDropDownList($model, 'partnership', $model->getPartnerTypes(), [
            'prompt' => t('employee', 'Choose the partnership'),
            'class'  => 'default_select',
        ]) ?>
        <?= Html::error($model, 'partnership',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($model, 'partnership')->end(); ?>
</section>

<!--ФИО-->
<section class="row">
    <div class="row_label">
        <label <? if ($model->isAttributeRequired('last_name')): ?>class="required"<? endif ?>>
            <?= Html::encode($model->getAttributeLabel('last_name')); ?>
        </label>&nbsp;
        <label <? if ($model->isAttributeRequired('name')): ?>class="required"<? endif ?>>
            <?= Html::encode($model->getAttributeLabel('name')); ?>
        </label>&nbsp;
        <label <? if ($model->isAttributeRequired('second_name')): ?>class="required"<? endif ?>>
            <?= Html::encode($model->getAttributeLabel('second_name')); ?>
        </label>
    </div>
    <div class="row_input">
        <div class="grid_3">
            <?= $form->field($model, 'last_name')->begin(); ?>
            <div class="grid_item"><?= Html::activeTextInput($model, 'last_name', ['dir' => 'auto']) ?></div>
            <?= Html::error($model, 'last_name',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
            <?= $form->field($model, 'last_name')->end(); ?>
            <?= $form->field($model, 'name')->begin(); ?>
            <div class="grid_item"><?= Html::activeTextInput($model, 'name', ['dir' => 'auto']) ?></div>
            <?= Html::error($model, 'name',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
            <?= $form->field($model, 'name')->end(); ?>
            <?= $form->field($model, 'second_name')->begin(); ?>
            <div class="grid_item"><?= Html::activeTextInput($model, 'second_name', ['dir' => 'auto']) ?></div>
            <?= $form->field($model, 'second_name')->end(); ?>
        </div>
    </div>
</section>

<? if (ArrayHelper::getValue($showFieldList, 'worker.' . FieldRecord::NAME_WORKER_BIRTHDAY, true)) : ?>
    <!--Дата рождения-->
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'birthday') ?></div>
        <div class="row_input">
            <div class="grid_3">
                <?= $form->field($model, 'birth_day')->begin(); ?>
                <div class="grid_item">
                    <?= Html::activeDropDownList($model, 'birth_day', $formData['birth_day'], [
                        'prompt' => t('user', 'Day'),
                        'class'  => 'default_select',
                    ]); ?>
                </div>

                <?= $form->field($model, 'birth_day')->end(); ?>
                <?= $form->field($model, 'birth_month')->begin(); ?>
                <div class="grid_item">
                    <?= Html::activeDropDownList($model, 'birth_month', $formData['birth_month'], [
                        'prompt' => t('user', 'Month'),
                        'class'  => 'default_select',
                    ]); ?>
                </div>
                <?= $form->field($model, 'birth_month')->end(); ?>
                <?= $form->field($model, 'birth_year')->begin(); ?>
                <div class="grid_item">
                    <?= Html::activeDropDownList($model, 'birth_year', $formData['birth_year'], [
                        'prompt' => t('user', 'Year'),
                        'class'  => 'default_select',
                    ]); ?>
                </div>
                <?= $form->field($model, 'birth_year')->end(); ?>
            </div>
        </div>
    </section>
<? endif; ?>

<!--Моб. телефон-->
<section class="row">
    <?= $form->field($model, 'phone')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($model, 'phone') ?></div>
    <div class="row_input">
        <div class="input_with_icon">
            <? if ($model->phone): ?>
                <a href="tel:+<?= $model->phone ?>" class="call_now"><span></span></a>
            <? endif ?>
            <div class="input_with_text">
                <?= Html::activeTextInput($model, 'phone', [
                    'class'     => 'mask_phone',
                    'data-lang' => mb_substr(app()->language, 0, 2),
                ]); ?>
                <span></span>
            </div>
        </div>
        <?= Html::error($model, 'phone',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($model, 'phone')->end(); ?>
</section>

<? if (ArrayHelper::getValue($showFieldList, 'worker.' . FieldRecord::NAME_WORKER_EMAIL, true)) : ?>
    <!--E-mail-->
    <section class="row">
        <?= $form->field($model, 'email')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'email') ?></div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'email') ?>
            <?= Html::error($model, 'email',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'email')->end(); ?>
    </section>
<? endif; ?>

<? if (ArrayHelper::getValue($showFieldList, 'worker.' . FieldRecord::NAME_WORKER_CARD_NUMBER, true)) : ?>
    <!--Номер карты-->
    <section class="row">
        <?= $form->field($model, 'card_number')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'card_number') ?></div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'card_number') ?>
            <?= Html::error($model, 'card_number',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'card_number')->end(); ?>
    </section>
<? endif; ?>

<? if (ArrayHelper::getValue($showFieldList, 'worker.' . FieldRecord::NAME_WORKER_YANDEX_ACCOUNT_NUMBER, true)) : ?>
    <!--Номер Яндекс.Кошелька-->
    <section class="row">
        <?= $form->field($model, 'yandex_account_number')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'yandex_account_number') ?></div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'yandex_account_number') ?>
            <?= Html::error($model, 'yandex_account_number',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'yandex_account_number')->end(); ?>
    </section>
<? endif; ?>

<? if (ArrayHelper::getValue($showFieldList, 'worker.' . FieldRecord::NAME_WORKER_DESCRIPTION, true)) : ?>
    <!-- Описание -->
    <section class="row">
        <?= $form->field($model, 'description')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'description') ?></div>
        <div class="row_input">
            <?= Html::activeTextarea($model, 'description') ?>
        </div>
        <?= $form->field($model, 'description')->end(); ?>
    </section>
<? endif; ?>


<!-- Компании -->
<? if (!app()->user->can(User::ROLE_STAFF_COMPANY)): ?>
    <section class="row">
        <?= $form->field($model, 'tenant_company_id')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'tenant_company_id') ?></div>
        <div class="row_input">
            <?= Html::activeDropDownList($model, 'tenant_company_id', $formData['companies'], [
                'prompt' => t('tenant_company', 'Select company'),
                'class'  => 'default_select',
            ]); ?>
        </div>
        <?= $form->field($model, 'tenant_company_id')->end(); ?>
    </section>
<? endif; ?>

<? if (!$isCreateForm): ?>

    <?= Documents::widget([
        'form'                 => $form,
        'entityHasDocumentMap' => $workerDocumentMap,
        'scanDocumentMap'      => $scanDocumentMap,
        'documentMap'          => $documentMap,
        'ajaxUpload'           => true,
        'scanModelConfig'      => [
            'class' => WorkerDocumentScan::className(),
        ],
        'entityId'             => get('id'),
    ]) ?>
<? endif ?>


<section class="submit_form">
    <div>
        <? if (!$isCreateForm): ?>
            <? if ($model->activate == 0): ?>
                <label>
                    <?= Html::activeCheckbox($model, 'activate', ['label' => null]) ?> <b><?= t('app',
                            'Activate') ?></b>
                </label>
            <? else : ?>
                <label>
                    <?= Html::activeCheckbox($model, 'block', ['label' => null]) ?> <b><?= t('app', 'Block') ?></b>
                </label>
            <? endif; ?>
        <? endif ?>
    </div>
    <?= Html::submitInput(t('app', 'Save')) ?>
</section>
<?php ActiveForm::end(); ?>

