<?php

/* @var $this yii\web\View */
/* @var $workerReviewRating WorkerReviewRating */
/* @var $reviewService ReviewService */
/* @var $clientReviews \app\modules\client\models\ClientReview[] */
/* @var array $workerPositionMap */
/* @var int $position_id */
/* @var int $workerId */
/* @var bool $notActive */

use frontend\modules\employee\components\review\ReviewService;
use frontend\modules\employee\models\worker\WorkerReviewRating;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

//Открытие карточки заказа в отзывах
$this->registerJs('registerOrderOpenInModalEvent(".open_order");');

$formatter = app()->formatter;

Pjax::begin(['enablePushState' => false, 'id' => 'pjax-reviews-list']);
?>
<div class="order_cities">
    <ul>
        <? foreach ($workerPositionMap as $workerPositionId => $positionName): ?>
                <li>
                <? if ($position_id == $workerPositionId):
                    ?>
                    <a class="active" onclick="return false" href=""><span><?= $positionName ?></span></a>
                <? else: ?>
                    <a href="<?= Url::to([
                        '/employee/worker/show-reviews',
                        'id'          => get('id'),
                        'position_id' => $workerPositionId,
                    ]) ?>"><span><?= $positionName ?></span></a>
                <? endif; ?>
            </li>
        <? endforeach; ?>
    </ul>
</div>
<? if (!empty($workerReviewRating)): ?>
    <div class="reviews_statistic rs_stat_page">
        <div class="driver_rating">
            <b><?= Html::encode($reviewService->getAverageValue($workerReviewRating)); ?></b>
            <span><?= t('employee', 'Rating') ?><br><?= t('employee', 'of employee') ?></span>
        </div>
        <ul>
            <li>
                <? $percent = $reviewService->getPercentValue($workerReviewRating, $workerReviewRating->one) ?>
                <span class="rs_stars"><i class="active"></i><i></i><i></i><i></i><i></i></span>
                <span class="rs_line"><span style="width: <?= Html::encode($percent); ?>"></span></span>
                <span class="rs_proc"><?= Html::encode($percent); ?></span>
                <span class="rs_items"><?= Html::encode($workerReviewRating->one); ?> <?= t('client', 'times') ?></span>
            </li>
            <li>
                <? $percent = $reviewService->getPercentValue($workerReviewRating, $workerReviewRating->two) ?>
                <span class="rs_stars"><i class="active"></i><i class="active"></i><i></i><i></i><i></i></span>
                <span class="rs_line"><span style="width: <?= Html::encode($percent); ?>"></span></span>
                <span class="rs_proc"><?= Html::encode($percent); ?></span>
                <span class="rs_items"><?= Html::encode($workerReviewRating->two); ?> <?= t('client', 'times') ?></span>
            </li>
            <li>
                <? $percent = $reviewService->getPercentValue($workerReviewRating, $workerReviewRating->three) ?>
                <span class="rs_stars"><i class="active"></i><i class="active"></i><i class="active"></i><i></i><i></i></span>
                <span class="rs_line"><span style="width: <?= Html::encode($percent); ?>"></span></span>
                <span class="rs_proc"><?= Html::encode($percent); ?></span>
                <span class="rs_items"><?= Html::encode($workerReviewRating->three); ?> <?= t('client',
                        'times') ?></span>
            </li>
            <li>
                <? $percent = $reviewService->getPercentValue($workerReviewRating, $workerReviewRating->four) ?>
                <span class="rs_stars"><i class="active"></i><i class="active"></i><i class="active"></i><i
                        class="active"></i><i></i></span>
                <span class="rs_line"><span style="width: <?= Html::encode($percent); ?>"></span></span>
                <span class="rs_proc"><?= Html::encode($percent); ?></span>
                <span class="rs_items"><?= Html::encode($workerReviewRating->four) ?> <?= t('client', 'times') ?></span>
            </li>
            <li>
                <? $percent = $reviewService->getPercentValue($workerReviewRating, $workerReviewRating->five) ?>
                <span class="rs_stars"><i class="active"></i><i class="active"></i><i class="active"></i><i
                        class="active"></i><i class="active"></i></span>
                <span class="rs_line"><span style="width: <?= Html::encode($percent); ?>"></span></span>
                <span class="rs_proc"><?= Html::encode($percent); ?></span>
                <span class="rs_items"><?= Html::encode($workerReviewRating->five); ?> <?= t('client',
                        'times') ?></span>
            </li>
        </ul>
    </div>

<? endif; ?>

    <div class="order_cities">
        <ul>
            <? if (!$notActive): ?>
            <li><a class="active" onclick="return false" href=""><span><?= Html::encode(t('app', 'All reviews')) ?></span></a></li>
            <li><a href="<?= Url::to([
                    '/employee/worker/show-reviews',
                    'id'          => $workerId,
                    'position_id' => $position_id,
                    'notActive' => true,
                ]) ?>"><span><?= Html::encode(t('app', 'Disregarded')) ?></span></a></li>
            <? else: ?>
                <li><a href="<?= Url::to([
                        '/employee/worker/show-reviews',
                        'id'          => $workerId,
                        'position_id' => $position_id,
                    ]) ?>"><span><?= Html::encode(t('app', 'All reviews')) ?></span></a></li>
                <li><a class="active" onclick="return false" href=""><span><?= Html::encode(t('app', 'Disregarded')) ?></span></a></li>
            <?endif; ?>
        </ul>
    </div>

    <? if (!empty($clientReviews)): ?>

    <ul class="review_list">

        <? foreach ($clientReviews as $review): ?>
            <li>
            <span class="rl_id">
                <a href="<?= Url::to([
                    '/order/order/view',
                    'order_number' => $review->order->order_number,
                ]) ?>" class="open_order">№<?= Html::encode($review->order->order_number); ?></a>
                <? $order_time = $review->order->getOrderTimestamp($review->order->create_time) ?>
                <span>
                    <?= t('app', 'on') ?>
                    <?= $formatter->asDate($order_time, 'shortDate') ?>
                    <br/>
                    <?= $formatter->asTime($order_time, 'short') ?>
                </span>
            </span>
            <span class="rl_content">
                <span class="rl_stars">
                    <? for ($i = 1; $i <= 5; $i++): ?>
                        <? if ($i <= $review->rating): ?>
                            <i class="active"></i>
                        <? else: ?>
                            <i></i>
                        <? endif ?>
                    <? endfor; ?>
                </span>
                <i>«<?= Html::encode($review->text); ?>»</i>
                <div align="right" class="js-active-review-content" style="display: <?= !$review->isActive ? 'none' : 'block' ?>">
                    <?= Html::a(t('app', 'Disregard in rating'), null, [
                        'class' => 'js-not-active-review',
                        'data'  => [
                            'url'    => Url::to([
                                '/client/base/not-active-review',
                                'id' => $review->review_id,
                            ]),
                            'reload' => Url::to([
                                '/employee/worker/show-reviews',
                                'id'          => $workerId,
                                'position_id' => $position_id,
                                'notActive'   => $notActive,
                            ]),
                        ],
                    ]); ?>
                </div>
                <div align="right" class="js-not-active-review-content" style="display: <?= $review->isActive ? 'none' : 'block' ?>">
                        <?= Html::encode(t('app', 'Disregarded')); ?>

                        <?= Html::a(t('app', 'Consider'), null, [
                            'class' => 'js-active-review',
                            'data'  => [
                                'url'    => Url::to([
                                    '/client/base/active-review',
                                    'id' => $review->review_id,
                                ]),
                                'reload' => Url::to([
                                    '/employee/worker/show-reviews',
                                    'id'          => $workerId,
                                    'position_id' => $position_id,
                                    'notActive'   => $notActive,
                                ]),
                            ],
                        ]); ?>
                </div>

            </span>
            </li>
        <? endforeach; ?>
    </ul>
<? else: ?>
    <p><?= t('app', 'Empty') ?></p>
<? endif; ?>
<?Pjax::end()?>
