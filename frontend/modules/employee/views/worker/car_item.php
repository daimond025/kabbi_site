<?php

/* @var $this yii\web\View */
/* @var array $car */



use \yii\helpers\Url;
use yii\helpers\Html;
?>
<div id="insert_car_item_<?= $car['car_id']?>"  class="draggable cont-2-link-id" data-id="<?= $car['car_id'] ?>">

    <div id="cont-2-link-id-<?= $car['car_id'] ?>"  >
        <div class="up-link">
            <span> <?= $car['name']. " (" .$car['year'] .  ") - ". $car['gos_number']   ?>  </span>
        </div>

        <div  id="cont-2-link-id-mal-<?= $car['car_id']?>" >

        </div>

    </div>
</div>