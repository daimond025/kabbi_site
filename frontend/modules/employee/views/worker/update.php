<?php

use common\modules\employee\models\worker\WorkerHasCity;
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\balance\models\Account;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model \frontend\modules\employee\models\worker\Worker */
/* @var $showFieldList array */

\frontend\modules\balance\assets\BalanceAsset::register($this);
$this->registerJs('balanceOperationInit();');

$reportAsset = app\modules\reports\assets\ReportsAsset::register($this);
$this->registerJsFile($reportAsset->baseUrl . '/workers.js');

$this->title = $model->fullName;

$isCreateForm = false;

$cityId = WorkerHasCity::find()
    ->select('city_id')
    ->byWorkerId(get('id'))
    ->orderBy(['last_active' => SORT_DESC, 'city_id' => SORT_ASC])
    ->limit(1)
    ->scalar();

echo Breadcrumbs::widget([
    'links'        => [
        ['label' => t('employee', 'Workers'), 'url' => ['/employee/worker/list']],
    ],
    'itemTemplate' => '{link}',
    'tag'          => 'div',
    'options'      => ['class' => 'bread'],
    'homeLink'     => false,
]);
?>
<h1><?= Html::encode($this->title) ?> <?= Html::tag('span', '',
        ['class' => 'pt_' . ($model->isOnline ? 'online' : 'offline')]) ?></h1>
<section class="main_tabs">
    <div class="tabs_links">
        <ul>
            <li><a href="#profile" class="t01"><?= t('employee', 'Profile') ?></a></li>
            <? if (app()->user->can('drivers')): ?>
                <li><a href="#job" class="t02"
                       data-href="<?= Url::to(['/employee/worker/job', 'id' => get('id')]) ?>"><?= t('employee',
                            'Job') ?></a></li>
            <? endif ?>
            <li><a href="#callsign_and_password" class="t03"
                   data-href="<?= Url::to(['/employee/worker/change-password', 'id' => get('id')]) ?>"><?= t('employee',
                        'Callsign and password') ?></a></li>
            <li><a href="#balance" class="t04" data-href="<?= Url::to([
                    '/balance/balance/show-balance',
                    'id'      => get('id'),
                    'kind_id' => Account::WORKER_KIND,
                    'city_id' => $cityId,
                ]) ?>"><?= t('employee', 'Balance') ?></a></li>
            <li><a href="#feedback" class="t05"
                   data-href="<?= Url::to(['/employee/worker/show-reviews', 'id' => get('id')]) ?>"><?= t('employee',
                        'Feedback') ?></a></li>
            <li><a href="#shifts" class="t06"
                   data-href="<?= Url::to(['/reports/worker/show-shifts', 'id' => get('id')]) ?>"><?= t('reports',
                        'Shifts') ?></a></li>
            <li><a href="#promocodes" class="t07"
                   data-href="<?= Url::to(['/promocode/promo-reports/promo-report-worker', 'id' => get('id')]) ?>"><?= t('promo',
                        'Promo codes') ?></a></li>
            <li><a href="#aboniments" class="t08"
                   data-href="<?= Url::to(["/employee/worker-active-aboniment/get-active-aboniment-by-worker?" . http_build_query(get())]) ?>"><?= t('employee', 'Purchased tariffs') ?></a>
            </li>

            <li><a href="#blocking" class="t09"
                   data-href="<?= Url::to(['/client/base/client-has-blocked', 'id' => get('id')]) ?>"><?= t('client',
                        'Blocking') ?></a>
            </li>
        </ul>
    </div>
    <div class="tabs_content">
        <div id="t01">
            <?= $this->render(app()->user->can('drivers') ? '_form' : '_form_read',
                compact('model', 'formData', 'workerDocumentMap',
                    'scanDocumentMap', 'documentMap', 'isCreateForm', 'showFieldList')) ?>
        </div>
        <div id="t02"></div>
        <div id="t03"></div>
        <div id="t04"></div>
        <div id="t05"></div>
        <div id="t06" class="js-shifts-tab"></div>
        <div id="t07"></div>
        <div id="t08"></div>
        <div id="t09"></div>
    </div>
</section>
