<?php

/* @var string $pjaxId */
/* @var array $cityList */
/* @var array $positionList */
/* @var array $carList */
/* @var array $groupWorkersList */
/* @var array $onlineWorkers */
/* @var array $companyList */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\employee\models\worker\WorkerSearch */
/* @var string $workerDetailViewUrl */
/* @var string $type */

use frontend\modules\employee\models\worker\Worker;
use frontend\modules\tenant\models\Currency;
use frontend\widgets\filter\Filter;
use frontend\widgets\LinkPager;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;
use app\modules\tenant\models\User;

$workerDetailViewUrl = getValue($workerDetailViewUrl, '/employee/worker/update');



$filter = Filter::begin([
    'id'     => 'filter_' . $pjaxId,
    'pjaxId' => $pjaxId,
    'model'  => $searchModel,
]);
echo $filter->checkboxList('city_id', $cityList, ['id' => $pjaxId . '-city_id']);
echo $filter->checkboxList('positionList', $positionList, ['id' => $pjaxId . '-positionList']);
if (!app()->user->can(User::ROLE_STAFF_COMPANY)) {
    echo $filter->checkboxList('groupWorkersList', $groupWorkersList, ['id' => $pjaxId . '-groupWorkersList']);
}
echo $filter->checkboxList('carList', $carList, ['id' => $pjaxId . '-carList']);
if (!app()->user->can(User::ROLE_STAFF_COMPANY)) {
    echo $filter->checkboxList('tenantCompanyIds', $companyList, ['id' => $pjaxId . '-companyList']);
}
echo $filter->input('stringSearch', ['id' => $pjaxId . '-stringSearch']);
if ($type === Worker::ACTIVE) {
    echo $filter->checkbox('onlyOnline', ['id' => $pjaxId . '-onlyOnline']);
}
Filter::end();

Pjax::begin(['id' => $pjaxId]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'       => "{items}\n{pager}",
    'tableOptions' => [
        'class' => 'people_table',
    ],
    'pager'        => [
        'class'         => LinkPager::className(),
        'prevPageLabel' => t('app', 'Prev'),
        'nextPageLabel' => t('app', 'Next'),
    ],
    'columns'      => [
        [
            'attribute'      => 'fullName',
            'label'          => t('app', 'Full name'),
            'contentOptions' => function ($model, $key, $index, $column) {
                return ['class' => 'pt_fio pt_wor'];
            },
            'content'        => function ($model) use ($workerDetailViewUrl, $onlineWorkers) {
                $innerHtml = Html::tag('span', $model->logo, ['class' => 'pt_photo']);
                $innerHtml .= Html::tag('span', Html::encode($model->fullName), ['class' => 'pt_fios']);
                $innerHtml .= ' ' . Html::tag('span', ' ', [
                        'class' => 'pt_' . (in_array($model->worker_id, $onlineWorkers) ? 'online' : 'offline'),
                    ]);

                return Html::a($innerHtml, [$workerDetailViewUrl, 'id' => $model->worker_id], ['data-pjax' => 0]);
            },
            'headerOptions'  => ['class' => 'pt_wor pt_fio'],
        ],
        [
            'attribute'      => 'callsign',
            'contentOptions' => function ($model, $key, $index, $column) {
                return ['class' => 'pt_pos'];
            },
            'headerOptions'  => ['class' => 'pt_pos'],
            'label'          => t('employee', 'Callsign'),
        ],
        [
            'attribute' => 'position',
            'label'     => t('employee', 'Profession'),
            'content'   => function ($model) {
                $positionNameList = ArrayHelper::getColumn($model->positions, function ($item) {
                    return t('employee', Html::encode($item['name']));
                });

                return implode(', ', $positionNameList);
            },
        ],
        [
            'label'   => t('balance', 'Balance'),
            'content' => function ($model) {
                $summary = [];
                if (is_array($model['accounts'])) {
                    foreach ($model['accounts'] as $account) {
                        $currencySymbol = Currency::getCurrencySymbol($account['currency_id']);
                        $minorUnit      = Currency::getMinorUnit($account['currency_id']);
                        $summary[]      = app()->formatter->asMoney(
                            round(+$account['balance'], $minorUnit), $currencySymbol);
                    }
                }

                return implode(', ', $summary);
            },
        ],
        [
            'attribute'      => 'availableCars',
            'label'          => t('car', 'Car'),
            'contentOptions' => function ($model, $key, $index, $column) {
                return ['class' => 'pt_au'];
            },
            'content'        => function ($model) {
                return $this->render('_carTd', ['model' => $model]);
            },
            'headerOptions'  => ['class' => 'pt_au'],
        ],

    ],
]);

Pjax::end();