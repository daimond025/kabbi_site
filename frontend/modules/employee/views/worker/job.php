<?php
/* @var $this yii\web\View */
/* @var array $moduleMap */
/* @var integer $worker_id */
/* @var array $arPositionData */
/* @var string $actionUrl */
/* @var array $showFieldList */

\frontend\modules\employee\assets\job\JobAsset::register($this);
$key = 1;
?>
<a href="#add_prof" class="button add_prof" style="float: right; position: relative; top: 5px;"><?= t('app',
        'Add') ?></a>
<?= $this->render('/position/_addPositionForm', ['moduleMap' => $moduleMap, 'worker_id' => $worker_id]) ?>

<? foreach ($arPositionData as $arPosition) {
    if (!isset($moduleMap[$arPosition['position']->module_id])) {
        continue;
    }
    echo $this->render('/position/index', [
        'position'            => $arPosition['position'],
        'cars'                => $arPosition['cars']['car_data'],
        'group_data'          => getValue($arPosition['cars']['group_data']),
        'workerHasPositionId' => $arPosition['workerHasPositionId'],
        'documentMap'         => $arPosition['documentMap'],
        'fieldMap'            => $arPosition['fieldMap'],
        'fieldModels'         => $arPosition['fieldModels'],
        'active'              => $arPosition['active'],
        'positionClassMap'    => $arPosition['positionClassMap'],
        'positionGroupMap'    => $arPosition['positionGroupMap'],
        'groupId'             => $arPosition['groupId'],
        'workerDocumentMap'   => $arPosition['workerDocumentMap'],
        'scanDocumentMap'     => $arPosition['scanDocumentMap'],
        'worker_id'           => $worker_id,
        'actionUrl'           => $actionUrl,
        'moduleMap'           => $moduleMap,
        '$showFieldList'      => $showFieldList,
        'pjaxId'              => 'pjax-' . $key++,
    ]);
} ?>
