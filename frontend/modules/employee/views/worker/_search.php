<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\employee\models\worker\WorkerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="worker-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'worker_id') ?>

    <?= $form->field($model, 'tenant_id') ?>

    <?= $form->field($model, 'city_id') ?>

    <?= $form->field($model, 'callsign') ?>

    <?= $form->field($model, 'password') ?>

    <?php // echo $form->field($model, 'last_name') ?>

    <?php // echo $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'second_name') ?>

    <?php // echo $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'photo') ?>

    <?php // echo $form->field($model, 'device') ?>

    <?php // echo $form->field($model, 'device_token') ?>

    <?php // echo $form->field($model, 'lang') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'device_info') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'partnership') ?>

    <?php // echo $form->field($model, 'birthday') ?>

    <?php // echo $form->field($model, 'block') ?>

    <?php // echo $form->field($model, 'create_time') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
