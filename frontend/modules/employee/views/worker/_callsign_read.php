<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $worker \frontend\modules\employee\models\worker\Worker */
?>
<div class="non-editable">
    <section class="row">
        <div class="row_label">
            <label>
                <?=$worker->getAttributeLabel('callsign');?>
                <a data-tooltip="2" class="tooltip">?
                    <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint', 'The unique identifier of the worker. Used for authorization in the worker application.'); ?>
                </span>
                </a>
            </label>
        </div>
        <div class="row_input">
            <?= Html::encode($worker->callsign); ?>
        </div>
    </section>
</div>