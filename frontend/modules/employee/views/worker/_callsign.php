<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $worker \frontend\modules\employee\models\worker\Worker */
/* @var $form yii\widgets\ActiveForm */

$form = ActiveForm::begin([
    'id'                     => 'worker-callsign-form',
    'errorCssClass'          => 'input_error',
    'successCssClass'        => 'input_success',
    'enableClientValidation' => true,
]);
?>
    <section class="row">
        <?= $form->field($worker, 'callsign')->begin(); ?>
        <div class="row_label">
            <?= Html::activeLabel($worker, 'callsign') ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint', 'The unique identifier of the worker. Used for authorization in the worker application.'); ?>
                </span>
            </a>
        </div>
        <div class="row_input">
            <?= Html::activeTextInput($worker, 'callsign') ?>
            <?= Html::error($worker, 'callsign',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($worker, 'callsign')->end(); ?>
    </section>
    <section class="row">
        <?= $form->field($worker, 'password')->begin(); ?>
        <div class="row_label">
            <label><?= t('app', 'Сhange password') ?></label>
        </div>
        <div class="row_input">
            <?= Html::activePasswordInput($worker, 'password', ['value' => '']) ?>
            <?= Html::error($worker, 'password',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($worker, 'password')->end(); ?>
    </section>
    <section class="row">
        <?= $form->field($worker, 'password_repeat')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($worker, 'password_repeat') ?></div>
        <div class="row_input">
            <?= Html::activePasswordInput($worker, 'password_repeat', ['value' => '']) ?>
            <?= Html::error($worker, 'password_repeat',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($worker, 'password_repeat')->end(); ?>
    </section>
    <section class="submit_form">
        <?= Html::submitInput(t('app', 'Save')) ?>
    </section>
<?php ActiveForm::end(); ?>