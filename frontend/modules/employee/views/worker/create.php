<?php
/* @var $model \common\modules\employee\models\worker\Worker */
/* @var $showFieldList array */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

$this->title = Yii::t('employee', 'Add worker');

$isCreateForm = true;

echo Breadcrumbs::widget([
    'links'        => [
        ['label' => t('employee', 'Workers'), 'url' => ['/employee/worker/list']],
    ],
    'itemTemplate' => '{link}',
    'tag'          => 'div',
    'options'      => ['class' => 'bread'],
    'homeLink'     => false,
]);
?>
<h1><?= Html::encode($this->title) ?></h1>
<section class="main_tabs">
    <div class="tabs_content">
        <div class="active" id="t01">
            <?= $this->render('_form',
                compact('model', 'formData', 'isCreateForm', 'showFieldList')) ?>
        </div>
    </div>
</section>
