<?php

use common\modules\employee\models\worker\WorkerHasCity;
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\balance\models\Account;
use yii\widgets\Breadcrumbs;
use backend\assets\AppAsset;

/* @var $this yii\web\View */
/* @var $model \frontend\modules\employee\models\worker\Worker */
/* @var $showFieldList array */

frontend\modules\employee\assets\workCar\WorkCarAsset::register($this);

?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.css" integrity="sha256-PF6MatZtiJ8/c9O9HQ8uSUXr++R9KBYu4gbNG5511WE=" crossorigin="anonymous" />
<script>

    function init() {
        select_init();
        draggable();
        show_hide_select();
    }

    function show_hide_select() {
        $('a.a_sc').click(function (event) {
            event.preventDefault();
            if($(this).hasClass('sel_active')){

                $(this).removeClass('sel_active');
                let div_id = $(this).attr('id');
                console.log(div_id);
                console.log($('div#' + div_id));

                $('div#' + div_id).each(function (index ,element) {
                    $(element).css('display', 'none');
                });
                event.stopPropagation();
            }
        });
    }

    // link car
    function render( idWorker,idCar){

        let show = true;
        let display_tariff =  $('#cont-2-link-id-'+idCar + ' #tariffs_config').css('display');
        if(display_tariff === 'block' ){
            show = false;
            $('#cont-2-link-id-'+idCar + ' #tariffs_config').css('display', 'none')
        }else {
            $('#cont-2-link-id-'+idCar + ' #tariffs_config').css('display', 'block')
        }

        if(show){
            $.ajax({
                type: "GET",
                url: "/employee/worker/worker-link-car",
                data: "worker_id=" + idWorker + "&car_id=" + idCar,

                success: function (msg) {

                    let car_info  = (typeof msg.cars_date.html !== "undefined" ) ? msg.cars_date.html : '';
                    if(car_info.length  === ''){
                        return;
                    }
                    $('#insert_car_item_'+idCar).empty();
                    $('#insert_car_item_'+idCar).append(car_info);
                    $('#insert_car_item_'+idCar).removeClass('cont-2-link-id');
                    select_init();

                }
            });
        }

    }
    // unlink car
    function unLink(car_id, worker_id) {

        $.ajax({
            type: "GET",
            url: "/employee/worker/worker-unlink-car",

            data: "worker_id="+ worker_id +"&car_id="+car_id,

            success: function(msg){
                let car_item  = msg.html;
               /* if( (parseInt(msg.rezult)   === 0)){
                    return;
                }*/
                $('#insert_car_item_'+car_id).remove();
                $('#cars').append(car_item);
                draggable();


            }
        });

    }
    // add group
    function changeGroup(event, car_id) {
        let group_id = event.target.value;

        $.ajax({
            type: "GET",
            url: "/employee/worker/add-driver-group",

            data: "car_id="+ car_id +"&group_id="+group_id,

            success: function(msg){
                let car_item  = msg.html.html;
                if( (parseInt(msg.result)   === 0)){
                     return 0;
                }
                $('#insert_car_item_'+car_id).empty();
                $('#insert_car_item_'+car_id).append(car_item);
                $('#insert_car_item_'+car_id).removeClass('cont-2-link-id');
                init();

            }
        });        
    }
    // changetariff
    function changeTariff(event, car_id) {
        let tariff= event.target;
        let all_tariff= $(tariff).closest(' ul.group_tariff');

        let value_tariff = $(all_tariff).find("input[type=checkbox]");
        let select_tariff = [];
        $(all_tariff).find("input[type=checkbox]").each(function (index ,element) {
            if($(element).is(":checked")){
                select_tariff.push($(element).val());
            }
        });

        let param = '';
        if(select_tariff.length !== 0){
            param = "&tariffs=" + select_tariff.join(',');
        }


        let select_tariff_id = select_tariff.join(',');

        $.ajax({
            type: "GET",
            url: "/employee/worker/add-driver-group-tariff",

            data: "car_id="+ car_id + param,

            success: function(msg){
                if( (parseInt(msg.rezult)   === 1)){
                    show_hide_select();
                    return 1;
                }
            }
        });
    }

    // change Class car
    function changeClass(event, car_id) {
        let tariff= event.target;
        let all_class= $(tariff).closest(' ul.group_class');

        let value_tariff = $(all_class).find("input[type=checkbox]");
        let select_class = [];
        $(all_class).find("input[type=checkbox]").each(function (index ,element) {
            if($(element).is(":checked")){
                select_class.push($(element).val());
            }
        });

        let param = '';
        if(select_class.length !== 0){
            param = "&classes=" + select_class.join(',');
        }

        $.ajax({
            type: "GET",
            url: "/employee/worker/add-driver-group-class",

            data: "car_id="+ car_id + param,

            success: function(msg){
                console.log( msg);
                if( (parseInt(msg.rezult)  === 1)){
                    show_hide_select();
                    return 1;
                }
            }
        });
    }

     // draggable
    function draggable(){
        $('.draggable').draggable({
            containment: "#wrapper",
            cursor: "pointer",
            revert: true
        });
    }

    $( document ).ready(function() {
        $(function() {

            // берем элемент
            $('.draggable').draggable({
                containment: "#wrapper",
                cursor: "pointer",
                revert: true
            });

            $('.droppable').droppable({

                out:function(event, ui){

                },

                activate: function(event, ui){

                    var idCar =  ui.helper.context.dataset.id;

                    var  idWorker = event.target.dataset.ar;

                    var car  = $('#'+ui.helper.context.attributes[0].nodeValue);

                    var worker = $('#'+event.target.id);

                    //$('#cont-2-link-id-mal-'+idCar).remove();

                },

                drop: function(event, ui) {
                    var idCar =  ui.helper.context.dataset.id;
                    var idWorker = event.target.dataset.ar;

                    var car  = $('#'+ ui.helper.context.attributes[0].nodeValue);
                    var worker = $('#'+ event.target.id);

                    car.css({"position": "relative","top":0,"left":0});
                    //car.attr('class','cont-2-link');

                    // чтоб элемент сам себя не добавил второй раз
                    if($('.driver #insert_car_item_'+idCar).length !== 1){
                        worker.append(car);
                        // car.append('<div  id="cont-2-link-id-mal-'+idCar+'" > </div>');
                        render(idWorker ,idCar);
                    }

                },

                activeClass: "active2",
                hoverClass: "hover",
                tolerance: "fit"
            });

            <?php foreach ($workerList as $worker): ?>
            $('#touchDrop-<?= $worker['worker_id'] ?>').droppable("option", "tolerance", "touch");
            <?php endforeach ; ?>
        });

        $('span#icon').click(function () {
            let worker_id = $(this).data('worker_id');
            $('div#worker_car_container_' +worker_id ).slideToggle(300);

        });
    });




</script>
<style>

    .wrapper{
        font-size: 18px;
        padding: 10px;
        overflow-y: scroll;
        height: 60vh;
        width: 100%;
        display: grid;
        grid-gap: 15px;
        grid-template-columns: 1fr 1fr 1fr;
    }
    .cont-2{
        grid-column-start: 2;
       /* background: #fffad3;*/
        grid-column-end: 4;
    }
    .cont-2-driver-title{
        font-size: 25px;
        font-weight:bold;
        margin-top: 20px;
        display: flex;
        flex-direction: column;
        justify-content: space-around;
        align-items: center;
        padding-bottom: 10px;
    }
    .cont-2-link-id-mal{
        padding-left: 15px;
    }
    .cont-2-link-id-mal a{
        color: #127cce
    }

    /* item link car*/
    .cont-2-link{
        border: 1px solid rgba(54, 53, 54, 0.91);
        align-content: space-around;
        padding: 10px;

        display: grid;
        grid-template-areas:
                "car show_hide"
                "tariffs_config tariffs_config";
        grid-template-rows: 1fr 0fr ;
        grid-template-columns: 70% 30%;
        background: rgba(195, 198, 194, 0.91);
    }
    .cont-2-link_show{
        transition:all 300ms ease;
        grid-template-rows: 1fr 1fr ;
    }
    .cont-2-link_hide{
        transition:all 300ms ease;
        grid-template-rows: 1fr 0fr ;
    }
    #car_name{
        grid-area: car;
    }
    .up-link_driver{
        display: flex;
        justify-content: center;
    }
    #show_hide{
        grid-area: show_hide;
    }
    #tariffs_config{
        grid-area: tariffs_config;

    }



    .driver{
        border-radius: 10px ;
        border: 1px solid rgba(54, 53, 54, 0.91);
    }

    .driver-mal{
        text-align: center;
        background: #ffc607;
        border-top-left-radius: 8px;
        border-top-right-radius:8px ;
        font-size: 25px;
        font-weight: bold;

    }
    .active2{
        background: rgb(210, 221, 255);
    }


    /* Cars*/
    .cont-1{

    }
    .cont-2-link-id-title{
        font-size: 25px;
        font-weight:bold;
        margin-top: 20px;
        display: flex;
        flex-direction: column;
        justify-content: space-around;
        align-items: center;
    }
    .cont-2-link-id{
        box-shadow: 0 0 5px 2px rgba(0,0,0,.35);
        background: rgba(195, 198, 194, 0.91);
        margin-top: 20px;
        padding: 5px;
        display: flex;
        flex-direction: column;
        justify-content: space-around;
        align-items: center;
        cursor: pointer;
    }
    .cont-2-link{

    }
    .up-link{
        display: flex;
       /* align-content: space-around;*/
        align-items: center;
        justify-content: center;
    }
</style>
<div class="wrapper" id="wrapper">

    <div class="cont-1">
        <p class="cont-2-link-id-title cars"> <?=t('car', 'Cars')?></p>

        <div id="cars">
        <?php foreach ($carList as $car): ?>
           <div id="insert_car_item_<?= $car['car_id']?>"  class="draggable cont-2-link-id" data-id="<?= $car['car_id'] ?>">

            <div id="cont-2-link-id-<?= $car['car_id'] ?>"  >
                <div class="up-link">
                    <span> <?= $car['name']. " (" .$car['year'] .  ") - ". $car['gos_number']   ?>  </span>
                </div>

                <div  id="cont-2-link-id-mal-<?= $car['car_id']?>" >

                </div>

            </div>
           </div>
        <?php endforeach ; ?>
        </div>
    </div>


    <div class="cont-2">
        <p class="cont-2-driver-title"> <?=t('employee', 'Workers')?></p>
        <?php foreach ($workerList as $worker): ?>
              <div class="driver" data-id="<?= $worker['worker_id'] ?>">
                  <div class="driver-mal">
                          <a style="color: #000000" href="/employee/worker/update/<?=$worker['worker_id']?>">   <?= $worker['last_name']. " " . $worker['name'] . " (" . $worker['callsign'] .")"  ?>  </a>
                          <span id="icon" data-worker_id="<?= $worker['worker_id'] ?>" style="float: right; margin-right: 10px;">  <i id="worker_car_toggle"  class="fa fa-1x fa-caret-down"> </i> </span>


                  </div>

                  <div id="worker_car_container_<?= $worker['worker_id'] ?>" style="display: none">
            <?php if (!empty($worker["cars"]) ): ?>

                <?php foreach ($worker["cars"] as $car): ?>
                    <div id="insert_car_item_<?= $car['car_id']?>">

                    <div id="cont-2-link-id-<?= $car['car_id']?>" data-id="<?= $car['car_id']?>" data-ar="<?=$worker['worker_id']?>" class="draggable cont-2-link">

                        <div class="up-link_driver" id="car_name">
                            <a style="color: #0e3ada" href="/lib/car/update/<?=$car['car_id']?>">  <span> <?= $car['name']. " (" .$car['year'] .  ") - ". $car['gos_number']   ?>  </span> </a>
                        </div>
                        <div id="show_hide" data-id="cont-2-link-id-mal-<?= $car['car_id']?>" class="cont-2-link-id-mal">
                            <a style="float: right" id="a-btn-<?= $car['car_id']?>" onclick="render(<?= $worker['worker_id'] ?>,<?= $car['car_id']?>)"> <?= t('reports', 'Show/Hide')?></a>
                        </div>

                        <div id="tariffs_config" style="display: none" ></div>
                    </div>

                    </div>

                <?php endforeach ; ?>



            <?php endif  ?>
                  <div id="touchDrop-<?= $worker['worker_id'] ?>" data-ar="<?=$worker['worker_id']?>" class="droppable" style="min-height: 150px;margin-bottom: 100px">

                  </div>
                </div>
              </div>
        <?php endforeach ; ?>
    </div>
</div>