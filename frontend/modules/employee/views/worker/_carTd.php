<?php
/* @var $this yii\web\View */
/* @var $model \frontend\modules\employee\models\worker\Worker */
/* @var $car \frontend\modules\car\models\Car */

use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;

?>

    <div class="pt_buttons">
        <?php if ($model->phone !== '') { ?>
            <a href="tel:+<?= $model->phone ?>" class="call_now"><span></span></a>
        <?php } ?>
        <a href="#" class="pt_message" data-receivertype="worker"
           data-receiverid="<?= Html::encode($model->callsign) ?>"
           data-cityid="<?= Html::encode($model->getLastActiveCityId()) ?>"><span></span></a>
    </div>
<?
$cars = ArrayHelper::getColumn($model->availableCars, 'car');
foreach ($cars as $car): ?>
    <a data-pjax="0"
       href="<?= Url::to([
           '/car/car/update',
           'id' => Html::encode($car->car_id),
       ]) ?>"><?= Html::encode($car->name); ?></a>
    <span style="display: block; margin-bottom: 8px;"><span
                dir="auto"><?= Html::encode($car->gos_number); ?></span></span>
<? endforeach; ?>