<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\modules\employee\models\documents\Document;
use frontend\modules\employee\widgets\document\Documents;
use frontend\modules\employee\models\documents\WorkerDocumentScan;
use app\modules\tenant\models\field\FieldRecord;

/**
 * Created by PhpStorm.
 * User: artem
 * Date: 31.08.17
 * Time: 16:17
 */

/* @var $this yii\web\View */
/* @var $model \common\modules\employee\models\worker\Worker */
/* @var array $formData */
/* @var array $documentMap */

$this->registerJs('phone_mask_init();');
if (isset($jsInit) && $jsInit) {
    $this->registerJs('select_init();');
}
?>
<div class="non-editable">

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'cityIds') ?></div>
        <div class="row_input">
            <div class="row_input">
                <?= !empty($model->cityIds) ? implode(', ', array_map(function ($item) use ($formData) {
                    return ArrayHelper::getValue($formData['city_list'], $item);
                }, $model->cityIds)) : t('app', 'Choose city') ?>
            </div>
        </div>
    </section>

    <section class="row" id="logo">
        <div class="row_label"></div>
        <div class="row_input" style="background: none;">
            <div class="input_file_logo <!--file_loader-->">
                <?php if (file_exists(Yii::getAlias('@app') . '/web/' . '/upload/' . user()->tenant_id . '/thumb_' . Html::encode($model->photo))): ?>
                    <a class="input_file_logo logo cboxElement"
                       href="<?= $model->getPicturePath($model->photo) ?>"
                       style="border: none">
                        <div class="row_input" style="background: none"><span><img
                                        src="<?= $model->getPicturePath($model->photo, false) ?>"></span>
                        </div>
                    </a>
                <? endif ?>
            </div>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'partnership') ?></div>
        <div class="row_input">
            <div class="row_input">
                <?= ArrayHelper::getValue($model->getPartnerTypes(), $model->partnership, '') ?>
            </div>
        </div>
    </section>

    <section class="row">
        <div class="row_label">
            <div class="row_input">
                <?= Html::encode($model->getAttributeLabel('last_name')); ?>
                <?= Html::encode($model->getAttributeLabel('name')); ?>
                <?= Html::encode($model->getAttributeLabel('second_name')); ?>
            </div>
        </div>
        <div class="row_input">
            <div class="grid_3">
                <div class="row_input"><?= !empty($model->last_name) ? Html::encode($model->last_name) : '<div class="row_input"></div>' ?></div>
                <div class="row_input"><?= !empty($model->name) ? Html::encode($model->name) : '<div class="row_input"></div>' ?></div>
                <div class="grid_item"><?= !empty($model->second_name) ? Html::encode($model->second_name) : '<div class="row_input"></div>' ?></div>
            </div>
        </div>
    </section>

    <? if (ArrayHelper::getValue($showFieldList,'worker.' . FieldRecord::NAME_WORKER_BIRTHDAY, true)) : ?>
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'birthday') ?></div>
        <div class="row_input">
            <div class="row_input">
                <?= ArrayHelper::getValue($formData['birth_day'], $model->birth_day, '') ?>
                <?= ArrayHelper::getValue($formData['birth_month'], $model->birth_month, '') ?>
                <?= ArrayHelper::getValue($formData['birth_year'], $model->birth_year, '') ?>
            </div>
        </div>
    </section>
    <? endif; ?>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'phone') ?></div>
        <div class="row_input">
            <div class="input_with_icon">
                <div class="input_with_icon"><?= Html::encode($model->phone) ?></div>
                <span></span>
            </div>
        </div>
    </section>

    <? if (ArrayHelper::getValue($showFieldList,'worker.' . FieldRecord::NAME_WORKER_EMAIL, true)) : ?>
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'email') ?></div>
        <div class="row_input">
            <div class="input_with_icon">
                <div class="input_with_icon"><?= $model->email ? Html::encode($model->email) : '<div class="row_input"></div>' ?></div>
                <span></span>
            </div>
        </div>
    </section>
    <? endif; ?>

    <? if (ArrayHelper::getValue($showFieldList,'worker.' . FieldRecord::NAME_WORKER_CARD_NUMBER, true)) : ?>
        <section class="row">
            <div class="row_label"><?= Html::activeLabel($model, 'card_number') ?></div>
            <div class="row_input">
                <div class="input_with_icon">
                    <div class="input_with_icon"><?= $model->card_number ? Html::encode($model->card_number) : '<div class="row_input"></div>' ?></div>
                    <span></span>
                </div>
            </div>
        </section>
    <? endif; ?>

    <? if (ArrayHelper::getValue($showFieldList,'worker.' . FieldRecord::NAME_WORKER_YANDEX_ACCOUNT_NUMBER, true)) : ?>
        <section class="row">
            <div class="row_label"><?= Html::activeLabel($model, 'yandex_account_number') ?></div>
            <div class="row_input">
                <div class="input_with_icon">
                    <div class="input_with_icon"><?= $model->yandex_account_number ? Html::encode($model->yandex_account_number) : '<div class="row_input"></div>' ?></div>
                    <span></span>
                </div>
            </div>
        </section>
    <? endif; ?>

    <? if (ArrayHelper::getValue($showFieldList,'worker.' . FieldRecord::NAME_WORKER_DESCRIPTION, true)) : ?>
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'description') ?></div>
        <div class="row_input">
            <div class="input_with_icon">
                <div class="input_with_icon"><?= $model->description ? Html::encode($model->description) : '<div class="row_input"></div>' ?></div>
                <span></span>
            </div>
        </div>
    </section>
    <? endif; ?>

    <?= Documents::widget([
        //        'form'                 => $form,
        'entityHasDocumentMap' => $workerDocumentMap,
        'scanDocumentMap'      => $scanDocumentMap,
        'documentMap'          => $documentMap,
        'ajaxUpload'           => true,
        'scanModelConfig'      => [
            'class' => WorkerDocumentScan::className(),
        ],
        'entityId'             => get('id'),
        'isRead'               => true,
    ]) ?>
</div>


