<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\tenant\models\User;

/* @var string $pjaxId */
/* @var $this yii\web\View */
/* @var array $cityList */
/* @var array $positionList */
/* @var array $onlineWorkers */
/* @var array $companyList */
/* @var integer $notActivatedWorkers */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\employee\models\worker\WorkerSearch */
/* @var string $type */

$this->title = Yii::t('employee', 'Workers');

if (app()->user->can('drivers')) {
    echo Html::a(t('app', 'Add'), ['create'],
        ['class' => 'button', 'style' => 'float: right; position: relative; top: 5px;']);
} ?>
<h1><?= Html::encode($this->title) ?></h1>
<section class="main_tabs">
    <div class="tabs_links">
        <ul>
            <li><a href="#active" class="t01"><?= t('app', 'Active') ?></a></li>
            <li><a href="#blocked" class="t02" data-href="<?= Url::to('list-blocked') ?>"><?= t('app', 'Blocked') ?></a>
            <li><a href="#notActivated" class="t03" data-href="<?= Url::to('list-not-activated') ?>">
                    <?= t('app', 'Not activated') ?><sup><?= $notActivatedWorkers; ?></sup>
                </a>
            </li>
        </ul>
    </div>
    <div class="tabs_content">
        <div id="t01" data-view="index">
            <?= $this->render('_grid', compact('searchModel', 'dataProvider', 'cityList', 'pjaxId', 'positionList', 'onlineWorkers', 'type', 'groupWorkersList', 'carList', 'companyList')) ?>
        </div>
        <div id="t02" data-view="blocked">
        </div>
        <div id="t03" data-view="notActivated">
        </div>
    </div>
</section>
