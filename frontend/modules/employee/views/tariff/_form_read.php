<?php

use frontend\modules\employee\assets\tariff\TariffAsset;
use frontend\modules\employee\models\tariff\OptionTariff;
use frontend\modules\employee\models\tariff\Tariff;
use frontend\widgets\position\PositionChooser;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/* @var $this yii\web\View */
/* @var $tariff Tariff */
/* @var $formData array */
/* @var $currentOption OptionTariff */
/* @var $otherOptions OptionTariff[] */

TariffAsset::register($this);

$currencySymbol = Html::encode(getCurrencySymbol($tariff->city_list));

?>

<section class="row">
    <div class="row_label">
        <?= Html::activeLabel($tariff, 'city_list') ?>
    </div>
    <div class="row_input">
        <?= Html::activeDropDownList($tariff, 'city_list', $formData['USER_CITY_LIST'], [
            'data-placeholder' => Html::encode(
                $tariff->city_list ? $formData['USER_CITY_LIST'][$tariff->city_list] : t('app', 'Choose city')),
            'class'            => 'default_select',
            'disabled'         => true,
        ]) ?>
    </div>
</section>

<section class="row">
    <div class="row_label">
        <?= Html::activeLabel($tariff, 'name') ?>
        <a data-tooltip="2" class="tooltip">?
            <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint', 'The tariff name is displayed in the worker application.'); ?>
                </span>
        </a>
    </div>
    <div class="row_input">
        <?= Html::activeTextInput($tariff, 'name', ['dir' => 'auto', 'disabled' => true]) ?>
    </div>
</section>

<section class="row">
    <div class="row_label">
        <?= Html::activeLabel($tariff, 'description') ?>
        <a data-tooltip="2" class="tooltip">?
            <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint', 'Description allows you to specify the features of the tariff. This information the workers do not see.'); ?>
                </span>
        </a>
    </div>
    <div class="row_input">
        <?= Html::activeTextarea($tariff, 'description', ['dir' => 'auto', 'disabled' => true]) ?>
    </div>
</section>

<?= PositionChooser::widget([
    'form'                  => $form,
    'model'                 => $tariff,
    'carClassMap'           => $formData['CAR_CLASS_LIST'],
    'carPositionList'       => $formData['CAR_POSITION_LIST'],
    'positionMap'           => $formData['POSITION_MAP'],
    'positionDisabled'      => true,
    'carClassDisabled'      => true,
    'positionClassDisabled' => true,
]) ?>

<section class="row">
    <div class="row_label">
        <?= Html::activeLabel($tariff, 'type') ?>
    </div>
    <div class="row_input">
        <?= Html::activeDropDownList($tariff, 'type', $formData['TARIFF_TYPES'], [
            'data-placeholder' => !empty($tariff->type) ? Html::encode($formData['TARIFF_TYPES'][$tariff->type]) : t('driverTariff',
                'Choose a type'),
            'class'            => 'default_select',
            'disabled'         => true,
        ]) ?>
    </div>
</section>

<? $style = $tariff->type == 'ONCE' ? 'style="display: none"' : '' ?>

<section class="row" <?= $style ?>>
    <div class="row_label">
        <?= Html::activeLabel($tariff, 'subscription_limit_type') ?>
    </div>
    <div class="row_input">
        <?= Html::activeRadioList($tariff, 'subscription_limit_type', $tariff->getSubscriptionLimitTypeList(), [
            'item' => function ($index, $label, $name, $checked, $value) {
                return '<label class="label_js_inp" data-inp="' . $name . '[' . $index . ']">' .
                    Html::radio($name, $checked, ['value' => $value, 'disabled' => true]) . $label . '</label>';
            },
        ]) ?>
    </div>
</section>

<section class="row" <?= $style ?>>
    <div class="row_label">
        <?= Html::activeLabel($tariff, 'days') ?><a data-tooltip="5" class="tooltip">?<span
                    style="display: none; opacity: 0;"><?= HtmlPurifier::process(Yii::t('hint',
                    'Available for input integers greater than 0')) ?></span></a>
    </div>
    <div class="row_input">
        <?= Html::activeTextInput($tariff, 'days', ['class' => 'mini_input_2', 'disabled' => true]) ?>
    </div>
</section>

<? $styleSubscription = $tariff->type == 'SUBSCRIPTION' ? 'style="display: none"' : '' ?>
<section class="row" <?= $styleSubscription ?>>
    <div class="row_label">
        <?= Html::activeLabel($tariff, 'action_new_shift') ?>
    </div>
    <div class="row_input">
        <?= $form->field($tariff, 'action_new_shift')->begin(); ?>
        <?= Html::activeRadioList($tariff, 'action_new_shift', $tariff->getActionNewShift(), [
            'item' => function ($index, $label, $name, $checked, $value) {
                return '<label>'
                    . Html::radio($name, $checked, ['value' => $value, 'disabled' => true]) . $label
                    . '</label>';
            },
        ]) ?>
        <?= $form->field($tariff, 'action_new_shift')->end(); ?>
    </div>
</section>

<section class="row">
    <div class="row_label">
        <?= Html::activeLabel($tariff, 'period_type') ?>
    </div>
    <div class="row_input">
        <?= Html::activeRadioList($tariff, 'period_type', $tariff->getPeriodTypeList(), [
            'item' => function ($index, $label, $name, $checked, $value) {
                return '<label class="label_js_inp" data-inp="' . $name . '[' . $index . ']">' .
                    Html::radio($name, $checked, ['value' => $value, 'disabled' => true]) .
                    $label .
                    '</label>';
            },
        ]) ?>
        <div data-input="<?= $tariff->formName() ?>[period_type][0]"
             class="label_js_input<? if ($tariff->period_type === Tariff::PERIOD_TYPE_INTERVAL || empty($tariff->period_type)): ?> active_input<? endif ?>">

            <?= Html::activeTextInput($tariff, 'start_interval', ['type' => 'time', 'disabled' => true]) ?> -
            <?= Html::activeTextInput($tariff, 'end_interval', ['type' => 'time', 'disabled' => true]) ?>
        </div>
        <div style="height: 30px;" data-input="<?= $tariff->formName() ?>[period_type][1]"
             class="label_js_input<? if ($tariff->period_type === Tariff::PERIOD_TYPE_HOUR): ?> active_input<? endif ?> grid_3">
            <?= Html::activeDropDownList($tariff, 'period', $tariff->getHoursList(), [
                'class'    => 'default_select',
                'disabled' => true,
            ]) ?>
        </div>
    </div>
</section>

<section class="row">
    <div class="row_label">
        <?=Html::activeLabel($tariff, $formData['LABELS']['COST']);?>
    </div>
    <div class="row_input">
        <?= Html::activeTextInput($tariff, 'cost', ['class' => 'mini_input_2', 'disabled' => true]) ?>
        <span class="subtext"><?= $currencySymbol ?></span>
    </div>
</section>

<? if (!$tariff->isNewRecord): ?>
    <?= $this->render('_optionGrid', [
        'currentOption' => $currentOption,
        'otherOptions'  => $otherOptions,
        'cityId'        => $tariff->city_list,
        'disabled'      => true,
    ]) ?>
<? endif ?>
