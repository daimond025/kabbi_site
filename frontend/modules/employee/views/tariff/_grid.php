<?php

/* @var string $pjaxId */
/* @var array $cityList */
/* @var array $positionList */
/* @var array $tenantCompany */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\employee\models\worker\WorkerSearch */
/* @var string $tariffDetailViewUrl */

use frontend\widgets\filter\Filter;
use frontend\widgets\LinkPager;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use app\modules\tenant\models\User;

$tariffDetailViewUrl = getValue($tariffDetailViewUrl, '/employee/tariff/update');
?>

<? $filter = Filter::begin([
    'pjaxId' => $pjaxId,
    'model'  => $searchModel,
]); ?>

<?= $filter->checkboxList('city_list', $cityList); ?>
<?= $filter->checkboxList('position_id', $positionList); ?>
<?//
//    if (!app()->user->can(User::USER_ROLE_4)) {
//        echo $filter->checkboxList('tenant_company_id', $tenantCompany);
//    }
//?>
<? Filter::end(); ?>


<? Pjax::begin(['id' => $pjaxId]) ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'       => "{items}\n{pager}",
    'tableOptions' => [
        'class' => 'people_table',
    ],
    'pager'        => [
        'class'         => LinkPager::className(),
        'prevPageLabel' => t('app', 'Prev'),
        'nextPageLabel' => t('app', 'Next'),
    ],
    'columns'      => [
        [
            'attribute' => 'name',
            'content'   => function ($model) use ($tariffDetailViewUrl) {
                return Html::a(Html::encode($model->name), [$tariffDetailViewUrl, 'id' => $model->tariff_id],
                    ['data-pjax' => 0]);
            },
        ],
        [
            'attribute' => 'type',
            'content'   => function ($model) {
                return $model->getTariffTypes()[$model->type];
            },
        ],
        [
            'attribute' => 'class',
            'label'     => t('employee', 'Class'),
            'content'   => function ($model) {
                if (!empty($model['class_id'])) {
                    return t('car', Html::encode($model['class']['class']));
                }

                if (!empty($model['position_class_id'])) {
                    return t('employee', Html::encode($model['positionClass']['name']));
                }

                return null;
            },
        ],
        [
            'attribute' => 'city',
            'label'     => t('employee', 'City'),
            'content'   => function ($model) {
                $attribute = 'name' . getLanguagePrefix();

                return Html::encode($model->cities[0]->$attribute);
            },
        ],
    ],
]); ?>
<? Pjax::end() ?>