<?php

use frontend\modules\employee\models\tariff\OptionTariff;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $form ActiveForm */
/* @var $option OptionTariff */
/* @var $types array */
/* @var $currencySymbol string */

$formatter = app()->formatter;

?>

<section class="row">
    <div class="row_label">
        <label><?= t('driverTariff', 'Surcharge for order') ?></label>
    </div>
    <div class="row_input">
        <div class="add-cost-to-orders">
            <div class="left">
                <div>
                    <label>
                        <input type="checkbox" disabled <?= empty($option->increase_order_sum) ? '' : 'checked' ?>>
                        <?= $option->getAttributeLabel('increase_order_sum') ?>
                    </label>
                </div>
                <div>
                    <span><?= $formatter->asMoney($option->increase_order_sum, $currencySymbol) ?></span>
                </div>
            </div>
            <div class="right">
                <div>
                    <label>
                        <input type="checkbox" disabled <?= empty($option->increase_sum_fix) ? '' : 'checked' ?>>
                        <?= $option->getAttributeLabel('increase_sum_fix') ?>
                    </label>
                </div>
                <div>
                    <span>
                        <?= $formatter->asMoney($option->increase_sum_fix, $types[$option->increase_sum_fix_type]) ?>
                    </span>
                </div>
                <div>
                    <label>
                        <input type="checkbox" disabled <?= empty($option->increase_sum_limit) ? '' : 'checked' ?>>
                        <?= $option->getAttributeLabel('increase_sum_limit') ?>
                    </label>
                </div>
                <div>
                    <span><?= $formatter->asMoney($option->increase_sum_limit, $currencySymbol) ?></span>
                </div>
            </div>
        </div>
    </div>
</section>

