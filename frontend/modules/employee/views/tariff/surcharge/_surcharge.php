<?php

use frontend\modules\employee\models\tariff\OptionTariff;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $form ActiveForm */
/* @var $option OptionTariff */
/* @var $types array */
/* @var $currencySymbol string */

?>

<section class="row">
    <div class="row_label">
        <label><?= t('driverTariff', 'Surcharge for order') ?></label>
    </div>
    <div class="row_input">
        <div class="add-cost-to-orders">
            <div class="left">
                <div>
                    <label>
                        <input class="js-trigger-disabled" data-trigger-target="input-1" type="checkbox"
                            <?= empty($option->increase_order_sum) ? '' : 'checked' ?>>
                        <?= $option->getAttributeLabel('increase_order_sum') ?>
                    </label>
                </div>
                <?= $form->field($option, 'increase_order_sum')->begin(); ?>
                <?= Html::activeTextInput($option, 'increase_order_sum', [
                    'class'    => 'input_number_mini',
                    'data'     => ['trigger' => 'input-1'],
                    'readonly' => empty($option->increase_order_sum),
                ]) ?>
                <span><?= $currencySymbol ?></span>
                <?= Html::error($option, 'increase_order_sum',
                    ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']) ?>
                <?= $form->field($option, 'increase_order_sum')->end(); ?>
            </div>
            <div class="right">
                <div>
                    <label>
                        <input class="js-trigger-disabled" type="checkbox" data-trigger-target="input-2"
                            <?= empty($option->increase_sum_fix) ? '' : 'checked' ?>>
                        <?= $option->getAttributeLabel('increase_sum_fix') ?>
                    </label>
                </div>
                <div>
                    <?= $form->field($option, 'increase_sum_fix')->begin(); ?>
                    <?= Html::activeTextInput($option, 'increase_sum_fix', [
                        'class'    => 'input_number_mini',
                        'data'     => ['trigger' => 'input-2'],
                        'readonly' => empty($option->increase_sum_fix),
                    ]) ?>
                    <span>
                        <?= Html::activeDropDownList($option, 'increase_sum_fix_type', $types, [
                            'class'    => 'default_select',
                            'data'     => ['trigger' => 'input-2'],
                            'readonly' => empty($option->increase_sum_fix),
                        ]) ?>
                    </span>
                    <?= Html::error($option, 'increase_sum_fix',
                        ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']) ?>
                    <?= $form->field($option, 'increase_sum_fix')->end(); ?>
                </div>
                <div>
                    <label>
                        <input class="js-trigger-disabled" type="checkbox" data-trigger-target="input-3"
                            <?= empty($option->increase_sum_limit) ? '' : 'checked' ?>>
                        <?= $option->getAttributeLabel('increase_sum_limit') ?>
                    </label>
                </div>
                <div>
                    <?= $form->field($option, 'increase_sum_limit')->begin(); ?>
                    <?= Html::activeTextInput($option, 'increase_sum_limit', [
                        'class'    => 'input_number_mini',
                        'data'     => ['trigger' => 'input-3'],
                        'readonly' => empty($option->increase_sum_limit),
                    ]) ?>
                    <span><?= $currencySymbol ?></span>
                    <?= Html::error($option, 'increase_sum_limit',
                        ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']) ?>
                    <?= $form->field($option, 'increase_sum_limit')->end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>

