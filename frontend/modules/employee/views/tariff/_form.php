<?php

use frontend\modules\employee\assets\tariff\TariffAsset;
use frontend\modules\employee\models\tariff\OptionTariff;
use frontend\modules\employee\models\tariff\Tariff;
use frontend\widgets\position\PositionChooser;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $tariff Tariff */
/* @var $formData array */
/* @var $currentOption OptionTariff */
/* @var $otherOptions OptionTariff[] */

TariffAsset::register($this);

$currencySymbol = Html::encode(getCurrencySymbol($tariff->city_list));

?>

<? $form = ActiveForm::begin([
    'id'                     => 'driver-tariff-form',
    'errorCssClass'          => 'input_error',
    'successCssClass'        => 'input_success',
    'enableClientValidation' => true,
]); ?>

<?= Html::activeHiddenInput($tariff, 'tariff_id') ?>

    <section class="row">
        <?= $form->field($tariff, 'city_list')->begin(); ?>
        <div class="row_label">
            <?= Html::activeLabel($tariff, 'city_list') ?>
        </div>
        <div class="row_input">
            <?= Html::activeDropDownList($tariff, 'city_list', $formData['USER_CITY_LIST'], [
                'data-placeholder' => Html::encode(
                    $tariff->city_list ? $formData['USER_CITY_LIST'][$tariff->city_list] : t('app', 'Choose city')),
                'class'            => 'default_select',
            ]) ?>
            <?= $form->field($tariff, 'city_list')->end(); ?>
        </div>
    </section>

    <section class="row">
        <?= $form->field($tariff, 'name')->begin(); ?>
        <div class="row_label">
            <?= Html::activeLabel($tariff, 'name') ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint', 'The tariff name is displayed in the worker application.'); ?>
                </span>
            </a>
        </div>
        <div class="row_input">
            <?= Html::activeTextInput($tariff, 'name', ['dir' => 'auto']) ?>
        </div>
        <?= $form->field($tariff, 'name')->end(); ?>
    </section>

    <section class="row">
        <?= $form->field($tariff, 'description')->begin(); ?>
        <div class="row_label">
            <?= Html::activeLabel($tariff, 'description') ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint', 'Description allows you to specify the features of the tariff. This information the workers do not see.'); ?>
                </span>
            </a>
        </div>
        <div class="row_input">
            <?= Html::activeTextarea($tariff, 'description', ['dir' => 'auto']) ?>
        </div>
        <?= $form->field($tariff, 'description')->end(); ?>
    </section>

<?= PositionChooser::widget([
    'form'            => $form,
    'model'           => $tariff,
    'carClassMap'     => $formData['CAR_CLASS_LIST'],
    'carPositionList' => $formData['CAR_POSITION_LIST'],
    'positionMap'     => $formData['POSITION_MAP'],
]) ?>

    <section class="row">
        <?= $form->field($tariff, 'type')->begin(); ?>
        <div class="row_label">
            <?= Html::activeLabel($tariff, 'type') ?>
        </div>
        <div class="row_input">
            <?= Html::activeDropDownList($tariff, 'type', $formData['TARIFF_TYPES'], [
                'data-placeholder' => !empty($tariff->type) ? Html::encode($formData['TARIFF_TYPES'][$tariff->type]) : t('driverTariff',
                    'Choose a type'),
                'class'            => 'default_select',
                'disabled'         => $tariff->isNewRecord ? false : 'disabled',
            ]) ?>
        </div>
        <?= $form->field($tariff, 'type')->end(); ?>
    </section>

<? $styleOnce = $tariff->type == 'ONCE' ? 'style="display: none"' : '' ?>

    <section class="row" <?= $styleOnce ?>>
        <div class="row_label">
            <?= Html::activeLabel($tariff, 'subscription_limit_type') ?>
        </div>
        <div class="row_input">
            <?= $form->field($tariff, 'subscription_limit_type')->begin(); ?>
            <?= Html::activeRadioList($tariff, 'subscription_limit_type', $tariff->getSubscriptionLimitTypeList(), [
                'item' => function ($index, $label, $name, $checked, $value) {
                    return '<label class="label_js_inp" data-inp="' . $name . '[' . $index . ']">' .
                        Html::radio($name, $checked, ['value' => $value]) . $label . '</label>';
                },
            ]) ?>
            <?= Html::error($tariff, 'subscription_limit_type',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']) ?>
            <?= $form->field($tariff, 'subscription_limit_type')->end(); ?>
        </div>
    </section>

    <section class="row" <?= $styleOnce ?>>
        <?= $form->field($tariff, 'days')->begin(); ?>
        <div class="row_label">
            <?= Html::activeLabel($tariff, 'days') ?><a data-tooltip="5" class="tooltip">?<span
                        style="display: none; opacity: 0;"><?= HtmlPurifier::process(Yii::t('hint',
                        'Available for input integers greater than 0')) ?></span></a>
        </div>
        <div class="row_input">
            <?= Html::activeTextInput($tariff, 'days', ['class' => 'mini_input_2']) ?>
        </div>
        <?= $form->field($tariff, 'days')->end(); ?>
    </section>


<? $styleSubscription = $tariff->type == 'SUBSCRIPTION' ? 'style="display: none"' : '' ?>
    <section class="row" <?= $styleSubscription ?>>
        <div class="row_label">
            <?= Html::activeLabel($tariff, 'action_new_shift') ?>
        </div>
        <div class="row_input">
            <?= $form->field($tariff, 'action_new_shift')->begin(); ?>
            <?= Html::activeRadioList($tariff, 'action_new_shift', $tariff->getActionNewShift(), [
            'item' => function ($index, $label, $name, $checked, $value) {
                return '<label>'
                    . Html::radio($name, $checked, ['value' => $value]) . $label
                    . '</label>';
            },
        ]) ?>
            <?= $form->field($tariff, 'action_new_shift')->end(); ?>
        </div>
    </section>

    <section class="row">
        <div class="row_label">
            <?= Html::activeLabel($tariff, 'period_type') ?>
        </div>
        <div class="row_input">
            <?= $form->field($tariff, 'period_type')->begin(); ?>
            <?= Html::activeRadioList($tariff, 'period_type', $tariff->getPeriodTypeList(), [
                'item' => function ($index, $label, $name, $checked, $value) {
                    return '<label class="label_js_inp" data-inp="' . $name . '[' . $index . ']">' .
                        Html::radio($name, $checked, ['value' => $value]) .
                        $label .
                        '</label>';
                },
            ]) ?>
            <?= $form->field($tariff, 'period_type')->end(); ?>
            <div data-input="<?= $tariff->formName() ?>[period_type][0]"
                 class="label_js_input<? if ($tariff->period_type === Tariff::PERIOD_TYPE_INTERVAL || empty($tariff->period_type)): ?> active_input<? endif ?>">

                <?= Html::activeTextInput($tariff, 'start_interval', ['type' => 'time']) ?> -
                <?= Html::activeTextInput($tariff, 'end_interval', ['type' => 'time']) ?>
            </div>
            <div style="height: 30px;" data-input="<?= $tariff->formName() ?>[period_type][1]"
                 class="label_js_input<? if ($tariff->period_type === Tariff::PERIOD_TYPE_HOUR): ?> active_input<? endif ?> grid_3">
                <?= $form->field($tariff, 'period')->begin(); ?>
                <?= Html::activeDropDownList($tariff, 'period', $tariff->getHoursList(), [
                    'class' => 'default_select',
                ]) ?>
                <?= $form->field($tariff, 'period')->end(); ?>
            </div>
        </div>
    </section>

    <section class="row">
        <?= $form->field($tariff, 'cost')->begin(); ?>
        <div class="row_label default_label_cost">
            <?=Html::activeLabel($tariff, 'cost')?>
        </div>
        <div class="row_label cost_of_entry_per_shift" style="display:none">
            <?=Html::activeLabel($tariff, 'cost_of_entry_per_shift');?>
        </div>
        <div class="row_label cost_of_subscription" style="display:none">
            <?=Html::activeLabel($tariff, 'cost_of_subscription');?>
        </div>
        <div class="row_input">
            <?= Html::activeTextInput($tariff, 'cost', ['class' => 'mini_input_2']) ?><span
                    class="subtext"><?= $currencySymbol ?></span>
        </div>

        <?= $form->field($tariff, 'cost')->end(); ?>
    </section>

    <section class="row">
        <?= $form->field($tariff, 'tenant_company_id')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($tariff, 'tenant_company_id') ?></div>
        <div class="row_input">
            <?= Html::activeDropDownList($tariff, 'tenant_company_id', $formData['COMPANIES'], [
                'prompt' => t('tenant_company', 'Select company'),
                'class'  => 'default_select',
            ]); ?>
        </div>
        <?= $form->field($tariff, 'tenant_company_id')->end(); ?>
    </section>

<? if (!$tariff->isNewRecord): ?>
    <?= $this->render('_optionGrid', [
        'currentOption' => $currentOption,
        'otherOptions'  => $otherOptions,
        'cityId'        => $tariff->city_list,
    ]) ?>
<? endif ?>

    <section class="submit_form">
        <? if (!$tariff->isNewRecord): ?>
            <div>
                <label><?= Html::activeCheckbox($tariff, 'block', ['label' => null]) ?> <b><?= t('app', 'Block') ?></b></label>
            </div>
            <?= Html::submitInput(t('app', 'Save')) ?>
        <? else: ?>
            <?= Html::submitInput(t('employee', 'Continue')) ?>
        <? endif ?>
    </section>

<?php ActiveForm::end(); ?>