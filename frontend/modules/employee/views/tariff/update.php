<?php

use frontend\modules\employee\models\tariff\OptionTariff;
use frontend\modules\employee\models\tariff\Tariff;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $tariff Tariff */
/* @var $formData array */
/* @var $currentOption OptionTariff */
/* @var $otherOptions OptionTariff[] */

$this->title = $tariff->name;

echo \yii\widgets\Breadcrumbs::widget([
    'links'        => [
        ['label' => t('employee', 'Tariffs for employee'), 'url' => ['/employee/tariff/list']],
    ],
    'itemTemplate' => '{link}',
    'tag'          => 'div',
    'options'      => ['class' => 'bread'],
    'homeLink'     => false,
]);
?>

    <h1><?= Html::encode($this->title) ?></h1>


    <section class="main_tabs">
        <div class="tabs_links">
            <ul>
                <li><a href="#tariff" class="t01"><?= t('employee', 'Tariff') ?></a></li>
                <li><a href="#aboniments" class="t02"
                       data-href="<?= Url::to([
                           '/employee/worker-active-aboniment/get-active-aboniment-by-tariff?' . http_build_query(get())]
                       ) ?>"><?= t('employee','Purchased tariffs') ?></a></li>
            </ul>
        </div>
        <div class="tabs_content">
            <div id="t01">
                <?= $this->render(app()->user->can('driverTariff') ? '_form' : '_form_read', [
                    'tariff'        => $tariff,
                    'formData'      => $formData,
                    'currentOption' => $currentOption,
                    'otherOptions'  => $otherOptions,
                ]) ?>
            </div>
            <div id="t02"></div>
        </div>
    </section>

