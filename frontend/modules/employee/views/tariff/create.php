<?php

use frontend\modules\employee\models\tariff\Tariff;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $tariff Tariff */
/* @var $formData array */

$this->title = t('driverTariff', 'Add tariff');

echo \yii\widgets\Breadcrumbs::widget([
    'links'        => [
        ['label' => t('employee', 'Tariffs for employee'), 'url' => ['/employee/tariff/list']],
    ],
    'itemTemplate' => '{link}',
    'tag'          => 'div',
    'options'      => ['class' => 'bread'],
    'homeLink'     => false,
]);
?>

    <h1><?= Html::encode($this->title) ?></h1>

<?= $this->render('_form', [
    'tariff'   => $tariff,
    'formData' => $formData,
]) ?>