<?php

use frontend\modules\employee\models\tariff\OptionTariff;

/* @var $option OptionTariff */

if ($option->tariff_type === OptionTariff::TYPE_CURRENT) {
    echo t('driverTariff', 'All the time except exceptions');
} else {
    $activeDates = explode(';', $option->active_date);

    if (!empty($activeDates)) {
        $days      = [];
        $yearDates = [];
        $onceDates = [];

        $activeDates = array_unique($activeDates);
        array_walk($activeDates, function ($item) use (&$days, &$yearDates, &$onceDates) {
            $parts = explode('|', $item);

            if (preg_match('/\d{2}\.\d{2}\.\d{4}/', $parts[0]) === 1) {
                $onceDates[] = $parts;
            } elseif (preg_match('/\d{2}\.\d{2}/', $parts[0]) === 1) {
                $yearDates[] = $parts;
            } elseif (preg_match('/[^.]+/', $parts[0]) === 1) {
                $days[date('N', strtotime($parts[0]))] = $parts;
            }
        });

        ksort($days);
        sort($yearDates);
        sort($onceDates);

        $regularlyStr = implode(', ', array_merge(
            array_map(function ($item) {
                return mb_strtolower(
                        app()->formatter->asDate(DateTimeImmutable::createFromFormat('l', $item[0]), 'cccc'))
                    . (empty($item[1]) ? '' : ' ' . $item[1]);
            }, $days),
            array_map(function ($item) {
                return mb_strtolower(app()->formatter->asDate(
                        DateTimeImmutable::createFromFormat('d.m', $item[0]), 'd LLL'))
                    . (empty($item[1]) ? '' : ' ' . $item[1]);
            }, $yearDates)));

        $onceStr = implode(', ', array_map(function ($item) {
            return mb_strtolower(app()->formatter->asDate(
                    DateTimeImmutable::createFromFormat('d.m.Y', $item[0]), 'd LLL yyyy'))
                . (empty($item[1]) ? '' : ' ' . $item[1]);
        }, $onceDates));

        $lines = [];
        if (!empty($regularlyStr)) {
            $lines[] = t('driverTariff', 'Regularly') . ": $regularlyStr";
        }
        if (!empty($onceStr)) {
            $lines[] = t('taxi_tariff', 'Once') . ": $onceStr";
        }

        echo implode('<br>', $lines);
    }
}