<?php

use frontend\modules\employee\models\tariff\OptionTariff;

/* @var $this yii\web\View */
/* @var $option OptionTariff */
/* @var $commissionTypes array */
/* @var $currencySymbol string */

?>

<tr>
    <td>
        <?= $this->render('_workingHours', compact('option')) ?>
    </td>
    <td>
        <?= $this->render('_commission', compact('option','commissionTypes')) ?>
    </td>
    <td>
        <?= $this->render('_surcharges', compact('option','commissionTypes', 'currencySymbol')) ?>
    </td>
    <td style="text-align: right">
        <?= $this->render('_buttons', compact('option')) ?>
    </td>
</tr>