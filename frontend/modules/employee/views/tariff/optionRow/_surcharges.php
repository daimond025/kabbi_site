<?php

use frontend\modules\employee\models\tariff\OptionTariff;

/* @var $option OptionTariff */
/* @var $commissionTypes array */
/* @var $currencySymbol string */

$lines = [];
if (!empty($option->increase_order_sum)) {
    $increaseOrderSum = (float)$option->increase_order_sum;
    $lines[]          = t('driverTariff', 'Increase the value of the order to')
        . " {$increaseOrderSum} {$currencySymbol}";
}

if (!empty($option->increase_sum_fix)) {
    $increaseSumFix     = (float)$option->increase_sum_fix;
    $increaseSumFixType = empty($option->increase_sum_fix_type) ? '' : $commissionTypes[$option->increase_sum_fix_type];
    $message            = t('driverTariff', 'Fixed surcharge')
        . " {$increaseSumFix} {$increaseSumFixType}";

    if (!empty($option->increase_sum_limit)) {
        $increaseSumLimit = (float)$option->increase_sum_limit;
        $message          .= ' ' . mb_strtolower(t('driverTariff', 'For orders of less than'))
            . "{$increaseSumLimit} {$currencySymbol}";
    }

    $lines[] = $message;
}

echo empty($lines) ? t('app', 'No') : implode('<br>', $lines);

