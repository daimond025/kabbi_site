<?php

use frontend\modules\employee\models\tariff\OptionTariff;
use yii\helpers\Url;

/* @var $option OptionTariff */

?>

<a class="ot_edit js-edit-ex" href="<?= Url::to([
    'update-option',
    'tariffId' => $option->tariff_id,
    'optionId' => $option->option_id,
]) ?>">
</a>