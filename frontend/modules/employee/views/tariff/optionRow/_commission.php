<?php

use frontend\modules\employee\models\tariff\OptionTariff;
use frontend\modules\employee\models\tariff\TariffCommission;

/* @var $option OptionTariff */
/* @var $commissionTypes array */

list($isFixedCommission, $fixedCommission, $intervalCommissions) = $option->getCommissionData();

$commissionClasses = TariffCommission::getCommissionClasses();

if ($isFixedCommission) {
    $lines          = [];
    $commission     = empty($fixedCommission['value']) ? 0 : (float)$fixedCommission['value'];
    $commissionType = empty($fixedCommission['type']) ? '' : $commissionTypes[$fixedCommission['type']];
    $lines[]        = "{$commission} {$commissionType}";

    if (!empty($fixedCommission['tax'])) {
        $tax     = (float)$fixedCommission['tax'];
        $lines[] = t('commission', 'Tax when paying with corp. balance')
            . " {$tax} {$commissionTypes[TariffCommission::TYPE_PERCENT]}";
    }
    echo implode('<br>', $lines);
} else {
    echo $commissionClasses[TariffCommission::CLASS_EXTENDED];
}