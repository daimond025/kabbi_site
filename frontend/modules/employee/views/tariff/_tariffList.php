<?php

use yii\helpers\Html;

$bundle = \app\modules\driver\assets\DriverAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/function.js');
$this->registerJs("cityFilterOnClick();");

if(!empty($tariffs)):?>
    <?=$this->render('_filter', ['city_list' => $city_list])?>
    <table class="people_table sort">
        <tr>
            <th class="pt_wor pt_fio"><?=t('driverTariff', 'Name')?></th>
            <th class="pt_pos"><?=t('driverTariff', 'Type')?></th>
            <th class="pt_pos"><?=t('driverTariff', 'Car class')?></th>
            <th class="pt_re"><?=t('driverTariff', 'City')?></th>
        </tr>
        <?foreach ($tariffs as $tariff):?>
            <tr>
                <td class="pt_fio pt_wor">
                    <a href="<?=\yii\helpers\Url::to(['update', 'id' => $tariff->tariff_id])?>"><?= Html::encode($tariff->name); ?></a>
                </td>

                <td class="pt_pos"><?= Html::encode($tariff->getTypeByKey($tariff->type)); ?></td>
                <td><?= Html::encode(t('car', $tariff->class->class)); ?></td>
                <td class="pt_re"><?= Html::encode(implode(', ', \yii\helpers\ArrayHelper::getColumn($tariff->cities, 'name' . getLanguagePrefix()))); ?></td>
            </tr>
        <?endforeach;?>
    </table>
<?php else: ?>
    <p><?=t('app', 'Empty')?></p>
<?php endif; ?>