<?php

use frontend\modules\employee\models\tariff\OptionTariff;
use frontend\modules\employee\models\tariff\TariffCommission;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $currentOption OptionTariff */
/* @var $otherOptions OptionTariff[] */
/* @var $cityId int */
/* @var $disabled bool */

$commissionTypes = TariffCommission::getCommissionTypes($cityId);
$currencySymbol  = Html::encode(getCurrencySymbol($cityId));

?>

<section class="row">
    <h4 style="margin-top: 30px;"><?= t('driverTariff', 'Main working time') ?></h4>
    <table class="styled_table" style="margin-bottom: 30px;">
        <tr>
            <th style="width: 22%"><?= t('driverTariff', 'Working hours') ?></th>
            <th style="width: 22%"><?= t('driverTariff', 'Commission') ?></th>
            <th style="width: 46%"><?= t('driverTariff', 'Surcharges') ?></th>
            <th style="10%"></th>
        </tr>
        <? if (isset($currentOption)): ?>
            <?= $this->render('optionRow/_mainOptionRow', [
                'option'          => $currentOption,
                'commissionTypes' => $commissionTypes,
                'currencySymbol'  => $currencySymbol,
            ]) ?>
        <? endif ?>
    </table>
</section>

<? if (!empty($otherOptions)): ?>
    <section class="row">
        <h4 style="margin-top: 30px;"><?= t('driverTariff', 'Exceptions') ?></h4>
        <table class="styled_table" style="margin-bottom: 30px;">
            <tr>
                <th style="width: 22%"><?= t('driverTariff', 'Working hours') ?></th>
                <th style="width: 22%"><?= t('driverTariff', 'Commission') ?></th>
                <th style="width: 28%"><?= t('driverTariff', 'Surcharges') ?></th>
                <th style="width: 18%"><?= t('driverTariff', 'Sort') ?></th>
                <th style="10%"></th>
            </tr>
            <? foreach ($otherOptions as $option): ?>
                <?= $this->render('optionRow/_otherOptionRow', [
                    'option'          => $option,
                    'commissionTypes' => $commissionTypes,
                    'currencySymbol'  => $currencySymbol,
                ]) ?>
            <? endforeach; ?>
        </table>
    </section>
<? endif ?>

<? if (!$disabled): ?>
    <a style="margin-bottom: 30px;" class="button js-edit-ex"
       href="<?= Url::to(['update-option', 'tariffId' => $currentOption->tariff_id]) ?>">
        <?= t('driverTariff', 'Add an exception') ?>
    </a>
<? endif ?>
<a class="js-modal-ex-hidden" style="display: none;"></a>