<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;

?>
<form id="driver_tariff_search" name="driver_tariff_search" method="post" onsubmit="return false" action="<?=Url::to('/driver/tariff/search/')?>">
    <div class="grid_3" data-activity="active">
        <div class="grid_item">
            <div class="select_checkbox">
                <a data-filter="no" class="a_sc select" rel="<?=t('user', 'All cities')?>"><?=t('user', 'All cities')?></a>
                <div class="b_sc">
                    <ul>
                        <?foreach ($city_list as $city_id => $city):?>
                            <li><label><input name="city_id[]" type="checkbox" value="<?=$city_id?>"/> <?= Html::encode($city); ?></label></li>
                        <?endforeach;?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</form>
