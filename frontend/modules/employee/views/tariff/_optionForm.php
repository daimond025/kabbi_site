<?php

use yii\helpers\Html;
use frontend\modules\employee\models\tariff\TariffCommission;
use frontend\modules\employee\models\tariff\OptionTariff;
use frontend\widgets\validity_input\ValidityInput;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $option OptionTariff */
/* @var $add_options array */
/* @var $active_add_options array */
/* @var $cityId int */
/* @var $hasCar bool */

$arOptionDiscount = $option->getDiscountOptionList();
$commissionTypes  = TariffCommission::getCommissionTypes($cityId);
$currencySymbol   = Html::encode(getCurrencySymbol($cityId));

?>

<? $form = ActiveForm::begin([
    'id'                     => 'driver-tariff-form',
    'errorCssClass'          => 'input_error',
    'successCssClass'        => 'input_success',
    'enableClientValidation' => true,
    'options'                => [
        'data-tariff-id'   => $option->tariff_id,
        'data-tariff-type' => $option->tariff_type,
        'data-option-id'   => $option->option_id,
    ],
]); ?>

    <div class="worker-tariff-exception">

        <? if ($option->tariff_type !== OptionTariff::TYPE_CURRENT): ?>
            <h2><?= $option->isNewRecord ? t('driverTariff', 'Add exception') : t('driverTariff',
                    'Update exception') ?></h2>
            <?= $form->field($option, 'active_date', ['options' => ['class' => 'js-active-date']])->begin(); ?>
            <?= ValidityInput::widget([
            'id'                      => strtolower($option->tariff_type) . '_date',
            'label'                   => false,
            'data'                    => explode(';', $option->active_date),
            'input_name'              => 'option',
            'show_upload_celebration' => false,
            'show_time_interval'      => true,
            'model'                   => $option,
            'attr_name'               => 'active_date',
        ]) ?>
            <?= $form->field($option, 'active_date')->end(); ?>

            <h2><?= Html::activeLabel($option, 'sort') ?></h2>
            <?= $form->field($option, 'sort')->begin(); ?>
            <?= Html::activeTextInput($option, 'sort', ['dir' => 'auto']) ?>
            <?= $form->field($option, 'sort')->end(); ?>
        <? else: ?>
            <h2><?= t('driverTariff', 'Main working time') ?></h2>
        <? endif ?>

        <h2><?= t('driverTariff', 'Surcharges') ?></h2>
        <?= $this->render('surcharge/_surcharge', [
            'form'           => $form,
            'option'         => $option,
            'currencySymbol' => $currencySymbol,
            'types'          => $commissionTypes,
        ]) ?>

        <h2><?= t('driverTariff', 'Commission') ?></h2>
        <?= $this->render('commission/_commission', [
            'form'            => $form,
            'option'          => $option,
            'currencySymbol'  => $currencySymbol,
            'commissionTypes' => $commissionTypes,
        ]) ?>

        <?
        $has_active_options = !empty(array_intersect(array_keys($add_options), $active_add_options));
        ?>
        <section class="additional_options"
                 <? if (!$hasCar || !$has_active_options): ?>style="display: none"<? endif; ?>>
            <h2><?= t('driverTariff', 'Discount for additional options') ?></h2>
            <section class="row">
                <div class="row_label"></div>
                <div class="row_input">
                    <b class="tarif_column"><?= Html::encode(t('driverTariff', 'Tariff discount')); ?></b>
                    <b class="tarif_column"><?= Html::encode(t('driverTariff', 'Decrease commission')); ?></b>
                </div>
            </section>
            <? foreach ($add_options as $add_option_id => $add_option_name): ?>
                <section class="row check_trigger
                        <?= !in_array($add_option_id, $active_add_options, false) ? 'hide' : ''; ?>"
                         data-option="<?= $add_option_id ?>">
                    <?
                    $isset               = isset($arOptionDiscount[$add_option_id]);
                    $discount_line       = $isset ? $arOptionDiscount[$add_option_id]['discount_line'] : '';
                    $discount_line_type  = $isset ? $arOptionDiscount[$add_option_id]['discount_line_type'] : '';
                    $discount_order      = $isset ? $arOptionDiscount[$add_option_id]['discount_order'] : '';
                    $discount_order_type = $isset ? $arOptionDiscount[$add_option_id]['discount_order_type'] : '';
                    ?>
                    <div class="row_label">
                        <label>
                            <input <? if ($isset): ?>checked="checked"<? endif ?>
                                   name="addOptions[<?= $add_option_id ?>][id]"
                                   type="checkbox"
                                   value="<?= $add_option_id ?>"> <?= Html::encode($add_option_name); ?></label>
                    </div>
                    <div class="row_input">
                       <span class="tarif_column">
                           <input name="addOptions[<?= $add_option_id ?>][discount_line]"
                                  <? if (!$isset): ?>disabled=""<? endif ?> class="mini_input_2" type="text"
                                  value="<?= $discount_line ?>">
                          <span class="subtext">
                             <select
                                     name="addOptions[<?= $add_option_id ?>][discount_line_type]"
                                     <? if (!$isset): ?>disabled=""<? endif ?> data-placeholder="" multiple=""
                                     class="default_select">
                                 <?
                                 $index = 0;
                                 foreach ($commissionTypes as $type => $value):
                                     $selected = (!$index && !$isset) || ($isset && $discount_line_type == $type) ? 'selected=selected' : '';
                                     $index++;
                                     ?>
                                     <option <?= $selected ?> value="<?= $type ?>"><?= Html::encode($value); ?></option>
                                 <? endforeach; ?>
                             </select>
                          </span>
                       </span>
                        <span class="tarif_column">
                          <input name="addOptions[<?= $add_option_id ?>][discount_order]"
                                 <? if (!$isset): ?>disabled=""<? endif ?> class="mini_input_2" type="text"
                                 value="<?= $discount_order ?>">
                          <span class="subtext">
                             <select
                                     name="addOptions[<?= $add_option_id ?>][discount_order_type]"
                                     <? if (!$isset): ?>disabled=""<? endif ?> data-placeholder="" multiple=""
                                     class="default_select">
                                 <?
                                 $index = 0;
                                 foreach ($commissionTypes as $type => $value):
                                     $selected = (!$index && !$isset) || ($isset && $discount_order_type == $type) ? 'selected=selected' : '';
                                     $index++;
                                     ?>
                                     <option <?= $selected ?> value="<?= $type ?>"><?= Html::encode($value); ?></option>
                                 <? endforeach; ?>
                             </select>
                          </span>
                       </span>
                    </div>
                </section>
            <? endforeach; ?>
        </section>

        <section class="submit_form">
            <? if (!$option->isNewRecord && $option->tariff_type !== OptionTariff::TYPE_CURRENT): ?>
                <b><?= Html::a(t('app', 'Delete'), '#', ['class' => 'link_red js-option-delete']) ?></b>
            <? endif ?>
            <?= Html::submitInput(t('app', 'Save'), ['class' => 'js-submit-option']) ?>
        </section>
    </div>

<?php ActiveForm::end(); ?>