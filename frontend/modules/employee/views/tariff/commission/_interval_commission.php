<?php

use frontend\modules\employee\models\tariff\TariffCommission;

/* @var $active boolean */
/* @var $commissions array */
/* @var $commissionTypes array */
/* @var $currencySymbol string */

$tax           = null;
$isSetupTax    = false;
$classFixed    = TariffCommission::CLASS_UNITED;
$classInterval = TariffCommission::CLASS_EXTENDED;

?>

<div class="interval-section js-interval <?= $active ? 'active_rtt' : '' ?>" style="display: none;">

    <? foreach ($commissions as $kind => $data): ?>
        <div class="interval-block">
            <section class="row">
                <div class="row_label">
                    <label style="line-height: 30px;"><?= t('commission', $kind) ?></label>
                </div>

                <div class="row_input">
                    <div class="worker-tariff-interval js-checkbox-trigger-parent">
                        <?
                        list($isIntervalCommission, $fixedCommission, $intervalCommissions) = $data;
                        $commission = !$isIntervalCommission ? $fixedCommission : current($intervalCommissions);
                        $tax        = empty($commission['tax']) ? null : $commission['tax'];
                        $isSetupTax = $tax !== null;
                        ?>
                        <div class="col col__one-item js-checkbox-trigger-not-checked"
                             style="<?= $isIntervalCommission ? 'display: none' : '' ?>">
                            <input type="text" class="input_number_mini"
                                   name="<?= "commissions[kind][{$kind}][{$classFixed}][value]" ?>"
                                   value="<?= $fixedCommission->value ?>">
                            <div class="interval-type">
                                <?= $this->render('_commission_type', [
                                    'name'            => "commissions[kind][{$kind}][{$classFixed}][type]",
                                    'selectedType'    => $fixedCommission->type,
                                    'commissionTypes' => $commissionTypes,
                                ]) ?>
                            </div>
                        </div>

                        <div class="col col__many-items js-checkbox-trigger-checked"
                             style="<?= $isIntervalCommission ? '' : 'display: none' ?>">
                            <?
                            for ($i = 0, $l = count($intervalCommissions); $i < $l; $i++):
                                $commission = $intervalCommissions[$i];
                                ?>
                                <div class="item <?= $i === $l - 1 ? 'last-item' : '' ?>">
                                    <span><?= t('tariff-commission', 'more') ?></span>
                                    <input type="text" class="input_number_mini js-from" disabled
                                           name="<?= "commissions[kind][{$kind}][{$classInterval}][$i][from]" ?>"
                                           value="<?= $commission->from ?>">
                                    <span class="last-hidden"><?= t('tariff-commission', 'less') ?></span>
                                    <input type="text" class="input_number_mini js-to last-hidden"
                                           name="<?= "commissions[kind][{$kind}][{$classInterval}][$i][to]" ?>"
                                           value="<?= $commission->to ?>">
                                    <span class="last-hidden"><?= $currencySymbol ?></span>
                                    <span><i class="line"></i></span>
                                    <input type="text" class="input_number_mini"
                                           name="<?= "commissions[kind][{$kind}][{$classInterval}][$i][value]" ?>"
                                           value="<?= $commission->value ?>">
                                    <div class="interval-type">
                                        <?= $this->render('_commission_type', [
                                            'name'            => "commissions[kind][{$kind}][{$classInterval}][$i][type]",
                                            'selectedType'    => $commission->type,
                                            'commissionTypes' => $commissionTypes,
                                        ]) ?>
                                    </div>
                                </div>
                            <? endfor ?>
                        </div>

                        <div class="interval-trigger">
                            <label>
                                <input type="checkbox" class="js-checkbox-trigger" value="1"
                                       name="<?= "commissions[kind][{$kind}][is_interval_commission]" ?>"
                                    <?= $isIntervalCommission ? 'checked' : '' ?>>
                                <?= t('tariff-commission', 'Interval') ?>
                            </label>
                        </div>
                    </div>
                </div>
            </section>

            <section class="row">
                <div class="row_label"></div>
                <div class="row_input">
                    <div class="fixed-commission-nds js-checkbox-trigger-parent">
                        <label>
                            <input type="checkbox" class="js-checkbox-trigger" value="1"
                                   name="<?= "commissions[kind][{$kind}][is_setup_tax]" ?>"
                                <?= $isSetupTax ? 'checked' : '' ?>>
                            <?= t('commission', 'Add tax') ?>
                        </label>

                        <div class="js-checkbox-trigger-checked"
                             style="<?= $isSetupTax ? '' : 'display: none;' ?>">
                            <input type="text" class="input_number_mini"
                                   name="<?= "commissions[kind][{$kind}][tax]" ?>"
                                   value="<?= $tax ?>">
                            <span>%</span>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    <? endforeach ?>

</div>