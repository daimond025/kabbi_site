<?php

use frontend\modules\employee\models\tariff\OptionTariff;
use frontend\modules\employee\models\tariff\TariffCommission;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $form ActiveForm */
/* @var $option OptionTariff */
/* @var $currencySymbol string */
/* @var $commissionTypes array */

list($isFixedCommission, $fixedCommission, $intervalCommissions) = $option->getCommissionData();
$commissionClass = $isFixedCommission
    ? TariffCommission::CLASS_UNITED : TariffCommission::CLASS_EXTENDED;

?>

<div class="js-commission">
    <section class="row">
        <div class="row_label">
            <label><?= Html::activeLabel($option, 'commission') ?></label>
        </div>
        <div class="row_input">
            <div class="js-interval-commission radio_trigger"
                 data-united-type="<?= TariffCommission::CLASS_UNITED ?>">
                <?= Html::radioList("commissions[commission_class]",
                    $commissionClass, TariffCommission::getCommissionClasses()) ?>
            </div>
        </div>
    </section>

    <?= $this->render('_fixed_commission', [
        'active'          => $isFixedCommission,
        'commission'      => $fixedCommission,
        'commissionTypes' => $commissionTypes,
    ]) ?>

    <?= $this->render('_interval_commission', [
        'active'          => !$isFixedCommission,
        'commissions'     => $intervalCommissions,
        'commissionTypes' => $commissionTypes,
        'currencySymbol'  => $currencySymbol,
    ]) ?>
</div>