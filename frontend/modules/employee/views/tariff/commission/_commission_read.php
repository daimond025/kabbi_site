<?php

use frontend\modules\employee\models\tariff\OptionTariff;
use frontend\modules\employee\models\tariff\TariffCommission;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $form ActiveForm */
/* @var $option OptionTariff */
/* @var $currencySymbol string */
/* @var $commissionTypes array */

list($isFixedCommission, $fixedCommission, $intervalCommissions) = $option->getCommissionData();
$commissionClass = $isFixedCommission
    ? TariffCommission::CLASS_UNITED : TariffCommission::CLASS_EXTENDED;
?>

<div class="js-commission">
    <section class="row">
        <div class="row_label">
            <label><?= Html::activeLabel($option, 'commission') ?></label>
        </div>
        <div class="row_input">
            <div class="js-interval-commission radio_trigger">
                <?= Html::radioList("commissions[commission_class]",
                    $commissionClass, TariffCommission::getCommissionClasses(), [
                        'item' => function ($index, $label, $name, $checked, $value) {
                            return Html::tag('label',
                                Html::radio($name, $checked, ['value' => $value, 'disabled' => true]) . ' ' . $label);
                        },
                    ]) ?>
            </div>
        </div>
    </section>

    <?= $this->render('_fixed_commission_read', [
        'active'          => $isFixedCommission,
        'commission'      => $fixedCommission,
        'commissionTypes' => $commissionTypes,
    ]) ?>

    <?= $this->render('_interval_commission_read', [
        'active'          => !$isFixedCommission,
        'commissions'     => $intervalCommissions,
        'commissionTypes' => $commissionTypes,
        'currencySymbol'  => $currencySymbol,
    ]) ?>
</div>