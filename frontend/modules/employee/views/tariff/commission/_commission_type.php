<?php

use yii\helpers\Html;

/* @var $name string */
/* @var $selectedType string */
/* @var $commissionTypes array */

?>

<select name="<?= $name ?>" class=" default_select">
    <? foreach ($commissionTypes as $value => $label) {
        echo Html::tag('option', Html::encode($label), [
            'value'    => $value,
            'selected' => $value == $selectedType,
        ]);
    } ?>
</select>