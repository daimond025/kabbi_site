<?php

/* @var $active boolean */
use frontend\modules\employee\models\tariff\TariffCommission;

/* @var $active boolean */
/* @var $commission TariffCommission */
/* @var $commissionTypes array */

$kind = TariffCommission::KIND_FIXED;

?>

<div class="js-fixed <?= $active ? 'active_rtt' : '' ?>" style="display: none">
    <section class="row">
        <div class="row_label">
            <label></label>
        </div>
        <div class="row_input">
            <?= $commission->value ?>
            <?= empty($commissionTypes[$commission->type])
                ? '' : $commissionTypes[$commission->type] ?>
        </div>
    </section>

    <section class="row">
        <div class="row_label"></div>
        <div class="row_input">
            <div class="fixed-commission-nds js-checkbox-trigger-parent">
                <label>
                    <?= t('commission', 'Add tax when paying with corp. balance') ?>
                </label>
                <div class="js-checkbox-trigger-checked"
                     style="<?= empty($commission->tax) ? 'display: none;' : '' ?>">
                    <span><?= $commission->tax ?> %</span>
                </div>
            </div>
        </div>
    </section>
</div>