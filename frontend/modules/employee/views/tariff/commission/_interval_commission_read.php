<?php

use frontend\modules\employee\models\tariff\TariffCommission;

/* @var $active boolean */
/* @var $commissions array */
/* @var $commissionTypes array */
/* @var $currencySymbol string */

$tax           = null;
$isSetupTax    = false;
$classFixed    = TariffCommission::CLASS_UNITED;
$classInterval = TariffCommission::CLASS_EXTENDED;

?>

<div class="interval-section js-interval <?= $active ? 'active_rtt' : '' ?>" style="display: none;">

    <? foreach ($commissions as $kind => $data): ?>
        <div class="interval-block">
            <section class="row">
                <div class="row_label">
                    <label style="line-height: 30px;"><?= t('commission', $kind) ?></label>
                </div>

                <div class="row_input">
                    <div class="worker-tariff-interval js-checkbox-trigger-parent">
                        <?
                        list($isIntervalCommission, $fixedCommission, $intervalCommissions) = $data;
                        $commission = !$isIntervalCommission ? $fixedCommission : current($intervalCommissions);
                        $tax        = empty($commission['tax']) ? null : $commission['tax'];
                        $isSetupTax = $tax !== null;
                        ?>
                        <div class="col col__one-item js-checkbox-trigger-not-checked"
                             style="<?= $isIntervalCommission ? 'display: none' : '' ?>">
                            <?= $fixedCommission->value ?>
                            <?= empty($commissionTypes[$fixedCommission->type]) ? '' : $commissionTypes[$fixedCommission->type] ?>
                        </div>

                        <div class="col col__many-items js-checkbox-trigger-checked"
                             style="<?= $isIntervalCommission ? '' : 'display: none' ?>">
                            <?
                            for ($i = 0, $l = count($intervalCommissions); $i < $l; $i++):
                                $commission = $intervalCommissions[$i];
                                ?>
                                <div class="item <?= $i === $l - 1 ? 'last-item' : '' ?>">
                                    <span>
                                        <?= t('tariff-commission', 'more') ?>
                                        <?= $commission->from ?>
                                        <?= $currencySymbol ?>
                                    </span>
                                    <span class="last-hidden">
                                        <?= t('tariff-commission', 'less') ?>
                                        <?= $commission->to ?>
                                    </span>
                                    <span class="last-hidden"><?= $currencySymbol ?></span>
                                    <span>
                                        <i class="line"></i>
                                        <?= $commission->value ?>
                                        <?= empty($commissionTypes[$fixedCommission->type]) ? '' : $commissionTypes[$fixedCommission->type] ?>
                                    </span>
                                </div>
                            <? endfor ?>
                        </div>
                    </div>
                </div>
            </section>

            <section class="row">
                <div class="row_label"></div>
                <div class="row_input">
                    <div class="fixed-commission-nds js-checkbox-trigger-parent">
                        <label><?= t('commission', 'Add tax') ?></label>

                        <div class="js-checkbox-trigger-checked"
                             style="<?= $isSetupTax ? '' : 'display: none;' ?>">
                            <span><?= $tax ?> %</span>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    <? endforeach ?>

</div>