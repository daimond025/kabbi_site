<?php

/* @var $active boolean */
use frontend\modules\employee\models\tariff\TariffCommission;

/* @var $active boolean */
/* @var $commission TariffCommission */
/* @var $commissionTypes array */

$kind = TariffCommission::KIND_FIXED;

?>

<div class="js-fixed <?= $active ? 'active_rtt' : '' ?>" style="display: none">
    <section class="row">
        <div class="row_label">
            <label></label>
        </div>
        <div class="row_input">
            <input type="text" class="input_number_mini" style="float: left; margin-right: 10px; width: 15%!important;"
                   name="<?= "commissions[kind][{$kind}][value]" ?>"
                   value="<?= $commission->value ?>">
            <div style="float: left; line-height: 30px;">
                <?= $this->render('_commission_type', [
                    'name'            => "commissions[kind][{$kind}][type]",
                    'selectedType'    => $commission->type,
                    'commissionTypes' => $commissionTypes,
                ]) ?>
            </div>
        </div>
    </section>

    <section class="row">
        <div class="row_label"></div>
        <div class="row_input">
            <div class="fixed-commission-nds js-checkbox-trigger-parent">
                <label><input type="checkbox" class="js-checkbox-trigger" value="1"
                              name="<?= "commissions[kind][{$kind}][is_setup_tax]" ?>"
                        <?= empty($commission->tax) ? '' : 'checked' ?>>
                    <?= t('commission', 'Add tax when paying with corp. balance') ?></label>
                <div class="js-checkbox-trigger-checked"
                     style="<?= empty($commission['tax']) ? 'display: none;' : '' ?>">
                    <input type="text" class="input_number_mini"
                           name="<?= "commissions[kind][{$kind}][tax]" ?>"
                           value="<?= $commission->tax ?>">
                    <span>%</span>
                </div>
            </div>
        </div>
    </section>
</div>