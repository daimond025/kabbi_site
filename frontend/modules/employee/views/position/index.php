<?php

/* @var bool $has_car Does the worker position have car? */
/* @var bool $active Position activity */
/* @var array $documentMap */
/* @var array $fieldMap */
/* @var array $cars */
/* @var array $group_data */
/* @var integer $workerHasPositionId */
/* @var integer $worker_id */
/* @var integer $groupId */
/* @var $position common\modules\employee\models\position\Position */
/* @var array $fieldModels */
/* @var array $positionClassMap */
/* @var array $positionGroupMap */
/* @var array $workerDocumentMap */
/* @var array $scanDocumentMap */
/* @var string $actionUrl */
/* @var array $moduleMap */

/* @var array $showFieldList */

use frontend\modules\employee\widgets\car\WorkerCars;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

$spoler_id = rand_str(10);
?>
<h2 class="check_spoler">
    <label><input data-option="airport" data-worker="<?= $worker_id ?>" data-position="<?= $position->position_id ?>"
                  type="checkbox" <? if ($active): ?>checked<? endif; ?>></label>
    <a data-spolerid="<?= $spoler_id ?>"
       class="spoler <? if (!$active): ?>locked<? endif; ?>"><?= Html::encode(getValue($moduleMap[$position->module_id])) ?></a>
</h2>
<section class="spoler_content" id="<?= $spoler_id ?>" style="display: none;">
    <? if ($position->has_car): ?>
        <?= WorkerCars::widget([
            'cars'                    => getValue($cars, []),
            'searchAction'            => Url::to(['/employee/car/car-search']),
            'entity_id'               => $workerHasPositionId,
            'groupMapGroupedByClass'  => getValue($group_data['groupMapGroupedByClass'], []),
            'tariffMapGroupedByClass' => getValue($group_data['tariffMapGroupedByClass'], []),
            'showingClassesMap'       => getValue($group_data['showingClassesMap'], []),
        ]); ?>
    <? else: ?>
        <table class="people_table group">
            <thead>
            <tr>
                <? if (!empty($positionClassMap)): ?>
                    <th><?= t('employee', 'Position') ?></th>
                <? endif ?>
                <th><?= t('employee', 'Worker group') ?></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <? if (!empty($positionClassMap)): ?>
                    <td>
                        <div style="width: 300px;"><?= Html::dropDownList('positionClassId', $position->position_id,
                                $positionClassMap,
                                ['class' => 'default_select']) ?></div>
                    </td>
                <? endif ?>
                <td>
                    <div style="width: 300px;"><?= Html::dropDownList('positionGroupMap', $groupId, $positionGroupMap,
                            ['class' => 'default_select', 'prompt' => t('employee', 'Choose a group')]) ?></div>
                </td>
            </tr>
            </tbody>
        </table>
    <? endif ?>
        <?= $this->render('_formData', [
            'documentMap'         => $documentMap,
            'fieldMap'            => $fieldMap,
            'position_id'         => $position->position_id,
            'workerHasPositionId' => $workerHasPositionId,
            'worker_id'           => $worker_id,
            'fieldModels'         => $fieldModels,
            'actionUrl'           => $actionUrl,
            'workerDocumentMap'   => $workerDocumentMap,
            'scanDocumentMap'     => $scanDocumentMap,
            'isSaved'             => false,
            'pjaxId'              => $pjaxId,
        ]) ?>
</section>


