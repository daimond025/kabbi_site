<?php
/* @var $this yii\web\View */
/* @var array $moduleMap */
/* @var integer $worker_id */

use yii\helpers\Html;

?>
<div style="display: none">
    <div id="add_prof">
        <h2><?= t('employee', 'Choose the position') ?></h2>
        <?= Html::dropDownList('position_list', null, $moduleMap, [
            'prompt' => t('employee', 'Profession'),
            'class'  => 'default_select',
        ]) ?>
        <div class="help-block help-block-error" style="color:red; margin-bottom: 10px"></div>
        <div style="text-align: center">
            <button data-worker="<?= $worker_id ?>"><?= t('app', 'Add') ?></button>
        </div>
    </div>
</div>
