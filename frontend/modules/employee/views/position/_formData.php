<?php

/** @var $this yii\web\View */
/** @var array $documentMap */
/** @var array $fieldMap */
/** @var array $fieldModels */
/** @var integer $position_id */
/** @var bool $isSaved */
/** @var integer $workerHasPositionId */
/** @var integer $worker_id */
/** @var array $fieldModels */
/** @var string $actionUrl */
/* @var array $workerDocumentMap */
/* @var array $scanDocumentMap */

use frontend\modules\employee\models\documents\WorkerDocumentScan;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\modules\employee\widgets\document\Documents;
use frontend\modules\employee\widgets\fields\DynamicTypeFields;
use yii\widgets\Pjax;

Pjax::begin([
    'id'              => $pjaxId,
    'enablePushState' => false,
]);
if (!empty($documentMap) || !empty($fieldMap)):

$form = ActiveForm::begin([
    'action'          => [
        $actionUrl,
        'position_id' => $position_id,
        'worker_id'   => $worker_id,
        'pjax_id'     => $pjaxId,
    ],
    'errorCssClass'   => 'input_error',
    'successCssClass' => 'input_success',
    'options'         => [
        'id'        => 'position_form_' . rand(),
        'data-pjax' => true,
    ],
]) ?>

<?= Documents::widget([
    'form'                 => $form,
    'documentMap'          => $documentMap,
    'ajaxUpload'           => true,
    'entityHasDocumentMap' => $workerDocumentMap,
    'scanDocumentMap'      => $scanDocumentMap,
    'scanModelConfig'      => [
        'class' => WorkerDocumentScan::className(),
    ],
    'entityId'             => $worker_id,
]); ?>

<?= DynamicTypeFields::widget([
    'label'         => t('employee', 'About employee'),
    'form'          => $form,
    'fieldMap'      => $fieldMap,
    'hasPositionId' => $workerHasPositionId,
    'models'        => $fieldModels,
]); ?>

<section class="submit_form">
    <? if ($isSaved): ?>
        <div style="color: green;"><?= t('app', 'The data have saved successfully') ?></div>
    <? endif ?>
    <?= Html::submitInput(t('app', 'Save')) ?>
</section>

<? ActiveForm::end(); ?>

<? endif ?>

<? Pjax::end() ?>
