<?php

return [
    'controllerNamespace' => 'frontend\modules\employee\controllers',
    'components' => [
        'position'          => [
            'class' => 'frontend\modules\employee\components\position\PositionService',
        ],
        'workerCar'         => [
            'class'      => 'common\modules\employee\components\worker\WorkerHasCarLayer',
            'modelClass' => 'common\modules\employee\models\worker\WorkerHasCar',
        ],
        'workerHasPosition' => [
            'class'      => 'common\modules\employee\components\worker\WorkerHasPositionLayer',
            'modelClass' => 'frontend\modules\employee\models\worker\WorkerHasPosition',
        ],
        'groups'            => [
            'class'            => 'frontend\modules\employee\components\groups\GroupsLayer',
            'modelClass'       => 'frontend\modules\employee\models\groups\WorkerGroup',
            'repositoryConfig' => [
                'class' => 'frontend\modules\employee\components\groups\GroupRepository',
            ],
        ],
        'tariff'            => [
            'class'            => 'frontend\modules\employee\components\tariff\TariffService',
            'modelClass'       => 'frontend\modules\employee\models\tariff\Tariff',
            'repositoryConfig' => [
                'class'      => 'frontend\modules\employee\components\tariff\TariffRepository',
                'modelClass' => 'frontend\modules\employee\models\tariff\Tariff',
            ],
        ],
        'clientTariff'      => [
            'class' => 'frontend\modules\employee\components\tariff\TaxiTariffService',
        ],
        'workerHasDocument' => [
            'class'       => '\frontend\modules\employee\components\worker\WorkerHasDocumentService',
            'modelConfig' => [
                'class' => '\frontend\modules\employee\models\worker\WorkerHasDocument',
            ],
        ],
        'reviews'           => [
            'class' => '\frontend\modules\employee\components\review\ReviewService',
        ],
        'worker'            => [
            'class' => '\frontend\modules\employee\components\worker\WorkerService',
        ],
        'workerShift'       => [
            'class'       => '\frontend\modules\employee\components\worker\WorkerShiftService',
            'modelConfig' => [
                'class' => '\frontend\modules\employee\models\worker\WorkerShift',
            ],
        ],
    ],
    'params'     => [
        'intervalCommission.count' => 3,
    ],
];