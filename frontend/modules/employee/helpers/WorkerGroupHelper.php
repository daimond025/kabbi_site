<?php

namespace frontend\modules\employee\helpers;

use \frontend\modules\employee\models\groups\WorkerGroup;
use yii\helpers\ArrayHelper;

class WorkerGroupHelper
{
    /**
     * @return array
     */
    public static function createGroupWorkersMap()
    {
        return ArrayHelper::map(WorkerGroup::getAllGroupWorkers(), 'group_id', 'name');
    }

}