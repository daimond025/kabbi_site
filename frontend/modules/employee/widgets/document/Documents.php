<?php

namespace frontend\modules\employee\widgets\document;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\widgets\ActiveForm;

class Documents extends Widget
{
    const VIEW_PREFIX = 'document_';

    /**
     * @var object yii\widgets\ActiveForm
     */
    public $form;
    /**
     * @var array Map of document codes ["code" => "AR document model"]
     */
    public $documentMap;
    public $documentCodesHiddenName = 'Documents[]';
    public $entityHasDocumentMap;
    public $scanDocumentMap;
    public $ajaxUpload = false;
    /**
     * Primery key of entity which has the document
     * @var int
     */
    public $entityId;

    /**
     * A configuration array: the array must contain a `class` element which is treated as the object class,
     * and the rest of the name-value pairs will be used to initialize the corresponding object properties
     * @var array
     */
    public $scanModelConfig;
    public $isRead = false;
    private $_scanModel;

    public function init()
    {
        parent::init();

        if (!is_array($this->documentMap)) {
            throw new InvalidConfigException('Field "document" must be array.');
        }

        if (!$this->isRead && !($this->form instanceof ActiveForm)) {
            throw new InvalidConfigException('Field "form" must be instance of yii\widgets\ActiveForm');
        }

        $this->initScanModel();
    }

    public function run()
    {
        if($this->isRead) {
            return $this->render('index_read', [
                'documentMap'          => $this->documentMap,
                'viewPrefix'           => self::VIEW_PREFIX,
                'hiddenName'           => $this->documentCodesHiddenName,
                'entityHasDocumentMap' => $this->entityHasDocumentMap,
                'scanDocumentMap'      => $this->scanDocumentMap,
                'ajaxUpload'           => $this->ajaxUpload,
                'scanModel'            => $this->_scanModel,
                'entityId'             => $this->entityId,
            ]);
        }

        return $this->render('index', [
            'documentMap'          => $this->documentMap,
            'form'                 => $this->form,
            'viewPrefix'           => self::VIEW_PREFIX,
            'hiddenName'           => $this->documentCodesHiddenName,
            'entityHasDocumentMap' => $this->entityHasDocumentMap,
            'scanDocumentMap'      => $this->scanDocumentMap,
            'ajaxUpload'           => $this->ajaxUpload,
            'scanModel'            => $this->_scanModel,
            'entityId'             => $this->entityId,
        ]);
    }

    private function initScanModel()
    {
        if (!empty($this->scanModelConfig)) {
            $this->_scanModel = Yii::createObject($this->scanModelConfig);
        }
    }
}