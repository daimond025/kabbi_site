<?php
use yii\helpers\Html;

/* @var $model \common\modules\employee\models\documents\WorkerPassport */
/* @var $scans \frontend\modules\employee\models\documents\WorkerDocumentScan[] */

?>
<h3><?= t('app', 'Passport') ?></h3>

<section class="row">
<?
echo \frontend\widgets\file\FileMultipleRead::widget([
    'models'       => $scans,
    'attribute'    => 'filename',
]) ?>
</section>

<section class="row">
    <div class="row_label"><?= t('app', 'Serial/Number') ?></div>
    <div class="row_input">
        <div class="input_with_icon">
            <div class="input_with_icon">
                <?= !empty($model->series) ? Html::encode($model->series) : '' ?>
                <?= !empty($model->number) ? Html::encode($model->number) : '' ?>
                <?= empty($model->number) && empty($model->series) ? '<div class="row_input"></div>' : '' ?>
            </div>
            <span></span>
        </div>
    </div>
</section>

<section class="row">
    <div class="row_label"><?= Html::activeLabel($model, 'issued') ?></div>
    <div class="row_input">
        <div class="input_with_icon">
            <div class="input_with_icon">
                <?= !empty($model->issued) ? Html::encode($model->issued) : '<div class="row_input"></div>' ?>
            </div>
            <span></span>
        </div>
    </div>
</section>

<section class="row">
    <div class="row_label"><?= Html::activeLabel($model, 'registration') ?></div>
    <div class="row_input">
        <div class="input_with_icon">
            <div class="input_with_icon">
                <?= !empty($model->registration) ? Html::encode($model->registration) : '<div class="row_input"></div>' ?>
            </div>
            <span></span>
        </div>
    </div>
</section>

<section class="row">
    <div class="row_label"><?= Html::activeLabel($model, 'actual_address') ?></div>
    <div class="row_input">
        <div class="input_with_icon">
            <div class="input_with_icon">
                <?= !empty($model->actual_address) ? Html::encode($model->actual_address) : '<div class="row_input"></div>' ?>
            </div>
            <span></span>
        </div>
    </div>
</section>



