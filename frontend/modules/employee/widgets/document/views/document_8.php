<?php

use frontend\widgets\file\File;
use yii\helpers\Html;
use common\helpers\DateTimeHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var integer $code Code of document */
/* @var $model \common\modules\employee\models\documents\WorkerPts */
/* @var integer $entityId */
/* @var integer $documentCode */
/* @var integer $entityHasDocumentId */
/* @var bool $ajaxUpload */

?>
<h3><?= t('employee', 'PTS') ?></h3>
<section>
    <!--Серия/Номер-->
    <section class="row">
        <div class="row_label"><label><?= t('app', 'Serial/Number') ?></label></div>
        <div class="row_input">
            <div class="pasport_input">
                <?= $form->field($model, 'series')->begin(); ?>
                <?= Html::activeTextInput($model, 'series', ['class' => 'pasport_ser']) ?>
                <?= $form->field($model, 'series')->end(); ?>
                <?= $form->field($model, 'number')->begin(); ?>
                <?= Html::activeTextInput($model, 'number', ['class' => 'pasport_num']) ?>
                <?= $form->field($model, 'number')->end(); ?>
            </div>
        </div>
    </section>

    <!--Скан-->
    <?
    echo \frontend\widgets\file\FileMultiple::widget([
        'models'       => $scans,
        'attribute'    => 'filename',
        'form_name'    => 'WorkerDocumentScan',
        'label'        => t('app', 'Scan'),
        'ajaxUpload'   => $ajaxUpload,
        'uploadAction' => Url::to([
            '/employee/worker/scan-upload',
            'has_document_id' => $entityHasDocumentId,
            'multiple'        => true,
            'entityId'        => $entityId,
            'code'            => $documentCode,
        ]),
        'removeAction' => Url::to('/employee/worker/scan-remove'),
    ]) ?>

</section>
