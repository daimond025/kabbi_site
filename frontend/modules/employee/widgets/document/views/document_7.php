<?php

use frontend\widgets\file\File;
use yii\helpers\Html;
use common\helpers\DateTimeHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var integer $code Code of document */
/* @var $model \common\modules\employee\models\documents\WorkerDriverLicense */
/* @var integer $entityId */
/* @var integer $documentCode */
/* @var integer $entityHasDocumentId */
/* @var bool $ajaxUpload */
?>
<h3><?= t('employee', 'Driver License') ?></h3>
<section>
    <!--Серия/Номер-->
    <section class="row">
        <div class="row_label"><label><?= t('app', 'Serial/Number') ?></label></div>
        <div class="row_input">
            <div class="pasport_input">
                <?= $form->field($model, 'series')->begin(); ?>
                <?= Html::activeTextInput($model, 'series', ['class' => 'pasport_ser']) ?>
                <?= $form->field($model, 'series')->end(); ?>
                <?= $form->field($model, 'number')->begin(); ?>
                <?= Html::activeTextInput($model, 'number', ['class' => 'pasport_num']) ?>
                <?= $form->field($model, 'number')->end(); ?>
            </div>
        </div>
    </section>

    <!--Категория-->
    <section class="row">
        <div class="row_label"><label><?= t('employee', 'Driver license Category') ?></label></div>
        <div class="row_input">
            <div class="grid_item">
                <div class="select_checkbox">
                    <a class="a_sc select" rel="<?= t('employee', 'Choose categories') ?>"><?= t('employee',
                            'Choose categories') ?></a>
                    <div class="b_sc">
                        <?= Html::activeCheckboxList($model, 'categoryMap', $model->getDriverCategoryMap(),
                            ['separator' => '<br>']) ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--Дата начала вождения-->
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'start_date') ?></div>
        <div class="row_input">
            <div class="grid_3">
                <?= $form->field($model, 'start_day')->begin(); ?>
                <div class="grid_item">
                    <?= Html::activeDropDownList($model, 'start_day', DateTimeHelper::getMonthDayNumbers(), [
                        'class'  => 'default_select',
                        'prompt' => t('user', 'Day'),
                    ]); ?>
                </div>
                <?= $form->field($model, 'start_day')->end(); ?>
                <?= $form->field($model, 'start_month')->begin(); ?>
                <div class="grid_item">
                    <?= Html::activeDropDownList($model, 'start_month', DateTimeHelper::getMonthList(), [
                        'class'  => 'default_select',
                        'prompt' => t('user', 'Month'),
                    ]); ?>
                </div>
                <?= $form->field($model, 'start_month')->end(); ?>
                <?= $form->field($model, 'start_year')->begin(); ?>
                <div class="grid_item">
                    <?= Html::activeDropDownList($model, 'start_year', DateTimeHelper::getOldYearList(0, 50), [
                        'class'  => 'default_select',
                        'prompt' => t('user', 'Year'),
                    ]); ?>
                </div>
                <?= $form->field($model, 'start_year')->end(); ?>
            </div>
        </div>
    </section>

    <!--Скан-->
    <?
    echo \frontend\widgets\file\FileMultiple::widget([
        'models'       => $scans,
        'attribute'    => 'filename',
        'form_name'    => 'WorkerDocumentScan',
        'label'        => t('app', 'Scan'),
        'ajaxUpload'   => $ajaxUpload,
        'uploadAction' => Url::to([
            '/employee/worker/scan-upload',
            'has_document_id' => $entityHasDocumentId,
            'multiple'        => true,
            'entityId'        => $entityId,
            'code'            => $documentCode,
        ]),
        'removeAction' => Url::to('/employee/worker/scan-remove'),
    ]) ?>
</section>
