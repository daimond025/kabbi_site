<?php

/**
 * @var array $documentMap
 * @var array $entityHasDocumentMap
 * @var array $scanDocumentMap
 * @var $form yii\widgets\ActiveForm
 * @var string $viewPrefix
 * @var string $hiddenName
 * @var integer $worker_id
 * @var bool $ajaxUpload
 * @var $model \common\modules\employee\models\documents\WorkerDocumentBase;
 * @var $scanModel \yii\db\ActiveRecord
 * @var int $entityId
 */

foreach ($documentMap as $code => $model) {
    echo $this->render('read_' . $viewPrefix . $code, [
        'model'               => $model,
        'scans'               => getValue($scanDocumentMap[$code], [$scanModel]),
    ]);
}