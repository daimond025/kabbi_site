<?php

/**
 * @var array $documentMap
 * @var array $entityHasDocumentMap
 * @var array $scanDocumentMap
 * @var $form yii\widgets\ActiveForm
 * @var string $viewPrefix
 * @var string $hiddenName
 * @var integer $worker_id
 * @var bool $ajaxUpload
 * @var $model \common\modules\employee\models\documents\WorkerDocumentBase;
 * @var $scanModel \yii\db\ActiveRecord
 * @var int $entityId
 */

use yii\helpers\Html;

foreach ($documentMap as $code => $model) {
    echo $this->render($viewPrefix . $code, [
        'form'                => $form,
        'model'               => $model,
        'entityHasDocumentId' => getValue($entityHasDocumentMap[$code]),
        'scans'               => getValue($scanDocumentMap[$code], [$scanModel]),
        'ajaxUpload'          => $ajaxUpload,
        'entityId'            => $entityId,
        'documentCode'        => $code,
    ]);
    echo Html::hiddenInput($hiddenName, $code);
}