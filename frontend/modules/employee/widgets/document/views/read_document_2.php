<?php
use yii\helpers\Html;

/* @var $model \common\modules\employee\models\documents\WorkerPassport */
/* @var $scans \frontend\modules\employee\models\documents\WorkerDocumentScan[] */
?>
<h3><?= t('employee', 'INN') ?></h3>

<section class="row">
    <?
    echo \frontend\widgets\file\FileMultipleRead::widget([
        'models'       => $scans,
        'attribute'    => 'filename',
    ]) ?>
</section>

<section class="row">
    <div class="row_label"><?= Html::activeLabel($model, 'number') ?></div>
    <div class="row_input">
        <div class="input_with_icon">
            <div class="input_with_icon">
                <?= !empty($model->number) ? Html::encode($model->number) : '<div class="row_input"></div>' ?>
            </div>
            <span></span>
        </div>
    </div>
</section>


