<?php
use frontend\widgets\file\FileMultiple;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $form yii\widgets\ActiveForm */
/* @var integer $code Code of document */
/* @var $model \common\modules\employee\models\documents\WorkerPassport */
/* @var $scans \frontend\modules\employee\models\documents\WorkerDocumentScan[] */
/* @var bool $ajaxUpload */
/* @var integer $entityHasDocumentId */
/* @var integer $entityId */
/* @var integer $documentCode */
?>
<h3><?= t('app', 'Passport') ?></h3>
<section>
    <!--Серия/Номер-->
    <section class="row">
        <div class="row_label"><label><?= t('app', 'Serial/Number') ?></label></div>
        <div class="row_input">
            <div class="pasport_input">
                <?= $form->field($model, 'series')->begin(); ?>
                <?= Html::activeTextInput($model, 'series', ['class' => 'pasport_ser']) ?>
                <?= $form->field($model, 'series')->end(); ?>
                <?= $form->field($model, 'number')->begin(); ?>
                <?= Html::activeTextInput($model, 'number', ['class' => 'pasport_num']) ?>
                <?= $form->field($model, 'number')->end(); ?>
            </div>
        </div>
    </section>

    <!--Когда и кем выдан-->
    <section class="row">
        <?= $form->field($model, 'issued')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'issued') ?></div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'issued') ?>
        </div>
        <?= $form->field($model, 'issued')->end(); ?>
    </section>

    <!--Адрес прописки-->
    <section class="row">
        <?= $form->field($model, 'registration')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'registration') ?></div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'registration') ?>
        </div>
        <?= $form->field($model, 'registration')->end(); ?>
    </section>

    <!--Адрес проживания-->
    <section class="row">
        <?= $form->field($model, 'actual_address')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'actual_address') ?></div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'actual_address') ?>
        </div>
        <?= $form->field($model, 'actual_address')->end(); ?>
    </section>

    <?
    echo FileMultiple::widget([
        'models'       => $scans,
        'attribute'    => 'filename',
        'form_name'    => 'WorkerDocumentScan',
        'label'        => t('app', 'Scan'),
        'ajaxUpload'   => $ajaxUpload,
        'uploadAction' => Url::to([
            '/employee/worker/scan-upload',
            'has_document_id' => $entityHasDocumentId,
            'multiple'        => true,
            'entityId'        => $entityId,
            'code'            => $documentCode,
        ]),
        'removeAction' => Url::to('/employee/worker/scan-remove'),
    ]) ?>
</section>