<?php

use frontend\widgets\file\File;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var integer $code Code of document */
/* @var $model \common\modules\employee\models\documents\WorkerMedicalCertificate */
/* @var integer $entityId */
/* @var integer $documentCode */
/* @var integer $entityHasDocumentId */
/* @var bool $ajaxUpload */

?>
<h3><?= t('employee', 'Medical certificate') ?></h3>
<section>
    <!--Номер-->
    <section class="row">
        <?= $form->field($model, 'number')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'number') ?></div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'number') ?>
            <?= Html::error($model, 'number',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'snils')->end(); ?>
    </section>

    <!--Скан-->
    <?= File::widget([
        'form'         => $form,
        'model'        => current($scans),
        'attribute'    => 'filename',
        'ajaxUpload'   => $ajaxUpload,
        'uploadAction' => Url::to([
            '/employee/worker/scan-upload',
            'has_document_id' => $entityHasDocumentId,
            'entityId'        => $entityId,
            'code'            => $documentCode,
        ]),
    ]) ?>
</section>
