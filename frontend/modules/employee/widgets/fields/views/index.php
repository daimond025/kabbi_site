<?php

use yii\helpers\Html;
use common\modules\employee\models\position\PositionField;

/** @var array $fieldMap */
/** @var array $models */
/** @var string $modelValuePrefix */
/** @var string $label */
/** @var string $message_category */
/** @var integer $hasPositionId */
/** @var string $fieldTemplate */
/** @var $model common\modules\employee\models\worker\WorkerPositionFieldValue */
?>
<? if (!empty($fieldMap)): ?>
    <h3><?= $label ?></h3>
    <section>
        <?
        $index = 0;
        foreach ($fieldMap as $field_id => $data):
            if (!isset($models[$field_id])) {
                continue;
            }
            $model = $models[$field_id];
            $modelField = $modelValuePrefix . $data['type'];
            $modelFormName = $model->formName();
            $config = [
                'id'           => strtolower($modelFormName) . '_' . $index . '-' . $modelField,
                'value'        => $model->$modelField,
                'name'         => $modelFormName . "[$index][$modelField]",
                'autocomplete' => 'off',
            ];
            $field = $form->field($model, $modelField, [
                'template'     => $fieldTemplate,
                'errorOptions' => ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red'],
            ]);
            ?>
            <section class="row">
                <div class="row_label"><?= Html::label(Html::encode(t($message_category, $data['name']))) ?></div>
                <div class="row_input">
                    <? if ($data['type'] === PositionField::TEXT): ?>
                        <?= $field->textarea($config) ?>
                    <? else: ?>
                        <?= $field->textInput($config) ?>
                    <? endif ?>
                </div>
            </section>
            <?= Html::hiddenInput($modelFormName . "[$index][has_position_id]", $hasPositionId) ?>
            <?= Html::hiddenInput($modelFormName . "[$index][field_id]", $field_id) ?>
            <? $index++ ?>
        <? endforeach; ?>
    </section>
<? endif; ?>