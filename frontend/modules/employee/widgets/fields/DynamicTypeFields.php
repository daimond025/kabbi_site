<?php

namespace frontend\modules\employee\widgets\fields;

use common\modules\employee\models\worker\WorkerPositionFieldValue;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

class DynamicTypeFields extends Widget
{
    const VIEW_NAME = 'index';

    public $label;
    /**
     * @var object yii\widgets\ActiveForm
     */
    public $form;
    /**
     * Fields for display.
     * @var array [field_id => ['name' => $name, 'type' => $type]]
     * Ex.: [1 => ['name' => 'Experience', 'type' => 'int']]
     */
    public $fieldMap;
    /**
     * @var array Array of models which instance of \common\modules\employee\models\worker\WorkerPositionFieldValue
     */
    public $models = [];
    public $modelValuePrefix = 'value_';
    /**
     * @var string Category for translating field label
     */
    public $message_category = 'worker_spec_fields';
    public $hasPositionId;
    public $fieldTemplate = "{input}\n{error}";

    public function init()
    {
        parent::init();

        if (empty($this->hasPositionId)) {
            throw new InvalidConfigException('Field "has_position_id" must be configured');
        }

        if (!($this->form instanceof ActiveForm)) {
            throw new InvalidConfigException('Field "form" must be instance of yii\widgets\ActiveForm');
        }

        if (!is_array($this->fieldMap)) {
            throw new InvalidConfigException('Field "fieldMap" must be array.');
        }

        if (!empty($this->models)) {
            if (!is_array($this->models)) {
                throw new InvalidConfigException('Field "models" must be array.');
            }
            $this->models = ArrayHelper::index($this->models, 'field_id');
        } else {
            $this->modelsInit();
        }
    }

    public function run()
    {
        return $this->render(self::VIEW_NAME, [
            'fieldMap'         => $this->fieldMap,
            'models'           => $this->models,
            'modelValuePrefix' => $this->modelValuePrefix,
            'form'             => $this->form,
            'message_category' => $this->message_category,
            'hasPositionId'    => $this->hasPositionId,
            'label'            => $this->label,
            'fieldTemplate'    => $this->fieldTemplate,
        ]);
    }

    private function modelsInit()
    {
        foreach ($this->fieldMap as $field_id => $type) {
            $this->models[$field_id] = new WorkerPositionFieldValue(['field_id' => $field_id]);
        }
    }
}