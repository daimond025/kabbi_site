<?php

namespace frontend\modules\employee\widgets\car;

use frontend\modules\employee\widgets\car\assets\operation\OperationAsset;
use yii\base\InvalidConfigException;
use yii\base\Widget;

class WorkerCars extends Widget
{
    const VIEW_NAME = 'index';

    /**
     * @var array List of car data.
     */
    public $cars = [];
    /**
     * @var integer Entity id which to link with car
     */
    public $entity_id;
    /**
     * @var string Url to car searching action for choosing worker car.
     */
    public $searchAction;
    public $groupMapGroupedByClass;
    public $tariffMapGroupedByClass;
    public $showingClassesMap;
    /**
     * @var string View with 'Remove' or 'Add' button
     */
    public $button = 'Remove';

    public function init()
    {
        parent::init();

        if (!is_array($this->cars)) {
            throw new InvalidConfigException('The field "cars" must be array');
        }

        if (empty($this->searchAction)) {
            throw new InvalidConfigException('The field "searchAction" must be configured');
        }

        OperationAsset::register($this->getView());
    }

    public function run()
    {
        return $this->render(self::VIEW_NAME, [
            'cars'                    => $this->cars,
            'searchAction'            => $this->searchAction,
            'entity_id'               => $this->entity_id,
            'button'                  => $this->button,
            'groupMapGroupedByClass'  => $this->groupMapGroupedByClass,
            'tariffMapGroupedByClass' => $this->tariffMapGroupedByClass,
            'showingClassesMap'       => $this->showingClassesMap,
        ]);
    }

}