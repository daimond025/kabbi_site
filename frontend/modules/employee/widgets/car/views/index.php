<?php
/* @var $this yii\web\View */
/* @var integer $entity_id */
/* @var string $searchAction */
/* @var array $groupMapGroupedByClass */
/* @var array $tariffMapGroupedByClass */
/* @var array $showingClassesMap */
/* @var array $groups */
/* @var array $cars */
/* @var string $button */
?>
<div class="position_cars">
    <div class="car_ajax_table">
        <?= $this->render('_cars_table',
            compact('cars', 'groups', 'button', 'groupMapGroupedByClass', 'tariffMapGroupedByClass',
                'showingClassesMap')) ?>
    </div>

    <?php
    if (app()->user->can('drivers')):?>
        <h3><?= t('employee', 'Select vehicle') ?></h3>
        <section class="car_ajax_search" data-driver="<?= $entity_id ?>">
            <div class="grid_3">
                <div class="grid_item" style="width: 100%">
                    <input style="width: 32%" class="driver_car_search" type="text"
                           placeholder="<?= t('employee', 'Brand, model or car number') ?>" value=""
                           data-search="<?= $searchAction ?>">
                </div>
            </div>
            <table class="people_table" style="margin-bottom: 30px; display: none">
                <tbody class="ajax_content">
                <tr>
                    <th class="pt_fio pt_fio2"><a href="#"><?= t('employee', 'Car') ?> <span data-type="name"
                                                                                            class="pt_down"></span></a>
                    </th>
                    <th style="width: 16%; text-align: right; padding-right: 30px;"><?= t('employee', 'Raiting') ?> <span
                            data-type="raiting" class=""></span></th>
                    <th style="width: 22%;"><?= t('employee', 'Driver group') ?></th>
                    <th style="width: 22%;"><?= t('employee', 'Tariff and class') ?></th>
                    <th style="width: 10%;"></th>
                </tr>
                </tbody>
            </table>
        </section>
    <?php endif ?>
</div>
