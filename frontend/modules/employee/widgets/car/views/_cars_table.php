<?php
/* @var $this yii\web\View */
/* @var array $groupMapGroupedByClass */
/* @var array $tariffMapGroupedByClass */
/* @var array $showingClassesMap */
/* @var array $groups */
/* @var array $cars */
/* @var string $button */
?>
<? if (!empty($cars)): ?>
    <table class="people_table" style="margin-bottom: 30px;">
        <tbody id="driver_cars">
        <tr>
            <th class="pt_fio pt_fio2"><a href="#"><?= t('employee', 'Car') ?> <span data-type="name"
                                                                                    class="pt_down"></span></a></th>
            <th style="width: 16%; text-align: right; padding-right: 30px;"><?= t('employee', 'Raiting') ?> <span
                    data-type="raiting" class=""></span></th>
            <th style="width: 22%;"><?= t('employee', 'Driver group') ?></th>
            <th style="width: 22%;"><?= t('employee', 'Tariff and class') ?></th>
            <th style="width: 10%;"></th>
        </tr>
        <?php
        $form = app()->user->can('drivers') ? '_cars' : '_cars_read';
        echo $this->render($form,
            compact('cars', 'groups', 'button', 'groupMapGroupedByClass', 'tariffMapGroupedByClass',
                'showingClassesMap')) ?>
        </tbody>
    </table>
<? else: ?>
    <p><?= t('app', 'Empty') ?></p><br/>
<? endif ?>