<?php

/* @var $this yii\web\View */
/* @var array $groupMapGroupedByClass */
/* @var array $tariffMapGroupedByClass */
/* @var array $showingClassesMap */
/* @var array $groups */
/* @var array $cars */
/* @var string $button */

use \yii\helpers\Url;
use yii\helpers\Html;

foreach ($cars as $car):
    $groupCities = [];
    ?>
    <tr data-car="<?= $car['ID'] ?>">
        <td class="pt_fio">
            <a href="<?= Url::to(['/car/car/update', 'id' => $car['ID']]) ?>">
                <span class="pt_photo"><?= $car['PHOTO'] ?></span>
                <span class="pt_fios"><?= Html::encode($car['NAME']); ?></span>
                <span class="auto_num"><span dir="auto"><?= Html::encode($car['GOV_NUMBER']); ?></span></span>
            </a>
        </td>
        <td style="text-align: right; padding-right: 30px;"><?= Html::encode($car['RAITING']); ?></td>
        <? if ($button == 'Remove'): ?>
            <td style="padding-right: 20px;">
                <select name="driver_group" class="default_select driver_group"
                        data-placeholder="<?= t('employee', 'Choose a group') ?>">
                    <?
                    if (is_array($groupMapGroupedByClass[$car['CLASS_ID']]) && !empty($groupMapGroupedByClass[$car['CLASS_ID']])) {
                        foreach ($groupMapGroupedByClass[$car['CLASS_ID']] as $groupId => $group) {
                            $selected = $car['GROUP_ID'] == $groupId;

                            echo Html::tag('option', Html::encode($group), [
                                'selected' => $selected,
                                'value'    => $groupId,
                            ]);
                        }
                    }
                    ?>
                </select>
            </td>
            <td style="padding-right: 20px;">
                <div<? if (!$car['GROUP_ID']): ?> class="hide driver_group_options"<? endif ?>>
                    <div class="select_checkbox">
                        <a class="a_sc select" data-filter="no"
                           rel="<?= t('employee', 'Choose a tariff') ?>"><?= t('employee',
                                'Choose a tariff') ?></a>
                        <div class="b_sc">
                            <ul class="group_tariff">
                                <?
                                if (is_array($tariffMapGroupedByClass[$car['CLASS_ID']]) && !empty($tariffMapGroupedByClass[$car['CLASS_ID']])) {
                                    foreach ($tariffMapGroupedByClass[$car['CLASS_ID']] as $tariffId => $tariff) {
                                        $checked = !empty($car['GROUP_ID'])
                                            && in_array($tariffId, $car['GROUP_TARIFFS']);

                                        echo Html::tag('li', Html::tag('label',
                                            Html::input('checkbox', 'tariff[]', $tariffId, [
                                                'checked' => $checked,
                                            ]) . Html::encode($tariff)));
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <div class="select_checkbox">
                        <a data-filter="no" class="a_sc select"
                           rel="<?= t('employee', 'Choose a class') ?>"><?= t('employee',
                                'Choose a class') ?></a>
                        <div class="b_sc">
                            <ul class="group_class">
                                <?
                                if (is_array($showingClassesMap) && !empty($showingClassesMap)) {
                                    foreach ($showingClassesMap as $classId => $class) {
                                        $isCarClass = $classId == $car['CLASS_ID'];
                                        $checked    = $isCarClass
                                            || (!empty($car['GROUP_ID']) && in_array($classId, $car['GROUP_CLASSES']));

                                        echo Html::tag('li', Html::tag('label',
                                            Html::input('checkbox', 'class[]', $classId, [
                                                'disabled' => $isCarClass,
                                                'checked'  => $checked,
                                            ]) . Html::encode($class)));
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </td>
            <td>
                <? if (app()->user->can('drivers')): ?>
                    <div style="text-align: right">
                        <a href="<?= Url::to(['/employee/car/delete-car', 'car_id' => $car['ID']]) ?>"
                           class="pt_del"><?= t('app',
                                'Remove') ?></a>
                    </div>
                <? endif ?>
            </td>
        <? else: ?>
            <td></td>
            <td></td>
            <td>
                <? if (app()->user->can('drivers')): ?>
                    <div style="text-align: right">
                        <a href="<?= Url::to(['/employee/car/add-car', 'car_id' => $car['ID']]) ?>"
                           class="pt_add"><?= t('app',
                                'Choose') ?></a>
                    </div>
                <? endif ?>
            </td>
        <? endif ?>
    </tr>
<?php endforeach; ?>
