<?php

/* @var $this yii\web\View */
/* @var array $groupMapGroupedByClass */
/* @var array $tariffMapGroupedByClass */
/* @var array $showingClassesMap */
/* @var array $groups */
/* @var array $cars */
/* @var string $button */

use \yii\helpers\Url;
use yii\helpers\Html;


foreach ($cars as $car):
    ?>
    <tr data-car="<?= $car['ID'] ?>">
        <td class="pt_fio">
            <a href="<?= Url::to(['/car/car/update', 'id' => $car['ID']]) ?>">
                <span class="pt_photo"><?= $car['PHOTO'] ?></span>
                <span class="pt_fios"><?= Html::encode($car['NAME']); ?></span>
                <span class="auto_num"><?= Html::encode($car['GOV_NUMBER']); ?></span>
            </a>
        </td>
        <td style="text-align: right; padding-right: 30px;"><?= Html::encode($car['RAITING']); ?></td>
        <? if ($button == 'Remove'): ?>
            <td style="padding-right: 20px;">
                <? if (!empty($groupMapGroupedByClass[$car['CLASS_ID']])): ?>
                    <?
                    foreach ($groupMapGroupedByClass[$car['CLASS_ID']] as $groupId => $group):
                        $selected = $car['GROUP_ID'] == $groupId ? 'selected="" ' : '';
                        ?>
                        <div class="row-input"><?= ($selected == '') ? '' : Html::encode($group) ?></div>
                    <? endforeach; ?>
                <? endif ?>
            </td>
            <td style="padding-right: 20px;">
                <div<? if (!$car['GROUP_ID']): ?> class="hide driver_group_options"<? endif ?>>
                    <div class="b_sc">
                        <ul class="group_tariff">
                            <? if (!empty($tariffMapGroupedByClass[$car['CLASS_ID']])): ?>
                                <?
                                foreach ($tariffMapGroupedByClass[$car['CLASS_ID']] as $tariffId => $tariff):
                                    $checked = !empty($car['GROUP_ID']) && in_array($tariffId,
                                        $car['GROUP_TARIFFS']) ? ' checked' : '';
                                    ?>
                                    <div
                                            class="row-input"><?= ($checked == '') ? '' : Html::encode($tariff) ?></div>
                                <? endforeach; ?>
                            <? endif ?>
                        </ul>
                    </div>
                    <div class="b_sc">
                        <ul class="group_class">
                            <?
                            foreach ($showingClassesMap as $classId => $class):
                                $checked = !empty($car['GROUP_ID']) && in_array($classId,
                                    $car['GROUP_CLASSES']) ? ' checked' : '';
                                ?>
                                <li><label><input<?= $checked ?> name="class[]" type="checkbox"
                                                                 value="<?= $classId ?>"> <?= Html::encode($class); ?>
                                    </label></li>
                            <? endforeach; ?>
                        </ul>
                    </div>
                </div>
            </td>
        <? endif ?>
    </tr>
<?php endforeach; ?>