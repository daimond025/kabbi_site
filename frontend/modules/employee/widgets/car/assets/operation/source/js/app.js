;(function () {
    'use strict';

    var app = {
        container: '.position_cars',
        init: function () {
            this.deleteCarEvent();
            this.addCarEvent();
            this.searchCarEvent();
            this.sortEvent();
            this.chooseDriverGroupEvent();
            this.groupTariffUpdateEvent();
            this.groupClassUpdateEvent();
            this.selectStylesInit();
        },
        afterAjax: function () {
            this.selectStylesInit();
        },
        selectStylesInit: function () {
            createCheckboxStringOnReady();
            createCheckboxStringOnClick();
            select_init();
        },
        addCarEvent: function () {
            var _this = this;
            $('div.tabs_content').on('click', '.people_table div a.pt_add', function (e) {
                e.preventDefault();
                var button = $(this);
                var container = button.parents(_this.container);
                var tr = button.parents('tr');
                var ajax_container = container.find('.car_ajax_table');
                var entity_id = container.find('.car_ajax_search').data('driver');
                var position_id = $(this).parents('.spoler_content').prev('.check_spoler').find('input').data('position');

                $.get(
                    button.attr('href'),
                    {entity_id: entity_id, position_id: position_id},
                    function (html) {
                        //tr.remove();
                        ajax_container.html(html);
                        _this.afterAjax();
                    }
                );
            });
        },
        deleteCarEvent: function () {
            var _this = this;
            $('div.tabs_content').on('click', '.people_table div a.pt_del', function (e) {
                e.preventDefault();
                var button = $(this);
                var tr = button.parents('tr');
                var tr_size = button.parents('table.people_table').find('tr').size();
                var container = button.parents(_this.container);
                var ajax_container = container.find('.car_ajax_table');
                var entity_id = container.find('.car_ajax_search').data('driver');

                $.get(
                    button.attr('href'),
                    {entity_id: entity_id},
                    function (json) {
                        if (json) {
                            if ((tr_size - 1) == 1) {
                                ajax_container.empty();
                            }
                            else {
                                tr.remove();
                            }
                        }
                    },
                    'json'
                );
            });
        },
        searchCarEvent: function () {
            var search_val_old = '';
            var _this = this;
            //Поиск авто
            $('div.tabs_content').on('keyup', '.driver_car_search', function (e) {
                var input_search = $(this);

                if (e.which != 13) {
                    var parent = input_search.parents('.car_ajax_search');
                    var entity_id = parent.data('driver');
                    var ajax_content = parent.find('.ajax_content');
                    var position_id = $(this).parents('.spoler_content').prev('.check_spoler').find('input').data('position');

                    setTimeout(function () {

                        var car = input_search.val();
                        if (search_val_old === car)
                            return false;

                        search_val_old = car;

                        if (car != '') {
                            $.ajax({
                                type: "POST",
                                url: input_search.data('search'),
                                data: {
                                    input_search: car,
                                    view: 'index',
                                    button: '',
                                    driver_car: 0,
                                    entity_id: entity_id,
                                    position_id: position_id
                                },
                                dataType: "html",
                                beforeSend: function () {
                                    input_search.addClass('input_preloader');
                                },
                                success: function (html) {
                                    clearSortArrows();
                                    input_search.removeClass('input_preloader');
                                    put_search(ajax_content, html);
                                    _this.afterAjax();

                                    if (html.trim() != '')
                                        ajax_content.parent().show();
                                }
                            });
                        } else {
                            ajax_content.find('tr:not(:first)').remove();
                        }
                    }, 700);
                }
            });
        },
        sortEvent: function () {
            var _this = this;
            $('div.tabs_content').on('click', '.car_ajax_search .people_table th a, .car_ajax_table .people_table th a', function (e) {
                e.preventDefault();

                var a = $(this);
                var container = a.parents(_this.container);
                var entity_id = container.find('.car_ajax_search').data('driver');
                var tbody = a.parents('tbody');
                var is_search_table = tbody.hasClass('ajax_content');
                var url = $('.driver_car_search').eq(0).data('search');
                var sort_span = a.find('span');
                var sort_class_attr = sort_span.attr('class');
                var sort_class = sort_class_attr === 'pt_down' ? 'pt_up' : 'pt_down';
                var sort = sort_class_attr === 'pt_down' ? 'desc' : 'asc';
                var sort_type = sort_span.data('type');

                var all_sort_span = a.parents('tr').find('th a span');
                all_sort_span.attr('class', '');

                var data = {
                    sort_type: sort_type,
                    sort: sort,
                    view: 'index',
                    entity_id: entity_id
                };

                //Первая - сортировка авто в поиске, вторая - авто у водителя
                if (is_search_table) {
                    var search_string = a.parents('.car_ajax_search').find('.driver_car_search').val();

                    if (!search_string) {
                        return false;
                    }

                    data.input_search = search_string;
                    data.button = '';
                    data.driver_car = 0;
                } else {
                    data.button = 'Remove';
                }

                $.post(
                    url,
                    data,
                    function (html) {
                        put_search(tbody, html);
                        _this.afterAjax();
                    }
                );

                sort_span.attr('class', sort_class);
            });
        },
        chooseDriverGroupEvent: function () {
            $('div.tabs_content').on('change', 'select.driver_group', function () {
                var tr = $(this).parents('tr');
                var car_id = tr.data('car');
                var group_id = $(this).val();
                var tariffs = tr.find('.group_tariff input');
                var classes = tr.find('.group_class input');

                $.post(
                    '/car/car/add-driver-group',
                    {car_id: car_id, group_id: group_id},
                    function (json) {
                        if (json.result == 1) {
                            if ('group_tariffs' in json) {
                                tariffs.prop('checked', false);
                                for (var i = 0; i < json.group_tariffs.length; i++) {
                                    tariffs.each(function () {
                                        if (json.group_tariffs[i] == $(this).val()) {
                                            $(this).prop('checked', true);
                                        }
                                    });
                                }
                            }

                            if ('group_classes' in json) {
                                classes.prop('checked', false);
                                for (var i = 0; i < json.group_classes.length; i++) {
                                    classes.each(function () {
                                        if (json.group_classes[i] == $(this).val()) {
                                            $(this).prop('checked', true);
                                        }
                                    });
                                }
                            }

                            tr.find('.driver_group_options').removeClass('hide');
                            createCheckboxStringOnReady();
                        }
                    },
                    'json'
                );
            });
        },
        groupTariffUpdateEvent: function () {
            $('div.tabs_content').on('change', '.group_tariff input', function () {
                var tr = $(this).parents('tr');
                var tariffs = tr.find('.group_tariff input:checked');
                var tariffs_val = [];
                var car_id = tr.data('car');

                tariffs.each(function () {
                    tariffs_val.push($(this).val());
                });

                $.post(
                    '/car/car/add-driver-group-tariff',
                    {car_id: car_id, tariffs: tariffs_val},
                    function (json) {
                    },
                    'json'
                );
            });
        },
        groupClassUpdateEvent: function () {
            $('div.tabs_content').on('change', '.group_class input', function () {
                var tr = $(this).parents('tr');
                var classes = tr.find('.group_class input:checked');
                var classes_val = [];
                var car_id = tr.data('car');

                classes.each(function () {
                    classes_val.push($(this).val());
                });

                $.post(
                    '/car/car/add-driver-group-class',
                    {car_id: car_id, classes: classes_val},
                    function (json) {
                    },
                    'json'
                );
            });
        }
    };

    app.init();
})();