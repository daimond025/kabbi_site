<?php

namespace frontend\modules\employee\widgets\car\assets\operation;

use yii\web\AssetBundle;

class OperationAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/employee/widgets/car/assets/operation/source';

    public $publishOptions = [
        'forceCopy' => true,
    ];

    public $css = [
    ];
    public $js = [
        'js/app.js',
    ];
    public $depends = [
    ];
}