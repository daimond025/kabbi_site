<?php


namespace frontend\modules\employee\components\tariff;


use app\modules\tariff\models\TaxiTariff;

class TaxiTariffRepository
{
    /**
     * @param array|int $cityId
     * @param array|int $positionId
     * @param int       $block
     * @param array     $arVisible ['operator', 'cabinet']
     *
     * @return array|false
     */
    public function getTariffsGroupByClass($cityId, $positionId, $block = 0, array $arVisible = [])
    {
        return TaxiTariff::getTariffsGroupByClass($cityId, $positionId, $block, $arVisible);
    }

    public function getTariffsGroupByPositionClass($cityId, $positionId, $block = 0)
    {
        return TaxiTariff::getTariffsGroupByPositionId($positionId, $cityId, $block);
    }

    /**
     * Getting tariffs filtered by conditions
     *
     * @param int[]        $cityIds
     * @param int          $classId
     * @param boolean|null $active
     * @param array        $roleFilter
     *
     * @return array
     */
    public function getFilteredTariffs(
        $cityIds,
        $classId = null,
        $active = null,
        array $roleFilter = []
    ) {
        return TaxiTariff::getFilteredTariffs(
            $cityIds, $classId, $active, $roleFilter);
    }
}