<?php


namespace frontend\modules\employee\components\tariff;


use common\modules\employee\components\tariff\TariffRepository as TariffRepositoryBase;

class TariffRepository extends TariffRepositoryBase
{
    public function getDriverTariffs($cityId, $carClasses, $positionId = null)
    {
        $modelClass = $this->modelClass;

        return (array)$modelClass::getDriverTariffs($cityId, $carClasses, $positionId);
    }
}