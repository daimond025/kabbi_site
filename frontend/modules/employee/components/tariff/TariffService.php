<?php


namespace frontend\modules\employee\components\tariff;


use common\modules\employee\components\tariff\TariffService as TariffServiceBase;
use yii\helpers\ArrayHelper;

class TariffService extends TariffServiceBase
{
    public function getTariffMapGroupByClass($tariffs)
    {
        return ArrayHelper::map($tariffs, 'tariff_id', 'name', 'class_id');
    }
}