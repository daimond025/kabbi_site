<?php


namespace frontend\modules\employee\components\tariff;


use yii\helpers\ArrayHelper;

class TaxiTariffService
{
    /**
     * @var TaxiTariffRepository
     */
    private $repository;

    public function __construct(TaxiTariffRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getShowingClassesMap($tariffs)
    {
        return ArrayHelper::map($tariffs, 'class_id', function ($item) {
            return t('car', $item['class']);
        });
    }

    /**
     * @return TaxiTariffRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }
}