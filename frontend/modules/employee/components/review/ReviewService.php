<?php


namespace frontend\modules\employee\components\review;

use app\modules\client\models\ClientReview;
use frontend\modules\employee\models\worker\WorkerReviewRating;
use yii\base\Object;

class ReviewService extends Object
{
    /**
     * @param int     $tenantId
     * @param int     $workerId PK of tbl_worker
     * @param int     $positionId PK of tbl_position
     * @param booelan $notActive
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getClientReviews($tenantId, $workerId, $positionId, $notActive)
    {
        $clientReviews = ClientReview::find()
            ->alias('review')
            ->joinWith(['client c'])
            ->with('order')
            ->andWhere([
                'review.worker_id'   => $workerId,
                'review.position_id' => $positionId,
                'c.tenant_id'        => $tenantId,
            ])
            ->orderBy(['create_time' => SORT_DESC]);

        if ($notActive) {
            $clientReviews->andWhere([
                'review.active' => 0,
            ]);
        }

        return $clientReviews->all();
    }

    public function getWorkerReviewRating($workerId, $workerPositionId)
    {
        return WorkerReviewRating::find()
            ->where([
                WorkerReviewRating::tableName() . '.worker_id' => $workerId,
                'position_id'                                  => $workerPositionId,
            ])
            ->joinWith([
                'worker' => function ($query) {
                    $query->where(['tenant_id' => user()->tenant_id]);
                },
            ], false)
            ->one();
    }

    /**
     * @param \app\modules\client\models\ClientReview[] $clientReviews
     * @param int                                       $workerId
     * @param int                                       $workerPositionId
     *
     * @return array|WorkerReviewRating
     */
    public function ratingInit($clientReviews, $workerId, $workerPositionId)
    {
        $arRating           = $this->initRatingByClientReviews($clientReviews);
        $workerReviewRating = new WorkerReviewRating([
            'worker_id'   => $workerId,
            'position_id' => $workerPositionId,
        ]);
        $workerReviewRating->load($arRating, '');
        if ($workerReviewRating->save()) {
            return $workerReviewRating;
        }

        return $workerReviewRating->getErrors();
    }

    /**
     * Calculate raiting.
     *
     * @param \app\modules\client\models\ClientReview[] $clientReviews
     *
     * @return array
     */
    protected function initRatingByClientReviews($clientReviews)
    {
        $rating = [
            'one'   => 0,
            'two'   => 0,
            'three' => 0,
            'four'  => 0,
            'five'  => 0,
            'count' => 0,
        ];

        foreach ($clientReviews as $review) {
            switch ($review->rating) {
                case 1:
                    $rating['one']++;
                    break;
                case 2:
                    $rating['two']++;
                    break;
                case 3:
                    $rating['three']++;
                    break;
                case 4:
                    $rating['four']++;
                    break;
                case 5:
                    $rating['five']++;
                    break;
            }

            $rating['count']++;
        }

        return $rating;
    }

    /**
     * Average of rating.
     *
     * @param $workerReviewRating WorkerReviewRating
     *
     * @return float
     */
    public function getAverageValue($workerReviewRating)
    {
        if ($workerReviewRating->count == 0) {
            return 0;
        }

        return (int)(
                ($workerReviewRating->one
                    + 2 * $workerReviewRating->two
                    + 3 * $workerReviewRating->three
                    + 4 * $workerReviewRating->four
                    + 5 * $workerReviewRating->five) / $workerReviewRating->count * 10)
            / 10;
    }

    /**
     * Getting percent value of the raiting.
     *
     * @param         $workerReviewRating WorkerReviewRating
     * @param integer $value
     * @param integer $decimals The number of digits after the decimal point.
     *
     * @return integer|float
     */
    public function getPercentValue($workerReviewRating, $value, $decimals = 1)
    {
        if (!$workerReviewRating->count) {
            return 0;
        }

        return round(($value * 100) / $workerReviewRating->count, $decimals) . '%';
    }
}