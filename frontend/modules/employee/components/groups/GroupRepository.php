<?php


namespace frontend\modules\employee\components\groups;


use frontend\modules\employee\models\groups\WorkerGroup;
use yii\db\ActiveQuery;

class GroupRepository
{
    /**
     * @param int $classId
     * @param null $positionId
     * @param null $cityId
     * @param int $block
     * @return WorkerGroup[]
     */
    public function getGroupsByClass($classId, $positionId = null, $cityId = null, $block = 0)
    {
        return WorkerGroup::find()
            ->alias('wg')
            ->where([
                'tenant_id' => user()->tenant_id,
                'class_id'  => $classId,
            ])
            ->andFilterWhere(['position_id' => $positionId, 'block' => $block])
            ->with([
                'tariffs'    => function (ActiveQuery $query) {
                    $query->select(['tariff_id', 'name'])->indexBy('tariff_id');
                },
                'showOrders' => function (ActiveQuery $query) {
                    $query->indexBy('class_id');
                },
            ])
            ->joinWith([
                'workerGroupHasCities' => function (ActiveQuery $query) use ($cityId) {
                    $query->andFilterWhere(['city_id' => $cityId]);
                },
            ], false)
            ->select(['wg.group_id', 'name', 'class_id'])
            ->indexBy('group_id')
            ->asArray()
            ->all();
    }

    /**
     * @param array $filter
     * @return WorkerGroup[]
     */
    public function getList(array $filter)
    {
        return WorkerGroup::find()
            ->where(['tenant_id' => user()->tenant_id])
            ->andFilterWhere($filter)
            ->asArray()
            ->all();
    }

}