<?php

namespace frontend\modules\employee\components\position;


use app\modules\tenant\models\User;
use common\modules\employee\models\position\Position;
use common\modules\employee\models\worker\Worker;
use common\modules\employee\models\worker\WorkerHasPosition;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

class PositionService
{
    /**
     * @var null|int
     */
    private $tenantId;

    public function __construct($tenantId = null)
    {
        $this->tenantId = $tenantId;
    }


    /**
     * @param int|array $cityId
     * @param bool $considerSubCompany
     * @return array
     */
    public function getPositionsByCity($cityId, $considerSubCompany = true)
    {
        $positionsQuery = Position::find()
            ->byCity($cityId, $this->tenantId)
            ->with([
                'module' => function (ActiveQuery $query) {
                    $query->select(['id', 'name', 'description', 'active']);
                },
            ])
            ->asArray();

        if ($considerSubCompany && app()->user->can(User::ROLE_STAFF_COMPANY)) {
            $positionsQuery
                ->leftJoin(
                    WorkerHasPosition::tableName() . ' whp',
                    Position::tableName() . '.`position_id` = `whp`.`position_id`'
                )
                ->leftJoin(
                    Worker::tableName() . ' w',
                    '`whp`.`worker_id` = `w`.`worker_id`'
                )
                ->where(['w.tenant_company_id' => user()->tenant_company_id]);
        }


        return array_map(function ($item) {
            $item['name'] = t('employee', $item['name']);
            $item['module']['name'] = t('gootax_modules', $item['module']['name']);

            return $item;
        }, $positionsQuery->all());
    }

    /**
     * @return array|Position[]
     */
    public function getAllPositions()
    {
        $positions = Position::find()
            ->asArray()
            ->all();

        return array_map(function ($item) {
            $item['name'] = t('employee', $item['name']);

            return $item;
        }, $positions);
    }

    /**
     * @return array
     */
    public function getPositionMap()
    {
        return $this->createPositionMap($this->getAllPositions());
    }

    /**
     * @param $cityId
     * @return array
     */
    public function getPositionMapByCity($cityId)
    {
        return $this->createPositionMap($this->getPositionsByCity($cityId));
    }

     /**
     * @param int|array $cityId
     * @param bool $considerSubCompany
     * @return array
     */
    public function getModuleMap($cityId, $considerSubCompany = true)
    {
        $positions = $this->getPositionsByCity($cityId, $considerSubCompany);
        $positions = ArrayHelper::map($positions, 'module.id', 'module.name');

        return array_unique($positions);
    }

    /**
     * @return array
     */
    public function getPositionIdsHasCar()
    {
        return Position::find()
            ->onlyHasCar()
            ->select('position_id')
            ->column();
    }

    /**
     * @param $positionId
     *
     * @return bool
     */
    public function hasCarPositionId($positionId){
        return in_array($positionId, $this->getPositionIdsHasCar());
    }

    /**
     * @param int $positionId
     * @return bool
     */
    public function hasCar($positionId)
    {
        return in_array($positionId, $this->getPositionIdsHasCar());
    }

    /**
     * Getting position transport type
     * @param int $positionId
     * @return string
     */
    public function getPositionTransportType($positionId)
    {
        return Position::find()->where(['position_id' => $positionId])->select('transport_type_id')->scalar();
    }

    public function setTenantId($tenantId)
    {
        $this->tenantId = $tenantId;
    }

    public function getPositionsByModule($moduleId)
    {
        $positions = Position::find()
            ->where(['module_id' => $moduleId])
            ->select(['position_id', 'name'])
            ->asArray()
            ->all();

        return array_map(function ($item) {
            $item['name'] = t('employee', $item['name']);

            return $item;
        }, $positions);
    }

    /**
     * @param int $moduleId
     * @return array
     */
    public function getPositionIdsByModule($moduleId)
    {
        return ArrayHelper::getColumn($this->getPositionsByModule($moduleId), 'position_id');
    }

    public function getPositionMapByModule($moduleId)
    {
        return $this->createPositionMap($this->getPositionsByModule($moduleId));
    }

    /**
     * @param int $positionId
     * @param array $with
     * @return array|null|Position
     */
    public function getPositionById($positionId, array $with = [])
    {
        return Position::find()->where(['position_id' => $positionId])->with($with)->one();
    }

    /**
     * @param array $positions
     * @return array
     */
    private function createPositionMap(array $positions)
    {
        return ArrayHelper::map($positions, 'position_id', 'name');
    }
}