<?php

namespace frontend\modules\employee\components\document;

use common\modules\employee\components\document\WorkerDocumentFactory as WorkerDocumentFactoryBase;
use frontend\modules\employee\models\documents\WorkerDriverLicense;
use frontend\modules\employee\models\documents\WorkerOsago;

class WorkerDocumentFactory extends WorkerDocumentFactoryBase
{
    public static function getDriverLicense()
    {
        return WorkerDriverLicense::className();
    }

    public static function getOsago()
    {
        return WorkerOsago::className();
    }

}