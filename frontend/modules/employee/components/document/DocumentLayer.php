<?php

namespace frontend\modules\employee\components\document;

use common\modules\employee\components\document\DocumentLayer as DocumentLayerBase;

class DocumentLayer extends DocumentLayerBase
{
    public static function createDocumentObjectsByCode($code)
    {
        if (is_array($code)) {
            $arObjects = [];
            foreach ($code as $item) {
                $arObjects[$item] = WorkerDocumentFactory::createDocument($item);
            }

            return $arObjects;
        }

        return WorkerDocumentFactory::createDocument($code);
    }

    public static function getDocumentObject($code, $worker_document_id)
    {
        return WorkerDocumentFactory::getDocument($code, $worker_document_id);
    }

}