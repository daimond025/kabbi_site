<?php

namespace frontend\modules\employee\components\worker;

use common\modules\employee\models\worker\WorkerHasCity;
use frontend\modules\employee\models\worker\Worker;
use yii\helpers\ArrayHelper;

class WorkerService
{
    public function getWorkerPositionMap($workerId)
    {
        $worker = $this->getWorkerWithPositions($workerId);

        return ArrayHelper::map($worker->workerHasPositions, 'position_id', function ($item) {
            return t('employee', $item['position']['name']);
        });
    }

    public function getWorkerWithPositions($workerId)
    {
        return Worker::find()
            ->where(['worker_id' => $workerId, 'tenant_id' => user()->tenant_id])
            ->with(['workerHasPositions.position'])
            ->select('worker_id')
            ->one();
    }

    public function getShortName($last_name, $name, $second_name)
    {
        $short_name = $last_name;

        if (!empty($name)) {
            $short_name .= ' ' . mb_substr($name, 0, 1, "UTF-8") . '.';
        }

        if (!empty($second_name)) {
            $short_name .= mb_substr($second_name, 0, 1, "UTF-8") . '.';
        }

        return trim($short_name);
    }

    public function getCallsign($workerId)
    {
        return Worker::find()->where(['worker_id' => $workerId])->select('callsign')->scalar();
    }

    /**
     * @param int $workerId
     *
     * @return array
     */
    public function getCityIds($workerId)
    {
        return WorkerHasCity::find()
            ->where(['worker_id' => $workerId])
            ->select('city_id')
            ->column();
    }

    /**
     * Getting worker by callsign.
     *
     * @param integer $workerCallsign
     * @param integer $tenant_id
     * @param array   $select
     *
     * @return array|null
     */
    public function getWorkerByCallsign($workerCallsign, $tenant_id = null, array $select = [])
    {
        $tenant_id = is_null($tenant_id) ? user()->tenant_id : $tenant_id;

        return Worker::find()
            ->where([
                'tenant_id' => $tenant_id,
                'callsign'  => $workerCallsign,
            ])
            ->select($select)
            ->asArray()
            ->one();
    }


}
