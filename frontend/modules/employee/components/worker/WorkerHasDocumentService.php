<?php


namespace frontend\modules\employee\components\worker;

use Yii;
use common\modules\employee\models\documents\Document;
use frontend\modules\employee\components\document\DocumentLayer;
use frontend\modules\employee\models\documents\WorkerDocumentScan;
use yii\base\Object;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class WorkerHasDocumentService extends Object
{
    /**
     * A configuration array: the array must contain a `class` element which is treated as the object class,
     * and the rest of the name-value pairs will be used to initialize the corresponding object properties
     * @var array
     */
    public $modelConfig;

    public function getWorkerDocumentMap($workerHasDocuments)
    {
        return ArrayHelper::map($workerHasDocuments, 'document.code', 'id');
    }

    public function getScanDocumentMap($workerHasDocuments)
    {
        return ArrayHelper::map($workerHasDocuments, 'document.code', function ($item) {
            if ($item['document']['code'] == Document::PASSPORT) {
                return $item['documentScans'] ?: [];
            }

            return empty($item['documentScans']) ? [(new WorkerDocumentScan())] : $item['documentScans'];
        });
    }

    /**
     * @param int $entityId
     * @param int $documentCode
     * @param null $hasPositionId
     * @return array|ActiveRecord
     * @throws \yii\base\InvalidConfigException
     */
    public function createWorkerHasDocumentByCode($entityId, $documentCode, $hasPositionId = null)
    {
        $documentId = $this->getDocumentIdByCode($documentCode);
        $modelParams = ArrayHelper::merge($this->modelConfig, [
            'worker_id'       => $entityId,
            'document_id'     => $documentId,
            'has_position_id' => $hasPositionId,
        ]);
        /** @var $workerHasDocument ActiveRecord */
        $workerHasDocument = Yii::createObject($modelParams);

        if ($workerHasDocument->save()) {
            return $workerHasDocument;
        }

        return $workerHasDocument->getErrors();
    }

    /**
     * @param int $code Documetn code
     * @return int|null
     */
    private function getDocumentIdByCode($code)
    {
        return DocumentLayer::getDccumentIdByCode($code);
    }
}