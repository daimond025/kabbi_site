<?php


namespace frontend\modules\employee\components\worker;


use app\modules\order\models\forms\FilterMapForm;
use app\modules\tenant\models\User;
use frontend\modules\companies\components\services\CompanyCheck;
use frontend\modules\order\components\services\filter\FilterWorkerMapService;
use Yii;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\parking\models\Parking;
use common\modules\employee\models\worker\WorkerBlock;
use frontend\modules\tenant\models\Currency;
use yii\base\Object;
use yii\helpers\ArrayHelper;

class WorkerShiftService extends Object
{
    /**
     * A configuration array: the array must contain a `class` element which is treated as the object class,
     * and the rest of the name-value pairs will be used to initialize the corresponding object properties
     * @var array
     */
    public $modelConfig;

    public function getEntityClass()
    {
        return $this->modelConfig['class'];
    }

    /**
     * Statistic searching by period time
     * @param integer $worker_id
     * @param array $arPeriod ['first_timestamp', 'last_timestamp']
     * @param array $positionIdFilter
     * @return array
     */
    public function getShiftsByPeriod($worker_id, $arPeriod, array $positionIdFilter = [])
    {
        list($first_date, $last_date) = $arPeriod;

        return $this->getShiftData($worker_id, $first_date, $last_date, $positionIdFilter);
    }

    public function getShifts($worker_id, $first_date, $last_date, array $positionIdFilter = [])
    {
        $entity = $this->getEntityClass();

        return $entity::find()
            ->with([
                'position' => function ($query) {
                    $query->select(['position_id', 'name']);
                },
                'worker',
                'tariff.class',
                'car'      => function ($query) {
                    $query->select(['car_id', 'name', 'gos_number']);
                },
            ])
            ->where(['worker_id' => $worker_id])
            ->andFilterWhere(['position_id' => $positionIdFilter])
            ->andWhere(['between', 'start_work', $first_date, $last_date])
            ->orderBy(['end_work' => SORT_DESC])
            ->all();
    }

    private function getCurrencySymbol($currencyId)
    {
        return Currency::getCurrencySymbol($currencyId);
    }

    public function getActiveShifts(array $workerShifts, array $workerOrders = [])
    {
        $activeShifts = [];
        //Сортировка заказов по сменам
        foreach ($workerShifts as $index => $shift) {
            $shift->ordersSummaryCost = [];
            foreach ($workerOrders as $key => $order) {
                if ($order->status_time >= $shift->start_work
                    && (!empty($shift->end_work) && $order->status_time <= $shift->end_work)
                ) {
                    if ($order->status_id == OrderStatus::STATUS_COMPLETED_PAID) {
                        $shift->compleatedOrdersCnt++;
                        $shift->ordersSummaryCost[$this->getCurrencySymbol($order->currency_id)]
                            += $order->detailCost->summary_cost;
                    } else {
                        $shift->rejectedOrdersCnt++;
                    }
                    //Найденный элемент с заказом удаляем из массива, чтобы умеьшать кол-во итераций для следующих смен
                    unset($workerOrders[$key]);
                }
            }
            if (empty($shift->end_work)) {
                $activeShifts[] = $shift;
                unset($workerShifts[$index]);
            }
        }

        return $activeShifts;
    }

    /**
     * Getting data of the shift.
     * @param integer $worker_id
     * @param integer $first_date
     * @param integer $last_date
     * @param array $positionIdFilter
     * @return array
     */
    public function getShiftData($worker_id, $first_date, $last_date, array $positionIdFilter = [])
    {
        $workerShifts = $this->getShifts($worker_id, $first_date, $last_date, $positionIdFilter);
        $workerOrders = $this->getShiftOrders($worker_id, $first_date, $last_date, $positionIdFilter);
        $activeShifts = $this->getActiveShifts($workerShifts, $workerOrders);
;
        return [
            'activeShifts' => $activeShifts,
            'workerShifts' => $workerShifts,
        ];
    }

    /**
     * Getting orders by shift period
     * @param integer $worker_id
     * @param integer $first_date
     * @param integer $last_date
     * @param array $positionIdFilter
     * @return Order[]
     */
    public function getShiftOrders($worker_id, $first_date, $last_date, array $positionIdFilter = [])
    {
        return Order::find()
            ->where(['between', 'status_time', $first_date, $last_date])
            ->andWhere([
                'status_id' => ArrayHelper::merge([OrderStatus::STATUS_COMPLETED_PAID], OrderStatus::getRejectedStatusId()),
                'worker_id' => $worker_id,
            ])
            ->andFilterWhere(['position_id' => $positionIdFilter])
            ->with([
                'detailCost' => function ($query) {
                    $query->select(['order_id', 'summary_cost']);
                },
            ])
            ->select([Order::tableName() . '.order_id', 'status_id', 'worker_id', 'status_time', 'currency_id'])
            ->all();
    }

    /**
     * Block time list
     * @param int $shiftId PK of tbl_worker_shift
     * @return string array
     */
    public function getBlockList($shiftId)
    {
        return WorkerBlock::find()
            ->select(['block_id', 'start_block', 'end_block', 'type_block', 'is_unblocked'])
            ->andWhere(['shift_id' => $shiftId])
            ->orderBy('start_block')
            ->asArray()
            ->all();
    }

    /**
     * Data of online workers from redis
     * @return array
     */
    public function getOnlineWorkers()
    {
        return Yii::$app->redis_workers->executeCommand('hvals', [$this->getUserTenantId()]);
    }

    private function getParkingsByCity($cities)
    {
        return Parking::find()
            ->where(['tenant_id' => $this->getUserTenantId()])
            ->andWhere(['not in', 'type', ['basePolygon', 'reseptionArea']])
            ->andWhere(['city_id' => $cities])
            ->select(['parking_id', 'name'])
            ->all();
    }

    private function getUserCityKeys()
    {
        return array_keys(user()->getUserCityList());
    }

    private function getUserTenantId()
    {
        return user()->tenant_id;
    }

    /**
     * Getting active workers from redis.
     * @param integer $city_id
     * @return array Count of workers grouped by status.
     */
    public function getCntWorkersOnShift($city_id = null)
    {
        $db_workers = $this->getOnlineWorkers();

        $dataFilter = (new CompanyCheck())->workWithFilterMap([]);
        $db_workers = (new FilterWorkerMapService($db_workers, new FilterMapForm($dataFilter)))->getFilterWorkers();

        $arCnt = [
            'free'     => 0,
            'busy'     => 0,
            'parkings' => [
                'free' => [
                    'no_parking' => [
                        'name' => t('employee', 'Not tied to a parking'),
                        'cnt'  => 0,
                    ],
                ],
                'busy' => [
                    'no_parking' => [
                        'name' => t('employee', 'Not tied to a parking'),
                        'cnt'  => 0,
                    ],
                ],
            ],
        ];

        $cities = empty($city_id) ? $this->getUserCityKeys() : [$city_id];

        $parkings = $this->getParkingsByCity($cities);

        $arParking = [];
        foreach ($parkings as $parking) {
            $arParking[$parking->parking_id] = [
                'name' => $parking->name,
                'cnt'  => 0,
            ];
        }

        $arCnt['parkings']['free'] = ArrayHelper::merge($arParking, $arCnt['parkings']['free']);
        $arCnt['parkings']['busy'] = ArrayHelper::merge($arParking, $arCnt['parkings']['busy']);

        foreach ($db_workers as $cab) {
            $cab = unserialize($cab);

            if (!in_array($cab['worker']['city_id'],
                    $cities) || (!empty($city_id) && $cab['worker']['city_id'] != $city_id)
            ) {
                continue;
            }

            $status = $cab['worker']['status'] == 'FREE' ? 'free' : 'busy';

            if ($status == 'free') {
                $arCnt['free']++;
            } else {
                $arCnt['busy']++;
            }

            if (!empty($cab['geo']['parking_id']) && isset($arCnt['parkings'][$status][$cab['geo']['parking_id']])) {
                $arCnt['parkings'][$status][$cab['geo']['parking_id']]['cnt']++;
            } else {
                $arCnt['parkings'][$status]['no_parking']['cnt']++;
            }
        }

        return $arCnt;
    }

    public function getCntWorkersOnParkings($status, $city_id = null)
    {
        $db_workers = $this->getOnlineWorkers();

        $cities = empty($city_id) ? $this->getUserCityKeys() : [$city_id];
        $parkings = $this->getParkingsByCity($cities);

        $arCnt = [];
        foreach ($parkings as $parking) {
            $arCnt[$parking->parking_id] = [
                'name' => $parking->name,
                'cnt'  => 0,
            ];
        }

        foreach ($db_workers as $cab) {
            $cab = unserialize($cab);
            $worker_status = $cab['worker']['status'] == 'FREE' ? 'free' : 'busy';

            if (!in_array($cab['worker']['city_id'], $cities)
                || (!empty($city_id) && $cab['worker']['city_id'] != $city_id)
                || $status != $worker_status
            ) {
                continue;
            }

            if (!empty($cab['geo']['parking_id']) && isset($arCnt[$cab['geo']['parking_id']])) {
                $arCnt[$cab['geo']['parking_id']]['cnt']++;
            } else {
                if (!isset($arCnt['no_parking'])) {
                    $no_parking = [
                        'no_parking' => [
                            'name' => t('employee', 'Not tied to a parking'),
                            'cnt'  => 0,
                        ],
                    ];

                    $arCnt = $no_parking + $arCnt;
                }

                $arCnt['no_parking']['cnt']++;
            }
        }

        return $arCnt;
    }

    /**
     * Getting worker on shift by callsign form redis
     * @param integer $callsign
     * @return array|false
     */
    public function getActiveWorker($callsign)
    {
        return unserialize(Yii::$app->redis_workers->executeCommand('hget', [$this->getUserTenantId(), $callsign]));
    }

    /**
     * Getting current worker coords
     * @param integer $callsign
     * @return array
     */
    public function getWorkerLocation($callsign)
    {
        $data = [];
        $activeWorker = $this->getActiveWorker($callsign);

        if ($activeWorker !== false) {
            $data = [
                'lat'       => $activeWorker['geo']['lat'],
                'lon'       => $activeWorker['geo']['lon'],
                'speed'     => $activeWorker['geo']['speed'],
                'iconAngle' => $activeWorker['geo']['degree'],
            ];
        }

        return $data;
    }

    /**
     * Getting external worker coords
     * @param $externalCar
     * @return array
     */
    public function getExternalWorkerLocation($externalCar)
    {
        $data = [
            'lat'       => isset($externalCar['lat']) ? $externalCar['lat'] : null,
            'lon'       => isset($externalCar['lon']) ? $externalCar['lon'] : null,
            'speed'     => 0,
            'iconAngle' => null,
        ];
        return $data;
    }

    /**
     * Count workers of current tenant on shift
     * @return integer
     */
    public function getQuantityWorkersOnShift()
    {
        $cityList = User::getCurUserCityIds();
        $workers = $this->getOnlineWorkers();

        $filterData = (new CompanyCheck())->workWithFilterMap();
        $workers = (new FilterWorkerMapService($workers, new FilterMapForm($filterData)))->getFilterWorkers();

        $count = 0;

        foreach ($workers as $cab) {
            $worker = unserialize($cab);

            if (!in_array($worker['worker']['city_id'], $cityList)) {
                continue;
            }

            $count++;
        }

        return $count;
    }

    /**
     * Return workers of current tenant on shift
     * @return array
     */
    public function getWorkersOnShiftJson()
    {
        $cityList = User::getCurUserCityIds();
        $workers = $this->getOnlineWorkers();

        $workersList = [];
        foreach ($workers as $key => $cab) {
            $worker = unserialize($cab);
            if (in_array($worker['worker']['city_id'], $cityList)) {
                $workerTmp['worker_id'] = $worker['worker']['worker_id'];
                $workerTmp['fio'] = $worker['worker']['last_name'] . " " . $worker['worker']['name'] . " " . $worker['worker']['second_name'];
                $workerTmp['phone'] = $worker['worker']['phone'];
                $workerTmp['callsign'] = $worker['worker']['callsign'];
                $car = [];
                $car['brand'] = $worker['car']['brand'];
                $car['model'] = $worker['car']['model'];
                $car['gos_number'] = $worker['car']['gos_number'];
                $car['color'] = $worker['car']['color'];
                $workerTmp['car'] = $car;
                $workersList[] = $workerTmp;
            }
        }

        return $workersList;
    }

}