<?php

namespace frontend\modules\notifications\models;

use frontend\modules\car\models\CarClass;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%notification_has_class}}".
 * @package app\modules\notifications\model
 *
 * @property int $id
 * @property int $notification_id
 * @property int $class_id
 */
class NotificationHasClass extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%notification_has_class}}';
    }
    public function rules()
    {
        return [
            [['class_id', 'notification_id'], 'required'],
            [
                ['notification_id'],
                'exist',
                'targetClass' => Notification::className(),
            ],
            [
                ['class_id'],
                'exist',
                'targetClass' => CarClass::className(),
            ],
        ];
    }

}