<?php

namespace frontend\modules\notifications\models;

use common\modules\city\models\City;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%notification_has_city}}".
 * @package app\modules\notifications\model
 *
 * @property int $id
 * @property int $notification_id
 * @property int $city_id
 */
class NotificationHasCity extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%notification_has_city}}';
    }

    public function rules()
    {
        return [
            [['city_id', 'notification_id'], 'required'],
            [
                ['notification_id'],
                'exist',
                'targetClass' => Notification::className(),
            ],
            [
                ['city_id'],
                'exist',
                'targetClass' => City::className(),
            ],
        ];
    }
}