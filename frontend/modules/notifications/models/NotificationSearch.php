<?php

namespace frontend\modules\notifications\models;

use yii\data\ActiveDataProvider;

class NotificationSearch extends Notification
{
    const BLOCKED = 1;
    const UNBLOCKED = 0;

    public static function search($blocked=self::UNBLOCKED)
    {
        $dataProvider = new ActiveDataProvider([
            'query'      => Notification::find()
                ->where([
                    'tenant_id' => user()->tenant_id,
                    'blocked'   => $blocked
                ]),
            'sort'       => false,
            'pagination' => false,
        ]);

        return $dataProvider;
    }
}