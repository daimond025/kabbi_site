<?php

namespace frontend\modules\notifications\models;

use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%notification}}".
 * @package app\modules\notifications\model
 *
 * @property int                       $notification_id
 * @property int                       $tenant_id
 * @property string                    $name
 * @property string                    $addressee
 * @property int                       $addressee_id
 * @property string                    $title
 * @property string                    $text
 * @property string                    $device
 * @property int                       $on_shift
 * @property int                       $type_notice
 * @property NotificationHasCity[]     $hasCity
 * @property NotificationHasPosition[] $hasPosition
 * @property NotificationHasClass[]    $hasClass
 *
 * @property array                     $cityList
 * @property array                     $positionList
 * @property array                     $classList
 * @property string                    $deviceList
 */
class Notification extends ActiveRecord
{

    public $cityList = [];
    public $positionList = [];
    public $classList = [];
    public $deviceList = [];

    protected $old_cityList = [];
    protected $old_positionList = [];
    protected $old_classList = [];
    protected $old_deviceList = [];

    const TYPE_WORKER = 'WORKER';
    const TYPE_CLIENT = 'CLIENT';

    const TYPE_NOTICE_SMS = 1;
    const TYPE_NOTICE_PUSH = 2;



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%notification}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['blocked', 'boolean'],
            [['tenant_id', 'name', 'addressee', 'title', 'text', 'cityList', 'type_notice'], 'required'],
            [['tenant_id', 'addressee_id', 'type_notice'], 'integer'],
            [['name', 'title'], 'string', 'max' => 100],
            [['text'], 'string', 'max' => 200],
            [
                ['on_shift'],
                'in',
                'range' => [0, 1, 2]],

            [
                ['positionList'],
                'required',
                'when'       => function ($model) {
                    return($model->addressee === self::TYPE_WORKER);
                },
                'whenClient' => 'function(attr,value){
                    var addressee = $("#notification-addressee").val();
                    return addressee === "' . self::TYPE_WORKER . '";
                }',
            ],
            [
                ['classList'],
                'required',
                'when'       => function ($model) {
                    return ($model->addressee === self::TYPE_WORKER);
                },
                'whenClient' => 'function(attr,value){
                    var classList = $("#notification-addressee").val();
                    return classList === "' . self::TYPE_WORKER . '";
                }',
            ],
            [
                ['deviceList'],
                'required',
                'when'       => function ($model) {
                    return (int)$model->type_notice === (int)self::TYPE_NOTICE_PUSH ;
                },
                'whenClient' => 'function(attr,value){
                    var deviceList = $("#notification-type_notice").val();
                    return deviceList === "' . self::TYPE_NOTICE_PUSH . '";
                }',
            ],
            [
                ['deviceList'],
                'in',
                'range'      => array_keys($this->deviceList()),
                'allowArray' => 'true',
                'when'       => function ($model) {
                    return (int)$model->type_notice === (int)self::TYPE_NOTICE_PUSH ;
                },
                'whenClient' => 'function(attr,value){
                    var addressee = $("#notification-devicelist").val();
                    return addressee === "' . self::TYPE_CLIENT . '";
                }',
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name'         => t('notification', 'Name'),
            'addressee'    => t('notification', 'Addressee'),
            'cityList'     => t('app', 'Cities'),
            'addressee_id' => t('notification', 'Callsign worker or phone client'),
            'deviceList'   => t('notification', 'Device'),
            'positionList' => t('employee', 'Profession'),
            'classList'    => t('car', 'Type of car'),
            'title'        => t('notification', 'Title'),
            'text'         => t('notification', 'Text'),
            'on_shift'     => t('notification', 'Shift'),
            'blocked'      => t('notification', 'Blocked'),
            'type_notice'      => t('notification', 'type_notice'),
            'delete'      => t('notification', 'Delete'),
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHasCity()
    {
        return $this->hasMany(NotificationHasCity::className(), ['notification_id' => 'notification_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHasPosition()
    {
        return $this->hasMany(NotificationHasPosition::className(), ['notification_id' => 'notification_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHasClass()
    {
        return $this->hasMany(NotificationHasClass::className(), ['notification_id' => 'notification_id']);
    }

    public function afterFind()
    {

        $this->old_classList    = $this->classList = ArrayHelper::getColumn($this->hasClass, 'class_id');
        $this->old_cityList     = $this->cityList = ArrayHelper::getColumn($this->hasCity, 'city_id');
        $this->old_positionList = $this->positionList = ArrayHelper::getColumn($this->hasPosition, 'position_id');

        $devices =   explode(',', $this->device);
        if(is_array($devices) && (count($devices) > 0 )){
            $this->deviceList = explode(',', $this->device);
        }
        parent::afterFind();
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            $this->positionList = is_array($this->positionList) ? $this->positionList : [];
            $this->classList    = is_array($this->classList) ? $this->classList : [];
            $this->cityList     = is_array($this->cityList) ? $this->cityList : [];

            if ($this->addressee === self::TYPE_CLIENT) {
                if(!empty(($this->deviceList))){
                    $this->device = implode(',', $this->deviceList);
                }else{
                    $this->device = '';
                }

            }

            return true;
        }

        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($this->addressee === self::TYPE_WORKER) {
            $this->savePosition();
            $this->saveClass();
        }
        $this->saveCity();

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * Save position
     * @return bool
     * @throws Exception
     * @throws \Exception
     * @throws \yii\db\StaleObjectException
     */
    public function savePosition()
    {
        $change = $this->changePosition();
        foreach ($change['add'] as $positionId) {
            $model = new NotificationHasPosition([
                'notification_id' => $this->notification_id,
                'position_id'     => $positionId,
            ]);
            if (!$model->save()) {
                throw new Exception('failed to save NotificationHasPosition');
            }
        }

        if (!empty($change['delete'])) {
            $model = NotificationHasPosition::find()
                ->where([
                    'notification_id' => $this->notification_id,
                    'position_id'     => $change['delete'],
                ])
                ->all();
            if ($model) {
                foreach ($model as $m) {
                    if (!$m->delete()) {
                        throw new Exception('failed to delete NotificationHasPosition');
                    }
                }
            }
        }

        return true;
    }

    public function saveClass()
    {
        $change = $this->changeClass();
        foreach ($change['add'] as $classId) {
            $model = new NotificationHasClass([
                'notification_id' => $this->notification_id,
                'class_id'        => $classId,
            ]);
            if (!$model->save()) {
                throw new Exception('failed to save NotificationHasPosition');
            }
        }

        if (!empty($change['delete'])) {
            $model = NotificationHasClass::find()
                ->where([
                    'notification_id' => $this->notification_id,
                    'class_id'        => $change['delete'],
                ])
                ->all();
            if ($model) {
                foreach ($model as $m) {
                    if (!$m->delete()) {
                        throw new Exception('failed to delete NotificationHasPosition');
                    }
                }
            }
        }

        return true;
    }

    public function saveCity()
    {
        $change = $this->changeCity();
        foreach ($change['add'] as $cityId) {
            $model = new NotificationHasCity([
                'notification_id' => $this->notification_id,
                'city_id'         => $cityId,
            ]);
            if (!$model->save()) {
                throw new Exception('failed to save NotificationHasPosition');
            }
        }

        if (!empty($change['delete'])) {
            $model = NotificationHasCity::find()
                ->where([
                    'notification_id' => $this->notification_id,
                    'city_id'         => $change['delete'],
                ])
                ->all();
            if ($model) {
                foreach ($model as $m) {
                    if (!$m->delete()) {
                        throw new Exception('failed to delete NotificationHasPosition');
                    }
                }
            }
        }

        return true;
    }

    protected function changePosition()
    {
        return [
            'add'    => array_diff($this->positionList, $this->old_positionList),
            'delete' => array_diff($this->old_positionList, $this->positionList),
        ];
    }

    protected function changeCity()
    {
        return [
            'add'    => array_diff($this->cityList, $this->old_cityList),
            'delete' => array_diff($this->old_cityList, $this->cityList),
        ];
    }

    protected function changeClass()
    {
        return [
            'add'    => array_diff($this->classList, $this->old_classList),
            'delete' => array_diff($this->old_classList, $this->classList),
        ];
    }

    public function addresseeList()
    {
        return [
            self::TYPE_CLIENT => t('app', 'Client'),
            self::TYPE_WORKER => t('app', 'Worker'),
        ];
    }

    public function addTypeNotificationList()
    {
        return [
            self::TYPE_NOTICE_SMS => t('app', 'Sms'),
            self::TYPE_NOTICE_PUSH => t('app', 'Push'),
        ];
    }

    public function getAddresseeName()
    {
        $arr = $this->addresseeList();

        return (string)$arr[$this->addressee];
    }

    public function deviceList()
    {
        return [
            '1' => 'android',
            '2' => 'ios',
        ];
    }

    public function onShiftList()
    {
        return [
            0 => t('notification', 'All'),
            1 => t('notification', 'On shift'),
            2 => t('notification', 'Not on shift'),
        ];
    }

}