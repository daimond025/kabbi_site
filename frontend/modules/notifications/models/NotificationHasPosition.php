<?php

namespace frontend\modules\notifications\models;

use common\modules\employee\models\position\Position;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%notification_has_position}}".
 * @package app\modules\notifications\model
 *
 * @property int $id
 * @property int $notification_id
 * @property int $position_id
 */
class NotificationHasPosition extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%notification_has_position}}';
    }

    public function rules()
    {
        return [
            [['position_id', 'notification_id'], 'required'],
            [
                ['notification_id'],
                'exist',
                'targetClass' => Notification::className(),
            ],
            [
                ['position_id'],
                'exist',
                'targetClass' => Position::className(),
            ],
        ];
    }
}