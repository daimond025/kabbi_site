<?php

namespace frontend\modules\notifications\controllers;

use app\modules\client\models\Client;
use app\modules\client\models\ClientPhone;
use frontend\modules\car\models\CarClass;
use frontend\modules\employee\components\position\PositionService;
use frontend\modules\notifications\components\BulkNotify;
use frontend\modules\notifications\models\Notification;
use frontend\components\behavior\CityListBehavior;
use frontend\modules\notifications\models\NotificationSearch;
use function GuzzleHttp\Promise\all;
use yii\base\Module;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii;

class PushController extends Controller
{
    private $positionService;

    public function behaviors()
    {
        return [
            'access'   => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['notifications'],
                    ],
                    [
                        'actions' => ['mass-mailing', 'update-mass-mailing'],
                        'allow'   => true,
                        'roles'   => ['read_notifications'],
                    ],
                ],
            ],
            'cityList' => [
                'class' => CityListBehavior::className(),
            ],
        ];
    }

    public function __construct($id, Module $module, PositionService $positionService, array $config = [])
    {
        $this->positionService = $positionService;
        $this->positionService->setTenantId(user()->tenant_id);

        parent::__construct($id, $module, $config);
    }

    private function getDataMassMailing($blocked = NotificationSearch::UNBLOCKED)
    {
        $dataProvider = NotificationSearch::search($blocked);

        $cityList = $this->getUserCityList();

        return compact('dataProvider', 'cityList');
    }

    public function actionMassMailing()
    {

        $data = $this->getDataMassMailing();

        return $this->render('index', ['dataProvider' => $data['dataProvider'], 'cityList' => $data['cityList']]);

    }

    public function actionMassMailingBlocked()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $data = $this->getDataMassMailing(NotificationSearch::BLOCKED);

        return $this->renderAjax('grid', ['dataProvider' => $data['dataProvider'], 'cityList' => $data['cityList']]);

    }

    /**
     * @return array
     */
    private function getPositionMap()
    {
        $positions = $this->positionService->getAllPositions();

        return empty($positions) ? [] : ArrayHelper::map($positions, 'position_id', 'name');
    }

    /**
     * Редактирование уведомления
     *
     * @param $id
     *
     * @return string
     * @throws yii\web\NotFoundHttpException
     */
    public function actionUpdateMassMailing($id)
    {
        $model = Notification::find()
            ->where([
                'tenant_id'       => user()->tenant_id,
                'notification_id' => $id,
            ])
            ->limit(1)
            ->one();

        if (!$model) {
            throw new yii\web\NotFoundHttpException(t('app', 'The requested page does not exist.'));
        }

        $cityList     = $this->getUserCityList();
        $positionList = $this->getPositionMap();
        $carClass     = CarClass::find()->all();

        if ($model->load(post())) {
            try {
                $model->save();

                if (post('execute')) {
                    // SMS рассылка
                    if($model->type_notice == Notification::TYPE_NOTICE_SMS ){
                        list($sucess, $testMessages) = $this->executeSms($model, $model->text);
                        session()->setFlash('success', t('app', $testMessages));
                    }
                    elseif($model->type_notice == Notification::TYPE_NOTICE_PUSH ){
                        $response = $this->execute($model);
                        if ($response['status'] === 'error') {
                            switch ($response['message']) {
                                case 'Empty value':
                                    session()->setFlash('error',
                                        t('notification', 'Empty the list of recipients to be sent'));
                                    break;
                                case 'No connection':
                                    session()->setFlash('error', t('notification', 'Service is temporarily unavailable'));
                                    break;
                                default:
                                    session()->setFlash('error', $response['message']);
                            }
                        } else {
                            session()->setFlash('success', t('app', 'Data send PUSH successfully'));
                        }
                    }
                } else {
                    session()->setFlash('success', t('app', 'Data updated successfully'));
                    $this->redirect(['mass-mailing']);
                }

            } catch (\Exception $e) {
                Yii::error($e);
                session()->setFlash('error', t('status_event', 'Error saving. Notify to administrator, please.'));
            }
        }

        $classList = ArrayHelper::map($carClass, 'class_id', function ($item) {
            return t('car', $item['class']);
        });

        return $this->render('update', compact('model', 'cityList', 'positionList', 'classList'));
    }
    public function actionDeleteMassMailing($id)
    {
        $model = Notification::find()
            ->where([
                'tenant_id'       => user()->tenant_id,
                'notification_id' => $id,
            ])
            ->limit(1)
            ->one();

        if ($model->delete()){
            session()->setFlash('success', t('app', 'Delete was successfully!'));
        }
        else{
            session()->setFlash('error', t('status_event', 'Error saving. Notify to administrator, please.'));
        }
        $this->redirect(['mass-mailing']);

    }

    public function actionCreateMassMailing()
    {
        $model = new Notification([
            'tenant_id' => user()->tenant_id,
        ]);

        $cityList     = $this->getUserCityList();
        $positionList = $this->getPositionMap();
        $carClass     = CarClass::find()->all();

        if ( $model->load(post()) ) {
            $model->validate();
            try {
                $model->save();

                if (post('execute')) {
                    // SMS рассылка
                    if($model->type_notice == Notification::TYPE_NOTICE_SMS ){
                        list($sucess, $testMessages) = $this->executeSms($model, $model->text);
                        session()->setFlash('success', t('app', $testMessages));
                    }
                    elseif($model->type_notice == Notification::TYPE_NOTICE_PUSH ){
                        $response = $this->execute($model);
                        if ($response['status'] === 'error') {
                            switch ($response['message']) {
                                case 'Empty value':
                                    session()->setFlash('error',
                                        t('notification', 'Empty the list of recipients to be sent'));
                                    break;
                                case 'No connection':
                                    session()->setFlash('error', t('notification', 'Service is temporarily unavailable'));
                                    break;
                                default:
                                    session()->setFlash('error', $response['message']);
                            }
                        } else {
                            session()->setFlash('success', t('app', 'Data send PUSH successfully'));
                        }
                    }
                } else {
                    session()->setFlash('success', t('app', 'Data updated successfully'));
                    $this->redirect(['mass-mailing']);
                }
            } catch (\Exception $e) {
                session()->setFlash('error', t('status_event', 'Error saving. Notify to administrator, please.'));
            }
        }

        $classList = ArrayHelper::map($carClass, 'class_id', function ($item) {
            return t('car', $item['class']);
        });


        return $this->render('create', compact('model', 'cityList', 'positionList', 'classList'));
    }

    public function actionReadMassMailing($id)
    {
        Yii::$app->response->format = yii\web\Response::FORMAT_JSON;

        $model = Notification::find()
            ->where([
                'tenant_id'       => user()->tenant_id,
                'notification_id' => $id,
            ])
            ->limit(1)
            ->one();

        if (!$model) {
            return false;
        }

        $notify = new BulkNotify($model);

        return [
            'tenant_id'   => $notify->getTenantId(),
            'client_type' => $notify->getClientType(),
            'client_ids'  => $notify->getClientIds(),
            'title'       => $notify->getTitle(),
            'message'     => $notify->getText(),
        ];
    }

    public function findModel()
    {
        $model = Notification::find()
            ->where([
                'tenant_id' => user()->tenant_id,
            ])
            ->all();

        return $model;
    }

    public function execute(Notification $model)
    {

        $bulk = new BulkNotify($model);

        return app()->pushApi->bulkNotify($bulk);
    }

    // отправка смс сообщений
    public function executeSms(Notification $model, $text)
    {
        $bulk = new BulkNotify($model);
        $phones =  $bulk->getClientWorkerPhone();


        if(count($phones) > 0){
            $serviceApi = app()->get('serviceApi');
            $clients['text'] =$text;
            $clients['phone'] =$phones;
            $clients['tenant_id'] =user()->tenant_id;
            $rez_send =  $serviceApi->sendAllSms($clients);
            return [$rez_send, 'Success sent Sms!'];
        }else{
            return [true, 'There isn`t sent Sms!'];
        }



    }

}