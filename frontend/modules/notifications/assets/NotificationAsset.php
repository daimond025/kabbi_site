<?php


namespace frontend\modules\notifications\assets;

use yii\web\AssetBundle;

class NotificationAsset extends AssetBundle
{


    public function init()
    {
        $this->sourcePath     = '@app/modules/notifications/assets/js';
        $this->publishOptions = [
            'forceCopy' => true,
        ];
        $this->js             = [
            'functions.js',
            'main.js',
        ];
        $this->css = [];

        parent::init();
    }

}
