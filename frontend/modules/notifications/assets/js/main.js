$(function () {

    $(document).on('click', '.js-spoiler', function() {
        var id = $(this).data('id');
        $('.js-form-' + id).toggle();
    });

    $(document).on('click', '.js-button-save', function() {
        var _this = $(this);
        var _form = $(this).parents('form').first();
        var id = $(this).parents('.js-spoiler'). first().data('id');
        $.ajax({
            type: 'POST',
            url: 'save-mass-mailing/' + _this.data('id'),
            data: _form.serialize(),
            success: function (result) {
                console.log(result);
            }
        });
    });


    $(document).on('change', '.js-addressee', function() {
        var _this = $(this);
        var _form = $(this).parents('form').first();

        var type_notice = $('.js-type_notice').val();


        if (_this.val() === 'CLIENT') {
            _form.find('.js-position').parents('section').first().hide();
            _form.find('.js-class').parents('section').first().hide();
            _form.find('.js-on_shift').parents('section').first().hide();
        } else{
            _form.find('.js-position').parents('section').first().show();
            _form.find('.js-class').parents('section').first().show();
            _form.find('.js-on_shift').parents('section').first().show();
        }

        if(type_notice == 1){
            _form.find('.js-device').parents('section').first().hide();
        }else {
            _form.find('.js-device').parents('section').first().show();
        }



    });

    $(document).on('change', '.js-type_notice', function() {
        var _this = $(this);
        var _form = $(this).parents('form').first();

        if(_this.val() == 1) {
            _form.find('.js-device').parents('section').first().hide();
        }
        if(_this.val() == 2){
            _form.find('.js-device').parents('section').first().show();
        }
    });
});