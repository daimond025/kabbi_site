<?php

namespace frontend\modules\notifications\components;

interface BulkNotifyInterface
{
    public function getNotificationId();

    public function getClientType();

    public function getTenantId();

    public function getClientIds();

    public function getCityIds();

    public function getTitle();

    public function getText();
}