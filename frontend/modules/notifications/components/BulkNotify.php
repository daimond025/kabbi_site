<?php

namespace frontend\modules\notifications\components;

use app\modules\client\models\Client;
use frontend\modules\employee\models\worker\Worker;
use frontend\modules\notifications\models\Notification;
use yii\base\Object;
use yii\helpers\ArrayHelper;

class BulkNotify extends Object implements BulkNotifyInterface
{

    private $model;

    public function __construct(Notification $model)
    {
        $this->model = $model;
    }

    /**
     * Get notification id
     * @return int
     */
    public function getNotificationId()
    {
        return $this->model->notification_id;
    }

    /**
     * Get tenant id
     * @return string
     */
    public function getTenantId()
    {
        return user()->tenant_id;
    }

    /**
     * Get notification city ids as string
     * @return string
     */
    public function getCityIds()
    {
        return implode(",", $this->model->cityList);
    }

    /**
     * Get client type
     * @return string (client/worker)
     */
    public function getClientType()
    {
        switch ($this->model->addressee) {
            case Notification::TYPE_CLIENT :
                return 'client';
            case Notification::TYPE_WORKER :
                return 'worker';
        }

        return '';
    }

    /**
     * Get notification clients ids as string
     * @return string
     */
    public function getClientIds()
    {
        $ids = [];

        switch ($this->model->addressee) {
            case Notification::TYPE_CLIENT :
                $ids = $this->getClientIdsByClient();
                break;
            case Notification::TYPE_WORKER :
                $ids = $this->getClientIdsByWorker();
                break;
        }

        return implode(',', $ids);
    }

    //TODO  получение телефона
    public function getClientWorkerPhone()
    {
        $phones = [];

        switch ($this->model->addressee) {
            case Notification::TYPE_CLIENT :
                $phones = $this->getClientByPhones();
                break;
            case Notification::TYPE_WORKER :
                $phones = $this->getWorkerByPhones();
                break;
        }

        // фильтрация
        $phonesSent = [];
        foreach ($phones as $phone){
            if(strlen($phone['phone']) > 9){
                $phonesSent[] =$phone;
            }

        }

        return $phonesSent;
    }
    public function getClientByPhones()
    {
        $query = Client::find()
            ->select('cl.client_id, cl.city_id')
            ->alias('cl')
            ->where([
                'cl.tenant_id'  => $this->getTenantId(),
                'cl.black_list' => '0',
                'cl.city_id'    => $this->model->cityList,
            ]);
        if (!empty($this->model->addressee_id)) {
            $query->joinWith([
                'clientPhones' => function ($query) {
                    $query->andWhere(['value' => $this->model->addressee_id]);
                },
            ], false)->limit(1);
        }

        $model = $query->all();


        $phones = [];
        foreach ($model as $client){
            if(!empty($client->clientPhone->value)){
                $phonesCity['phone'] = $client->clientPhone->value;
                $phonesCity['city'] = $client->city_id;
                $phones[] = $phonesCity;
            }
        }
        if (!$model) {
            return [];
        }
        return $phones;
    }
    public function getWorkerByPhones()
    {

        $query = Worker::find()
            ->alias('w')
            ->joinWith(['workerHasCities wc'])
            ->where([
                'w.tenant_id' => $this->getTenantId(),
                'w.block'     => '0',
                'wc.city_id'  => $this->model->cityList,
                'position_id' => $this->model->positionList,
            ])
            ->andWhere(['not', ['w.phone' => null]])
            ->andFilterWhere([
                'car.class_id' => $this->model->classList,
            ])
            ->joinWith([
                'workerHasPositions.workerHasCars.car car',
            ], false);

        if (!empty($this->model->addressee_id)) {
            $query->andWhere(['w.callsign' => $this->model->addressee_id])->limit(1);
        }

        if ($this->model->on_shift == 1) {
            $query->andWhere([
                'w.worker_id' => Worker::getOnlineWorkersId(),
            ]);
        } else {
            if ($this->model->on_shift == 2) {
                $query->andWhere([
                    'not',
                    [
                        'w.worker_id' => Worker::getOnlineWorkersId(),
                    ],
                ]);
            }
        }

        $phones = [];
        $models = $query->all();
        foreach ($models as $model){
            if(isset($model->cities[0])){
                $phonesCity['phone'] = $model->phone;
                $phonesCity['city'] = $model->cities[0]->city_id;
                $phones[] = $phonesCity;
            }
        }

        return $phones;

    }


    /**
     * get title
     * @return string
     */
    public function getTitle()
    {
        return $this->model->title;
    }

    /**
     * get message
     * @return string
     */
    public function getText()
    {
        return $this->model->text;
    }

    /**
     * Get device list
     * @return array
     */
    public function getDeviceList()
    {
        return array_map(function ($item) {
            switch ($item) {
                case '1':
                    return 'ANDROID';
                case '2':
                    return 'IOS';
            }

            return $item;
        }, $this->model->deviceList);
    }

    public function getClientIdsByClient()
    {
        $query = Client::find()
            ->select('cl.client_id')
            ->alias('cl')
            ->where([
                'cl.tenant_id'  => $this->getTenantId(),
                'cl.black_list' => '0',
                'cl.device'     => $this->getDeviceList(),
                'cl.city_id'    => $this->model->cityList,
            ])
            ->andWhere(['not', ['cl.device_token' => null]]);
        if (!empty($this->model->addressee_id)) {
            $query->joinWith([
                'clientPhones' => function ($query) {
                    $query->andWhere(['value' => $this->model->addressee_id]);
                },
            ], false)->limit(1);
        }

        $model = $query->all();

        if (!$model) {
            return [];
        }
        return ArrayHelper::getColumn($model, 'client_id');
    }

    public function getClientIdsByWorker()
    {
        $query = Worker::find()
            ->alias('w')
            ->joinWith(['workerHasCities wc'])
            ->where([
                'w.tenant_id' => $this->getTenantId(),
                'w.block'     => '0',
                'wc.city_id'  => $this->model->cityList,
                'position_id' => $this->model->positionList,
                'w.device'    => 'ANDROID'
            ])
            ->andWhere(['not', ['w.device_token' => null]])
            ->andFilterWhere([
                'car.class_id' => $this->model->classList,
            ])
            ->joinWith([
                'workerHasPositions.workerHasCars.car car',
            ], false);

        if (!empty($this->model->addressee_id)) {
            $query->andWhere(['w.callsign' => $this->model->addressee_id])->limit(1);
        }

        if ($this->model->on_shift == 1) {
            $query->andWhere([
                'w.worker_id' => Worker::getOnlineWorkersId(),
            ]);
        } else {
            if ($this->model->on_shift == 2) {
                $query->andWhere([
                    'not',
                    [
                        'w.worker_id' => Worker::getOnlineWorkersId(),
                    ],
                ]);
            }
        }

        $model = $query->all();

        if (!$model) {
            return [];
        }

        return ArrayHelper::getColumn((array)$model, 'worker_id');
    }

}