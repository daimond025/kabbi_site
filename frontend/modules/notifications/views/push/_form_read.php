<?php

/* @var $this yii\web\View */
/* @var $model \frontend\modules\notifications\models\Notification */
/* @var $cityList array */
/* @var $positionList array */
/* @var $classList array */
/* @var $device array */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

?>
<div class="non-editable">

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'name') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($model->name) ? Html::encode($model->name) : '<div class="row_input"></div>' ?></div>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'cityList') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($model->cityList) ? implode(', ',
                    array_map(function ($item) use ($cityList) {
                        return ArrayHelper::getValue($cityList, $item);
                    }, $model->cityList)) : '<div class="row_input"></div>' ?></div>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'addressee') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($model->addressee) ? ArrayHelper::getValue($model->addresseeList(),
                    $model->addressee) : '<div class="row_input"></div>' ?></div>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'addressee_id') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($model->addressee_id) ? Html::encode($model->addressee_id) : t('notification',
                    'All') ?></div>
        </div>
    </section>


    <? if ($model->addressee === \frontend\modules\notifications\models\Notification::TYPE_CLIENT): ?>
        <section class="row">
            <div class="row_label"><?= Html::activeLabel($model, 'deviceList') ?></div>
            <div class="row_input">
                <div class="row_input"><?= !empty($model->deviceList) ? ArrayHelper::getValue($model->deviceList(),
                        $model->deviceList) : '<div class="row_input"></div>' ?></div>
            </div>
        </section>
    <? else: ?>
        <section class="row">
            <div class="row_label"><?= Html::activeLabel($model, 'on_shift') ?></div>
            <div class="row_input">
                <div class="row_input"><?= !empty($model->on_shift) ? ArrayHelper::getValue($model->onShiftList(),
                        $model->on_shift) : '<div class="row_input"></div>' ?></div>
            </div>
        </section>

        <section class="row">
            <div class="row_label"><?= Html::activeLabel($model, 'positionList') ?></div>
            <div class="row_input">
                <div class="row_input"><?= !empty($model->positionList) ? implode(', ',
                        array_map(function ($item) use ($positionList) {
                            return ArrayHelper::getValue($positionList, $item);
                        }, $model->positionList)) : '<div class="row_input"></div>' ?></div>
            </div>
        </section>

        <section class="row">
            <div class="row_label"><?= Html::activeLabel($model, 'classList') ?></div>
            <div class="row_input">
                <div class="row_input"><?= !empty($model->classList) ? implode(', ',
                        array_map(function ($item) use ($classList) {
                            return ArrayHelper::getValue($classList, $item);
                        }, $model->classList)) : '<div class="row_input"></div>' ?></div>
            </div>
        </section>
    <? endif; ?>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'title') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($model->title) ? Html::encode($model->title) : '<div class="row_input"></div>' ?></div>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'text') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($model->text) ? str_replace(PHP_EOL, '<br>',
                    Html::encode($model->text)) : '<div class="row_input"></div>' ?></div>
        </div>
    </section>

</div>