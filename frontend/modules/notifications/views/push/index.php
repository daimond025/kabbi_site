<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $cityList array */

$this->title = t('setting', 'Push-notification delivery');

$asset = \frontend\modules\notifications\assets\NotificationAsset::register($this);

if (app()->user->can('notifications')) {
    echo Html::a(t('app', 'Add'), ['create-mass-mailing'], ['class' => 'button', 'style' => 'float: right;']);
}
?>
<h1><?= Html::encode($this->title) ?></h1>

<section class="main_tabs">
    <div class="tabs_links">
        <ul>
            <li><a href="#active" class="t01"><?= t('car', 'Active') ?></a></li>
            <li><a href="#blocked" class="t02" data-href="<?= Url::to('mass-mailing-blocked') ?>"><?= t('car', 'Blocked') ?></a></li>
        </ul>
    </div>
    <div class="tabs_content">
        <div id="t01" data-view="index">
            <?=$this->render('grid',compact('dataProvider', 'cityList')) ?>
        </div>
        <div id="t02" data-view="blocked">
        </div>
    </div>
</section>

