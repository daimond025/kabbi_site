<?php
use yii\grid\GridView;
use yii\helpers\Html;


echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'       => '{items}',
    'tableOptions' => [
        'class' => 'people_table',
    ],
    'columns'      => [
        [
            'attribute'     => 'name',
            'content'       => function ($model) {
                return Html::a(Html::encode($model->name), ['update-mass-mailing', 'id' => $model->notification_id],
                    ['data-pjax' => 0]);
            },
            'headerOptions' => ['style' => 'width: 40%'],
        ],
        [
            'attribute'     => 'addressee',
            'content'       => function ($model) {
                return $model->getAddresseeName();
            },
            'headerOptions' => ['style' => 'width: 20%'],
        ],
        [
            'attribute'     => 'cityList',
            'content'       => function ($model) use ($cityList) {
                return implode(', ', array_map(function ($item) use ($cityList) {
                    return $cityList[$item];
                }, $model->cityList));
            },
            'headerOptions' => ['style' => 'width: 40%'],
        ],
        [
            'attribute'     => 'Delete',
            'content'       => function ($model) {
                return Html::a('<div style="margin-left: 19%" ><span>X</span></div>', ['delete-mass-mailing', 'id' => $model->notification_id],
                    ['data-pjax' => 0]);
            },
            'headerOptions' => ['style' => 'width: 10%'],
        ],
    ],
]);
?>