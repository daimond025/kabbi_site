<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \frontend\modules\notifications\models\Notification */
/* @var $cityList array */
/* @var $positionList array */
/* @var $classList array */
/* @var $device array */


$form = ActiveForm::begin([
    'id'                     => 'notification-form-' . $model->notification_id,
    'errorCssClass'          => 'input_error',
    'successCssClass'        => 'input_success',
   // 'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'validateOnBlur'         => false,
    'options'                => ['enctype' => 'multipart/form-data'],
]); ?>

    <section class="row">
        <?= $form->field($model, 'name')->begin(); ?>
        <div class="row_label">
            <?= Html::activeLabel($model, 'name') ?>
        </div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'name') ?>
            <?= Html::error($model, 'name',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'name')->end(); ?>
    </section>

    <!--Тип оповещения-->
    <section class="row">
        <?= $form->field($model, 'type_notice')->begin(); ?>
        <? $model->type_notice = \frontend\modules\notifications\models\Notification::TYPE_NOTICE_SMS ;  ?>
        <div class="row_label"><?= Html::activeLabel($model, 'type_notice') ?></div>
        <div class="row_input">
            <?= Html::activeDropDownList($model, 'type_notice', $model->addTypeNotificationList(), [
                'data-placeholder'  => !empty($model->type_notice) ? Html::encode($model->type_notice) : t('notification',
                    'Choose type notification'),
                'class'             => 'default_select js-type_notice',
                'data-change-level' => '1',
            ]) ?>
        </div>
        <?= $form->field($model, 'type_notice')->end(); ?>
    </section>

    <!--Город-->
    <section class="row">
        <?= $form->field($model, 'cityList')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'cityList') ?></div>
        <div class="row_input">
            <?= Html::activeCheckboxList($model, 'cityList', $cityList, [
                'class' => 'select_checkbox',
                'item'  => function ($index, $label, $name, $checked, $value) {
                    $input = Html::checkbox($name, $checked, ['value' => $value]);

                    $label = Html::label($input . $label);

                    return $label . '<br>';
                },
            ]) ?>
        </div>
        <?= $form->field($model, 'cityList')->end(); ?>
    </section>

    <!--Адресат-->
    <section class="row">
        <?= $form->field($model, 'addressee')->begin(); ?>
        <? $model->addressee = \frontend\modules\notifications\models\Notification::TYPE_CLIENT ;  ?>
        <div class="row_label"><?= Html::activeLabel($model, 'addressee') ?></div>
        <div class="row_input">
            <?= Html::activeDropDownList($model, 'addressee', $model->addresseeList(), [
                'data-placeholder'  => !empty($model->addressee) ? Html::encode($model->addressee) : t('notification',
                    'Choose addressee'),
                'class'             => 'default_select js-addressee',
                'data-change-level' => '1',
            ]) ?>
        </div>
        <?= $form->field($model, 'addressee')->end(); ?>
    </section>

    <!--Ид адресата-->
    <section class="row">
        <?= $form->field($model, 'addressee_id')->begin(); ?>
        <div class="row_label">
            <?= Html::activeLabel($model, 'addressee_id') ?>
        </div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'addressee_id',
                ['class' => 'js-addressee-id', 'placeholder' => t('notification', 'All')]) ?>
            <?= Html::error($model, 'title',
                [
                    'tag'               => 'span',
                    'class'             => 'help-block help-block-error',
                    'style'             => 'color:red',
                    'data-level'        => 1,
                    'data-change-level' => '2',
                ]); ?>
        </div>
        <?= $form->field($model, 'addressee_id')->end(); ?>
    </section>

    <!--Тип устройства клиента-->
    <?
       $type_device_display = '';
       if($model->addressee !== 'CLIENT' || $model->type_notice === $model::TYPE_NOTICE_SMS){
           $type_device_display = 'style="display:none"';
           
       }
    ?>
    <section class="row" <?= $type_device_display?>>
        <?= $form->field($model, 'deviceList', ['options' => ['class' => 'required']])->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'deviceList') ?></div>
        <div class="row_input">
            <?= Html::activeCheckboxList($model, 'deviceList', $model->deviceList(), [
                'class' => 'select_checkbox js-device',
                'item'  => function ($index, $label, $name, $checked, $value) {
                    $input = Html::checkbox($name, $checked, ['value' => $value]);

                    $label = Html::label($input . $label);

                    return $label . '<br>';
                },
            ]) ?>
        </div>
        <?= $form->field($model, 'deviceList')->end(); ?>
    </section>


    <!--Смена-->
    <section class="row" <?= $model->addressee !== 'WORKER' ? 'style="display:none"' : ''; ?>>
        <?= $form->field($model, 'on_shift', ['options' => ['class' => 'required']])->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'on_shift') ?></div>
        <div class="row_input">
            <?= Html::activeDropDownList($model, 'on_shift', $model->onShiftList(),
                ['class' => 'default_select js-on_shift',]) ?>
        </div>
        <?= $form->field($model, 'on_shift')->end(); ?>
    </section>


    <!--Должность исполнителя-->
    <section class="row" <?= $model->addressee !== 'WORKER' ? 'style="display:none"' : ''; ?>>
        <?= $form->field($model, 'positionList', ['options' => ['class' => 'required']])->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'positionList') ?></div>
        <div class="row_input">
            <?= Html::activeCheckboxList($model, 'positionList', $positionList, [
                'class' => 'select_checkbox js-position',
                'item'  => function ($index, $label, $name, $checked, $value) {
                    $input = Html::checkbox($name, $checked, ['value' => $value]);

                    $label = Html::label($input . $label);

                    return $label . '<br>';
                },
            ]) ?>
        </div>
        <?= $form->field($model, 'positionList')->end(); ?>
    </section>

    <!--Класс исполнителя-->
    <section class="row" <?= $model->addressee !== 'WORKER' ? 'style="display:none"' : ''; ?>>
        <?= $form->field($model, 'classList')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'classList') ?></div>
        <div class="row_input">
            <?= Html::activeCheckboxList($model, 'classList', $classList, [
                'class' => 'select_checkbox js-class',
                'item'  => function ($index, $label, $name, $checked, $value) {
                    $input = Html::checkbox($name, $checked, ['value' => $value]);
                    $label = Html::label($input . $label);
                    return $label . '<br>';
                },
            ]) ?>
            <?= Html::error($model, 'classList'); ?>
        </div>
    </section>


    <!--Заголовок уведомления-->
    <section class="row">
        <?= $form->field($model, 'title')->begin(); ?>
        <div class="row_label">
            <?= Html::activeLabel($model, 'title') ?>
        </div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'title') ?>
            <?= Html::error($model, 'title',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'title')->end(); ?>
    </section>

    <!--Текст уведомления-->
    <section class="row">
        <?= $form->field($model, 'text')->begin(); ?>
        <div class="row_label">
            <?= Html::activeLabel($model, 'text') ?>
        </div>
        <div class="row_input">
            <?= Html::activeTextarea($model, 'text') ?>
            <?= Html::error($model, 'text',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'text')->end(); ?>
    </section>

    <div style="margin-left: 30%">
        <?= $form->errorSummary($model);?>
    </div>

    <section class="submit_form">
        <div>
            <label><?= Html::checkbox('execute', false) ?> <b><?= t('notification',
                        'Execute') ?></b></label>
        </div>

        <div <?= $create ? "style='display:none'" : ''?>>
            <?= Html::ActiveCheckbox($model, 'blocked') ?>
        </div>

        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
    </section>

    <style>
        .input_error{
            border: 1px double red;
        }
    </style>

<?php ActiveForm::end(); ?>