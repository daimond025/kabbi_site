<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $cityList array */
/* @var $positionList array */
/* @var $classList array */
$this->title = t('setting', 'Push-notification delivery');
$asset       = \frontend\modules\notifications\assets\NotificationAsset::register($this);

?>
<div class="bread"><a href="<?= Url::toRoute('mass-mailing') ?>"><?= Html::encode($this->title) ?></a></div>
<h1><?= Html::encode($model->name) ?></h1>
<?php
$form = app()->user->can('notifications') ? '_form' : '_form_read';

echo $this->render($form, array_merge(compact('model', 'cityList', 'positionList', 'classList'),['update'=>true]));



