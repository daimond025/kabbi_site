<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $cityList array */
/* @var $positionList array */
/* @var $classList array */
$this->title = Yii::t('notification', 'Add notification');
$asset       = \frontend\modules\notifications\assets\NotificationAsset::register($this);

?>
<div class="bread"><a href="<?= Url::toRoute('mass-mailing') ?>"><?= t('setting', 'Push-notification delivery') ?></a>
</div>
<h1><?= Html::encode($this->title) ?></h1>
<?php
echo $this->render('_form', array_merge(compact('model', 'cityList', 'positionList', 'classList'),['create'=>true]));
?>
