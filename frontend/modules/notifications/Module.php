<?php

namespace app\modules\notifications;

use app\components\behavior\ControllerBehavior;
use yii\helpers\ArrayHelper;

/**
 * Class Module
 *
 * @mixin ControllerBehavior
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
        \Yii::configure($this, require __DIR__ . '/config.php');
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'common' => [
                'class' => ControllerBehavior::className(),
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $this->commonBeforeAction();

        return parent::beforeAction($action);
    }
}
