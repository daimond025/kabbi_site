<?php

namespace frontend\modules\upload\controllers;

use common\modules\employee\models\documents\Document;
use frontend\components\behavior\file\FileBehavior;
use frontend\modules\car\models\Car;
use frontend\modules\employee\components\document\WorkerDocumentFactory;
use frontend\modules\employee\models\documents\WorkerDocumentScan;
use frontend\modules\employee\models\worker\Worker;
use frontend\modules\employee\models\worker\WorkerHasDocument;
use frontend\modules\employee\models\worker\WorkerHasPosition;
use frontend\modules\upload\models\User;
use Yii;
use yii\filters\auth\HttpBasicAuth;
use yii\web\Controller;


class UploadController extends Controller
{
    public function behaviors()
    {
        return [
            'authenticator' => [
                'class' => HttpBasicAuth::className(),
                'except' => ['index'],
                'auth' => function ($username, $password) {
                    $user = User::findByUsername($username);

                    return isset($user) && $user->validatePassword($password) ? $user : null;
                }
            ],
        ];
    }


    public function actionClientPhoto()
    {
        $client_id = Yii::$app->request->post('client_id');
        $phone = Yii::$app->request->post('phone');
        if (isset($client_id) && isset($phone)) {
            $client = \app\modules\client\models\Client::findOne($client_id);
            if ($client) {
                user()->tenant_id = $client->tenant_id;
                $client->phone = [$phone];
                $client->detachBehavior('fileBehavior');
                $client->attachBehavior('fileBehavior', [
                    'class' => FileBehavior::className(),
                    'upload_dir' => app()->params['upload'],
                    'fileField' => ['photo'],
                    'max_big_picture_side' => 600,
                ]);
                if ($client->save()) {
                    return json_encode(['result' => true]);
                }
            }
        }

        return json_encode(['result' => false]);
    }


    public function actionWorkerPhoto()
    {
        $workerId = Yii::$app->request->post('worker_id');
        if (!empty($workerId)) {
            $worker = Worker::findOne($workerId);
            if ($worker) {
                user()->tenant_id = $worker->tenant_id;
                $worker->detachBehavior('fileBehavior');
                $worker->attachBehavior('fileBehavior', [
                    'class' => FileBehavior::className(),
                    'upload_dir' => app()->params['upload'],
                    'fileField' => ['photo'],
                    'max_big_picture_side' => 600,
                ]);
                if ($worker->save(true, ['photo'])) {
                    return json_encode(['result' => true]);
                }
            }
        }

        return json_encode(['result' => false]);
    }

    public function actionCarPhoto()
    {
        $carId = Yii::$app->request->post('car_id');
        if (!empty($carId)) {
            $car = Car::findOne($carId);
            if ($car) {
                user()->tenant_id = $car->tenant_id;
                $car->detachBehavior('fileBehavior');
                $car->attachBehavior('fileBehavior', [
                    'class' => FileBehavior::className(),
                    'upload_dir' => app()->params['upload'],
                    'fileField' => ['photo'],
                    'max_big_picture_side' => 600,
                ]);
                if ($car->save(true, ['photo'])) {
                    return json_encode(['result' => true]);
                }
            }
        }

        return json_encode(['result' => false]);
    }

    public function actionWorkerDocuments()
    {

        $workerId = Yii::$app->request->post('worker_id');
        $positionId = Yii::$app->request->post('position_id');

        $workerHasPositionId = WorkerHasPosition::find()
            ->select('id')
            ->where([
                'worker_id' => $workerId,
                'position_id' => $positionId,
                'active' => 1,
            ])
            ->scalar();

        $worker = Worker::find()
            ->where(['worker_id' => $workerId])
            ->with([
                'workerHasDocuments' => function ($query) use ($workerHasPositionId) {
                    $query->indexBy('document_id')
                        ->joinWith([
                            'document' => function ($subQuery) {
                                $subQuery->where([
                                    'code' => [
                                        Document::PASSPORT,
                                        Document::SNILS,
                                        Document::INN,
                                        Document::OGRNIP,
                                        Document::OSAGO,
                                        Document::DRIVER_LICENSE,
                                    ],
                                ]);

                            },
                        ]);
                },
            ])
            ->one();

        if ($worker) {
            user()->tenant_id = $worker->tenant_id;
            $passport = $worker->workerHasDocuments[Document::PASSPORT]->passport ?: WorkerDocumentFactory::createDocument(Document::PASSPORT);
            $passport->load(Yii::$app->request->post());
            $passport->worker_id = $workerId;
            $passport->save();

            $ogrnip = $worker->workerHasDocuments[Document::OGRNIP]->ogrnip ?: WorkerDocumentFactory::createDocument(Document::OGRNIP);
            $ogrnip->load(Yii::$app->request->post());
            $ogrnip->worker_id = $workerId;
            $ogrnip->save();

            if(!empty($workerHasPositionId)) {
                $osago = $worker->workerHasDocuments[Document::OSAGO]->osago ?: WorkerDocumentFactory::createDocument(Document::OSAGO);
                $osago->load(Yii::$app->request->post());
                $osago->worker_id = $workerId;
                $osago->has_position_id = $workerHasPositionId;
                $osago->save();

                $driver_license = $worker->workerHasDocuments[Document::DRIVER_LICENSE]->driver_license ?: WorkerDocumentFactory::createDocument(Document::DRIVER_LICENSE);
                $driver_license->load(Yii::$app->request->post());
                $driver_license->worker_id = $workerId;
                $driver_license->has_position_id = $workerHasPositionId;
                $driver_license->save();

                $pts = $worker->workerHasDocuments[Document::DRIVER_LICENSE]->pts ?: WorkerDocumentFactory::createDocument(Document::PTS);
                $pts->load(Yii::$app->request->post());
                $pts->worker_id = $workerId;
                $pts->has_position_id = $workerHasPositionId;
                $pts->save();
            }


        }

        return true;
    }

    public function actionDocumentScan()
    {
        $documentCode = Yii::$app->request->post('document_code');
        $workerId     = Yii::$app->request->post('worker_id');
        $positionId   = Yii::$app->request->post('position_id');

        user()->tenant_id = Yii::$app->request->post('tenant_id');

        $workerHasPosition = WorkerHasPosition::findOne(['worker_id' => $workerId, 'position_id' => $positionId]);

        if (!$workerHasPosition) {
            return false;
        }

        $documentId = WorkerHasDocument::find()
            ->select('id')
            ->where([
                'worker_id'       => $workerId,
                'document_id'     => $documentCode,
            ])
            ->andWhere([
                'or',
                ['has_position_id' => $workerHasPosition->id],
                ['has_position_id' => null],
            ])
            ->scalar();

        if($documentId) {
            $documentScan = new WorkerDocumentScan(['worker_document_id' => $documentId]);
            return $documentScan->save();
        }

        return false;

    }



    public function actionWorkerFiles()
    {
        $post = post();
        $worker_id = $post['worker_id'];

        $model = Worker::find()
            ->where(['worker_id' => $worker_id])
            ->with([
                'workerHasDocuments' => function ($query) {
                    $query->indexBy('document_id')
                        ->with(['documentScans'])
                        ->joinWith([
                            'document' => function ($subQuery) {
                                $subQuery->where([
                                    'code' => [
                                        Document::PASSPORT,
                                        Document::SNILS,
                                        Document::INN,
                                        Document::OGRNIP,
                                    ],
                                ]);
                            },
                        ]);
                },
            ])
            ->one();
        user()->tenant_id = $model->tenant_id;

        $passport = $model->workerHasDocuments[Document::PASSPORT]->passport
            ?: WorkerDocumentFactory::createDocument(Document::PASSPORT);
        $passport->load($post);
        $passport->worker_id = $worker_id;

        $passportScan = new WorkerDocumentScan(['worker_document_id' => $passport->worker_document_id]);
        $passportScan->save();

        $passport->save();
        return $passport->worker_document_id;
    }

}