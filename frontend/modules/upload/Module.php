<?php

namespace frontend\modules\upload;

use Yii;


class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\upload\controllers';

    public function init()
    {

        parent::init();

        app()->setComponents([
            'user' => [
                'class' => 'yii\web\User',
                'identityClass' => 'frontend\modules\upload\models\User',
                'enableSession' => false
            ],
            'request' => [
                'class' => 'yii\web\Request',
                'enableCsrfValidation' => false,
                'enableCookieValidation' => false
            ],
        ]);

    }
}
