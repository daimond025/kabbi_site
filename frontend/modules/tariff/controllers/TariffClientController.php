<?php

namespace app\modules\tariff\controllers;

use app\modules\tariff\models\FixTariff;
use app\modules\tariff\models\Holiday;
use app\modules\tariff\models\OptionTariff;
use app\modules\tariff\models\TaxiTariff;
use app\modules\tariff\models\TaxiTariffGroup;
use app\modules\tariff\models\TaxiTariffGroupSearch;
use app\modules\tariff\models\TaxiTariffHasCity;
use app\modules\tariff\models\TaxiTariffSearch;
use common\modules\tenant\models\TenantHasCarOption;
use frontend\components\behavior\CityListBehavior;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\ActiveQuery;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * TaxiTariffController implements the CRUD actions for TaxiTariff model.
 * @mixin CityListBehavior
 */
class TariffClientController extends Controller
{
    public function behaviors()
    {
        return [
            'access'   => [
                'class'  => AccessControl::className(),
                'except' => [],
                'rules'  => [
                    [
                        'actions' => ['list', 'update', 'holidays', 'get-fix-tariff', 'list-blocked'],
                        'allow'   => true,
                        'roles'   => ['read_clientTariff'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['clientTariff'],
                    ],
                ],
            ],
            'cityList' => [
                'class' => CityListBehavior::className(),
            ],
        ];
    }

    private function getListTariffData($params, $block = false)
    {
        $groups           = TaxiTariffGroup::find()
            ->where(['tenant_id' => user()->tenant_id])
            ->asArray()
            ->all();
        $groups           = ArrayHelper::merge([
            [
                'group_id'  => 'null',
                'tenant_id' => '68',
                'name'      => '',
                'sort'      => '1',
            ],
        ], $groups);
        $arr_dataProvider = [];
        foreach ($groups as $group) {
            $searchModel                          = new TaxiTariffSearch();
            $cityList                             = $this->getUserCityList();
            $searchModel->accessCityList          = array_keys($cityList);
            $searchModel->block                   = (int)$block;
            $searchModel->group_id                = $group['group_id'];
            $dataProvider                         = new ArrayDataProvider([
                'allModels'  => $searchModel->search($params)->asArray()->all(),
                'pagination' => false,
            ]);
            $arr_dataProvider[$group['group_id']] = $dataProvider;
        }

        return $arr_dataProvider;

    }

    private function getListData($params, $block = false)
    {
        $city_list = $this->getUserCityList();

        $searchModel      = new TaxiTariffSearch();
        $arr_dataProvider = $this->getListTariffData($params, $block);

        $searchModelGroup = new TaxiTariffGroupSearch();
        $groupQuery       = $searchModelGroup->hasTariffs($arr_dataProvider);
        $data             = ArrayHelper::merge([
            [
                'group_id'  => 'null',
                'tenant_id' => (string)user()->tenant_id,
                'name'      => 'Без группы',
                'sort'      => '1',
            ],
        ], $groupQuery->asArray()->all());
        $dataProvider     = new ArrayDataProvider([
            'allModels'  => $data,
            'sort'       => [
                'attributes' => ['sort', 'name'],
            ],
            'pagination' => false,
        ]);
        $pjaxId           = $block ? 'taxi_tariff_list_block' : 'taxi_tariff_list';

        return compact('dataProvider', 'searchModel', 'arr_dataProvider', 'city_list', 'pjaxId');
    }

    /**
     * Lists all TariffGroup models.
     * @return mixed
     */
    public function actionList()
    {
        return $this->render('index', $this->getListData(Yii::$app->request->queryParams));
    }


    public function actionListBlocked()
    {
        return $this->renderAjax('_groupList', $this->getListData(Yii::$app->request->queryParams, 1));
    }

    /**
     * Creates a new TaxiTariff model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $tariff            = new TaxiTariff(); //Основной тариф
        $currentCity       = new OptionTariff(); //Обычные дни - Город
        $currentTrack      = new OptionTariff(); //Обычные дни - Загород
        $currentAirport    = new OptionTariff(); //Обычные дни - Аэропорт
        $currentRailway    = new OptionTariff(); //Обычные дни - ЖД
        $holidaysCity      = new OptionTariff(['active' => 0]); //Выходные дни - Город
        $holidaysTrack     = new OptionTariff(['active' => 0]); //Выходные дни - Загород
        $holidaysAirport   = new OptionTariff(['active' => 0]); //Выходные дни - Аэропорт
        $holidaysRailway   = new OptionTariff(['active' => 0]); //Выходные дни - ЖД
        $exclusionsCity    = new OptionTariff(['active' => 0]); //Исключения - Город
        $exclusionsTrack   = new OptionTariff(['active' => 0]); //Исключения - Загород
        $exclusionsAirport = new OptionTariff(['active' => 0]); //Исключения - Аэропорт
        $exclusionsRailway = new OptionTariff(['active' => 0]); //Исключения - ЖД

        if (app()->user->can('clientTariff') && $tariff->load(Yii::$app->request->post())) {
            //БЛОК ВАЛИДАЦИИ
            $tariff->validate();

            //"ОБЫЧНЫЕ ДНИ"
            $tariff->optionValidate($currentCity, $currentTrack, $currentAirport, $currentRailway,
                $_POST[OptionTariff::CURRENT_TYPE], OptionTariff::CURRENT_TYPE);

            //"ВЫХОДНЫЕ, ПРАЗДНИКИ"
            if ($_POST[OptionTariff::HOLIDAYS_TYPE]['active']) {
                $tariff->optionValidate($holidaysCity, $holidaysTrack, $holidaysAirport, $holidaysRailway,
                    $_POST[OptionTariff::HOLIDAYS_TYPE], OptionTariff::HOLIDAYS_TYPE);
            }
            //"ИСКЛЮЧЕНИЯ"
            if ($_POST[OptionTariff::EXCEPTIONS_TYPE]['active']) {
                $tariff->optionValidate($exclusionsCity, $exclusionsTrack, $exclusionsAirport, $exclusionsRailway,
                    $_POST[OptionTariff::EXCEPTIONS_TYPE], OptionTariff::EXCEPTIONS_TYPE);
            }
            //----------------------------------------------------------------------------------------------------------------------------------------

            if (!$tariff->hasErrors()) {
                $transaction = app()->db->beginTransaction();
                try {
                    //Сохранение тарифа
                    $tariff->save(false);

                    //Добавление городов тарифа
                    TaxiTariffHasCity::manySave($tariff->city_list, $tariff->tariff_id);

                    //СОХРАНЕНИЕ ОПЦИЙ
                    $cityId = current($tariff->city_list);
                    //"Обычные дни"
                    $tariff->saveOptiontariff($cityId, $currentCity, $currentTrack, $currentAirport, $currentRailway,
                        $_POST[OptionTariff::CURRENT_TYPE], OptionTariff::CURRENT_TYPE);

                    //"Выходные, праздники"
                    if ($_POST[OptionTariff::HOLIDAYS_TYPE]['active']) {
                        $tariff->saveOptiontariff($cityId, $holidaysCity, $holidaysTrack, $holidaysAirport, $holidaysRailway,
                            $_POST[OptionTariff::HOLIDAYS_TYPE], OptionTariff::HOLIDAYS_TYPE);
                    }

                    //"Исключения"
                    if ($_POST[OptionTariff::EXCEPTIONS_TYPE]['active']) {
                        $tariff->saveOptiontariff($cityId, $exclusionsCity, $exclusionsTrack, $exclusionsAirport,
                            $exclusionsRailway, $_POST[OptionTariff::EXCEPTIONS_TYPE], OptionTariff::EXCEPTIONS_TYPE);
                    }
                    //--------------------------------------------------------------------------------------------------------------------------------
                    $transaction->commit();

                    session()->setFlash('success', t('taxi_tariff', 'The tariff successfully added'));

                    return $this->redirect(['/tariff/tariff-client/list', '#' => 'active']);
                    //return $this->redirect(['update', 'id' => $tariff->tariff_id]);
                } catch (Exception $ex) {
                    $transaction->rollback();
                    session()->setFlash('error',
                        t('taxi_tariff', 'Error saving tariff. Notify to administrator, please.'));
                }
            } else {
                displayErrors([$tariff]);
            }
        }

        //Получение необходимых для формы данных
        $form_data = $tariff->getFormData();

        return $this->render('create', [
            'tariff'          => $tariff,
            'form_data'       => $form_data,
            'currentCity'     => $currentCity,
            'currentTrack'    => $currentTrack,
            'holidaysCity'    => $holidaysCity,
            'holidaysTrack'   => $holidaysTrack,
            'exclusionsCity'  => $exclusionsCity,
            'exclusionsTrack' => $exclusionsTrack,
        ]);
    }

    public function actionUploadLogo($id)
    {
        app()->response->format = Response::FORMAT_JSON;

        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $model = $this->findTariff($id);

        if (!$model) {
            throw new HttpException(404);
        }
        $model->load(post());

        if($model->save(true, ['logo'])) {
            return [
                'src' => [
                    'big' => $model->getPicturePath($model->logo),
                    'thumb' => $model->getPicturePath($model->logo, false),
                ],
            ];
        }

        return $model->getFirstErrors();
    }


    /**
     * Updates an existing TaxiTariff model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param string $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        /** @var TaxiTariff $tariff */
        //Основной тариф
        $tariff = TaxiTariff::find()
            ->where([
                'tariff_id' => $id,
                'tenant_id' => user()->tenant_id,
            ])
            ->with([
                'addOptions',
                'optionActiveDates',
                'option' => function (ActiveQuery $query) {
                    $query->with([
                        'fixHasOptions',
                        'fixHasOptions.fix',
                    ]);
                },
                'class',
                'cities' => function (ActiveQuery $query) {
                    $query->select(['city_id', 'name']);
                },
            ])
            ->one();

        if (!$tariff) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        //Доп. опции - список
        $addOptions = [];
        foreach ($tariff->addOptions as $item) {
            $addOptions[$item->tariff_type][$item->additional_option_id] = $item;
        }

        //Сгруппированный по типу массив опций + запросы на получение фикс тарифа
        $arOption       = [];
        $fixCity        = [];
        $fixTrack       = [];
        $fixAiroport    = [];
        $fixRailway     = [];
        $need_city_fix  = false;
        $need_track_fix = false;
        $need_air_fix   = false;
        $need_rail_fix  = false;

        foreach ($tariff->option as $option) {
            if (!$need_city_fix && $option->area == OptionTariff::AREA_CITY && $option->accrual == OptionTariff::ACCRUAL_FIX) {
                $need_city_fix = true;
                $fixCity       = FixTariff::getFixTariffs($tariff->cities[0]->city_id, 'city');
            }

            if (!$need_track_fix && $option->area == OptionTariff::AREA_TRACK && $option->accrual == OptionTariff::ACCRUAL_FIX) {
                $need_track_fix = true;
                $fixTrack       = FixTariff::getFixTariffs($tariff->cities[0]->city_id, 'outCity');
            }

            if (!$need_air_fix && $option->area == OptionTariff::AREA_AIRPORT && $option->accrual == OptionTariff::ACCRUAL_FIX) {
                $need_air_fix = true;
                $fixAiroport  = FixTariff::getFixTariffs($tariff->cities[0]->city_id,
                    'airport');
            }

            if (!$need_rail_fix && $option->area == OptionTariff::AREA_RAILWAY && $option->accrual == OptionTariff::ACCRUAL_FIX) {
                $need_rail_fix = true;
                $fixRailway    = FixTariff::getFixTariffs($tariff->cities[0]->city_id,
                    'station');
            }

            $arOption[$option->tariff_type][$option->area] = $option;
        }

        //Даты активности тарифа
        $tariffActiveDate = [];
        foreach ($tariff->optionActiveDates as $date) {
            $tariffActiveDate[$date->tariff_type][] = $date->active_date;
        }
        //Обычные дни - Город
        $currentCity = $arOption[OptionTariff::CURRENT_TYPE][OptionTariff::AREA_CITY];
        //Обычные дни - Загород
        $currentTrack = $arOption[OptionTariff::CURRENT_TYPE][OptionTariff::AREA_TRACK];
        //Обычные дни - Аэропорт
        $currentAirport = isset($arOption[OptionTariff::CURRENT_TYPE][OptionTariff::AREA_AIRPORT]) ?
            $arOption[OptionTariff::CURRENT_TYPE][OptionTariff::AREA_AIRPORT] : new OptionTariff;
        //Обычные дни - ЖД
        $currentRailway = isset($arOption[OptionTariff::CURRENT_TYPE][OptionTariff::AREA_RAILWAY]) ?
            $arOption[OptionTariff::CURRENT_TYPE][OptionTariff::AREA_RAILWAY] : new OptionTariff;

        //Выходные дни - Город
        if (isset($arOption[OptionTariff::HOLIDAYS_TYPE][OptionTariff::AREA_CITY])) {
            $holidaysCity               = $arOption[OptionTariff::HOLIDAYS_TYPE][OptionTariff::AREA_CITY];
            $holidaysCity->action_dates = isset($tariffActiveDate[$holidaysCity->tariff_type]);
        } else {
            $holidaysCity = new OptionTariff(['active' => 0]);
        }

        //Выходные дни - Загород
        $holidaysTrack = isset($arOption[OptionTariff::HOLIDAYS_TYPE][OptionTariff::AREA_TRACK]) ?
            $arOption[OptionTariff::HOLIDAYS_TYPE][OptionTariff::AREA_TRACK] : new OptionTariff(['active' => 0]);
        //Выходные дни - Аэропорт
        $holidaysAirport = isset($arOption[OptionTariff::HOLIDAYS_TYPE][OptionTariff::AREA_AIRPORT]) ?
            $arOption[OptionTariff::HOLIDAYS_TYPE][OptionTariff::AREA_AIRPORT] : new OptionTariff;
        //Выходные дни - ЖД
        $holidaysRailway = isset($arOption[OptionTariff::HOLIDAYS_TYPE][OptionTariff::AREA_RAILWAY]) ?
            $arOption[OptionTariff::HOLIDAYS_TYPE][OptionTariff::AREA_RAILWAY] : new OptionTariff;

        //Исключения - Город
        if (isset($arOption[OptionTariff::EXCEPTIONS_TYPE][OptionTariff::AREA_CITY])) {
            $exclusionsCity               = $arOption[OptionTariff::EXCEPTIONS_TYPE][OptionTariff::AREA_CITY];
            $exclusionsCity->action_dates = isset($tariffActiveDate[$exclusionsCity->tariff_type]);
        } else {
            $exclusionsCity = new OptionTariff(['active' => 0]);
        }

        //Исключения - Загород
        $exclusionsTrack = isset($arOption[OptionTariff::EXCEPTIONS_TYPE][OptionTariff::AREA_TRACK]) ?
            $arOption[OptionTariff::EXCEPTIONS_TYPE][OptionTariff::AREA_TRACK] : new OptionTariff(['active' => 0]);
        //Исключения - Аэропорт
        $exclusionsAirport = isset($arOption[OptionTariff::EXCEPTIONS_TYPE][OptionTariff::AREA_AIRPORT]) ?
            $arOption[OptionTariff::EXCEPTIONS_TYPE][OptionTariff::AREA_AIRPORT] : new OptionTariff;
        //Исключения - ЖД
        $exclusionsRailway = isset($arOption[OptionTariff::EXCEPTIONS_TYPE][OptionTariff::AREA_RAILWAY]) ?
            $arOption[OptionTariff::EXCEPTIONS_TYPE][OptionTariff::AREA_RAILWAY] : new OptionTariff;

        $tariff->city_list = $tariff->cities[0]->city_id;
        if (app()->user->can('clientTariff') && $tariff->load(Yii::$app->request->post())) {
            //БЛОК ВАЛИДАЦИИ
            $tariff->validate();
            $tariff->cropParams = post("TaxiTariff")["cropParams"];
            $tariff->setCropParams();

            if ($tariff->getOldAttribute('class_id') != $tariff->class_id &&
                $tariff->is_exists($_POST['TaxiTariff']['class_id'], $_POST['TaxiTariff']['city_list'][0])
            ) {
                $tariff->addError('tariff_id', t('taxi_tariff', 'This tariff is already added.'));
            }

            //"ОБЫЧНЫЕ ДНИ"
            $tariff->optionValidate($currentCity, $currentTrack, $currentAirport, $currentRailway,
                $_POST[OptionTariff::CURRENT_TYPE], OptionTariff::CURRENT_TYPE);

            //"ВЫХОДНЫЕ, ПРАЗДНИКИ"
            if ($_POST[OptionTariff::HOLIDAYS_TYPE]['active']) {
                $tariff->optionValidate($holidaysCity, $holidaysTrack, $holidaysAirport, $holidaysRailway,
                    $_POST[OptionTariff::HOLIDAYS_TYPE], OptionTariff::HOLIDAYS_TYPE);
            }

            //"ИСКЛЮЧЕНИЯ"
            if ($_POST[OptionTariff::EXCEPTIONS_TYPE]['active']) {
                $tariff->optionValidate($exclusionsCity, $exclusionsTrack, $exclusionsAirport, $exclusionsRailway,
                    $_POST[OptionTariff::EXCEPTIONS_TYPE], OptionTariff::EXCEPTIONS_TYPE);
            }
            //----------------------------------------------------------------------------------------------------------------------------------------

            if (!$tariff->hasErrors()) {
                try {
                    $dirtyAttributes = $tariff->dirtyAttributes;
                    unset($dirtyAttributes['logo']);
                    //Сохранение тарифа
                    $tariff->save(false, array_keys($dirtyAttributes));

                    //Добавление городов тарифа
                    //                    TaxiTariffHasCity::deleteAll(['tariff_id' => $tariff->tariff_id]);
                    //                    TaxiTariffHasCity::manySave($tariff->city_list, $tariff->tariff_id);

                    //СОХРАНЕНИЕ ОПЦИЙ
                    $cityId = $tariff->city_list;
                    //"Обычные дни"
                    $tariff->saveOptiontariff($cityId, $currentCity, $currentTrack, $currentAirport, $currentRailway,
                        getValue($_POST[OptionTariff::CURRENT_TYPE], []), OptionTariff::CURRENT_TYPE);
                    //"Выходные, праздники"
                    $tariff->saveOptiontariff($cityId, $holidaysCity, $holidaysTrack, $holidaysAirport, $holidaysRailway,
                        getValue($_POST[OptionTariff::HOLIDAYS_TYPE], []), OptionTariff::HOLIDAYS_TYPE);
                    //"Исключения"
                    $tariff->saveOptiontariff($cityId, $exclusionsCity, $exclusionsTrack, $exclusionsAirport, $exclusionsRailway,
                        getValue($_POST[OptionTariff::EXCEPTIONS_TYPE], []), OptionTariff::EXCEPTIONS_TYPE);
                    //--------------------------------------------------------------------------------------------------------------------------------

                    session()->setFlash('success', t('app', 'Data updated successfully'));

                    return $this->refresh();
                    //return $this->redirect(['update', 'id' => $tariff->tariff_id]);
                } catch (Exception $ex) {
                    session()->setFlash('error',
                        t('taxi_tariff', 'Error saving tariff. Notify to administrator, please.'));
                }
            } else {
                displayErrors([$tariff]);
            }
        }

        //Получение необходимых для формы данных
        $form_data         = $tariff->getFormData();

        return $this->render('update', [
            'tariff'           => $tariff,
            'form_data'        => $form_data,
            'currentCity'      => $currentCity,
            'currentTrack'     => $currentTrack,
            'holidaysCity'     => $holidaysCity,
            'holidaysTrack'    => $holidaysTrack,
            'exclusionsCity'   => $exclusionsCity,
            'exclusionsTrack'  => $exclusionsTrack,
            'addOptions'       => $addOptions,
            'fixCity'          => $fixCity,
            'fixTrack'         => $fixTrack,
            'fixAiroport'      => $fixAiroport,
            'fixRailway'       => $fixRailway,
            'arOption'         => $arOption,
            'tariffActiveDate' => $tariffActiveDate,
        ]);
    }

    /**
     * Ajax method for getting fix tariff.
     *
     * @param integer $city_id
     * @param string  $parking_type
     * @param string  $tariff_type
     *
     * @return mixed
     */
    public function actionGetFixTariff($city_id, $parking_type, $tariff_type)
    {
        return $this->renderAjax('fixTariff', [
            'arParking'    => FixTariff::getFixTariffs($city_id, $parking_type),
            'parking_type' => $parking_type,
            'tariff_type'  => $tariff_type,
            'city_id'      => $city_id,
        ]);
    }

    public function actionHolidays()
    {
        return $this->renderAjax('holidays',
            ['holidays' => ArrayHelper::getColumn(Holiday::find()->select('date')->all(), 'date')]);
    }

    public function actionAddOptions($city_id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return TaxiTariff::getActiveCarOptionList($city_id);
    }

    public function findTariff($tariffId)
    {
        return TaxiTariff::find()
            ->where([
                'tariff_id' => $tariffId,
                'tenant_id' => user()->tenant_id,
            ])
            ->limit(1)
            ->one();
    }
}
