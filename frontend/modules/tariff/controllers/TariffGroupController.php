<?php

namespace app\modules\tariff\controllers;

use Yii;
use app\modules\tariff\models\TaxiTariffGroup;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\tariff\models\TaxiTariffGroupSearch;
use app\modules\tariff\models\TaxiTariff;
use yii\data\ArrayDataProvider;

/**
 * TariffGroupController implements the CRUD actions for TaxiTariffGroup model.
 */
class TariffGroupController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    

    private function getListData()
    {
        $searchModel = new TaxiTariffGroupSearch();
        $dataProvider = new ArrayDataProvider([
            'allModels' => $searchModel->tariffs()->asArray()->all(),
            'sort' => [
                'attributes' => ['sort', 'name'],
            ],
            'pagination' => false,
        ]);
        $pjaxId = 'taxi_tariff_group';
        return compact('dataProvider', 'pjaxId');
    }

    /**
     * Lists all WorkerGroup models.
     * @return mixed
     */
    public function actionList()
    {
        return $this->render('index', $this->getListData() );
    }

    /**
     * Creates a new TaxiTariffGroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaxiTariffGroup();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            session()->setFlash('success', t('taxi_tariff', 'Tariff group was successfully added'));
            return $this->redirect('list');
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TaxiTariffGroup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            session()->setFlash('success', t('taxi_tariff', 'Tariff group was successfully updated'));
            return $this->refresh();
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TaxiTariffGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $json = ['error' => '', 'success' => 0];
        $groupHasTariff = TaxiTariff::find()->where(['tenant_id' => user()->tenant_id, 'group_id' => $id])->exists();
        if ($groupHasTariff) {
            $json['error'] = t('taxi_tariff', 'The group has tariffs');
        } else {
            $this->findModel($id)->delete();
            session()->setFlash('success', t('taxi_tariff', 'The tariff group was successfully removed'));
            $json['success'] = 1;
        }

        return json_encode($json);
    }

    /**
     * Finds the TaxiTariffGroup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TaxiTariffGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = TaxiTariffGroup::find()->where(['group_id' => $id, 'tenant_id' => user()->tenant_id])->one();
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Ajax driver seacrh from filter data
     * @return html
     */
    public function actionSearch()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $search = new TaxiTariffGroupSearch();
        $dataProvider = $search->search(post());

        return $this->renderAjax('_groupListTr', ['groups' => $dataProvider->getModels()]);
    }
}
