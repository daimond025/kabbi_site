<?php

/* @var string $pjaxId */
/* @var $this yii\web\View */
/* @var array $cityList */
/* @var $dataProvider yii\data\ArrayDataProvider */
/* @var array $ar_dataProvider */

use yii\widgets\ListView;
use yii\widgets\Pjax;
use frontend\widgets\filter\Filter;

$filter = Filter::begin([
    'pjaxId' => $pjaxId,
    'model'  => $searchModel,
]);
echo $filter->checkboxList('city_id', $city_list);
Filter::end();

Pjax::begin(['id' => $pjaxId]);

echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView'     => '_group',
    'emptyText'    => t('app', 'Empty'),
    'layout'       => "{items}",
    'viewParams'   => [
        'arr_dataProvider' => $arr_dataProvider,
        'city_list' => $city_list,
    ],
]);

Pjax::end();
