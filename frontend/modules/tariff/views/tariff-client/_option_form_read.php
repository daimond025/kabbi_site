<?php

use yii\helpers\Html;
use app\modules\tariff\models\OptionTariff;

$nextCostUnitList = OptionTariff::getNextCostUnitList($tariff_city_id);
$currencySymbol   = Html::encode(getCurrencySymbol($tariff_city_id));
?>

<div class="add_plan">
    <section class="row">
        <div class="row_label"></div>
        <div class="row_input">
            <?= Html::activeCheckbox($modelCity, 'calculation_fix', ['disabled' => true]) ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint',
                        'To fix the preliminary calculation of the cost of the trip for the client. A taximeter will not count the cost of the trip. It will be considered only the waiting time for the customer before and during the trip.'); ?>
                </span>
            </a>
        </div>
    </section>

    <section class="row" style="display:none">
        <div class="row_label"></div>
        <div class="row_input">
            <?= Html::activeCheckbox($modelCity, 'enabled_parking_ratio', ['disabled' => true, 'value'=>0]) ?>
        </div>
    </section>

    <div class="day_night_block">
        <section class="row">
            <div class="row_label"></div>
            <div class="row_input">
                <?= Html::activeCheckbox($modelCity, 'allow_day_night', ['disabled' => true]) ?>
            </div>
        </section>

        <section class="row">
            <div class="row_label"><?= t('taxi_tariff', 'Day time') ?></div>
            <div class="row_input">
                <?= Html::encode($modelCity->start_day) ?> — <?= Html::encode($modelCity->end_day) ?>
            </div>
        </section>

        <section class="row">
            <div class="row_label"><?= t('taxi_tariff', 'Night time (auto)') ?></div>
            <div class="row_input">
                <?= Html::encode($modelCity->end_day) ?> — <?= Html::encode($modelCity->start_day) ?>
            </div>
        </section>
    </div>

    <div class="ap_top tariff_change">
        <div class="ap_top_f">
            <h3><?= t('taxi_tariff', 'City') ?></h3>
            <?= Html::activeDropDownList($modelCity, 'accrual', $form_data['ACCRUALS'], [
                'data-placeholder' => Html::encode(is_null($modelCity->accrual) ? current($form_data['ACCRUALS']) : $form_data['ACCRUALS'][$modelCity->accrual]),
                'class'            => 'default_select city js-tariff-type',
                'name'             => $modelType . '[OptionTariff][city][accrual]',
                'id'               => 'cur_accrual_city',
                'disabled'         => true,
            ]) ?>
            <span class="ap_top_d"><?= t('taxi_tariff', 'Day') ?></span>
            <span class="ap_top_n"><?= t('taxi_tariff', 'Night') ?></span>
        </div>

        <div class="ap_top_s">
            <h3><?= t('taxi_tariff', 'Track') ?></h3>
            <?= Html::activeDropDownList($modelTrack, 'accrual', $form_data['ACCRUALS'], [
                'data-placeholder' => Html::encode(is_null($modelTrack->accrual) ? current($form_data['ACCRUALS']) : $form_data['ACCRUALS'][$modelTrack->accrual]),
                'class'            => 'default_select track js-tariff-type',
                'name'             => $modelType . '[OptionTariff][track][accrual]',
                'id'               => 'cur_accrual_track',
                'disabled'         => true,
            ]) ?>
            <span class="ap_top_d"><?= t('taxi_tariff', 'Day') ?></span>
            <span class="ap_top_n"><?= t('taxi_tariff', 'Night') ?></span>
        </div>
    </div>

    <div class="ap_cont">
        <?
        $disabledCity  = $modelCity->accrual == OptionTariff::ACCRUAL_FIX ? 'disabled' : false;
        $disabledTrack = $modelTrack->accrual == OptionTariff::ACCRUAL_FIX ? 'disabled' : false;
        ?>

        <!--Стоимость посадки-->
        <div class="disable_none">
            <?= Html::activeLabel($modelCity, 'planting_price_day') ?>
            <div class="city">
                <div class="day">
                    <span class="city currency_symbol">
                        <?= Html::tag('span', Html::encode($modelCity->planting_price_day)) ?>
                        <?= Html::encode($currencySymbol) ?>
                    </span>
                </div>
                <div class="night">
                    <span class="city currency_symbol">
                        <?= Html::tag('span', Html::encode($modelCity->planting_price_night)) ?>
                        <?= Html::encode($currencySymbol) ?>
                    </span>
                </div>
            </div>

            <div class="track">
                <div class="day">
                    <span class="track currency_symbol">
                        <?= Html::tag('span', Html::encode($modelTrack->planting_price_day)) ?>
                        <?= html::encode($currencySymbol) ?>
                    </span>
                </div>
                <div class="night">
                    <span class="track currency_symbol">
                        <?= Html::tag('span', Html::encode($modelTrack->planting_price_night)) ?>
                        <?= Html::encode($currencySymbol) ?>
                    </span>
                </div>
            </div>
        </div>

        <!--Включено в стоимость посадки-->
        <div class="planting_include">
            <?= Html::activeLabel($modelCity, 'planting_include_day') ?>
            <div class="city">
                <div class="day">
                    <? $next_select_flag = $modelCity->accrual != OptionTariff::ACCRUAL_TIME ? false : true; ?>
                    <span<? if (!$next_select_flag): ?> style="display:none"<? endif ?>
                            class="change_km_city city time_unit">
                        <?= Html::tag('span', Html::encode($modelCity->planting_include_day)) ?>
                        <?= t('app', 'min.') ?>
                    </span>
                    <span<? if ($next_select_flag): ?> style="display:none"<? endif ?>
                            class="change_km_city city km_unit">
                        <?= Html::tag('span', Html::encode($modelCity->planting_include_day)) ?>
                        <?= t('app', 'km') ?>
                    </span>
                </div>

                <div class="night">
                    <span<? if (!$next_select_flag): ?> style="display:none"<? endif ?>
                            class="change_km_city city time_unit">
                        <?= Html::tag('span', Html::encode($modelCity->planting_include_night)) ?>
                        <?= t('app', 'min.') ?>
                    </span>
                    <span<? if ($next_select_flag): ?> style="display:none"<? endif ?>
                            class="change_km_city city km_unit">
                        <?= Html::tag('span', Html::encode($modelCity->planting_include_night)) ?>
                        <?= t('app', 'km') ?>
                    </span>
                </div>
            </div>

            <div class="track">
                <div class="day">
                    <? $next_select_flag = $modelTrack->accrual != OptionTariff::ACCRUAL_TIME ? false : true; ?>
                    <span<? if (!$next_select_flag): ?> style="display:none"<? endif ?>
                            class="change_km_city track time_unit">
                        <?= Html::tag('span', Html::encode($modelTrack->planting_include_day)) ?>
                        <?= t('app', 'min.') ?>
                    </span>
                    <span<? if ($next_select_flag): ?> style="display:none"<? endif ?>
                            class="change_km_city track km_unit">
                        <?= Html::tag('span', Html::encode($modelTrack->planting_include_day)) ?>
                        <?= t('app', 'km') ?>
                    </span>
                </div>

                <div class="night">
                    <span<? if (!$next_select_flag): ?> style="display:none"<? endif ?>
                            class="change_km_city track time_unit">
                        <?= Html::tag('span', Html::encode($modelTrack->planting_include_night)) ?>
                        <?= t('app', 'min.') ?>
                    </span>
                    <span<? if ($next_select_flag): ?> style="display:none"<? endif ?>
                            class="change_km_city track km_unit">
                        <?= Html::tag('span', Html::encode($modelTrack->planting_include_night)) ?>
                        <?= t('app', 'km') ?>
                    </span>
                </div>
            </div>
        </div>

        <!--Включено в стоимость посадки для смешанного типа-->
        <div<? if ($modelCity->accrual !== OptionTariff::ACCRUAL_MIX && $modelTrack->accrual !== OptionTariff::ACCRUAL_MIX): ?> style="display:none"<? endif ?>
                class="planting_include_time">
            <label></label>
            <div class="city"<? if ($modelCity->accrual !== OptionTariff::ACCRUAL_MIX): ?> style="display:none"<? endif ?>>
                <div class="day">
                    <span>
                        <?= Html::tag('span', Html::encode($modelCity->planting_include_day_time)) ?>
                        <?= t('app', 'min.') ?>
                    </span>
                </div>
                <div class="night">
                    <span>
                        <?= Html::tag('span', Html::encode($modelCity->planting_include_night_time)) ?>
                        <?= t('app', 'min.') ?>
                    </span>
                </div>
            </div>

            <div class="track"<? if ($modelTrack->accrual !== OptionTariff::ACCRUAL_MIX): ?> style="display:none"<? endif ?>>
                <div class="day">
                    <span>
                        <?= Html::tag('span', Html::encode($modelTrack->planting_include_day_time)) ?>
                        <?= t('app', 'min.') ?>
                    </span>
                </div>
                <div class="night">
                    <span>
                        <?= Html::tag('span', Html::encode($modelTrack->planting_include_night_time)) ?>
                        <?= t('app', 'min.') ?>
                    </span>
                </div>
            </div>
        </div>

        <!--Стоимость далее-->
        <div class="next_km_price">
            <?= Html::activeLabel($modelCity, 'next_km_price_day',
                ['class' => $modelCity->accrual == OptionTariff::ACCRUAL_INTERVAL ? 'label_shift' : null]) ?>
            <div class="city tar_dist_and_diap">
                <div class="not_interval_value"
                     <? if ($modelCity->accrual == OptionTariff::ACCRUAL_INTERVAL): ?>style="display:none"<? endif ?>>
                    <div class="day">
                        <?
                        $next_select_flag   = $modelCity->accrual != OptionTariff::ACCRUAL_TIME ? false : true;
                        $cur_next_cost_unit = current($nextCostUnitList);
                        ?>
                        <span<? if (!$next_select_flag): ?> style="display:none"<? endif ?>
                                class="change_r_km_city select city currency_symbol">
                            <?= Html::tag('span', Html::encode($modelCity->next_km_price_day)) ?>
                            <?= Html::tag('span', Html::encode(isset($nextCostUnitList[$modelCity->next_cost_unit_day])
                                ? $nextCostUnitList[$modelCity->next_cost_unit_day] : '')) ?>
                        </span>
                        <span<? if ($next_select_flag): ?> style="display:none"<? endif ?>
                                class="change_r_km_city default_unit city currency_symbol">
                            <?= Html::tag('span', Html::encode($modelCity->next_km_price_day)) ?>
                            <?= Html::encode($currencySymbol) ?>/<?= t('app', 'km') ?>
                        </span>
                    </div>

                    <div class="night">
                        <span<? if (!$next_select_flag): ?> style="display:none"<? endif ?>
                                class="change_r_km_city select city currency_symbol">
                            <?= Html::tag('span', Html::encode($modelCity->next_km_price_night)) ?>
                            <?= Html::tag('span',
                                Html::encode(isset($nextCostUnitList[$modelCity->next_cost_unit_night])
                                    ? $nextCostUnitList[$modelCity->next_cost_unit_night] : '')) ?>
                        </span>
                        <span<? if ($next_select_flag): ?> style="display:none"<? endif ?>
                                class="change_r_km_city default_unit city currency_symbol">
                            <?= Html::tag('span', Html::encode($modelCity->next_km_price_night)) ?>
                            <?= Html::encode($currencySymbol) ?>/<?= t('app', 'km') ?>
                        </span>
                    </div>
                </div>

                <div class="interval"
                     <? if ($modelCity->accrual != OptionTariff::ACCRUAL_INTERVAL): ?>style="display:none"<? endif ?>>
                    <?= $this->render('_interval_read', [
                        'model'           => $modelCity,
                        'interval_day'    => $modelCity->next_km_price_interval_day,
                        'interval_night'  => $modelCity->next_km_price_interval_night,
                        'model_type'      => $modelType,
                        'area'            => 'city',
                        'currency_symbol' => $currencySymbol,
                    ]) ?>
                </div>
            </div>

            <div class="track tar_dist_and_diap">
                <div class="not_interval_value"
                     <? if ($modelTrack->accrual == OptionTariff::ACCRUAL_INTERVAL): ?>style="display:none"<? endif ?>>
                    <div class="day">
                        <? $next_select_flag = $modelTrack->accrual != OptionTariff::ACCRUAL_TIME ? false : true; ?>
                        <span<? if (!$next_select_flag): ?> style="display:none"<? endif ?>
                                class="change_r_km_track track select currency_symbol">
                            <?= Html::tag('span', Html::encode($modelTrack->next_km_price_day)) ?>
                            <?= Html::tag('span', Html::encode(isset($nextCostUnitList[$modelTrack->next_cost_unit_day])
                                ? $nextCostUnitList[$modelTrack->next_cost_unit_day] : '')) ?>
                        </span>

                        <span<? if ($next_select_flag): ?> style="display:none"<? endif ?>
                                class="change_r_km_track track default_unit currency_symbol">
                            <?= Html::tag('span', Html::encode($modelTrack->next_km_price_day)) ?>
                            <?= Html::encode($currencySymbol) ?>/<?= t('app', 'km') ?>
                        </span>
                    </div>

                    <div class="night">
                        <span<? if (!$next_select_flag): ?> style="display:none"<? endif ?>
                                class="change_r_km_track track select currency_symbol">
                            <?= Html::tag('span', Html::encode($modelTrack->next_km_price_night)) ?>
                            <?= Html::tag('span',
                                Html::encode(isset($nextCostUnitList[$modelTrack->next_cost_unit_night])
                                    ? $nextCostUnitList[$modelTrack->next_cost_unit_night] : '')) ?>
                        </span>

                        <span<? if ($next_select_flag): ?> style="display:none"<? endif ?>
                                class="change_r_km_track track default_unit currency_symbol">
                            <?= Html::tag('span', Html::encode($modelTrack->next_km_price_night)) ?>
                            <?= Html::encode($currencySymbol) ?>/<?= t('app', 'km') ?>
                        </span>
                    </div>
                </div>

                <div class="interval"
                     <? if ($modelTrack->accrual != OptionTariff::ACCRUAL_INTERVAL): ?>style="display:none"<? endif ?>>
                    <?= $this->render('_interval_read', [
                        'model'           => $modelTrack,
                        'interval_day'    => $modelTrack->next_km_price_interval_day,
                        'interval_night'  => $modelTrack->next_km_price_interval_night,
                        'model_type'      => $modelType,
                        'area'            => 'track',
                        'currency_symbol' => $currencySymbol,
                    ]) ?>
                </div>
            </div>
        </div>

        <!--Стоимость далее для смешанного типа-->
        <div<? if ($modelCity->accrual !== OptionTariff::ACCRUAL_MIX && $modelTrack->accrual !== OptionTariff::ACCRUAL_MIX): ?> style="display:none"<? endif ?>
                class="next_km_price_time">
            <label></label>
            <div class="city"<? if ($modelCity->accrual !== OptionTariff::ACCRUAL_MIX): ?> style="display:none"<? endif ?>>
                <div class="day">
                    <span class="change_r_km_city day currency_symbol">
                        <?= Html::tag('span', Html::encode($modelCity->next_km_price_day_time)) ?>
                        <?= Html::tag('span', Html::encode(isset($nextCostUnitList[$modelCity->next_cost_unit_day_time])
                            ? $nextCostUnitList[$modelCity->next_cost_unit_day_time] : '')) ?>
                    </span>
                </div>
                <div class="night">
                    <span class="change_r_km_city currency_symbol">
                        <?= Html::tag('span', Html::encode($modelCity->next_km_price_night_time)) ?>
                        <?= Html::tag('span',
                            Html::encode(isset($nextCostUnitList[$modelCity->next_cost_unit_night_time])
                                ? $nextCostUnitList[$modelCity->next_cost_unit_night_time] : '')) ?>
                    </span>
                </div>
            </div>

            <div class="track"<? if ($modelTrack->accrual !== OptionTariff::ACCRUAL_MIX): ?> style="display:none"<? endif ?>>
                <div class="day">
                    <span class="change_r_km_track track day currency_symbol">
                        <?= Html::tag('span', Html::encode($modelTrack->next_km_price_day_time)) ?>
                        <?= Html::tag('span',
                            Html::encode(isset($nextCostUnitList[$modelTrack->next_cost_unit_day_time])
                                ? $nextCostUnitList[$modelTrack->next_cost_unit_day_time] : '')) ?>
                    </span>
                </div>
                <div class="night">
                    <span class="change_r_km_track track currency_symbol">
                        <?= Html::tag('span', Html::encode($modelTrack->next_km_price_night_time)) ?>
                        <?= Html::tag('span',
                            Html::encode(isset($nextCostUnitList[$modelTrack->next_cost_unit_night_time])
                                ? $nextCostUnitList[$modelTrack->next_cost_unit_night_time] : '')) ?>
                    </span>
                </div>
            </div>
        </div>

        <!--Минимальная стоимость поездки-->
        <div>
            <?= Html::activeLabel($modelCity, 'min_price_day') ?>
            <div class="city">
                <div class="day">
                    <span class="city currency_symbol">
                        <?= Html::tag('span', Html::encode($modelCity->min_price_day)) ?>
                        <?= Html::encode($currencySymbol) ?>
                    </span>
                </div>
                <div class="night">
                    <span class="city currency_symbol">
                        <?= Html::tag('span', Html::encode($modelCity->min_price_night)) ?>
                        <?= Html::encode($currencySymbol) ?>
                    </span>
                </div>
            </div>

            <div class="track">
                <div class="day">
                    <span class="track currency_symbol">
                        <?= Html::tag('span', Html::encode($modelTrack->min_price_day)) ?>
                        <?= Html::encode($currencySymbol) ?>
                    </span>
                </div>
                <div class="night">
                    <span class="track currency_symbol">
                        <?= Html::tag('span', Html::encode($modelTrack->min_price_night)) ?>
                        <?= Html::encode($currencySymbol) ?>
                    </span>
                </div>
            </div>
        </div>

        <!--Стоимость 1 км до адреса подачи-->
        <div>
            <?= Html::activeLabel($modelCity, 'supply_price_day') ?>
            <div style="width: 35.75%; float: left; height: 20px"></div>
            <div class="track">
                <div class="day">
                    <span class="track currency_symbol">
                        <?= Html::tag('span', Html::encode($modelTrack->supply_price_day)) ?>
                        <?= Html::encode($currencySymbol) ?>
                    </span>
                </div>
                <div class="night">
                    <span class="track currency_symbol">
                        <?= Html::tag('span', Html::encode($modelTrack->supply_price_night)) ?>
                        <?= Html::encode($currencySymbol) ?>
                    </span>
                </div>
            </div>
        </div>

        <!--Бесплатное время ожидания до начала поездки-->
        <div class="disable_none">
            <?= Html::activeLabel($modelCity, 'wait_time_day') ?>
            <div class="city">
                <div class="day">
                    <span class="city">
                        <?= Html::tag('span', Html::encode($modelCity->wait_time_day)) ?>
                        <?= t('app', 'min.') ?>
                    </span>
                </div>
                <div class="night">
                    <span class="city">
                        <?= Html::tag('span', Html::encode($modelCity->wait_time_night)) ?>
                        <?= t('app', 'min.') ?>
                    </span>
                </div>
            </div>
            <div class="track">
                <div class="day">
                    <span class="track">
                        <?= Html::tag('span', Html::encode($modelTrack->wait_time_day)) ?>
                        <?= t('app', 'min.') ?>
                    </span>
                </div>
                <div class="night">
                    <span class="track">
                        <?= Html::tag('span', Html::encode($modelTrack->wait_time_night)) ?>
                        <?= t('app', 'min.') ?>
                    </span>
                </div>
            </div>
        </div>

        <!--Бесплатное время ожидания во время поездки за 1 раз, от-->
        <div>
            <?= Html::activeLabel($modelCity, 'wait_driving_time_day') ?>
            <div class="city">
                <div class="day">
                    <span class="city">
                        <?= Html::tag('span', Html::encode($modelCity->wait_driving_time_day)) ?>
                        <?= t('app', 'sec.') ?>
                    </span>
                </div>
                <div class="night">
                    <span class="city">
                        <?= Html::tag('span', Html::encode($modelCity->wait_driving_time_night)) ?>
                        <?= t('app', 'sec.') ?>
                    </span>
                </div>
            </div>
            <div class="track">
                <div class="day">
                    <span class="track">
                        <?= Html::tag('span', Html::encode($modelTrack->wait_driving_time_day)) ?>
                        <?= t('app', 'sec.') ?>
                    </span>
                </div>
                <div class="night">
                    <span class="track">
                        <?= Html::tag('span', Html::encode($modelTrack->wait_driving_time_night)) ?>
                        <?= t('app', 'sec.') ?>
                    </span>
                </div>
            </div>
        </div>

        <!--Стоимость 1 минуты ожидания-->
        <div class="disable_none">
            <?= Html::activeLabel($modelCity, 'wait_price_day') ?>
            <div class="city">
                <div class="day">
                    <span class="city currency_symbol">
                        <?= Html::tag('span', Html::encode($modelCity->wait_price_day)) ?>
                        <?= Html::encode($currencySymbol) ?>
                    </span>
                </div>
                <div class="night">
                    <span class="city currency_symbol">
                        <?= Html::tag('span', Html::encode($modelCity->wait_price_night)) ?>
                        <?= Html::encode($currencySymbol) ?>
                    </span>
                </div>
            </div>

            <div class="track">
                <div class="day">
                    <span class="track currency_symbol">
                        <?= Html::tag('span', Html::encode($modelTrack->wait_price_day)) ?>
                        <?= Html::encode($currencySymbol) ?>
                    </span>
                </div>
                <div class="night">
                    <span class="track currency_symbol">
                        <?= Html::tag('span', Html::encode($modelTrack->wait_price_night)) ?>
                        <?= Html::encode($currencySymbol) ?>
                    </span>
                </div>
            </div>
        </div>

        <!--Скорость, при которой вкл. ожидание-->
        <div>
            <?= Html::activeLabel($modelCity, 'speed_downtime_day') ?>
            <div class="city">
                <div class="day">
                    <span class="city">
                        <?= Html::tag('span', Html::encode($modelCity->speed_downtime_day)) ?>
                        <?= t('app', 'km/h') ?>
                    </span>
                </div>
                <div class="night">
                    <span class="city">
                        <?= Html::tag('span', Html::encode($modelCity->speed_downtime_night)) ?>
                        <?= t('app', 'km/h') ?>
                    </span>
                </div>
            </div>

            <div class="track">
                <div class="day">
                    <span class="track">
                        <?= Html::tag('span', Html::encode($modelTrack->speed_downtime_day)) ?>
                        <?= t('app', 'km/h') ?>
                    </span>
                </div>
                <div class="night">
                    <span class="track">
                        <?= Html::tag('span', Html::encode($modelTrack->speed_downtime_night)) ?>
                        <?= t('app', 'km/h') ?>
                    </span>
                </div>
            </div>
        </div>

        <!--Округление-->
        <div class="disable_none">
            <?= Html::activeLabel($modelCity, 'rounding_day') ?>
            <div class="city">
                <div class="day">
                    <span class="city currency_symbol">
                        <?= Html::tag('span', Html::encode($modelCity->rounding_day)) ?>
                        <?= Html::encode($currencySymbol) ?>
                    </span>
                </div>
                <div class="night">
                    <span class="city currency_symbol">
                        <?= Html::tag('span', Html::encode($modelCity->rounding_night)) ?>
                        <?= Html::encode($currencySymbol) ?>
                    </span>
                </div>
            </div>

            <div class="track">
                <div class="day">
                    <span class="track currency_symbol">
                        <?= Html::tag('span', Html::encode($modelTrack->rounding_day)) ?>
                        <?= Html::encode($currencySymbol) ?>
                    </span>
                </div>
                <div class="night">
                    <span class="track currency_symbol">
                        <?= Html::tag('span', Html::encode($modelTrack->rounding_night)) ?>
                        <?= Html::encode($currencySymbol) ?>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<h2><?= t('taxi_tariff', 'Additional options') ?></h2>
<div class="ap_cont js-options">
    <? foreach ($form_data['ADD_OPTIONS'] as $id => $name): ?>
        <div class="check_option" data-option-id="<?= $id ?>">
            <label>
                <input name="<?= $modelType ?>[AdditionalOption][active][]"
                       <? if (isset($addOptions[$modelType][$id])): ?>checked="checked"<? endif ?> type="checkbox"
                       disabled="disabled" value="<?= $id ?>"/> <?= Html::encode($name) ?>
            </label>
            <span class="currency_symbol">
                <?= Html::tag('span',
                    Html::encode(isset($addOptions[$modelType][$id]) ? $addOptions[$modelType][$id]->price : 0)) ?>
                <?= Html::encode($currencySymbol) ?>
            </span>
        </div>
    <? endforeach; ?>
</div>

<div class="fix_tariff"
     <? if (!isset($arOption) || ($arOption[$modelType][OptionTariff::AREA_CITY]['accrual'] != OptionTariff::ACCRUAL_FIX && $arOption[$modelType][OptionTariff::AREA_TRACK]['accrual'] != OptionTariff::ACCRUAL_FIX)): ?>style="display: none"<? endif ?>
     data-url="<?= yii\helpers\Url::to('/tariff/tariff-client/get-fix-tariff') ?>">
    <h2><?= t('taxi_tariff', 'Fix tariffs') ?></h2>
    <div class="fix_city"
         <? if (!isset($arOption) || ($arOption[$modelType][OptionTariff::AREA_CITY]['accrual'] != OptionTariff::ACCRUAL_FIX)): ?>style="display: none"<? endif ?>>
        <h3><?= t('taxi_tariff', 'City') ?></h3>
        <div class="ajax_city">
            <?
            if (isset($arOption) && $arOption[$modelType][OptionTariff::AREA_CITY]['accrual'] == OptionTariff::ACCRUAL_FIX) {
                echo $this->render('fixTariff_read', [
                    'arParking'      => $fixCity,
                    'parking_type'   => 'city',
                    'tariff_type'    => $modelType,
                    'tariff'         => $arOption[$modelType][OptionTariff::AREA_CITY],
                    'currencySymbol' => $currencySymbol,
                ]);
            }
            ?>
        </div>
    </div>

    <div class="fix_track"
         <? if (!isset($arOption) || ($arOption[$modelType][OptionTariff::AREA_TRACK]['accrual'] != OptionTariff::ACCRUAL_FIX)): ?>style="display: none"<? endif ?>>
        <h3><?= t('taxi_tariff', 'Track') ?></h3>
        <div class="ajax_track">
            <?
            if (isset($arOption) && $arOption[$modelType][OptionTariff::AREA_TRACK]['accrual'] == OptionTariff::ACCRUAL_FIX) {
                echo $this->render('fixTariff_read', [
                    'arParking'      => $fixTrack,
                    'parking_type'   => 'outCity',
                    'tariff_type'    => $modelType,
                    'tariff'         => $arOption[$modelType][OptionTariff::AREA_TRACK],
                    'currencySymbol' => $currencySymbol,
                ]);
            }
            ?>
        </div>
    </div>
</div>

<h2 class="check_spoler">
    <label>
        <input
            <? if (isset($arOption) && !empty($arOption[$modelType][OptionTariff::AREA_AIRPORT])): ?>checked="checked"<? endif ?>
            disabled="disabled" data-option="airport" type="checkbox"/>
    </label>
    <a data-spolerid="a1_<?= $modelType ?>"
       class="spoler <? if (!isset($arOption) || empty($arOption[$modelType][OptionTariff::AREA_AIRPORT])): ?>locked<? endif ?>">
        <?= t('taxi_tariff', 'Airport fix tariffs') ?>
    </a>
</h2>

<section class="spoler_content" id="a1_<?= $modelType ?>">
    <div class="ap_cont spoler_wide ajax_airport">
        <?
        if (isset($arOption) && !empty($arOption[$modelType][OptionTariff::AREA_AIRPORT])) {
            echo $this->render('fixTariff_read', [
                'arParking'      => $fixAiroport,
                'parking_type'   => 'airport',
                'tariff_type'    => $modelType,
                'tariff'         => $arOption[$modelType][OptionTariff::AREA_AIRPORT],
                'currencySymbol' => $currencySymbol,
            ]);
        }
        ?>
    </div>
</section>

<h2 class="check_spoler">
    <label>
        <input
            <? if (isset($arOption) && !empty($arOption[$modelType][OptionTariff::AREA_RAILWAY])): ?>checked="checked"<? endif ?>
            disabled="disabled" data-option="station" type="checkbox"/>
    </label>
    <a data-spolerid="s1_<?= $modelType ?>"
       class="spoler <? if (!isset($arOption) || empty($arOption[$modelType][OptionTariff::AREA_RAILWAY])): ?>locked<? endif ?>">
        <?= t('taxi_tariff', 'Railway fix tariffs') ?>
    </a>
</h2>

<section class="spoler_content" id="s1_<?= $modelType ?>">
    <div class="ap_cont spoler_wide ajax_station">
        <?
        if (isset($arOption) && !empty($arOption[$modelType][OptionTariff::AREA_RAILWAY])) {
            echo $this->render('fixTariff_read', [
                'arParking'      => $fixRailway,
                'parking_type'   => 'station',
                'tariff_type'    => $modelType,
                'tariff'         => $arOption[$modelType][OptionTariff::AREA_RAILWAY],
                'currencySymbol' => $currencySymbol,
            ]);
        }
        ?>
    </div>
</section>