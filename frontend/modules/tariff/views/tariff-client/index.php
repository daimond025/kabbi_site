<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $tariff app\modules\tariff\models\TaxiTariff */

$this->title = Yii::t('app', 'Tariff for clients');
?>
<?php
if (app()->user->can('clientTariff')) {
    echo Html::a(t('app', 'Add'), ['/client/tariff/create'], ['class' => 'button', 'style' => 'float: right; position: relative; top: 5px;']);
}
?>

<h1><?= Html::encode($this->title) ?></h1>
<section class="main_tabs">
    <div class="tabs_links">
        <ul>
            <li><a href="#active" class="t01"><?= t('app', 'Active') ?></a></li>
            <li><a href="#blocked" class="t02" data-href="<?= Url::to('list-blocked') ?>"><?= t('app', 'Blocked') ?></a>
            </li>
        </ul>
    </div>
    <div class="tabs_content">
        <div id="t01" data-view="">
            <?= $this->render('_groupList', compact('dataProvider', 'searchModel', 'arr_dataProvider', 'city_list', 'pjaxId')) ?>
        </div>
        <div id="t02" data-view="blocked"></div>
    </div>
</section>
