<?php

use app\modules\tariff\models\OptionTariff;
use frontend\widgets\position\PositionChooser;
use frontend\widgets\validity_input\ValidityInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $tariff app\modules\tariff\models\TaxiTariff */
/* @var $currentCity app\modules\tariff\models\OptionTariff */
/* @var $currentTrack app\modules\tariff\models\OptionTariff */
/* @var $holidaysCity app\modules\tariff\models\OptionTariff */
/* @var $holidaysTrack app\modules\tariff\models\OptionTariff */
/* @var $exclusionsCity app\modules\tariff\models\OptionTariff */
/* @var $exclusionsTrack app\modules\tariff\models\OptionTariff */
/* @var $form yii\widgets\ActiveForm */
/* @var array $form_data */

$tariff_city_id = $tariff->city_list;
?>

<section class="row">
    <div class="row_label">
        <label><?= t('logo', 'Фото тарифа') ?></label>
        <?php echo $suggest?>
    </div>
    <div class="row_input">
            <span class="file-input">
                <? if ($tariff->isFileExists($tariff->logo)): ?>
                    <div class="input_file_logo input_file_loaded">
                        <a class="logo" href="<?= $tariff->getPicturePath($tariff->logo) ?>">
                            <?= $tariff->getPictureHtml($tariff->logo, false) ?>
                        </a>
                    </div>
                <? else: ?>
                    <div class="input_file_logo"></div>
                <? endif ?>
            </span>
    </div>
</section>

<div class="non-editable">
    <section class="row">
        <div class="row_label">
            <?= Html::activeLabel($tariff, 'name') ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint',
                        "Displays: <br> - Operators; <br> - In the iOS and Android client application. Length is not more than 14 characters; <br> - In the artist's application; <br> - On the site."); ?>
                </span>
            </a>
        </div>
        <div class="row_input">
            <?= Html::tag('span', Html::encode($tariff->name), ['dir' => 'auto', 'disabled' => true]) ?>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($tariff, 'city_list') ?></div>
        <div class="row_input">
            <?= Html::tag('span',
                Html::encode(isset($form_data['CITY'][$tariff->city_list]) ? $form_data['CITY'][$tariff->city_list] : '')) ?>
        </div>
    </section>

    <?= PositionChooser::widget([
        'model'                 => $tariff,
        'carClassMap'           => $form_data['TYPE'],
        'carPositionList'       => $form_data['CAR_POSITION_LIST'],
        'positionMap'           => $form_data['POSITION_MAP'],
        'positionDisabled'      => true,
        'carClassDisabled'      => true,
        'positionClassDisabled' => true,
    ]) ?>

    <section class="row">
        <div class="row_label">
            <?= Html::activeLabel($tariff, 'group_id') ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint',
                        'Grouping of tariffs for convenient use and display of tariffs. You can create groups in the tariff group section.'); ?>
                </span>
            </a>
        </div>
        <div class="row_input">
            <?= Html::tag('span',
                Html::encode(isset($form_data['GROUP'][$tariff->group_id]) ? $form_data['GROUP'][$tariff->group_id] : '')) ?>
        </div>
    </section>

    <section class="row">
        <div class="row_label">
            <?= Html::activeLabel($tariff, 'description') ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint',
                        'The tariff description is displayed in the mobile application for the customer and the form of the order from the operator. Write down the basic conditions that you offer to customers. For example: landing 100 ₽, then 5 ₽ / 1 km, free waiting 5 minutes.'); ?>
                </span>
            </a>
        </div>
        <div class="row_input">
            <?= Html::tag('span', Html::encode($tariff->description), ['dir' => 'auto']) ?>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::label(t('taxi_tariff', 'Where available the tariff')) ?></div>
        <div class="row_input">
            <div>
                <?= Html::activeCheckbox($tariff, 'enabled_site', ['disabled' => true]) ?>
            </div>
            <div>
                <?= Html::activeCheckbox($tariff, 'enabled_app', ['disabled' => true]) ?>
            </div>
            <div>
                <?= Html::activeCheckbox($tariff, 'enabled_operator', ['disabled' => true]) ?>
            </div>
            <div>
                <?= Html::activeCheckbox($tariff, 'enabled_bordur', ['disabled' => true]) ?>
            </div>
            <div>
                <?= Html::activeCheckbox($tariff, 'enabled_cabinet', ['disabled' => true]) ?>
            </div>
        </div>
    </section>

    <section class="row">
        <div class="row_label">
            <?= Html::activeLabel($tariff, 'sort') ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint',
                        'Sort determines the order of the tariff in the list. It is made for convenience of use in the mobile application for the client and the executor, in the form of the order for the operator. The smaller the digit, the higher the item in the list.'); ?>
                </span>
            </a>
        </div>
        <div class="row_input">
            <?= Html::tag('span', Html::encode($tariff->sort)) ?>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($tariff, 'type') ?></div>
        <div class="row_input">
            <?= Html::tag('span',
                Html::encode(isset($form_data['ACCESS'][$tariff->type]) ? $form_data['ACCESS'][$tariff->type] : ''),
                ['class' => 'js-type']) ?>
        </div>
    </section>

    <section class="row" <?= !in_array($tariff->type, ['COMPANY', 'COMPANY_CORP_BALANCE'],
        false) ? 'style="display: none"' : '' ?>>
        <div class="row_label"></div>
        <div class="row_input">
            <?= Html::activeCheckboxList($tariff, 'company_ids', $form_data['COMPANIES_NAME'], [
                'class' => 'select_checkbox',
                'item'  => function ($index, $label, $name, $checked, $value) use ($tariff, $form_data) {
                    $input     = Html::checkbox($name, $checked, ['value' => $value, 'disabled' => true]);
                    $label     = Html::label($input . $label);
                    $innerHtml = Html::tag('div', $label, [
                    ]);

                    return $innerHtml;
                },
            ]) ?>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($tariff, 'auto_downtime') ?></div>
        <div class="row_input">
            <?= Html::activeRadioList($tariff, 'auto_downtime', $form_data['AUTO_DOWNTIME'],
                ['itemOptions' => ['disabled' => true]]); ?>
        </div>
    </section>

    <div class="tabs_links">
        <ul>
            <li><a href="#current_days" class="t01">
                    <?= t('taxi_tariff', 'Ordinary days') ?></a></li>
            <li><a href="#weekends_and_holidays" class="t02">
                    <?= t('taxi_tariff', 'Weekends, holidays') ?></a></li>
            <li><a href="#exclusion" class="t03"><?= t('taxi_tariff', 'Exclusions') ?></a></li>
        </ul>
    </div>

    <div class="tabs_content">
        <!--Обычные дни-->
        <div id="t01" data-type="<?= OptionTariff::CURRENT_TYPE ?>">
            <?= $this->render('_option_form_read', [
                'modelCity'      => $currentCity,
                'modelTrack'     => $currentTrack,
                'modelType'      => OptionTariff::CURRENT_TYPE,
                'form_data'      => $form_data,
                'addOptions'     => $addOptions,
                'arOption'       => $arOption,
                'fixCity'        => $fixCity,
                'fixTrack'       => $fixTrack,
                'fixAiroport'    => $fixAiroport,
                'fixRailway'     => $fixRailway,
                'tariff_city_id' => $tariff_city_id,
            ]) ?>
        </div>

        <!--Выходные, праздники-->
        <div id="t02" data-type="<?= OptionTariff::HOLIDAYS_TYPE ?>">
            <section class="row">
                <div class="row_label"><label><?= t('app', 'Enable') ?></label></div>
                <div class="row_input"><?= Html::activeCheckbox($holidaysCity, 'active', [
                        'label'    => null,
                        'disabled' => true,
                    ]) ?></div>
            </section>
            <section id="holidays" <? if (!$holidaysCity->active): ?>style="display: none"<? endif ?>>
                <?= ValidityInput::widget([
                    'label'                   => t('taxi_tariff', 'Weekends, holidays'),
                    'data'                    => $tariffActiveDate[OptionTariff::HOLIDAYS_TYPE],
                    'input_name'              => OptionTariff::HOLIDAYS_TYPE,
                    'show_upload_celebration' => true,
                    'model'                   => $holidaysCity,
                    'attr_name'               => 'action_dates',
                    'read_view'               => true,
                ]) ?>

                <?= $this->render('_option_form_read', [
                    'modelCity'      => $holidaysCity,
                    'modelTrack'     => $holidaysTrack,
                    'modelType'      => OptionTariff::HOLIDAYS_TYPE,
                    'form_data'      => $form_data,
                    'addOptions'     => $addOptions,
                    'arOption'       => $arOption,
                    'fixCity'        => $fixCity,
                    'fixTrack'       => $fixTrack,
                    'fixAiroport'    => $fixAiroport,
                    'fixRailway'     => $fixRailway,
                    'tariff_city_id' => $tariff_city_id,
                ]) ?>
            </section>
        </div>

        <!--Исключения-->
        <div id="t03" data-type="<?= OptionTariff::EXCEPTIONS_TYPE ?>">
            <section class="row">
                <div class="row_label"><label><?= t('app', 'Enable') ?></label></div>
                <div class="row_input"><?= Html::activeCheckbox($exclusionsCity, 'active', [
                        'label'    => null,
                        'disabled' => true,
                    ]) ?></div>
            </section>
            <section id="exclusions" <? if (!$exclusionsCity->active): ?>style="display: none"<? endif ?>>
                <?= ValidityInput::widget([
                    'label'      => t('taxi_tariff', 'Exclusions'),
                    'data'       => $tariffActiveDate[OptionTariff::EXCEPTIONS_TYPE],
                    'input_name' => OptionTariff::EXCEPTIONS_TYPE,
                    'model'      => $exclusionsCity,
                    'attr_name'  => 'action_dates',
                    'read_view'  => true,
                ]) ?>

                <?= $this->render('_option_form_read', [
                    'modelCity'      => $exclusionsCity,
                    'modelTrack'     => $exclusionsTrack,
                    'modelType'      => OptionTariff::EXCEPTIONS_TYPE,
                    'form_data'      => $form_data,
                    'addOptions'     => $addOptions,
                    'arOption'       => $arOption,
                    'fixCity'        => $fixCity,
                    'fixTrack'       => $fixTrack,
                    'fixAiroport'    => $fixAiroport,
                    'fixRailway'     => $fixRailway,
                    'tariff_city_id' => $tariff_city_id,
                ]) ?>
            </section>
        </div>
    </div>

    <section class="submit_form">
        <div>
            <label>
                <?= Html::activeCheckbox($tariff, 'block', ['label' => null, 'disabled' => true]) ?>
                <b><?= t('app', 'Block') ?></b>
            </label>
        </div>
    </section>

</div>
