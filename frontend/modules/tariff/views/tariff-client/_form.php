<?php

use app\modules\tariff\models\OptionTariff;
use frontend\widgets\position\PositionChooser;
use frontend\widgets\validity_input\ValidityInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $tariff app\modules\tariff\models\TaxiTariff */
/* @var $currentCity app\modules\tariff\models\OptionTariff */
/* @var $currentTrack app\modules\tariff\models\OptionTariff */
/* @var $holidaysCity app\modules\tariff\models\OptionTariff */
/* @var $holidaysTrack app\modules\tariff\models\OptionTariff */
/* @var $exclusionsCity app\modules\tariff\models\OptionTariff */
/* @var $exclusionsTrack app\modules\tariff\models\OptionTariff */
/* @var $form yii\widgets\ActiveForm */
/* @var array $form_data */

$tariff_city_id = $tariff->city_list;

$form = ActiveForm::begin([
    'id'                     => 'tariff_current-form',
    'errorCssClass'          => 'input_error',
    'successCssClass'        => 'input_success',
    'enableClientValidation' => true,
    'validateOnBlur'         => false,
    'options'                => ['enctype' => 'multipart/form-data'],
]); ?>

<? if (!$tariff->isNewRecord): ?>
    <?= \frontend\widgets\file\Logo::widget([
        'form'         => $form,
        'model'        => $tariff,
        'attribute'    => 'logo',
        'min_height'   => 160,
        'min_width'    => 360,
        'ajax'         => true,
        'uploadAction' => Url::to(['/lib/tariff-client/upload-logo/', 'id' => $tariff->tariff_id]),
        'suggest'      => $suggest,
    ]) ?>
<? endif; ?>

    <section class="row">
        <?= $form->field($tariff, 'name')->begin(); ?>
        <div class="row_label">
            <?= Html::activeLabel($tariff, 'name') ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint',
                        "Displays: <br> - Operators; <br> - In the iOS and Android client application. Length is not more than 14 characters; <br> - In the artist's application; <br> - On the site."); ?>
                </span>
            </a>
        </div>
        <div class="row_input">
            <?= Html::activeTextInput($tariff, 'name', ['class' => 'not_valid', 'dir' => 'auto']) ?>
        </div>
        <?= $form->field($tariff, 'name')->end(); ?>
    </section>

    <section class="row">
        <?= $form->field($tariff, 'city_list')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($tariff, 'city_list') ?></div>
        <div class="row_input">
            <?= Html::activeDropDownList($tariff, 'city_list', $form_data['CITY'], [
                'data-placeholder' => Html::encode(!empty($tariff_city_id) ? $form_data['CITY'][$tariff_city_id] : t('app',
                    'Choose')),
                'class'            => 'default_select',
                'name'             => 'TaxiTariff[city_list][]',
                'disabled'         => $tariff->isNewRecord ? false : 'disabled',
            ]) ?>
            <?= $form->field($tariff, 'city_list')->end(); ?>
        </div>
    </section>

<?= PositionChooser::widget([
    'form'                  => $form,
    'model'                 => $tariff,
    'carClassMap'           => $form_data['TYPE'],
    'carPositionList'       => $form_data['CAR_POSITION_LIST'],
    'positionMap'           => $form_data['POSITION_MAP'],
    'positionDisabled'      => !$tariff->isNewRecord,
    'carClassDisabled'      => !$tariff->isNewRecord,
    'positionClassDisabled' => !$tariff->isNewRecord,
]) ?>

    <section class="row">
        <?= $form->field($tariff, 'group_id')->begin(); ?>
        <div class="row_label">
            <?= Html::activeLabel($tariff, 'group_id') ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint',
                        'Grouping of tariffs for convenient use and display of tariffs. You can create groups in the tariff group section.'); ?>
                </span>
            </a>
        </div>
        <div class="row_input">
            <?= Html::activeDropDownList($tariff, 'group_id', $form_data['GROUP'], [
                'data-placeholder' => Html::encode(!empty($tariff->group_id) ? $form_data['GROUP'][$tariff->group_id] : current($form_data['GROUP'])),
                'class'            => 'default_select',
            ]) ?>
        </div>
        <?= $form->field($tariff, 'group_id')->end(); ?>
    </section>

    <section class="row">
        <?= $form->field($tariff, 'description')->begin(); ?>
        <div class="row_label">
            <?= Html::activeLabel($tariff, 'description') ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint',
                        'The tariff description is displayed in the mobile application for the customer and the form of the order from the operator. Write down the basic conditions that you offer to customers. For example: landing 100 ₽, then 5 ₽ / 1 km, free waiting 5 minutes.'); ?>
                </span>
            </a>
        </div>
        <div class="row_input">
            <?= Html::activeTextarea($tariff, 'description', ['rows' => 5, 'maxlength' => 1000, 'dir' => 'auto']) ?>
        </div>
        <?= $form->field($tariff, 'description')->end(); ?>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::label(t('taxi_tariff', 'Where available the tariff')) ?></div>
        <div class="row_input">
            <?= $form->field($tariff, 'enabled_site')->begin(); ?>
            <?= Html::activeCheckbox($tariff, 'enabled_site') ?>
            <?= $form->field($tariff, 'enabled_site')->end(); ?>

            <?= $form->field($tariff, 'enabled_app')->begin(); ?>
            <?= Html::activeCheckbox($tariff, 'enabled_app') ?>
            <?= $form->field($tariff, 'enabled_app')->end(); ?>

            <?= $form->field($tariff, 'enabled_operator')->begin(); ?>
            <?= Html::activeCheckbox($tariff, 'enabled_operator') ?>
            <?= $form->field($tariff, 'enabled_operator')->end(); ?>

            <?= $form->field($tariff, 'enabled_bordur')->begin(); ?>
            <?= Html::activeCheckbox($tariff, 'enabled_bordur') ?>
            <?= $form->field($tariff, 'enabled_bordur')->end(); ?>

            <?= $form->field($tariff, 'enabled_cabinet')->begin(); ?>
            <?= Html::activeCheckbox($tariff, 'enabled_cabinet') ?>
            <?= $form->field($tariff, 'enabled_cabinet')->end(); ?>
        </div>
    </section>

    <section class="row">
        <?= $form->field($tariff, 'sort')->begin(); ?>
        <div class="row_label">
            <?= Html::activeLabel($tariff, 'sort') ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint',
                        'Sort determines the order of the tariff in the list. It is made for convenience of use in the mobile application for the client and the executor, in the form of the order for the operator. The smaller the digit, the higher the item in the list.'); ?>
                </span>
            </a>
        </div>
        <div class="row_input">
            <?= Html::activeTextInput($tariff, 'sort') ?>
        </div>
        <?= $form->field($tariff, 'sort')->end(); ?>
    </section>

    <section class="row">
        <?= $form->field($tariff, 'type')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($tariff, 'type') ?></div>
        <div class="row_input">
            <?= Html::activeDropDownList($tariff, 'type', $form_data['ACCESS'], [
                'data-placeholder' => Html::encode(!empty($tariff->type) ? $form_data['ACCESS'][$tariff->type] : current($form_data['ACCESS'])),
                'class'            => 'default_select js-type',
            ]) ?>
        </div>
        <?= $form->field($tariff, 'type')->end(); ?>
    </section>

    <section class="row js-row-company">
        <?= $form->field($tariff, 'company_ids')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($tariff, 'company_ids') ?></div>
        <div class="row_input">
            <?= Html::activeCheckboxList($tariff, 'company_ids', $form_data['COMPANIES_NAME'], [
                'class' => 'select_checkbox',
                'item'  => function ($index, $label, $name, $checked, $value) use ($tariff, $form_data) {
                    $cityId    = $form_data['COMPANIES_CITY_ID'][$value];
                    $input     = Html::checkbox($name, $checked, ['value' => $value]);
                    $label     = Html::label($input . $label);
                    $innerHtml = Html::tag('div', $label, [
                        'data-city-id' => $cityId,
                        'class'        => 'js-item-company',
                    ]);

                    return $innerHtml;
                },
            ]) ?>
            <?= Html::error($tariff, 'company_ids',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']) ?>
        </div>
        <?= $form->field($tariff, 'company_ids')->end(); ?>
    </section>

    <section class="row">
        <?= $form->field($tariff, 'auto_downtime')->begin(); ?>
        <div class="row_label">
            <?= Html::activeLabel($tariff, 'auto_downtime') ?>
        </div>
        <div class="row_input">
            <?= Html::activeRadioList($tariff, 'auto_downtime', $form_data['AUTO_DOWNTIME'],
                ['class' => 'js-auto-downtime']); ?>
        </div>
        <?= $form->field($tariff, 'auto_downtime')->end(); ?>
    </section>

    <div class="tabs_links">
        <ul>
            <li><a href="#current_days" class="t01"><?= t('taxi_tariff', 'Ordinary days') ?></a></li>
            <li><a href="#weekends_and_holidays" class="t02"><?= t('taxi_tariff', 'Weekends, holidays') ?></a></li>
            <li><a href="#exclusion" class="t03"><?= t('taxi_tariff', 'Exclusions') ?></a></li>
        </ul>
    </div>
    <div class="tabs_content">
        <!--Обычные дни-->
        <div id="t01" data-type="<?= OptionTariff::CURRENT_TYPE ?>">
            <?= $this->render('_option_form', [
                'modelCity'      => $currentCity,
                'modelTrack'     => $currentTrack,
                'modelType'      => OptionTariff::CURRENT_TYPE,
                'form_data'      => $form_data,
                'addOptions'     => $addOptions,
                'arOption'       => $arOption,
                'fixCity'        => $fixCity,
                'fixTrack'       => $fixTrack,
                'fixAiroport'    => $fixAiroport,
                'fixRailway'     => $fixRailway,
                'tariff_city_id' => $tariff_city_id,
            ]) ?>
        </div>
        <!--Выходные, праздники-->
        <div id="t02" data-type="<?= OptionTariff::HOLIDAYS_TYPE ?>">
            <section class="row">
                <div class="row_label"><label><?= t('app', 'Enable') ?></label></div>
                <div class="row_input"><?= Html::activeCheckbox($holidaysCity, 'active', [
                        'name'         => OptionTariff::HOLIDAYS_TYPE . '[active]',
                        'class'        => 'switch',
                        'data-section' => 'holidays',
                        'label'        => null,
                        'id'           => 'activity_' . strtolower(OptionTariff::HOLIDAYS_TYPE),
                    ]) ?></div>
            </section>
            <section id="holidays" <? if (!$holidaysCity->active): ?>style="display: none"<? endif ?>>
                <?= $form->field($holidaysCity, 'action_dates')->begin(); ?>
                <?= ValidityInput::widget([
                    'id'                      => 'add_holidays',
                    'label'                   => t('taxi_tariff', 'Weekends, holidays'),
                    'data'                    => $tariffActiveDate[OptionTariff::HOLIDAYS_TYPE],
                    'input_name'              => OptionTariff::HOLIDAYS_TYPE,
                    'show_upload_celebration' => true,
                    'model'                   => $holidaysCity,
                    'attr_name'               => 'action_dates',
                ]) ?>
                <?= $form->field($holidaysCity, 'action_dates')->end(); ?>
                <?= $this->render('_option_form', [
                    'modelCity'      => $holidaysCity,
                    'modelTrack'     => $holidaysTrack,
                    'modelType'      => OptionTariff::HOLIDAYS_TYPE,
                    'form_data'      => $form_data,
                    'addOptions'     => $addOptions,
                    'arOption'       => $arOption,
                    'fixCity'        => $fixCity,
                    'fixTrack'       => $fixTrack,
                    'fixAiroport'    => $fixAiroport,
                    'fixRailway'     => $fixRailway,
                    'tariff_city_id' => $tariff_city_id,
                ]) ?>
            </section>
        </div>
        <!--Исключения-->
        <div id="t03" data-type="<?= OptionTariff::EXCEPTIONS_TYPE ?>">
            <section class="row">
                <div class="row_label"><label><?= t('app', 'Enable') ?></label></div>
                <div class="row_input"><?= Html::activeCheckbox($exclusionsCity, 'active', [
                        'name'         => OptionTariff::EXCEPTIONS_TYPE . '[active]',
                        'class'        => 'switch',
                        'data-section' => 'exclusions',
                        'label'        => null,
                        'id'           => 'activity_' . strtolower(OptionTariff::EXCEPTIONS_TYPE),
                    ]) ?></div>
            </section>
            <section id="exclusions" <? if (!$exclusionsCity->active): ?>style="display: none"<? endif ?>>
                <?= $form->field($exclusionsCity, 'action_dates')->begin(); ?>
                <?= ValidityInput::widget([
                    'id'         => 'add_exclusions',
                    'label'      => t('taxi_tariff', 'Exclusions'),
                    'data'       => $tariffActiveDate[OptionTariff::EXCEPTIONS_TYPE],
                    'input_name' => OptionTariff::EXCEPTIONS_TYPE,
                    'model'      => $exclusionsCity,
                    'attr_name'  => 'action_dates',
                ]) ?>
                <?= $form->field($exclusionsCity, 'action_dates')->end(); ?>
                <?= $this->render('_option_form', [
                    'modelCity'      => $exclusionsCity,
                    'modelTrack'     => $exclusionsTrack,
                    'modelType'      => OptionTariff::EXCEPTIONS_TYPE,
                    'form_data'      => $form_data,
                    'addOptions'     => $addOptions,
                    'arOption'       => $arOption,
                    'fixCity'        => $fixCity,
                    'fixTrack'       => $fixTrack,
                    'fixAiroport'    => $fixAiroport,
                    'fixRailway'     => $fixRailway,
                    'tariff_city_id' => $tariff_city_id,
                ]) ?>
            </section>
        </div>
    </div>

    <section class="submit_form">
        <? if (!$tariff->isNewRecord): ?>
            <div>
                <label><?= Html::activeCheckbox($tariff, 'block', ['label' => null]) ?> <b><?= t('app',
                            'Block') ?></b></label>
            </div>
        <? endif ?>
        <?= Html::submitInput(t('app', 'Save')) ?>
    </section>
<?php ActiveForm::end(); ?>