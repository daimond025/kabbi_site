<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\tariff\models\TaxiTariff */

$this->title = empty($tariff->name) ? t('taxi_tariff', 'No name') : $tariff->name;
\app\modules\tariff\assets\TariffAsset::register($this);
?>
<div class="bread"><a href="<?= Url::toRoute('/tariff/tariff-client/list') ?>"><?= t('app', 'Tariff for clients') ?></a>
</div>
<h1><?= Html::encode($this->title) ?></h1>

<?php

$suggest =  "<label>";

$suggest .= t('logo', 'Photo of a tariff');

$suggest .= "</label>";

$suggest .= "<a data-tooltip='2' class='tooltip'>?
                              <span style='display: none; opacity: 0;'>";
$suggest .= Yii::t('hint', 'The photo is required for the client mobile application iOS and Android, displayed in the list of tariffs. Recommended size: not less than 360 × 160 px.');
$suggest .= "</span></a>";

?>

<?php $form = app()->user->can('clientTariff') ? '_form' : '_form_read';
echo $this->render($form, [
    'tariff'           => $tariff,
    'form_data'        => $form_data,
    'currentCity'      => $currentCity,
    'currentTrack'     => $currentTrack,
    'holidaysCity'     => $holidaysCity,
    'holidaysTrack'    => $holidaysTrack,
    'exclusionsCity'   => $exclusionsCity,
    'exclusionsTrack'  => $exclusionsTrack,
    'addOptions'       => $addOptions,
    'fixAiroport'      => $fixAiroport,
    'fixRailway'       => $fixRailway,
    'fixCity'          => $fixCity,
    'fixTrack'         => $fixTrack,
    'arOption'         => $arOption,
    'tariffActiveDate' => $tariffActiveDate,
    'suggest'          => $suggest,
]) ?>
