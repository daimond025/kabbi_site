<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $currencySymbol string */

if (!empty($arParking)):
    $fixPrice = 0;
    $translateMap = [
        'city'    => 'Within the area',
        'outCity' => 'Within the area',
        'airport' => 'Within Airport',
        'station' => 'Within Railway',
    ];

    if ($parking_type == 'airport' || $parking_type == 'station'):?>
        <!--Бесплатное время ожидания до начала поездки-->
        <div>
            <label><?= t('taxi_tariff', 'Free waiting time before a trip') ?></label>
            <? $value = isset($tariff['wait_time_day']) ? $tariff['wait_time_day'] : 0 ?>
            <span><?= Html::encode($value) ?> <?= t('app', 'min.') ?></span>

            <? $value = isset($tariff['wait_time_night']) ? $tariff['wait_time_night'] : 0 ?>
            <span><?= Html::encode($value) ?> <?= t('app', 'min.') ?></span>
        </div>

        <!--Стоимость 1 минуты ожидания-->
        <div>
            <label><?= t('taxi_tariff', 'Cost of 1 minute waiting') ?></label>
            <? $value = isset($tariff['wait_price_day']) ? $tariff['wait_price_day'] : 0 ?>
            <span><?= Html::encode($value) ?> <?= Html::encode($currencySymbol) ?></span>

            <? $value = isset($tariff['wait_price_night']) ? $tariff['wait_price_night'] : 0 ?>
            <span><?= Html::encode($value) ?> <?= Html::encode($currencySymbol) ?></span>
        </div>

        <!--Округление-->
        <div>
            <label><?= t('taxi_tariff', 'Rounding') ?></label>
            <? $value = isset($tariff['rounding_day']) ? $tariff['rounding_day'] : 0 ?>
            <span><?= Html::encode($value) ?> <?= Html::encode($currencySymbol) ?></span>

            <? $value = isset($tariff['rounding_night']) ? $tariff['rounding_night'] : 0 ?>
            <span><?= Html::encode($value) ?> <?= Html::encode($currencySymbol) ?></span>
        </div>
        <?
    endif;

    if (isset($tariff['fixHasOptions'])) {
        $fixPrice = \yii\helpers\ArrayHelper::index($tariff->fixHasOptions, 'fix_id');
    }

    foreach ($arParking as $parking):
        $key = uniqid();
        ?>
        <h3><a class="spoler" data-spolerid="id<?= $key ?>"><?= Html::encode($parking['NAME']); ?></a></h3>
        <section class="spoler_content" id="id<?= $key ?>">
            <div class="ap_cont">
                <?
                if (!empty($parking['ITEMS'])):
                    foreach ($parking['ITEMS'] as $item):
                        $priceParams = [
                            'TO'   => [
                                'PRICE' => 0,
                                'TYPE'  => 'PRICE_TO',
                            ],
                            'BACK' => [
                                'PRICE' => 0,
                                'TYPE'  => 'PRICE_BACK',
                            ],
                        ];
                        if (isset($fixPrice[$item['FIX_ID']])) {
                            if ($fixPrice[$item['FIX_ID']]['fix']['from'] == $item['FROM_ID']) {
                                $priceParams = [
                                    'TO'   => [
                                        'PRICE' => $fixPrice[$item['FIX_ID']]['price_to'],
                                        'TYPE'  => 'PRICE_TO',
                                    ],
                                    'BACK' => [
                                        'PRICE' => $fixPrice[$item['FIX_ID']]['price_back'],
                                        'TYPE'  => 'PRICE_BACK',
                                    ],
                                ];
                            } else {
                                $priceParams = [
                                    'TO'   => [
                                        'PRICE' => $fixPrice[$item['FIX_ID']]['price_back'],
                                        'TYPE'  => 'PRICE_BACK',
                                    ],
                                    'BACK' => [
                                        'PRICE' => $fixPrice[$item['FIX_ID']]['price_to'],
                                        'TYPE'  => 'PRICE_TO',
                                    ],
                                ];
                            }
                        } elseif ($item['TYPE'] == 'BACK') {
                            $priceParams['TO']['TYPE']   = 'PRICE_BACK';
                            $priceParams['BACK']['TYPE'] = 'PRICE_TO';
                        }
                        ?>
                        <div>
                            <? if ($item['TO'] == $item['FROM']): ?>
                                <label><?= Html::encode(t('taxi_tariff', $translateMap[$parking_type])); ?></label>
                                <? $value = $priceParams['TO']['PRICE'] ?>
                            <? else: ?>
                                <label><?= Html::encode(mb_substr($item['FROM'], 0, 1, "UTF-8")); ?>.
                                    > <?= Html::encode($item['TO']); ?></label>
                                <span><?= Html::encode($priceParams['TO']['PRICE']) ?> <?= Html::encode($currencySymbol) ?></span>

                                <span><?= t('taxi_tariff', 'Back') ?></span>
                                <? $value = $priceParams['BACK']['PRICE'] ?>
                            <? endif; ?>
                            <span><?= Html::encode($value) ?> <?= Html::encode($currencySymbol) ?></span>
                        </div>
                    <? endforeach; ?>
                <? endif; ?>
            </div>
        </section>
    <? endforeach; ?>
<? else: ?>
    <p style="color: red"><?= t('taxi_tariff', 'Do not set the parking!') ?></p>
<? endif; ?>