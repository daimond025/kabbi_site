<?
$last_key = count($interval_day) - 1;
$km = t('app', 'km');
$km_next = t('taxi_tariff', 'km and next');
foreach ($interval_day as $key => $value):
    $first_interval_name_day = $model_type . '[OptionTariff][' . $area . '][next_km_price_interval_day][' . $key . '][interval][0]';
    $first_interval_name_night = $model_type . '[OptionTariff][' . $area . '][next_km_price_interval_night][' . $key . '][interval][0]';
    $interval_price_name_day = $model_type . '[OptionTariff][' . $area . '][next_km_price_interval_day][' . $key . '][price]';
    $interval_price_name_night = $model_type . '[OptionTariff][' . $area . '][next_km_price_interval_night][' . $key . '][price]';
    $first_interval_value_day = !$key && $model->planting_include_day ? $model->planting_include_day : $value['interval'][0];
    $first_interval_value_night = !$key && $model->planting_include_night ? $model->planting_include_night : $interval_night[$key]['interval'][0];
    ?>
    <div class="tdad_item<? if ($key == $last_key): ?> tdad_last<? endif ?>">
        <div class="tdad_number"><?= $key + 1 ?></div>
        <div class="day">
            <? $second_interval_name_day = $model_type . '[OptionTariff][' . $area . '][next_km_price_interval_day][' . $key . '][interval][1]'; ?>
            <input type="text" value="<?= $first_interval_value_day ?>" disabled="disabled" class="first_interval_number">
            <input type="hidden" name="<?= $first_interval_name_day ?>" value="<?= $first_interval_value_day ?>" class="first_interval_number">
            <? if ($key !== $last_key): ?>
                <i>-</i><input type="text" name="<?= $second_interval_name_day ?>" value="<?= $value['interval'][1] ?>" class="second_interval_number">
                <span class="<?= $area ?>"><?= $km ?></span>
            <? else: ?>
                <span class="<?= $area ?>"><?= $km_next ?></span>
            <? endif ?>
            <div class="tdad_sec_line">
                <input type="text" name="<?= $interval_price_name_day ?>" value="<?= $value['price'] ?>">
                <span class="<?= $area ?> currency_symbol"><?= $currency_symbol ?>/<?= $km ?></span>
            </div>
        </div>
        
        <div class="night">
            <? $second_interval_name_night = $model_type . '[OptionTariff][' . $area . '][next_km_price_interval_night][' . $key . '][interval][1]'; ?>
            <input type="text" value="<?= $first_interval_value_night ?>" disabled="disabled" class="first_interval_number">
            <input type="hidden" name="<?= $first_interval_name_night ?>" value="<?= $first_interval_value_night ?>" class="first_interval_number">
            <? if ($key !== $last_key): ?>
                <i>-</i><input type="text" name="<?= $second_interval_name_night ?>" value="<?= $interval_night[$key]['interval'][1] ?>" class="second_interval_number">
                <span class="<?= $area ?>"><?= $km ?></span>
            <? else: ?>
                <span class="<?= $area ?>"><?= $km_next ?></span>
            <? endif ?>
            <div class="tdad_sec_line">
                <input type="text" name="<?= $interval_price_name_night ?>" value="<?= $interval_night[$key]['price'] ?>">
                <span class="<?= $area ?> currency_symbol"><?= $currency_symbol ?>/<?= $km ?></span>
            </div>
        </div>
        <? if ($key == $last_key - 1): ?>
            <a class="close_red"></a>
        <? endif ?>
    </div>
<? endforeach; ?>
<a href="#" class="plus_icon"><i></i></a>