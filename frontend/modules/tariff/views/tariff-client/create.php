<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\tariff\models\TaxiTariff */

$this->title = Yii::t('taxi_tariff', 'Add tariff');
\app\modules\tariff\assets\TariffAsset::register($this);
?>
<div class="bread"><a href="<?= Url::toRoute('/tariff/tariff-client/list')?>"><?= t('app', 'Tariff for clients')?></a></div>
<h1><?= Html::encode($this->title) ?></h1>

<?php
$form = app()->user->can('clientTariff') ? '_form' : '_form_read';
echo $this->render($form, [
    'tariff' => $tariff,
    'form_data' => $form_data,
    'currentCity' => $currentCity,
    'currentTrack' => $currentTrack,
    'holidaysCity' => $holidaysCity,
    'holidaysTrack' => $holidaysTrack,
    'exclusionsCity' => $exclusionsCity,
    'exclusionsTrack' => $exclusionsTrack,
]) ?>

