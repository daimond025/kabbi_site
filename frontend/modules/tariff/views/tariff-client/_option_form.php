<?php

use yii\helpers\Html;
use app\modules\tariff\models\OptionTariff;

$nextCostUnitList = OptionTariff::getNextCostUnitList($tariff_city_id);
$currencySymbol   = Html::encode(getCurrencySymbol($tariff_city_id));
?>
<div class="add_plan">
    <section class="row">
        <div class="row_label"></div>
        <div class="row_input">
            <?= Html::activeCheckbox($modelCity, 'calculation_fix',
                ['name' => $modelType . '[calculation_fix]', 'class' => 'js-calculation_fix']) ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint',
                        'To fix the preliminary calculation of the cost of the trip for the client. A taximeter will not count the cost of the trip. It will be considered only the waiting time for the customer before and during the trip.'); ?>
                </span>
            </a>
        </div>
    </section>
    <section class="row" style="display:none">
        <div class="row_label"></div>
        <div class="row_input">
            <?= Html::activeCheckbox($modelCity, 'enabled_parking_ratio',
                ['name' => $modelType . '[enabled_parking_ratio]', 'value'=>0]) ?>
        </div>
    </section>
    <div class="day_night_block">
        <section class="row">
            <div class="row_label"></div>
            <div class="row_input">
                <?= Html::activeCheckbox($modelCity, 'allow_day_night', ['name' => $modelType . '[allow_day_night]']) ?>
            </div>
        </section>
        <section class="row">
            <div class="row_label"><?= t('taxi_tariff', 'Day time') ?></div>
            <div class="row_input">
                <?= Html::activeTextInput($modelCity, 'start_day', [
                    'style' => 'display: inline-block; zoom: 1; *display: inline; width: 12.9%;',
                    'name'  => $modelType . '[start_day]',
                    'class' => 'day_night',
                    'type'  => 'time',
                ]) ?> —
                <?= Html::activeTextInput($modelCity, 'end_day', [
                    'style' => 'display: inline-block; zoom: 1; *display: inline; width: 12.9%;',
                    'name'  => $modelType . '[end_day]',
                    'class' => 'day_night',
                    'type'  => 'time',
                ]) ?>
            </div>
        </section>
        <section class="row">
            <div class="row_label"><?= t('taxi_tariff', 'Night time (auto)') ?></div>
            <div class="row_input">
                <input class="end_day" disabled type="text"
                       style="display: inline-block; zoom: 1; *display: inline; width: 12.9%;"
                       value="<?= $modelCity->end_day ?>"> —
                <input class="start_day" disabled type="text"
                       style="display: inline-block; zoom: 1; *display: inline; width: 12.9%;"
                       value="<?= $modelCity->start_day ?>">
            </div>
        </section>
    </div>
    <div class="ap_top tariff_change">
        <div class="ap_top_f">
            <h3><?= t('taxi_tariff', 'City') ?></h3>
            <?= Html::activeDropDownList($modelCity, 'accrual', $form_data['ACCRUALS'], [
                'data-placeholder' => Html::encode(is_null($modelCity->accrual) ? current($form_data['ACCRUALS']) : $form_data['ACCRUALS'][$modelCity->accrual]),
                'class'            => 'default_select city js-tariff-type',
                'name'             => $modelType . '[OptionTariff][city][accrual]',
                'id'               => 'cur_accrual_city',
            ]) ?>
            <span class="ap_top_d"><?= t('taxi_tariff', 'Day') ?></span>
            <span class="ap_top_n"><?= t('taxi_tariff', 'Night') ?></span>
        </div>
        <div class="ap_top_s">
            <h3><?= t('taxi_tariff', 'Track') ?></h3>
            <?= Html::activeDropDownList($modelTrack, 'accrual', $form_data['ACCRUALS'], [
                'data-placeholder' => Html::encode(is_null($modelTrack->accrual) ? current($form_data['ACCRUALS']) : $form_data['ACCRUALS'][$modelTrack->accrual]),
                'class'            => 'default_select track js-tariff-type',
                'name'             => $modelType . '[OptionTariff][track][accrual]',
                'id'               => 'cur_accrual_track',
            ]) ?>
            <span class="ap_top_d"><?= t('taxi_tariff', 'Day') ?></span>
            <span class="ap_top_n"><?= t('taxi_tariff', 'Night') ?></span>
        </div>
    </div>
    <div class="ap_cont">
        <?
        $disabledCity  = $modelCity->accrual == OptionTariff::ACCRUAL_FIX ? 'disabled' : false;
        $disabledTrack = $modelTrack->accrual == OptionTariff::ACCRUAL_FIX ? 'disabled' : false;

        ?>
        <!--Стоимость посадки-->
        <div class="disable_none">
            <?= Html::activeLabel($modelCity, 'planting_price_day') ?>
            <div class="city">
                <div class="day">
                    <?= Html::activeTextInput($modelCity, 'planting_price_day',
                        ['name' => $modelType . '[OptionTariff][city][planting_price_day]']) ?>
                    <span class="city currency_symbol"><?= $currencySymbol ?></span>
                </div>
                <div class="night">
                    <?= Html::activeTextInput($modelCity, 'planting_price_night',
                        ['name' => $modelType . '[OptionTariff][city][planting_price_night]']) ?>
                    <span class="city currency_symbol"><?= $currencySymbol ?></span>
                </div>
            </div>
            <div class="track">
                <div class="day">
                    <?= Html::activeTextInput($modelTrack, 'planting_price_day',
                        ['name' => $modelType . '[OptionTariff][track][planting_price_day]']) ?>
                    <span class="track currency_symbol"><?= $currencySymbol ?></span>
                </div>
                <div class="night">
                    <?= Html::activeTextInput($modelTrack, 'planting_price_night',
                        ['name' => $modelType . '[OptionTariff][track][planting_price_night]']) ?>
                    <span class="track currency_symbol"><?= $currencySymbol ?></span>
                </div>
            </div>
        </div>

        <!--Включено в стоимость посадки-->
        <div class="planting_include">
            <?= Html::activeLabel($modelCity, 'planting_include_day') ?>
            <div class="city">
                <div class="day">
                    <?= Html::activeTextInput($modelCity, 'planting_include_day', [
                        'name'     => $modelType . '[OptionTariff][city][planting_include_day]',
                        'disabled' => $disabledCity,
                    ]) ?>
                    <? $next_select_flag = $modelCity->accrual != OptionTariff::ACCRUAL_TIME ? false : true; ?>
                    <span<? if (!$next_select_flag): ?> style="display:none"<? endif ?>
                            class="change_km_city city time_unit"><?= t('app', 'min.') ?></span>
                    <span<? if ($next_select_flag): ?> style="display:none"<? endif ?>
                            class="change_km_city city km_unit"><?= t('app', 'km') ?></span>
                </div>
                <div class="night">
                    <?= Html::activeTextInput($modelCity, 'planting_include_night', [
                        'name'     => $modelType . '[OptionTariff][city][planting_include_night]',
                        'disabled' => $disabledCity,
                    ]) ?>
                    <span<? if (!$next_select_flag): ?> style="display:none"<? endif ?>
                            class="change_km_city city time_unit"><?= t('app', 'min.') ?></span>
                    <span<? if ($next_select_flag): ?> style="display:none"<? endif ?>
                            class="change_km_city city km_unit"><?= t('app', 'km') ?></span>
                </div>
            </div>
            <div class="track">
                <div class="day">
                    <?= Html::activeTextInput($modelTrack, 'planting_include_day', [
                        'name'     => $modelType . '[OptionTariff][track][planting_include_day]',
                        'disabled' => $disabledTrack,
                    ]) ?>
                    <? $next_select_flag = $modelTrack->accrual != OptionTariff::ACCRUAL_TIME ? false : true; ?>
                    <span<? if (!$next_select_flag): ?> style="display:none"<? endif ?>
                            class="change_km_city track time_unit"><?= t('app', 'min.') ?></span>
                    <span<? if ($next_select_flag): ?> style="display:none"<? endif ?>
                            class="change_km_city track km_unit"><?= t('app', 'km') ?></span>
                </div>
                <div class="night">
                    <?= Html::activeTextInput($modelTrack, 'planting_include_night', [
                        'name'     => $modelType . '[OptionTariff][track][planting_include_night]',
                        'disabled' => $disabledTrack,
                    ]) ?>
                    <span<? if (!$next_select_flag): ?> style="display:none"<? endif ?>
                            class="change_km_city track time_unit"><?= t('app', 'min.') ?></span>
                    <span<? if ($next_select_flag): ?> style="display:none"<? endif ?>
                            class="change_km_city track km_unit"><?= t('app', 'km') ?></span>
                </div>
            </div>
        </div>

        <!--Включено в стоимость посадки для смешанного типа-->
        <div<? if ($modelCity->accrual !== OptionTariff::ACCRUAL_MIX && $modelTrack->accrual !== OptionTariff::ACCRUAL_MIX): ?> style="display:none"<? endif ?>
                class="planting_include_time">
            <label></label>
            <div class="city"<? if ($modelCity->accrual !== OptionTariff::ACCRUAL_MIX): ?> style="display:none"<? endif ?>>
                <div class="day">
                    <?= Html::activeTextInput($modelCity, 'planting_include_day_time',
                        ['name' => $modelType . '[OptionTariff][city][planting_include_day_time]']) ?>
                    <span><?= t('app', 'min.') ?></span>
                </div>
                <div class="night">
                    <?= Html::activeTextInput($modelCity, 'planting_include_night_time',
                        ['name' => $modelType . '[OptionTariff][city][planting_include_night_time]']) ?>
                    <span><?= t('app', 'min.') ?></span>
                </div>
            </div>
            <div class="track"<? if ($modelTrack->accrual !== OptionTariff::ACCRUAL_MIX): ?> style="display:none"<? endif ?>>
                <div class="day">
                    <?= Html::activeTextInput($modelTrack, 'planting_include_day_time',
                        ['name' => $modelType . '[OptionTariff][track][planting_include_day_time]']) ?>
                    <span><?= t('app', 'min.') ?></span>
                </div>
                <div class="night">
                    <?= Html::activeTextInput($modelTrack, 'planting_include_night_time',
                        ['name' => $modelType . '[OptionTariff][track][planting_include_night_time]']) ?>
                    <span><?= t('app', 'min.') ?></span>
                </div>
            </div>
        </div>

        <!--Стоимость далее-->
        <div class="next_km_price">
            <?= Html::activeLabel($modelCity, 'next_km_price_day',
                ['class' => $modelCity->accrual == OptionTariff::ACCRUAL_INTERVAL ? 'label_shift' : null]) ?>
            <div class="city tar_dist_and_diap">
                <div class="not_interval_value"
                     <? if ($modelCity->accrual == OptionTariff::ACCRUAL_INTERVAL): ?>style="display:none"<? endif ?>>
                    <div class="day">
                        <?= Html::activeTextInput($modelCity, 'next_km_price_day', [
                            'name'     => $modelType . '[OptionTariff][city][next_km_price_day]',
                            'disabled' => $disabledCity,
                        ]) ?>
                        <?
                        $next_select_flag   = $modelCity->accrual != OptionTariff::ACCRUAL_TIME ? false : true;
                        $cur_next_cost_unit = current($nextCostUnitList);
                        ?>
                        <span<? if (!$next_select_flag): ?> style="display:none"<? endif ?>
                                class="change_r_km_city select city currency_symbol"><?= Html::activeDropDownList($modelCity,
                                'next_cost_unit_day', $nextCostUnitList, [
                                    'name'             => $modelType . '[OptionTariff][city][next_cost_unit_day]',
                                    'class'            => 'default_select',
                                    'data-placeholder' => Html::encode($cur_next_cost_unit),
                                ]) ?></span>
                        <span<? if ($next_select_flag): ?> style="display:none"<? endif ?>
                                class="change_r_km_city default_unit city currency_symbol"><?= $currencySymbol ?>
                            /<?= t('app', 'km') ?></span>
                    </div>
                    <div class="night">
                        <?= Html::activeTextInput($modelCity, 'next_km_price_night', [
                            'name'     => $modelType . '[OptionTariff][city][next_km_price_night]',
                            'disabled' => $disabledCity,
                        ]) ?>
                        <span<? if (!$next_select_flag): ?> style="display:none"<? endif ?>
                                class="change_r_km_city select city currency_symbol"><?= Html::activeDropDownList($modelCity,
                                'next_cost_unit_night', $nextCostUnitList, [
                                    'name'             => $modelType . '[OptionTariff][city][next_cost_unit_night]',
                                    'class'            => 'default_select',
                                    'data-placeholder' => Html::encode($cur_next_cost_unit),
                                ]) ?></span>
                        <span<? if ($next_select_flag): ?> style="display:none"<? endif ?>
                                class="change_r_km_city default_unit city currency_symbol"><?= $currencySymbol ?>
                            /<?= t('app', 'km') ?></span>
                    </div>
                </div>
                <div class="interval"
                     <? if ($modelCity->accrual != OptionTariff::ACCRUAL_INTERVAL): ?>style="display:none"<? endif ?>>
                    <?= $this->render('_interval', [
                        'model'           => $modelCity,
                        'interval_day'    => $modelCity->next_km_price_interval_day,
                        'interval_night'  => $modelCity->next_km_price_interval_night,
                        'model_type'      => $modelType,
                        'area'            => 'city',
                        'currency_symbol' => $currencySymbol,
                    ]) ?>
                </div>
            </div>
            <div class="track tar_dist_and_diap">
                <div class="not_interval_value"
                     <? if ($modelTrack->accrual == OptionTariff::ACCRUAL_INTERVAL): ?>style="display:none"<? endif ?>>
                    <div class="day">
                        <? $next_select_flag = $modelTrack->accrual != OptionTariff::ACCRUAL_TIME ? false : true; ?>
                        <?= Html::activeTextInput($modelTrack, 'next_km_price_day', [
                            'name'     => $modelType . '[OptionTariff][track][next_km_price_day]',
                            'disabled' => $disabledTrack,
                        ]) ?>
                        <span<? if (!$next_select_flag): ?> style="display:none"<? endif ?>
                                class="change_r_km_track track select currency_symbol"><?= Html::activeDropDownList($modelTrack,
                                'next_cost_unit_day', $nextCostUnitList, [
                                    'name'             => $modelType . '[OptionTariff][track][next_cost_unit_day]',
                                    'class'            => 'default_select',
                                    'data-placeholder' => Html::encode($cur_next_cost_unit),
                                ]) ?></span>
                        <span<? if ($next_select_flag): ?> style="display:none"<? endif ?>
                                class="change_r_km_track track default_unit currency_symbol"><?= $currencySymbol ?>
                            /<?= t('app', 'km') ?></span>
                    </div>
                    <div class="night">
                        <?= Html::activeTextInput($modelTrack, 'next_km_price_night', [
                            'name'     => $modelType . '[OptionTariff][track][next_km_price_night]',
                            'disabled' => $disabledTrack,
                        ]) ?>
                        <span<? if (!$next_select_flag): ?> style="display:none"<? endif ?>
                                class="change_r_km_track track select currency_symbol"><?= Html::activeDropDownList($modelTrack,
                                'next_cost_unit_night', $nextCostUnitList, [
                                    'name'             => $modelType . '[OptionTariff][track][next_cost_unit_night]',
                                    'class'            => 'default_select',
                                    'data-placeholder' => Html::encode($cur_next_cost_unit),
                                ]) ?></span>
                        <span<? if ($next_select_flag): ?> style="display:none"<? endif ?>
                                class="change_r_km_track track default_unit currency_symbol"><?= $currencySymbol ?>
                            /<?= t('app', 'km') ?></span>
                    </div>
                </div>
                <div class="interval"
                     <? if ($modelTrack->accrual != OptionTariff::ACCRUAL_INTERVAL): ?>style="display:none"<? endif ?>>
                    <?= $this->render('_interval', [
                        'model'           => $modelTrack,
                        'interval_day'    => $modelTrack->next_km_price_interval_day,
                        'interval_night'  => $modelTrack->next_km_price_interval_night,
                        'model_type'      => $modelType,
                        'area'            => 'track',
                        'currency_symbol' => $currencySymbol,
                    ]) ?>
                </div>
            </div>
        </div>

        <!--Стоимость далее для смешанного типа-->
        <div<? if ($modelCity->accrual !== OptionTariff::ACCRUAL_MIX && $modelTrack->accrual !== OptionTariff::ACCRUAL_MIX): ?> style="display:none"<? endif ?>
                class="next_km_price_time">
            <label></label>
            <div class="city"<? if ($modelCity->accrual !== OptionTariff::ACCRUAL_MIX): ?> style="display:none"<? endif ?>>
                <div class="day">
                    <?= Html::activeTextInput($modelCity, 'next_km_price_day_time',
                        ['name' => $modelType . '[OptionTariff][city][next_km_price_day_time]']) ?>
                    <span class="change_r_km_city day currency_symbol"><?= Html::activeDropDownList($modelCity,
                            'next_cost_unit_day_time', $nextCostUnitList, [
                                'name'             => $modelType . '[OptionTariff][city][next_cost_unit_day_time]',
                                'class'            => 'default_select',
                                'data-placeholder' => Html::encode($cur_next_cost_unit),
                            ]) ?></span>
                </div>
                <div class="night">
                    <?= Html::activeTextInput($modelCity, 'next_km_price_night_time',
                        ['name' => $modelType . '[OptionTariff][city][next_km_price_night_time]']) ?>
                    <span class="change_r_km_city currency_symbol"><?= Html::activeDropDownList($modelCity,
                            'next_cost_unit_night_time', $nextCostUnitList, [
                                'name'             => $modelType . '[OptionTariff][city][next_cost_unit_night_time]',
                                'class'            => 'default_select',
                                'data-placeholder' => Html::encode($cur_next_cost_unit),
                            ]) ?></span>
                </div>
            </div>
            <div class="track"<? if ($modelTrack->accrual !== OptionTariff::ACCRUAL_MIX): ?> style="display:none"<? endif ?>>
                <div class="day">
                    <?= Html::activeTextInput($modelTrack, 'next_km_price_day_time',
                        ['name' => $modelType . '[OptionTariff][track][next_km_price_day_time]']) ?>
                    <span class="change_r_km_track track day currency_symbol"><?= Html::activeDropDownList($modelTrack,
                            'next_cost_unit_day_time', $nextCostUnitList, [
                                'name'             => $modelType . '[OptionTariff][track][next_cost_unit_day_time]',
                                'class'            => 'default_select',
                                'data-placeholder' => Html::encode($cur_next_cost_unit),
                            ]) ?></span>
                </div>
                <div class="night">
                    <?= Html::activeTextInput($modelTrack, 'next_km_price_night_time',
                        ['name' => $modelType . '[OptionTariff][track][next_km_price_night_time]']) ?>
                    <span class="change_r_km_track track currency_symbol"><?= Html::activeDropDownList($modelTrack,
                            'next_cost_unit_night_time', $nextCostUnitList, [
                                'name'             => $modelType . '[OptionTariff][track][next_cost_unit_night_time]',
                                'class'            => 'default_select',
                                'data-placeholder' => Html::encode($cur_next_cost_unit),
                            ]) ?></span>
                </div>
            </div>
        </div>

        <!--Минимальная стоимость поездки-->
        <div>
            <?= Html::activeLabel($modelCity, 'min_price_day') ?>
            <div class="city">
                <div class="day">
                    <?= Html::activeTextInput($modelCity, 'min_price_day', [
                        'name'     => $modelType . '[OptionTariff][city][min_price_day]',
                        'disabled' => $disabledCity,
                        'class'    => 'js-min-price',
                    ]) ?>
                    <span class="city currency_symbol"><?= $currencySymbol ?></span>
                </div>
                <div class="night">
                    <?= Html::activeTextInput($modelCity, 'min_price_night', [
                        'name'     => $modelType . '[OptionTariff][city][min_price_night]',
                        'disabled' => $disabledCity,
                        'class'    => 'js-min-price',
                    ]) ?>
                    <span class="city currency_symbol"><?= $currencySymbol ?></span>
                </div>

            </div>
            <div class="track">
                <div class="day">
                    <?= Html::activeTextInput($modelTrack, 'min_price_day', [
                        'name'     => $modelType . '[OptionTariff][track][min_price_day]',
                        'disabled' => $disabledTrack,
                        'class'    => 'js-min-price',
                    ]) ?>
                    <span class="track currency_symbol"><?= $currencySymbol ?></span>
                </div>
                <div class="night">
                    <?= Html::activeTextInput($modelTrack, 'min_price_night', [
                        'name'     => $modelType . '[OptionTariff][track][min_price_night]',
                        'disabled' => $disabledTrack,
                        'class'    => 'js-min-price',
                    ]) ?>
                    <span class="track currency_symbol"><?= $currencySymbol ?></span>
                </div>
            </div>
        </div>

        <!--Минимальная стоимость поездки от трех точек-->
        <div>
            <?= Html::activeLabel($modelCity, 'second_min_price_day') ?>
            <div class="city">
                <div class="day">
                    <?= Html::activeTextInput($modelCity, 'second_min_price_day', [
                        'name'     => $modelType . '[OptionTariff][city][second_min_price_day]',
                        'disabled' => $disabledCity || $modelCity->calculation_fix != 1,
                        'class'    => 'js-second-min-price',
                    ]) ?>
                    <span class="city currency_symbol"><?= $currencySymbol ?></span>
                </div>
                <div class="night">
                    <?= Html::activeTextInput($modelCity, 'second_min_price_night', [
                        'name'     => $modelType . '[OptionTariff][city][second_min_price_night]',
                        'disabled' => $disabledCity || $modelCity->calculation_fix != 1,
                        'class'    => 'js-second-min-price',
                    ]) ?>
                    <span class="city currency_symbol"><?= $currencySymbol ?></span>
                </div>

            </div>
            <div class="track">
                <div class="day">
                    <?= Html::activeTextInput($modelTrack, 'second_min_price_day', [
                        'name'     => $modelType . '[OptionTariff][track][second_min_price_day]',
                        'disabled' => $disabledTrack || $modelTrack->calculation_fix != 1,
                        'class'    => 'js-second-min-price',
                    ]) ?>
                    <span class="track currency_symbol"><?= $currencySymbol ?></span>
                </div>
                <div class="night">
                    <?= Html::activeTextInput($modelTrack, 'second_min_price_night', [
                        'name'     => $modelType . '[OptionTariff][track][second_min_price_night]',
                        'disabled' => $disabledTrack || $modelTrack->calculation_fix != 1,
                        'class'    => 'js-second-min-price',
                    ]) ?>
                    <span class="track currency_symbol"><?= $currencySymbol ?></span>
                </div>
            </div>
        </div>

        <!--Стоимость 1 км до адреса подачи-->
        <div>
            <?= Html::activeLabel($modelCity, 'supply_price_day') ?>
            <div style="width: 35.75%; float: left; height: 20px"></div>
            <div class="track">
                <div class="day">
                    <?= Html::activeTextInput($modelTrack, 'supply_price_day', [
                        'name'     => $modelType . '[OptionTariff][track][supply_price_day]',
                        'disabled' => $disabledTrack,
                    ]) ?>
                    <span class="track currency_symbol"><?= $currencySymbol ?></span>
                </div>
                <div class="night">
                    <?= Html::activeTextInput($modelTrack, 'supply_price_night', [
                        'name'     => $modelType . '[OptionTariff][track][supply_price_night]',
                        'disabled' => $disabledTrack,
                    ]) ?>
                    <span class="track currency_symbol"><?= $currencySymbol ?></span>
                </div>
            </div>
        </div>

        <!--Бесплатное время ожидания до начала поездки-->
        <div class="disable_none">
            <?= Html::activeLabel($modelCity, 'wait_time_day') ?>
            <div class="city">
                <div class="day">
                    <?= Html::activeTextInput($modelCity, 'wait_time_day',
                        ['name' => $modelType . '[OptionTariff][city][wait_time_day]']) ?>
                    <span class="city"><?= t('app', 'min.') ?></span>
                </div>
                <div class="night">
                    <?= Html::activeTextInput($modelCity, 'wait_time_night',
                        ['name' => $modelType . '[OptionTariff][city][wait_time_night]']) ?>
                    <span class="city"><?= t('app', 'min.') ?></span>
                </div>
            </div>
            <div class="track">
                <div class="day">
                    <?= Html::activeTextInput($modelTrack, 'wait_time_day',
                        ['name' => $modelType . '[OptionTariff][track][wait_time_day]']) ?>
                    <span class="track"><?= t('app', 'min.') ?></span>
                </div>
                <div class="night">
                    <?= Html::activeTextInput($modelTrack, 'wait_time_night',
                        ['name' => $modelType . '[OptionTariff][track][wait_time_night]']) ?>
                    <span class="track"><?= t('app', 'min.') ?></span>
                </div>
            </div>
        </div>

        <!--Бесплатное время ожидания во время поездки за 1 раз, от-->
        <div>
            <?= Html::activeLabel($modelCity, 'wait_driving_time_day') ?>
            <div class="city">
                <div class="day">
                    <?= Html::activeTextInput($modelCity, 'wait_driving_time_day', [
                        'name'     => $modelType . '[OptionTariff][city][wait_driving_time_day]',
                        'disabled' => $disabledCity,
                    ]) ?>
                    <span class="city"><?= t('app', 'sec.') ?></span>
                </div>
                <div class="night">
                    <?= Html::activeTextInput($modelCity, 'wait_driving_time_night', [
                        'name'     => $modelType . '[OptionTariff][city][wait_driving_time_night]',
                        'disabled' => $disabledCity,
                    ]) ?>
                    <span class="city"><?= t('app', 'sec.') ?></span>
                </div>
            </div>
            <div class="track">
                <div class="day">
                    <?= Html::activeTextInput($modelTrack, 'wait_driving_time_day', [
                        'name'     => $modelType . '[OptionTariff][track][wait_driving_time_day]',
                        'disabled' => $disabledTrack,
                    ]) ?>
                    <span class="track"><?= t('app', 'sec.') ?></span>
                </div>
                <div class="night">
                    <?= Html::activeTextInput($modelTrack, 'wait_driving_time_night', [
                        'name'     => $modelType . '[OptionTariff][track][wait_driving_time_night]',
                        'disabled' => $disabledTrack,
                    ]) ?>
                    <span class="track"><?= t('app', 'sec.') ?></span>
                </div>
            </div>
        </div>

        <!--Стоимость 1 минуты ожидания-->
        <div class="disable_none">
            <?= Html::activeLabel($modelCity, 'wait_price_day') ?>
            <div class="city">
                <div class="day">
                    <?= Html::activeTextInput($modelCity, 'wait_price_day',
                        ['name' => $modelType . '[OptionTariff][city][wait_price_day]']) ?>
                    <span class="city currency_symbol"><?= $currencySymbol ?></span>
                </div>
                <div class="night">
                    <?= Html::activeTextInput($modelCity, 'wait_price_night',
                        ['name' => $modelType . '[OptionTariff][city][wait_price_night]']) ?>
                    <span class="city currency_symbol"><?= $currencySymbol ?></span>
                </div>
            </div>
            <div class="track">
                <div class="day">
                    <?= Html::activeTextInput($modelTrack, 'wait_price_day',
                        ['name' => $modelType . '[OptionTariff][track][wait_price_day]']) ?>
                    <span class="track currency_symbol"><?= $currencySymbol ?></span>
                </div>
                <div class="night">
                    <?= Html::activeTextInput($modelTrack, 'wait_price_night',
                        ['name' => $modelType . '[OptionTariff][track][wait_price_night]']) ?>
                    <span class="track currency_symbol"><?= $currencySymbol ?></span>
                </div>
            </div>
        </div>

        <!--Скорость, при которой вкл. ожидание-->
        <div>
            <?= Html::activeLabel($modelCity, 'speed_downtime_day') ?>
            <div class="city">
                <div class="day">
                    <?= Html::activeTextInput($modelCity, 'speed_downtime_day', [
                        'name'     => $modelType . '[OptionTariff][city][speed_downtime_day]',
                        'disabled' => $disabledCity,
                        'class'    => 'js-speed_downtime_on_auto',
                    ]) ?>
                    <span class="city"><?= t('app', 'km/h') ?></span>
                </div>
                <div class="night">
                    <?= Html::activeTextInput($modelCity, 'speed_downtime_night', [
                        'name'     => $modelType . '[OptionTariff][city][speed_downtime_night]',
                        'disabled' => $disabledCity,
                        'class'    => 'js-speed_downtime_on_auto',
                    ]) ?>
                    <span class="city"><?= t('app', 'km/h') ?></span>
                </div>
            </div>
            <div class="track">
                <div class="day">
                    <?= Html::activeTextInput($modelTrack, 'speed_downtime_day', [
                        'name'     => $modelType . '[OptionTariff][track][speed_downtime_day]',
                        'disabled' => $disabledTrack,
                        'class'    => 'js-speed_downtime_on_auto',
                    ]) ?>
                    <span class="track"><?= t('app', 'km/h') ?></span>
                </div>
                <div class="night">
                    <?= Html::activeTextInput($modelTrack, 'speed_downtime_night', [
                        'name'     => $modelType . '[OptionTariff][track][speed_downtime_night]',
                        'disabled' => $disabledTrack,
                        'class'    => 'js-speed_downtime_on_auto',
                    ]) ?>
                    <span class="track"><?= t('app', 'km/h') ?></span>
                </div>
            </div>
        </div>
        <!--Округление-->
        <div class="disable_none">
            <?= Html::activeLabel($modelCity, 'rounding_day') ?>
            <div class="city">
                <div class="day">
                    <?= Html::activeTextInput($modelCity, 'rounding_day',
                        ['name' => $modelType . '[OptionTariff][city][rounding_day]']) ?>
                    <span class="city currency_symbol"><?= $currencySymbol ?></span>
                </div>
                <div class="night">
                    <?= Html::activeTextInput($modelCity, 'rounding_night',
                        ['name' => $modelType . '[OptionTariff][city][rounding_night]']) ?>
                    <span class="city currency_symbol"><?= $currencySymbol ?></span>
                </div>
            </div>
            <div class="track">
                <div class="day">
                    <?= Html::activeTextInput($modelTrack, 'rounding_day',
                        ['name' => $modelType . '[OptionTariff][track][rounding_day]']) ?>
                    <span class="track currency_symbol"><?= $currencySymbol ?></span>
                </div>
                <div class="night">
                    <?= Html::activeTextInput($modelTrack, 'rounding_night',
                        ['name' => $modelType . '[OptionTariff][track][rounding_night]']) ?>
                    <span class="track currency_symbol"><?= $currencySymbol ?></span>
                </div>
            </div>
        </div>
        <div class="disable_none">
            <?= Html::activeLabel($modelCity, 'rounding_type_day') ?>
            <div class="city">
                <div class="day" style="width: 140px; margin-right: 10px;">
                    <?= Html::activeDropDownList($modelCity, 'rounding_type_day', OptionTariff::getRoundingTypeMap(), [
                            'name'                      => $modelType . '[OptionTariff][city][rounding_type_day]',
                            'class'                     => 'default_select',
                            'data-additional-classes'   => 'normal-select'
                    ]); ?>
                </div>
                <div class="night" style="width: 140px; margin-right: 10px;">
                    <?= Html::activeDropDownList($modelCity, 'rounding_type_night', OptionTariff::getRoundingTypeMap(), [
                        'name'                      => $modelType . '[OptionTariff][city][rounding_type_night]',
                        'class'                     => 'default_select',
                        'data-additional-classes'   => 'normal-select'
                    ]); ?>
                </div>
            </div>
            <div class="track">
                <div class="day" style="width: 140px; margin-right: 10px;">
                    <?= Html::activeDropDownList($modelTrack, 'rounding_type_day', OptionTariff::getRoundingTypeMap(), [
                        'name'                      => $modelType . '[OptionTariff][track][rounding_type_day]',
                        'class'                     => 'default_select',
                        'data-additional-classes'   => 'normal-select'
                    ]); ?>
                </div>
                <div class="night" style="width: 140px; margin-right: 10px;">
                    <?= Html::activeDropDownList($modelTrack, 'rounding_type_night', OptionTariff::getRoundingTypeMap(), [
                        'name'                      => $modelType . '[OptionTariff][track][rounding_type_night]',
                        'class'                     => 'default_select',
                        'data-additional-classes'   => 'normal-select'
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<h2><?= t('taxi_tariff', 'Additional options') ?></h2>
<div class="ap_cont js-options">
    <? foreach ($form_data['ADD_OPTIONS'] as $id => $name): ?>
        <?
        $isAvailableOption = in_array($id, $form_data['ACTIVE_ADD_OPTIONS'], false);
        $checked           = isset($addOptions[$modelType][$id]);
        ?>
        <div class="check_option" style="<?= !$isAvailableOption ? 'display:none' : '' ?>" data-option-id="<?= $id ?>">
            <label>
                <input type="checkbox" name="<?= $modelType ?>[AdditionalOption][active][]" value="<?= $id ?>"
                    <?= $checked ? 'checked' : '' ?>/>
                <?= Html::encode($name) ?>
            </label>
            <input type="text" name="<?= $modelType ?>[AdditionalOption][price][<?= $id ?>]"
                <?= $checked && $isAvailableOption ? '' : 'disabled' ?>
                   value="<?= $checked ? Html::encode($addOptions[$modelType][$id]->price) : 0 ?>"/>
            <span class="currency_symbol"><?= $currencySymbol ?></span>
        </div>
    <? endforeach; ?>
</div>

<div class="fix_tariff"
     <? if (!isset($arOption) || ($arOption[$modelType][OptionTariff::AREA_CITY]['accrual'] != OptionTariff::ACCRUAL_FIX && $arOption[$modelType][OptionTariff::AREA_TRACK]['accrual'] != OptionTariff::ACCRUAL_FIX)): ?>style="display: none"<? endif ?>
     data-url="<?= yii\helpers\Url::to('/tariff/tariff-client/get-fix-tariff') ?>">
    <h2><?= t('taxi_tariff', 'Fix tariffs') ?></h2>
    <div class="fix_city"
         <? if (!isset($arOption) || ($arOption[$modelType][OptionTariff::AREA_CITY]['accrual'] != OptionTariff::ACCRUAL_FIX)): ?>style="display: none"<? endif ?>>
        <h3><?= t('taxi_tariff', 'City') ?></h3>
        <div class="ajax_city">
            <?
            if (isset($arOption) && $arOption[$modelType][OptionTariff::AREA_CITY]['accrual'] == OptionTariff::ACCRUAL_FIX) {
                echo $this->render('fixTariff', [
                    'arParking'      => $fixCity,
                    'parking_type'   => 'city',
                    'tariff_type'    => $modelType,
                    'tariff'         => $arOption[$modelType][OptionTariff::AREA_CITY],
                    'currencySymbol' => $currencySymbol,
                ]);
            }
            ?>
        </div>
    </div>
    <div class="fix_track"
         <? if (!isset($arOption) || ($arOption[$modelType][OptionTariff::AREA_TRACK]['accrual'] != OptionTariff::ACCRUAL_FIX)): ?>style="display: none"<? endif ?>>
        <h3><?= t('taxi_tariff', 'Track') ?></h3>
        <div class="ajax_track">
            <?
            if (isset($arOption) && $arOption[$modelType][OptionTariff::AREA_TRACK]['accrual'] == OptionTariff::ACCRUAL_FIX) {
                echo $this->render('fixTariff', [
                    'arParking'      => $fixTrack,
                    'parking_type'   => 'outCity',
                    'tariff_type'    => $modelType,
                    'tariff'         => $arOption[$modelType][OptionTariff::AREA_TRACK],
                    'currencySymbol' => $currencySymbol,
                ]);
            }
            ?>
        </div>
    </div>
</div>
<h2 class="check_spoler"><label><input
            <? if (isset($arOption) && !empty($arOption[$modelType][OptionTariff::AREA_AIRPORT])): ?>checked="checked"<? endif ?>
            data-option="airport" type="checkbox"/></label> <a data-spolerid="a1_<?= $modelType ?>"
                                                               class="spoler <? if (!isset($arOption) || empty($arOption[$modelType][OptionTariff::AREA_AIRPORT])): ?>locked<? endif ?>"><?= t('taxi_tariff',
            'Airport fix tariffs') ?></a></h2>
<section class="spoler_content" id="a1_<?= $modelType ?>">
    <div class="ap_cont spoler_wide ajax_airport">
        <?
        if (isset($arOption) && !empty($arOption[$modelType][OptionTariff::AREA_AIRPORT])) {
            echo $this->render('fixTariff', [
                'arParking'      => $fixAiroport,
                'parking_type'   => 'airport',
                'tariff_type'    => $modelType,
                'tariff'         => $arOption[$modelType][OptionTariff::AREA_AIRPORT],
                'currencySymbol' => $currencySymbol,
            ]);
        }
        ?>
    </div>
</section>

<h2 class="check_spoler"><label><input
            <? if (isset($arOption) && !empty($arOption[$modelType][OptionTariff::AREA_RAILWAY])): ?>checked="checked"<? endif ?>
            data-option="station" type="checkbox"/></label> <a data-spolerid="s1_<?= $modelType ?>"
                                                               class="spoler <? if (!isset($arOption) || empty($arOption[$modelType][OptionTariff::AREA_RAILWAY])): ?>locked<? endif ?>"><?= t('taxi_tariff',
            'Railway fix tariffs') ?></a></h2>
<section class="spoler_content" id="s1_<?= $modelType ?>">
    <div class="ap_cont spoler_wide ajax_station">
        <?
        if (isset($arOption) && !empty($arOption[$modelType][OptionTariff::AREA_RAILWAY])) {
            echo $this->render('fixTariff', [
                'arParking'      => $fixRailway,
                'parking_type'   => 'station',
                'tariff_type'    => $modelType,
                'tariff'         => $arOption[$modelType][OptionTariff::AREA_RAILWAY],
                'currencySymbol' => $currencySymbol,
            ]);
        }
        ?>
    </div>
</section>