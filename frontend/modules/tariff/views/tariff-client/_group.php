<?php
use yii\grid\GridView;
use yii\helpers\Html;
use app\modules\tariff\models\TaxiTariffGroup;

?>
<h3><?php
    echo TaxiTariffGroup::getGroupName($model['name']);
    ?></h3>
<section style="margin-bottom: 30px;">
    <?= GridView::widget([
        'dataProvider' => $arr_dataProvider[$model['group_id']],
        'layout'       => "{items}",
        'tableOptions' => [
            'class' => 'people_table',
        ],
        'columns'      => [
            [
                'attribute'     => 'name',
                'label'         => t('taxi_tariff', 'Name'),
                'content'       => function ($model) {
                    $name = empty($model['name']) ? t('app', 'Untitled') : $model['name'];

                    return Html::a(Html::encode($name), ['/client/tariff/update', 'id' => $model['tariff_id']], ['data-pjax' => 0]);
                },
                'headerOptions' => ['style' => 'width: 25%'],
            ],
            [
                'attribute'     => 'class_car',
                'label'         => t('taxi_tariff', 'Class'),
                'content'       => function ($model) {
                    return t('car', $model['class']['class']);
                },
                'headerOptions' => ['style' => 'width: 25%'],
            ],
            [
                'attribute'     => 'city',
                'label'         => t('taxi_tariff', 'City'),
                'content'       => function ($model) use ($city_list) {
                    return $city_list[$model['tariffHasCity'][0]['city_id']];
                },
                'headerOptions' => ['style' => 'width: 25%'],
            ],

            [
                'attribute'     => 'sort',
                'label'         => t('app', 'Sort'),
                'headerOptions' => ['style' => 'width: 25%'],
            ],
        ],
    ]);
    ?>
</section>