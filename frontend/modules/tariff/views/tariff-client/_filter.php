<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\helpers\Html;

?>
<form name="tariff_search" method="post" onsubmit="return false" action="<?=Url::to('/tariff/tariff-client/search')?>">
    <div class="grid_3" data-activity="active">
        <div class="grid_item">
            <div class="select_checkbox">
                <a data-filter="no" class="a_sc select" rel="<?=t('user', 'All cities')?>"><?=t('user', 'All cities')?></a>
                <div class="b_sc">
                    <ul>
                        <?foreach ($city_list as $city_id => $city):?>
                            <li><label><input name="city_id[]" type="checkbox" value="<?=$city_id?>"/> <?= Html::encode($city); ?></label></li>
                        <?endforeach;?>
                    </ul>
                </div>
            </div>
        </div>
         <div class="grid_item" style="height: 50px;"></div>
        <div class="grid_item" style="height: 50px;"></div>
    </div>
</form>
