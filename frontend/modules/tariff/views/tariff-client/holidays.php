<?php

use yii\helpers\Html;
use common\helpers\DateTimeHelper;
use app\modules\tariff\models\OptionTariff;

$days = DateTimeHelper::getDayOfWeekList();
$months = DateTimeHelper::getMonthList();
foreach ($holidays as $date):
    $formatedDate = $date;
    if (strripos($date, '.') == 2) {
        $params = explode(".", $date);
        $formatedDate = $params[0] . ' ' . $months[$params[1]];
    } elseif (isset($days[$date])) {
        $formatedDate = $days[$date];
    }
    ?>
    <li><?= Html::encode($formatedDate); ?> <a></a><input type="text" name="<?= OptionTariff::HOLIDAYS_TYPE ?>[ActiveDate][]" value="<?= $date; ?>"></li>
<?php endforeach; ?>