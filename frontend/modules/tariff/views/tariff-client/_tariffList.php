<?
$this->registerJs("cityFilterInit();");

if (!empty($tariffs)):?>
    <?= $this->render('_filter', ['city_list' => $city_list]) ?>
    <div class="ajax_update">
        <?=$this->render('_tariff_table', compact('tariffs'))?>
    </div>
<? else: ?>
    <p><?= t('app', 'Empty') ?></p>
<? endif ?>