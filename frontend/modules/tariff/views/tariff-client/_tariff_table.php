<?php

use yii\helpers\Html;

if (!empty($tariffs)):
    $spolerid = 1;
    foreach($tariffs as $group => $tariff):
        $spoler_name = $tariff[0]->block ? 'sp_block_' : 'sp_';
        $spoler_name .= $spolerid;
    ?>
        <h3><a class="spoler" data-spolerid="<?=$spoler_name?>"><?= $group == 'none' ? t('taxi_tariff', 'No group') : Html::encode($group); ?></a></h3>
        <section class="spoler_content" style="margin-bottom: 30px;" id="<?=$spoler_name?>" style="display: none;">
            <table class="people_table">
                <tr>
                    <th style="width: 25%;"><?= t('driverTariff', 'Name') ?></th>
                    <th style="width: 25%;"><?= t('taxi_tariff', 'Class') ?></th>
                    <th style="width: 25%;"><?= t('driverTariff', 'City') ?></th>
                    <th style="width: 25%;"><?= t('taxi_tariff', 'Sort') ?></th>
                </tr>
                <? foreach ($tariff as $item): ?>
                    <tr>
                        <td><a href="<?= \yii\helpers\Url::to(['update', 'id' => $item->tariff_id]) ?>"><?= Html::encode($item->name ? $item->name : t('taxi_tariff', 'No name')) ?></a></td>
                        <td><?= Html::encode(t('car', $item->class->class)); ?></td>
                        <td><?= Html::encode($item->cities[0]->{name . getLanguagePrefix()}); ?></td>
                        <td><?= Html::encode($item->sort)?></td>
                    </tr>
                <? endforeach; ?>
            </table>
        </section>
        <?$spolerid++;?>
    <? endforeach; ?>
<? else: ?>
    <p><?= t('app', 'Empty') ?></p>
<? endif ?>