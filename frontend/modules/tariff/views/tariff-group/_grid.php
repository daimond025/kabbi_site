<?php

use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Html;

Pjax::begin(['id' => $pjaxId]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'       => "{items}",
    'tableOptions' => [
        'class' => 'people_table',
    ],
    'columns'      => [
        [
            'attribute'     => 'name',
            'label'         => t('taxi_tariff', 'Name'),
            'headerOptions' => ['class' => 'pt_wor'],
            'options'       => ['class' => 'pt_fio pt_wor'],
            'content'       => function ($model) {
                return Html::a(Html::encode($model['name']), ['update', 'id' => $model['group_id']],
                    ['data-pjax' => 0]);
            },
        ],
        [
            'attribute'     => 'sort',
            'label'         => t('app', 'Sort'),
            'headerOptions' => ['class' => 'pt_re'],
            'options'       => ['class' => 'pt_re'],
        ],
    ],
]);

Pjax::end();
