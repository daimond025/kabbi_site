<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\tariff\models\TaxiTariffGroup */
/* @var $form yii\widgets\ActiveForm */
?>
<section class="row">
    <div class="row_label">
        <?=Html::activeLabel($model, 'name')?>
    </div>
    <div class="row_input">
        <?= Html::encode($model->name); ?>
    </div>
</section>

<section class="row">
    <div class="row_label">
        <?=Html::activeLabel($model, 'sort')?>
    </div>
    <div class="row_input">
        <?= Html::encode($model->sort); ?>
    </div>
</section>

