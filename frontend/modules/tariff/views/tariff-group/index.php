<?php

use yii\helpers\Html;

/* @var string $pjaxId */
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ArrayDataProvider */

$this->title = Yii::t('app', 'Tariff groups');
if (app()->user->can('clientTariff'))
    echo Html::a(t('app', 'Add'), ['create'], ['class' => 'button', 'style' => 'float: right; position: relative; top: 5px;']);
?>
<h1><?= Html::encode($this->title) ?></h1>

<section class="main_tabs">
    <?= $this->render('_grid', compact('dataProvider', 'pjaxId')) ?>
</section>