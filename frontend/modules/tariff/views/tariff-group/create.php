<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\tariff\models\TaxiTariffGroup */

$this->title = t('app', 'Add');
?>
<div class="bread"><a href="<?= Url::toRoute('/tariff/tariff-group/list') ?>"><?= t('app', 'Tariff groups') ?></a></div>
<h1><?= Html::encode($this->title) ?></h1>

<?= $this->render('_form', compact('model')) ?>

