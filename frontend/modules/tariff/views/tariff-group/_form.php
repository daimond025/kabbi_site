<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\tariff\models\TaxiTariffGroup */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([
    'errorCssClass' => 'input_error',
    'successCssClass' => 'input_success',
]); ?>

<section class="row">
    <?= $form->field($model, 'name')->begin();?>
    <div class="row_label">
        <?=Html::activeLabel($model, 'name')?>
    </div>
    <div class="row_input">
        <?=Html::activeTextInput($model, 'name')?>
    </div>
    <?= $form->field($model, 'name')->end();?>
</section>

<section class="row">
    <?= $form->field($model, 'sort')->begin();?>
    <div class="row_label">
        <?=Html::activeLabel($model, 'sort')?>
    </div>
    <div class="row_input">
        <?=Html::activeTextInput($model, 'sort')?>
    </div>
    <?= $form->field($model, 'sort')->end();?>
</section>

<section class="submit_form">
    <?if(!$model->isNewRecord):?>
        <b><?= Html::a(t('app', 'Delete'), ['delete', 'id' => $model->group_id], ['class' => 'link_red']) ?></b>
    <?endif?>
    <?= Html::submitInput(t('app', 'Save')) ?>
</section>
<?php ActiveForm::end(); ?>

