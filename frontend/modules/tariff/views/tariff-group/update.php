<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \frontend\modules\employee\models\groups\WorkerGroup */
app\modules\tariff\assets\GroupAsset::register($this);
$this->title = $model->name;
?>
<div class="bread"><a href="<?= Url::toRoute('/tariff/tariff-group/list') ?>"><?= t('app', 'Tariff groups') ?></a></div>
<h1><?= Html::encode($this->title) ?></h1>

<?php
$form = app()->user->can('clientTariff') ? '_form' : '_form_read';
echo $this->render($form, compact('model'));
?>
