<?php

namespace app\modules\tariff;

use app\components\behavior\ControllerBehavior;
use yii\helpers\ArrayHelper;

/**
 * Class Module
 * @package app\modules\tariff
 *
 * @mixin ControllerBehavior
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\tariff\controllers';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'common' => [
                'class' => ControllerBehavior::className(),
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $this->commonBeforeAction();

        return parent::beforeAction($action);
    }
}
