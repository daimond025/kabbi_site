<?php

namespace app\modules\tariff\models;


use yii\base\Model;
use yii\db\ActiveQuery;

/**
 * TaxiTariffGroupSearch represents the model behind the search form about `app\modules\tariff\models\TaxiTariffGroup`.
 *
 * @property array $accessCityList
 */
class TaxiTariffSearch extends TaxiTariff
{

    public $city_id;
    private $_accessCityList = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['block', 'safe'],
            ['city_id', 'in', 'range' => $this->getAccessCityList(), 'allowArray' => true]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'city_id' => t('user', 'All cities'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveQuery
     */

    public function search($params)
    {
        $query = self::find();
        $query->where([
            'tenant_id' => user()->tenant_id,
            'block' => $this->block,
            'group_id' => $this->group_id == 'null' ? null : $this->group_id,
        ]);

        $city_list = $this->accessCityList;
        $query->joinWith([
            'tariffHasCity' => function (ActiveQuery $subquery) use ($city_list) {
                $subquery->where(['city_id' => $city_list]);
            },
            'class'
        ]);
        $query->orderBy([
            'sort' => SORT_ASC,
            'name' => SORT_ASC
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $query;
        }

        $query->joinWith([
            'tariffHasCity' => function (ActiveQuery $subquery) use ($city_list) {
                $subquery->where(['city_id' => $this->getSearchCityList()]);
            },
            'class'
        ]);

        return $query;
    }
    

    public function getAccessCityList()
    {
        return $this->_accessCityList;
    }

    public function setAccessCityList($value)
    {
        $this->_accessCityList = $value;
    }

    private function getSearchCityList()
    {
        return $this->city_id ? $this->city_id : $this->getAccessCityList();
    }
}
