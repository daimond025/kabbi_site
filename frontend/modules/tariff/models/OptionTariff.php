<?php

namespace app\modules\tariff\models;

use Yii;

/**
 * This is the model class for table "{{%option_tariff}}".
 *
 * @property string            $option_id
 * @property string            $tariff_id
 * @property string            $accrual
 * @property string            $area
 * @property string            $planting_price_day
 * @property string            $planting_price_night
 * @property string            $planting_include_day
 * @property string            $planting_include_night
 * @property string            $planting_include_day_time
 * @property string            $planting_include_night_time
 * @property string            $next_km_price_day
 * @property string            $next_km_price_night
 * @property string            $min_price_day
 * @property string            $second_min_price_day
 * @property string            $min_price_night
 * @property string            $second_min_price_night
 * @property string            $supply_price_day
 * @property string            $supply_price_night
 * @property integer           $wait_time_day
 * @property integer           $wait_time_night
 * @property integer           $wait_driving_time_day
 * @property integer           $wait_driving_time_night
 * @property string            $wait_price_day
 * @property string            $wait_price_night
 * @property integer           $speed_downtime_day
 * @property integer           $speed_downtime_night
 * @property string            $rounding_day
 * @property string            $rounding_night
 * @property string            $rounding_type_day
 * @property string            $rounding_type_night
 * @property string            $tariff_type
 * @property string            $allow_day_night
 * @property string            $start_day
 * @property string            $end_day
 * @property integer           $active
 * @property integer           $calculation_fix
 * @property integer           $enabled_parking_ratio
 * @property integer           $next_cost_unit_day
 * @property integer           $next_cost_unit_night
 *
 * @property TblFixHasOption[] $tblFixHasOptions
 * @property TblFixTariff[]    $fixes
 * @property TblTaxiTariff     $tariff
 */
class OptionTariff extends \yii\db\ActiveRecord
{

    //$accrual
    const ACCRUAL_FIX = 'FIX';
    const ACCRUAL_DISTANCE = 'DISTANCE';
    const ACCRUAL_TIME = 'TIME';
    const ACCRUAL_MIX = 'MIXED';
    const ACCRUAL_INTERVAL = 'INTERVAL';
    //$tariff_type
    const CURRENT_TYPE = 'CURRENT';
    const HOLIDAYS_TYPE = 'HOLIDAYS';
    const EXCEPTIONS_TYPE = 'EXCEPTIONS';
    //$area
    const AREA_CITY = 'CITY';
    const AREA_TRACK = 'TRACK';
    const AREA_RAILWAY = 'RAILWAY';
    const AREA_AIRPORT = 'AIRPORT';

    const ROUNDING_TYPE_FLOOR = 'FLOOR';
    const ROUNDING_TYPE_ROUND = 'ROUND';
    const ROUNDING_TYPE_CEIL = 'CEIL';

    const DISTANCE_NEXT_COST_UNIT = '1_km';

    public $action_dates;
    /**
     * For mixed accrual
     */
    public $next_cost_unit_day_time = '1_minute';
    public $next_cost_unit_night_time = '1_minute';
    //---------------------------------
    /**
     * For interval accrual
     */
    public $next_km_price_interval_day;
    public $next_km_price_interval_night;
    //---------------------------------
    private $_old_accrual;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%option_tariff}}';
    }

    public function init()
    {
        parent::init();
        //Init vars of default values
        $this->loadDefaultValues();
        $this->setDefaultIntervalNextPriceValues();
        $this->next_km_price_day   = getValue($this->next_km_price_day, 0);
        $this->next_km_price_night = getValue($this->next_km_price_night, 0);
        //-----------------------------------------
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['area', 'tariff_type', 'accrual'], 'required'],
            [
                ['action_dates'],
                'required',
                'requiredValue' => true,
                'when'          => function ($model) {
                    return $model->tariff_type == self::HOLIDAYS_TYPE && $model->area == self::AREA_CITY;
                },
                'whenClient'    => 'function(attr,value){
                    return $("#activity_holidays").prop("checked");
                }',
                'message'       => t('taxi_tariff', 'Not selected period of activity rate "{tariff_type}"',
                    ['tariff_type' => self::getTariffTypeNameByType(self::HOLIDAYS_TYPE)]),
            ],
            [
                ['action_dates'],
                'required',
                'requiredValue' => true,
                'when'          => function ($model) {
                    return $model->tariff_type == self::EXCEPTIONS_TYPE && $model->area == self::AREA_CITY;
                },
                'whenClient'    => 'function(attr,value){
                    return $("#activity_exceptions").prop("checked");
                }',
                'message'       => t('taxi_tariff', 'Not selected period of activity rate "{tariff_type}"',
                    ['tariff_type' => self::getTariffTypeNameByType(self::EXCEPTIONS_TYPE)]),
            ],
            [
                ['tariff_id', 'wait_time_day', 'wait_time_night', 'wait_driving_time_day', 'wait_driving_time_night'],
                'integer',
            ],
            [
                [
                    'accrual',
                    'area',
                    'tariff_type',
                    'start_day',
                    'end_day',
                    'next_cost_unit_day',
                    'next_cost_unit_night',
                    'next_cost_unit_day_time',
                    'next_cost_unit_night_time',
                ],
                'string',
            ],
            [
                [
                    'planting_price_day',
                    'planting_price_night',
                    'planting_include_day',
                    'planting_include_night',
                    'next_km_price_day',
                    'next_km_price_night',
                    'min_price_day',
                    'second_min_price_day',
                    'min_price_night',
                    'second_min_price_night',
                    'supply_price_day',
                    'supply_price_night',
                    'wait_price_day',
                    'wait_price_night',
                    'rounding_day',
                    'rounding_night',
                    'speed_downtime_day',
                    'speed_downtime_night',
                    'planting_include_day_time',
                    'planting_include_night_time',
                ],
                'number',
            ],
            [
                [
                    'planting_price_day',
                    'planting_price_night',
                    'planting_include_day',
                    'planting_include_night',
                    'planting_include_day_time',
                    'planting_include_night_time',
                    'next_km_price_day',
                    'next_km_price_night',
                    'min_price_day',
                    'second_min_price_day',
                    'min_price_night',
                    'second_min_price_night',
                    'supply_price_day',
                    'supply_price_night',
                    'wait_price_day',
                    'wait_price_night',
                    'rounding_day',
                    'rounding_night',
                    'wait_time_day',
                    'wait_time_night',
                    'wait_driving_time_day',
                    'wait_driving_time_night',
                    'speed_downtime_day',
                    'speed_downtime_night',
                    'next_km_price_day_time',
                    'next_km_price_night_time',
                ],
                'default',
                'value' => 0,
            ],
            [['allow_day_night'], 'default', 'value' => 1],
            [['next_cost_unit_day', 'next_cost_unit_night'], 'default', 'value' => '1_km'],
            [['active', 'allow_day_night', 'calculation_fix', 'enabled_parking_ratio'], 'boolean'],
            [['rounding_type_day', 'rounding_type_night'], 'in', 'range' => self::getRoundingTypeList()],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'option_id'               => Yii::t('taxi_tariff', 'Option ID'),
            'tariff_id'               => Yii::t('taxi_tariff', 'Tariff ID'),
            'accrual'                 => Yii::t('taxi_tariff', 'Accrual'),
            'area'                    => Yii::t('taxi_tariff', 'Area'),
            'planting_price_day'      => Yii::t('taxi_tariff', 'Planting price'),
            'planting_price_night'    => Yii::t('taxi_tariff', 'Planting price'),
            'planting_include_day'    => Yii::t('taxi_tariff', 'Included in the cost of planting'),
            'planting_include_night'  => Yii::t('taxi_tariff', 'Included in the cost of planting'),
            'next_km_price_day'       => Yii::t('taxi_tariff', 'Price next'),
            'next_km_price_night'     => Yii::t('taxi_tariff', 'Price next'),
            'min_price_day'           => Yii::t('taxi_tariff', 'Min price of trip'),
            'min_price_night'         => Yii::t('taxi_tariff', 'Min price of trip'),
            'second_min_price_day'    => Yii::t('taxi_tariff',
                'The added price to the minimum for each point, starting with the third'),
            'second_min_price_night'  => Yii::t('taxi_tariff',
                'The added price to the minimum for each point, starting with the third'),
            'supply_price_day'        => Yii::t('taxi_tariff', 'Cost of 1 km to the feed address'),
            'supply_price_night'      => Yii::t('taxi_tariff', 'Cost of 1 km to the feed address'),
            'wait_time_day'           => Yii::t('taxi_tariff', 'Free waiting time before a trip'),
            'wait_time_night'         => Yii::t('taxi_tariff', 'Free waiting time before a trip'),
            'wait_driving_time_day'   => Yii::t('taxi_tariff', 'Free waiting time during a trip one time'),
            'wait_driving_time_night' => Yii::t('taxi_tariff', 'Free waiting time during a trip one time'),
            'wait_price_day'          => Yii::t('taxi_tariff', 'Cost of 1 minute waiting'),
            'wait_price_night'        => Yii::t('taxi_tariff', 'Cost of 1 minute waiting'),
            'speed_downtime_day'      => Yii::t('taxi_tariff', 'Speed downtime'),
            'speed_downtime_night'    => Yii::t('taxi_tariff', 'Speed downtime'),
            'rounding_day'            => Yii::t('taxi_tariff', 'Rounding'),
            'rounding_night'          => Yii::t('taxi_tariff', 'Rounding'),
            'rounding_type_day'       => Yii::t('taxi_tariff', 'Rounding rule'),
            'rounding_type_night'     => Yii::t('taxi_tariff', 'Rounding rule'),
            'tariff_type'             => Yii::t('taxi_tariff', 'Tariff Type'),
            'allow_day_night'         => Yii::t('taxi_tariff', 'Allow day and night'),
            'calculation_fix'         => Yii::t('taxi_tariff', 'Calculation fix'),
            'enabled_parking_ratio'   => Yii::t('taxi_tariff', 'Сorrection factor to take into account parking'),
        ];
    }

    public static function getRoundingTypeList()
    {
        return [
            self::ROUNDING_TYPE_CEIL,
            self::ROUNDING_TYPE_FLOOR,
            self::ROUNDING_TYPE_ROUND,
        ];
    }

    public static function getRoundingTypeMap()
    {
        return [
            self::ROUNDING_TYPE_CEIL  => t('taxi_tariff', 'to greater'),
            self::ROUNDING_TYPE_ROUND => t('taxi_tariff', 'to nearest'),
            self::ROUNDING_TYPE_FLOOR => t('taxi_tariff', 'to smaller'),
        ];
    }

    public function afterFind()
    {
        parent::afterFind();
        if ($this->accrual == self::ACCRUAL_MIX) {
            $this->next_cost_unit_day_time   = $this->next_cost_unit_day;
            $this->next_cost_unit_night_time = $this->next_cost_unit_night;
        } elseif ($this->accrual == self::ACCRUAL_INTERVAL) {
            $this->next_km_price_interval_day = unserialize($this->next_km_price_day);
            if (empty($this->next_km_price_interval_day)) {
                $this->next_km_price_interval_day = $this->getDefaultIntervalNextPriceValues();
            }

            $this->next_km_price_interval_night = unserialize($this->next_km_price_night);
            if (empty($this->next_km_price_interval_night)) {
                $this->next_km_price_interval_night = $this->getDefaultIntervalNextPriceValues();
            }

            $this->next_km_price_day   = 0;
            $this->next_km_price_night = 0;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFixHasOptions()
    {
        return $this->hasMany(FixHasOption::className(), ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFixes()
    {
        return $this->hasMany(FixTariff::className(), ['fix_id' => 'fix_id'])->viaTable('{{%fix_has_option}}',
            ['option_id' => 'option_id']);
    }

    public static function getTariffTypeNameByType($tariff_type)
    {
        $arTariffTypes = [
            self::CURRENT_TYPE    => t('taxi_tariff', self::CURRENT_TYPE),
            self::HOLIDAYS_TYPE   => t('taxi_tariff', self::HOLIDAYS_TYPE),
            self::EXCEPTIONS_TYPE => t('taxi_tariff', self::EXCEPTIONS_TYPE),
        ];

        return getValue($arTariffTypes[$tariff_type]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(TaxiTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * Variants of tariff accruals
     * @return array
     */
    public static function getAccruals()
    {
        return [
            self::ACCRUAL_DISTANCE => t('taxi_tariff', 'Distance'),
            self::ACCRUAL_TIME     => t('taxi_tariff', 'Time'),
            self::ACCRUAL_FIX      => t('taxi_tariff', 'Fix'),
            self::ACCRUAL_MIX      => t('taxi_tariff', 'For distance and time'),
            self::ACCRUAL_INTERVAL => t('taxi_tariff', 'Distance range'),
        ];
    }

    public function is_fix()
    {
        return $this->accrual == self::ACCRUAL_FIX;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (!$insert) {
            if ($this->_old_accrual == self::ACCRUAL_FIX && $this->accrual != self::ACCRUAL_FIX) {
                FixHasOption::deleteAll(['option_id' => $this->option_id]);
            }
        }
    }

    public function beforeSave($insert)
    {
        if (!$insert) {
            $this->_old_accrual = $this->getOldAttribute('accrual');
        }

        if ($this->accrual === self::ACCRUAL_DISTANCE) {
            $this->next_cost_unit_day   = self::DISTANCE_NEXT_COST_UNIT;
            $this->next_cost_unit_night = self::DISTANCE_NEXT_COST_UNIT;
        } elseif ($this->accrual === self::ACCRUAL_MIX) {
            $this->next_cost_unit_day   = $this->next_cost_unit_day_time;
            $this->next_cost_unit_night = $this->next_cost_unit_night_time;
        } elseif ($this->accrual == self::ACCRUAL_INTERVAL) {
            $this->next_km_price_day   = serialize($this->next_km_price_day);
            $this->next_km_price_night = serialize($this->next_km_price_night);
        }

        return parent::beforeSave($insert);
    }

    /**
     * Getting select variants for next_cost_unit_day or next_cost_unit_night.
     *
     * @param integer $city_id It needs for getting currency symbol of current city.
     *
     * @return array
     */
    public static function getNextCostUnitList($city_id)
    {
        $currencySymbol = getCurrencySymbol($city_id);

        return [
            '1_minute'  => $currencySymbol . '/' . t('app', 'min.'),
            '30_minute' => $currencySymbol . '/30 ' . t('app', 'min.'),
            '1_hour'    => $currencySymbol . '/' . t('app', 'hour'),
        ];
    }

    public function getDefaultIntervalNextPriceValues()
    {
        return [
            [
                'interval' => [0, 0],
                'price'    => 0,
            ],
            [
                'interval' => [0, 0],
                'price'    => 0,
            ],
            [
                'interval' => [0, 0],
                'price'    => 0,
            ],
            [
                'interval' => [0],
                'price'    => 0,
            ],
        ];
    }

    public function setDefaultIntervalNextPriceValues()
    {
        $interval_value = $this->getDefaultIntervalNextPriceValues();

        $this->next_km_price_interval_day   = $interval_value;
        $this->next_km_price_interval_night = $interval_value;
    }

}
