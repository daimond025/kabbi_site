<?php

namespace app\modules\tariff\models;

use Yii;

/**
 * This is the model class for table "tbl_option_active_date".
 *
 * @property string     $date_id
 * @property string     $tariff_id
 * @property string     $active_date
 * @property string     $tariff_type
 *
 * @property TaxiTariff $tariff
 */
class OptionActiveDate extends \yii\db\ActiveRecord
{

    const PATTERN_WEEKS = '(Monday|Tuesday|Wednesday|Thursday|Friday|Saturday|Sunday)';
    const PATTERN_DAYS = '(((0[1-9]|[12]\d)\.(0[1-9]|1[012])|31\.(0[13578]|1[02])|30\.(0[13-9]|1[012])|(0[1-9]|[12]\d)\.)(\.(20)\d\d)?)';
    const PATTERN_INTERVAL = '(([0-1]\d|2[0-3])(:[0-5]\d)-([0-1]\d|2[0-3])(:[0-5]\d))';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%option_active_date}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id', 'active_date', 'tariff_type'], 'required'],
            [['tariff_id'], 'integer'],
            [['tariff_type'], 'string'],
            [
                ['active_date'],
                'match',
                'pattern' => '/^(' . self::PATTERN_WEEKS . '|' . self::PATTERN_DAYS . ')(\|' . self::PATTERN_INTERVAL . ')?$/',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'date_id'     => Yii::t('tariff', 'Date ID'),
            'tariff_id'   => Yii::t('tariff', 'Tariff ID'),
            'active_date' => Yii::t('tariff', 'Active Date'),
            'tariff_type' => Yii::t('tariff', 'Tariff Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(TaxiTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * Allow to save multiple rows
     *
     * @param array $arOptions
     *
     * @return bool|integer number of rows affected by the execution.
     * @throws Exception execution failed
     */
    public function manySave(array $arOptions)
    {
        if (!empty($arOptions)) {
            $arOptions   = array_unique($arOptions);
//            $insertValue = [];
//            $connection  = app()->db;

            foreach ($arOptions as $date) {
                $model = new OptionActiveDate([
                    'tariff_id' => $this->tariff_id,
                    'active_date' => $date,
                    'tariff_type' => $this->tariff_type
                ]);
//                if(!$model->validate()) dd($model->errors);
                $model->save();

//                if (!empty($date)) {
//                    $insertValue[] = [$this->tariff_id, $date, $this->tariff_type];
//                }
            }

//            if (!empty($insertValue)) {
//                return $connection->createCommand()->batchInsert(self::tableName(),
//                    ['tariff_id', 'active_date', 'tariff_type'], $insertValue)->execute();
//            }
        }

        return false;
    }
}
