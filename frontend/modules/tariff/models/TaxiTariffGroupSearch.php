<?php

namespace app\modules\tariff\models;

use Yii;
use yii\base\Model;

/**
 * TaxiTariffGroupSearch represents the model behind the search form about `app\modules\tariff\models\TaxiTariffGroup`.
 */
class TaxiTariffGroupSearch extends TaxiTariffGroup
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = self::find()->getListByCity($this->getCity(), ['position_id' => $this->position_id]);
        $query->block($this->block);
        $query->andFilterWhere(['like', 'name', $this->name]);

        return $query;
    }

    public function tariffs()
    {
        $query = self::find();
        $query->where(['tenant_id' => user()->tenant_id]);
        $query->orderBy(['sort' => SORT_ASC, 'name' => SORT_ASC]);

        return $query;
    }

    public function hasTariffs($array)
    {
        $query = $this->tariffs();
        $query->andWhere(['group_id' => $this->getGroupList($array) ]);
        return $query;
    }
    
    private function getGroupList($array) 
    {
        $result = [];
        
        foreach ($array as $key => $item) {
            if( !empty($item->allModels) ) {
                $result[$key] = $key;
            }
            
        }
        return $result;
    }
}
