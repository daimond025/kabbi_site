<?php

namespace app\modules\tariff\models;

use app\modules\client\models\ClientCompany;
use app\modules\order\models\Order;
use common\components\behaviors\ActiveRecordBehavior;
use common\modules\car\models\CarOption;
use common\modules\city\models\City;
use common\modules\employee\models\position\Position;
use common\modules\tenant\models\Tenant;
use common\modules\tenant\models\TenantHasCarOption;
use common\modules\tenant\models\TenantHelper;
use frontend\components\behavior\file\CropFileBehavior;
use frontend\modules\car\models\CarClass;
use frontend\modules\employee\components\position\PositionService;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%taxi_tariff}}".
 *
 * @property string                                $tariff_id
 * @property string                                $tenant_id
 * @property integer                               $class_id
 * @property integer                               $block
 * @property integer                               $group_id
 * @property string                                $name
 * @property string                                $description
 * @property integer                               $sort
 * @property integer                               $auto_downtime
 * @property integer                               $enabled_site
 * @property integer                               $enabled_app
 * @property integer                               $enabled_operator
 * @property integer                               $enabled_bordur
 * @property integer                               $position_id
 * @property string                                $type
 * @property integer[]                             $company_ids
 *
 * @property AdditionalOption[]                    $additionalOptions
 * @property OptionActiveDate[]                    $optionActiveDates
 * @property OptionTariff[]                        $optionTariffs
 * @property TaxiTariffGroup                       $group
 * @property \frontend\modules\car\models\CarClass $class
 * @property Tenant                                $tenant
 * @property TaxiTariffHasCity[]                   $taxiTariffHasCities
 * @property TaxiTariffHasCompany[]                $hasCompanies
 * @property City[]                                $cities
 * @mixin    CropFileBehavior
 */
class TaxiTariff extends ActiveRecord
{
    public $cropParams;
    public $city_list = [];
    public $company_ids = [];

    const TYPE_BASE = 'BASE';
    const TYPE_COMPANY = 'COMPANY';
    const TYPE_COMPANY_CORP_BALANCE = 'COMPANY_CORP_BALANCE';
    const TYPE_ALL = 'ALL';

    public function init()
    {
        parent::init();
        $this->loadDefaultValues();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%taxi_tariff}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'position_id', 'type'], 'required'],
            [
                [
                    'class_id',
                    'block',
                    'sort',
                    'enabled_site',
                    'enabled_app',
                    'enabled_operator',
                    'enabled_bordur',
                    'enabled_cabinet',
                ],
                'integer',
            ],
            [['city_list', 'group_id', 'company_ids', 'cropParams'], 'safe'],
            [['name'], 'string', 'max' => 250],
            [['auto_downtime'], 'in', 'range' => [0, 1]],
            [['description'], 'string', 'max' => 1000],
            [
                'logo',
                'image',
                'extensions' => ['png', 'jpeg', 'jpg'],
                'maxSize'    => getUploadMaxFileSize(),
                'tooBig'     => t('file', 'The file size must be less than {file_size}Mb',
                    ['file_size' => getUploadMaxFileSize(true)]),
                'minWidth'   => 360,
                'minHeight'  => 160,
            ],
            [
                ['class_id'],
                'required',
                'when'       => function ($model) {
                    return in_array($model->position_id,
                        Yii::$app->getModule('employee')->get('position')->getPositionIdsHasCar());
                },
                'whenClient' => 'function(attr,value){
                    var position_id = $("#taxitariff-position_id").val();
                    return $("[data-position=\'" + position_id + "\']").size() > 0;
                }',
            ],
            ['type', 'in', 'range' => array_keys(self::getTypeList())],
            [
                'company_ids',
                'required',
                'when'       => function ($model) {
                    return $model->type === self::TYPE_COMPANY || $model->type === self::TYPE_COMPANY_CORP_BALANCE;
                },
                'whenClient' => 'function(attr,value){
                    var type = $("#taxitariff-type").val();
                    return type == "' . self::TYPE_COMPANY . '" || type == "' . self::TYPE_COMPANY_CORP_BALANCE . '";
                }',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tariff_id'        => Yii::t('tariff', 'Tariff ID'),
            'tenant_id'        => Yii::t('tariff', 'Tenant ID'),
            'class_id'         => Yii::t('taxi_tariff', 'Tariff type'),
            'city_list'        => t('taxi_tariff', 'The city where tariff will work'),
            'block'            => t('app', 'Block'),
            'group_id'         => Yii::t('taxi_tariff', 'Group'),
            'name'             => Yii::t('taxi_tariff', 'Name'),
            'description'      => Yii::t('taxi_tariff', 'Description'),
            'sort'             => Yii::t('app', 'Sort'),
            'auto_downtime'    => t('taxi_tariff', 'Enabling standby'),
            'enabled_site'     => Yii::t('taxi_tariff', 'Site'),
            'enabled_app'      => Yii::t('taxi_tariff', 'Application'),
            'enabled_operator' => Yii::t('taxi_tariff', 'Operator'),
            'enabled_bordur'   => Yii::t('taxi_tariff', 'Driver (Bordur)'),
            'enabled_cabinet'  => t('taxi_tariff', 'Personal cabinet'),
            'logo'             => 'Logo',
            'position_id'      => t('employee', 'Profession'),
            'type'             => t('taxi_tariff', 'Is available'),
            'company_ids'      => t('app', 'Organization'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddOptions()
    {
        return $this->hasMany(AdditionalOption::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(TaxiTariffGroup::className(), ['group_id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptionActiveDates()
    {
        return $this->hasMany(OptionActiveDate::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOption()
    {
        return $this->hasMany(OptionTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClass()
    {
        return $this->hasOne(CarClass::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffHasCity()
    {
        return $this->hasMany(TaxiTariffHasCity::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(),
            ['city_id' => 'city_id'])->viaTable('{{%taxi_tariff_has_city}}', ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHasCompanies()
    {
        return $this->hasMany(TaxiTariffHasCompany::className(), ['tariff_id' => 'tariff_id']);
    }

    public function behaviors()
    {
        return [
            'common' => [
                'class' => ActiveRecordBehavior::className(),
            ],
            'logo'   => [
                'class'                => CropFileBehavior::className(),
                'fileField'            => ['logo'],
                'upload_dir'           => app()->params['upload'],
                'thumb_width'          => 180,
                'thumb_height'         => 80,
                'small_picture_width'  => 360,
                'small_picture_height' => 160,
                'big_picture_width'    => 360,
                'big_picture_height'   => 160,
            ],
        ];
    }

    /**
     * It needs for form.
     * @return array
     */
    public function getFormData()
    {
        $city       = reset($this->cities);
        $addOptions = TenantHasCarOption::find()
            ->alias('t')
            ->select('t.option_id, o.name')
            ->where([
                't.tenant_id' => user()->tenant_id,
                't.city_id'   => $city->city_id,
                't.is_active' => '1',
            ])
            ->joinWith('option o', false)
            ->asArray()
            ->all();

        if (!$addOptions) {
            $addOptions = CarOption::find()->all();
        }

        $companies       = ClientCompany::findAll([
            'block'     => 0,
            'tenant_id' => user()->tenant_id,
        ]);
        $companiesName   = ArrayHelper::map($companies, 'company_id', function ($model) {
            return Html::encode($model->name);
        });
        $companiesCityId = ArrayHelper::map($companies, 'company_id', function ($model) {
            return $model->city->city_id;
        });

        $userCityList = user()->getUserCityList();

        /** @var PositionService $positionService */
        $positionService = Yii::$app->getModule('employee')->get('position');

        $positionMap = !empty($this->city_list) ? $positionService->getPositionMapByCity($this->city_list) : [];

        if (!array_key_exists($this->position_id, $positionMap)) {
            $tariffPosition = $positionService->getPositionById($this->position_id);
            $positionMap    = ArrayHelper::merge($positionMap,
                [$tariffPosition->position_id => t('employee', $tariffPosition->name)]);
        }

        return [
            'CITY'               => $userCityList,
            'TYPE'               => ArrayHelper::map(CarClass::find()->all(), 'class_id', function ($array) {
                return t('car', $array['class']);
            }),
            'ACCRUALS'           => OptionTariff::getAccruals(),
            'ADD_OPTIONS'        => ArrayHelper::map(CarOption::find()->all(), 'option_id', function ($element) {
                return t('car-options', $element['name']);
            }),
            'ACTIVE_ADD_OPTIONS' => self::getActiveCarOptionList($this->city_list),
            'GROUP'              => ArrayHelper::merge(['no_group' => t('taxi_tariff', 'No group')],
                ArrayHelper::map(TaxiTariffGroup::getGroups(), 'group_id', 'name')),
            'CAR_POSITION_LIST'  => $positionService->getPositionIdsHasCar(),
            'POSITION_MAP'       => $positionMap,
            'AUTO_DOWNTIME'      => [
                1 => t('taxi_tariff', 'Automatically by GPS'),
                0 => t('taxi_tariff', 'Manually via a button'),
            ],
            'ACCESS'             => self::getTypeList(),
            'COMPANIES_NAME'     => $companiesName,
            'COMPANIES_CITY_ID'  => $companiesCityId,
        ];
    }

    public static function getActiveCarOptions($city_id)
    {
        return CarOption::find()
            ->joinWith([
                'tenantHasCarOptions' => function (ActiveQuery $q) use ($city_id) {
                    $q->where([
                        'is_active' => 1,
                        'tenant_id' => user()->tenant_id,
                        'city_id'   => $city_id,
                    ]);
                },
            ], false)
            ->all();
    }

    public static function getActiveCarOptionList($city_id)
    {
        $addOptions = self::getActiveCarOptions($city_id);

        return ArrayHelper::getColumn($addOptions, 'option_id');
    }

    public static function getActiveCarOptionMap($city_id)
    {
        $addOptions = self::getActiveCarOptions($city_id);

        return ArrayHelper::map($addOptions, 'option_id', function ($element) {
            return t('car-options', $element['name']);
        });
    }

    /**
     * Testing the existence of tariff
     *
     * @param integer $class_id
     * @param integer $city_id
     *
     * @return bool
     */
    public static function is_exists($class_id, $city_id)
    {
        return self::find()
            ->where([
                'tenant_id' => user()->tenant_id,
                'class_id'  => $class_id,
            ])
            ->joinWith([
                'tariffHasCity' => function (ActiveQuery $query) use ($city_id) {
                    $query->andWhere(['city_id' => $city_id]);
                },
            ], false)
            ->exists();
    }

    /**
     * Validate option tariff.
     *
     * @param \app\modules\tariff\models\OptionTariff $cityObj
     * @param \app\modules\tariff\models\OptionTariff $trackObj
     * @param \app\modules\tariff\models\OptionTariff $airportObj
     * @param \app\modules\tariff\models\OptionTariff $railwayObj
     * @param array                                   $optionData
     * @param string                                  $tariff_type
     */
    public function optionValidate(
        OptionTariff $cityObj,
        OptionTariff $trackObj,
        OptionTariff $airportObj,
        OptionTariff $railwayObj,
        array $optionData,
        $tariff_type
    ) {

        //Заполнение - Город
        $cityObjData = ArrayHelper::merge(
            $optionData['OptionTariff']['city'],
            [
                'tariff_type'  => $tariff_type,
                'area'         => OptionTariff::AREA_CITY,
                'action_dates' => $tariff_type !== OptionTariff::CURRENT_TYPE && !empty($optionData['ActiveDate']),
                'active'       => 1,
            ]);
        $cityObj->load($cityObjData, '');
        $cityObj->validate();

        //Проверка наличия парковок
        if ($cityObj->is_fix()) {
            if (!isset($optionData['FixOptions']['city']) || empty($optionData['FixOptions']['city'])) {
                $cityObj->addError('accrual', t('taxi_tariff', 'Do not set the parking!'));
            }
        }

        if ($cityObj->hasErrors()) {
            $this->addErrors($cityObj->getErrors());
        }

        //Заполнение - Загород
        $trackObj->load($optionData['OptionTariff']['track'], '');

        $trackObj->tariff_type = $tariff_type;
        $trackObj->area        = OptionTariff::AREA_TRACK;
        $trackObj->validate();

        //Проверка наличия парковок
        if ($trackObj->is_fix()) {
            if (!isset($optionData['FixOptions']['outCity']) || empty($optionData['FixOptions']['outCity'])) {
                $trackObj->addError('accrual', t('taxi_tariff', 'Do not set the parking!'));
            }
        }

        if ($trackObj->hasErrors()) {
            $this->addErrors($trackObj->getErrors());
        }

        //Заполнение Аэропорт
        if (isset($optionData['FixOptions']['airport'])) {
            $airportObj->load($optionData['FixOptions']['airport']['options'], '');
            $airportObj->area        = OptionTariff::AREA_AIRPORT;
            $airportObj->tariff_type = $tariff_type;
            $airportObj->accrual     = OptionTariff::ACCRUAL_FIX;
            $airportObj->validate();

            if ($airportObj->hasErrors()) {
                $this->addErrors($airportObj->getErrors());
            }
        }

        //Заполнение ЖД
        if (isset($optionData['FixOptions']['station'])) {
            $railwayObj->load($optionData['FixOptions']['station']['options'], '');
            $railwayObj->area        = OptionTariff::AREA_RAILWAY;
            $railwayObj->tariff_type = $tariff_type;
            $railwayObj->accrual     = OptionTariff::ACCRUAL_FIX;
            $railwayObj->validate();

            if ($railwayObj->hasErrors()) {
                $this->addErrors($railwayObj->getErrors());
            }
        }
    }

    /**
     * Create tariff option.
     *
     * @param int                                     $cityId
     * @param \app\modules\tariff\models\OptionTariff $cityObj
     * @param \app\modules\tariff\models\OptionTariff $trackObj
     * @param \app\modules\tariff\models\OptionTariff $airportObj
     * @param \app\modules\tariff\models\OptionTariff $railwayObj
     * @param array                                   $optionData
     * @param string                                  $tariff_type
     *
     * @return bool
     */
    public function saveOptiontariff(
        $cityId,
        OptionTariff $cityObj,
        OptionTariff $trackObj,
        OptionTariff $airportObj,
        OptionTariff $railwayObj,
        array $optionData,
        $tariff_type
    ) {
        if (empty($optionData)) {
            return false;
        }

        if ($tariff_type == OptionTariff::HOLIDAYS_TYPE || $tariff_type == OptionTariff::EXCEPTIONS_TYPE) {
            $active = $optionData['active'];
            if (!$active) {
                if (!$cityObj->isNewRecord) {
                    OptionTariff::updateAll(['active' => 0],
                        ['tariff_id' => $this->tariff_id, 'tariff_type' => $tariff_type]);
                }

                return false;
            }
        } else {
            $active = 1;
        }

        //Сохранение - "Город"
        $cityObj->tariff_id             = $this->tariff_id;
        $cityObj->allow_day_night       = $optionData['allow_day_night'];
        $cityObj->calculation_fix       = $optionData['calculation_fix'];
        $cityObj->enabled_parking_ratio = $optionData['enabled_parking_ratio'];
        $cityObj->start_day             = $optionData['start_day'];
        $cityObj->end_day               = $optionData['end_day'];
        $cityObj->active                = $active;
        if ($optionData['OptionTariff']['city']['accrual'] == OptionTariff::ACCRUAL_INTERVAL) {
            $cityObj->next_km_price_day   = $optionData['OptionTariff']['city']['next_km_price_interval_day'];
            $cityObj->next_km_price_night = $optionData['OptionTariff']['city']['next_km_price_interval_night'];
        }
        $cityObj->save(false);

        //Сохранение фикса "Город"
        if ($cityObj->is_fix()) {
            if (!$cityObj->isNewRecord) {
                FixHasOption::deleteAll(['option_id' => $cityObj->option_id]);
            }

            FixHasOption::manySave($optionData['FixOptions']['city']['fix'], $cityObj->option_id);
        }

        //Сохранение - "Загород"
        $trackObj->tariff_id             = $this->tariff_id;
        $trackObj->allow_day_night       = $optionData['allow_day_night'];
        $trackObj->calculation_fix       = $optionData['calculation_fix'];
        $trackObj->enabled_parking_ratio = $optionData['enabled_parking_ratio'];
        $trackObj->start_day             = $optionData['start_day'];
        $trackObj->end_day               = $optionData['end_day'];
        $trackObj->active                = $active;
        if ($optionData['OptionTariff']['track']['accrual'] == OptionTariff::ACCRUAL_INTERVAL) {
            $trackObj->next_km_price_day   = $optionData['OptionTariff']['track']['next_km_price_interval_day'];
            $trackObj->next_km_price_night = $optionData['OptionTariff']['track']['next_km_price_interval_night'];
        }
        $trackObj->save(false);

        //Сохранение фикса "Загород"
        if ($trackObj->is_fix()) {
            if (!$trackObj->isNewRecord) {
                FixHasOption::deleteAll(['option_id' => $trackObj->option_id]);
            }

            FixHasOption::manySave($optionData['FixOptions']['outCity']['fix'], $trackObj->option_id);
        }

        //Сохранение - "Аэропорт"
        if (isset($optionData['FixOptions']['airport'])) {
            $airportObj->tariff_id = $this->tariff_id;
            $airportObj->save(false);

            if (!$airportObj->isNewRecord) {
                FixHasOption::deleteAll(['option_id' => $airportObj->option_id]);
            }

            FixHasOption::manySave($optionData['FixOptions']['airport']['fix'], $airportObj->option_id);
        } elseif (!$airportObj->isNewRecord) {
            $airportObj->delete();
        }

        //Сохранение - "ЖД"
        if (isset($optionData['FixOptions']['station'])) {
            $railwayObj->tariff_id = $this->tariff_id;
            $railwayObj->save(false);

            if (!$railwayObj->isNewRecord) {
                FixHasOption::deleteAll(['option_id' => $railwayObj->option_id]);
            }

            FixHasOption::manySave($optionData['FixOptions']['station']['fix'], $railwayObj->option_id);
        } elseif (!$railwayObj->isNewRecord) {
            $railwayObj->delete();
        }

        //Сохранение доп. опций
        if (isset($optionData['AdditionalOption'])) {
            $add_option              = new AdditionalOption();
            $add_option->tariff_id   = $this->tariff_id;
            $add_option->tariff_type = $tariff_type;

            if (!$cityObj->isNewRecord) {
                AdditionalOption::deleteAll(['tariff_id' => $this->tariff_id, 'tariff_type' => $tariff_type]);
            }

            $availableOptions = self::getActiveCarOptionList($cityId);
            $options          = empty($optionData['AdditionalOption']['price']) ? [] : $optionData['AdditionalOption']['price'];
            $filteredOptions  = array_filter($options,
                function ($key) use ($availableOptions) {
                    return in_array($key, $availableOptions, false);
                }, ARRAY_FILTER_USE_KEY);

            $add_option->manySave($filteredOptions);
        } elseif (!$cityObj->isNewRecord) {
            AdditionalOption::deleteAll(['tariff_id' => $this->tariff_id, 'tariff_type' => $tariff_type]);
        }

        //Сохранение дат активности тарифа
        if (isset($optionData['ActiveDate'])) {
            $optionActiveDate              = new OptionActiveDate();
            $optionActiveDate->tariff_id   = $this->tariff_id;
            $optionActiveDate->tariff_type = $tariff_type;

            if (!$cityObj->isNewRecord) {
                OptionActiveDate::deleteAll(['tariff_id' => $this->tariff_id, 'tariff_type' => $tariff_type]);
            }

            $optionActiveDate->manySave($optionData['ActiveDate']);
        } elseif (!$cityObj->isNewRecord) {
            OptionActiveDate::deleteAll(['tariff_id' => $this->tariff_id, 'tariff_type' => $tariff_type]);
        }

        return true;
    }

    /**
     * Getting tenant taxi tariff list.
     *
     * @param null|int $city_id
     * @param array    $arVisible ['operator', 'cabinet']
     *
     * @return array ['tariff_list' => ['tariff_id' => 'class'], 'comfort_list' => ['option_id' => 'name']]
     */
    public static function getTenantTariffList($city_id = null, array $arVisible = [])
    {
        $arResult = [];

        $query = TaxiTariff::find()
            ->where(['tenant_id' => user()->tenant_id, 'block' => 0])
            ->orderBy(['sort' => SORT_ASC, 'name' => SORT_ASC])
            ->with([
                    'addOptions' => function (ActiveQuery $query) {
                        $query->select(['additional_option_id', 'tariff_id']);
                    },
                    'addOptions.option',
                ]
            )->joinWith([
                'tariffHasCity' => function (ActiveQuery $query) use ($city_id) {
                    $query->andFilterWhere(['city_id' => $city_id]);
                },
                'class',
            ])
            ->asArray();

        if (in_array('operator', $arVisible)) {
            $query->andWhere(['enabled_operator' => 1]);
        }

        if (in_array('cabinet', $arVisible)) {
            $query->andWhere(['enabled_cabinet' => 1]);
        }

        $tariffs = $query->all();

        foreach ($tariffs as $key => $tariff) {
            $arResult[$key] = [
                'tariff_id' => $tariff['tariff_id'],
                'name'      => isset($tariff['name']) ? $tariff['name'] : t('taxi_tariff', 'No name'),
            ];
            foreach ($tariff['addOptions'] as $value) {
                $arResult[$key]['comfort_list'][$value['option']['option_id']] = t('car-options',
                    $value['option']['name']);
            }
        }

        return $arResult;
    }

    public function afterFind()
    {
        $this->company_ids = [];
        array_walk($this->hasCompanies, function ($item) {
            $this->company_ids[] = (string)$item->company_id;
        });
    }

    public function beforeSave($insert)
    {
        $this->group_id = $this->group_id == 'no_group' ? null : $this->group_id;

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            TenantHelper::fillHelpItem(TenantHelper::CLIENT_TARIFF_FILL);
        }

        $this->saveCompanies();
    }

    // Сохранение компаний тарифа
    protected function saveCompanies()
    {
        $oldCompanies = ArrayHelper::map($this->hasCompanies, 'id', function ($model) {
            return (string)$model->company_id;
        });

        if ($this->type === self::TYPE_COMPANY || $this->type === self::TYPE_COMPANY_CORP_BALANCE) {
            $delete = array_diff($oldCompanies, $this->company_ids);
            $add    = array_diff($this->company_ids, $oldCompanies);
        } else { // Если тариф базовый, то удаляем все существующие компании тарифа
            $delete = $oldCompanies;
            $add    = [];
        }

        $this->deleteHasCompanies($delete);
        $this->addHasCompanies($add);
    }

    protected function deleteHasCompanies($data)
    {
        foreach ($this->hasCompanies as $item) {
            if (in_array($item->company_id, $data)) {
                $item->delete();
            }
        }
    }

    protected function addHasCompanies($data)
    {
        foreach ($data as $companyId) {
            $model = new TaxiTariffHasCompany([
                'tariff_id'  => $this->tariff_id,
                'company_id' => $companyId,
            ]);
            $model->save();
        }
    }

    /**
     * Getting tariffs filtered by conditions
     *
     * @param int[]        $cityIds
     * @param int          $classId
     * @param boolean|null $active
     * @param array        $roleFilter
     *
     * @return array
     */
    public static function getFilteredTariffs(
        $cityIds,
        $classId = null,
        $active = null,
        array $roleFilter = []
    ) {
        $query = self::find()
            ->alias('t')
            ->select([
                't.tariff_id',
                't.name',
                't.block',
                't.sort',
            ])
            ->joinWith([
                'tariffHasCity c' => function (ActiveQuery $subQuery) use ($cityIds) {
                    $subQuery->where(['c.city_id' => $cityIds]);
                },
            ], false)
            ->where([
                't.tenant_id' => user()->tenant_id,
                't.class_id'  => $classId,
            ])
            ->andFilterWhere(['t.block' => $active === null ? null : (int)!$active])
            ->asArray()
            ->orderBy(['t.sort' => SORT_ASC]);

        if (in_array('operator', $roleFilter, false)) {
            $query->andWhere(['t.enabled_operator' => 1]);
        }

        if (in_array('cabinet', $roleFilter, false)) {
            $query->andWhere(['t.enabled_cabinet' => 1]);
        }

        $tariffs = $query->all();

        return empty($tariffs) ? [] : $tariffs;
    }

    /**
     * @param array|int $city_list
     * @param array|int $position_list
     * @param int       $block
     * @param array     $arVisible ['operator', 'cabinet']
     *
     * @return array|false
     */
    public static function getTariffsGroupByClass($city_list, $position_list = null, $block = 0, array $arVisible = [])
    {
        $block = $block == -1 ? null : $block;

        $query = self::find()
            ->alias('t')
            ->where([
                'tenant_id' => user()->tenant_id,
            ])
            ->andFilterWhere(['block' => $block])
            ->andFilterWhere(['position_id' => $position_list])
            ->with('class')
            ->joinWith([
                'tariffHasCity' => function (ActiveQuery $query) use ($city_list) {
                    $query->where(['city_id' => $city_list]);
                },
            ], false)
            ->select(['t.tariff_id', 't.name', 't.class_id', 't.block', 't.sort', 't.description'])
            ->asArray()
            ->orderBy('class_id');

        if (in_array('operator', $arVisible)) {
            $query->andWhere(['enabled_operator' => 1]);
        }

        if (in_array('cabinet', $arVisible)) {
            $query->andWhere(['enabled_cabinet' => 1]);
        }

        $tariffs = $query->all();

        if (is_array($tariffs)) {
            $result = [];
            foreach ($tariffs as $tariff) {


                if (empty($tariff['class_id'])) {
                    continue;
                }
                if (empty($result[$tariff['class_id']])) {
                    $result[$tariff['class_id']] = [
                        'class_id' => $tariff['class']['class_id'],
                        'class'    => $tariff['class']['class'],
                        'type_id'  => $tariff['class']['type_id'],
                    ];
                }
                $result[$tariff['class_id']]['tariffs'][$tariff['tariff_id']] = [
                    'tariff_id'   => $tariff['tariff_id'],
                    'name'        => $tariff['name'],
                    'class_id'    => $tariff['class_id'],
                    'block'       => $tariff['block'],
                    'sort'        => +$tariff['sort'],
                    'description' => $tariff['description'],
                ];
            }

            return $result;
        } else {
            return false;
        }
    }

    /**
     * Getting map of tenant tariff list by city_id
     *
     * @param integer $city_id
     *
     * @return array ['tariff_id' => 'name']
     */
    public static function getTariffMap($city_id)
    {
        return ArrayHelper::map(self::getTenantTariffList($city_id), 'tariff_id', 'name');
    }

    public static function getTypeList()
    {
        return [
            self::TYPE_ALL                  => t('taxi_tariff', self::TYPE_ALL),
            self::TYPE_BASE                 => t('taxi_tariff', self::TYPE_BASE),
            self::TYPE_COMPANY              => t('taxi_tariff', self::TYPE_COMPANY),
            self::TYPE_COMPANY_CORP_BALANCE => t('taxi_tariff', self::TYPE_COMPANY_CORP_BALANCE),
        ];
    }
}
