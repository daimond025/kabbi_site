<?php

namespace app\modules\tariff\models;

use Yii;

/**
 * This is the model class for table "{{%taxi_tariff_has_city}}".
 *
 * @property string $tariff_id
 * @property string $city_id
 *
 * @property City $city
 * @property TaxiTariff $tariff
 */
class TaxiTariffHasCity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%taxi_tariff_has_city}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id', 'city_id'], 'required'],
            [['tariff_id', 'city_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tariff_id' => Yii::t('tariff', 'Tariff ID'),
            'city_id' => Yii::t('tariff', 'City ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(\common\modules\city\models\City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(TaxiTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * Allow to save multiple rows
     * @param array $arCity
     * @param integer $tariff_id
     * @return integer number of rows affected by the execution.
     * @throws Exception execution failed
     */
    public static function manySave($arCity, $tariff_id)
    {
        $insertValue = [];
        $connection = app()->db;

        if(empty($arCity))
            $arCity = \yii\helpers\ArrayHelper::getColumn(user()->cities, 'city_id');

        foreach($arCity as $city_id)
        {
            $insertValue[] = [$tariff_id, $city_id];
        }

        return $connection->createCommand()->batchInsert(self::tableName(), ['tariff_id', 'city_id'], $insertValue)->execute();
    }
}
