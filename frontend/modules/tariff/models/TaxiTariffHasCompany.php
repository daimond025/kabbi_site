<?php

namespace app\modules\tariff\models;

use app\modules\client\models\ClientCompany;

/**
 * This is the model class for table "{{%taxi_tariff_has_company}}".
 *
 * @property string $tariff_id
 * @property string $company_id
 */
class TaxiTariffHasCompany extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%taxi_tariff_has_company}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id', 'company_id'], 'required'],
            ['tariff_id', 'exist', 'targetClass' => TaxiTariff::className()],
            ['company_id', 'exist', 'targetClass' => ClientCompany::className()],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(ClientCompany::className(), ['company_id' => 'company_id']);
    }
}
