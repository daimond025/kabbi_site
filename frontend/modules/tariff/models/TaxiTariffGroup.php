<?php

namespace app\modules\tariff\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%taxi_tariff_group}}".
 *
 * @property integer $group_id
 * @property integer $tenant_id
 * @property string $name
 * @property integer $sort
 *
 * @property TaxiTariff[] $taxiTariffs
 */
class TaxiTariffGroup extends \yii\db\ActiveRecord
{

    public function init()
    {
        parent::init();
        $this->loadDefaultValues();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%taxi_tariff_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['sort'], 'integer'],
            [['sort'], 'default', 'value' => 100],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'group_id'  => 'Group ID',
            'tenant_id' => 'Tenant ID',
            'name'      => t('taxi_tariff', 'Name'),
            'sort'      => t('app', 'Sort'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxiTariffs()
    {
        return $this->hasMany(TaxiTariff::className(), ['group_id' => 'group_id']);
    }

    public function behaviors()
    {
        return [
            'common' => [
                'class' => \common\components\behaviors\ActiveRecordBehavior::className()
            ]
        ];
    }
    /**
     * Getting models from cache.
     * @return array app\modules\tariff\models\TaxiTariffGroup
     */
    public static function getGroups()
    {
        return self::find()->where(['tenant_id' => user()->tenant_id])->orderBy('name')->all();
    }

    public static function getGroupName($group){

        return $group == 'Без группы' ? t('taxi_tariff', 'No group') : Html::encode($group);
    }

}
