<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\tariff\assets;

use yii\web\AssetBundle;

class GroupAsset extends AssetBundle
{

    public $sourcePath = '@app/modules/tariff/assets/js';
    public $publishOptions = [
        'forceCopy' => true,
        'only'      => ['group.js']
    ];
    public $css = [
    ];
    public $js = [
        'group.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];

}
