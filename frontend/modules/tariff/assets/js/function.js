//Функция изменения прозрачности полей при установке флага "Учитывать день и ночь"
function day_night_reload(checkbox)
{
    var parent = checkbox.parents('div.add_plan');
    var day_night_str = parent.find('.tariff_change span').filter('.ap_top_d, .ap_top_n');
    var day_night_inputs = parent.find('div.day_night_block section:not(:first)');

    if(checkbox.prop('checked')) {
        day_night_inputs.show();
        day_night_str.show();
        parent.find('.night').show();
    } else {
        day_night_inputs.hide();
        day_night_str.hide();
        parent.find('.night').hide();
    }
}

function cityFilterInit()
{
    $('form[name="tariff_search"] ul li input[type="checkbox"]').on('click',function(e){
        var parent = $(this).parents('div.active');
        var form = $(this).parents('form');
        var view_type = $(this).parents('div.active').data('view');
        var block = view_type === 'index' ? 0 : 1;
        $.post(
            form.attr('action'),
            form.serialize() + '&block=' + block,
            function(html){
                parent.find('.ajax_update').html(html);
            }
        );
    });
}

function changeAddOption(keys)
{
    keys = Array.isArray(keys) ? keys.map(function (value) {return +value;}) : [];
    $('.js-options .check_option').each(function() {
        var $item = $(this),
            id = +$item.data('option-id');

        if (keys.indexOf(id) === -1) {
            $item.find('input[type="text"]').prop('disabled', true);
            $item.find('input[type="checkbox"]').prop('checked', false);
            $item.hide();
        } else {
            $item.show();
        }
    });
}

function toggleCompany(type)
{
    if(type === 'COMPANY' || type === 'COMPANY_CORP_BALANCE') {
        $('.js-row-company').show();
    } else {
        $('.js-row-company').hide();
    }
}

function toggleCity(cityId)
{
    cityId = parseInt(cityId);
    $('.js-item-company').each(function($idx) {
        var currentCityId = $(this).data('city-id');

        if(currentCityId === cityId) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
}