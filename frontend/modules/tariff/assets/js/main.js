$(function () {

    //Манипуляции с выбором типа расчета
    $(document).on('change', '.tariff_change select.default_select', function () {
        var chacnge_type = $(this).hasClass('city') ? 'city' : 'track';
        var accrual = $(this).val();
        var active_tab = $(this).parents('div.active');
        var rows = active_tab.find('div.ap_cont').children('div:not(.disable_none)');
        var fix_tariff_block = active_tab.find('.fix_tariff');
        var fix_select_cnt = active_tab.find('.ap_top select option:selected').filter('[value="FIX"]').size();
        var mixed_div = active_tab.find('div.next_km_price_time, div.planting_include_time');

        mixed_div.find('.' + chacnge_type).hide();

        if (active_tab.find('.tariff_change select.default_select :selected').filter('[value="MIXED"]').size() == 0) {
            mixed_div.hide();
        }

        if (accrual == 'FIX') {
            var parent_next_cost_unit = active_tab.find('.next_km_price .' + chacnge_type);
            parent_next_cost_unit.find('span.default_unit').show();
            parent_next_cost_unit.find('span.select').hide();

            //Отключение ненужных опций
            rows.each(function (e) {
                var span = $(this).find('span.' + chacnge_type);
                span.addClass('locked').prev('input').attr('disabled', 'disabled');

                if (fix_select_cnt == 2)
                    span.prevAll('label').addClass('locked');
            });

            //Показ фиксированных тарифов
            fix_tariff_block.show();
            active_tab.find('.fix_' + chacnge_type).show();

            var city_id = $('#taxitariff-city_list option:selected').val();
            var parking_type = chacnge_type == 'track' ? 'outCity' : chacnge_type;

            $.get(
                fix_tariff_block.data('url'),
                {
                    city_id: city_id,
                    parking_type: parking_type,
                    tariff_type: active_tab.data('type')
                },
                function (html) {
                    $('div.active .ajax_' + chacnge_type).html(html);
                }
            );
        }
        else {
            var calculation_fix = $(this).parents('form').first().find('.js-calculation_fix').prop('checked');

            //Включение опций после отключения
            rows.each(function () {
                var span = $(this).find('span.' + chacnge_type);
                var input = span.prev('input');
                var autoDownTime = $('#taxitariff-auto_downtime :checked').val();
                if ((!input.hasClass('js-speed_downtime_on_auto') || autoDownTime == 1) && span.hasClass('locked')) {
                    span.removeClass('locked');
                    input.removeAttr('disabled');

                    if (fix_select_cnt < 2) {
                        span.prevAll('label').removeClass('locked');
                    }
                }

                if(input.hasClass('js-second-min-price')) {
                    if(!calculation_fix) {
                        span.addClass('locked').prev('input').attr('disabled', 'disabled');
                    } else {
                        span.removeClass('locked');
                        input.removeAttr('disabled');
                    }

                }


            });

            //Прячем фиксированные тарифы
            active_tab.find('.fix_' + chacnge_type).hide();

            if (active_tab.find('.fix_city').css('display') == 'none' && active_tab.find('.fix_track').css('display') == 'none')
                fix_tariff_block.hide();
            //---------------------------------------------------------------------------

            //Стоимость далее
            var parent_next_cost_unit = active_tab.find('.next_km_price .' + chacnge_type);
            var default_unit = parent_next_cost_unit.find('span.default_unit');
            var select_unit = parent_next_cost_unit.find('span.select');
            //----------------------------------------------------------------------------

            //Включено в стоимость посадки
            var planting_include_unit = active_tab.find('.planting_include .' + chacnge_type);
            var planting_include_time_unit = planting_include_unit.find('span.time_unit');
            var planting_include_km_unit = planting_include_unit.find('span.km_unit');
            //----------------------------------------------------------------------------

            if (accrual == 'TIME') {
                //Стоимость далее
                default_unit.hide();
                select_unit.show();
                //Включено в стоимость посадки
                planting_include_km_unit.hide();
                planting_include_time_unit.show();
            } else {
                //Стоимость далее
                default_unit.show();
                select_unit.hide();

                //Включено в стоимость посадки
                planting_include_km_unit.show();
                planting_include_time_unit.hide();

                if (accrual == 'MIXED') {
                    mixed_div.show().find('.' + chacnge_type).show();
                }
            }

            if (accrual == 'INTERVAL') {
                parent_next_cost_unit.find('.not_interval_value').hide();
                parent_next_cost_unit.find('.interval').show();
                if (chacnge_type == 'city') {
                    $('div.next_km_price label').addClass('label_shift');
                }
            } else {
                parent_next_cost_unit.find('.not_interval_value').show();
                parent_next_cost_unit.find('.interval').hide();
                if (chacnge_type == 'city') {
                    $('div.next_km_price label').removeClass('label_shift');
                }
            }
        }
    });

    /**
     * Удаление блока интервала.
     * Имена переменных выбраны по классам селекторов.
     */
    $(document).on('click', '.tdad_item a.close_red', function () {
        var tdad_item = $(this).parent();
        var tdad_last = tdad_item.next('.tdad_last');
        var last_tdad_number = tdad_last.find('.tdad_number');
        var last_number = last_tdad_number.text();

        //Меняем порядковый номер последнего блока интервала
        last_tdad_number.text(last_number - 1);

        //Изменяем атрибуты name у input последнего блока интервалов
        var interval_index = last_number - 1;
        var new_index = interval_index - 1;
        interval_index = interval_index.toString();

        var inerval_inputs = tdad_last.find('input');
        inerval_inputs.each(function () {
            var name = $(this).attr('name');
            if (name !== undefined) {
                var new_input_name = name.replace('[' + interval_index + ']', '[' + new_index + ']');
                $(this).attr('name', new_input_name);
            }
        });

        //Копирование крестика в предыдущий блок интервала
        var all_prev_tdad_item = tdad_item.prevAll('.tdad_item');
        if (all_prev_tdad_item.size() > 1) {
            $(this).clone().appendTo(all_prev_tdad_item.eq(0));
        }

        //Меняем значения инпутов последнего блока интервалов
        var second_prev_interval_number = all_prev_tdad_item.eq(0).find('.second_interval_number');
        second_prev_interval_number.each(function () {
            var times_of_day = $(this).parent().attr('class');
            tdad_last.find('.' + times_of_day + ' .first_interval_number').val($(this).val());
        });

        tdad_item.remove();
    });

    /**
     * Добавление блока интервала.
     */
    $(document).on('click', '.interval a.plus_icon', function (e) {
        e.preventDefault();

        var interval = $(this).parent();
        var tdad_last = interval.find('.tdad_last');

        //Копирование блока интервала
        var prev_tdad_item = tdad_last.prev('.tdad_item');
        var clone_content = prev_tdad_item.clone();
        clone_content.insertBefore(tdad_last);

        //Работа с крестиком удаления блока интервалов
        var close_red = prev_tdad_item.find('a.close_red');
        if (close_red.size() > 0) {
            close_red.remove();
        } else {
            clone_content.append('<a class="close_red"></a>');
        }

        //Изменение данных у склонированного блока

        //Значения инпутов
        var second_interval_number = clone_content.find('.second_interval_number');
        second_interval_number.each(function () {
            $(this).parent().find('.first_interval_number').val($(this).val());
            $(this).val(0);
        });
        //Меняем значение у последнего блока
        tdad_last.find('.first_interval_number').val(0);

        var clone_tdad_item_number = clone_content.find('.tdad_number');
        //Порядковый номер склонированного блока
        var clone_number = parseInt(clone_tdad_item_number.text());
        //Увеличиваем порядковый номер
        clone_tdad_item_number.text(clone_number + 1);
        //Изменяем атрибуты name у инпутов
        var interval_index = clone_number - 1;
        interval_index = interval_index.toString();
        inerval_inputs = clone_content.find('input');
        inerval_inputs.each(function () {
            var name = $(this).attr('name');
            if (name !== undefined) {
                var new_input_name = name.replace('[' + interval_index + ']', '[' + clone_number + ']');
                $(this).attr('name', new_input_name);
            }
        });

        //--------------------------------------------------------------

        //Увеличиваем порядковый номер последнего интервала
        var tdad_last_number = tdad_last.find('.tdad_number');
        var last_number = parseInt(tdad_last_number.text());
        tdad_last_number.text(last_number + 1);

        //Изменяем атрибуты name у инпутов последнего блока
        interval_index = last_number - 1;
        interval_index = interval_index.toString();
        var inerval_inputs = tdad_last.find('input');
        inerval_inputs.each(function () {
            var name = $(this).attr('name');
            if (name !== undefined) {
                var new_input_name = name.replace('[' + interval_index + ']', '[' + last_number + ']');
                $(this).attr('name', new_input_name);
            }
        });
    });

    //Автозаполнение интервалов
    $(document).on('keyup', '.tdad_item .second_interval_number', function () {
        var times_of_day = $(this).parent().attr('class');
        $(this).parents('.tdad_item').next('.tdad_item').find('.' + times_of_day + ' .first_interval_number').val($(this).val());
    });

    //Первое значение интервала
    $(document).on('keyup', '.planting_include input', function () {
        var parent = $(this).parent();
        var times_of_day = parent.attr('class');
        var accrual = parent.parent().attr('class');
        var val = $(this).val();
        val = !val ? 0 : val;
        $(this).parents('.planting_include').nextAll('.next_km_price')
            .find('.' + accrual + ' .interval .tdad_item:eq(0)' + ' .' + times_of_day + ' .first_interval_number').val(val);
    });
    //------------------------------------------------------------------

    //Фильтр тарифов на index.php
    $('.plan_cities li a').on('click', function (e) {

        e.preventDefault();
        $(this).parent().siblings().removeClass('active');
        $(this).parent().addClass('active');

        var table = $('table.plans');

        if ($(this).data('city') == '0') {
            table.find('tr').show();
            return false;
        }

        table.find('tr').hide();

        var city = $(this).text();
        var rows = table.find('td:odd');

        rows.each(function () {
            if ($(this).text().search(city) != -1 || $(this).text().search('Все города') != -1)
                $(this).parents('tr').show();
        });
    });
    //------------------------------------------------------------------

    //Фикс Аэропорт и ЖД
    $('.check_spoler input').on('click', function () {
        var area = $(this).data('option');
        var active_tab = $(this).parents('div.active');

        if ($(this).prop("checked")) {
            $.get(
                $('div.active .fix_tariff').data('url'),
                {
                    city_id: $('#taxitariff-city_list option:selected').val(),
                    parking_type: area,
                    tariff_type: active_tab.data('type')
                },
                function (html) {
                    active_tab.find('.ajax_' + area).html(html)
                }
            );
        }
        else {
            active_tab.find('.ajax_' + area + ' input').attr('disabled', 'disabled');
        }
    });

    //Валидация формы
    $('#tariff_current-form').on('change', 'input[type="text"]:not(.day_night, .not_valid)', function (e) {
        var input_val = $(this).val();
        var planting_include = $(this).parents('.planting_include');

        if (isNaN(input_val)) {
            $(this).addClass('input_error');
        } else if (planting_include.size() > 0) {
            var parent = $(this).parent();
            var times_of_day = parent.attr('class');
            var accrual = parent.parent().attr('class');
            var parent_of_interval_next_block = $(this).parents('.planting_include').nextAll('.next_km_price')
                .find('.' + accrual + ' .interval .tdad_item:eq(0)' + ' .' + times_of_day);
            var second_interval_number_of_next_block = parent_of_interval_next_block.find(' .second_interval_number');
            if (+second_interval_number_of_next_block.val() > 0) {
                var first_interval_value_of_next_block = parent_of_interval_next_block.find('.first_interval_number:eq(0)');
                if (+second_interval_number_of_next_block.val() <= +first_interval_value_of_next_block.val()) {
                    second_interval_number_of_next_block.addClass('input_error');
                }
            }
        } else if ($(this).hasClass('second_interval_number')) {
            var first_interval_value = $(this).prevAll('.first_interval_number:eq(0)').val();
            if (+$(this).val() <= +first_interval_value) {
                $(this).addClass('input_error');
            } else {
                $(this).removeClass('input_error');
                //Проверяем следующий блок интервалов, т.к. его первое число изменилось, а во втором числе могут быть введены данные
                var times_of_day = $(this).parent().attr('class');
                var parent_of_interval_next_block = $(this).parents('.tdad_item').next('.tdad_item').find('.' + times_of_day);
                var second_interval_number_of_next_block = parent_of_interval_next_block.find(' .second_interval_number');
                if (+second_interval_number_of_next_block.val() > 0) {
                    var first_interval_value_of_next_block = parent_of_interval_next_block.find('.first_interval_number:eq(0)');
                    if (+second_interval_number_of_next_block.val() <= +first_interval_value_of_next_block.val()) {
                        second_interval_number_of_next_block.addClass('input_error');
                    }
                }
            }
        } else {
            $(this).removeClass('input_error');

            //Дублирование значений фиксированных тарифов
            // var cur_class = $(this).attr('class');
            // if (cur_class !== undefined && cur_class.search('fix_') != -1) {
            //     $('#tariff_current-form .' + cur_class).val(input_val);
            // }
        }

    });

    $('#tariff_current-form').on('change', 'input.day_night', function (e) {
        var reg = /^([0-1][0-9]|[2][0-3]):[0-5]{1}[0-9]{1}$/i;
        var input_val = $(this).val();
        $(this).removeClass('input_error');

        if (!reg.test(input_val)) {
            $(this).addClass('input_error');
            return false;
        }

        var input_index = $(this).index();

        if (input_index == 0) {
            $(this).parents('.add_plan').find('input.start_day').val(input_val);
        }
        else if (input_index == 1) {
            $(this).parents('.add_plan').find('input.end_day').val(input_val);
        }
    });

    $('#tariff_current-form').submit(function () {
        $(this).attr('action', location.href);

        var errors = $(this).find('input.input_error');
        if (errors.size() > 0)
            return false;
    });

    //----------------------------------------------------------------

    $('input.switch').on('change', function () {
        if ($(this).prop('checked'))
            $('#' + $(this).data('section')).show();
        else
            $('#' + $(this).data('section')).hide();
    });

    //Включение/выключение учета времени суток
    $(document).on('click', 'div.day_night_block input[type="checkbox"]', function () {
        day_night_reload($(this));
    });

    var day_night_checkbox = $('div.day_night_block input[type="checkbox"]');
    day_night_checkbox.each(function () {
        day_night_reload($(this));
    });
    //------------------------------------------------------------------------------------------
    //Замена валюты при смене филиала
    //Замена доп. опций при смене филиала
    function getCurrencySymbolByUnit(unit, new_currency_symbol) {
        var symbol_parts = unit.split('/');
        return unit.replace(symbol_parts[0], new_currency_symbol);
    }

    $('#taxitariff-city_list').on('change', function () {
        var cityId = $(this).val();
        $.get(
            '/city/city/get-city-currency-symbol',
            {city_id: cityId},
            function (json) {
                toggleCity(cityId);
                $('.currency_symbol').each(function () {
                    //Внутри могут быть селекты
                    var select = $(this).find('select');
                    if (select.size() > 0) {
                        var new_symbol = getCurrencySymbolByUnit(select.attr('data-placeholder'), json);
                        select.attr('data-placeholder', new_symbol);

                        select.find('option').each(function () {
                            var new_symbol = getCurrencySymbolByUnit($(this).text(), json);
                            $(this).text(new_symbol);
                        });

                        select.trigger('update');
                    } else {
                        var symbol = $(this).text();
                        var new_symbol = symbol.indexOf('/') !== -1 ? getCurrencySymbolByUnit(symbol, json) : json;
                        $(this).text(new_symbol);
                    }
                });
            },
            'json'
        );

        $.get(
            'add-options',
            {city_id: cityId},
            function (json) {
                changeAddOption(json);
            },
            'json'
        );

        $.get(
            '/employee/position/get-positions',
            {city_id: cityId},
            function (json) {
                if ($.isArray(json)) {
                    var select = $('#taxitariff-position_id');
                    select.find('option:not(":first")').remove();
                    var options = $();

                    for (var i = 0; i < json.length; i++) {
                        options = options.add($('<option/>').val(json[i]['position_id']).text(json[i]['name']));
                    }

                    select.append(options);

                    select.trigger('update');
                }
            },
            'json'
        );
    });


// Включение/выключение дополнительного расчета минимальной стоимости
    $('.js-calculation_fix').on('change', function () {
        var calculationFix = $(this).prop('checked');
        $('.js-second-min-price').each(function () {
            if (!calculationFix) {
                $(this).attr('disabled', true).next('span').addClass('locked');
            } else {
                var type = $(this).parent('div').parent('div').hasClass('city') ? 'city' : 'track';
                var tariff = $(this).parents('.ap_cont').first().prev('div').find('.js-tariff-type.' + type).val();
                if (tariff !== 'FIX') {
                    $(this).removeAttr('disabled').next('span').removeClass('locked');
                }
            }
        });
    });

    //------------------------------------------------------------------------------------------
    // Включение/выключение авто ожидания по GPS
    $('#taxitariff-auto_downtime').on('change', function () {
        var autoDownTime = $('#taxitariff-auto_downtime :checked').val();
        $('.js-speed_downtime_on_auto').each(function () {
            if (autoDownTime == 0) {
                $(this).attr('disabled', true).next('span').addClass('locked');
            } else {
                var type = $(this).parent('div').parent('div').hasClass('city') ? 'city' : 'track';
                var tariff = $(this).parents('.ap_cont').first().prev('div').find('.js-tariff-type.' + type).val();
                if (tariff != 'FIX') {
                    $(this).removeAttr('disabled').next('span').removeClass('locked');
                }
            }
        });
    });

    $('.js-type').on('change', function () {
        var type = $(this).val();

        toggleCompany(type);
    });


    toggleCompany($('.js-type').val());
    toggleCity($('#taxitariff-city_list').val());
});