<?php

use yii\helpers\Html;
use frontend\modules\exchange\assets\ExchangeAsset;

/* @var $this yii\web\View */
/* @var $model frontend\modules\exchange\models\entities\ExchangeConnection */
/* @var $dataForm array  */

ExchangeAsset::register($this);


$this->title = Yii::t('exchange', 'Add a connection');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="exchange-connection-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'dataForm' => $dataForm
    ]) ?>

</div>
