<?php

/* @var $this \yii\web\View */
/* @var $dataForm array */
/* @var $pjaxId string */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\exchange\models\search\ExchangeConnectionSearch */

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\modules\exchange\models\entities\ExchangeConnection;
use frontend\widgets\filter\Filter;
use yii\widgets\Pjax;
use frontend\widgets\LinkPager;

?>

<? $filter = Filter::begin([
    'pjaxId' => $pjaxId,
    'model'  => $searchModel,
]); ?>

<?= $filter->checkboxList('city_id', $dataForm['cities']); ?>
<?= $filter->checkboxList('exchange_program_id', $dataForm['exchangeProgram']); ?>

<? Filter::end(); ?>


<? Pjax::begin(['id' => $pjaxId]) ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'       => "{items}\n{pager}",
    'tableOptions' => [
        'class' => 'people_table',
    ],
    'pager'        => [
        'class'         => LinkPager::className(),
        'prevPageLabel' => t('app', 'Prev'),
        'nextPageLabel' => t('app', 'Next'),
    ],
    'columns'      => [
        [
            'attribute' => 'name',
            'headerOptions' => ['class' => 'pt_fio'],
            'content' => function(ExchangeConnection $model) {
                $text = Html::encode($model->name);
                return Html::a($text,['update', 'id' => $model->exchange_connection_id]);
            },
            'contentOptions' => ['class' => 'pt_fio'],
        ],
        [
            'label' => t('exchange', 'Program'),
            'attribute' => 'exchange_program_id',
            'content' => function(ExchangeConnection $model) {
                return Html::encode($model->exchangeProgram->name);
            },
        ],
        [
            'label' => t('exchange', 'Visibility of orders in the stock exchange'),
            'attribute' => 'is_show_automate',
            'content' => function(ExchangeConnection $model) {
                return $model->is_show_automate ? 'Да' : 'Нет';
            },
        ],
        [
            'attribute' => 'rate'
        ],
        [
            'attribute' => 'city_id',
            'content' => function(ExchangeConnection $model) use ($dataForm) {
                return $dataForm['cities'][$model->city_id];
            },
        ],
        [
            'attribute' => 'sort',
            'headerOptions' => ['class' => 'pt_job'],
            'contentOptions' => ['class' => 'pt_fio'],
        ],
    ],
]);
?>

<? Pjax::end() ?>