<?php

use yii\helpers\Html;
use frontend\modules\exchange\assets\ExchangeAsset;

/* @var $this yii\web\View */
/* @var $model frontend\modules\exchange\models\entities\ExchangeConnection */
/* @var $dataForm array */

$this->title = $model->name;


ExchangeAsset::register($this);


?>
<div class="exchange-connection-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'dataForm' => $dataForm
    ]) ?>

</div>
