<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\exchange\models\entities\ExchangeConnection */
/* @var $form yii\widgets\ActiveForm */
/* @var $dataForm array  */
?>

<div class="exchange-connection-form">

    <?php $form = ActiveForm::begin(); ?>

    <section class="row">
        <?= $form->field($model, 'name')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'name') ?></div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'name') ?>
            <?= Html::error($model, 'name',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'name')->end(); ?>
    </section>

    <section class="row">
        <?= $form->field($model, 'city_id')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'city_id') ?></div>
        <div class="row_input">
            <?= Html::activeDropDownList($model, 'city_id', $dataForm['cities'], ['class' => 'default_select']) ?>
            <?= Html::error($model, 'city_id',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'city_id')->end(); ?>
    </section>


    <section class="row">
        <?= $form->field($model, 'position_id')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'position_id') ?></div>
        <div class="row_input">
            <?= Html::activeDropDownList(
                $model,
                'position_id',
                $dataForm['positions'],
                [
                    'class' => 'default_select',
                    'prompt' => t('exchange', 'Сhoose a profession')
                ]
            ) ?>
            <?= Html::error($model, 'position_id',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'position_id')->end(); ?>
    </section>


    <section class="row">
        <?= $form->field($model, 'exchange_program_id')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'exchange_program_id') ?></div>
        <div class="row_input">
            <?= Html::activeDropDownList(
                $model,
                'exchange_program_id',
                $dataForm['exchangeProgram'],
                [
                    'class' => 'default_select',
                    'prompt' => t('exchange', 'Сhoose a program')
                ]
            ) ?>
            <?= Html::error($model, 'exchange_program_id',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'exchange_program_id')->end(); ?>
    </section>


    <section class="row">
        <?= $form->field($model, 'clid')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'clid') ?></div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'clid') ?>
            <?= Html::error($model, 'clid',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'clid')->end(); ?>
    </section>

    <section class="row">
        <?= $form->field($model, 'key_api')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'key_api') ?></div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'key_api') ?>
            <?= Html::error($model, 'key_api',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'key_api')->end(); ?>
    </section>

    <section class="row">
        <?= $form->field($model, 'rate')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'rate') ?></div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'rate') ?>
            <?= Html::error($model, 'rate',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'rate')->end(); ?>
    </section>

    <section class="row">
        <?= $form->field($model, 'is_show_automate')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'is_show_automate') ?></div>
        <div class="row_input">
            <?= Html::activeDropDownList(
                $model,
                'is_show_automate',
                [
                    0 => t('exchange', 'No'),
                    1 => t('exchange', 'Yes')
                ],
                ['class' => 'default_select']
            ) ?>
            <?= Html::error($model, 'is_show_automate',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'is_show_automate')->end(); ?>
    </section>

    <section class="row">
        <?= $form->field($model, 'sort')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'sort') ?></div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'sort') ?>
            <?= Html::error($model, 'sort',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'sort')->end(); ?>
    </section>

    <section class="submit_form">
        <div>
            <label><?= Html::activeCheckbox($model, 'block', ['label' => null]) ?>
                <b><?= t('exchange', 'Block') ?></b></label>
        </div>
        <?= Html::submitInput(t('exchange', 'Save')) ?>
    </section>

    <?php ActiveForm::end(); ?>

</div>
