<?php

namespace frontend\modules\exchange\models\search;

use frontend\modules\exchange\models\entities\ExchangeProgram;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\exchange\models\entities\ExchangeConnection;

/**
 * ExchangeConnectionSearch represents the model behind the search form about
 * `frontend\modules\exchange\models\entities\ExchangeConnection`.
 */
class ExchangeConnectionSearch extends ExchangeConnection
{

    const PAGE_SIZE = 20;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id'], 'each', 'rule' => ['in', 'range' => array_keys(user()->getUserCityList())]],
            [
                ['exchange_program_id'],
                'each',
                'rule' => [
                    'exist',
                    'skipOnError'     => true,
                    'targetClass'     => ExchangeProgram::className(),
                    'targetAttribute' => ['exchange_program_id'],
                ]
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $isBlock = false)
    {
        $query = ExchangeConnection::find()->alias('ec')
            ->joinWith('exchangeProgram ep', false)
            ->joinWith('city c', false);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'sort'       => [
                'defaultOrder' => [
                    'sort' => SORT_ASC,
                ],
                'attributes'   => [
                    'name',
                    'sort',
                    'rate',
                    'is_show_automate',
                    'exchange_program_id' => [
                        'asc'  => [
                            'ep.name' => SORT_ASC,
                        ],
                        'desc' => [
                            'ep.name' => SORT_DESC,
                        ],
                    ],
                    'city_id'             => [
                        'asc'  => [
                            'c.name' . getLanguagePrefix() => SORT_ASC,
                        ],
                        'desc' => [
                            'c.name' . getLanguagePrefix() => SORT_DESC,
                        ],
                    ],
                ],
            ],
            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);

        $this->load($params);
        // grid filtering conditions
        $query->andFilterWhere([
            'ec.tenant_id'           => user()->tenant,
            'ec.city_id'             => $this->city_id,
            'ec.exchange_program_id' => $this->exchange_program_id,
            'ec.block'               => $this->block
        ]);

        return $dataProvider;
    }


    public function attributeLabels()
    {
        $labels = parent::attributeLabels();

        $labels['city_id'] = t('exchange', 'All cities');

        return $labels;
    }

}
