<?php

namespace frontend\modules\exchange\models\entities;

use common\modules\city\models\City;
use common\modules\employee\models\position\Position;
use Yii;

/**
 * This is the model class for table "{{%exchange_connection}}".
 *
 * @property integer $exchange_connection_id
 * @property integer $exchange_program_id
 * @property integer $city_id
 * @property integer $position_id
 * @property string $block
 * @property string $clid
 * @property string $key_api
 * @property integer $rate
 * @property string $is_show_automate
 * @property integer $sort
 * @property string $name
 * @property integer $tenant_id
 *
 * @property City $city
 * @property ExchangeProgram $exchangeProgram
 * @property Position $position
 */
class ExchangeConnection extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%exchange_connection}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['exchange_program_id', 'city_id', 'position_id', 'rate', 'clid', 'key_api', 'name'], 'required'],
            [['exchange_program_id', 'city_id', 'position_id', 'rate', 'sort'], 'integer'],
            [['block', 'is_show_automate'], 'in', 'range' => ['0', '1']],
            [['clid', 'key_api', 'name'], 'string', 'max' => 255],
            [
                ['name','tenant_id'],
                'unique',
                'targetClass'     => ExchangeConnection::className(),
                'targetAttribute' => ['name', 'tenant_id'],
                'filter'          => ['not', ['exchange_connection_id' => $this->exchange_connection_id]],
                'message'         => t('exchange', 'Exchange connection with this name already exists')
            ],
            [
                ['city_id', 'position_id', 'exchange_program_id','tenant_id'],
                'unique',
                'targetClass'     => ExchangeConnection::className(),
                'targetAttribute' => ['city_id', 'position_id', 'exchange_program_id', 'tenant_id'],
                'filter'          => ['not', ['exchange_connection_id' => $this->exchange_connection_id]],
                'message'         => t('exchange', 'Exchange connection with this settings already exists')
            ],
            [
                ['city_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => City::className(),
                'targetAttribute' => ['city_id' => 'city_id'],
            ],
            [
                ['exchange_program_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => ExchangeProgram::className(),
                'targetAttribute' => ['exchange_program_id' => 'exchange_program_id'],
            ],
            [
                ['position_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Position::className(),
                'targetAttribute' => ['position_id' => 'position_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'exchange_connection_id' => Yii::t('exchange', 'Exchange Connection'),
            'position_id'            => Yii::t('exchange', 'Position'),
            'exchange_program_id'    => Yii::t('exchange', 'Program'),
            'city_id'                => Yii::t('exchange', 'City'),
            'clid'                   => Yii::t('exchange', 'Caller ID'),
            'key_api'                => Yii::t('exchange', 'Key API'),
            'rate'                   => Yii::t('exchange', 'Rate'),
            'is_show_automate'       => Yii::t('exchange', 'Automatic visibility of orders in the exchange'),
            'sort'                   => Yii::t('exchange', 'Sort'),
            'name'                   => Yii::t('exchange', 'Name'),
        ];
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->tenant_id = user()->tenant_id;
        }

        return parent::beforeValidate();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExchangeProgram()
    {
        return $this->hasOne(ExchangeProgram::className(), ['exchange_program_id' => 'exchange_program_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }
}
