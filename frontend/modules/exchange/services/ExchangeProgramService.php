<?php

namespace frontend\modules\exchange\services;

use frontend\modules\exchange\models\entities\ExchangeProgram;

class ExchangeProgramService
{
    /**
     * Get exchange program name by id
     * @param $exchangeProgramId
     * @return string
     */
    public static function getExchangeProgramNameById($exchangeProgramId)
    {
        $program = ExchangeProgram::find()
            ->where(['exchange_program_id' => $exchangeProgramId])
            ->one();
        if ($program) {
            return $program->name;
        }
        return '';
    }
}