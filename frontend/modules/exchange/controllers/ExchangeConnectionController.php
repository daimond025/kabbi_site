<?php

namespace frontend\modules\exchange\controllers;


use frontend\modules\employee\components\position\PositionService;
use frontend\modules\exchange\components\datacentr\DataForForm;
use frontend\modules\exchange\models\entities\ExchangeConnection;
use frontend\modules\exchange\models\search\ExchangeConnectionSearch;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ExchangeConnectionController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'roles'   => ['read_exchange'],
                        'actions' => ['list', 'view', 'list-blocked'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['exchange'],
                    ],
                ],
            ],
        ];
    }


    public function actionList()
    {
        $searchModel = new ExchangeConnectionSearch(['block' => '0']);

        return $this->render('index', [
            'dataProvider' => $searchModel->search(Yii::$app->request->get()),
            'dataForm'     => DataForForm::getDataForm(),
            'searchModel'  => $searchModel,
            'pjaxId'       => 'exchange-connection-active',
        ]);

    }


    public function actionListBlocked()
    {
        $searchModel = new ExchangeConnectionSearch(['block' => '1']);

        return $this->renderAjax('_grid', [
            'dataProvider' => $searchModel->search(Yii::$app->request->get()),
            'dataForm'     => DataForForm::getDataForm(),
            'searchModel'  => $searchModel,
            'pjaxId'       => 'exchange-connection-block',
        ]);

    }

    public function actionCreate()
    {
        $model = new ExchangeConnection();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            session()->setFlash('success', t('exchange', 'Connection was successfully added.'));

            return $this->redirect(Url::to('/exchange/exchange-connection/list'));
        }

        return $this->render('create', [
            'model'    => $model,
            'dataForm' => DataForForm::getDataForm(),
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            session()->setFlash('success', t('exchange', 'Connection was successfully updated.'));

            return $this->redirect(Url::to('/exchange/exchange-connection/update/' . $id));
        }

        return $this->render('update', [
            'model'    => $model,
            'dataForm' => DataForForm::getDataForm(),
        ]);
    }


    public function actionGetPositions($city_id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $positionService = \Yii::createObject(PositionService::class, [user()->tenant_id]);

        return $positionService->getPositionsByCity($city_id);
    }


    protected function findModel($id)
    {
        if (($model = ExchangeConnection::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}