<?php

namespace frontend\modules\exchange\components\datacentr;


use frontend\modules\employee\components\position\PositionService;
use frontend\modules\exchange\models\entities\ExchangeProgram;
use yii\helpers\ArrayHelper;

class DataForForm
{

    public static function getDataForm()
    {
        $positionService = \Yii::createObject(PositionService::class, [user()->tenant_id]);


        $cities          = user()->getUserCityList();
        $positions       = $positionService->getPositionMapByCity(key($cities));
        $exchangeProgram = ArrayHelper::map(
            ExchangeProgram::find()->where(['block' => '0'])->all(),
            'exchange_program_id',
            'name'
        );

        return compact('cities', 'positions', 'exchangeProgram');
    }

}