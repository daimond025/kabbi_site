$(function () {

    $('body').on('change', '#exchangeconnection-city_id', function () {

        $.get(
            '/exchange/exchange-connection/get-positions',
            {city_id: this.value},
            function (json) {
                if ($.isArray(json)) {
                    console.log(json);
                    var select = $('#exchangeconnection-position_id');
                    select.find('option:not(":first")').remove();
                    var options = $();

                    for (var i = 0; i < json.length; i++) {
                        options = options.add($('<option/>').val(json[i]['position_id']).text(json[i]['name']));
                    }

                    select.append(options);

                    select.trigger('update');
                }
            },
            'json'
        );
    });


});