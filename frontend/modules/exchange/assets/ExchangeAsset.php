<?php

namespace frontend\modules\exchange\assets;


use yii\web\AssetBundle;

class ExchangeAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/exchange/assets/source';

    public $publishOptions = [
        'forceCopy' => true
    ];

    public $css = [

    ];
    public $js = [
        'js/app.js'
    ];
    public $depends = [
        
    ];
}