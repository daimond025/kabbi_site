<?php

namespace frontend\modules\tenant\repositories;

use frontend\modules\tenant\models\email\TenantEmailSetting;
use frontend\modules\tenant\services\dto\EmailProviderData;
use frontend\modules\tenant\services\dto\EmailSenderData;

class TenantEmailSettingRepository
{

    public function findOrCreate($tenantId, $cityId)
    {
        try {
            return $this->find($tenantId, $cityId);

        } catch (\RuntimeException $exception) {
            $emailSetting            = new TenantEmailSetting();
            $emailSetting->tenant_id = $tenantId;
            $emailSetting->city_id   = $cityId;
            $emailSetting->deactivate();

            return $emailSetting;
        }
    }

    /**
     * @param integer $tenantId
     * @param integer $cityId
     *
     * @return TenantEmailSetting
     */
    public function find($tenantId, $cityId)
    {
        if (!$emailSetting = TenantEmailSetting::findOne([
            'tenant_id' => $tenantId,
            'city_id'   => $cityId,
        ])) {
            throw new \RuntimeException('Find model.');
        }

        return $emailSetting;
    }

    /**
     * @param integer           $tenantId
     * @param integer           $cityId
     * @param integer           $activate
     * @param EmailProviderData $provider
     * @param EmailSenderData   $sender
     * @param string            $template
     */
    public function save($tenantId, $cityId, $activate, EmailProviderData $provider, EmailSenderData $sender, $template)
    {
        $model         = $this->findOrCreate($tenantId, $cityId);
        $model->active = $activate;

        $model->provider_server = $provider->server;
        $model->provider_port   = $provider->port;

        $model->sender_name     = $sender->name;
        $model->sender_email    = $sender->email;
        $model->sender_password = $sender->password;
        $model->template        = $template;

        if (!$model->save(false)) {
            throw new \RuntimeException('Save model.');
        }
    }
}