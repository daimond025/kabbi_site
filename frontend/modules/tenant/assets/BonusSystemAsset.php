<?php

namespace frontend\modules\tenant\assets;

use yii\web\AssetBundle;

class BonusSystemAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/tenant/assets/js/';

    public $publishOptions = [
        'forceCopy' => true,
        'only'      => [
            'bonusSystem.js',
        ],
    ];
    public $js = [
        'bonusSystem.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}