<?php


namespace app\modules\tenant\assets;


use yii\web\AssetBundle;

class PositionAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/tenant/assets/js/';

    public $publishOptions = [
        'forceCopy' => true,
        'only' => [
            'position.js',
            'tenant-company.js',
        ]
    ];

    public $css = [
    ];
    public $js = [
        'position.js',
        'tenant-company.js',
    ];
    public $depends = [
    ];
}