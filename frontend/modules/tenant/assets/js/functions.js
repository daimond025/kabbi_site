function doUserAction(user_id, url) {
    if (user_id) {
        $.post(
            url,
            {user_id: user_id},
            function (json) {
                if (json == 'ok') {
                    $('div.tabs_content div.active').load($('.tabs_links li.active a').data('href'));
                }
            },
            'json'
        );
    }
}