$(function () {
    //Изменения настроек при выборе типа распределения
    $('#distribution_order').on('change', function () {
        var section = $(this).parents('section');
        if ($(this).val() == 2) {
            section.nextAll('section[data-type="parkings"]').show();
            section.nextAll('section[data-type="distance"]').hide();
        } else {
            section.nextAll('section[data-type="parkings"]').hide();
            section.nextAll('section[data-type="distance"]').show();
        }
    });

    $('#offer_order_type').on('change', function () {
        if ($(this).val() === 'batch') {
            $('#offer_order_count').show();
        } else {
            $('#offer_order_count').hide();
        }
    });

    $('.field-settingsworker-needcloseworkershiftbybadpositionupdatetime input').on('change', function () {
        if ($(this).prop('checked')) {
            $('.field-settingsworker-workerpositioncheckdelaytime').show();
        } else {
            $('.field-settingsworker-workerpositioncheckdelaytime').hide();
        }
    });

    $('.field-settingsworker-needcloseworkershiftbybadparkingposition input').on('change', function () {
        if ($(this).prop('checked')) {
            $('.field-settingsworker-workerparkingpositioncheckdelaytime').show();
            $('.field-settingsworker-workerparkingpositionchecktype').show();
        } else {
            $('.field-settingsworker-workerparkingpositioncheckdelaytime').hide();
            $('.field-settingsworker-workerparkingpositionchecktype').hide();
        }
    });

    $('.field-settingsworker-denysettingarrivalstatusearly input').on('change', function () {
        if ($(this).prop('checked')) {
            $('.field-settingsworker-mindistancetosetarrivalstatus').show();
        } else {
            $('.field-settingsworker-mindistancetosetarrivalstatus').hide();
        }
    });

    $('.field-settingsworker-allowreserveurgentorders input').on('change', function () {
        if ($(this).prop('checked')) {
            $('.field-settingsworker-maxcountreserveurgentorders').show();
        } else {
            $('.field-settingsworker-maxcountreserveurgentorders').hide();
        }
    });

    //Валидация формы настроек
    $('#setting_form').on('submit', function () {

        var number_input = $(this).find('.number');
        var error = false;

        $(this).find('.input_error').removeClass('input_error');

        number_input.each(function () {
            if ($(this).is(":visible") && !isNumeric($(this).val())) {
                error = true;
                $(this).addClass('input_error');
            }
        });

        if (error)
            return false;
    });

    $(document).on('change', '#settings_position_id', function (e) {
        var position_id = $(this).val();
        var action = location.origin + location.pathname;
        var urlVars = getUrlVar();

        action = urlVars['city_id'] ? action + '?city_id=' + urlVars['city_id'] + '&' : action + '?';
        action += 'position_id=' + position_id;

        location.href = action;
    });

    //Добавить новый круг распределения заказа
    $('.add-distribution-circle').on('click', function () {
        var lastCircle = $(document).find('.repetitions_circles_settings:visible:last');
        lastCircle.find('.js-del-distribution-circle').hide();
        var cirle = $(document).find('.repetitions_circles_settings:hidden:first');
        if (cirle.length) {
            cirle.show();
            var nextCircle = $(document).find('.repetitions_circles_settings:hidden:first');
            if (!nextCircle.length) {
                $('.add-distribution-circle').hide();
            }
        }
    });

    $('.js-del-distribution-circle').on('click', function () {
        var settings = $(this).parent().parent();
        if (settings && settings.length) {
            settings.find('.input_number_mini').attr('value', '');
            settings.hide();
        }
        var allVisible = $(document).find('.repetitions_circles_settings:visible');
        if (allVisible && allVisible.length > 1) {
            var lastCircle = $(document).find('.repetitions_circles_settings:visible:last');
            lastCircle.find('.js-del-distribution-circle').show();
            if (allVisible.length < 5) {
                $('.add-distribution-circle').show();
            }
        }

    });

    // обработка свободных заказов
    $('input.free_order_on').on('change', function () {

        if($(this).prop('checked') !== true){
            $('input.FREE_ORDER').each(function (index, value) {
               $(value).prop( "disabled", true );
            });

        }else{
            $('input.FREE_ORDER').each(function (index, value) {
               $(value).prop( "disabled", false );
            });
        }
    });
    // первоначальная настройка
    if( $('input.free_order_on').prop('checked') !== true){
        $('input.FREE_ORDER').each(function (index, value) {
            $(value).prop( "disabled", true );
        });
    }

    // время срабатывания при предложении нового заказа водителю
    $('input.js-min-order-time-assign').on('change', function () {
        var ss = parseInt( $('input[name ="orders[NEW][ORDER_OFFER_SEC]"]').val());
        if ($(this).val() >= ss ){
            $(this).val(ss-1);
        }
    });





});