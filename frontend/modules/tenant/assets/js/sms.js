$(function(){
    var template_id = null;
    $(document).on('click', '.ot_edit', function(e){
        e.preventDefault();
        template_id = $(this).data('template');
        
        $.colorbox({
            href: $(this).attr('href'),
            overlayClose: false,
            width: "820px"
        });
    });
    
    $(document).on('click', '#edit_template input[type="submit"]', function(e){
        e.preventDefault();
        var form = $(this).parents('form');
        var text = form.find('textarea').text();
        
        $.post(
            form.attr('action'),
            form.serialize(),
            function(html){
                $('#edit_template .message').html(html);
                var form_data = form.serializeArray();
                $('#template_' + template_id).text(form_data[1].value);
            }
        );
    });
    });
    
function counter(){
    $('#smstemplate-text').keyup(function () { 
        $('#counter').html($('#smstemplate-text').val().length); 
    });
    
}
function pushCounter(){
    $('#clientpushnotifications-text').keyup(function () {
        $('#counter').html($('#clientpushnotifications-text').val().length);
    });
}

$(document).on('change', '#event_position_id', function(e){
    var position_id = $(this).val();
    var action = location.origin + location.pathname;
    var urlVars = getUrlVar();

    action = urlVars['city_id'] ? action + '?city_id=' + urlVars['city_id'] + '&' : action + '?';
    action += 'position_id=' + position_id;

    $.pjax.reload({
        url: action,
        container: "#client_event_templates"
    });
});

$(document).on('pjax:end', function() {
    select_init();
});