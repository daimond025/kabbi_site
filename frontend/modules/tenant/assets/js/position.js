;(function () {
    'use strict';

    var app = {
        positionSelector: '#user_position_list',
        init: function () {
            select_init();
            this.loadDefaultRightsEvent();
            this.changeUserPosition();
        },
        loadDefaultRightsEvent: function () {
            var _this = this;
            $(document).on('click', '.load_defaults_for_job', function (e) {
                e.preventDefault();
                _this.getDefaultRules();
            });
        },
        changeUserPosition: function () {
            var _this = this;
            $(document).on('change', this.positionSelector, function () {
                _this.getDefaultRules();
            });
        },
        getDefaultRules: function () {
            $.get(
                '/tenant/user/get-default-rights',
                {position_id: $(this.positionSelector).val()},
                function (json) {
                    if (json != '') {
                        for (var i = 0; i < json.length; i++) {
                            var optionSelected = document
                                .querySelector(
                                    'select[data-type="' + json[i].permission + '"]>option[selected="selected"]'
                                );

                            if (optionSelected !== null) {
                                optionSelected.removeAttribute('selected');
                            }

                            var option = document
                                .querySelector(
                                    'select[data-type="' + json[i].permission + '"]>option[value="' + json[i].rights + '"]'
                                );

                            if (option !== null) {
                                option.setAttribute('selected', 'selected');
                            }

                            $('select[data-type="' + json[i].permission + '"]').trigger('update');
                        }
                    }
                },
                'json'
            );
        }
    };

    app.init();
})();