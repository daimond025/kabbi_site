$(function () {
    var current_server_id = $('#tenanthassms-server_id').val();
    var button = $('#balance').html();
    $(document).on('change', function () {
        if ($('#tenanthassms-server_id').val() == current_server_id)
        {
            $('#balance').html(button);
            $('#balance').parent('section').show();
        }
        else
        {
            $('#balance').parent('section').hide();
        }
    });

    function eyeline() {
        if ($('#tenanthassms-server_id').val() == 4) {
            $('#tenanthassms-password').attr('disabled', 'disabled');
            $('.form-group.field-tenanthassms-password').attr('style', 'display: none');
            $('#tenanthassms-password').val('-');
        }
        else
        {
            $('.form-group.field-tenanthassms-password').attr('style', 'display: block');
            $('#tenanthassms-password').removeAttr('disabled', '');
        }
    }

    $('#tenanthassms-server_id').change(function () {
        eyeline();
    });

    $('#tenanthassms-server_id').ready(function () {
        eyeline();
    });

    $('#balance').on('click', function () {
        $.get(
            "/sms/sms/get-balance",
            {
                serverId: $('#tenanthassms-server_id').val(),
                login: $('#tenanthassms-login').val(),
                password: $('#tenanthassms-password').val()
            }
        ).done(function (json) {
            $('#balance_label').html(json);
            $('#balance_label').trigger('update');
        });
    });

});