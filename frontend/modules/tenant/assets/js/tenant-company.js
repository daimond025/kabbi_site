;(function () {
    'use strict';

    var app = {
        init: function () {
            this.eventRegistration();
            this.setDefaultRules();
        },

        eventRegistration: function () {
            var _this = this;

            $('#user_position_list').on('change', function () {
                _this.hideFieldCompany();
                if ($(this).find('[selected]').val() == 13) {
                    _this.showFieldCompany();
                }
            });
        },

        showFieldCompany: function () {
            $('.field-user-tenant_company_id').show();
        },

        hideFieldCompany: function () {
            $('.field-user-tenant_company_id').hide();
        },

        setDefaultRules: function () {
            if (location.href.indexOf("create") != -1) {
                $('.load_defaults_for_job').trigger('click');
            }
        }

    };

    app.init();
})();