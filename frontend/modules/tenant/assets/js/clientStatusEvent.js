;(function () {
    'use strict';

    var app = {
        'pjaxContainerSelector': '#client_status_event',
        'currentCityIdSelector': '.js-city-id',
        'currentPositionIdSelector': '#client_status_event_position_id',
        'formSelector': '#client-status-event-form',

        init: function () {
            createCheckboxStringOnReady();
            this.registerChangePosition();
            this.registerCheckListEvent();
        },

        registerChangePosition: function () {
            this.onChangePosition();
        },

        registerCheckListEvent: function () {
            this.onChangeListEvent();
        },

        onChangePosition: function () {
            var _this = this;
            $(this.currentPositionIdSelector).off('change').on('change', function () {

                var cityId = $(_this.currentCityIdSelector).val();
                var positionId = $(this).val();
                $.pjax.reload({
                    container: _this.pjaxContainerSelector,
                    url: window.location.pathname + '?city_id=' + cityId + '&position_id=' + positionId,
                });
            });

            $(document).on('pjax:end', function () {
                createCheckboxStringOnReady();
                _this.onChangeListEvent();
            })
        },

        onChangeListEvent: function () {
            var _this = this;
            $(_this.formSelector).find('.a_sc').off('click').on('click', function (event) {
                showStyleFilter($(this), event);
                createCheckboxStringOnClick();
            });
        }
    };

    app.init();
})();