/* global jQuery */
(function ($) {
    'use strict';
    var app = {
        $profile: null,
        init: function () {
            this.$profile = $('.js-profile');
            this.registerEvents();
            this.registerChangeFilterTypeEvents();
            this.registerChooseFilterDateEvents();
        },
        registerEvents: function () {
            var _this = this;

            _this.$profile.on('change', '#profile-typeid', function () {
                var $this = $(this),
                    formData = $this.closest('form').serialize();

                $.post('/tenant/bank-card/change-profile-type', formData)
                    .done(function (data) {
                        _this.$profile.html(data);
                        $('#profile-typeid').trigger('update');
                        $('#profile-typeid').closest('form').find('.default_select').trigger('update');
                    })
                    .fail(function (data) {
                        _this.$profile.html(null);
                        console.log('fail', data);
                    });
            });

            _this.$profile.on('click', '#profile-securedeal', function () {
                var $secureDealBlock = $('.js-secure-deal');
                var $yandexFailedPaymentsLink = $('.js-yandex-failed-payments');

                if ($(this).prop('checked')) {
                    $secureDealBlock.show();
                    $yandexFailedPaymentsLink.show();
                } else {
                    $secureDealBlock.hide();
                    $yandexFailedPaymentsLink.hide();
                }
            });

            _this.$profile.on('click', '#profile-usereceipt', function () {
                var $receiptBlock = $('.js-receipt');

                if ($(this).prop('checked')) {
                    $receiptBlock.show();
                } else {
                    $receiptBlock.hide();
                }
            });
        },
        registerChangeFilterTypeEvents: function () {
            var _this = this;
            $(document).on('click', 'form[name="yandex-failed-payments-search"] [type="radio"]', function () {
                _this.getYandexFailedPayments($(this).closest('form'));
            });
        },
        registerChooseFilterDateEvents: function () {
            var _this = this;

            $('.sdp').each(function () {
                var $obj = $(this);
                $obj.datepicker({
                    altField: $obj.prev('input'),
                    onSelect: function (date) {
                        var $input = $(this);
                        $input.parent().prev('.a_sc').html(date).removeClass('sel_active');
                        $input.parent().hide();
                        _this.getYandexFailedPayments($obj.closest('form'));
                    }
                });
                if ($obj.hasClass('first_date')) {
                    $obj.datepicker("setDate", '-14');
                }
            });
        },
        getYandexFailedPayments: function (form) {
            $.post(form.attr('action'), form.serialize())
                .done(function (data) {
                    var $grid = form.closest('.yandex-failed-payments').find('.grid');
                    $grid.html(data);
                })
                .fail(function () {

                });
        }
    };
    app.init();
})(jQuery);