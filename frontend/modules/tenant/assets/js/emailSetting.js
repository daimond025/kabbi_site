(function () {
    var emailSetting = {
        selectorActivateField: '.js-email-setting-activate',
        selectorActivatedContainer: '.js-show-is-activated',
        init: function () {
            this.registerChangeActivate();
            this.changeForm();
        },

        registerChangeActivate: function () {
            var _this = this;
            $(document).on('change', this.selectorActivateField, function () {
                _this.changeForm();
            });
        },

        changeForm: function () {
            var value = $(this.selectorActivateField).val();

            if (value === '1') {
                this.activate();
            } else {
                this.deactivate();
            }
        },

        activate: function () {
            $(document).find(this.selectorActivatedContainer).each(function (index) {
                $(this).show().find('input, textarea').removeAttr('disabled');
            })
        },

        deactivate: function () {
            $(document).find(this.selectorActivatedContainer).each(function (index) {
                $(this).hide().find('input, textarea').attr('disabled', true);
            })
        }

    };

    $(document).ready(function () {
        emailSetting.init();
    });
})();