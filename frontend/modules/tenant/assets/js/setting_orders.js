/**
 * show/hide elements on page /setting/orders
 */

/* global jQuery */

(function ($) {
    'use strict';
    var app = {
        $timeAllowWorkerToCancelOrder: null,

        init: function () {
            this.$timeAllowWorkerToCancelOrder = $('#time-allow-worker-to-cancel-order');
            this.registerEvents();
            this.registerSmsFromRecipient();

            $('#allow-worker-to-cancel-order').trigger('change');
            $('#restrict_visibility_of_pre_order').trigger('change');
            $('.js-order-form-add-point-phone').trigger('change');
        },

        registerEvents: function () {
            var _this = this;
            $('#allow-worker-to-cancel-order').change(function () {
                if ($('#allow-worker-to-cancel-order').prop('checked')) {
                    _this.$timeAllowWorkerToCancelOrder.show();
                } else {
                    _this.$timeAllowWorkerToCancelOrder.hide();
                }
            });

            $('#restrict_visibility_of_pre_order').change(function () {
                if ($('#restrict_visibility_of_pre_order').prop('checked')) {
                    $('#time_of_pre_order_visibility').show();
                } else {
                    $('#time_of_pre_order_visibility').hide();
                }
            });

            $('#exchange_enable_sale_orders').change(function () {
                if ($('#exchange_enable_sale_orders').prop('checked')) {
                    $('#exchange_how_wait_worker').show();
                } else {
                    $('#exchange_how_wait_worker').hide();
                }
            });
        },
        registerSmsFromRecipient: function () {
            $('.js-order-form-add-point-phone').change(function () {
                var target = $('.js-order-notify-point-recipient-about-order-executing');
                if ($(this).val() === '1') {
                    target.show().find('input').attr('disabled', false);
                } else {
                    target.hide().find('input').attr('disabled', true);
                }
            });
        }
    };

    app.init();

})(jQuery);