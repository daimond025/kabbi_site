;(function () {
    'use strict';

    var geoService = {

        autoGeoServiceTemplateSelector: '.js-auto-geo-services',
        autoGeoServiceTypeSelector: '.js-auto_service_provider_id',
        autoGeoServiceKey1Selector: '.js-auto-geo-key-1',
        autoGeoServiceKeyLabel1Selector: '.js-auto-geo-key-label-1',
        autoGeoServiceKey2Selector: '.js-auto-geo-key-2',
        autoGeoServiceKeyLabel2Selector: '.js-auto-geo-key-label-2',

        routingServiceTemplateSelector: '.js-routing-services',
        routingServiceTypeSelector: '.js-routing_service_provider_id',
        routingServiceKey1Selector: '.js-routing-key-1',
        routingServiceKeyLabel1Selector: '.js-routing-key-label-1',
        routingServiceKey2Selector: '.js-routing-key-2',
        routingServiceKeyLabel2Selector: '.js-routing-key-label-2',

        init: function () {
            this.startEvent();
            this.registerChangeAutoGeoServiceType();
            this.registerChangeRoutingServiceType();
        },

        startEvent: function () {
            this.changeAutoGeoServiceKeys();
            this.changeRoutingServiceKeys();
        },

        registerChangeAutoGeoServiceType: function () {
            var _this = this;
            $(this.autoGeoServiceTypeSelector).on('change', function() {
                _this.changeAutoGeoServiceKeys();
            });
        },

        registerChangeRoutingServiceType: function () {
            var _this = this;
            $(this.routingServiceTypeSelector).on('change', function() {
                _this.changeRoutingServiceKeys();
            });
        },

        changeAutoGeoServiceKeys: function () {
            var autoGeoServiceId = $(this.autoGeoServiceTypeSelector).val();
            var data = this.getAutoGeoServiceDataById(autoGeoServiceId);

            if(data.hasKey1 === 1) {
                $(this.autoGeoServiceKeyLabel1Selector).text(data.keyLabel1);
                $(this.autoGeoServiceKeyLabel1Selector).parents('section').first().show();
            } else {
                $(this.autoGeoServiceKeyLabel1Selector).text('');
                $(this.autoGeoServiceKeyLabel1Selector).parents('section').first().hide();
            }

            if(data.hasKey2 === 1) {
                $(this.autoGeoServiceKeyLabel2Selector).text(data.keyLabel2);
                $(this.autoGeoServiceKeyLabel2Selector).parents('section').first().show();
            } else {
                $(this.autoGeoServiceKeyLabel2Selector).text('');
                $(this.autoGeoServiceKeyLabel2Selector).parents('section').first().hide();
            }
        },

        changeRoutingServiceKeys: function () {
            var routingServiceId = $(this.routingServiceTypeSelector).val();
            var data = this.getRoutingServiceDataById(routingServiceId);

            if(data.hasKey1 === 1) {
                $(this.routingServiceKeyLabel1Selector).text(data.keyLabel1);
                $(this.routingServiceKeyLabel1Selector).parents('section').first().show();
            } else {
                $(this.routingServiceKeyLabel1Selector).text('');
                $(this.routingServiceKeyLabel1Selector).parents('section').first().hide();
            }

            if(data.hasKey2 === 1) {
                $(this.routingServiceKeyLabel2Selector).text(data.keyLabel2);
                $(this.routingServiceKeyLabel2Selector).parents('section').first().show();
            } else {
                $(this.routingServiceKeyLabel2Selector).text('');
                $(this.routingServiceKeyLabel2Selector).parents('section').first().hide();
            }
        },

        getAutoGeoServiceDataById: function (id) {
            var selector = $(this.autoGeoServiceTemplateSelector).find('[data-auto-geo-service-type-id=' + id + ']');
            return {
                'hasKey1': selector.data('autoGeoServiceTypeHasKey1'),
                'keyLabel1': selector.data('autoGeoServiceTypeKeyLabel1'),
                'hasKey2': selector.data('autoGeoServiceTypeHasKey2'),
                'keyLabel2': selector.data('autoGeoServiceTypeKeyLabel2')
            };
        },

        getRoutingServiceDataById: function (id) {
            var selector = $(this.routingServiceTemplateSelector).find('[data-routing-service-type-id=' + id + ']');
            return {
                'hasKey1': selector.data('routingServiceTypeHasKey1'),
                'keyLabel1': selector.data('routingServiceTypeKeyLabel1'),
                'hasKey2': selector.data('routingServiceTypeHasKey2'),
                'keyLabel2': selector.data('routingServiceTypeKeyLabel2')
            };
        }
    };

    geoService.init();
})();