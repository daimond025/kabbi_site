$(function () {
    createCheckboxStringOnReady();
    phone_mask_init();
    //Кнопка "Выслать заново"
    $(document).on('click', 'div#user_invited .invites_control .again, .send_invite_again', function (e) {
        e.preventDefault();
        var user_id = $(this).parent().data('user');

        $(this).find('i').css({ opacity: 1, display: 'inline-block' }).fadeOut(3500);
        
        if (user_id) {
            $.post($(this).attr('href'), { user_id: user_id });
        }
    });

    //Кнопка "Отменить приглашение"
    $(document).on('click', 'div#user_invited .invites_control .cancel_invite', function (e) {
        e.preventDefault();
        var user_id = $(this).parent().data('user');
        doUserAction(user_id, $(this).attr('href'));
    });

    //Кнопка "Разблокировать"
    $(document).on('click', 'div#user_blocked .invites_control .remove_from_blacklist', function (e) {

        e.preventDefault();
        var user_id = $(this).parent().data('user');
        doUserAction(user_id, $(this).attr('href'));
    });

    //--------------------------------------------------------------------------------------------

    //Стилизация чекбоксов
    $('#add-user-form .a_sc, #update-user-form .a_sc').on('click', function (event) {
        showStyleFilter($(this), event);
        createCheckboxStringOnClick();
    });

    //--------------------------------------------------------------------------------------------
    //view.php
    $('#cancel_invite').click(function (e) {
        e.preventDefault();
        var parent = $(this).parent();
        var user_id = parent.data('user');
        var redirect = parent.data('red');

        $.post(
            $(this).attr('href'),
            { user_id: user_id },
            function (json) {
                if (json == 'ok')
                    location.href = redirect;
            },
            'json'
        );
    });

    $(document).on('change','.lang-selector',function(e){

        var lang = $(this).children().val();

        location.href = $(this).data('url')+lang;

    });

    //--------------------------------------------------------------------------------------------
});