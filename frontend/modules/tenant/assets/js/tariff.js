/*global jQuery*/
(function ($) {
    var app = {
        init: function () {
            this.registerChangeFilterTypeEvents();
            this.registerChooseFilterDateEvents();
        },
        registerChangeFilterTypeEvents: function () {
            var _this = this;
            $(document).on('click', 'form[name="history-search"] [type="radio"]', function () {
                _this.getPaymentHistory($(this).closest('form'));
            });
        },
        registerChooseFilterDateEvents: function () {
            var _this = this;

            $('.sdp').each(function () {
                var $obj = $(this);
                $obj.datepicker({
                    altField: $obj.prev('input'),
                    onSelect: function (date) {
                        var $input = $(this);
                        $input.parent().prev('.a_sc').html(date).removeClass('sel_active');
                        $input.parent().hide();
                        _this.getPaymentHistory($obj.closest('form'));
                    }
                });
                if ($obj.hasClass('first_date')) {
                    $obj.datepicker("setDate", '-14');
                }
            });
        },
        getPaymentHistory: function (form) {
            $.post(form.attr('action'), form.serialize())
                .done(function (data) {
                    var $grid = form.closest('.payment-history').find('.grid');
                    $grid.html(data);
                })
                .fail(function () {

                });
        }
    };

    app.init();
})(jQuery);