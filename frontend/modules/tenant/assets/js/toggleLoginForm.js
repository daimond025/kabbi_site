$(function () {


    $('body').on('click', '.change_tab', function () {
        $('.login, .restore_pass').toggle();
        if (!$('#loginform-email').is('[disabled]'))
        {
            $('#loginform-email').attr('disabled', 'disabled');
            $('#loginform-password').attr('disabled', 'disabled');
            $('#passwordresetrequestform-email').removeAttr('disabled');
        }
        else if ($('#loginform-email').is('[disabled]'))
        {
            $('#loginform-email').removeAttr('disabled');
            $('#loginform-password').removeAttr('disabled');
            $('#passwordresetrequestform-email').attr('disabled', 'disabled');
        }
    });
    $('#passwordResetRequest').on('click', function () {
        if (!$('#passwordresetrequestform-email').val())
        {
            $.get(
                    "/tenant/user/error-translation",
                    {'lang': $(this).data('lang')},
                    function (json)
                    {
                        $('#passwordresetrequestform-email').addClass('input_error');
                        $('#error').html(json);
                    },
                    'json'
                    );
        }
        else
        {
             $('#passwordresetrequestform-email').removeClass('input_error');
             $('#error').html('');
        }
        if ($('#passwordresetrequestform-email').val() != '')
        {

            var lang = $(this).data('lang');

            $.get(
                "/tenant/user/does-email-exists",
                {email: $('#passwordresetrequestform-email').val(), lang: lang},
                function (json)
                {
                    if (json != 'ok')
                    {
                        $('#passwordresetrequestform-email').addClass('input_error');
                        $('#error').html(json);
                    }
                    else
                    {
                        $.get(
                            "/tenant/user/request-password-reset",
                            {login: $('#passwordresetrequestform-email').val(), lang: lang},
                            function (json)
                            {
                                $('#messageWasSent').html(json);
                                $('#passwordresetrequestform-email').addClass('input_succes');
                            },
                            'json'
                        );
                    }
                },
                'json'
            );
    }
    });



    $(document).on('change','.lang-selector',function(e){

        var lang = $(this).children().val();

        location.href = $(this).data('url')+lang;

    });



});
$(window).on('load', function () {
   $('.wrapper').addClass('active');
});