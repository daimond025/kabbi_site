/* global jQuery */
(function ($) {
    'use strict';
    var app = {
        $bonusSystemForm: null,
        init: function () {
            this.$bonusSystemForm = $('.js-bonus-system-form');
            this.registerEvents();

            $('.js-bonus-system select').trigger('change');
        },
        registerEvents: function () {
            var _this = this;
            $('.js-bonus-system select').change(function () {
                var $this = $(this),
                    gootaxId = $this.data('gootax-id');

                if (+$this.val()) {
                    _this.$bonusSystemForm.show();
                    if ($this.val() == gootaxId) {
                        _this.$bonusSystemForm.find('[data-scenario="UDS Game"]').hide();
                    } else {
                        _this.$bonusSystemForm.find('[data-scenario="UDS Game"]').show();
                    }
                } else {
                    _this.$bonusSystemForm.hide();
                }
            });
        }
    };
    app.init();
})(jQuery);