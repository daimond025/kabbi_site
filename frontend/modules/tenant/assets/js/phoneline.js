$('#phone-line-city-select').on('change', function () {
    var city_id = $('#phone-line-city-select option:selected').val();
    var line_id = $('#line_id').val();
    
    tariffLoader(city_id, line_id);
});

//$(document).ready(function () {
//    var city_id = $('#phone-line-city-select option:selected').val();
//    var line_id = $('#line_id').val();
//    tariffLoader(city_id, line_id);
//});

function tariffLoader(city_id, line_id)
{
    $('#tariff_list').show();

    $.get(
        "/lib/phone-line/tariffs-by-city",
        {city_id: city_id, line_id: line_id},
        function (json)
        {
            var obj = JSON.parse(json);
            var select = '';
            for (var elem in obj)
            {
                if(obj[elem].isCurrent == 1)
                {
                    select += '<option selected = "selected" value='+  elem +'>'  + obj[elem].value + '</option>';
                }
                else
                {
                    select += '<option value='+  elem +'>'  + obj[elem].value + '</option>';
                }
            }

            $('#phone-line-tariff').html(select);
            $('#phone-line-tariff').trigger('update');
        }
    );
}