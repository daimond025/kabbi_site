<?php

namespace app\modules\tenant\assets;

use yii\web\AssetBundle;

class GeoServiceAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/tenant/assets/';

    public $publishOptions = [
        'forceCopy' => true
    ];

    public $js = [
        'js/geo-service.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}