<?php


namespace app\modules\tenant\assets;


use yii\web\AssetBundle;

class UserAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/tenant/assets/js/';

    public $publishOptions = [
        'forceCopy' => true,
        'only' => [
            'functions.js',
            'main.js',
        ]
    ];

    public $css = [
    ];
    public $js = [
        'functions.js',
        'main.js',
    ];
    public $depends = [
    ];
}