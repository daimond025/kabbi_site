<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\tenant\assets;

use yii\web\AssetBundle;

/**
 * author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class TenantAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/tenant/assets/js/';
    
    public $publishOptions = [
        'forceCopy' => true
    ];

    public $css = [
    ];
    public $js = [
    ];
    public $depends = [
    ];
}