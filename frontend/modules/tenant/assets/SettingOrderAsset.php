<?php
namespace app\modules\tenant\assets;

use yii\web\AssetBundle;

class SettingOrderAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/tenant/assets/js/';

    public $publishOptions = [
        'forceCopy' => true,
        'only' => [
            'setting.js',
            'setting_orders.js',
        ]
    ];
    public $css = [
    ];
    public $js = [
        'setting.js',
        'setting_orders.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}