<?php

namespace frontend\modules\tenant\assets;

use yii\web\AssetBundle;

/**
 * Class TariffAsset
 * @package frontend\tenant\assets
 */
class TariffAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/tenant/assets/';
    public $publishOptions = [
        'forceCopy' => true,
        'only'      => [
            'js/tariff.js',
        ],
    ];
    public $css = [
    ];
    public $js = [
        'js/tariff.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}