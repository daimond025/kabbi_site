<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\modules\tenant\assets;

use yii\web\AssetBundle;

class LoginAsset extends AssetBundle
{

    public $sourcePath = '@frontend/modules/tenant/assets/';
    public $publishOptions = [
        'forceCopy' => true,
        'only' => [
            //'css/login.css',
            'css/login_new.css',
            'js/html5.js',
            'js/toggleLoginForm.js',
            //'images/icons.png',
        ]
    ];
    public $css = [
       // 'css/login.css',
        'css/login_new.css',
    ];
    public $js = [
        'js/html5.js',
        'js/toggleLoginForm.js',
    ];
    public $depends = [
        'frontend\assets\PluginsAsset',
        'yii\web\JqueryAsset',
    ];

}
