<?php

namespace frontend\modules\tenant\assets;

use yii\web\AssetBundle;

/**
 * author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class PaymentGateProfileAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/tenant/assets/js/';

    public $publishOptions = [
        'forceCopy' => true,
        'only' => [
            'paymentGateProfile.js',
        ]
    ];
    public $css = [
    ];
    public $js = [
        'paymentGateProfile.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}