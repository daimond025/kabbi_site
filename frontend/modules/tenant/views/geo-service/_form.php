<?php
/**
 * @var $this yii\web\View
 * @var $model \frontend\modules\tenant\models\GeoServiceForm
 * @var $cityList array
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\modules\tenant\assets\GeoServiceAsset;
use \frontend\modules\tenant\models\GeoServiceForm;

GeoServiceAsset::register($this);

$form = ActiveForm::begin([
    'id'                     => 'geo-service-form',
    'errorCssClass'          => 'input_error',
    'successCssClass'        => 'input_success',
    'enableClientValidation' => true,
    'options'                => [
        'enctype' => 'multipart/form-data',
    ],
]);


$autoGeoServiceTypeContainer = [];
foreach ($model->getAutoGeoServiceTypes() as $autoGeoServiceType) :
    $autoGeoServiceTypeContainer[] = Html::tag('span', '1', [
        'data' => [
            'auto-geo-service-type-id'          => $autoGeoServiceType->id,
            'auto-geo-service-type-has-key1'   => (int)$autoGeoServiceType->has_key_1,
            'auto-geo-service-type-key-label1' => (string)$autoGeoServiceType->key_1_label,
            'auto-geo-service-type-has-key2'   => (int)$autoGeoServiceType->has_key_2,
            'auto-geo-service-type-key-label2' => (string)$autoGeoServiceType->key_2_label,
        ],
    ]);

endforeach;

$routingServiceTypeContainer = [];
foreach ($model->getRoutingServiceTypes() as $routingServiceType) :
    $routingServiceTypeContainer[] = Html::tag('span', '1', [
        'data' => [
            'routing-service-type-id'          => $routingServiceType->id,
            'routing-service-type-has-key1'   => (int)$routingServiceType->has_key_1,
            'routing-service-type-key-label1' => (string)$routingServiceType->key_1_label,
            'routing-service-type-has-key2'   => (int)$routingServiceType->has_key_2,
            'routing-service-type-key-label2' => (string)$routingServiceType->key_2_label,
        ],
    ]);

endforeach; ?>

<?= Html::tag('div', implode(PHP_EOL, $autoGeoServiceTypeContainer), [
    'class' => 'js-auto-geo-services',
    'style' => 'display: none',
]); ?>

<?= Html::tag('div', implode(PHP_EOL, $routingServiceTypeContainer), [
    'class' => 'js-routing-services',
    'style' => 'display: none',
]); ?>

<!--Автокомплит и геокодинг-->
<section class="row">
    <?= $form->field($model, 'auto_service_provider_id')->begin(); ?>
    <div class="row_label">
        <?= Html::activeLabel($model, 'auto_service_provider_id'); ?>
    </div>
    <div class="row_input">
        <?= Html::activeDropDownList($model, 'auto_service_provider_id', $model->getAutoGeoServiceProviderNameList(), [
            'data-height' => 150,
            'class'       => 'default_select js-auto_service_provider_id',
        ]); ?>
        <div class="help-block" style="color: red">
            <?= Html::error($model, 'auto_service_provider_id'); ?>
        </div>
    </div>
    <?= $form->field($model, 'auto_service_provider_id')->end(); ?>
</section>

<!--Радиус поиска-->
<section class="row">
    <?= $form->field($tenantSetting, 'value')->begin(); ?>
    <div class="row_label">
        <?= Html::activeLabel($tenantSetting, t('app', 'Radius')); ?>
    </div>
    <div class="row_input">
        <?=
        Html::activeTextInput($tenantSetting, 'value'); ?>
        <div class="help-block" style="color: red">
            <?= Html::error($tenantSetting, 'value'); ?>
        </div>
    </div>
    <?= $form->field($tenantSetting, 'value')->end(); ?>
</section>

<!--key 1-->
<section class="row">
    <?= $form->field($model, 'auto_key_1')->begin(); ?>
    <div class="row_label">
        <?= Html::activeLabel($model, 'auto_key_1', [
            'class' => 'js-auto-geo-key-label-1'
        ]); ?>
    </div>
    <div class="row_input">
        <?= Html::activeTextInput($model, 'auto_key_1', [
                'class' => 'js-auto-geo-key-1'
        ]); ?>
        <div class="help-block" style="color: red">
            <?= Html::error($model, 'auto_key_1') ?>
        </div>
    </div>
    <?= $form->field($model, 'auto_key_1')->end(); ?>
</section>

<!--key 2-->
<section class="row">
    <?= $form->field($model, 'auto_key_2')->begin(); ?>
    <div class="row_label">
        <?= Html::activeLabel($model, 'auto_key_2', [
            'class' => 'js-auto-geo-key-label-2'
        ]); ?>
    </div>
    <div class="row_input">
        <?= Html::activeTextInput($model, 'auto_key_2', [
            'class' => 'js-auto-geo-key-2'
        ]); ?>
        <div class="help-block" style="color: red">
            <?= Html::error($model, 'auto_key_2') ?>
        </div>
    </div>
    <?= $form->field($model, 'auto_key_2')->end(); ?>
</section>


<!--Расчет маршрута-->
<section class="row">
    <?= $form->field($model, 'routing_service_provider_id')->begin(); ?>
    <div class="row_label">
        <?= Html::activeLabel($model, 'routing_service_provider_id'); ?>
    </div>
    <div class="row_input">
        <?= Html::activeDropDownList($model, 'routing_service_provider_id', $model->getRoutingServiceProviderNameList(),
            [
                'data-height' => 150,
                'class'       => 'default_select js-routing_service_provider_id',
            ]); ?>
        <div class="help-block" style="color: red">
            <?= Html::error($model, 'routing_service_provider_id'); ?>
        </div>
    </div>
    <?= $form->field($model, 'auto_service_provider_id')->end(); ?>
</section>

<!--key 1-->
<section class="row">
    <?= $form->field($model, 'routing_key_1')->begin(); ?>
    <div class="row_label">
        <?= Html::activeLabel($model, 'routing_key_1', [
            'class' => 'js-routing-key-label-1'
        ]); ?>
    </div>
    <div class="row_input">
        <?= Html::activeTextInput($model, 'routing_key_1', [
            'class' => 'js-routing-key-1'
        ]); ?>
        <div class="help-block" style="color: red">
            <?= Html::error($model, 'routing_key_1') ?>
        </div>
    </div>
    <?= $form->field($model, 'routing_key_1')->end(); ?>
</section>

<!--key 2-->
<section class="row">
    <?= $form->field($model, 'routing_key_2')->begin(); ?>
    <div class="row_label">
        <?= Html::activeLabel($model, 'routing_key_2', [
            'class' => 'js-routing-key-label-2'
        ]); ?>
    </div>
    <div class="row_input">
        <?= Html::activeTextInput($model, 'routing_key_2', [
        ]); ?>
        <div class="help-block" style="color: red">
            <?= Html::error($model, 'routing_key_2', [
                'class' => 'js-routing-key-2'
            ]) ?>
        </div>
    </div>
    <?= $form->field($model, 'routing_key_2')->end(); ?>
</section>


<section class="row">
    <?= $form->field($model, 'default')->begin(); ?>
    <div class="row_label">

    </div>
    <div class="row_input">
        <?= Html::activeCheckbox($model, 'default', [
        ]); ?>
        <div class="help-block" style="color: red">
            <?= Html::error($model, 'default', [
                'class' => 'js-routing-key-2'
            ]) ?>
        </div>
    </div>
    <?= $form->field($model, 'default')->end(); ?>
</section>

<section class="submit_form">
    <?= Html::submitInput(t('app', 'Save')) ?>
</section>

<?php ActiveForm::end(); ?>
