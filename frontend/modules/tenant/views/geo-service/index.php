<?php

use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $model \frontend\modules\tenant\models\GeoServiceForm
 * @var $cityList array
 */

$this->title = t('setting', 'Maps and routes');
?>

<h1><?= Html::encode($this->title) ?></h1>

<?= \frontend\modules\tenant\widgets\CityListNavigateWidget::widget([
    'cityList'      => $cityList,
    'currentCityId' => (int)$model->auto_city_id,
    'url'           => ['/tenant/geo-service/list'],
]); ?>

<?= $this->render('_form', compact('model','tenantSetting')); ?>
