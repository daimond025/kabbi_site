<?php

/* @var $this \yii\web\View */
/* @var $payments array */

?>

<? if (empty($payments)): ?>
    <p><?= t('app', 'Empty') ?></p>
<? else: ?>
    <table class="people_table money_table transaction_list">
        <tbody>
        <tr>
            <th style="width: 15%">Дата платежа (время сервера)</th>
            <th style="width: 15%">Идентификатор магазина (shopId)</th>
            <th style="width: 15%">Идентификатор платежа (invoiceId)</th>
            <th style="width: 15%">Номер заказа</th>
            <th style="width: 15%">Сумма</th>
            <th style="width: 10%">Оплачен?</th>
            <th style="width: 15%">Статус отложенного платежа</th>
        </tr>
        <?= $this->render('_rows', compact('payments')) ?>
        </tbody>
    </table>
<? endif; ?>
