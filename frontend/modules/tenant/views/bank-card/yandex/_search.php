<?php

use common\helpers\DateTimeHelper;
use yii\helpers\Url;

/* @var $this \yii\web\View */

$formatter = app()->formatter;

?>

<form name="yandex-failed-payments-search"
      action="<?= Url::to(['/tenant/bank-card/yandex-failed-payments?ajaxSearch=1']) ?>"
      method="post">
    <div class="oor_fil" style="height: 70px; float: left; width: 100%;">
        <p>Максимальный диапазон времени, ограничивающий выборку, не должен превышать 31 день, иначе будет получен пустой результат</p>
        <div class="cof_left" style="margin-right: 0;">
            <div class="cof_first">
                <label><input name="filter" type="radio" checked="" value="today"> <?= t('reports', 'Today') ?></label>
            </div>
            <div class="cof_first">
                <label><input name="filter" type="radio" value="month"> <?= DateTimeHelper::getMonthList()[date('m')] ?>
                </label>
            </div>

            <div class="cof_second disabled_cof">
                <label>
                    <input name="filter" class="cof_time_choser" type="radio" value="period"> <?= t('reports',
                        'Period') ?>
                </label>
                <div class="cof_date notranslate disabled_select">
                    <a class="s_datepicker a_sc select"
                       id="datepicker"><?= $formatter->asDate(strtotime('-14day')); ?></a>
                    <div class="s_datepicker_content b_sc" style="display: none;">
                        <input name="first_date" class="sdp_input" type="text"
                               value="<?= $formatter->asDate(strtotime('-14day')) ?>">
                        <div class="sdp first_date"></div>
                    </div>
                </div>
                <hr>
                <div class="cof_date notranslate disabled_select">
                    <a class="s_datepicker a_sc select"><?= $formatter->asDate('now') ?></a>
                    <div class="s_datepicker_content b_sc" style="display: none;">
                        <input name="second_date" class="sdp_input" type="text"
                               value="<?= $formatter->asDate('now') ?>">
                        <div class="sdp"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>