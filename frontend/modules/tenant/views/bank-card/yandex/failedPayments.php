<?php

use frontend\modules\tenant\assets\PaymentGateProfileAsset;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $payments array */

PaymentGateProfileAsset::register($this);

$this->title = t('setting', 'Bank cards');

?>

<h1><?= Html::encode($this->title) ?></h1>

<h2>Ошибочные платежи Яндекс кассы (безопасная сделка)</h2>

<div class="yandex-failed-payments">
    <?= $this->render('_search') ?>

    <div class="grid">
        <?= $this->render('_grid', compact('payments')); ?>
    </div>
</div>

