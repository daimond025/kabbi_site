<?php

use frontend\modules\tenant\models\Currency;

/* @var $this \yii\web\View */
/* @var $payments array */

$formatter = app()->formatter;

$currencyCodeMap = Currency::getSymbolMapIndexedByNumericCode();
?>

<?php foreach ($payments as $payment): ?>
    <tr>
        <?php
        $date           = isset($payment['date']) ? $formatter->asDatetime($payment['date']) : null;
        $shopId         = isset($payment['shopId']) ? $payment['shopId'] : null;
        $invoiceId      = isset($payment['invoiceId']) ? $payment['invoiceId'] : null;
        $orderNumber    = isset($payment['orderNumber']) ? $payment['orderNumber'] : null;
        $sum            = isset($payment['sum']) ? $payment['sum'] : 0;
        $currencyCode   = isset($payment['currency']) ? $payment['currency'] : null;
        $currencySymbol = isset($currencyCodeMap[$currencyCode]) ? $currencyCodeMap[$currencyCode] : null;
        $paid           = empty($payment['paid']) ? t('app', 'No') : t('app', 'Yes');
        $clearingStatus = isset($payment['clearing']) ? $payment['clearing'] : null;
        ?>

        <td><?= $date ?></td>
        <td><?= $shopId ?></td>
        <td><?= $invoiceId ?></td>
        <td><?= $orderNumber ?></td>
        <td><?= $formatter->asMoney(+$sum, $currencySymbol) ?></td>
        <td><?= $paid ?></td>
        <td><?= $clearingStatus ?></td>
    </tr>
<?php endforeach; ?>