<?php

use paymentGate\models\BaseProfile;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $profile BaseProfile */

?>

<?
$viewFile = 'profiles/_' . $profile->getViewPath();
echo $this->render($viewFile, compact('form', 'profile'));
?>
