<?php

use paymentGate\models\BaseProfile;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $profile BaseProfile */
/* @var $profileTypes array */
/* @var $cities array */
/* @var $currentCityId integer */
/* @var $isAllowed bool */

$this->title = t('setting', 'Bank cards');

?>

<h1><?= Html::encode($this->title) ?></h1>

<h2><?= t('bank-card', 'Profile of payment gate') ?></h2>

<?= $this->render('_city_list', [
    'currentCityId' => $currentCityId,
    'userCityList'  => $cities,
]) ?>

<div class="js-profile">
    <?= $this->render('_form', compact('profile', 'profileTypes', 'isAllowed', 'currentCityId')) ?>
</div>
