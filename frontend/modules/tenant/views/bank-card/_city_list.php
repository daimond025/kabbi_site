<?php

/* @var $userCityList array */
/* @var $currentCityId integer */


use yii\helpers\Url;
use yii\helpers\Html;

if(count($userCityList) > 1): ?>
    <div class="order_cities">
        <ul>
            <?php foreach ($userCityList as $cityId => $name): ?>
                <li>
                    <a
                        <?php if ($currentCityId == $cityId): ?>
                            class="active" onclick="return false"
                        <? endif ?>
                            href="<?= Url::to(['/tenant/bank-card/update', 'city_id' => $cityId]) ?>">
                        <span><?= Html::encode($name); ?></span>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>