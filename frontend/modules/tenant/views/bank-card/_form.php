<?php

use paymentGate\models\BaseProfile;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\modules\tenant\assets\PaymentGateProfileAsset;

/* @var $this yii\web\View */
/* @var $profile BaseProfile */
/* @var $profileTypes array */
/* @var $isAllowed bool */
/* @var $currentCityId int */

PaymentGateProfileAsset::register($this);

?>

<?
$form = null;
if ($isAllowed) {
    $form = ActiveForm::begin([
        'action'                 => '/tenant/bank-card/update',
        'errorCssClass'          => 'input_error',
        'successCssClass'        => 'input_success',
        'options'                => ['enctype' => 'multipart/form-data'],
        'enableClientValidation' => false,
        'enableAjaxValidation'   => false,
    ]);
}
?>

<section class="row">
    <?= $form === null ? null : $form->field($profile, 'typeId')->begin() ?>
    <div class="row_label">
        <?= Html::activeLabel($profile, 'typeId') ?>
    </div>
    <div class="row_input">
        <?= Html::activeDropDownList($profile, 'typeId', $profileTypes, [
            'disabled' => $form === null,
            'class'    => 'default_select',
        ]) ?>
    </div>
    <?= $form === null ? null : $form->field($profile, 'typeId')->end() ?>
</section>

<?= $form->field($profile, 'city_id')->hiddenInput([
    'value' => $currentCityId
])->label(false) ?>

<?= $this->render('_profile', compact('form', 'profile')) ?>

<? if ($isAllowed): ?>
    <section class="submit_form">
        <?= Html::submitInput(Yii::t('app', 'Save')) ?>
    </section>

    <? ActiveForm::end() ?>
<? endif ?>
