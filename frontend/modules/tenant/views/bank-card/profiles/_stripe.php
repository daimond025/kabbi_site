<?php

use paymentGate\models\Stripe;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $form ActiveForm */
/* @var $profile Stripe */
/* @var $useStripeConnect bool */

?>

<?php foreach (['useStripeConnect', 'publicKey', 'privateKey', 'clientId', 'commission'] as $attribute): ?>
    <section class="row">
        <?= $form === null ? null : $form->field($profile, $attribute)->begin() ?>
        <div class="row_label">
            <?= \in_array($attribute, ['isDebug', 'useStripeConnect'], true) ? null : Html::activeLabel($profile,
                $attribute) ?>
        </div>
        <div class="row_input">
            <?php
            switch ($attribute) {
                case 'privateKey':
                    echo Html::activePasswordInput($profile, $attribute, ['disabled' => $form === null]);
                    break;
                case 'currency':
                    echo Html::activeDropDownList($profile, 'currency', Currency::getNumberCurrencyMap(), [
                        'class'    => 'default_select',
                        'disabled' => $form === null,
                    ]);
                    break;
                case 'privateCertificateFile':
                case 'publicCertificateFile':
                    echo Html::activeFileInput($profile, $attribute, ['disabled' => $form === null]);
                    break;
                case 'useStripeConnect':
                    echo Html::activeCheckbox($profile, $attribute, ['disabled' => $form === null]);
                    break;
                default:
                    echo Html::activeTextInput($profile, $attribute, ['disabled' => $form === null]);
            }
            ?>
        </div>
        <?= $form === null ? null : $form->field($profile, $attribute)->end() ?>
    </section>
<? endforeach ?>