<?php

use frontend\modules\tenant\models\Currency;
use paymentGate\models\Alfabank;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $form ActiveForm */
/* @var $profile Alfabank */

?>

<?php foreach ([
                   'username',
                   'password',
                   'url',
                   'returnUrl',
                   'failUrl',
                   'commission',
                   'cardBindingSum',
                   'currency',
               ] as $attribute): ?>
    <section class="row">
        <?= $form === null ? null : $form->field($profile, $attribute)->begin() ?>
        <div class="row_label">
            <?= Html::activeLabel($profile, $attribute) ?>
        </div>
        <div class="row_input">
            <?php
            switch ($attribute) {
                case 'password':
                    echo Html::activePasswordInput($profile, $attribute, ['disabled' => $form === null]);
                    break;
                case 'currency':
                    echo Html::activeDropDownList($profile, 'currency', Currency::getNumberCurrencyMap(), [
                        'class' => 'default_select',
                    ]);
                    break;
                default:
                    echo Html::activeTextInput($profile, $attribute, ['disabled' => $form === null]);
            }
            ?>
        </div>
        <?= $form === null ? null : $form->field($profile, $attribute)->end() ?>
    </section>
<? endforeach ?>