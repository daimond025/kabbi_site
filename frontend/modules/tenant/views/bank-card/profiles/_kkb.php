<?php

use frontend\modules\tenant\models\Currency;
use paymentGate\models\Kkb;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $form ActiveForm */
/* @var $profile Kkb */

?>

<?php foreach ([
                   'merchantId',
                   'merchantName',
                   'certificateId',
                   'privateCertificateFile',
                   'privateCertificatePass',
                   'publicCertificateFile',
                   'isDebug',
                   'cardBindingSum',
                   'commission',
                   'currency',
               ] as $attribute): ?>

    <section class="row">
        <?= $form === null ? null : $form->field($profile, $attribute)->begin() ?>
        <div class="row_label">
            <?= $attribute === 'isDebug' ? null : Html::activeLabel($profile, $attribute) ?>
        </div>
        <div class="row_input">
            <?php
            switch ($attribute) {
                case 'privateCertificatePass':
                    echo Html::activePasswordInput($profile, $attribute, ['disabled' => $form === null]);
                    break;
                case 'currency':
                    echo Html::activeDropDownList($profile, 'currency', Currency::getNumberCurrencyMap(), [
                        'class'    => 'default_select',
                        'disabled' => $form === null,
                    ]);
                    break;
                case 'privateCertificateFile':
                case 'publicCertificateFile':
                    echo Html::activeFileInput($profile, $attribute, ['disabled' => $form === null]);
                    break;
                case 'isDebug':
                    echo Html::activeCheckbox($profile, $attribute, ['disabled' => $form === null]);
                    break;
                default:
                    echo Html::activeTextInput($profile, $attribute, ['disabled' => $form === null]);
            }
            ?>
        </div>
        <?= $form === null ? null : $form->field($profile, $attribute)->end() ?>
    </section>
<? endforeach ?>