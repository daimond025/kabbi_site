<?php

use paymentGate\models\Paycom;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $form ActiveForm */
/* @var $profile Paycom */

?>

<? foreach ([
                'apiId',
                'password',
                'isDebug',
                'commission',
            ] as $attribute): ?>
    <section class="row">
        <?= $form === null ? null : $form->field($profile, $attribute)->begin() ?>
        <div class="row_label">
            <?= Html::activeLabel($profile, $attribute) ?>
        </div>
        <div class="row_input">
            <?php switch ($attribute) {
                case 'password':
                    echo Html::activePasswordInput($profile, $attribute, ['disabled' => $form === null]);
                    break;
                case 'isDebug':
                    echo Html::activeCheckbox($profile, $attribute, ['disabled' => $form === null]);
                    break;
                default:
                    echo Html::activeTextInput($profile, $attribute, ['disabled' => $form === null]);
            } ?>
        </div>
        <?= $form === null ? null : $form->field($profile, $attribute)->end() ?>
    </section>
<? endforeach ?>