<?php

use paymentGate\models\Yandex;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $form ActiveForm */
/* @var $profile Yandex */

?>

<? foreach ([
                'shopId',
                'scid',
                //'shopArticleId',
                'password',
                'secureDeal',
                'secureDealShopId',
                'secureDealScid',
                'secureDealShopArticleId',
                'secureDealPaymentShopId',
                'secureDealPaymentScid',
                'secureDealPaymentShopArticleId',
                'certificateFile',
                'keyFile',
                'certificatePassword',
                'isDebug',
                'cardBindingSum',
                'commission',
                'useReceipt',
                'product',
                'vat',
            ] as $attribute): ?>

    <?= $attribute === 'secureDealShopId'
        ? '<div class="js-secure-deal" style="' . (empty($profile['secureDeal']) ? 'display: none' : '') . '">' : '' ?>
    <?= $attribute === 'product'
        ? '<div class="js-receipt" style="' . (empty($profile['useReceipt']) ? 'display: none' : '') . '">' : '' ?>

    <section class="row">
        <?= $form === null ? null : $form->field($profile, $attribute)->begin() ?>
        <div class="row_label">
            <?= in_array($attribute, ['isDebug', 'secureDeal', 'useReceipt'], true) ? null : Html::activeLabel($profile,
                $attribute) ?>
        </div>
        <div class="row_input">
            <?
            switch ($attribute) {
                case 'password':
                case 'certificatePassword':
                    echo Html::activePasswordInput($profile, $attribute, ['disabled' => $form === null]);
                    break;
                case 'certificateFile':
                case 'keyFile':
                    echo Html::activeFileInput($profile, $attribute, ['disabled' => $form === null]);
                    break;
                case 'isDebug':
                case 'useReceipt':
                    echo Html::activeCheckbox($profile, $attribute, ['disabled' => $form === null]);
                    break;
                case 'vat':
                    echo Html::activeDropDownList($profile, $attribute, Yandex::getVatMap(),
                        ['class' => 'default_select']);
                    break;
                case 'secureDeal':
                    echo Html::activeCheckbox($profile, $attribute, ['disabled' => $form === null]);
                    echo Html::tag('p',
                        Html::a('Ошибочные платежи (безопасная сделка)', ['yandex-failed-payments'],
                            [
                                'class' => 'js-yandex-failed-payments',
                                'style' => empty($profile['secureDeal']) ? 'display: none' : '',
                            ]));
                    break;
                default:
                    echo Html::activeTextInput($profile, $attribute, ['disabled' => $form === null]);
            }
            ?>
        </div>
        <?= $form === null ? null : $form->field($profile, $attribute)->end() ?>
    </section>
    <?= $attribute === 'secureDealPaymentShopArticleId' ? '</div>' : '' ?>
    <?= $attribute === 'vat' ? '</div>' : '' ?>
<? endforeach ?>