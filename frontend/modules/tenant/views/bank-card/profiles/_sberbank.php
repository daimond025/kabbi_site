<?php

use paymentGate\models\Sberbank;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $form ActiveForm */
/* @var $profile Sberbank */

?>

<? foreach ([
                'usernameBinding',
                'passwordBinding',
                'usernamePayment',
                'passwordPayment',
                'url',
                'returnUrl',
                'failUrl',
                'commission',
            ] as $attribute): ?>
    <section class="row">
        <?= $form === null ? null : $form->field($profile, $attribute)->begin() ?>
        <div class="row_label">
            <?= Html::activeLabel($profile, $attribute) ?>
        </div>
        <div class="row_input">
            <?=
            in_array($attribute, ['passwordBinding', 'passwordPayment'], true)
                ? Html::activePasswordInput($profile, $attribute, ['disabled' => $form === null])
                : Html::activeTextInput($profile, $attribute, ['disabled' => $form === null])
            ?>
        </div>
        <?= $form === null ? null : $form->field($profile, $attribute)->end() ?>
    </section>
<? endforeach ?>