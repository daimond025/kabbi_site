<?php

use yii\widgets\ActiveForm;
use app\modules\tenant\models\field\EmployeeFieldModel;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**
 * @var $this yii\web\View
 * @var $model EmployeeFieldModel
 * @var $isSavedModel boolean
 */

Pjax::begin(['id' => 'car_field_setting']);

?>


<? $form = ActiveForm::begin([
    'id'                     => 'car-field-form',
    'errorCssClass'          => 'input_error',
    'successCssClass'        => 'input_success',
    'enableClientValidation' => true,
    'validateOnBlur'         => false,
    'options'                => [
        'enctype' => 'multipart/form-data',
        'data'    => [
            'pjax' => true,
        ],
    ],
]); ?>

<?= Html::tag('h3', Html::encode(t('setting', 'Profile'))); ?>

        <section class="row">
            <?= $form->field($model, 'position_car_carCallsign')->begin(); ?>
            <div class="row_input">
                <?= Html::activeCheckbox($model, 'position_car_carCallsign') ?>
            </div>
            <?= $form->field($model, 'position_car_carCallsign')->end(); ?>
        </section>

        <section class="row">
            <?= $form->field($model, 'position_car_carLicense')->begin(); ?>
            <div class="row_input">
                <?= Html::activeCheckbox($model, 'position_car_carLicense') ?>
            </div>
            <?= $form->field($model, 'position_car_carLicense')->end(); ?>
        </section>

    <section class="submit_form">
        <? if ($isSavedModel === true): ?>
            <?= Html::tag('div', t('app', 'The data have saved successfully'), ['style' => 'color: green']); ?>
        <? endif; ?>
        <?= Html::submitInput(t('app', 'Save')) ?>
    </section>

<?php ActiveForm::end(); ?>
<? Pjax::end() ?>