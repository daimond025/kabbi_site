<?php


use yii\helpers\Html;
use app\modules\tenant\models\field\EmployeeFieldModel;
use yii\widgets\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $model EmployeeFieldModel
 * @var $form ActiveForm
 * @var $positionId integer
 * @var $positionName string
 * @var $isAllowed boolean
 */

$attributes = $model->getAttributesByPosition($positionId);

?>

<? if (!empty($attributes)): ?>
    <?= Html::tag('h3', $positionName); ?>
    <? foreach ($attributes as $attribute): ?>
        <section class="row">
            <?= $form->field($model, "position_{$positionId}_$attribute")->begin(); ?>
            <div class="row_input">
                <?= Html::activeCheckbox($model, "position_{$positionId}_$attribute",
                    ['label' => $model->getAttributeLabel($attribute), 'disabled' => !$isAllowed]) ?>
            </div>
            <?= $form->field($model, "position_{$positionId}_$attribute")->end(); ?>
        </section>
    <? endforeach; ?>
<? endif; ?>
