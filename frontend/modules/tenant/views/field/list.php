<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\modules\tenant\models\field\EmployeeFieldModel;
use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $model EmployeeFieldModel
 * @var $cityId integer
 * @var $cityMap array
 * @var $positionMap
 * @var $isSavedModel boolean
 * @var $isAllowed boolean
 */

$this->title = t('setting', 'Custom Fields');

?>

<h1><?= Html::encode($this->title) ?></h1>

<?= $this->render('@app/modules/tenant/views/setting/_city_list',
    [
        'setting_city_id' => $cityId,
        'user_city_list'  => $cityMap,
        'action'          => [
            ArrayHelper::getValue(Yii::$app->request->queryParams, 'q'),
            'city_id'     => ArrayHelper::getValue(Yii::$app->request->queryParams, 'city_id'),
            'position_id' => ArrayHelper::getValue(Yii::$app->request->queryParams, 'position_id'),
        ],
    ]);
?>

<p style="margin-bottom: 20px;">
    <?= t('setting', 'In this section, you can hide one or more fields from the Artists and Cars section.') ?>
</p>

<section class="main_tabs">
    <div class="tabs_links">
        <ul>
            <li><a href="#worker" class="t01"><?= t('setting', 'Workers') ?></a></li>
            <li><a href="#car" class="t02"
                   data-href="<?= Url::to(['car-list', 'city_id' => $cityId]) ?>"><?= t('setting', 'Cars') ?></a>
        </ul>
    </div>
    <div class="tabs_content">
        <div id="t01" data-view="worker">
            <?= $this->render('_employee', compact('model', 'positionMap', 'isSavedModel', 'isAllowed')) ?>
        </div>
        <div id="t02" data-view="car">
        </div>
    </div>
</section>


