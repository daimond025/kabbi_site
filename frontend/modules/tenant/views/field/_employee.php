<?php

use yii\widgets\ActiveForm;
use app\modules\tenant\models\field\EmployeeFieldModel;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**
 * @var $this yii\web\View
 * @var $model EmployeeFieldModel
 * @var $cityId integer
 * @var $cityMap array
 * @var $positionMap array
 * @var $isSavedModel boolean
 * @var $isAllowed boolean
 */

Pjax::begin(['id' => 'employee_field_setting']);

?>


<? $form = ActiveForm::begin([
    'id'                     => 'employee-field-form',
    'errorCssClass'          => 'input_error',
    'successCssClass'        => 'input_success',
    'enableClientValidation' => true,
    'validateOnBlur'         => false,
    'options'                => [
        'enctype' => 'multipart/form-data',
        'data'    => [
            'pjax' => true,
        ],
    ],
]); ?>

<?= Html::tag('h3', Html::encode(t('setting', 'Profile'))); ?>

    <div class="first">
        <section class="row">
            <?= $form->field($model, 'position_worker_workerBirthday')->begin(); ?>
            <div class="row_input">
                <?= Html::activeCheckbox($model, 'position_worker_workerBirthday', ['disabled' => !$isAllowed]) ?>
            </div>
            <?= $form->field($model, 'position_worker_workerBirthday')->end(); ?>
        </section>

        <section class="row">
            <?= $form->field($model, 'position_worker_workerEmail')->begin(); ?>
            <div class="row_input">
                <?= Html::activeCheckbox($model, 'position_worker_workerEmail', ['disabled' => !$isAllowed]) ?>
            </div>
            <?= $form->field($model, 'position_worker_workerEmail')->end(); ?>
        </section>

        <section class="row">
            <?= $form->field($model, 'position_worker_workerCardNumber')->begin(); ?>
            <div class="row_input">
                <?= Html::activeCheckbox($model, 'position_worker_workerCardNumber', ['disabled' => !$isAllowed]) ?>
            </div>
            <?= $form->field($model, 'position_worker_workerCardNumber')->end(); ?>
        </section>

        <section class="row">
            <?= $form->field($model, 'position_worker_workerYandexAccountNumber')->begin(); ?>
            <div class="row_input">
                <?= Html::activeCheckbox($model, 'position_worker_workerYandexAccountNumber',
                    ['disabled' => !$isAllowed]) ?>
            </div>
            <?= $form->field($model, 'position_worker_workerYandexAccountNumber')->end(); ?>
        </section>

        <section class="row">
            <?= $form->field($model, 'position_worker_workerDescription')->begin(); ?>
            <div class="row_input">
                <?= Html::activeCheckbox($model, 'position_worker_workerDescription', ['disabled' => !$isAllowed]) ?>
            </div>
            <?= $form->field($model, 'position_worker_workerDescription')->end(); ?>
        </section>
    </div>

    <div class="second">
        <section class="row">
            <?= $form->field($model, 'position_worker_workerPassport')->begin(); ?>
            <div class="row_input">
                <?= Html::activeCheckbox($model, 'position_worker_workerPassport', ['disabled' => !$isAllowed]) ?>
            </div>
            <?= $form->field($model, 'position_worker_workerPassport')->end(); ?>
        </section>

        <section class="row">
            <?= $form->field($model, 'position_worker_workerSNILS')->begin(); ?>
            <div class="row_input">
                <?= Html::activeCheckbox($model, 'position_worker_workerSNILS', ['disabled' => !$isAllowed]) ?>
            </div>
            <?= $form->field($model, 'position_worker_workerSNILS')->end(); ?>
        </section>

        <section class="row">
            <?= $form->field($model, 'position_worker_workerINN')->begin(); ?>
            <div class="row_input">
                <?= Html::activeCheckbox($model, 'position_worker_workerINN', ['disabled' => !$isAllowed]) ?>
            </div>
            <?= $form->field($model, 'position_worker_workerINN')->end(); ?>
        </section>

        <section class="row">
            <?= $form->field($model, 'position_worker_workerOGRNIP')->begin(); ?>
            <div class="row_input">
                <?= Html::activeCheckbox($model, 'position_worker_workerOGRNIP', ['disabled' => !$isAllowed]) ?>
            </div>
            <?= $form->field($model, 'position_worker_workerOGRNIP')->end(); ?>
        </section>
    </div>

<? foreach ($positionMap as $positionId => $positionName): ?>
    <?= $this->render('_position', compact('model', 'form', 'positionId', 'positionName', 'isAllowed')); ?>
<? endforeach; ?>

<? if ($isAllowed): ?>
    <section class="submit_form">
        <? if ($isSavedModel === true): ?>
            <?= Html::tag('div', t('app', 'The data have saved successfully'), ['style' => 'color: green']); ?>
        <? endif; ?>
        <?= Html::submitInput(t('app', 'Save')) ?>
    </section>
<? endif; ?>

<?php ActiveForm::end(); ?>
<? Pjax::end() ?>