<?php

use frontend\modules\tenant\models\clientStatusEvent\ClientStatusEventModel;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $form ActiveForm
 * @var $group string
 * @var $model ClientStatusEventModel
 * @var $events array
 * @var $noticeMap array
 */
?>

<? foreach ($events as $currentEvents): ?>
    <?php
        $eventKey  = key($currentEvents);
        $eventName = reset($currentEvents);
    ?>
    <? $attribute = "events[{$group}][{$eventKey}]"; ?>
    <section class="row">
        <?= $form->field($model, 'events')->begin(); ?>

        <div class="row_label"><?= Html::label($eventName) ?></div>
        <div class="row_input">
            <div class="select_checkbox">
                <a class="a_sc" rel="<?= t('status_event', 'Nothing') ?>">
                    <?= t('status_event', 'Nothing') ?>
                </a>
                <div class="b_sc">
                    <?= Html::activeCheckboxList($model, "events[$group][$eventKey]", $noticeMap , [
                        'tag'  => 'ul',
                        'item' => function ($index, $label, $name, $checked, $value) {
                            return Html::tag('li', Html::checkbox($name, $checked, [
                                'value' => $value,
                                'label' => Html::encode($label),
                            ]));
                        },
                    ]) ?>
                </div>
            </div>
            <?= Html::error($model, 'events',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($model, 'cityIds')->end(); ?>
    </section>

<?endforeach; ?>