<?php

use yii\widgets\ActiveForm;
use frontend\modules\tenant\models\clientStatusEvent\ClientStatusEventModel;
use yii\helpers\Html;
use frontend\modules\tenant\models\clientStatusEvent\ClientStatusEventHelper;

/**
 * @var $this yii\web\View
 * @var $form ActiveForm
 * @var $group string
 * @var $model ClientStatusEventModel
 * @var $events array
 * @var $noticeMap array
 */
?>
<div class="non-editable">
    <? foreach ($events as $eventKey => $eventName): ?>
        <section class="row">
            <div class="row_label"><?= Html::label($eventName); ?></div>
            <div class="row_input">
                <div class="row_input">
                    <?= ClientStatusEventHelper::getStringNotice($model, $group, $eventKey); ?>
                </div>
            </div>
        </section>
    <? endforeach; ?>
</div>