<?php

use yii\widgets\ActiveForm;
use frontend\modules\tenant\models\clientStatusEvent\ClientStatusEventModel;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use app\modules\tenant\assets\ClientStatusEventAsset;
use frontend\modules\tenant\models\clientStatusEvent\ClientStatusEventHelper;

/**
 * @var $this yii\web\View
 * @var $isAllowed boolean
 * @var $model ClientStatusEventModel
 * @var $userCityMap array
 * @var $userPositionMap array
 * @var $groupMap array
 * @var $noticeMap array
 * @var $eventMap array
 * @var $cityId integer
 * @var $positionId integer
 */

$asset       = ClientStatusEventAsset::register($this);
$this->title = t('setting', 'Client notification');
?>

<?= $this->render('@app/modules/tenant/views/setting/_city_list',
    [
        'setting_city_id' => $cityId,
        'user_city_list'  => $userCityMap,
        'action'          => [
            'city_id'     => $cityId,
            'position_id' => $positionId,
        ],
    ]);
?>

<?= Html::hiddenInput('cityId', $cityId, ['class' => 'js-city-id']); ?>

<section class="row">
    <div class="row_input row_low ">
        <?= Html::dropDownList('position_id', $positionId, $userPositionMap,
            ['class' => 'default_select', 'id' => 'client_status_event_position_id']); ?>
    </div>
</section>

<? Pjax::begin(['id' => 'client_status_event']); ?>

<? $form = ActiveForm::begin([
    'id'                     => 'client-status-event-form',
    'enableClientValidation' => false,
    'validateOnSubmit'       => false,
]) ?>

<? foreach ($groupMap as $groupKey => $groupName): ?>
    <?= Html::tag('h2', Html::encode($groupName)); ?>
    <?= $this->render($isAllowed ? '_forms_group' : '_forms_group_read', [
        'form'      => $form,
        'model'     => $model,
        'group'     => $groupKey,
        'events'    => ArrayHelper::getValue($eventMap, $groupKey),
        'noticeMap' => ClientStatusEventHelper::getNoticeMapByGroup($groupKey),
    ]) ?>
<? endforeach; ?>

<? if ($isAllowed) : ?>
    <section class="submit_form">
        <?= Html::submitInput(t('app', 'Save')) ?>
    </section>
<? endif; ?>

<? ActiveForm::end(); ?>
<? Pjax::end() ?>
