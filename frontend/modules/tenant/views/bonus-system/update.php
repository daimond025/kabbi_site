<?php

use frontend\modules\bonus\models\TenantHasBonusSystem;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model TenantHasBonusSystem */
/* @var $bonusSystems array */
/* @var $isAllowed bool */

use frontend\modules\tenant\assets\BonusSystemAsset;

BonusSystemAsset::register($this);

$this->title = t('setting', 'Bonus system');
?>

    <h1><?= Html::encode($this->title) ?></h1>

<? if (!empty($model)) {
    $form = $isAllowed ? '_form' : '_formRead';
    echo $this->render($form, compact('model', 'bonusSystems'));
} ?>