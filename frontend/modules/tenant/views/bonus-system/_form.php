<?php

use frontend\modules\bonus\components\BonusSystemService;
use frontend\modules\bonus\models\TenantHasBonusSystem;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $model TenantHasBonusSystem */
/* @var $bonusSystems array */

?>

<?
$form = ActiveForm::begin([
    'errorCssClass'          => 'input_error',
    'successCssClass'        => 'input_success',
    'enableClientValidation' => false,
    'enableAjaxValidation'   => false,
]);
?>
    <div class="js-bonus-system-form">
        <section class="row js-bonus-system">
            <?= $form->field($model, 'bonus_system_id')->begin(); ?>
            <div class="row_label">
                <?= Html::activeLabel($model, 'bonus_system'); ?>
            </div>
            <div class="row_input">
                <?= Html::activeDropDownList($model, 'bonus_system_id', $bonusSystems, [
                    'class' => 'default_select',
                    'data'  => [
                        'gootax-id' => BonusSystemService::BONUS_SYSTEM_ID_GOOTAX,
                    ],
                ]); ?>
            </div>
            <?= $form->field($model, 'typeId')->end(); ?>
        </section>

        <section class="row" data-scenario="<?= $model::SCENARIO_UDS_GAME ?>">
            <?= $form->field($model, 'api_key')->begin(); ?>
            <div class="row_label">
                <?= Html::activeLabel($model, 'api_key'); ?>
            </div>
            <div class="row_input">
                <?= Html::activeTextInput($model, 'api_key'); ?>
            </div>
            <?= $form->field($model, 'api_key')->end(); ?>
        </section>

        <section class="submit_form">
            <?= Html::submitInput(Yii::t('app', 'Save')) ?>
        </section>
    </div>
<? ActiveForm::end(); ?>