<?php

use frontend\modules\bonus\components\BonusSystemService;
use frontend\modules\bonus\models\TenantHasBonusSystem;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $model TenantHasBonusSystem */
/* @var $bonusSystems array */

?>

<div class="js-bonus-system-form">
    <section class="row js-bonus-system">
        <div class="row_label">
            <?= Html::activeLabel($model, 'bonus_system'); ?>
        </div>
        <div class="row_input">
            <?= Html::activeDropDownList($model, 'bonus_system_id', $bonusSystems, [
                'class'    => 'default_select',
                'data'     => [
                    'gootax-id' => BonusSystemService::BONUS_SYSTEM_ID_GOOTAX,
                ],
                'disabled' => true,
            ]); ?>
        </div>
    </section>

    <section class="row" data-scenario="<?= $model::SCENARIO_UDS_GAME ?>">
        <div class="row_label">
            <?= Html::activeLabel($model, 'api_key'); ?>
        </div>
        <div class="row_input">
            <?= Html::encode($model->api_key) ?>
        </div>
    </section>
</div>