<?php

use yii\helpers\Html;

/**
 * @var \yii\web\View                                    $this
 * @var \frontend\modules\tenant\forms\EmailSettingModel $formModel
 * @var boolean                                          $isAllowWrite
 * @var integer                                          $cityId
 * @var integer[]                                        $cityMap
 */

$this->title = t('setting', 'Email');
echo Html::tag('h1', Html::encode($this->title));
echo $this->render('@app/modules/tenant/views/setting/_city_list',
    [
        'setting_city_id' => $cityId,
        'user_city_list'  => $cityMap,
        'action'          => [
            'city_id' => $cityId,
        ],
    ]);
echo $this->render($isAllowWrite ? '_form' : '_form_read', compact('formModel'));
