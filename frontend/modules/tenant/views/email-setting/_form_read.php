<?php

use frontend\modules\tenant\helpers\TenantEmailHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @var \yii\web\View                                            $this
 * @var \frontend\modules\tenant\forms\EmailSettingModel         $formModel
 */

?>

<section class="row">
    <div class="row_label"><?= Html::activeLabel($formModel, 'activate') ?></div>
    <div class="row_input">
        <div class="row_input">
            <?= Html::encode(ArrayHelper::getValue(TenantEmailHelper::getActivateMap(), $formModel->activate)) ?>
        </div>
    </div>
</section>

<? if ($formModel->isActivated): ?>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($formModel, 'provider') ?></div>
        <div class="row_input">
            <div class="row_input">
                <?= Html::encode(ArrayHelper::getValue(TenantEmailHelper::getProviderNames(), $formModel->provider)) ?>
            </div>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($formModel, 'senderName') ?></div>
        <div class="row_input">
            <div class="row_input">
                <?= Html::encode($formModel->senderName) ?>
            </div>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($formModel, 'senderEmail') ?></div>
        <div class="row_input">
            <div class="row_input">
                <?= Html::encode($formModel->senderEmail) ?>
            </div>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($formModel, 'senderPassword') ?></div>
        <div class="row_input">
            <div class="row_input">
                <?= Html::encode($formModel->senderPassword) ?>
            </div>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($formModel, 'template') ?></div>
        <div class="row_input">
            <div class="row_input">
                <?= Html::encode($formModel->template) ?>
            </div>
        </div>
    </section>

    <?= $this->render('_params') ?>

<? endif; ?>
