<?php

use frontend\modules\tenant\assets\EmailSettingAsset;
use frontend\modules\tenant\helpers\TenantEmailHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var \yii\web\View                                            $this
 * @var \frontend\modules\tenant\forms\EmailSettingModel         $formModel
 */

EmailSettingAsset::register($this);

$activeForm = ActiveForm::begin([
    'errorCssClass'          => 'input_error',
    'successCssClass'        => 'input_success',
    'enableClientValidation' => true,
]);
?>

<section class="row">
    <?= $activeForm->field($formModel, 'activate')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($formModel, 'activate') ?></div>
    <div class="row_input">
        <?= Html::activeDropDownList($formModel, 'activate', TenantEmailHelper::getActivateMap(), [
            'class' => 'default_select js-email-setting-activate',
        ]) ?>
    </div>
    <?= $activeForm->field($formModel, 'activate')->end(); ?>
</section>

<section class="row js-show-is-activated" style="display: none">
    <?= $activeForm->field($formModel, 'provider')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($formModel, 'provider') ?></div>
    <div class="row_input">
        <?= Html::activeDropDownList($formModel, 'provider', TenantEmailHelper::getProviderNames(), [
            'class' => 'default_select js-email-setting-activate',
        ]) ?>
    </div>
    <?= $activeForm->field($formModel, 'provider')->end(); ?>
</section>

<section class="row js-show-is-activated" style="display: none">
    <?= $activeForm->field($formModel, 'senderName')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($formModel, 'senderName') ?></div>
    <div class="row_input">
        <?= Html::activeTextInput($formModel, 'senderName') ?>
    </div>
    <?= $activeForm->field($formModel, 'senderName')->end(); ?>
</section>

<section class="row js-show-is-activated" style="display: none">
    <?= $activeForm->field($formModel, 'senderEmail')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($formModel, 'senderEmail') ?></div>
    <div class="row_input">
        <?= Html::activeTextInput($formModel, 'senderEmail') ?>
    </div>
    <?= $activeForm->field($formModel, 'senderEmail')->end(); ?>
</section>

<section class="row js-show-is-activated" style="display: none">
    <?= $activeForm->field($formModel, 'senderPassword')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($formModel, 'senderPassword') ?></div>
    <div class="row_input">
        <?= Html::activeTextInput($formModel, 'senderPassword') ?>
    </div>
    <?= $activeForm->field($formModel, 'senderPassword')->end(); ?>
</section>

<section class="row js-show-is-activated" style="display: none">
    <?= $activeForm->field($formModel, 'template')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($formModel, 'template') ?></div>
    <div class="row_input">
        <?= Html::activeTextarea($formModel, 'template', ['rows' => '6']) ?>
    </div>
    <?= $activeForm->field($formModel, 'template')->end(); ?>
</section>

<?= $this->render('_params'); ?>

<section class="submit_form">
    <?= Html::submitInput(t('app', 'Save')) ?>
</section>


<?php ActiveForm::end() ?>
