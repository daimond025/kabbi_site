<?php

use yii\helpers\Html;
use frontend\modules\tenant\helpers\TenantEmailHelper;
/**
 * @var \yii\web\View $this
 * @var string[]      $paramsMap
 */
$params = [];
foreach (TenantEmailHelper::getParamsMap() as $key => $value) {
    $params[] = Html::encode($key) . ' - ' . Html::encode($value);
}
?>

<section class="row js-show-is-activated" style="display: none">
    <div class="row_label"></div>
    <div class="row_input">
        <?= implode('<br>', $params) ?>
    </div>
</section>