<?php
/* @var $this yii\web\View */

/* @var $model \app\modules\tenant\models\UserPositionForm */

use app\modules\tenant\assets\PositionAsset;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

PositionAsset::register($this);
?>
<? if ($model->canEdit()): ?>
    <?php $form = ActiveForm::begin([
        'id'                     => 'update-user-position',
        'errorCssClass'          => 'input_error',
        'successCssClass'        => 'input_success',
        'enableClientValidation' => true,
        'options'                => ['enctype' => 'multipart/form-data'],
    ]); ?>

    <section class="row">
        <div class="row_label"><label><?= t('user', 'Position') ?></label></div>
        <div class="row_input special_jobs">
            <div><?= Html::activeDropDownList($model, 'position_id', $model->position_list, [
                    'data-placeholder' => Html::encode(is_null($model->position_id) ? current($model->position_list) : $model->position_list[$model->position_id]),
                    'class'            => 'default_select',
                    'id'               => 'user_position_list',
                ]) ?></div>
            <a class="load_defaults_for_job"><?= t('user', 'Return the default settings for the position') ?></a>
        </div>
    </section>
    <?
    foreach ($model->permission_list as $name => $permission):
        if (strpos($name, 'read') !== false || in_array($name, ['streets', 'publicPlace'])) {
            continue;
        }
        ?>
        <section class="row">
            <div class="row_label"><label><?= Html::encode(t('auth_item', $permission->description)); ?></label></div>
            <div class="row_input">
                <select data-type="<?= $name ?>" name="<?= $model->formName() ?>[rights][<?= $name ?>]"
                        class="default_select"
                        data-placeholder="<? t('rights', 'Read') ?>">
                    <? foreach ($model->rights_map as $key => $value):
                        $selected = '';
                        if ((!empty($model->permissions) && $model->getUserRightsByPermission($name) == $key)
                            || (!empty ($model->defaultRights) && $key == $model->defaultRights[$name])
                        ) {
                            $selected = 'selected="selected"';
                        }

                        if ($key == 'write') {
                            $class = ' opt_green ';
                        } elseif ($key == 'off') {
                            $class = ' opt_red ';
                        } else {
                            $class = '';
                        }
                        ?>
                        <option <?= $selected ?> value="<?= $key ?>"
                                                 class="<?= $class ?>"><?= Html::encode($value); ?></option>
                    <? endforeach ?>
                </select>
            </div>
        </section>
    <? endforeach ?>
    <section class="submit_form">
        <?= Html::submitInput(t('app', 'Save')) ?>
    </section>
    <?php ActiveForm::end(); ?>
<? else: ?>
    <section class="row">
        <div class="row_label"><label><?= t('user', 'Position') ?>:</label></div>
        <div class="row_input">
            <?= isset($model->position_list[$model->position_id]) ? Html::encode($model->position_list[$model->position_id]) : t('user',
                'Not determined') ?>
        </div>
    </section>
<? endif ?>
