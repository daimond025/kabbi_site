<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = $tenant->company_name ? t('app', ' {name}', ['name' => $tenant->company_name],
    $model->lang) : t('app', 'Login', [], $model->lang);

\app\modules\tenant\assets\UserAsset::register($this);
?>
<div class="login_form">
<?php
$form = ActiveForm::begin([
    'id' => 'login-form',
    'errorCssClass' => 'input_error',
    'successCssClass' => 'input_success',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'validateOnSubmit' => true,
]);
?>

    <div class="login">
        <?= $this->render('_login_header', compact('tenant')) ?>
        <? if (!empty($info)): ?>
            <div class="already-confirmed">
                <?= Html::encode($info); ?>
            </div>
        <? endif; ?>
        <section>
            <?= $form->field($model, 'email')->begin(); ?>
            <?= Html::activeTextInput($model, 'email', [
                'placeholder' => t('app', 'Email', [], $model->lang),
                'class' => 'form-group field-loginform-email required',
            ]); ?>
            <?= Html::error($model, 'email', ['tag' => 'p', 'class' => 'help-block help-block-error']); ?>
            <?= $form->field($model, 'email')->end(); ?>
            <?= $form->field($model, 'password')->begin(); ?>
            <?= Html::activePasswordInput($model, 'password', [
                'placeholder' => t('app', 'Password', [], $model->lang),
                'class' => 'form-group field-loginform-password required',
            ]); ?>
            <?= Html::error($model, 'password', ['tag' => 'p', 'class' => 'help-block help-block-error']); ?>
            <?= $form->field($model, 'password')->end(); ?>
            <div class="cols">
                <a class="change_tab"><?= t('app', 'Reset password', [], $model->lang) ?></a>
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
            </div>

            <div style="text-align: center; margin: 0;">
                <input class="button" type="submit" value="<?= t('app', 'Login', [], $model->lang) ?>">
            </div>
        </section>
        <?php ActiveForm::end(); ?>

    </div>
    <div class="restore_pass" style="display: none;">
        <?php
        $form = ActiveForm::begin([
            'id' => 'second-form',
            'errorCssClass' => 'input_error',
            'successCssClass' => 'input_success',
            'enableAjaxValidation' => false,
            'enableClientValidation' => false,
            'validatingCssClass' => 'input_success',
            'validateOnSubmit' => false,
            'validateOnChange' => true,
            'validateOnBlur' => true,
        ]);
        ?>
        <?= $this->render('_login_header', compact('tenant')) ?>
        <section>
            <label class="request-success" id="messageWasSent"></label>
            <?= $form->field($resetPwdModel, 'email')->begin(); ?>
            <?= Html::activeTextInput($resetPwdModel, 'email', [
                'placeholder' => t('app', 'Email', [], $model->lang),
                'class' => 'form-group field-passwordresetrequestform-email required',
            ]); ?>
            <?= Html::error($resetPwdModel, 'email',
                ['tag' => 'p', 'class' => 'help-block help-block-error', 'id' => 'error']); ?>
            <?= $form->field($resetPwdModel, 'email')->end(); ?>
            <div class="form-group field-passwordresetrequestform-password">
                <input disabled type="password" placeholder="<?= t('app', 'Password', [], $model->lang) ?>">
                <p class="help-block help-block-error"></p>
            </div>
            <div class="form-group field-loginform-rememberme" style="display: none;">
                <div class="checkbox">
                    <label>
                        <input type="checkbox"><?= t('app', 'Remember Me', [], $model->lang); ?></label>
                    <p class="help-block help-block-error"></p>
                </div>
            </div>
            <div class="cols">
                <a class="change_tab"><?= t('app', 'Password found', [], $model->lang) ?></a>
            </div>
            <div style="text-align: center; margin: 0;"><input data-lang="<?=$model->lang?>" id="passwordResetRequest" class="button" type="button"
                                                               value="<?= t('app', 'Send a password reset request', [], $model->lang) ?>"/>
            </div>
        </section>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<?php

echo Html::tag('div', Html::dropDownList('lang', $model->lang,
    ['ru' => 'Русский', 'en' => 'English', 'de' => 'Deutsch']),
    ['class' => 'lang-selector','data-url'=>"/login?lang="]);