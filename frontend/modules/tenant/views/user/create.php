<?php
/* @var $this yii\web\View */
/* @var $model app\modules\tenant\models\User */

/* @var $companies array */

use app\modules\tenant\assets\PositionAsset;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\tenant\models\UserPosition;
use yii\helpers\Url;
use app\modules\tenant\models\User;

$bundle = \app\modules\tenant\assets\TenantAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/functions.js');
$this->registerJsFile($bundle->baseUrl . '/main.js');

$this->title = Yii::t('user', 'Adding an employee');

$this->registerJs("createCheckboxStringOnReady();phone_mask_init();");
PositionAsset::register($this);
?>
    <div class="bread"><a href="<?= Url::toRoute('/tenant/user/list') ?>"><?= t('app', 'Staff') ?></a></div>
    <h1><?= Html::encode($this->title) ?></h1>

<?php
$form = ActiveForm::begin([
    'id'                     => 'add-user-form',
    'errorCssClass'          => 'input_error',
    'successCssClass'        => 'input_success',
    'enableClientValidation' => true,
]);
?>

<?= $form->field($model, 'city_list')->begin(); ?>
    <section class="row">
        <div class="row_label"><label><?= t('user', 'Working in cities') ?></label></div>
        <div class="row_input">
            <div class="grid_item">
                <div class="select_checkbox">
                    <a class="a_sc" rel="<?= t('user', 'All cities') ?>"><?= t('user', 'All cities') ?></a>
                    <div class="b_sc">
                        <ul>
                            <? foreach ($city_list as $city_id => $city_name): ?>
                                <li><label><input <? if (isset($_POST['User']['city_list']) && in_array($city_id,
                                            $_POST['User']['city_list'])): ?>checked="checked"<? endif ?>
                                                  name="User[city_list][]" type="checkbox"
                                                  value="<?= $city_id ?>"/> <?= Html::encode($city_name); ?></label>
                                </li>
                            <? endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?= $form->field($model, 'city_list')->end(); ?>

    <section class="row">
        <div class="row_label"><label
                <? if ($model->isAttributeRequired('last_name')): ?>class="required"<? endif ?>><?= Html::encode($model->getAttributeLabel('last_name')); ?></label>&nbsp;<label
                <? if ($model->isAttributeRequired('name')): ?>class="required"<? endif ?>><?= Html::encode($model->getAttributeLabel('name')); ?></label>&nbsp;<label
                <? if ($model->isAttributeRequired('second_name')): ?>class="required"<? endif ?>><?= Html::encode($model->getAttributeLabel('second_name')); ?></label>
        </div>
        <div class="row_input">
            <div class="grid_3">
                <?= $form->field($model, 'last_name')->begin(); ?>
                <div class="grid_item"><?= Html::activeTextInput($model, 'last_name') ?></div>
                <?= $form->field($model, 'last_name')->end(); ?>
                <?= $form->field($model, 'name')->begin(); ?>
                <div class="grid_item"><?= Html::activeTextInput($model, 'name') ?></div>
                <?= $form->field($model, 'name')->end(); ?>
                <?= $form->field($model, 'second_name')->begin(); ?>
                <div class="grid_item"><?= Html::activeTextInput($model, 'second_name') ?></div>
                <?= $form->field($model, 'second_name')->end(); ?>
            </div>
        </div>
    </section>

<?= $form->field($model, 'email')->begin(); ?>
    <section class="row">
        <div class="row_label"><label>E-mail</label></div>
        <div class="row_input">
            <div class="input_with_text">
                <?= Html::activeTextInput($model, 'email') ?>
                <?= Html::error($model, 'email',
                    ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
                <span><?= t('user', 'This e-mail will be sent a link to sign in') ?></span>
            </div>
        </div>
    </section>
<?= $form->field($model, 'email')->end(); ?>

<?= $form->field($model, 'phone')->begin(); ?>
    <section class="row">
        <div class="row_label"><label><?= t('user', 'Mobile phone') ?></label></div>
        <div class="row_input">
            <div class="input_with_icon">
                <div class="pt_buttons">
                    <a href="#" class="pt_call"><span></span></a>
                </div>
                <div class="input_with_text">
                    <div><?= Html::activeTextInput($model, 'phone', [
                            'class'     => 'mask_phone',
                            'data-lang' => mb_substr(app()->language, 0, 2),
                        ]) ?>
                    </div>
                    <span></span>
                </div>
            </div>
        </div>
    </section>
<?= $form->field($model, 'phone')->end(); ?>

<?= $form->field($model, 'position_id')->begin(); ?>
    <section class="row">
        <div class="row_label"><label><?= t('user', 'Position') ?></label></div>
        <div class="row_input special_jobs">
            <div><?= Html::activeDropDownList($model, 'position_id', $model->position_list, [
                    'data-placeholder' => Html::encode(is_null($model->position_id) ? current($model->position_list) : $model->position_list[$model->position_id]),
                    'class'            => 'default_select',
                    'id'               => 'user_position_list',
                ]) ?></div>
        </div>
    </section>
<?= $form->field($model, 'position_id')->end(); ?>


<? if (!app()->user->can(User::ROLE_STAFF_COMPANY)): ?>
    <?= $form->field($model, 'tenant_company_id', [
        'options' => ['style' => 'display:none;']
    ])->begin(); ?>
        <section class="row">
            <div class="row_label"><?= Html::activeLabel($model, 'tenant_company_id') ?></div>
            <div class="row_input special_jobs">
                <div>
                    <?= Html::activeDropDownList($model, 'tenant_company_id', $companies, [
                        'prompt' => '',
                        'data-placeholder' => t('tenant_company', 'Select company'),
                        'class'            => 'default_select',
                    ]) ?>
                    <?= Html::error($model, 'tenant_company_id',
                        ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
                </div>
            </div>
        </section>
    <?= $form->field($model, 'tenant_company_id')->end(); ?>
<? endif; ?>


<?= $form->field($model, 'lang')->begin(); ?>
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'lang') ?></div>
        <div class="row_input special_jobs">
            <div><?= Html::activeDropDownList($model, 'lang', $languages, [
                    'data-placeholder' => empty($model->lang) ? current($languages) : $languages[$model->position_id],
                    'class'            => 'default_select',
                ]) ?></div>
        </div>
    </section>
<?= $form->field($model, 'lang')->end(); ?>

<?= $form->field($model, 'chat_email_notify')->begin(); ?>
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'chat_email_notify') ?></div>
        <div class="row_input special_jobs">
            <div>
                <?= Html::activeCheckbox($model, 'chat_email_notify', ['label' => null]) ?>
            </div>
        </div>
    </section>
<?= $form->field($model, 'chat_email_notify')->end(); ?>

<? if (user()->getUserRole() != User::ROLE_STAFF): ?>
    <h2><a class="spoler" data-spolerid="id01"><?= t('user', 'Rights and access') ?></a></h2>
    <section class="spoler_content" id="id01" style="display: none;">
        <?
        $auth = Yii::$app->authManager;
        $permissions = $auth->getPermissions();
        $rights = app\modules\tenant\models\User::getRightsVariantsOfPermission();
        $defaultRights = \frontend\modules\tenant\models\UserDefaultRight::getDefaultRightSettingByPosition(UserPosition::POSITION_ADMINISTRATOR);

        foreach ($permissions as $name => $permission):
            if (strpos($name, 'read') !== false) {
                continue;
            }
            ?>
            <section class="row">
                <div class="row_label"><label><?= Html::encode(t('auth_item', $permission->description)); ?></label>
                </div>
                <div class="row_input">
                    <select data-type="<?= $name ?>" name="User[rights][<?= $name ?>]" class="default_select">
                        <? foreach ($rights as $key => $value):

                            $selected = '';

                            if (!empty ($defaultRights) && $key == $defaultRights[$name]) {
                                $selected = 'selected="selected"';
                            }
                            if ($key == 'write') {
                                $class = 'opt_green';
                            } elseif ($key == 'off') {
                                $class = 'opt_red';
                            } else {
                                $class = '';
                            }
                            ?>
                            <option <?= $selected ?> value="<?= $key ?>"
                                                     class="<?= $class ?>"><?= Html::encode($value); ?></option>
                        <? endforeach ?>
                    </select>
                </div>
            </section>
        <? endforeach ?>
        <section class="row">
            <div class="row_label"></div>
            <div class="row_input">
                <a class="load_defaults_for_job"><?= t('user', 'Return the default settings for the position') ?></a>
            </div>
        </section>
    </section>
<? endif ?>
    <section class="submit_form">
        <?= Html::submitInput(t('app', 'Submitting')) ?>
    </section>
<?php ActiveForm::end(); ?>