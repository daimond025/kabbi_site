<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

$lang = $model->lang;

$this->title = $tenant_name ? t('app', ' {name}', ['name' => $tenant_name], $lang) : t('app', 'Reset password', [], $lang);
?>
<div class="login_form">
    <?php $form = ActiveForm::begin([
        'id' => 'reset-password-form',
        'errorCssClass' => 'input_error',
        'fieldConfig' => [
            //'template' => '{input}{error}',
        ],
    ]); ?>
    <div class="reset-password">
        <?= $this->render('_login_header', compact('tenant')) ?>
        <section>
            <?//= $form->field($model, 'email')->begin(); ?>

                <?= $form->field($model, 'password')->passwordInput([
                    'class' => 'form-group field-loginform-password',
                    'placeholder' => t('user', 'Enter a new password', [], $lang),
                    'style' => 'width: 100%'
                ]) ?>
                <?= $form->field($model, 'password_repeat')->passwordInput([
                    'class' => 'form-group field-loginform-password',
                    'placeholder' => t('user', 'Once again', [], $lang),
                    'style' => 'width: 100%'
                ]) ?>
            <div style="text-align: center; margin: 0; padding-top: 15px;">
                <?= Html::submitButton(t('app', 'Save', [], $lang), ['class' => 'button']) ?>
            </div>
        </section>
        <?php ActiveForm::end(); ?>
    </div>
</div>
