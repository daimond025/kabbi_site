<?php
/* @var $this yii\web\View */
/* @var $model \app\modules\tenant\models\UserLoginDataForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<? if ($model->user_id != user()->id): ?>
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'email') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($model->email) ? Html::encode($model->email) : '' ?></div>
        </div>
    </section>
<? else: ?>
    <?php $form = ActiveForm::begin([
        'id'                     => 'update-login-data',
        'errorCssClass'          => 'input_error',
        'successCssClass'        => 'input_success',
        'enableClientValidation' => true,
        'enableAjaxValidation'   => true,
        'validationUrl'          => ['/tenant/user/check-login-data', 'id' => $model->user_id],
        'fieldConfig'            => [
            'errorOptions' => ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red'],
            'template'     => "{input}{error}",
        ],
    ]); ?>
    <section class="row">
        <?= $form->field($model, 'email')->begin(); ?>
        <div class="row_label"><?= Html::activeLabel($model, 'email') ?></div>
        <div class="row_input">
            <?= $form->field($model, 'email')->textInput() ?>
        </div>
    </section>
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'password',
                ['label' => t('user', 'Change password')]) ?></div>
        <div class="row_input">
            <?= $form->field($model, 'password', ['enableAjaxValidation' => false])->passwordInput() ?>
        </div>
    </section>
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'password_repeat') ?></div>
        <div class="row_input">
            <?= $form->field($model, 'password_repeat', ['enableAjaxValidation' => false])->passwordInput() ?>
        </div>
    </section>

    <section class="submit_form">
        <?= Html::submitInput(t('app', 'Save')) ?>
    </section>
    <?php ActiveForm::end(); ?>
<? endif ?>