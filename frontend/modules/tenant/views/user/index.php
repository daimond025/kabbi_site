<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var string $pjaxId */
/* @var $this yii\web\View */
/* @var array $cityList */
/* @var array $positionList */
/* @var array $tenantCompaniesList */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \app\modules\tenant\models\UserSearch */

$bundle = \app\modules\tenant\assets\TenantAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/functions.js');
$this->registerJsFile($bundle->baseUrl . '/main.js');
$this->title = t('app', 'Staff');
if (app()->user->can('users')) {
    echo Html::a(t('user', 'Create user'), ['create'],
        ['class' => 'button', 'style' => 'float: right; position: relative; top: 5px;']);
}

?>
<h1><?= Html::encode($this->title) ?></h1>
<section class="main_tabs">
    <div class="tabs_links">
        <ul>
            <li><a href="#active" class="t01"><?= t('user', 'Active') ?></a></li>
            <li><a href="#invited" class="t02" data-href="<?= Url::to('invited') ?>"><?= t('user',
                        'Sent invitations') ?></a>
            <li><a href="#blocked" class="t03" data-href="<?= Url::to('blocked') ?>"><?= t('user', 'Blocked') ?></a>
            </li>
        </ul>
    </div>
    <div class="tabs_content">
        <div id="t01" data-view="index">
            <?= $this->render(
                '_grid',
                compact(
                    'searchModel',
                    'dataProvider',
                    'cityList',
                    'pjaxId',
                    'positionList',
                    'type',
                    'tenantCompaniesList'
                )) ?>
        </div>
        <div id="t02" data-view="invited">
        </div>
        <div id="t03" data-view="blocked">
        </div>
    </div>
</section>
