<?php
/* @var $this yii\web\View */
/* @var $model app\modules\tenant\models\User */
/* @var string $curUserRole */
/* @var array $city_list */
/* @var array $languages */
/* @var bool $canEdit */

use app\modules\tenant\assets\UserAsset;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Update');

UserAsset::register($this);

?>
<div class="bread"><a href="<?= Url::toRoute('/tenant/user/list') ?>"><?= t('app', 'Staff') ?></a></div>
<div class="pt_buttons" style="position: relative; top: 10px;">
    <?if($intersectedCityId):?>
        <a href="#" class="pt_message" data-receivertype="user" data-receiverid="<?= $model->user_id ?>"
           data-cityid="<?= $intersectedCityId?>"><span></span></a>
    <?endif;?>
</div>
<h1><?= Html::encode($model->fullName); ?><span class="pt_<?= Html::encode($model->online); ?>line"></span></h1>

<section class="main_tabs">
    <div class="tabs_links">
        <ul>
            <li><a href="#profile" class="t01"><?= t('app', 'Profile') ?></a></li>
            <li><a href="#email_and_password" data-href="<?= Url::to(['change-login-data', 'id' => $model->user_id]) ?>"
                   class="t02"><?= t('user', 'E-mail and password') ?></a></li>
            <li><a href="#rights_and_access"
                   data-href="<?= Url::to(['change-user-position', 'id' => $model->user_id]) ?>"
                   class="t03"><?= t('user', 'Rights and access') ?></a></li>
        </ul>
    </div>
    <div class="tabs_content">
        <div id="t01">
            <?= $this->render($canEdit ? '_profileForm' : '_profileRead', compact('model', 'city_list', 'curUserRole', 'languages')) ?>
        </div>
        <div id="t02">
        </div>
        <div id="t03">
        </div>
    </div>
</section>