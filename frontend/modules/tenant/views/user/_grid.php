<?php

/* @var string $pjaxId */
/* @var array $cityList */
/* @var array $positionList */
/* @var array $tenantCompaniesList */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \app\modules\tenant\models\UserSearch */

use frontend\widgets\filter\Filter;
use frontend\widgets\LinkPager;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Html;
use app\modules\tenant\models\User;

$workerDetailViewUrl = getValue($workerDetailViewUrl, '/employee/worker/update');


$filter = Filter::begin([
    'id'     => 'filter_' . $pjaxId,
    'pjaxId' => $pjaxId,
    'model'  => $searchModel,
]);
echo $filter->checkboxList('city_id', $cityList, ['id' => $pjaxId . '-city_id']);
echo $filter->checkboxList('positionList', $positionList, ['id' => $pjaxId . '-positionList']);

if (!app()->user->can(User::ROLE_STAFF_COMPANY)) {
    echo $filter->checkboxList('tenantCompanyIds', $tenantCompaniesList, ['id' => $pjaxId . '-tenantCompanyIds']);
}

echo $filter->input('stringSearch', ['id' => $pjaxId . '-stringSearch']);
Filter::end();

Pjax::begin(['id' => $pjaxId]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'       => "{items}\n{pager}",
    'tableOptions' => [
        'class' => 'people_table',
    ],
    'pager'        => [
        'class'         => LinkPager::className(),
        'prevPageLabel' => t('app', 'Prev'),
        'nextPageLabel' => t('app', 'Next'),
    ],
    'columns'      => [
        [
            'attribute' => 'fullName',
            'content' => function ($model) {
                $innerHtml = Html::tag('span', $model->logo, ['class' => 'pt_photo']);
                $innerHtml .= Html::tag('span', Html::encode($model->fullName), ['class' => 'pt_fios']);
                $innerHtml .= ' ' . Html::tag('span', '', ['class' => 'pt_' . ($model->isOnline ? 'online' : 'offline')]);
                return Html::a($innerHtml, ['update', 'id' => $model->user_id], ['data-pjax' => 0]);
            },
            'headerOptions'  => ['class' => 'pt_fio'],
            'contentOptions' => ['class' => 'pt_fio'],
        ],
        [
            'attribute' => 'position',
            'content' => function ($model) use ($positionList) {
                return $positionList[$model->position_id];
            },
            'headerOptions'  => ['class' => 'pt_job'],
            'contentOptions' => ['class' => 'pt_job'],
        ],
        [
            'attribute' => 'city_list',
            'content' => function ($model) use ($cityList, $type) {
                $city_list = \yii\helpers\ArrayHelper::getColumn($model->userHasCities, 'city_id');
                $city_list = array_intersect($city_list, array_keys($cityList));
                $city_list = array_map(function ($item) use ($cityList){
                    return $cityList[$item];
                }, $city_list);
                $innerHtml = implode(', ', $city_list);

                switch ($type) {

                    case 'invited':
                        if (app()->user->can('users')) {
                            $send = Html::a('<i></i>' . Html::tag('span', t('user', 'Send again')),
                                ['/tenant/user/send-invite'],
                                ['class' => 'send_invite_again again']
                            );
                            $cancel = Html::a(t('user', 'Cancel invitation'),
                                ['/tenant/user/cancel-invite'],
                                ['class' => 'link_red cancel_invite', 'style' => 'margin-left: 20px;']
                            );
                            $innerHtml .= Html::tag('div',
                                $send . ' ' . $cancel,
                                ['class' =>'invites_control', 'style' => 'position: static; width: auto;',
                                    'data-user' => $model->user_id]);
                        }
                        break;

                    case 'blocked':
                        if (app()->user->can('users')) {
                            $innerHtml .= Html::tag('div',
                                Html::a(
                                    Html::tag('span', t('user', 'Unlock')),
                                    ['/tenant/user/unblock'], ['class'=>'remove_from_blacklist']),
                                ['class' =>'invites_control', 'style' => 'position: static; width: auto;', 
                                    'data-user' => $model->user_id]);
                        }
                        break;
                }
                return $innerHtml;
            },
            'headerOptions'  => ['class' => 'pt_city'],
//            'contentOptions' => ['class' => 'pt_city'],
        ]
    ],
]);

Pjax::end();