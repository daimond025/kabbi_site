<?php

use yii\helpers\Html;
use yii\helpers\Url;


?>

<header>
    <? if ($tenant->isFileExists($tenant->logo)): ?>
        <div class="logotype">
            <?= Html::img(Url::to(['/file/show-external-file', 'filename' => $tenant->getThumbPrefix() . $tenant->logo, 'id' => $tenant->tenant_id])) ?>
        </div>
    <? endif ?>
    <!--  <h1><?= Html::encode($this->title); ?></h1> -->
</header>