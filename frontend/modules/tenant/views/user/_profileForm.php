<?php
/* @var $this yii\web\View */
/* @var $model app\modules\tenant\models\User */
/* @var string $curUserRole */
/* @var array $city_list */
/* @var array $languages */

use app\modules\tenant\models\User;
use common\helpers\DateTimeHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    'id'                     => 'update-user-form',
    'errorCssClass'          => 'input_error',
    'successCssClass'        => 'input_success',
    'enableClientValidation' => true,
    'options'                => ['enctype' => 'multipart/form-data'],
]); ?>
<section class="row">
    <div class="row_label"><label><?= t('user', 'Working in cities') ?></label></div>
    <div class="row_input">
        <div class="grid_item">
            <div class="select_checkbox">
                <?
                if ($curUserRole == User::ROLE_TOP_MANAGER || ($model->getUserRole() == User::ROLE_STAFF && $curUserRole == User::ROLE_BRANCH_DIRECTOR)):?>
                    <a class="a_sc" rel="<?= t('user', 'All cities') ?>"><?= t('user', 'All cities') ?></a>
                    <div class="b_sc">
                        <ul>
                            <? foreach ($city_list as $city_id => $city_name): ?>
                                <?
                                $checked = null;
                                if (isset($_POST['User']['city_list'])
                                    && in_array($city_id, $_POST['User']['city_list'])
                                    || in_array($city_id, $model->city_list)
                                ) {
                                    $checked = 'checked="checked"';
                                }
                                ?>
                                <li><label><input <?= $checked ?> name="User[city_list][]" type="checkbox"
                                                                  value="<?= $city_id ?>"/> <?= Html::encode($city_name); ?>
                                    </label></li>
                            <? endforeach; ?>
                        </ul>
                    </div>
                <? else: ?>
                    <div class="row_input">
                        <?
                        $cities = '';
                        foreach ($city_list as $city_id => $city_name) {
                            if (in_array($city_id, $model->city_list)) {
                                $cities .= $city_name . ', ';
                            }
                        }
                        ?>
                        <?= Html::encode(substr($cities, 0, -2)); ?>
                    </div>
                <? endif ?>
            </div>
        </div>
    </div>
    <input id="city_data" type="hidden" name="User[city_data]" value="0">
</section>
<!--Фото-->
<?= \frontend\widgets\file\Logo::widget(['form' => $form, 'model' => $model, 'attribute' => 'photo']) ?>
<section class="row">
    <div class="row_label"><label><?= t('user', 'Last name Name Second name') ?></label></div>
    <div class="row_input">
        <div class="grid_3">
            <?= $form->field($model, 'last_name')->begin(); ?>
            <div class="grid_item"><?= Html::activeTextInput($model, 'last_name') ?></div>
            <?= $form->field($model, 'last_name')->end(); ?>
            <?= $form->field($model, 'name')->begin(); ?>
            <div class="grid_item"><?= Html::activeTextInput($model, 'name') ?></div>
            <?= $form->field($model, 'name')->end(); ?>
            <?= $form->field($model, 'second_name')->begin(); ?>
            <div class="grid_item"><?= Html::activeTextInput($model, 'second_name') ?></div>
            <?= $form->field($model, 'second_name')->end(); ?>
        </div>
    </div>
</section>
<section class="row">
    <div class="row_label"><?= Html::activeLabel($model, 'birth') ?></div>
    <div class="row_input">
        <div class="grid_3">
            <?= $form->field($model, 'birth_day')->begin(); ?>
            <div class="grid_item">
                <?
                $birthDay = [0 => t('app', 'Not selected')] + DateTimeHelper::getMonthDayNumbers();
                echo Html::activeDropDownList($model, 'birth_day', $birthDay, [
                    'data-placeholder' => !empty($model->birth_day) ? Html::encode($model->birth_day) : t('user',
                        'Day'),
                    'class'            => 'default_select',
                ]);
                ?>
            </div>
            <?= $form->field($model, 'birth_day')->end(); ?>
            <?= $form->field($model, 'birth_month')->begin(); ?>
            <div class="grid_item">
                <?
                $monthList = DateTimeHelper::getMonthList();
                echo Html::activeDropDownList($model, 'birth_month', $monthList, [
                    'data-placeholder' => !empty($model->birth_month) ? Html::encode($monthList[$model->birth_month]) : t('user',
                        'Month'),
                    'class'            => 'default_select',
                ]);
                ?>
            </div>
            <?= $form->field($model, 'birth_month')->end(); ?>
            <?= $form->field($model, 'birth_year')->begin(); ?>
            <div class="grid_item">
                <?
                echo Html::activeDropDownList($model, 'birth_year', DateTimeHelper::getOldYearList(), [
                    'data-placeholder' => !empty($model->birth_year) ? Html::encode($model->birth_year) : t('user',
                        'Year'),
                    'class'            => 'default_select',
                ]);
                ?>
            </div>
            <?= $form->field($model, 'birth_year')->end(); ?>
        </div>
    </div>
</section>
<section class="row">
    <?= $form->field($model, 'phone')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($model, 'phone') ?></div>
    <div class="row_input">
        <div class="input_with_icon">
            <a href="tel:<?= ($model->phone && !(app()->user->getId() == $model->user_id)) ? '+'
                . $model->phone : '' ?>" class="call_now"><span></span></a>
            <div class="input_with_text">
                <?= Html::activeTextInput($model, 'phone', [
                    'class'     => 'mask_phone',
                    'data-lang' => mb_substr(app()->language, 0, 2),
                ]); ?>
                <span></span>
            </div>
        </div>
    </div>
    <?= $form->field($model, 'phone')->end(); ?>
</section>
<section class="row">
    <?= $form->field($model, 'address')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($model, 'address') ?></div>
    <div class="row_input">
        <?= Html::activeTextInput($model, 'address') ?>
    </div>
    <?= $form->field($model, 'address')->end(); ?>
</section>
<?= $form->field($model, 'lang')->begin(); ?>
<section class="row">
    <div class="row_label"><?= Html::activeLabel($model, 'lang') ?></div>
    <div class="row_input special_jobs">
        <div>
            <?= Html::activeDropDownList($model, 'lang', $languages, [
                'data-placeholder' => empty($model->lang) ? current($languages) : $languages[$model->lang],
                'class'            => 'default_select',
            ]) ?>
        </div>
    </div>
</section>
<?= $form->field($model, 'lang')->end(); ?>
<?= $form->field($model, 'chat_email_notify')->begin(); ?>
<section class="row">
    <div class="row_label"><?= Html::activeLabel($model, 'chat_email_notify') ?></div>
    <div class="row_input special_jobs">
        <div>
            <? if ($curUserRole == User::ROLE_TOP_MANAGER || ($model->getUserRole() == User::ROLE_STAFF && $curUserRole == User::ROLE_BRANCH_DIRECTOR)): ?>
                <?= Html::activeCheckbox($model, 'chat_email_notify', ['label' => null]) ?>
            <? else: ?>
                <?= Html::encode($model->chat_email_notify); ?>
            <? endif; ?>
        </div>
    </div>
</section>
<?= $form->field($model, 'chat_email_notify')->end(); ?>

<section class="row">
    <?= $form->field($model, 'access_token')->begin(); ?>
    <div class="row_label">
        <?= Html::activeLabel($model, 'access_token') ?>
        <a data-tooltip="2" class="tooltip">?
            <span style="display: none; opacity: 0;">
                <?= Yii::t('hint', "The access key is used for quick access to the control panel without entering a login and password. <br> <br> For example, for the operator, a quick entry to the order page."); ?>
            </span>
        </a>
    </div>
    <div class="row_input">
        <?= Html::activeTextInput($model, 'access_token', ['readonly' => true]) ?>
    </div>
    <?= $form->field($model, 'access_token')->end(); ?>
</section>

<section class="submit_form">
    <div data-user="<?= $model->user_id ?>" data-red="<?= Url::to('/tenant/user/list') ?>">
        <? if (!$model->isUserInvited()):
            if ($model->user_id != user()->id):?>
                <?= $form->field($model, 'block')->begin(); ?>
                <label><?= Html::activeCheckbox($model, 'block', ['label' => null]) ?> <b><?= t('user',
                            'Block') ?></b></label>
                <?= $form->field($model, 'block')->end(); ?>
            <? endif ?>
        <? else: ?>
            <a href="<?= Url::to('/tenant/user/send-invite') ?>"
               class="send_invite_again link_green again"><i></i><b><?= t('user',
                        'Send again') ?></b></a>
            <a id="cancel_invite" href="<?= Url::to('/tenant/user/cancel-invite') ?>"
               class="link_red"><b><?= t('user', 'Cancel invitation') ?></b></a>
        <? endif ?>
    </div>
    <?= Html::submitInput(t('app', 'Save')) ?>
</section>
<?php ActiveForm::end(); ?>


