<?php
/* @var $this yii\web\View */
/* @var $model app\modules\tenant\models\User */
/* @var array $city_list */
/* @var array $languages */

use yii\helpers\Html;

?>

    <section class="row">
        <div class="row_label"><label><?= t('user', 'Working in cities') ?></label></div>
        <div class="row_input">
            <div class="grid_item">
                <div class="select_checkbox">
                    <div class="row_input">
                        <?
                        $cities = '';
                        foreach ($city_list as $city_id => $city_name) {
                            if (in_array($city_id, $model->city_list)) {
                                $cities .= $city_name . ', ';
                            }
                        }
                        ?>
                        <?= Html::encode(substr($cities, 0, -2)); ?>
                    </div>
                </div>
            </div>
        </div>
        <input id="city_data" type="hidden" name="User[city_data]" value="0">
    </section>

    <section class="row" id="logo">
        <div class="row_label"></div>
        <div class="row_input" style="background: none;">
            <div class="input_file_logo">
                <? if ($model->photo && file_exists(Yii::getAlias('@app') . '/web/' . '/upload/' . user()->tenant_id . '/' . $model->photo)): ?>
                    <a class="input_file_logo logo cboxElement"
                       href="<?= '/upload/' . user()->tenant_id . '/' . $model->photo ?>"
                       style="border: none">
                        <div class="row_input" style="background: none"><span><img
                                        src="<?= '/upload/' . user()->tenant_id . '/' . $model->photo ?>"></span>
                        </div>
                    </a>
                <? endif ?>
            </div>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><label><?= t('user', 'Last name Name Second name') ?></label></div>
        <div class="row_input">
            <div class="grid_3">
                <div class="grid_item"><?= Html::encode($model->last_name) ?></div>
                <div class="grid_item"><?= Html::encode($model->name) ?></div>
                <div class="grid_item"><?= Html::encode($model->second_name) ?></div>
            </div>
        </div>
    </section>
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'birth') ?></div>
        <div class="row_input">
            <div class="grid_3">
                <div class="grid_item">
                    <div
                            class="grid_item"><?= !empty($model->birth_day) ? Html::encode($model->birth_day) : '<div class="row_input"></div>' ?></div>
                </div>
                <div class="grid_item">
                    <div
                            class="grid_item"><?= !empty($model->birth_month) ? Html::encode($model->birth_month) : '<div class="row_input"></div>' ?></div>
                </div>
                <div class="grid_item">
                    <div
                            class="grid_item"><?= !empty($model->birth_year) ? Html::encode($model->birth_year) : '<div class="row_input"></div>' ?></div>
                </div>
            </div>
        </div>
    </section>
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'phone') ?></div>
        <div class="row_input">
            <div class="input_with_icon">
                <a href="tel:<?= ($model->phone && !(app()->user->getId() == $model->user_id)) ? '+'
                    . $model->phone : '' ?>" class="call_now"><span></span></a>
                <div class="input_with_text">
                    <div
                            class="row_input"><?= !empty($model->phone) ? Html::encode($model->phone) : '' ?></div>
                    <span></span>
                </div>
            </div>
        </div>
    </section>
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'address') ?></div>
        <div class="row_input">
            <div
                    class="row_input"><?= !empty($model->address) ? Html::encode($model->address) : '' ?></div>
        </div>
    </section>
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'lang') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($model->lang) ? Html::encode($model->lang) : '' ?></div>
        </div>
    </section>
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'chat_email_notify') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($model->chat_email_notify) ? 1 : '0' ?></div>
        </div>
    </section>
<? if (!$model->isUserInvited()):
    if ($model->user_id != user()->id):?>
        <label><?= Html::activeCheckbox($model, 'block', ['label' => null, 'disabled' => 'disabled']) ?>
            <b><?= t('user', 'Block') ?></b></label>
    <? endif ?>
<? endif ?>