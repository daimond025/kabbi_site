<?php

use frontend\modules\tenant\assets\TariffAsset;

/* @var $this \yii\web\View */
/* @var $operations \app\modules\balance\models\Operation[]|null */

TariffAsset::register($this);

?>
<div class="payment-history">
    <?= $this->render('_search') ?>
    <div class="grid">
        <?= $this->render('_grid', compact('operations')); ?>
    </div>
</div>

