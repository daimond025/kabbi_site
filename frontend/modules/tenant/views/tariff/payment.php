<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */

$this->title = t('tenant-tariff', 'Payment');

?>

<div class="bread">
    <a href="<?= Url::to(['/tenant/tariff/index']) ?>">
        <?= t('setting', 'Tariff and payment history') ?>
    </a>
</div>

<h1><?= Html::encode($this->title) ?></h1>

<p><?= t('tenant-tariff', 'Payment service is not available at this time.') ?></p>