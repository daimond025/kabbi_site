<?php

/* @var $this \yii\web\View */
/* @var $operations \app\modules\balance\models\Operation[]|null */

?>

<? if (empty($operations)): ?>
    <p><?= t('app', 'Empty') ?></p>
<? else: ?>
    <table class="people_table money_table transaction_list">
        <tbody>
        <tr>
            <th style="width: 20%"><?= t('balance', 'Operation date') ?></th>
            <th style="width: 30%;"><?= t('balance', 'Operation') ?></th>
            <th style="width: 13%; text-align: right;"><?= t('balance', 'Debet') ?></th>
            <th style="width: 13%; text-align: right;"><?= t('balance', 'Credit') ?></th>
        </tr>
        <?= $this->render('_rows', compact('operations')) ?>
        </tbody>
    </table>
<? endif; ?>
