<?php

use app\modules\tariff\models\AdditionalOption;
use frontend\modules\tenant\models\Currency;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $tariffs string */
/* @var $expiryDates integer[] */
/* @var $balanceInfo array */
/* @var $isAllowed bool */
/* @var $additionalOptions null|AdditionalOption[] */

?>

<h3>
    <span style="font-weight: normal;"><?= t('setting', 'Your tariff') ?>:</span> <?= Html::encode($tariffs) ?>
</h3>
<div style="margin-bottom: 20px;">
    <?
    $lines = [];
    if (!empty($additionalOptions)) {
        $value = t('setting', 'Additional options') . ': ';
        $options = [];
        foreach ($additionalOptions as $option) {
            $options[] = Html::tag('b', Html::encode($option));
        }
        $lines[] = $value . implode(", ", $options);
    }
    if (!empty($expiryDates)) {
        $value = t('setting', 'Paid to') . ': ';

        $dates = [];
        foreach ($expiryDates as $date) {
            $interval = date_diff(
                date_create(date('d.m.Y', $date)),
                date_create(date('d.m.Y', time()))
            );
            $days = $interval->days < 0 ? 0 : ($interval->days + 1);

            $dates[] = app()->formatter->asDate($date) . ' (' . Html::tag('b',
                    t('setting', '{daysCount, plural, one{# day left} other{# days left}}', [
                        'daysCount' => $days,
                    ])) . ')';
        }
        $lines[] = $value . implode(', ', $dates);
    }
    if (!empty($balanceInfo)) {
        $lines[] = t('balance', 'Your balance') . ': '
            . app()->formatter->asMoney(
                $balanceInfo['balance'],
                Currency::getCurrencySymbol($balanceInfo['currency_id']));
    }
    echo implode('<br>', $lines);
    ?>
</div>

<? if ($isAllowed): ?>
    <a class="button" style="display: none" href="<?= Url::to('/tenant/tariff/payment', [], true) ?>">
        <?= t('setting', 'Extend services') ?>
    </a>
<? endif; ?>