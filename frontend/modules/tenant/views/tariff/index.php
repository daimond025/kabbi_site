<?php

use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $tariff \common\modules\tenant\modules\tariff\models\Tariff */
/* @var $additionalOptions \common\modules\tenant\modules\tariff\models\TariffAdditionalOption[]|null */
/* @var $expiryDate integer */

$this->title = t('setting', 'Tariff and payment history');
?>

<h1><?= Html::encode($this->title) ?></h1>
<section class="main_tabs">
    <div class="tabs_links">
        <ul>
            <li><a href="#index" class="t01"><?= t('setting', 'Tariff') ?></a></li>
            <li>
                <a href="#history" class="t02" data-href="<?= Url::to('/tenant/tariff/payment-history') ?>">
                    <?= t('setting', 'Payment history') ?>
                </a>
            </li>
        </ul>
    </div>
    <div class="tabs_content">
        <div id="t01" data-view="index">
            <?= $this->render('_tariff', compact('tariffs', 'additionalOptions', 'expiryDates', 'balanceInfo', 'isAllowed')) ?>
        </div>
        <div id="t02" data-view="history"></div>
    </div>
</section>
