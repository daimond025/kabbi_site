<?php

use frontend\modules\tenant\models\TenantHasCityForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\city\models\Country;
use frontend\modules\tenant\models\Currency;
use yii\helpers\HtmlPurifier;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model TenantHasCityForm
/* @var $positions array */
/* @var $activePositions array */

$this->registerJs('phone_mask_init();');
if ($jsInit) {
    $this->registerJs('select_init();');
}
?>

<?php
$form = ActiveForm::begin([
    'errorCssClass'          => 'input_error',
    'successCssClass'        => 'input_success',
    'enableClientValidation' => true,
]);
?>

<? if ($model->isNewRecord): ?>
    <section class="row required">
        <div class="row_label">
            <?= Html::activeLabel($model, 'city_id'); ?>
        </div>
        <div class="row_input" id="add_city_organization">
            <select class="default_select" data-placeholder="<?= t('app', 'Choose country') ?>">
                <?
                $country = Country::getCountryList();
                foreach ($country as $value):
                    $selected = $value['country_id'] == 1 ? 'selected="selected"' : '';
                    ?>
                    <option <?= $selected ?>
                            value="<?= $value['country_id'] ?>"><?= Html::encode($value['name' . getLanguagePrefix()]); ?></option>
                <? endforeach; ?>
            </select>
            <!--селект с поиском-->
            <div class="select_checkbox s_search" style="margin-top: 10px">
                <a class="a_sc" rel="Placeholder" data-city=""><?= t('app', 'Choose city') ?></a>
                <div class="b_sc">
                    <ul>
                        <input AUTOCOMPLETE="off" id="city_search" data-only-cities="1" type="text"
                               name="<?= $model->formName() ?>[city_id]" value=""/>
                    </ul>
                </div>
            </div>
        </div>
    </section>
<? endif ?>

<section class="row">
    <?= $form->field($model, 'lang')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($model, 'lang') ?></div>
    <div class="row_input">
        <?= Html::activeDropDownList($model, 'lang', $languages, [
            'data-placeholder' => !empty($model->lang) ? $languages[$model->lang] : t('setting', 'Choose the language'),
            'class'            => 'default_select',
        ]) ?>
    </div>
    <?= $form->field($model, 'lang')->end(); ?>
</section>

<section class="row">
    <?= $form->field($model, 'currency')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($model, 'currency') ?></div>
    <div class="row_input">
        <? $arCurrencyMap = Currency::getCurrencyMap() ?>
        <?= Html::activeDropDownList($model, 'currency', $arCurrencyMap, [
            'data-placeholder' => !empty($model->currency) ? $arCurrencyMap[$model->city_id] : t('setting',
                'Choose the сurrency'),
            'class'            => 'default_select',
        ]) ?>
    </div>
    <?= $form->field($model, 'currency')->end(); ?>
</section>

<section class="row">
    <?= $form->field($model, 'phone')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($model, 'phone') ?><a data-tooltip="5" class="tooltip">?<span
                    style="display: none; opacity: 0;"><?= HtmlPurifier::process(Yii::t('hint',
                    'This phone will be displayed in the client application in order')) ?></span></a></div>
    <div class="row_input">
        <?= Html::activeTextInput($model, 'phone', [
            'type' => 'tel',
        ]) ?>
        <?= Html::tag('span', ''); ?>
    </div>
    <?= $form->field($model, 'phone')->end(); ?>
</section>

<section class="row">
    <?= $form->field($model, 'isUseSignalNewOrder')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($model, 'isUseSignalNewOrder') ?></div>
    <div class="row_input">

        <?= Html::activeDropDownList($model, 'isUseSignalNewOrder', [
                0 => t('app', 'No'), 1 => t('app', 'Yes')
        ], [
            'class'            => 'default_select',
        ]) ?>

        <?= Html::tag('span', ''); ?>
    </div>
    <?= $form->field($model, 'isUseSignalNewOrder')->end(); ?>
</section>

<section class="row">
    <?= $form->field($model, 'sort')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($model, 'sort') ?></div>
    <div class="row_input">
        <?= Html::activeTextInput($model, 'sort') ?>
        <?= Html::error($model, 'sort',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($model, 'sort')->end(); ?>
</section>

<section class="row required">
    <div class="row_label">
        <?= Html::activeLabel($model, 'active_position_ids'); ?>
    </div>
    <div class="row_input">
        <?= $this->render('_positions', [
            'positions'       => $positions,
            'activePositions' => $activePositions,
            'modelName'       => $model->formName(),
        ]) ?>
    </div>
</section>

<section class="submit_form">
    <div>
        <? if (!$model->isNewRecord): ?>
            <label><?= Html::activeCheckbox($model, 'block', ['label' => null]) ?> <b><?= t('car',
                        'Block') ?></b></label>
        <? endif ?>
    </div>
    <?= Html::submitInput(t('app', 'Save')) ?>
</section>

<?php ActiveForm::end(); ?>

