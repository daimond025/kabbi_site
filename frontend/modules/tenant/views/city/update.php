<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \common\modules\tenant\models\TenantHasCity */
/* @var $positions array */
/* @var $activePositions array */

$this->title = $model->city->{name . getLanguagePrefix()};
$form        = !app()->user->can('cities') ? '_form_read' : '_form';
?>

<div class="bread"><?= Html::a(t('app', 'Cities'), '/tenant/city/list') ?></div>
<h1><?= Html::encode($this->title) ?></h1>
<section class="main_tabs">
    <div class="tabs_content">
        <div class="active" id="t01">
            <?= $this->render($form, compact('model', 'languages', 'positions', 'activePositions')); ?>
        </div>
    </div>
</section>
