<?php

use yii\helpers\Html;
use frontend\modules\tenant\models\Currency;
use backend\modules\setting\models\Message;

/* @var $this yii\web\View */
/* @var $model \common\modules\tenant\models\TenantHasCity */
/* @var $form yii\widgets\ActiveForm */
/* @var $positions array */
/* @var $activePositions array */

?>

<div class="non-editable">
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'lang') ?></div>
        <div class="row_input">
            <?= !empty($model->lang) ? $languages[$model->lang] : t('setting', 'Choose the language') ?>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'currency') ?></div>
        <div class="row_input">
            <? $arCurrencyMap = Currency::getCurrencyMap() ?>
            <?= !empty($model->currency) ? Html::encode($arCurrencyMap[$model->currency]) : t('setting',
                'Choose the сurrency') ?>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'sort') ?></div>
        <div class="row_input">
            <?= Html::encode($model->sort) ?>
        </div>
    </section>

    <section class="row">
        <div class="row_label">
            <?= Html::label(t('tenantHasCity', 'Active positions')); ?>
        </div>
        <div class="row_input">
            <?= $this->render('_positions', [
                'positions'       => $positions,
                'activePositions' => $activePositions,
                'modelName'       => $model->formName(),
                'readonly'        => true,
            ]) ?>
        </div>
    </section>

    <section class="submit_form">
        <div>
            <? if (!$model->isNewRecord): ?>
                <label><?= Html::activeCheckbox($model, 'block', ['label' => null, 'disabled' => 'disabled']) ?>
                    <b><?= t('car', 'Block') ?></b></label>
            <? endif ?>
        </div>
    </section>
</div>

