<?php

/* @var $city \common\modules\tenant\models\TenantHasCity */

use yii\helpers\Html;
?>

<? foreach ($cities as $city): ?>
    <tr>
        <td class="pt_fio">
            <?=  Html::a(Html::encode($city->city->{name . getLanguagePrefix()}), ['update', 'id' => $city->city_id])?>
        </td>
        <td class="pt_job">
            <?=$city->sort?>
        </td>
    </tr>
<? endforeach; ?>