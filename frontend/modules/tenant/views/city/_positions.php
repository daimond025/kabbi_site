<?php

use yii\helpers\Html;

/* @var $positions array */
/* @var $activePositions array */
/* @var $modelName string */
/* @var $readonly boolean */

if (is_array($positions)) {
    foreach ($positions as $key => $value) {
        echo Html::tag('div',
            Html::checkbox("{$modelName}[active_position_ids][]",
                in_array($key, empty($activePositions) ? [] : $activePositions, false),
                [
                    'value'    => $key,
                    'label'    => Html::encode($value),
                    'disabled' => (bool)$readonly,
                ]));
    }
}