<? if (!empty($cities)): ?>
    <table class="people_table sort">
        <tr>
            <th class="pt_fio"><?= t('city', 'Name') ?></th>
            <th class="pt_job"><?= t('app', 'Sort') ?></th>
        </tr>
        <?= $this->render('_tr', ['cities' => $cities]); ?>
    </table>
<? else: ?>
    <p><?= t('app', 'Empty') ?></p>
<?endif?>