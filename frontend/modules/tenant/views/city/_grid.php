<?php

/* @var string $pjaxId */
/* @var array $cityList */
/* @var array $positionList */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\employee\models\worker\WorkerSearch */
/* @var string $workerDetailViewUrl */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

$workerDetailViewUrl = getValue($workerDetailViewUrl, '/employee/worker/update');
?>

<? Pjax::begin(['id' => $pjaxId]) ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'       => "{items}",
    'tableOptions' => [
        'class' => 'people_table',
    ],
    'columns'      => [
        [
            'attribute' => 'city_id',
            'label' => t('city', 'Name'),
            'headerOptions' => ['class' => 'pt_fio'],
            'content' => function($model) {
                $text = Html::encode($model['city']['name' . getLanguagePrefix()]);
                return Html::a($text,['update', 'id' => $model['city_id']], ['data-pjax' => 0]);
            },
            'contentOptions' => ['class' => 'pt_fio'],
        ],
        [
            'attribute' => 'sort',
            'label' => t('app', 'Sort'),
            'headerOptions' => ['class' => 'pt_job'],
            'contentOptions' => ['class' => 'pt_fio'],
        ]
    ],
]); ?>
<? Pjax::end() ?>