<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \common\modules\tenant\models\TenantHasCity */
/* @var $positions array */
/* @var $currentPositions array */
/* @var $activePositions array */

$this->title = Yii::t('city', 'Add a city');
?>
<div class="bread"><?= Html::a(t('app', 'Cities'), '/tenant/city/list') ?></div>
<h1><?= Html::encode($this->title) ?></h1>
<section class="main_tabs">
    <div class="tabs_content">
        <div class="active" id="t01">
            <?= $this->render('_form', compact('model', 'positions', 'activePositions', 'languages')); ?>
        </div>
    </div>
</section>
