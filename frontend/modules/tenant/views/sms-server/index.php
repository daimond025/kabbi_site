<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\tenant\models\TenantHasSms */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('setting', 'Sms');
?>
<h1><?= Html::encode($this->title) ?></h1>
<?
if (!empty($model->city_id)):
    $bundle_order = \app\modules\tenant\assets\TenantAsset::register($this);
    $this->registerJsFile($bundle_order->baseUrl . '/smsProviderChanger.js');
    ?>
    <?=
    $this->render('@app/modules/tenant/views/setting/_city_list', [
        'setting_city_id' => $model->city_id,
        'user_city_list'  => $user_city_list,
        'action'          => ['/tenant/sms-server/list', 'city_id' => $model->city_id]
    ]);
    ?>
    <?php
    $form = ActiveForm::begin([
                'errorCssClass'   => 'input_error',
                'successCssClass' => 'input_success'
    ]);
    ?>
    <section class="row">
            <?= $form->field($model, 'server_id')->begin(); ?>
        <div class="row_label"><label><?= t('tenant', 'Choose SMS - provider') ?></label></div>
        <div class="row_input row_low " id = "dropdown">
            <?=
            Html::activeDropDownList($model, 'server_id', $servers, [
                'data-placeholder' => Html::encode(!empty($model->server_id) ? $servers[$model->server_id] : current($servers)),
                'class'            => 'default_select']);
            ?>
        </div>
    <?= $form->field($model, 'server_id')->end(); ?>
    </section>
    <section class="row">
        <?= $form->field($model, 'login')->begin(); ?>
        <div class="row_label"><label><?= t('tenant', 'Login') ?></label></div>
        <div class="row_input" id = "password">
        <?= Html::activeTextInput($model, 'login') ?>
        </div>
    <?= $form->field($model, 'login')->end(); ?>
    </section>
    <section class="row">
        <?= $form->field($model, 'password')->begin(); ?>
        <div class="row_label"><label><?= t('tenant', 'Password') ?></label></div>
        <div class="row_input">
        <?= Html::activeTextInput($model, 'password') ?>
        </div>
    <?= $form->field($model, 'password')->end(); ?>
    </section>
    <section class="row">
        <?= $form->field($model, 'sign')->begin(); ?>
        <div class="row_label"><label><?= t('tenant', 'Signature') ?></label></div>
        <div class="row_input">
    <?= Html::activeTextInput($model, 'sign') ?>
        </div>
    <?= $form->field($model, 'sign')->end(); ?>
    </section>
    <section class="row">
        <?= $form->field($model, 'default')->begin(); ?>
        <div class="row_label"></div>
        <div class="row_input">
            <?= Html::activeCheckbox($model, 'default') ?>
        </div>
        <?= $form->field($model, 'default')->end(); ?>
    </section>
    <section class="row">
        <div class="row_label"><label><?= t('balance', 'Balance') ?></label></div>
        <div class="row_input" id="balance">
            <label id="balance_label" style="width: 150px; display: inline-block; zoom: 1;"><?= t('balance', 'Unknown') ?></label>
            <a id="checkBalance" style="cursor: pointer; border-bottom-style: dashed"><?= t('balance', 'Check balance') ?></a>
        </div>
    </section>
    <section class="submit_form">
        <input type="submit" value="<?= t('app', 'Save') ?>">
    <?= Html::activeHiddenInput($model, 'active', ['value' => 1]) ?>
    </section>
    <?php ActiveForm::end(); ?>
<? else: ?>
    <p><?= t('city', 'No cities') ?></p>
<? endif; ?>