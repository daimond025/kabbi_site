<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\tenant\models\TenantHasSms */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('setting', 'Sms');
?>
<h1><?= Html::encode($this->title) ?></h1>
<?
if (!empty($model->city_id)):
    $bundle_order = \app\modules\tenant\assets\TenantAsset::register($this);
    $this->registerJsFile($bundle_order->baseUrl . '/smsProviderChanger.js');
    ?>
    <?=
    $this->render('@app/modules/tenant/views/setting/_city_list', [
        'setting_city_id' => $model->city_id,
        'user_city_list'  => $user_city_list,
        'action'          => ['/tenant/sms-server/list', 'city_id' => $model->city_id]
    ]);
    ?>
    <div class="non-editable">
        <section class="row">
            <div class="row_label"><label><?= t('tenant', 'SMS - provider') ?></label></div>
            <div class="row_input row_low " id = "dropdown">
                <div class="row_input"> <?= Html::encode(!empty($model->server_id) ? $servers[$model->server_id] : current($servers)); ?></div>
            </div>
            <label id = "balance_label"></label>
        </section>
    </div>
<? else: ?>
    <p><?= t('city', 'No cities') ?></p>
<? endif; ?>
