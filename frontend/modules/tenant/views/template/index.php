<?php

use yii\helpers\Html;
use app\modules\order\models\OrderStatus;
use yii\widgets\Pjax;
use frontend\modules\tenant\helpers\TemplateHelper;
use frontend\modules\tenant\models\ClientPushNotifications;
use frontend\modules\tenant\models\RecipientSmsTemplate;
use frontend\modules\tenant\models\SmsTemplate;
use frontend\modules\tenant\models\WorkerPushNotifications;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var array $positionMap */
/* @var bool $isAllowed */
/* @var array $clientPushModels */
/* @var array $workerPushModels */
/* @var array $smsModels */
/* @var array $recipientSmsModels */

$this->title = t('client-push-notices', 'Notices templates');
$bundle_order = \app\modules\tenant\assets\TenantAsset::register($this);
$this->registerJsFile($bundle_order->baseUrl . '/sms.js');
?>
<h1><?= Html::encode($this->title) ?></h1>
<? if (!empty($cur_city_id)):
    Pjax::begin(['id' => 'client_event_templates']);
    $actionParams = Yii::$app->request->queryParams;
    ?>
    <?= $this->render('@app/modules/tenant/views/setting/_city_list',
    [
        'setting_city_id' => $cur_city_id,
        'user_city_list'  => $user_city_list,
        'action'          => [
            $actionParams['q'],
            'city_id'     => getValue($actionParams['city_id']),
            'position_id' => getValue($actionParams['position_id']),
        ],
    ]);
    ?>
    <section class="row">
        <div class="row_input row_low ">
            <? $selectedPositionId = get('position_id') ?: key($positionMap) ?>
            <?= Html::dropDownList('position_id', $selectedPositionId, $positionMap,
                ['class' => 'default_select', 'id' => 'event_position_id']); ?>
        </div>
    </section>
    <? $preStateArr = [
    OrderStatus::STATUS_PRE,
    OrderStatus::STATUS_PRE_GET_WORKER,
    OrderStatus::STATUS_PRE_REFUSE_WORKER,
]; ?>
    <h2><?= t('setting', 'PUSH for clients') ?></h2>
    <table class="order_table templates_table">
        <tbody>
        <tr>
            <th style="width: 27%;"><?= t('tenant', 'Event') ?></th>
            <th style="width: 69%;"><?= t('tenant', 'Text') ?></th>
        </tr>
        <? foreach ($clientPushModels as $model): ?>
            <tr>
                <td><?= Html::encode(TemplateHelper::getTemplateNameByType($model->type, ClientPushNotifications::$templateList, get('position_id'))) ?>
                    <? if (in_array($model->type, $preStateArr, false)): ?>
                        (<?= t('order', 'Preliminary') ?>)
                    <? endif ?>
                </td>
                <td>
                    <? if ($isAllowed): ?>
                        <a data-template="client_push_<?= $model->push_id ?>" class="ot_edit" href="<?= yii\helpers\Url::to([
                            '/tenant/template/update',
                            'id'          => $model->push_id,
                            'position_id' => $selectedPositionId,
                            'notify_type' => 'ClientPush',
                        ]) ?>"></a>
                    <? endif ?>
                    <div id="template_client_push_<?= $model->push_id ?>"
                         style="padding-right: 10px;"><?= Html::encode($model->text); ?></div>
                </td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>

    <h2><?= t('setting', 'SMS for clients') ?></h2>
    <table class="order_table templates_table">
        <tbody>
        <tr>
            <th style="width: 27%;"><?= t('tenant', 'Event') ?></th>
            <th style="width: 69%;"><?= t('tenant', 'Text') ?></th>
        </tr>
        <? foreach ($smsModels as $smsModel): ?>
            <tr>
                <td><?= Html::encode(TemplateHelper::getTemplateNameByType($smsModel->type, SmsTemplate::$templateList, get('position_id'))) ?>
                    <? if (in_array($smsModel->type, $preStateArr, false)): ?>
                        (<?= t('order', 'Preliminary') ?>)
                    <? endif ?>
                </td>
                <td>
                    <? if ($isAllowed): ?>
                        <a data-template="client_sms_<?= $smsModel->template_id ?>" class="ot_edit" href="<?= yii\helpers\Url::to([
                            '/tenant/template/update',
                            'id'          => $smsModel->template_id,
                            'position_id' => $selectedPositionId,
                            'notify_type' => 'ClientSms',
                        ]) ?>"></a>
                    <? endif ?>
                    <div id="template_client_sms_<?= $smsModel->template_id ?>"
                         style="padding-right: 10px;"><?= Html::encode($smsModel->text); ?></div>
                </td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>

    <h2><?= t('setting', 'PUSH for executors') ?></h2>
    <table class="order_table templates_table">
        <tbody>
        <tr>
            <th style="width: 27%;"><?= t('tenant', 'Event') ?></th>
            <th style="width: 69%;"><?= t('tenant', 'Text') ?></th>
        </tr>
        <? foreach ($workerPushModels as $model): ?>
            <tr>
                <td><?= Html::encode(TemplateHelper::getTemplateNameByType($model->type, WorkerPushNotifications::$templateList, get('position_id'))) ?>
                    <? if (in_array($model->type, $preStateArr, false)): ?>
                        (<?= t('order', 'Preliminary') ?>)
                    <? endif ?>
                </td>
                <td>
                    <? if ($isAllowed): ?>
                        <a data-template="worker_push_<?= $model->push_id ?>" class="ot_edit" href="<?= yii\helpers\Url::to([
                            '/tenant/template/update',
                            'id'          => $model->push_id,
                            'position_id' => $selectedPositionId,
                            'notify_type' => 'WorkerPush',
                        ]) ?>"></a>
                    <? endif ?>
                    <div id="template_worker_push_<?= $model->push_id ?>"
                         style="padding-right: 10px;"><?= Html::encode($model->text); ?></div>
                </td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>

    <h2><?= t('setting', 'SMS for recipient') ?></h2>
    <table class="order_table templates_table">
        <tbody>
        <tr>
            <th style="width: 27%;"><?= t('tenant', 'Event') ?></th>
            <th style="width: 69%;"><?= t('tenant', 'Text') ?></th>
        </tr>
        <? foreach ($recipientSmsModels as $smsModel): ?>
            <tr>
                <td><?= Html::encode(TemplateHelper::getTemplateNameByType($smsModel->type, RecipientSmsTemplate::$templateList, get('position_id'))) ?>
                    <? if (in_array($smsModel->type, $preStateArr, false)): ?>
                        (<?= t('order', 'Preliminary') ?>)
                    <? endif ?>
                </td>
                <td>
                    <? if ($isAllowed): ?>
                        <a data-template="client_sms_<?= $smsModel->template_id ?>" class="ot_edit" href="<?= yii\helpers\Url::to([
                            '/tenant/template/update',
                            'id'          => $smsModel->template_id,
                            'position_id' => $selectedPositionId,
                            'notify_type' => 'RecipientClientSms',
                        ]) ?>"></a>
                    <? endif ?>
                    <div id="template_client_sms_<?= $smsModel->template_id ?>"
                         style="padding-right: 10px;"><?= Html::encode($smsModel->text); ?></div>
                </td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>

    <? Pjax::end() ?>
<? else: ?>
    <p><?= t('city', 'No cities') ?></p>
<? endif; ?>
