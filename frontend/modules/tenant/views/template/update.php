<?php

use common\services\OrderStatusService;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\HtmlPurifier;
use frontend\modules\tenant\helpers\TemplateHelper;

/* @var $this yii\web\View */
/* @var $model frontend\modules\tenant\models\SmsTemplate */
/* @var $params array */

$bundle_order = \app\modules\tenant\assets\TenantAsset::register($this);
$this->registerJsFile($bundle_order->baseUrl . '/sms.js');
?>
<div id="edit_template">
    <h1 style="text-align: left;"><?= Html::encode(TemplateHelper::getTemplateNameByType($model->type, $model::$templateList, get('position_id'))) ?></h1>
    <section class="row">
        <div class="row_label"></div>
        <div class="row_input message" style="color: green; text-align: left;"></div>
    </section>
    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'validateOnSubmit' => false
    ]);?>  
        <section class="row">
            <?= $form->field($model, 'text')->begin();?>
            <div class="row_label"><?=Html::activeLabel($model, 'text')?></div>
            <div class="row_input">
                <?=Html::activeTextarea($model, 'text', ['rows' => 4, 'oninput' => 'pushCounter()', 'onready' => 'precounter()'])?>
            </div>
            <?= $form->field($model, 'text')->end();?>
            <label id = "counter"><?= mb_strlen($model->text)?></label>
        </section>
        <section class="row">
            <div class="row_label"></div>
            <div class="row_input" style="text-align: left;">


                    <span style="display: block; margin-bottom: 8px"><?= $params;?></span>

            </div>
        </section> 
        <section class="submit_form" style="margin-bottom: 0;">
            <?= Html::submitInput(t('app', 'Save')) ?>
        </section>
    <?php ActiveForm::end(); ?>
</div>