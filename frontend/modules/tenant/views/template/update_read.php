<?php

use common\services\OrderStatusService;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\modules\tenant\helpers\TemplateHelper;

/* @var $this yii\web\View */
/* @var $model frontend\modules\tenant\models\SmsTemplate */
$bundle_order = \app\modules\tenant\assets\TenantAsset::register($this);
$this->registerJsFile($bundle_order->baseUrl . '/sms.js');
?>
<div class="non-editable">
    <div id="edit_template">
        <h1 style="text-align: left;"><?= Html::encode(TemplateHelper::getTemplateNameByType($model->type, $model->statusList, get('position_id'))) ?></h1>
        <section class="row">
        </section>
        <section class="row">
            <div class="row_label"><?= Html::activeLabel($model, 'text') ?></div>
            <div class="row_input">
                <div class="row_input">
                    <?=!empty($model->text) ? Html::encode($model->text) : ''?>
                </div>
            </div>
            <label id = "counter"><?= mb_strlen($model->text) ?></label>
        </section>
        <section class="row">
            <div class="row_label"></div>
            <div class="row_input" style="text-align: left;">
                <?if(!empty($defaultTemplate['description'])):?>
                <span style="display: block; margin-bottom: 8px"><?= Html::encode($defaultTemplate['description']); ?></span>
                <?endif?>
            </div>
        </section> 
    </div>
</div>