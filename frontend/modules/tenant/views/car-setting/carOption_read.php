<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\tenant\models\Setting */
/* @var $form yii\widgets\ActiveForm */

$this->title = t('setting', 'Car options');
?>
    <h1><?= Html::encode($this->title) ?></h1>
<?
if (!empty($model->city_id)):
    $action = ['/tenant/car-setting/car-option', 'city_id' => $model->city_id];
    ?>
    <?=
    $this->render('@app/modules/tenant/views/setting/_city_list', [
        'setting_city_id' => $model->city_id,
        'user_city_list'  => $user_city_list,
        'action'          => $action,
    ]);
    ?>
    <h2><?= t('setting', 'Rating options car') ?></h2>
    <div class="non-editable">
        <?php
        foreach ($model->option_list as $option_id => $name):
            $value = isset($model->tenant_options[$option_id]) ? $model->tenant_options[$option_id] : null;
            ?>
            <section class="row">
                <div class="row_label">
                    <label class="no_star"><?= Html::encode($name); ?></label>
                </div>
                <div class="row_input">
                    <div class="row_input">
                        <?= Html::label(!is_null($model->value) ? $model->value : '0'); ?>
                    </div>
                </div>
            </section>
        <? endforeach; ?>
    </div>
<? else: ?>
    <p><?= t('city', 'No cities') ?></p>
<? endif; ?>