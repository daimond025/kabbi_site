<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\tenant\models\Setting */
/* @var $form yii\widgets\ActiveForm */

$this->title = t('setting', 'Car options');
?>
    <h1><?= Html::encode($this->title) ?></h1>
<?
if (!empty($model->city_id)):
    $action = ['/tenant/car-setting/car-option', 'city_id' => $model->city_id];
    ?>
    <?=
    $this->render('@app/modules/tenant/views/setting/_city_list', [
        'setting_city_id' => $model->city_id,
        'user_city_list'  => $user_city_list,
        'action'          => $action,
    ]);
    ?>
    <h2><?= t('setting', 'Rating options car') ?></h2>
    <h4><?= t('setting', 'Switch on the options which you need for showing in client tariffs and cars') ?></h4>
    <?php
    $form = ActiveForm::begin([
        'action'                 => $action,
        'enableClientValidation' => false,
        'validateOnSubmit'       => false,
    ]);
    ?>
    <div class="ap_cont">
        <?php
        foreach ($model->option_list as $option_id => $name):
            $value = isset($model->tenant_options[$option_id]) ? $model->tenant_options[$option_id] : 0;
            $active = isset($model->tenant_options_active[$option_id]) ? $model->tenant_options_active[$option_id] : 0;
            ?>
            <div class="check_option">
                <label style="text-align: left">
                    <input name="Active[<?= $option_id ?>]" <? if ($active != 0): ?>
                        checked="checked"<? endif ?> type="checkbox"
                    />
                    <?= Html::encode($name) ?>
                </label>
                <input name="Option[<?= $option_id ?>]" <? if ($active == 0): ?>disabled<? endif ?> type="number"
                       value="<?= $value ?>" min="0"/>
            </div>
        <? endforeach; ?>

        <section class="submit_form">
            <?= Html::submitInput(Yii::t('app', 'Save')) ?>
        </section>
    </div>
    <?php ActiveForm::end(); ?>
<? else: ?>
    <p><?= t('city', 'No cities') ?></p>
<? endif; ?>