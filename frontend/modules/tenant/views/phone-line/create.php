<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\tenant\models\PhoneLine */
$bundle= app\modules\tenant\assets\TenantAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/phoneline.js');
$this->title = t('phone_line', 'Add phone line');
?>
<div class="bread"><a href="<?= Url::toRoute('/tenant/phone-line/list')?>"><?= t('app', 'Phone line')?></a></div>
<h1><?= Html::encode($this->title) ?></h1>

<?php $form = app()->user->can('phoneLine') ? '_form' : '_form_read';
echo $this->render($form, compact('model', 'user_city_list', 'tariff_list')) ?>

