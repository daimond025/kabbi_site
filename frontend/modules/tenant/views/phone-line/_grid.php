<?php

/* @var string $pjaxId */
/* @var array $cityList */
/* @var array $positionList */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\employee\models\worker\WorkerSearch */
/* @var string $workerDetailViewUrl */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use frontend\widgets\filter\Filter;

$filter = Filter::begin([
    'pjaxId' => $pjaxId,
    'model'  => $searchModel,
]);
echo $filter->checkboxList('city_id', $cityList);
Filter::end();

Pjax::begin(['id' => $pjaxId]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'       => "{items}",
    'tableOptions' => [
        'class' => 'people_table',
    ],
    'columns'      => [
        [
            'attribute' => 'phone',
            'label' => t('app', 'Phone line'),
            'headerOptions' => ['class' => 'pt_fio'],
            'content' => function($model) {
                return Html::a($model['phone'],['update', 'id' => $model['line_id']], ['data-pjax' => 0]);
            }
        ],
        [
            'attribute' => 'tariff_id',
            'label' => t('app', 'Tariff'),
            'headerOptions' => ['class' => 'pt_job'],
            'content' => function($model) {
                return Html::encode($model['tariff']['name']);
            }
        ],
        [
            'attribute' => 'city_id',
            'label' => t('app', 'City'),
            'headerOptions' => ['class' => 'pt_city'],
            'content' => function($model) use ($cityList){
                return $cityList[ $model['city_id'] ];
            }
        ],
    ],
]);

Pjax::end();