<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\tenant\assets\TenantAsset;
/* @var $this yii\web\View */
/* @var $model app\modules\tenant\models\PhoneLine */
/* @var $form yii\widgets\ActiveForm */
//dump($model->getAttributes());
$bundle = TenantAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/phoneline.js');

$this->registerJs('phone_mask_init()');
?>
<?php $form = ActiveForm::begin([
    'id' => 'phone_line-form',
    'errorCssClass' => 'input_error',
    'successCssClass' => 'input_success',
    'enableClientValidation' => true,
    ]); ?>
<input type="hidden" id="line_id" value="<?=$model->line_id?>">
    <section class="main_tabs">
        <section class="row">
            <?= $form->field($model, 'phone')->begin();?>
            <div class="row_label"><?=Html::activeLabel($model, 'phone')?></label></div>
            <div class="row_input">
                <div class="input_with_text">
                    <?= Html::activeTextInput($model, 'phone', [
                            'class' => 'mask_phone',
                            'data-lang' => mb_substr(app()->language, 0, 2),
                        ]); ?>
                    <span></span>
                    <div class="help-block" style="color: red"><?=Html::error($model, 'phone')?></div>
                </div>
            </div>
            <?= $form->field($model, 'phone')->end();?>
        </section>

        <? if($model->isNewRecord): ?>
            <section class="row">
                <?= $form->field($model, 'city_id')->begin();?>
                <div class="row_label"><?=Html::activeLabel($model, 'city_id')?></div>
                <div class="row_input">
                    <?= Html::activeDropDownList($model, 'city_id', $user_city_list, [
                            'data-placeholder' => Html::encode(!empty($model->city_id) ? $user_city_list[$model->city_id] : t('car', 'Choose city')),
                            'class' => 'default_select',
                            'id' => 'phone-line-city-select']); ?>
                </div>
                <?= $form->field($model, 'city_id')->end();?>
            </section>
        <? else: ?>
            <section class="row">
                <div class="row_label"><?= Html::activeLabel($model, 'city_id') ?></div>
                <div class="row_input">
                    <div class="row_input"><?= !empty($model->city_id) ? Html::encode($user_city_list[$model->city_id]) : t('car', 'Choose city') ?></div>
                </div>
            </section>
        <? endif; ?>

        <section class="row" id="tariff_list">
            <?= $form->field($model, 'tariff_id')->begin();?>
            <div class="row_label"><?=Html::activeLabel($model, 'tariff_id')?></div>
            <div class="row_input">
                <?=Html::activeDropDownList($model, 'tariff_id', $tariff_list, ['data-placeholder' => t('phone_line', 'Choose tariff'), 'class' => 'default_select', 'id' => 'phone-line-tariff'])?>
            </div>
            <?= $form->field($model, 'tariff_id')->end();?>
        </section>
        <section class="submit_form">
            <?if(!$model->isNewRecord):?>
                <div>
                    <label><?=Html::activeCheckbox($model, 'block', ['label' => null])?> <b><?=t('app', 'Block')?></b></label>
                </div>
            <?endif?>
            <?= Html::submitInput(t('app', 'Save')) ?>
        </section>
    </section>

<?php ActiveForm::end(); ?>

