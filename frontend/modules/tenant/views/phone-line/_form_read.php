<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\tenant\models\PhoneLine */
/* @var $form yii\widgets\ActiveForm */
$this->registerJs('phone_mask_init()');
?>
<div class="non-editable">
    <section class="main_tabs">
        <section class="row">
            <div class="row_label"><?= Html::activeLabel($model, 'phone') ?></label></div>
            <div class="row_input">
                <div class="input_with_text">
                    <div class="row_input"><?= !empty($model->phone) ? Html::encode($model->phone) : '' ?></div>
                    <span></span>
                </div>
            </div>
        </section>
        <section class="row">
            <div class="row_label"><?= Html::activeLabel($model, 'city_id') ?></div>
            <div class="row_input">
                <div class="row_input"><?= !empty($model->city_id) ? Html::encode($user_city_list[$model->city_id]) : t('car', 'Choose city') ?></div>
            </div>
        </section>
        <section class="row">
            <div class="row_label"><?= Html::activeLabel($model, 'tariff_id') ?></div>
            <div class="row_input">
                <div class="row_input"><?= !empty($model->tariff_id) ? Html::encode($tariff_list[$model->tariff_id]) : t('phone_line', 'Choose tariff') ?></div>
            </div>
        </section>
    </section>
    <?if(!$model->isNewRecord):?>
    <div>
        <label><?= Html::activeCheckbox($model, 'block', ['label' => null, 'disabled' => 'disabled']) ?> <b><?= t('app', 'Block') ?></b></label>
    </div>
    <?endif?>
</div>