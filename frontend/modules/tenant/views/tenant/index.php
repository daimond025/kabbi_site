<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\modules\tenant\models\Tenant */
$this->registerJs('phone_mask_init();');
$this->title = t('app', 'Organization data');
?>
<h1><?= Html::encode($this->title)?></h1>

<?php $form = ActiveForm::begin(['id' => 'tenant_form', 'options' => ['enctype' => 'multipart/form-data'], 'errorCssClass' => 'input_error',]); ?>
    <!--Фото-->
    <?=  \frontend\widgets\file\Logo::widget(['form' => $form, 'model' => $model, 'attribute' => 'logo'])?>
    <h2><?= t('tenant', 'Organization')?></h2>
    <section class="row">
        <?= $form->field($model, 'full_company_name')->begin();?>
            <div class="row_label">
                <?= Html::activeLabel($model, 'full_company_name')?>
            </div>
            <div class="row_input">
                <div class="input_with_text">
                <?= Html::activeTextInput($model, 'full_company_name');?>
                    <span><?=t('tenant','example full company name')?></span>
                </div>
            </div>
        <?= $form->field($model, 'full_company_name')->end();?>
    </section>

    <section class="row">
        <?= $form->field($model, 'company_name')->begin();?>
            <div class="row_label">
                <?= Html::activeLabel($model, 'company_name')?>
            </div>
            <div class="row_input">
                <?= Html::activeTextInput($model, 'company_name');?>
            </div>
        <?= $form->field($model, 'company_name')->end();?>
    </section>

    <section class="row">
        <?= $form->field($model, 'legal_address')->begin();?>
            <div class="row_label">
                <?= Html::activeLabel($model, 'legal_address')?>
            </div>
            <div class="row_input">
                    <?= Html::activeTextInput($model, 'legal_address');?>
            </div>
        <?= $form->field($model, 'legal_address')->end();?>
    </section>

    <section class="row">
        <?= $form->field($model, 'post_address')->begin();?>
            <div class="row_label">
                <?= Html::activeLabel($model, 'post_address')?>
            </div>
            <div class="row_input">
                    <?= Html::activeTextInput($model, 'post_address');?>
            </div>
        <?= $form->field($model, 'post_address')->end();?>
    </section>

    <section class="row">
        <?= $form->field($model, 'director')->begin();?>
            <div class="row_label">
                <?= Html::activeLabel($model, 'director')?>
            </div>
            <div class="row_input">
                    <?= Html::activeTextInput($model, 'director');?>
            </div>
        <?= $form->field($model, 'director')->end();?>
    </section>

    <section class="row">
        <?= $form->field($model, 'director_position')->begin();?>
            <div class="row_label">
                <?= Html::activeLabel($model, 'director_position')?>
            </div>
            <div class="row_input">
                    <?= Html::activeTextInput($model, 'director_position');?>
            </div>
        <?= $form->field($model, 'director_position')->end();?>
    </section>

    <section class="row">
        <?= $form->field($model, 'bookkeeper')->begin();?>
            <div class="row_label">
                <?= Html::activeLabel($model, 'bookkeeper')?>
            </div>
            <div class="row_input">
                    <?= Html::activeTextInput($model, 'bookkeeper');?>
            </div>
        <?= $form->field($model, 'bookkeeper')->end();?>
    </section>

    <section class="row">
        <?= $form->field($model, 'inn')->begin();?>
            <div class="row_label">
                <?= Html::activeLabel($model, 'inn')?>
            </div>
            <div class="row_input">
                    <?= Html::activeTextInput($model, 'inn');?>
            </div>
        <?= $form->field($model, 'inn')->end();?>
    </section>

    <section class="row">
        <?= $form->field($model, 'kpp')->begin();?>
            <div class="row_label">
                <?= Html::activeLabel($model, 'kpp')?>
            </div>
            <div class="row_input">
                    <?= Html::activeTextInput($model, 'kpp');?>
            </div>
        <?= $form->field($model, 'kpp')->end();?>
    </section>

    <section class="row">
        <?= $form->field($model, 'ogrn')->begin();?>
            <div class="row_label">
                <?= Html::activeLabel($model, 'ogrn')?>
            </div>
            <div class="row_input">
                    <?= Html::activeTextInput($model, 'ogrn');?>
            </div>
        <?= $form->field($model, 'ogrn')->end();?>
    </section>

    <h2><?= t('tenant', 'Organization contact')?></h2>

    <section class="row">
        <?= $form->field($model, 'company_phone')->begin();?>
            <div class="row_label">
                <?= Html::activeLabel($model, 'company_phone')?>
            </div>
            <div class="row_input">
                <div class="input_with_text">
                    <?= Html::activeTextInput($model, 'company_phone', [
                            'class' => 'mask_phone',
                            'data-lang' => mb_substr(app()->language, 0, 2),
                        ]);?>
                    <span></span>
                </div>
            </div>
        <?= $form->field($model, 'company_phone')->end();?>
    </section>

    <section class="row">
        <?= $form->field($model, 'email')->begin();?>
            <div class="row_label">
                <?= Html::activeLabel($model, 'email')?>
            </div>
            <div class="row_input">
                <?= Html::activeTextInput($model, 'email');?>
                <?= Html::error($model, 'email', ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
            </div>

        <?= $form->field($model, 'email')->end();?>
    </section>

    <section class="row">
        <?= $form->field($model, 'site')->begin();?>
            <div class="row_label">
                <?= Html::activeLabel($model, 'site')?>
            </div>
            <div class="row_input">
                <?= Html::activeTextInput($model, 'site');?>
                <?= Html::error($model, 'site', ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
            </div>
        <?= $form->field($model, 'site')->end();?>
    </section>

    <h2><?= t('tenant', 'Contact person')?></h2>

    <section class="row">
        <div class="row_label"><label><?=Html::activeLabel($model, 'contact_last_name')?> <?=Html::activeLabel($model, 'contact_name')?> <?=Html::activeLabel($model, 'contact_second_name')?></label></div>
        <div class="row_input">
            <div class="grid_3">

        <?= $form->field($model, 'contact_last_name')->begin();?>
        <div class="grid_item">
            <?=Html::activeTextInput($model, 'contact_last_name')?>
        </div>
        <?= $form->field($model, 'contact_last_name')->end();?>

        <?= $form->field($model, 'contact_name')->begin();?>
        <div class="grid_item">
            <?=Html::activeTextInput($model, 'contact_name')?>
        </div>
        <?= $form->field($model, 'contact_name')->end();?>

        <?= $form->field($model, 'contact_second_name')->begin();?>
        <div class="grid_item">
            <?=Html::activeTextInput($model, 'contact_second_name')?>
        </div>
        <?= $form->field($model, 'contact_second_name')->end();?>
        </div>
        </div>
    </section>

    <section class="row">
        <?= $form->field($model, 'contact_phone')->begin();?>
            <div class="row_label">
                <?= Html::activeLabel($model, 'contact_phone')?>
            </div>
            <div class="row_input">
                <div class="input_with_text">
                    <?= Html::activeTextInput($model, 'contact_phone', [
                            'class' => 'mask_phone',
                            'data-lang' => mb_substr(app()->language, 0, 2),
                        ]);?>
                    <span></span>
                </div>
            </div>
        <?= $form->field($model, 'contact_phone')->end();?>
    </section>

    <section class="row">
        <?= $form->field($model, 'contact_email')->begin();?>
            <div class="row_label">
                <?= Html::activeLabel($model, 'contact_email')?>
            </div>
            <div class="row_input">
                <?= Html::activeTextInput($model, 'contact_email');?>
                <?= Html::error($model, 'contact_email', ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
            </div>
        <?= $form->field($model, 'contact_email')->end();?>
    </section>

    <section class="submit_form">
        <div>
            <label><?= Html::activeCheckbox($model, 'blocked', ['label' => null])?> <b><?= t('app', 'Block')?></b></label>
            <?/**
            <b><?= Html::a(t('app', 'Remove'), ['delete', 'id' => $model->tenant_id], [
                'class' => 'link_red',
                'data' => [
                    'confirm' => t('tenant', 'Are sure you want to delete this organization?'),
                    'method' => 'post',
                ],
            ]) ?></b>*/?>
        </div>
        <?= Html::submitInput(t('app', 'Save')) ?>
    </section>

<?php ActiveForm::end(); ?>
