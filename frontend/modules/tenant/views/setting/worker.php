<?php

/* @var $this \yii\web\View */
/* @var $tenantSetting SettingsWorker */

/* @var $allowWrite boolean */

use frontend\modules\tenant\models\worker\SettingsWorker;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\widgets\ActiveForm;
use frontend\modules\tenant\widgets\ActiveNonForm;

$this->title = Html::encode(t('employee', 'Workers'));

$bundle = app\modules\tenant\assets\TenantAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/setting.js');

$currentCity = empty($cities) ? null : current($cities);


?>

<h1><?= Html::encode($this->title) ?></h1>

<? if (!empty(user()->getUserCityList())): ?>

    <?= $this->render('_city_list', [
        'setting_city_id' => $tenantSetting->cityId,
        'user_city_list'  => $cities,
        'action'          => [
            '/tenant/setting/list',
            'city_id'     => $tenantSetting->cityId,
            'position_id' => $tenantSetting->positionId,
            'type'        => 'drivers',
        ],
    ]) ?>

    <? if (empty($positions)): ?>
        <?= Html::tag('p', Html::encode(t('setting', 'No positions in the current city ({city})',
            ['city' => $currentCity]))) ?>
    <? else: ?>
        <section class="row">
            <div class="row_input row_low ">
                <?= Html::dropDownList('position_id', $tenantSetting->positionId, $positions,
                    ['class' => 'default_select', 'id' => 'settings_position_id']); ?>
            </div>
        </section>

        <?php $options = [
            'id'            => 'setting_form',
            'errorCssClass' => 'input_error',
            'method'        => 'POST',
            'fieldConfig'   => [
                'template'     =>
                    '<section class="row">' .
                    '<div class="row_label">{hint}</div>' .
                    '<div class="row_input">{input}{label}</div>' .
                    '</section>',
                'labelOptions' => [],
                'hintOptions'  => [
                    'tag'          => 'a',
                    'class'        => 'tooltip',
                    'data-tooltip' => '2',
                ],
            ],
        ] ?>
        <?php $form = $allowWrite ? ActiveForm::begin($options) : ActiveNonForm::begin($options) ?>

        <?php /*@var $form \yii\widgets\ActiveForm */ ?>


        <section class="row">
            <!-- Выключить/включить  расчета времени от водителя  до  заказа -->
            <div class="row_label">
                <?//= Html::activeLabel($tenantSetting, 'workerTimeCalculate') ?>
            </div>
            <div class="row_input">
                <?= Html::activeCheckbox($tenantSetting, 'workerTimeCalculate') ?>
            </div>
        </section>

        <!-- Щаг временной метоки   расчета времени от водителя  до  заказа -->
        <section class="row">
            <div class="row_label">

            </div>
            <div class="row_input">
                <?= Html::activeTextInput($tenantSetting, 'workerTimeCalculateStep', ['class' => 'input_number_mini']) ?>
                <?= Html::activeLabel($tenantSetting, 'workerTimeCalculateStep') ?>
            </div>

        </section>

        <!-- Количество временных меток   расчета времени от водителя  до  заказа -->
        <section class="row">
            <div class="row_label">

            </div>
            <div class="row_input">
                <?= Html::activeTextInput($tenantSetting, 'workerTimeCalculateStepCount', ['class' => 'input_number_mini']) ?>
                <?= Html::activeLabel($tenantSetting, 'workerTimeCalculateStepCount') ?>
            </div>

        </section>


        <?= $form->field($tenantSetting, 'arrivalTime')->begin(); ?>
        <?
          $disable_arrival_time = false;
          if((int)$tenantSetting->workerTimeCalculate === 1){
              $disable_arrival_time = true;
          }

        ?>
        <section class="row">
            <div class="row_label">
                <?= Html::activeLabel($tenantSetting, 'arrivalTime'); ?>
            </div>
            <div class="row_input">
                <? foreach ($tenantSetting->arrivalTime as $key => $arrivalTime): ?>
                    <?= Html::activeTextInput($tenantSetting, 'arrivalTime[' . $key . ']',
                        ['class' => 'input_number_mini ', 'disabled' => $disable_arrival_time]); ?>
                <? endforeach; ?>
                <?= t('app', 'min.'); ?>
            </div>
        </section>
        <?= $form->field($tenantSetting, 'arrivalTime')->end(); ?>



        <?= $form->field($tenantSetting, 'allowWorkWithoutGps')->checkbox([], true) ?>
        <?= $form->field($tenantSetting, 'workerMinBalance', [
            'template' => '<section class="row">' .
                '<div class="row_label">{label}{hint}</div>' .
                '<div class="row_input">{input}<span style="line-height: 30px"> ' . Html::encode(getCurrencySymbol($tenantSetting->cityId)) . '</span></div>
                </section>',
        ])->input('text', ['class' => 'input_number_mini number'])->hint('?' .
            Html::tag('span',
                HtmlPurifier::process(Yii::t('hint',
                    'This is the minimum amount that can a worker has on the balance, after commission write-off for shift')),
                ['style' => 'display: none; opacity: 0;']
            )
        ) ?>

        <section class="row">
            <?= $form->field($tenantSetting, 'workerMinReviewRating')->begin(); ?>
            <div class="row_label">
                <?= Html::activeLabel($tenantSetting, 'workerMinReviewRating') ?>
            </div>
            <div class="row_input">
                <?= Html::activeTextInput($tenantSetting, 'workerMinReviewRating', ['class' => 'input_number_mini']) ?>
            </div>
            <?= $form->field($tenantSetting, 'workerMinReviewRating')->end(); ?>
        </section>

        <section class="row">
            <?= $form->field($tenantSetting, 'workerReviewCount')->begin(); ?>
            <div class="row_label">
                <?= Html::activeLabel($tenantSetting, 'workerReviewCount') ?>
            </div>
            <div class="row_input">
                <?= Html::activeTextInput($tenantSetting, 'workerReviewCount', ['class' => 'input_number_mini']) ?>
            </div>
            <?= $form->field($tenantSetting, 'workerReviewCount')->end(); ?>
        </section>

        <?= $form->field($tenantSetting, 'showEstimation')->checkbox() ?>
        <?= $form->field($tenantSetting, 'allowEditOrder')->checkbox() ?>
        <?= $form->field($tenantSetting, 'allowEditCostOrder')->checkbox() ?>
        <?= $form->field($tenantSetting, 'showWorkerAddress')->checkbox([], true) ?>
        <?= $form->field($tenantSetting, 'showWorkerPhone')->checkbox([], true) ?>
        <section class="row">
            <div class="row_label">
                <?= Html::activeLabel($tenantSetting, 'showWorkerClient'); ?>
            </div>
            <div class="row_input">
                <?= Html::activeDropDownList($tenantSetting, 'showWorkerClient', [
                    t('setting',SettingsWorker::DONT_SHOW_DEFAULT_SETTING),
                    t('setting',SettingsWorker::SHOW_ONLY_NAME_SETTING),
                    t('setting',SettingsWorker::SHOW_NAME_AND_PATRONYMIC_SETTING),
                    t('setting',SettingsWorker::SHOW_SURNAME_AND_NAME_AND_PATRONYMIC_SETTING),
                ], ['class' => 'default_select']); ?>
            </div>
        </section>

        <?= $form->field($tenantSetting, 'showUrgentOrderTime')->checkbox([], true) ?>
        <?= $form->field($tenantSetting, 'showChatDispatcherExit')->checkbox([], true) ?>
        <?= $form->field($tenantSetting, 'showChatDispatcherShift')->checkbox([], true) ?>
        <?= $form->field($tenantSetting, 'showGeneralChatShift')->checkbox([], true) ?>
        <?= $form->field($tenantSetting, 'allowUseBankCard')->checkbox([], true) ?>
        <?= $form->field($tenantSetting, 'needCloseWorkerShiftByBadPositionUpdateTime')->checkbox([], true) ?>
        <?= $form->field($tenantSetting, 'workerPositionCheckDelayTime', [
            'options'  => [
                'style' => ((bool)$tenantSetting->needCloseWorkerShiftByBadPositionUpdateTime) ?: 'display: none;',
            ],
            'template' => '<section class="row">' .
                '<div class="row_label">{label}</div>' .
                '<div class="row_input">{input}<span style="line-height: 30px; padding-left: 5px">' . t('app',
                    'min.') . '</span></div>
                </section>',
        ])->input('text', ['class' => 'input_number_mini number']) ?>

        <?= $form->field($tenantSetting, 'needCloseWorkerShiftByBadParkingPosition')->checkbox([], true) ?>
        <?= $form->field($tenantSetting, 'workerParkingPositionCheckDelayTime', [
            'options'  => [
                'style' => ((bool)$tenantSetting->needCloseWorkerShiftByBadParkingPosition) ?: 'display: none;',
            ],
            'template' => '<section class="row">' .
                '<div class="row_label">{label}</div>' .
                '<div class="row_input">{input}<span style="line-height: 30px; padding-left: 5px">' . t('app',
                    'min.') . '</span></div>
                </section>',
        ])->input('text', ['class' => 'input_number_mini number']) ?>

        <?= $form->field($tenantSetting, 'workerParkingPositionCheckType', [
            'options'  => [
                'style' => ((bool)$tenantSetting->needCloseWorkerShiftByBadParkingPosition) ?: 'display: none;',
            ],
            'template' => '<section class="row">' .
                '<div class="row_label">{label}</div>' .
                '<div class="row_input row_low">{input}<span style="line-height: 30px; padding-left: 5px">
                </section>',
        ])->dropDownList(translateAssocArray('setting', SettingsWorker::PARKING_POSITION_CHECK_TYPES), [
            'class' => 'default_select',
        ]) ?>

        <?= $form->field($tenantSetting, 'allowWorkerToCreateOrder')->checkbox([], true) ?>
        <?= $form->field($tenantSetting, 'denyFakegps')->checkbox([], true) ?>
        <?= $form->field($tenantSetting, 'denySettingArrivalStatusEarly')->checkbox([], true) ?>
        <?= $form->field($tenantSetting, 'minDistanceToSetArrivalStatus', [
            'options'  => [
                'style' => ((bool)$tenantSetting->denySettingArrivalStatusEarly) ?: 'display: none;',
            ],
            'template' => '<section class="row">' .
                '<div class="row_label">{label}</div>' .
                '<div class="row_input">{input}<span style="line-height: 30px; padding-left: 5px">' . t('app',
                    'm.') . '</span></div>
                </section>',
        ])->input('text', ['class' => 'input_number_mini number']) ?>
        <?= $form->field($tenantSetting, 'requirePasswordEveryTimeLogInApplication')->checkbox([], true) ?>
        <?= $form->field($tenantSetting, 'printCheck')->checkbox([], true) ?>
        <?= $form->field($tenantSetting, 'showDriverPrivacyPolicy')->checkbox([], true) ?>
        <?= $form->field($tenantSetting, 'controlOwnCarMileage')->checkbox() ?>
        <?= $form->field($tenantSetting, 'addIntermediatePointInOrderForStop')->checkbox() ?>

        <?= $form->field($tenantSetting, 'allowReserveUrgentOrders')->checkbox([], true) ?>

        <?= $form->field($tenantSetting, 'workerOrderSound')->checkbox([], true) ?>

        <?= $form->field($tenantSetting, 'maxCountReserveUrgentOrders', [
            'options'  => [
                'style' => ((bool)$tenantSetting->allowReserveUrgentOrders) ?: 'display: none;',
            ],
            'template' => '<section class="row">' .
                '<div class="row_label">{label}</div>' .
                '<div class="row_input">{input}<span style="line-height: 30px; padding-left: 5px"></span></div>
                </section>',
        ])->input('text', ['class' => 'input_number_mini number']) ?>

        <? if ($allowWrite): ?>
            <?= Html::tag('section', Html::submitButton(Html::encode(t('app', 'Save'))), ['class' => 'submit_form']) ?>
        <? endif; ?>
        <?php $allowWrite ? ActiveForm::end() : ActiveNonForm::end() ?>
    <? endif ?>
<? else: ?>
    <?= Html::tag('p', Html::encode(t('city', 'No cities'))) ?>
<? endif; ?>


<script>
    $( document ).ready(function() {
        var calculateDistance = $('#settingsworker-workertimecalculate');
        $(calculateDistance).change(function(){
            if(this.checked){
                selectDirverTimeCalculate();
            }else{
                selectFixedDriveTime();
            }
        });

    });
    
    function selectDirverTimeCalculate() {
        var arrivalTime =  $("[id^=settingsworker-arrivaltime]");
        $("[id^=settingsworker-arrivaltime]").each(function(index, value) {
            $(value).prop( "disabled", true );
        });
        $("[id^=settingsworker-workertimecalculatestep]").prop( "disabled", false );
        $("[id^=settingsworker-workertimecalculatestepcount]").prop( "disabled", false );

    }

    function selectFixedDriveTime() {
        var arrivalTime =  $("[id^=settingsworker-arrivaltime]");
        $("[id^=settingsworker-arrivaltime]").each(function(index, value) {
            $(value).prop( "disabled", false );
        });

        $("[id^=settingsworker-workertimecalculatestep]").prop( "disabled", true );
        $("[id^=settingsworker-workertimecalculatestepcount]").prop( "disabled", true );
    }
</script>
