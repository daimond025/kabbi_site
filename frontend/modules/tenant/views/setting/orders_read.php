<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = t('setting', 'Orders');

\app\modules\tenant\assets\SettingOrderAsset::register($this);

$bundle = app\modules\tenant\assets\TenantAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/setting.js');
?>
    <h1><?= Html::encode($this->title) ?></h1>
<?
if (!empty($cityId)):
    $action = ['/tenant/setting/list', 'city_id' => $cityId, 'position_id' => $positionId, 'type' => $settingType];
    ?>

    <?= $this->render('_city_list', [
    'setting_city_id' => $cityId,
    'user_city_list'  => $cities,
    'action'          => $action,
]) ?>

    <section class="row">
        <div class="row_input row_low ">
            <?= Html::dropDownList('position_id', $positionId, $positions,
                ['class' => 'default_select', 'id' => 'settings_position_id']); ?>
        </div>
    </section>

    <h2><?= t('setting', 'Distribution') ?></h2>
    <div class="non-editable">
        <section class="row">
            <div class="row_label">
                <label><?= t('setting', 'Distribution type') ?></label>
            </div>
            <div class="row_input">
                <div class="row_input"><?= Html::encode(t('setting',
                        app()->params['settings.orderDistributionTypes'][$tenantSettings['TYPE_OF_DISTRIBUTION_ORDER']])); ?></div>
            </div>
        </section>

        <section data-type="distance" class="row"
                 <? if ($tenantSettings['TYPE_OF_DISTRIBUTION_ORDER'] != 1): ?>style="display:none"<? endif ?>>
            <div class="row_label"></div>
            <div class="row_input">
                <table class="styled_table distribution-circle-table js-distribution-circle-table" style="width: 90%;">
                    <tr>
                        <th style="width: 20%"><?= t('setting', 'Circles') ?></th>
                        <th style="width: 35%"><?= t('setting', 'Search radius') ?></th>
                        <th style="width: 35%"><?= t('setting', 'Number of repetitions') ?></th>
                        <th style="width: 10%"></th>
                    </tr>
                    <? for ($i = 1; $i <= 5; $i++): ?>
                        <tr class='repetitions_circles_settings'
                            <? if (empty($tenantSettings['CIRCLE_DISTANCE_' . $i])): ?>style="display:none"<? endif ?>>
                            <td><span style="line-height: 30px;"><?= t('setting', 'Circle') ?> <?= $i ?></span></td>
                            <td>
                                <div class="input_number_mini number"><?= $tenantSettings['CIRCLE_DISTANCE_' . $i] ?> m</div>
                            </td>
                            <td>
                                <div class="input_number_mini number"><?= $tenantSettings['CIRCLE_REPEAT_' . $i] ?></div>
                            </td>
                            <td></td>
                        </tr>
                    <? endfor; ?>
                </table>
            </div>
        </section>
        <section class="row"
                 <? if ($tenantSettings['TYPE_OF_DISTRIBUTION_ORDER'] != 2): ?>style="display:none"<? endif ?>>
            <div class="row_label">
                <label><?= t('setting', 'Quantity of order distribution repetitions') ?></label>
            </div>
            <div class="row_input">
                <div class="row_input"><?= $tenantSettings['CIRCLES_DISTRIBUTION_ORDER']; ?></div>
            </div>
        </section>

        <section class="row">
            <div class="row_label">
                <label><?= t('setting', 'Order status between the distribution repetitions') ?></label>
            </div>
            <div class="row_input">
                <?
                $values = ['0' => 'New order', '1' => 'Free order'];
                echo isset($values[$tenantSettings['FREE_STATUS_BETWEEN_DISTRIBUTION_CYCLES']])
                    ? t('order', $values[$tenantSettings['FREE_STATUS_BETWEEN_DISTRIBUTION_CYCLES']]) : '';
                ?>
            </div>
        </section>

        <section class="row">
            <div class="row_label">
                <label><?= t('setting', 'The delay between distribution repetitions') ?></label>
            </div>
            <div class="row_input">
                <div class="row_input"><?= $tenantSettings['DELAY_CIRCLES_DISTRIBUTION_ORDER']; ?> <?= t('app',
                        'sec.') ?></div>
            </div>
        </section>

        <section class="row">
            <div class="row_label">
                <label><?= t('setting', 'Offer an order to workers') ?></label>
            </div>
            <div class="row_input">
                <div class="row_input"><?= Html::encode(t('setting',
                        app()->params['settings.offerOrderTypes'][$tenantSettings['OFFER_ORDER_TYPE']])); ?></div>
            </div>
        </section>

        <section data-type="parkings" class="row"
                 <? if ($tenantSettings['TYPE_OF_DISTRIBUTION_ORDER'] != 2): ?>style="display:none"<? endif ?>>
            <div class="row_label"></div>
            <div class="row_input">
                <label>
                    <?= t('setting', 'Consider worker rating on distribution') ?>
                </label>
            </div>
        </section>

        <h2><?= t('setting', 'Orders') ?></h2>
        <section class="row">
            <div class="row_label">
                <label><?= t('setting',
                        'Send PUSH notification to workers who are not on the shift about free order') ?></label>
            </div>
            <div class="row_input">
                <?
                $values = ['0' => 'No', '1' => 'Yes'];
                echo isset($values[$tenantSettings['SEND_PUSH_FREE_ORDER_FOR_ALL']])
                    ? t('app', $values[$tenantSettings['SEND_PUSH_FREE_ORDER_FOR_ALL']]) : '';
                ?>
            </div>
        </section>

        <section class="row">
            <div class="row_label">
                <label><?= t('setting', 'The radius of visibility free orders for the worker') ?></label>
            </div>
            <div class="row_input">
                <div class="row_input">
                    <?= !empty($tenantSettings['DISTANCE_TO_FILTER_FREE_ORDERS']) ? Html::encode($tenantSettings['DISTANCE_TO_FILTER_FREE_ORDERS']) : '' ?> <?= t('app',
                        'm') ?>
                </div>
            </div>
        </section>

        <section class="row">
            <div class="row_label">
                <label><?= t('setting', 'Time after which the order will become overdue') ?></label>
            </div>
            <div class="row_input">
                <div
                        class="row_input"><?= !empty($tenantSettings['PICK_UP']) ? Html::encode($tenantSettings['PICK_UP']) : '' ?> <?= t('app',
                        'sec.') ?></div>
            </div>
        </section>

        <section class="row">
            <div class="row_label">
                <label><?= t('setting', 'Lock Type worker failure / ignore the order') ?></label>
            </div>
            <div class="row_input">
                <div class="row_input"><?= !empty($tenantSettings['WORKER_TYPE_BLOCK']) ? t('setting',
                        Html::encode(app()->params['settings.workerBlockTypes'][$tenantSettings['WORKER_TYPE_BLOCK']])) : '' ?></div>
            </div>
        </section>

        <section class="row">
            <div class="row_label">
                <label><?= t('setting', 'Quantity') ?></label>
            </div>
            <div class="row_input">
                <div
                        class="row_input"><?= !empty($tenantSettings['WORKER_COUNT_TO_BLOCK']) ? Html::encode($tenantSettings['WORKER_COUNT_TO_BLOCK']) : '' ?> <?= t('app',
                        'pcs.') ?> <span style="line-height: 30px"></span></div>
            </div>
        </section>

        <section class="row">
            <div class="row_label">
                <label><?= t('setting', 'Time lock') ?></label>
            </div>
            <div class="row_input">
                <div
                        class="row_input"><?= !empty($tenantSettings['WORKER_TIME_OF_BLOCK']) ? Html::encode($tenantSettings['WORKER_TIME_OF_BLOCK']) : '' ?> <?= t('app',
                        'min.') ?> <span style="line-height: 30px"></span></div>
            </div>
        </section>

        <section class="row">
            <div class="row_label"></div>
            <div class="row_input">
                <?php
                $checked = $tenantSettings['ALLOW_WORKER_TO_CANCEL_ORDER'] ? 'checked="checked"' : '';
                ?>
                <label>
                    <input <?= $checked ?> type="checkbox" disabled id="allow-worker-to-cancel-order">
                    <?= t('setting', 'Allow worker to cancel the order if client does not go out') ?>
                </label>
            </div>
        </section>

        <section class="row" id="time-allow-worker-to-cancel-order">
            <div class="row_label">
                <label><?= t('setting', 'for (min)') ?></label>
            </div>
            <div class="row_input">
                <div class="row_input">
                    <?= !empty($tenantSettings['TIME_ALLOW_WORKER_TO_CANCEL_ORDER']) ? Html::encode($tenantSettings['TIME_ALLOW_WORKER_TO_CANCEL_ORDER']) : '0' ?>
                    <span style="line-height: 30px"><?= t('setting', 'min after start a pay waiting') ?></span>
                    <span style="line-height: 30px"></span>
                </div>
            </div>
        </section>

        <section class="row">
            <div class="row_label"></div>
            <div class="row_input">
                <?php
                $checked = $tenantSettings['REQUIRE_POINT_CONFIRMATION_CODE'] ? 'checked="checked"' : '';
                ?>
                <label>
                    <input <?= $checked ?> type="checkbox" disabled id="allow-worker-to-cancel-order">
                    <?= t('setting', 'Require code to be entered at the end of the order') ?>
                </label>
            </div>
        </section>

        <h2><?= t('setting', 'Order form') ?></h2>

        <section class="row">
            <div class="row_label">
                <label><?= t('setting', 'Add "comment" field to each point of the order') ?></label>
            </div>
            <div class="row_input">
                <?
                $values = ['0' => 'No', '1' => 'Yes'];
                echo isset($values[$tenantSettings['ORDER_FORM_ADD_POINT_COMMENT']])
                    ? t('app', $values[$tenantSettings['ORDER_FORM_ADD_POINT_COMMENT']]) : '';
                ?>
            </div>
        </section>

        <? if ($tenantSettings['ORDER_FORM_ADD_POINT_PHONE']): ?>
            <section class="row">
                <div class="row_label"></div>
                <div class="row_input">
                    <?php
                    $checked = $tenantSettings['REQUIRE_POINT_CONFIRMATION_CODE'] ? 'checked="checked"' : '';
                    ?>
                    <label>
                        <input <?= $checked ?> type="checkbox" disabled id="allow-worker-to-cancel-order">
                        <?= t('setting', 'Require code to be entered at the end of the order') ?>
                    </label>
                </div>
            </section>
        <? endif; ?>

        <section class="row">
            <div class="row_label">
                <label><?= t('setting', 'Add "phone" field to each point of the order') ?></label>
            </div>
            <div class="row_input">
                <?
                $values = ['0' => 'No', '1' => 'Yes'];
                echo isset($values[$tenantSettings['ORDER_FORM_ADD_POINT_PHONE']])
                    ? t('app', $values[$tenantSettings['ORDER_FORM_ADD_POINT_PHONE']]) : '';
                ?>
            </div>
        </section>


        <h2><?= t('setting', 'Pre-orders') ?></h2>
        <section class="row">
            <div class="row_label">
                <label><?= t('setting',
                        'Send PUSH notification to workers who are not on the shift about pre-order') ?></label>
            </div>
            <div class="row_input">
                <?
                $values = ['0' => 'No', '1' => 'Yes'];
                echo isset($values[$tenantSettings['SEND_PUSH_PRE_ORDER_FOR_ALL']])
                    ? t('app', $values[$tenantSettings['SEND_PUSH_PRE_ORDER_FOR_ALL']]) : '';
                ?>
            </div>
        </section>

        <section class="row">
            <div class="row_label">
                <label><?= t('setting', 'Think the preliminary inquiry') ?></label>
            </div>
            <div class="row_input">
                <div
                        class="row_input"><?= !empty($tenantSettings['PRE_ORDER']) ? Html::encode($tenantSettings['PRE_ORDER']) : '' ?> <?= t('setting',
                        'min. to the time of order') ?> <span style="line-height: 30px"></span></div>
            </div>
        </section>

        <section class="row">
            <div class="row_label">
                <label><?= t('setting', 'Will work through') ?></label>
            </div>
            <div class="row_input">
                <div
                        class="row_input"><?= !empty($tenantSettings['PRE_ORDER_WORKING']) ? Html::encode($tenantSettings['PRE_ORDER_WORKING']) : '' ?> <?= t('app',
                        'min.') ?> <span style="line-height: 30px"></span></div>
            </div>
        </section>

        <section class="row">
            <div class="row_label">
                <label><?= t('setting', 'Inform the worker of') ?></label>
            </div>
            <div class="row_input">
                <?php
                $curVal = unserialize($tenantSettings['WORKER_PRE_ORDER_REMINDER']);
                ?>
                <div
                        class="row_input"><?= !empty($tenantSettings['WORKER_PRE_ORDER_REMINDER']) ? Html::encode($curVal[0]) : '' ?> <?= t('app',
                        'min.') ?> <span
                            style="line-height: 30px; display: inline-block; zoom: 1; *display: inline; margin-right: 20px;"></span>
                </div>
                <div
                        class="row_input"><?= !empty($tenantSettings['WORKER_PRE_ORDER_REMINDER']) ? Html::encode($curVal[1]) : '' ?> <?= t('app',
                        'min.') ?> <span
                            style="line-height: 30px; display: inline-block; zoom: 1; *display: inline; margin-right: 20px;"></span>
                </div>
                <div
                        class="row_input"><?= !empty($tenantSettings['WORKER_PRE_ORDER_REMINDER']) ? Html::encode($curVal[2]) : '' ?> <?= t('app',
                        'min.') ?> <span
                            style="line-height: 30px; display: inline-block; zoom: 1; *display: inline; margin-right: 20px;"></span>
                </div>
            </div>
        </section>

        <section class="row">
            <div class="row_label">
                <label><?= t('setting', 'Refusal pre-order without blocking for') ?></label>
            </div>
            <div class="row_input">
                <div
                        class="row_input"><?= !empty($tenantSettings['WORKER_PRE_ORDER_REJECT_MIN']) ? Html::encode($tenantSettings['WORKER_PRE_ORDER_REJECT_MIN']) : '' ?> <?= t('app',
                        'min.') ?> <span style="line-height: 30px"></span></div>
            </div>
        </section>

        <section class="row">
            <div class="row_label">
                <label><?= t('setting', 'Lock the worker for pre-orders') ?></label>
            </div>
            <div class="row_input">
                <div
                        class="row_input"><?= !empty($tenantSettings['WORKER_PRE_ORDER_BLOCK_HOUER']) ? Html::encode($tenantSettings['WORKER_PRE_ORDER_BLOCK_HOUER']) : '' ?> <?= t('app',
                        'h.') ?> <span style="line-height: 30px"></span></div>
            </div>
        </section>

        <h2><?= t('setting', 'Notifications') ?></h2>
        <section class="row">
            <div class="row_label">
                <label><?= t('setting', 'Notify client after workers lateness in ') ?></label>
            </div>
            <div class="row_input">
                <div
                        class="row_input"><?= !empty($tenantSettings['WORKER_LATE_TIME_NOTIFICATION']) ? Html::encode($tenantSettings['WORKER_LATE_TIME_NOTIFICATION']) : '' ?> <?= t('app',
                        'min.') ?> <span style="line-height: 30px"></span></div>
            </div>
        </section>

        <section class="row">
            <div class="row_label">
                <label><?= t('setting', 'Quantity of retries if client does not answer on phone call') ?></label>
            </div>
            <div class="row_input">
                <div
                        class="row_input"><?= !empty($tenantSettings['CALL_COUNT_TRY']) ? Html::encode($tenantSettings['CALL_COUNT_TRY']) : '' ?> <?= t('app',
                        'pcs.') ?> <span style="line-height: 30px"></span></div>
            </div>
        </section>

        <section class="row">
            <div class="row_label">
                <label><?= t('setting', 'Period of time when client already listened a notification') ?></label>
            </div>
            <div class="row_input">
                <div
                        class="row_input"><?= !empty($tenantSettings['CALL_WAIT_TIMEOUT']) ? Html::encode($tenantSettings['CALL_WAIT_TIMEOUT']) : '' ?> <?= t('app',
                        'min.') ?> <span style="line-height: 30px"></span></div>
            </div>
        </section>

        <h2><?= t('setting', 'Module personal cabinet') ?></h2>
        <section class="row">
            <div class="row_label">
                <label><?= t('setting', 'Allow to negotiate the price') ?></label>
            </div>
            <div class="row_input">
                <div class="row_input"><?= !empty($tenantSettings['IS_FIX']) && $tenantSettings['IS_FIX'] == 1
                        ? Html::encode(t('setting', 'Allow')) : Html::encode(t('setting', 'Disable')); ?>
                </div>
        </section>
    </div>
<? else: ?>
    <p><?= t('city', 'No cities') ?></p>
<? endif; ?>