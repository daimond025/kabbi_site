<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model \common\modules\tenant\models\TenantSetting */

$this->title = 'Create Tenant Setting';
$this->params['breadcrumbs'][] = ['label' => 'Tenant Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenant-setting-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
