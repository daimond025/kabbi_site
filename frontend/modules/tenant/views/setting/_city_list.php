<?php

use yii\helpers\Url;
use yii\helpers\Html;

if( count($user_city_list) > 1 ) :
?>
<div class="order_cities">
    <ul>
        <?php foreach ($user_city_list as $city_id => $name): ?>
            <li><a <?php if ($setting_city_id == $city_id): ?>class="active" onclick="return false"<? endif ?> href="<?= Url::to(array_merge($action, ['city_id' => $city_id])) ?>"><span><?= Html::encode($name); ?></span></a></li>
        <?php endforeach; ?>
    </ul>
</div>

<?php endif; ?>