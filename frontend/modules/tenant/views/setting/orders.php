<?php
/* @var $this yii\web\View */

use app\modules\order\services\CallsignService;
use common\modules\tenant\models\DefaultSettings;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

$this->title = t('setting', 'Orders');

\app\modules\tenant\assets\SettingOrderAsset::register($this);
?>

<h1><?= Html::encode($this->title) ?></h1>
<? if (!empty($cityId)):
    $action = ['/tenant/setting/list', 'city_id' => $cityId, 'position_id' => $positionId, 'type' => $settingType];
    ?>

    <?= $this->render('_city_list', [
    'setting_city_id' => $cityId,
    'user_city_list'  => $cities,
    'action'          => $action,
]) ?>
    <section class="row">
        <div class="row_input row_low ">
            <?= Html::dropDownList('position_id', $positionId, $positions,
                ['class' => 'default_select', 'id' => 'settings_position_id']); ?>
        </div>
    </section>


    <h2><?= t('setting', 'Distribution') ?></h2>
    <?= Html::beginForm($action, 'post', ['id' => 'setting_form']) ?>

    <section class="row">
        <div class="row_label">
            <label><?= t('setting', 'Distribution type') ?></label><a data-tooltip="2" class="tooltip">?<span
                        style="display: none; opacity: 0;"><?= HtmlPurifier::process(Yii::t('hint',
                        'Distance: depends on how close car is relatively to a client. Parking zones: cars take a part only being on one parking zone relatively to a client')); ?></span></a>
        </div>


        <div class="row_input row_low">
            <select id="distribution_order" name="<?= $settingType ?>[NEW][TYPE_OF_DISTRIBUTION_ORDER]"
                    class="default_select" data-placeholder="<?= t('setting', 'Choose the type') ?>">
                <?php
                $arData = app()->params['settings.orderDistributionTypes'];
                foreach ($arData as $key => $data):
                    $selected = $tenantSettings['TYPE_OF_DISTRIBUTION_ORDER'] == $key ? 'selected="selected"' : '';
                    ?>
                    <option <?= $selected ?> value="<?= $key ?>"><?= Html::encode(t('setting', $data)); ?></option>
                <? endforeach; ?>
            </select>
        </div>
    </section>

    <section data-type="distance" class="row"
             <? if ($tenantSettings['TYPE_OF_DISTRIBUTION_ORDER'] != 1): ?>style="display:none"<? endif ?>>
        <div class="row_label"></div>
        <div class="row_input">
            <table class="styled_table distribution-circle-table js-distribution-circle-table" style="width: 90%;">
                <tr>
                    <th style="width: 20%"><?= t('setting', 'Circles') ?></th>
                    <th style="width: 35%"><?= t('setting', 'Search radius') ?></th>
                    <th style="width: 35%"><?= t('setting', 'Number of repetitions') ?></th>
                    <th style="width: 10%"></th>
                </tr>
                <? for ($i = 1; $i <= 5; $i++): ?>
                    <tr class='repetitions_circles_settings'
                        <? if (empty($tenantSettings['CIRCLE_DISTANCE_' . $i])): ?>style="display:none"<? endif ?>>
                        <td><span style="line-height: 30px;"><?= t('setting', 'Circle') ?> <?= $i ?></span></td>
                        <td>
                            <input type="text" name="<?= $settingType ?>[NEW][CIRCLE_DISTANCE_<?= $i ?>]"
                                   class="input_number_mini number"
                                   value="<?= $tenantSettings['CIRCLE_DISTANCE_' . $i] ?>">
                            <span style="line-height: 30px;">m</span>
                        </td>
                        <td>
                            <input type="text" name="<?= $settingType ?>[NEW][CIRCLE_REPEAT_<?= $i ?>]"
                                   class="input_number_mini number"
                                   value="<?= $tenantSettings['CIRCLE_REPEAT_' . $i] ?>">
                        </td>
                        <td>
                            <a <? if ($i !== 1 && (!isset($tenantSettings['CIRCLE_DISTANCE_' . ($i + 1)]) || empty($tenantSettings['CIRCLE_DISTANCE_' . ($i + 1)]))): ?>
                                style="display:block"
                            <? else: ?>
                                style="display:none"
                            <? endif ?>
                                    class="close_red js-del-distribution-circle">
                            </a>
                        </td>
                        <td></td>
                    </tr>
                <? endfor; ?>
            </table>
            <a class="add-distribution-circle js-add-distribution-circle"
               <? if (!empty($tenantSettings['CIRCLE_DISTANCE_5'])): ?>style="display:none"<? endif ?>>
                <?= t('setting', 'Add circle') ?>
            </a>
        </div>
    </section>

    <section data-type="parkings" class="row"
             <? if ($tenantSettings['TYPE_OF_DISTRIBUTION_ORDER'] != 2): ?>style="display:none"<? endif ?>>
        <div class="row_label">
            <label><?= t('setting', 'Quantity of order distribution repetitions') ?></label>
        </div>
        <div class="row_input">
            <input name="<?= $settingType ?>[NEW][CIRCLES_DISTRIBUTION_ORDER]" type="text"
                   class="input_number_mini number"
                   value="<?= $tenantSettings['CIRCLES_DISTRIBUTION_ORDER']; ?>">
        </div>
    </section>

    <section class="row">
        <div class="row_label">
            <label><?= t('setting', 'Order status between the distribution repetitions') ?></label>
        </div>
        <div class="row_input row_low">
            <select id="free_status" name="<?= $settingType ?>[NEW][FREE_STATUS_BETWEEN_DISTRIBUTION_CYCLES]"
                    class="default_select">
                <?php
                $values = ['0' => 'New order', '1' => 'Free order'];

                foreach ($values as $key => $data):
                    $selected = $tenantSettings['FREE_STATUS_BETWEEN_DISTRIBUTION_CYCLES'] == $key ? 'selected="selected"' : ''; ?>
                    <option <?= $selected ?> value="<?= $key ?>"><?= Html::encode(t('order', $data)); ?></option>
                <? endforeach; ?>
            </select>
        </div>
    </section>

    <section class="row">
        <div class="row_label">
            <label><?= t('setting', 'The delay between distribution repetitions') ?></label>
        </div>
        <div class="row_input">
            <input name="<?= $settingType ?>[NEW][DELAY_CIRCLES_DISTRIBUTION_ORDER]" type="text"
                   class="input_number_mini number"
                   value="<?= $tenantSettings['DELAY_CIRCLES_DISTRIBUTION_ORDER']; ?>"> <span
                    style="line-height: 30px"><?= t('app', 'sec.') ?></span>
        </div>
    </section>

    <section class="row">
        <div class="row_label">
            <label><?= t('setting', 'Offer an order to workers') ?></label>
        </div>
        <div class="row_input row_low">
            <select id="offer_order_type" name="<?= $settingType ?>[NEW][OFFER_ORDER_TYPE]"
                    class="default_select" data-placeholder="<?= t('setting', 'Choose the type') ?>">
                <?php
                $arData = app()->params['settings.offerOrderTypes'];
                foreach ($arData as $key => $data):
                    $selected = $tenantSettings['OFFER_ORDER_TYPE'] == $key ? 'selected="selected"' : '';
                    ?>
                    <option <?= $selected ?> value="<?= $key ?>"><?= Html::encode(t('setting', $data)); ?></option>
                <? endforeach; ?>
            </select>
        </div>
    </section>

    <section id="offer_order_count"
             class="row"
             <? if ($tenantSettings['OFFER_ORDER_TYPE'] === 'queue') : ?>style="display:none"<? endif ?>>
        <div class="row_label">
            <label><?= t('setting', 'How many at the same time') ?></label>
        </div>
        <div class="row_input row_low">
            <select name="<?= $settingType ?>[NEW][OFFER_ORDER_WORKER_BATCH_COUNT]"
                    class="default_select" data-placeholder="<?= t('setting', 'Choose the type') ?>">
                <?
                $maxCount = app()->params['settings.offerOrderWorkerBatchCountMax'];
                for ($i = 2; $i <= $maxCount; $i++): $selected = $tenantSettings['OFFER_ORDER_WORKER_BATCH_COUNT'] == $i ? 'selected="selected"' : ''; ?>
                    <option <?= $selected ?> value="<?= $i ?>"><?= $i ?></option>
                <? endfor; ?>
            </select>
        </div>
    </section>

    <section id="check_worker_rating" class="row">
        <div class="row_label"></div>
        <div class="row_input">
            <label>
                <input name="<?= $settingType ?>[NEW][CHECK_WORKER_RATING_TO_OFFER_ORDER]" type="checkbox" value="1"
                       <? if ($tenantSettings['CHECK_WORKER_RATING_TO_OFFER_ORDER']): ?>checked="checked"<? endif ?>>
                <?= t('setting', 'Consider worker rating on distribution') ?>
            </label>
        </div>
    </section>

    <h2><?= t('setting', 'View workers on a map') ?></h2>
    <section class="row">
        <div class="row_label">
            <label><?= t('setting',
                    'Whose callsign show on a map') ?></label>
        </div>
        <?php if ($positionHasCar): ?>
            <div class="row_input row_low">
                <select class="default_select" name="<?= $settingType ?>[NEW][WHOSE_CALLSIGN_ON_MAP]">
                    <?php
                    $callsignTypes = CallsignService::getCallsignTypes();

                    foreach ($callsignTypes as $key => $type):
                        $selected = $tenantSettings['WHOSE_CALLSIGN_ON_MAP'] == $key ? 'selected="selected"' : ''; ?>
                        <option <?= $selected ?> value="<?= $key ?>"><?= Html::encode(t('app', $type)); ?></option>
                    <? endforeach; ?>
                </select>
            </div>
        <?php endif; ?>
    </section>

    <h2><?= t('setting', 'Free order') ?></h2>
   <!-- Включить предлодение свободных заказов -->
    <section class="row">
        <div class="row_label"></div>
        <div class="row_input row_low">
            <label>
                <?php
                $checked = $tenantSettings['OFFER_FREE_ORDER'] ? 'checked="checked"' : '';
                ?>
                <input <?= $checked ?> name="<?= $settingType ?>[NEW][OFFER_FREE_ORDER]" type="checkbox" value="1" id="OFFER_FREE_ORDER" class="free_order_on">
                <?= t('setting', 'Offer free order to drivers') ?>
            </label>
        </div>
    </section>

    <!-- Предел расстояние для которого предлагается свободные заказы   -->
    <section class="row">
        <div class="row_label">
            <label><?= t('setting', 'Order radius of order for drivers') ?></label> <br>
            <span> (<?= t('setting', 'ONLY for drivers on order ') ?>) </span>
        </div>
        <div class="row_input">
            <input name="<?= $settingType ?>[NEW][OFFER_FREE_ORDER_DISTANCE]" type="number" max="12000" min="100"
                   class="input_number_mini number FREE_ORDER" value="<?= !empty($tenantSettings['OFFER_FREE_ORDER_DISTANCE']) ? $tenantSettings['OFFER_FREE_ORDER_DISTANCE'] : "8500"  ?>">
            <span style="line-height: 30px"><?= t('app', 'm') ?></span>
        </div>
    </section>
    <!-- Количество предлоджений в цикле -->
    <section class="row">
        <div class="row_label">
            <label><?= t('setting', 'Number of offers for free order') ?></label>
        </div>
        <div class="row_input">
            <input name="<?= $settingType ?>[NEW][OFFER_FREE_ORDER_COUNT]"  type="number" max="6" min="1"
                   class="input_number_mini number FREE_ORDER" value="<?= (isset($tenantSettings['OFFER_FREE_ORDER_COUNT']) && ($tenantSettings['OFFER_FREE_ORDER_COUNT'] > 0))  ? $tenantSettings['OFFER_FREE_ORDER_COUNT'] : 1;   ?>"  >
        </div>
    </section>

    <!-- Предлодить водителям которые на заказе - по умолчанию вкл -->
    <section class="row">
        <div class="row_label"></div>
        <div class="row_input row_low">
            <label>
                <?php
                $checked =  'checked="checked"' ;
                ?>
                <input <?= $checked ?> name="<?= $settingType ?>[NEW][OFFER_FREE_ORDER_WORKER_ON_ORDER]" type="checkbox" value="1"
                                       id="OFFER_FREE_ORDER_WORKER_ON_ORDER" disabled >
                <?= t('setting', 'Offer drivers who are on order') ?>
            </label>
        </div>
    </section>

    <!-- Предлодить водителям которые на паузе - по умолчанию вкл -->
    <section class="row">
        <div class="row_label"></div>
        <div class="row_input row_low">
            <label>
                <?php
                $checked = $tenantSettings['OFFER_FREE_ORDER_WORKER_ON_BREAK'] ? 'checked="checked"' : '';
                ?>
                <input <?= $checked ?> name="<?= $settingType ?>[NEW][OFFER_FREE_ORDER_WORKER_ON_BREAK]" type="checkbox" value="1"
                       id="OFFER_FREE_ORDER_WORKER_ON_BREAK" class="FREE_ORDER" >
                <?= t('setting', 'Offer drivers who are on break') ?>
            </label>
        </div>
    </section>

    <!-- ОТправлять пуш уведомления -->
    <section class="row">
        <div class="row_label">
            <label><?= t('setting',
                    'Send PUSH notification to workers who are not on the shift about free order') ?></label>
        </div>
        <div class="row_input row_low">
            <select id="free_status" name="<?= $settingType ?>[NEW][SEND_PUSH_FREE_ORDER_FOR_ALL]"
                    class="default_select">
                <?php
                $values = ['0' => 'No', '1' => 'Yes'];

                foreach ($values as $key => $data):
                    $selected = $tenantSettings['SEND_PUSH_FREE_ORDER_FOR_ALL'] == $key ? 'selected="selected"' : ''; ?>
                    <option <?= $selected ?> value="<?= $key ?>"><?= Html::encode(t('app', $data)); ?></option>
                <? endforeach; ?>
            </select>
        </div>
    </section>
    <!-- ОТправлять пуш уведомления - предел расстояния -->
    <section class="row">
        <div class="row_label">
            <label><?= t('setting', 'The radius of visibility free orders for the worker. Only for PUSH notification ') ?></label>
        </div>
        <div class="row_input">
            <input name="<?= $settingType ?>[NEW][DISTANCE_TO_FILTER_FREE_ORDERS]" type="text"
                   class="input_number_mini number" value="<?= $tenantSettings['DISTANCE_TO_FILTER_FREE_ORDERS']; ?>">
            <span style="line-height: 30px"><?= t('app', 'm') ?></span>
        </div>
    </section>

    <h2><?= t('setting', 'Orders') ?></h2>
    <section class="row">
        <div class="row_label">
            <label><?= t('setting', 'Time after which the order will become overdue') ?></label>
        </div>
        <div class="row_input">
            <input name="<?= $settingType ?>[NEW][PICK_UP]" type="text" class="input_number_mini number"
                   value="<?= $tenantSettings['PICK_UP']; ?>"> <span style="line-height: 30px"><?= t('app',
                    'sec.') ?></span>
        </div>
    </section>

    <section class="row">
        <div class="row_label">
            <label><?= t('setting', 'Offer time to order for worker') ?></label>
        </div>
        <div class="row_input">
            <input name="<?= $settingType ?>[NEW][ORDER_OFFER_SEC]" type="text" class="input_number_mini number"
                   value="<?= $tenantSettings['ORDER_OFFER_SEC']; ?>"> <span style="line-height: 30px"><?= t('app',
                    'sec.') ?></span>
        </div>
    </section>

    <!-- Количество секунд  - время принятия заказ водителем  -->
    <section class="row">
        <div class="row_label">
            <label><?= t('setting', 'Time of order assign to worker') ?></label>
        </div>
        <div class="row_input">
            <input name="<?= $settingType ?>[NEW][NEW_ORDER_TIME_ASSIGN]" type="text"
                   class="input_number_mini number js-min-order-time-assign"
                   min="0" max="<?= $tenantSettings['ORDER_OFFER_SEC']; ?>"
                   value="<?= $tenantSettings['NEW_ORDER_TIME_ASSIGN']; ?>"> <span style="line-height: 30px">
                <?= t('app',       'sec.') ?></span>
        </div>
    </section>

    <!-- Количество секунд  - показ свободного заказа водителю -->
    <section class="row">
        <div class="row_label">
            <label><?= t('setting', 'Offer time to free order for worker') ?></label>
        </div>
        <div class="row_input">
            <input name="<?= $settingType ?>[NEW][FREE_ORDER_OFFER_SEC]" type="text" class="input_number_mini number"
                   value="<?= $tenantSettings['FREE_ORDER_OFFER_SEC']; ?>"> <span style="line-height: 30px"><?= t('app',
                    'sec.') ?></span>
        </div>
    </section>

    <section class="row">
        <div class="row_label">
            <label><?= t('setting', 'What time the order with status Rejected - no cars will cancel') ?></label>
        </div>
        <div class="row_input">
            <input name="<?= $settingType ?>[NEW][ORDER_REJECT_TIME]" type="text" class="input_number_mini number"
                   value="<?= $tenantSettings['ORDER_REJECT_TIME']; ?>"> <span style="line-height: 30px"><?= t('app',
                    'min.') ?></span>
        </div>
    </section>

    <section class="row">
        <div class="row_label">
            <label><?= t('setting', 'Lock Type worker failure / ignore the order') ?></label>
        </div>
        <div class="row_input row_low">
            <select name="<?= $settingType ?>[NEW][WORKER_TYPE_BLOCK]" class="default_select"
                    data-placeholder="<?= t('setting', 'Choose the type') ?>">
                <?php
                $arData = app()->params['settings.workerBlockTypes'];
                foreach ($arData as $key => $data):
                    $selected = $tenantSettings['WORKER_TYPE_BLOCK'] == $key ? 'selected="selected"' : '';
                    ?>
                    <option <?= $selected ?> value="<?= $key ?>"><?= Html::encode(t('setting', $data)); ?></option>
                <? endforeach; ?>
            </select>
        </div>
    </section>

    <section class="row">
        <div class="row_label">
            <label><?= t('setting', 'Quantity') ?></label>
        </div>
        <div class="row_input">
            <input name="<?= $settingType ?>[NEW][WORKER_COUNT_TO_BLOCK]" type="text" class="input_number_mini number"
                   value="<?= $tenantSettings['WORKER_COUNT_TO_BLOCK']; ?>"> <span
                    style="line-height: 30px"><?= t('app',
                    'pcs.') ?></span>
        </div>
    </section>

    <section class="row">
        <div class="row_label">
            <label><?= t('setting', 'Time lock') ?></label>
        </div>
        <div class="row_input">
            <input name="<?= $settingType ?>[NEW][WORKER_TIME_OF_BLOCK]" type="text" class="input_number_mini number"
                   value="<?= $tenantSettings['WORKER_TIME_OF_BLOCK']; ?>"> <span style="line-height: 30px"><?= t('app',
                    'min.') ?></span>
        </div>
    </section>

    <section class="row">
        <div class="row_label"></div>
        <div class="row_input">
            <?php
            $checked = $tenantSettings['ALLOW_WORKER_TO_CANCEL_ORDER'] ? 'checked="checked"' : '';
            ?>
            <label>
                <input <?= $checked ?> name="<?= $settingType ?>[NEW][ALLOW_WORKER_TO_CANCEL_ORDER]" type="checkbox"
                                       value="1" id="allow-worker-to-cancel-order">
                <?= t('setting', 'Allow worker to cancel the order if client does not go out') ?>
            </label>
        </div>
    </section>

    <section class="row" id="time-allow-worker-to-cancel-order">
        <div class="row_label">
            <label><?= t('setting', 'for (min)') ?></label>
        </div>
        <div class="row_input">
            <?php $value = $tenantSettings['TIME_ALLOW_WORKER_TO_CANCEL_ORDER'] ? $tenantSettings['TIME_ALLOW_WORKER_TO_CANCEL_ORDER'] : 20; ?>
            <input name="<?= $settingType ?>[NEW][TIME_ALLOW_WORKER_TO_CANCEL_ORDER]" type="text"
                   class="input_number_mini number" value="<?= $value ?>">
            <span style="line-height: 30px"><?= t('setting', 'min after start a pay waiting') ?></span>
        </div>
    </section>

    <section class="row">
        <div class="row_label"></div>
        <div class="row_input">
            <?php
            $checked = $tenantSettings['REQUIRE_POINT_CONFIRMATION_CODE'] ? 'checked="checked"' : '';
            ?>
            <label>
                <input <?= $checked ?> name="<?= $settingType ?>[NEW][REQUIRE_POINT_CONFIRMATION_CODE]" type="checkbox"
                                       value="1">
                <?= t('setting', 'Require code to be entered at the end of the order') ?>
            </label>
        </div>
    </section>

    <h2><?= t('setting', 'Order form') ?></h2>

    <section class="row">
        <div class="row_label">
            <label><?= t('setting', 'Add "phone" field to each point of the order') ?></label>
        </div>
        <div class="row_input row_low">
            <select id="free_status" name="<?= $settingType ?>[NEW][ORDER_FORM_ADD_POINT_PHONE]"
                    class="default_select js-order-form-add-point-phone">
                <?php
                $values = ['0' => 'No', '1' => 'Yes'];

                foreach ($values as $key => $data):
                    $selected = $tenantSettings['ORDER_FORM_ADD_POINT_PHONE'] == $key ? 'selected="selected"' : ''; ?>
                    <option <?= $selected ?> value="<?= $key ?>"><?= Html::encode(t('app', $data)); ?></option>
                <? endforeach; ?>
            </select>
        </div>
    </section>

    <section class="row js-order-notify-point-recipient-about-order-executing">
        <div class="row_label"></div>
        <div class="row_input">
            <?php
            $checked = $tenantSettings['SMS_NOTIFY_POINT_RECIPIENT_ABOUT_ORDER_EXECUTING'] ? 'checked="checked"' : '';
            ?>
            <label>
                <input <?= $checked ?> name="<?= $settingType ?>[NEW][SMS_NOTIFY_POINT_RECIPIENT_ABOUT_ORDER_EXECUTING]" type="checkbox"
                                       value="1">
                <?= t('setting', 'Notify to the specified number by SMS on the beginning of the order execute') ?>
            </label>
        </div>
    </section>

    <section class="row">
        <div class="row_label">
            <label><?= t('setting', 'Add "comment" field to each point of the order') ?></label>
        </div>
        <div class="row_input row_low">
            <select id="free_status" name="<?= $settingType ?>[NEW][ORDER_FORM_ADD_POINT_COMMENT]"
                    class="default_select">
                <?php
                $values = ['0' => 'No', '1' => 'Yes'];

                foreach ($values as $key => $data):
                    $selected = $tenantSettings['ORDER_FORM_ADD_POINT_COMMENT'] == $key ? 'selected="selected"' : ''; ?>
                    <option <?= $selected ?> value="<?= $key ?>"><?= Html::encode(t('app', $data)); ?></option>
                <? endforeach; ?>
            </select>
        </div>
    </section>

    <h2><?= t('setting', 'Pre-orders') ?></h2>
    <section class="row">
        <div class="row_label">
            <label><?= t('setting',
                    'Send PUSH notification to workers who are not on the shift about pre-order') ?></label>
        </div>
        <div class="row_input row_low">
            <select id="free_status" name="<?= $settingType ?>[NEW][SEND_PUSH_PRE_ORDER_FOR_ALL]"
                    class="default_select">
                <?php
                $values = ['0' => 'No', '1' => 'Yes'];

                foreach ($values as $key => $data):
                    $selected = $tenantSettings['SEND_PUSH_PRE_ORDER_FOR_ALL'] == $key ? 'selected="selected"' : ''; ?>
                    <option <?= $selected ?> value="<?= $key ?>"><?= Html::encode(t('app', $data)); ?></option>
                <? endforeach; ?>
            </select>
        </div>
    </section>

    <!--Время показа окна предварительного заказа -->
    <section class="row">
        <div class="row_label">
            <label><?= t('setting', 'Offer time to pre-order for worker') ?></label>
        </div>
        <div class="row_input">
            <input name="<?= $settingType ?>[NEW][PRE_ORDER_OFFER_SEC]" type="text" class="input_number_mini number"
                   value="<?= $tenantSettings['PRE_ORDER_OFFER_SEC']; ?>"> <span style="line-height: 30px"><?= t('app',
                    'sec.') ?></span>
        </div>
    </section>

    <section class="row">
        <div class="row_label">
            <label><?= t('setting', 'Think the preliminary inquiry') ?></label>
        </div>
        <div class="row_input">
            <input name="<?= $settingType ?>[NEW][PRE_ORDER]" type="text" class="input_number_mini number"
                   value="<?= $tenantSettings['PRE_ORDER']; ?>"> <span style="line-height: 30px"><?= t('setting',
                    'min. to the time of order') ?></span>
        </div>
    </section>

    <section class="row">
        <div class="row_label"></div>
        <div class="row_input">
            <?php
            $checked = (int)$tenantSettings[DefaultSettings::SETTING_RESTRICT_VISIBILITY_OF_PRE_ORDER] === 1;
            ?>
            <label>
                <input <?= $checked ? 'checked="checked"' : '' ?>
                        name="<?= $settingType ?>[NEW][<?= DefaultSettings::SETTING_RESTRICT_VISIBILITY_OF_PRE_ORDER ?>]"
                        type="checkbox"
                        value="1" id="restrict_visibility_of_pre_order">
                <?= t('setting', 'Restrict the time of pre order visibility for worker') ?>
            </label>
        </div>
    </section>

    <section class="row" id="time_of_pre_order_visibility" style="<?= !$checked ? 'display: none' : '' ?>">
        <div class="row_label">
            <label><?= t('setting', 'Show worker orders for') ?></label>
        </div>
        <div class="row_input">
            <input name="<?= $settingType ?>[NEW][<?= DefaultSettings::SETTING_TIME_OF_PRE_ORDER_VISIBILITY ?>]"
                   type="text"
                   class="input_number_mini number"
                   value="<?= $tenantSettings[DefaultSettings::SETTING_TIME_OF_PRE_ORDER_VISIBILITY] ?>">
            <span style="line-height: 30px"><?= t('setting', 'hours before start') ?></span>
        </div>
    </section>

    <section class="row">
        <div class="row_label">
            <label><?= t('setting', 'Will work through') ?></label>
        </div>
        <div class="row_input">
            <input name="<?= $settingType ?>[NEW][PRE_ORDER_WORKING]" type="text" class="input_number_mini number"
                   value="<?= $tenantSettings['PRE_ORDER_WORKING']; ?>"> <span style="line-height: 30px"><?= t('app',
                    'min.') ?></span>
        </div>
    </section>

    <section class="row">
        <div class="row_label">
            <label><?= t('setting', 'Inform the worker of') ?></label>
        </div>
        <div class="row_input">
            <?php
            $curVal = unserialize($tenantSettings['WORKER_PRE_ORDER_REMINDER']);
            ?>
            <input name="<?= $settingType ?>[NEW][WORKER_PRE_ORDER_REMINDER][]" type="text"
                   class="input_number_mini number" value="<?= $curVal[0] ?>"> <span
                    style="line-height: 30px; display: inline-block; zoom: 1; *display: inline; margin-right: 20px;"><?= t('app',
                    'min.') ?></span>
            <input name="<?= $settingType ?>[NEW][WORKER_PRE_ORDER_REMINDER][]" type="text"
                   class="input_number_mini number" value="<?= $curVal[1] ?>"> <span
                    style="line-height: 30px; display: inline-block; zoom: 1; *display: inline; margin-right: 20px;"><?= t('app',
                    'min.') ?></span>
            <input name="<?= $settingType ?>[NEW][WORKER_PRE_ORDER_REMINDER][]" type="text"
                   class="input_number_mini number" value="<?= $curVal[2] ?>"> <span
                    style="line-height: 30px; display: inline-block; zoom: 1; *display: inline; margin-right: 20px;"><?= t('app',
                    'min.') ?></span>
        </div>
    </section>

    <section class="row">
        <div class="row_label">
            <label><?= t('setting', 'Refusal pre-order without blocking for') ?></label>
        </div>
        <div class="row_input">
            <input name="<?= $settingType ?>[NEW][WORKER_PRE_ORDER_REJECT_MIN]" type="text"
                   class="input_number_mini number" value="<?= $tenantSettings['WORKER_PRE_ORDER_REJECT_MIN']; ?>">
            <span
                    style="line-height: 30px"><?= t('app', 'min.') ?></span>
        </div>
    </section>

    <section class="row">
        <div class="row_label">
            <label><?= t('setting', 'Lock the worker for pre-orders') ?></label>
        </div>
        <div class="row_input">
            <input name="<?= $settingType ?>[NEW][WORKER_PRE_ORDER_BLOCK_HOUER]" type="text"
                   class="input_number_mini number" value="<?= $tenantSettings['WORKER_PRE_ORDER_BLOCK_HOUER']; ?>">
            <span style="line-height: 30px"><?= t('app', 'h.') ?></span>
        </div>
    </section>

    <section class="row">
        <div class="row_label">
            <label><?= t('setting', 'Allow the worker to start preordering for') ?></label>
        </div>
        <div class="row_input">
            <input name="<?= $settingType ?>[NEW][START_TIME_BY_ORDER]" type="text" class="input_number_mini number"
                   value="<?= $tenantSettings['START_TIME_BY_ORDER']; ?>"> <span style="line-height: 30px"><?= t('app',
                    'min.') ?></span>
        </div>
    </section>

    <h2><?= t('setting', 'Exchange of orders') ?></h2>
    <section class="row">
        <div class="row_label"></div>
        <div class="row_input">
            <?php
            $checked = (int)$tenantSettings[DefaultSettings::SETTING_EXCHANGE_ENABLE_SALE_ORDERS] === 1;
            ?>
            <label>
                <input <?= $checked ? 'checked="checked"' : '' ?>
                        name="<?= $settingType ?>[NEW][<?= DefaultSettings::SETTING_EXCHANGE_ENABLE_SALE_ORDERS ?>]"
                        type="checkbox"
                        value="1" id="exchange_enable_sale_orders">
                <?= t('setting', 'Automatically sell order to the exchanger?') ?>
            </label>
        </div>
    </section>

    <section class="row" id="exchange_how_wait_worker" style="<?= !$checked ? 'display: none' : '' ?>">
        <div class="row_label">
            <label><?= t('setting', 'Send the order to the exchanger in') ?></label>
        </div>
        <div class="row_input">
            <input name="<?= $settingType ?>[NEW][<?= DefaultSettings::SETTING_EXCHANGE_HOW_WAIT_WORKER ?>]"
                   type="text"
                   class="input_number_mini number"
                   value="<?= $tenantSettings[DefaultSettings::SETTING_EXCHANGE_HOW_WAIT_WORKER] ?>">
            <span style="line-height: 30px"><?= t('setting', 'sec. after the order becomes overdue') ?></span>
        </div>
    </section>

    <h2><?= t('setting', 'Notifications') ?></h2>

    <section class="row">
        <div class="row_label">
            <label><?= t('setting', 'Quantity of retries if client does not answer on phone call') ?></label>
        </div>
        <div class="row_input">
            <input name="<?= $settingType ?>[NEW][CALL_COUNT_TRY]" type="text" class="input_number_mini number"
                   value="<?= $tenantSettings['CALL_COUNT_TRY']; ?>"> <span style="line-height: 30px"><?= t('app',
                    'pcs.') ?></span>
        </div>
    </section>

    <section class="row">
        <div class="row_label">
            <label><?= t('setting', 'Period of time when client already listened a notification') ?></label>
        </div>
        <div class="row_input">
            <input name="<?= $settingType ?>[NEW][CALL_WAIT_TIMEOUT]" type="text" class="input_number_mini number"
                   value="<?= $tenantSettings['CALL_WAIT_TIMEOUT']; ?>"> <span style="line-height: 30px"><?= t('app',
                    'sec.') ?></span>
        </div>
    </section>

    <h2><?= t('setting', 'Module personal cabinet') ?></h2>
    <section class="row">
        <div class="row_label"></div>
        <div class="row_input">
            <?php
            $checked = $tenantSettings['IS_FIX'] ? 'checked="checked"' : '';
            ?>
            <label>
                <input <?= $checked ?> name="<?= $settingType ?>[NEW][IS_FIX]" type="checkbox" value="1" id="is-fix">
                <?= t('setting', 'Allow to negotiate the price') ?>
            </label>
        </div>
    </section>

    <section class="row">
        <div class="row_label"></div>
        <div class="row_input">
            <?php
            $checked = $tenantSettings['VISIBLE_WORKER_PHONE'] ? 'checked="checked"' : '';
            ?>
            <label>
                <input <?= $checked ?> name="<?= $settingType ?>[NEW][VISIBLE_WORKER_PHONE]" type="checkbox" value="1"
                                       id="visible-worker-phone">
                <?= t('setting', 'Show the client phone worker') ?>
            </label>
        </div>
    </section>

    <section class="submit_form">
        <?php
        foreach ($tenantSettings as $key => $value) {
            echo Html::hiddenInput($settingType . "[OLD][$key]", $value);
        }
        ?>
        <input type="submit" value="<?= t('app', 'Save') ?>">
    </section>
    <?= Html::endForm() ?>
<? else: ?>
    <p><?= t('city', 'No cities') ?></p>
<? endif; ?>
