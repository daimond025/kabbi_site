<?php

namespace frontend\modules\tenant\services\dto;

class EmailProviderData
{
    public $server;
    public $port;

    public function __construct($server, $port)
    {
        $this->server = $server;
        $this->port   = $port;
    }
}