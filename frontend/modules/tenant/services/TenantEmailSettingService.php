<?php

namespace frontend\modules\tenant\services;

use frontend\modules\tenant\models\email\TenantEmailSetting;
use frontend\modules\tenant\repositories\TenantEmailSettingRepository;
use frontend\modules\tenant\services\dto\EmailProviderData;
use frontend\modules\tenant\services\dto\EmailSenderData;

class TenantEmailSettingService
{
    private $tenantEmailSettingRepository;

    public function __construct(TenantEmailSettingRepository $tenantEmailSettingRepository)
    {
        $this->tenantEmailSettingRepository = $tenantEmailSettingRepository;
    }

    /**
     * @param $id
     * @param $tenantId
     *
     * @return TenantEmailSetting
     */
    public function find($id, $tenantId)
    {
        return $this->tenantEmailSettingRepository->find($id, $tenantId);
    }

    /**
     * @param integer $tenantId
     * @param integer $cityId
     *
     * @return TenantEmailSetting
     */
    public function findOrCreate($tenantId, $cityId)
    {
        return $this->tenantEmailSettingRepository->findOrCreate($tenantId, $cityId);
    }

    /**
     * @param integer           $tenantId
     * @param integer           $cityId
     * @param integer           $activate
     * @param EmailProviderData $provider
     * @param EmailSenderData   $sender
     * @param string            $template
     */
    public function save($tenantId, $cityId, $activate, EmailProviderData $provider, EmailSenderData $sender, $template)
    {
        $this->tenantEmailSettingRepository->save($tenantId, $cityId, $activate, $provider, $sender, $template);
    }
}