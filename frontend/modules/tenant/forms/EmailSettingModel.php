<?php

namespace frontend\modules\tenant\forms;

use frontend\modules\tenant\helpers\TenantEmailHelper;
use frontend\modules\tenant\models\email\TenantEmailSetting;
use yii\base\Model;

/**
 * Class EmailSettingModel
 * @package frontend\modules\tenant\forms
 *
 * @property boolean $isActivated
 */
class EmailSettingModel extends Model
{
    public $activate;
    public $provider;
    public $senderName;
    public $senderEmail;
    public $senderPassword;
    public $template;

    public function __construct(TenantEmailSetting $emailSetting, $config = [])
    {
        $this->activate       = $emailSetting->active;
        $this->senderName     = $emailSetting->sender_name;
        $this->senderEmail    = $emailSetting->sender_email;
        $this->senderPassword = $emailSetting->sender_password;
        $this->template       = $emailSetting->template;
        $this->provider       = TenantEmailHelper::getProviderByServer($emailSetting->provider_server,
            $emailSetting->provider_port);

        parent::__construct($config);
    }

    public function attributeLabels()
    {
        return [
            'activate'       => t('email-setting', 'Use'),
            'provider'       => t('email-setting', 'Provider'),
            'senderName'     => t('email-setting', 'Sender name'),
            'senderEmail'    => t('email-setting', 'Sender email'),
            'senderPassword' => t('email-setting', 'Sender password'),
            'template'       => t('email-setting', 'Template'),
        ];
    }

    public function rules()
    {
        return [
            ['activate', 'required'],
            ['activate', 'in', 'range' => TenantEmailHelper::getActivateList()],

            ['provider', 'required', 'when' => [$this, 'whenActivate']],
            ['provider', 'in', 'range' => TenantEmailHelper::getProviderList(), 'when' => [$this, 'whenActivate']],

            ['senderName', 'required', 'when' => [$this, 'whenActivate']],
            ['senderName', 'string', 'max' => 100, 'when' => [$this, 'whenActivate']],

            ['senderEmail', 'required', 'when' => [$this, 'whenActivate']],
            ['senderEmail', 'email', 'when' => [$this, 'whenActivate']],
            ['senderEmail', 'string', 'max' => 255, 'when' => [$this, 'whenActivate']],

            ['senderPassword', 'required', 'when' => [$this, 'whenActivate']],
            ['senderPassword', 'string', 'max' => 100, 'when' => [$this, 'whenActivate']],

            ['template', 'required', 'when' => [$this, 'whenActivate']],
            ['template', 'string', 'when' => [$this, 'whenActivate']],
        ];
    }

    public function whenActivate(EmailSettingModel $model)
    {
        return $model->isActivated;
    }


    public function getIsActivated()
    {
        return (boolean)$this->activate;
    }
}