<?php

namespace app\modules\tenant\controllers;

use common\modules\tenant\models\DefaultClientStatusEvent;
use frontend\components\behavior\CityListBehavior;
use frontend\modules\employee\components\position\PositionService;
use frontend\modules\tenant\models\ClientStatusEvent;
use frontend\modules\tenant\models\clientStatusEvent\ClientStatusEventHelper;
use frontend\modules\tenant\models\clientStatusEvent\ClientStatusEventModel;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

/**
 * StatusEventController implements the CRUD actions for ClientStatusEventRecord model.
 * @mixin CityListBehavior
 */
class StatusEventController extends Controller
{
    private $positionService;

    public function behaviors()
    {
        return [
            'verbs'    => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access'   => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['list'],
                        'roles'   => ['read_settings'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['settings'],
                    ],
                ],
            ],
            'cityList' => [
                'class' => CityListBehavior::className(),
            ],
        ];
    }

    public function __construct($id, Module $module, PositionService $positionService, array $config = [])
    {
        $this->positionService = $positionService;
        $this->positionService->setTenantId(user()->tenant_id);

        parent::__construct($id, $module, $config);
    }

    /**
     * @param int $cityId
     *
     * @return array
     */
    private function getPositionMap($cityId)
    {
        $positions = $this->positionService->getPositionsByCity($cityId);

        return empty($positions) ? [] : ArrayHelper::map($positions, 'position_id', 'name');
    }


    /**
     * @param integer $city_id
     * @param integer $position_id
     *
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function actionList($city_id = null, $position_id = null)
    {
        $isAllowed       = app()->user->can('settings');
        $userCityMap     = $this->getUserCityList();
        $cityId          = ArrayHelper::keyExists($city_id, $userCityMap) ? $city_id : key($userCityMap);
        $userPositionMap = $this->getPositionMap($cityId);
        $positionId      = ArrayHelper::keyExists($position_id,
            $userPositionMap) ? $position_id : key($userPositionMap);
        $model           = ClientStatusEventModel::find(user()->tenant_id, $cityId, $positionId);
        $groupMap        = ClientStatusEventHelper::getGroupMap();
        $noticeMap       = ClientStatusEventHelper::getNoticeMap();
        $eventMap        = ClientStatusEventHelper::getEventMap($positionId, user()->lang);

        if ($isAllowed && app()->request->isPost) {
            $model->load(post());

            if ($model->saveMany()) {
                session()->setFlash('success', t('app', 'Data updated successfully'));
            } else {
                session()->setFlash('error', t('status_event', 'Error saving. Notify to administrator, please.'));
            }
        }

        return $this->render('index',
            compact('isAllowed', 'model', 'cityId', 'positionId', 'userCityMap', 'userPositionMap', 'groupMap',
                'noticeMap', 'eventMap'));
    }


}
