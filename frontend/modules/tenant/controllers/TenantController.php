<?php

namespace app\modules\tenant\controllers;

use Yii;
use common\modules\tenant\models\Tenant;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;

/**
 * TenantController implements the CRUD actions for Tenant model.
 */
class TenantController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['tenant'],
                    ],
                ],
            ],
        ];
    }
    
    /**
     * Updates an existing Tenant model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionInfo()
    {
        $model = $this->findModel(user()->tenant_id);
        $model->blocked = $model->status == Tenant::ACTIVE ? 0 : 1;

        if ($model->load(Yii::$app->request->post())) {
            $model->status = $model->blocked ? Tenant::BLOCKED : Tenant::ACTIVE;
            $model->setCropParams(post('Crop'));
            if ($model->save()) {
                session()->setFlash('success', t('app', 'Data updated successfully'));
            } else {
                displayErrors([$model]);
            }

            return $this->refresh();
        }

        return $this->render('index', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Tenant model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete()
    {
        $model = $this->findModel(user()->tenant_id);
        $model->status = Tenant::REMOVED;

        if ($model->save(false)) {
            Yii::$app->user->logout();

            return $this->render('removed');
        } else
            throw new \Exception('Delete organization error');
    }

    /**
     * Finds the Tenant model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Tenant the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        return Tenant::findOne($id);
    }

}
