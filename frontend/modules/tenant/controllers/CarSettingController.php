<?php
namespace app\modules\tenant\controllers;

use Yii;
use yii\filters\AccessControl;
use common\modules\tenant\models\TenantHasCarOption;
use frontend\modules\car\models\CarOption;
use yii\helpers\ArrayHelper;

class CarSettingController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['car-option'],
                        'roles'   => ['read_settings'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['settings'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Allow to edit car option raiting
     * @param int|null $city_id
     * @return mixed
     */
    public function actionCarOption($city_id = null)
    {
        $model = new TenantHasCarOption;
        $isAllowed = app()->user->can('settings');
        $user_city_list = user()->getUserCityList();
        $model->city_id = getValue($city_id, key($user_city_list));
        //Список опций системы
        $model->option_list = CarOption::find()->orderBy(['sort' => SORT_ASC, 'option_id' => SORT_ASC])->all();
        $model->option_list = translateAssocArray('car-options', ArrayHelper::map($model->option_list, 'option_id', 'name'));
        //Список опций тенанта
        $model->tenant_id = user()->tenant_id;
        $tenant_options = TenantHasCarOption::find()->where(['tenant_id' => $model->tenant_id, 'city_id' => $model->city_id])->all();
        $model->tenant_options = ArrayHelper::map($tenant_options, 'option_id', 'value');
        $model->tenant_options_active = ArrayHelper::map($tenant_options, 'option_id', 'is_active');
        if($isAllowed && app()->request->isPost) {
            $option = post('Option');
            $active = post('Active');
            $arSetting = [];
            foreach($model->option_list as $option_id => $item) {
                $a = isset($active[$option_id]) ? 1 : 0;
                $v = isset($option[$option_id]) && $a == 1 ? intval($option[$option_id]) : 0;
                $v = $v < 0 ? 0 : $v;
                $arSetting[$option_id] = [
                    'value'     => $v,
                    'is_active' => $a,
                ];

                $model->tenant_options[$option_id] = $v;
                $model->tenant_options_active[$option_id] = $a;
            }
            $transaction = Yii::$app->db->beginTransaction();
            try {
                TenantHasCarOption::manySave($arSetting, $model->tenant_id, $model->city_id);
                //Нужно обновить рейтинг автомобилей
                TenantHasCarOption::refreshCarRating($model->city_id);
                session()->setFlash('success', t('app', 'Data updated successfully'));
                $transaction->commit();
            } catch (\Exception $e) {
                session()->setFlash('error', t('app', 'Save error'));
                Yii::error($e->getMessage());
                $transaction->rollBack();
            }
        }

        $form = $isAllowed ? 'carOption' : 'carOption_read';

        return $this->render($form, compact('model', 'user_city_list'));
    }
}
