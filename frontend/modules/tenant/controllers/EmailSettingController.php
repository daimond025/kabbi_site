<?php

namespace app\modules\tenant\controllers;

use frontend\components\behavior\CityListBehavior;
use frontend\modules\tenant\forms\EmailSettingModel;
use frontend\modules\tenant\helpers\TenantEmailHelper;
use frontend\modules\tenant\services\dto\EmailProviderData;
use frontend\modules\tenant\services\dto\EmailSenderData;
use frontend\modules\tenant\services\TenantEmailSettingService;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

/**
 * Class EmailSettingController
 * @package app\modules\tenant\controllers
 * @mixin CityListBehavior
 */
class EmailSettingController extends Controller
{
    private $tenantEmailSettingService;

    public function __construct(
        $id,
        $module,
        TenantEmailSettingService $tenantEmailSettingService,
        $config = []
    ) {
        parent::__construct($id, $module, $config = []);

        $this->tenantEmailSettingService = $tenantEmailSettingService;
    }

    public function behaviors()
    {
        return [
            'access'   => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index'],
                        'roles'   => ['read_emailSetting'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['emailSetting'],
                    ],
                ],
            ],
            'cityList' => [
                'class' => CityListBehavior::className(),
            ],
        ];
    }

    public function actionIndex()
    {
        $tenantId     = user()->tenant_id;
        $cityList     = $this->getUserCityIds();
        $cityMap      = $this->getUserCityList();
        $cityId       = get('city_id', reset($cityList));
        $cityId       = ArrayHelper::isIn($cityId, $cityList) ? $cityId : reset($cityList);
        $isAllowWrite = app()->user->can('emailSetting');

        $tenantEmailSettingModel = $this->tenantEmailSettingService->findOrCreate($tenantId, $cityId);
        $formModel               = new EmailSettingModel($tenantEmailSettingModel);


        if ($isAllowWrite && $formModel->load(post()) && $formModel->validate()) {
            try {
                $this->tenantEmailSettingService->save(
                    $tenantId,
                    $cityId,
                    $formModel->activate,
                    new EmailProviderData(TenantEmailHelper::getProviderServer($formModel->provider),
                        TenantEmailHelper::getProviderPort($formModel->provider)),
                    new EmailSenderData($formModel->senderName, $formModel->senderEmail, $formModel->senderPassword),
                    $formModel->template
                );
                session()->setFlash('success', t('app', 'Data updated successfully'));

            } catch (\RuntimeException $exception) {
                session()->setFlash('error', t('status_event', 'Error saving. Notify to administrator, please.'));
            }
        }

        return $this->render('index', [
            'formModel'    => $formModel,
            'cityMap'      => $cityMap,
            'cityId'       => $cityId,
            'isAllowWrite' => $isAllowWrite,
        ]);
    }
}