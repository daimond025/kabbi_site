<?php

namespace app\modules\tenant\controllers;

use common\modules\tenant\models\DefaultClientPushNotification;
use common\modules\tenant\models\DefaultSmsTemplate;
use common\modules\tenant\models\DefaultWorkerPushNotification;
use common\modules\tenant\models\NotifyTemplateHelper;
use frontend\modules\employee\components\position\PositionService;
use frontend\modules\tenant\models\ClientPushNotifications;
use frontend\modules\tenant\models\DefaultRecipientSmsTemplate;
use frontend\modules\tenant\models\RecipientSmsTemplate;
use frontend\modules\tenant\models\SmsTemplate;
use frontend\modules\tenant\models\WorkerPushNotifications;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;


/**
 * ClientPushNotificationsController implements the CRUD actions for ClientPushNotifications model.
 */
class TemplateController extends Controller
{
    public $defaultAction = 'list';
    private $positionService;

    public function __construct($id, Module $module, PositionService $positionService, array $config = [])
    {
        $this->positionService = $positionService;
        $this->positionService->setTenantId(user()->tenant_id);

        parent::__construct($id, $module, $config);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['list'],
                        'roles'   => ['read_settings'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['settings'],
                    ],
                ],
            ],
        ];
    }

    private function getSmsTemplates($tenant_id, $city_id, $position_id)
    {
        return SmsTemplate::find()
            ->where([
                'tenant_id'   => $tenant_id,
                'city_id'     => $city_id,
                'position_id' => $position_id,
                'type'        => SmsTemplate::$templateList,
            ])
            ->indexBy('type')
            ->all();
    }

    private function getRecipientSmsTemplates($tenantId, $cityId, $positionId)
    {
        return RecipientSmsTemplate::find()
            ->where([
                'tenant_id'   => $tenantId,
                'city_id'     => $cityId,
                'position_id' => $positionId,
            ])
            ->indexBy('type')
            ->all();
    }

    private function getClientPushTemplates($tenant_id, $city_id, $position_id)
    {
        return ClientPushNotifications::find()
            ->where([
                'tenant_id'   => $tenant_id,
                'city_id'     => $city_id,
                'position_id' => $position_id,
                'type'        => ClientPushNotifications::$templateList,
            ])
            ->indexBy('type')
            ->all();
    }

    private function getWorkerPushTemplates($tenant_id, $city_id, $position_id)
    {
        return WorkerPushNotifications::find()
            ->where([
                'tenant_id'   => $tenant_id,
                'city_id'     => $city_id,
                'position_id' => $position_id,
                'type'        => WorkerPushNotifications::$templateList,
            ])
            ->indexBy('type')
            ->all();
    }

    /**
     * @param int $cityId
     *
     * @return array
     */
    private function getPositionMap($cityId)
    {
        $positions = $this->positionService->getPositionsByCity($cityId);

        return empty($positions) ? [] : ArrayHelper::map($positions, 'position_id', 'name');
    }

    /**
     * Lists all ClientPushNotifications models.
     *
     * @param integer|null $city_id
     * @param integer|null $position_id
     *
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionList($city_id = null, $position_id = null)
    {
        $tenant_id = user()->tenant_id;

        $user_city_list = user()->getUserCityList();
        $cur_city_id    = getValue($city_id, key($user_city_list));

        $positionMap = $this->getPositionMap($cur_city_id);
        $position_id = $position_id ?: key($positionMap);

        $smsModels          = $this->getSmsTemplates($tenant_id, $cur_city_id, $position_id);
        $recipientSmsModels = $this->getRecipientSmsTemplates($tenant_id, $cur_city_id, $position_id);
        $clientPushModels   = $this->getClientPushTemplates($tenant_id, $cur_city_id, $position_id);
        $workerPushModels   = $this->getWorkerPushTemplates($tenant_id, $cur_city_id, $position_id);

        return $this->render('index', [
            'clientPushModels'   => $clientPushModels,
            'workerPushModels'   => $workerPushModels,
            'smsModels'          => $smsModels,
            'recipientSmsModels' => $recipientSmsModels,
            'user_city_list'     => $user_city_list,
            'cur_city_id'        => $cur_city_id,
            'positionMap'        => $positionMap,
            'isAllowed'          => app()->user->can('settings'),
        ]);
    }

    /**
     * Updates an existing ClientPushNotifications model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @param integer $position_id
     * @param string  $notify_type
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id, $position_id, $notify_type)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        switch ($notify_type) {
            case 'ClientPush':
                $model           = ClientPushNotifications::findOne($id);
                $defaultTemplate = DefaultClientPushNotification::getPushNotification($model->type, $position_id);
                $params          = NotifyTemplateHelper::getParams($defaultTemplate->params);
                break;
            case 'ClientSms':
                $model           = SmsTemplate::findOne($id);
                $defaultTemplate = DefaultSmsTemplate::getSmsTemplate($model->type, $position_id);
                $params          = NotifyTemplateHelper::getParams($defaultTemplate->params);
                break;
            case 'WorkerPush':
                $model           = WorkerPushNotifications::findOne($id);
                $defaultTemplate = DefaultWorkerPushNotification::getPushNotification($model->type, $position_id);
                $params          = NotifyTemplateHelper::getParams($defaultTemplate->params);
                break;
            case 'RecipientClientSms':
                $model           = RecipientSmsTemplate::findOne($id);
                $defaultTemplate = DefaultRecipientSmsTemplate::getSmsTemplate($model->type, $position_id);
                $params          = NotifyTemplateHelper::getParams($defaultTemplate->params);
                break;
            default:
                return false;
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return t('app', 'Data updated successfully');
            }

            return t('app', 'Save error');
        }
        $form = app()->user->can('settings') ? '' : '_read';

        return $this->renderAjax('update' . $form, [
            'model'  => $model,
            'params' => $params,
        ]);
    }

}
