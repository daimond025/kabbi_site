<?php

namespace app\modules\tenant\controllers;

use GuzzleHttp\Exception\ClientException;
use paymentGate\exceptions\UnknownProfileTypeException;
use paymentGate\models\BaseProfile;
use paymentGate\models\Disabled;
use paymentGate\ProfileService;
use Yii;
use yii\base\InvalidParamException;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;

/**
 * Controller for use with bank cards
 */
class BankCardController extends Controller
{
    const FILTER_TODAY = 'today';
    const FILTER_MONTH = 'month';
    const FILTER_PERIOD = 'period';

    /**
     * @var ProfileService
     */
    private $profileService;


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['change-profile-type', 'update', 'yandex-failed-payments'],
                        'roles'   => ['read_bankCard'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->profileService = app()->profileService;
    }

    /**
     * Change type of payment gate profile
     *
     * @return mixed
     * @throws UnknownProfileTypeException
     * @throws InvalidParamException
     */
    public function actionChangeProfileType()
    {
        $isAllowed = app()->user->can('bankCard');

        if (!$isAllowed) {
            return $this->redirect('update');
        }

        $profile = $this->profileService->getProfileModel(Disabled::TYPE_ID);
        if (app()->request->isPost) {
            $profileData = app()->request->post();
            $typeId = (int)$profileData['Profile']['typeId'];

            if ($typeId !== 0) {
                $profile = $this->profileService->getProfileModel($typeId);
                $profile->load($profileData);
            }
        }

        try {
            $profileTypes = $this->profileService->getProfileTypes();
        } catch (\Exception $ex) {
            session()->setFlash('error',
                t('bank-card', 'An error occurred while working with payment gate settings'));
            Yii::error($ex, 'bank-card');

            return $this->redirect('update');
        }
        $profileTypes = ArrayHelper::merge([
            ['id' => 0, 'name' => t('bank-card', 'Disable')],
        ], $profileTypes);

        return $this->renderAjax('_form', [
            'profile'       => $profile,
            'profileTypes'  => ArrayHelper::map($profileTypes, 'id', 'name'),
            'isAllowed'     => $isAllowed,
            'currentCityId' => (int)$profileData['Profile']['city_id'],
        ]);
    }

    /**
     * Update profile of payment gate
     * @param null $city_id
     * @return string
     * @throws \yii\db\Exception
     */
    public function actionUpdate($city_id = null)
    {
        $isAllowed = app()->user->can('bankCard');
        $cities = user()->getUserCityList();
        $currentCityId = $this->profileService->getCurrentCityId($city_id, post());
        $paymentName = $this->profileService->getPaymentName(user()->tenant_id, $currentCityId);


        try {
            $profileTypes = (array)$this->profileService->getProfileTypes();


            $transaction = app()->db->beginTransaction();

            if (Yii::$app->request->isGet) {

                if (!$paymentName) {
                    $profile = $this->profileService->getProfileModel(Disabled::TYPE_ID);

                } else {

                    try {
                        $profile = $this->profileService->getProfile($paymentName);

                    } catch (UnknownProfileTypeException $ex) {
                        Yii::error($ex->getMessage());
                    }
                }
            } elseif ($isAllowed && Yii::$app->request->isPost) {

                $profilePostData = app()->request->post();
                $typeIdFromPost = (int)$profilePostData['Profile']['typeId'];

                if ($this->profileService->isCreateProfile($paymentName, $typeIdFromPost)) {

                    $newProfile = $this->prepareNewProfile($typeIdFromPost, $profilePostData);

                    if ($newProfile->validate()) {
                        $this->profileService->createProfile($newProfile, $currentCityId);
                        $this->profileService->saveAdvanceSettings(user()->tenant_id, $currentCityId, $newProfile);

                        session()->setFlash('success', t('bank-card', 'Profile of payment gate was saved'));

                        $transaction->commit();

                        return $this->redirect(Url::to(['/tenant/bank-card/update', 'city_id' => $currentCityId]));
                    }

                } else {

                    /* @var $profile BaseProfile */
                    $profile = $this->profileService->getProfile($paymentName);

                    if ($this->profileService->isDeleteProfile($paymentName, $typeIdFromPost)) {

                        $this->profileService->deleteProfile($paymentName);
                        session()->setFlash('success', t('bank-card', 'Profile of payment gate was removed'));

                        $transaction->commit();

                        return $this->redirect(Url::to(['/tenant/bank-card/update', 'city_id' => $currentCityId]));

                    } elseif ($this->profileService->isUpdateCurrentProfile($paymentName, $typeIdFromPost, $profile)) {

                        $newProfile = $this->prepareNewProfile($typeIdFromPost, $profilePostData);
                        $newProfile->paymentName = $paymentName;
                        if ($newProfile->validate()) {
                            $this->profileService->updateProfile($paymentName, $newProfile);
                            $this->profileService->saveAdvanceSettings(user()->tenant_id, $currentCityId, $newProfile);
                            session()->setFlash('success', t('bank-card', 'Profile of payment gate was saved'));

                            $transaction->commit();

                            return $this->redirect(Url::to(['/tenant/bank-card/update', 'city_id' => $currentCityId]));
                        }

                    } elseif ($this->profileService->isUpdateTypeProfile($paymentName, $typeIdFromPost, $profile)) {

                        $newProfile = $this->prepareNewProfile($typeIdFromPost, $profilePostData);

                        if ($newProfile->validate()) {
                            $this->profileService->deleteProfile($paymentName);
                            $this->profileService->createProfile($newProfile, $currentCityId);
                            $this->profileService->saveAdvanceSettings(user()->tenant_id, $currentCityId, $newProfile);
                            session()->setFlash('success', t('bank-card', 'Profile of payment gate was saved'));

                            $transaction->commit();

                            return $this->redirect(Url::to(['/tenant/bank-card/update', 'city_id' => $currentCityId]));
                        }

                    }
                }
                if (null === $newProfile) {
                    $newProfile = $this->profileService->getProfileModel(Disabled::TYPE_ID);
                }
                $profile = $newProfile;
                session()->setFlash('error', implode(' ', $newProfile->getFirstErrors()));
            }

        } catch (\Exception $ex) {
            $transaction->rollBack();
            session()->setFlash('error',
                t('bank-card', 'An error occurred while working with payment gate settings'));
            Yii::error($ex, 'bank-card');
        }

        $types = ArrayHelper::merge([
            ['id' => 0, 'name' => t('bank-card', 'Disable')],
        ], $profileTypes);

        $this->profileService->loadAdvanceSettings(user()->tenant_id, $currentCityId, $profile);

        return $this->render('update', [
            'profile'       => $profile,
            'cities'        => $cities,
            'currentCityId' => $currentCityId,
            'profileTypes'  => ArrayHelper::map($types, 'id', 'name'),
            'isAllowed'     => $isAllowed,
        ]);
    }

    protected function prepareNewProfile($type, $profilePostData)
    {
        $newProfile = $this->profileService->getProfileModel($type);
        $newProfile->load($profilePostData);

        return $newProfile;
    }


    /**
     * @param string $ajaxSearch
     *
     * @return string
     * @throws UnknownProfileTypeException
     * @throws ClientException
     * @throws InvalidParamException
     */
    public function actionYandexFailedPayments($ajaxSearch = null)
    {
        list($fromTime, $toTime) = $this->getFilteredDates(post());

        $paymentName = user()->tenant_id;
        $profile = $this->profileService->getProfile($paymentName);
        $shopId = isset($profile->secureDealShopId) ? $profile->secureDealShopId : null;
        $payments = $this->profileService->getYandexFailedPayments($paymentName, $shopId, $fromTime, $toTime);

        if ($ajaxSearch === null) {
            return $this->render('yandex/failedPayments', compact('payments'));
        }

        return $this->renderAjax('yandex/_grid', compact('payments'));
    }

    /**
     * Getting filtered dates
     *
     * @param $data
     *
     * @return array
     */
    private function getFilteredDates($data = null)
    {
        $filter = isset($data['filter']) ? $data['filter'] : null;
        switch ($filter) {
            case self::FILTER_MONTH:
                $dateFrom = strtotime(date('1.m.Y'));

                return [$dateFrom, strtotime('+1month -1second', $dateFrom)];
            case self::FILTER_PERIOD:
                $dateFrom = isset($data['first_date']) ? $data['first_date'] : null;
                $dateTo = isset($data['second_date']) ? $data['second_date'] : null;

                return [strtotime($dateFrom), strtotime('+1day -1second', strtotime($dateTo))];
            case self::FILTER_TODAY:
            default:
                $dateFrom = strtotime(date('d.m.Y'));

                return [$dateFrom, strtotime('+1day -1second', $dateFrom)];
        }
    }
}
