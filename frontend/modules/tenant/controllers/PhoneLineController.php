<?php

namespace app\modules\tenant\controllers;

use common\components\voipApi\VoipApi;
use Yii;
use app\modules\tenant\models\PhoneLine;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\modules\tariff\models\TaxiTariff;
use app\modules\tenant\models\PhoneLineSearch;
use common\modules\city\models\City;
use frontend\components\behavior\CityListBehavior;
use yii\data\ArrayDataProvider;

/**
 * PhoneLineController implements the CRUD actions for PhoneLine model.
 */
class PhoneLineController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs'    => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access'   => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['list', 'update'],
                        'allow'   => true,
                        'roles'   => ['read_phoneLine'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['phoneLine'],
                    ],
                ],
            ],
            'cityList' => [
                'class' => CityListBehavior::className(),
            ],
        ];
    }

    private function getListData($searchParams, $block = false)
    {
        $cityList                    = $this->getUserCityList();
        $searchModel                 = new PhoneLineSearch();
        $searchModel->accessCityList = array_keys($cityList);
        $dataProvider                = new ArrayDataProvider([
            'allModels'  => $searchModel->search($searchParams, $block)->asArray()->all(),
            'sort'       => [
                'attributes' => ['phone'],
            ],
            'pagination' => false,
        ]);
        $pjaxId                      = $block ? 'phone-line_blocked' : 'phone-line';

        return compact('searchModel', 'dataProvider', 'cityList', 'pjaxId');
    }

    public function actionList()
    {
        return $this->render('index', $this->getListData(Yii::$app->request->queryParams));
    }

    public function actionListBlocked()
    {
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_grid', $this->getListData(Yii::$app->request->queryParams, 1));
        }

        return $this->redirect(['list', '#' => 'blocked']);

    }

    /**
     * Creates a new PhoneLine model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PhoneLine();
        $model->scenario = PhoneLine::SCENARIO_CREATE;
        $model->tenant_id = user()->tenant_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            session()->setFlash('success', t('phone_line', 'The phone line successfully added'));

            return $this->redirect('list');
        } else {
            $user_city_list = user()->getUserCityList();
            $model->city_id = getValue($model->city_id, key($user_city_list));
            $tariff_list    = TaxiTariff::getTariffMap($model->city_id);

            return $this->render('create', compact('model', 'user_city_list', 'tariff_list'));
        }
    }

    public function actionTariffsByCity($city_id, $line_id)
    {
        $options       = TaxiTariff::getTariffMap($city_id);
        $currentTariff = $line_id ? $this->findModel($line_id) : null;
        $response      = [];

        foreach ($options as $key => $value) {
            $response[$key] = ['value'     => $value,
                               'isCurrent' => is_object($currentTariff) && $currentTariff->tariff_id == $key ? 1 : 0,
            ];
        }

        return json_encode($response);
    }

    /**
     * Updates an existing PhoneLine model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $transaction = app()->db->beginTransaction();

            if ($model->save()) {

                $resp = [];

                if (!$model->isDraft) {
                    $paramsByVoipApi = $model->getParamsByVoipApi(PhoneLine::TYPE_UPDATE);

                    $resp            = app()->get('voipApi')->setPhoneLine($paramsByVoipApi, VoipApi::TYPE_UPDATE);
                }

                if ($resp['state'] === VoipApi::STATE_ERROR) {
                    session()->setFlash('error', t('error', 'Error saving. Notify to administrator, please.'));
                    Yii::error($resp['message']);
                } else {
                    $transaction->commit();
                    session()->setFlash('success', t('app', 'Data updated successfully'));

                    return $this->redirect(['update', 'id' => $model->line_id]);
                }
            }
            $transaction->rollBack();
        }

        $user_city_list = user()->getUserCityList();
        $model->city_id = getValue($model->city_id, key($user_city_list));
        $tariff_list    = TaxiTariff::getTariffMap($model->city_id);

        return $this->render('update', compact('model', 'user_city_list', 'tariff_list'));

    }

    /**
     * Finds the TenantSetting model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param string $id
     *
     * @return TenantSetting the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PhoneLine::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Action for filtering and sorting model.
     * @return html
     */
    public function actionSearch()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $search_params          = post();
        $search_params['block'] = $search_params['view'] == 'blocked' ? 1 : 0;
        $search_model           = new PhoneLineSearch();
        $dataProvider           = $search_model->search($search_params);
        $phone_lines            = $dataProvider->getModels();

        return $this->renderAjax('_phone_list_tr', compact('phone_lines'));
    }

}
