<?php

namespace app\modules\tenant\controllers;

use common\modules\city\models\City;
use common\modules\tenant\models\DefaultSettings;
use common\modules\tenant\models\TenantHasCity;
use common\modules\tenant\models\TenantSetting;
use frontend\components\behavior\CityListBehavior;
use frontend\modules\employee\components\position\PositionService;
use frontend\modules\tenant\models\TenantHasCityForm;
use frontend\modules\tenant\models\TenantHasCitySearch;
use Yii;
use yii\base\Module;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * CityController implements the CRUD actions for TenantHasCity model.
 */
class CityController extends Controller
{
    private $positionService;

    public function behaviors()
    {
        return [
            'access'   => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['list', 'list-blocked', 'update'],
                        'roles'   => ['read_cities'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['cities'],
                    ],
                ],
            ],
            'cityList' => [
                'class' => CityListBehavior::className(),
            ],
        ];
    }

    public function __construct($id, Module $module, PositionService $positionService, array $config = [])
    {
        $this->positionService = $positionService;
        $this->positionService->setTenantId(user()->tenant_id);

        parent::__construct($id, $module, $config);
    }

    private function getListData($block = false)
    {
        $model        = new TenantHasCitySearch;
        $data         = $model->search($block);
        $dataProvider = new ArrayDataProvider([
            'allModels'  => $data->asArray()->all(),
            'sort'       => [
                'attributes' => [
                    'sort',
                    'city_id' => [
                        'asc'  => ['city.name' . getLanguagePrefix() => SORT_ASC],
                        'desc' => ['city.name' . getLanguagePrefix() => SORT_DESC],
                    ],
                ],
            ],
            'pagination' => false,
        ]);
        $pjaxId       = $block ? 'filials_blocked' : 'filials';

        return compact('dataProvider', 'pjaxId');
    }

    public function actionList()
    {
        return $this->render('index', $this->getListData());
    }

    public function actionListBlocked()
    {
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_grid', $this->getListData(1));
        }

        return $this->redirect(['list', '#' => 'blocked']);

    }

    private function getSupportedLanguages()
    {
        $languages = app()->params['supportedLanguages'];
        array_walk($languages, function (&$value, $key) {
            $value = t('language', $value, null, $key);
        });

        return $languages;
    }

    /**
     * List of all blocked models.
     * @return mixed
     */
    public function actionShowBlocked()
    {
        return $this->renderAjax('_table', ['cities' => $this->getTenantCityList(1)]);
    }

    /**
     * Getting tenant city
     *
     * @param integer $block (1|0)
     *
     * @return array
     */
    private function getTenantCityList($block = 0)
    {
        return TenantHasCity::find()
            ->where(['tenant_id' => user()->tenant_id, 'block' => $block])
            ->with([
                'city' => function ($query) {
                    $query->select(['name' . getLanguagePrefix(), City::tableName() . '.city_id']);
                },
            ])
            ->orderBy('sort')
            ->all();
    }

    /**
     * @return array
     */
    private function getPositionMap()
    {
        $positions = $this->positionService->getAllPositions();

        return empty($positions) ? [] : ArrayHelper::map($positions, 'position_id', 'name');
    }

    /**
     * Creates a new TenantHasCityForm model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\base\InvalidParamException
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        $model = new TenantHasCityForm([
            'scenario'   => TenantHasCity::SCENARIO_INSERT,
            'currency'   => 1,
            'lang'       => 'ru',
        ]);

        if ($model->load(Yii::$app->request->post())) {
            $transaction = app()->db->beginTransaction();
            try {
                if ($model->save()) {
                    session()->setFlash('success', t('app', 'Data updated successfully'));
                    $transaction->commit();

                    return $this->redirect('list');
                }
                displayErrors([$model]);
            } catch (\yii\db\Exception $ex) {
                Yii::error("Ошибка в БД при сохранении филиала\n" . $ex->getMessage(), 'city');
                session()->setFlash('error', t('status_event', 'Error saving. Notify to administrator, please.'));
            }

            $transaction->rollBack();
        }

        $languages = $this->getSupportedLanguages();
        $positions = $this->getPositionMap();
        $activePositions = $model->getDefaultActivePositionIds();

        return $this->render('create', compact('model', 'positions', 'activePositions', 'languages'));
    }

    /**
     * Updates an existing TenantHasCityForm model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id City id
     *
     * @return mixed
     * @throws \yii\base\InvalidParamException
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model     = $this->findModel($id);
        $arSetting = TenantSetting::find()
            ->where([
                'tenant_id' => user()->tenant_id,
                'city_id'   => $model->city_id,
                'name'      => [
                    'CURRENCY',
                    'LANGUAGE',
                    'PHONE',
                    DefaultSettings::SETTING_SIGNAL_NEW_ORDER
                ],
            ])
            ->indexBy('name')
            ->all();

        if ($model->load(Yii::$app->request->post())) {
            $transaction = app()->db->beginTransaction();
            if ($model->save()) {
                foreach ($arSetting as $key => $value) {
                    switch ($key) {
                        case 'CURRENCY':
                            $value->value = $model->currency;
                            break;
                        case 'LANGUAGE':
                            $value->value = $model->lang;
                            break;
                        case 'PHONE':
                            $value->value = $model->phone;
                            break;
                        case DefaultSettings::SETTING_SIGNAL_NEW_ORDER:
                            $value->value = $model->isUseSignalNewOrder;
                            break;
                    }
                    $value->save(false, ['value']);
                }
                $transaction->commit();
                session()->setFlash('success', t('app', 'Data updated successfully'));

                return $this->refresh();
            }
            $transaction->rollBack();
            displayErrors([$model]);
        }

        $model->currency = getValue($_POST['TenantSetting']['currency'], getValue($arSetting['CURRENCY']->value));
        $model->lang     = getValue($_POST['TenantSetting']['lang'], getValue($arSetting['LANGUAGE']->value));
        $model->phone    = getValue($_POST['TenantSetting']['phone'], getValue($arSetting['PHONE']->value));

        $model->isUseSignalNewOrder = getValue(
            $_POST['TenantSetting']['isUseSignalNewOrder'],
            getValue($arSetting[DefaultSettings::SETTING_SIGNAL_NEW_ORDER]->value)
        );

        $languages       = $this->getSupportedLanguages();
        $positions       = $this->getPositionMap();
        $activePositions = $model->getActivePositionIds();

        return $this->render('update', compact('model', 'positions', 'activePositions', 'languages'));
    }

    /**
     * @param integer $city_id
     *
     * @return TenantHasCityForm
     * @throws NotFoundHttpException
     */
    protected function findModel($city_id)
    {
        if (($model = TenantHasCityForm::findOne([
                'tenant_id' => user()->tenant_id,
                'city_id'   => $city_id,
            ])) !== null
        ) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
