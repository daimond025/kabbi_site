<?php

namespace app\modules\tenant\controllers;

use Yii;
use frontend\modules\tenant\models\SmsTemplate;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
/**
 * SmsTemplateController implements the CRUD actions for SmsTemplate model.
 */
class SmsTemplateController extends Controller
{
    public $defaultAction = 'list';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['list'],
                        'roles'   => ['read_settings'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['settings'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all SmsTemplate models.
     * @return mixed
     */
    public function actionList()
    {
        return $this->render('index', [
            'models' => SmsTemplate::findAll(['tenant_id' => user()->tenant_id]),
        ]);
    }

    /**
     * Updates an existing SmsTemplate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $position_id, $notify_type)
    {
        if (!Yii::$app->request->isAjax)
            return false;

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()))
        {
            if($model->save())
                return t('app', 'Data updated successfully');

            return t('app', 'Save error');
        }
        $form = app()->user->can('settings') ? '' : '_read';

        return $this->renderAjax('update' . $form, [
            'model' => $model,
            'defaultTemplate' => SmsTemplate::getDefaulTemplate($model->type, $position_id),
        ]);
    }

    /**
     * Finds the SmsTemplate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SmsTemplate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SmsTemplate::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
