<?php

namespace app\modules\tenant\controllers;

use app\modules\order\models\OrderViews;
use app\modules\tenant\models\EmailUserConfirm;
use app\modules\tenant\models\LoginForm;
use app\modules\tenant\models\PasswordResetRequestForm;
use app\modules\tenant\models\ResetPasswordForm;
use app\modules\tenant\models\User;
use app\modules\tenant\models\UserLoginDataForm;
use app\modules\tenant\models\UserPosition;
use app\modules\tenant\models\UserPositionForm;
use app\modules\tenant\models\UserSearch;
use common\components\services\users\community\CommunityUserService;
use common\models\LoginMembersOfTenantStrategy;
use common\modules\tenant\models\Tenant;
use common\modules\tenant\models\TenantHasCity;
use frontend\components\behavior\CityListBehavior;
use frontend\modules\companies\components\repositories\TenantCompanyRepository;
use frontend\modules\companies\components\services\CompanyCheck;
use frontend\modules\employee\models\worker\Worker;
use frontend\modules\tenant\components\services\PermissionsService;
use frontend\modules\tenant\exceptions\RoleException;
use frontend\modules\tenant\models\UserDefaultRight;
use frontend\modules\tenant\models\UserDispetcher;
use frontend\modules\tenant\models\UserHasCity;
use Yii;
use yii\base\InvalidParamException;
use yii\caching\TagDependency;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{

    protected $communityUserService;

    public function __construct(
        $id,
        $module,
        CommunityUserService $communityUserService,
        array $config = []
    ) {
        parent::__construct($id, $module, $config);

        $this->communityUserService = $communityUserService;
    }

    public function behaviors()
    {
        return [
            'access'   => [
                'class'  => AccessControl::className(),
                'except' => [
                    'login',
                    'cross-login',
                    'logout',
                    'request-password-reset',
                    'request-password-reset-by-landing',
                    'reset-password',
                    'confirm-email',
                    'error-translation',
                    'does-email-exists',
                    'external-incoming-call',
                ],
                'rules'  => [
                    [
                        'actions' => [
                            'update',
                        ],
                        'allow'   => false,
                        'roles'   => [User::ROLE_EXTRINSIC_DISPATCHER],
                    ],
                    [
                        'actions' => [
                            'update',
                            'list',
                            'show-blocked',
                            'show-invited',
                            'search',
                            'change-login-data',
                            'check-login-data',
                            'change-user-position',
                            'list-online-json',
                        ],
                        'allow'   => true,
                        'roles'   => ['read_users'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['users'],
                    ],
                    [
                        'actions' => ['get-photo-url', 'get-chat-server'],
                        'allow'   => true,
                    ],
                ],
            ],
            'cityList' => [
                'class' => CityListBehavior::className(),
            ],
        ];
    }

    private function getSupportedLanguages()
    {
        $languages = app()->params['supportedLanguages'];
        array_walk($languages, function (&$value, $key) {
            $value = t('language', $value, null, $key);
        });

        return $languages;
    }

    private function getListData($searchParams, $type = 'active')
    {
        $cityList                    = $this->getUserCityList();
        $positionList                = UserPosition::getUserPositionList();
        $searchModel                 = new UserSearch(['type' => $type]);
        $searchModel->accessCityList = array_keys($cityList);

        if (!empty($searchParams['WorkerSearch']['positionList'])) {
            $searchModel->positionActivity = 1;
        }

        $dataProvider = $searchModel->search($searchParams);
        $pjaxId       = 'user_' . $type;

        $tenantCompaniesList = TenantCompanyRepository::selfCreate()
            ->getForForm(['tenant_id' => user()->tenant_id]);

        return compact('searchModel', 'dataProvider', 'cityList', 'pjaxId', 'positionList', 'type',
            'tenantCompaniesList');
    }

    /**
     * Lists all active User models.
     * @return mixed
     */
    public function actionList()
    {
        return $this->render('index', $this->getListData(Yii::$app->request->queryParams));
    }

    /**
     * Lists all invited User models.
     * @return mixed
     */
    public function actionInvited()
    {
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_grid', $this->getListData(Yii::$app->request->queryParams, 'invited'));
        }

        return $this->redirect(['list', '#' => 'invited']);
    }

    /**
     * Lists all active User models.
     * @return mixed
     */
    public function actionListOnlineJson()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $usersOnline = User::getOnlineStaffJson();

        Yii::$app->response->format = Response::FORMAT_JSON;

        return $usersOnline;
    }

    /**
     * Lists all blocked User models.
     * @return mixed
     */
    public function actionBlocked()
    {
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_grid', $this->getListData(Yii::$app->request->queryParams, 'blocked'));
        }

        return $this->redirect(['list', '#' => 'blocked']);
    }

    private function getFormCityList($curUserRole)
    {
        if (in_array($curUserRole, [User::ROLE_BRANCH_DIRECTOR, User::ROLE_STAFF_COMPANY])) {
            return user()->getUserCityList();
        } else {
            return ArrayHelper::map(TenantHasCity::getCityList(false),
                'city_id', 'city.name' . getLanguagePrefix());
        }
    }

    /**
     * @param $id
     *
     * @return array|null|User
     * @throws NotFoundHttpException
     */
    protected function findUserModel($id)
    {
        $model = User::find()
            ->where(['user_id' => $id, 'tenant_id' => user()->tenant_id])
            ->with(['cities', 'position'])
            ->one();

        if (is_null($model)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $model;
    }

    /**
     * Displays a single User model for view or update.
     *
     * @param integer $id user_id
     *
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        (new CompanyCheck())->isAccess(User::findOne($id));

        $model       = $this->findUserModel($id);
        $canEdit     = app()->user->can('users', ['user_id' => $id]) || $id == user()->id;
        $curUserRole = user()->getUserRole();
        $city_list   = $this->getFormCityList($curUserRole);

        $post = Yii::$app->request->post();
        $user = $post['User'];
        if ($model->load($post)) {
            if (!$canEdit) {
                throw new ForbiddenHttpException;
            }

            if (!$model->isUserInvited()) {
                $model->active = $model->block ? 0 : 1;
            }

            if ($model->validate()) {
                if (!empty($_POST['User']['birth_year']) && !empty($_POST['User']['birth_month']) && !empty($_POST['User']['birth_day'])) {
                    $model->birth = $_POST['User']['birth_year'] . '-' . $_POST['User']['birth_month'] . '-' . $_POST['User']['birth_day'];
                } else {
                    $model->birth = null;
                }

                $model->setCropParams(post('Crop'));

                if ($model->save(false)) {
                    //Сохранение городов работы пользователя
                    if ($user['city_list']) {
                        $user_city_list = empty($user['city_list']) ? array_keys($city_list) : $user['city_list'];
                        UserHasCity::deleteAll(['user_id' => $model->user_id]);
                        UserHasCity::manySave($user_city_list, $model->user_id);
                        foreach ($user_city_list as $city_id) {
                            app()->cache->delete('user_list_' . $model->tenant_id . '_' . $city_id);
                        }
                        //Корректировка таблицы счетчиков заказов вследствии изменнения городов
                        if ($model->position_id != UserPosition::POSITION_BOOKKEEPER && $model->position_id != UserPosition::POSITION_LEADBOOKKEEPER) {
                            OrderViews::refreshRows($id, $model->tenant_id, $user_city_list);
                        }
                    }

                    $this->communityUserService->synchronizationCommunityUserWithUser($model);

                    session()->setFlash('success', t('app', 'Data updated successfully', [], user()->lang));

                    return $this->refresh();
                }
            }
        }

        list($model->birth_year, $model->birth_month, $model->birth_day) = !empty($model->birth) ? explode('-',
            $model->birth) : null;
        $model->city_list = ArrayHelper::map($model->cities, 'city_id', 'city_id');
        $model->online    = (time() - $model->active_time) < app()->params['user.online.ttlSec'] ? 'on' : 'off';
        $model->block     = !$model->active;
        $languages        = $this->getSupportedLanguages();
        //get first intercected  city_id of current user and other user
        $intersectedCityId = null;
        $intersectedCitys  = array_intersect_key(user()->getUserCityList(), $model->city_list);
        if (count($intersectedCitys) > 0) {
            reset($intersectedCitys);
            $intersectedCityId = key($intersectedCitys);
        }

        return $this->render('update',
            compact('model', 'city_list', 'curUserRole', 'languages', 'canEdit', 'intersectedCityId'));
    }

    protected function getUserPositionForm($id)
    {
        return new UserPositionForm(['user_id' => $id]);
    }

    public function actionChangeUserPosition($id)
    {
        $model = $this->getUserPositionForm($id);

        if (Yii::$app->request->isPost) {
            if (!$model->canEdit()) {
                throw new ForbiddenHttpException;
            }

            if ($model->load(post()) && $model->validate()) {

                if (app()->user->can(User::ROLE_STAFF_COMPANY)) {
                    $permissionService = (new PermissionsService())
                        ->setUser($model->getUser())
                        ->setUpdatedRights($model->rights);
                    $elevatedRights    = $permissionService->getElevatedRights();

                    if ($elevatedRights) {
                        session()->setFlash(
                            'success',
                            t('tenant_company',
                                'Raising user rights, access is denied',
                                [],
                                user()->lang
                            )
                        );

                        return $this->redirect(['update', 'id' => $id, '#' => 'rights_and_access']);
                    }
                }

                $model->save();

                session()->setFlash('success', t('app', 'Data updated successfully', [], user()->lang));

                return $this->redirect(['update', 'id' => $id, '#' => 'rights_and_access']);
            }
        }

        return $this->renderAjax('_positionForm', compact('model'));
    }

    protected function getUserLoginDataForm($id)
    {
        return new UserLoginDataForm(['user_id' => $id, 'tenant_id' => user()->tenant_id]);
    }

    public function actionChangeLoginData($id)
    {
        $loginDataForm = $this->getUserLoginDataForm($id);

        if (Yii::$app->request->isPost) {
            if (user()->user_id != $id) {
                throw new ForbiddenHttpException;
            }

            if ($loginDataForm->load(post()) && $loginDataForm->save()) {
                session()->setFlash('success', t('app', 'Data updated successfully', [], user()->lang));

                $this->communityUserService->synchronizationCommunityUserWithUser($loginDataForm->getUser());

                return $this->redirect(['update', 'id' => $id, '#' => 'email_and_password']);
            }
        }

        return $this->renderAjax('_passwordForm', ['model' => $loginDataForm]);
    }

    public function actionCheckLoginData($id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $loginDataForm = $this->getUserLoginDataForm($id);
        $loginDataForm->load(Yii::$app->request->post());

        return ActiveForm::validate($loginDataForm);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'update' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model       = new User(['scenario' => 'insert']);
        $curUserRole = user()->getUserRole();
        $city_list   = $this->getFormCityList($curUserRole);

        if ($model->load(Yii::$app->request->post())) {

            if (in_array($model->position_id, User::getExcludedPositionIdList())) {
                throw new ForbiddenHttpException();
            }

            if (app()->user->can(User::ROLE_STAFF_COMPANY)) {
                $permissionService = (new PermissionsService())
                    ->setUser($model)
                    ->setUpdatedRights($model->rights);
                $elevatedRights    = $permissionService->getElevatedRights();

                if ($elevatedRights) {
                    session()->setFlash(
                        'success',
                        t('tenant_company',
                            'Raising user rights, access is denied',
                            [],
                            user()->lang
                        )
                    );

                    return $this->redirect(['create', '#' => 'rights_and_access']);
                }

                $model->tenant_company_id = user()->tenant_company_id;
            }

            $password = Yii::$app->security->generateRandomString(10);
            $model->setEmailToken();
            $model->setPassword($password);
            $model->generateAuthKey();
            $model->setAccessToken();
            $model->tenant_id = user()->tenant_id;

            //Генерация хешей для авторизации в софтфоне
            $operator_name = UserDispetcher::createOperatorName();
            $model->ha1    = UserDispetcher::generateHa1Hash($operator_name, $password);
            $model->ha1b   = UserDispetcher::generateHa1bHash($operator_name, $password);
            $model->setOperatorPasswordHash($operator_name, $password);

            $model->setCropParams(post('Crop'));

            try {
                if ($model->save()) {
                    //Сохранение городов работы пользователя
                    $user_city_list = empty($_POST['User']['city_list']) ? array_keys($city_list) : $_POST['User']['city_list'];
                    $res            = UserHasCity::manySave($user_city_list, $model->user_id);

                    if (!$res) {
                        Yii::error('Ошибка сохранения городов работы для сотрудника арендатора!');
                    }

                    //Отправка письма для подтверждения
                    $model->sendInvite($model->lang, $password);

                    //Сохранение прав
                    $model->assignRights();

                    session()->setFlash('success', t('user', 'Invitation has been sent to user.'));

                    return $this->redirect('list');
                }
            } catch (RoleException $e) {
                Yii::error($e->getMessage());
                session()->setFlash('success', t('tenant_company', 'Access denied.'));
            }
        }

        $model->position_list = translateAssocArray('company-roles', $model->getPositionList());

        $languages = $this->getSupportedLanguages();
        $companies = TenantCompanyRepository::selfCreate()->getForForm([], false);

        $model->tenant_company_id = count($companies) === 1 ? current(array_keys($companies)) : null;

        if (!Tenant::getHasTenantCities()) {
            return $this->render('not-create');
        }

        return $this->render('create', compact('model', 'city_list', 'languages', 'companies'));

    }

    /**
     * User confirm email
     *
     * @param string $token token for confirm email
     * @param string $lang
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionConfirmEmail($token)
    {
        $lang = get('amp;lang');

        $emailConfirm = new EmailUserConfirm();

        if ($emailConfirm->confirm($token)) {
            session()->setFlash('success', t('app', 'You have successfully registered in the system.', [], $lang));

            return $this->redirect(['update', 'id' => user()->user_id]);
        } else {

            return $this->redirect(['login', 'reason' => 'failedConfirmEmail']);
        }
    }

    public function actionLogin()
    {
        if (Yii::$app->request->isAjax) {
            return false;
        }

        $lang = (new LoginMembersOfTenantStrategy(get('lang')))->getLanguage();

        $resetPwdModel       = new PasswordResetRequestForm();
        $resetPwdModel->lang = $lang;

        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = '//login';

        $model = new LoginForm();

        $model->lang = $lang;

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if (user()->is_dispatcher()) {
                return $this->redirect('/order/operator/');
            }

            return $this->goBack();
        }
        $domain = current(explode('.', $_SERVER['HTTP_HOST']));
        $tenant = $this->getTenantByDomain($domain);

        $info = '';
        if (app()->request->isGet && app()->request->get('reason') === 'failedConfirmEmail') {
            $info = t('app',
                'Your reference link has expired or was used.',
                [], $lang);
        }

        return $this->render('login', [
            'resetPwdModel' => $resetPwdModel,
            'model'         => $model,
            'tenant'        => $tenant,
            'info'          => $info
        ]);
    }

    public function actionCrossLogin($user_id, $auth_key)
    {
        if (!\Yii::$app->user->isGuest) {
            $this->logout();
        }

        $user = User::findIdentityByAuthKey($user_id, $auth_key);

        if (!empty($user) && Yii::$app->user->login($user)) {
            return $this->goHome();
        }

        return $this->goBack();
    }


    /**
     * Link for auth users by access_token and redirect them to operator form
     *
     * @param $access_token
     * @param $clid
     * @param $did
     * @param $call_id
     *
     * @return Response
     */
    public function actionExternalIncomingCall($access_token, $clid, $did, $call_id)
    {
        /* @var $user User */
        $user = User::findIdentityByAccessToken($access_token, User::className());

        if ($user) {
            $currentDomain = domainName();
            $currentTenantId = Tenant::getIdByDomain($currentDomain);
            $urlRedirect = '/order/operator/call/?did=' . $did . '&clid=' . $clid . '&call_id=' . $call_id;

            if ($user->isExtrinsicDispatcher()) {
                $extrinsicDispatcher = User::findExtrinsicDispatcher($user->email, $currentTenantId);
                if ($extrinsicDispatcher && Yii::$app->user->loginByAccessToken($access_token, get_class($this))) {
                    return $this->redirect($urlRedirect);
                }
            } elseif ($user->tenant_id == $currentTenantId && Yii::$app->user->loginByAccessToken($access_token, get_class($this))) {
                return $this->redirect($urlRedirect);
            }

        }

        return $this->goHome();
    }

    protected function logout()
    {
        if (in_array(user()->position_id, User::getOperatorPositions())) {
            UserDispetcher::dispetcherStop();
        }
        Yii::$app->user->logout();
    }

    public function actionLogout()
    {
        $this->logout();

        return $this->goHome();
    }

    public function actionRequestPasswordReset($login = '', $lang = null)
    {
        $model = new PasswordResetRequestForm();
        if (Yii::$app->request->isAjax) {
            $model->email = $login;
            $model->lang  = $lang;

            $domain   = domainName();
            $tenant = $this->getTenantByDomain($domain);

            /** @var User $user */
            $user     = User::findByEmail($login, $domain);
            $cities = $user->cities;

            $model->tenantId = $tenant->tenant_id;
            $model->cityId   = ArrayHelper::getValue($cities, '0.city_id');

            if ($model->validate()) {
                if ($model->sendEmail()) {
                    return json_encode(t('app', 'Email was sent to the the address you provided', [], $lang));
                }
            } else {
                dd($model->getErrors());
            }
        }

        $this->layout = '//signup';
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success',
                    t('app', 'Check your email for further instructions.', [], $lang));

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error',
                    t('app', 'Sorry, we are unable to reset password for email provided.', [], $lang));
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionRequestPasswordResetByLanding()
    {
        $model    = new PasswordResetRequestForm();
        $responce = ['error' => null];

        if ($model->load(Yii::$app->request->get())) {
            if (!($model->validate() && $model->sendEmail())) {
                $responce['error'] = $model->errors;
            }
        }

        return json_encode($responce);
    }

    public function actionResetPassword($token, $lang = null)
    {
        try {
            $model       = new ResetPasswordForm($token);
            $model->lang = $lang;
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', t('app', 'New password was saved.', [], $lang));

            return $this->goHome();
        }

        $this->layout = '//login';
        $domain       = current(explode('.', $_SERVER['HTTP_HOST']));
        $tenant       = $this->getTenantByDomain($domain);

        return $this->render('resetPassword', [
            'model'  => $model,
            'tenant' => $tenant,
        ]);
    }

    /**
     * Ajax user seacrh from filter data
     * @return mixed|bool
     */
    public function actionSearch()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $view         = !empty($_POST['view']) ? '_' . $_POST['view'] : '_index';
        $user_search  = new UserSearch();
        $dataProvider = $user_search->search($_POST);
        $arUsers      = $user_search->getGridData($dataProvider->getModels(), false);

        return $this->renderAjax($view, ['users' => $arUsers['USERS']]);
    }

    /**
     * Send the invitation again
     */
    public function actionSendInvite()
    {
        $result = 'error';

        if ($user_id = post('user_id')) {
            $user     = User::find()->select('tenant_id, user_id, email, name, second_name, last_name, lang')->where(['user_id' => $user_id])->one();
            //$user->email = 'daimond025@yandex.ru';
            $password = Yii::$app->security->generateRandomString(10);
            $user->setEmailToken();
            $user->setPassword($password);

            if ($user->save(false)) {
                $user->sendInvite($user->lang, $password);
                $result = 'ok';
            } else {
                Yii::error('Ошибка отправки повторного приглашения пользователя user_id = ' . $user_id);
            }
        }

        return json_encode($result);
    }

    /**
     * Cancel user invite
     */
    public function actionCancelInvite()
    {
        $result = 'error';

        if ($user_id = post('user_id')) {
            $user = User::findOne($user_id);
            if ($user->delete()) {
                $result = 'ok';
            } else {
                Yii::error('Ошибка отмены приглашения пользователя user_id = ' . $user_id);
            }
        }

        return json_encode($result);
    }

    /**
     * Unblock user.
     */
    public function actionUnblock()
    {
        $result = 'error';

        if ($user_id = post('user_id')) {
            $user         = User::findOne($user_id);
            $user->active = 1;

            if ($user->save(false)) {
                $result = 'ok';
            } else {
                Yii::error('Ошибка разблокировки пользователя user_id = ' . $user_id);
            }
        }

        return json_encode($result);
    }

    public function actionGetDefaultRights($position_id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        return json_encode(UserDefaultRight::find()->where(['position_id' => $position_id])->select([
            'permission',
            'rights',
        ])->asArray()->all());
    }

    public function actionGetPhotoUrl()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        $tenantId   = user()->tenant_id;
        $senderType = Yii::$app->request->post('sender_type');
        $senderId   = Yii::$app->request->post('sender_id');
        if ($senderType == 'worker') {
            $workerData = Worker::getDb()->cache(function ($db) use ($tenantId, $senderId) {
                return Worker::find()
                    ->where(['tenant_id' => $tenantId])
                    ->andWhere(['callsign' => $senderId])
                    ->one();
            }, null, new TagDependency(['tags' => Worker::TAG_DEPENDENCY_PREFIX . $senderId . '_' . $tenantId]));
            if (isset($workerData->photo) && !empty(trim($workerData->photo))) {
                return json_encode([
                    'id'  => isset($workerData->worker_id) ? $workerData->worker_id : null,
                    'url' => $workerData->isFileExists($workerData->photo) ? $workerData->getPictureHtml($workerData->photo,
                        false) : '',
                ]);
            } else {
                return json_encode([
                    'id'  => isset($workerData->worker_id) ? $workerData->worker_id : null,
                    'url' => "",
                ]);
            }
        } else {
            $userData = User::getDb()->cache(function ($db) use ($tenantId, $senderId) {
                return User::find()
                    ->where(['tenant_id' => $tenantId])
                    ->andWhere(['user_id' => $senderId])
                    ->one();
            }, null, new TagDependency(['tags' => User::TAG_DEPENDENCY_PREFIX . $senderId]));
            if (isset($userData->photo) && !empty(trim($userData->photo))) {
                return json_encode([
                    'id'  => isset($userData->user_id) ? $userData->user_id : null,
                    'url' => $userData->isFileExists($userData->photo) ? $userData->getPictureHtml($userData->photo,
                        false) : '',
                ]);
            } else {
                return json_encode([
                    'id'  => isset($userData->user_id) ? $userData->user_id : null,
                    'url' => "",
                ]);
            }
        }
    }

    public function actionGetChatServer()
    {
        $chatUrl = isset(app()->params['chatSocketServerUrl']) ? app()->params['chatSocketServerUrl'] : null;

        return $chatUrl;
    }

    public function actionGetChatConnectError()
    {
        return t('app', 'The chat is temporarily inaccessible. Please, tell to the administrator');
    }

    public static function actionErrorTranslation($lang=null)
    {
        $lang = (new LoginMembersOfTenantStrategy($lang))->getLanguage();

        return json_encode(t('app', 'Email cannot be blank', [], $lang));
    }

    public static function actionDoesEmailExists($email = '', $lang=null)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return json_encode(t('app', 'Invalid email format', [], $lang));
        }
        $user = User::findByEmailAndTenantId($email, User::getTenantIdByNameHost());
        if (empty($user)) {
            return json_encode(t('app', 'Email does not exists', [], $lang));
        } else {
            return json_encode(t('app', 'ok', [], $lang));
        }
    }

    private function getTenantByDomain($domain)
    {
        $tenant = Tenant::find()->where(['domain' => $domain])->select(['tenant_id', 'logo', 'company_name'])->one();

        if (empty($tenant)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $tenant;
    }

    public function actionGetAccessToken()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        app()->response->format = Response::FORMAT_JSON;

        return ['token' => user()->access_token];
    }

}
