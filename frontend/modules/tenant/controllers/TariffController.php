<?php

namespace app\modules\tenant\controllers;

use app\modules\balance\models\Account;
use common\modules\tenant\modules\tariff\models\TenantAdditionalOption;
use common\modules\tenant\modules\tariff\models\TenantTariff;
use frontend\modules\tenant\models\PaymentHistory;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class TariffController extends Controller
{
    const FILTER_TODAY = 'today';
    const FILTER_MONTH = 'month';
    const FILTER_PERIOD = 'period';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'payment-history'],
                        'roles'   => ['read_gootaxTariff'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['gootaxTariff'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Getting balance information
     * @return array|null|\yii\db\ActiveRecord
     */
    private function getBalanceInfo()
    {
        return Account::find()
            ->select(['balance', 'currency_id'])
            ->where([
                'acc_type_id' => Account::PASSIVE_TYPE,
                'acc_kind_id' => Account::TENANT_KIND,
                'tenant_id'   => user()->tenantId,
            ])->asArray()->one();
    }

    /**
     * Showing current tariff info and payment history
     * @return mixed
     */
    public function actionIndex()
    {
        $user          = user();
        $tenantTariffs = array_filter(TenantTariff::getTenantTariffs(time(), $user->tenant_id, app()->cache),
            function ($value) {
                return isset($value['tariff']);
            });

        $tariffs = implode(', ', ArrayHelper::getColumn($tenantTariffs, function ($model) {
            return $model['tariff']['name'] . ' (' . \Yii::t('gootax_modules', $model['name']) . ')';
        }, false));

        $expiryDates = array_filter(ArrayHelper::getColumn($tenantTariffs, function ($tariff) {
            if (!isset($tariff['tariff']['expiry_date'])) {
                return false;
            } elseif (!empty($tariff['nextExpiryDate'])) {
                return $tariff['nextExpiryDate'];
            } else {
                return $tariff['tariff']['expiry_date'];
            }
        }, false),
            function ($value) {
                return !empty($value);
            });

        $additionalOptions = ArrayHelper::getColumn(
            TenantAdditionalOption::getTenantAdditionalOptions(time(), $user->tenant_id, app()->cache),
            'name');

        $balanceInfo = $this->getBalanceInfo();

        $isAllowed = app()->user->can('gootaxTariff');

        return $this->render('index',
            compact('tariffs', 'expiryDates', 'additionalOptions', 'balanceInfo', 'isAllowed'));
    }

    /**
     * Getting filtered dates
     *
     * @param $data
     *
     * @return array
     */
    private function getFilteredDates($data = null)
    {
        $filter = isset($data['filter']) ? $data['filter'] : null;
        switch ($filter) {
            case self::FILTER_MONTH:
                $dateFrom = strtotime(date('1.m.Y'));

                return [$dateFrom, strtotime('+1month -1second', $dateFrom)];
            case self::FILTER_PERIOD:
                $dateFrom = isset($data['first_date']) ? $data['first_date'] : null;
                $dateTo   = isset($data['second_date']) ? $data['second_date'] : null;

                return [strtotime($dateFrom), strtotime('+1day -1second', strtotime($dateTo))];
            case self::FILTER_TODAY:
            default:
                $dateFrom = strtotime(date('d.m.Y'));

                return [$dateFrom, strtotime('+1day -1second', $dateFrom)];
        }
    }

    /**
     * Getting payment history
     *
     * @param boolean $withoutSearch
     *
     * @return string
     */
    public function actionPaymentHistory($withoutSearch = null)
    {
        $paymentHistory = new PaymentHistory();

        list($dateFrom, $dateTo) = $this->getFilteredDates(post());
        $operations = $paymentHistory->getPaymentTransactions($dateFrom, $dateTo);

        if (empty($withoutSearch)) {
            return $this->renderAjax('paymentHistory', compact('operations'));
        } else {
            return $this->renderAjax('_grid', compact('operations'));
        }
    }

    /**
     * Tariff payment page
     * @return mixed
     */
    public function actionPayment()
    {
        return $this->render('payment');
    }
}