<?php

namespace app\modules\tenant\controllers;

use common\modules\tenant\models\DefaultSettings;
use common\modules\tenant\models\TenantSetting;
use frontend\modules\employee\components\position\PositionService;
use frontend\modules\tenant\models\worker\SettingsWorker;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

/**
 * SettingController implements the CRUD actions for TenantSetting model.
 */
class SettingController extends Controller
{
    private $positionService;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['list', 'get-worker'],
                        'roles'   => ['read_settings'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['settings'],
                    ],
                ],
            ],
        ];
    }

    public function __construct($id, Module $module, PositionService $positionService, array $config = [])
    {
        $this->positionService = $positionService;
        $this->positionService->setTenantId(user()->tenant_id);

        parent::__construct($id, $module, $config);
    }

    /**
     * @param int $cityId
     *
     * @return array
     */
    private function getPositionMap($cityId)
    {
        $positions = $this->positionService->getPositionsByCity($cityId);

        return empty($positions) ? [] : ArrayHelper::map($positions, 'position_id', 'name');
    }

    public function actionList($type, $city_id = null, $position_id = null)
    {
        $tenantId = user()->tenant_id;

        $cities = user()->getUserCityList();
        $cityId = empty($city_id) || !array_key_exists($city_id, $cities) ? key($cities) : $city_id;

        $positions  = $this->getPositionMap($cityId);
        $positionId = empty($position_id) || !array_key_exists($position_id,
            $positions) ? key($positions) : $position_id;

        $isAllowed = app()->user->can('settings');

        if ($isAllowed && isset($_POST[$type])) {
            if ($type == 'orders' && isset($_POST[$type]['NEW'][DefaultSettings::SETTING_WORKER_PRE_ORDER_REMINDER])) {
                $_POST[$type]['NEW'][DefaultSettings::SETTING_WORKER_PRE_ORDER_REMINDER] = serialize($_POST[$type]['NEW'][DefaultSettings::SETTING_WORKER_PRE_ORDER_REMINDER]);
            }

            foreach ($_POST[$type]['NEW'] as $key => $value) {
                $setting = TenantSetting::find()
                    ->where([
                        'tenant_id' => $tenantId,
                        'name'      => $key,
                        'type'      => $type,
                        'city_id'   => $cityId,
                    ])
                    ->andFilterWhere([
                        'position_id' => $positionId,
                    ])
                    ->one();

                if (empty($setting)) {
                    $setting = new TenantSetting([
                        'tenant_id'   => $tenantId,
                        'name'        => $key,
                        'type'        => $type,
                        'city_id'     => $cityId,
                        'position_id' => $positionId,
                    ]);
                }
                $setting->value = $value;
                $setting->save();
            }

            if (isset($_POST[$type]['NEW']['LANGUAGE'])) {
                app()->cache->delete(domainName());
            }

            //Галочки отжаты
            $array_diff = array_diff(array_keys($_POST[$type]['OLD']), array_keys($_POST[$type]['NEW']));


            if (!empty($array_diff)) {
                foreach ($array_diff as $key) {
                    TenantSetting::updateSettingValue($key, $tenantId, $cityId, $positionId, 0);
                }
            }
            TenantSetting::updateCachedSettings($tenantId, $type, $cityId, $positionId);
            session()->setFlash('success',
                t('app', 'Data updated successfully', null, $_POST[$type]['NEW']['LANGUAGE']));

            return $this->refresh();
        }
        $tenantSettings = TenantSetting::getCachedSettings($tenantId, $type, $cityId, $positionId);
        $form           = $isAllowed ? '' : '_read';

        $positionHasCar = $this->positionService->hasCarPositionId($positionId);

        return $this->render($type . $form, [
            'tenantSettings' => ArrayHelper::map($tenantSettings, 'name', 'value'),
            'settingType'    => $type,
            'cities'         => $cities,
            'cityId'         => $cityId,
            'positions'      => $positions,
            'positionId'     => $positionId,
            'positionHasCar' => $positionHasCar,
        ]);
    }

    public function actionGetWorker($city_id = null, $position_id = null)
    {
        $cities = user()->getUserCityList();
        $cityId = empty($city_id) || !array_key_exists($city_id, $cities) ? key($cities) : $city_id;
        $positions  = $this->getPositionMap($cityId);
        $positionId = empty($position_id) || !array_key_exists($position_id,
            $positions) ? key($positions) : $position_id;

        return $this->render('worker', [
            'tenantSetting' => new SettingsWorker($cityId, $positionId),
            'cities'        => $cities,
            'positions'     => $positions,
            'allowWrite'    => app()->user->can('settings'),
        ]);
    }

    public function actionUpdateWorker($city_id = null, $position_id = null)
    {
        $cities = user()->getUserCityList();
        $cityId = empty($city_id) || !array_key_exists($city_id, $cities) ? key($cities) : $city_id;

        $positions  = $this->getPositionMap($cityId);
        $positionId = empty($position_id) || !array_key_exists($position_id,
            $positions) ? key($positions) : $position_id;

        $settings = new SettingsWorker($cityId, $positionId);

        if ($settings->load(app()->request->post()) && $settings->save()) {
            session()->setFlash('success', t('app', 'Data updated successfully'));
        } else {
            session()->setFlash('error', t('setting', 'Failed to save worker settings'));
            \Yii::error(implode('; ', $settings->getFirstErrors()));
        }

        return $this->refresh();
    }

}
