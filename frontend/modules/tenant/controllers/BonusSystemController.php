<?php

namespace app\modules\tenant\controllers;

use frontend\modules\bonus\components\BonusSystemService;
use frontend\modules\bonus\models\BonusSystem;
use frontend\modules\bonus\models\TenantHasBonusSystem;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

/**
 * Class BonusSystemController
 * @package frontend\modules\tenant\controllers
 */
class BonusSystemController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['update'],
                        'roles'   => ['read_bonus'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Update bonus system
     * @throws \yii\base\InvalidParamException
     */
    public function actionUpdate()
    {
        $tenantId  = user()->tenant_id;
        $isAllowed = app()->user->can('bankCard');

        $bonusSystemId = (app()->request->post('TenantHasBonusSystem') !== null
            && array_key_exists('bonus_system_id', app()->request->post('TenantHasBonusSystem')))
            ? app()->request->post('TenantHasBonusSystem')['bonus_system_id']
            : BonusSystemService::BONUS_SYSTEM_ID_GOOTAX;

        $model = TenantHasBonusSystem::find()->where(['tenant_id' => $tenantId])->one();
        if ($model === null) {
            $model = new TenantHasBonusSystem(['tenant_id' => $tenantId]);
        }

        $model->scenario = (int)$bonusSystemId === BonusSystemService::BONUS_SYSTEM_ID_GOOTAX
            ? TenantHasBonusSystem::SCENARIO_DEFAULT
            : TenantHasBonusSystem::SCENARIO_UDS_GAME;

        if ($isAllowed && $model->load(app()->request->post())) {
            if ($model->validate()) {
                $model->save();

                session()->setFlash('success', t('bonus-system', 'Bonus system was saved'));

                return $this->refresh();
            } else {
                session()->setFlash('error', implode("\n", $model->getFirstErrors()));
            }
        }

        return $this->render('update', [
            'isAllowed'    => $isAllowed,
            'model'        => $model,
            'bonusSystems' => ArrayHelper::map(BonusSystem::find()->all(), 'id', 'name'),
        ]);
    }

}