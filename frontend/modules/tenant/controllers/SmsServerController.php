<?php

namespace app\modules\tenant\controllers;

use Yii;
use common\modules\tenant\models\TenantHasSms;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\modules\tenant\models\SmsServer;
use yii\helpers\ArrayHelper;

/**
 * SmsServerController implements the CRUD actions for TenantHasSms model.
 */
class SmsServerController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['list'],
                        'roles'   => ['read_settings'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['settings'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all TenantHasSms models.
     * @return mixed
     */
    public function actionList($city_id = null)
    {
        $user_city_list = user()->getUserCityList();
        $cur_city_id = getValue($city_id, key($user_city_list));

        $model = TenantHasSms::findOne(['tenant_id' => user()->tenant_id, 'city_id' => $cur_city_id]);

        if (empty($model)) {
            $model = new TenantHasSms;
        }

        $model->city_id = $cur_city_id;

        $isAllowed = app()->user->can('settings');

        if ($isAllowed && $model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                session()->setFlash('success', t('app', 'Data updated successfully'));
                return $this->refresh();
            }
        }
        $form = $isAllowed ? 'index' : 'index_read';
        return $this->render($form, [
                    'model' => $model,
                    'servers' => ArrayHelper::map(SmsServer::find()->orderBy('sort')->all(), 'server_id', 'name'),
                    'user_city_list' => $user_city_list,
        ]);
    }

    public function actionAjaxUpdateServerInfo($server_id = NULL)
    {
        return json_encode(ArrayHelper::toArray(\common\models\AdminSmsServer::findOne(['server_id' => $server_id])));
    }

    public function actionAjaxGetCurrencyFormat($value = 0)
    {
        return json_encode(app()->formatter->asMoney($value, '₽'));
    }

    public function actionButtonText()
    {
        return t('balance', 'Check current balance');
    }

}
