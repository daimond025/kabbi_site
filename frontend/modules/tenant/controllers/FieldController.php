<?php

namespace app\modules\tenant\controllers;

use app\modules\tenant\models\field\CarFieldModel;
use app\modules\tenant\models\field\EmployeeFieldModel;
use frontend\components\behavior\CityListBehavior;
use frontend\modules\employee\components\position\PositionService;
use frontend\modules\tenant\components\services\FieldService;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\base\Module;

/**
 * Class FieldController
 * @package app\modules\tenant\controllers
 * @mixin CityListBehavior
 */
class FieldController extends Controller
{

    private $positionService;
    private $fieldService;

    public function __construct(
        $id,
        Module $module,
        PositionService $positionService,
        FieldService $fieldService,
        array $config = []
    ) {
        $this->positionService = $positionService;
        $this->positionService->setTenantId(user()->tenant_id);

        $this->fieldService = $fieldService;

        parent::__construct($id, $module, $config);
    }


    public function behaviors()
    {
        return [
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['list'],
                        'roles'   => ['read_settings'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['settings'],
                    ],
                ],
            ],
        ];
    }

    public function actionList($city_id = null)
    {

        $this->fieldService->getFields('worker');

        $isAllowed = app()->user->can('settings');
        $cityMap     = user()->getUserCityList();
        $cityId      = isset($cityMap[$city_id]) ? (int)$city_id : key($cityMap);
        $positionMap = $this->getPositionMap($cityId);

        $model        = new EmployeeFieldModel([
            'tenantId' => user()->tenant_id,
            'cityId'   => $cityId,
        ]);
        $isSavedModel = null;
        try {
            if (app()->request->isPjax) {
                foreach (post($model->formName()) as $attr => $value) {
                    $model->$attr = $value;
                }
                $isSavedModel = $this->fieldService->saveEmployee($model);
            }
        } catch (\Exception $ignore) {
        }

        return $this->render('list', compact('model', 'cityId', 'cityMap', 'positionMap', 'isSavedModel', 'isAllowed'));
    }


    public function actionCarList($city_id = null)
    {

        $cityMap = user()->getUserCityList();
        $cityId  = isset($cityMap[$city_id]) ? (int)$city_id : key($cityMap);

        if (!app()->request->isAjax) {
            return $this->redirect(['list', 'city_id' => $city_id]);
        }

        $model        = new CarFieldModel([
            'tenantId' => user()->tenant_id,
            'cityId'   => $cityId,
        ]);
        $isSavedModel = null;
        try {
            if (app()->request->isPjax) {
                foreach (post($model->formName()) as $attr => $value) {
                    $model->$attr = $value;
                }
                $isSavedModel = $this->fieldService->saveCar($model);
            }
        } catch (\Exception $ignore) {
        }


        return $this->renderAjax('_car', compact('model', 'isSavedModel'));
    }


    private function getPositionMap($cityId)
    {
        $positions = $this->positionService->getPositionsByCity($cityId);

        return empty($positions) ? [] : ArrayHelper::map($positions, 'position_id', 'name');
    }
}