<?php

namespace app\modules\tenant\controllers;

use frontend\components\behavior\CityListBehavior;
use frontend\modules\tenant\models\GeoServiceForm;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use \common\modules\tenant\models\TenantSetting;

/**
 * @mixin CityListBehavior
 */
class GeoServiceController extends Controller
{

    public function behaviors()
    {
        return [
            'access'   => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['list'],
                        'roles'   => ['read_settings'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['settings'],
                    ],
                ],
            ],
            'cityList' => [
                'class' => CityListBehavior::className(),
            ],
        ];
    }

    public function actionList($id = null)
    {
        $cityList = $this->getUserCityList();

        $cityId = ArrayHelper::keyExists($id, $cityList) ? (int)$id : null;

        if (ArrayHelper::keyExists($id, $cityList)) {
            $cityId = (int)$id;
        } elseif (!empty($cityList) && $id !== null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        } else {
            $cityId = (int)key($cityList);
        }

        $model = GeoServiceForm::findOrCreate(user()->tenant_id, $cityId);

        $tenantSetting = (new TenantSetting())->find()->where(['tenant_id'=>user()->tenant_id,'city_id'=>$cityId,'name'=>'AUTOCOMPLETE_RADIUS'])->one();

        if ($model->load(post()) && $model->validate()) {
                $tenantSettingUpdate = TenantSetting::updateSettingValue('AUTOCOMPLETE_RADIUS',user()->tenant_id,$cityId,null,post('TenantSetting')['value']);

                $model->attributes = [
                    'auto_tenant_id'    => user()->tenant_id,
                    'auto_city_id'      => $cityId,
                    'routing_tenant_id' => user()->tenant_id,
                    'routing_city_id'   => $cityId,
                ];

                $default = (isset( post('GeoServiceForm')['default'])) ? intval(post('GeoServiceForm')['default']) : 0;


                if ($model->save($default) && $tenantSettingUpdate) {
                    session()->setFlash('success', t('app', 'Data updated successfully'));
                    $tenantSetting = (new TenantSetting())->find()
                        ->where([
                            'tenant_id' => user()->tenant_id,
                             'city_id'  => $cityId,
                             'name'     => 'AUTOCOMPLETE_RADIUS'
                        ])
                        ->one();
                }

        }

        return $this->render('index', compact('model', 'cityList', 'tenantSetting'));
    }

    public function actionUpdate()
    {
        return $this->render('index', [
        ]);
    }
}