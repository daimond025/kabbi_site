<?php

namespace app\modules\tenant\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use frontend\modules\tenant\models\UserDispetcher;

class UserDispetcherController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    // Диспетчер нажимает кнопу начать смену
    // Функиця пишет в mysql начало смены и добавляет юзера в редис в онлайн операторы
    public function actionDispetcherStartWork()
    {
        return \frontend\modules\tenant\models\UserDispetcher::dispetcherStartWork();
    }

    public function actionDispetcherPauseClick($reason)
    {
        return \frontend\modules\tenant\models\UserDispetcher::dispetcherPauseClick($reason);
    }

    public function actionGetDispetcherWorkTime()
    {
        return \frontend\modules\tenant\models\UserDispetcher::getDispetcherOnlineTime();
    }

    public function actionDispetcherStop()
    {
        return \frontend\modules\tenant\models\UserDispetcher::dispetcherStop();
    }

    public function actionDispetcherMessageRefresh()
    {
        if (!Yii::$app->request->isAjax) return false;
        $userId = user()->id;
        $message = \Yii::$app->redis_dispetcher_live_notice->executeCommand('GET', [$userId]);
        $message = unserialize($message);
        if (is_array($message) && !empty($message)) {
            switch ($message["action"]) {
                case 'set_pause':
                    //Если уже на паузе то не надо ничего посылать
                    $dispetcherData = \Yii::$app->redis_active_dispetcher->executeCommand('GET', [$userId]);
                    $dispetcherData = unserialize($dispetcherData);
                    $onPause = $dispetcherData['on_pause'];
                    if ($onPause != 1) {
                        // Поставить на паузу
                        $dispetcherData['on_pause'] = 1;
                        $pauseArr = $dispetcherData['pause_info'];
                        $pauseArr[] = array(
                            'pause_start'  => time(),
                            'pause_end'    => null,
                            'pause_reason' => strtoupper($message['message'])
                        );
                        $dispetcherData['pause_info'] = $pauseArr;
                        $dispetcherData = serialize($dispetcherData);
                        $setResult = \Yii::$app->redis_active_dispetcher->executeCommand('SET', [$userId, $dispetcherData]);
                        $result = [
                            'action'  => 'set_pause',
                            'message' => t('order', 'You are put on a pause, reason: ') . t('order', $message['message']),
                        ];
                        echo json_encode($result);
                    }
                    \Yii::$app->redis_dispetcher_live_notice->executeCommand('DEL', [$userId]);
                    break;

                case 'set_shift':
                    //Если уже на смене то не надо ничего посылать
                    $dispetcherData = \Yii::$app->redis_active_dispetcher->executeCommand('GET', [$userId]);
                    $dispetcherData = unserialize($dispetcherData);
                    $onPause = $dispetcherData['on_pause'];
                    if ($onPause == 1) {
                        // Поставить на мену
                        $dispetcherData['on_pause'] = 0;
                        $pauseArr = $dispetcherData['pause_info'];
                        $countPauseArr = count($pauseArr);
                        $pauseArr[$countPauseArr - 1]['pause_end'] = time();
                        $dispetcherData['pause_info'] = $pauseArr;

                        $message = time() - $dispetcherData['work_start'];
                        foreach ($dispetcherData['pause_info'] as $pause) {
                            $message -= $pause['pause_end'] - $pause['pause_start'];
                        }
                        $days = date('z', $message);
                        $h = date('H', $message);
                        $message = ($days*24 + $h) . date(':i:s', $message);

                        $dispetcherData = serialize($dispetcherData);
                        $setResult = \Yii::$app->redis_active_dispetcher->executeCommand('SET', [$userId, $dispetcherData]);

                        $result = [
                            'action'  => 'set_shift',
                            'message' => $message,
                        ];
                        \Yii::$app->redis_dispetcher_live_notice->executeCommand('DEL', [$userId]);
                        echo json_encode($result);
                    }
                    \Yii::$app->redis_dispetcher_live_notice->executeCommand('DEL', [$userId]);
                    break;
            }
        }
        ob_flush();
        flush();
        exit;
    }

    public static function sendDispetcherStateToPhoneApi($userId, $state)
    {

        $url = app()->params['phoneApiUrl'] . "call/setstate?" . "agent=$userId&state=$state";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        $result = curl_exec($ch);
        if (isset($result)) {
            return json_decode($result);
        } else {
            return false;
        }
    }

    public function actionSendDispetcherState($state)
    {
        $userId = user()->user_id;
        self::sendDispetcherStateToPhoneApi($userId, $state);
    }

    public function actionGetSoftphoneConfig()
    {
        return UserDispetcher::getSoftphoneConfig();
    }

    public function actionGenerate($cnt = 100)
    {
        return UserDispetcher::operatorGenerate($cnt);
    }

}
