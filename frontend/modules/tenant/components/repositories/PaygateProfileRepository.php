<?php

namespace frontend\modules\tenant\components\repositories;


use frontend\modules\tenant\models\entities\PaygateProfile;
use frontend\modules\tenant\models\entities\TenantCityHasPaygateProfile;

class PaygateProfileRepository
{

    /**
     * @param $tenantId
     * @param $cityId
     *
     * @return null|PaygateProfile
     */
    public function getPaygateProfile($tenantId, $cityId)
    {
        /* @var $tenantCityHasPaygateProfile TenantCityHasPaygateProfile */
        $tenantCityHasPaygateProfile = TenantCityHasPaygateProfile::find()
            ->where([
                'tenant_id' => $tenantId,
                'city_id' => $cityId,
            ])
            ->one();

        return $tenantCityHasPaygateProfile ? $tenantCityHasPaygateProfile->paygateProfile : null;
    }

}