<?php

namespace frontend\modules\tenant\components\services;


use app\modules\tenant\models\User;
use frontend\modules\tenant\models\UserDefaultRight;

class PermissionsService
{

    const RIGHTS_WEIGHT = [
        self::RIGHT_OFF   => 0,
        self::RIGHT_READ  => 1,
        self::RIGHT_WRITE => 2,
    ];

    const RIGHT_OFF = 'off';
    const RIGHT_READ = 'read';
    const RIGHT_WRITE = 'write';

    /* @var $updatedUser User */
    protected $updatedUser;

    protected $defaultRights;
    protected $updatedRights;

    public function setUser(User $updatedUser)
    {
        $this->updatedUser   = $updatedUser;
        $this->defaultRights = UserDefaultRight::getDefaultRightSettingByPosition($this->updatedUser->position_id);

        return $this;
    }

    public function setUpdatedRights($updatedRights)
    {
        $this->updatedRights = $updatedRights;

        return $this;
    }

    public function getElevatedRights()
    {
        $notDefaultRights = $this->getNotDefaultRights();

        if (!$notDefaultRights) {
            return [];
        }

        $elevatedRights = [];
        foreach ($notDefaultRights as $key => $notDefaultRight) {
            if (self::RIGHTS_WEIGHT[$notDefaultRight] > self::RIGHTS_WEIGHT[$this->defaultRights[$key]]) {
                $elevatedRights[$key] = $notDefaultRight;
            }
        }

        return $elevatedRights;
    }

    public function getNotDefaultRights()
    {

        $notDefaultPermissions = [];
        foreach ($this->updatedRights as $key => $right) {
            if (!array_key_exists($key, $this->defaultRights)) {
                continue;
            }

            if ($this->defaultRights[$key] != $right) {
                $notDefaultPermissions[$key] = $right;
            }
        }

        return $notDefaultPermissions;
    }

}