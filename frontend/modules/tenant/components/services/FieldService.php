<?php

namespace frontend\modules\tenant\components\services;

use app\modules\tenant\models\field\CarFieldModel;
use app\modules\tenant\models\field\EmployeeFieldModel;
use app\modules\tenant\models\field\FieldRecord;
use yii\helpers\ArrayHelper;

class FieldService
{
    /**
     * @param EmployeeFieldModel $model
     *
     * @return boolean
     */
    public function saveEmployee($model)
    {
        $transaction = app()->db->beginTransaction();

        FieldRecord::deleteAll(['tenant_id' => $model->tenantId, 'city_id' => $model->cityId, 'type' => 'worker']);

        $array = $model->getModels();
        $data  = [];

        foreach ($array as $key => $item) {
            $positionId = $key === 'worker' ? null : $key;
            foreach ($item as $name => $value) {
                $data[] = [$model->tenantId, $model->cityId, $positionId, 'worker', $name, $value];
            }
        }

        if (!empty($data)) {
            app()->db->createCommand()->batchInsert(FieldRecord::tableName(),
                ['tenant_id', 'city_id', 'position_id', 'type', 'name', 'value'], $data)->execute();
        }

        $transaction->commit();


        return true;
    }

    /**
     * @param CarFieldModel $model
     *
     * @return boolean
     */
    public function saveCar($model)
    {
        $transaction = app()->db->beginTransaction();

        FieldRecord::deleteAll(['tenant_id' => $model->tenantId, 'city_id' => $model->cityId, 'type' => 'car']);

        $array = $model->getModels();
        $data  = [];

        foreach ($array as $key => $item) {
            $positionId = $key === 'car' ? null : $key;
            foreach ($item as $name => $value) {
                $data[] = [$model->tenantId, $model->cityId, $positionId, 'car', $name, $value];
            }
        }

        if (!empty($data)) {
            app()->db->createCommand()->batchInsert(FieldRecord::tableName(),
                ['tenant_id', 'city_id', 'position_id', 'type', 'name', 'value'], $data)->execute();
        }

        $transaction->commit();


        return true;
    }

    public function getFields($type)
    {
        $cityList = array_keys(user()->getUserCityList());

        $models = FieldRecord::find()
            ->where([
                'tenant_id' => user()->tenant_id,
                'city_id'   => $cityList,
                'type'      => $type,
            ])
            ->asArray()
            ->all();

        $result = [];
        foreach ($models as $model) {
            $name                     = ArrayHelper::getValue($model, 'name');
            $position                 = ArrayHelper::getValue($model, 'position_id');
            $position                 = $position === null ? $type : $position;
            $result[$position][$name] = ArrayHelper::getValue($result, "{$position}.$name",
                    false) || (bool)ArrayHelper::getValue($model, 'value');
        }

        return $result;
    }

    public function getWorkerFieldByDocument($documentId)
    {
        return ArrayHelper::getValue(EmployeeFieldModel::getWorkerPositionNameList(), $documentId);
    }

}