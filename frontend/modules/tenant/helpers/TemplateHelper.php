<?php

namespace frontend\modules\tenant\helpers;

use common\helpers\CacheHelper;
use common\modules\order\models\OrderStatus;
use common\services\OrderStatusService;
use yii\helpers\ArrayHelper;

class TemplateHelper
{

    public static function getTemplateNameByType($type, $statusList, $positionId)
    {
        $templateMap = self::getTemplateMap($statusList, $positionId);

        return ArrayHelper::getValue($templateMap, $type);
    }

    private static function getTemplateMap($statusList, $positionId)
    {
        $statuses   = array_map(function ($status) use ($statusList, $positionId) {
            return OrderStatusService::translateByName($status, $positionId);
        }, self::getStatusMap($statusList));

        return $statuses;
    }

    private static function getStatusMap($statusList)
    {
        $key = 'template_order_status_models_' . md5(implode(',', $statusList));

        $statuses = CacheHelper::getFromCache($key, function () use ($statusList) {
            return OrderStatus::find()->where(['status_id' => $statusList])->select([
                'status_id',
                'name',
            ])->all();
        });

        return ArrayHelper::map($statuses, 'status_id', 'name');
    }
}
