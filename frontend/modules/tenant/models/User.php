<?php

namespace app\modules\tenant\models;

use backend\modules\setting\models\entities\TenantHasExtrinsicDispatcher;
use common\components\helpers\UserHelper;
use common\components\serviceApi\EmailTypes;
use common\components\serviceApi\ServiceApi;
use common\helpers\CacheHelper;
use common\modules\city\models\City;
use common\modules\tenant\models\Tenant;
use common\modules\tenant\models\TenantHasCity;
use frontend\components\behavior\file\CropFileBehavior;
use frontend\components\repositories\TenantRepository;
use frontend\modules\companies\models\TenantCompany;
use frontend\modules\tenant\exceptions\RoleException;
use frontend\modules\tenant\models\UserDefaultRight;
use frontend\modules\tenant\models\UserDispetcher;
use frontend\modules\tenant\models\UserExtrinsicDispatcher;
use frontend\modules\tenant\models\UserHasCity;
use Yii;
use yii\caching\TagDependency;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;
use yii\web\UserEvent;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property string $user_id
 * @property string $tenant_id
 * @property string $position_id
 * @property string $email
 * @property string $email_confirm
 * @property string $password
 * @property string $last_name
 * @property string $name
 * @property string $second_name
 * @property string $phone
 * @property string $photo
 * @property integer $active
 * @property string $birth
 * @property string $address
 * @property string $create_time
 * @property string $auth_key
 * @property string $password_reset_token
 * @property timestamp $active_time
 * @property string $ha1
 * @property string $ha1b
 * @property integer $auth_exp
 * @property string $lang
 * @property string $last_session_id
 * @property string $md5_password
 * @property string $access_token
 * @property integer $chat_email_notify
 * @property integer $tenant_company_id
 *
 * @property Order[] $Orders
 * @property OrderHistory[] $OrderHistories
 * @property Support[] $Supports
 * @property SupportFeedback[] $SupportFeedbacks
 * @property Tenant $tenant
 * @property UserPosition $position
 * @property UserHasCity[] $UserHasCities
 * @property \common\modules\city\models\City[] $cities
 * @property UserSetting[] $UserSettings
 * @property UserWorkingTime[] $UserWorkingTimes
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * position_id должности администратора
     */
    const ADMIN = 7;
    const BRANCH_DIRECTOR = 11;
    const POSITION_OPERATOR = 6;
    const POSITION_MAIN_OPERATOR = 8;
    const POSITION_PARTNER_EMPLOYEE = 13;
    const POSITION_EXTRINSIC_DISPATCHER = 14;
    const ROLE_TOP_MANAGER = 'top_manager';
    const ROLE_BRANCH_DIRECTOR = 'branch_director';
    const ROLE_STAFF = 'staff';
    const ROLE_STAFF_COMPANY = 'staff_company';
    const ROLE_EXTRINSIC_DISPATCHER = 'extrinsic_dispatcher';
    /**
     * Tag prefix for yii\caching\TagDependency
     * Example: "user_123", where 123 is user_id
     */
    const TAG_DEPENDENCY_PREFIX = 'user_';
    const SCENARIO_PHOTO = 'photo';

    public $city_list = [];
    public $rights;
    public $cropParams;

    public $position_list;
    public $isRightsChange = false;
    /**
     * Flag of block user
     * @var bool
     */
    public $block;

    /**
     * Is user online
     * @var string on|off The value immediately substituted in name of css class
     */
    public $online = 'off';

    /**
     * Vars for user birth
     */
    public $birth_day;
    public $birth_month;
    public $birth_year;
    public $permissions = null;

    protected static $userExtrinsicDispatcher;

    /**
     * @inheritdoc
     */
    public function init()
    {
        Yii::$app->user->on('afterLogout', [$this, 'onAfterLogout']);
        Yii::$app->user->on('afterLogin', [$this, 'onAfterLogin']);
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    public function scenarios()
    {
        return ArrayHelper::merge([
            self::SCENARIO_PHOTO => ['photo', 'cropParams'],
            'update',
        ], parent::scenarios());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'last_name', 'position_id'], 'required', 'on' => 'insert'],
            [['email'], 'required', 'message' => t('app', 'Email cannot be blank'), 'on' => 'insert'],
            [['email'], 'string', 'max' => 45],
            [
                ['email'],
                'unique',
                'targetClass'     => User::className(),
                'targetAttribute' => ['email', 'tenant_id'],
                'message'         => t('validator', 'This email is already registered'),
                'on'              => 'insert',
            ],
            [
                ['email'],
                'unique',
                'targetClass'     => User::className(),
                'targetAttribute' => ['email', 'tenant_id'],
                'filter'          => ['not', ['user_id' => $this->user_id]],
                'message'         => t('validator', 'This email is already registered'),
                'except'          => 'insert',
            ],
            [['email'], 'email'],
            [
                ['phone'],
                'filter',
                'filter' => function ($value) {
                    return preg_replace("/[^0-9]/", '', $value);
                },
            ],
            [['active'], 'integer'],
            [['city_list', 'block', 'isRightsChange', 'rights', 'cropParams'], 'safe'],
            [['address'], 'string', 'max' => 255],
            [['last_name', 'name', 'second_name'], 'string', 'max' => 45],
            [['phone'], 'string', 'max' => 15],
            ['lang', 'string', 'max' => 10],
            ['chat_email_notify', 'boolean'],
            [
                'photo',
                'image',
                'extensions' => ['png', 'jpeg', 'jpg'],
                'maxSize'    => getUploadMaxFileSize(),
                'tooBig'     => t('file', 'The file size must be less than {file_size}Mb',
                    ['file_size' => getUploadMaxFileSize(true)]),
                'minWidth'   => 300,
                'minHeight'  => 300,
            ],

            [
                ['tenant_company_id'],
                'required',
                'when'       => function (User $model) {
                    return self::getUserRoleByPosition($model->position_id) == self::ROLE_STAFF_COMPANY;
                },
                'whenClient' => sprintf("function (attribute, value) {
                                    return $(\"#user_position_list option[selected]\").val() == %d;
                                }", User::POSITION_PARTNER_EMPLOYEE),
                'message'    => t('tenant_company', '«{attribute}» cannot be blank.',
                    ['attribute' => $this->getAttributeLabel('tenant_company_id')]),
                'on' => 'insert'
            ],

            [
                ['position_id'],
                function ($attribute) {
                    if (self::getUserRoleByPosition(user()->position_id) == self::ROLE_STAFF_COMPANY) {
                        if (self::getUserRoleByPosition($this->position_id) != self::ROLE_STAFF_COMPANY) {
                            throw new RoleException('Сделан недопустимый запрос.');
                        }
                    }
                },
            ],

        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'new_password'      => t('user', 'Password'),
            'password_repeat'   => t('user', 'Repeat password'),
            'address'           => t('user', 'Address'),
            'birth'             => t('user', 'Birth'),
            'name'              => t('user', 'Name'),
            'last_name'         => t('user', 'Last name'),
            'second_name'       => t('user', 'Second name'),
            'create_time'       => t('user', 'Registration date'),
            'position_id'       => t('user', 'Position'),
            'position.name'     => t('user', 'Position'),
            'city'              => t('user', 'City'),
            'email'             => t('app', 'Username'),
            'phone'             => t('user', 'Mobile phone'),
            'active'            => t('user', 'Active user'),
            'photo'             => t('user', 'Photo'),
            'contact_name'      => t('user', 'Contact name'),
            'active_time'       => 'Online',
            'city_list'         => t('user', 'Working in cities'),
            'lang'              => t('app', 'Language'),
            'chat_email_notify' => t('user', 'Send to email not read messages from a chat'),
            'access_token'      => t('user', 'Access token'),
            'tenant_company_id' => t('tenant_company', 'Companies'),
        ];
    }

    /**
     * Get user position list
     * @return array [position_id => name]
     */
    public function getPositionList()
    {
        $edit_user_role = self::getUserRoleByPosition($this->position_id);
        $cur_user_role = self::getUserRoleByPosition(user()->position_id);

        if ($cur_user_role == self::ROLE_TOP_MANAGER) {
            $position_list = $edit_user_role == self::ROLE_TOP_MANAGER ? self::getTopManagerPositionIdList() : null;
        } elseif ($cur_user_role == self::ROLE_BRANCH_DIRECTOR) {
            $position_list = $edit_user_role == self::ROLE_BRANCH_DIRECTOR ? [$this->position_id] : self::getStaffPositionIdList();
        } elseif ($cur_user_role == self::ROLE_STAFF_COMPANY) {
            $position_list = self::getStaffCompanyPositionIdList();
        } else {
            $position_list = self::getStaffPositionIdList();
        }

        $user_positions = UserPosition::find()
            ->where(['NOT IN', 'position_id', self::getExcludedPositionIdList()])
            ->andFilterwhere(['position_id' => $position_list])
            ->all();

        return UserPosition::getUserPositionMap($user_positions);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['user_modifed' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupports()
    {
        return $this->hasMany(Support::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupportFeedbacks()
    {
        return $this->hasMany(SupportFeedback::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(UserPosition::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserHasCities()
    {
        return $this->hasMany(UserHasCity::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(\common\modules\city\models\City::className(),
            ['city_id' => 'city_id'])->viaTable('{{%user_has_city}}', ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDispetchers()
    {
        return $this->hasMany(UserDispetcher::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserWorkingTimes()
    {
        return $this->hasMany(UserWorkingTime::className(), ['user_id' => 'user_id']);
    }

    public function getTenantCompany()
    {
        return $this->hasOne(TenantCompany::className(), ['tenant_company_id' => 'tenant_company_id']);
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     *
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return true;
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    public function setAccessToken()
    {
        $security = Yii::$app->security;
        $this->access_token = $security->generatePasswordHash($security->generateRandomString());
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return self::find()->where(['access_token' => $token])->one();
    }

    public static function findIdentity($id)
    {
        return self::findOne(['user_id' => $id]);
    }

    public function beforeSave($insert)
    {
        if (!$insert) {
            if ($this->getOldAttribute('password') !== $this->password) {
                $this->setAccessToken();
            }
        }

        return parent::beforeSave($insert);
    }

    public static function findIdentityByEmail($email, $domain = null)
    {
        $user = self::findByEmail($email, $domain);

        if (is_null($user) ||
            ($user->position_id != self::ADMIN && $user->tenant->status == Tenant::BLOCKED)
        ) {
            return false;
        }

        return $user;
    }

    public static function findByEmail($email, $domain = null)
    {
        return self::find()
            ->where([
                static::tableName() . '.email' => $email,
                'active'                       => 1,
            ])
            ->joinWith([
                'tenant' => function ($query) use ($domain) {
                    $domain = empty($domain) ? domainName() : $domain;
                    $query->andWhere("status != 'REMOVED' AND domain = '$domain'");
                },
            ], false)
            ->one();
    }

    public static function findIdentityByEmailAndTenantId($email, $tenantId, $domain = null)
    {
        $user = self::findByEmailAndTenantId($email, $tenantId, $domain);

        if (is_null($user) ||
            ($user->position_id != self::ADMIN && $user->tenant->status == Tenant::BLOCKED)
        ) {
            return self::findExtrinsicDispatcher($email, $tenantId);
        }

        return $user;
    }

    public static function findByEmailAndTenantId($email, $tenantId, $domain = null)
    {
        return self::find()
            ->where([
                static::tableName() . '.email'     => $email,
                'active'                           => 1,
                static::tableName() . '.tenant_id' => $tenantId,
            ])
            ->joinWith([
                'tenant' => function ($query) use ($domain) {
                    $domain = empty($domain) ? domainName() : $domain;
                    $query->andWhere("status != 'REMOVED' AND domain = '$domain'");
                },
            ], false)
            ->one();
    }

    public static function findExtrinsicDispatcher($email, $tenantId)
    {
        /* @var $extrinsicDispatcher self */
        $extrinsicDispatcher = self::find()
            ->where([
                static::tableName() . '.email' => $email,
                'active'                       => 1,
                'position_id'                  => self::POSITION_EXTRINSIC_DISPATCHER,
            ])->one();

        if (!$extrinsicDispatcher) {
            return false;
        }

        $res = TenantHasExtrinsicDispatcher::find()
            ->where([
                'user_id'   => $extrinsicDispatcher->user_id,
                'tenant_id' => $tenantId,
            ])
            ->exists();

        if ($res || ($extrinsicDispatcher->tenant_id == $tenantId)) {
            return $extrinsicDispatcher;
        }

        return false;
    }

    public static function getTenantIdByNameHost()
    {

        $partsNameServer = explode('.', $_SERVER['HTTP_HOST']);
        $domain = array_shift($partsNameServer);
        $tenantId = Tenant::getIdByDomain($domain);

        if (!ctype_digit($tenantId)) {
            throw new \DomainException('Authorization for an existing tenant');
        }

        return $tenantId;
    }

    public static function findIdentityByAuthKey($user_id, $auth_key)
    {
        return self::find()->where([
            'user_id'  => $user_id,
            'auth_key' => $auth_key,
        ])->andWhere(time() . '<= auth_exp')->one();
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    //---------------------------------------------------------
    //Recovery password

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     *
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!self::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'active'               => 1,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     *
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int)end($parts);

        return $timestamp + $expire >= time();
    }

    //------------------------------------------------------------------------------

    public function behaviors()
    {
        return [
            'fileBehavior' => [
                'class'      => CropFileBehavior::className(),
                'fileField'  => ['photo'],
                'upload_dir' => app()->params['upload'],
            ],
        ];
    }

    public function setEmailToken()
    {
        $this->email_confirm = md5($this->email . time());
    }

    /**
     * Format array of user models for table.
     *
     * @param array $users Array of \app\modules\tenant\models\User
     * @param bool $position_list Return result with position list from all users.
     *
     * @return array
     */
    public static function getGridData($users, $position_list = true)
    {
        $arUsers['USERS'] = [];
        $onlineUserCityIds = [];
        foreach ($users as $user) {
            if ($user->user_id === user()->user_id) {
                $onlineUserCityIds = ArrayHelper::getColumn($user->cities, 'city_id');
                break;
            }
        }

        if ($position_list) {
            $arPosition = [];
        }

        $city_list = array_keys(user()->getUserCityList());
        foreach ($users as $user) {
            $arUserCity = [];

            foreach ($user->cities as $city) {
                if (in_array($city->city_id, $city_list)) {
                    $arUserCity[] = $city->{name . getLanguagePrefix()};
                }
            }

            if ($position_list && !array_key_exists($user['position']['position_id'], $arPosition)) {
                $arPosition[$user['position']['position_id']] = $user['position']['name'];
            }

            $arUsers['USERS'][] = [
                'ID'           => $user->user_id,
                'CHAT_CITY_ID' => $user->getChatCityId($onlineUserCityIds),
                'CITIES'       => implode(', ', $arUserCity),
                'PHOTO'        => $user->isFileExists($user->photo) ? $user->getPictureHtml($user->photo) : '',
                'ONLINE'       => self::isUserOnline($user['active_time']) ? 'on' : 'off',
                'NAME'         => trim($user->last_name . ' ' . $user->name . ' ' . $user->second_name),
                'POSITION'     => $user->position->name,
            ];
        }

        if ($position_list) {
            asort($arPosition);
            $arUsers['POSITION_LIST'] = $arPosition;
        }

        return $arUsers;
    }

    /**
     * Getting city id for opening user chat in the city.
     * The current online user must contain this city.
     *
     * @param array $onlineUserCityIds Array of city ids of current online user
     *
     * @return integer City id
     */
    public function getChatCityId($onlineUserCityIds)
    {
        $curCityIds = ArrayHelper::getColumn($this->cities, 'city_id');
        foreach ($curCityIds as $curCityId) {
            if (in_array($curCityId, $onlineUserCityIds)) {
                return $curCityId;
            }
        }
    }

    public static function isUserOnline($active_time)
    {
        return time() - $active_time < app()->params['user.online.ttlSec'];
    }

    public function getIsOnline()
    {
        return time() - $this->active_time < app()->params['user.online.ttlSec'];
    }

    /**
     * Send invite to email
     *
     * @param      $lang
     * @param null $password
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function sendInvite($lang, $password = null)
    {
        /** @var ServiceApi $serviceApi */
        $serviceApi = app()->get('serviceApi');

        $cityList = $this->getUserCityList();
        $cityId   = empty($cityList) ? null : key($cityList);

        $serviceApi->sendEmail($this->tenant_id, $cityId, EmailTypes::EMAIL_USER_CONFIRM, $this->lang,
            $this->email, [
                'user_name'     => UserHelper::getNameUser($this),
                'user_email'    => $this->email,
                'user_password' => $password,
                'url'           => app()->urlManager->createAbsoluteUrl([
                    'tenant/user/confirm-email',
                    'token' => $this->email_confirm,
                    'lang'  => $lang,
                ]),
            ]);
    }

    /**
     * Check user invite status
     * @return bool
     */
    public function isUserInvited()
    {
        return $this->email_confirm != 1 && $this->active == 0;
    }

    public function isExtrinsicDispatcher()
    {
        return $this->position_id == self::POSITION_EXTRINSIC_DISPATCHER;
    }

    /**
     * Getting user city map
     * @return array ['city_id => 'name']
     */
    public function getUserCityList()
    {
        return UserHasCity::getUserCityMap($this->user_id);
    }

    /**
     * Get active users in city
     *
     * @param $tenantId
     * @param $cityId
     * @param $userId
     *
     * @return mixed
     */
    public static function getUserListInCityFromCache($tenantId, $cityId, $userId)
    {
        $userCityListCacheKey = 'user_list_' . $tenantId . '_' . $cityId;
        $usersInCity = CacheHelper::getFromCache($userCityListCacheKey,
            function () use ($tenantId, $cityId, $userId) {
                return self::find()
                    ->where(['tenant_id' => $tenantId])
                    ->andWhere(["active" => 1])
                    ->joinWith('position')
                    ->joinWith([
                        'userHasCities' => function ($query) use ($cityId, $userId) {
                            $query->andWhere(['city_id' => $cityId]);
                        },
                    ])
                    ->asArray()
                    ->all();
            });

        return $usersInCity;
    }

    /**
     * Города, где работает пользователь с республиками и временным смещением
     * @return array
     */
    public function getUserCityListWithRepublic()
    {
        if (Yii::$app->user->can(User::ROLE_EXTRINSIC_DISPATCHER)) {
            /* @var $tenantRepository TenantRepository */
            $tenantRepository = Yii::createObject(TenantRepository::class);

            return $tenantRepository->getTenantCityListWithRepublic();
        } else {
            $lang_prefix = getLanguagePrefix();

            return CacheHelper::getFromCache('UserCityListWithRepublic_' . $this->tenant_id . '_' . $this->user_id . $lang_prefix,
                function () use ($lang_prefix) {
                    return City::find()->
                    joinWith([
                        'userHasCities' => function ($query) {
                            $query->where(['user_id' => $this->user_id]);
                            $query->joinWith([
                                'tenantHasCities' => function ($sub_query) {
                                    $sub_query->where(['block' => 0]);
                                },
                            ]);
                        },
                    ], false)->
                    with([
                        'republic' => function ($query) {
                            $query->select(['republic_id', 'timezone']);
                        },
                    ])->
                    select([City::tableName() . '.city_id', 'name' . $lang_prefix, 'republic_id'])->
                    asArray()->
                    orderBy(TenantHasCity::tableName() . '.sort')->
                    all();
                });
        }
    }

    public function getFullName()
    {
        return trim($this->last_name . ' ' . $this->name . ' ' . $this->second_name);
    }

    /**
     * Варианты прав для разрешений.
     * @return array
     */
    public static function getRightsVariantsOfPermission()
    {
        return [
            'read'  => t('rights', 'Read'),
            'write' => t('rights', 'Write'),
            'off'   => t('rights', 'Off'),
        ];
    }

    /**
     * Определение прав пользователя на конкретное разрешение.
     *
     * @param string $permision
     *
     * @return string all|read|write|off
     */
    public function getUserRightsByPermission($permision)
    {
        if (is_null($this->permissions)) {
            $this->permissions = $this->getPermissions();
        }

        if (in_array($permision, $this->permissions)) {
            $rights = 'write';
        } elseif (in_array("read_$permision", $this->permissions)) {
            $rights = 'read';
        } else {
            $rights = 'off';
        }

        return $rights;
    }

    /**
     * Получение списка всех разрешений пользователя.
     * Метод создан из-за того, что \Yii::$app->authManager->getPermissionsByUser()
     * не отдает дочерние разрешения.
     * @return array
     */
    public function getPermissions()
    {
        $rows = (new \yii\db\Query())
            ->select(['item_name'])
            ->from('tbl_auth_assignment')
            ->where(['user_id' => $this->id])
            ->all();

        return ArrayHelper::getColumn($rows, 'item_name');
    }

    public function assignRights()
    {
        if (!empty($this->rights) && is_array($this->rights)) {
            $auth = Yii::$app->authManager;
            $auth->revokeAll($this->id);

            foreach ($this->rights as $name => $rights) {
                if ($rights == 'off') {
                    continue;
                }

                $permission_name = $rights == 'read' ? $rights . '_' . $name : $name;
                $permission = $auth->getPermission($permission_name);
                $auth->assign($permission, $this->id);
            }

            $role_name = self::getUserRoleByPosition($this->position_id);
            $role = $auth->getRole($role_name);
            $auth->assign($role, $this->id);
        }
    }

    /**
     *  Hash generation for login to softphone
     */
    public function setOperatorHash($password)
    {
        //Генерация хешей для авторизации в софтфоне
        $operator_name = UserDispetcher::getOperatorName($this->user_id);

        $this->ha1 = UserDispetcher::generateHa1Hash($operator_name, $password);
        $this->ha1b = UserDispetcher::generateHa1bHash($operator_name, $password);
    }

    public function beforeDelete()
    {
        //Удаление из базы операторов
        if (in_array($this->position_id, self::getOperatorPositions())) {
            UserDispetcher::deleteOperatorFromPhoneBase($this->user_id);
        }

        return parent::beforeDelete();
    }

    public function afterDelete()
    {
        parent::afterDelete();
        Yii::$app->authManager->revokeAll($this->id);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($this->scenario == self::SCENARIO_PHOTO) {
            return false;
        }

        //Сохранение прав
        if ($this->isRightsChange || $insert) {
            if (empty($this->rights) && user()->getUserRole() == self::ROLE_STAFF) {
                $this->rights = UserDefaultRight::getDefaultRightSettingByPosition($this->position_id);
            }
            $this->assignRights();
        }

        //При добавлении пользователя, либо при изменении должности
        if ($insert || isset($changedAttributes['position_id']) && $changedAttributes['position_id'] != $this->position_id) {
            $operatorPositions = self::getOperatorPositions();

            //Добавление должности оператора. Проверяем чтобы был переход с неоператорской должности на операторскую c полными правами на заказы.
            if ($this->is_dispatcher() && !in_array($changedAttributes['position_id'], $operatorPositions)) {
                UserDispetcher::addOperatorToPhoneBase($this->ha1, $this->ha1b, $this->md5_password, $this->user_id);
            } //Снятие с должности оператора
            elseif (in_array($changedAttributes['position_id'], $operatorPositions) && !$this->is_dispatcher()) {
                UserDispetcher::deleteOperatorFromPhoneBase($this->user_id);
            }
        } //При изменении прав на заказы у диспетчера
        elseif ($this->isRightsChange && $this->is_dispatcher()) {
            //Получены права на запись и нет записи в БД операторов
            if (app()->authManager->checkAccess($this->user_id, 'orders') &&
                !UserDispetcher::find()->where(['user_id' => $this->user_id])->exists()
            ) {
                UserDispetcher::addOperatorToPhoneBase($this->ha1, $this->ha1b, $this->md5_password, $this->user_id);
            } elseif (!app()->authManager->checkAccess($this->user_id, 'orders')) {
                UserDispetcher::deleteOperatorFromPhoneBase($this->user_id);
            }
        } //Либо смена пароля
        elseif (isset($changedAttributes['password'])) {
            UserDispetcher::updateOperatorHash($this->user_id, $this->ha1, $this->ha1b, $this->md5_password);
        }

        //Блок сброса кеша
        UserHasCity::deleteCache($this->user_id);

        $cities = $this->cities;
        foreach ($cities as $city) {
            self::deleteUserListCache($city['city_id'], $this->tenant_id);
        }

        if (!$insert) {
            TagDependency::invalidate(Yii::$app->cache, self::TAG_DEPENDENCY_PREFIX . $this->user_id);
        }
        //-----------------------------------------------------------------------------------
        parent::afterSave($insert, $changedAttributes);
    }

    public static function deleteUserListCache($city_id, $tenant_id)
    {
        app()->cache->delete('user_list_' . $tenant_id . '_' . $city_id);
    }

    /**
     * Getting operator position list.
     * @return array
     */
    public static function getOperatorPositions()
    {
        return [
            self::POSITION_OPERATOR,
            self::POSITION_MAIN_OPERATOR,
        ];
    }

    /**
     * Checking - is user dispatcher.
     * @return bool
     */
    public function is_dispatcher()
    {
        return in_array($this->position_id, self::getOperatorPositions());
    }

    /**
     * Getting position name of user.
     * @return string
     */
    public function getPositionName()
    {
        $position_map = self::getPositionMap();

        return getValue($position_map[$this->position_id]);
    }

    public static function getPositionMap()
    {
        return \common\helpers\CacheHelper::getFromCache('position_name' . __CLASS__, function () {
            $res = (new \yii\db\Query)
                ->from(UserPosition::tableName())
                ->all();

            return ArrayHelper::map($res, 'position_id', 'name');
        });
    }

    /**
     * Getting statistic of online users.
     * @return array ['dispatchers' => 0, 'others' => 11]
     */
    public static function getOnlineStat($conditions = [])
    {
        $users = self::find()
            ->select('active_time, position_id')
            ->where(array_merge([
                'tenant_id' => user()->tenant_id,
            ], $conditions))
            ->all();

        $arStat = [
            'dispatchers' => 0,
            'others'      => 0,
        ];

        foreach ($users as $user) {
            if (self::isUserOnline($user->active_time)) {
                if ($user->is_dispatcher()) {
                    $arStat['dispatchers']++;
                } else {
                    $arStat['others']++;
                }
            }
        }

        return $arStat;
    }

    public static function getOnlineStaffJson()
    {
        $users = self::find()
            ->select('user_id, last_name, name, second_name, phone, position_id')
            ->where(['tenant_id' => user()->tenant_id])
            ->andWhere(['>', 'active_time', time() - app()->params['user.online.ttlSec']])
            ->andWhere(['position_id' => array_merge(self::getOperatorPositions(), [self::POSITION_PARTNER_EMPLOYEE, self::ADMIN])])
            ->all();

        foreach ($users as $key => $user) {
            if (empty(array_intersect($user->getUserCityList(), user()->getUserCityList()))) {
                unset($users[$key]);
            }
        }

        return $users;
    }

    /**
     * Update active user time for identity online.
     */
    public function updateUserActiveTime()
    {
        app()->db->createCommand('UPDATE ' . User::tableName() . ' SET active_time=' . time() . ' WHERE user_id=' . $this->id)->execute();
    }

    public static function getCurUserCityIds()
    {
        return ArrayHelper::getColumn(user()->userHasCities, 'city_id');
    }

    /**
     * Getting string user role name by position.
     *
     * @param integer $position_id
     *
     * @return string
     */
    public static function getUserRoleByPosition($position_id)
    {
        if (in_array($position_id, self::getTopManagerPositionIdList())) {
            return self::ROLE_TOP_MANAGER;
        } elseif ($position_id == self::BRANCH_DIRECTOR) {
            return self::ROLE_BRANCH_DIRECTOR;
        } elseif (in_array($position_id, self::getStaffCompanyPositionIdList())) {
            return self::ROLE_STAFF_COMPANY;
        } else {
            return self::ROLE_STAFF;
        }
    }

    /**
     * Getting role name of current user.
     * @return string
     */
    public function getUserRole()
    {
        return self::getUserRoleByPosition($this->position_id);
    }

    public static function getTopManagerPositionIdList()
    {
        return [1, 2, 3, 4, self::ADMIN];
    }

    public static function getStaffPositionIdList()
    {
        return [5, 6, 8, 9, 10];
    }

    public static function getStaffCompanyPositionIdList()
    {
        return [13];
    }

    public static function getExcludedPositionIdList()
    {
        return [14];
    }

    public function isAdmin()
    {
        return in_array($this->position_id, self::getTopManagerPositionIdList());
    }

    /**
     * AfterLogin event callback
     *
     * @param UserEvent $event
     */
    protected function onAfterLogin($event)
    {
        $user = $event->identity;

        if (isset($user->last_session_id)) {
            session()->destroySession($user->last_session_id);
        }
        $user->last_session_id = session()->getId();
        $user->generateAuthKey();
        $user->save();
    }

    /**
     * AfterLogout event callback
     *
     * @throws \yii\db\StaleObjectException
     */
    protected function onAfterLogout()
    {
        $this->active_time = 0;
        $this->update(false, ['active_time']);
    }

    /**
     * Getting online user count by tenant_id
     *
     * @param integer $tenantId
     * @param integer $exceptUserId
     *
     * @return integer
     */
    public static function getOnlineUserCount($tenantId, $exceptUserId = null)
    {
        return static::find()
            ->where(['tenant_id' => $tenantId])
            ->andWhere(['>', 'active_time', time() - app()->params['user.online.ttlSec']])
            ->andFilterWhere(['!=', 'user_id', $exceptUserId])
            ->count();
    }

    public function setOperatorPasswordHash($operator_name, $password)
    {
        $this->md5_password = UserDispetcher::getOperatorPasswordHash($operator_name, $password);
    }

    /**
     * @return integer
     */
    public static function getCurrentTenantId()
    {
        return user()->tenant_id;
    }

    public function getLogo()
    {
        return $this->isFileExists($this->photo) ? $this->getPictureHtml($this->photo, false) : '';
    }

    public static function getUserExtrinsicDispatcher($user)
    {
        if (is_null(self::$userExtrinsicDispatcher)) {
            self::$userExtrinsicDispatcher = new UserExtrinsicDispatcher($user->getAttributes());
            self::$userExtrinsicDispatcher->tenant_id = (int)User::getTenantIdByNameHost();
        }

        return self::$userExtrinsicDispatcher;
    }

    /**
     * @return bool
     */
    public static function isCurrentPartnerEmployee()
    {
        return user()->position_id === User::POSITION_PARTNER_EMPLOYEE;
    }

    public function getPartnerCompanyName()
    {
        if ($this->position_id == self::POSITION_PARTNER_EMPLOYEE) {
            return CacheHelper::getFromCache('partner_company_name_' . $this->tenant_company_id,
                function () {
                    return TenantCompany::find()
                        ->select('name')
                        ->where(['tenant_company_id' => $this->tenant_company_id])
                        ->scalar();
                });
        }

        return $this->getCompanyName();
    }

    public function getCompanyName()
    {
        return CacheHelper::getFromCache('company_name_' . $this->tenant_id, function () {
            return Tenant::find()
                ->select('company_name')
                ->where(['tenant_id' => $this->tenant_id])
                ->scalar();
        });
    }
}
