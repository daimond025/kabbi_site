<?php

namespace app\modules\tenant\models;

use Yii;

/**
 * This is the model class for table "tbl_user_working_time".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $start_work
 * @property integer $end_work
 * @property string $pause_data
 * @property integer $count_income_answered_calls
 * @property integer $count_income_noanswered_calls
 * @property integer $count_outcome_answered_calls
 * @property integer $count_outcome_noanswered_calls
 * @property integer $count_completed_orders
 * @property integer $count_rejected_orders
 * @property string $summ_completed_arr
 * @property string $summ_rejected_arr
 * @property integer $count_income_calls
 * @property integer $count_outcome_calls
 * @property integer $summ_income_calls_time
 * @property integer $summ_outcome_calls_time
 */
class UserWorkingTime extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_user_working_time';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'start_work', 'end_work', 'count_income_answered_calls', 'count_income_noanswered_calls', 'count_outcome_answered_calls', 'count_outcome_noanswered_calls', 'count_completed_orders', 'count_rejected_orders', 'count_income_calls', 'count_outcome_calls', 'summ_income_calls_time', 'summ_outcome_calls_time'], 'integer'],
            [['pause_data'], 'string'],
            [['summ_completed_arr', 'summ_rejected_arr'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                             => 'ID',
            'user_id'                        => 'User ID',
            'start_work'                     => 'Start Work',
            'end_work'                       => 'End Work',
            'pause_data'                     => 'Pause Data',
            'count_income_answered_calls'    => 'Count Income Answered Calls',
            'count_income_noanswered_calls'  => 'Count Income Noanswered Calls',
            'count_outcome_answered_calls'   => 'Count Outcome Answered Calls',
            'count_outcome_noanswered_calls' => 'Count Outcome Noanswered Calls',
            'count_completed_orders'         => 'Count Completed Orders',
            'count_rejected_orders'          => 'Count Rejected Orders',
            'summ_completed_arr'             => 'Summ Completed Arr',
            'summ_rejected_arr'              => 'Summ Rejected Arr',
            'count_income_calls'             => 'Count Income Calls',
            'count_outcome_calls'            => 'Count Outcome Calls',
            'summ_income_calls_time'         => 'Summ Income Calls Time',
            'summ_outcome_calls_time'        => 'Summ Outcome Calls Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_id']);
    }
    
    
    /**
     * Поиск статиcтики за конкретный период.
     * Исторические данные кешируются.
     * @param integer $driver_id
     * @param array $arPeriod ['first_timestamp', 'last_timestamp']
     * @return array
     */
    public static function getShiftsByPeriod($operator_id, $arPeriod)
    {
        list($first_date, $last_date) = $arPeriod;

        return self::getShiftData($operator_id, $first_date, $last_date);
    }
    
    /**
     * Получаем данные по смене.
     * @param integer $driver_id
     * @param integer $first_date
     * @param integer $last_date
     */
    private static function getShiftData($operator_id, $first_date, $last_date)
    {

        
        return [
            'activeShifts' => self::getActiveShiftData($operator_id),
            'operatorShifts' => self::getFinishedShiftData($operator_id, $first_date, $last_date),
        ];
    }
    
    
    private static function getFinishedShiftData($operator_id, $first_date, $last_date)
    {
        $operatorShifts = self::find()
            ->where(['user_id' => $operator_id])
            ->andWhere(['between', 'start_work', $first_date, $last_date])
            ->orderBy(['end_work' => SORT_DESC])
            ->asArray()
            ->all();
        
        return $operatorShifts;
    }
    
    private static function getActiveShiftData($operator_id)
    {
        $dispetcherData = \Yii::$app->redis_active_dispetcher->executeCommand('GET', [$operator_id]);
        if( is_null($dispetcherData) )
            return [];
        else
            $data = unserialize($dispetcherData);

        
        $activeShift = [
            'id' => 'active',
            'user_id' => $data['user_id'],
            'start_work' => $data['work_start'],
            'end_work' => time(),
            'pause_data' => serialize($data['pause_info']),
            'count_income_answered_calls' => $data['count_income_answered_calls'],
            'count_income_noanswered_calls' => $data['count_income_noanswered_calls'],
            'count_outcome_answered_calls' => $data['count_outcome_answered_calls'],
            'count_outcome_noanswered_calls' => $data['count_outcome_noanswered_calls'],
            'count_completed_orders' => $data['count_completed_orders'],
            'count_rejected_orders' => $data['count_rejected_orders'],
            'summ_completed_arr' => serialize($data['summ_completed_arr']),
            'summ_rejected_arr' => serialize($data['summ_rejected_arr']),
            'count_income_calls' => $data['count_income_calls'],
            'count_outcome_calls' => $data['count_outcome_calls'],
            'summ_income_calls_time' => $data['summ_income_calls_time'],
            'summ_outcome_calls_time' => $data['summ_outcome_calls_time'],
        ];
        return [$activeShift];
    }
    
    
}