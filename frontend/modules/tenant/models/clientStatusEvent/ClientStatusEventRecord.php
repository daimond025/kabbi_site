<?php

namespace frontend\modules\tenant\models\clientStatusEvent;

use yii\db\ActiveRecord;

/**
 * Class ClientStatusEventRecord
 * @package frontend\modules\tenant\models\clientStatusEvent
 *
 * @property integer $event_id
 * @property integer $status_id
 * @property integer $tenant_id
 * @property string  $group
 * @property string  $notice
 * @property integer $position_id
 * @property integer $city_id
 */
class ClientStatusEventRecord extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_status_event}}';
    }
}