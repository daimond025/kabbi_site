<?php

namespace frontend\modules\tenant\models\clientStatusEvent;

use app\modules\order\models\OrderStatus;
use common\services\OrderStatusService;
use yii\helpers\ArrayHelper;

class ClientStatusEventHelper
{
    const GROUP_APP = 'APP';
    const GROUP_CALL = 'CALL';
    const GROUP_WEB = 'WEB';
    const GROUP_BORDER = 'BORDER';

    const NOTICE_AUTOCALL = 'autocall';
    const NOTICE_SMS_FOR_CLIENT = 'sms';
    const NOTICE_SMS_FOR_PASSENGER = 'smsPassenger';
    const NOTICE_PUSH = 'push';
    const NOTICE_NOTHING = 'nothing';

    public static function getNoticeListByGroup($group)
    {
        return array_keys(self::getNoticeMapByGroup($group));
    }

    public static function getNoticeMapByGroup($group)
    {
        $noticeMap = self::getNoticeMap();
        switch ($group) {
            case self::GROUP_BORDER:
            case self::GROUP_CALL:
            case self::GROUP_WEB:
                unset($noticeMap[self::NOTICE_PUSH]);
                break;
        }

        return $noticeMap;
    }

    public static function getGroupList()
    {
        return array_keys(self::getGroupMap());
    }

    public static function getGroupMap()
    {
        return [
            self::GROUP_APP    => t('status_event', 'Order is created through the application'),
            self::GROUP_CALL   => t('status_event', 'Order is created through a call to the control room'),
            self::GROUP_WEB    => t('status_event', 'Order is created through the site'),
            self::GROUP_BORDER => t('status_event', 'Order is created on the street'),
        ];
    }

    public static function getNoticeList()
    {
        return array_keys(self::getNoticeMap());
    }

    public static function getNoticeMap()
    {
        return [
            self::NOTICE_AUTOCALL          => t('status_event', 'Autocall for client'),
            self::NOTICE_SMS_FOR_CLIENT    => t('status_event', 'SMS for client'),
            self::NOTICE_PUSH              => t('status_event', 'PUSH for client'),
            self::NOTICE_SMS_FOR_PASSENGER => t('status_event', 'SMS for passenger'),
        ];
    }

    /**
     * @param $positionId
     * @param $lang
     *
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public static function getEventMap($positionId, $lang)
    {
        /** @var OrderStatusService $orderStatusService */
        $orderStatusService = \Yii::createObject(OrderStatusService::class);

        $result = [];
        $list   = self::getEventList();
        foreach ($list as $groupKey => $item) {
            foreach ($item as $statusKeys) {
                if (!ArrayHelper::isTraversable($statusKeys)) {
                    $statusKeys = (array)$statusKeys;
                }

                $currentResult = [];
                foreach ($statusKeys as $statusKey) {
                    $currentResult[$statusKey] = $orderStatusService->translate($statusKey, $positionId, $lang);
                }

                $result[$groupKey][] = $currentResult;
            }
        }

        return $result;
    }

    public static function getEventList()
    {
        return [
            self::GROUP_APP    => [
                OrderStatus::STATUS_NEW,
                OrderStatus::STATUS_PRE,
                OrderStatus::STATUS_GET_WORKER,
                OrderStatus::STATUS_WORKER_WAITING,
                OrderStatus::STATUS_EXECUTING,
                OrderStatus::STATUS_COMPLETED_PAID,
                OrderStatus::STATUS_REJECTED,
                OrderStatus::STATUS_WORKER_LATE,
                OrderStatus::STATUS_EXECUTION_PRE,
                OrderStatus::STATUS_OVERDUE,
                OrderStatus::STATUS_PRE_GET_WORKER,
                OrderStatus::STATUS_PRE_REFUSE_WORKER,
                [
                    OrderStatus::STATUS_NO_CARS_BY_TIMER,
                    OrderStatus::STATUS_NO_CARS,
                ],
                OrderStatus::STATUS_CANCELED_BY_WORKER,
            ],
            self::GROUP_WEB    => [
                OrderStatus::STATUS_NEW,
                OrderStatus::STATUS_PRE,
                OrderStatus::STATUS_GET_WORKER,
                OrderStatus::STATUS_WORKER_WAITING,
                OrderStatus::STATUS_EXECUTING,
                OrderStatus::STATUS_COMPLETED_PAID,
                OrderStatus::STATUS_REJECTED,
                OrderStatus::STATUS_WORKER_LATE,
                OrderStatus::STATUS_EXECUTION_PRE,
                OrderStatus::STATUS_OVERDUE,
                OrderStatus::STATUS_PRE_GET_WORKER,
                OrderStatus::STATUS_PRE_REFUSE_WORKER,
                [
                    OrderStatus::STATUS_NO_CARS_BY_TIMER,
                    OrderStatus::STATUS_NO_CARS,
                ],
                OrderStatus::STATUS_CANCELED_BY_WORKER,
            ],
            self::GROUP_CALL   => [
                OrderStatus::STATUS_NEW,
                OrderStatus::STATUS_PRE,
                OrderStatus::STATUS_GET_WORKER,
                OrderStatus::STATUS_WORKER_WAITING,
                OrderStatus::STATUS_EXECUTING,
                OrderStatus::STATUS_COMPLETED_PAID,
                OrderStatus::STATUS_REJECTED,
                OrderStatus::STATUS_WORKER_LATE,
                OrderStatus::STATUS_EXECUTION_PRE,
                OrderStatus::STATUS_OVERDUE,
                OrderStatus::STATUS_PRE_GET_WORKER,
                OrderStatus::STATUS_PRE_REFUSE_WORKER,
                [
                    OrderStatus::STATUS_NO_CARS_BY_TIMER,
                    OrderStatus::STATUS_NO_CARS,
                ],
                OrderStatus::STATUS_CANCELED_BY_WORKER,
            ],
            self::GROUP_BORDER => [
                OrderStatus::STATUS_EXECUTING,
                OrderStatus::STATUS_COMPLETED_PAID,
            ],
        ];
    }

    /**
     * @param ClientStatusEventModel $model
     * @param string                 $group
     * @param string                 $statusId
     *
     * @return string
     */
    public static function getStringNotice($model, $group, $statusId)
    {
        $array = ArrayHelper::getValue($model->events, "$group.$statusId");

        if (!$array) {
            return t('status_event', 'Nothing');
        }

        $nameList = self::getNoticeMap();
        $array    = array_map(function ($item) use ($nameList) {
            return ArrayHelper::getValue($nameList, $item, t('status_event', 'Nothing'));
        }, $array);

        return implode(', ', $array);
    }
}