<?php

namespace frontend\modules\tenant\models\clientStatusEvent;

use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class ClientStatusEventModel
 * @package frontend\modules\tenant\models\clientStatusEvent
 *
 * @property ClientStatusEvent[] $records
 */
class  ClientStatusEventModel extends Model
{
    public $events = [];
    public $tenantId;
    public $cityId;
    public $positionId;

    public function rules()
    {
        return [
            ['events', 'filter', 'filter' => [$this, 'filterEvents']],
            ['events', 'safe'],
        ];
    }

    public function filterEvents($value)
    {
        $eventList = ClientStatusEventHelper::getEventList();

        foreach ($eventList as $groupKey => $group) {
            foreach ($group as $statusId) {
                if ($noticeList = ArrayHelper::getValue($value, "$groupKey.$statusId", false)) {
                    foreach ($noticeList as $key => $notice) {
                        if (!in_array($notice, ClientStatusEventHelper::getNoticeListByGroup($groupKey))) {
                            unset($value[$groupKey][$statusId][$key]);
                        }
                    }
                }
            }
        }

        return $value;
    }

    public static function find($tenantId, $cityId, $positionId)
    {
        $records = ClientStatusEvent::find()
            ->where([
                'tenant_id'   => $tenantId,
                'city_id'     => $cityId,
                'position_id' => $positionId,
            ])
            ->indexBy('event_id')
            ->all();

        $events = [];

        /** @var ClientStatusEvent $model */
        foreach ($records as $model) {
            $groupId                     = $model->group;
            $statusId                    = $model->status_id;
            $events[$groupId][$statusId] = explode(',', $model->notice);
        }

        return new self(compact('tenantId', 'cityId', 'positionId', 'events'));
    }

    /**
     * @return boolean
     * @throws \yii\db\Exception
     */
    public function saveMany()
    {
        if(!$this->validate()) {
            return false;
        }

        $insert      = [];
        $transaction = app()->db->beginTransaction();

        $result = ClientStatusEvent::deleteAll([
            'tenant_id'   => $this->tenantId,
            'city_id'     => $this->cityId,
            'position_id' => $this->positionId,
        ]);

        if (!$result) {
            $transaction->rollBack();

            return false;
        }

        $eventList = ClientStatusEventHelper::getEventList();

        foreach ($eventList as $groupKey => $group) {
            foreach ($group as $statusIds) {
                if (!ArrayHelper::isTraversable($statusIds)) {
                    $statusIds = (array)$statusIds;
                }
                $firstStatusId = reset($statusIds);
                $noticeList = ArrayHelper::getValue($this, "events.$groupKey.$firstStatusId");
                if (empty($noticeList)) {
                    $noticeList = [ClientStatusEventHelper::NOTICE_NOTHING];
                }

                foreach ($statusIds as $statusId) {
                    $insert[] = [
                        'tenant_id'   => $this->tenantId,
                        'city_id'     => $this->cityId,
                        'position_id' => $this->positionId,
                        'group'       => $groupKey,
                        'status_id'   => $statusId,
                        'notice'      => implode(',', $noticeList),
                    ];
                }
            }
        }

        if (!empty($insert)) {
            $columns = array_keys(reset($insert));
            $result  = app()->db->createCommand()->batchInsert(ClientStatusEventRecord::tableName(), $columns,
                $insert)->execute();
            if (!$result) {
                $transaction->rollBack();

                return false;
            }
        }

        $transaction->commit();

        return true;
    }
}