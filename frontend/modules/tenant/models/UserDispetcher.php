<?php

namespace frontend\modules\tenant\models;

use app\modules\tenant\models\User;
use chat\ChatService;
use common\modules\city\models\City;
use common\modules\tenant\models\Tenant;
use frontend\modules\employee\models\worker\Worker;
use Yii;
use yii\helpers\Html;


/**
 * This is the model class for table "tbl_user_dispetcher".
 *
 * @property integer $dispetcher_id
 * @property integer $user_id
 * @property string  $sip_id
 *
 * @property User    $user
 */
class UserDispetcher extends \yii\db\ActiveRecord
{
    const KAMAILIO_USER_TABLE = 'subscriber';
    const KAMAILIO_USER_GROUP_TABLE = 'dr_groups';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_dispetcher}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'sip_id'], 'required'],
            [['user_id'], 'integer'],
            [['sip_id'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dispetcher_id' => 'Dispetcher ID',
            'user_id'       => 'User ID',
            'sip_id'        => 'Sip ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_id']);
    }

    public static function isUserDispetcher()
    {
        $user = user();
        if (is_object($user)) {
            if ($user->position_id == 6 || $user->position_id == 8) {
                return true;
            }
        }

        return false;
    }

    public static function isDispetcherOffline()
    {
        $user                = user();
        $userId              = $user->user_id;
        $dispetcherRedisData = \Yii::$app->redis_active_dispetcher->executeCommand('GET', [$userId]);
        $dispetcherRedisData = unserialize($dispetcherRedisData);
        if (empty($dispetcherRedisData)) {
            return true;
        } else {
            return false;
        }
    }

    public static function isDispetcherOnline()
    {
        $user                = user();
        $userId              = $user->user_id;
        $dispetcherRedisData = \Yii::$app->redis_active_dispetcher->executeCommand('GET', [$userId]);
        $dispetcherRedisData = unserialize($dispetcherRedisData);
        if (empty($dispetcherRedisData)) {
            return false;
        } else {
            if ($dispetcherRedisData['on_pause'] == '0') {
                return true;
            } else {
                return false;
            }
        }
    }

    public static function getDispetcherOnlineTime()
    {
        $user                = user();
        $userId              = $user->user_id;
        $dispetcherRedisData = \Yii::$app->redis_active_dispetcher->executeCommand('GET', [$userId]);
        $dispetcherRedisData = unserialize($dispetcherRedisData);
        if (empty($dispetcherRedisData)) {
            return '00:00';
        } else {
            $startTime = $dispetcherRedisData['work_start'];
            $pauseTime = 0;
            $pauseArr  = $dispetcherRedisData['pause_info'];
            foreach ($pauseArr as $pause) {
                if (isset($pause['pause_start']) && isset($pause['pause_end'])) {
                    $pauseTime += $pause['pause_end'] - $pause['pause_start'];
                }
                if (isset($pause['pause_start']) && !isset($pause['pause_end'])) {
                    $pauseTime += time() - $pause['pause_start'];
                }
            }
            $nowTime  = time();
            $diffTime = $nowTime - $startTime - $pauseTime;
            $result   = date("H:i:s", mktime(0, 0, $diffTime));

            return $result;
        }
    }

    public static function getDispetcherPauseTime()
    {
        $user                = user();
        $userId              = $user->user_id;
        $dispetcherRedisData = \Yii::$app->redis_active_dispetcher->executeCommand('GET', [$userId]);
        $dispetcherRedisData = unserialize($dispetcherRedisData);
        if (empty($dispetcherRedisData)) {
            return '00:00:00';
        } else {
            $pauseArr = $dispetcherRedisData['pause_info'];
            $diffTime = '00:00:00';
            if (!empty($pauseArr)) {
                end($pauseArr);
                $lastPauseKey = key($pauseArr);
                $start        = $pauseArr[$lastPauseKey]['pause_start'];
                $now          = time();
                $diffTime     = $now - $start;
            }
            $result = date("H:i:s", mktime(0, 0, $diffTime));

            return $result;
        }
    }

    public static function isDispetcherOnPause()
    {
        $user                = user();
        $userId              = $user->user_id;
        $dispetcherRedisData = \Yii::$app->redis_active_dispetcher->executeCommand('GET', [$userId]);
        $dispetcherRedisData = unserialize($dispetcherRedisData);
        if (empty($dispetcherRedisData)) {
            return false;
        } else {
            if ($dispetcherRedisData['on_pause'] != '0') {
                return true;
            } else {
                return false;
            }
        }
    }

    public static function getPauseReason()
    {
        return [
            'WC'               => t('user_pause', 'WC'),
            'SNACK_TIME'       => t('user_pause', 'SNACK_TIME'),
            'PERSONAL_REASONS' => t('user_pause', 'PERSONAL_REASONS'),
            'OTHER'            => t('user_pause', 'OTHER'),
        ];
    }

    public static function getOperatorPasswordHash($operator_name, $password)
    {
        return $password ? md5($operator_name . ':asterisk:' . $password) : '';
    }

    public static function addOperatorToPhoneBase($ha1, $ha1b, $md5_password, $user_id = null, $domain = null)
    {
        if (empty($ha1) || empty($ha1b)) {
            return false;
        }

        $domain = Tenant::getTenantHostInPhoneBase($domain);
        if (!empty($user_id)) {
            $user_name               = self::createOperatorName();
            $userDispetcher          = new self();
            $userDispetcher->user_id = $user_id;
            $userDispetcher->sip_id  = $user_name . '@' . $domain;
            $userDispetcher->save(false);
        } else {
            $user_name = 'notify';
        }

        app()->kamailio->createCommand()->insert(self::KAMAILIO_USER_TABLE, [
            'username'     => $user_name,
            'domain'       => $domain,
            'ha1'          => $ha1,
            'ha1b'         => $ha1b,
            'md5_password' => $md5_password,
        ])->execute();

        //Временное решение
        if ($domain == 'gpstaxi.v1.gootax.pro' || $domain == 'satory.v1.gootax.pro') {
            $groupid = ($domain == 'gpstaxi.v1.gootax.pro') ? 3 : 4;
            app()->kamailio->createCommand()->insert(self::KAMAILIO_USER_GROUP_TABLE, [
                'username' => $user_name,
                'domain'   => $domain,
                'groupid'  => $groupid,
            ])->execute();
        }

        app()->curl->get(app()->params['softphone.updateSIPUserUrl']);
    }

    public static function deleteOperatorFromPhoneBase($user_id)
    {
        $userDispetcher = self::findOne(['user_id' => $user_id]);

        if (!empty($userDispetcher)) {
            $sip = explode('@', $userDispetcher->sip_id);
            $userDispetcher->delete();
            app()->kamailio->createCommand()->delete(self::KAMAILIO_USER_TABLE,
                ['username' => $sip[0], 'domain' => $sip[1]])->execute();
            app()->kamailio->createCommand()->delete(self::KAMAILIO_USER_GROUP_TABLE,
                ['username' => $sip[0], 'domain' => $sip[1]])->execute();
            app()->curl->get(app()->params['softphone.updateSIPUserUrl']);
        }

        return null;
    }

    public static function updateOperatorHash($user_id, $ha1, $ha1b, $md5_password)
    {
        if (empty($ha1) || empty($ha1b) || empty($md5_password)) {
            return false;
        }
        $userDispetcher = self::findOne(['user_id' => $user_id]);
        if (!empty($userDispetcher)) {
            $sip = explode('@', $userDispetcher->sip_id);
            $res = app()->kamailio->createCommand('SELECT COUNT(*) FROM ' . self::KAMAILIO_USER_TABLE . ' WHERE domain=:domain AND username=:username')
                ->bindValue(':username', $sip[0])
                ->bindValue(':domain', $sip[1])
                ->queryScalar();

            if ($res > 0) {
                $result = app()
                    ->kamailio
                    ->createCommand('UPDATE ' . self::KAMAILIO_USER_TABLE .
                        ' SET ha1 = "' . $ha1 . '", ha1b = "' . $ha1b . '", md5_password="' . $md5_password . '" WHERE username = "' . $sip[0] . '" AND domain = "' . $sip[1] . '"')
                    ->execute();
                app()->voipApi->updatesipuser($sip[0]);

                return $result;
            }

            $userDispetcher->delete();
        }

        return self::addOperatorToPhoneBase($ha1, $ha1b, $md5_password, $user_id);
    }

    public static function sendDispetcherStateToPhoneApi($userId, $state)
    {

        $url = app()->params['phoneApiUrl'] . "call/setstate?" . "agent=$userId&state=$state";
        $ch  = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        if (isset($result)) {
            return json_decode($result);
        } else {
            return false;
        }
    }

    // Диспетчер нажимает кнопу начать смену
    // Функиця пишет в mysql начало смены и добавляет юзера в редис в онлайн операторы
    public static function dispetcherStartWork()
    {
        $user        = user();
        $userId      = $user->user_id;
        $disp        = User::find()
            ->joinWith('userHasCities')
            ->joinWith('userDispetchers')
            ->where([User::tableName() . '.user_id' => $userId])
            ->asArray()
            ->one();
        $cityBaseArr = $disp['userHasCities'];
        $cityArr     = [];
        foreach ($cityBaseArr as $city) {
            $cityArr[] = $city['city_id'];
        }
        $dispetcherId   = $disp['userDispetchers']['0']['dispetcher_id'];
        $dispetcherData = [
            'dispetcher_id'                  => $dispetcherId,
            'user_id'                        => $disp['user_id'],
            'tenant_id'                      => $disp['tenant_id'],
            'sip_id'                         => $disp['userDispetchers']['0']['sip_id'],
            'city_arr'                       => $cityArr,
            'work_start'                     => time(),
            'on_pause'                       => 0,
            'pause_info'                     => [],
            'count_income_answered_calls'    => 0,
            'count_income_noanswered_calls'  => 0,
            'count_outcome_answered_calls'   => 0,
            'count_outcome_noanswered_calls' => 0,
            'count_completed_orders'         => 0,
            'count_rejected_orders'          => 0,
            'count_income_calls'             => 0,
            'count_outcome_calls'            => 0,
            'summ_income_calls_time'         => 0,
            'summ_outcome_calls_time'        => 0,
            'summ_completed_arr'             => [],
            'summ_rejected_arr'              => [],
        ];
        $dispetcherData = serialize($dispetcherData);
        if (\Yii::$app->redis_active_dispetcher->executeCommand('SET', [$userId, $dispetcherData])) {
            $html = '<span id="dispetcher_work_time" class="hcmr_time">00:00:00</span><a href="#dispetcher_p" id="dispetcher_pause" class="button green_button">' . t('user',
                    'Pause') . '</a><a id="dispetcher_stop" class ="button red_button">' . t('user',
                    'End work') . '</a>';
            self::sendDispetcherStateToPhoneApi($userId, 'idle');

            return $html;
        }

        return false;
    }

    public static function dispetcherPauseClick($reason)
    {
        $user           = user();
        $userId         = $user->user_id;
        $dispetcherData = \Yii::$app->redis_active_dispetcher->executeCommand('GET', [$userId]);
        $dispetcherData = unserialize($dispetcherData);
        $onPause        = $dispetcherData['on_pause'];
        // Поставить на паузу
        if ($onPause == 0) {
            $result                       = 1;
            $dispetcherData['on_pause']   = 1;
            $pauseArr                     = $dispetcherData['pause_info'];
            $pauseArr[]                   = [
                'pause_start'  => time(),
                'pause_end'    => null,
                'pause_reason' => $reason,
            ];
            $dispetcherData['pause_info'] = $pauseArr;
            // посылаем запрос в телефонное апи о том что поменялся статус оператора
            self::sendDispetcherStateToPhoneApi($userId, 'unavailable');
        } else {
            // Убрать с паузы
            $result                     = 0;
            $dispetcherData['on_pause'] = 0;
            $pauseArr                   = $dispetcherData['pause_info'];
            if (!empty($pauseArr)) {
                end($pauseArr);
                $lastPauseKey                         = key($pauseArr);
                $pauseArr[$lastPauseKey]['pause_end'] = time();
            }
            $dispetcherData['pause_info'] = $pauseArr;
            // посылаем запрос в телефонное апи о том что поменялся статус оператора
            self::sendDispetcherStateToPhoneApi($userId, 'idle');
        }
        $dispetcherData = serialize($dispetcherData);
        \Yii::$app->redis_active_dispetcher->executeCommand('SET', [$userId, $dispetcherData]);

        return $result;
    }

    public static function dispetcherStop()
    {
        $user   = user();
        $userId = $user->user_id;
        // посылаем запрос в телефонное апи о том что поменялся статус оператора
        self::sendDispetcherStateToPhoneApi($userId, 'unavailable');
        // Удаляем из списка  активных операторов
        $dispetcherData = \Yii::$app->redis_active_dispetcher->executeCommand('GET', [$userId]);
        $dispetcherData = unserialize($dispetcherData);
        if (!empty($dispetcherData)) {
            $pauseArr = $dispetcherData['pause_info'];
            if (!empty($pauseArr)) {
                end($pauseArr);
                $lastPauseKey = key($pauseArr);
                if (!isset($pauseArr[$lastPauseKey]['pause_end'])) {
                    $pauseArr[$lastPauseKey]['pause_end'] = time();
                    $dispetcherData['pause_info']         = $pauseArr;
                }
            }
            $dispetcherWorkingTime                                 = new \app\modules\tenant\models\UserWorkingTime();
            $dispetcherWorkingTime->user_id                        = $userId;
            $dispetcherWorkingTime->start_work                     = (int)$dispetcherData['work_start'];
            $dispetcherWorkingTime->end_work                       = (int)time();
            $dispetcherWorkingTime->pause_data                     = serialize($dispetcherData['pause_info']);
            $dispetcherWorkingTime->count_income_answered_calls    = (int)isset($dispetcherData['count_income_answered_calls']) ? $dispetcherData['count_income_answered_calls'] : 0;
            $dispetcherWorkingTime->count_income_noanswered_calls  = (int)isset($dispetcherData['count_income_noanswered_calls']) ? $dispetcherData['count_income_noanswered_calls'] : 0;
            $dispetcherWorkingTime->count_outcome_answered_calls   = (int)isset($dispetcherData['count_outcome_answered_calls']) ? $dispetcherData['count_outcome_answered_calls'] : 0;
            $dispetcherWorkingTime->count_outcome_noanswered_calls = (int)isset($dispetcherData['count_outcome_noanswered_calls']) ? $dispetcherData['count_outcome_noanswered_calls'] : 0;
            $dispetcherWorkingTime->count_completed_orders         = (int)isset($dispetcherData['count_completed_orders']) ? $dispetcherData['count_completed_orders'] : 0;
            $dispetcherWorkingTime->count_rejected_orders          = (int)isset($dispetcherData['count_rejected_orders']) ? $dispetcherData['count_rejected_orders'] : 0;
            $dispetcherWorkingTime->count_income_calls             = (int)isset($dispetcherData['count_income_calls']) ? $dispetcherData['count_income_calls'] : 0;
            $dispetcherWorkingTime->count_outcome_calls            = (int)isset($dispetcherData['count_outcome_calls']) ? $dispetcherData['count_outcome_calls'] : 0;
            $dispetcherWorkingTime->summ_income_calls_time         = (int)isset($dispetcherData['summ_income_calls_time']) ? $dispetcherData['summ_income_calls_time'] : 0;
            $dispetcherWorkingTime->summ_outcome_calls_time        = (int)isset($dispetcherData['summ_outcome_calls_time']) ? $dispetcherData['summ_outcome_calls_time'] : 0;
            $dispetcherWorkingTime->summ_completed_arr             = (string)isset($dispetcherData['summ_completed_arr']) ? serialize($dispetcherData['summ_completed_arr']) : serialize([]);
            $dispetcherWorkingTime->summ_rejected_arr              = (string)isset($dispetcherData['summ_rejected_arr']) ? serialize($dispetcherData['summ_rejected_arr']) : serialize([]);
            if (!$dispetcherWorkingTime->save()) {
                Yii::error($dispetcherWorkingTime->errors);
            }
        }
        $result = \Yii::$app->redis_active_dispetcher->executeCommand('DEL', [$userId]);

        return 1;
    }

    /**
     * Возвращает массив вида [0 => username, 1 => domain].
     * Поля соответствуют таблице subscriber БД - "kamailio".
     *
     * @param integer $user_id
     *
     * @return array|null
     */
    public static function getUserSip($user_id)
    {
        $userDispetcher = self::findOne(['user_id' => $user_id]);

        if (!empty($userDispetcher)) {
            return explode('@', $userDispetcher->sip_id);
        }

        return null;
    }

    public static function getNextOperatorNumber()
    {
        return app()->kamailio->createCommand('SELECT MAX(id) FROM ' . self::KAMAILIO_USER_TABLE)->queryScalar() + 1;
    }

    public static function getSoftphoneConfig($user_id = null)
    {
        $user_id       = empty($user_id) ? user()->user_id : $user_id;
        $operator      = self::getOperatorFromPhoneBase($user_id);
        $server        = getValue($operator['domain']);
        $tenant_domain = str_replace(app()->params['softphone.subDomain'] . '.', '', $server);

        $port = app()->params['softphone.port'];

        if ($server == 'etaxi.v1.gootax.pro') {
            $server = 'm1-vpn.gootax.pro';
        } elseif ($server == 'shabashnik.v1.gootax.pro') {
            $server = 'shabashnik.v1.gootax.pro';
        } elseif ($server == 'black24.v1.gootax.pro') {
            $server = 'black24.v1.gootax.pro';
        } elseif ($server == 'mytaxi.v1.gootax.pro') {
            $server = 'm1-vpn.gootax.pro';
        } elseif ($server == 'azetaxi.v1.gootax.pro') {
            $server = '62.212.235.118';
            $port = '15060';
        } elseif ($server == 'taximbaku.v1.gootax.pro') {
            $server = '37.26.63.168';
            $port = '5060';
        } else {
            $server = app()->params['softphone.asteriskHost'];
        }

        $config = [
            'server'   => $server,
            'port'     => $port,
            'user'     => getValue($operator['username']),
            'call_uri' => app()->params['softphone.protocol'] . '://' . $tenant_domain . app()->params['softphone.callUri'],
        ];

        return json_encode($config, JSON_UNESCAPED_SLASHES);
    }

    public static function getOperatorFromPhoneBase($user_id)
    {
        $sip = self::getUserSip($user_id);
        //@TODO Удалить лишний запрос
        if (!empty($sip)) {
            return app()->kamailio->createCommand('SELECT username, domain FROM ' . self::KAMAILIO_USER_TABLE . ' WHERE `username` = "' . $sip[0] . '" AND `domain` = "' . $sip[1] . '"')->queryOne();
        }

        return null;
    }

    public static function createOperatorName($prefix = 'op_')
    {
        return $prefix . self::getNextOperatorNumber();
    }

    public static function generateHa1Hash($operator_name, $password, $domain = null)
    {
        return md5($operator_name . ':' . \common\modules\tenant\models\Tenant::getTenantHostInPhoneBase($domain) . ':' . $password);
    }

    public static function generateHa1bHash($operator_name, $password, $domain = null)
    {
        $host = Tenant::getTenantHostInPhoneBase($domain);

        return md5($operator_name . '@' . $host . ':' . $host . ':' . $password);
    }

    public static function getOperatorName($user_id)
    {
        $userDispetcher = self::findOne(['user_id' => $user_id]);

        if (!empty($userDispetcher)) {
            $sip = explode('@', $userDispetcher->sip_id);

            return $sip[0];
        }

        return null;
    }

    /**
     * Generating operators for test
     *
     * @param integer $cnt
     *
     * @return json
     */
    public static function operatorGenerate($cnt)
    {
        $arName     = ['Вася', 'Петя', 'Ваня', 'Костя', 'Егор'];
        $arLastName = ['Иванов', 'Ибрагимов', 'Петров', 'Пиздюков', 'Сурков'];
        $arUsers    = [];

        for ($i = 0; $i < $cnt; $i++) {
            $model = new User(['scenario' => 'insert']);

            $password             = Yii::$app->security->generateRandomString(10);
            $model->email         = rand_str(10, 'abcdefghijklmnopqrstuvwxyz') . '@ya.ru';
            $model->email_confirm = 1;
            $model->active        = 1;
            $model->last_name     = $arLastName[rand(0, 4)];
            $model->name          = $arName[rand(0, 4)];
            $model->setPassword($password);
            $model->generateAuthKey();
            $model->setAccessToken();
            $model->tenant_id   = user()->tenant_id;
            $model->position_id = User::POSITION_OPERATOR;

            $arUsers[] = ['user' => $model->email, 'password' => $password];

            //Генерация хешей для авторизации в софтфоне
            $operator_name = UserDispetcher::createOperatorName();
            $model->ha1    = UserDispetcher::generateHa1Hash($operator_name, $password);
            $model->ha1b   = UserDispetcher::generateHa1bHash($operator_name, $password);

            //Права
            $model->rights = [
                'cars'         => 'off',
                'clients'      => 'off',
                'clientTariff' => 'read',
                'drivers'      => 'off',
                'driverTariff' => 'off',
                'exchange'     => 'write',
                'orders'       => 'write',
                'organization' => 'off',
                'parkings'     => 'write',
                'phoneLine'    => 'write',
                'publicPlace'  => 'write',
                'reports'      => 'off',
                'settings'     => 'write',
                'streets'      => 'write',
                'tenant'       => 'write',
                'users'        => 'read',
            ];

            if ($model->save()) {
                //Сохранение городов работы пользователя
                \frontend\modules\tenant\models\UserHasCity::manySave([26068], $model->user_id);
            }
        }

        return json_encode($arUsers);
    }

    public static function getChatData()
    {
        $user        = user();
        $worker      = new Worker();
        $userId      = $user->user_id;
        $tenantId    = $user->tenant_id;
        $chatData    = [];
        $userCityArr = user()->getUserCityList();
        if (!empty($userCityArr)) {
            foreach ($userCityArr as $cityId => $userCityName) {
                $cityData         = City::getCityDataFromCache($cityId);
                $cityName         = $cityData['name' . getLanguagePrefix()];
                $service          = \Yii::createObject(ChatService::class);
                $workersInCityAll = $service->getCachedWorkersByCity($tenantId, $cityId);
                $workerArr        = [];
                if (!empty($workersInCityAll)) {
                    foreach ($workersInCityAll as $workerData) {
                        $workerArr[] = [
                            "worker_id"       => $workerData['worker_id'],
                            "worker_callsign" => $workerData['callsign'],
                            "city_id"         => $cityId,
                            "last_name"       => $workerData['last_name'],
                            "name"            => $workerData['name'],
                            "second_name"     => $workerData['second_name'],
                            /** @var $worker Worker */
                            'photo_url'       => $worker->isFileExists($workerData['photo']) ? $worker->getPictureHtml($workerData['photo'],
                                false) : '',
                        ];
                    }
                }

                $usersInCityList = User::getUserListInCityFromCache($tenantId, $cityId, $userId);
                $userArr         = [];
                if (!empty($usersInCityList)) {
                    foreach ($usersInCityList as $userData) {
                        $curUserId = user()->user_id;
                        if ($curUserId != $userData['user_id']) {
                            $userArr[] = [
                                "user_id"     => $userData['user_id'],
                                "user_role"   => t('company-roles', $userData['position']['name']),
                                "city_id"     => $cityId,
                                "last_name"   => $userData['last_name'],
                                "name"        => $userData['name'],
                                "second_name" => $userData['second_name'],
                                /** @var $user User */
                                'photo_url'   => $user->isFileExists($userData['photo']) ? $user->getPictureHtml($userData['photo'],
                                    false) : '',
                            ];
                        }
                    }
                }

                $chatData [] = [
                    'city_id'    => $cityId,
                    'city_name'  => $cityName,
                    'worker_arr' => $workerArr,
                    'user_arr'   => $userArr,
                ];
            }
        }

        return $chatData;
    }

    public static function getUserData()
    {
        $user              = user();
        $userId            = $user->user_id;
        $tenantId          = $user->tenant_id;
        $tenantLogin       = Tenant::getDomainName();
        $userF             = isset($user->last_name) ? $user->last_name : "";
        $userI             = isset($user->name) ? $user->name : "";
        $userO             = isset($user->second_name) ? $user->second_name : "";
        $userRole          = isset($user->position['name']) ? t('company-roles', $user->position['name']) : "";
        $userRoleid        = $user->position['position_id'];
        $chatAdminRoles    = [1, 2, 3, 4, 7];
        $userIsAdminRole   = in_array($userRoleid, $chatAdminRoles);
        $userCityArr       = [];
        $messageSaveText   = t('app', 'Save');
        $messageDeleteText = t('app', 'Delete');
        $messageCancelText = t('app', 'Cancel');
        if (!empty($user->cities)) {
            foreach ($user->cities as $city) {
                $userCityArr[] = $city['city_id'];
            }
        }
        $userCityArr = json_encode($userCityArr);
        $html        = '<div id="message_cancel_text">' . Html::encode($messageCancelText) . '</div><div id="message_delete_text">' . Html::encode($messageDeleteText) . '</div><div id="message_save_text">' . Html::encode($messageSaveText) . '</div><div id="user_is_admin">' . Html::encode($userIsAdminRole) . '</div><div id="user_id">' . Html::encode($userId) . '</div><div id="user_role">' . Html::encode($userRole) . '</div><div id="user_f">' . Html::encode($userF) . '</div><div id="user_i">' . Html::encode($userI) . '</div><div id="user_o">' . Html::encode($userO) . '</div><div id="tenant_login">' . Html::encode($tenantLogin) . '</div><div id="user_city_arr">' . Html::encode($userCityArr) . '</div>';

        return $html;
    }
}