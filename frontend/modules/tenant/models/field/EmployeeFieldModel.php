<?php

namespace app\modules\tenant\models\field;

use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class EmployeeFieldModel
 * @package app\modules\tenant\models\field
 *
 */
class EmployeeFieldModel extends Model
{
    private $_models;

    public $tenantId;
    public $cityId;

    public function __construct(array $config = [])
    {
        parent::__construct($config);

        if (!ArrayHelper::keyExists('tenantId', $config) || !ArrayHelper::keyExists('cityId', $config)) {
            throw new InvalidConfigException();
        }
    }

    public function init()
    {
        $models = FieldRecord::find()
            ->where([
                'tenant_id' => $this->tenantId,
                'city_id'   => $this->cityId,
                'type'      => 'worker',
                'name'      => EmployeeFieldModel::getNameList(),
            ])
            ->all();

        $this->_models = ArrayHelper::map($models, 'name', 'value', function ($model) {
            return $model->position_id ? $model->position_id : 'worker';
        });
    }

    public function __get($name)
    {
        $array = explode('_', $name, 3);

        if (ArrayHelper::getValue($array, 0) === 'position' && count($array) === 3) {
            $position  = ArrayHelper::getValue($array, 1);
            $attribute = ArrayHelper::getValue($array, 2);

            if (ArrayHelper::keyExists($position, $this->_models) && ArrayHelper::keyExists($attribute,
                    $this->_models[$position])) {
                return ArrayHelper::getValue($this->_models, "$position.$attribute");
            }
        }

        return parent::__get($name);
    }

    public function __set($name, $value)
    {
        $array = explode('_', $name, 3);

        if (ArrayHelper::getValue($array, 0) === 'position' && count($array) === 3) {

            $position  = ArrayHelper::getValue($array, 1);
            $attribute = ArrayHelper::getValue($array, 2);
            if (ArrayHelper::keyExists($position, $this->_models) && ArrayHelper::keyExists($attribute,
                    $this->_models[$position])) {
                $this->_models[$position][$attribute] = $value;
            } else {
                parent::__set($name, $value);
            }
        } else {
            parent::__set($name, $value);
        }
    }

    public function attributeLabels()
    {
        return [
            'position_worker_workerBirthday'            => t('employee', 'Birthday'),
            'position_worker_workerEmail'               => t('employee', 'Email'),
            'position_worker_workerCardNumber'          => t('employee', 'Card number'),
            'position_worker_workerYandexAccountNumber' => t('employee', 'Yandex account number'),
            'position_worker_workerDescription'         => t('employee', 'Description'),
            'position_worker_workerPassport'            => t('app', 'Passport'),
            'position_worker_workerSNILS'               => t('employee', 'SNILS'),
            'position_worker_workerINN'                 => t('employee', 'INN'),
            'position_worker_workerOGRNIP'              => t('employee', 'OGRNIP'),

            'workerPositionOSAGO'              => t('employee', 'OSAGO'),
            'workerPositionDriverLicense'      => t('employee', 'Driver License'),
            'workerPositionPTS'                => t('employee', 'PTS'),
            'workerPositionMedicalCertificate' => t('employee', 'Medical certificate'),

        ];
    }


    public function getModels()
    {
        return $this->_models;
    }

    public static function getNameList()
    {
        return array_merge(EmployeeFieldModel::getWorkerNameList(), EmployeeFieldModel::getWorkerPositionNameList());
    }

    public static function getWorkerNameList()
    {
        return [
            FieldRecord::NAME_WORKER_BIRTHDAY,
            FieldRecord::NAME_WORKER_CARD_NUMBER,
            FieldRecord::NAME_WORKER_DESCRIPTION,
            FieldRecord::NAME_WORKER_EMAIL,
            FieldRecord::NAME_WORKER_INN,
            FieldRecord::NAME_WORKER_OGRNIP,
            FieldRecord::NAME_WORKER_PASSPORT,
            FieldRecord::NAME_WORKER_SNILS,
            FieldRecord::NAME_WORKER_YANDEX_ACCOUNT_NUMBER,
        ];
    }

    public static function getWorkerPositionNameList()
    {

        return [
            5 => FieldRecord::NAME_WORKER_POSITION_MEDICAL_CERTIFICATE,
            6 => FieldRecord::NAME_WORKER_POSITION_OSAGO,
            7 => FieldRecord::NAME_WORKER_POSITION_DRIVER_LICENSE,
            8 => FieldRecord::NAME_WORKER_POSITION_PTS,
        ];
    }

    public function getAttributesByPosition($positionId)
    {
        if (ArrayHelper::keyExists($positionId, $this->_models)) {
            $attributes = ArrayHelper::getValue($this->_models, $positionId);

            return array_keys($attributes);
        }

        return [];
    }
}