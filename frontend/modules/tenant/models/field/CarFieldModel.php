<?php

namespace app\modules\tenant\models\field;

use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class CarFieldModel
 * @package app\modules\tenant\models\field
 *
 */
class CarFieldModel extends Model
{
    private $_models;

    public $tenantId;
    public $cityId;

    public function __construct(array $config = [])
    {
        parent::__construct($config);

        if (!ArrayHelper::keyExists('tenantId', $config) || !ArrayHelper::keyExists('cityId', $config)) {
            throw new InvalidConfigException();
        }
    }

    public function init()
    {
        $models = FieldRecord::find()
            ->where([
                'tenant_id' => $this->tenantId,
                'city_id'   => $this->cityId,
                'type'      => 'car',
                'name'      => CarFieldModel::getNameList(),
            ])
            ->all();

        $this->_models = ArrayHelper::map($models, 'name', 'value', function ($model) {
            return $model->position_id ? $model->position_id : 'car';
        });
    }

    public function __get($name)
    {
        $array = explode('_', $name, 3);

        if (ArrayHelper::getValue($array, 0) === 'position' && count($array) === 3) {
            $position  = ArrayHelper::getValue($array, 1);
            $attribute = ArrayHelper::getValue($array, 2);

            if (ArrayHelper::keyExists($position, $this->_models) && ArrayHelper::keyExists($attribute,
                    $this->_models[$position])) {
                return ArrayHelper::getValue($this->_models, "$position.$attribute");
            }
        }

        return parent::__get($name);
    }

    public function __set($name, $value)
    {
        $array = explode('_', $name, 3);

        if (ArrayHelper::getValue($array, 0) === 'position' && count($array) === 3) {

            $position  = ArrayHelper::getValue($array, 1);
            $attribute = ArrayHelper::getValue($array, 2);
            if (ArrayHelper::keyExists($position, $this->_models) && ArrayHelper::keyExists($attribute,
                    $this->_models[$position])) {
                $this->_models[$position][$attribute] = $value;
            } else {
                parent::__set($name, $value);
            }
        } else {
            parent::__set($name, $value);
        }
    }

    public function attributeLabels()
    {
        return [
            'position_car_carCallsign' => t('car', 'Callsign'),
            'position_car_carLicense'  => t('car', 'License to work in a taxi'),

        ];
    }


    public function getModels()
    {
        return $this->_models;
    }

    public static function getNameList()
    {
        return [
            FieldRecord::NAME_CAR_CALLSIGN,
            FieldRecord::NAME_CAR_LICENSE,
        ];
    }

    public function getAttributesByPosition($positionId)
    {
        if (ArrayHelper::keyExists($positionId, $this->_models)) {
            $attributes = ArrayHelper::getValue($this->_models, $positionId);

            return array_keys($attributes);
        }

        return [];
    }
}