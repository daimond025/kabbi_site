<?php

namespace frontend\modules\tenant\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%user_default_right}}".
 *
 * @property integer $id
 * @property integer $position_id
 * @property string $permission
 * @property string $rights
 *
 * @property AuthItem $permission0
 * @property UserPosition $position
 */
class UserDefaultRight extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_default_right}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position_id', 'permission'], 'required'],
            [['position_id'], 'integer'],
            [['rights'], 'string'],
            [['permission'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'position_id' => 'Position ID',
            'permission' => 'Permission',
            'rights' => 'Rights',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPermission0()
    {
        return $this->hasOne(AuthItem::className(), ['name' => 'permission']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(UserPosition::className(), ['position_id' => 'position_id']);
    }
    
    /**
     * Дефолтные настройки прав для должности.
     * @param integer $position_id
     * @return array
     */
    public static function getDefaultRightSettingByPosition($position_id)
    {
        return ArrayHelper::map(self::findAll(['position_id' => $position_id]), 'permission', 'rights');
    }
}
