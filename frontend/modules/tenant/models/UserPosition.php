<?php

namespace app\modules\tenant\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%tbl_user_position}}".
 *
 * @property string $position_id
 * @property string $name
 *
 * @property TblUser[] $tblUsers
 */
class UserPosition extends \yii\db\ActiveRecord
{
    const POSITION_FOUNDER = 1;
    const POSITION_GENDIRECTOR = 2;
    const POSITION_TECHDIRECTOR = 3;
    const POSITION_ADMINDIRECTOR = 4;
    const POSITION_SYSADMIN = 5;
    const POSITION_DISPATCHER = 6;
    const POSITION_ADMINISTRATOR = 7;
    const POSITION_LEADDISPATCHER = 8;
    const POSITION_BOOKKEEPER = 9;
    const POSITION_LEADBOOKKEEPER = 10;
    const POSITION_BRANCH_MANAGER = 11;
    const POSITION_MANAGER = 12;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_position}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'position_id' => Yii::t('user', 'Position ID'),
            'name' => Yii::t('user', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['position_id' => 'position_id']);
    }

    public static function getUserPositionMap($positionList)
    {
        return ArrayHelper::map($positionList,
            'position_id', function ($item) {
                return t('company-roles', $item['name']);
            });
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->name = t('company-roles', $this->name);
    }

    public static function getUserPositionList()
    {
        $positionList = self::find()->all();
        $positionList = ArrayHelper::map($positionList,'position_id', 'name');
        asort($positionList, SORT_STRING);
        return $positionList;
    }
}
