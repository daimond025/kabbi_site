<?php

namespace frontend\modules\tenant\models;

use common\components\behaviors\ActiveRecordBehavior;
use common\helpers\CacheHelper;
use common\modules\city\models\City;
use common\modules\employee\models\position\Position;
use common\modules\order\models\OrderStatus;
use common\modules\tenant\models\Tenant;
use common\services\OrderStatusService;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_client_push_notifications".
 *
 * @property integer  $push_id
 * @property string   $text
 * @property integer  $type
 * @property integer  $tenant_id
 * @property integer  $city_id
 * @property integer  $position_id
 *
 * @property Tenant   $tenant
 * @property City     $city
 * @property Position $position
 */
class ClientPushNotifications extends \yii\db\ActiveRecord
{
    public static $templateList = [
        OrderStatus::STATUS_NEW,
        OrderStatus::STATUS_GET_WORKER,
        OrderStatus::STATUS_WORKER_WAITING,
        OrderStatus::STATUS_WORKER_LATE,
        OrderStatus::STATUS_EXECUTING,
        OrderStatus::STATUS_COMPLETED_PAID,
        OrderStatus::STATUS_REJECTED,
        OrderStatus::STATUS_PRE,
        OrderStatus::STATUS_PRE_GET_WORKER,
        OrderStatus::STATUS_PRE_REFUSE_WORKER,
        OrderStatus::STATUS_EXECUTION_PRE,
        OrderStatus::STATUS_NO_CARS_BY_TIMER,
        OrderStatus::STATUS_CANCELED_BY_WORKER,
        'CODE'
    ];

    protected $duplicateStatuses = [
        OrderStatus::STATUS_NO_CARS_BY_TIMER => OrderStatus::STATUS_NO_CARS,
    ];


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_push_notifications}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'type'], 'required'],
            [['tenant_id', 'city_id', 'position_id'], 'integer'],
            [['text', 'type'], 'string'],
            [['type'], 'unique', 'targetAttribute' => ['tenant_id', 'city_id', 'position_id', 'type']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'push_id'     => 'Push ID',
            'text'        => t('client-push-notices', 'Text'),
            'type'        => t('client-push-notices', 'Type'),
            'tenant_id'   => 'Tenant ID',
            'city_id'     => 'City ID',
            'position_id' => 'Position ID',
        ];
    }

    public function behaviors()
    {
        return [
            'common' => [
                'class' => ActiveRecordBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }
}

