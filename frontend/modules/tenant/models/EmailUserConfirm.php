<?php
namespace app\modules\tenant\models;

use yii\base\Model;
use Yii;
use yii\helpers\ArrayHelper;
/**
 * Email confirm
 */
class EmailUserConfirm extends Model
{
    private $_user = false;

    /**
     * Confirm registration
     *
     * @return boolean whether the user is logged in successfully
     */
    public function confirm($token)
    {
        if ($user = $this->getUser($token))
        {
            if(app()->user->login($user))
            {
                $user->email_confirm = 1;
                $user->active = 1;
                $user->save(false);

                //Создание служебной записи в таблице tbl_order_views для показа количества непросмотренных заказов
                if(app()->authManager->checkAccess(user()->id, 'read_orders'))
                {
                    $arCity = ArrayHelper::getColumn($user->cities, 'city_id');
                    \app\modules\order\models\OrderViews::batchInsert($arCity, $user->user_id, $user->tenant_id);
                }

                return true;
            }
        }

        return false;
    }

    /**
     * Find user by [[token]]
     *
     * @return user|null
     */
    public function getUser($token)
    {
        if ($this->_user === false)
        {
            $this->_user = User::findOne(['email_confirm' => $token]);
        }

        return $this->_user;
    }
}
