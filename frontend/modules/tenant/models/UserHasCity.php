<?php

namespace frontend\modules\tenant\models;

use app\modules\order\models\Order;
use app\modules\tenant\models\User;
use common\modules\tenant\models\TenantHasCity;
use frontend\components\repositories\TenantRepository;
use Yii;
use common\modules\city\models\City;
use common\modules\city\models\Republic;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%tbl_user_has_city}}".
 *
 * @property string $city_id
 * @property string $user_id
 *
 * @property Order[] $tblOrders
 * @property User $user
 * @property City $city
 */
class UserHasCity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_has_city}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'user_id'], 'required'],
            [['city_id', 'user_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'city_id' => Yii::t('user', 'City ID'),
            'user_id' => Yii::t('user', 'User ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantHasCities()
    {
        return $this->hasOne(TenantHasCity::className(), ['city_id' => 'city_id'])
            ->onCondition([TenantHasCity::tableName() . '.tenant_id' => user()->tenant_id]);
    }

    /**
     * Allow to save multiple city
     * @param array $arCity
     * @param array $user_id
     * @return boolean
     */
    public static function manySave($arCity, $user_id)
    {
        if (is_array($arCity) && !empty($arCity)) {
            $insertValue = [];
            $connection = app()->db;
            $arCity = array_unique($arCity);

            foreach ($arCity as $city_id) {
                if ($city_id > 0) {
                    $insertValue[] = [$city_id, $user_id];
                }
            }

            if (!empty($insertValue)) {
                $connection->createCommand()
                    ->batchInsert(self::tableName(), ['city_id', 'user_id'], $insertValue)->execute();
                self::deleteCache($user_id);

                return true;
            }
        }

        return false;
    }

    public static function deleteUserCityListWithRepublic($user_id)
    {
        app()->cache->delete('UserCityListWithRepublic_' . user()->tenant_id . '_' . $user_id . getLanguagePrefix());
    }

    public static function deleteUserCityList($userId)
    {
        $userCityCacheKey = 'user_has_city_' . $userId . '_' . getLanguagePrefix();
        app()->cache->delete($userCityCacheKey);
    }

    public static function getCityList()
    {
        if (Yii::$app->user->can(User::ROLE_EXTRINSIC_DISPATCHER)) {
            /* @var $tenantRepository TenantRepository */
            $tenantRepository = Yii::createObject(TenantRepository::class);

            return $tenantRepository->getCityList();
        }

        return self::find()->
            asArray()->
            where(['user_id' => user()->user_id])->
            select(self::tableName() . '.city_id')->
            joinWith(['tenantHasCities' => function (ActiveQuery $query) {
                $query->where(['block' => 0]);
            }])->
            with([
                'city' => function (ActiveQuery $query) {
                    $query->with(['republic' => function (ActiveQuery $query) {
                        $query->select(['republic_id',
                        Republic::tableName() . '.name' . getLanguagePrefix()]);
                    }]);
                }])->
            orderBy(TenantHasCity::tableName() . '.sort')->
            all();
    }

    /**
     * Getting user city map
     * @param integer $userId
     * @return array ['city_id => 'name']
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     */
    public static function getUserCityMap($userId)
    {
        if (Yii::$app->user->can(User::ROLE_EXTRINSIC_DISPATCHER)) {
            /* @var $tenantRepository TenantRepository */
            $tenantRepository = Yii::createObject(TenantRepository::class);

            return $tenantRepository->getTenantCityMap();
        }

        $languagePrefix = getLanguagePrefix();

        $cities = self::getDb()->cache(function () use ($userId, $languagePrefix) {
            return self::find()
                ->where(['user_id' => $userId])
                ->with([
                    'city' => function (ActiveQuery $query) use ($languagePrefix) {
                        $query->select(['city_id', 'name' . $languagePrefix]);
                    },
                ])
                ->joinWith([
                    'tenantHasCities' => function (ActiveQuery $query) {
                        $query->where(['block' => 0]);
                    },
                ])
                ->orderBy('sort')
                ->all();
        }, 10);

        return ArrayHelper::map($cities, 'city_id', 'city.name' . $languagePrefix);
    }

    public static function deleteCache($userId)
    {
        self::deleteUserCityListWithRepublic($userId);
        self::deleteUserCityList($userId);
    }

    public static function getUsersIdByCity($cityId)
    {
        $tableName = self::tableName();

        return self::find()
            ->where([$tableName . '.city_id' => $cityId])
            ->select([$tableName . '.user_id'])
            ->joinWith([
                'user' => function (ActiveQuery $query) {
                    $query->where(['tenant_id' => user()->tenant_id]);
                },
            ], false)
            ->column();
    }
}
