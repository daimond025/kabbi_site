<?php

namespace frontend\modules\tenant\models;

use app\modules\tenant\models\field\CarFieldModel;
use app\modules\tenant\models\field\EmployeeFieldModel;
use app\modules\tenant\models\field\FieldRecord;
use common\modules\employee\models\position\Position;
use common\modules\employee\models\position\PositionHasDocument;
use common\modules\tenant\models\TenantCityHasPosition;
use common\modules\tenant\models\TenantHasCity;
use yii\base\Exception;
use yii\helpers\ArrayHelper;

/**
 * Class TenantHasCityForm
 * @package frontend\modules\tenant\models
 */
class TenantHasCityForm extends TenantHasCity
{
    /* @var array */
    public $active_position_ids;

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['active_position_ids', 'required', 'message'=>'Необходимо выбрать действующие профессии'],
        ]);
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'active_position_ids' => t('tenantHasCity', 'Active positions'),
        ]);
    }

    /**
     * @return array
     */
    public function getActivePositionIds()
    {
        $positions = $this->activePositions;

        return empty($positions) ? [] : ArrayHelper::getColumn($positions, 'position_id');
    }

    public function getDefaultActivePositionIds()
    {
        return [Position::TAXI_DRIVER];
    }

    /**
     * @throws \yii\base\Exception
     */
    private function saveActivePositions()
    {
        TenantCityHasPosition::deleteAll([
            'tenant_id' => $this->tenant_id,
            'city_id'   => $this->city_id,
        ]);

        if (is_array($this->active_position_ids)) {
            foreach ($this->active_position_ids as $positionId) {
                $model = new TenantCityHasPosition([
                    'tenant_id'   => $this->tenant_id,
                    'city_id'     => $this->city_id,
                    'position_id' => $positionId,
                ]);

                if (!$model->save()) {
                    throw new Exception(implode(';', $model->getFirstErrors()));
                }
            }
        }
    }

    private function saveShowFields()
    {
        $workerNameList         = EmployeeFieldModel::getWorkerNameList();
        $workerPositionNameList = EmployeeFieldModel::getWorkerPositionNameList();
        $carNameList            = CarFieldModel::getNameList();

        $positions = TenantCityHasPosition::find()
            ->select('position_id')
            ->where([
                'tenant_id' => $this->tenant_id,
                'city_id'   => $this->city_id,
            ])
            ->column();

        $positionHasDocument = PositionHasDocument::find()->where(['position_id' => $positions])->all();
        $positionHasDocument = ArrayHelper::map($positionHasDocument, 'document_id', 'document_id',
            'position_id');

        $data = [];
        foreach ($workerNameList as $name) {
            $data[] = [$this->tenant_id, $this->city_id, null, 'worker', $name, 1];
        }

        foreach ($workerPositionNameList as $documentId => $name) {
            foreach ($positions as $positionId) {
                if (!isset($positionHasDocument[$positionId][$documentId])) {
                    continue;
                }
                $data[] = [$this->tenant_id, $this->city_id, (int)$positionId, 'worker', $name, 1];
            }
        }

        foreach ($carNameList as $name) {
            $data[] = [$this->tenant_id, $this->city_id, null, 'car', $name, 1];
        }

        FieldRecord::deleteAll([
            'tenant_id' => $this->tenant_id,
            'city_id' => $this->city_id,
        ]);

        if (!empty($data)) {
            app()->db->createCommand()->batchInsert(FieldRecord::tableName(),
                ['tenant_id', 'city_id', 'position_id', 'type', 'name', 'value'], $data)->execute();
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->saveActivePositions();
        $this->saveShowFields();

        parent::afterSave($insert, $changedAttributes);
    }
}