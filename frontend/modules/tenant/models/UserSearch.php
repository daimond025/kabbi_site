<?php

namespace app\modules\tenant\models;

use frontend\modules\companies\components\repositories\TenantCompanyRepository;
use frontend\modules\companies\components\services\TenantCompanyService;
use frontend\modules\companies\models\TenantCompany;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * UserSearch represents the model behind the search form about `app\modules\tenant\models\User`.
 */
class UserSearch extends User
{
    const PAGE_SIZE = 20;

    public $formName = null;
    public $stringSearch;
    public $positionList;
    public $type;
    public $city_id;
    public $tenantCompanyIds;

    private $_accessCityList = [];


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stringSearch'], 'string'],
            [['city_id'], 'in', 'range' => $this->getAccessCityList(), 'allowArray' => true],
            [['positionList'], 'safe'],
            [
                ['tenantCompanyIds'],
                'each',
                'rule' => [
                    'match',
                    'pattern' => sprintf('/^\d+|%s$/', TenantCompany::FIELD_NAME_MAIN_COMPANY)
                ]
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'positionList'     => t('user', 'Position'),
            'city_id'          => t('user', 'All cities'),
            'stringSearch'     => t('user', 'Search by name'),
            'position'         => t('user', 'Position'),
            'city_list'        => t('user', 'Working in cities'),
            'fullName'         => t('user', 'Last name Name Second name'),
            'tenantCompanyIds' => t('tenant_company', 'Companies'),
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        /** @var $query ActiveQuery */
        $query = parent::find();
        $query->alias('user');
        $query->distinct();
        // add conditions that should always apply here
        $query->where([
            'tenant_id'    => user()->tenant_id,
            'city.city_id' => $this->getAccessCityList(),
        ]);

        $query->joinWith('userHasCities city', false);

        switch ($this->type) {
            case 'invited':
                $query->andWhere([
                    'not',
                    [
                        'email_confirm' => '1',
                    ],
                ]);
                break;
            case 'blocked':
                $query->andWhere([
                    'active'        => '0',
                    'email_confirm' => '1',
                ]);
                break;
            default:
                $query->andWhere([
                    'email_confirm' => '1',
                    'active'        => '1',
                ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'sort'       => [
                'defaultOrder' => [
                    'fullName' => SORT_ASC,
                ],
                'attributes'   => [
                    'fullName' => [
                        'asc'  => ['last_name' => SORT_ASC, 'name' => SORT_ASC, 'second_name' => SORT_ASC],
                        'desc' => ['last_name' => SORT_DESC, 'name' => SORT_DESC, 'second_name' => SORT_DESC],
                    ],
                ],
            ],
            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);

        $this->load($params, $this->formName);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andWhere(['city.city_id' => $this->getSearchCityList()]);

        $query->andFilterWhere([
            'position_id' => $this->positionList,
        ]);

        // grid filtering conditions
        $query->andFilterWhere([
            'or',
            ['like', 'last_name', $this->stringSearch],
            ['like', 'user.name', $this->stringSearch],
            ['like', 'second_name', $this->stringSearch],
        ]);

        (new TenantCompanyService())
            ->queryChange($query, $this->tenantCompanyIds, 'user.tenant_company_id');

        return $dataProvider;
    }

    public function getAccessCityList()
    {
        return $this->_accessCityList;
    }

    public function setAccessCityList($value)
    {
        $this->_accessCityList = $value;
    }

    private function getSearchCityList()
    {
        return $this->city_id ? $this->city_id : $this->getAccessCityList();
    }

}
