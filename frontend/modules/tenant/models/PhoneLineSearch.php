<?php

namespace app\modules\tenant\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\tenant\models\PhoneLine;
use app\modules\tariff\models\TaxiTariff;
use common\modules\city\models\City;

/**
 * PhoneLineSearch represents the model behind the search form about `app\modules\tenant\models\PhoneLine`.
 */
class PhoneLineSearch extends PhoneLine
{

    private $_accessCityList = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id'], 'in', 'range' => $this->getAccessCityList(), 'allowArray' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'city_id' => t('user', 'All cities'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $block = false)
    {
        $query = self::find();
        $query->where([
            'tenant_id' => user()->tenant_id,
            'block' => (int) $block,
            'city_id'   => $this->getAccessCityList(),
        ]);
        $query->with('tariff');

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $query;
        }

        $query->andWhere(['city_id' => $this->getSearchCityList()]);

        return $query;
    }

    public function getAccessCityList()
    {
        return $this->_accessCityList;
    }

    public function setAccessCityList($value)
    {
        $this->_accessCityList = $value;
    }

    private function getSearchCityList()
    {
        return $this->city_id ? $this->city_id : $this->getAccessCityList();
    }
}