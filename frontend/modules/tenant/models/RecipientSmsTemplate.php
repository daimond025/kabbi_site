<?php

namespace frontend\modules\tenant\models;

use common\components\behaviors\ActiveRecordBehavior;
use common\helpers\CacheHelper;
use common\modules\city\models\City;
use common\modules\employee\models\position\Position;
use common\modules\order\models\OrderStatus;
use common\modules\tenant\models\Tenant;
use common\services\OrderStatusService;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%recipient_sms_template}}".
 *
 * @property integer  $template_id
 * @property integer  $tenant_id
 * @property string   $type
 * @property string   $text
 * @property integer  $city_id
 * @property integer  $position_id
 *
 * @property Tenant   $tenant
 * @property City     $city
 * @property Position $position
 */
class RecipientSmsTemplate extends \yii\db\ActiveRecord
{
    public static $templateList = [
        OrderStatus::STATUS_EXECUTING,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%recipient_sms_template}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'text'], 'required'],
            [['type'], 'unique', 'targetAttribute' => ['type', 'tenant_id', 'city_id', 'position_id']],
            [['tenant_id', 'position_id', 'city_id'], 'integer'],
            [['text'], 'string'],
            [['type'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'template_id' => 'Template ID',
            'tenant_id'   => 'Tenant ID',
            'city_id'     => 'City ID',
            'position_id' => 'Position ID',
            'type'        => t('sms-template', 'Type'),
            'text'        => t('sms-template', 'Text'),
        ];
    }

    public function behaviors()
    {
        return [
            'common' => [
                'class' => ActiveRecordBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }
}
