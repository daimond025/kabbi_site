<?php

namespace frontend\modules\tenant\models\entities;

use common\modules\city\models\City;
use common\modules\tenant\models\Tenant;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%tenant_city_has_paygate_profile}}".
 *
 * @property integer        $id
 * @property integer        $tenant_id
 * @property integer        $city_id
 * @property integer        $profile_id
 * @property integer        $created_at
 * @property integer        $updated_at
 *
 * @property City           $city
 * @property PaygateProfile $paygateProfile
 * @property Tenant         $tenant
 */
class TenantCityHasPaygateProfile extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_city_has_paygate_profile}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'city_id', 'profile_id'], 'required'],
            [['tenant_id', 'city_id', 'profile_id', 'created_at', 'updated_at'], 'integer'],
            [
                ['tenant_id', 'city_id'],
                'unique',
                'targetAttribute' => ['tenant_id', 'city_id'],
                'message'         => 'The combination of Tenant ID and City ID has already been taken.',
            ],
            [
                ['city_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => City::className(),
                'targetAttribute' => ['city_id' => 'city_id'],
            ],
            [
                ['profile_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => PaygateProfile::className(),
                'targetAttribute' => ['profile_id' => 'id'],
            ],
            [
                ['tenant_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Tenant::className(),
                'targetAttribute' => ['tenant_id' => 'tenant_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'tenant_id'  => Yii::t('app', 'Tenant ID'),
            'city_id'    => Yii::t('app', 'City ID'),
            'profile_id' => Yii::t('app', 'Profile ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaygateProfile()
    {
        return $this->hasOne(PaygateProfile::className(), ['id' => 'profile_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }
}
