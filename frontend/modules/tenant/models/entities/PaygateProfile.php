<?php

namespace frontend\modules\tenant\models\entities;

use common\modules\city\models\City;
use common\modules\tenant\models\Tenant;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%paygate_profile}}".
 *
 * @property integer                       $id
 * @property integer                       $tenant_id
 * @property string                        $profile
 * @property integer                       $created_at
 * @property integer                       $updated_at
 *
 * @property Tenant                        $tenant
 * @property TenantCityHasPaygateProfile[] $tenantCityHasPaygateProfiles
 */
class PaygateProfile extends \yii\db\ActiveRecord
{
    const TEMPLATE_PROFILE = '%d_%d';

    public $city_id;

    public function behaviors()
    {
        return [
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%paygate_profile}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'city_id'], 'required'],
            [['tenant_id', 'created_at', 'updated_at'], 'integer'],
            [['profile'], 'string', 'max' => 32],
            [['profile'], 'unique'],
            [
                ['tenant_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Tenant::className(),
                'targetAttribute' => ['tenant_id' => 'tenant_id'],
            ],
            [
                ['city_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => City::className(),
                'targetAttribute' => ['city_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'tenant_id'  => Yii::t('app', 'Tenant ID'),
            'profile'    => Yii::t('app', 'Profile'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }


    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            $this->profile = sprintf(self::TEMPLATE_PROFILE, $this->tenant_id, $this->id);
            $this->update(false, ['profile']);

            $tenantCityHasPaygateProfile = new TenantCityHasPaygateProfile([
                'tenant_id'  => $this->tenant_id,
                'city_id'    => $this->city_id,
                'profile_id' => $this->id,
            ]);
            $tenantCityHasPaygateProfile->save();
        }
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantCityHasPaygateProfiles()
    {
        return $this->hasMany(TenantCityHasPaygateProfile::className(), ['profile_id' => 'id']);
    }
}
