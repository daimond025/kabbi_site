<?php

namespace frontend\modules\tenant\models;

use app\modules\tenant\models\AutoGeoServiceType;
use app\modules\tenant\models\GeoServiceProviderDefault;
use app\modules\tenant\models\RoutingServiceType;
use common\modules\city\models\City;
use common\modules\tenant\models\Tenant;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class GeoServiceForm
 * @package frontend\modules\tenant\models
 *
 * @property AutoGeoServiceType[] $_autoGeoServiceTypes
 * @property RoutingServiceType[] $_routingServiceTypes
 *
 * @property GeoServiceProvider   $_routingServiceModel
 * @property GeoServiceProvider   $_autoGeoServiceModel
 *
 * @property GeoServiceProviderDefault   $_autoGeoServiceTypesDefault
 * @property GeoServiceProviderDefault   $_routingServiceTypesDefault
 *
 * @property integer              $routing_tenant_id
 * @property integer              $routing_city_id
 * @property integer              $routing_service_provider_id
 * @property string               $routing_key_1
 * @property string               $routing_key_2
 *
 * @property integer              $auto_tenant_id
 * @property integer              $auto_city_id
 * @property integer              $auto_service_provider_id
 * @property string               $auto_key_1
 * @property string               $auto_key_2
 * @property string               $default
 *
 */
class GeoServiceForm extends Model
{
    protected $_autoGeoServiceTypes;
    protected $_routingServiceTypes;

    protected $_autoGeoServiceModel = null;
    protected $_routingServiceModel = null;

    protected $_autoGeoServiceTypesDefault = null;
    protected $_routingServiceTypesDefault = null;

    const PREFIX_ROUTING = 'routing';
    const PREFIX_AUTO_GEO = 'auto';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->_autoGeoServiceTypes = AutoGeoServiceType::find()->where(['active' => 1])->indexBy('id')->all();
        $this->_routingServiceTypes = RoutingServiceType::find()->where(['active' => 1])->indexBy('id')->all();

    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            'routing_tenant_id',
            'routing_city_id',
            'routing_service_provider_id',
            'routing_key_1',
            'routing_key_2',

            'auto_tenant_id',
            'auto_city_id',
            'auto_service_provider_id',
            'auto_key_1',
            'auto_key_2',
            'default',

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'routing_service_provider_id' => t('setting', 'Route calculation'),
            'auto_service_provider_id'    => t('setting', 'Autocomplete and geocoding'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['routing_tenant_id', 'auto_tenant_id'],
                'exist',
                'targetClass'     => Tenant::className(),
                'targetAttribute' => 'tenant_id',
            ],
            [
                ['routing_city_id', 'auto_city_id'],
                'exist',
                'targetClass'     => City::className(),
                'targetAttribute' => 'city_id',
            ],

            [
                'routing_service_provider_id',
                'in',
                'range' => $this->getRoutingServiceProviderIds(),
            ],

            [
                'auto_service_provider_id',
                'in',
                'range' => $this->getAutoGeoServiceProviderIds(),
            ],

            [
                'default',
                'in',
                'range' => [0,1],
            ],

            [
                ['routing_key_1', 'routing_key_2', 'auto_key_1', 'auto_key_2'],
                'string',
                'max' => 50,
            ],

            [
                'routing_key_1',
                'required',
                'when'       => function ($model) {
                    $modelType = ArrayHelper::getValue($this->_routingServiceTypes,
                        $model->routing_service_provider_id);

                    return $modelType && $modelType->has_key_1;
                },
                'whenClient' => 'function(attr,value){
                    return false;
                }',
                'message'    => t('app', 'Field cannot be blank'),
            ],

            [
                'routing_key_2',
                'required',
                'when'       => function ($model) {
                    $modelType = ArrayHelper::getValue($this->_routingServiceTypes,
                        $model->routing_service_provider_id);

                    return $modelType && $modelType->has_key_2;
                },
                'whenClient' => 'function(attr,value){
                    return false;
                }',
                'message'    => t('app', 'Field cannot be blank'),
            ],

            [
                'auto_key_1',
                'required',
                'when'       => function ($model) {
                    $modelType = ArrayHelper::getValue($this->_autoGeoServiceTypes, $model->auto_service_provider_id);

                    return $modelType && $modelType->has_key_1;
                },
                'whenClient' => 'function(attr,value){
                    return false;
                }',
                'message'    => t('app', 'Field cannot be blank'),
            ],

            [
                'auto_key_2',
                'required',
                'when'       => function ($model) {
                    $modelType = ArrayHelper::getValue($this->_autoGeoServiceTypes, $model->auto_service_provider_id);

                    return $modelType && $modelType->has_key_2;
                },
                'whenClient' => 'function(attr,value){
                    return false;
                }',
                'message'    => t('app', 'Field cannot be blank'),
            ],

        ];
    }

    /**
     * Возвращает массив идентификаторов всех доступных провайдеров для Расчета маршрута
     *
     * @return array
     */
    public function getRoutingServiceProviderIds()
    {
        return array_keys($this->_routingServiceTypes);
    }

    /**
     * Возвращает массив идентификаторов доступных провайдеров для Автокомплита и геокодинга
     *
     * @return array
     */
    public function getAutoGeoServiceProviderIds()
    {
        return array_keys($this->_autoGeoServiceTypes);
    }


    public function getRoutingServiceProviderNameList()
    {
        return ArrayHelper::map($this->_routingServiceTypes, 'id', function ($model) {
            return t('setting', $model->service_label);
        });
    }

    public function getAutoGeoServiceProviderNameList()
    {
        return ArrayHelper::map($this->_autoGeoServiceTypes, 'id', function ($model) {
            return t('setting', $model->service_label);
        });
    }

    /**
     * Получить или создать экземпляр класса
     *
     * @param integer $tenantId
     * @param integer $cityId
     *
     * @return GeoServiceForm
     */
    public static function findOrCreate($tenantId, $cityId)
    {
        $domain              = Tenant::getDomainName($tenantId);
        $autoGeoServiceModel = GeoServiceProvider::findOneProvider($tenantId, $domain, $cityId,
            GeoServiceProvider::AUTO_GEO_SERVICE_TYPE);

        $routingServiceModel = GeoServiceProvider::findOneProvider($tenantId, $domain, $cityId,
            GeoServiceProvider::ROUTING_SERVICE_TYPE);

        $model = new self();

        $model->setAutoGeoServiceModel($autoGeoServiceModel);
        $model->setRoutingServiceModel($routingServiceModel);


        // дефолтные значения
        $autoGeoServiceModelDefault = GeoServiceProviderDefault::findOneProvider($tenantId, $domain, $cityId,
            GeoServiceProvider::AUTO_GEO_SERVICE_TYPE);

        $routingServiceModelDefault = GeoServiceProviderDefault::findOneProvider($tenantId, $domain, $cityId,
            GeoServiceProvider::ROUTING_SERVICE_TYPE);

        $model->setAutoGeoServiceModelDefault($autoGeoServiceModelDefault);
        $model->setRoutingServiceModelDefault($routingServiceModelDefault);

        return $model;
    }

    public function save($default = 0)
    {
        if (!$this->beforeSave()) {
            return false;
        }

        if($default){
            $this->saveDefault();
        }


        $transaction = app()->db->beginTransaction();

        if($default){
            if ($this->_autoGeoServiceModel->save() && $this->_routingServiceModel->save() && $this->_autoGeoServiceTypesDefault->save(false) && $this->_routingServiceTypesDefault->save(false)) {
                $transaction->commit();

                // очистка кеша в сервисе гео
                $this->clearCasheOn();
                return true;
            }
        }elseif ($default === 0 ){
            if ($this->_autoGeoServiceModel->save() && $this->_routingServiceModel->save()) {
                $this->clearCasheOn();
                $transaction->commit();

                return true;
            }
        }



        $transaction->rollBack();

        return false;
    }

    public function clearCasheOn($action = 'clear'){

        $autoComplete = app()->autocomplete;

        $url = $autoComplete->url .$action;

        $curl       = app()->curl;
        $response   = $curl->get($url, []);
        $statusCode = $response->headers['Status-Code'];

        if ($statusCode != 200) {
            $error = $curl->error();

            return false;
        }

        return true;


    }

    public function saveDefault(){
        GeoServiceProviderDefault::deleteAll();

        // построчное присование параметров
        $geoServiceAuto = new GeoServiceProviderDefault();
        $geoServiceAuto->service_type = $this->_autoGeoServiceModel->service_type;
        $geoServiceAuto->tenant_id = $this->_autoGeoServiceModel->tenant_id;
        $geoServiceAuto->city_id = $this->_autoGeoServiceModel->city_id;
        $geoServiceAuto->service_provider_id = $this->_autoGeoServiceModel->service_provider_id;
        $geoServiceAuto->key_1 = isset($this->_autoGeoServiceModel->key_1) ? $this->_autoGeoServiceModel->key_1 : "" ;
        $geoServiceAuto->key_2 = isset($this->_autoGeoServiceModel->key_2) ? $this->_autoGeoServiceModel->key_2 : "" ;
        $this->setAutoGeoServiceModelDefault($geoServiceAuto);


        $geoServiceRouting = new GeoServiceProviderDefault();
        $geoServiceRouting->service_type = $this->_routingServiceModel->service_type;
        $geoServiceRouting->tenant_id = $this->_routingServiceModel->tenant_id;
        $geoServiceRouting->city_id = $this->_routingServiceModel->city_id;
        $geoServiceRouting->service_provider_id = $this->_routingServiceModel->service_provider_id;
        $geoServiceRouting->key_1 = $this->_routingServiceModel->key_1;
        $geoServiceRouting->key_2 = $this->_routingServiceModel->key_2;
        $this->setRoutingServiceModelDefault($geoServiceRouting);

    }

    public function beforeSave()
    {
        if (!ArrayHelper::getValue($this->_routingServiceTypes, $this->routing_service_provider_id . '.has_key_1',
            false)) {
            $this->routing_key_1 = null;
        }
        if (!ArrayHelper::getValue($this->_routingServiceTypes, $this->routing_service_provider_id . '.has_key_2',
            false)) {
            $this->routing_key_2 = null;
        }
        if (!ArrayHelper::getValue($this->_autoGeoServiceTypes, $this->auto_service_provider_id . '.has_key_1', false)) {
            $this->auto_key_1 = null;
        }
        if (!ArrayHelper::getValue($this->_autoGeoServiceTypes, $this->auto_service_provider_id . '.has_key_2', false)) {
            $this->auto_key_2 = null;
        }

        return true;
    }

    public function getAutoGeoServiceTypes()
    {
        return $this->_autoGeoServiceTypes;
    }

    public function getRoutingServiceTypes()
    {
        return $this->_routingServiceTypes;
    }

    public function setAutoGeoServiceModel($model)
    {
        $this->_autoGeoServiceModel = $model;
    }

    public function setRoutingServiceModel($model)
    {
        $this->_routingServiceModel = $model;
    }


    // для дефольтных значений
    public function getAutoGeoServiceTypesDefault()
    {
        return $this->_autoGeoServiceTypesDefault;
    }
    public function getRoutingServiceTypesDefault()
    {
        return $this->_routingServiceTypesDefault;
    }

    public function setAutoGeoServiceModelDefault($model)
    {
        $this->_autoGeoServiceTypesDefault = $model;
    }

    public function setRoutingServiceModelDefault($model)
    {
        $this->_routingServiceTypesDefault = $model;
    }

    public function __get($name)
    {
        // Ищем аттрибуты с префиксом "routing_"
        if (preg_match('#^' . self::PREFIX_ROUTING . '_(\w+)$#', $name, $data)) {
            $attribute = $data[1];
            if ($this->_routingServiceModel->hasAttribute($attribute)) {
                return $this->_routingServiceModel->$attribute;
            }
        }
        // Ищем аттрибуты с префиксом "auto_"
        if (preg_match('#^' . self::PREFIX_AUTO_GEO . '_(\w+)$#', $name, $data)) {
            $attribute = $data[1];
            if ($this->_autoGeoServiceModel->hasAttribute($attribute)) {
                return $this->_autoGeoServiceModel->$attribute;
            }
        }

        // проверка того  что это дефолтное значеник
        if($name === 'default'){

            $default =   isset( $this->_autoGeoServiceTypesDefault) ? 1 : 0 ;
             if($default){
                 $this->_autoGeoServiceModel->default = 1;
             } else{
                 $this->_autoGeoServiceModel->default = 0;
             }

             return $this->_autoGeoServiceModel->default;
        }



        return parent::__get($name);
    }

    public function __set($name, $value)
    {
        // Ищем аттрибуты с префиксом "routing_"
        if (preg_match('#^' . self::PREFIX_ROUTING . '_(\w+)$#', $name, $data)) {
            $attribute = $data[1];

            if ($this->_routingServiceModel->hasAttribute($attribute)) {
                $this->_routingServiceModel->$attribute = $value;

                return;
            }
        }

        // Ищем аттрибуты с префиксом "auto_"
        if (preg_match('#^' . self::PREFIX_AUTO_GEO . '_(\w+)$#', $name, $data)) {
            $attribute = $data[1];
            if ($this->_autoGeoServiceModel->hasAttribute($attribute)) {
                $this->_autoGeoServiceModel->$attribute = $value;

                return;
            }
        }

        if($name === 'default'){
            $this->default = $value;
            return;
        }

        parent::__set($name, $value);
    }

}