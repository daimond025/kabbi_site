<?php

namespace frontend\modules\tenant\models;

use common\components\behaviors\ActiveRecordBehavior;
use common\helpers\CacheHelper;
use common\modules\city\models\City;
use common\modules\employee\models\position\Position;
use common\modules\order\models\OrderStatus;
use common\modules\tenant\models\Tenant;
use common\services\OrderStatusService;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_worker_push_notifications".
 *
 * @property integer  $push_id
 * @property string   $text
 * @property integer  $type
 * @property integer  $tenant_id
 * @property integer  $city_id
 * @property integer  $position_id
 *
 * @property Tenant   $tenant
 * @property City     $city
 * @property Position $position
 */
class WorkerPushNotifications extends \yii\db\ActiveRecord
{
    public static $templateList = [
        OrderStatus::STATUS_NEW,
        OrderStatus::STATUS_PRE,
    ];


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_push_notifications}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'type'], 'required'],
            [['tenant_id', 'city_id', 'position_id'], 'integer'],
            [['text', 'type'], 'string'],
            [['type'], 'unique', 'targetAttribute' => ['tenant_id', 'city_id', 'position_id', 'type']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'push_id'     => 'Push ID',
            'text'        => t('client-push-notices', 'Text'),
            'type'        => t('client-push-notices', 'Type'),
            'tenant_id'   => 'Tenant ID',
            'city_id'     => 'City ID',
            'position_id' => 'Position ID',
        ];
    }

    public function behaviors()
    {
        return [
            'common' => [
                'class' => ActiveRecordBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }
}

