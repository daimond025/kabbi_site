<?php

namespace app\modules\tenant\models;

use common\modules\tenant\models\Tenant;
use common\modules\tenant\models\TenantHasSms;
use Yii;

/**
 * This is the model class for table "{{%auto_geo_key_default}}".

 */
class GeoServiceProviderDefault extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%auto_geo_key_default}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['service_type', 'tenant_id', 'tenant_domain', 'city_id', 'service_provider_id',  ], 'required'],
            [['row_id', 'service_type', 'service_provider_id', 'key_2' ], 'string'],
            [['tenant_id', 'city_id', 'service_provider_id' ], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'row_id' => 'Row search',
            'service_type' => 'Service type',
            'tenant_id' => 'Tenant id',
            'tenant_domain' => 'tenant domain',
            'city_id' => 'city id',
            'service_provider_id' => 'service provider id',
            'key_1' => 'key_1',
            'key_2' => 'key_2',
        ];
    }

    public static function findOneProvider($tenantId, $tenantDomain, $cityId, $serviceType)
    {
        $provider = GeoServiceProviderDefault::find()
            ->where([
                'row_id' => $tenantId . "_" . $cityId . '_' . $serviceType,
            ])
            ->one();

        return $provider;
    }

    public function beforeSave($insert)
    {

        if (parent::beforeSave($insert)) {
            $this->attributes = [
                'row_id'        => implode('_', [$this->tenant_id, $this->city_id, $this->service_type]),
                'tenant_domain' => Tenant::getDomainById($this->tenant_id),
            ];
            return true;
        }

        return false;
    }
}
