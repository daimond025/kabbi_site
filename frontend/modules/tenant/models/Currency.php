<?php

namespace frontend\modules\tenant\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "tbl_currency".
 *
 * @property integer      $currency_id
 * @property integer      $type_id
 * @property string       $name
 * @property string       $code
 *
 * @property Account[]    $accounts
 * @property CurrencyType $type
 */
class Currency extends \yii\db\ActiveRecord
{
    const CURRENCY_ID_RUS_RUBLE = 1;
    const CURRENCY_ID_AZN_MANAT = 3;
    const CURRENCY_ID_UZS = 15;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%currency}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'name'], 'required'],
            [['type_id'], 'integer'],
            [['name', 'code'], 'string', 'max' => 255],
            [['symbol'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'currency_id' => 'Currency ID',
            'type_id'     => 'Type ID',
            'name'        => 'Name',
            'code'        => 'Code',
        ];
    }

    public static function getCurrencySymbol($currencyId)
    {
        return app()->cache->getOrSet(__CLASS__ . "_currency_symbol_$currencyId", function () use ($currencyId) {
            return self::find()
                ->select('symbol')
                ->where(['currency_id' => $currencyId])
                ->scalar();
        }, 2 * 60 * 60, new \yii\caching\TagDependency(['tags' => 'currency']));
    }

    public static function getCurrencyCode($currencyId)
    {
        return app()->db->cache(function ($db) use ($currencyId) {
            return self::find()
                ->select('code')
                ->where(['currency_id' => $currencyId])
                ->scalar();
        }, 2 * 60 * 60, new \yii\caching\TagDependency(['tags' => 'currency']));
    }

    public static function getMinorUnit($currencyId)
    {
        return app()->db->cache(function ($db) use ($currencyId) {
            return self::find()
                ->select('minor_unit')
                ->where(['currency_id' => $currencyId])
                ->scalar();
        }, 2 * 60 * 60, new \yii\caching\TagDependency(['tags' => 'currency']));
    }

    /**
     * Getting currency map ['currency_id' => 'name']
     * @return array
     */
    public static function getCurrencyMap()
    {
        $currencies    = self::getCurrencies();
        $arCurrencyMap = [];

        foreach ($currencies as $currency) {
            $arCurrencyMap[$currency->currency_id] = t('currency', Html::encode($currency->name));
        }

        return $arCurrencyMap;
    }

    public static function getSymbolMapIndexedById()
    {
        return ArrayHelper::map(self::getCurrencies(), 'currency_id', 'symbol');
    }

    /**
     * Getting currency code map ['code' => 'symbol']
     * @return array
     */
    public static function getSymbolMapIndexedByNumericCode()
    {
        $currencies    = self::getCurrencies();
        $arCurrencyMap = [];

        foreach ($currencies as $currency) {
            $arCurrencyMap[$currency->numeric_code] = t('currency', Html::encode($currency->symbol));
        }

        return $arCurrencyMap;
    }

    public static function getNumberCurrencyMap()
    {
        $currencies = self::getCurrencies();

        return empty($currencies) ? [] : ArrayHelper::map($currencies, 'numeric_code', function ($model) {
            return t('currency', Html::encode($model->name));
        });
    }

    private static function getCurrencies()
    {
        return self::find()->where(['type_id' => 1])->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccounts()
    {
        return $this->hasMany(Account::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(CurrencyType::className(), ['type_id' => 'type_id']);
    }

}
