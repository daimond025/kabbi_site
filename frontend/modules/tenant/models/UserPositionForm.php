<?php

namespace app\modules\tenant\models;

use frontend\modules\tenant\models\UserDefaultRight;
use Yii;

class UserPositionForm extends UserFormBase
{
    public $position_id;
    public $position_list = [];
    public $rights;
    public $rights_map;
    public $permissions;
    public $permission_list;
    public $defaultRights;

    public function init()
    {
        parent::init();
        /** @var $user User */
        $user = $this->getUser();
        $this->position_id = $user->position_id;
        $this->position_list = $this->canEdit() ? $user->getPositionList() : UserPosition::getUserPositionMap($this->getPositionList());
        $this->rights_map = User::getRightsVariantsOfPermission();
        $this->permissions = $user->getPermissions();
        $this->permission_list = Yii::$app->authManager->getPermissions();
        $this->defaultRights = empty($this->permissions) ? UserDefaultRight::getDefaultRightSettingByPosition($this->position_id) : null;
    }

    public function rules()
    {
        return [
            [['position_id'], 'required'],
            [['position_id'], 'in', 'range' => array_keys($this->position_list)],
            [['rights'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'position_id' => t('user', 'Position'),
        ];
    }

    public function save()
    {
        if ($this->validate()) {
            /** @var $user User */
            $user = $this->getUser();
            $user->position_id = $this->position_id;
            $user->rights = $this->rights;
            $user->isRightsChange = !empty($user->rights);

            return $user->save(false, ['position_id']);
        }

        return false;
    }

    public function canEdit()
    {
        return app()->user->can('users',
                ['user_id' => $this->user_id]) && (user()->id != $this->user_id) && (user()->getUserRole() !== User::ROLE_STAFF);
    }

    public function getUserRightsByPermission($name)
    {
        return $this->getUser()->getUserRightsByPermission($name);
    }

    private function getPositionList()
    {
        return UserPosition::find()->all();
    }
}