<?php

namespace frontend\modules\tenant\models;

use app\modules\order\models\OrderStatus;
use common\components\behaviors\ActiveRecordBehavior;
use common\modules\city\models\City;
use common\modules\employee\models\position\Position;
use common\modules\tenant\models\DefaultClientStatusEvent;
use common\modules\tenant\models\Tenant;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%client_status_event}}".
 *
 * @property integer     $event_id
 * @property integer     $tenant_id
 * @property integer     $position_id
 * @property integer     $city_id
 * @property integer     $status_id
 * @property string      $group
 * @property string      $notice
 *
 * @property Tenant      $tenant
 * @property Position    $position
 * @property City        $city
 * @property OrderStatus $status
 */
class ClientStatusEvent extends \yii\db\ActiveRecord
{
    private static $cachedStatuses;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'common' => [
                'class' => ActiveRecordBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_status_event}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notice', 'status_id', 'position_id', 'city_id'], 'required'],
            [['status_id', 'tenant_id', 'position_id', 'city_id'], 'integer'],
            [['notice'], 'string'],
            [['group'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'event_id'    => 'Event ID',
            'tenant_id'   => 'Tenant ID',
            'position_id' => t('status_event', 'Position'),
            'city_id'     => t('status_event', 'City'),
            'status_id'   => t('status_event', 'Status'),
            'group'       => t('status_event', 'Group'),
            'notice'      => t('status_event', 'Notice'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['status_id' => 'status_id']);
    }


    /**
     * @param int $positionId
     *
     * @return array
     */
    public static function getStatusMap($positionId = null)
    {
        if (static::$cachedStatuses === null) {
            static::$cachedStatuses = DefaultClientStatusEvent::getStatusMap($positionId);
        }

        return static::$cachedStatuses;
    }

    /**
     * @param string $group
     *
     * @return array
     */
    public static function getStatusMapByGroup($group)
    {
        $statusMap = static::getStatusMap();

        return $group === DefaultClientStatusEvent::GROUP_BORDER
            ? [37 => $statusMap[37]] : $statusMap;
    }

    /**
     * @param string $group
     *
     * @return array
     */
    public static function getNoticeMapByGroup($group)
    {
        $availableNotices = [];
        switch ($group) {
            case DefaultClientStatusEvent::GROUP_CALL:
            case DefaultClientStatusEvent::GROUP_WEB:
                $availableNotices = [
                    DefaultClientStatusEvent::NOTICE_SMS,
                    DefaultClientStatusEvent::NOTICE_AUTOCALL,
                    DefaultClientStatusEvent::NOTICE_NOTHING,
                ];
                break;
            case DefaultClientStatusEvent::GROUP_APP:
                $availableNotices = [
                    DefaultClientStatusEvent::NOTICE_SMS,
                    DefaultClientStatusEvent::NOTICE_AUTOCALL,
                    DefaultClientStatusEvent::NOTICE_PUSH,
                    DefaultClientStatusEvent::NOTICE_NOTHING,
                ];
                break;
            case DefaultClientStatusEvent::GROUP_BORDER:
                $availableNotices = [DefaultClientStatusEvent::NOTICE_SMS, DefaultClientStatusEvent::NOTICE_NOTHING];
                break;
        }

        return array_filter(DefaultClientStatusEvent::getNoticeMap(), function ($key) use ($availableNotices) {
            return in_array($key, $availableNotices, false);
        }, ARRAY_FILTER_USE_KEY);
    }

    /**
     * Getting models
     *
     * @param int $tenantId
     * @param int $cityId
     * @param int $positionId
     *
     * @return static[]
     */
    public static function getModels($tenantId, $cityId, $positionId)
    {
        $models = ClientStatusEvent::find()
            ->where([
                'tenant_id'   => $tenantId,
                'city_id'     => $cityId,
                'position_id' => $positionId,
            ])
            ->orderBy(['group' => SORT_ASC, 'status_id' => SORT_ASC])
            ->all();

        return empty($models) ? [] : $models;
    }

    /**
     * @return array
     */
    public static function getStatusesWithoutCall()
    {
        $arStatuses = [
            OrderStatus::STATUS_PRE_GET_WORKER,
            OrderStatus::STATUS_PRE_REFUSE_WORKER,
            OrderStatus::STATUS_EXECUTING,
            OrderStatus::STATUS_COMPLETED_PAID,
            OrderStatus::STATUS_CANCELED_BY_WORKER
        ];

        $arRejectedStatuses = OrderStatus::getRejectedStatusId();

        return array_merge($arStatuses, $arRejectedStatuses);
    }

    /**
     * @return array
     */
    public static function getStatusesPreorder()
    {
        return [
            OrderStatus::STATUS_PRE,
            OrderStatus::STATUS_PRE_GET_WORKER,
            OrderStatus::STATUS_PRE_REFUSE_WORKER,
        ];
    }

    /**
     * @param $positionId
     *
     * @return array
     */
    public static function getDefaultClientStatusEvents($positionId)
    {
        $models = DefaultClientStatusEvent::find()
            ->select(['status_id', 'position_id', 'group', 'notice'])
            ->where(['position_id' => $positionId])
            ->asArray()
            ->all();

        return empty($models) ? [] : $models;
    }


    /**
     * @param int $tenantId
     * @param int $cityId
     * @param int $positionId
     *
     * @throws \Exception
     */
    public static function initDefaultEvents($tenantId, $cityId, $positionId)
    {
        $transaction = app()->db->beginTransaction();
        try {
            static::deleteAll([
                'tenant_id'   => $tenantId,
                'city_id'     => $cityId,
                'position_id' => $positionId,
            ]);

            static::batchInsert($tenantId, $cityId, $positionId, static::getDefaultClientStatusEvents($positionId));

            $transaction->commit();
        } catch (\Exception $ex) {
            $transaction->rollBack();
            \Yii::error($ex, 'client-status-event');
            throw $ex;
        }
    }

    /**
     * @param int    $statusId
     * @param string $group
     *
     * @return string
     */
    private static function generateEventKey($statusId, $group)
    {
        return implode('_', [$statusId, $group]);
    }

    /**
     * @param int $tenantId
     * @param int $cityId
     * @param int $positionId
     *
     * @throws \Exception
     */
    public static function copyMissedDefaultEvents($tenantId, $cityId, $positionId)
    {
        $eventKeys     = ArrayHelper::getColumn(static::getModels($tenantId, $cityId, $positionId), function ($model) {
            return static::generateEventKey($model['status_id'], $model['group']);
        });
        $defaultEvents = static::getDefaultClientStatusEvents($positionId);

        $transaction = app()->db->beginTransaction();
        try {
            foreach ($defaultEvents as $event) {
                if (!in_array(static::generateEventKey($event['status_id'], $event['group']), $eventKeys, false)) {
                    app()->db->createCommand()->insert(
                        static::tableName(),
                        [
                            'tenant_id'   => $tenantId,
                            'city_id'     => $cityId,
                            'position_id' => $positionId,
                            'status_id'   => $event['status_id'],
                            'group'       => $event['group'],
                            'notice'      => $event['notice'],
                        ]
                    )->execute();
                }
            }
            $transaction->commit();
        } catch (\Exception $ex) {
            $transaction->rollBack();
            \Yii::error($ex, 'client-status-event');
            throw $ex;
        }
    }

    /**
     * @param int   $tenantId
     * @param int   $cityId
     * @param int   $positionId
     * @param array $events
     *
     * @throws \Exception
     */
    public static function batchInsert($tenantId, $cityId, $positionId, $events)
    {
        $transaction = app()->db->beginTransaction();
        try {
            app()->db->createCommand()->batchInsert(
                static::tableName(),
                ['tenant_id', 'city_id', 'position_id', 'status_id', 'group', 'notice'],
                array_map(function ($event) use ($tenantId, $cityId, $positionId) {
                    return [
                        $tenantId,
                        $cityId,
                        $positionId,
                        $event['status_id'],
                        $event['group'],
                        $event['notice'],
                    ];
                }, $events))->execute();
            $transaction->commit();
        } catch (\Exception $ex) {
            $transaction->rollBack();
            \Yii::error($ex, 'client-status-event');
            throw $ex;
        }
    }

    public static function batchUpdate($events)
    {
        $transaction = app()->db->beginTransaction();
        try {
            foreach ($events as $eventId => $notice) {
                app()->db->createCommand()
                    ->update(static::tableName(), [
                        'notice' => $notice,
                    ], [
                        'event_id' => $eventId,
                    ])->execute();
            }
            $transaction->commit();
        } catch (\Exception $ex) {
            $transaction->rollBack();
            \Yii::error($ex, 'client-status-event');
            throw $ex;
        }
    }


    /**
     * Обновление настроек одним запросом.
     *
     * @param array $arEvents [$event_id => $notice]
     *
     * @return boolean
     */
    public static function updateEvents($arEvents)
    {
        if (is_array($arEvents) && !empty($arEvents)) {
            $case   = '';
            $events = '';
            foreach ($arEvents as $event_id => $notice) {
                $case .= "WHEN `event_id` = $event_id THEN '$notice' ";

                if (!empty($events)) {
                    $events .= ', ';
                }

                $events .= $event_id;
            }

            //Аналогично этому запросу UPDATE `tbl_client_status_event` SET `notice` = CASE WHEN `event_id` = 139 THEN 'autocall' WHEN `event_id` = 140 THEN 'autocall' END WHERE `event_id` IN (139, 140)
            return app()->db->createCommand('UPDATE ' . self::tableName() . ' SET `notice` = CASE ' . $case . 'END WHERE `event_id` IN (' . $events . ')')->execute();
        }

        return false;
    }


}
