<?php

namespace frontend\modules\tenant\models;

use app\modules\balance\models\Account;
use app\modules\balance\models\Operation;
use app\modules\balance\models\Transaction;
use yii\base\Model;

/**
 * Class PaymentHistory
 * @package frontend\modules\tenant\models
 */
class PaymentHistory extends Model
{
    /**
     * Getting payment transactions
     *
     * @param integer $dateFrom
     * @param integer $dateTo
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getPaymentTransactions($dateFrom, $dateTo)
    {
        return Operation::find()
            ->alias('o')
            ->joinWith(['account a', 'account.currency c', 'transaction t'], true)
            ->where(['a.acc_type_id' => Account::PASSIVE_TYPE])
            ->andWhere(['acc_kind_id' => Account::TENANT_KIND])
            ->andWhere(['tenant_id' => user()->tenant_id])
            ->andWhere([
                'or',
                ['t.type_id' => Transaction::GOOTAX_PAYMENT_TYPE],
                ['!=', 't.payment_method', Transaction::TERMINAL_ELECSNET_PAYMENT],
            ])
            ->andFilterWhere(['>=', 'o.date', date('Y-m-d H:i:s', $dateFrom)])
            ->andFilterWhere(['<=', 'o.date', date('Y-m-d H:i:s', $dateTo)])
            ->orderBy(['o.date' => SORT_DESC])
            ->all();
    }

}