<?php

namespace frontend\modules\tenant\models;

use common\modules\tenant\models\TenantHasCity;

class TenantHasCitySearch extends TenantHasCity
{

    /**
     * @inheritdoc
     */
    public function rules() {
        return [];
    }

    public function search($block = false)
    {
        $query = self::find()
            ->where([
                'tenant_id' => user()->tenant_id,
                'block' => (int) $block
            ])
            ->with('city')
            ->orderBy(['sort' => SORT_ASC]);
        return $query;
    }
}