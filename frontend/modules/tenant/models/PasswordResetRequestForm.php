<?php

namespace app\modules\tenant\models;

use common\components\helpers\UserHelper;
use common\components\serviceApi\EmailTypes;
use common\components\serviceApi\ServiceApi;
use Yii;
use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $tenantId;
    public $cityId;
    public $email;
    public $lang;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required', 'message' => t('app', '{attribute} cannot be blank', [], $this->lang)],
            ['email', 'email', 'message' => t('app', 'Invalid email format', [], $this->lang)],
            [
                'email',
                'exist',
                'targetClass' => 'app\modules\tenant\models\User',
                'filter'      => ['active' => 1],
                'message'     => t('validator', 'There is no user with such email.', [], $this->lang),
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     * @throws \yii\base\InvalidConfigException
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findIdentityByEmailAndTenantId($this->email, User::getTenantIdByNameHost());

        if ($user) {
            if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
                $user->save(false, ['password_reset_token']);
            }

            /** @var ServiceApi $serviceApi */
            $serviceApi = app()->get('serviceApi');

            $result = $serviceApi->sendEmail($this->tenantId, $this->cityId, EmailTypes::PASSWORD_RESET_TOKEN,
                $user->lang,
                $this->email, [
                    'user_name' => UserHelper::getNameUser($user),
                    'url'       => Yii::$app->urlManager->createAbsoluteUrl([
                        '/tenant/user/reset-password',
                        'token' => $user->password_reset_token,
                    ]),
                ]);

            return $result;
        }

        return false;
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Email',
        ];
    }
}
