<?php


namespace app\modules\tenant\models;


use yii\base\Model;
use yii\web\NotFoundHttpException;

class UserFormBase extends Model
{
    public $user_id;
    protected $_user;

    /**
     * @return User
     * @throws NotFoundHttpException
     */
    public function getUser()
    {
        if (empty($this->_user)) {
            $this->_user = User::findOne(['tenant_id' => user()->tenant_id, 'user_id' => $this->user_id]);

            if (is_null($this->_user)) {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }

        return $this->_user;
    }
}