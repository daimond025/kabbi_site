<?php
namespace app\modules\tenant\models;

use common\modules\tenant\models\Tenant;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $email;
    public $password;
    public $rememberMe = false;
    public $lang;

    private $_user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['lang','safe'],
            // email and password are both required
            [['password', 'email'], 'trim'],
            ['email', 'required', 'message'=>t('app', 'Email cannot be blank', [], $this->lang)],
            [['password'], 'required', 'message' => t('app', '{attribute} cannot be blank', [], $this->lang)],
            ['email', 'email', 'message'=>t('app','Invalid email format', [], $this->lang)],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, t('validator', 'Incorrect email or password.', [], $this->lang));
            }
        }
    }

    /**
     * Logs in a user using the provided email and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? app()->params['rememberMe'] : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUser()
    {

        if ($this->_user === false)
            $this->_user = User::findIdentityByEmailAndTenantId($this->email, User::getTenantIdByNameHost());

        return $this->_user;
    }

    public function attributeLabels()
    {
        return [
            'email' => t('app', 'E-mail', [], $this->lang),
            'password' => t('app', 'Password', [], $this->lang),
            'rememberMe' => t('app', 'Remember Me', [], $this->lang),
            'domain' => t('app', 'Domain', [], $this->lang)
        ];
    }
}
