<?php

namespace frontend\modules\tenant\models;

use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Exception\ClientException;
use yii\web\HttpException;
use yii\web\Response;

/**
 * This is the model class for table "tbl_call".
 *
 * @property integer $id
 * @property string $uniqueid
 * @property integer $tenant_id
 * @property integer $order_id
 * @property integer $driver_id
 * @property integer $user_opertor_id
 * @property string $type
 * @property string $source
 * @property string $destination
 * @property integer $phone_line_id
 * @property string $channel
 * @property string $destinationchannel
 * @property string $destinationcontext
 * @property string $lastapplication
 * @property string $starttime
 * @property string $answertime
 * @property string $endtime
 * @property integer $duration
 * @property integer $billableseconds
 * @property string $disposition
 * @property string $amaflags
 * @property string $userfield
 * @property integer $create_time
 */
class Call extends \yii\db\ActiveRecord
{
    
    
    const TYPE_OUTCOME = 'outcome'; // исходящий звонок
    
    const TYPE_INCOME = 'income'; // входящий звонок
    
    const ANSWERED = 'ANSWERED'; // Отвечен
    
    const NO_ANSWERED = 'NO ANSWER'; // Не отвечен
    
    const STATUS_RECEIVED = 1; // принят
    
    const STATUS_BUSY = 2; // занято
    

    
    
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_call';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uniqueid', 'tenant_id', 'type', 'source', 'destination', 'channel', 'destinationchannel', 'duration', 'billableseconds', 'disposition', 'create_time'], 'required'],
            [['tenant_id', 'order_id', 'driver_id', 'user_opertor_id', 'phone_line_id', 'duration', 'billableseconds', 'create_time'], 'integer'],
            [['type'], 'string'],
            [['starttime', 'answertime', 'endtime'], 'safe'],
            [['uniqueid'], 'string', 'max' => 60],
            [['source', 'destination', 'lastapplication'], 'string', 'max' => 20],
            [['channel', 'destinationchannel', 'destinationcontext', 'disposition', 'amaflags'], 'string', 'max' => 50],
            [['userfield'], 'string', 'max' => 30],
            [['driver_id'], 'exist', 'skipOnError' => true, 'targetClass' => Driver::className(), 'targetAttribute' => ['driver_id' => 'driver_id']],
            [['phone_line_id'], 'exist', 'skipOnError' => true, 'targetClass' => PhoneLine::className(), 'targetAttribute' => ['phone_line_id' => 'line_id']],
            [['tenant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tenant::className(), 'targetAttribute' => ['tenant_id' => 'tenant_id']],
            [['user_opertor_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_opertor_id' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uniqueid' => 'Uniqueid',
            'tenant_id' => 'Tenant ID',
            'order_id' => 'Order ID',
            'driver_id' => 'Driver ID',
            'user_opertor_id' => 'User Opertor ID',
            'type' => 'Type',
            'source' => 'Source',
            'destination' => 'Destination',
            'phone_line_id' => 'Phone Line ID',
            'channel' => 'Channel',
            'destinationchannel' => 'Destinationchannel',
            'destinationcontext' => 'Destinationcontext',
            'lastapplication' => 'Lastapplication',
            'starttime' => 'Starttime',
            'answertime' => 'Answertime',
            'endtime' => 'Endtime',
            'duration' => 'Duration',
            'billableseconds' => 'Billableseconds',
            'disposition' => 'Disposition',
            'amaflags' => 'Amaflags',
            'userfield' => 'Userfield',
            'create_time' => 'Create Time',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['user_operator_is' => 'user_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }
    
    
    // Получение статуса звонка
//    public function getClient()
//    {
//        if($this->type )
//        $client = ClientPhone::find()
//                ->where('value=:phone',['phone' => user()->tenant_id])
//                ->andWhere
//    }
    
    public function getStatus()
    {
        if($this->disposition == self::ANSWERED)
            return self::STATUS_RECEIVED;
    }

    /**
     * Сгенерировать запись звонка
     * @return null|\Psr\Http\Message\StreamInterface
     */
    public function exportRecord()
    {
        app()->response->format = Response::FORMAT_RAW;

        $response = $this->exportMp3File();
        if (!$response) {
            $response = $this->exportWavFile();
        }
        if (!$response) {
            $response = $this->exportOldWavFile();
        }

        return $response;
    }

    /**
     * Получить файл для старых записей
     *
     * @return string
     */
    protected function getOldWavFile()
    {
        return $this->getOldFilePath() . '/' . $this->uniqueid . '.wav';
    }

    /**
     * Получить путь до wav файла
     *
     * @return string
     */
    protected function getWavFile()
    {
        return $this->getFilePath() . '/' . $this->uniqueid . '.wav';
    }

    /**
     * Получить путь до mp3 файла
     *
     * @return string
     */
    protected function getMp3File()
    {
        return $this->getFilePath() . '/' . $this->uniqueid . '.mp3';
    }

    /**
     * Получить путь, в котором хранятся файлы
     *
     * @return string
     */
    protected function getFilePath()
    {
        return app()->params['uploadCallRecord'] . '/' . date('Y/m/d', $this->create_time);
    }

    /**
     * Получить путь, в котором хранятся старые файлы
     *
     * @return mixed
     */
    protected function getOldFilePath()
    {
        return app()->params['uploadCallRecord'];
    }

    /**
     * Сгенерировать wav для старых файлов
     *
     * @return null|\Psr\Http\Message\StreamInterface
     */
    protected function exportOldWavFile()
    {
        $file = $this->getOldWavFile();

        $response = $this->exportFile($file);

        if ($response) {
            app()->response->getHeaders()->set('Accept-Ranges', 'bytes');
            app()->response->getHeaders()->set('Content-Type', 'audio/vnd.wave');
            app()->response->getHeaders()->set('Content-Disposition', 'attachment;filename="' . $this->id . '.wav"');

            $headers = $response->getHeaders();
            if (isset($headers['Content-Length'])) {
                app()->response->getHeaders()->set('Content-Length', reset($headers['Content-Length']));
            }
            if (isset($headers['Connection'])) {
                app()->response->getHeaders()->set('Connection', reset($headers['Connection']));
            }

            return $response->getBody();
        }

        return null;
    }

    /**
     * Сгенерировать wav файл
     *
     * @return null|\Psr\Http\Message\StreamInterface
     */
    protected function exportWavFile()
    {
        $file = $this->getWavFile();

        $response = $this->exportFile($file);

        if ($response) {
            app()->response->getHeaders()->set('Accept-Ranges', 'bytes');
            app()->response->getHeaders()->set('Content-Type', 'audio/vnd.wave');
            app()->response->getHeaders()->set('Content-Disposition', 'attachment;filename="' . $this->id . '.wav"');

            $headers = $response->getHeaders();
            if (isset($headers['Content-Length'])) {
                app()->response->getHeaders()->set('Content-Length', reset($headers['Content-Length']));
            }
            if (isset($headers['Connection'])) {
                app()->response->getHeaders()->set('Connection', reset($headers['Connection']));
            }

            return $response->getBody();
        }

        return null;
    }

    /**
     * Сгенерировать mp3 файл
     *
     * @return null|\Psr\Http\Message\StreamInterface
     */
    protected function exportMp3File()
    {

        // Если запись была создана сегодня, то не ищем в mp3
        $currentDate = date('d.m.Y', time());
        $fileDate = date('d.m.Y', (int)$this->create_time);
        if ($fileDate === $currentDate) {
            return null;
        }

        $file = $this->getMp3File();

        $response = $this->exportFile($file);

        if ($response) {
            app()->response->getHeaders()->set('Accept-Ranges', 'bytes');
            app()->response->getHeaders()->set('Content-Type', 'audio/mpeg');
            app()->response->getHeaders()->set('Content-Disposition', 'attachment;filename="' . $this->id . '.mp3"');

            $headers = $response->getHeaders();
            if (isset($headers['Content-Length'])) {
                app()->response->getHeaders()->set('Content-Length', reset($headers['Content-Length']));
            }
            if (isset($headers['Connection'])) {
                app()->response->getHeaders()->set('Connection', reset($headers['Connection']));
            }

            return $response->getBody();
        }

        return null;
    }

    /**
     * Сгенерировать файл
     *
     * @param $file
     *
     * @return mixed|null|\Psr\Http\Message\ResponseInterface
     */
    protected function exportFile($file)
    {
        try {
            $client = new GuzzleHttpClient();
            $res = $client->request('GET', $file);

        } catch (ClientException $ex) {
            return null;
        }

        return $res;
    }
}
