<?php

namespace app\modules\tenant\models;

use Yii;
use common\modules\tenant\models\Tenant;

/**
 * This is the model class for table "{{%autocall_file}}".
 *
 * @property integer $file_id
 * @property integer $tenant_id
 * @property string $key
 * @property string $name
 * @property string $group
 *
 * @property \common\modules\tenant\models\Tenant $tenant
 */
class AutocallFile extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 1;
    const STATUS_OFFER_ORDER = 2;
    const STATUS_DRIVER_REFUSED = 3;
    const STATUS_FREE = 4;
    const STATUS_NOPARKING = 5;
    const STATUS_GET_DRIVER = 17;
    const STATUS_CLIENT_NOTIFIED = 19;
    const STATUS_DRIVER_WAITING = 26;
    const STATUS_EXECUTING = 36;
    const STATUS_OVERDUE = 52;
    const STATUS_DRIVER_LATE = 54;
    const STATUS_EXECUTION_PRE = 55;
    
    public $tenant_id;
    public $order_id;
    
    private $_order;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%autocall_file}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id'], 'integer'],
            [['key', 'name', 'group'], 'required'],
            [['group'], 'string'],
            [['key', 'name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'file_id' => 'File ID',
            'tenant_id' => 'Tenant ID',
            'key' => 'Key',
            'name' => 'Name',
            'group' => 'Group',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }
    
    /**
     * Возвращает список файлов в нужном порядке.
     * @param integer $tenant_id
     * @param integer $order_id
     * @return boolean|array 
     * [
     *   0 => [
     *     'key' => 'taxi' Ключ соответствия с именем файла
     *     'name' => '76.wav' Имя файла
     *     'group' => 'phrases' Групповая принадлежность
     *   ]
     * ]
     */
    public function getFiles()
    {
        $order = \Yii::$app->redis_orders_active->executeCommand('hget', [$this->tenant_id, $this->order_id]);
        
        if(empty($order))
            return false;
        
        $this->_order = unserialize($order);
        
        //Массив статусов поиска авто
        $arLookingCarStatus = [
            self::STATUS_NEW,
            self::STATUS_OFFER_ORDER,
            self::STATUS_DRIVER_REFUSED,
            self::STATUS_FREE,
            self::STATUS_NOPARKING
        ];
        
        //Массив статусов принял заказ, приехал, либо опаздывает
        $arDriverStatus = [
            self::STATUS_GET_DRIVER,
            self::STATUS_DRIVER_WAITING,
            self::STATUS_DRIVER_LATE,
            self::STATUS_EXECUTION_PRE
        ];
        
        //Повторный звонок клиента (когда ещё идет поиск авто)
        if(in_array($this->_order['status_id'], $arLookingCarStatus))
        {
            $autoCallFiles = self::getFromDb(['taxi', 'looking']);
            
            //Порядок файлов
            $arFiles = [
                $autoCallFiles['taxi'],
                $autoCallFiles['looking']
            ];
        }
        //Принял заказ, приехал, либо опаздывает
        elseif(in_array($this->_order['status_id'], $arDriverStatus))
        {
            $car = $this->_order['car'];
            $brand = current(explode(' ', $car['name']));
            
            //Разбираем гос. номер
            $arNumber = [];
            if(preg_match('/[0-9]{3,}/', $this->_order['car']['gos_number'], $matches))
            {
                $length = strlen($matches[0]);
                for($i = 0; $i < $length; $i++)
                {
                    $arNumber[] = $matches[0][$i];
                }
            }
            
            //Формирование ключей в порядке воспроизведения
            $arKeys = [
                'taxi', 
                $this->_order['status_id'] != self::STATUS_DRIVER_WAITING ? 'drives' : 'waiting', 
                $brand,
                'color',
                $car['color'],
                'number',
            ];
            
            //Добавляем гос. номер
            $arKeys = array_merge($arKeys, $arNumber);
            
            //Добавление ключей в зависимости от статуса
            if(($this->_order['status_id'] == self::STATUS_GET_DRIVER ||
                $this->_order['status_id'] == self::STATUS_EXECUTION_PRE) && 
                !$this->isNotified())
            {
                $arKeys[] = 'in';
                $arKeys[] = $this->_order['time_to_client'];
                $arKeys[] = $this->declension_words($this->_order['time_to_client'], ['minute', 'minuty', 'minutes']);
                $arKeys[] = 'godspeed';
            }
            elseif($this->_order['status_id'] == self::STATUS_DRIVER_WAITING)
            {
                $arKeys[] = 'leave';
            }
            else
            {
                $arKeys[] = 'wait';
            }
            
            $autoCallFiles = self::getFromDb($arKeys);

            //Формирование результирующего массива, сортируя данные из БД
            foreach ($arKeys as $key)
            {
                $arFiles[] = $autoCallFiles[$key];
            }
        }
        //Повторный звонок клиента во время выполнения заказа
        elseif($this->_order['status_id'] == self::STATUS_EXECUTING)
        {
            $autoCallFiles = self::getFromDb(['taxi', 'execution']);

            //Порядок файлов
            $arFiles = [
                $autoCallFiles['taxi'],
                $autoCallFiles['execution']
            ];
        }
        //нет машин (для просроченного заказа)
        elseif($this->_order['status_id'] == self::STATUS_OVERDUE)
        {
            $autoCallFiles = self::getFromDb(['taxi', 'sorry']);
            
            //Порядок файлов
            $arFiles = [
                $autoCallFiles['taxi'],
                $autoCallFiles['sorry']
            ];
        }
        else
        {
            $arFiles = [];
        }
        
        return $arFiles;
    }
    
    /**
     * Получаем значеия из БД по полю key.
     * @param array|string $keys
     * @return array
     */
    public static function getFromDb($keys)
    {
        return self::find()->
                    where(['key' => $keys])->
                    asArray()->
                    indexBy('key')->
                    select(['key', 'name', 'group'])->
                    all();
    }
    
    /**
     * Уведомлен ли пользователь о подъзде авто.
     * @return bool
     */
    private function isNotified()
    {
        return (new \yii\db\Query())
            ->from('tbl_order_change_data')
            ->where([
                'order_id' => $this->order_id,
                'change_field' => 'status_id',
                'change_val' => self::STATUS_CLIENT_NOTIFIED
            ])
            ->exists();
    }
    
    /**
    * Склонение слов
    * @param integer $num Число
    * @param array $arWords Склонения
    * @return string Слово
    */
    private function declension_words($num, $arWords)
    {
        $number = $num < 21 ? $num : (int)substr($num, -1);

        if ($number == 1)
            $w = $arWords[0];
        elseif ($number > 1 && $number < 5)
            $w = $arWords[1];
        else
            $w = $arWords[2];

        return $w;
    }
}
