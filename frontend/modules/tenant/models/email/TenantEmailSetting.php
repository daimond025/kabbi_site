<?php

namespace frontend\modules\tenant\models\email;

use yii\db\ActiveRecord;

/**
 * Class TenantEmailSetting
 * @package frontend\modules\tenant\models\email
 *
 * @property integer $setting_id
 * @property integer $tenant_id
 * @property integer $city_id
 * @property integer $active
 *
 * @property string  $provider_server
 * @property integer $provider_port
 *
 * @property string  $sender_name
 * @property string  $sender_email
 * @property string  $sender_password
 *
 * @property string  $template
 *
 * @property boolean $isActive
 *
 */
class TenantEmailSetting extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_email_settings}}';
    }

    public function getIsActive()
    {
        return (boolean)$this->active;
    }

    public function setIsActive($value)
    {
        $this->active = $value ? 1 : 0;
    }

    public function activate()
    {
        $this->isActive = true;
    }

    public function deactivate()
    {
        $this->isActive = false;
    }
}