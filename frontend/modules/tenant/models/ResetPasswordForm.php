<?php
namespace app\modules\tenant\models;

use app\modules\tenant\models\User;
use yii\base\InvalidParamException;
use yii\base\Model;
use Yii;

/**
 * Password reset form
 */
class ResetPasswordForm extends Model
{
    public $password;
    public $password_repeat;
    public $lang;
    /**
     * @var app\modules\tenant\models\User
     */
    private $_user;

    /**
     * Creates a form model given a token.
     *
     * @param  string                          $token
     * @param  array                           $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Password reset token cannot be blank.');
        }
        $this->_user = User::findByPasswordResetToken($token);
        if (!$this->_user) {
            throw new InvalidParamException('Wrong password reset token.');
        }
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password', 'password_repeat'], 'required', 'message'=>t('app', '{attribute} cannot be blank', [], $this->lang)],
            ['password', 'string', 'min' => 6, 'tooShort'=>t('employee', 'at least 6 characters',[],$this->lang)],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => t('user', 'Does not match',[],$this->lang)],
        ];
    }

    /**
     * Resets password.
     *
     * @return boolean if password was reset.
     */
    public function resetPassword()
    {
        $user = $this->_user;
        Yii::$app->user->login($user);
        $user->setPassword($this->password);
        $user->removePasswordResetToken();

        return $user->save(false);
    }

    public function attributeLabels()
    {
        return [
            'password' => t('app', 'New password',[],$this->lang),
            'password_repeat' => t('app', 'Password confirm',[],$this->lang),
        ];
    }
}
