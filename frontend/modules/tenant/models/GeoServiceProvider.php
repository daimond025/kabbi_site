<?php

namespace frontend\modules\tenant\models;

use common\modules\tenant\models\Tenant;
use Yii;
use yii\data\ActiveDataProvider;

use yii\mongodb\ActiveRecord;

class GeoServiceProvider extends ActiveRecord   //\yii\elasticsearch\ActiveRecord ActiveRecord
{
    const AUTO_GEO_SERVICE_TYPE = 'auto_geo_service';
    const ROUTING_SERVICE_TYPE = 'routing_service';
    const DEFAULT_GEO_SERVICE_PROVIDER = 1; //Yandex free
    const DEFAULT_ROUTING_SERVICE_PROVIDER = 1; //OSM free

    public static function primaryKey()
    {
        return ['row_id'];
    }

    //public $index = 'service_provider';

  /*  public static function index()
    {
        return 'service_provider';
    }

    public static function type()
    {
        return 'row';
    }*/

   // todo для монго
    public static function collectionName()
    {
        return 'service_geo';
    }

    /**
     * @return array the list of attributes for this record
     */
    public function attributes()
    {
        return [
            '_id',  // todo для монго
            'row_id',
            'tenant_id',
            'tenant_domain',
            'city_id',
            'service_type',
            'service_provider_id',
            'key_1',
            'key_2',
            'default',
        ];
    }

    public function rules()
    {
        return [
            [
                [
                    'row_id',
                    'tenant_id',
                    'tenant_domain',
                    'city_id',
                    'service_type',
                    'service_provider_id',
                ],
                'required',
            ],
            [['key_1', 'key_2'], 'string', 'max' => 50],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GeoServiceProvider::find();
        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 30,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }

    /**
     * Create geo-service provider
     * @param type $tenantId
     * @param type $tenantDomain
     * @param type $cityId
     * @param type $serviceType
     * @param type $serviceProviderId
     * @param type $key1
     * @param type $key2
     * @return bool
     */
    public static function create($tenantId, $tenantDomain, $cityId, $serviceType, $serviceProviderId, $key1, $key2)
    {
        $row = new GeoServiceProvider([
            'row_id'              => $tenantId . '_' . $cityId . '_' . $serviceType,
            'tenant_id'           => $tenantId,
            'tenant_domain'       => $tenantDomain,
            'city_id'             => $cityId,
            'service_type'        => $serviceType,
            'service_provider_id' => $serviceProviderId,
            'key_1'               => $key1,
            'key_2'               => $key2,
            'default'               => 0,
        ]);

        return $row->save();
    }

    /**
     * Get one geo-service provider
     *
     * @param integer $tenantId
     * @param string $tenantDomain
     * @param integer $cityId
     * @param string $serviceType
     *
     * @return \frontend\modules\tenant\models\GeoServiceProvider
     */
    public static function findOneProvider($tenantId, $tenantDomain, $cityId, $serviceType)
    {
        $provider = GeoServiceProvider::find()
            ->where([
                'row_id' => $tenantId . "_" . $cityId . '_' . $serviceType,
            ])
            ->one();
        if (!$provider) {
            $provider = new GeoServiceProvider();
            $provider->row_id = $tenantId . "_" . $cityId . '_' . $serviceType;
            $provider->service_type = $serviceType;
            $provider->tenant_id = $tenantId;
            $provider->tenant_domain = $tenantDomain;
            $provider->city_id = $cityId;
            $provider->default = 0;
            if ($serviceType == self::AUTO_GEO_SERVICE_TYPE) {
                $provider->service_provider_id = self::DEFAULT_GEO_SERVICE_PROVIDER;
            } elseif ($serviceType == self::ROUTING_SERVICE_TYPE) {
                $provider->service_provider_id = self::DEFAULT_ROUTING_SERVICE_PROVIDER;
            }
        }

        return $provider;
    }

    /**
     * Get all geo-providers of tenant in city
     * @param type $tenantId
     * @param type $tenantDomain
     * @param type $cityId
     * @return array
     */
    public static function getProviders($tenantId, $tenantDomain, $cityId)
    {
        $autoGeoProvider = self::findOneProvider($tenantId, $tenantDomain, $cityId, self::AUTO_GEO_SERVICE_TYPE);
        $routingProvider = self::findOneProvider($tenantId, $tenantDomain, $cityId, self::ROUTING_SERVICE_TYPE);

        return [$autoGeoProvider, $routingProvider];
    }

    /**
     * create default geo-provider (using when tenant create city)
     * @param int $tenantId
     * @param string $tenantDomain
     * @param int $cityId
     * @return boolean
     */
    public static function createDefaultGeoProviders($tenantId, $tenantDomain, $cityId)
    {
        $autoGeoProvider = self::findOneProvider($tenantId, $tenantDomain, $cityId, self::AUTO_GEO_SERVICE_TYPE);
        $routingProvider = self::findOneProvider($tenantId, $tenantDomain, $cityId, self::ROUTING_SERVICE_TYPE);
        if ($autoGeoProvider->save()) {
            if ($routingProvider->save()) {
                return true;
            }
            Yii::error($routingProvider->errors);
            $autoGeoProvider->delete();

            return false;
        }
        Yii::error($autoGeoProvider->errors);

        return false;
    }

    /**
     * Create all geo-providers to tenant in city with custom types
     * @param type $tenantId
     * @param type $tenantDomain
     * @param type $cityId
     * @param type $autoGeoServiceType
     * @param type $geocodeServiceType
     * @return boolean
     */
    public static function createProvidersWithCustomType(
        $tenantId,
        $tenantDomain,
        $cityId,
        $autoGeoServiceType,
        $geocodeServiceType
    )
    {
        $autoGeoProvider = self::findOneProvider($tenantId, $tenantDomain, $cityId, self::AUTO_GEO_SERVICE_TYPE);
        $autoGeoProvider->service_provider_id = $autoGeoServiceType;
        $routingProvider = self::findOneProvider($tenantId, $tenantDomain, $cityId, self::ROUTING_SERVICE_TYPE);
        $routingProvider->service_provider_id = $geocodeServiceType;
        if ($autoGeoProvider->save()) {
            if ($routingProvider->save()) {
                return true;
            }
            Yii::error($routingProvider->errors);
            $autoGeoProvider->delete();

            return false;
        }
        Yii::error($autoGeoProvider->errors);

        return false;
    }

    public function beforeSave($insert)
    {

        if (parent::beforeSave($insert)) {
            $this->attributes = [
                'row_id'        => implode('_', [$this->tenant_id, $this->city_id, $this->service_type]),
                'tenant_domain' => Tenant::getDomainById($this->tenant_id),
            ];
            return true;
        }

        return false;
    }
}