<?php


namespace app\modules\tenant\models;


use frontend\modules\tenant\models\UserDispetcher;

class UserLoginDataForm extends UserFormBase
{
    public $password;
    public $password_repeat;
    public $email;
    public $tenant_id;

    public function init()
    {
        parent::init();
        $this->email = $this->getUser()->email;
    }

    public function rules()
    {
        return [
            [['email'], 'required', 'message' => t('app', 'Email cannot be blank')],
            [['email'], 'string', 'max' => 45],
            [
                ['email'],
                'unique',
                'targetClass'     => User::className(),
                'targetAttribute' => ['email', 'tenant_id'],
                'filter'          => ['not', ['user_id' => $this->user_id]],
                'message'         => t('validator', 'This email is already registered'),
            ],
            [['email'], 'email'],
            ['password', 'string', 'min' => 6],
            [
                'password_repeat',
                'compare',
                'compareAttribute' => 'password',
                'message'          => t('validator', 'Password not compare'),
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => t('user', 'Password'),
            'password_repeat' => t('user', 'Repeat password'),
            'email' => t('app', 'Username'),
        ];
    }

    public function save()
    {
        if ($this->validate()) {
            /** @var $user User */
            $user = $this->getUser();
            if (!empty($this->password)) {
                $user->setPassword($this->password);
                $user->setOperatorHash($this->password);
                $operator_name = UserDispetcher::getOperatorName($user->user_id);
                $user->ha1 = UserDispetcher::generateHa1Hash($operator_name, $this->password);
                $user->ha1b = UserDispetcher::generateHa1bHash($operator_name, $this->password);
                $user->setOperatorPasswordHash($operator_name, $this->password);
            }

            $user->email = $this->email;

            return $user->save(false);
        }

        return false;
    }
}