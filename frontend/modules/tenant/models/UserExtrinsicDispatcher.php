<?php

namespace frontend\modules\tenant\models;


use app\modules\tenant\models\User;


class UserExtrinsicDispatcher extends User
{

    public function save($runValidation = true, $attributeNames = null)
    {
        return true;
    }

    public function insert($runValidation = true, $attributeNames = null)
    {
        return true;
    }

    public function update($runValidation = true, $attributeNames = null)
    {
        return true;
    }

    public function delete()
    {
        return false;
    }

}