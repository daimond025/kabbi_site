<?php

namespace app\modules\tenant\models;

use app\modules\tariff\models\TaxiTariff;
use app\modules\tariff\models\TaxiTariffHasCity;
use common\modules\tenant\models\TenantHasCity;
use Yii;
use common\modules\tenant\models\Tenant;
use common\modules\city\models\City;

/**
 * This is the model class for table "{{%phone_line}}".
 *
 * @property integer                          $line_id
 * @property integer                          $tenant_id
 * @property string                           $phone
 * @property integer                          $tariff_id
 * @property integer                          $city_id
 * @property integer                          $incoming_scenario
 * @property integer                          $block
 * @property integer                          $draft
 *
 * @property \common\modules\city\models\City $city
 * @property TaxiTariff                       $tariff
 * @property Tenant                           $tenant
 *
 * @property boolean                          $isBlock
 * @property boolean                          $isDraft
 */
class PhoneLine extends \yii\db\ActiveRecord
{

    const DRAFT = 1;
    const NOT_DRAFT = 0;

    const BLOCK = 1;
    const NOT_BLOCK = 0;

    const SCENARIO_UPDATE = 'update';
    const SCENARIO_CREATE = 'create';

    const TYPE_CREATE = 'create';
    const TYPE_UPDATE = 'update';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%phone_line}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //            [['phone', 'tariff_id', 'city_id'], 'required'],
            //            [['phone'], 'filter', 'filter' => function($value){ return preg_replace("/[^0-9]/", '', $value);}],
            //            [['phone'], 'unique'],
            //            [['tenant_id', 'tariff_id', 'city_id'], 'integer'],
            //            [['phone'], 'string', 'max' => 20],
            //            [['block'], 'safe'],

            ///////////////////////////////////////////
            ///////////////////////////////////////////
            ///////////////////////////////////////////

            [['phone', 'tariff_id'], 'required'],
            [
                ['phone'],
                'filter',
                'filter' => function ($value) {
                    return preg_replace("/[^0-9]/", '', $value);
                },
            ],
            ['phone', 'unique'],
            ['phone', 'string', 'max' => 20],

            ['city_id', 'required', 'on' => self::SCENARIO_CREATE],
            [
                'city_id',
                'exist',
                'targetClass'     => TenantHasCity::className(),
                'targetAttribute' => ['city_id', 'tenant_id'],
                'on'              => self::SCENARIO_CREATE,
            ],

            [
                'tariff_id',
                'exist',
                'targetClass'     => TaxiTariff::className(),
                'targetAttribute' => ['tariff_id', 'tenant_id'],
            ],
            [
                'tariff_id',
                'exist',
                'targetClass'     => TaxiTariffHasCity::className(),
                'targetAttribute' => ['tariff_id', 'city_id'],
            ],

            ['block', 'in', 'range' => self::getBlockTypeList()],

        ];
    }

    public function subRules()
    {
        $rules = [
            [['phone', 'tariff_id', 'city_id'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process'],
        ];

        return array_merge(parent::rules(), $rules);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'line_id'   => 'Line ID',
            'tenant_id' => 'Tenant ID',
            'phone'     => t('phone_line', 'Phone'),
            'tariff_id' => t('phone_line', 'Tariff'),
            'city_id'   => t('app', 'City'),
        ];
    }


    public function getIsDraft()
    {
        return (int)$this->draft === self::DRAFT;
    }

    public function setIsDraft($value)
    {
        $value       = (bool)$value;
        $this->draft = (int)$value;
    }


    public function getIsBlock()
    {
        return (int)$this->block === self::BLOCK;
    }

    public function setIsBlock($value)
    {
        $value       = (bool)$value;
        $this->block = (int)$value;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(\common\modules\city\models\City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(TaxiTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(\common\modules\tenant\models\Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    public function behaviors()
    {
        return [
            'common' => [
                'class' => \common\components\behaviors\ActiveRecordBehavior::className(),
            ],
        ];
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->deleteInRedis();

            return true;
        }

        return false;
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->incoming_scenario = (new \yii\db\Query())
                ->select(['value'])
                ->from('{{%default_settings}}')
                ->where(['name' => 'PHONE_LINE_SCENARIO'])
                ->scalar();
        } else {
            $diff           = array_diff($this->attributes, $this->oldAttributes);
            $diffAttributes = array_keys($diff);

            if (array_intersect($diffAttributes,
                ['tenant_id', 'tariff_id', 'city_id', 'phone', 'incoming_scenario', 'block'])) {
                $this->deleteInRedis();
            }
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if (!$insert) {
            $phone = isset($changedAttributes['phone']) ? $changedAttributes['phone'] : $this->phone;
            Yii::$app->redis_phone_scenario->executeCommand('del', [$phone]);
        }
        parent::afterSave($insert, $changedAttributes);
    }

    public function deleteInRedis()
    {
        \Yii::$app->get('redis_phone_scenario')->executeCommand('del', [$this->oldAttributes['phone']]);
    }

    public static function getBlockTypeList()
    {
        return [self::BLOCK, self::NOT_BLOCK];
    }

    public function getParamsByVoipApi($type)
    {
        switch ($type) {
            case self::TYPE_CREATE:
                return [
                    'r_username'     => $this->r_username,
                    'auth_password'  => $this->password,
                    'r_domain'       => $this->r_domain,
                    'sipServerLocal' => $this->sip_server_local,
                    'auth_proxy'     => $this->auth_proxy,
                    'port'           => $this->port,
                    'realm'          => $this->realm,
                    'phoneLine'      => $this->phone,
                    'typeServer'     => $this->typeServer,
                    'typeQuery'      => $type,
                    'tenantId'       => $this->tenant_id,
                    'phoneLineId'    => $this->line_id,
                ];
            case self::TYPE_UPDATE:
                $params = [
                    'r_username'     => $this->r_username,
                    'r_domain'       => $this->r_domain,
                    'sipServerLocal' => $this->sip_server_local,
                    'auth_proxy'     => $this->auth_proxy,
                    'port'           => $this->port,
                    'realm'          => $this->realm,
                    'phoneLine'      => $this->phone,
                    'typeServer'     => $this->typeServer,
                    'typeServerOld'     => $this->typeServer,
                    'typeQuery'      => $type,
                    'tenantId'       => $this->tenant_id,
                    'phoneLineId'    => $this->line_id,
                ];

                return $params;
        }
    }
}
