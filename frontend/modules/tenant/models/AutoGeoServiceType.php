<?php

namespace app\modules\tenant\models;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "tbl_auto_geo_service_type".
 *
 * @property integer $id
 * @property string $service_label
 * @property integer $active
 * @property integer $has_key_1
 * @property integer $has_key_2
 * @property string $key_1_label
 * @property string $key_2_label
 */
class AutoGeoServiceType extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_auto_geo_service_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['service_label'], 'string'],
            [['active', 'has_key_1', 'has_key_2'], 'integer'],
            [['key_1_label', 'key_2_label'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'service_label' => 'Service Label',
            'active'        => 'Active',
            'has_key_1'     => 'Has Key 1',
            'has_key_2'     => 'Has Key 2',
            'key_1_label'   => 'Key 1 Label',
            'key_2_label'   => 'Key 2 Label',
        ];
    }

    public static function getActiveArray()
    {
        return self::find()
                ->where([
                    'active' => 1
                ])
                ->asArray()
                ->all();
    }

    public static function getServicesIdsWithKey1()
    {
        $services = self::find()->select(['id'])->where([
                'has_key_1' => 1
            ])
            ->asArray()
            ->all();
        return ArrayHelper::getColumn($services, 'id');
    }

    public static function getServicesIdsWithKey2()
    {
        $services = self::find()->select(['id'])->where([
                'has_key_2' => 1
            ])
            ->asArray()
            ->all();
        return ArrayHelper::getColumn($services, 'id');
    }
}