<?php

namespace frontend\modules\tenant\models\worker;

use yii\base\Model;
use yii\helpers\ArrayHelper;
use common\modules\tenant\models\TenantSetting;

/**
 *
 * @property integer $tenantId
 * @property integer $cityId
 * @property integer $positionId
 * @property integer $allowEditOrder            0,1
 * @property integer $allowEditCostOrder        0,1
 * @property integer $showEstimation            0,1
 * @property integer $allowWorkWithoutGps       0,1
 * @property integer $workerTimeCalculate       0,1
 * @property integer $workerTimeCalculateStep
 * @property integer $workerTimeCalculateStepCount

 * @property integer $workerMinBalance
 * @property integer $showUrgentOrderTime       0,1
 * @property integer $showWorkerAddress         0,1
 * @property integer $showWorkerPhone           0,1
 * @property integer $showWorkerClient          0,1
 * @property integer $showChatDispatcherExit    0,1
 * @property integer $showChatDispatcherShift   0,1
 * @property integer $showGeneralChatShift      0,1
 * @property integer $allowUseBankCard          0,1
 * @property integer $allowReserveUrgentOrders  0,1
 * @property integer $addIntermediatePointInOrderForStop    0,1
 * @property integer $needCloseWorkerShiftByBadPositionUpdateTime 0,1
 * @property integer $workerPositionCheckDelayTime
 * @property integer $needCloseWorkerShiftByBadParkingPosition 0,1
 * @property integer $workerParkingPositionCheckDelayTime
 * @property string  $workerParkingPositionCheckType basePolygon/receptionArea
 * @property array   $workerArrivalTime
 * @property array   $allowWorkerToCreateOrder
 * @property array   $denyFakegps
 * @property array   $denySettingArrivalStatusEarly
 * @property array   $minDistanceToSetArrivalStatus
 * @property integer $controlOwnCarMileage
 * @property integer $maxCountReserveUrgentOrders
 * @property integer $workerReviewCount
 * @property integer $workerMinReviewRating
 * @property integer $workerOrderSound
 *
 */
class SettingsWorker extends Model
{

    const DONT_SHOW_DEFAULT_SETTING = 'Do not show';
    const SHOW_ONLY_NAME_SETTING = 'Show only name';
    const SHOW_NAME_AND_PATRONYMIC_SETTING = 'Show name and patronymic';
    const SHOW_SURNAME_AND_NAME_AND_PATRONYMIC_SETTING = 'Show surname,name and patronymic';
    const TYPE = 'drivers';
    const PARKING_POSITION_CHECK_TYPES = [
        'basePolygon'   => 'Worker out of basic parking zone',
        'reseptionArea' => 'Worker out of reception area',
    ];

    private $cityId;

    private $positionId;

    private $oldParams;

    private $newParams = [];


    public function __construct($cityId, $position_id, $config = [])
    {
        $this->cityId     = $cityId;
        $this->positionId = $position_id;

        parent::__construct($config);
    }

    public function init() // TODO rememmber - first - get value of cache
    {
        parent::init();


        $this->oldParams = ArrayHelper::map(TenantSetting::find()->andWhere([
            'tenant_id'   => $this->getTenantId(),
            'city_id'     => $this->getCityId(),
            'position_id' => $this->getPositionId(),
            'type'        => self::TYPE,
            'name'        => WorkerOptionsName::asArray(),
        ])->all(), 'name', 'value');
    }

    public function rules()
    {
        return [
            [['tenantId', 'cityId', 'positionId'], 'required'],
            [['tenantId', 'cityId', 'positionId'], 'integer'],
            [
                [
                    'showEstimation',
                    'allowEditOrder',
                    'allowEditCostOrder',
                    'allowWorkWithoutGps',
                    'showWorkerAddress',
                    'showWorkerPhone',
                    'showChatDispatcherExit',
                    'showChatDispatcherShift',
                    'showGeneralChatShift',
                    'allowUseBankCard',
                    'needCloseWorkerShiftByBadPositionUpdateTime',
                    'needCloseWorkerShiftByBadParkingPosition',
                    'showUrgentOrderTime',
                    'allowWorkerToCreateOrder',
                    'denyFakegps',
                    'denySettingArrivalStatusEarly',
                    'controlOwnCarMileage',
                    'printCheck',
                    'requirePasswordEveryTimeLogInApplication',
                    'showDriverPrivacyPolicy',
                    'addIntermediatePointInOrderForStop',
                    'allowReserveUrgentOrders',
                    'workerTimeCalculate',
                    'workerOrderSound'
                ],
                'integer',
                'min' => 0,
                'max' => 1,
            ],
            [
                ['workerTimeCalculateStep'],
                'integer',
                'min' => 5,
                'max' => 60,
            ],
            [
                ['workerTimeCalculateStepCount'],
                'integer',
                'min' => 1,
                'max' => 5,
            ],
            [
                [
                    'workerMinBalance',
                    'workerPositionCheckDelayTime',
                    'workerParkingPositionCheckDelayTime',
                    'minDistanceToSetArrivalStatus',
                    'maxCountReserveUrgentOrders',
                ],
                'integer',
            ],
            ['workerReviewCount', 'integer', 'min' => 1],
            ['workerMinReviewRating', 'double', 'min' => 0, 'max' => 5, 'numberPattern' => "/^[0-9]+(\.[0-9])?$/"],
            [['showWorkerClient'], 'string'],
            [['workerParkingPositionCheckType'], 'string'],
            [['workerArrivalTime'], 'match', 'pattern' => '/^(\d+\;){5}\d+$/'],
            [['arrivalTime'], 'safe'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'showEstimation'                              => t('setting', 'Show estimation'),
            'allowEditOrder'                              => t('setting', 'Allow editing adresses of the order'),
            'allowEditCostOrder'                          => t('setting', 'Allow editing cost of the order'),
            'allowWorkWithoutGps'                         => t('setting', 'Allow employee to work without GPS'),
            'workerMinBalance'                            => t('setting', 'Minimum balance to enter into system'),
            'showWorkerAddress'                           => t('tenant', 'Worker is able to see destination'),
            'showWorkerPhone'                             => t('tenant', 'Show client number by default'),
            'showWorkerClient'                            => t('tenant', 'Show client name'),
            'showChatDispatcherExit'                      => t('setting', 'Show dispatcher chat before a shift'),
            'showChatDispatcherShift'                     => t('setting', 'Show dispatcher chat on the shift'),
            'showGeneralChatShift'                        => t('setting', 'Show common chat on the shift'),
            'allowUseBankCard'                            => t('setting', 'Allow to fill worker balance by bank card'),
            'needCloseWorkerShiftByBadPositionUpdateTime' => t('setting',
                'Allow to close the shift if the coordinates of the worker is not actual'),
            'workerPositionCheckDelayTime'                => t('setting',
                'Time after which the coordinates of the worker will not relevant'),
            'needCloseWorkerShiftByBadParkingPosition'    => t('setting',
                'Allow to close the shift if the worker is not in the city or is not in the reception area'),
            'workerParkingPositionCheckDelayTime'         => t('setting',
                'Time after which worker shift will closed if worker is not in the city or is not in the reception area'),
            'workerParkingPositionCheckType'              => t('setting', 'Worker shift will closed if'),
            'arrivalTime'                                 => t('setting', 'Arrival Time'),
            'showUrgentOrderTime'                         => t('setting', 'Show urgent order time to worker'),
            'allowWorkerToCreateOrder'                    => t('setting', 'Allow worker to create order'),
            'denyFakegps'                                 => t('setting', 'Disallow using "fakeGPS"'),
            'denySettingArrivalStatusEarly'               => t('setting',
                'Forbid to worker to click button "Arrived" in advance'),
            'minDistanceToSetArrivalStatus'               => t('setting',
                'The minimum distance to the point A, which is allowed to click button "Arrived"'),
            'controlOwnCarMileage'                        => t('settings', 'Control the mileage of their cars?'),
            'printCheck'                                  => t('setting', 'Print a check after a trip'),
            'requirePasswordEveryTimeLogInApplication'    => t('setting',
                'Require a password every time you log in to the application'),
            'showDriverPrivacyPolicy'                     => t('setting', 'Show privacy policy'),
            'addIntermediatePointInOrderForStop'          => t('setting', 'Add an intermediate point to the order when you click the Stop button'),
            'allowReserveUrgentOrders'                    => t('setting', 'Allow the performer to book urgent orders'),
            'maxCountReserveUrgentOrders'                 => t('setting', 'Maximum number of booked urgent orders'),
            'workerReviewCount'                           => t('setting', 'Count of the last reviews taking into account in rating'),
            'workerMinReviewRating'                       => t('setting', 'Minimum rating to enter into system'),
            'workerTimeCalculate'                      => t('setting', 'Сalculate the minimum time to order'),
            'workerTimeCalculateStep'                      => t('setting', 'Time increment step from the minimum calculated time'),
            'workerTimeCalculateStepCount'                      => t('setting', 'The number of time steps for the calculated time to order'),
            'workerOrderSound'                      => t('setting', 'Allow to voice order details'),
        ];
    }


    public function getArrivalTime()
    {
        return explode(';', $this->workerArrivalTime);
    }

    public function setArrivalTime($val)
    {
        $this->workerArrivalTime = implode(';', $val);
    }


    public function getTenantId()
    {
        return (string)user()->tenant_id;
    }

    public function getCityId()
    {
        return $this->cityId;
    }

    public function getPositionId()
    {
        return $this->positionId;
    }

    public function __get($name)
    {
        // allowWorkWithoutGps -> ALLOW_WORK_WITHOUT_GPS
        $nameParams = strtoupper(preg_filter("#[A-Z]#", "_$0", $name));

        if (key_exists($nameParams, $this->newParams)) {
            return $this->newParams[$nameParams];
        }

        if (key_exists($nameParams, $this->oldParams)) {
            return $this->oldParams[$nameParams];
        }

        return parent::__get($name);
    }

    public function __set($name, $value)
    {
        // allowWorkWithoutGps -> ALLOW_WORK_WITHOUT_GPS
        $nameParams = strtoupper(preg_filter("#[A-Z]#", "_$0", $name));

        if (in_array($nameParams, WorkerOptionsName::asArray())) {
            $this->newParams[$nameParams] = $value;
        } else {
            parent::__set($name, $value);
        }
    }

    public function save() // TODO rememmber - save value of cache
    {
        if (!$this->validate()) {
            return false;
        }

        $newSettings = array_diff_assoc($this->newParams, $this->oldParams);

        array_walk($newSettings, [$this, 'saveTenantSettings']);

        return array_reduce($newSettings, function ($c, $it) {
            return $c && $it;
        }, true);
    }

    // For array_walk, result recive on params $value
    private function saveTenantSettings(&$value, $name)
    {
        $tenSet = TenantSetting::findOne([
            'name'        => $name,
            'tenant_id'   => $this->getTenantId(),
            'city_id'     => $this->getCityId(),
            'position_id' => $this->getPositionId(),
        ]);
        if ($tenSet instanceof TenantSetting) {
            $tenSet->value = $value;
            $value         = $tenSet->save(false, ['value']);
        } else {
            $tenSet = new TenantSetting([
                'tenant_id'   => $this->getTenantId(),
                'name'        => $name,
                'value'       => $value,
                'type'        => self::TYPE,
                'city_id'     => $this->getCityId(),
                'position_id' => $this->getPositionId(),
            ]);
            $value  = $tenSet->insert(false);
        }

        TenantSetting::updateCachedSettings($this->getTenantId(), self::TYPE, $this->getCityId(),
            $this->getPositionId());
    }

    public function getWorkerMinBalance()
    {
        $name = WorkerOptionsName::WORKER_MIN_BALANCE;

        return key_exists($name, $this->newParams) ? $this->newParams[$name] : $this->oldParams[$name];
    }

    public function setWorkerMinBalance($value)
    {
        $this->newParams[WorkerOptionsName::WORKER_MIN_BALANCE] = $value;
    }

}