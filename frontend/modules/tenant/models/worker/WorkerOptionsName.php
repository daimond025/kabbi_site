<?php

namespace frontend\modules\tenant\models\worker;

use yii\base\Object;

class WorkerOptionsName extends Object
{

    /**
     * Показывать предварительный расчет
     */
    const SHOW_ESTIMATION = 'SHOW_ESTIMATION';

    /**
     * Разрешить редактировать заказ
     */
    const ALLOW_EDIT_ORDER = 'ALLOW_EDIT_ORDER';

    /**
     * Разрешить редактировать стоимость заказа
     */
    const ALLOW_EDIT_COST_ORDER = 'ALLOW_EDIT_COST_ORDER';

    /**
     * Разрешить работать исполнителю без GPS?
     */
    const ALLOW_WORK_WITHOUT_GPS = 'ALLOW_WORK_WITHOUT_GPS';

    /**
     * Минимальный баланс для входа в систему
     */
    const WORKER_MIN_BALANCE = 'WORKER_MIN_BAlANCE';

    /**
     * Показывать исполнителю пункт назначения
     */
    const SHOW_WORKER_ADDRESS = 'SHOW_WORKER_ADDRESS';

    /**
     * Показывать номер клиента по умолчанию
     */
    const SHOW_WORKER_PHONE = 'SHOW_WORKER_PHONE';

    /**
     * Показывать ФИО клиента
     */
    const SHOW_WORKER_CLIENT = 'SHOW_WORKER_CLIENT';

    /**
     * Отображать чат с диспетчером до выхода на смену
     */
    const SHOW_CHAT_DISPATCHER_EXIT = 'SHOW_CHAT_DISPATCHER_EXIT';

    /**
     * Отображать чат с диспетчером во время смены
     */
    const SHOW_CHAT_DISPATCHER_SHIFT = 'SHOW_CHAT_DISPATCHER_SHIFT';

    /**
     * "Отображать общий чат во время смены?
     */
    const SHOW_GENERAL_CHAT_SHIFT = 'SHOW_GENERAL_CHAT_SHIFT';

    /**
     * Разрешить пополнять баланс исполнителю через банковскую карту
     */
    const ALLOW_USE_BANK_CARD = 'ALLOW_USE_BANK_CARD';

    /**
     * Разрешить закрывать смену, если координаты исполнителюю не актуальны
     */
    const NEED_CLOSE_WORKER_SHIFT_BY_BAD_POSITION_UPDATE_TIME = 'NEED_CLOSE_WORKER_SHIFT_BY_BAD_POSITION_UPDATE_TIME';

    /**
     * Время, по истечению которого, координаты исполнителю станут не актуальны
     */
    const WORKER_POSITION_CHECK_DELAY_TIME = 'WORKER_POSITION_CHECK_DELAY_TIME';

    /**
     * Разрешить закрывать смену, если координаты исполнителю вне зоны города или зоны приема заказов
     */
    const NEED_CLOSE_WORKER_SHIFT_BY_BAD_PARKING_POSITION = 'NEED_CLOSE_WORKER_SHIFT_BY_BAD_PARKING_POSITION';

    /**
     * Время, по истечению которого, будет закрыта смена исполнителю, если координаты исполнителю вне зоны города или
     * зоны приема заказов
     */
    const WORKER_PARKING_POSITION_CHECK_DELAY_TIME = 'WORKER_PARKING_POSITION_CHECK_DELAY_TIME';

    /**
     * Исполнителю вне зоны города/ исполнителю вне зоны приема заказов
     */
    const WORKER_PARKING_POSITION_CHECK_TYPE = 'WORKER_PARKING_POSITION_CHECK_TYPE';

    /**
     * Время прибытия исполнителя
     */
    const WORKER_ARRIVAL_TIME = 'WORKER_ARRIVAL_TIME';

    /**
     * Время прибытия исполнителя
     */
    const SHOW_URGENT_ORDER_TIME = 'SHOW_URGENT_ORDER_TIME';

    /**
     * Разрешить создавать заказ с бордюра?
     */
    const ALLOW_WORKER_TO_CREATE_ORDER = 'ALLOW_WORKER_TO_CREATE_ORDER';

    /**
     * Запретить использовать fakeGPS"
     */
    const DENY_FAKEGPS = 'DENY_FAKEGPS';

    /**
     * Запретить нажимать исполнителю кнопку "Приехал" заранее?
     */
    const DENY_SETTING_ARRIVAL_STATUS_EARLY = 'DENY_SETTING_ARRIVAL_STATUS_EARLY';

    /**
     * Минимальное расстояние до точки А, на котором разрешено нажать кнопку Приехал
     */
    const MIN_DISTANCE_TO_SET_ARRIVAL_STATUS = 'MIN_DISTANCE_TO_SET_ARRIVAL_STATUS';

    /**
     * Контролировать пробег своих авто?
     */
    const CONTROL_OWN_CAR_MILEAGE = 'CONTROL_OWN_CAR_MILEAGE';
    /**
     * Печатать чек по окончании поездки?
     */
    const PRINT_CHECK = 'PRINT_CHECK';
    /**
     * Требовать пароль при каждом входе в приложение
     */
    const REQUIRE_PASSWORD_EVERY_TIME_LOG_IN_APPLICATION = 'REQUIRE_PASSWORD_EVERY_TIME_LOG_IN_APPLICATION';

    /**
     * Показывать политику конфиденциальности или нет
     */
    const SHOW_DRIVER_PRIVACY_POLICY = 'SHOW_DRIVER_PRIVACY_POLICY';

    /**
     * Разрешить исполнителю бронировать за собой срочные заказы
     */
    const ALLOW_RESERVE_URGENT_ORDERS = 'ALLOW_RESERVE_URGENT_ORDERS';

    /**
     * Максимальное число забронированных срочных заказов
     */
    const MAX_COUNT_RESERVE_URGENT_ORDERS = 'MAX_COUNT_RESERVE_URGENT_ORDERS';

    /**
     * Добавлять промежуточную точку в заказ при нажатии кнопки Остановка
     */
    const ADD_INTERMEDIATE_POINT_IN_ORDER_FOR_STOP = 'ADD_INTERMEDIATE_POINT_IN_ORDER_FOR_STOP';

    /**
     * Максимальное кол-во последних отзывов использующие в расчете рейтинга исполнителя
     */
    const WORKER_REVIEW_COUNT = 'WORKER_REVIEW_COUNT';

    /**
     * Минимальный рейтинг для входа исполнителя на смену
     */
    const WORKER_MIN_REVIEW_RATING = 'WORKER_MIN_REVIEW_RATING';

    /**
     * показывать минимальное время  как вычисленное  от водителя до заказа (через сервисы)
     */
    const WORKER_TIME_CALCULATE = 'WORKER_TIME_CALCULATE';

    /**
     шаг вычисления минимального времени  вычесленного от водителя до заказа (через сервисы)
     */
    const WORKER_TIME_CALCULATE_STEP = 'WORKER_TIME_CALCULATE_STEP';

    /**
      количество временных меток   вычесленного от водителя до заказа (через сервисы)
     */
    const WORKER_TIME_CALCULATE_STEP_COUNT = 'WORKER_TIME_CALCULATE_STEP_COUNT';

    /**
     опция озвучивание дедатлей узаказа в водительском приложении
     */

    const WORKER_ORDER_SOUND = 'WORKER_ORDER_SOUND';

    public static function asArray()
    {
        return [
            self::SHOW_ESTIMATION,
            self::ALLOW_EDIT_ORDER,
            self::ALLOW_EDIT_COST_ORDER,
            self::ALLOW_WORK_WITHOUT_GPS,
            self::WORKER_MIN_BALANCE,
            self::SHOW_WORKER_ADDRESS,
            self::SHOW_WORKER_PHONE,
            self::SHOW_WORKER_CLIENT,
            self::SHOW_URGENT_ORDER_TIME,
            self::SHOW_CHAT_DISPATCHER_EXIT,
            self::SHOW_CHAT_DISPATCHER_SHIFT,
            self::SHOW_GENERAL_CHAT_SHIFT,
            self::ALLOW_USE_BANK_CARD,
            self::NEED_CLOSE_WORKER_SHIFT_BY_BAD_POSITION_UPDATE_TIME,
            self::WORKER_POSITION_CHECK_DELAY_TIME,
            self::NEED_CLOSE_WORKER_SHIFT_BY_BAD_PARKING_POSITION,
            self::WORKER_PARKING_POSITION_CHECK_DELAY_TIME,
            self::WORKER_PARKING_POSITION_CHECK_TYPE,
            self::WORKER_ARRIVAL_TIME,
            self::ALLOW_WORKER_TO_CREATE_ORDER,
            self::DENY_FAKEGPS,
            self::DENY_SETTING_ARRIVAL_STATUS_EARLY,
            self::MIN_DISTANCE_TO_SET_ARRIVAL_STATUS,
            self::SHOW_DRIVER_PRIVACY_POLICY,
            self::CONTROL_OWN_CAR_MILEAGE,
            self::PRINT_CHECK,
            self::REQUIRE_PASSWORD_EVERY_TIME_LOG_IN_APPLICATION,
            self::ADD_INTERMEDIATE_POINT_IN_ORDER_FOR_STOP,
            self::ALLOW_RESERVE_URGENT_ORDERS,
            self::MAX_COUNT_RESERVE_URGENT_ORDERS,
            self::WORKER_REVIEW_COUNT,
            self::WORKER_MIN_REVIEW_RATING,
            self::WORKER_TIME_CALCULATE,
            self::WORKER_TIME_CALCULATE_STEP,
            self::WORKER_TIME_CALCULATE_STEP_COUNT,
            self::WORKER_ORDER_SOUND,
        ];
    }
}
