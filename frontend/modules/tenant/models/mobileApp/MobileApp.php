<?php

namespace frontend\modules\tenant\models\mobileApp;

use common\modules\tenant\models\mobileApp\MobileApp as BaseMobileApp;

/**
 * Class MobileApp
 *
 * @package frontend\modules\tenant\mobileApp
 */
class MobileApp extends BaseMobileApp
{
    public function rules()
    {
        return [];
    }

    /**
     * @return bool
     */
    public function getIsActive()
    {
        return $this->active === 1;
    }

    public function getIsAndroidLink()
    {
        return !empty($this->link_android);
    }

    public function getIsIosLink()
    {
        return !empty($this->link_ios);
    }

    public function getIsLink()
    {
        return $this->getIsAndroidLink() || $this->getIsIosLink();
    }

}