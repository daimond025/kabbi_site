<?php

use yii\helpers\Html;

/**
 * @var array   $cityList
 * @var integer $currentCityId
 * @var array   $url
 * @var string  $attribute
 */

?>

<div class="order_cities">
    <ul>
        <? foreach ($cityList as $cityId => $cityName):
            $label = Html::tag('span', $cityName);
            $link  = Html::a($label, array_merge($url, [$attribute => $cityId]), [
                'class'   => $cityId === $currentCityId ? 'active' : null,
                'onclick' => $cityId === $currentCityId ? 'return false' : null,
            ]);
            ?>
            <?= Html::tag('li', $link); ?>
        <? endforeach; ?>
    </ul>
</div>
