<?php
namespace frontend\modules\tenant\widgets;

use yii\widgets\ActiveForm;
use yii\helpers\Html;

class ActiveNonForm extends ActiveForm
{
    
    public $validateOnSubmit = false;

    public $validateOnChange = false;

    public $validateOnBlur = false;

    public $validateOnType = false;
    
    public $enableClientValidation = false;

    public $enableAjaxValidation = false;

    public $enableClientScript = false;
    
    public $fieldClass = 'frontend\modules\tenant\widgets\ActiveNonField';
    
    public function run()
    {
        if (!empty($this->_fields)) {
            throw new InvalidCallException('Each beginField() should have a matching endField() call.');
        }
        $content = ob_get_clean();
        echo Html::beginTag('div', ['class' => 'non-editable']), $content, Html::endTag('div');
    }
    
}