<?php
namespace frontend\modules\tenant\widgets;

use yii\widgets\ActiveField;
use yii\helpers\Html;

// TODO Not complete
class ActiveNonField extends ActiveField
{
    
    public function textInput($options = [])
    {
        $options = array_merge($this->inputOptions, $options);
        $this->adjustLabelFor($options);
        if (!array_key_exists('id', $options)) {
            $options['id'] = Html::getInputId($this->model, $this->attribute);
        }
        $this->parts['{input}'] = Html::tag('label',
                Html::getAttributeValue($this->model, $this->attribute), $options);
        return $this;
    }
    
    public function input($type, $options = [])
    {
        return $this->textInput($options);
    }
    
    public function hiddenInput($options = [])
    {
        $this->parts['{input}'] = '';
        return $this;
    }
    
    public function passwordInput($options = [])
    {
        return $this->hiddenInput($options);
    }
    
    public function fileInput($options = [])
    {
        return $this;
    }
    
    public function textarea($options = [])
    {
        return $this;
    }
    
    public function dropDownList($items, $options = [])
    {
        return $this;
    }
    
    public function listBox($items, $options = [])
    {
        return $this;
    }
}