<?php

namespace frontend\modules\tenant\widgets;

use yii\base\Widget;

/**
 * Class CityListNavigateWidget
 * @package frontend\modules\tenant\widgets
 *
 * @property array   $cityList       Массив филиалов [26068 => 'Ижевск']
 * @property integer $currencyCityId Идентификатор текущего филиала
 * @property array   $url            Массив для формирования ссылок ['geo-service', 'page' => 50]
 * @property string  $attribute      Значение параметра, в котором будет передаваться идентификатор филиала
 */
class CityListNavigateWidget extends Widget
{
    public $cityList;
    public $currentCityId;
    public $url;
    public $attribute = 'id';

    public function run()
    {
        return $this->render('city-list-navigate', [
            'cityList'      => $this->cityList,
            'currentCityId' => $this->currentCityId,
            'url'           => $this->url,
            'attribute'     => $this->attribute,
        ]);
    }
}