<?php

namespace frontend\modules\car;

use app\components\behavior\ControllerBehavior;
use yii\helpers\ArrayHelper;

/**
 * Class Module
 * @package frontend\modules\car
 *
 * @mixin ControllerBehavior
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\car\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'common' => [
                'class' => ControllerBehavior::className(),
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $this->commonBeforeAction();

        return parent::beforeAction($action);
    }
}
