function updateCarOptionList(brand, model, city_id) {
    $.get(
        '/car/car/get-tenant-car-options',
        {city_id: city_id, brand: brand, model: model},
        function (json) {

            if ($.isEmptyObject(json.carOptions)) {
                $('#car_options').hide();
            } else {
                $('#car_options').show();
            }

            var last = $('.auto_options').find('li').last();
            $('.auto_options').empty();
            for (var key in json.carOptions) {
                $('.auto_options').append('<li><span>+' + json.tenantOptions[key] + '</span><label><input data-raiting="' +
                    json.tenantOptions[key] + '" value="' + key + '" name="Car[options][]" type="checkbox"> ' + json.carOptions[key] + '</label></li>');
            }
            $('.auto_options').append(last);
            $('#total_raiting').text(0);
        },
        'json'
    );
}

function updateCarClassList(model_id) {
    $.get(
        '/car/car/get-car-class',
        {model_id: model_id},
        function (json) {

            json.sort(function (a, b) {
                if (a['sort'] > b['sort']) {
                    return 1;
                }
                if (a['sort'] < b['sort']) {
                    return -1;
                }
                return 0;
            });

            var $classes = $();
            var class_select = $('#car-class_id');

            class_select.find('option:not(":first")').remove();

            if (!$.isEmptyObject(json)) {
                for (var id in json) {
                    $classes = $classes.add($('<option/>').val(json[id]['class_id']).text(json[id]['class']));
                }

                class_select.append($classes);
            }

            class_select.trigger('update');
        },
        'json'
    );
}

$(function () {
    //Получение опций авто для нового города
    var old_city_id = null;
    $('#car-city_id').on('change', function () {
        var city_id = $(this).val();

        if (city_id === old_city_id) {
            return false;
        }
        old_city_id = city_id;

        var brand = $('#car-brand_id :selected').text();
        var model = $('#car-model_id :selected').text();

        updateCarOptionList(brand, model, city_id);
    });

    //Подсчет рейтинга
    $(document).on('click', '#car-form .auto_options input[type="checkbox"]', function () {
        var tota_obj = $('#total_raiting');
        var total = parseInt(tota_obj.text());
        var this_val = parseInt($(this).data('raiting'));

        if ($(this).prop("checked"))
            total += this_val;
        else
            total -= this_val;

        tota_obj.text(total);
    });

    //Подгрузка модели авто
    $('#car-form .field-car-brand_id div.c_select a').on('click', function () {
        var brand_id = $(this).attr('rel');
        var url = $(this).parents('.row_input').data('url');

        if (brand_id) {
            $.post(
                url,
                {brand_id: brand_id},
                function (json) {
                    var select = $('#car-model_id');
                    var a_select = select.next('.d_select').find('.c_select ul');

                    select.empty();
                    a_select.empty();

                    for (var i = 0; i < json.length; i++) {
                        select.append('<option value="' + json[i].model_id + '">' + json[i].name + '</option>');
                        a_select.append('<li class="li_filter" data-instafilta-hide="true"><a rel="' + json[i].model_id + '">' + json[i].name + '</a></li>');
                    }

                    initInstaFilta();
                },
                'json'
            );
        }
    });

    //Формирование названия авто
    $('#car-form').on('submit', function () {
        var brand = $('#car-brand_id :selected').text();
        var model = $('#car-model_id :selected').text();

        $('#car_brand').val(brand);
        $('#car_model').val(model);
    });
    //--------------------------------------------------------------------------------------------

    //Выбор модели авто
    $('#car-model_id').on('change', function () {
        var brand = $('#car-brand_id :selected').text();
        var model = $('#car-model_id :selected').text();
        var city_id = $('#car-city_id').val();

        $.get(
            '/car/car/add-fields',
            {brandName: brand, modelName: model},
            function (html) {
                $("#dynamic_fields").html(html);
                select_init();
            }
        );

        updateCarOptionList(brand, model, city_id);
        updateCarClassList($(this).val());
    });

    $(document).on('click', 'form[name="mileage-search"] a.oor_compare', function (e) {
        e.preventDefault();
        var form = $(this).parents('form');
        var $this = $(this);
        var formData;

        if ($this.hasClass('loading_button')) {
            return;
        }

        formData = form.serializeArray();
        formData.push({name: 'hideFilter', value: '1'});
        formData = formData.reduce(function (obj, item) {
            obj[item.name] = item.value;
            return obj;
        }, {});

        $this.addClass('loading_button');
        $.post(form.attr('action'), formData)
            .done(function (html) {
                $('#car-mileage').html(html);
            })
            .always(function () {
                $this.removeClass('loading_button');
            });
    });
});