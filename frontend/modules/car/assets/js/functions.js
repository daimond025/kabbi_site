/* global $ */
function calendarFilterInit() {
    $('.sdp').each(function () {
        $(this).datepicker({
            altField: $(this).prev('input'),
            onSelect: function (date) {
                $(this).parent().prev('.a_sc').html(date).removeClass('sel_active');
                $(this).parent().hide();
            }
        });
        if ($(this).hasClass('first_date')) {
            $(this).datepicker("setDate", '-14');
        }
        $(this).closest('.cof_date').find('.a_sc').text($(this).val());
    });
}