<?php

namespace frontend\modules\car\assets;

use yii\web\AssetBundle;


class CarAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/car/assets/js';

    public $publishOptions = [
        'forceCopy' => true,
    ];

    public $css = [
    ];
    public $js = [
        'main.js',
        'functions.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}