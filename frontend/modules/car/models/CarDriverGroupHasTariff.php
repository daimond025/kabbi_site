<?php

namespace frontend\modules\car\models;

use Yii;

/**
 * This is the model class for table "{{%car_driver_group_has_tariff}}".
 *
 * @property integer $car_id
 * @property integer $tariff_id
 *
 */
class CarDriverGroupHasTariff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%car_has_worker_group_tariff}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_id', 'tariff_id'], 'required'],
            [['car_id', 'tariff_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'car_id'    => 'Group ID',
            'tariff_id' => 'Tariff ID',
        ];
    }

    /**
     * Allow to save multiple rows
     *
     * @param array   $arValues
     * @param integer $car_id
     *
     * @return integer number of rows affected by the execution|null
     * @throws Exception execution failed
     */
    public static function insertRows($arValues, $car_id)
    {
        $insertValue = [];
        $connection  = app()->db;

        foreach ($arValues as $tariff_id) {
            if (!empty($tariff_id)) {
                $insertValue[] = [$car_id, $tariff_id];
            }
        }

        return !empty($insertValue) ? $connection->createCommand()->batchInsert(self::tableName(),
            ['car_id', 'tariff_id'], $insertValue)->execute() : null;
    }

    /**
     * Update rows.
     *
     * @param array   $arValues
     * @param integer $car_id
     * @param integer $old_tariff_ids
     *
     * @return int number of rows affected by the execution.
     */
    public static function updateRows($arValues, $car_id, $old_tariff_ids = null)
    {
        $condition = $old_tariff_ids === null
            ? ['car_id' => $car_id]
            : ['car_id' => $car_id, 'tariff_id' => $old_tariff_ids];

        self::deleteAll($condition);

        return self::insertRows($arValues, $car_id);
    }
}
