<?php

namespace frontend\modules\car\models;

use common\modules\car\models\CarHasOption as CarHasOptionBase;
use Yii;

/**
 * This is the model class for table "{{%car_has_option}}".
 *
 * @property string $car_id
 * @property integer $option_id
 *
 * @property TblCar $car
 * @property TblCarOption $option
 */
class CarHasOption extends CarHasOptionBase
{
    /**
     * Allow to save multiple models
     * @param array $arOptions
     * @param array $car_id
     * @return boolean
     */
    public static function batchInsert($arOptions, $car_id)
    {
        if (is_array($arOptions) && !empty($arOptions)) {
            $insertValue = [];
            $connection = app()->db;

            foreach ($arOptions as $option_id) {
                if ($option_id > 0) {
                    $insertValue[] = [$car_id, $option_id];
                }
            }

            if (!empty($insertValue)) {
                $connection->createCommand()->batchInsert(self::tableName(), ['car_id', 'option_id'],
                    $insertValue)->execute();

                return true;
            }
        }

        return false;
    }
}
