<?php

namespace frontend\modules\car\models;

use app\modules\tariff\models\TaxiTariff;
use common\modules\car\models\CarClass as CarClassBase;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%car_class}}".
 *
 * @property integer $class_id
 * @property string $class
 *
 * @property TaxiTariff[] $tblTaxiTariffs
 */
class CarClass extends CarClassBase
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxiTariffs()
    {
        return $this->hasMany(TaxiTariff::className(), ['class_id' => 'class_id']);
    }

    /**
     * @param bool $locale
     * @return array
     */
    public static function getClasses($locale = false)
    {
        return ArrayHelper::map(self::find()->all(), 'class_id', function($array) use ($locale){
            return $locale ? t('car', $array['class']) : $array['class'];
        });
    }

    /**
     * @return array
     */
    public static function getClassIdList()
    {
        return self::find()->select('class_id')->column();
    }

    /**
     * Locale car class.
     * @return string
     */
    public function getClass()
    {
        return t('car', $this->class);
    }

    /**
     * Getting car class map
     * @param int $transport_type_id
     * @return array
     */
    public static function getCarClassMapByTransportType($transport_type_id)
    {
        $carClasses = self::find()
            ->select(['class_id', 'class', 'sort'])
            ->where(['type_id' => $transport_type_id])
            ->orderBy('sort')
            ->asArray()
            ->all();

        return array_map(function ($item) {
            $item['class'] = t('car', $item['class']);
            $item['sort'] = (int)$item['sort'];
            return $item;
        }, $carClasses);
    }
}
