<?php

namespace frontend\modules\car\models;

use common\modules\employee\models\worker\WorkerGroup;
use Yii;

/**
 * This is the model class for table "{{%car_driver_group_has_class}}".
 *
 * @property integer     $car_id
 * @property integer     $class_id
 *
 * @property CarClass    $class
 * @property WorkerGroup $group
 */
class CarDriverGroupHasClass extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%car_has_worker_group_class}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_id', 'class_id'], 'required'],
            [['car_id', 'class_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'car_id'   => 'Group ID',
            'class_id' => 'Class ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClass()
    {
        return $this->hasOne(CarClass::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(WorkerGroup::className(), ['car_id' => 'car_id']);
    }

    /**
     * Allow to save multiple rows
     *
     * @param array   $arValues
     * @param integer $car_id
     *
     * @return integer number of rows affected by the execution|null
     * @throws Exception execution failed
     */
    public static function insertRows($arValues, $car_id)
    {
        $insertValue = [];
        $connection  = app()->db;

        foreach ($arValues as $class_id) {
            if (!empty($class_id)) {
                $insertValue[] = [$car_id, $class_id];
            }
        }

        return !empty($insertValue) ? $connection->createCommand()->batchInsert(self::tableName(),
            ['car_id', 'class_id'], $insertValue)->execute() : null;
    }

    /**
     * Update rows.
     *
     * @param array     $arValues
     * @param integer   $car_id
     * @param integer[] $old_class_ids
     *
     * @return integer number of rows affected by the execution.
     */
    public static function updateRows($arValues, $car_id, $old_class_ids = null)
    {
        $condition = $old_class_ids === null
            ? ['car_id' => $car_id]
            : ['car_id' => $car_id, 'class_id' => $old_class_ids];

        self::deleteAll($condition);

        return self::insertRows($arValues, $car_id);
    }
}
