<?php


namespace frontend\modules\car\models;


use app\modules\order\models\OrderHasOption;
use app\modules\tariff\models\AdditionalOption;
use common\modules\car\models\CarOption as CarOptionBase;

class CarOption extends CarOptionBase
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdditionalOptions()
    {
        return $this->hasMany(AdditionalOption::className(), ['additional_option_id' => 'option_id']);
    }

    public function getOrderOptions()
    {
        return $this->hasMany(OrderHasOption::class, ['option_id' => 'option_id']);
    }
}
