<?php

namespace frontend\modules\car\models;


use common\components\behaviors\ActiveRecordBehavior;
use common\modules\car\models\Car as CarBase;
use common\modules\car\models\CarModel;
use common\modules\city\models\City;
use common\modules\tenant\models\TenantHasCarOption;
use frontend\modules\companies\models\TenantCompany;
use frontend\modules\employee\models\groups\WorkerGroup;
use Yii;
use common\modules\tenant\models\Tenant;
use common\modules\employee\models\worker\WorkerHasCar;
use frontend\components\behavior\file\FileBehavior;
use frontend\components\behavior\file\CropFileBehavior;
use common\modules\tenant\models\TenantHelper;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_car".
 *
 * @property string         $car_id
 * @property string         $tenant_id
 * @property integer        $class_id
 * @property string         $city_id
 * @property string         $name
 * @property string         $gos_number
 * @property string         $color
 * @property string         $year
 * @property string         $photo
 * @property integer        $owner
 * @property integer        $raiting
 * @property string         $create_time
 * @property integer        $active
 * @property string         $license
 * @property string         $brand
 * @property string         $model
 * @property string         $license_scan
 * @property string         $group_id
 * @property string         $callsign
 *
 * @property CarClass       $class
 * @property Tenant         $tenant
 * @property City           $city
 * @property CarHasOption[] $tblCarHasOptions
 * @property CarOption[]    $options
 */
class Car extends CarBase
{
    const SCENARIO_PHOTO = 'logo';

    public $cropParams;
    public $brand_id;
    public $brand_name;
    public $model_name;
    public $options = [];
    public $new_options = 0;
    public $block;
    public $driver_group_tariffs;
    public $driver_group_classes;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['callsign', 'string', 'max' => 10],
            ['callsign', 'checkUniqueByTenant'],
            [['name', 'gos_number', 'year', 'owner', 'city_id', 'class_id'], 'required'],
            [['tenant_id', 'class_id', 'city_id', 'raiting', 'active', 'tenant_company_id'], 'integer'],
            [['year', 'brand_id', 'options', 'block', 'new_options', 'cropParams', 'model_id', 'license_scan'], 'safe'],
            [['gos_number', 'color'], 'string', 'max' => 10],
            [['license'], 'string', 'max' => 45],
            [['brand', 'model'], 'string'],
            [
                ['photo'],
                'image',
                'extensions' => ['png', 'jpeg', 'jpg'],
                'maxSize'    => getUploadMaxFileSize(),
                'tooBig'     => t('file', 'The file size must be less than {file_size}Mb',
                    ['file_size' => getUploadMaxFileSize(true)]),
                'minWidth'   => 300,
                'minHeight'  => 300,
            ],

            [
                ['tenant_company_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => TenantCompany::className(),
                'targetAttribute' => ['tenant_company_id' => 'tenant_company_id'],
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'callsign'          => Yii::t('car', 'Callsign'),
            'car_id'            => Yii::t('car', 'Car ID'),
            'tenant_id'         => Yii::t('car', 'Tenant ID'),
            'class_id'          => Yii::t('car', 'Type of car'),
            'city_id'           => Yii::t('car', 'City'),
            'name'              => Yii::t('car', 'Car name'),
            'gos_number'        => Yii::t('car', 'Gov Number'),
            'color'             => Yii::t('car', 'Color'),
            'year'              => Yii::t('car', 'Year'),
            'photo'             => Yii::t('car', 'Photo'),
            'owner'             => Yii::t('car', 'Whose car'),
            'raiting'           => Yii::t('car', 'Priority'),
            'create_time'       => Yii::t('car', 'Create Time'),
            'active'            => Yii::t('car', 'Active'),
            'license'           => Yii::t('car', 'License'),
            'license_scan'      => Yii::t('car', 'Scan'),
            'brand'             => Yii::t('car', 'Brand'),
            'model'             => Yii::t('car', 'Model'),
            'group_id'          => 'group_id',
            'tenant_company_id' => Yii::t('tenant_company', 'Companies'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClass()
    {
        return $this->hasOne(CarClass::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarHasOptions()
    {
        return $this->hasMany(CarHasOption::className(), ['car_id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptions()
    {
        return $this->hasMany(CarOption::className(), ['option_id' => 'option_id'])->viaTable('{{%car_has_option}}',
            ['car_id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(WorkerGroup::className(), ['group_id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupOnClassId()
    {
        return $this->hasOne(WorkerGroup::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupCanViewClass()
    {
        return $this->hasMany(CarDriverGroupHasClass::className(), ['car_id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupHasDriverTariff()
    {
        return $this->hasMany(CarDriverGroupHasTariff::className(), ['car_id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerHasCars()
    {
        return $this->hasMany(WorkerHasCar::className(), ['car_id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModel()
    {
        return $this->hasOne(CarModel::className(), ['model_id' => 'model_id']);
    }

    public function behaviors()
    {
        return [
            'logo'   => [
                'class'      => CropFileBehavior::className(),
                'fileField'  => ['photo'],
                'upload_dir' => app()->params['upload'],
            ],
            'file'   => [
                'class'                => FileBehavior::className(),
                'fileField'            => ['license_scan'],
                'upload_dir'           => app()->params['uploadScanDir'],
                'max_big_picture_side' => 900,
            ],
            'common' => [
                'class' => ActiveRecordBehavior::className(),
            ],
        ];
    }

    /**
     * For getting variants for field $owner
     * @return array
     */
    public function getVariantsOfOwnerCar()
    {
        return [
            'COMPANY' => t('car', 'Company'),
            'WORKER'  => t('car', 'Driver'),
        ];
    }

    /**
     * Getting color text.
     * @return string
     */
    public function getColorText()
    {
        $variants = CarColor::getColorMap();

        return getValue($variants[$this->color]);
    }

    public static function getGridData($cars, $city_list = true, $type_list = true, $with_groups = false)
    {
        $arCars['CARS'] = [];

        if ($city_list) {
            $arCity = [];
        }

        if ($type_list) {
            $arTypes = [];
            $types   = [];
        }

        foreach ($cars as $car) {
            if ($city_list && !array_key_exists($car->city['city_id'], $arCity)) {
                $arCity[$car->city['city_id']] = $car->city['name' . getLanguagePrefix()];
            }

            if ($type_list && !in_array($car->class['class_id'], $types)) {
                $types[]   = $car->class['class_id'];
                $arTypes[] = [
                    'class_id' => $car->class['class_id'],
                    'class'    => $car->class['class'],
                    'sort'     => $car->class['sort'],
                ];
            }

            $arCars['CARS'][] = [
                'ID'            => $car->car_id,
                'PHOTO'         => $car->isFileExists($car->photo) ? $car->getPictureHtml($car->photo, false) : '',
                'NAME'          => $car->name,
                'RAITING'       => $car->raiting,
                'GOV_NUMBER'    => $car->gos_number,
                'YEAR'    => $car->year,
                'CLASS'         => $car->class['class'],
                'CLASS_ID'      => $car->class['class_id'],
                'GROUP_ID'      => $car->group_id,
                'GROUP_TARIFFS' => $with_groups && !empty($car->groupHasDriverTariff) ? ArrayHelper::getColumn($car->groupHasDriverTariff,
                    'tariff_id') : [],
                'GROUP_CLASSES' => $with_groups && !empty($car->groupCanViewClass) ? ArrayHelper::getColumn($car->groupCanViewClass,
                    'class_id') : [],
            ];
        }

        if ($city_list) {
            asort($arCity);
            $arCars['CITI_LIST'] = $arCity;
        }

        if ($type_list) {
            usort($arTypes, function ($a, $b) {
                if ($a['sort'] === $b['sort']) {
                    return 0;
                }

                return (int)($a['sort'] > $b['sort']);
            });
            $arCars['TYPE_LIST'] = ArrayHelper::map($arTypes, 'class_id', 'class');
        }

        return $arCars;
    }

    /**
     *
     * @param array $arOptions [option_id, option_id...]
     *
     * @return integer
     */
    public function calculateRating($arOptions)
    {
        $rating = 0;

        if (!empty($arOptions)) {
            $tenant_car_options = TenantHasCarOption::find()
                ->where([
                    'tenant_id' => user()->tenant_id,
                    'city_id'   => $this->city_id,
                    'option_id' => $arOptions,
                ])
                ->asArray()
                ->all();

            foreach ($tenant_car_options as $item) {
                $rating += $item['value'];
            }
        }

        return $rating;
    }

    public function beforeSave($insert)
    {
        if ($this->new_options) {
            $this->raiting = $this->calculateRating($this->options);
        }

        if (!$insert) {
            if ($this->getOldAttribute('class_id') != $this->class_id) {
                $this->group_id = null;
            }
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->scenario == self::SCENARIO_PHOTO) {
            return false;
        }

        if ($insert) {
            TenantHelper::fillHelpItem(TenantHelper::CAR_FILL);
        }

        if (array_key_exists('group_id', $changedAttributes)) {
            if (!empty($this->group_id)) {
                if (!empty($this->driver_group_tariffs)) {
                    CarDriverGroupHasTariff::updateRows($this->driver_group_tariffs, $this->car_id);
                }
                if (!empty($this->driver_group_classes)) {
                    CarDriverGroupHasClass::updateRows($this->driver_group_classes, $this->car_id);
                }
            } else {
                CarDriverGroupHasTariff::deleteAll(['car_id' => $this->car_id]);
                CarDriverGroupHasClass::deleteAll(['car_id' => $this->car_id]);
            }
        } elseif (array_key_exists('class_id',
                $changedAttributes) && $this->class_id != $changedAttributes['class_id']) {
            CarDriverGroupHasTariff::deleteAll(['car_id' => $this->car_id]);
            CarDriverGroupHasClass::deleteAll(['car_id' => $this->car_id]);
        }
    }

    public function beforeValidate()
    {
        $this->name = trim($this->brand . ' ' . $this->model);

        return parent::beforeValidate();
    }

    public static function quantityByTenant($tenant_id)
    {
        $driversQuantity = new \yii\db\Query();
        $driversQuantity->from(self::tableName());
        $driversQuantity->where(['tenant_id' => $tenant_id]);

        return $driversQuantity->count();
    }

    public function checkUniqueByTenant($attr)
    {
        $find = self::find()->where([$attr => $this->$attr, 'tenant_id' => $this->tenant_id])
            ->andWhere(['!=', 'car_id', $this->car_id])
            ->exists();
        if ($find) {
            $this->addError($attr, t('car', 'This callsign is already'));
        }
    }
}
