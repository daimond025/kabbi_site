<?php
/**
 * Created by PhpStorm.
 * User: zigfried123
 * Date: 27.12.2017
 * Time: 13:21
 */

namespace frontend\modules\car\models;

use frontend\modules\employee\models\worker\WorkerShift;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;


class CarShiftReportSearch extends WorkerShift
{
    /**
     * @var string
     */
    public $formName;
    /**
     * @var int
     */
    public $workerId;

    /**
     * @var int
     */
    public $carId;

    /**
     * @var int
     */
    public $tenantId;

    /**
     * @var array
     */
    public $positions;

    /**
     * @var string
     */
    public $filter = 'today';

    /**
     * @var string
     */
    public $firstDate;

    /**
     * @var string
     */
    public $secondDate;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['carId','workerId', 'tenantId'], 'integer'],
            [['filter', 'firstDate', 'secondDate', 'positions'], 'safe'],
        ];
    }

    /**
     * Search
     *
     * @param array $params
     * @param bool  $active
     *
     * @return ActiveDataProvider
     */
    public function search($params, $active)
    {
        $languagePrefix = getLanguagePrefix();

        $this->load($params, $this->formName);

        $tenantId = $this->tenantId;

        list($firstDate, $secondDate) = $this->getSearchPeriod();

        /** @var $query ActiveQuery */
        $query = parent::find()->alias('t');

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'sort'       => [
                'defaultOrder' => [
                    'start_work' => SORT_DESC,
                ],
            ],
            'pagination' => false,
        ]);

        $query->select([
            't.id',
            't.worker_id',
            't.position_id',
            't.tariff_id',
            't.car_id',
            't.start_work',
            't.end_work',
            't.pause_data',
            't.worker_late_time',
            't.worker_late_count',
            't.completed_order_count',
            't.rejected_order_count',
            't.accepted_order_offer_count',
            't.rejected_order_offer_count',
            't.order_payment_time',
            't.order_payment_count',
            't.group_id',
            't.group_priority',
            't.position_priority',
            't.city_id',
        ]);

        $query->with([
            'position'                     => function ($subQuery) {
                $subQuery->select(['position_id', 'name']);
            },
            'tariff'                       => function ($subQuery) {
                $subQuery->select(['tariff_id', 'name']);
            },
            'car'                          => function ($subQuery) {
                $subQuery->select(['car_id', 'brand', 'model', 'gos_number']);
            },
            'carMileage'                   => function ($subQuery) {
                $subQuery->select(['shift_id', 'begin_value', 'end_value']);
            },
            'group'                        => function ($subQuery) {
                $subQuery->select(['group_id', 'name']);
            },
            'blocks'                       => function ($subQuery) {
                $subQuery->select([
                    'block_id',
                    'shift_id',
                    'worker_id',
                    'start_block',
                    'end_block',
                    'type_block',
                    'is_unblocked',
                ]);
            },
            'shiftOrders'                  => function ($subQuery) {
                $subQuery->select(['id', 'order_id', 'shift_id']);
            },
            'shiftOrders.order'            => function ($subQuery) {
                $subQuery->select(['order_id', 'payment', 'status_id', 'currency_id']);
            },
            'shiftOrders.order.detailCost' => function ($subQuery) {
                $subQuery->select(['detail_id', 'order_id', 'summary_cost', 'surcharge', 'tax', 'commission']);
            },
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0 = 1');
            return $dataProvider;
        }

        $query->andFilterWhere(['t.position_id' => $this->positions]);

        $query->joinWith([
            'city c'          => function ($subQuery) use ($languagePrefix) {
                $subQuery->select(['city_id', 'republic_id', 'name' => 'name' . $languagePrefix]);
            },
            'city.republic r' => function ($subQuery) {
                $subQuery->select(['republic_id', 'timezone']);
            },
        ]);

        $query->joinWith([
            'worker w' => function ($subQuery) use ($tenantId) {
                //$subQuery->andWhere(['w.tenant_id' => $tenantId]);
            },
        ], false);




            $query->andWhere([
                'and',
                ['t.car_id' => $this->carId],
                ['between', '(t.start_work + IFNULL(r.timezone, 0)*3600)', $firstDate, $secondDate],
                [$active ? 'is' : 'is not', 't.end_work', null],
            ]);



        return $dataProvider;
    }

    /**
     * Getting search period
     * @return array
     */
    private function getSearchPeriod()
    {

        switch ($this->filter) {
            case 'today':
                return [mktime(0, 0, 0), mktime(23, 59, 59)];
            case 'month':
                return [
                    mktime(0, 0, 0, date('n'), 1),
                    mktime(23, 59, 59),
                ];
            case 'period':
                return [
                    strtotime($this->firstDate . ' 00:00:00'),
                    strtotime($this->secondDate . ' 23:59:59'),
                ];
            default:
                return [];
        }
    }

}