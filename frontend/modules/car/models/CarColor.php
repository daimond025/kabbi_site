<?php

namespace frontend\modules\car\models;

use common\helpers\CacheHelper;
use common\modules\car\models\CarColor as CarColorBase;
use Yii;
use yii\helpers\ArrayHelper;
use yii\caching\TagDependency;

/**
 * This is the model class for table "{{%car_color}}".
 *
 * @property integer $color_id
 * @property string $name
 *
 * @property Car[] $cars
 */
class CarColor extends CarColorBase
{
    const CACHE_KEY = 'gootax_car_color';

    public function afterFind()
    {
        parent::afterFind();
        $this->name = t('car', $this->name);
    }

    /**
     * For getting car color map
     * @return array [color_id => name]
     */
    public static function getColorMap()
    {
        return CacheHelper::getFromCache(self::CACHE_KEY . app()->language, function () {
            return ArrayHelper::map(self::find()->all(), 'color_id', 'name');
        }, 0, new TagDependency(['tags' => self::CACHE_KEY]));
    }

    public static function getColorText($color_id)
    {
        $color_map = self::getColorMap();
        return getValue($color_map[$color_id],$color_id);
    }

}
