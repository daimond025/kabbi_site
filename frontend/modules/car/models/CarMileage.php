<?php

namespace frontend\modules\car\models;

use common\modules\car\models\Car;
use common\modules\employee\models\worker\WorkerShift;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%car_mileage}}".
 *
 * @property integer     $id
 * @property integer     $car_id
 * @property integer     $shift_id
 * @property double      $begin_value
 * @property double      $end_value
 * @property integer     $created_at
 * @property integer     $updated_at
 *
 * @property WorkerShift $shift
 */
class CarMileage extends \yii\db\ActiveRecord
{
    const MILEAGE_MAX_VALUE = 9999999999;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%car_mileage}}';
    }

    /**
     * @inherited
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            TimestampBehavior::className(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car_id', 'shift_id', 'begin_value'], 'required'],
            [['car_id', 'shift_id', 'created_at', 'updated_at'], 'integer'],
            [['begin_value', 'end_value'], 'number', 'max' => self::MILEAGE_MAX_VALUE],
            [['shift_id'], 'unique'],
            [
                ['shift_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => WorkerShift::className(),
                'targetAttribute' => ['shift_id' => 'id'],
            ],
            ['end_value', 'compare', 'compareAttribute' => 'begin_value', 'operator' => '>=', 'type' => 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'car_id'      => 'Car ID',
            'shift_id'    => 'Shift ID',
            'begin_value' => 'Begin Value',
            'end_value'   => 'End Value',
            'created_at'  => 'Created At',
            'updated_at'  => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShift()
    {
        return $this->hasOne(WorkerShift::className(), ['id' => 'shift_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::className(), ['car_id' => 'car_id']);
    }

}
