<?php

namespace frontend\modules\car\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * Class CarMileageSearch
 * @package frontend\modules\car\models
 */
class CarMileageSearch extends Model
{
    /**
     * @var string
     */
    public $formName;

    /**
     * @var int
     */
    public $carId;

    /**
     * @var int
     */
    public $tenantId;

    /**
     * @var string
     */
    public $filter = 'today';

    /**
     * @var string
     */
    public $firstDate;

    /**
     * @var string
     */
    public $secondDate;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['carId', 'tenantId'], 'integer'],
            [['filter', 'firstDate', 'secondDate'], 'safe'],
        ];
    }

    /**
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $languagePrefix = getLanguagePrefix();

        $this->load($params, $this->formName);

        list($firstDate, $secondDate) = $this->getSearchPeriod();

        /* @var $query ActiveQuery */
        $query = CarMileage::find()->alias('t');

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'sort'       => [
                'defaultOrder' => [
                    'shift_id' => SORT_DESC,
                ],
            ],
            'pagination' => false,
        ]);

        $query->select(['t.shift_id', 't.begin_value', 't.end_value'])
            ->innerJoinWith([
                'shift s' => function ($subQuery) use ($languagePrefix) {
                    /* @var $subQuery ActiveQuery */
                    $subQuery->select([
                        's.id',
                        's.worker_id',
                        's.start_work',
                        's.end_work',
                        's.pause_data',
                        's.city_id',
                    ])
                        ->with([
                            'worker' => function ($subQuery) {
                                $subQuery->select([
                                    'worker_id',
                                    'callsign',
                                    'name',
                                    'last_name',
                                    'second_name',
                                ]);
                            },
                        ])
                        ->innerJoinWith([
                            'city c'          => function ($subQuery) use ($languagePrefix) {
                                $subQuery->select(['c.city_id', 'c.republic_id', 'name' => 'c.name' . $languagePrefix]);
                            },
                            'city.republic r' => function ($subQuery) {
                                $subQuery->select(['r.republic_id', 'r.timezone']);
                            },
                        ]);
                },
                'car cr'  => function ($subQuery) {
                    /* @var $subQuery ActiveQuery */
                    $subQuery->select(['car_id', 'tenant_id']);
                },
            ]);

        if (!$this->validate()) {
            $query->where('0 = 1');

            return $dataProvider;
        }

        $query->andWhere([
            'and',
            ['cr.car_id' => $this->carId],
            ['cr.tenant_id' => $this->tenantId],
            ['between', '(s.start_work + IFNULL(r.timezone, 0)*3600)', $firstDate, $secondDate],
            ['is not', 's.end_work', null],
        ]);

        return $dataProvider;
    }

    /**
     * Getting search period
     * @return array
     */
    private function getSearchPeriod()
    {
        switch ($this->filter) {
            case 'today':
                return [mktime(0, 0, 0), mktime(23, 59, 59)];
            case 'month':
                return [
                    mktime(0, 0, 0, date('n'), 1),
                    mktime(23, 59, 59),
                ];
            case 'period':
                return [
                    strtotime($this->firstDate . ' 00:00:00'),
                    strtotime($this->secondDate . ' 23:59:59'),
                ];
            default:
                return [];
        }
    }

}