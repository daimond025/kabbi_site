<?php

namespace frontend\modules\car\models;

use app\modules\tenant\models\User;
use common\modules\city\models\City;
use common\modules\employee\models\worker\WorkerHasCar;
use frontend\modules\companies\components\repositories\TenantCompanyRepository;
use frontend\modules\companies\components\services\TenantCompanyService;
use frontend\modules\companies\models\TenantCompany;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * CarSearch represents the model behind the search form about `frontend\modules\car\models\Car`.
 */
class CarSearch extends Car
{
    public $input_search;
    public $view;
    public $sort;
    public $sortBy = ['asc' => SORT_ASC, 'desc' => SORT_DESC];
    public $sort_type;
    public $orderBy = 'name';
    public $entity_id;
    public $position_id;
    public $driver_car = 1;
    public $with_groups = false;
    public $tenantCompanyIds;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'sort_type',
                    'sort',
                    'view',
                    'city_id',
                    'input_search',
                    'class_id',
                    'entity_id',
                    'driver_car',
                    'position_id',
                ],
                'safe',
            ],
            [
                ['tenantCompanyIds'],
                'each',
                'rule' => [
                    'match',
                    'pattern' => sprintf('/^\d+|%s$/', TenantCompany::FIELD_NAME_MAIN_COMPANY)
                ]
            ],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params Filter data
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        //Заполнение модели данными из фильтра
        $this->load($params, '');
        $query = self::find()->alias('c');

        $arWhereByType = [
            'index'   => ['active' => 1],
            'blocked' => ['active' => 0],
        ];
        $query->where(['c.tenant_id' => user()->tenant_id]);

        $query->andFilterWhere(['c.city_id' => $this->city_id])
            ->andFilterWhere(['c.class_id' => $this->class_id])
            ->andFilterWhere($arWhereByType[$this->view]);

        //Текстовый фильтр
        $query->andFilterWhere([
                'or',
                ['like', 'c.name', $this->input_search],
                ['like', 'c.gos_number', $this->input_search],
            ]
        );

        $query->joinWith([
            'city' => function ($query) {
                $query->where([City::tableName() . '.city_id' => ArrayHelper::getColumn(user()->cities, 'city_id')]);
            },
        ], false);

        //Фильтр по водителю
        if (!empty($this->entity_id)) {
            if ($this->driver_car) {
                $query->joinWith([
                    'workerHasCars whc' => function ($sub_query) {
                        $sub_query->andFilterWhere(['whc.has_position_id' => $this->entity_id]);
                    },
                ]);
            } else {
                $arCars = ArrayHelper::getColumn(WorkerHasCar::find()
                    ->asArray()
                    ->where(['has_position_id' => $this->entity_id])
                    ->select('car_id')
                    ->all(), 'car_id');
                if ($arCars) {
                    $query->andFilterWhere(['NOT IN', 'c.car_id', $arCars]);
                }
            }

            //@todo Доделать, когда сделаем привязку авто к модели
//            if ($this->position_id) {
//                /** @var PositionService $positionService */
//                $positionService = Yii::$app->getModule('employee')->get('position');
//                $transportType = $positionService->getPositionTransportType($this->position_id);
//
//                if ($transportType) {
//                    $query->joinWith([
//                        'carModel.type t' => function ($q) use ($transportType) {
//                            $q->where(['t.type_id' => $transportType]);
//                        },
//                    ], false);
//                }
//            }
        }

        if ($this->with_groups) {
            $query->with(['groupCanViewClass', 'groupHasDriverTariff']);
        }

        //Сортировка
        if ($this->sort_type == 'name') {
            $this->orderBy = ['name' => $this->sortBy[$this->sort]];
        } elseif ($this->sort_type == 'raiting') {
            $this->orderBy = ['raiting' => $this->sortBy[$this->sort]];
        } elseif ($this->sort_type == 'class') {
            $this->orderBy = [];
            $query->joinWith([
                'class cls' => function ($sub_query) {
                    $sub_query->orderBy(['cls.class' => $this->sortBy[$this->sort]]);
                },
            ]);
        }

        $query->orderBy($this->orderBy);


        (new TenantCompanyService())
            ->queryChange($query, $this->tenantCompanyIds, 'c.tenant_company_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}