<?php

use common\modules\car\models\transport\TransportField;
use common\modules\car\models\transport\TransportTypeFieldValue;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var array $fieldMap */
/* @var $fieldModels TransportTypeFieldValue[] */
/* @var $this yii\web\View */
/* @var $form ActiveForm */

$form = $form ? $form : ActiveForm::begin([
    'id'              => 'car-form',
    'errorCssClass'   => 'input_error',
    'successCssClass' => 'input_success',
]);
if (!empty($fieldMap)):
    foreach ($fieldMap as $id => $item):
        $fieldModel = $fieldModels[$id];
        $fieldName = TransportTypeFieldValue::getFieldNameByType($item['type']);
        $modelFormName = $fieldModel->formName();
        $config = [
            'id'    => strtolower($modelFormName) . '_' . $id . '-' . $fieldName,
            'value' => $fieldModel->$fieldName,
            'name'  => $modelFormName . "[$id][$fieldName]",
        ];
        $field = $form->field($fieldModel, $fieldName, [
            'template'     => "{input}",
            'errorOptions' => ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red'],
        ]);
        ?>
        <section class="row">
            <div class="row_label"><?= Html::label(Html::encode($item['name'])) ?></div>
            <div class="row_input">
                <? if ($item['type'] === TransportField::TEXT_TYPE): ?>
                    <?= $field->textarea($config) ?>
                <? elseif ($item['type'] === TransportField::ENUM_TYPE): ?>
                    <?= $field->dropDownList($item['enum'],
                        array_merge($config, ['class' => 'default_select', 'prompt' => t('app', 'Not selected')])) ?>
                <? else: ?>
                    <?= $field->textInput($config) ?>
                <? endif ?>
            </div>
        </section>
        <?= Html::hiddenInput($modelFormName . "[$id][has_field_id]", $fieldModel->has_field_id) ?>
    <? endforeach; ?>
<? endif; ?>
