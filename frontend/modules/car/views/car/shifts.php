<?php

use frontend\modules\reports\models\WorkerReportSearch;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $positions array */
/* @var $showFilter bool */
/* @var $timeOffset int */
/* @var $searchModel WorkerReportSearch */
/* @var $dataProviderActiveShifts ActiveDataProvider */
/* @var $dataProviderShifts ActiveDataProvider */

//Модальное окно смены
$this->registerJs("$('a.driver_history').colorbox({inline: true, width: \"300px\"});");
?>

<? if ($showFilter): ?>
    <?=
    $this->render('_shift_filter', [
        'action'      => \yii\helpers\Url::to(['car-show-shifts', 'id' => get('id')]),
        'form_name'   => 'shift_search',
        'positions'   => $positions,
        'searchModel' => $searchModel,
    ])
    ?>
<? endif; ?>

<div id="shift_stat">
    <?= $dataProviderActiveShifts->totalCount
        ? $this->render('_shift_table', [
            'dataProvider' => $dataProviderActiveShifts,
            'timeOffset'   => $timeOffset,
            'active'       => true,
        ]) : '' ?>

    <?= $this->render('_shift_table', [
        'dataProvider' => $dataProviderShifts,
        'timeOffset'   => $timeOffset,
        'active'       => false,
    ]) ?>
</div>
