<?php

use common\helpers\DateTimeHelper;
use yii\helpers\Html;
use frontend\modules\car\models\CarColor;
use yii\helpers\ArrayHelper;
use app\modules\tenant\models\field\FieldRecord;

/* @var $this yii\web\View */
/* @var $model \frontend\modules\car\models\Car
/* @var $form yii\widgets\ActiveForm */

\frontend\modules\car\assets\CarAsset::register($this);
?>
<div class="non-editable">
    <section class="main_tabs">
        <div class="non_editable">
            <? if (ArrayHelper::getValue($showFieldList, 'car.' . FieldRecord::NAME_CAR_CALLSIGN, true)) : ?>
            <section class="row">
                <div class="row_label"><?= Html::activeLabel($model, 'callsign') ?></div>
                <div class="row_input">
                    <?= Html::encode($model->callsign); ?>
                </div>
            </section>
            <? endif; ?>
            <section class="row">
                <div class="row_label"><?= Html::activeLabel($model, 'city_id') ?></div>
                <div class="row_input">
                    <?= Html::encode(!empty($data['USER_CITY_LIST']) ? current($data['USER_CITY_LIST']) : NULL); ?>
                </div>
            </section>
            <section class="row" id="logo">
                <div class="row_label"></div>
                <div class="row_input" style="background: none;">
                    <div class="input_file_logo <!--file_loader-->">
                        <? if (file_exists(Yii::getAlias('@app') . '/web/' . '/upload/' . user()->tenant_id. '/' . $model->photo || empty($model->photo))):?>
                        <a class="input_file_logo logo cboxElement" href="<?= '/upload/' . user()->tenant_id . '/' . $model->photo ?>" style="border: none">
                            <div class="row_input" style="background: none"><span><img src="<?= '/upload/' . user()->tenant_id . '/' . $model->photo ?>"></span></div>
                        </a>
                        <? endif ?>
                    </div>
                </div>
            </section>
            <section class="row">
                <div class="row_label"><?= Html::activeLabel($model, 'owner') ?></div>
                <div class="row_input">
                    <?$arOwnerCarVariants = $model->getVariantsOfOwnerCar()?>

                    <div class="row_input"><?= Html::encode(!empty($model->owner) ? $arOwnerCarVariants[$model->owner] : NULL); ?></div>
                </div>
            </section>
            <section class="row">
                <div class="row_label"><label><?= t('car', 'Car') ?></label></div>
                <div class="row_input" data-url="<?= \yii\helpers\Url::to('/car/car/get-model-list') ?>">
                    <div class="grid_2">
                        <div class="grid_item">
                            <div class="row_input"><?= !empty($model->brand_id) ? Html::encode($data['BRAND_LIST'][$model->brand_id]) : NULL ?></div>
                        </div>
                        <div class="grid_item">
                            <div class="row_input"><?= !empty($model->model_id) ? Html::encode($data['MODEL_LIST'][$model->model_id]) : NULL ?></div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="row">
                <div class="row_label"><?= Html::activeLabel($model, 'class_id') ?></div>
                <div class="row_input">
                    <div class="row_input"><?= !empty($model->class_id) ? Html::encode(t('car', $data['CAR_TYPES'][$model->class_id])) : NULL ?></div>
                </div>
            </section>
            <section class="row">
                <div class="row_label"><?= Html::activeLabel($model, 'color') ?></div>
                <div class="row_input">
                    <?$arCarColors = CarColor::getColorMap()?>
                    <div class="row_input"><?= Html::encode(!empty($model->color) ? $arCarColors[$model->color] : current($arCarColors)); ?></div>
                </div>
            </section>
            <section class="row">
                <div class="row_label"><?= Html::activeLabel($model, 'gos_number') ?></div>
                <div class="row_input">
                    <div class="input_with_text">
                        <div class="row_input"><?= Html::encode($model->gos_number); ?></div>
                    </div>
                </div>
            </section>
            <section class="row">
                <div class="row_label"><?= Html::activeLabel($model, 'year') ?></div>
                <div class="row_input">
                    <? $arYear = DateTimeHelper::getOldYearList(0, 24); ?>
                    <div class="row_input"><?= !empty($model->year) ? $arYear[$model->year] : t('app', 'Not selected') ?></div>
                </div>
            </section>
            <? if (ArrayHelper::getValue($showFieldList, 'car.' . FieldRecord::NAME_CAR_LICENSE, true)) : ?>
            <h2><?= t('car', 'License to work in a taxi') ?></h2>
            <section class="row">
                <div class="row_label"><?= Html::activeLabel($model, 'license') ?></div>
                <div class="row_input">
                    <div class="row_input"><?= !empty($model->license) ? Html::encode($model->license) : '-' ?></div>
                </div>
            </section>
            <? endif; ?>

            <?if (!empty($data['TENANT_CAR_OPTION'])):?>
                <h2><?t('car', 'Options')?></h2>
                <ul class="auto_options">
                    <?foreach($data['TENANT_CAR_OPTION'] as $option_id => $value):
                        $checked = in_array($option_id, $model->options) ? 'checked="checked"' : null;
                        $option_name = $data['CAR_OPTION_LIST'][$option_id];
                    ?>
                        <li><span>+<?= Html::encode($value); ?></span><label><input data-raiting="<?= Html::encode($value); ?>" <?= $checked ?> value="<?= $option_id ?>" name="Car[options][]" type="checkbox" disabled="disabled"/> <?= Html::encode(t('car-options', $option_name)); ?></label></li>
                    <?endforeach;?>
                    <li><span><div c="row_input"><?= !$model->raiting ? 0 : Html::encode($model->raiting); ?></div></span><b class="auto_options_res"><?= t('car', 'Total raiting') ?></b></li>
                </ul>
            <?endif?>
            <?= $this->render('_dynamicFieldsRead', ['fieldMap' => $fieldMap, 'fieldModels' => $fieldModels]) ?>
        </div>
    </section>
</div>
<div>
    <label><?= Html::activeCheckbox($model, 'block', ['label' => null, 'disabled' => 'disabled']) ?> <b><?= t('car', 'Block') ?></b></label>
</div>
