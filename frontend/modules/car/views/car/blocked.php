<?php
/* @var $this yii\web\View */
/* @var tenantCompanies array */

?>
<?if(!empty($cars)):?>
    <?=$this->render('_filter', [
        'city_list' => $city_list,
        'car_type_list' => $car_type_list,
        'tenantCompanies' => $tenantCompanies
    ])?>
    <table class="people_table sort">
        <?=$this->render('_thead') ;?>
        <?=$this->render('_blocked', ['cars' => $cars]) ;?>
    </table>
<?else:?>
    <p><?=t('app', 'Empty')?></p>
<?endif?>