<?php

/* @var $city_list array */
/* @var $car_type_list array */

/* @var $tenantCompanies array */

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
\frontend\modules\car\assets\CarAsset::register($this);
$this->title = Yii::t('car', 'Cars');
?>
<?php if (app()->user->can('cars')) echo Html::a(t('app', 'Add'), ['create'],
    ['class' => 'button', 'style' => 'float: right;']) ?>
<h1><?= Html::encode($this->title) ?></h1>
<section class="main_tabs">
    <div class="tabs_links">
        <ul>
            <li><a href="#active" class="t01"><?= t('car', 'Active') ?></a></li>
            <li><a href="#blocked" class="t02" data-href="<?= Url::to('show-blocked') ?>"><?= t('car', 'Blocked') ?></a>
            </li>
        </ul>
    </div>
    <div class="tabs_content">
        <div id="t01" data-view="index">
            <? if (!empty($cars)): ?>
                <?= $this->render('_filter', [
                    'city_list'       => $city_list,
                    'car_type_list'   => $car_type_list,
                    'tenantCompanies' => $tenantCompanies,
                ]) ?>
                <table class="people_table sort">
                    <?= $this->render('_thead'); ?>
                    <?= $this->render('_index', ['cars' => $cars]); ?>
                </table>
            <? else: ?>
                <p><?= t('app', 'Empty') ?></p>
            <? endif ?>
        </div>
        <div id="t02" data-view="blocked">
        </div>
    </div>
</section>