<?php

/* @var $this yii\web\View */

/* @var $city_list array*/
/* @var $car_type_list array*/
/* @var $tenantCompanies array*/

use yii\helpers\Url;
use yii\helpers\Html;
use app\modules\tenant\models\User;

?>
<form name="user_search" method="post" onsubmit="return false" action="<?=Url::to('/car/car/search/')?>">
    <div class="grid_3 <? if (!app()->user->can(User::ROLE_STAFF_COMPANY)): ?>car_grid<? endif; ?>" data-activity="active">
        <div class="grid_item">
            <div class="select_checkbox">
                <a class="a_sc select " rel="<?=t('user', 'All cities')?>"><?=t('user', 'All cities')?></a>
                <div class="b_sc">
                    <ul>
                        <?foreach ($city_list as $city_id => $city):?>
                            <li><label><input name="city_id[]" type="checkbox" value="<?=$city_id?>"/> <?= Html::encode($city); ?></label></li>
                        <?endforeach;?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="grid_item">
            <div class="select_checkbox">
                <a class="a_sc select " rel="<?= t('car', 'All car types')?>"><?= t('car', 'All car types')?></a>
                <div class="b_sc">
                    <ul>
                        <?foreach ($car_type_list as $class_id => $class_name):?>
                            <li><label><input name="class_id[]" type="checkbox" value="<?=$class_id?>"/> <?= Html::encode($class_name); ?></label></li>
                        <?endforeach;?>
                    </ul>
                </div>
            </div>
        </div>

        <? if (!app()->user->can(User::ROLE_STAFF_COMPANY)): ?>
            <div class="grid_item">
                <div class="select_checkbox">
                    <a class="a_sc select " rel="<?= t('tenant_company', 'All companies')?>"><?= t('tenant_company', 'All companies')?></a>
                    <div class="b_sc">
                        <ul>
                            <?foreach ($tenantCompanies as $company_id => $company_name):?>
                                <li>
                                    <label>
                                        <input name="tenantCompanyIds[]" type="checkbox" value="<?=$company_id?>"/> <?= Html::encode($company_name); ?>
                                    </label>
                                </li>
                            <?endforeach;?>
                        </ul>
                    </div>
                </div>
            </div>
        <? endif; ?>

        <div class="grid_item">
            <input name="input_search" type="text" placeholder="<?=t('car', 'Brand Model Number')?>" value=""/>
        </div>
    </div>
</form>
