<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
?>

<?foreach ($cars as $car):?>
    <tr>
        <td class="pt_fio">
            <a href="<?=\yii\helpers\Url::to(['update', 'id' => $car['ID']])?>">
                <span class="pt_photo"><?= $car['PHOTO']; ?></span>
                <span class="pt_fios"><?= Html::encode($car['NAME']); ?></span>
            </a>
        </td>
        <td><span dir="auto"><?= Html::encode($car['GOV_NUMBER']); ?></span></td>
        <td class="pt_job"><?=t('car',$car['CLASS'])?></td>
        <td><?= Html::encode($car['RAITING']); ?></td>
    </tr>
<?endforeach;?>  