<?php
/* @var $this yii\web\View */
/* @var $model \frontend\modules\car\models\Car */
/* @var array $fieldMap */
/* @var array $fieldModels */
/* @var array $showFieldList */

/* @var array $data */

use frontend\modules\car\assets\CarAsset;
use yii\helpers\Html;
use yii\helpers\Url;

$reportAsset = CarAsset::register($this);
$this->registerJsFile($reportAsset->baseUrl . '/cars.js');

$this->title = $model->name;
?>
<div class="bread"><a href="<?= Url::toRoute('/car/car/list') ?>"><?= t('app', 'Cars') ?></a></div>
<h1><?= Html::encode($this->title) ?></h1>
<input type="hidden" id="userRights" value="<?= app()->user->can('organization') ?>">

<section class="main_tabs">
    <div class="tabs_links">
        <ul>
            <li><a href="#car" class="t01"><?= t('car', 'Profile') ?></a></li>
            <li>
                <a href="#car_mileage" class="t02"
                   data-href="<?= Url::to(['/car/car/show-mileage', 'id' => get('id')]) ?>">
                    <?= t('car', 'Car mileage control') ?>
                </a>
            </li>
            <li><a href="#shifts" class="t03"
                   data-href="<?= Url::to(['/car/car/car-show-shifts', 'id' => get('id')]) ?>"><?= t('reports',
                        'Shifts') ?></a>
            </li>
        </ul>
    </div>
    <div class="tabs_content">
        <div id="t01">
            <?php $form = !app()->user->can('cars') ? '_form_read' : '_form';
            echo $this->render($form, [
                'model'         => $model,
                'data'          => $data,
                'fieldMap'      => $fieldMap,
                'fieldModels'   => $fieldModels,
                'showFieldList' => $showFieldList,
            ]); ?>
        </div>
        <div id="t02"></div>
        <div id="t03" class="js-shifts-tab"></div>

    </div>
</section>

