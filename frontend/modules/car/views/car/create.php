<?php
/* @var $this yii\web\View */
/* @var $model \frontend\modules\car\models\Car */
/* @var array $fieldMap */
/* @var array $fieldModels */
/* @var array $showFieldList */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('car', 'Add car');
?>
<div class="bread"><a href="<?= Url::toRoute('/car/car/list') ?>"><?= t('app', 'Cars') ?></a></div>
<h1><?= Html::encode($this->title) ?></h1>

<?= $this->render(app()->user->can('cars') ? '_form' : '_form_read',
    compact('model', 'data', 'fieldMap', 'fieldModels', 'showFieldList')) ?>
