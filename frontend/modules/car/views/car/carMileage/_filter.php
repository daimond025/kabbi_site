<?php

use frontend\modules\car\models\CarMileageSearch;

/* @var $this yii\web\View */
/* @var $searchModel CarMileageSearch */
/* @var $action string */
/* @var $formName string */

$this->registerJs('calendarFilterInit();');
?>

<form name="<?= $formName ?>" action="<?= $action ?>" method="post">
    <div class="oor_fil">
        <div class="cof_left" style="margin-right: 0;">
            <div class="cof_first">
                <label>
                    <input <?= $searchModel->filter === 'today' ? 'checked="checked"' : '' ?>
                            name="filter" type="radio" value="today"> <?= t('reports', 'Today') ?>
                </label>
            </div>

            <div class="cof_first">
                <label>
                    <input name="filter" <?= $searchModel->filter === 'month' ? 'checked="checked"' : '' ?>
                           type="radio" value="month"> <?= app()->formatter->asDate(time(), 'LLLL') ?>
                </label>
            </div>

            <div class="cof_second disabled_cof">
                <label>
                    <input name="filter"
                        <?= $searchModel->filter === 'period' ? 'checked="checked"' : '' ?>
                           class="cof_time_choser" type="radio" value="period"><?= t('reports', 'Period') ?>
                </label>
                <div class="cof_date notranslate disabled_select">
                    <a class="s_datepicker a_sc select" id="first"><?= date("d.m.Y",
                            mktime(0, 0, 0, date("m"), date("d") - 14, date("Y"))); ?></a>
                    <div class="s_datepicker_content b_sc" style="display: none;">
                        <input name="first_date" class="sdp_input" type="text"
                               value="<?= date("d.m.Y", mktime(0, 0, 0, date("m"), date("d") - 14, date("Y"))); ?>">
                        <div class="sdp first_date"></div>
                    </div>
                </div>
                <hr>
                <div class="cof_date notranslate disabled_select">
                    <a class="s_datepicker a_sc select"><?= date("d.m.Y"); ?></a>
                    <div class="s_datepicker_content b_sc" style="display: none;">
                        <input name="second_date" class="sdp_input" type="text" value="<?= date("d.m.Y"); ?>">
                        <div class="sdp"></div>
                    </div>
                </div>
                <a style="margin-left: 15px;" class="button oor_compare" href=""><?= t('reports', 'Show') ?></a>
            </div>
        </div>
    </div>
</form>
