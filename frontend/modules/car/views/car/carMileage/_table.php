<?php

use common\components\formatter\Formatter;
use common\helpers\DateTimeHelper;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider ActiveDataProvider */

//Модальное окно смены
$this->registerJs("$('a.driver_history').colorbox({inline: true, width: \"300px\"});");

echo Html::tag('h2', t('reports', 'Finished shifts'));

/* @var $formatter Formatter */
$formatter = app()->formatter;

$shiftTime = $pauseTime = 0;
$shiftEvents = [];

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'       => '{items}',
    'tableOptions' => ['class' => 'people_table'],
    'beforeRow'    => function ($model) use (&$shiftEvents, &$shiftTime, &$pauseTime) {
        $shiftTime = $pauseTime = 0;
        $shiftEvents = [];

        if (!empty($model->shift)) {
            $shiftTime = $model->shift->end_work - $model->shift->start_work;
            $pauses = $model->shift->pause_data;
            if (is_array($pauses)) {
                foreach ($pauses as $pause) {
                    $label = isset($pause['pause_reason']) ? $pause['pause_reason'] : '';

                    $pauseStart = (isset($pause['pause_start']) ? $pause['pause_start'] : $model->shift->start_work);
                    $pauseEnd = (isset($pause['pause_end']) ? $pause['pause_end'] : $model->shift->end_work);

                    $shiftEvents[] = [
                        'type'       => 'pause',
                        'startTime'  => $pauseStart,
                        'finishTime' => $pauseEnd,
                        'label'      => $label,
                    ];

                    $pauseTime += $pauseEnd - $pauseStart;
                }
            }
        }
    },
    'afterRow'     => function ($model) use (&$shiftEvents, &$shiftTime, &$pauseTime) {
        $timeOffset = empty($model->shift->city->republic->timezone) ? 0 : $model->shift->city->republic->timezone * 3600;

        if (!empty($model->shift)) {
            return $this->render('_info', [
                'id'         => $model->shift->id,
                'startWork'  => $model->shift->start_work,
                'endWork'    => $model->shift->end_work,
                'shiftTime'  => $shiftTime,
                'pauseTime'  => $pauseTime,
                'timeOffset' => $timeOffset,
                'events'     => $shiftEvents,
            ]);
        }
    },
    'columns'      => [
        [
            'label'          => t('reports', 'Date'),
            'contentOptions' => function () {
                return ['style' => 'font-size: 14px'];
            },
            'content'        => function ($model) use ($formatter) {
                $timeZone = empty($model->shift->city->republic->timezone) ? 0 : $model->shift->city->republic->timezone;
                $startWork = empty($model->shift->start_work) ? null : $model->shift->start_work + $timeZone * 3600;
                $endWork = empty($model->shift->end_work) ? null : $model->shift->end_work + $timeZone * 3600;

                $lines = [];
                $lines[] = $startWork === null ? ''
                    : implode(', ', [
                        $formatter->asDate($startWork, 'shortDate'),
                        $formatter->asTime($startWork, 'short'),
                    ]);
                $lines[] = $endWork === null ? ''
                    : implode(', ', [
                        $formatter->asDate($endWork, 'shortDate'),
                        $formatter->asTime($endWork, 'short'),
                    ]);

                $lines[] = empty($model->shift->city->name) ? '' : Html::encode($model->shift->city->name)
                    . ' (' . ($timeZone > 0 ? '+' : '') . $timeZone . ')';

                return implode('<br>', $lines);
            },
            'headerOptions'  => ['style' => 'width: 20%'],
        ],
        [
            'label'         => t('reports', 'Worker'),
            'content'       => function ($model) {
                $lines = [];
                $lines[] = empty($model->shift->worker) ? '' : Html::encode($model->shift->worker->callsign);
                $lines[] = empty($model->shift->worker) ? ''
                    : Html::encode(implode(' ', array_filter([
                        $model->shift->worker->last_name,
                        $model->shift->worker->name,
                        $model->shift->worker->second_name,
                    ])));

                return implode('<br>', $lines);
            },
            'headerOptions' => ['style' => 'width: 20%'],
        ],
        [
            'label'         => t('reports', 'Shift'),
            'content'       => function ($model) use (&$shiftTime, &$pauseTime) {
                $shiftId = !empty($model->shift) ? $model->shift->id : null;

                $lines = [];
                $lines[] = Html::tag('i', '', ['class' => 'icon_play'])
                    . ' ' . Html::tag('span',
                        Html::a(DateTimeHelper::getFormatTimeFromSeconds($shiftTime),
                            '#dr_hist_' . $shiftId, ['class' => 'driver_history']));
                $lines[] = Html::tag('i', '', ['class' => 'icon_pause'])
                    . ' ' . Html::tag('span',
                        Html::a(DateTimeHelper::getFormatTimeFromSeconds($pauseTime),
                            '#dr_hist_' . $shiftId, ['class' => 'driver_history']));

                return implode('<br>', $lines);
            },
            'headerOptions' => ['style' => 'width: 14%'],
        ],
        [
            'label'         => t('reports', 'Begin car mileage'),
            'content'       => function ($model) use ($formatter) {
                return Html::tag('span', $model->begin_value === null ? ''
                    : $formatter->asDecimal($model->begin_value, 1) . ' ' . t('app', 'km'));
            },
            'headerOptions' => ['style' => 'width: 12%'],
        ],
        [
            'label'         => t('reports', 'End car mileage'),
            'content'       => function ($model) use ($formatter) {
                return Html::tag('span', $model->end_value === null ? ''
                    : $formatter->asDecimal($model->end_value, 1) . ' ' . t('app', 'km'));
            },
            'headerOptions' => ['style' => 'width: 12%'],
        ],
        [
            'label'         => t('reports', 'Shift car mileage'),
            'content'       => function ($model) use ($formatter) {
                $carMileage = (float)(empty($model->end_value) ? $model->begin_value : $model->end_value)
                    - (float)$model->begin_value;

                return Html::tag('span',
                    $formatter->asDecimal($carMileage, 1) . ' ' . t('app', 'km'));
            },
            'headerOptions' => ['style' => 'width: 12%'],
        ],
    ],
]);