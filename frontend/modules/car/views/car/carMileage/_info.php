<?php

use common\helpers\DateTimeHelper;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $id int */
/* @var $startWork int */
/* @var $endWork int */
/* @var $shiftTime int */
/* @var $pauseTime int */
/* @var $timeOffset int */
/* @var $events array */

$formatter = app()->formatter;

?>

<div style="display: none">
    <?php
    $formId = "dr_hist_{$id}";
    $title = t('reports', 'Shift');
    ?>

    <div id="<?= $formId ?>" class="dr_hist">
        <h2><?= $title ?></h2>
        <ul>
            <li>
                <span>
                    <?= implode(',', [
                        $formatter->asDate($startWork + $timeOffset, 'shortDate'),
                        $formatter->asTime($startWork + $timeOffset, 'short'),
                    ]) ?>
                </span>
                <i><?= t('reports', 'Start shift') ?></i>
            </li>

            <?php foreach ($events as $event): ?>
                <li>
                    <span>
                        <?= date('H:i', $event['startTime'] + $timeOffset)
                        . '–' . date('H:i', $event['finishTime'] + $timeOffset) ?>
                    </span>

                    <?php $eventTitle = $event['type'] === 'pause' ? t('reports', 'Pause') . ': ' : ''; ?>

                    <i><?= Html::encode($eventTitle . $event['label']) ?></i>
                </li>
            <?php endforeach ?>

            <li>
                <span>
                    <?= empty($endWork) ? '' : implode(',', [
                        $formatter->asDate($endWork + $timeOffset, 'shortDate'),
                        $formatter->asTime($endWork + $timeOffset, 'short'),
                    ]) ?>
                </span>
                <i><?= t('reports', 'Finish shift') ?></i>
            </li>

            <li>
                <span>
                    <b><?= DateTimeHelper::getFormatTimeFromSeconds($shiftTime) ?></b><br/>
                    <?= Html::tag('b', DateTimeHelper::getFormatTimeFromSeconds($pauseTime)) . Html::tag('br') ?>
                </span>
                <i>
                    <b><?= t('reports', 'Shift') ?></b><br/>
                    <?= Html::tag('b', t('reports', 'Pauses')) . Html::tag('br') ?>
                </i>
            </li>
        </ul>
    </div>
</div>