<?php

use frontend\modules\car\models\CarMileageSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $showFilter bool */
/* @var $searchModel CarMileageSearch */
/* @var $dataProvider ActiveDataProvider */

?>

<?php if ($showFilter): ?>
    <?= $this->render('_filter', [
        'action'      => Url::to(['/car/car/show-mileage', 'id' => get('id')]),
        'formName'    => 'mileage-search',
        'searchModel' => $searchModel,
    ]) ?>
<? endif; ?>

<div id="car-mileage">
    <?= $this->render('_table', compact('dataProvider')); ?>
</div>
