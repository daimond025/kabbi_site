<?php

use common\helpers\DateTimeHelper;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $id int */
/* @var $startWork int */
/* @var $endWork int */
/* @var $shiftTime int */
/* @var $pauseTime int */
/* @var $blockTime int */
/* @var $timeOffset int */
/* @var $events array */

$forms     = ['history', 'block', 'pause'];
$formatter = app()->formatter;

?>

<div style="display: none">
    <? foreach ($forms as $form):
        switch ($form) {
            case 'block':
                $formId = "dr_block_{$id}";
                $title  = t('reports', 'Blocks');
                break;
            case 'pause':
                $formId = "dr_pause_{$id}";
                $title  = t('reports', 'Pauses');
                break;
            default:
                $formId = "dr_hist_{$id}";
                $title  = t('reports', 'Shift');
        } ?>
        <div id="<?= $formId ?>" class="dr_hist">
            <h2><?= $title ?></h2>
            <ul>
                <li>
                    <span>
                        <?= implode(',', [
                            $formatter->asDate($startWork + $timeOffset, 'shortDate'),
                            $formatter->asTime($startWork + $timeOffset, 'short'),
                        ]) ?>
                    </span>
                    <i><?= t('reports', 'Start shift') ?></i>
                </li>

                <? foreach ($events as $event):
                    if ($form == 'history' || $form == $event['type']): ?>
                        <li>
                        <span>
                            <?= date('H:i', $event['startTime'] + $timeOffset)
                            . '–' . date('H:i', $event['finishTime'] + $timeOffset) ?>
                        </span>

                            <? if ($form == 'history' && $event['type'] == 'block') {
                                $eventTitle = t('reports', 'Block') . ': ';
                            } elseif ($form == 'history' && $event['type'] != 'block') {
                                $eventTitle = t('reports', 'Pause') . ': ';
                            } else {
                                $eventTitle = '';
                            } ?>

                            <i><?= Html::encode($eventTitle . $event['label']) ?></i>
                        </li>
                    <? endif ?>
                <? endforeach ?>

                <li>
                    <span>
                        <?= empty($endWork) ? '' : implode(',', [
                            $formatter->asDate($endWork + $timeOffset, 'shortDate'),
                            $formatter->asTime($endWork + $timeOffset, 'short'),
                        ]) ?>
                    </span>
                    <i><?= t('reports', 'Finish shift') ?></i>
                </li>

                <li>
                    <span>
                        <b><?= DateTimeHelper::getFormatTimeFromSeconds($shiftTime) ?></b><br/>
                        <?= ($form != 'block') ? Html::tag('b',
                                DateTimeHelper::getFormatTimeFromSeconds($pauseTime)) . Html::tag('br') : '' ?>
                        <?= ($form != 'pause') ? Html::tag('b',
                            DateTimeHelper::getFormatTimeFromSeconds($blockTime)) : '' ?>
                    </span>
                    <i>
                        <b><?= t('reports', 'Shift') ?></b><br/>
                        <?= ($form != 'block') ? Html::tag('b', t('reports', 'Pauses')) . Html::tag('br') : '' ?>
                        <?= ($form != 'pause') ? Html::tag('b', t('reports', 'Blocks')) : '' ?>
                    </i>
                </li>
            </ul>
        </div>
    <? endforeach ?>
</div>