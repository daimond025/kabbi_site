<?php

use common\modules\car\models\transport\TransportField;
use common\modules\car\models\transport\TransportTypeFieldValue;
use yii\helpers\Html;

/* @var array $fieldMap */
/* @var $fieldModels TransportTypeFieldValue[] */
/* @var $this yii\web\View */
/* @var $form ActiveForm */

if (!empty($fieldMap)):
    foreach ($fieldMap as $id => $item):
        $fieldModel = $fieldModels[$id];
        $fieldName = TransportTypeFieldValue::getFieldNameByType($item['type']);
        ?>
        <section class="row">
            <div class="row_label"><?= Html::label(Html::encode($item['name'])) ?></div>
            <div class="row_input">
               <?if ($item['type'] === TransportField::ENUM_TYPE): ?>
                    <?= $field->dropDownList($item['enum'],
                        array_merge($config, ['class' => 'default_select', 'prompt' => t('app', 'Not selected')])) ?>
                <? else: ?>
                    <?= $fieldModel->$fieldName ?>
                <? endif ?>
            </div>
        </section>
        <?= Html::hiddenInput($modelFormName . "[$id][has_field_id]", $fieldModel->has_field_id) ?>
    <? endforeach; ?>
<? endif; ?>
