<?php

use common\helpers\DateTimeHelper;
use frontend\widgets\file\Logo;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\modules\car\models\CarColor;
use frontend\widgets\file\File;
use app\modules\tenant\models\User;
use yii\helpers\ArrayHelper;
use app\modules\tenant\models\field\FieldRecord;

/* @var $this yii\web\View */
/* @var $model \frontend\modules\car\models\Car */
/* @var $form yii\widgets\ActiveForm */
/* @var array $fieldMap */
/* @var array $fieldModels */
/* @var array $showFieldList */

\frontend\modules\car\assets\CarAsset::register($this);

?>

<?php $form = ActiveForm::begin([
    'id'              => 'car-form',
    'errorCssClass'   => 'input_error',
    'successCssClass' => 'input_success',
    'options'         => ['enctype' => 'multipart/form-data'],
]); ?>
<section class="main_tabs">
    <div class="tabs_content">
        <div class="active" id="t01" style="padding: 0;">
            <!--Фото-->
            <? if (!$model->isNewRecord): ?>
                <?= Logo::widget([
                    'form'         => $form,
                    'model'        => $model,
                    'attribute'    => 'photo',
                    'ajax'         => true,
                    'uploadAction' => Url::to([
                        '/car/car/upload-logo/',
                        'entity_id' => get('id'),
                        'scenario'  => 'photo',
                    ]),
                ]) ?>
            <? endif ?>

            <? if (ArrayHelper::getValue($showFieldList, 'car.' . FieldRecord::NAME_CAR_CALLSIGN, true)) : ?>
                <section class="row">
                    <?= $form->field($model, 'callsign')->begin(); ?>
                    <div class="row_label">
                        <?= Html::activeLabel($model, 'callsign') ?>
                    </div>
                    <div class="row_input">
                        <?= Html::activeTextInput($model, 'callsign') ?>
                        <?= Html::error($model, 'callsign',
                            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
                    </div>
                    <?= $form->field($model, 'callsign')->end(); ?>
                </section>
            <? endif; ?>

            <section class="row">
                <?= $form->field($model, 'city_id')->begin(); ?>
                <div class="row_label"><?= Html::activeLabel($model, 'city_id') ?></div>
                <div class="row_input">
                    <?= Html::activeDropDownList($model, 'city_id', $data['USER_CITY_LIST'], [
                        'data-placeholder' => Html::encode(!empty($model->city_id) ? $data['USER_CITY_LIST'][$model->city_id] : !empty($data['USER_CITY_LIST']) ? current($data['USER_CITY_LIST']) : t('car',
                            'Choose city')),
                        'class'            => 'default_select',
                    ]) ?>
                </div>
                <?= $form->field($model, 'city_id')->end(); ?>
            </section>

            <section class="row">
                <?= $form->field($model, 'owner')->begin(); ?>
                <div class="row_label"><?= Html::activeLabel($model, 'owner') ?></div>
                <div class="row_input">
                    <? $arOwnerCarVariants = $model->getVariantsOfOwnerCar() ?>
                    <?= Html::activeDropDownList($model, 'owner', $arOwnerCarVariants, [
                        'data-placeholder' => Html::encode(!empty($model->owner) ? $arOwnerCarVariants[$model->owner] : current($arOwnerCarVariants)),
                        'class'            => 'default_select',
                    ]) ?>
                </div>
                <?= $form->field($model, 'owner')->end(); ?>
            </section>

            <section class="row">
                <div class="form-group required">
                    <div class="row_label"><label><?= t('car', 'Car') ?></label></div>
                    <div class="row_input" data-url="<?= \yii\helpers\Url::to('/car/car/get-model-list') ?>">
                        <div class="grid_2">
                            <div class="grid_item">
                                <?= $form->field($model, 'brand_id')->begin(); ?>
                                <?= Html::activeDropDownList($model, 'brand_id', $data['BRAND_LIST'], [
                                    'data-placeholder' => Html::encode(!empty($model->brand_id) ? $data['BRAND_LIST'][$model->brand_id] : t('car',
                                        'Brand')),
                                    'data-lang' => Yii::$app->language,
                                    'class'            => 'default_select select_w_filter',
                                ]) ?>
                                <?= $form->field($model, 'brand_id')->end(); ?>
                                <?= Html::activeHiddenInput($model, 'brand', ['id' => 'car_brand']) ?>
                            </div>
                            <div class="grid_item">
                                <?= $form->field($model, 'model_id')->begin(); ?>
                                <?= Html::activeDropDownList($model, 'model_id', $data['MODEL_LIST'], [
                                    'data-placeholder' => Html::encode(!empty($model->model_id) ? $data['MODEL_LIST'][$model->model_id] : t('car',
                                        'Model')),
                                    'data-lang' => Yii::$app->language,
                                    'class'            => 'default_select select_w_filter',
                                ]) ?>
                                <?= $form->field($model, 'model_id')->end(); ?>
                                <?= Html::activeHiddenInput($model, 'model', ['id' => 'car_model']) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="row">
                <?= $form->field($model, 'class_id')->begin(); ?>
                <div class="row_label"><?= Html::activeLabel($model, 'class_id') ?></div>
                <div class="row_input">
                    <?= Html::activeDropDownList($model, 'class_id', getValue($data['CAR_TYPES'], []), [
                        'class'  => 'default_select',
                        'prompt' => t('client-bonus', 'Choose car class'),
                    ]) ?>
                    <?= Html::error($model, 'class_id',
                        ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']) ?>
                </div>
                <?= $form->field($model, 'class_id')->end(); ?>
            </section>

            <section class="row">
                <?= $form->field($model, 'color')->begin(); ?>
                <div class="row_label"><?= Html::activeLabel($model, 'color') ?></div>
                <div class="row_input">
                    <? $arCarColors = CarColor::getColorMap() ?>
                    <? asort($arCarColors) ?>
                    <?= Html::activeDropDownList($model, 'color', $arCarColors, [
                        'class'  => 'default_select',
                        'prompt' => t('app', 'Not selected'),
                    ]) ?>
                    <?= Html::error($model, 'color',
                        ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']) ?>
                </div>
                <?= $form->field($model, 'color')->end(); ?>
            </section>

            <section class="row">
                <?= $form->field($model, 'gos_number')->begin(); ?>
                <div class="row_label"><?= Html::activeLabel($model, 'gos_number') ?></div>
                <div class="row_input">
                    <div class="input_with_text">
                        <?= Html::activeTextInput($model, 'gos_number', ['dir' => 'auto']) ?>
                        <span>A001AA</span>
                    </div>
                </div>
                <?= $form->field($model, 'gos_number')->end(); ?>
            </section>

            <section class="row">
                <?= $form->field($model, 'year')->begin(); ?>
                <div class="row_label"><?= Html::activeLabel($model, 'year') ?></div>
                <div class="row_input">
                    <?= Html::activeDropDownList($model, 'year', DateTimeHelper::getOldYearList(0, 24), [
                        'class'  => 'default_select',
                        'prompt' => t('app', 'Not selected'),
                    ]) ?>
                    <?= Html::error($model, 'year',
                        ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']) ?>
                </div>
                <?= $form->field($model, 'year')->end(); ?>
            </section>


            <? if (!app()->user->can(User::ROLE_STAFF_COMPANY)): ?>
                <section class="row">
                    <?= $form->field($model, 'tenant_company_id')->begin(); ?>
                    <div class="row_label"><?= Html::activeLabel($model, 'tenant_company_id') ?></div>
                    <div class="row_input">
                        <?= Html::activeDropDownList($model, 'tenant_company_id', $data['COMPANIES'], [
                            'prompt' => t('tenant_company', 'Select company'),
                            'class'  => 'default_select',
                        ]); ?>
                    </div>
                    <?= $form->field($model, 'tenant_company_id')->end(); ?>
                </section>
            <? endif; ?>


            <? if (ArrayHelper::getValue($showFieldList, 'car.' . FieldRecord::NAME_CAR_LICENSE, true)) : ?>
                <h2><?= t('car', 'License to work in a taxi') ?></h2>
                <section class="row">
                    <?= $form->field($model, 'license')->begin(); ?>
                    <div class="row_label"><?= Html::activeLabel($model, 'license') ?></div>
                    <div class="row_input">
                        <?= Html::activeTextInput($model, 'license') ?>
                    </div>
                    <?= $form->field($model, 'license')->end(); ?>
                </section>

                <? if (!$model->isNewRecord): ?>
                    <?= File::widget([
                        'form'          => $form,
                        'model'         => $model,
                        'attribute'     => 'license_scan',
                        'behavior_name' => 'file',
                        'ajaxUpload'    => true,
                        'uploadAction'  => Url::to([
                            '/car/car/upload-logo',
                            'entity_id' => $model->car_id,
                            'scenario'  => 'license_scan',
                        ]),
                    ]) ?>
                <? endif ?>
            <? endif; ?>


            <div id="car_options" <? if (empty($data['CAR_OPTION_LIST'])): ?>style="display:none"<? endif ?>>
                <h2><?= t('car', 'Options') ?></h2>
                <ul class="auto_options">
                    <? if (!empty($data['CAR_OPTION_LIST'])): ?>
                        <? foreach ($data['CAR_OPTION_LIST'] as $option_id => $name):
                            $checked = !empty($model->options) && in_array($option_id, $model->options) ? 'checked="checked"' : null;
                            $value = getValue($data['TENANT_CAR_OPTIONS'][$option_id], 0);
                            ?>
                            <li><span>+<?= Html::encode($value) ?></span><label><input
                                            data-raiting="<?= $value ?>" <?= $checked ?> value="<?= $option_id ?>"
                                            name="Car[options][]" type="checkbox"/> <?= Html::encode(t('car-options',
                                        $name)) ?></label></li>
                        <? endforeach; ?>
                    <? else: ?>
                        <li><?= Html::hiddenInput('Car[options][]') ?></li>
                    <? endif; ?>
                    <li><span><b id="total_raiting"><?= !$model->raiting ? 0 : $model->raiting ?></b></span><b
                                class="auto_options_res"><?= t('car', 'Total raiting') ?></b></li>
                </ul>
                <?= $form->field($model, 'new_options')->begin(); ?>
                <?= Html::activeHiddenInput($model, 'new_options') ?>
                <?= $form->field($model, 'new_options')->end(); ?>
            </div>
            <div id="dynamic_fields">
                <?= $this->render('_dynamicFields', compact('fieldMap', 'form', 'fieldModels')) ?>
            </div>


            <section class="submit_form">
                <? if (!$model->isNewRecord): ?>
                    <div>
                        <label><?= Html::activeCheckbox($model, 'block', ['label' => null]) ?> <b><?= t('car',
                                    'Block') ?></b></label>
                    </div>
                <? endif ?>
                <?= Html::submitInput(t('app', 'Save')) ?>
            </section>
        </div>
    </div>
</section>
<?php ActiveForm::end(); ?>
