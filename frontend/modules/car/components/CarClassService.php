<?php

namespace frontend\modules\car\components;

use frontend\modules\car\models\CarClass;
use yii\base\Object;
use yii\helpers\ArrayHelper;

class CarClassService extends Object
{
    public function getCarClassMap()
    {
        $carClass = CarClass::find()->all();

        return ArrayHelper::map($carClass,'class_id', function (CarClass $model) {
            return t('car', $model->class);
        });
    }
}
