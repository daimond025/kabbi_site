<?php

namespace frontend\modules\car\controllers;

use app\modules\tariff\models\TaxiTariff;
use app\modules\tenant\models\User;
use common\modules\car\models\CarBrand;
use frontend\modules\car\models\CarMileageSearch;
use frontend\modules\car\models\CarOption;
use common\modules\car\models\transport\TransportTypeFieldValue;
use common\modules\car\models\transport\TransportTypeHasField;
use common\modules\tenant\models\TenantHasCarOption;
use frontend\modules\car\models\CarClass;
use frontend\modules\car\models\CarSearch;
use frontend\modules\companies\components\repositories\TenantCompanyRepository;
use frontend\modules\car\models\CarShiftReportSearch;
use frontend\modules\employee\models\groups\WorkerGroup;
use frontend\modules\employee\models\tariff\Tariff;
use frontend\modules\employee\models\tariff\TariffHasCity;
use frontend\modules\tenant\components\services\FieldService;
use Yii;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\filters\AccessControl;
use yii\helpers\Html;
use frontend\modules\car\models\Car;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use common\modules\city\models\City;
use yii\helpers\ArrayHelper;
use frontend\modules\car\models\CarDriverGroupHasTariff;
use frontend\modules\car\models\CarDriverGroupHasClass;
use frontend\modules\car\models\CarHasOption;
use common\modules\car\models\CarModel;
use yii\web\Response;

/**
 * CarController implements the CRUD actions for Car model.
 */
class CarController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class'  => AccessControl::className(),
                'except' => [],
                'rules'  => [
                    [
                        'actions' => ['list', 'show-blocked', 'search', 'update', 'get-car-class', 'show-mileage'],
                        'allow'   => true,
                        'roles'   => ['read_cars'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['cars'],
                    ],
                ],
            ],
        ];
    }

    /**
     * List of all Car models.
     * @return mixed
     */
    public function actionList()
    {
        $queryArCarModels = Car::find()
            ->alias('c')
            ->where(['c.tenant_id' => user()->tenant_id, 'c.active' => 1])
            ->with(['city', 'class'])
            ->joinWith([
                'city' => function ($query) {
                    $query->where([City::tableName() . '.city_id' => array_keys(user()->getUserCityList())]);
                },
            ], false)
            ->orderBy('name');

        if (app()->user->can(User::ROLE_STAFF_COMPANY)) {
            $queryArCarModels->joinWith([
                'tenantCompany tc' => function (ActiveQuery $subQuery) {
                    $subQuery->andFilterWhere(['tc.tenant_company_id' => user()->tenant_company_id]);
                },
            ], false);
        }

        $arCars = Car::getGridData($queryArCarModels->all());

        return $this->render('index', [
            'cars'            => $arCars['CARS'],
            'city_list'       => $arCars['CITI_LIST'],
            'tenantCompanies' => TenantCompanyRepository::selfCreate()->getForForm(),
            'car_type_list'   => translateAssocArray('car', $arCars['TYPE_LIST']),
        ]);
    }

    /**
     * List of all blocked Car models.
     * @return mixed
     */
    public function actionShowBlocked()
    {
        $query = Car::find()
            ->where([
                'tenant_id' => user()->tenant_id,
                'active'    => 0,
            ])
            ->joinWith([
                'city' => function (ActiveQuery $query) {
                    $query->where([City::tableName() . '.city_id' => array_keys(user()->getUserCityList())]);
                },
            ], false);

        if (app()->user->can(User::ROLE_STAFF_COMPANY)) {
            $query->andWhere(['tenant_company_id' => user()->tenant_company_id]);
        }

        $arCars = Car::getGridData($query->all());

        return $this->renderAjax('blocked', [
            'cars'            => $arCars['CARS'],
            'city_list'       => $arCars['CITI_LIST'],
            'car_type_list'   => translateAssocArray('car', $arCars['TYPE_LIST']),
            'tenantCompanies' => TenantCompanyRepository::selfCreate()->getForForm(),
        ]);
    }

    public function actionGetTenantCarOptions($brand, $model, $city_id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $tenantCarOptionMap = $this->getTenantCarOptions($city_id);

        return [
            'carOptions'    => $this->getCarOptionList($brand, $model, $tenantCarOptionMap),
            'tenantOptions' => $tenantCarOptionMap,
        ];
    }

    public function actionGetCarClass($model_id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $transportTypeId = CarModel::find()->where(['model_id' => $model_id])->select('type_id')->scalar();

        return $this->getCarClassMap($transportTypeId);
    }

    /**
     * @param $id
     *
     * @return null|Car
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = Car::find()
            ->where([
                'car_id'    => $id,
                'tenant_id' => user()->tenant_id,
            ])
            ->with([
                'carHasOptions',
                'fieldValues' => function ($query) {
                    $query->indexBy('has_field_id');
                },
            ])
            ->one();

        if (($model !== null)) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findCarModel($brand, $model)
    {
        return CarModel::find()
            ->where([CarModel::tableName() . '.name' => $model])
            ->joinWith([
                'brand b' => function ($query) use ($brand) {
                    $query->andWhere(['b.name' => $brand]);
                },
            ], false)
            ->one();
    }

    protected function updateCarOptions($car_id, $options)
    {
        CarHasOption::deleteAll(['car_id' => $car_id]);

        return $this->addCarOptions($car_id, $options);
    }

    protected function addCarOptions($car_id, $options)
    {
        return CarHasOption::batchInsert($options, $car_id);
    }

    /**
     * Creates a new Car model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model       = new Car();
        $fieldMap    = [];
        $fieldModels = [];
        $post        = Yii::$app->request->post();

        if ($model->load($post)) {

            if (app()->user->can(User::ROLE_STAFF_COMPANY)) {
                $model->tenant_company_id = user()->tenant_company_id;
            }

            $model->new_options = 1;
            $fieldList          = $this->getAdditionalFieldList($model->brand, $model->model);
            $fieldMap           = $this->getFieldMap($fieldList);
            $fieldModels        = $this->getFieldModels($fieldMap, $model->fieldValues);
            $isValid            = $model->validate();
            Model::loadMultiple($fieldModels, $post);

            $isValid = Model::validateMultiple($fieldModels) && $isValid;


            if ($isValid) {
                if ($model->save(false)) {
                    if (!empty($model->options)) {
                        $this->addCarOptions($model->car_id, $model->options);
                    }

                    $this->saveFieldsData($fieldModels, $model->car_id);

                    session()->setFlash('success', t('car', 'The Car successfully added'));

                    return $this->redirect(['update', 'id' => $model->car_id]);
                }
            }
        }

        $data = $this->getCarParams();

        if (!empty($model->brand_id)) {
            $data['MODEL_LIST'] = $this->getCarModelMap($model->brand_id);
        }

        $fieldService  = new FieldService();
        $showFieldList = $fieldService->getFields('car');

        return $this->render('create', compact('model', 'data', 'fieldMap', 'fieldModels', 'showFieldList'));
    }

    public function actionUploadLogo($entity_id, $scenario)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $entity = $this->findOne($entity_id);
        $entity->load(post());

        if ($entity->save(true, [$scenario])) {
            $behavior = $scenario == 'photo' ? 'logo' : 'file';

            return [
                'src' => [
                    'big'   => $entity->getBehavior($behavior)->getPicturePath($entity->$scenario),
                    'thumb' => $entity->getBehavior($behavior)->getPicturePath($entity->$scenario, false),
                ],
            ];
        }

        return $entity->getFirstErrors();
    }

    protected function findOne($entity_id)
    {
        return Car::find()
            ->where([
                'car_id'    => $entity_id,
                'tenant_id' => user()->tenant_id,
            ])
            ->one();
    }

    /**
     * Updates an existing Car model.
     * If update is successful, the browser will be refresh the page.
     *
     * @param string $id
     *
     * @throws NotFoundHttpException
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (app()->user->can(User::ROLE_STAFF_COMPANY)) {
            if (!app()->user->can(User::ROLE_STAFF_COMPANY, ['model' => $model])) {
                throw new ForbiddenHttpException(t('tenant_company', 'Access denied.'));
            }
        }

        $model->options = ArrayHelper::getColumn($model->carHasOptions, 'option_id');
        $old_options    = $model->options;
        $fieldList      = $this->getAdditionalFieldList($model->brand, $model->model);
        $fieldMap       = $this->getFieldMap($fieldList);
        $fieldModels    = $this->getFieldModels($fieldMap, $model->fieldValues);
        $post           = Yii::$app->request->post();

        if ($model->load($post)) {
            $postOptions    = $post['Car']['options'];
            $model->options = $postOptions;
            if ($old_options != $model->options) {
                $model->new_options = 1;
                $model->raiting     = $model->calculateRating($model->options);
            }

            $model->active = $model->block ? 0 : 1;
            $isValid       = $model->validate();

            $changedFields = false;
            if ($model->getOldAttribute('brand') != $model->brand || $model->getOldAttribute('model') != $model->model) {
                $fieldList     = $this->getAdditionalFieldList($model->brand, $model->model);
                $fieldMap      = $this->getFieldMap($fieldList);
                $fieldModels   = $this->getFieldModels($fieldMap, []);
                $changedFields = true;
            }

            Model::loadMultiple($fieldModels, $post);
            $isValid = Model::validateMultiple($fieldModels) && $isValid;
            if ($isValid) {
                $dirtyAttributes = $model->dirtyAttributes;
                unset($dirtyAttributes['photo']);
                if ($model->save(false, array_keys($dirtyAttributes))) {
                    if ($model->new_options) {
                        $this->updateCarOptions($model->car_id, $model->options);
                    }

                    if ($changedFields) {
                        $this->deleteOldCarFields(['car_id' => $model->car_id]);
                    }

                    $this->saveFieldsData($fieldModels, $model->car_id);

                    session()->setFlash('success', t('app', 'Data updated successfully'));

                    return $this->refresh();
                }
            }
        }

        $data                       = $this->getCarParams();
        $model->brand_id            = array_search($model->brand, $data['BRAND_LIST'], true);
        $data['MODEL_LIST']         = $this->getCarModelMap($model->brand_id);
        $data['TENANT_CAR_OPTIONS'] = $this->getTenantCarOptions($model->city_id);
        //Общий список опций авто
        $data['CAR_OPTION_LIST'] = $this->getCarOptionList($model->brand, $model->model, $data['TENANT_CAR_OPTIONS']);
        $model->model_id         = array_search($model->model, $data['MODEL_LIST'], true);
        $model->block            = $model->active ? 0 : 1;
        $types                   = $this->getCarClassMap($model->carModel->type_id);
        $data['CAR_TYPES']       = ArrayHelper::map($types, 'class_id', 'class');

        $fieldService  = new FieldService();
        $showFieldList = $fieldService->getFields('car');

        return $this->render('update', compact('model', 'data', 'fieldMap', 'fieldModels', 'showFieldList'));

    }

    /**
     * Getting car class map
     *
     * @param int $transport_type_id
     *
     * @return array
     */
    protected function getCarClassMap($transport_type_id)
    {
        return CarClass::getCarClassMapByTransportType($transport_type_id);
    }

    /**
     * It needs for car form.
     * @return array
     */
    protected function getCarParams()
    {
        //Массив возвращаемых параметров
        $data = [];

        //Получение списка моделей городов тенанта
        $data['USER_CITY_LIST'] = user()->getUserCityList();

        //Получение списка марок авто
        $data['BRAND_LIST'] = ArrayHelper::map(CarBrand::find()->all(), 'brand_id', 'name');
        asort($data['BRAND_LIST']);

        //При необходимости модельный ряд авто дополняется самостоятельно. Отсюда удалять нельзя т.к. ключ участвует в форме.
        $data['MODEL_LIST'] = [];

        $data['COMPANIES'] = TenantCompanyRepository::selfCreate()->getForForm([], false);

        return $data;
    }

    protected function getCarOptionList($brand, $model, $tenantCarOptionMap)
    {
        $carModel     = $this->findCarModel($brand, $model);
        $carOptions   = CarOption::find()
            ->where(['type_id' => $carModel->type_id])
            ->orderBy(['sort' => SORT_ASC, 'option_id' => SORT_ASC])
            ->all();
        $carOptionMap = ArrayHelper::map($carOptions, 'option_id', function ($item) {
            return t('car-options', $item['name']);
        });

        return array_intersect_key($carOptionMap, $tenantCarOptionMap);
    }

    protected function getTenantCarOptions($city_id)
    {
        //Получение списка опций авто, которые выбрал тенант для работы
        return ArrayHelper::map(TenantHasCarOption::findAll([
            'tenant_id' => user()->tenant_id,
            'city_id'   => $city_id,
            'is_active' => 1,
        ]), 'option_id', 'value');
    }

    protected function getCarModelMap($brand_id)
    {
        return ArrayHelper::map($this->getCarModelList($brand_id), 'model_id', 'name');
    }

    protected function getCarModelList($brand_id)
    {
        return CarModel::find()->where(['brand_id' => $brand_id])->all();
    }

    protected function deleteOldCarFields($where)
    {
        return TransportTypeFieldValue::deleteAll($where);
    }

    protected function saveFieldsData(array $fieldModels, $car_id)
    {
        $isSaved = true;
        foreach ($fieldModels as $fieldModel) {
            $fieldModel->car_id = $car_id;
            $isSaved            = $fieldModel->save(false) && $isSaved;
        }

        return $isSaved;
    }

    protected function getFieldModels(array $fieldMap, array $carFieldModels)
    {
        $fieldModels = [];
        foreach ($fieldMap as $id => $field) {
            if (isset($carFieldModels[$id])) {
                $fieldModels[$id] = $carFieldModels[$id];
            } else {
                $fieldModels[$id] = new TransportTypeFieldValue(['has_field_id' => $id]);
            }
        }

        return $fieldModels;
    }

    protected function getAdditionalFieldList($brand, $model)
    {
        $carModel = $this->findCarModel($brand, $model);

        return TransportTypeHasField::find()
            ->where(['type_id' => $carModel->type_id])
            ->with(['field', 'enum'])
            ->all();
    }

    protected function getFieldMap(array $fieldList)
    {
        return ArrayHelper::map($fieldList, 'id', function ($item) {
            return [
                'name' => $item['field']['name'],
                'type' => $item['field']['type'],
                'enum' => ArrayHelper::map($item['enum'], 'enum_id', 'value'),
            ];
        });
    }

    public function actionAddFields($brandName, $modelName)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        $fieldList   = $this->getAdditionalFieldList($brandName, $modelName);
        $fieldMap    = $this->getFieldMap($fieldList);
        $fieldModels = $this->getFieldModels($fieldMap, []);

        return $this->renderPartial('_dynamicFields', compact('fieldMap', 'fieldModels'));
    }

    /**
     * Ajax method for find list of car models
     */
    public function actionGetModelList()
    {
        $model    = [];
        $brand_id = post('brand_id');

        if ($brand_id) {
            $model = CarModel::find()->asArray()->where(['brand_id' => $brand_id])->all();
        }

        return json_encode($model);
    }

    /**
     * Ajax car seacrh from filter data
     * @return html
     */
    public function actionSearch()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $view         = !empty($_POST['view']) ? '_' . $_POST['view'] : '_index';
        $car_search   = new CarSearch();
        $dataProvider = $car_search->search($_POST);
        $arCars       = $car_search->getGridData($dataProvider->getModels(), false, false);

        return $this->renderAjax($view, ['cars' => $arCars['CARS']]);
    }

    /**
     * Link driver group to car.
     * @return boolean
     */
    public function actionAddDriverGroup()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $car            = Car::find()->where(['car_id' => post('car_id')])->select(['car_id', 'group_id'])->one();
        $group_id       = post('group_id');
        $json['result'] = 0;

        if (!empty($car) && !empty($group_id) && $car->group_id != $group_id) {
            $driver_group              = WorkerGroup::find()
                ->where([
                    'group_id'  => $group_id,
                    'tenant_id' => user()->tenant_id,
                ])
                ->with([
                    'workerGroupHasTariffs',
                    'workerGroupCanViewClientTariffs',
                ])
                ->select(['group_id'])
                ->one();
            $car->driver_group_tariffs = ArrayHelper::getColumn($driver_group->workerGroupHasTariffs, 'tariff_id');
            $car->driver_group_classes = ArrayHelper::getColumn($driver_group->workerGroupCanViewClientTariffs,
                'class_id');
            $json['group_tariffs']     = $car->driver_group_tariffs;
            $json['group_classes']     = $car->driver_group_classes;

            $car->group_id  = $group_id;
            $json['result'] = (int)$car->save(false, ['group_id']);
        }

        return json_encode($json);
    }

    /**
     * Add driver group tariff.
     * @return boolean
     */
    public function actionAddDriverGroupTariff()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $json           = 0;
        $car_id         = post('car_id');
        $arTariff       = getValue(post('tariffs'), []);
        $city_ids       = array_keys(user()->getUserCityList());
        $old_tariff_ids = TariffHasCity::find()
            ->select('tariff_id')
            ->where(['city_id' => $city_ids])
            ->column();

        if (is_array($arTariff) && !empty($car_id)) {
            try {
                $res = CarDriverGroupHasTariff::updateRows($arTariff, $car_id, $old_tariff_ids);
                if ($res) {
                    $json = 1;
                }
            } catch (\yii\db\Exception $exc) {
                Yii::error($exc->getMessage());
            }
        }

        return json_encode($json);
    }

    /**
     * Add driver group class.
     * @return boolean
     */
    public function actionAddDriverGroupClass()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $json      = 0;
        $car_id    = post('car_id');
        $arClasses = getValue(post('classes'), []);
        $tenant_id = user()->tenant_id;
        $city_ids  = array_keys(user()->getUserCityList());

        $old_class_ids = TaxiTariff::find()
            ->select('class_id')
            ->joinWith('tariffHasCity')
            ->where([
                'city_id'   => $city_ids,
                'tenant_id' => $tenant_id,
            ])
            ->column();

        $car = Car::find()->where(['car_id' => post('car_id')])->one();
        if (!empty($car) && !in_array($car->class_id, $arClasses)) {
            $arClasses[] = $car->class_id;
        }

        if (is_array($arClasses) && !empty($car_id)) {
            try {
                $res = CarDriverGroupHasClass::updateRows($arClasses, $car_id, $old_class_ids);
                if ($res) {
                    $json = 1;
                }
            } catch (\yii\db\Exception $exc) {
                Yii::error($exc->getMessage());
            }
        }

        return json_encode($json);
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function actionShowMileage($id)
    {
        $searchModel  = new CarMileageSearch(['formName' => '']);
        $searchParams = [
            'carId'      => $id,
            'tenantId'   => user()->tenantId,
            'filter'     => post('filter', $searchModel->filter),
            'firstDate'  => post('first_date'),
            'secondDate' => post('second_date'),
        ];
        $dataProvider = $searchModel->search($searchParams);
        $showFilter   = !post('hideFilter');

        return $this->renderAjax('carMileage/index', compact('showFilter', 'searchModel', 'dataProvider'));
    }

    public function actionCarShowShifts($id)
    {

        $searchModel  = new CarShiftReportSearch(['formName' => '']);
        $searchParams = [
            'carId'      => $id,
            'tenantId'   => user()->tenantId,
            'filter'     => post('filter', $searchModel->filter),
            'firstDate'  => post('first_date'),
            'secondDate' => post('second_date'),
        ];

        $dataProviderActiveShifts = $searchModel->search($searchParams, true);
        $dataProviderShifts       = $searchModel->search($searchParams, false);

        $showFilter = !post('hideFilter');

        return $this->renderAjax('shifts',
            compact('showFilter', 'searchModel', 'dataProviderActiveShifts', 'dataProviderShifts'));


    }


}
