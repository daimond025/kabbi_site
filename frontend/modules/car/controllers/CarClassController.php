<?php


namespace frontend\modules\car\controllers;


use common\modules\employee\components\position\PositionService;
use frontend\modules\car\models\CarClass;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

class CarClassController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class'  => AccessControl::className(),
                'except' => [],
                'rules'  => [
                    [
                        'allow'   => true,
                        'roles'   => ['read_cars'],
                    ],
                ],
            ],
        ];
    }

    public function actionGetCarClassByTransportType($transportTypeId)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        return $this->getCarClassMap($transportTypeId);
    }

    public function actionGetCarClassByPosition($positionId)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var PositionService $positionService */
        $positionService = Yii::$app->getModule('employee')->get('position');
        $transportTypeId = $positionService->getPositionTransportType($positionId);

        return $this->getCarClassMap($transportTypeId);
    }

    /**
     * Getting car class map
     * @param int $transport_type_id
     * @return array
     */
    protected function getCarClassMap($transport_type_id)
    {
        return CarClass::getCarClassMapByTransportType($transport_type_id);
    }

}