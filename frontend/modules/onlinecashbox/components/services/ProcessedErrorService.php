<?php

namespace frontend\modules\onlinecashbox\components\services;


use GuzzleHttp\Exception\ClientException;
use yii\base\Model;

class ProcessedErrorService
{
    const BAD_REQUEST = 400;
    const NOT_FOUND_HTTP = 404;

    const PROCESSED_RESPONSE_CODE = [self::NOT_FOUND_HTTP, self::BAD_REQUEST];



    public function processedResponseError(ClientException $e, Model $model)
    {
        if (in_array($e->getResponse()->getStatusCode(), self::PROCESSED_RESPONSE_CODE)) {

            $errors = json_decode($e->getResponse()->getBody()->getContents(), true);

            foreach ($errors['errors'] as $error) {
                $model->addError($error['code'], t('online_cashbox', $error['message']));
            }

        }

    }


}