<?php
namespace frontend\modules\onlinecashbox\components\services;


use frontend\modules\onlinecashbox\components\exceptions\UnknownProfileTypeException;
use frontend\modules\onlinecashbox\models\profiles\ProfileDisabled;
use frontend\modules\onlinecashbox\models\profiles\ProfileModulKassa;
use frontend\modules\onlinecashbox\models\profiles\ProfileOrangeData;

class ProfileOnlineCashboxService
{

    public function getProfileModel($typeId, $attributes = [])
    {
        $typeId = (int)$typeId;
        switch ($typeId) {
            case ProfileDisabled::TYPE_ID:
                return new ProfileDisabled();
            case ProfileModulKassa::TYPE_ID:
                return new ProfileModulKassa($attributes);
            case ProfileOrangeData::TYPE_ID:
                return new ProfileOrangeData($attributes);
            default:
                throw new UnknownProfileTypeException("Unknown profile type: \"{$typeId}\"");
        }
    }


    public function addDisableType($typesOnlineCashbox)
    {
        array_unshift(
            $typesOnlineCashbox, ['id' => 0, 'name' => t('online_cashbox', 'Choose the type of ticket office')]
        );

        return $typesOnlineCashbox;
    }

}