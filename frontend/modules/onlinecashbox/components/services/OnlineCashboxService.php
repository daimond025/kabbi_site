<?php

namespace frontend\modules\onlinecashbox\components\services;


use frontend\components\onlineCashbox\OnlineCashboxApi;
use frontend\modules\onlinecashbox\models\Config;
use GuzzleHttp\Exception\ClientException;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class OnlineCashboxService
{

    /* @var $onlineCashboxApi OnlineCashboxApi */
    protected $onlineCashboxApi;

    protected $profileOnlineCashboxService;

    public function __construct(
        ProfileOnlineCashboxService $profileOnlineCashboxService
    ) {
        $this->onlineCashboxApi            = Yii::$app->onlineСashboxApi;
        $this->profileOnlineCashboxService = $profileOnlineCashboxService;
    }


    public function getTypesOnlineCashbox()
    {
        return ArrayHelper::map(
            $this->profileOnlineCashboxService->addDisableType(
                $this->onlineCashboxApi->getListTypesCashbox()
            ),
            'id',
            'name'
        );
    }


    public function getProfilesCurrentTenant()
    {
        $profilesFromApi = $this->onlineCashboxApi->getListProfilesCashbox(user()->tenant_id);

        $profiles = [];
        foreach ($profilesFromApi as $profileFromApi) {
            $profiles[] = $this->profileOnlineCashboxService
                ->getProfileModel($profileFromApi['type_id'], $profileFromApi);
        }

        return $profiles;
    }


    public function getProfile($id)
    {
        try {
            $profileFromApi = $this->onlineCashboxApi->viewProfile($id);
        } catch (ClientException $e) {
            if ($e->getResponse()->getStatusCode() == ProcessedErrorService::NOT_FOUND_HTTP) {
                throw new NotFoundHttpException();
            }
        }

        return $this->profileOnlineCashboxService
            ->getProfileModel($profileFromApi['type_id'], $profileFromApi);
    }


    public function getConfig($id)
    {
        try {
            $configFromApi = $this->onlineCashboxApi->viewConfig($id);
        } catch (ClientException $e) {
            if ($e->getResponse()->getStatusCode() == ProcessedErrorService::NOT_FOUND_HTTP) {
                throw new NotFoundHttpException();
            }
        }

        $config = new Config($configFromApi);

        return $config;
    }

    public function getConfigsCurrentTenant()
    {

        $configsFromApi = $this->onlineCashboxApi->getListConfigCashbox(user()->tenant_id);

        $configs = [];
        foreach ($configsFromApi as $configFromApi) {
            $config = new Config($configFromApi);

            $configs[] = $config;
        }


        return $configs;
    }


    public function createConfig(Config $config)
    {
        return $this->onlineCashboxApi->createConfig($config);
    }

    public function updateConfig(Config $config, $id)
    {
        return $this->onlineCashboxApi->updateConfig($config, $id);
    }

    public function deleteConfig($id)
    {
        return $this->onlineCashboxApi->deleteConfig($id);
    }

}