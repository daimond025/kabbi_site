<?php

namespace frontend\modules\onlinecashbox\components\datacentr;


use frontend\modules\employee\components\position\PositionService;
use frontend\modules\onlinecashbox\components\services\OnlineCashboxService;
use frontend\modules\onlinecashbox\models\Config;
use yii\helpers\ArrayHelper;

class DataForms
{

    /* @var $onlineCashboxService OnlineCashboxService */
    protected $onlineCashboxService;
    /* @var $positionService PositionService */
    protected $positionService;

    public function __construct()
    {
        $this->onlineCashboxService = \Yii::$container->get(OnlineCashboxService::class);
        $this->positionService      = \Yii::$container->get(PositionService::class);
        $this->positionService->setTenantId(user()->tenant_id);
    }

    public function getDataConfigForm(Config $config)
    {
        $profilesModels = $this->onlineCashboxService->getProfilesCurrentTenant();
        $profiles       = ArrayHelper::map($profilesModels, 'id', 'name');
        $profilesTypeId = ArrayHelper::map($profilesModels, 'id', 'type_id');
        $cities         = user()->getUserCityList();

        if ($config->city_id) {
            $positions = $this->getPositionMap($config->city_id);
        } else {
            $positions = $this->getPositionMap(key($cities));
        }

        return compact('profiles', 'cities', 'positions', 'profilesTypeId');
    }


    private function getPositionMap($cityId)
    {

        $positions = $this->positionService->getPositionsByCity($cityId);

        return empty($positions) ? [] : ArrayHelper::map($positions, 'position_id', 'name');
    }

}