<?php

namespace frontend\modules\onlinecashbox\components\behaviors;


use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

class FileBehavior extends Behavior
{

    public $fieldsModel;

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
        ];
    }


    public function beforeValidate()
    {
        foreach ($this->fieldsModel as $fieldModel) {
            $file = UploadedFile::getInstance($this->owner, $fieldModel);

            $this->owner->$fieldModel = $file;
        }
    }


}