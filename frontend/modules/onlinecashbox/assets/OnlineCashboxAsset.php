<?php

namespace frontend\modules\onlinecashbox\assets;


use yii\web\AssetBundle;

class OnlineCashboxAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/onlinecashbox/assets/';

    public $publishOptions = [
        'forceCopy' => true,
        'only' => [
            'js/cashbox.js',
            'js/position.js'
        ]
    ];
    public $css = [
    ];
    public $js = [
        'js/cashbox.js',
        'js/position.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}