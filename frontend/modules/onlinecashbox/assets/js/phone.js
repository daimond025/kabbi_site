(function ($) {
    'use strict';

    var app = {

        init: function () {
            this.registerEvent();
        },


        deleteItem: function (item) {
            var phoneLine = $(item).find('phone_line');

            if (phoneLine.length() == 0) {
                item.find('delete-item').remove();
            }
        },

        registerEvent: function () {
            $('#profileonlinecashbox-type_id').change(function () {
                var phoneLine = $('.phone_line');

                var _this = this;

                $.each(phoneLine, function (index, value) {
                    _this.deleteItem(this);
                });
            });
        }

    };

    app.init();
})(jQuery);