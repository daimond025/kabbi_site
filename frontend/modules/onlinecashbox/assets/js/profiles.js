(function ($) {
    'use strict';

    var app = {

        profileTypeIds: null,

        init: function () {
            this.profileTypeIds = JSON.parse($('#profile-type_ids').text());
            this.toggle();
            this.profileChangeEvent();
        },


        profileChangeEvent: function () {
            var _this = this;
            $('body').on('change', '#config-profile_id', function () {
                _this.toggle();
            });
        },

        toggle: function () {
            var tax = $('#config-taxation_system').parent().parent().parent();
            if (this.getCurrentTypeId() == 1) {
                tax.hide();
            } else {
                tax.show();
            }
        },

        getCurrentTypeId: function () {
            var id = $('#config-profile_id option[selected]').val();
            return this.profileTypeIds[id];
        }

    };


    app.init();
})(jQuery);