/* global jQuery */
(function ($) {
    'use strict';
    var app = {
        $profile: null,
        init: function () {
            this.$profile = $('.js-profile');
            this.registerEvents();
            this.registerChangeFilterTypeEvents();
            this.registerChooseFilterDateEvents();
            this.runPhoneMask();
            this.deleteItem();
            this.registerAddPhoneEvents();
        },
        registerEvents: function () {
            var _this = this;

            _this.$profile.on('change', '#profileonlinecashbox-type_id', function () {
                var $this = $(this),
                    formData = $this.closest('form').serialize();

                $.post('/onlinecashbox/profile/change-profile-type', formData)
                    .done(function (data) {
                        _this.$profile.html(data);
                        $('#profileonlinecashbox-type_id').trigger('update');
                        $('#profileonlinecashbox-type_id').closest('form').find('.default_select').trigger('update');
                        _this.deleteItem();
                        _this.runPhoneMask();
                        _this.registerAddPhoneEvents();
                    })
                    .fail(function (data) {
                        _this.$profile.html(null);
                        console.log('fail', data);
                    });
            });

            _this.$profile.on('click', '#profile-securedeal', function () {
                var $secureDealBlock = $('.js-secure-deal');
                var $yandexFailedPaymentsLink = $('.js-yandex-failed-payments');

                if ($(this).prop('checked')) {
                    $secureDealBlock.show();
                    $yandexFailedPaymentsLink.show();
                } else {
                    $secureDealBlock.hide();
                    $yandexFailedPaymentsLink.hide();
                }
            });

            _this.$profile.on('click', '#profile-usereceipt', function () {
                var $receiptBlock = $('.js-receipt');

                if ($(this).prop('checked')) {
                    $receiptBlock.show();
                } else {
                    $receiptBlock.hide();
                }
            });
        },

        registerAddPhoneEvents: function () {
            var _this = this;

            $(".dynamicform_wrapper_payment_transfer_operator_phones").on("afterInsert", function(e, item) {
                _this.runPhoneMask();
            });
            $(".dynamicform_wrapper_payment_agent_phones").on("afterInsert", function(e, item) {
                _this.runPhoneMask();
            });
            $(".dynamicform_wrapper_payment_operator_phones").on("afterInsert", function(e, item) {
                _this.runPhoneMask();
            });
            $(".dynamicform_wrapper_supplier_phones").on("afterInsert", function(e, item) {
                _this.runPhoneMask();
            });
        },

        runPhoneMask: function () {
            var maskList = $.masksSort($.masksLoad("/js/inputmask/phone-codes.json"), ['#'], /[0-9]|#/, "mask");
            var maskOpts = {
                inputmask: {
                    definitions: {
                        '#': {
                            validator: "[0-9]",
                            cardinality: 1
                        }
                    },
                    oncomplete: function () {
                        $(this).attr('is-valid', 1);
                    },
                    onincomplete: function () {
                        $(this).attr('is-valid', null);
                    },
                    showMaskOnHover: false,
                    autoUnmask: true
                },
                match: /[0-9]/,
                replace: '#',
                list: maskList,
                listKey: "mask"
            };

            $.each($('.mask_phone'), function (index, value) {
                $(this).inputmasks(maskOpts);
            });

        },

        deleteItem: function () {
            var phonesTable = $('.phone_line');

            var _this = this;

            $.each(phonesTable, function (index, value) {
                _this.deleteLabel(this);
            });
        },

        deleteLabel: function (item) {
            var first = $(item).find('.delete-item')[0];
            $(first).remove();
        },

        registerChangeFilterTypeEvents: function () {
            var _this = this;
            $(document).on('click', 'form[name="yandex-failed-payments-search"] [type="radio"]', function () {
                _this.getYandexFailedPayments($(this).closest('form'));
            });
        },
        registerChooseFilterDateEvents: function () {
            var _this = this;

            $('.sdp').each(function () {
                var $obj = $(this);
                $obj.datepicker({
                    altField: $obj.prev('input'),
                    onSelect: function (date) {
                        var $input = $(this);
                        $input.parent().prev('.a_sc').html(date).removeClass('sel_active');
                        $input.parent().hide();
                        _this.getYandexFailedPayments($obj.closest('form'));
                    }
                });
                if ($obj.hasClass('first_date')) {
                    $obj.datepicker("setDate", '-14');
                }
            });
        },
        getYandexFailedPayments: function (form) {
            $.post(form.attr('action'), form.serialize())
                .done(function (data) {
                    var $grid = form.closest('.yandex-failed-payments').find('.grid');
                    $grid.html(data);
                })
                .fail(function () {

                });
        }
    };
    app.init();
})(jQuery);