(function ($) {
    'use strict';

    var app = {


        init: function () {
            this.cityChangingEvent();
            this.confirmWindow();
        },


        cityChangingEvent: function () {
            var _this = this;
            $('body').on('change', '#config-city_id', function () {
                var cur_element = $(this);
                var city_id = cur_element.val();
                _this.updatePositionList(city_id);
            });
        },

        updatePositionList: function (cityId) {
            $.get(
                '/onlinecashbox/config/get-positions/' + cityId,
                function (json) {
                    console.log(json);
                    var select = $('#config-position_id');
                    select.find('option').remove();
                    var options = $();

                    for (var i = 0; i < json.length; i++) {
                        options = options.add($('<option/>').val(json[i]['position_id']).text(json[i]['name']));
                    }

                    select.append(options);

                    select.trigger('update');
                },
                'json'
            );
        },

        confirmWindow: function () {
            $(".js-confirm-window").gootaxModal({
                inline: true,
                url: '#delete-confirm',
                windowStyle: 'padding: 20px; left: 50%; top: 50%; width: 400px; margin: -100px 0 0 -200px; min-height: 0; text-align: center; box-sizing: border-box;',
                onComplete: function () {
                },
                onClosed: function () {
                }
            });

            $('body').on('click', '.js-close-confirm-window', function () {
                $(".js-confirm-window").gootaxModal('close');
            });
        }
    };


    app.init();
})(jQuery);