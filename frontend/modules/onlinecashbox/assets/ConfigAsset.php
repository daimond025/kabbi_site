<?php

namespace frontend\modules\onlinecashbox\assets;


use yii\web\AssetBundle;

class ConfigAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/onlinecashbox/assets/';

    public $publishOptions = [
        'forceCopy' => true,
        'only' => [
            'js/profiles.js',
        ]
    ];
    public $css = [
    ];
    public $js = [
        'js/profiles.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}