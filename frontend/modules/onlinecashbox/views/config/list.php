<?php

/* @var $config \frontend\modules\onlinecashbox\models\Config[] */
/* @var $arrayDataProvider \yii\data\ArrayDataProvider */

/* @var $this \yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = t('online_cashbox', 'List configurations');

if (app()->user->can('onlineCashbox')) {
    echo Html::a(t('app', 'Add'), ['create'], ['class' => 'button', 'style' => 'float: right; position: relative; top: 5px;']);
}


echo \yii\widgets\Breadcrumbs::widget([
    'links'        => [
        [
            'label' => t('online_cashbox', 'Online cashboxs'),
            'url' => ['/onlinecashbox/base/list']
        ],
    ],
    'itemTemplate' => '{link}',
    'tag'          => 'div',
    'options'      => [
        'class' => 'bread',
        'style' => 'display: inline;'
    ],
    'homeLink'     => false,
]);

?>



<h1><?= Html::encode($this->title) ?></h1>



<?= $this->render('_grid', compact('arrayDataProvider')) ?>