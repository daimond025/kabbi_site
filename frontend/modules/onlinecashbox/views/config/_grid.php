<?php

/* @var $arrayDataProvider \yii\data\ArrayDataProvider */
/* @var $typesOnlineCashbox array */

use yii\grid\GridView;
use yii\helpers\Html;
use frontend\modules\onlinecashbox\models\Config;
use frontend\widgets\LinkPager;



echo GridView::widget([
    'dataProvider' => $arrayDataProvider,
    'layout'       => "{items}\n{pager}",
    'tableOptions' => [
        'class' => 'people_table',
    ],
    'pager'        => [
        'class'         => LinkPager::className(),
        'prevPageLabel' => t('app', 'Prev'),
        'nextPageLabel' => t('app', 'Next'),
    ],
    'columns'      => [
        [
            'attribute' => 'product_name',
            'content' => function (Config $model) {
                $text = Html::encode($model->product_name);
                return Html::a($text, ['update', 'id' => $model->id]);
            }
        ],
        [
            'attribute' => 'profile_id',
            'content' => function (Config $model) {
                return Html::encode($model->getProfile()->name);
            }
        ],
        [
            'attribute' => 'city_id',
            'content' => function (Config $model) {
                $langName = 'name' . getLanguagePrefix();
                return $model->getCity()->$langName;
            }
        ],
        [
            'attribute' => 'position_id',
            'content' => function (Config $model) {
                return t('employee', $model->getPosition()->name);
            }
        ],
    ],
]);