<?php

/* @var $config \frontend\modules\onlinecashbox\models\Config */
/* @var $profiles array */
/* @var $cities array */
/* @var $positions array */
/* @var $profilesTypeId array */

/* @var $this \yii\web\View */


\frontend\modules\onlinecashbox\assets\ConfigAsset::register($this);
\frontend\modules\onlinecashbox\assets\OnlineCashboxAsset::register($this);

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\modules\onlinecashbox\models\SendingCondition;

?>

<?php $form = ActiveForm::begin(); ?>

    <section class="row">
        <?= $form->field($config, 'tenant_id')->begin() ?>
        <div class="row_label">

        </div>
        <div class="row_input">
            <?= Html::error($config, 'tenant_id',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($config, 'tenant_id')->end() ?>
    </section>

    <section class="row">
        <?= $form->field($config, 'profile_id')->begin() ?>
        <div class="row_label">
            <?= Html::activeLabel($config, 'profile_id') ?>
        </div>
        <div class="row_input">
            <?= Html::activeDropDownList($config, 'profile_id', $profiles, [
                'prompt' => t('online_cashbox', 'Select profile cashbox'),
                'class'  => 'default_select',
            ]); ?>
            <?= Html::error($config, 'profile_id',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <div id="profile-type_ids" style="display: none"><?= json_encode($profilesTypeId) ?></div>
        <?= $form->field($config, 'profile_id')->end() ?>
    </section>

    <section class="row">
        <?= $form->field($config, 'city_id')->begin() ?>
        <div class="row_label">
            <?= Html::activeLabel($config, 'city_id') ?>
        </div>
        <div class="row_input">
            <?= Html::activeDropDownList($config, 'city_id', $cities, [
                'class' => 'default_select',
            ]); ?>
            <?= Html::error($config, 'city_id',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($config, 'city_id')->end() ?>
    </section>

    <section class="row">
        <?= $form->field($config, 'position_id')->begin() ?>
        <div class="row_label">
            <?= Html::activeLabel($config, 'position_id') ?>
        </div>
        <div class="row_input">
            <?= Html::activeDropDownList($config, 'position_id', $positions, [
                'class' => 'default_select',
            ]); ?>
            <?= Html::error($config, 'position_id',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($config, 'position_id')->end() ?>
    </section>

    <section class="row">
        <?= $form->field($config, 'product_name')->begin() ?>
        <div class="row_label">
            <?= Html::activeLabel($config, 'product_name') ?>
        </div>
        <div class="row_input">
            <?= Html::activeTextInput($config, 'product_name'); ?>
            <?= Html::error($config, 'product_name',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
            <p>
                <strong>
                    <?= t('online_cashbox', 'In the product name, you can substitute one or more additional parameters:') ?>
                </strong>
            </p>
            <p>#ORDER_ID# - <?= t('online_cashbox', 'Order ID') ?></p>
            <p>#ORDER_NUMBER# - <?= t('online_cashbox', 'order number') ?></p>
            <p>#CLIENT_PHONE# - <?= t('online_cashbox', 'customer\'s phone number') ?></p>
            <p>#CLIENT_EMAIL# - <?= t('online_cashbox', 'email client') ?></p>
            <p>#COST# - <?= t('online_cashbox', 'order cost') ?></p>
            <p>#CAR_NUMBER# - <?= t('online_cashbox', 'state car number') ?></p>
        </div>
        <?= $form->field($config, 'product_name')->end() ?>
    </section>

    <section class="row">
        <?= $form->field($config, 'sending_condition')->begin() ?>
        <div class="row_label">
            <?= Html::activeLabel($config, 'sending_condition') ?>
        </div>
        <div class="row_input">
            <?= Html::activeCheckboxList($config, 'sending_condition', SendingCondition::getVariants(), [
                'class' => 'select_checkbox',
                'item'  => function ($index, $label, $name, $checked, $value) {
                    $input = Html::checkbox($name, $checked, ['value' => $value]);
                    $label = Html::label($input . $label);
                    $innerHtml = Html::tag('div', $label, [
                        'class' => 'js-item-company',
                    ]);

                    return $innerHtml;
                },
            ]); ?>
            <?= Html::error($config, 'sending_condition',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($config, 'sending_condition')->end() ?>
    </section>

    <section class="row">
        <?= $form->field($config, 'tax')->begin() ?>
        <div class="row_label">
            <?= Html::activeLabel($config, 'tax') ?>
        </div>
        <div class="row_input">
            <?= Html::activeDropDownList($config, 'tax', [
                1 => t('online_cashbox', 'VAT rate 18%'),
                2 => t('online_cashbox', 'VAT rate 10%'),
                3 => t('online_cashbox', 'VAT rate calc. 18/118'),
                4 => t('online_cashbox', 'VAT rate calc. 10/110'),
                5 => t('online_cashbox', 'VAT rate 0%'),
                6 => t('online_cashbox', 'NDS is not appearing'),
            ], [
                'prompt' => t('online_cashbox', 'Choose a VAT rate'),
                'class'  => 'default_select',
            ]); ?>
            <?= Html::error($config, 'tax',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($config, 'tax')->end() ?>
    </section>

    <section class="row">
        <?= $form->field($config, 'taxation_system')->begin() ?>
        <div class="row_label">
            <?= Html::activeLabel($config, 'taxation_system') ?>
        </div>
        <div class="row_input">
            <?= Html::activeDropDownList($config, 'taxation_system', [
                0 => t('online_cashbox', 'General, OSN'),
                1 => t('online_cashbox', 'Simplified income, STS income'),
                2 => t('online_cashbox', 'Simplified income minus expenditure, STS income - expense'),
                3 => t('online_cashbox', 'Unified tax on imputed income, UTII'),
                4 => t('online_cashbox', 'Unified agricultural tax, UST'),
                5 => t('online_cashbox', 'Patent system of taxation, Patent'),
            ], [
                'prompt' => t('online_cashbox', 'Choose a tax system'),
                'class'  => 'default_select',
            ]); ?>
            <?= Html::error($config, 'taxation_system',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        </div>
        <?= $form->field($config, 'taxation_system')->end() ?>
    </section>


    <section class="submit_form">
        <div>
            <?php if ($config->id): ?>
                <section class="delete-profile">
                    <div class="delete-cashbox-profile">
                        <a class="button red_button js-confirm-window"><?= t('online_cashbox', 'Delete'); ?></a>
                    </div>
                </section>
            <?php endif; ?>
        </div>
        <?= Html::submitInput(t('online_cashbox', 'Save')) ?>
    </section>
    <div style="display: none">
        <div id="delete-confirm">
            <h2> <?php echo t('online_cashbox', 'Are you sure?') ?></h2>
            <div class="confirm-buttons">
                <a class="button js-close-confirm-window"><?php echo t('online_cashbox', 'Cancel') ?></a>
                <?= Html::a(
                    t('online_cashbox', 'Delete'),
                    ['delete', 'id' => $config->id],
                    ['id' => 'item-delete', 'class' => 'button red_button']
                ) ?>
            </div>
        </div>
    </div>

<?php $form = ActiveForm::end(); ?>