<?php

/* @var $config \frontend\modules\onlinecashbox\models\Config */
/* @var $cities array */
/* @var $positions array */
/* @var $profiles array */
/* @var $profilesTypeId array */

/* @var $this \yii\web\View */


use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('online_cashbox', 'Cashier configuration');
echo \yii\widgets\Breadcrumbs::widget([
    'links'        => [
        [
            'label' => t('online_cashbox', 'List configurations'),
            'url' => ['/onlinecashbox/config/list']
        ],
    ],
    'itemTemplate' => '{link}',
    'tag'          => 'div',
    'options'      => ['class' => 'bread'],
    'homeLink'     => false,
]);
?>

<?= Html::tag('h1', Html::encode($this->title)); ?>


<?= $this->render('_form', [
    'config'         => $config,
    'profiles'       => $profiles,
    'cities'         => $cities,
    'positions'      => $positions,
    'profilesTypeId' => $profilesTypeId,
]); ?>
