<?php

/* @var $arrayDataProvider \yii\data\ArrayDataProvider */
/* @var $typesOnlineCashbox array */

/* @var $this \yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = t('online_cashbox', 'List of cashier profiles');

if (app()->user->can('onlineCashbox')) {
    echo Html::a(t('app', 'Add'), ['create'],
        ['class' => 'button', 'style' => 'float: right; position: relative; top: 5px;']);
}

echo \yii\widgets\Breadcrumbs::widget([
    'links'        => [
        [
            'label' => t('online_cashbox', 'Online cashboxs'),
            'url' => ['/onlinecashbox/base/list']
        ],
    ],
    'itemTemplate' => '{link}',
    'tag'          => 'div',
    'options'      => [
        'class' => 'bread',
        'style' => 'display: inline;'
    ],
    'homeLink'     => false,
]);

?>



<h1><?= Html::encode($this->title) ?></h1>


<section class="main_tabs">
    <?php echo $this->render('_grid', [
        'arrayDataProvider'  => $arrayDataProvider,
        'typesOnlineCashbox' => $typesOnlineCashbox,

    ]); ?>
</section>