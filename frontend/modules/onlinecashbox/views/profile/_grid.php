<?php

/* @var $arrayDataProvider \yii\data\ArrayDataProvider */
/* @var $typesOnlineCashbox array */

use yii\grid\GridView;
use yii\helpers\Html;
use frontend\modules\onlinecashbox\models\profiles\ProfileBase;
use frontend\widgets\LinkPager;

echo GridView::widget([
    'dataProvider' => $arrayDataProvider,
    'layout'       => "{items}\n{pager}",
    'tableOptions' => [
        'class' => 'people_table',
    ],
    'pager'        => [
        'class'         => LinkPager::className(),
        'prevPageLabel' => t('app', 'Prev'),
        'nextPageLabel' => t('app', 'Next'),
    ],
    'columns'      => [
        [
            'attribute' => 'name',
            'content' => function (ProfileBase $model) {
                $text = Html::encode($model->name);
                return Html::a($text, ['update', 'id' => $model->id]);
            }
        ],
        [
            'attribute' => 'type_id',
            'content' => function (ProfileBase $model) use ($typesOnlineCashbox) {
                return $typesOnlineCashbox[$model->type_id];
            }
        ]
    ],
]);