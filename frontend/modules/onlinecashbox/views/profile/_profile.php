<?php

use frontend\modules\onlinecashbox\models\profiles\ProfileBase;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $profile ProfileBase */

?>

<?
$viewFile = 'profiles/_' . $profile->getViewPath();
echo $this->render($viewFile, compact('form', 'profile'));
?>
