<?php

/* @var $profile \frontend\modules\onlinecashbox\models\profiles\ProfileBase */
/* @var $typesOnlineCashbox array */

/* @var $this \yii\web\View */


use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Html::encode($profile->name);

echo \yii\widgets\Breadcrumbs::widget([
    'links'        => [
        [
            'label' => t('online_cashbox', 'List of cashier profiles'),
            'url' => ['/onlinecashbox/profile/list']
        ],
    ],
    'itemTemplate' => '{link}',
    'tag'          => 'div',
    'options'      => ['class' => 'bread'],
    'homeLink'     => false,
]);
?>

<?= Html::tag('h1', Html::encode($this->title)); ?>



<div class="js-profile">
    <?= $this->render('_form', [
        'profile'            => $profile,
        'typesOnlineCashbox' => $typesOnlineCashbox,
    ]); ?>
</div>
