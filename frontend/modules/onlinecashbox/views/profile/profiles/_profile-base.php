<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\modules\onlinecashbox\models\profiles\ProfileBase;

/* @var $form ActiveForm */
/* @var $profile ProfileBase */
/* @var $this \yii\web\View */

?>

<section class="row">
    <?= $form->field($profile, 'name')->begin() ?>
    <div class="row_label">
        <?= Html::activeLabel($profile, 'name') ?>
    </div>
    <div class="row_input">
        <?= Html::activeTextInput($profile, 'name'); ?>
        <?= Html::error($profile, 'name',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($profile, 'name')->end() ?>
</section>


<section class="row">
    <?= $form->field($profile, 'debug')->begin() ?>
    <div class="row_label">
        <?= Html::activeLabel($profile, 'debug') ?>
    </div>
    <div class="row_input">
        <?= Html::activeDropDownList($profile, 'debug', [
            0 => t('online_cashbox', 'Working'),
            1 => t('online_cashbox', 'Test'),
        ], [
            'class'    => 'default_select',
        ]); ?>
        <?= Html::error($profile, 'debug',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($profile, 'debug')->end() ?>
</section>