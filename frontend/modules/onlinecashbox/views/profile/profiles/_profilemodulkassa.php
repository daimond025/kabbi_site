<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\modules\onlinecashbox\models\profiles\ProfileModulKassa;

/* @var $form ActiveForm */
/* @var $profile ProfileModulKassa */
/* @var $this \yii\web\View */

?>

<?= $this->render('_profile-base', [
    'profile' => $profile,
    'form'    => $form
]) ?>


<section class="row">
    <?= $form->field($profile, 'modulkassa_email')->begin() ?>
    <div class="row_label">
        <?= Html::activeLabel($profile, 'modulkassa_email') ?>
    </div>
    <div class="row_input">
        <?= Html::activeTextInput($profile, 'modulkassa_email'); ?>
        <?= Html::error($profile, 'modulkassa_email',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($profile, 'modulkassa_email')->end() ?>
</section>


<section class="row">
    <?= $form->field($profile, 'modulkassa_password')->begin() ?>
    <div class="row_label">
        <?= Html::activeLabel($profile, 'modulkassa_password') ?>
    </div>
    <div class="row_input">
        <?= Html::activeTextInput($profile, 'modulkassa_password'); ?>
        <?= Html::error($profile, 'modulkassa_password',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($profile, 'modulkassa_password')->end() ?>
</section>

<section class="row">
    <?= $form->field($profile, 'modulkassa_retail_point_uuid')->begin() ?>
    <div class="row_label">
        <?= Html::activeLabel($profile, 'modulkassa_retail_point_uuid') ?>
    </div>
    <div class="row_input">
        <?= Html::activeTextInput($profile, 'modulkassa_retail_point_uuid'); ?>
        <?= Html::error($profile, 'modulkassa_retail_point_uuid',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($profile, 'modulkassa_retail_point_uuid')->end() ?>
</section>