<?php

/* @var $form \yii\widgets\ActiveForm */
/* @var $field string */
/* @var $phones \frontend\modules\onlinecashbox\models\profiles\orangeDataPhones\Phone[] */
/* @var $profile \frontend\components\onlineCashbox\interfaces\ProfileOrangeData */

/* @var $this \yii\web\View */

use wbraganca\dynamicform\DynamicFormWidget;
use yii\helpers\Html;

$widgetContainer = 'dynamicform_wrapper_' . $field;
$widgetBody      = 'form-options-body_' . $field;
$widgetItem      = 'form-options-item_' . $field;

?>

<section class="row">


    <?php DynamicFormWidget::begin([
        'widgetContainer' => $widgetContainer,
        'widgetBody'      => '.' . $widgetBody,
        'widgetItem'      => '.' . $widgetItem,
        'min'             => 1,
        'insertButton'    => '.add-item',
        'deleteButton'    => '.delete-item',
        'model'           => $phones[0],
        'formId'          => 'dynamic-form',
        'formFields'      => [
            'phone',
        ],
    ]); ?>

    <div class="row_label"><?= Html::activeLabel($profile, $field) ?></div>
    <div class="row_input">
        <table style="width: 100%;" class="<?= $widgetBody ?> phone_line">
            <? foreach ($phones as $index => $phone): ?>
                <tr class="<?= $widgetItem ?>" style="margin-bottom: 10px; display: inline-block; width: 100%">

                    <td style="display: inline-block; width: 90%">
                        <?= $form->field($phone, "[{$index}]phone", [
                            'options' => [
                                'class' => 'help-block help-block-error',
                                'style' => 'color:red',
                            ],
                        ])->label(false)->textInput([
                            'class'       => 'mask_phone',
                            'maxlength'   => 30,
                            'placeholder' => '+_(___)___-____'
                        ]); ?>
                    </td>
                    <td style="display: inline-block; width: 9%">
                        <a style="margin-left: 10px; color: red; cursor: pointer" class="delete-item"><?= t('online_cashbox', 'Delete') ?></a>
                    </td>

                </tr>
            <? endforeach; ?>
            <? unset($index) ?>
        </table>
        <a style="margin-bottom: 0;" class="add-distribution-circle add-phone add-item">
            <?= t('online_cashbox', 'Add phone') ?>
        </a>
    </div>
    <?php DynamicFormWidget::end(); ?>
</section>

