<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $form ActiveForm */
/* @var $profile \frontend\modules\onlinecashbox\models\profiles\ProfileOrangeData */
/* @var $this \yii\web\View */

?>

<?= $this->render('_profile-base', [
    'profile' => $profile,
    'form'    => $form,
]) ?>


<section class="row">
    <?= $form->field($profile, 'inn')->begin() ?>
    <div class="row_label">
        <?= Html::activeLabel($profile, 'inn') ?>
    </div>
    <div class="row_input">
        <?= Html::activeTextInput($profile, 'inn'); ?>
        <?= Html::error($profile, 'inn',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($profile, 'inn')->end() ?>
</section>

<section class="row">
    <?= $form->field($profile, 'sign_pkey')->begin() ?>
    <div class="row_label">
        <?= Html::activeLabel($profile, 'sign_pkey') ?>
    </div>
    <div class="row_input">
        <?= Html::activeFileInput($profile, 'sign_pkey'); ?>
        <?= Html::error($profile, 'sign_pkey',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($profile, 'sign_pkey')->end() ?>
</section>

<section class="row">
    <?= $form->field($profile, 'ssl_client_key')->begin() ?>
    <div class="row_label">
        <?= Html::activeLabel($profile, 'ssl_client_key') ?>
    </div>
    <div class="row_input">
        <?= Html::activeFileInput($profile, 'ssl_client_key'); ?>
        <?= Html::error($profile, 'ssl_client_key',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($profile, 'ssl_client_key')->end() ?>
</section>

<section class="row">
    <?= $form->field($profile, 'ssl_client_crt')->begin() ?>
    <div class="row_label">
        <?= Html::activeLabel($profile, 'ssl_client_crt') ?>
    </div>
    <div class="row_input">
        <?= Html::activeFileInput($profile, 'ssl_client_crt'); ?>
        <?= Html::error($profile, 'ssl_client_crt',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($profile, 'ssl_client_crt')->end() ?>
</section>

<section class="row">
    <?= $form->field($profile, 'ssl_ca_cert')->begin() ?>
    <div class="row_label">
        <?= Html::activeLabel($profile, 'ssl_ca_cert') ?>
    </div>
    <div class="row_input">
        <?= Html::activeFileInput($profile, 'ssl_ca_cert'); ?>
        <?= Html::error($profile, 'ssl_ca_cert',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($profile, 'ssl_ca_cert')->end() ?>
</section>

<section class="row">
    <?= $form->field($profile, 'ssl_client_crt_pass')->begin() ?>
    <div class="row_label">
        <?= Html::activeLabel($profile, 'ssl_client_crt_pass') ?>
    </div>
    <div class="row_input">
        <?= Html::activeTextInput($profile, 'ssl_client_crt_pass'); ?>
        <?= Html::error($profile, 'ssl_client_crt_pass',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($profile, 'ssl_client_crt_pass')->end() ?>
</section>




<section class="row">
    <?= $form->field($profile, 'payment_agent_operation')->begin() ?>
    <div class="row_label">
        <?= Html::activeLabel($profile, 'payment_agent_operation') ?>
    </div>
    <div class="row_input">
        <?= Html::activeTextInput($profile, 'payment_agent_operation'); ?>
        <?= Html::error($profile, 'payment_agent_operation',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($profile, 'payment_agent_operation')->end() ?>
</section>

<section class="row">
    <?= $form->field($profile, 'payment_operator_name')->begin() ?>
    <div class="row_label">
        <?= Html::activeLabel($profile, 'payment_operator_name') ?>
    </div>
    <div class="row_input">
        <?= Html::activeTextInput($profile, 'payment_operator_name'); ?>
        <?= Html::error($profile, 'payment_operator_name',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($profile, 'payment_operator_name')->end() ?>
</section>

<section class="row">
    <?= $form->field($profile, 'payment_operator_address')->begin() ?>
    <div class="row_label">
        <?= Html::activeLabel($profile, 'payment_operator_address') ?>
    </div>
    <div class="row_input">
        <?= Html::activeTextInput($profile, 'payment_operator_address'); ?>
        <?= Html::error($profile, 'payment_operator_address',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($profile, 'payment_operator_address')->end() ?>
</section>

<section class="row">
    <?= $form->field($profile, 'payment_operator_inn')->begin() ?>
    <div class="row_label">
        <?= Html::activeLabel($profile, 'payment_operator_inn') ?>
    </div>
    <div class="row_input">
        <?= Html::activeTextInput($profile, 'payment_operator_inn'); ?>
        <?= Html::error($profile, 'payment_operator_inn',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($profile, 'payment_operator_inn')->end() ?>
</section>



<?= $this->render('phones/_phones', [
    'form'    => $form,
    'field'   => 'payment_transfer_operator_phones',
    'profile' => $profile,
    'phones'  => $profile->getPayment_transfer_operator_phones()
]) ?>

<?= $this->render('phones/_phones', [
    'form'    => $form,
    'field'   => 'payment_agent_phones',
    'profile' => $profile,
    'phones'  => $profile->getPayment_agent_phones()
]) ?>

<?= $this->render('phones/_phones', [
    'form'    => $form,
    'field'   => 'payment_operator_phones',
    'profile' => $profile,
    'phones'  => $profile->getPayment_operator_phones()
]) ?>

<?= $this->render('phones/_phones', [
    'form'    => $form,
    'field'   => 'supplier_phones',
    'profile' => $profile,
    'phones'  => $profile->getSupplier_phones()
]) ?>



