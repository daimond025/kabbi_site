<?php

/* @var $profile \frontend\modules\onlinecashbox\models\profiles\ProfileBase */
/* @var $config \frontend\modules\onlinecashbox\models\Config */
/* @var $typesOnlineCashbox array */

/* @var $this \yii\web\View */


\frontend\modules\onlinecashbox\assets\OnlineCashboxAsset::register($this);

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\modules\onlinecashbox\models\profiles\ProfileDisabled;

$settings = [
    'errorCssClass'          => 'input_error',
    'successCssClass'        => 'input_success',
    'options'                => [
        'enctype' => 'multipart/form-data',
    ],
    'enableClientValidation' => true,
    'enableAjaxValidation'   => false,
    'id'                     => 'dynamic-form',
];

if (!$profile->id) {
    $settings = array_merge($settings, ['action' => '/onlinecashbox/profile/create']);
}


$form = ActiveForm::begin($settings);

?>

<section class="row">
    <?= $form->field($profile, 'type_id')->begin() ?>
    <div class="row_label">
        <?= Html::activeLabel($profile, 'type_id') ?>
    </div>
    <div class="row_input">
        <?= Html::activeDropDownList($profile, 'type_id', $typesOnlineCashbox, [
            'class'    => 'default_select',
            'disabled' => (bool)$profile->id,
        ]) ?>
        <?= Html::error($profile, 'type_id',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($profile, 'type_id')->end() ?>
</section>


<?= $this->render('_profile', compact('form', 'profile')) ?>

<section class="submit_form">
    <?php if ($profile->id): ?>
        <div class="delete-cashbox-profile">
            <a class="button red_button js-confirm-window"><?= t('online_cashbox', 'Delete'); ?></a>
        </div>
    <?php endif; ?>
    <?= Html::submitInput(t('online_cashbox', 'Save')) ?>
</section>
<div style="display: none">
    <div id="delete-confirm">
        <h2> <?php echo t('online_cashbox', 'Are you sure?') ?></h2>
        <div class="confirm-buttons">
            <a class="button js-close-confirm-window"><?php echo t('online_cashbox', 'Cancel') ?></a>
            <?= Html::a(
                t('online_cashbox', 'Delete'),
                ['delete', 'id' => $profile->id],
                ['id' => 'item-delete', 'class' => 'button red_button']
            ) ?>
        </div>
    </div>
</div>

<? ActiveForm::end() ?>

