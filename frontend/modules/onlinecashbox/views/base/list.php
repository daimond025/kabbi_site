<?php

use yii\helpers\Html;

$this->title = Yii::t('online_cashbox', 'Online cashboxs');

?>

<?= Html::tag('h1', Html::encode($this->title)); ?>

<a href="/onlinecashbox/profile/list"><?= t('online_cashbox', 'List of cashier profiles') ?></a><br>
<a href="/onlinecashbox/config/list"><?= t('online_cashbox', 'List of cash registers') ?></a>