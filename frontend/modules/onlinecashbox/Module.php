<?php

namespace frontend\modules\onlinecashbox;

use app\components\behavior\ControllerBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * onlineСashbox module definition class
 *
 * @mixin ControllerBehavior
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\onlinecashbox\controllers';

    public function behaviors()
    {
        return ArrayHelper::merge([
            'common' => [
                'class' => ControllerBehavior::className(),
            ],
        ], parent::behaviors());
    }

    public function beforeAction($action)
    {
        $this->commonBeforeAction();

        return parent::beforeAction($action);
    }

}
