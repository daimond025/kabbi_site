<?php

namespace frontend\modules\onlinecashbox\models\search;


use frontend\modules\onlinecashbox\components\services\OnlineCashboxService;
use yii\base\Model;
use yii\data\ArrayDataProvider;

class ConfigSearch extends Model
{

    const PAGE_SIZE = 20;

    /* @var $onlineCashboxService OnlineCashboxService */
    protected $onlineCashboxService;

    public function init()
    {
        parent::init();

        $this->onlineCashboxService = \Yii::$container->get(OnlineCashboxService::class);
    }


    public function rules()
    {
        return [];
    }

    public function search()
    {

        $configs = $this->onlineCashboxService->getConfigsCurrentTenant();

        return new ArrayDataProvider([
            'key' => 'id',
            'allModels'  => $configs,
            //            'sort'       => [
            //                'defaultOrder' => [
            //                    'name' => SORT_ASC,
            //                ],
            //            ],
            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);
    }

}