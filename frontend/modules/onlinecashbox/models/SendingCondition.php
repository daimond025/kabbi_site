<?php

namespace frontend\modules\onlinecashbox\models;


class SendingCondition
{
    const CASH = 1;
    const PERSONAL = 2;
    const CORP = 4;
    const CARD = 8;
    const BORDUR = 16;

    protected $flags;

    public static function getVariants()
    {
        return [
            self::CASH     => t('online_cashbox', 'Sending cash when paying for the order'),
            self::PERSONAL => t('online_cashbox', 'Sending when paying for an order from a personal account'),
            self::CORP     => t('online_cashbox', 'Sending when paying for an order with a corporate account'),
            self::CARD     => t('online_cashbox', 'Sending by credit card payment'),
            self::BORDUR   => t('online_cashbox', 'Sending when creating an order from the curb'),
        ];
    }


    public function isExistFlag($flag)
    {
        return (bool)($this->flags & $flag);
    }

    public function getCodedFlags()
    {
        return $this->flags;
    }

    public function clearFlags()
    {
        $this->flags = null;
    }

    public function getArrayFlags()
    {
        $flags = array_keys(self::getVariants());

        $result = [];
        foreach ($flags as $flag) {
            if ($this->isExistFlag($flag)) {
                $result[] = $flag;
            }
        }
        return $result;
    }

    public function setFlag($flag)
    {
        if ($this->flags) {
            $this->flags |= $flag;
        } else {
            $this->flags = $flag;
        }
    }

    public function deleteFlag($flag)
    {
        $this->flags &= ~$flag;
    }
}