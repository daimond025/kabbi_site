<?php

namespace frontend\modules\onlinecashbox\models\profiles;


use frontend\components\onlineCashbox\interfaces\ProfileBaseInterface;
use paymentGate\models\Disabled;
use yii\base\Model;

abstract class ProfileBase extends Model implements ProfileBaseInterface
{

    const TYPE_MODULKASSA = 1;
    const TYPE_ORANGE_DATA = 2;

    const SCENARIO_UPDATE = 'scenario_update';

    public $id;
    public $tenant_id;
    public $type_id;
    public $name;
    public $debug;

    public function rules()
    {

        return [
            [['name', 'debug'], 'required'],
            ['type_id', 'required', 'except' => ProfileBase::SCENARIO_UPDATE],
            [['type_id', 'debug'], 'integer'],
            [['name'], 'string', 'max' => 255],
            ['debug', 'in', 'range' => [0, 1]],
        ];
    }

    public function formName()
    {
        return 'ProfileOnlineCashbox';
    }

    public function afterValidate()
    {
        $this->tenant_id = user()->tenant_id;
    }

    public function attributeLabels()
    {
        return [
            'type_id' => t('online_cashbox', 'Type of online checkout'),
            'name'    => t('online_cashbox', 'Name'),
            'debug'   => t('online_cashbox', 'Mode'),
        ];
    }

    public function getViewPath()
    {
        $reflector = new \ReflectionClass($this);

        return mb_strtolower($reflector->getShortName());
    }


    abstract public function save();

    abstract public function update();

    abstract public function delete();

    public function getTenantId()
    {
        if (!$this->hasErrors()) {
            return $this->tenant_id;
        }

        return null;
    }

    public function getTypeId()
    {
        if (!$this->hasErrors()) {
            return $this->type_id;
        }

        return null;
    }

    public function getName()
    {
        if (!$this->hasErrors()) {
            return $this->name;
        }

        return null;
    }

    public function getDebug()
    {
        if (!$this->hasErrors()) {
            return $this->debug;
        }

        return null;
    }

}