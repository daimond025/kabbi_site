<?php
namespace frontend\modules\onlinecashbox\models\profiles\orangeDataPhones;


use yii\base\Model;

abstract class Phone extends Model
{
    public $phone;


    public function rules()
    {
        return [
            [['phone'], 'required'],
            [
                ['phone'],
                'filter',
                'filter' => function ($value) {
                    preg_match_all('/\d+/', $value, $matches);
                    return '+' . implode('', $matches[0]);
                },
                'message' => t('online_cashbox', 'Format "{attribute}" entered incorrectly', [
                    'attribute' => $this->getAttributeLabel('phone')
                ])
            ],
            ['phone', 'string', 'max' => 19]
        ];
    }
}