<?php

namespace frontend\modules\onlinecashbox\models\profiles\orangeDataPhones;

class PaymentAgentPhone extends Phone
{

    public function attributeLabels()
    {
        return [
            'phone' => t('online_cashbox', 'Phone of the payment agent')
        ];
    }

}