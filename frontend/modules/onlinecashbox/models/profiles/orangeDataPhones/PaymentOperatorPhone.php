<?php

namespace frontend\modules\onlinecashbox\models\profiles\orangeDataPhones;

class PaymentOperatorPhone extends Phone
{
    public function attributeLabels()
    {
        return [
            'phone' => t('online_cashbox', 'Telephone operator for receiving payments')
        ];
    }
}