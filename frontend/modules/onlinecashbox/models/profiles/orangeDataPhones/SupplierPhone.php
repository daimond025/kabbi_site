<?php
namespace frontend\modules\onlinecashbox\models\profiles\orangeDataPhones;



class SupplierPhone extends Phone
{
    public function attributeLabels()
    {
        return [
            'phone' => t('online_cashbox', 'Provider phone number')
        ];
    }
}