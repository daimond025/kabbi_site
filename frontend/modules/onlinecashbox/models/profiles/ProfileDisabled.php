<?php
namespace frontend\modules\onlinecashbox\models\profiles;


class ProfileDisabled extends ProfileBase
{
    const TYPE_ID = 0;


    public function init()
    {
        $this->type_id = static::TYPE_ID;
    }

    public function save()
    {
        return false;
    }

    public function update()
    {
        return false;
    }

    public function delete()
    {
        return false;
    }


}