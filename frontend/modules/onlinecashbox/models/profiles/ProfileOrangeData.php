<?php

namespace frontend\modules\onlinecashbox\models\profiles;


use common\components\extensions\ModelMultiple;
use frontend\components\onlineCashbox\OnlineCashboxApi;
use frontend\components\onlineCashbox\interfaces\ProfileOrangeData as ProfileOrangeDataI;
use frontend\modules\onlinecashbox\components\behaviors\FileBehavior;
use frontend\modules\onlinecashbox\models\profiles\orangeDataPhones\PaymentAgentPhone;
use frontend\modules\onlinecashbox\models\profiles\orangeDataPhones\PaymentOperatorPhone;
use frontend\modules\onlinecashbox\models\profiles\orangeDataPhones\PaymentTransferOperatorPhone;
use frontend\modules\onlinecashbox\models\profiles\orangeDataPhones\Phone;
use frontend\modules\onlinecashbox\models\profiles\orangeDataPhones\SupplierPhone;
use yii\base\Model;
use yii\web\UploadedFile;

class ProfileOrangeData extends ProfileBase implements ProfileOrangeDataI
{
    const TYPE_ID = 2;

    const PHONES_DELIMITER = ';';

    public $inn;
    public $sign_pkey;
    public $ssl_client_key;
    public $ssl_client_crt;
    public $ssl_ca_cert;
    public $ssl_client_crt_pass;
    public $payment_agent_operation;
    public $payment_operator_name;
    public $payment_operator_address;
    public $payment_operator_inn;

    /* @var $onlineCashboxApi OnlineCashboxApi */
    protected $onlineCashboxApi;

    /* @var $supplierPhones SupplierPhone[] */
    protected $supplierPhones;
    /* @var $paymentOperatorPhones PaymentTransferOperatorPhone[] */
    protected $paymentOperatorPhones;
    /* @var $paymentAgentPhones PaymentOperatorPhone[] */
    protected $paymentAgentPhones;
    /* @var $paymentTransferOperatorPhones PaymentTransferOperatorPhone[] */
    protected $paymentTransferOperatorPhones;


    public function init()
    {
        $this->onlineCashboxApi = \Yii::$app->onlineСashboxApi;
        $this->type_id          = static::TYPE_ID;
    }

    public function behaviors()
    {
        return [
            [
                'class'       => FileBehavior::className(),
                'fieldsModel' => [
                    'sign_pkey',
                    'ssl_ca_cert',
                    'ssl_client_key',
                    'ssl_client_crt',
                ],
            ],
        ];
    }

    public function rules()
    {
        $rules = parent::rules();

        return array_merge($rules, [
            [
                [
                    'payment_agent_operation',
                    'payment_operator_name',
                    'payment_operator_address',
                    'payment_operator_inn',
                    'ssl_client_crt_pass',
                ],
                'trim'
            ],
            [
                [
                    'inn',
                    'payment_agent_operation',
                    'payment_operator_name',
                    'payment_operator_address',
                    'payment_operator_inn',
                ],
                'required',
            ],
            [
                [
                    'ssl_client_crt_pass',
                    'payment_operator_address',
                ],
                'string',
                'max' => 244,
            ],
            [
                ['payment_agent_operation'],
                'string',
                'max' => 24,
            ],
            [
                ['payment_operator_name'],
                'string',
                'max' => 64,
            ],
            [
                ['payment_operator_inn'],
                'match',
                'pattern' => '/^\d{12}$|^\d{10}$/',
                'message' => t('online_cashbox', 'Format "{attribute}" entered incorrectly', [
                    'attribute' => $this->getAttributeLabel('payment_operator_inn')
                ])
            ],
            [
                ['ssl_ca_cert'],
                'file',
                'extensions'               => ['pem'],
                'maxSize'                  => getUploadMaxFileSize(),
                'checkExtensionByMimeType' => false,
                'tooBig'                   => t(
                    'online_cashbox',
                    'The file size must be less than {file_size}Mb', [
                        'file_size' => getUploadMaxFileSize(true),
                    ]
                ),
                'wrongExtension'           => t(
                    'online_cashbox',
                    'Only files with the following extensions are allowed: {extensions}'
                ),
            ],
            [
                ['ssl_client_key'],
                'file',
                'extensions'               => ['key'],
                'maxSize'                  => getUploadMaxFileSize(),
                'checkExtensionByMimeType' => false,
                'tooBig'                   => t(
                    'online_cashbox',
                    'The file size must be less than {file_size}Mb', [
                        'file_size' => getUploadMaxFileSize(true),
                    ]
                ),
                'wrongExtension'           => t(
                    'online_cashbox',
                    'Only files with the following extensions are allowed: {extensions}'
                ),
            ],
            [
                ['ssl_client_crt'],
                'file',
                'extensions'               => ['crt'],
                'maxSize'                  => getUploadMaxFileSize(),
                'checkExtensionByMimeType' => false,
                'tooBig'                   => t(
                    'online_cashbox',
                    'The file size must be less than {file_size}Mb', [
                        'file_size' => getUploadMaxFileSize(true),
                    ]
                ),
                'wrongExtension'           => t(
                    'online_cashbox',
                    'Only files with the following extensions are allowed: {extensions}'
                ),
            ],
            [
                ['sign_pkey'],
                'file',
                'extensions'               => ['crt', 'pem'],
                'maxSize'                  => getUploadMaxFileSize(),
                'checkExtensionByMimeType' => false,
                'tooBig'                   => t(
                    'online_cashbox',
                    'The file size must be less than {file_size}Mb', [
                        'file_size' => getUploadMaxFileSize(true),
                    ]
                ),
                'wrongExtension'           => t(
                    'online_cashbox',
                    'Only files with the following extensions are allowed: {extensions}'
                ),
            ],
            [
                ['sign_pkey', 'ssl_client_crt', 'ssl_client_key', 'ssl_ca_cert'],
                'file',
                'skipOnEmpty'    => false,
                'uploadRequired' => t('online_cashbox', 'Download the file.'),
                'except'         => static::SCENARIO_UPDATE,
            ],
            [
                ['sign_pkey'],
                'filter',
                'filter' => function ($value) {
                    if ($value instanceof UploadedFile) {
                        return file_get_contents($value->tempName);
                    }

                    return null;
                },
            ],
            [
                ['ssl_client_key'],
                'filter',
                'filter' => function ($value) {
                    if ($value instanceof UploadedFile) {
                        return file_get_contents($value->tempName);
                    }

                    return null;
                },
            ],
            [
                ['ssl_client_crt'],
                'filter',
                'filter' => function ($value) {
                    if ($value instanceof UploadedFile) {
                        return file_get_contents($value->tempName);
                    }

                    return null;
                },
            ],
            [
                ['ssl_ca_cert'],
                'filter',
                'filter' => function ($value) {
                    if ($value instanceof UploadedFile) {
                        return file_get_contents($value->tempName);
                    }

                    return null;
                },
            ],

            [
                ['ssl_client_key'],
                function ($attribute) {
                    if (!openssl_x509_check_private_key($this->ssl_client_crt, $this->ssl_client_key)) {
                        $this->addError($attribute, t('online_cashbox', 'Invalid key file'));
                    }
                },
            ],
        ]);

    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();

        return array_merge($labels, [
            'inn'                              => t('online_cashbox', 'INN'),
            'sign_pkey'                        => t('online_cashbox', 'Private key'),
            'ssl_client_key'                   => t('online_cashbox', 'Client private key for SSL'),
            'ssl_client_crt'                   => t('online_cashbox', 'Client certificate for SSL'),
            'ssl_ca_cert'                      => t('online_cashbox', 'Cacert for ssl'),
            'ssl_client_crt_pass'              => t('online_cashbox', 'Password for client certificate for ssl'),
            'payment_transfer_operator_phones' => t('online_cashbox', 'Telephone operator'),
            'payment_agent_operation'          => t('online_cashbox', 'Payment agent operation'),
            'payment_agent_phones'             => t('online_cashbox', 'Phone of the payment agent'),
            'payment_operator_phones'          => t('online_cashbox', 'Telephone operator for receiving payments'),
            'payment_operator_name'            => t('online_cashbox', 'Name of the translation operator'),
            'payment_operator_address'         => t('online_cashbox', 'Address of the transfer operator'),
            'payment_operator_inn'             => t('online_cashbox', 'INN of the translation operator'),
            'supplier_phones'                  => t('online_cashbox', 'Provider phone number'),
        ]);
    }

    public function save()
    {
        return $this->onlineCashboxApi->createProfilesOrangeData($this);
    }

    public function update()
    {
        return $this->onlineCashboxApi->updateProfilesOrangeData($this, $this->id);

    }

    public function delete()
    {
        return $this->onlineCashboxApi->deleteProfile($this->id);
    }


    public function beforeValidate()
    {
        $valid = $this->setPayment_operator_phones();
        $valid = $this->setSupplier_phones() && $valid;
        $valid = $this->setPayment_agent_phones() && $valid;
        $valid = $this->setPayment_transfer_operator_phones() && $valid;

        return parent::beforeValidate() && $valid;
    }


    public function getPayment_transfer_operator_phones()
    {
        if (!$this->paymentTransferOperatorPhones) {
            $this->paymentTransferOperatorPhones[] = new PaymentTransferOperatorPhone();
        }

        return $this->paymentTransferOperatorPhones;
    }
    public function setPayment_transfer_operator_phones($values = null)
    {
        return $this->setPhones(
            PaymentTransferOperatorPhone::class, 'paymentTransferOperatorPhones', $values
        );
    }


    public function getPayment_agent_phones()
    {
        if (!$this->paymentAgentPhones) {
            $this->paymentAgentPhones[] = new PaymentAgentPhone();
        }

        return $this->paymentAgentPhones;
    }
    public function setPayment_agent_phones($values = null)
    {
        return $this->setPhones(
            PaymentAgentPhone::class, 'paymentAgentPhones', $values
        );
    }


    public function getPayment_operator_phones()
    {
        if (!$this->paymentOperatorPhones) {
            $this->paymentOperatorPhones[] = new PaymentOperatorPhone();
        }

        return $this->paymentOperatorPhones;
    }
    public function setPayment_operator_phones($values = null)
    {
        return $this->setPhones(
            PaymentOperatorPhone::class, 'paymentOperatorPhones', $values
        );
    }


    public function getSupplier_phones()
    {
        if (!$this->supplierPhones) {
            $this->supplierPhones[] = new SupplierPhone();
        }

        return $this->supplierPhones;
    }
    public function setSupplier_phones($values = null)
    {
        return $this->setPhones(
            SupplierPhone::class, 'supplierPhones', $values
        );
    }


    protected function setPhones($class, $nameAttribute, $values)
    {
        $isValid = true;
        if (\Yii::$app->request->isPost) {
            $phones = ModelMultiple::createMultiple($class, []);
            Model::loadMultiple($phones, post());

            $isValid = Model::validateMultiple($phones);
        } else {

            $phonesFromDb = explode(self::PHONES_DELIMITER, $values);
            $phones       = [];
            foreach ($phonesFromDb as $phone) {
                $phones[] = new $class(['phone' => $phone]);
            }

        }

        $this->$nameAttribute = $phones;

        return $isValid;
    }


    public function getInn()
    {
        if (!$this->hasErrors()) {
            return $this->inn;
        }

        return null;
    }

    public function getSslClientCrtPass()
    {
        if (!$this->hasErrors()) {
            return $this->ssl_client_crt_pass;
        }

        return null;
    }

    public function getSignPkey()
    {
        if (!$this->hasErrors()) {
            return $this->sign_pkey;
        }

        return null;
    }

    public function getSslClientKey()
    {
        if (!$this->hasErrors()) {
            return $this->ssl_client_key;
        }

        return null;
    }

    public function getSslClientCrt()
    {
        if (!$this->hasErrors()) {
            return $this->ssl_client_crt;
        }

        return null;
    }

    public function getSslCaCert()
    {
        if (!$this->hasErrors()) {
            return $this->ssl_ca_cert;
        }

        return null;
    }

    public function getPaymentAgentOperation()
    {
        if (!$this->hasErrors()) {
            return $this->payment_agent_operation;
        }

        return null;
    }

    public function getPaymentOperatorName()
    {
        if (!$this->hasErrors()) {
            return $this->payment_operator_name;
        }

        return null;
    }

    public function getPaymentOperatorAddress()
    {
        if (!$this->hasErrors()) {
            return $this->payment_operator_address;
        }

        return null;
    }

    public function getPaymentOperatorInn()
    {
        if (!$this->hasErrors()) {
            return $this->payment_operator_inn;
        }

        return null;
    }


    public function getSupplierPhones()
    {
        if (!$this->hasErrors()) {
            return $this->getStringPhones($this->supplierPhones);
        }

        return null;
    }

    public function getPaymentAgentPhones()
    {
        if (!$this->hasErrors()) {
            return $this->getStringPhones($this->paymentAgentPhones);
        }

        return null;
    }

    public function getPaymentOperatorPhones()
    {
        if (!$this->hasErrors()) {
            return $this->getStringPhones($this->paymentOperatorPhones);
        }

        return null;
    }

    public function getPaymentTransferOperatorPhones()
    {
        if (!$this->hasErrors()) {
            return $this->getStringPhones($this->paymentTransferOperatorPhones);
        }

        return null;
    }

    /**
     * @param $phones Phone[]
     *
     * @return string
     */
    protected function getStringPhones($phones)
    {
        $res = '';
        foreach ($phones as $phone) {
            $res .= $phone->phone . self::PHONES_DELIMITER;
        }

        $pattern = sprintf('#%s$#', self::PHONES_DELIMITER);

        return preg_replace($pattern, '', $res);
    }


}