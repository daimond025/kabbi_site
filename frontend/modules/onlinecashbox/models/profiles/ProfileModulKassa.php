<?php

namespace frontend\modules\onlinecashbox\models\profiles;


use frontend\components\onlineCashbox\interfaces\ProfileModulKassaInterface;
use frontend\components\onlineCashbox\OnlineCashboxApi;

class ProfileModulKassa extends ProfileBase implements ProfileModulKassaInterface
{
    const TYPE_ID = 1;

    public $modulkassa_email;
    public $modulkassa_password;
    public $modulkassa_retail_point_uuid;

    /* @var $onlineCashboxApi OnlineCashboxApi */
    protected $onlineCashboxApi;

    public function init()
    {
        $this->onlineCashboxApi = \Yii::$app->onlineСashboxApi;
        $this->type_id = static::TYPE_ID;
    }

    public function rules()
    {
        $rules = parent::rules();

        return array_merge($rules, [
            [['modulkassa_email', 'modulkassa_retail_point_uuid'], 'required'],
            [['modulkassa_email'], 'email'],
            [['modulkassa_password', 'modulkassa_retail_point_uuid', 'modulkassa_email'], 'string', 'max' => 255],
        ]);

    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();

        return array_merge($labels, [
            'modulkassa_email'             => t('online_cashbox', 'Email'),
            'modulkassa_password'          => t('online_cashbox', 'Password'),
            'modulkassa_retail_point_uuid' => t('online_cashbox', 'Cashbox key (uuid)'),
        ]);
    }


    public function save()
    {
        return $this->onlineCashboxApi->createProfilesModulKassa($this);
    }

    public function update()
    {
        return $this->onlineCashboxApi->updateProfilesModulKassa($this, $this->id);

    }

    public function delete()
    {
        return $this->onlineCashboxApi->deleteProfile($this->id);
    }


    public function getModulkassaEmail()
    {
        if (!$this->hasErrors()) {
            return $this->modulkassa_email;
        }

        return null;
    }

    public function getModulkassaPassword()
    {
        if (!$this->hasErrors()) {
            return $this->modulkassa_password;
        }

        return null;
    }

    public function getModulkassaRetailPointUuid()
    {
        if (!$this->hasErrors()) {
            return $this->modulkassa_retail_point_uuid;
        }

        return null;
    }

}