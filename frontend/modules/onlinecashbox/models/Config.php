<?php

namespace frontend\modules\onlinecashbox\models;


use common\modules\city\models\City;
use common\modules\employee\models\position\Position;
use frontend\components\onlineCashbox\interfaces\ConfigInterface;
use frontend\modules\employee\components\position\PositionService;
use frontend\modules\onlinecashbox\components\services\OnlineCashboxService;
use frontend\modules\onlinecashbox\models\profiles\ProfileBase;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class Config extends Model implements ConfigInterface
{

    const TAXATION_SYSTEM_OSN = 0;
    const TAXATION_SYSTEM_USN = 1;
    const TAXATION_SYSTEM_USN_DIFF = 2;
    const TAXATION_SYSTEM_ENVD = 3;
    const TAXATION_SYSTEM_ESN = 4;
    const TAXATION_SYSTEM_PATENT = 5;

    const TAX_18 = 1;
    const TAX_10 = 2;
    const TAX_18_118 = 3;
    const TAX_10_110 = 4;
    const TAX_0 = 5;
    const TAX_NO = 6;


    public $id;
    public $city_id;
    public $tenant_id;
    public $position_id;
    public $profile_id;
    public $product_name;
    public $taxation_system;
    public $tax;

    protected $profile;
    protected $city;
    protected $position;

    /* @var $sendingCondition SendingCondition */
    protected $sendingCondition;

    /* @var $onlineCashboxApi OnlineCashboxService */
    protected $onlineCashboxService;
    /* @var $positionService PositionService */
    protected $positionService;

    public function __construct(array $config = [])
    {
        $this->sendingCondition = new SendingCondition();
        parent::__construct($config);
    }

    public function init()
    {
        parent::init();
        $this->onlineCashboxService = \Yii::$container->get(OnlineCashboxService::class);
        $this->positionService      = \Yii::$container->get(PositionService::class);
        $this->positionService->setTenantId(user()->tenant_id);
    }

    public function rules()
    {
        return [
            [
                ['city_id', 'position_id', 'product_name', 'tax', 'profile_id', 'sending_condition'],
                'required',
            ],
            [['city_id', 'position_id', 'profile_id', 'tenant_id'], 'integer'],
            [
                'sending_condition',
                function ($attribute) {
                    foreach ($this->$attribute as $flag) {
                        if (!in_array($flag, array_keys(SendingCondition::getVariants()))) {
                            $this->addError($attribute, t('online_cashbox', 'Value "{attribute}" is not true', [
                                'attribute' => $this->getAttributeLabel($attribute)
                            ]));
                        }
                    }
                }
            ],
            [
                'tax',
                'in',
                'range' => [
                    self::TAX_18,
                    self::TAX_10,
                    self::TAX_18_118,
                    self::TAX_10_110,
                    self::TAX_0,
                    self::TAX_NO
                ]
            ],
            [
                'taxation_system',
                'in',
                'range' => [
                    self::TAXATION_SYSTEM_OSN,
                    self::TAXATION_SYSTEM_USN,
                    self::TAXATION_SYSTEM_USN_DIFF,
                    self::TAXATION_SYSTEM_ENVD,
                    self::TAXATION_SYSTEM_ESN,
                    self::TAXATION_SYSTEM_PATENT,
                ]
            ],
            [
                'taxation_system',
                'required',
                'when' => function (Config $config) {
                    return $config->getProfile()->type_id == ProfileBase::TYPE_ORANGE_DATA;
                },
                'whenClient' => sprintf("function (attribute, value) {
                                    return $('#config-profile_id option[selected]').val() == %d
                                }", ProfileBase::TYPE_ORANGE_DATA),
                'message' => t('online_cashbox', '«{attribute}» cannot be blank.',
                    ['attribute' => $this->getAttributeLabel('tax')])
            ],
            [['product_name'], 'string', 'max' => 255],
            [
                'position_id',
                function ($attribute) {
                    $positions = ArrayHelper::getColumn(
                        $this->positionService->getPositionsByCity($this->city_id), 'position_id'
                    );
                    if (!in_array($this->position_id, $positions)) {
                        $this->addError(
                            $attribute,
                            t('online_cashbox', 'This city does not have such a profession.')
                        );
                    }
                }
            ]

        ];
    }

    public function attributeLabels()
    {
        return [
            'product_name'      => t('online_cashbox', 'Product name'),
            'sending_condition' => t('online_cashbox', 'Sending condition check'),
            'taxation_system'   => t('online_cashbox', 'Taxation system'),
            'tax'               => t('online_cashbox', 'Tax'),
            'city_id'           => t('online_cashbox', 'City'),
            'position_id'       => t('online_cashbox', 'Position'),
            'profile_id'        => t('online_cashbox', 'Profile cashbox'),
        ];
    }


    public function beforeValidate()
    {

        $this->tenant_id = user()->tenant_id;
        return parent::beforeValidate();
    }


    public function setSending_condition($flags)
    {
        if (is_array($flags)) {
            foreach ($flags as $flag) {
                $this->sendingCondition->setFlag((int)$flag);
            }
        } else {
            $this->sendingCondition->setFlag((int)$flags);
        }
    }

    public function getSending_condition()
    {
        return $this->sendingCondition->getArrayFlags();
    }

    /**
     * @return null|City
     */
    public function getCity()
    {

        user()->getUserCityList();
        if (!$this->city) {
            $this->city = City::find()
                ->select(['city_id', 'name' . getLanguagePrefix()])
                ->where(['city_id' => $this->city_id])
                ->one();
        }
        return $this->city;
    }

    /**
     * @return null|Position
     */
    public function getPosition()
    {
        if (!$this->position) {
            $this->position = Position::find()
                ->where(['position_id' => $this->position_id])
                ->one();
        }
        return $this->position;
    }

    public function setProfile()
    {

        $profile = $this->onlineCashboxService->getProfile($this->profile_id);
        if ($profile->tenant_id == $this->tenant_id
            && $profile->id == $this->profile_id
        ) {
            $this->profile = $profile;
        }
    }

    /**
     * @return ProfileBase|null
     */
    public function getProfile()
    {
        if (!$this->profile) {
            $this->setProfile();
        }

        return $this->profile;
    }

    public function save()
    {
        return $this->onlineCashboxService->createConfig($this);
    }

    public function update()
    {
        return $this->onlineCashboxService->updateConfig($this, $this->id);
    }

    public function delete()
    {
        return $this->onlineCashboxService->deleteConfig($this->id);
    }

    public function clearSendingCondition()
    {
        $this->sendingCondition->clearFlags();
    }

    public function getCityId()
    {
        if (!$this->hasErrors()) {
            return $this->city_id;
        }

        return null;
    }

    public function getPositionId()
    {
        if (!$this->hasErrors()) {
            return $this->position_id;
        }

        return null;
    }

    public function getProfileId()
    {
        if (!$this->hasErrors()) {
            return $this->profile_id;
        }

        return null;
    }

    public function getProductName()
    {
        if (!$this->hasErrors()) {
            return $this->product_name;
        }

        return null;
    }

    public function getSendingCondition()
    {
        if (!$this->hasErrors()) {
            return $this->sendingCondition->getCodedFlags();
        }

        return null;
    }

    public function getTaxationSystem()
    {
        if (!$this->hasErrors()) {
            return $this->taxation_system;
        }

        return null;
    }

    public function getTax()
    {
        if (!$this->hasErrors()) {
            return $this->tax;
        }

        return null;
    }

    public function getTenantId()
    {
        if (!$this->hasErrors()) {
            return $this->tenant_id;
        }

        return null;
    }


}