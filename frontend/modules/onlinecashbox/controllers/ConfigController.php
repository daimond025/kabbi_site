<?php

namespace frontend\modules\onlinecashbox\controllers;


use frontend\modules\employee\components\position\PositionService;
use frontend\modules\onlinecashbox\components\datacentr\DataForms;
use frontend\modules\onlinecashbox\components\services\OnlineCashboxService;
use frontend\modules\onlinecashbox\components\services\ProcessedErrorService;
use frontend\modules\onlinecashbox\components\services\ProfileOnlineCashboxService;
use frontend\modules\onlinecashbox\models\Config;
use frontend\modules\onlinecashbox\models\search\ConfigSearch;
use GuzzleHttp\Exception\ClientException;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ConfigController extends Controller
{

    /* @var $positionService PositionService */
    protected $positionService;

    /* @var $onlineCashboxService OnlineCashboxService */
    protected $onlineCashboxService;

    protected $profileOnlineCashboxService;
    protected $processedErrorService;
    protected $dataForms;

    public function __construct(
        $id,
        Module $module,
        PositionService $positionService,
        OnlineCashboxService $onlineCashboxService,
        ProfileOnlineCashboxService $profileOnlineCashboxService,
        ProcessedErrorService $processedErrorService,
        DataForms $dataForms,
        array $config = []
    ) {
        $this->positionService = $positionService;
        $this->positionService->setTenantId(user()->tenant_id);

        $this->onlineCashboxService = $onlineCashboxService;

        $this->profileOnlineCashboxService = $profileOnlineCashboxService;
        $this->processedErrorService       = $processedErrorService;
        $this->dataForms                   = $dataForms;

        parent::__construct($id, $module, $config);
    }


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow'   => true,
                        'roles'   => ['onlineCashbox'],
                    ],
                ],
            ],
        ];
    }

    public function actionCreate()
    {
        $config    = new Config();
        $dataForm = $this->dataForms->getDataConfigForm($config);

        if (\Yii::$app->request->isPost) {
            $config->load(post());
            $dataForm = $this->dataForms->getDataConfigForm($config);
            if ($config->validate()) {
                try {
                    $config->save();
                    session()->setFlash('success', t('online_cashbox', 'Configuration saved successfully.'));

                    return $this->redirect(Url::to('/onlinecashbox/config/list'));
                } catch (\Exception $e) {
                    if ($e instanceof ClientException
                        && in_array($e->getResponse()->getStatusCode(),
                            ProcessedErrorService::PROCESSED_RESPONSE_CODE)) {
                        $this->processedErrorService->processedResponseError($e, $config);
                    } else {
                        session()->setFlash('error',
                            t('online_cashbox',
                                'An error occurred while working with the settings of online cash registers'));
                        \Yii::error($e, 'online_cashbox');

                        return $this->redirect('create');
                    }
                }
            }
        }

        return $this->render('create', [
            'config'         => $config,
            'cities'         => $dataForm['cities'],
            'profiles'       => $dataForm['profiles'],
            'positions'      => $dataForm['positions'],
            'profilesTypeId' => $dataForm['profilesTypeId'],
        ]);
    }

    public function actionUpdate($id)
    {

        $config    = $this->getConfig($id);
        $dataForm = $this->dataForms->getDataConfigForm($config);

        if (\Yii::$app->request->isPost) {
            $config->clearSendingCondition();
            $config->load(post());

            if ($config->validate()) {
                try {
                    $config->update();
                    session()->setFlash('success', t('online_cashbox', 'Configuration updated successfully.'));

                    return $this->redirect(Url::to(['/onlinecashbox/config/update', 'id' => $config->id]));
                } catch (\Exception $e) {
                    if ($e instanceof ClientException
                        && in_array($e->getResponse()->getStatusCode(),
                            ProcessedErrorService::PROCESSED_RESPONSE_CODE)) {
                        $this->processedErrorService->processedResponseError($e, $config);
                    } else {
                        session()->setFlash('error',
                            t('online_cashbox',
                                'An error occurred while working with the settings of online cash registers'));
                        \Yii::error($e, 'online_cashbox');

                        return $this->redirect('update');
                    }
                }
            }
        }

        return $this->render('update', [
            'config'         => $config,
            'cities'         => $dataForm['cities'],
            'profiles'       => $dataForm['profiles'],
            'positions'      => $dataForm['positions'],
            'profilesTypeId' => $dataForm['profilesTypeId'],
        ]);
    }

    public function actionGetPositions($id)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

        $positions = [];
        foreach ($this->getPositionMap($id) as $key => $position) {
            $positions[] = ['position_id' => $key, 'name' => $position];
        }

        return $positions;
    }

    public function actionList()
    {

        $search = new ConfigSearch();
        $arrayDataProvider = $search->search();

        return $this->render('list', [
            'arrayDataProvider' => $arrayDataProvider,
        ]);
    }

    public function actionDelete($id)
    {

        $config = $this->getConfig($id);

        try {
            $config->delete();
        } catch (\Exception $e) {
            session()->setFlash('error', t('online_cashbox', 'Uninstall error, contact the administrator.'));
            \Yii::error($e->getMessage(), 'online_cashbox');

            return $this->redirect(Url::to('/onlinecashbox/config/list'));
        }

        session()->setFlash('success', t('online_cashbox', 'Config successfully deleted.'));
        return $this->redirect(Url::to('/onlinecashbox/config/list'));
    }

    protected function getConfig($id)
    {
        /* @var $config Config */
        $config = $this->onlineCashboxService->getConfig($id);

        if ($config->tenant_id != user()->tenant_id) {
            throw new NotFoundHttpException();
        }

        return $config;
    }

    private function getPositionMap($cityId)
    {

        $positions = $this->positionService->getPositionsByCity($cityId);

        return empty($positions) ? [] : ArrayHelper::map($positions, 'position_id', 'name');
    }


}