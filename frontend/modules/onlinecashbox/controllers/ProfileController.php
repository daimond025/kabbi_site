<?php

namespace frontend\modules\onlinecashbox\controllers;


use frontend\modules\employee\components\position\PositionService;
use frontend\modules\onlinecashbox\components\services\OnlineCashboxService;
use frontend\modules\onlinecashbox\components\services\ProcessedErrorService;
use frontend\modules\onlinecashbox\components\services\ProfileOnlineCashboxService;
use frontend\modules\onlinecashbox\models\profiles\ProfileBase;
use frontend\modules\onlinecashbox\models\profiles\ProfileDisabled;
use frontend\modules\onlinecashbox\models\profiles\ProfileModulKassa;
use frontend\modules\onlinecashbox\models\search\ProfileOnlineCashboxSearch;
use GuzzleHttp\Exception\ClientException;
use yii\base\Module;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class ProfileController extends Controller
{

    /* @var $positionService PositionService */
    protected $positionService;

    /* @var $onlineCashboxService OnlineCashboxService */
    protected $onlineCashboxService;
    protected $profileOnlineCashboxService;
    protected $processedErrorService;

    public function __construct(
        $id,
        Module $module,
        PositionService $positionService,
        OnlineCashboxService $onlineCashboxService,
        ProfileOnlineCashboxService $profileOnlineCashboxService,
        ProcessedErrorService $processedErrorService,
        array $config = []
    ) {
        $this->positionService = $positionService;
        $this->positionService->setTenantId(user()->tenant_id);
        $this->onlineCashboxService        = $onlineCashboxService;
        $this->profileOnlineCashboxService = $profileOnlineCashboxService;
        $this->processedErrorService       = $processedErrorService;

        parent::__construct($id, $module, $config);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow'   => true,
                        'roles'   => ['onlineCashbox'],
                    ],
                ],
            ],
        ];
    }

    public function actionCreate()
    {
        $profile            = $this->profileOnlineCashboxService->getProfileModel(ProfileDisabled::TYPE_ID);
        $typesOnlineCashbox = $this->onlineCashboxService->getTypesOnlineCashbox();

        if (\Yii::$app->request->isPost) {
            $profile = $this->profileOnlineCashboxService->getProfileModel(
                post()['ProfileOnlineCashbox']['type_id']
            );

            $profile->load(post());

            if ($profile->validate()) {
                try {

                    $profile->save();
                    session()->setFlash('success', t('online_cashbox', 'Cashier profile successfully saved.'));

                    return $this->redirect(Url::to('/onlinecashbox/profile/list'));
                } catch (\Exception $e) {
                    if ($e instanceof ClientException
                        && in_array($e->getResponse()->getStatusCode(),
                            ProcessedErrorService::PROCESSED_RESPONSE_CODE)) {
                        $this->processedErrorService->processedResponseError($e, $profile);
                    } else {
                        session()->setFlash('error',
                            t('online_cashbox',
                                'An error occurred while working with the settings of online cash registers'));
                        \Yii::error($e->getMessage(), 'online_cashbox');

                        return $this->redirect('create');
                    }

                }
            }
        }


        return $this->render('create', [
            'profile'            => $profile,
            'typesOnlineCashbox' => $typesOnlineCashbox,
        ]);

    }


    public function actionUpdate($id)
    {
        /* @var $profile ProfileBase */
        $profile = $this->getProfile($id);
        $profile->scenario = ProfileBase::SCENARIO_UPDATE;

        if (\Yii::$app->request->isPost) {
            $profile->load(post());

            if ($profile->validate()) {
                try {
                    $profile->update();
                    session()->setFlash('success', t('online_cashbox', 'Cashier profile successfully updated.'));

                    return $this->redirect(Url::to(['/onlinecashbox/profile/update', 'id' => $profile->id]));
                } catch (\Exception $e) {
                    if ($e instanceof ClientException
                        && in_array($e->getResponse()->getStatusCode(),
                            ProcessedErrorService::PROCESSED_RESPONSE_CODE)) {
                        $this->processedErrorService->processedResponseError($e, $profile);
                    } else {
                        session()->setFlash('error',
                            t('online_cashbox',
                                'An error occurred while working with the settings of online cash registers'));
                        \Yii::error($e->getMessage(), 'online_cashbox');

                        return $this->redirect('create');
                    }

                }
            }
        }


        return $this->render('update', [
            'profile'            => $profile,
            'typesOnlineCashbox' => $this->onlineCashboxService->getTypesOnlineCashbox(),
        ]);
    }


    public function actionList()
    {
        $search            = new ProfileOnlineCashboxSearch();
        $arrayDataProvider = $search->search();

        $typesOnlineCashbox = $this->onlineCashboxService->getTypesOnlineCashbox();

        return $this->render('list', [
            'arrayDataProvider'  => $arrayDataProvider,
            'typesOnlineCashbox' => $typesOnlineCashbox,
        ]);
    }

    public function actionDelete($id)
    {
        /* @var $profile ProfileBase */
        $profile = $this->getProfile($id);
        try {
            $profile->delete();
        } catch (\Exception $e) {
            session()->setFlash('error', t('online_cashbox', 'Uninstall error, contact the administrator.'));
            \Yii::error($e->getMessage(), 'online_cashbox');

            return $this->redirect(Url::to('/onlinecashbox/profile/list'));
        }
        session()->setFlash('success', t('online_cashbox', 'Cashier profile successfully deleted.'));
        return $this->redirect(Url::to('/onlinecashbox/profile/list'));
    }

    public function actionChangeProfileType()
    {
        /* @var $profile ProfileBase */
        $profile = $this->profileOnlineCashboxService->getProfileModel(ProfileDisabled::TYPE_ID);

        if (app()->request->isPost) {
            $profileData = app()->request->post();
            $typeId      = (int)$profileData['ProfileOnlineCashbox']['type_id'];

            if ($typeId !== ProfileDisabled::TYPE_ID) {
                $profile = $this->profileOnlineCashboxService->getProfileModel($typeId);
                $profile->load($profileData);
            }
        }

        try {
            $typesOnlineCashbox = $this->onlineCashboxService->getTypesOnlineCashbox();
        } catch (\Exception $ex) {
            session()->setFlash('error',
                t('online_cashbox', 'An error occurred while working with the settings of online cash registers'));
            \Yii::error($ex, 'online_cashbox');

            return $this->redirect('update');
        }

        return $this->renderAjax('_form', [
            'profile'            => $profile,
            'typesOnlineCashbox' => $typesOnlineCashbox,
        ]);
    }

    protected function getProfile($id)
    {
        /* @var $profiles ProfileBase */
        $profile = $this->onlineCashboxService->getProfile($id);
        if ($profile->tenant_id != user()->tenant_id) {
            throw new NotFoundHttpException();
        }

        return $profile;
    }

}