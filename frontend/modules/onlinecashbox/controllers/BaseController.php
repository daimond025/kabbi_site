<?php

namespace frontend\modules\onlinecashbox\controllers;


use yii\filters\AccessControl;
use yii\web\Controller;

class BaseController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow'   => true,
                        'roles'   => ['onlineCashbox'],
                    ],
                ],
            ],
        ];
    }


    public function actionList()
    {
        return $this->render('list');
    }


}