<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $arRepublic \common\modules\city\models\Republic */
/* @var $form yii\widgets\ActiveForm */
//
if (!empty($arRepublic)):?>
    <? foreach ($arRepublic as $republic): ?>
        <? foreach ($republic['city'] as $city): ?>
        <?if($city['shortname'] == 'г'):?>
            <?$city['shortname'] = '';?>
        <?endif?>
            <li>
                <a data-city="<?= $city['city_id']; ?>"
                   data-lat="<?= $city['lat']; ?>" data-lon="<?= $city['lon']; ?>" >
                    <?= Html::encode($republic['name' . $languagePrefix] . ' ' . $republic['shortname' . $languagePrefix] . ', '.  $city['shortname' . $languagePrefix]. ' ' . $city['name' . $languagePrefix]) ?>
                </a>
            </li>
        <? endforeach; ?>
    <? endforeach; ?>
<? endif; ?>

