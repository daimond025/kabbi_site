<?php

namespace app\modules\city\controllers;

use Yii;
use yii\web\Controller;
use common\modules\city\models\City;
use common\modules\city\models\Republic;
use yii\filters\AccessControl;
use common\modules\tenant\models\TenantHasCity;
use common\modules\employee\models\worker\Worker;

class CityController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'except' => [
                    'get-city-coords',
                    'get-worker-coords'
                ],
                'rules' => [
                    [
                      'allow' => true,
                      'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCitySearch($all_search = 1)
    {
        $languagePrefix = getLanguagePrefix();
        $arRepublic = [];
        if(!empty($_POST['city']))
        {
            $country_id = post('country');
            $city = post('city');

            $tbl_republic = Republic::tableName();

            $arRepublic = Republic::find()->asArray()->
                filterWhere(['country_id' => $country_id])->
                select([
                    "$tbl_republic.republic_id",
                    "$tbl_republic.name" . $languagePrefix,
                    "$tbl_republic.shortname" . $languagePrefix])->
                joinWith([
                  'city' => function($query) use ($city, $languagePrefix, $all_search){
                      $tbl_city = City::tableName();
                      $query->andWhere(['LIKE', "$tbl_city.name" . $languagePrefix, "$city%", false])
                              ->andFilterWhere(['search' => $all_search ? null : 1])
                              ->orderBy(["$tbl_city.sort"=>SORT_DESC]);
                  }
                  ])->
                all();
        }
        return $this->renderAjax('citySearch', [
                            'languagePrefix' => $languagePrefix,
                            'arRepublic'     => $arRepublic,
                ]);
    }

    public function actionGetCityCoords()
    {
        app()->response->format = \yii\web\Response::FORMAT_JSON;

        $city = City::findOne(post('city_id'));
        if (empty($city)) {
            return 'error';
        } else {

            return [
                'lat' => $city->lat,
                'lon' => $city->lon,
            ];
        }
    }

    public function actionGetWorkerCoords()
    {
        app()->response->format = \yii\web\Response::FORMAT_JSON;

        $city = City::findOne(post('city_id'));

        if (empty($city)) {
            return 'error';
        } else {
            return [
                'lat' => $city->lat,
                'lon' => $city->lon,
            ];
        }
    }

    /**
     * Getting city currency symbol by city_id
     * @param integer $city_id
     * @return json
     */
    public function actionGetCityCurrencySymbol($city_id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        app()->response->format = \yii\web\Response::FORMAT_JSON;
        return getCurrencySymbol($city_id);
    }
}
