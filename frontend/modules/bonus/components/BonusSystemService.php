<?php

namespace frontend\modules\bonus\components;

use app\modules\balance\models\Account;
use yii\base\Object;
use yii\db\Query;

/**
 * Class BonusSystemService
 * @package frontend\modules\bonus\components
 */
class BonusSystemService extends Object
{
    const BONUS_SYSTEM_ID_GOOTAX = 1;
    const BONUS_SYSTEM_ID_UDS_GAME = 2;

    /**
     * Getting bonus system by tenant
     *
     * @param int $tenantId
     *
     * @return int
     */
    public function getBonusSystemId($tenantId)
    {
        $bonusSystemId = (new Query())
            ->select('bonus_system_id')
            ->from('{{%tenant_has_bonus_system}}')
            ->where(['tenant_id' => $tenantId])
            ->scalar();

        return $bonusSystemId === false ? self::BONUS_SYSTEM_ID_GOOTAX : (int)$bonusSystemId;
    }

    /**
     * Getting bonus kind by tenant
     *
     * @param int $tenantId
     *
     * @return int
     */
    public function getBonusKindId($tenantId)
    {
        $bonusSystemId = $this->getBonusSystemId($tenantId);
        switch ($bonusSystemId) {
            case self::BONUS_SYSTEM_ID_GOOTAX:
                return Account::CLIENT_BONUS_KIND;
            case self::BONUS_SYSTEM_ID_UDS_GAME:
                return Account::CLIENT_BONUS_UDS_GAME_KIND;
        }
    }

}