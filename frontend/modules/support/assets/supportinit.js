$(document).ready(function () {
    function getCookie(name) {
        var matches = document.cookie.match(new RegExp(
                "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
                ))
        return matches ? decodeURIComponent(matches[1]) : undefined
    }
    var activeTabClass = getCookie('tab_active_id');
    if (typeof activeTabClass !== undefined) {
        if (activeTabClass == 't02') {
            $(document).find('a.t01').parent().removeClass('active');
            $(document).find('#t01').removeClass('active');
            $('#t02').addClass('active');
        } else {
            $('document').find('a.t02').parent().removeClass('active');
            $(document).find('#t02').removeClass('active');
            $('#t01').addClass('active');
        }
        $('.' + activeTabClass).parent('li').addClass('active');
    } else {
        $('.t01').parent('li').addClass('active');
    }
});