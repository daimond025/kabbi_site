$(document).ready(function () {
    ticket_id = $('#close_ticket').data('ticketid');
    $('#open_t').on('click', function () {
        $.ajax({
            type: 'POST',
            cache: false,
            url: '/support/support/open-ticket/' + ticket_id,
            success: function (response) {
                if (response == "1") {
                    window.location.assign(document.URL);
                 //  location.reload();
                }
            }
        });
    });
    $('#close_t').on('click', function () {
        $.ajax({
            type: 'POST',
            cache: false,
            url: '/support/support/close-ticket/' + ticket_id,
            success: function (response) {
                if (response == "1") {
                    window.location.assign(document.URL);
                  //  location.reload();
                }
            }
        });

    });
    $(".rating li").on("click", function () {
        var is_locked_list = $('.rating.locked').length;
        if (is_locked_list == "0")
        {
            var raiting = $(this).data('rating');
            $.ajax({
                type: 'POST',
                cache: false,
                url: '/support/support/set-rating/' + ticket_id,
                data: 'raiting=' + raiting,
                success: function (response) {
                    $('.rating').addClass('locked');
                }
            });
        }
    });

});