<?php

namespace app\modules\support\models;
use Yii;

/**
 * This is the model class for table "{{%support_message}}".
 *
 * @property string $message_id
 * @property string $support_id
 * @property string $support_user_id
 * @property string $message
 * @property string $create_time
 * @property TblAdminUser $supportUser
 * @property TblSupport $support
 */
class SupportMessage extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%support_message}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['support_id', 'message'], 'required'],
            [['support_id', 'message'], 'required', 'on' => 'insert'],
            [['support_id', 'support_user_id'], 'integer'],
            [['message'], 'string'],
            [['create_time'], 'safe'],
          /*  ['file_name', 'file', 'extensions' => ['png', 'jpeg', 'jpg']],*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'message_id'      => Yii::t('support', 'Message ID'),
            'support_id'      => Yii::t('support', 'Support ID'),
            'support_user_id' => Yii::t('support', 'Support User ID'),
            'message'         => Yii::t('support', 'Message'),
            'create_time'     => Yii::t('support', 'Create Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupportUser()
    {
        return $this->hasOne(AdminUser::className(), ['user_id' => 'support_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupport()
    {
        return $this->hasOne(Support::className(), ['support_id' => 'support_id']);
    }

    public function getFirstMessage($supportId)
    {
        return SupportMessage::find()
                        ->where(['support_id' => $supportId])
                        ->asArray()
                        ->one();
    }





}
