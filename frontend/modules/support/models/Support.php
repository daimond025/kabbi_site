<?php

namespace app\modules\support\models;

use Yii;

/**
 * This is the model class for table "{{%support}}".
 *
 * @property string $support_id
 * @property string $tenant_id
 * @property string $user_id
 * @property string $create_time
 * @property string $priority
 * @property string $title
 * @property string $status
 * @property string $email
 * @property TblTenant $tenant
 * @property TblUser $user
 * @property TblSupportMessage[] $tblSupportMessages
 */
class Support extends \yii\db\ActiveRecord
{

    public $titleArr;
    public $priorityArr;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%support}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'user_id', 'priority', 'title'], 'required'],
            [['tenant_id', 'user_id', 'priority', 'title'], 'required', 'on' => 'insert'],
            [['tenant_id', 'user_id'], 'integer'],
            [['create_time'], 'safe'],
            [['priority', 'title'], 'string', 'max' => 100],
            [['status'], 'string', 'max' => 25],
            [['email'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'support_id'  => Yii::t('support', 'Support ID'),
            'tenant_id'   => Yii::t('support', 'Tenant ID'),
            'user_id'     => Yii::t('support', 'User ID'),
            'create_time' => Yii::t('support', 'Create Time'),
            'priority'    => Yii::t('support', 'Priority'),
            'title'       => Yii::t('support', 'Title'),
            'status'      => Yii::t('support', 'Status'),
            'email'       => Yii::t('support', 'Email'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblSupportMessages()
    {
        return $this->hasMany(SupportMessage::className(), ['support_id' => 'support_id']);
    }

    public function getTitleArr()
    {
        return array(
            'Main Questions'                                  => t('support', 'Main Questions'),
            'Mistakes in work of the program'                 => t('support', 'Mistakes in work of the program'),
            'Offers on development'                           => t('support', 'Offers on development'),
            'Change and addition of streets or public places' => t('support', 'Change and addition of streets or public places'),
            'Another'                                         => t('support', 'Another')
        );
    }

    public function getPriorityArr()
    {
        return array(
            'LOW'    => t('support', 'LOW'),
            'MEDIUM' => t('support', 'MEDIUM'),
            'HIGHT'  => t('support', 'HIGHT')
        );
    }

}
