<?php

namespace app\modules\support\models;

use frontend\components\behavior\file\FileMultipleBehavior;
use Yii;

/**
 * This is the model class for table "tbl_support_message_photo".
 *
 * @property string $photo_id
 * @property string $message_id
 * @property string $image
 *
 * @property Support $support
 * @property SupportMessage $message
 */
class SupportMessagePhoto extends \yii\db\ActiveRecord
{

    public $new_image = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_support_message_photo';
    }

    public function behaviors()
    {
        return [
            'fileBehavior' => [
                'class'     => FileMultipleBehavior::className(),
                'fileField' => [
                    'image',
                ],
                'upload_dir' => app()->params['upload'],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message_id', 'image'], 'required'],
            [['message_id'], 'integer'],
            [['image'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'photo_id'   => 'Photo ID',
            'message_id' => 'Message ID',
            'image'      => 'Image',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessage()
    {
        return $this->hasOne(SupportMessage::className(), ['message_id' => 'message_id']);
    }

    /**
     * Save all support images
     * @param integer $info_id
     * @param array $newFiles
     * @param array $oldFiles
     * @return boolean
     */
    public function manySave($message_id, array $newFiles, array $oldFiles = [])
    {
        if (!empty($newFiles)) {
            $insertValue = [];
            $connection = app()->db;

            foreach ($newFiles as $file) {
                $insertValue[] = [$message_id, $file];
            }

            if (!empty($insertValue)) {
                //Удаление старых фото
                if (!empty($oldFiles)) {
                    self::deleteAll(['message_id' => $message_id]);
                    foreach ($oldFiles as $old_file)
                        $this->deleteDocument($old_file);
                }
                $connection->createCommand()->batchInsert(self::tableName(), ['message_id', 'image'], $insertValue)->execute();
                return true;
            }
        }

        return false;
    }

}
