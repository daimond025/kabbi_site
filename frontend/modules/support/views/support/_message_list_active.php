<?php

use app\modules\support\models\SupportMessage;
use frontend\widgets\LinkPager;
use yii\helpers\Html;

function filterStatus($status)
{
    switch ($status) {
        case 'OPEN':
            return t('support', 'Please wait for answer');
        case 'CLOSE':
            return t('support', 'Topic is closed');
        case 'WAIT_ANSWER':
            return t('support', 'Please wait for answer');
        case 'ANSWER_RECIVED':
            return t('support', 'Answer is received');
    }
}

function getMessage($supportId)
{
    $model = new SupportMessage();
    $messageData = $model->getFirstMessage($supportId);
    return $messageData['message'];
}

function formatDate($date)
{
    $date = new DateTime($date);
    $result['date'] = $date->format('d.m.Y');
    $result['time'] = $date->format('H.i');
    return $result;
}

function formatTitle($title)
{
    return t('support', $title);
}
?>

<ul class="tickets">
    <?php

    foreach ($data1 as $rec) {
        if ($rec->status !== 'CLOSE') {
            $status = filterStatus($rec->status);
            $statusClass='';
            if ($rec->status == "ANSWER_RECIVED") {
                $statusClass = "green";
            }
            $message = Html::encode(getMessage($rec->support_id));
            $dateInfo = formatDate($rec->create_time);
            $date = Html::encode($dateInfo['date']);
            $time = Html::encode($dateInfo['time']);
            $title = Html::encode(formatTitle($rec->title));
            $url = yii\helpers\Url::to(['/support/support/update', 'id' => $rec->support_id]);
            $supportId = Html::encode($rec->support_id);
            $you = t('support', 'You');
            $at = t('support', 'at');
            echo <<<EOT
                        <li>
                        <div class="t_status"><b class="$statusClass">$status</b></div>
                        <div class="t_number">№$supportId</div>
                        <div class="t_content">
                            <a href="$url">$title</a>
                            <div>
                                <p style="word-wrap: break-word;">$message</p>
                                <span> $you $date $at $time</span>
                            </div>
                        </div>
                    </li>
EOT;
        }
    }
    ?>
</ul>
<?php
echo LinkPager::widget([
    'pagination'         => $pages1,
    'prevPageLabel' => t('app', 'Prev'),
    'nextPageLabel' => t('app', 'Next'),
]);
?>