<?php

use app\modules\support\assets\SupportAsset;
use yii\helpers\Url;

$bundle = SupportAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/supportinit.js', ['depends' => 'yii\web\JqueryAsset']);
$this->title = Yii::t('app', 'Support');
?>
<a href="<?=  Url::to(['/support/support/create'])?>" class="button" style="float: right; position: relative; top: 5px;"><?=t('support', 'Create')?></a>
<h1><?= t('support', 'Technical support')?></h1>
<div class="support_message inline">
    <p><b><?= t('support', 'Opening hours')?></b><br/>
        <?= t('support', 'On control panel request - day and night')?>. <?= t('support', 'On weekdays: phone from 10 to 18 (Moscow timezone)')?>.
    </p>
    <p><b><?= t('support', 'Response time')?></b><br/><?= t('support', ' 10 minutes up to 24 hours')?>. <?= t('support', 'In dependence of difficulty it can take more time')?></p>
    <!--i><?= t('support', 'Before you create a request please check ')?><a href="<?=Url::to(['/support/support/faq'])?>"><?= t('support', 'FAQ')?></a></i-->
</div>
<section class="main_tabs">
    <div class="tabs_links">
        <ul>
            <li><a href="#current" class="t01"><?= t('support', 'Current messages')?></a></li>
            <li><a href="#closed" class="t02"><?= t('support', 'Closed messages')?></a></li>
        </ul>
    </div>
    <div class="tabs_content">
        <div id="t01" >
            <?php
            echo $this->render('_message_list_active',  [
                'data1'            => $data1,
                'pages1'           => $pages1,
                'registerLinkTags' => false
            ]);
            ?>
        </div>
        <div class="" id="t02">
            <?php
            echo $this->render('_message_list_done', [
                'data2'            => $data2,
                'pages2'           => $pages2,
                'registerLinkTags' => false]);
            ?>
        </div>
    </div>
</section>