<?php

use yii\helpers\Html;
use app\modules\support\assets\SupportAsset;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;
use yii\helpers\Url;

$bundle = SupportAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/supportupdate.js', ['depends' => 'yii\web\JqueryAsset']);
$this->title = Yii::t('app', 'Support');
?>

<div class="bread"><a href="<?= Url::toRoute('/support/support/list')?>"><?= t('app', 'Support')?></a></div>
<h1><?= t('support', 'Request') ?> №<?= Html::encode($modelSupport->support_id) ?></h1>
<div class="ticket_detail">
    <div class="td_subject"><?= Html::encode(t('support', $modelSupport->title)) ?></div>
    <div class="td_topic_start inline">
        <p><?= Html::encode($modelSupportMessageArr[0]->message) ?></p>
        <p>
            <?php
            if ($image_path_arr) {
                $image_path_arr_first = reset($image_path_arr);
                if (isset($image_path_arr_first)) {
                    $str = '</br>';
                    foreach ($image_path_arr_first as $image) {
                        $name = $image[name];
                        $str = $str . "<a target='_blank' href='$image[path]'>$name</a></br>";
                    }
                    if (!empty($str)) {
                        echo '<div><span>'.t('support', 'Attached files').'</span>'.$str.'</div>';
                    }
                }
            }
            ?>
        </p>
        <span class="tdts_time"><?= t('support', 'You')?> <?= app()->formatter->asDate($modelSupport->create_time, 'php:d.m.Y') ?> <?= t('support', 'at')?> <?= app()->formatter->asDate($modelSupport->create_time, 'php:H:i') ?> </span>
    </div>
    <div class="td_answers">
        <?php
        echo $this->render('_chat_list', [
            'modelSupportMessage' => $modelSupportMessageArr,
            'image_path_arr'      => $image_path_arr,
        ]);
        ?>
    </div>
</div>
<?php
$form = ActiveForm::begin([
            'id'                     => 'open_ticket',
            'errorCssClass'          => 'input_error',
            'successCssClass'        => 'input_success',
            'enableClientValidation' => true,
            'options'                => ['enctype' => 'multipart/form-data']
        ]);
?>
<div class="ticket_form" id="open_ticket" data-ticketid="<?php echo "$modelSupport->support_id" ?>" style="display:<?php
if ($modelSupport->status == 'CLOSE') {
    echo "none";
}
?>">
    <ul class="tf_header">
        <li><a class="active"><?= t('support', 'Give an answer')?></a></li>
        <li><a id="close_t"><?= t('support', 'Close ticket')?></a></li>
    </ul>
    <div class="tf_content">
        <section class="row">
            <div class="row_label"><label><?= t('support', 'Message')?></label></div>
            <div class="row_input">
                <?= Html::activeTextarea($modelSupportMessage, 'message', ['rows' => '6']); ?>
            </div>
        </section>
        <?/**
        <section class="row">
            <div class="row_label"><label><?= t('support', 'Attach files')?></label></div>
            <div class="row_input">
                <div class="input_file_replace2">
                    <a>
                        <?php
                        if ($image_arr) {
                            echo FileInput::widget([
                                'model'         => $modelSupportMessagePhoto,
                                'attribute'     => 'image',
                                'options'       => [
                                    'multiple' => true,
                                    'name'     => 'SupportMessagePhoto[image][]',
                                    'accept'   => 'image/*',
                                ],
                                'pluginOptions' => [
                                    'allowedFileExtensions' => ['jpg', 'gif', 'png'],
                                    'showUpload'            => false,
                                    'showRemove'            => false,
                                    'showCaption'           => false,
                                    'maxFileCount'          => 7,
                                    'initialPreview'        => $image_arr,
                                ]
                            ]);
                        } else {
                            echo FileInput::widget([
                                'model'         => $modelSupportMessagePhoto,
                                'attribute'     => 'image',
                                'options'       => [
                                    'multiple' => true,
                                    'name'     => 'SupportMessagePhoto[image][]',
                                    'accept'   => 'image/*',
                                ],
                                'pluginOptions' => [
                                    'allowedFileExtensions' => ['jpg', 'gif', 'png'],
                                    'showUpload'            => false,
                                    'showRemove'            => false,
                                    'showCaption'           => false,
                                    'maxFileCount'          => 7,
                                ]
                            ]);
                        }
                        ?>
                    </a>
                </div>
            </div>
        </section>*/?>
        <div style="text-align: right"><input type="submit" class="button" value="<?= t('support', 'Send')?>"/></div>

    </div>
</div>
<?php ActiveForm::end(); ?>
<div class="ticket_form" id="close_ticket"  data-ticketid="<?php echo "$modelSupport->support_id" ?>" style="display:<?php
if ($modelSupport->status !== 'CLOSE') {
    echo "none";
}
?>">
    <ul class="tf_header">
        <li><a id="open_t"><?= t('support', 'Open a ticket')?></a></li>
        <li><a class="active"><?= t('support', 'Close a ticket')?></a></li>
    </ul>
    <div class="tf_content">
        <?php
        echo $this->render('_raiting_list', [
            'modelSupportFeedback' => $modelSupportFeedback,
            'modelSupport'         => $modelSupport
        ]);
        ?>
    </div>
</div>


