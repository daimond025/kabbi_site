<?php

use app\modules\support\models\SupportMessage;
use frontend\widgets\LinkPager;
use yii\helpers\Html;

function filterStatus2($status)
{
    switch ($status) {
        case 'OPEN':
            return t('support', 'Please wait for answer');
        case 'CLOSE':
            return t('support', 'Topic is closed');
        case 'WAIT_ANSWER':
            return t('support', 'Please wait for answer');
        case 'ANSWER_RECIVED':
            return t('support', 'Answer is received');
    }
}

function getMessage2($supportId)
{
    $model = new SupportMessage();
    $messageData = $model->getFirstMessage($supportId);
    return $messageData['message'];
}

function formatDate2($date)
{
    $date = new DateTime($date);
    $result['date'] = $date->format('d.m.Y');
    $result['time'] = $date->format('H.i');
    return $result;
}

function formatTitle2($title)
{
    return t('support', $title);
}
?>

<ul class="tickets">
    <?php
    foreach ($data2 as $rec) {
        if ($rec->status == 'CLOSE') {
            $status = Html::encode(filterStatus2($rec->status));
            Html::encode($message = getMessage2($rec->support_id));
            $dateInfo = formatDate2($rec->create_time);
            $date = Html::encode($dateInfo['date']);
            $time = Html::encode($dateInfo['time']);
            $title = Html::encode(formatTitle2($rec->title));
            $url = yii\helpers\Url::to(['/support/support/update', 'id' => $rec->support_id]);
            $you = t('support', 'You');
            $at = t('support', 'at');
            echo <<<EOT
                        <li>
                        <div class="t_status"><b>$status</b></div>
                        <div class="t_number">№$rec->support_id</div>
                        <div class="t_content">
                            <a href="$url">$title</a>
                            <div>
                                <p style="word-wrap: break-word;">$message</p>
                                <span> $you $date $at $time</span>
                            </div>
                        </div>
                    </li>
EOT;
        }
    }
    ?>
</ul>
<?php
echo LinkPager::widget([
    'pagination'         => $pages2,
    'prevPageLabel' => t('app', 'Prev'),
    'nextPageLabel' => t('app', 'Next'),
]);
?>