<?php

use yii\helpers\Html;

$i = 0;
$you = t('support', 'You');
$at = t('support', 'at');
$supportuser = t('support', 'Support user,');
$attachedfiles = t('support', 'Attached files');
foreach ($modelSupportMessage as $model) {
    $str = '</br>';
    if (isset($image_path_arr[$model->message_id])) {
        $imageArr = $image_path_arr[$model->message_id];

        foreach ($imageArr as $image) {
            $name = Html::encode($image[name]);
            $str = $str . "<a target='_blank' href='$image[path]'>$name</a></br>";
        }
    }
    $message = Html::encode($model->message);
    if ($i <> 0) {
        if (!empty($model->support_user_id)) {
            $date = app()->formatter->asDate($modelSupport->create_time, 'php:d.m.Y');
            $time = app()->formatter->asDate($modelSupport->create_time, 'php:H:i');

            echo <<<EOT
        <div class="td_answer td_admin">
            <p>$message</p>
            <span>$supportuser $date $at $time</span>
EOT;

            if ($str !== '</br>') {
                echo <<<EOT
            <div>
                <span>Attached files</span>$str
            </div>
        </div>
EOT;
            } else {
                echo " </div>";
            }
        } else {
            $date = app()->formatter->asDate($modelSupport->create_time, 'php:d.m.Y');
            $time = app()->formatter->asDate($modelSupport->create_time, 'php:H:i');

            echo <<<EOT
        <div class="td_answer">
            <p>$message</p>
            <span>$you $date $at $time</span>
EOT;
            if ($str !== "</br>") {
                echo <<<EOT
            <div>
                <span>$attachedfiles</span>$str
            </div>
        </div>
EOT;
            } else {
                echo " </div>";
            }
        }
    }
    $i++;
}
?>
