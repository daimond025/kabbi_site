<?php

use yii\helpers\Html;
use app\modules\support\assets\SupportAsset;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;
use yii\helpers\Url;

$bundle = SupportAsset::register($this);
$this->title = Yii::t('app', 'Support');
?>
<div class="bread"><a href="<?= Url::toRoute('/support/support/list')?>"><?= t('app', 'Support')?></a></div>
<?php
$form = ActiveForm::begin([
            'id'                     => 'add-support-message',
            'errorCssClass'          => 'input_error',
            'successCssClass'        => 'input_success',
            'enableClientValidation' => true,
            'options'                => ['enctype' => 'multipart/form-data']
        ]);
?>
    <h1><?= t('support', 'New request')?></h1>
    <section class="row">
        <div class="row_label"><label><?= t('support', 'Subject')?></label></div>
        <div class="row_input">
            <?= Html::activeDropDownList($modelSupport, 'title', $modelSupport->titleArr, [
                    'data-placeholder' => Html::encode(!empty($model->title) ? $model->title : t('support', 'ChooseTitle')),
                    'class' => 'default_select']) ?>
        </div>
    </section>
    <section class="row">
        <div class="row_label"><label><?= t('support', 'Email address where you will receive answer')?></label></div>
        <div class="row_input">
            <?= Html::activeTextInput($modelSupport, 'email'); ?>
        </div>
    </section>
    <section class="row">
        <div class="row_label"><label><?= t('support', 'Message')?></label></div>
        <div class="row_input">
            <?= Html::activeTextarea($modelSupportMessage, 'message', ['rows' => '6']); ?>
        </div>
    </section>
    <?/**
    <section class="row">
        <div class="row_label"><label><?= t('support', 'Attach file')?></label></div>
        <div class="row_input">
            <div class="input_file_replace2">

                <?php
                if (isset($image_arr)) {
                    echo FileInput::widget([
                        'model'         => $modelSupportMessagePhoto,
                        'attribute'     => 'image',
                        'options'       => [
                            'multiple' => true,
                            'name'     => 'SupportMessagePhoto[image][]',
                            'accept'   => 'image/*',
                        ],
                        'pluginOptions' => [
                            'allowedFileExtensions' => ['jpg', 'gif', 'png'],
                            'showUpload'            => false,
                            'showRemove'            => false,
                            'showCaption'           => false,
                            'maxFileCount'          => 7,
                            'initialPreview'        => $image_arr,
                        ]
                    ]);
                } else {
                    echo FileInput::widget([
                        'model'         => $modelSupportMessagePhoto,
                        'attribute'     => 'image',
                        'options'       => [
                            'multiple' => true,
                            'name'     => 'SupportMessagePhoto[image][]',
                            'accept'   => 'image/*',
                        ],
                        'pluginOptions' => [
                            'allowedFileExtensions' => ['jpg', 'gif', 'png'],
                            'showUpload'            => false,
                            'showRemove'            => false,
                            'showCaption'           => false,
                            'maxFileCount'          => 7,
                        ]
                    ]);
                }
                ?>
            </div>
        </div>
    </section>*/?>
    <section class="row">
        <div class="row_label"><label><?= t('support', 'Please rate how improtant is your request')?></label></div>
        <div class="row_input">
            <div class="grid_3">
                <div style="height: 50px;">
                    <?= Html::activeDropDownList($modelSupport, 'priority', $modelSupport->priorityArr, [
                            'data-placeholder' => Html::encode(!empty($model->priority) ? $model->priority : t('support', 'ChoosePriority')),
                            'class' => 'default_select']) ?>
                    <div style="height: 50px;"></div>
                    <div style="height: 50px;"></div>
                </div>
            </div>
    </section>
    <section class="submit_form">
        <input type="submit" value="<?= t('support', 'Send')?>"/>
    </section>
<?php ActiveForm::end(); ?>



