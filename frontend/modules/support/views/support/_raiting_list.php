<?php

use yii\helpers\Html;

$yourRqNum = t('support', 'Your request') .  '№';
$closed = t('support', 'is closed');
$supportId = Html::encode($modelSupportFeedback->support_id);

if (isset($modelSupportFeedback->raiting)) {
    if (!empty($modelSupportFeedback->raiting)) {
        echo "<p>$yourRqNum $supportId $closed</p>";
        echo "<ul class = 'rating locked'>";
        $countActive = $modelSupportFeedback->raiting;
        $countNonActive = 5 - $countActive;
        for ($i = 1; $i <= $countActive; $i++) {
            echo "<li data-rating = '$i' class='active locked'></li>";
        }
        for ($i = $countActive + 1; $i <= 5; $i++) {
            echo "<li data-rating = '$i' class='locked'></li>";
        }
        echo "</ul>";
    }
} else {
    $reqnum = t('support', 'Request №');
    $closed_rateus = t('support', 'has been closed. Please rate us');
    $thanks = t('support', 'Thank you!');
    echo <<<EOT
                <p> $reqnum $supportId $closed_rateus</p>
                <ul class = "rating">
                    <li data-rating = "1"></li>
                    <li data-rating = "2"></li>
                    <li data-rating = "3"></li>
                    <li data-rating = "4"></li>
                    <li data-rating = "5"></li>
                    <li class = "rating_input"><input type = "text"/></li>
                 </ul>
                <div style = "height: 20px; text-align: center;"><span class = "thanks">$thanks</span></div>
EOT;
}
?>


