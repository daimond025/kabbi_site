<?php

namespace app\modules\support\controllers;

use Yii;
use yii\filters\AccessControl;
use app\modules\support\models\Support;
use app\modules\support\models\SupportMessage;
use app\modules\support\models\SupportMessagePhoto;
use app\modules\support\models\SupportFeedback;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\data\Pagination;

/**
 * SupportController implements the CRUD actions for Support model.
 */
class SupportController extends Controller
{
    public $layout = '//main';
    public $image_arr;
    public $image_path_arr;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Support models.
     * @return mixed
     */
    public function actionList()
    {
        $userId = user()->user_id;
        $query2 = Support::find()
            ->where(["user_id" => $userId])
            ->andWhere(["status" => 'CLOSE'])
            ->orderBy([
            'support_id' => SORT_DESC,
        ]);
        $countQuery2 = clone $query2;
        $pages2 = new Pagination(['totalCount' => $countQuery2->count(), 'pageSize' => 5, 'pageParam' => 'page2']);
        $pages2->pageSizeParam = false;
        $data2 = $query2->offset($pages2->offset)
            ->limit($pages2->limit)
            ->all();

        $query1 = Support::find()
            ->where(["user_id" => $userId])
            ->andWhere('status <> :close', [':close' => 'CLOSE'])
            ->orderBy([
            'support_id' => SORT_DESC,
        ]);

        $countQuery1 = clone $query1;
        $pages1 = new Pagination(['totalCount' => $countQuery1->count(), 'pageSize' => 5, 'pageParam' => 'page1']);
        $pages1->pageSizeParam = false;
        $data1 = $query1->offset($pages1->offset)
            ->limit($pages1->limit)
            ->all();
        return $this->render('index', [
                'data1'  => $data1,
                'pages1' => $pages1,
                'data2'  => $data2,
                'pages2' => $pages2,
                ]
        );
    }

    public function actionCreate()
    {
        $userEmail = user()->email;
        $modelSupport = new Support(['scenario' => 'insert']);
        $modelSupport->priority = "MEDIUM";
        $modelSupport->email = $userEmail;
        $modelSupportMessage = new SupportMessage(['scenario' => 'insert']);
        $modelSupportMessagePhoto = new SupportMessagePhoto();
        $modelSupport->titleArr = $modelSupport->getTitleArr();
        $modelSupport->priorityArr = $modelSupport->getPriorityArr();

        if ($modelSupport->load(Yii::$app->request->post()) && $modelSupportMessage->load(Yii::$app->request->post())) {
            if (empty($modelSupportMessage->message)) {
                $modelSupportMessage->support_id = 0;
                $modelSupportMessage->validate();
                displayErrors(array($modelSupport, $modelSupportMessage));
                return $this->render('create', [
                        'modelSupport'             => $modelSupport,
                        'modelSupportMessage'      => $modelSupportMessage,
                        'modelSupportMessagePhoto' => $modelSupportMessagePhoto,
                ]);
            }
            $modelSupportMessagePhoto->load(Yii::$app->request->post());
            $modelSupport->tenant_id = user()->tenant_id;
            $modelSupport->user_id = user()->user_id;
            if ($modelSupport->validate() && $modelSupport->save()) {
                $modelSupportMessage->support_id = $modelSupport->support_id;
                $transaction = app()->db->beginTransaction();
                if ($modelSupportMessage->validate() && $modelSupportMessage->save()) {
                    $modelSupportMessagePhoto->manySave($modelSupportMessage->message_id, $modelSupportMessagePhoto->saveFiles());
                    $transaction->commit();
                    $SupportMessagePhoto = SupportMessagePhoto::find()
                        ->where([
                            'message_id' => $modelSupportMessage->message_id,
                        ])
                        ->all();
                    if (count($SupportMessagePhoto) > 0) {
                        foreach ($SupportMessagePhoto as $photo) {
                            $image = $photo->image;
                            $imgArr[] = $modelSupportMessagePhoto->getPictureHtml($image, 'width:160px; height:160px');
                        }
                        $this->image_arr = $imgArr;
                    }
                    $host = $_SERVER['HTTP_HOST'];
                    $hostAr = explode('.', $host);
                    $link = 'admin.' . $hostAr[1] . '.' . $hostAr[2] . '/support/support/update/' . $modelSupport->support_id;
                    $params = [
                        'TEMPLATE' => 'newSupportMessageTechSupportNotification',
                        'TO'       => app()->params['supportEmail'],
                        'SUBJECT'  => t('app', 'Tech. support ticket by {name}', ['name' => \Yii::$app->name]),
                        'DATA'     => [
                            'LINK'      => $link,
                            'TICKETID'  => $modelSupportMessage->support_id,
                            'NAME'      => user()->last_name . ' ' . user()->name,
                            'LANGUAGE'  => app()->language,
                            'TEXT'      => $modelSupportMessage->message,
                            'USER_INFO' => [
                                'domain' => domainName(),
                                'name'   => user()->last_name . ' ' . user()->name,
                            ],
                        ]
                    ];
                    sendMail($params);
                    return $this->redirect('/support/list');
                } else {
                    $transaction->rollBack();
                    displayErrors(array($modelSupport, $modelSupportMessage));
                    return $this->render('create', [
                            'modelSupport'             => $modelSupport,
                            'modelSupportMessage'      => $modelSupportMessage,
                            'modelSupportMessagePhoto' => $modelSupportMessagePhoto,
                    ]);
                }
            } else {
                displayErrors(array($modelSupport, $modelSupportMessage));
                return $this->render('create', [
                        'modelSupport'             => $modelSupport,
                        'modelSupportMessage'      => $modelSupportMessage,
                        'modelSupportMessagePhoto' => $modelSupportMessagePhoto,
                ]);
            }
        }
        return $this->render('create', [
                'modelSupport'             => $modelSupport,
                'modelSupportMessage'      => $modelSupportMessage,
                'modelSupportMessagePhoto' => $modelSupportMessagePhoto,
        ]);
    }

    /**
     * Updates an existing Support model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $modelSupport = $this->findModel($id);
        $modelSupportMessage = new SupportMessage();
        $modelSupportMessagePhoto = new SupportMessagePhoto();
        $modelSupportFeedback = SupportFeedback::find()
            ->where([
                'support_id' => $id,
            ])
            ->one();
        //Вся переписка
        $modelSupportMessageArr = SupportMessage::find()
            ->where(["support_id" => $modelSupport->support_id])
            ->all();

        // Массив фотографий
        $imgArrAll = array();
        foreach ($modelSupportMessageArr as $message) {
            $message_id = $message->message_id;
            $SupportMessagePhoto = SupportMessagePhoto::find()
                ->where([
                    'message_id' => $message_id,
                ])
                ->all();
            foreach ($SupportMessagePhoto as $photo) {
                $image = $photo->image;
                $imgData['name'] = $image;
                $imgData['path'] = $modelSupportMessagePhoto->getPicturePath($image, $modelSupport->tenant_id);
                $imgArrAll[$message_id][] = $imgData;
            }
        }
        $this->image_path_arr = $imgArrAll;

        //Если сабмит формы
        if ($modelSupportMessage->load(Yii::$app->request->post())) {
            $modelSupportMessagePhoto->load(Yii::$app->request->post());
            $modelSupportMessage->support_id = $id;
            $transaction = app()->db->beginTransaction();
            //Если сохранилась фото
            if ($modelSupportMessage->validate() && $modelSupportMessage->save()) {
                //Сохраняем фотографии
                $modelSupportMessagePhoto->manySave($modelSupportMessage->message_id, $modelSupportMessagePhoto->saveFiles());
                $transaction->commit();
                session()->setFlash('success', t('support', 'Message send'));
                // Обновляем переписку
                $modelSupportMessageArr = SupportMessage::find()
                    ->where(["support_id" => $modelSupport->support_id])
                    ->all();
                // Обновляем массив фотографий
                $imgArrAll = array();
                foreach ($modelSupportMessageArr as $message) {
                    $message_id = $message->message_id;
                    $SupportMessagePhoto = SupportMessagePhoto::find()
                        ->where([
                            'message_id' => $message_id,
                        ])
                        ->all();
                    foreach ($SupportMessagePhoto as $photo) {
                        $image = $photo->image;
                        $imgData['name'] = $image;
                        $imgData['path'] = $modelSupportMessagePhoto->getPicturePath($image, $modelSupport->tenant_id);
                        $imgArrAll[$message_id][] = $imgData;
                    }
                }
                $this->image_path_arr = $imgArrAll;
                // Всё ок, обновляем статус и  рендерим модель
                $modelSupport->status = 'WAIT_ANSWER';
                $modelSupport->save();
                $modelSupportMessage = new SupportMessage();

                session()->setFlash('success', t('support', 'Message send'));
                return $this->render('update', [
                        'modelSupport'             => $modelSupport,
                        'modelSupportMessageArr'   => $modelSupportMessageArr,
                        'modelSupportMessage'      => $modelSupportMessage,
                        'modelSupportMessagePhoto' => $modelSupportMessagePhoto,
                        'modelSupportFeedback'     => $modelSupportFeedback,
                        'image_path_arr'           => $this->image_path_arr,
                        'image_arr'                => $this->image_arr
                ]);
            } else {
                displayErrors(array($modelSupport, $modelSupportMessage));
            }
            $transaction->rollBack();
        }
        return $this->render('update', [
                'modelSupport'             => $modelSupport,
                'modelSupportMessageArr'   => $modelSupportMessageArr,
                'modelSupportMessage'      => $modelSupportMessage,
                'modelSupportMessagePhoto' => $modelSupportMessagePhoto,
                'modelSupportFeedback'     => $modelSupportFeedback,
                'image_path_arr'           => $this->image_path_arr,
                'image_arr'                => $this->image_arr
        ]);
    }

    /**
     * Finds the Support model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Support the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Support::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCloseTicket($id)
    {
        if (Yii::$app->request->getIsAjax()) {
            $model = $this->findModel($id);
            $model->status = 'CLOSE';
            if ($model->validate() && $model->save()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function actionOpenTicket($id)
    {
        if (Yii::$app->request->getIsAjax()) {
            $model = $this->findModel($id);
            $model->status = 'OPEN';
            if ($model->validate() && $model->save()) {
                $modelSupportFeedback = SupportFeedback::find()
                    ->where([
                        'support_id' => $id,
                    ])
                    ->one();
                if (isset($modelSupportFeedback)) {
                    $modelSupportFeedback->delete();
                }
                return true;
            }
            return false;
        }
    }

    public function actionSetRating($id)
    {
        if (Yii::$app->request->getIsAjax()) {
            $model = new SupportFeedback();
            $model->support_id = $id;
            $model->raiting = Yii::$app->request->post('raiting');
            if ($model->validate() && $model->save()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function actionPrices()
    {
        return $this->render('prices');
    }

    public function actionDocs()
    {
        return $this->render('docs');
    }

    public function actionFaq()
    {
        return $this->render('faq');
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

}