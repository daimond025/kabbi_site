<?php

use yii\helpers\Url;

$this->title = Yii::t('parking', 'Parkings');
$form = app()->user->can('parkings') ? '_form' : '_formRead';
?>

<div class="bread"><a href="<?= Url::toRoute(['list','id' => $model->city_id]) ?>"><?= t('app', 'Parking') ?></a></div>
<div class="parking-index">
    <section  id="editParkBlock">
        <h1><?= t('parking', 'Edit') ?></h1>
        <?php
        echo $this->render($form, [
            'model' => $model,
            'coefficient_type' => $coefficient_type,
            'city_coords' => $city_coords,
        ]) ?>
    </section>
</div>

