<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\modules\parking\assets\ParkingAsset;
use app\modules\parking\models\Parking;

$bundle = ParkingAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/functions.js');
$this->registerJsFile($bundle->baseUrl . '/update.js');
$type_list = $model->getTypeTextList();
?>
<? if (!$model->is_empty() && $model->type !== Parking::TYPE_BASE_POLYGON) : ?>
    <section class="row js-all">
        <div class="row_label"><?= Html::activeLabel($model, 'type') ?></div>
        <div class="row_input">
            <div class="row_input"><?= Html::encode($type_list[$model['type']]); ?></div>
        </div>
    </section>

<? else : ?>

    <section class="row js-all">
        <div class="row_label"><?= Html::activeLabel($model, 'type') ?></div>
        <div class="row_input">
            <div class="input_with_icon">
                <div class="input_with_icon"><?= $type_list[Parking::TYPE_BASE_POLYGON]; ?></div>
            </div>
        </div>
    </section>

<? endif; ?>

<section class="row js-city js-outCity js-airport js-station js-all">
    <div class="row_label"><?= Html::activeLabel($model, 'name') ?></div>
    <div class="row_input"><?= Html::encode($model['name']); ?></div>
</section>

<section class="row js-city js-outCity js-airport js-station js-all">
    <div class="row_label"><?= Html::activeLabel($model, 'sort') ?></div>
    <div class="row_input">
        <div class="row_input"><?= Html::encode($model['sort']); ?> </div>
    </div>
</section>

<section class="row js-city js-outCity js-airport js-station js-all">
    <div class="row_label">
        <label><?= t('parking', 'District coefficient') ?>
        </label>
        <a data-tooltip="2" class="tooltip">?
            <span style="opacity: 0; display: none;">
                        <?= t('parking',
                            'The parking factor in % will increase or decrease the cost of a trip to the parking. And if this is an absolute value, the trip value is added to') ?>
                    </span>
        </a>
    </div>

    <div class="row_input">
        <div style="float: left; width: 150px; margin-right: 15px;">
            <?= $model['in_district']?>
            <div style="display: inline-block; zoom: 1; *display: inline;">
                <?= $coefficient_type[$model['in_distr_coefficient_type']]?>
            </div>
            <span style="display: block; font-size: 14px; padding-top: 4px;">
                    <?= t('parking', 'In district') ?>
                </span>
        </div>
        <div style="float: left; width: 150px;">
            <?= $model['out_district']?>
            <div style="display: inline-block; zoom: 1; *display: inline;">
                <?= $coefficient_type[$model['out_distr_coefficient_type']]?>
            </div>

            <span style="display: block; font-size: 14px; padding-top: 4px;">
                        <?= t('parking', 'Out of district') ?>
                    </span>
        </div>
    </div>
</section>

<?= Html::textInput('cityLat', $city_coords['lat'], ['id' => 'cityLat', 'style' => 'display: none']); ?>
<?= Html::textInput('cityLon', $city_coords['lon'], ['id' => 'cityLon', 'style' => 'display: none']); ?>
<?= Html::activeHiddenInput($model, 'city_id'); ?>
<?= Html::activeHiddenInput($model, 'polygon'); ?>
<div id="editMap" style="height: 590px; background: #ccc; margin-bottom: 30px;"></div>
<span id="polygon" class="help-block help-block-error" style="color:red"></span>

<section class="submit_form">
    <? if (!$model->is_empty()): ?>
        <div>
            <label><input id="showAll" type="checkbox"/><b><?= t('parking', 'Show all parking zones') ?></b></label>
        </div>
        <div>
            <?=Html::activeCheckbox($model, 'is_block', ['disabled' => 'disabled']); ?>
        </div>
    <? endif; ?>
</section>
