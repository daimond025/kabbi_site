<?php

use yii\helpers\Html;
use yii\widgets\Menu;
use frontend\modules\parking\assets\ParkingAsset;

$this->title = Yii::t('parking', 'Parkings');

if (!empty($activeCityId)):
    $bundle = ParkingAsset::register($this);
    $this->registerJsFile($bundle->baseUrl . '/init.js');
    $this->registerJsFile($bundle->baseUrl . '/functions.js');
    ?>
    <div class="parking-index">
        <? if (app()->user->can('parkings')): ?>
            <?= Html::a(t('app', 'Add'), ['create', 'id' => $activeCityId], ['class' => 'button', 'style' => 'float: right; position: relative; top: 5px;', 'id' => 'showAddParking']); ?>
        <? endif ?>

        <h1><?= t('parking', 'Parkings') ?></h1>
        <?=
        Menu::widget([
            'options' => ['class' => 'park_cities'],
            'items'   => $menuItems,
        ]);
        ?>
        <input id="cityLat" type="text" value="<?= $cityCoords['lat'] ?>" style="display:none"/>
        <input id="cityLon" type="text" value="<?= $cityCoords['lon'] ?>" style="display:none"/>

        <div id="mainMap" style="height: 590px; background: #ccc; margin-bottom: 30px;"></div>
        <div class="grid_4" style="overflow: hidden;">
            <div class="grid_item">
                <h3>
                    <label>
                        <? if (!empty($parkArrBase['polygon'])): ?>
                            <input id="cityParks" data-coords='<?= $parkArrBase['polygon'] ?>' type="checkbox"/>
                            <?= Html::a(t('parking', 'City'), ['/parking/parking/update', 'id' => $parkArrBase['parking_id']]) ?>
                        <? else: ?>
                            <input id="cityParks" data-coords="" type="checkbox"><?= t('parking', 'City') ?>
                        <? endif; ?>
                    </label>
                </h3>
                    <? foreach ($parkArrInCity as $park): ?>
                    <label>
                        <input id="park<?= $park['parking_id'] ?>" class="cityParks" data-coords='<?= $park['polygon'] ?>' type="checkbox"/>
                    <?= Html::a(Html::encode($park['name']), ['/parking/parking/update', 'id' => $park['parking_id']], ['class' => $park['is_active'] ? null : 'gray_link']) ?>
                    </label>
                    <? endforeach ?>
            </div>
            <div class="grid_item">
                <h3><label><input id="outCityParks" type="checkbox"/><?= t('parking', 'Track') ?></label></h3>
                    <? foreach ($parkArrOutCity as $park): ?>
                    <label>
                        <input id="park<?= $park['parking_id'] ?>" class="outCityParks" data-coords='<?= $park['polygon'] ?>' type="checkbox"/>
                    <?= Html::a(Html::encode($park['name']), ['/parking/parking/update', 'id' => $park['parking_id']], ['class' => $park['is_active'] ? null : 'gray_link']) ?>
                    </label>
                    <? endforeach; ?>
            </div>
            <div class="grid_item">
                <h3><label><input id="airportParks" type="checkbox"/><?= t('parking', 'Airport') ?></label></h3>
                    <? foreach ($parkArrAirports as $park): ?>
                    <label>
                        <input id="park<?= $park['parking_id'] ?>" class="airportParks" data-coords='<?= $park['polygon'] ?>' type="checkbox"/>
                    <?= Html::a(Html::encode($park['name']), ['/parking/parking/update', 'id' => $park['parking_id']], ['class' => $park['is_active'] ? null : 'gray_link']) ?>
                    </label>
                    <? endforeach; ?>
            </div>
            <div class="grid_item">
                <h3><label><input id="stationParks" type="checkbox"/><?= t('parking', 'Railway') ?></label></h3>
                    <? foreach ($parkArrStations as $park): ?>
                    <label>
                        <input id="park<?= $park['parking_id'] ?>" class="stationParks" data-coords='<?= $park['polygon'] ?>' type="checkbox"/>
                    <?= Html::a(Html::encode($park['name']), ['/parking/parking/update', 'id' => $park['parking_id']], ['class' => $park['is_active'] ? null : 'gray_link']) ?>
                    </label>
                    <? endforeach; ?>
            </div>
            <div class="grid_item">
                <h3>
                    <label>
                        <? if (isset($reseptionParking['polygon'])): ?>
                            <input id="reseptionParking" data-coords='<?= $reseptionParking['polygon'] ?>' type="checkbox"/>
                            <?= Html::a(t('parking', 'The reception area of orders'), ['/parking/parking/update', 'id' => $reseptionParking['parking_id']], ['class' => $reseptionParking['is_active'] ? null : 'gray_link']) ?>
                        <? else: ?>
                            <input id="reseptionParking" data-coords="" type="checkbox"><?= t('parking', 'The reception area of orders') ?>
                        <? endif; ?>
                    </label>
                </h3>
            </div>
        </div>
    </div>
<? else: ?>
    <h1><?= t('parking', 'Parkings') ?></h1>
    <p><?= t('city', 'No cities') ?></p>
<? endif; ?>

