<?php

use yii\helpers\Url;
use backend\widgets\Menu;

$this->title = Yii::t('parking', 'Parkings');
?>
<div class="bread"><a href="<?= Url::toRoute(['list','id' => $model->city_id]) ?>"><?= t('app', 'Parking') ?></a></div>
<h1><?= t('app', 'Create') ?></h1>
<?php
echo Menu::widget([
    'options' => ['class' => 'park_cities'],
    'items'   => $menu_items,
]);
?>
<section class="main_tabs">
    <div class="tabs_content">
        <?php
        echo $this->render('_form', [
            'model' => $model,
            'coefficient_type' => $coefficient_type,
            'city_coords' => $city_coords,
        ]) ?>
    </div>
</section>
