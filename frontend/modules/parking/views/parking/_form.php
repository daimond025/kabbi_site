<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 30.06.16
 * Time: 13:31
 */
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\modules\parking\assets\ParkingAsset;
use app\modules\parking\models\Parking;

$bundle = ParkingAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/functions.js');
if($model->isNewRecord) {
    $this->registerJsFile($bundle->baseUrl . '/create.js');
} else {
    $this->registerJsFile($bundle->baseUrl . '/update.js');
}

$type_list = $model->getTypeTextList();

$form = ActiveForm::begin([
    'id' => 'client-form',
    'errorCssClass' => 'input_error',
    'successCssClass' => 'input_success',
    'enableClientValidation' => true,
    'validateOnBlur' => false,
    'options' => ['enctype' => 'multipart/form-data']
]); ?>

    <? if( !$model->is_empty() && $model->type !== Parking::TYPE_BASE_POLYGON) : ?>
    <section class="row js-all">
        <?= $form->field($model, 'type')->begin();?>
        <div class="row_label"><?=Html::activeLabel($model, 'type')?></div>
        <div class="row_input">
            <?=Html::activeDropDownList($model, 'type', $type_list, ['data-placeholder'=> 'Не выбрано', 'class' => 'default_select'])?>
        </div>
        <?= $form->field($model, 'type')->end();?>
    </section>

    <? else : ?>

    <section class="row js-all">
        <div class="row_label"><?=Html::activeLabel($model, 'type')?></div>
        <div class="row_input">
            <div class="input_with_icon">
                <div class="input_with_icon"><?= $type_list[Parking::TYPE_BASE_POLYGON]; ?></div>
                <?= Html::activeHiddenInput($model, 'type', ['value' => Parking::TYPE_BASE_POLYGON]); ?>
                <span></span>
            </div>
        </div>
    </section>

    <? endif; ?>

    <section class="row js-city js-outCity js-airport js-station js-basePolygon" style="display: none">
        <?= $form->field($model, 'name')->begin();?>
        <div class="row_label"><?=Html::activeLabel($model, 'name')?></div>
        <div class="row_input">
            <?=Html::activeTextInput($model, 'name')?>
        </div>
        <?= $form->field($model, 'name')->end();?>
    </section>

    <section class="row js-city js-outCity js-airport js-station" style="display: none">
        <?= $form->field($model, 'sort')->begin();?>
        <div class="row_label"><?=Html::activeLabel($model, 'sort')?></div>
        <div class="row_input">
            <?=Html::activeTextInput($model, 'sort', ['style' => 'width: 90px;', 'max' => '999', 'min' => '1', 'type' => 'number'])?>
        </div>
        <?= $form->field($model, 'sort')->end();?>
    </section>


    <section class="row js-city js-outCity js-airport js-station" style="display: none">
        <div class="row_label">
            <label><?= t('parking', 'District coefficient') ?>
            </label>
            <a data-tooltip="2" class="tooltip">?
                    <span style="opacity: 0; display: none;">
                        <?= t('parking', 'The parking factor in % will increase or decrease the cost of a trip to the parking. And if this is an absolute value, the trip value is added to') ?>
                    </span>
            </a>
        </div>

        <div class="row_input">
            <div style="float: left; width: 150px; margin-right: 15px;">
                <?= Html::activeTextInput($model, 'in_district', ['type' => 'number', 'style' => 'width: 90px; display: inline-block; zoom: 1; *display: inline; margin-right: 6px; vertical-align: top;']); ?>
                <div style="display: inline-block; zoom: 1; *display: inline;">
                    <?=Html::activeDropDownList($model, 'in_distr_coefficient_type', $coefficient_type, ['data-placeholder'=> $model->in_distr_coefficient_type, 'class' => 'default_select']); ?>
                </div>
                <span style="display: block; font-size: 14px; padding-top: 4px;">
                    <?= t('parking', 'In district') ?>
                </span>
            </div>
            <div style="float: left; width: 150px;">
                <?= Html::activeTextInput($model, 'out_district', ['type' => 'number', 'style' => 'width: 90px; display: inline-block; zoom: 1; *display: inline; margin-right: 6px; vertical-align: top;']); ?>
                <div style="display: inline-block; zoom: 1; *display: inline;">
                    <?=Html::activeDropDownList($model, 'out_distr_coefficient_type', $coefficient_type, ['data-placeholder'=> $model->out_distr_coefficient_type, 'class' => 'default_select']); ?>

                </div>

                    <span style="display: block; font-size: 14px; padding-top: 4px;">
                        <?= t('parking', 'Out of district') ?>
                    </span>
            </div>
        </div>
    </section>

    <?= Html::textInput('cityLat', $city_coords['lat'], ['id' => 'cityLat', 'style' => 'display: none']); ?>
    <?= Html::textInput('cityLon', $city_coords['lon'], ['id' => 'cityLon', 'style' => 'display: none']); ?>
    <?= Html::activeHiddenInput($model, 'city_id'); ?>
    <?= Html::activeHiddenInput($model, 'polygon'); ?>
    <div id="editMap" style="height: 590px; background: #ccc; margin-bottom: 30px;"></div>
    <span id="polygon" class="help-block help-block-error" style="color:red"></span>




    <section class="submit_form">
        <? if( !$model->is_empty() ): ?>
            <div>
                <label><input id="showAll" type="checkbox"/><b><?= t('parking', 'Show all parking zones') ?></b></label>
            </div>
        <? endif; ?>
        <? if( !$model->isNewRecord ) : ?>
            <? if($model->type != Parking::TYPE_BASE_POLYGON) : ?>
                <div>
                    <?=Html::activeCheckbox($model, 'is_block'); ?>
                </div>
                <? if (!$model->is_empty()) : ?>
                    <div>
                        <?= Html::a( Html::tag('b', t('parking', 'Delete')), ['delete', 'id' => $model->parking_id], ['class' => 'link_red']); ?>
                    </div>
                <? endif; ?>
            <? endif; ?>
        <? endif;?>
        <input id="save" type="submit" value="<?= t('parking', 'Save') ?>" data-city="<?=get('id')?>"/>
    </section>


<?php ActiveForm::end(); ?>