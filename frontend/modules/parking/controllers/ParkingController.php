<?php

namespace app\modules\parking\controllers;

use app\modules\parking\models\FixTariff;
use Yii;
use app\modules\parking\models\Parking;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use frontend\modules\tenant\models\UserHasCity;
use yii\web\Response;

/**
 * ParkingController implements the CRUD actions for Parking model.
 */
class ParkingController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'list',
                            'update',
                            'get-parking-by-coords',
                            'get-parking-by-address',
                            'get-all-parks',
                            'ajax-translate',
                        ],
                        'allow'   => true,
                        'roles'   => ['read_parkings'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['parkings'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Parking models.
     * @return mixed
     */
    public function actionList($id = null)
    {
        $cityId = $id;
        $user_city_list = UserHasCity::getCityList();
        if (!empty($user_city_list)) {
            $tenant_id = user()->tenant_id;
            $cityCoords = [
                'lat' => null,
                'lon' => null,
            ];
            $cityField = 'name' . getLanguagePrefix();

            if (empty($cityId)) {
                // Выбираем первый город в списке
                $cityId = $user_city_list[0]['city_id'];
                // Генерим меню
                $menuItems = [];
                $i = 0;
                foreach ($user_city_list as $city) {
                    $i++;
                    if ($i == 1) {
                        $cityCoords['lat'] = $city['city']['lat'];
                        $cityCoords['lon'] = $city['city']['lon'];
                        $menuItems[] = [
                            'label'  => $city['city'][$cityField],
                            'url'    => ['list', 'id' => $city['city_id']],
                            'active' => true,
                        ];
                    } else {
                        $menuItems[] = [
                            'label' => $city['city'][$cityField],
                            'url'   => ['list', 'id' => $city['city_id']],
                        ];
                    }
                }
            } else {
                // Генерим меню
                $menuItems = [];
                foreach ($user_city_list as $city) {
                    if ($city['city_id'] == $cityId) {
                        $cityCoords['lat'] = $city['city']['lat'];
                        $cityCoords['lon'] = $city['city']['lon'];
                        $menuItems[] = [
                            'label'  => $city['city'][$cityField],
                            'url'    => ['list', 'id' => $cityId],
                            'active' => true,
                        ];
                    } else {
                        $menuItems[] = [
                            'label' => $city['city'][$cityField],
                            'url'   => ['list', 'id' => $city['city_id']],
                        ];
                    }
                }
            }

            $parkArrInCity = Parking::getParkingArrInCity($tenant_id, $cityId);
            $parkArrOutCity = Parking::getParkingArrOutCity($tenant_id, $cityId);
            $parkArrStations = Parking::getParkingArrStations($tenant_id, $cityId);
            $parkArrAirports = Parking::getParkingArrAirports($tenant_id, $cityId);
            $parkArrBase = Parking::getParkingArrBase($tenant_id, $cityId);
            $reseptionParking = Parking::getParkingArrReseptionArea($tenant_id, $cityId);
        }

        return $this->render('index', [
            'activeCityId'     => $cityId,
            'menuItems'        => $menuItems,
            'cityCoords'       => $cityCoords,
            'parkArrInCity'    => $parkArrInCity,
            'parkArrOutCity'   => $parkArrOutCity,
            'parkArrStations'  => $parkArrStations,
            'parkArrAirports'  => $parkArrAirports,
            'parkArrBase'      => $parkArrBase,
            'reseptionParking' => $reseptionParking,
        ]);
    }

    public function getUserCityCoordsById($id)
    {
        $coords = [
            'lat' => '',
            'lon' => '',
        ];
        $user_city_list = UserHasCity::getCityList();
        foreach ($user_city_list as $cityItem) {
            if ($cityItem['city_id'] == $id) {
                $coords['lat'] = $cityItem['city']['lat'];
                $coords['lon'] = $cityItem['city']['lon'];
            }
        }

        return $coords;
    }

    /**
     * Get All parks from City of tenant
     * @param integer $cityId
     * @return json|null
     */
    public function actionGetAllParks($cityId)
    {
        $parkArr = Parking::getParkingArrAll(user()->tenant_id, $cityId);

        return is_array($parkArr) ? json_encode($parkArr) : null;
    }

    /**
     * Get parking by address
     * @param string $address
     * @param integer $city_id
     * @return json
     */
    public function actionGetParkingByAddress($address, $city_id)
    {
        //Получение координат адреса.
        $coords = app()->geocoder->findCoordsByAddress($address);

        return $this->getParkingInsideByCoords($coords, $city_id);
    }

    /**
     * Get parking by coords.
     * @param integer $city_id
     * @param float $lat
     * @param float $lon
     * @return json
     */
    public function actionGetParkingByCoords($city_id, $lat, $lon)
    {
        $coords = [
            'lat' => $lat,
            'lon' => $lon,
        ];

        return $this->getParkingInsideByCoords($coords, $city_id);
    }

    /**
     * Getting all parking with parking which contains the point.
     * @param array $coords ['lat' => '', 'lon' => '']
     * @param integer $city_id
     * @return json
     */
    protected function getParkingInsideByCoords($coords, $city_id)
    {
        $json = [
            'parking' => null,
            'inside'  => null,
            'error'   => null,
        ];

        if (!empty($coords)) {
            $lat = (float)$coords['lat'];
            $lon = (float)$coords['lon'];

            //Получение парковки, содержащей кординаты
            $parking = app()->routeAnalyzer->getParkingByCoords(user()->tenant_id, $city_id, $lat, $lon);
            $json['parking'] = $parking['parking'];
            if (!empty($json['parking'])) {
                if (!empty($parking)) {
                    $json['inside'] = $parking['inside'];
                    $json['coords'] = compact('lat', 'lon');
                } else {
                    $json['error'] = 'Адрес вне парковок.';
                }
            } else {
                $json['error'] = 'Не получены парковки.';
            }
        } else {
            $json['error'] = 'Не получены координаты.';
        }

        return json_encode($json);
    }


    public function actionCreate($id)
    {
        $city_id = $id;

        $coefficient_type = [
            'PERCENT' => '%',
            'MONEY'   => getCurrencySymbol($city_id),
        ];

        //список всех филиалов
        $city_coords = $this->getUserCityCoordsById($city_id);

        $user_city_list = UserHasCity::getCityList();

        // При слиянии с веткой workers заменить
        $city_list = ArrayHelper::map($user_city_list, 'city_id', 'city_id');

        if (!in_array($city_id, $city_list)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $cityField = 'name' . getLanguagePrefix();
        $menu_items = [];
        foreach ($user_city_list as $city) {
            if ($city['city_id'] == $city_id) {
                $menu_items[] = [
                    'label'  => $city['city'][$cityField],
                    'url'    => ['create', 'id' => $city['city_id']],
                    'active' => true,
                ];
            } else {
                $menu_items[] = ['label' => $city['city'][$cityField], 'url' => ['create', 'id' => $city['city_id']]];
            }
        }
        $parking = new Parking();
        $parking->tenant_id = user()->tenant_id;
        $parking->city_id = $city_id;
        $parking->is_active = 1;

        if ($parking->load(post())) {
            if ($parking->save()) {
                session()->setFlash('success', t('parking', 'Parking has been added successfull.'));

                return $this->redirect(['list', 'id' => $parking->city_id]);
            }
        }

        $parking->attributes = [
            'type'                       => Parking::TYPE_CITY,
            'in_district'                => 0,
            'out_district'               => 0,
            'in_distr_coefficient_type'  => $coefficient_type['PERCENT'],
            'out_distr_coefficient_type' => $coefficient_type['PERCENT'],
            'sort'                       => 100,
        ];

        return $this->render('create', [
            'model'            => $parking,
            'coefficient_type' => $coefficient_type,
            'city_coords'      => $city_coords,
            'menu_items'       => $menu_items,
        ]);
    }

    public function actionUpdate($id)
    {

        $parking = Parking::find()
            ->where([
                'tenant_id'  => user()->tenant_id,
                'parking_id' => $id,
            ])
            ->one();

        if (!$parking) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $coefficient_type = [
            'PERCENT' => '%',
            'MONEY'   => getCurrencySymbol($parking->city_id),
        ];

        if (app()->user->can('parkings') && $parking->load(post())) {
            if ($parking->save()) {
                session()->setFlash('success', t('parking', 'Parking has been saved successfull.'));
            }
        }

        return $this->render('update', [
            'model'            => $parking,
            'coefficient_type' => $coefficient_type,
            'city_coords'      => $parking->getCentrePolygon(),
        ]);
    }


    /**
     * Deletes an existing Parking model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $parking = Parking::find()
            ->where([
                'parking_id' => $id,
                'tenant_id'  => user()->tenant_id,
            ])
            ->one();

        if ($parking->delete()) {
            session()->setFlash('success', t('parking', 'Parking has been deleted successfull.'));

            return $this->redirect(['list', 'id' => $parking->city_id]);
        } else {
            session()->setFlash('error', t('parking', 'Parkings are not present in a database.'));

            return $this->redirect(['update', 'id' => $parking->parking_id]);
        }
    }


    public function actionAjaxTranslate($type)
    {
        $returns = $this->translate($type);

        return $this->translateResponse($returns);
    }

    public function translate($type)
    {

        if ($type == 'create') {
            return [
                '<Strong>Error! <Strong> Can not describe the polygon!',
                'Parking type not selected',
                'Enter the name of the park',
                'Create a parking on the map',
                'On the map more than one parking',
                'Error saving parking',
                'On the map more than one parking',
                'Create a parking on the map',
            ];
        }

        if ($type == 'update') {
            return [
                'Error updating parking',
                'Are you sure you want to delete the parking?',
                'Error removing parking',
                'Error while unblocking parking',
                'Parking successfully unlocked',
                'Error while unblocking parking',
                'Parking successfully blocked',
                'Error blocking parking',
                'On the map more than one parking',
                'Create a parking on the map',
            ];
        }

        if ($type == 'leaflet') {
            return [
                'Cancel drawing',
                'Cancel',
                'Remove the last drawn pointRemove the last drawn point',
                'Delete the last point',
                'Draw a line',
                'Draw a parking',
                'Draw a rectangle',
                'Draw a circle',
                'Draw a point',
                'Click and drag to draw a circle',
                'To draw a point, click on the map',
                'To draw a parking Click on the map',
                'Click to continue editing Parking',
                'Click on the first point to complete the edit parking',
                '<strong>Error:</strong> shape edges cannot cross!',
                'Click to start drawing line',
                'Click to continue drawing line',
                'Click last point to finish line',
                'Click and drag to draw rectangle',
                'Release mouse to finish drawing',
                'Save changes',
                'Save',
                'Stop painting, undo all changes',
                'Cansel',
                'Parking editing',
                'No parking for editing',
                'Remove parking',
                'No parking for removal',
                'To edit a parking, drag the corners of polygon',
                'To cancel changes, click Cancel',
                'To remove the parking click on it',
            ];
        }

        return [];
    }

    public function translateResponse($data)
    {
        $result = [];

        foreach ($data as $key) {
            $result[$key] = t('map', $key);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        return ['values' => $result];

    }

    /**
     * Finds the Parking model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Parking the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Parking::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
