<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\modules\parking\assets;

use yii\web\AssetBundle;

class ParkingAsset extends AssetBundle
{

    public $sourcePath = '@frontend/modules/parking/assets/mapEditor';
    public $publishOptions = [
        'forceCopy' => true,
    ];
    public $css = [
    ];
    public $js = [
    ];
    public $depends = [
        'frontend\assets\MapsAsset',
        'frontend\modules\parking\assets\MapEditorAsset',
    ];

}
