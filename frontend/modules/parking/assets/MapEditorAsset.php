<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\modules\parking\assets;

use yii\web\AssetBundle;

class MapEditorAsset extends AssetBundle
{

    public $sourcePath = '@frontend/modules/parking/assets/mapEditor';
    public $publishOptions = [
        'forceCopy' => true,
        'except'    => [
            'init.js',
            'create.js',
            'update.js',
            'functions.js',
        ]
    ];
    public $css = [
        'libs/leaflet.css',
        'dist/leaflet.draw.css',
        'leaflet-0.7.3/leaflet.css',
    ];
    public $js = [
        'leaflet-0.7.3/leaflet.js',
        'libs/leaflet-src.js',
        'src/Leaflet.draw.js',
        'src/edit/handler/Edit.Poly.js',
        'src/edit/handler/Edit.SimpleShape.js',
        'src/edit/handler/Edit.Circle.js',
        'src/edit/handler/Edit.Rectangle.js',
        'src/draw/handler/Draw.Feature.js',
        'src/draw/handler/Draw.Polyline.js',
        'src/draw/handler/Draw.Polygon.js',
        'src/draw/handler/Draw.SimpleShape.js',
        'src/draw/handler/Draw.Rectangle.js',
        'src/draw/handler/Draw.Circle.js',
        'src/draw/handler/Draw.Marker.js',
        'src/ext/LatLngUtil.js',
        'src/ext/GeometryUtil.js',
        'src/ext/LineUtil.Intersect.js',
        'src/ext/Polyline.Intersect.js',
        'src/ext/Polygon.Intersect.js',
        'src/Control.Draw.js',
        'src/Tooltip.js',
        'src/Toolbar.js',
        'src/draw/DrawToolbar.js',
        'src/edit/EditToolbar.js',
        'src/edit/handler/EditToolbar.Edit.js',
        'src/edit/handler/EditToolbar.Delete.js',
//        'layer/tile/Google.js',
//        'layer/tile/Yandex.js',
        'translate.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];

}
