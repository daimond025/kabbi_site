$.ajax({
    "url" : "/lib/parking/ajax-translate",
    "data" : "type=leaflet",
}).done(function(data){
    //  Set the data
    i18n.translator.add(data);
    L.drawLocal = {
	draw: {
		toolbar: {
			actions: {
				title: i18n('Cancel drawing'),
				text: i18n('Cancel')
			},
			undo: {
				title: i18n('Remove the last drawn pointRemove the last drawn point'),
				text: i18n('Delete the last point')
			},
			buttons: {
				polyline: i18n('Draw a line'),
				polygon: i18n('Draw a parking'),
				rectangle: i18n('Draw a rectangle'),
				circle: i18n('Draw a circle'),
				marker: i18n('Draw a point')
			}
		},
		handlers: {
			circle: {
				tooltip: {
					start: i18n('Click and drag to draw a circle')
				}
			},
			marker: {
				tooltip: {
					start: i18n('To draw a point, click on the map')
				}
			},
			polygon: {
				tooltip: {
					start: i18n('To draw a parking Click on the map'),
					cont: i18n('Click to continue editing Parking'),
					end: i18n('Click on the first point to complete the edit parking')
				}
			},
			polyline: {
				error: i18n('<strong>Error:</strong> shape edges cannot cross!'),
				tooltip: {
					start: i18n('Click to start drawing line'),
					cont: i18n('Click to continue drawing line'), 
					end: i18n('Click last point to finish line')
				}
			},
			rectangle: {
				tooltip: {
					start: i18n('Click and drag to draw rectangle')
				}
			},
			simpleshape: {
				tooltip: {
					end: i18n('Release mouse to finish drawing')
				}
			}
		}
	},
	edit: {
		toolbar: {
			actions: {
				save: {
					title: i18n('Save changes'),
					text: i18n('Save')
				},
				cancel: {
					title: i18n('Stop painting, undo all changes'),
					text: i18n('Cancel')
				}
			},
			buttons: {
				edit: i18n('Parking editing'),
				editDisabled: i18n('No parking for editing'),
				remove: i18n('Remove parking'),
				removeDisabled: i18n('No parking for removal')
			}
		},
		handlers: {
			edit: {
				tooltip: {
					text: i18n('To edit a parking, drag the corners of polygon'),
					subtext: i18n('To cancel changes, click Cancel')
				}
			},
			remove: {
				tooltip: {
					text: i18n('To remove the parking click on it')
				}
			}
		}
	}
    };
});