$(document).ready(function () {


    function showMapEditor(container, lat, lon) {
        map = new L.Map(container, {center: new L.LatLng(lat, lon), zoom: 13, zoomAnimation: false});
        map.scrollWheelZoom.disable();
        //Подключаем тайлы
        var osm = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
        var googleLayer = L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
            maxZoom: 20,
            subdomains:['mt0','mt1','mt2','mt3']
        });
        var yndx = L.tileLayer('http://vec{s}.maps.yandex.net/tiles?l=map&v=17.10.19-0&z={z}&x={x}&y={y}&scale=1&lang=ru_RU',{
            maxZoom: 19,
            subdomains:['01','02','03','04'],
            attribution: '<a href="yandex.ru" target="_blank">Яндекс</a>',
            reuseTiles: true,
            updateWhenIdle: false
        });

        //Добавляем слой osm
        map.addLayer(osm);
        //Добавляем control для переключения карт
        map.addControl(new L.Control.Layers({'OSM': osm, "Yandex": yndx, "Google": googleLayer}));
        //Добавляем cintrol для рисования карт
        var drawnItems = new L.FeatureGroup();
        map.addLayer(drawnItems);

        map.on('baselayerchange', function(e){

            switch (e.name) {
                case 'Yandex':
                    map.options.crs = L.CRS.EPSG3395;
                    break;
                default:
                    map.options.crs = L.CRS.EPSG3857;
            }

            map._resetView(new L.LatLng(lat, lon), 13);
        });
    }

    var cityLat = $('#cityLat').val();
    var cityLon = $('#cityLon').val();
    
    if (cityLat != '' && cityLon != '') {
        showMapEditor('mainMap', cityLat, cityLon);

        var layers = new Array();
        var layerCity = new Array();
        var layerOutCity = new Array();
        var layerAirport = new Array();
        var layerStation = new Array();
        var layerReseption = new Array();

        $('input[type=checkbox]').click(function () {
            var id = $(this).attr('id');
            var elemClass = $(this).attr('class');
            if ($(this).is(':checked')) {
                switch (id) {
                    case 'cityParks':
                        showBlockParks('cityParks', getColor('city'));
                        break;
                    case 'reseptionParking':
                        showBlockParks('reseptionParking', getColor('reseptionArea'));
                        break;
                    case 'outCityParks':
                        showBlockParks('outCityParks', getColor('outCity'));
                        break;
                    case 'airportParks':
                        showBlockParks('airportParks', getColor('airport'));
                        break;
                    case 'stationParks':
                        showBlockParks('stationParks', getColor('station'));
                        break;
                    default:
                        addPark(id, elemClass);
                        break
                }
            } else {
                switch (id) {
                    case 'cityParks':
                        removeBlockParks('cityParks');
                        break;
                    case 'reseptionParking':
                        removeBlockParks('reseptionParking');
                        break;
                    case 'outCityParks':
                        removeBlockParks('outCityParks');
                        break;
                    case 'airportParks':
                        removeBlockParks('airportParks');
                        break;
                    case 'stationParks':
                        removeBlockParks('stationParks');
                        break;
                    default:
                        removePark(id, elemClass);
                        break;
                }

            }
        });
    }

    function removePark(id, elemClass) {
        var inGroup = $("#" + elemClass).prop("checked");
        if (inGroup) {
            switch (elemClass) {
                case 'cityParks':
                    map.removeLayer(layerCity[id]);
                    break
                case 'reseptionParking':
                    map.removeLayer(layerReseption[id]);
                    break
                case 'outCityParks':
                    map.removeLayer(layerOutCity[id]);
                    break
                case 'airportParks':
                    map.removeLayer(layerAirport[id]);
                    break
                case 'stationParks':
                    map.removeLayer(layerStation[id]);
                    break
            }
        } else {
            map.removeLayer(layers[id]);
        }

    }

    function  addPark(id, elemClass) {
        var geojsonString = $('#' + id).attr('data-coords');
        var geojsonObject = JSON.parse(geojsonString);
        var myLayer = L.geoJson().addTo(map);
        var inGroup = $("#" + elemClass).prop("checked");
        var type;
        if (inGroup) {
            switch (elemClass) {
                case 'cityParks':
                    layerCity[id] = myLayer;
                    type = 'city';
                    break;
                case 'reseptionParking':
                    layerReseption[id] = myLayer;
                    type = 'reseptionArea';
                    break;
                case 'outCityParks':
                    layerOutCity[id] = myLayer;
                    type = 'outCity';
                    break;
                case 'airportParks':
                    layerAirport[id] = myLayer;
                    type = 'airport';
                    break;
                case 'stationParks':
                    layerStation[id] = myLayer;
                    type = 'station';
                    break
            }
        } else {
            layers[id] = myLayer;
        }
        myLayer.addData(geojsonObject, getColor(type));

    }
    function showBlockParks(mainParkId, color) {
        var classOfParks = mainParkId;
        if (classOfParks === 'cityParks') {
            var geojsonString = $("#" + mainParkId).attr('data-coords');
            if (geojsonString !== '' && geojsonString !== undefined && geojsonString !== null) {
                var geojsonObject = JSON.parse(geojsonString);
                var myLayer = L.geoJson().addTo(map);
                layerCity['cityParks'] = myLayer;
                myLayer.addData(geojsonObject, getColor('basePolygon'));
            } else {
                //var geojsonObject = JSON.parse(geojsonString);
                var myLayer = L.geoJson().addTo(map);
                layerCity['cityParks'] = myLayer;
                //myLayer.addData(geojsonObject);
            }

        }
        if (classOfParks === 'reseptionParking') {
            var geojsonString = $("#" + mainParkId).attr('data-coords');
            if (geojsonString !== '' && typeof geojsonString !== "undefined" && geojsonString !== null) {
                var geojsonObject = JSON.parse(geojsonString);
                var myLayer = L.geoJson().addTo(map);
                layerReseption['reseptionParking'] = myLayer;
                myLayer.addData(geojsonObject, color);
            } else {
                //var geojsonObject = JSON.parse(geojsonString);
                var myLayer = L.geoJson().addTo(map);
                layerReseption['reseptionParking'] = myLayer;
                //myLayer.addData(geojsonObject);
            }
        }
        $("." + classOfParks).each(function () {
            $(this).prop("checked", true);
            var id = $(this).attr('id');
            deleteParkFromMianLayer(id);
            var geojsonString = $(this).attr('data-coords');
            var geojsonObject = JSON.parse(geojsonString);
            var myLayer = L.geoJson().addTo(map);
            switch (mainParkId) {
                case 'cityParks':
                    layerCity[id] = myLayer;
                    break;
                case 'reseptionParking':
                    layerReseption[id] = myLayer;
                    break;
                case 'outCityParks':
                    layerOutCity[id] = myLayer;
                    break;
                case 'airportParks':
                    layerAirport[id] = myLayer;
                    break;
                case 'stationParks':
                    layerStation[id] = myLayer;
                    break;
            }
            myLayer.addData(geojsonObject, color);
        });
    }

    function removeBlockParks(mainParkId) {
        var classOfParks = mainParkId;
        if (classOfParks == "reseptionParking") {
            map.removeLayer(layerReseption['reseptionParking']);
        } else if (classOfParks == 'cityParks') {
            map.removeLayer(layerCity['cityParks']);
        }
        $("." + classOfParks).each(function () {
            var id = $(this).attr('id');
            $(this).prop("checked", false);
            for (var item in layers) {
                map.removeLayer(item);
            }
            switch (mainParkId) {
                case 'cityParks':
                    map.removeLayer(layerCity[id]);
                    break
                case 'reseptionParking':
                    map.removeLayer(layerReseption[id]);
                    break
                case 'outCityParks':
                    map.removeLayer(layerOutCity[id]);
                    break
                case 'airportParks':
                    map.removeLayer(layerAirport[id]);
                    break
                case 'stationParks':
                    map.removeLayer(layerStation[id]);
                    break
            }

        });
    }

    function  deleteParkFromMianLayer(id) {
        if (layers[id] !== undefined) {
            map.removeLayer(layers[id]);
        }
    }
    
});