$(document).ready(function () {

    $.ajax({
        "url": "/lib/parking/ajax-translate",
        "data": "type=update",
    }).done(function (data) {
        //  Set the data
        i18n.translator.add(data);
    });

    parking_id = $_GET('id');
    cityId = $('#parking-city_id').val();

    function showMapEditor(container, lat, lon) {
        polygon = $('#parking-polygon').val();
        type = $('#parking-type').val();
        var geojsonObject = JSON.parse(polygon);
        // geo_layer = L.geoJson(geojsonObject);
        map = new L.Map(container, {center: new L.LatLng(lat, lon), zoom: 13, zoomAnimation: false});
        map.scrollWheelZoom.disable();
        //Подключаем тайлы
        var osm = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
        var googleLayer = L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
            maxZoom: 20,
            subdomains:['mt0','mt1','mt2','mt3']
        });
        var yndx = L.tileLayer('http://vec{s}.maps.yandex.net/tiles?l=map&v=17.10.19-0&z={z}&x={x}&y={y}&scale=1&lang=ru_RU',{
            maxZoom: 19,
            subdomains:['01','02','03','04'],
            attribution: '<a href="yandex.ru" target="_blank">Яндекс</a>',
            reuseTiles: true,
            updateWhenIdle: false
        });
        //Добавляем слой osm
        map.addLayer(osm);
        //Добавляем control для переключения карт
        map.addControl(new L.Control.Layers({'OSM': osm, "Yandex": yndx, "Google": googleLayer}));

        editableLayers = L.geoJson(geojsonObject, {
            style: {color: getColor(type)}
        }).addTo(map);
        map.addLayer(editableLayers);

        options = function (editableLayers) {
            return {
                position: 'topright',
                draw: {
                    polygon: true,
                    polyline: false,
                    circle: false,
                    marker: false
                },
                edit: {
                    featureGroup: editableLayers, //REQUIRED!!
                    remove: true
                }
            };
        };
        var drawControl = new L.Control.Draw(options(editableLayers));
        map.addControl(drawControl);
        function drawnItemHandler(layer) {
            editableLayers.addLayer(layer);
        }

        function build_json(layer) {
            var search_value = $('#leaflet-control-geosearch-qry').value;
            var map_bounds = map.getBounds();
            var geoJson = layer.toGeoJSON();
            var geo = $.extend(true, {}, geoJson,
                {
                    properties: {
                        search_value: search_value,
                        map_bounds: map_bounds
                    }
                });
            return JSON.parse(JSON.stringify(geo));
        }

        map.on('draw:created', function (e) {
            drawnItemHandler(e.layer);
        });
        map.on('draw:edited', function (e) {
            e.layers.eachLayer(drawnItemHandler);
        });
        map.on('draw:deleted', function (e) {
        });

        map.on('baselayerchange', function(e){

            switch (e.name) {
                case 'Yandex':
                    map.options.crs = L.CRS.EPSG3395;
                    break;
                default:
                    map.options.crs = L.CRS.EPSG3857;
            }

            map._resetView(new L.LatLng(lat, lon), 13);
        });
    }

    var cityLat = $('#cityLat').val();
    var cityLon = $('#cityLon').val();
    showMapEditor('editMap', cityLat, cityLon);
    mainLayer = new Array();
    $('#showAll').click(function () {
        if ($(this).is(':checked')) {
            $.ajax({
                type: 'POST',
                cache: false,
                url: '/parking/parking/get-all-parks/?cityId=' + cityId,
                success: function (response) {
                    var parkArr = JSON.parse(response);
                    var length = parkArr.length;
                    var park = null;
                    for (var i = 0; i < length; i++) {
                        park = parkArr[i];
                        var id = park.parking_id;
                        var geojsonString = park.polygon;
                        var geojsonObject = JSON.parse(geojsonString);
                        var myLayer = L.geoJson().addTo(map);
                        mainLayer[id] = myLayer;
                        myLayer.addData(geojsonObject, getColor(park.type));
                    }
                }
            });

        }
        if (!$(this).is(':checked')) {
            for (var item in mainLayer) {
                map.removeLayer(mainLayer[item]);
            }
        }

    });

    function validateCommand(parkCount) {
        var message = null;
        if (parkCount === 0) {
            message = i18n('Create a parking on the map');
        }
        if (parkCount > 1) {
            message = i18n('On the map more than one parking');
        }
        return message;
    }

    $('#save').click(function () {
        $('#polygon').empty();
        var shapes = [];
        editableLayers.eachLayer(function (layer) {
            var shape = layer.toGeoJSON();
            var shape_for_db = JSON.stringify(shape);
            shapes.push(shape_for_db);
        });
        var parkCount = shapes.length;
        var errorMessage = validateCommand(parkCount);
        if (errorMessage !== null) {
            $('#polygon').text(errorMessage);
            return false;
        } else {
            var polygon = shapes[0];
            $('#parking-polygon').val(polygon);
        }
    });

    changeType();
});
$(document).on('ready', function () {
    var selected = $('#type').val();
    if (selected === 'basePolygon') {
        $('#name-section').hide();
        $('#name').val($('select option:selected').text());
    }
    else if (selected === 'reseptionArea') {
        $('#name-section').hide();
        $('#name').val($('select option:selected').text());
    }
    else {
        $('#name-section').show();
        $('#district_section').show();
    }
});

$(document).on('change', '#parking-type', function () {
    changeType();
});
