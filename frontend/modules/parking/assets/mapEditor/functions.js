function getColor(type) {
    var color = '#0033ff';
    switch (type) {
        case 'basePolygon':
            color = '#358C36';
            break;
        case 'city':
            color = '#0033ff';
            break;
        case 'outCity':
            color = '#433879';
            break;
        case 'airport':
            color = '#FF91A0';
            break;
        case 'station':
            color = '#FF91A0';
            break;
        case 'reseptionArea':
            color = '#999999';
            break;
    }
    return color;
}

function changeType () {
    var type = $('#parking-type').val();
    $('section.row').each( function() {
        if ( $(this).hasClass('js-' + type) || $(this).hasClass('js-all') ) {
            $(this).show();
        }
        else {
            $(this).hide();
        }
    });
}