$(document).ready(function () {
    
    $.ajax({
        "url" : "/lib/parking/ajax-translate",
        "data" : "type=create",
    }).done(function(data){
        //  Set the data
        i18n.translator.add(data);
    });
    
    
    cityId = $('#save').data('city');
    mainLayer = new Array();

    function showMapEditor(container, lat, lon) {
        map = new L.Map(container, {center: new L.LatLng(lat, lon), zoom: 13, zoomAnimation: false});
        map.scrollWheelZoom.disable();
        //Подключаем тайлы
        var osm = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
        var googleLayer = L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
            maxZoom: 20,
            subdomains:['mt0','mt1','mt2','mt3']
        });
        var yndx = L.tileLayer('http://vec{s}.maps.yandex.net/tiles?l=map&v=17.10.19-0&z={z}&x={x}&y={y}&scale=1&lang=ru_RU',{
            maxZoom: 19,
            subdomains:['01','02','03','04'],
            attribution: '<a href="yandex.ru" target="_blank">Яндекс</a>',
            reuseTiles: true,
            updateWhenIdle: false
        });
        //Добавляем слой osm
        map.addLayer(osm);
        //Добавляем control для переключения карт
        map.addControl(new L.Control.Layers({'OSM': osm, "Yandex": yndx, "Google": googleLayer}));
        //Добавляем cintrol для рисования карт
        drawnItems = new L.FeatureGroup();
        map.addLayer(drawnItems);
        var options = {
            position: 'topright',
            draw: {
                polygon: {
                    allowIntersection: false, // Restricts shapes to simple polygons
                    drawError: {
                        color: '#e1e100', // Color the shape will turn when intersects
                        message: i18n('<Strong>Error! <Strong> Can not describe the polygon!') // Message that will show when intersect
                    },
                    shapeOptions: {
                        color: '#1b3834',
                        weight: 1,
                        opacity: 0.9,
                        stroke: true,
                        fill: true,
                        clickable: true
                    }
                }
            },
            edit: {
                featureGroup: drawnItems, //REQUIRED!!
                remove: true,
                polyline: {
                    shapeOptions: {
                        color: '#1b3834',
                        weight: 1,
                        opacity: 0.9
                    }
                },
                polygon: {
                    allowIntersection: false, // Restricts shapes to simple polygons
                    drawError: {
                        color: '#e1e100', // Color the shape will turn when intersects
                        message: i18n('<Strong>Error! <Strong> Can not describe the polygon!') // Message that will show when intersect
                    },
                    shapeOptions: {
                        color: '#1b3834',
                        weight: 1,
                        opacity: 0.9,
                        stroke: true,
                        fill: true,
                        clickable: true
                    }
                }
            }
        };
        var drawControl = new L.Control.Draw(options);
        map.addControl(drawControl);
        map.on('draw:created', function (e) {
            var type = e.layerType,
                    layer = e.layer;
            drawnItems.addLayer(layer);
        });

        map.on('baselayerchange', function(e){

            switch (e.name) {
                case 'Yandex':
                    map.options.crs = L.CRS.EPSG3395;
                    break;
                default:
                    map.options.crs = L.CRS.EPSG3857;
            }

            map._resetView(new L.LatLng(lat, lon), 13);
        });
    }

    var cityLat = $('#cityLat').val();
    var cityLon = $('#cityLon').val();
    showMapEditor('editMap', cityLat, cityLon);

    $('#showAll').click(function () {
        if ($(this).is(':checked')) {
            $.ajax({
                type: 'POST',
                cache: false,
                url: '/parking/parking/get-all-parks/?cityId=' + cityId,
                success: function (response) {
                    var parkArr = JSON.parse(response);
                    var length = parkArr.length;
                    var park = null;
                    for (var i = 0; i < length; i++) {
                        park = parkArr[i];
                        var id = park.parking_id;
                        var geojsonString = park.polygon;
                        var geojsonObject = JSON.parse(geojsonString);
                        var myLayer = L.geoJson().addTo(map);
                        mainLayer[id] = myLayer;
                        myLayer.addData(geojsonObject, getColor(park.type) );
                    }
                }
            });

        }
        if (!$(this).is(':checked')) {
            for (var item in mainLayer) {
                map.removeLayer(mainLayer[item]);
            }
        }

    });

    changeType();
});


function validateCommand(parkCount) {
    var message = null;
    if (parkCount === 0) {
        message = i18n('Create a parking on the map');
    }
    if (parkCount > 1) {
        message = i18n('On the map more than one parking');
    }
    return message;
}


$('#save').click(function () {
    $('#polygon').empty();
    var shapes = [];
    drawnItems.eachLayer(function (layer) {
        var shape = layer.toGeoJSON();
        var shape_for_db = JSON.stringify(shape);
        shapes.push(shape_for_db);
    });
    var parkCount = shapes.length;
    var errorMessage = validateCommand(parkCount);
    if (errorMessage !== null) {
        $('#polygon').text(errorMessage);
        return false;
    } else {
        var polygon = shapes[0];
        $('#parking-polygon').val(polygon);
    }
});

$(document).on('change', '#parking-type', function () {
    changeType();
});

