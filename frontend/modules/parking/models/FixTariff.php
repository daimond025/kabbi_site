<?php

namespace app\modules\parking\models;

use Yii;

/**
 * This is the model class for table "tbl_fix_tariff".
 *
 * @property integer $fix_id
 * @property integer $from
 * @property integer $to
 */
class FixTariff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_fix_tariff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from', 'to'], 'required'],
            [['from', 'to'], 'integer'],
            [['from'], 'exist', 'skipOnError' => true, 'targetClass' => Parking::className(), 'targetAttribute' => ['from' => 'parking_id']],
            [['to'], 'exist', 'skipOnError' => true, 'targetClass' => Parking::className(), 'targetAttribute' => ['to' => 'parking_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fix_id' => 'Fix ID',
            'from' => 'From',
            'to' => 'To',
        ];
    }
    
    
}
