<?php

namespace frontend\modules\client\modules\cabinet;

use app\components\behavior\ControllerBehavior;
use frontend\bootstrap\Language;
use yii\helpers\ArrayHelper;

/**
 * Class Module
 * @package app\modules\client
 *
 * @mixin ControllerBehavior
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\client\modules\cabinet\controllers';

    public function init()
    {
        parent::init();

        app()->setComponents([
            'user' => [
                'class' => 'yii\web\User',
                'identityClass'   => 'frontend\modules\client\modules\cabinet\models\Client',
                'enableAutoLogin' => true,
                'loginUrl'        => ['/client/cabinet/profile/login/'],
                'identityCookie' => ['name' => '_identity_client', 'httpOnly' => true],
                'idParam' => '__id_client',
            ],
            'orderService' => 'frontend\modules\order\components\OrderService',
        ]);

        app()->errorHandler->errorAction = 'client/cabinet/profile/error';

        app()->get('bootstrapLanguage')->bootstrap(app());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return \yii\base\Module::behaviors();
    }

}
