<?php

use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $company app\modules\client\Client */

if (!empty($orders)):?>
    <?
    $this->registerJs("calendarPeriodInit();");
    $this->registerJsFile('/js/leaflet-0.7.3/leaflet.js');
    ?>
    <div class="client_orders">
        <form name="order_filter" action="<?= Url::to(['order-search', 'id' => get('id')]) ?>" method="post">
            <div class="co_filters">
                <div class="cof_left">
                    <div class="cof_first">
                        <label><input name="filter" type="radio" checked value="limit"/> <?= t('client', 'Last 10') ?>
                        </label>
                    </div>
                    <div class="cof_second disabled_cof">
                        <label><input name="filter" id="cof_time_choser" type="radio" value="period"> <?= t('reports',
                                'Period') ?></label>
                        <div class="cof_date notranslate disabled_select">
                            <a class="s_datepicker a_sc select"><?= date("d.m.Y",
                                    mktime(0, 0, 0, date("m"), date("d") - 14, date("Y"))); ?></a>
                            <div class="s_datepicker_content b_sc" style="display: none;">
                                <input name="first_date" class="sdp_input" type="text" id="first_date_link" value="<?= date("d.m.Y",
                                    mktime(0, 0, 0, date("m"), date("d") - 14, date("Y"))); ?>">
                                <div class="sdp first_date"></div>
                            </div>
                        </div>
                        <hr>
                        <div class="cof_date notranslate disabled_select">
                            <a class="s_datepicker a_sc select"><?= date("d.m.Y"); ?></a>
                            <div class="s_datepicker_content b_sc" style="display: none;">
                                <input name="second_date" class="sdp_input" type="text" id="second_date_link" value="<?= date("d.m.Y"); ?>">
                                <div class="sdp"></div>
                            </div>
                        </div>
                    </div>
                    <?php
                          $this->registerJs('savePdfLink();');
                    ?>

                    <?php echo \yii\helpers\Html::a('Cкачать pdf',
                        ['/reports/order/dump-client-orders/pdf/' . get('id')], ['id' => 'dump_client_orders_link']); ?>
                </div>
            </div>
        </form>
        <ul class="co_list">
            <?= $this->render('_orders', ['orders' => $orders]) ?>
        </ul>
    </div>
<? else: ?>
    <p><?= t('app', 'Empty') ?></p>
<? endif; ?>
