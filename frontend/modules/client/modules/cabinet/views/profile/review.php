<?php

use yii\helpers\Html;

$formatter = app()->formatter;

if(!empty($client->reviews)):?>
    <div class="reviews_statistic">
        <ul>
            <li>
                <? $percent = $client->reviewRaiting->getPercentValue($client->reviewRaiting->one); ?>
                <span class="rs_stars"><i class="active"></i><i></i><i></i><i></i><i></i></span>
                <span class="rs_line"><span style="width: <?= Html::encode($percent); ?>"></span></span>
                <span class="rs_proc"><?= Html::encode($percent); ?></span>
                <span class="rs_items"><?= Html::encode($client->reviewRaiting->one); ?> <?=t('client', 'times')?></span>
            </li>
            <li>
                <?$percent = $client->reviewRaiting->getPercentValue($client->reviewRaiting->two)?>
                <span class="rs_stars"><i class="active"></i><i class="active"></i><i></i><i></i><i></i></span>
                <span class="rs_line"><span style="width: <?= Html::encode($percent); ?>"></span></span>
                <span class="rs_proc"><?= Html::encode($percent); ?></span>
                <span class="rs_items"><?= Html::encode($client->reviewRaiting->two); ?> <?=t('client', 'times')?></span>
            </li>
            <li>
                <?$percent = $client->reviewRaiting->getPercentValue($client->reviewRaiting->three)?>
                <span class="rs_stars"><i class="active"></i><i class="active"></i><i class="active"></i><i></i><i></i></span>
                <span class="rs_line"><span style="width: <?= Html::encode($percent); ?>"></span></span>
                <span class="rs_proc"><?= Html::encode($percent); ?></span>
                <span class="rs_items"><?= Html::encode($client->reviewRaiting->three); ?> <?=t('client', 'times')?></span>
            </li>
            <li>
                <?$percent = $client->reviewRaiting->getPercentValue($client->reviewRaiting->four)?>
                <span class="rs_stars"><i class="active"></i><i class="active"></i><i class="active"></i><i class="active"></i><i></i></span>
                <span class="rs_line"><span style="width: <?= Html::encode($percent); ?>"></span></span>
                <span class="rs_proc"><?= Html::encode($percent); ?></span>
                <span class="rs_items"><?= Html::encode($client->reviewRaiting->four); ?> <?=t('client', 'times')?></span>
            </li>
            <li>
                <?$percent = $client->reviewRaiting->getPercentValue($client->reviewRaiting->five)?>
                <span class="rs_stars"><i class="active"></i><i class="active"></i><i class="active"></i><i class="active"></i><i class="active"></i></span>
                <span class="rs_line"><span style="width: <?= Html::encode($percent); ?>"></span></span>
                <span class="rs_proc"><?= Html::encode($percent); ?></span>
                <span class="rs_items"><?= Html::encode($client->reviewRaiting->five); ?> <?=t('client', 'times')?></span>
            </li>
        </ul>
    </div>
    <ul class="review_list">
        <?foreach($client->reviews as $review):
            if (!is_object($review->order))
                continue;
            ?>
            <li>
            <span class="rl_id">
                <a href="<?=  \yii\helpers\Url::to(['/client/cabinet/order/view', 'order_number' => $review->order->order_number])?>" class="open_order">№<?= Html::encode($review->order->order_number); ?></a>
                <?$order_time = $review->order->getOrderTimestamp($review->order->create_time)?>
                <span>
                    <?= t('app', 'on') ?>
                    <?= $formatter->asDate($order_time, 'shortDate') ?>
                    <br/>
                    <?= $formatter->asTime($order_time, 'short') ?>
                </span>
            </span>
            <span class="rl_content">
                <span class="rl_stars">
                    <?for($i = 1; $i <= 5; $i++):?>
                        <?if($i <= $review->rating):?>
                            <i class="active"></i>
                        <?else:?>
                            <i></i>
                        <?endif?>
                    <?endfor;?>
                </span>
                <i>«<?= Html::encode($review->text); ?>»</i>
            </span>
            </li>
        <?endforeach;?>
    </ul>
<?else:?>
    <p><?=t('app', 'Empty')?></p>
<? endif; ?>
