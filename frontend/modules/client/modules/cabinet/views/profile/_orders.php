<?php
use common\services\OrderStatusService;
use yii\helpers\Html;
use frontend\modules\tenant\models\Currency;

/* @var $this yii\web\View */
/* @var $client app\modules\client\Client */

$formatter = app()->formatter;

if(!empty($orders)):
    $this->registerJs("spolerTextInit()");
    foreach ($orders as $order):?>
        <li>
            <span class="cl_id">
                <a href="<?php $form = '_read';
                echo  yii\helpers\Url::to(['order/view' , 'order_number' => $order->order_number])?>" class="open_order">№<?=$order->order_number?></a>
                <?$order_time = $order->getOrderTimestamp($order['create_time'])?>
                <span>
                    <?= t('app', 'on') ?>
                    <?= $formatter->asDate($order_time, 'shortDate') ?>
                    <br/>
                    <?= $formatter->asTime($order_time, 'short') ?>
                </span>
            </span>
            <span class="cl_info">
                <span class="cli_app"><span><i class="<?=  \app\modules\order\models\Order::getDeviceIconCssClass ($order->device)?>"></i> <b><?= Html::encode($order->getDeviceName()); ?></b></span>
                    <?if(!empty($order->company)):?>
                        <?=t('client', 'From organization')?>
                        <br/>
                        <a href="<?=  \yii\helpers\Url::to(['/client/company/update', 'id' => $order->company->company_id])?>"><?= Html::encode($order->company->name); ?></a>
                    <?endif?>
                </span>
                <div class="order_direction">
                <?foreach($order->address as $key => $address):?>
                    <span class="od_d"><?= Html::encode($key); ?></span>
                    <div>
                        <span>
                            <?= Html::encode($address['city']); ?>, <?=Html::encode($address['street']); ?>
                            <?php if(!empty($address['house'])) echo ', ' . Html::encode($address['house']); ?>
                        </span>
                        <i><?= Html::encode($address['parking']); ?></i>
                    </div>
                <?endforeach?>
                </div>
            </span>
            <?if(isset($order->clientReview)):?>
                <span class="cli_review">
                    <span class="clir_stars">
                        <?for($i = 1; $i <= 5; $i++):?>
                            <?if($i <= $order->clientReview->rating):?>
                                <i class="active"></i>
                            <?else:?>
                                <i></i>
                            <?endif?>
                        <?endfor;?>
                    </span>
                    <span class="clir_content">
                        <i class="clirc_text"></i>
                        <span style="display: none;">«<?= Html::encode($order->clientReview->text); ?>»</span>
                        <a class="review_spoler"><?=t('client', 'Fully')?></a>
                    </span>
                </span>
            <?endif?>
            <span class="cli_details">
                <b class="green"><?= Html::encode(OrderStatusService::translate($order->status_id, $order->position_id)); ?></b>
                <?if(!empty($order->detailCost->summary_cost)):?>
                    <b>
                        <?= $formatter->asMoney($order->detailCost->summary_cost,
                            Currency::getCurrencySymbol($order->currency_id)); ?></b>
                <?endif?>
            </span>
        </li>
    <?endforeach;?>
<?else:?>
    <p><?=t('app', 'Empty')?></p>
<?endif;?>