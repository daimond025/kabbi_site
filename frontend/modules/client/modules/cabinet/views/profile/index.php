<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\modules\client\modules\cabinet\models\Client */
/* @var $formData array */

app\modules\client\assets\ClientsAsset::register($this);
\frontend\assets\LeafletAsset::register($this);
\frontend\modules\balance\assets\BalanceAsset::register($this);
$this->registerJs('balanceOperationInit();');
?>

<h1><?= Html::encode($model->getFullName()) ?></h1>
<section class="main_tabs">
    <div class="tabs_links">
        <ul>
            <li><a href="#profile" class="t01"><?= t('app', 'Profile') ?></a></li>
            <li>
                <a href="#orders" class="t02" data-href="<?= Url::to(['orders', 'id' => $model->client_id]) ?>">
                    <?= t('client', 'Orders') ?>
                </a>
            </li>
            <li>
                <a href="#bonus" class="t03" data-href="<?= Url::to([
                    'balance/show-balance',
                    'id'      => $model->client_id,
                    'kind_id' => \app\modules\balance\models\Account::CLIENT_BONUS_KIND,
                    'city_id' => $model->city_id,
                ]) ?>">
                    <?= t('client', 'Bonuses') ?>
                </a>
            </li>
            <li>
                <a href="#reviews" class="t04" data-href="<?= Url::to(['reviews', 'id' => $model->client_id]) ?>">
                    <?= t('client', 'Reviews') ?>
                </a>
            </li>
            <li>
                <a href="#balance" class="t05" data-href="<?= Url::to([
                    'balance/show-balance',
                    'id'      => $model->client_id,
                    'kind_id' => \app\modules\balance\models\Account::CLIENT_KIND,
                    'city_id' => $model->city_id,
                ]) ?>">
                    <?= t('client', 'Balance') ?>
                </a>
            </li>
        </ul>
    </div>
    <div class="tabs_content">
        <div id="t01" data-view="index">
            <?= $this->render('_profile', ['model' => $model, 'formData' => $formData]); ?>
        </div>
        <div id="t02" data-view="orders"></div>
        <div id="t03" data-view="bonus"></div>
        <div id="t04" data-view="reviews"></div>
        <div id="t05" data-view="balance"></div>
    </div>
</section>
