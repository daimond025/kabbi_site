<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\client\modules\cabinet\models\Client */
/* @var $form yii\widgets\ActiveForm */
/* @var $formData array */
/* @var $languages array */

?>

<? $form = ActiveForm::begin([
    'id' => 'client-form',
    'errorCssClass' => 'input_error',
    'successCssClass' => 'input_success',
    'enableClientValidation' => true,
    'validateOnBlur' => false,
    'options' => ['enctype' => 'multipart/form-data'],
]); ?>

<!--Фото-->
<? if (!$model->isNewRecord): ?>
    <?= \frontend\widgets\file\Logo::widget([
        'form'         => $form,
        'model'        => $model,
        'attribute'    => 'photo',
        'ajax'         => true,
        'uploadAction' => \yii\helpers\Url::to(['profile/upload-logo', 'client_id' => $model->client_id]),
    ]) ?>
<? endif ?>

<!--Город-->
<section class="row">
    <div class="row_label"><?= Html::activeLabel($model, 'city') ?></div>
    <div class="row_input">
        <div class="input_with_icon">
            <div
                class="input_with_icon"><?= !empty($model->city)
                    ? Html::encode($model->city->name) : '<div class="row_input"></div>' ?></div>
            <span></span>
        </div>
    </div>
</section>

<!--ФИО-->
<section class="row">
    <div class="row_label">
        <label <? if ($model->isAttributeRequired('last_name')): ?>class="required"<? endif ?>>
            <?= Html::encode($model->getAttributeLabel('last_name')); ?>
        </label>&nbsp;
        <label <? if ($model->isAttributeRequired('name')): ?>class="required"<? endif ?>>
            <?= Html::encode($model->getAttributeLabel('name')); ?>
        </label>&nbsp;
        <label <? if ($model->isAttributeRequired('second_name')): ?>class="required"<? endif ?>>
            <?= Html::encode($model->getAttributeLabel('second_name')); ?>
        </label>
    </div>
    <div class="row_input">
        <div class="grid_3">
            <?= $form->field($model, 'last_name')->begin(); ?>
            <div class="grid_item"><?= Html::activeTextInput($model, 'last_name') ?></div>
            <?= Html::error($model, 'last_name',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
            <?= $form->field($model, 'last_name')->end(); ?>
            <?= $form->field($model, 'name')->begin(); ?>
            <div class="grid_item"><?= Html::activeTextInput($model, 'name') ?></div>
            <?= Html::error($model, 'name',
                ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
            <?= $form->field($model, 'name')->end(); ?>
            <?= $form->field($model, 'second_name')->begin(); ?>
            <div class="grid_item"><?= Html::activeTextInput($model, 'second_name') ?></div>
            <?= $form->field($model, 'second_name')->end(); ?>
        </div>
    </div>
</section>

<!--Дата рождения-->
<section class="row">
    <div class="row_label"><?= Html::activeLabel($model, 'birth') ?></div>
    <div class="row_input">
        <div class="grid_3">
            <?= $form->field($model, 'birth_day')->begin(); ?>
            <div class="grid_item">
                <?= Html::activeDropDownList($model, 'birth_day', $formData['birth_day'], [
                    'prompt' => t('user', 'Day'),
                    'class' => 'default_select',
                ]); ?>
            </div>

            <?= $form->field($model, 'birth_day')->end(); ?>
            <?= $form->field($model, 'birth_month')->begin(); ?>
            <div class="grid_item">
                <?= Html::activeDropDownList($model, 'birth_month', $formData['birth_month'], [
                    'prompt' => t('user', 'Month'),
                    'class' => 'default_select',
                ]); ?>
            </div>
            <?= $form->field($model, 'birth_month')->end(); ?>
            <?= $form->field($model, 'birth_year')->begin(); ?>
            <div class="grid_item">
                <?= Html::activeDropDownList($model, 'birth_year', $formData['birth_year'], [
                    'prompt' => t('user', 'Year'),
                    'class' => 'default_select',
                ]); ?>
            </div>
            <?= $form->field($model, 'birth_year')->end(); ?>
        </div>
    </div>
</section>

<!--Моб. телефон-->
<section class="row">
    <div class="row_label"><?= Html::activeLabel($model, 'phone') ?></div>
    <div class="row_input">
        <div class="input_with_icon">
            <div
                class="input_with_icon"><?= !empty($model->clientPhones)
                    ? Html::encode($model->clientPhones[0]->value) : '<div class="row_input"></div>' ?></div>
            <span></span>
        </div>
    </div>
</section>

<!--E-mail-->
<section class="row">
    <?= $form->field($model, 'email')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($model, 'email') ?></div>
    <div class="row_input">
        <?= Html::activeTextInput($model, 'email') ?>
        <?= Html::error($model, 'email',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($model, 'email')->end(); ?>
</section>

<section class="submit_form">
    <?= Html::submitInput(t('app', 'Save')) ?>
</section>

<?php ActiveForm::end(); ?>
