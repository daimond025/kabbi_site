<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model frontend\modules\client\modules\cabinet\models\LoginForm */
/* @var $tenant common\modules\tenant\models\Tenant */

frontend\modules\tenant\assets\LoginAsset::register($this);
//\frontend\assets\AppAsset::register($this);
$this->registerJsFile('/js/functions.js');
$this->registerJs('phone_mask_init();');

$this->title = $tenant->company_name ? t('app', ' {name}', ['name' => $tenant->company_name], $model->lang) : t('app', 'Login', $model->lang);
?>
<div class="login_form">
<?php
$form = ActiveForm::begin([
    'id' => 'login-form',
    'errorCssClass' => 'input_error',
    'successCssClass' => 'input_success',
    'enableAjaxValidation' => false,
    'enableClientValidation' => true,
    'validateOnSubmit' => true,
]);
?>
<div class="login">
    <?= $this->render('@app/modules/tenant/views/user/_login_header', compact('tenant')) ?>
    <section>
        <?= $form->field($model, 'phone')->begin(); ?>
        <?= Html::activeTextInput($model, 'phone',
            ['placeholder' => t('app', 'Phone', [], $model->lang), 'class' => 'form-group mask_phone required']); ?>
        <?= Html::error($model, 'phone', ['tag' => 'p', 'class' => 'help-block help-block-error']); ?>
        <?= $form->field($model, 'phone')->end(); ?>

        <?= $form->field($model, 'password')->begin(); ?>
        <?= Html::activePasswordInput($model, 'password',
            ['placeholder' => t('app', 'Password', [], $model->lang), 'class' => 'form-group required']); ?>
<!--        <a class="change_tab">--><?//= t('app', 'Reset password') ?><!--</a>-->
        <?= Html::error($model, 'password', ['tag' => 'p', 'class' => 'help-block help-block-error']); ?>
        <?= $form->field($model, 'password')->end(); ?>

        <?= $form->field($model, 'rememberMe')->checkbox() ?>
        <div style="text-align: center; margin: 0;"><input class="button" type="submit"
                                                           value="<?= t('app', 'Login', [], $model->lang) ?>"/>
        </div>
    </section>
    <?php ActiveForm::end(); ?>
</div>

<div class="restore_pass" style="display: none;">
    <?= $this->render('@app/modules/tenant/views/user/_login_header', compact('tenant')) ?>
    Восстановление пароля<br/>
    <a class="change_tab"><?= t('app', 'Password found', [], $model->lang) ?></a>
</div>
</div>

<?php

echo Html::tag('div', Html::dropDownList('lang', $model->lang,
    ['ru' => 'Русский', 'en' => 'English', 'de' => 'Deutschland']),
    ['class' => 'lang-selector','data-url'=>"/cabinet/login?lang="]);

