<?php
/* @var $this yii\web\View */
/* @var $company app\modules\client\models\ClientCompany */
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\balance\models\Account;

app\modules\client\assets\ClientsAsset::register($this);
\frontend\assets\LeafletAsset::register($this);
\frontend\modules\balance\assets\BalanceAsset::register($this);
$this->registerJs('balanceOperationInit();');

$this->title = $company->name;
?>
<!--div class="pt_buttons" style="position: relative; top: 10px;">
    <a href="#" class="pt_call"><span></span></a>
</div-->
<div class="bread"><a href="<?= Url::toRoute('index') ?>"><?= t('app', 'Organization') ?></a></div>
<h1><?= Html::encode($this->title) ?></h1>
<section class="main_tabs">
    <div class="tabs_links">
        <ul>
            <li><a href="#profile" class="t01"><?= t('client', 'Profile') ?></a>
            </li>
            <? if(user()->isAdmin($company->company_id)): ?>
            <li>
                <a href="#stuff" class="t02" data-href="<?= Url::to(['company/stuff', 'id' => $company->company_id]) ?>">
                    <?= t('client',
                        'Stuff') ?>
                </a>
            </li>
            <? endif; ?>
            <li>
                <a href="#orders" data-href="<?= Url::to(['company/orders', 'id' => $company->company_id]) ?>"class="t03">
                    <?= t('client', 'Orders') ?>
                </a>
            </li>
            <? if(user()->isAdmin($company->company_id)): ?>
            <li><a href="#balance" class="t04" data-href="<?= Url::to([
                    'balance/show-balance',
                    'id' => $company->company_id,
                    'kind_id' => Account::COMPANY_KIND,
                    'city_id' => $company->city_id
                ]) ?>">
                    <?= t('client', 'Balance') ?>
                </a>
            </li>
            <? endif; ?>
        </ul>
    </div>
    <div class="tabs_content">
        <div id="t01">
            <?php
            echo $this->render('_form_read', [
                'company' => $company,
                'user_city_list' => $user_city_list,
                'tariff_list' => $tariff_list,
                'dataForm' => $dataForm,
            ]) ?>
        </div>
        <? if(user()->isAdmin($company->company_id)): ?>
        <div id="t02"></div>
        <? endif; ?>
        <div id="t03"></div>
        <? if(user()->isAdmin($company->company_id)): ?>
            <div id="t04"></div>
        <? endif; ?>
    </div>
</section>
