<?php

/* @var string $pjaxId */
/* @var array $cityList */
/* @var array $positionList */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\employee\models\worker\WorkerSearch */
/* @var string $workerDetailViewUrl */

use frontend\widgets\filter\Filter;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

//$filter = Filter::begin([
//    'pjaxId' => $pjaxId,
//    'model'  => $searchModel,
//]);
//echo $filter->input('input_search');
//Filter::end();

Pjax::begin(['id' => $pjaxId]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'       => "{items}",
    'tableOptions' => [
        'class' => 'people_table',
    ],
    'columns'      => [
        [
            'attribute' => 'name',
            'label' => t('client', 'Name'),
            'content' => function ($model) {
                $name_innerHtml = Html::tag('span', $model->isFileExists($model->logo) ? $model->getPictureHtml(Html::encode($model->logo), false) : '', ['class' => 'pt_photo']);
                $name_innerHtml .= Html::tag('span', Html::encode($model->name), ['class' => 'pt_fios']);

                $innerHtml = Html::a($name_innerHtml,['view', 'id' => $model->company_id], ['data-pjax' => 0]);

                return $innerHtml;
            },
            'headerOptions' => ['class' => 'pt_fio', 'style' => 'width: 70%'],
        ],
        [
            'attribute' => 'block',
            'label' => t('client', 'Activity'),
            'content' => function ($model) {
                return $model->getActiveFieldValue();
            },
            'headerOptions' => ['style' => 'width: 30%'],
        ],
    ],
]);

Pjax::end();



