<?php

use yii\helpers\Html;

/* @var string $pjaxId */
/* @var $this yii\web\View */
/* @var array $positionList */
/* @var $dataProvider yii\data\ArrayDataProvider */
/* @var $searchModel \frontend\modules\employee\models\groups\WorkerGroupSearch */

app\modules\client\assets\ClientsAsset::register($this);
\frontend\assets\LeafletAsset::register($this);
\frontend\modules\balance\assets\BalanceAsset::register($this);
$this->title = Yii::t('client', 'Organizations');
?>

<h1><?= Html::encode($this->title) ?></h1>
<section class="main_tabs">
    <?= $this->render('_grid', compact('searchModel', 'dataProvider', 'pjaxId')) ?>
</section>