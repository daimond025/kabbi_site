<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $company app\modules\client\models\ClientCompany */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="non-editable">

    <!--Город, где работает организация-->


    <h2><?= t('client', 'Organization') ?></h2>

    <!--Юридический адрес-->
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'full_name') ?></div>
        <div class="row_input">
            <div class="input_with_text">
                <div class="row_input"><?= !empty($company->full_name) ? Html::encode($company->full_name) : '' ?></div>
            </div>
        </div>
    </section>

    <!--Сокращённое наименование-->
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'name') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($company->name) ? Html::encode($company->name) : '' ?></div>
        </div>
    </section>

    <!--Юридический адрес-->
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'legal_address') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($company->legal_address) ? Html::encode($company->legal_address) : '' ?></div>
        </div>
    </section>

    <!--Почтовый (фактический) адрес-->
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'post_address') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($company->post_address) ? Html::encode($company->post_address) : '' ?></div>
        </div>
    </section>

    <!--Директор-->
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'director') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($company->director) ? Html::encode($company->director) : '' ?></div>
        </div>
    </section>

    <!--Должность директора-->
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'director_post') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($company->director_post) ? Html::encode($company->director_post) : '' ?></div>
        </div>
    </section>

    <!--Главный бухглатер-->
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'bookkeeper') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($company->bookkeeper) ? Html::encode($company->bookkeeper) : '' ?></div>
        </div>
    </section>

    <!--ИНН-->
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'inn') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($company->inn) ? Html::encode($company->inn) : '' ?></div>
        </div>
    </section>

    <!--КПП-->
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'kpp') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($company->kpp) ? Html::encode($company->kpp) : '' ?></div>
        </div>
    </section>

    <!--ОГРН или ОГРНИП-->
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'ogrn') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($company->ogrn) ? Html::encode($company->ogrn) : '' ?></div>
        </div>
    </section>

    <h2><?= t('client', 'Contacts of organization') ?></h2>

    <!--Телефон-->
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'work_phone') ?></div>
        <div class="row_input">
            <div class="input_with_text input_with_icon">
                <a href="tel:<?= !empty($company->work_phone) ? '+' . $company->work_phone : '' ?>" class="call_now"><span></span></a>
                <div class="row_input"><?= !empty($company->work_phone) ? Html::encode($company->work_phone) : '' ?></div>
                <span></span>
            </div>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'email') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($company->email) ? Html::encode($company->email) : '' ?></div>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'site') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($company->site) ? Html::encode($company->site) : '' ?></div>
        </div>
    </section>

    <h2><?= t('client', 'Contact name') ?></h2>

    <section class="row">
        <div class="row_label">
            <div class="row_input">
                <?= Html::encode($company->getAttributeLabel('contact_last_name')) ?>
                <?= Html::encode($company->getAttributeLabel('contact_name')) ?>
                <?= Html::encode($company->getAttributeLabel('contact_second_name')) ?>
            </div>
        </div>
        <div class="row_input">
            <div class="grid_3">
                <div class="row_input"><?= Html::encode($company->contact_last_name) ?></div>
                <div class="row_input"><?= Html::encode($company->contact_name) ?></div>
                <div class="grid_item"><?= Html::encode($company->contact_second_name) ?></div>
            </div>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'contact_phone') ?></div>
        <div class="row_input">
            <div class="input_with_text input_with_icon">
                <a href="tel:<?= !empty($company->contact_phone) ? '+' . $company->contact_phone : '' ?>" class="call_now"><span></span></a>
                <div class="row_input"><?= !empty($company->contact_phone) ? Html::encode($company->contact_phone) : '' ?></div>
                <span></span>
            </div>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'contact_email') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($company->contact_email) ? Html::encode($company->contact_email) : '' ?></div>
        </div>
    </section>
</div>