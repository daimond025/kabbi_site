<?php
/* @var $this yii\web\View */
/* @var $clients app\modules\client\models\Client*/
use yii\helpers\Url;
?>

<div id="company_stuff" class="ajax">
        <?= $this->render('_stuff', compact('searchModel', 'dataProvider', 'pjaxId')) ?>
</div>
