<?php

use frontend\widgets\filter\Filter;
use yii\helpers\Html;
use frontend\widgets\LinkPager;
use yii\grid\GridView;
use yii\widgets\Pjax;

$filter = Filter::begin([
    'pjaxId' => $pjaxId,
    'model'  => $searchModel,
]);
echo $filter->input('input_search', ['placeHolder' => t('client', 'Filter by name or phone')]);
Filter::end();


Pjax::begin(['id' => $pjaxId]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'       => "{items}",
    'tableOptions' => [
        'class' => 'people_table',
    ],
    'pager'        => [
        'class'         => LinkPager::className(),
        'prevPageLabel' => t('app', 'Prev'),
        'nextPageLabel' => t('app', 'Next'),
    ],
    'columns'      => [
        [
            'attribute'      => 'fullName',
            'label'          => t('client', 'Client'),
            'contentOptions' => function ($model, $key, $index, $column) {
                return ['class' => 'pt_fio'];
            },
            'content'        => function ($model) use ($clientDetailViewUrl) {
                $innerHtml = Html::tag('span',
                    $model->isFileExists($model->photo) ? $model->getPictureHtml(Html::encode($model->photo),
                        false) : '', ['class' => 'pt_photo']);
                $name_innerHtml = [];
                if (!empty($model->fullName)) {
                    $name_innerHtml[] = Html::tag('span', Html::encode($model->fullName), ['class' => 'pt_fios']);
                }
                if (!empty($model->clientPhones)) {
                    foreach ($model->clientPhones as $phone) {
                        $name_innerHtml[] = Html::tag('span', Html::encode($phone->value));
                    }
                }
                $innerHtml .= implode('<br>', $name_innerHtml);

                return $innerHtml;
            },
            'headerOptions'  => ['class' => 'pt_fio', 'style' => 'width: 70%'],
        ],
        [
            'attribute' => 'activeFieldValue',
            'label'     => t('client', 'Black list'),
            'headerOptions'  => ['style' => 'width: 30%'],
        ],
    ],
]);

Pjax::end();