<?php

use yii\helpers\Html;
?>

<footer class="main_footer">
    <p>&copy; <?= date('Y') . ', ' . Html::encode($companyName); ?></p>
</footer>
