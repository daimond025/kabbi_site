<?php
use yii\helpers\Html;
?>
<header class="main_header">
    <div class="header_content">
        <div class="company-name"><?= Html::encode($companyName); ?></div>
        <? if (!Yii::$app->user->isGuest): ?>
            <div class="hc_main">
                <div class="hcm_right">
                    <div class="hcmr_user">
                        <a class="a_sc"><?= Html::encode(app()->user->identity->getShortName()); ?></a>
                        <div class="b_sc">
                            <a id="logout" href="<?= app()->urlManager->createUrl('/client/cabinet/profile/logout') ?>">
                                <?= t('user', 'Logout') ?></a>
                        </div>
                    </div>
                </div>
            </div>
        <? endif ?>
    </div>
</header>

