<?php

use yii\helpers\Html;
use frontend\modules\client\modules\cabinet\assets\HeaderAsset;

/* @var $this \yii\web\View */
/* @var $content string */
HeaderAsset::register($this);

$domain      = domainName();
$companyName = user()->getCompanyName();

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode(app()->name . ': ' . $this->title) ?></title>
    <?php $this->head() ?>

    <? if (app()->params['frontend.layout.images.includeAppleTouchIcons']): ?>
        <link rel="apple-touch-icon" href="/images/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="/images/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="/images/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="/images/icon180.png" sizes="180x180">
    <? endif ?>

    <link rel="shortcut icon" href="<?= app()->params['frontend.layout.images.favicon'] ?>">
</head>
<body data-notifications="0">
<?php $this->beginBody() ?>
<?= $this->render('_header', ['companyName' => $companyName]) ?>
<div class="main_wrapper cabinet-page">
    <nav class="main_nav">
        <?
        echo yii\widgets\Menu::widget([
            // Important: you need to specify url as 'controller/action',
            // not just as 'controller' even if default action is used.
            'activateParents' => true,
            'hideEmptyItems'  => true,
            'options'         => ['class' => 'first_nav'],
            'items'           => [
                [
                    'label'    => t('client', 'Profile'),
                    'url'      => ['/client/cabinet/profile/index'],
                    'template' => '<a class="mni03" href="{url}">{label}</a>',
                ],
                [
                    'label'    => t('client', 'Orders'),
                    'url'      => ['/client/cabinet/order/index'],
                    'template' => '<a class="mni01" href="{url}">{label}</a>',
                ],
                [
                    'label'    => t('client', 'Organizations'),
                    'url'      => ['/client/cabinet/company/index'],
                    'template' => '<a class="mni06" href="{url}">{label}</a>',
                ],
            ],
        ]);
        ?>
    </nav>
    <section class="gray_wrapper">
        <section class="content">
            <?= \frontend\widgets\Alert::widget(); ?>
            <div>
                <header class="header_notification may_close" style="display:none;">
                    <a class="close_header"><i></i></a>
                    <div id="errorBlock"></div>
                </header>
            </div>
            <section class="cont">
                <?= $content ?>
            </section>
        </section>
    </section>
</div>
<?= $this->render('_footer', ['companyName' => $companyName]) ?>

<?php $this->endBody() /*?>
        <!-- Yandex.Metrika counter --><script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter30670468 = new Ya.Metrika({ id:30670468, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch (e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="https://mc.yandex.ru/watch/30670468" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->*/ ?>
</body>
</html>
<?php $this->endPage() ?>
