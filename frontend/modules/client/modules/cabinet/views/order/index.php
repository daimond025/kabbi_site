<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $order app\modules\order\models\Order */

$this->title = Yii::t('order', 'Orders');

$bundle_order = \app\modules\order\assets\OrderAsset::register($this);
$this->registerJsFile($bundle_order->baseUrl . '/Timer.js');
$this->registerJsFile($bundle_order->baseUrl . '/functions.js');
$this->registerJsFile($bundle_order->baseUrl . '/orderDatepicker.js');
$this->registerJsFile($bundle_order->baseUrl . '/map/orderMap.js');
$this->registerJsFile($bundle_order->baseUrl . '/main.js');
$this->registerJsFile($bundle_order->baseUrl . '/bonusPayment.js');
$this->registerJsFile($bundle_order->baseUrl . '/refreshIsClient.js');
$this->registerJs('if (window.qt_handler !== undefined) { window.qt_handler.loadFinished(true); }');

$controller = 'order';

if($controller == 'order')
{
    $this->registerJs('colorbox_init();');
}
?>
<?php

    echo Html::a(t('app', 'Add'), ['create'], ['class' => 'button add_order', 'style' => 'float: right; position: relative; top: 5px;']) ?>

    <a class="open_map_win" href="#map_win" data-module="/client/cabinet/"><?=t('order', 'Map')?></a>
    <div class="filter_header">
        <h1><?=t('order', 'Orders')?></h1>
        <div class="fh_content">
            <?
            $date      = get('date');
            $timestamp = strtotime($date);
            ?>
            <a class="ordered_when a_sc">
                <?= is_null($date) || strtotime(date('d.m.Y')) == $timestamp
                    ? t('order', 'today') : app()->formatter->asDate($timestamp, 'shortDate'); ?>
            </a>
            <div class="s_datepicker_content b_sc sdpc_f">
                <div class="sdp_f">
                    <input class="date_input" type="hidden" value="<?= $date ?>">
                </div>
            </div>
        </div>
    </div>
    <div class="tabs_links tnums">
        <ul>
            <li data-group="new" class><a href="#new" data-ajax="<?=Url::to(["get-new", 'date' => $date])?>" class="t01"><?=t('order', 'New')?><sup></sup></a></li>
            <li data-group="works" class><a href="#works" data-ajax="<?=Url::to(["get-works", 'date' => $date])?>" class="t02"><?=t('order', 'On the go')?><sup></sup></a></li>
            <li data-group="warning" class><a href="#warning" data-ajax="<?=Url::to(["get-warning", 'date' => $date])?>" class="t03"><span class="link_red"><?=t('order', 'Warning')?></span><sup class="red_sup"></sup></a></li>
            <li data-group="pre_order" class><a href="#pre_order" data-ajax="<?=Url::to(["get-pre-orders", 'date' => $date])?>" class="t04"><?=t('order', 'Preliminary')?><sup></sup></a></li>
            <li data-group="completed" class><a href="#completed" data-ajax="<?=Url::to(["get-completed", 'date' => $date])?>" class="t05"><?=t('order', 'Compleated')?></a></li>
            <li data-group="rejected" class><a href="#rejected" data-ajax="<?=Url::to(["get-rejected", 'date' => $date])?>" class="t06"><span class="link_red"><?=t('order', 'Canceled')?></span></a></li>
        </ul>
    </div>
    <div class="tabs_content">
        <div  id="t01">
            <?=$this->render('@app/modules/order/views/order/new', ['orders' => $orders, 'isClient' => true])?>
        </div>
        <div id="t02"></div>
        <div id="t03"></div>
        <div id="t04"></div>
        <div id="t05"></div>
        <div id="t06"></div>
    </div>
<?=$this->render('@app/modules/order/views/general-map/map', ['isClient' => true])?>