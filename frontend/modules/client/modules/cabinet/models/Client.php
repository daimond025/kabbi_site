<?php

namespace frontend\modules\client\modules\cabinet\models;

use common\modules\tenant\models\Tenant;
use app\modules\client\models\Client as BaseClient;
use app\modules\client\models\ClientHasCompany;
use common\helpers\CacheHelper;
use common\modules\city\models\City;
use frontend\components\behavior\file\CropFileBehavior;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;
use Yii;

class Client extends BaseClient implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        Yii::$app->user->on('afterLogout', [$this, 'onAfterLogout']);
        parent::init();
    }

    public function behaviors()
    {
        return [
            'common' => [
                'class' => \common\components\behaviors\ActiveRecordBehavior::className(),
            ],
            'fileBehavior' => [
                'class' => CropFileBehavior::className(),
                'fileField' => ['photo'],
                'upload_dir' => app()->params['upload'],
                'action' => 'show-external-file',
                'tenant_id' => user()->tenant_id,
            ],
        ];
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(),[
            'city' => t('client', 'City'),
        ]);
    }


    public static function findIdentityByPhone($phone, $domain = null)
    {
        return self::findByPhone($phone, $domain);
    }

    public static function findByPhone($phone, $domain = null)
    {
        return self::find()
            ->where(['black_list' => 0])
            ->joinWith([
                'tenant' => function ($query) use ($domain) {
                    $domain = empty($domain) ? domainName() : $domain;
                    $query->andWhere("status != 'REMOVED' AND domain = '$domain'");
                },
                'clientPhones' => function ($query) use ($phone) {
                    $query->andWhere(['value' => $phone]);
                }
            ], false)
            ->one();
    }

    public static function findIdentity($id)
    {
        return self::findOne(['client_id' => $id]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public static function findIdentityByAuthKey($client_id, $auth_key)
    {
        return self::find()->where([
            'client_id' => $client_id,
            'auth_key' => $auth_key,
        ])->andWhere(time() . '<= auth_exp')->one();
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function updateUserActiveTime()
    {
        app()->db
            ->createCommand('UPDATE ' . self::tableName() . ' SET active_time=' . time() . ' WHERE client_id=' . $this->id)
            ->execute();
    }

    /**
     * AfterLogout event callback
     */
    protected function onAfterLogout()
    {
        $this->active_time = null;
        $this->save();
    }

    public function getUserCityList()
    {
        $prefix = getLanguagePrefix();
        return [$this->city_id => $this->city->{name . $prefix}];
    }

    public function isAdmin($company_id)
    {
        if ($this->clientHasCompanies) {
            foreach ($this->clientHasCompanies as $company) {
                if ($company->company_id == $company_id) {
                    return ($company->role == ClientHasCompany::ROLE_JURIDICAL);
                }
            }
        }
        return false;
    }

    public function getCompanyListIsAdmin()
    {
        $companies = [];
        if ($this->clientHasCompanies) {
            foreach ($this->clientHasCompanies as $company) {
                if($company->role == ClientHasCompany::ROLE_JURIDICAL) {
                    $companies[] = $company->company_id;
                }
            }
        }
        return $companies;
    }

    public function getCompanyListIsNotAdmin()
    {
        $companies = [];
        if ($this->clientHasCompanies) {
            foreach ($this->clientHasCompanies as $company) {
                if($company->role != ClientHasCompany::ROLE_JURIDICAL) {
                    $companies[] = $company->company_id;
                }
            }
        }
        return $companies;
    }

    public function getPhone()
    {
        return user()->clientPhones[0]->value;
    }

    /**
     * Города, где работает пользователь с республиками и временным смещением
     * @return array
     */
    public function getUserCityListWithRepublic()
    {
        $lang_prefix = getLanguagePrefix();

        return CacheHelper::getFromCache('ClientCityListWithRepublic_' . $this->tenant_id . '_' . $this->client_id . $lang_prefix,
            function () use ($lang_prefix) {
                return City::find()
                    ->where([
                        'city_id' => user()->city_id
                    ])
                    ->with([
                        'republic' => function ($query) {
                            $query->select(['republic_id', 'timezone']);
                        },
                    ])
                    ->select([City::tableName() . '.city_id', 'name' . $lang_prefix, 'republic_id'])
                    ->asArray()
                    ->all();
            });
    }

    public function getPartnerCompanyName()
    {
        return $this->getCompanyName();
    }

    public function getCompanyName()
    {
        return CacheHelper::getFromCache('company_name_' . $this->tenant_id, function () {
            return Tenant::find()
                ->select('company_name')
                ->where(['tenant_id' => $this->tenant_id])
                ->scalar();
        });
    }
}