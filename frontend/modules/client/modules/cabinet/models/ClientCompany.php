<?php

namespace frontend\modules\client\modules\cabinet\models;

use app\modules\client\models\ClientCompany as BaseClientCompany;
use frontend\components\behavior\file\CropFileBehavior;
use Yii;

class ClientCompany extends BaseClientCompany
{
    public function behaviors()
    {
        return [
            'fileBehavior' => [
                'class'      => CropFileBehavior::className(),
                'fileField'  => ['logo'],
                'upload_dir' => app()->params['upload'],
                'action' => 'show-external-file',
                'tenant_id' => user()->tenant_id,
            ],
        ];
    }
}