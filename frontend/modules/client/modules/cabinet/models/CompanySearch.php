<?php
namespace frontend\modules\client\modules\cabinet\models;

use yii\base\Model;

class CompanySearch extends ClientCompany
{
    public $input_search;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['input_search', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'input_search' => t('client', 'Name'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();
        $query->where([
            'tenant_id' => user()->tenant_id,
            'client_id' => user()->client_id
        ]);
        $query->orderBy(['name' => SORT_ASC]);
        $query->joinWith('clientHasCompanies');
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $query;
        }

        $query->andFilterWhere(['like', 'name', $this->input_search]);

        return $query;
    }

    public function getAccessCityList()
    {
        return $this->_accessCityList;
    }

    public function setAccessCityList($value)
    {
        $this->_accessCityList = $value;
    }

    private function getSearchCityList()
    {
        return $this->city_id ? $this->city_id : $this->getAccessCityList();
    }
}