<?php
namespace frontend\modules\client\modules\cabinet\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $phone;
    public $password;
    public $rememberMe = false;
    public $lang;

    private $_user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['lang','safe'],
            // email and password are both required
            [['password', 'phone'], 'trim'],
            [['password','phone'], 'required', 'message'=>t('app', '{attribute} cannot be blank', [], $this->lang)],
            [
                ['phone'],
                'filter',
                'filter' => function ($value) {
                    return preg_replace("/[^0-9]/", '', $value);
                },
            ],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }


    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || $user->password != $this->password) {
                $this->addError($attribute, t('validator', 'Incorrect phone or password', [], $this->lang));
            }
        }
    }

    /**
     * Logs in a user using the provided email and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? app()->params['rememberMe'] : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[email]]
     *
     * @return Client|null
     */
    public function getUser()
    {
        if ($this->_user === false)
            $this->_user = Client::findIdentityByPhone($this->phone);

        return $this->_user;
    }

    public function attributeLabels()
    {
        return [
            'phone' => t('app', 'Phone', [], $this->lang),
            'password' => t('app', 'Password', [], $this->lang),
            'rememberMe' => t('app', 'Remember Me', [], $this->lang),
            'domain' => t('app', 'Domain', [], $this->lang)
        ];
    }
}
