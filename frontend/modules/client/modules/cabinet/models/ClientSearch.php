<?php
namespace frontend\modules\client\modules\cabinet\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

class ClientSearch extends Client
{
    public $input_search;

    public $formName = null;

    const PAGE_SIZE = 20;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['input_search', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'input_search' => t('client', 'Name'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchInCompany($params, $company_id)
    {
        $query = parent::find();
        $query->where([
            'tenant_id' => user()->tenant_id,
            'black_list' => 0,
            'company.company_id' => $company_id,
        ]);

        $query->joinWith('clientHasCompanies company');

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'sort'       => [
                'defaultOrder' => [
                    'fullName' => SORT_ASC,
                ],
                'attributes'   => [
                    'fullName'         => [
                        'asc'  => ['last_name' => SORT_ASC, 'name' => SORT_ASC, 'second_name' => SORT_ASC],
                        'desc' => ['last_name' => SORT_DESC, 'name' => SORT_DESC, 'second_name' => SORT_DESC],
                    ],
                    'value'
                ],
            ],
            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);
        
        $this->load($params, $this->formName);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('clientPhones');
        $query->andFilterWhere([
                'or',
                ['like', 'last_name', $this->input_search],
                ['like', 'name', $this->input_search],
                ['like', 'second_name', $this->input_search],
                ['like', 'value', $this->input_search],
            ]
        );

        return $dataProvider;
    }

    public function getAccessCityList()
    {
        return $this->_accessCityList;
    }

    public function setAccessCityList($value)
    {
        $this->_accessCityList = $value;
    }

    private function getSearchCityList()
    {
        return $this->city_id ? $this->city_id : $this->getAccessCityList();
    }
}