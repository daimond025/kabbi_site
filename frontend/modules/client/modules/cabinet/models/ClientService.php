<?php
namespace frontend\modules\client\modules\cabinet\models;

use app\modules\client\models\ClientHasCompany;
use app\modules\client\models\ClientPhone;
use yii\base\Object;
use yii\db\Query;

class ClientService extends Object
{
    public static function findById($client_id)
    {
        return Client::find()
            ->alias('t1')
            ->where(['t1.client_id' => $client_id, 'black_list' => 0])
            ->joinWith(['clientPhones', 'companies', 'city'])
            ->one();
    }

    public static function getCompaniesByPhone($phone)
    {
        $query = ClientPhone::find()
            ->select('com.company_id')
            ->alias('ph')
            ->joinWith([
                'client cl',
                'client.clientHasCompanies com',
            ])
            ->where([
                'ph.value'     => $phone,
                'cl.tenant_id' => user()->tenant_id,
            ]);

        if ($phone != user()->getPhone()) {
            $query->andWhere(['com.company_id' => user()->getCompanyListIsAdmin()]);
        }

        $companies = $query->asArray()
            ->column();

        return $companies;
    }

    /**
     * Getting company client ids by admin client
     *
     * @param int $tenantId
     * @param int $adminId
     *
     * @return array
     */
    public static function getCompanyClientIds($tenantId, $adminId)
    {
        $clientIds = Client::find()
            ->select('t.client_id')
            ->alias('t')
            ->joinWith([
                'clientHasCompanies c' => function ($query) use ($adminId) {
                    /* @var $query Query */
                    $query->where([
                        'in',
                        'company_id',
                        ClientHasCompany::find()
                            ->select('company_id')
                            ->where([
                                'client_id' => $adminId,
                                'role'      => ClientHasCompany::ROLE_JURIDICAL,
                            ]),
                    ]);
                },
            ])
            ->where(['tenant_id' => $tenantId])
            ->column();

        return empty($clientIds) ? [] : $clientIds;
    }

}