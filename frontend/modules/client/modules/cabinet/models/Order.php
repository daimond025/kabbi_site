<?php
namespace frontend\modules\client\modules\cabinet\models;

use app\modules\order\models\Order as BaseOrder;

class Order extends BaseOrder
{
    public static function getOrdersFromRedisIsClient($status_group, $arFilter = [], $sort = SORT_DESC) {
        $orders = parent::getOrdersFromRedis($status_group, $arFilter, $sort);

        foreach($orders as $key => $order) {
            // Если заказ чужого клиента
            if($order->client_id != user()->client_id) {
                // ..и мы не администратор компании
                if(is_null($order->company_id) || !user()->isAdmin($order->company_id)){
                    // то удаляем элемент
                    unset($orders[$key]);
                }
            }
        }
        return $orders;
    }

    public static function getByStatusGroupIsClient(
        $status_group,
        $city_id = null,
        $date = null,
        $arSort = [],
        $filterStatus = [],
        $arOrderExclusion = []
    ) {
        $orders = parent::getByStatusGroup(
            $status_group,
            $city_id,
            $date,
            $arSort,
            $filterStatus,
            $arOrderExclusion
        );

        foreach($orders as $key => $order) {
            // Если заказ чужого клиента
            if($order->client_id != user()->client_id) {
                // ..и мы не администратор компании
                if(is_null($order->company_id) || !user()->isAdmin($order->company_id)){
                    // то удаляем элемент
                    unset($orders[$key]);
                }
            }
        }
        return $orders;
    }

    public static function getOrderCount($group_name, $city_id)
    {
        $arResult = [
            'new'       => 0,
            'works'     => 0,
            'warning'   => 0,
            'pre_order' => 0,
        ];

        return $arResult;
    }

    public function getUserCreatedId()
    {
        return null;
    }

    public function getDevice()
    {
        return self::DEVICE_CABINET;
    }
}