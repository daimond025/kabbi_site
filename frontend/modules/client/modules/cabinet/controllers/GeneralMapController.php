<?php
namespace frontend\modules\client\modules\cabinet\controllers;

use app\modules\order\models\OrderStatus;
use app\modules\order\services\CallsignService;
use frontend\modules\car\models\CarColor;
use frontend\modules\client\modules\cabinet\models\Order;
use frontend\modules\employee\components\worker\WorkerShiftService;
use yii\filters\AccessControl;
use yii\web\Controller;
use Yii;
use yii\web\Response;

class GeneralMapController extends Controller
{

    public $layout = 'main';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    private function getActiveOrders()
    {
        return Yii::$app->redis_orders_active->executeCommand('hvals', [user()->tenant_id]);
    }

    /**
     * Free orders
     */
    public function actionGetOrdersFree()
    {
        $city_list = [user()->city_id];
        $orders = $this->getActiveOrders();
        $city_id = post('city_id');
        $arResult['markers'] = [];
        $arResult['markers_keys'] = [];
        $arResult['type'] = 'order';
        $filterStatuses = array_merge(OrderStatus::getNewStatusId(), OrderStatus::getPreOrderStatusId());

        foreach ($orders as $order) {
            $order = unserialize($order);
            $order['address'] = unserialize($order['address']);

            //Фильтруем данные
            if (!in_array($order['city_id'], $city_list)
                || empty($order['address']['A']['lat'])
                || empty($order['address']['A']['lon'])
                || (!empty($city_id) && $city_id != $order['city_id'])
                || !(user()->client_id == $order['client_id'] || user()->isAdmin($order['company_id']))
                || !in_array($order['status_id'], $filterStatuses)
                || $order['status']['status_group'] == OrderStatus::STATUS_GROUP_6

            ) {
                continue;
            }

            $marker_key_name = 'order_' . $order['order_id'];
            $arResult['markers'][] = [
                'key_name'    => $marker_key_name,
                'coords'      => [
                    'lat' => $order['address']['A']['lat'],
                    'lon' => $order['address']['A']['lon'],
                ],
                'marker_info' => $this->renderPartial('@app/modules/order/views/general-map/order_info', [
                    'client_id'   => $order['client_id'],
                    'client_name' => trim($order['client']['last_name'] . ' ' . $order['client']['name']),
                    'phone'       => $order['phone'],
                    'device'      => $order['device'],
                    'isClient'    => true
                ]),
            ];
            $arResult['markers_keys'][$marker_key_name] = 1;
        }

        $this->createResponseData($arResult);
    }

    /**
     * Free orders
     */
    public function actionGetOrdersPre()
    {
        $city_list = [user()->city_id];
        $orders = $this->getActiveOrders();
        $city_id = post('city_id');
        $arResult['markers'] = [];
        $arResult['markers_keys'] = [];
        $arResult['type'] = 'order';
        $filterStatuses = array_merge(OrderStatus::getNewStatusId(), OrderStatus::getPreOrderStatusId());

        foreach ($orders as $order) {
            $order = unserialize($order);
            $order['address'] = unserialize($order['address']);

            //Фильтруем данные
            if (!in_array($order['city_id'], $city_list)
                || empty($order['address']['A']['lat'])
                || empty($order['address']['A']['lon'])
                || (!empty($city_id) && $city_id != $order['city_id'])
                || !(user()->client_id == $order['client_id'] || user()->isAdmin($order['company_id']))
                || !in_array($order['status_id'], $filterStatuses)
                || $order['status']['status_group'] != OrderStatus::STATUS_GROUP_6

            ) {
                continue;
            }

            $marker_key_name = 'order_' . $order['order_id'];
            $arResult['markers'][] = [
                'key_name'    => $marker_key_name,
                'coords'      => [
                    'lat' => $order['address']['A']['lat'],
                    'lon' => $order['address']['A']['lon'],
                ],
                'marker_info' => $this->renderPartial('@app/modules/order/views/general-map/order_info', [
                    'client_id'   => $order['client_id'],
                    'client_name' => trim($order['client']['last_name'] . ' ' . $order['client']['name']),
                    'phone'       => $order['phone'],
                    'device'      => $order['device'],
                    'isClient'    => true
                ]),
            ];
            $arResult['markers_keys'][$marker_key_name] = 1;
        }

        $this->createResponseData($arResult);
    }

    /**
     * Free workers
     */
    public function actionGetFreeCars()
    {
        $this->createResponseData([]);
        return;
// Не показывать клиенту свободные машинки
//        $city_list = [user()->city_id];
//        $workers = $this->getOnlineWorkers();
//        $city_id = post('city_id');
//        $arResult['markers'] = [];
//        $arResult['markers_keys'] = [];
//        $arResult['type'] = 'free_car';
//
//        foreach ($workers as $cab) {
//            $cab = unserialize($cab);
//
//            if (!in_array($cab['worker']['city_id'], $city_list)
//                || $cab['worker']['status'] != 'FREE'
//                || (!empty($city_id) && $cab['worker']['city_id'] != $city_id || empty($cab['geo']['lat']) || empty($cab['geo']['lon']))
//            ) {
//                continue;
//            }
//
//            $marker_key_name = 'free_car_' . $cab['worker']['worker_id'];
//            $arResult['markers'][] = [
//                'key_name'    => $marker_key_name,
//                'coords'      => [
//                    'lat' => $cab['geo']['lat'],
//                    'lon' => $cab['geo']['lon'],
//                ],
//                'marker_info' => $this->renderPartial('@app/modules/order/views/general-map/free_car', [
//                    'worker_id'   => $cab['worker']['worker_id'],
//                    'worker_name' => trim($cab['worker']['last_name'] . ' ' . $cab['worker']['name']),
//                    'phone'       => $cab['worker']['phone'],
//                    'callsign'    => $cab['worker']['callsign'],
//                    'city_id'     => $cab['worker']['city_id'],
//                    'car_id'      => $cab['car']['car_id'],
//                    'car_name'    => trim($cab['car']['name'] . ', ' . $cab['car']['gos_number'] . ', ' . CarColor::getColorText($cab['car']['color'])),
//                    'isClient'    => true,
//                ]),
//                'iconAngle'   => $cab['geo']['degree'],
//            ];
//            $arResult['markers_keys'][$marker_key_name] = 1;
//        }
//
//        $this->createResponseData($arResult);
    }

    private function getOnlineWorkers()
    {
        /** @var WorkerShiftService $workerShiftService */
        $workerShiftService = Yii::$app->getModule('employee')->get('workerShift');

        return $workerShiftService->getOnlineWorkers();
    }

    /**
     * Drivers are working
     */
    public function actionGetBusyCars()
    {
        $city_list = [user()->city_id];
        $workers = $this->getOnlineWorkers();
        $city_id = post('city_id');
        $arResult['markers'] = [];
        $arResult['markers_keys'] = [];
        $arResult['type'] = 'busy_car';

        //для OFFER_ORDER нужно реализовать поиск водителя которому осуществляется предложение клиентского заказа
        $arBuzyDrieverStatus = [
            'ON_ORDER',
            'ON_BREAK',
            'OFFER_ORDER'
        ];

        foreach ($workers as $cab) {
            $cab = unserialize($cab);
            $color = !empty($cab['car']['color']) ? t('car', $cab['car']['color']) : '';
            if (!in_array($cab['worker']['city_id'], $city_list)
                || !in_array($cab['worker']['status'], $arBuzyDrieverStatus)
                || ((!empty($city_id) && $cab['worker']['city_id'] != $city_id) || empty($cab['geo']['lat']) || empty($cab['geo']['lon']))
            ) {
                continue;
            }
            $order = Order::getOrderFromRedis(user()->tenant_id, $cab['worker']['last_order_id']);

            
            if(user()->client_id != $order['client_id'] && !user()->isAdmin($order['company_id'])) {
                continue;
            }


            $callsign = CallsignService::getCallsign($cab);


            $marker_key_name = 'busy_car_' . $cab['worker']['worker_id'];

            $isPause = $cab['worker']['status'] == 'ON_BREAK';
            $arResult['markers'][] = [
                'key_name'    => $marker_key_name,
                'coords'      => [
                    'lat' => $cab['geo']['lat'],
                    'lon' => $cab['geo']['lon'],
                ],
                'marker_info' => $this->renderPartial('@app/modules/order/views/general-map/busy_car', [
                    'client_id'    => $isPause ? null : $order['client_id'],
                    'client_name'  => $isPause ? null : trim($order['client']['last_name'] . ' ' . $order['client']['name']),
                    'client_phone' => $isPause ? null : $order['phone'],
                    'device'       => $order['device'],
                    'worker_id'    => $cab['worker']['worker_id'],
                    'worker_name'  => trim($cab['worker']['last_name'] . ' ' . $cab['worker']['name']),
                    'worker_phone' => $cab['worker']['phone'],
                    'callsign'     => $cab['worker']['callsign'],
                    'city_id'      => $cab['worker']['city_id'],
                    'car_id'       => $cab['car']['car_id'],
                    'car_name'     => trim($cab['car']['name'] . ', ' . $color),
                    'car_gos_number' => $cab['car']['gos_number'],
                    'isClient'     => true
                ]),
                'iconAngle'   => $cab['geo']['degree'],
                'worker_id'   => $callsign ? $callsign : '',
                'position_id' => $cab['position']['position_id'],
            ];
            $arResult['markers_keys'][$marker_key_name] = $cab['position']['position_id'];
        }

        $this->createResponseData($arResult);
    }


    /**
     * Generates response data.
     * @param array $data
     */
    private function createResponseData($data)
    {
        if (empty($data['markers'])) {
            $data['message'] = t('order', 'No data for display');
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->data = $data;
    }

    public function actionGetPauseCars()
    {
        $this->createResponseData([]);
        return;
    }

    public function actionGetOfferCars()
    {
        $this->createResponseData([]);
        return;
    }
}