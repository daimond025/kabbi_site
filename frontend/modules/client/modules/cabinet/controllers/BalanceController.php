<?php

namespace frontend\modules\client\modules\cabinet\controllers;

use app\modules\balance\models\Account;
use app\modules\balance\models\Operation;
use app\modules\balance\models\Transaction;
use common\modules\city\models\City;
use common\modules\tenant\models\DefaultSettings;
use common\modules\tenant\models\TenantSetting;
use frontend\modules\tenant\models\Currency;
use yii\filters\AccessControl;
use yii\web\Controller;
use Yii;

class BalanceController extends Controller
{

    public $layout = 'main';

    public $defaultAction = 'show-balance';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function actionTransactionSearch($account_id = null, $city_id = null)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $result = $this->transactionSearch($account_id, $city_id, post('filter'), post('first_date'),
            post('second_date'));

        return $this->renderAjax('@app/modules/balance/views/balance/' . post('view'), [
            'currency_id' => $result['currency_id'],
            'operations'  => $result['operations'],
            'time_offset' => $result['time_offset'],
            'isClient'    => true,
        ]);
    }

    /**
     * Display entity balance.
     * @return mixed
     */
    public function actionShowBalance(
        $id,
        $kind_id,
        $city_id = null,
        $currency_id = null
    ) {
        $tenant_id      = user()->tenant_id;
        $cityCurrencyId = TenantSetting::getSettingValue(
            user()->tenant_id, DefaultSettings::SETTING_CURRENCY, $city_id);

        if (empty($currency_id)) {
            $currency_id = $cityCurrencyId;
        }

        $currencyIds   = Account::find()
            ->select('currency_id')
            ->andWhere(['owner_id' => $id])
            ->andWhere(['tenant_id' => $tenant_id])
            ->andWhere(['acc_kind_id' => $kind_id])
            ->asArray()
            ->all();
        $currencyIds[] = $cityCurrencyId;

        $currencies = Currency::find()
            ->where(['currency_id' => $currencyIds])->all();

        $account = Account::findOne([
            'owner_id'    => $id,
            'tenant_id'   => $tenant_id,
            'acc_kind_id' => $kind_id,
            'currency_id' => $currency_id,
        ]);

        if (empty($account)) {
            $account = new Account();
            $account->loadDefaultValues();
            $account->owner_id    = $id;
            $account->tenant_id   = $tenant_id;
            $account->acc_kind_id = $kind_id;
            $account->acc_type_id = Account::PASSIVE_TYPE;
            $account->currency_id = $currency_id;
        }

        $isAllowed = Transaction::isAllowed($kind_id);

        if ($account->load(post())) {
            $account->sum = abs($account->sum);

            if (!$account->validate()) {
                session()->setFlash('error', implode("\n", $account->getFirstErrors()));

                return false;
            }

            if ($account->operation_type == Transaction::DEPOSIT_TYPE) {
                $payment_method = Transaction::CASH_PAYMENT;
            } elseif ($account->operation_type == Transaction::WITHDRAWAL_TYPE) {
                $payment_method = Transaction::WITHDRAWAL_PAYMENT;
            } elseif ($account->operation_type == Transaction::WITHDRAWAL_CASH_OUT_TYPE) {
                $account->operation_type = Transaction::WITHDRAWAL_TYPE;
                $payment_method          = Transaction::WITHDRAWAL_CASH_OUT_PAYMENT;
            }

            if ($account->acc_kind_id === Account::CLIENT_BONUS_KIND) {
                $payment_method = Transaction::BONUS_PAYMENT;
            }

            $transactionData = [
                'type_id'            => $account->operation_type,
                'tenant_id'          => $tenant_id,
                'sender_owner_id'    => $id,
                'sender_acc_kind_id' => $kind_id,
                'currency_id'        => $currency_id,
                'sum'                => $account->sum,
                'comment'            => $account->transaction_comment,
                'user_created'       => user()->id,
                'payment_method'     => $payment_method,
            ];

            if ($isAllowed) {
                $response_code = Transaction::createTransaction($transactionData);
            } else {
                session()->setFlash('error', t('balance', 'Forbbiden for current user'));

                return false;
            }

            if ($response_code == 100) {
                $account = Account::findOne([
                    'owner_id'    => $id,
                    'tenant_id'   => $tenant_id,
                    'acc_kind_id' => $kind_id,
                    'currency_id' => $currency_id,
                ]);
            } else {
                session()->setFlash('error',
                    t('error', 'Service is temporarily unavailable. Notify to administrator, please.'));
                Yii::warning('Код ответа операции биллинга - ' . $response_code . '. sender_acc_id - ' . $account->account_id,
                    'billing');

                return false;
            }
        }

        $time_offset = City::getTimeOffset($city_id);
        $operations  = Operation::getByWeekForAccount($account->account_id);
        $isClient    = true;

        return $this->renderAjax('@app/modules/balance/views/balance/transaction',
            compact('id', 'kind_id', 'city_id', 'currency_id', 'currencies',
                'account', 'operations', 'time_offset', 'isAllowed', 'isClient'));
    }

    public function transactionSearch(
        $account_id = null,
        $city_id = null,
        $filter = 'limit',
        $first_date = null,
        $second_date = null
    ) {
        $time_offset = City::getTimeOffset($city_id);

        $account = Account::findOne($account_id);
        if (empty($account)) {
            return false;
        }

        $currency_id = $account->currency_id;
        $operation   = new Operation();
        switch ($filter) {
            case 'limit':
                $operations = $operation->getByWeekForAccount($account_id);
                break;
            case 'period':
                $operations = $operation->getByPeriodForAccount($account_id, $first_date, $second_date);
                break;
            default:
                $operations = null;
        }

        return [
            'time_offset' => $time_offset,
            'currency_id' => $currency_id,
            'operations'  => $operations,
        ];
    }
}