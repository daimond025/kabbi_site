<?php
namespace frontend\modules\client\modules\cabinet\controllers;

use frontend\modules\client\modules\cabinet\models\ClientCompany;
use frontend\modules\client\modules\cabinet\models\ClientSearch;
use frontend\modules\client\modules\cabinet\models\CompanySearch;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;

class CompanyController extends Controller
{

    public $layout = 'main';


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

 
    public function actionIndex()
    {
        return $this->render('index', $this->getListData(Yii::$app->request->queryParams));
    }

    public function orderSearch($id, $filter, $first_date = null, $second_date = null, $client_id)
    {
        if ($filter == 'period') {
            return ClientCompany::getClientOrdersByPeriod($id, $first_date, $second_date, $client_id);
        }

        return ClientCompany::getClientOrdersByLimit($id, $client_id);
    }

    /**
     * Ajax client order seacrh from filter.
     * @param integer $id Company id
     * @return html
     */
    public function actionOrderSearch($id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $company = $this->orderSearch($id, post('filter'), post('first_date'), post('second_date'), user()->isAdmin($id) ? null : user()->client_id);

        return $this->renderAjax('_orders', ['company' => $company]);
    }

    /**
     * Orders tab.
     * @param integer $id Company id
     * @return html
     */
    public function actionOrders($id)
    {
        return $this->renderAjax('orders', ['company' => ClientCompany::getClientOrdersByLimit($id, 10, user()->isAdmin($id) ? null : user()->client_id)]);
    }

    /**
     * Company stuff list.
     * @param integer $id Company id
     * @return mixed
     */
    public function actionStuff($id)
    {
        return $this->renderAjax('stuff', $this->getClientsListData(Yii::$app->request->queryParams, $id));
    }

    /**
     * Updates an existing ClientCompany model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
//        dd(app()->getComponents());
        $company = ClientCompany::find()
            ->alias('t1')
            ->where([
                't1.company_id' => $id,
                'tenant_id'  => user()->tenant_id,
                't2.client_id' => user()->client_id
            ])
            ->joinWith('clientHasCompanies t2')
            ->with([
                'city',
            ])
            ->one();

        if (!$company) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        
        $post = Yii::$app->request->post();
        if ($company->load($post)) {
            $company->contract_start_date = $company->getDateFieldFormat($post['ClientCompany']['contract_start_day'],
                $post['ClientCompany']['contract_start_month'], $post['ClientCompany']['contract_start_year']);
            $company->contract_end_date = $company->getDateFieldFormat($post['ClientCompany']['contract_end_day'],
                $post['ClientCompany']['contract_end_month'], $post['ClientCompany']['contract_end_year']);
            if ($company->save()) {
                try {
                    session()->setFlash('success', t('app', 'Data updated successfully'));

                    return $this->refresh();
                } catch (\yii\db\Exception $ex) {
                    session()->setFlash('error',
                        t('client', 'Error saving organization. Notify to administrator, please.'));
                }
            } else {
                displayErrors([$company]);
            }
        }

        list($company->contract_start_year, $company->contract_start_month, $company->contract_start_day) = !empty($company->contract_start_date) ? explode('-',
            $company->contract_start_date) : null;
        list($company->contract_end_year, $company->contract_end_month, $company->contract_end_day) = !empty($company->contract_end_date) ? explode('-',
            $company->contract_end_date) : null;

        return $this->render('view', [
            'company'        => $company,
            'user_city_list' => user()->getUserCityList(),
        ]);
    }

    private function getClientsListData($searchParams, $company_id)
    {

        $searchModel = new ClientSearch();
        $dataProvider = $searchModel->searchInCompany($searchParams, $company_id);
        $pjaxId = 'clientsInCompany';
        return compact('searchModel', 'dataProvider', 'pjaxId');
    }
    
    private function getListData($searchParams)
    {

        $searchModel                 = new CompanySearch();
        $dataProvider                = new ArrayDataProvider([
            'allModels'  => $searchModel->search($searchParams)->all(),
            'sort'       => [
                'attributes' => ['name'],
            ],
            'pagination' => false,
        ]);

        $pjaxId = 'company';

        return compact('searchModel', 'dataProvider', 'pjaxId');
    }
}