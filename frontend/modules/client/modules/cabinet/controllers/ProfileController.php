<?php
namespace frontend\modules\client\modules\cabinet\controllers;

use app\modules\client\models\Client;
use common\helpers\DateTimeHelper;
use common\models\LoginMembersOfTenantStrategy;
use common\modules\tenant\models\Tenant;
use frontend\modules\client\modules\cabinet\models\ClientService;
use frontend\modules\client\modules\cabinet\models\LoginForm;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use Yii;
use yii\web\Response;

class ProfileController extends Controller
{

    public $layout = 'main';


    public function behaviors()
    {
        return [
            'access' => [
                'class'  => AccessControl::className(),
                'except' => [
                    'login',
                    'logout',
                    'error',
                ],
                'rules'  => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionError()
    {
        $this->layout = 'main';

        if (($exception = Yii::$app->getErrorHandler()->exception) === null) {
            // action has been invoked not from error handler, but by direct route, so we display '404 Not Found'
            $exception = new \yii\web\HttpException(404, Yii::t('yii', 'Page not found.'));
        }

        if ($exception instanceof \yii\db\Exception && strpos($exception->getFile(), 'redis')) {
            session()->setFlash('error', t('error', 'Temporarily does not work order creation. Soon we will fix it.'));

            return $this->goHome();
        }

        if ($exception instanceof \yii\web\HttpException) {
            $code = $exception->statusCode;
        } else {
            $code = $exception->getCode();
        }

        if ($exception instanceof \yii\base\Exception) {
            $name = $exception->getName();
        } else {
            $name = $this->defaultName ?: Yii::t('yii', 'Error');
        }

        if ($code) {
            $name .= " (#$code)";
        }

        if ($exception instanceof \yii\base\UserException) {
            $message = $exception->getMessage();
        } else {
            $message = Yii::t('yii', 'An internal server error occurred.');
        }

        if (Yii::$app->getRequest()->getIsAjax()) {
            return "$name: $message";
        } else {
            if (app()->user->isGuest) {
                $this->layout = false;
                $view         = '@app/views/site/guestError';
            } else {
                $view = '@app/views/site/error';
            }

            return $this->render($view, [
                'name'      => $name,
                'message'   => $message,
                'exception' => $exception,
            ]);
        }

    }

    public function actionTest()
    {
        return 'test';
    }

    public function actionLogin()
    {
        $this->layout = '//login';

        $model = new LoginForm();

        $model->lang = (new LoginMembersOfTenantStrategy(get('lang')))->getLanguage();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('index');
        }
        $domain = domainName();
        $tenant = $this->getTenantByDomain($domain);

        return $this->render('login', ['model' => $model, 'tenant' => $tenant]);
    }

    public function actionLogout()
    {
        app()->user->logout();

        return $this->redirect('login');
    }

    public function actionIndex()
    {
        $model = ClientService::findById(user()->client_id);

        if (!$model) {
            throw new ForbiddenHttpException;
        }
        $formData = $this->getFormData();

        if (Yii::$app->request->isPost) {
            $post              = post('Client');
            $model->attributes = [
                'cropParams'  => $post['cropParams'],
                'last_name'   => $post['last_name'],
                'name'        => $post['name'],
                'second_name' => $post['second_name'],
                'email'       => $post['email'],
                'birth_day'   => $post['birth_day'],
                'birth_month' => $post['birth_month'],
                'birth_year'  => $post['birth_year'],
                'phone'       => [(string)$model->clientPhones[0]->value],
            ];
            if ($model->save()) {
                session()->setFlash('success', t('app', 'Data updated successfully'));
            }
        }

        list($model->birth_year, $model->birth_month, $model->birth_day) = !empty($model->birth) ? explode('-',
            $model->birth) : null;

        return $this->render('index', ['model' => $model, 'formData' => $formData, 'languages' => $languages]);
    }

    public function actionTransactionSearch($account_id = null, $city_id = null)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $result = $this->transactionSearch($account_id, $city_id, post('filter'), post('first_date'),
            post('second_date'));

        return $this->renderAjax('@app/modules/balance/views/balance/' . post('view'), [
            'currency_id' => $result['currency_id'],
            'operations'  => $result['operations'],
            'time_offset' => $result['time_offset'],
            'isClient'    => true,
        ]);
    }

    public function actionOrders($id)
    {
        return $this->renderAjax('orders', ['orders' => Client::getClientOrdersByLimit($id)]);
    }

    public function actionOrderSearch($id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        $orders = null;

        if (post('filter') === 'period') {
            $orders = Client::getClientOrdersByPeriod($id, post('first_date'), post('second_date'));
        } else {
            $orders = Client::getClientOrdersByLimit($id);
        }

        return $this->renderAjax('_orders', ['orders' => $orders]);
    }

    public function actionReviews($id)
    {
        $client = Client::find()
            ->where([
                'client_id' => $id,
                'tenant_id' => user()->tenant_id,
            ])
            ->with([
                'reviews' => function ($query) {
                    $query->with('order')->orderBy(['create_time' => SORT_DESC]);
                },
                'reviewRaiting',
            ])
            ->select(['client_id', 'create_time'])
            ->one();

        return $this->renderAjax('review', ['client' => $client]);
    }

    private function getTenantByDomain($domain)
    {
        $tenant = Tenant::find()->where(['domain' => $domain])->select(['tenant_id', 'logo', 'company_name'])->one();

        if (empty($tenant)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $tenant;
    }

    /**
     * @param int $entity_id Worker id
     *
     * @return array|bool
     * @throws NotFoundHttpException
     */
    public function actionUploadLogo($client_id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $client = ClientService::findById($client_id);
        $client->load(post());

        if ($client->validate(true, ['photo'])) {
            return [
                'src' => [
                    'big'   => $client->getPicturePath($client->photo),
                    'thumb' => $client->getPicturePath($client->photo, false),
                ],
            ];
        }

        return $client->getFirstErrors();
    }

    /**
     * Getting data for model form.
     * @return array
     */
    protected function getFormData()
    {
        return [
            'birth_day'   => DateTimeHelper::getMonthDayNumbers(),
            'birth_month' => DateTimeHelper::getMonthList(),
            'birth_year'  => DateTimeHelper::getOldYearList(20),
        ];
    }
}