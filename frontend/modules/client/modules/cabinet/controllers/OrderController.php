<?php

namespace frontend\modules\client\modules\cabinet\controllers;

use app\modules\address\models\ClientAddressHistory;
use app\modules\client\models\ClientHasCompany;
use app\modules\client\models\ClientReview;
use app\modules\client\models\ClientSearch;
use app\modules\parking\models\Parking;
use app\modules\tariff\models\TaxiTariff;
use app\modules\tariff\models\TaxiTariffHasCompany;
use bonusSystem\BonusSystem;
use common\modules\order\models\OrderTrackService;
use common\modules\tenant\models\DefaultSettings;
use common\modules\tenant\models\TenantSetting;
use common\services\OrderStatusService;
use frontend\modules\car\models\CarOption;
use frontend\modules\client\modules\cabinet\models\Client;
use frontend\modules\client\modules\cabinet\models\ClientCompany;
use frontend\modules\client\modules\cabinet\models\ClientService;
use frontend\modules\client\modules\cabinet\models\Order;
use app\modules\order\models\OrderChangeData;
use app\modules\order\models\OrderDetailCost;
use app\modules\order\models\OrderStatus;
use common\modules\city\models\City;
use frontend\modules\order\components\ActionManager;
use frontend\modules\order\components\OrderRequestResult;
use frontend\modules\order\components\OrderService;
use frontend\modules\order\components\services\CalculationService;
use frontend\modules\order\delegations\OrderSetting;
use frontend\modules\order\exceptions\ForbiddenChangeOrderTimeException;
use frontend\modules\order\exceptions\InvalidOrderTimeException;
use frontend\modules\promocode\components\services\PromoCodesService;
use frontend\modules\promocode\models\search\PromoBonusOperation;
use frontend\modules\tenant\models\Currency;
use GuzzleHttp\Exception\ClientException;
use operatorApi\ApiInterface;
use orderApi\OrderApi;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class OrderController
 * @package frontend\modules\client\modules\cabinet\controllers
 */
class OrderController extends Controller
{
    const ACTION_AUTO_COMPLETE = 'autocomplete';
    const ACTION_HISTORY       = 'history';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->layout = 'main';
        parent::init();
    }

    private function isShowPhone($city_id)
    {

        return TenantSetting::getSettingValue(user()->tenant_id,
            DefaultSettings::SETTING_VISIBLE_WORKER_PHONE, $city_id);
    }

    /**
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex()
    {
        $city_id = user()->city_id;

        $date = get('date');
        $filterDate = strtotime($date) ? date('d.m.Y', strtotime($date)) : null;

        return $this->render('index', [
            'orders'         => Order::getOrdersFromRedisIsClient(
                OrderStatus::STATUS_GROUP_0, ['city_id' => $city_id, 'date' => $filterDate]),
            'user_city_list' => user()->getUserCityListWithRepublic(),
        ]);
    }


    /**
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {

        /** @var OrderApi $apiOrder */
        $apiOrder = Yii::$app->orderApi;

        $order = new Order();
        $order->city_id = user()->city_id;

        if ($order->load(Yii::$app->request->post())) {
            app()->response->format = Response::FORMAT_JSON;

            $companies = ClientService::getCompaniesByPhone($order->phone);

            if ($this->isCorpBalance($order->payment)) {
                $order->payment = Order::PAYMENT_CORP;
            }

            try {
                if ($order->phone != user()->getPhone()
                    && (!$order->isCorpBalance() || !in_array($order->company_id, $companies, false))
                ) {
                    return OrderRequestResult::getResult(
                        OrderRequestResult::ACTION_ERROR,
                        t('order', 'Impossible to create order')
                    );
                }

                $apiOrder->createOrder($order);

                return OrderRequestResult::getResult(OrderRequestResult::ACTION_CLOSE);

            } catch (ClientException $ex) {
                $errors = $this->parsingErrors(json_decode($ex->getResponse()->getBody(), true));

                return OrderRequestResult::getResult(OrderRequestResult::ACTION_ERROR, $errors);
            } catch (\Exception $exc) {
                \Yii::error('Ошибка сохранения заказа', 'order');
                \Yii::error(implode('; ', $order->getFirstErrors()), 'order');
                \Yii::error($exc->getMessage(), 'order');

                return OrderRequestResult::getResult(
                    OrderRequestResult::ACTION_ERROR,
                    t('order', 'Error saving order. Notify to administrator, please.')
                );
            }
        }

        $order->status_id = OrderStatus::STATUS_NEW;
        $order->orderAction = ActionManager::ACTION_NEW_ORDER;
        $dataForm = $order->getFormData(user()->city_id, $order->status_id);

        $order->tenant_id = user()->tenant_id;
        $order->client_id = user()->client_id;
        $order->phone = user()->getPhone();

        if (empty($order->order_date)) {
            $order->city_id = user()->city_id ?: key($dataForm['CITY_LIST']);

            $time = time() + $order->getOrderOffset() + $order->getPickUp();
            $order->order_now = 1;
            $order->order_date = date('Y-m-d', $time);
            $order->order_hours = date('H', $time);
            $order->order_minutes = date('i', $time);
        }

        $parkingList = $order->getAddressParklingList();
        $positions = app()->orderService->getPositions($order->city_id);
        $order->position_id = key($positions);
        $order->show_phone = TenantSetting::getSettingValue(user()->tenant_id,
            DefaultSettings::SETTING_SHOW_WORKER_PHONE, $order->city_id, $order->position_id);

        $clientInfo = app()->orderService->getClientInformationById(
            $order->tenant_id, $order->city_id, $order->client_id);
        $payment = is_array($clientInfo['payments']) ? reset($clientInfo['payments']) : null;
        $order->payment = isset($payment['type']) ? $payment['type'] : null;
        $order->company_id = isset($payment['companyId']) ? $payment['companyId'] : null;
        $tariffs = app()->orderService->getTariffs($order->city_id, $order->position_id, $order->client_id,
            $order->payment, $order->company_id, true, ['cabinet']);

        $order->tariff_id = isset(current($tariffs)['tariff_id']) ? current($tariffs)['tariff_id'] : '';
        $isClient = true;

        $orderActions = array_map(function ($value) {
            return [$value => t('order', $value)];
        }, [ActionManager::ACTION_NEW_ORDER]);


        $canBonusPayment = $this->canBonusPayment($order->tenant_id, $order->client_id, $order->city_id,
            $order->tariff_id);

        return $this->renderAjax('@app/modules/order/views/order/add',
            compact('order', 'dataForm', 'parkingList', 'positions', 'tariffs', 'isClient', 'orderActions',
                'clientInfo', 'canBonusPayment'));
    }

    public function actionGetSettingsOrder($city_id, $position_id, $order_id = null)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $order = Order::findOne($order_id);
        if ($order && $order->city_id == $city_id && $order->position_id == $position_id) {
            return json_encode($order->getSettingsCurrentOrder());
        }

        return json_encode(OrderSetting::getCommonSettingsOrder($city_id, $position_id));
    }

    public function actionGetSettingsExistOrder($order_id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $order = Order::findOne($order_id);
        if (!$order) {
            throw new NotFoundHttpException();
        }

        return json_encode($order->getSettingsCurrentOrder());
    }

    public function isCorpBalance($payment)
    {
        return strpos($payment, Order::PAYMENT_CORP) !== false;
    }

    public function parsingErrors($errors)
    {
        $res = '';
        foreach ($errors as $error) {
            $res .= t('order', $error['message']) . ', ';
        }

        return mb_substr($res, 0, -2);
    }

    /**
     * Вывести список доп опций
     * @return json
     */
    public function actionAddOptions()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        app()->response->format = Response::FORMAT_JSON;

        $post = post('Order');
        $order = Order::findOne([
            'order_number' => $post['order_number'],
            'tenant_id'    => user()->tenant_id,
        ]);
        if (!$order) {
            $order = new Order();
        }
        $order->load(post());

        $tariff_id = $order->tariff_id;

        // Если заказ на определенное время
        if ($order->order_now == 0) {
            $time = strtotime($order->order_date . ' ' . $order->order_hours . ':' . $order->order_minutes);
        } else { // если заказ на сейчас
            $tariff = TaxiTariff::find()
                ->where('tariff_id=:tariff_id', ['tariff_id' => $tariff_id])
                ->one();
            $offset = City::getTimeOffset($tariff->cities[0]->city_id);
            $time = time() + $offset;
        }
        $routeAnalyzer = app()->routeAnalyzer;
        $addoptions = $routeAnalyzer->addOptions($tariff_id, date('d.m.Y H:i:s', $time));

        $car_options = CarOption::find()
            ->where(['option_id' => $addoptions])
            ->asArray()
            ->all();
        $result = [];
        foreach ($car_options as $item) {
            $option_id = $item['option_id'];
            $result[$option_id] = t('car-options', $item['name']);
        }

        return $result;
    }

    /**
     * Is there a client bonus
     *
     * @param string $phone
     * @param int $cityId
     * @param integer $tariffId
     *
     * @return bool
     */
    public function actionCanBonusPayment($phone, $cityId, $tariffId)
    {
        if (!app()->request->isAjax) {
            return false;
        }

        if (user()->clientPhones[0]->value != $phone) {
            return false;
        }

        app()->response->format = Response::FORMAT_JSON;

        $client = ClientSearch::searchByPhone($phone);
        $tenantId = user()->tenant_id;

        return empty($client) ? false
            : $this->canBonusPayment($tenantId, $client['client_id'], $cityId, $tariffId);
    }

    /**
     * Can update order?
     *
     * @param int $tenantId
     * @param int $clientId
     * @param int $cityId
     * @param int $tariffId
     *
     * @return bool
     */
    private function canBonusPayment($tenantId, $clientId, $cityId, $tariffId)
    {
        try {
            $currencyId = TenantSetting::getSettingValue($tenantId, DefaultSettings::SETTING_CURRENCY, $cityId);

            $bonusSystem = $bonusSystem = \Yii::createObject(BonusSystem::class, [$tenantId]);
            if ($bonusSystem->isUDSGameBonusSystem()) {
                return false;
            }

            $paymentStrategy = $bonusSystem->getPaymentStrategy($tariffId);
            $balance = $bonusSystem->getBalance($clientId, $currencyId);

            return $paymentStrategy !== null && $balance > 0;
        } catch (\yii\base\Exception $ex) {
            return false;
        }
    }

    /**
     * Getting client full name by phone.
     *
     * @param string $phone
     *
     * @return json|bool
     */
    public function actionGetClient($phone, $city_id, $showCardPayment = false)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $companyList = ClientService::getCompaniesByPhone($phone);

        if (empty($companyList)) {
            return false;
        }

        $json = [];
        $client = ClientSearch::searchByPhone($phone);

        if (!empty($client)) {

            $json['client_id'] = $client->client_id;
            $json['black_list'] = $client->black_list;
            $json['client'] = Html::encode($client->getShortName());

            $currencyId = TenantSetting::getSettingValue(
                user()->tenant_id, DefaultSettings::SETTING_CURRENCY, $city_id);
            $account = $client->getAccountByCurrencyId($currencyId);
            $bonusAccount = $client->getBonusAccountByCurrencyId($currencyId);

            if ($phone == user()->getPhone()) {
                $json['client_more_info'] = $this->renderPartial('@app/modules/order/views/order/_clientMoreInfo', [
                    'successOrders' => +$client->success_order,
                    'failedOrders'  => $client->fail_worker_order + $client->fail_client_order,
                    'clientBalance' => app()->formatter->asMoney(
                        empty($account->balance) ? 0 : +$account->balance, Currency::getCurrencySymbol($currencyId)),
                    'bonusBalance'  => app()->formatter->asMoney(
                        empty($bonusAccount->balance) ? 0 : +$bonusAccount->balance,
                        t('currency', 'B') . '(' . Currency::getCurrencySymbol($currencyId) . ')'),
                ]);
                $companies = ClientCompany::find()
                    ->alias('com')
                    ->joinWith(['clientHasCompanies cl'], false)
                    ->where(['cl.client_id' => user()->client_id])
                    //                    ->asArray()
                    ->all();
                $json['payment_html_select'] = (new Order())->getPaymentHtmlSelect($companies, false,
                    intVal($showCardPayment));
            } else {
                $json['client_more_info'] = '';
                $companies = ClientCompany::find()
                    ->where(['company_id' => $companyList])
                    //                    ->asArray()
                    ->all();
                $json['payment_html_select'] = (new Order())->getPaymentHtmlSelect($companies, false,
                    intVal($showCardPayment), true);
            }
        }

        return json_encode($json);
    }

    /**
     * Add a new address in order card.
     * @return html
     */
    public function actionGetNewAddress()
    {
        $parking = ArrayHelper::map(
            Parking::getParking(user()->tenant_id, post('city_id')), 'parking_id', 'name');

        return $this->renderAjax(
            '@app/modules/order/views/order/new_address',
            [
                'char'      => post('char'),
                'city_id'   => post('city_id'),
                'city_name' => post('city_name'),
                'parking'   => $parking,
                'isClient'  => true,
            ]
        );
    }

    /**
     * Getting street list data for autocomplite
     *
     * @param string $search String search
     * @param string $city_id City Id of branch
     * @param string $lat Latitude of central searching point
     * @param string $lon Longitude of central searching point
     * @param string $action Search action. Ex.: 'autocomplete', 'search'
     * @param string $client_id Client ID
     *
     * @return array|false
     */
    public function actionGetStreetList($search, $city_id, $lat, $lon, $action, $client_id = null)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        app()->response->format = Response::FORMAT_JSON;

        switch ($action) {
            case self::ACTION_HISTORY:
                return $this->getAddressInDatabase($client_id, $city_id);

            case self::ACTION_AUTO_COMPLETE:
            default:
                return $this->getAddressInAutoComplete($search, $city_id, $lat, $lon, $action);
        }
    }

    /**
     * Получить адреса из автокомплита
     *
     * @param $search
     * @param $city_id
     * @param $lat
     * @param $lon
     * @param $action
     *
     * @return array
     */
    public function getAddressInAutoComplete($search, $city_id, $lat, $lon, $action)
    {
        $autoComplete = app()->autocomplete;
        $autoComplete->action = $action;

        $lang = TenantSetting::getSettingValue(user()->tenant_id, DefaultSettings::SETTING_LANGUAGE, $city_id);

        $result = Json::decode($autoComplete->getStreetList($search, $city_id, $lat, $lon, $lang));
        $preparedData = $this->prepareData(getValue($result['results'], []));

        return $preparedData;
    }

    /**
     * Получить адреса из БД
     *
     * @param $client_id
     * @param $city_id
     *
     * @return array
     */
    public function getAddressInDatabase($client_id, $city_id)
    {
        $models = $this->findClientAddressHistoryModel($client_id, $city_id);

        if (!$models || user()->client_id !== (int)$client_id) {
            return [];
        }

        $result = [];

        foreach ($models as $model) {
            /** @var $model ClientAddressHistory */
            $result[] = [
                'address'  => [
                    'city'    => $model->city,
                    'comment' => '',
                    'house'   => $model->house,
                    'housing' => '',
                    'label'   => $model->street,
                    'lat'     => $model->lat,
                    'lon'     => $model->lon,
                    'porch'   => '',
                    'street'  => '',
                ],
                'distance' => 0,
                'type'     => '',
                'label'    => $model->getLabel(),
                'value'    => $model->getLabel(),
            ];
        }

        return $result;
    }

    public function findClientAddressHistoryModel($clientId, $cityId)
    {
        return ClientAddressHistory::findLast($clientId, $cityId);
    }

    /**
     * Preparing autocomplete data for jquery plugin
     *
     * @param array $dirtyData Autocomplete data
     *
     * @return array
     */
    private function prepareData(array $dirtyData)
    {
        $preparedData = [];

        foreach ($dirtyData as $key => $item) {
            $preparedData[$key] = $item;
            if (!empty($item['address']['city'])) {
                if (isset($item['address']['label']) && !empty($item['address']['label'])) {
                    $preparedData[$key]['label'] = $item['address']['city'] . ', ' . $item['address']['label'];
                } else {
                    $preparedData[$key]['label'] = $item['address']['city'];
                }
            } else {
                $preparedData[$key]['label'] = $item['address']['label'];
            }

            $preparedData[$key]['value'] = $item['address']['label'];
        }

        return $preparedData;
    }

    /**
     * Get parking by coords.
     *
     * @param integer $city_id
     * @param float $lat
     * @param float $lon
     *
     * @return json
     */
    public function actionGetParkingByCoords($city_id, $lat, $lon)
    {
        $coords = [
            'lat' => $lat,
            'lon' => $lon,
        ];

        return $this->getParkingInsideByCoords($coords, $city_id);
    }

    /**
     * Getting all parking with parking which contains the point.
     *
     * @param array $coords ['lat' => '', 'lon' => '']
     * @param integer $city_id
     *
     * @return json
     */
    protected function getParkingInsideByCoords($coords, $city_id)
    {
        $json = [
            'parking' => null,
            'inside'  => null,
            'error'   => null,
        ];

        if (!empty($coords)) {
            //Получение парковки, содержащей кординаты
            $parking = app()->routeAnalyzer->getParkingByCoords(user()->tenant_id, $city_id, $coords['lat'],
                $coords['lon']);
            $json['parking'] = $parking['parking'];
            if (!empty($json['parking'])) {
                if (!empty($parking)) {
                    $json['inside'] = $parking['inside'];
                    $json['coords'] = $coords;
                } else {
                    $json['error'] = 'Адрес вне парковок.';
                }
            } else {
                $json['error'] = 'Не получены парковки.';
            }
        } else {
            $json['error'] = 'Не получены координаты.';
        }

        return json_encode($json);
    }

    /**
     * A preliminary calculation of the order.
     * @return json
     * @expectedException \Geocoder\Exception\ChainNoResultException
     */
    public function actionRouteAnalyzer()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $post = post('Order');
        $order = Order::findOne([
            'order_number' => $post['order_number'],
            'tenant_id'    => user()->tenant_id,
        ]);
        if (!$order) {
            $order = new Order();
        }
        $order->load(post());

        //Нет базовой парковки
        $error_no_base_parking = 100;

        if ($order->order_now == 0 && $order->order_date && $order->order_hours && $order->order_minutes) {
            $datetime = $order->order_date . ' ' . $order->order_hours . ':' . $order->order_minutes;
            $time = Yii::$app->formatter->asTimestamp($datetime);
        } else {
            $time = time() + $order->getOrderOffset();
        }

        $routeAnalyzer = app()->routeAnalyzer;
        $addressArray = array_values($order->addressFilter($order->address));
        $result = $routeAnalyzer->analyzeRoute(user()->tenant_id, $order->city_id, $addressArray,
            $order->additional_option, $order->tariff_id, date('d.m.Y H:i:s', $time), $order->client_id);

        if (empty($result)) {
            $result['error'] = $error_no_base_parking;
        }

        app()->response->headers->add('request-time',
            app()->request->headers->get('request-time'));

        return json_encode($result);
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionGetNew()
    {
        $city_id = user()->city_id;

        $date = get('date');
        $filterDate = strtotime($date) ? date('d.m.Y', strtotime($date)) : null;

        return $this->renderAjax('@app/modules/order/views/order/new', [
            'orders'         => Order::getOrdersFromRedisIsClient(OrderStatus::STATUS_GROUP_0,
                ['city_id' => $city_id, 'date' => $filterDate]),
            'user_city_list' => user()->getUserCityListWithRepublic(),
            'isClient'       => true,
        ]);
    }

    /**
     * List of all order in works.
     * @return mixed
     */
    public function actionGetWorks()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $date = get('date');
        $filterDate = strtotime($date) ? date('d.m.Y', strtotime($date)) : null;

        return $this->renderAjax('@app/modules/order/views/order/works', [
            'orders'   => Order::getOrdersFromRedisIsClient(OrderStatus::STATUS_GROUP_8,
                ['city_id' => get('city_id'), 'date' => $filterDate]),
            'isClient' => true,
        ]);
    }

    /**
     * List of all warning order.
     * @return mixed
     */
    public function actionGetWarning()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $date = get('date');
        $city_id = user()->city_id;
        $filterDate = strtotime($date) ? date('d.m.Y', strtotime($date)) : null;
        $orders = Order::getOrdersFromRedisIsClient(OrderStatus::STATUS_GROUP_7,
            ['city_id' => $city_id, 'date' => $filterDate]);
        $not_orders = ArrayHelper::getColumn($orders, 'order_id');
        $orders = array_merge($orders,
            Order::getByStatusGroupIsClient(OrderStatus::STATUS_GROUP_7, $city_id, $filterDate, [], [], $not_orders));

        return $this->renderAjax('@app/modules/order/views/order/warning', [
            'orders'   => $orders,
            'isClient' => true,
        ]);
    }

    public function actionGetPreOrders()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $date = get('date');
        $filterDate = strtotime($date) ? date('d.m.Y', strtotime($date)) : null;

        return $this->renderAjax('@app/modules/order/views/order/pre_order', [
            'orders'   => Order::getOrdersFromRedisIsClient(OrderStatus::STATUS_GROUP_6,
                ['city_id' => user()->city_id, 'date' => $filterDate]),
            'isClient' => true,
        ]);
    }

    /**
     * List of all completed order.
     * @return mixed
     */
    public function actionGetCompleted()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $date = get('date');
        $date_timestamp = strtotime($date);
        $filterDate = $date_timestamp ? date('d.m.Y', $date_timestamp) : null;
        $city_id = user()->city_id;
        $key = 'or_completed_client' . user()->tenant_id . '_' . user()->client_id . '_';

        //Today orders
        if (is_null($filterDate) || $filterDate == date('d.m.Y')) {
            $key .= 'today';
            $statuses = implode(', ', OrderStatus::getCompletedStatusId());
            $callback = function () use ($city_id) {
                return Order::getByStatusGroupIsClient(OrderStatus::STATUS_GROUP_4, $city_id);
            };
            $orders = Order::getTodayNoActiveOrdersFromCache($key, $statuses, $callback, 600, $city_id);
        } else {
            $orders = Order::getByStatusGroupIsClient(OrderStatus::STATUS_GROUP_4, $city_id, $filterDate);
        }

        return $this->renderAjax('@app/modules/order/views/order/completed', [
            'orders'   => $orders,
            'isClient' => true,
        ]);
    }

    /**
     * List of all completed order.
     * @return mixed
     */
    public function actionGetRejected()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $date = get('date');
        $date_timestamp = strtotime($date);
        $filterDate = $date_timestamp ? date('d.m.Y', $date_timestamp) : null;

        return $this->renderAjax('@app/modules/order/views/order/rejected', [
            'orders'   => Order::getByStatusGroupIsClient(OrderStatus::STATUS_GROUP_5, user()->city_id, $filterDate),
            'isClient' => true,
        ]);
    }

    /**
     * Return current time in city (+pickup)
     *
     * @param city_id
     *
     * @return array
     */
    public function actionGetCurrentTime()
    {
        app()->response->format = Response::FORMAT_JSON;

        $order = new Order;
        $date = time() + City::getTimeOffset(post('city_id'))
            + $order->getPickUp() + post('offset') * 60;

        return [
            'date'    => date('Y-m-d', $date),
            'hours'   => date('H', $date),
            'minutes' => date('i', $date),
        ];
    }

    public function actionOrderRefresh($controller = null)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $controller = is_null($controller) ? $this->id : $controller;
        $city_id = user()->city_id;
        $group = post('group');


        if (!empty($group)) {
            $json['count'] = Order::getOrderCount($group, $city_id);

            $arFilter = post('filter');

            if (!empty($city_id)) {
                $arFilter['city_id'] = user()->city_id;
            }
            $groupsFromRedis = OrderStatus::getGroupsFromRedis();

            if (in_array($group, $groupsFromRedis)) {

                $orders = Order::getOrdersFromRedisIsClient($group, $arFilter, post('sort'));
                if ($group == OrderStatus::STATUS_GROUP_7) {
                    $not_orders = ArrayHelper::getColumn($orders, 'order_id');
                    $orders = array_merge($orders,
                        Order::getByStatusGroupIsClient($group, $city_id, null, [], $arFilter['status'], $not_orders));
                }
            } else {
                $orders = Order::getByStatusGroupIsClient($group, $city_id, null,
                    ['sort' => post('sort'), 'order' => post('orderBy')], $arFilter['status']);
            }

            $json['html'] = $this->renderAjax('@app/modules/order/views/order/' . post('view'), [
                'orders'     => $orders,
                'controller' => $controller,
                'isClient'   => true,
            ]);
        }

        return json_encode($json);
    }

    public function actionSort($type, $city_id = null, $sourse = 'redis', $order = 'order_id')
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $arFilter = post('filter');

        if ($sourse == 'redis') {
            $orders = Order::getOrdersFromRedisIsClient($type, $arFilter, post('sort'));
        } else {
            $orders = Order::getByStatusGroupIsClient($type, user()->city_id, null,
                ['sort' => post('sort'), 'order' => $order],
                $arFilter['status']);
        }

        return $this->renderAjax("@app/modules/order/views/order/_$type", [
            'orders'   => $orders,
            'isClient' => true,
        ]);
    }

    private function getWorkerShiftService()
    {
        return Yii::$app->getModule('employee')->get('workerShift');
    }

    public function actionGetEvents($order_id, $city_id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        //Результирующий массив
        $events = [];
        //Доп. информация по логу (Расчет стоимости, Отзыв)
        $info = [];
        //Получаем события из БД
        $db_events = OrderChangeData::find()->asArray()->where(['order_id' => $order_id])->all();
        //Смещение часового пояса
        $city_offset = City::getTimeOffset($city_id);
        //Берем из кеша все статусы, формируем карту
        $arStatus = ArrayHelper::map(OrderStatus::getStatusData(), 'status_id', 'name');
        //Кеш запросов
        $arCache = [];
        $count = count($db_events);
        $order_finished = false;
        $formatter = app()->formatter;
        $lastStatusId = null;

        $order = Order::findOne($order_id);
        $positionId = isset($order->position_id) ? $order->position_id : null;

        for ($i = 0; $i < $count; $i++) {
            $text = '';
            $info = [];

            if ($db_events[$i]['change_object_type'] == 'order'
                && $db_events[$i]['change_field'] == 'status_id'
            ) {
                $lastStatusId = $db_events[$i]['change_val'];
            }

            //Первый элемент - Создание заказа
            if ($i == 0 && $db_events[$i]['change_object_type'] == 'order' && $db_events[$i]['change_field'] == 'status_id') {

                if (in_array($db_events[$i]['change_val'], [
                    OrderStatus::STATUS_NEW,
                    OrderStatus::STATUS_NOPARKING,
                    OrderStatus::STATUS_PRE,
                    OrderStatus::STATUS_PRE_NOPARKING,
                    OrderStatus::STATUS_FREE,
                    OrderStatus::STATUS_MANUAL_MODE,
                ], false)) {
                    $text = t('order', 'Order create');
                } elseif ($db_events[$i]['change_val'] == OrderStatus::STATUS_EXECUTING) {
                    $time = implode(" ", [
                        $formatter->asDate($city_offset + $db_events[$i]['change_time'], 'shortDate'),
                        t('order', 'on'),
                        $formatter->asTime($city_offset + $db_events[$i]['change_time'], 'medium'),
                    ]);
                    $events[] = [
                        'TEXT'         => t('order', 'Order was created from bordur'),
                        'TIME'         => $time,
                        'TYPE'         => $db_events[$i]['change_object_type'],
                        'CHANGE_FIELD' => $db_events[$i]['change_field'],
                        'INFO'         => $info,
                    ];

                    $text = OrderStatusService::translate($db_events[$i]['change_val'], $positionId);
                }
            } //Изменения с заказом
            elseif ($db_events[$i]['change_object_type'] == 'order') {
                //Изменения клиента
                if (strpos($db_events[$i]['change_subject'], 'client') !== false) {

                    if ($db_events[$i]['change_field'] === 'address' || $db_events[$i]['change_field'] === 'payment') {

                        $arClientPieces = explode('_', $db_events[$i]['change_subject']);
                        $client_id = end($arClientPieces);

                        $client = Client::find()
                            ->where(['client_id' => $client_id])
                            ->select(['last_name', 'name', 'second_name'])
                            ->one();
                        $name = '<a href="' . Url::to([
                                '/client/base/update',
                                'id' => $client_id,
                            ]) . '">' . Html::encode($client->last_name . ' ' . $client->name . ' ' . $client->second_name) . '</a>';

                        $text = t('order', 'Client {name} edited order. That is: {field}', [
                            'name'  => $name,
                            'field' => Html::encode($order->getAttributeLabel($db_events[$i]['change_field'])),
                        ]);

                    }
                } //Изменения диспетчера
                elseif (strpos($db_events[$i]['change_subject'], 'disputcher') !== false) {
                    //Пропускаем служебные поля, которые не должен видеть пользователь
                    if ($db_events[$i]['change_field'] === 'client_id') {
                        continue;
                    }
                    $arUserPieces = explode('_', $db_events[$i]['change_subject']);
                    $user_id = end($arUserPieces);

                    $user = \app\modules\tenant\models\User::find()->
                    where(['user_id' => $user_id])->
                    select(['last_name', 'name'])->
                    one();
                    $name = '<a href="' . Url::to([
                            '/tenant/user/update',
                            'id' => $user_id,
                        ]) . '">' . Html::encode($user->last_name . ' ' . $user->name) . '</a>';

                    $order = new Order();

                    if ($db_events[$i]['change_field'] === 'status_id'
                        && in_array($db_events[$i]['change_val'], OrderStatus::getCompletedStatusId(), false)
                    ) {
                        $text = t('order', 'The order was completed by the dispatcher {name}.', [
                            'name' => $name,
                        ]);
                    } else {
                        $text = t('order', 'Disputcher {name} edited order. That is: {field}', [
                            'name'  => $name,
                            'field' => Html::encode($order->getAttributeLabel($db_events[$i]['change_field'])),
                        ]);
                    }
                } //Изменения водителя
                elseif ($db_events[$i]['change_subject'] == 'worker' && $db_events[$i]['change_field'] == 'address') {
                    $order = new Order();
                    $text = t('order', 'Worker edited order. That is: {field}', [
                        'field' => Html::encode($order->getAttributeLabel($db_events[$i]['change_field'])),
                    ]);
                } //Все остальные, связанные со сменой статуса
                elseif ($db_events[$i]['change_field'] == 'status_id') {
                    if (in_array($db_events[$i]['change_val'], OrderStatus::getFinishedStatusId(), false)
                        || $db_events[$i]['change_val'] == OrderStatus::STATUS_EXECUTING
                        || $db_events[$i]['change_val'] == OrderStatus::STATUS_WAITING_FOR_PAYMENT
                    ) {
                        $order_finished = true;
                    }
                    //Статус завершенного заказа
                    if (in_array($db_events[$i]['change_val'], OrderStatus::getCompletedStatusId(), false)) {
                        $text = OrderStatusService::translate($db_events[$i]['change_val'], $positionId);
                        $info = OrderDetailCost::find()
                            ->where(['order_id' => $db_events[$i]['order_id']])
                            ->orderBy(['detail_id' => SORT_DESC])
                            ->asArray()
                            ->one();
                    } //Пропускаем статус "Предложение водителю", т.к. уже вывели эту инфу вместе с водилой.
                    elseif (in_array($db_events[$i]['change_val'], [
                        OrderStatus::STATUS_OFFER_ORDER,
                        OrderStatus::STATUS_WORKER_REFUSED,
                        OrderStatus::STATUS_WORKER_IGNORE_ORDER_OFFER,
                    ], false)) {
                        continue;
                    } //Все остальные статусы, название берется из карты статусов
                    else {
                        $text = OrderStatusService::translate($db_events[$i]['change_val'], $positionId);
                        if ((int)$db_events[$i]['change_val'] === OrderStatus::STATUS_PAYMENT_CONFIRM) {
                            $priorEvent = isset($db_events[$i - 1]) ? $db_events[$i - 1] : null;

                            if ($priorEvent !== null
                                && $priorEvent['change_object_type'] === 'order'
                                && $priorEvent['change_subject'] === 'worker'
                                && $priorEvent['change_field'] === 'payment'
                                && $priorEvent['change_val'] === Order::PAYMENT_CASH
                            ) {
                                $text .= '. ' . t('order', 'Error of non-cash payment (requires payment in cash)');
                            }
                        }
                    }
                }
            } //Изменения с исполнителем
            elseif ($db_events[$i]['change_object_type'] == 'worker') {
                //Формирование кеша для того чтобы не делать лишние запросы на след. эл. массива лога
                if (empty($arCache['worker'][$db_events[$i]['change_object_id']])) {
                    $workerService = $this->getWorkerService();

                    $dbWorker = $workerService->getWorkerByCallsign($db_events[$i]['change_object_id'],
                        $db_events[$i]['tenant_id'], ['worker_id', 'last_name', 'name']);

                    $arCache['worker'][$db_events[$i]['change_object_id']] = '<a href="' . Url::to([
                            '/employee/worker/update',
                            'id' => $dbWorker['worker_id'],
                        ]) . '">' . Html::encode($dbWorker['last_name'] . ' ' . $dbWorker['name']) . '</a>';
                }

                //Предложение заказа
                if ($db_events[$i]['change_val'] == 'OFFER_ORDER') {
                    $text = t('order', 'Order offer worker: {name}',
                        ['name' => $arCache['worker'][$db_events[$i]['change_object_id']]]);
                } elseif ($db_events[$i]['change_val'] == 'FREE' && !$order_finished) {
                    switch ($lastStatusId) {
                        case OrderStatus::STATUS_WORKER_REFUSED:
                            $message = 'Worker {name} refused';
                            break;
                        case OrderStatus::STATUS_WORKER_IGNORE_ORDER_OFFER:
                            $message = 'Worker {name} ignored order offer';
                            break;
                        default:
                            $message = 'Worker {name} removed from the order';
                    }
                    $text = t('order', $message,
                        ['name' => $arCache['worker'][$db_events[$i]['change_object_id']]]);
                } //Исполнитель заблокирован
                elseif ($db_events[$i]['change_val'] == 'BLOCKED') {
                    $text = t('order', 'Worker {name} blocked',
                        ['name' => $arCache['worker'][$db_events[$i]['change_object_id']]]);
                } //Взял заказ
                elseif ($db_events[$i]['change_val'] == 'ON_ORDER') {
                    //Для получения времени подъезда берем следующий эл. массива
                    $nextAfterElement = $db_events[$i + 1];
                    $time = $nextAfterElement['change_field'] == 'time_to_client' ? $nextAfterElement['change_val'] : 0;

                    $text = t('order', 'Worker {name} accept an order and arrive in {time} min.',
                        ['name' => $arCache['worker'][$db_events[$i]['change_object_id']], 'time' => $time]);

                    //Удаляем элементы, т.к. мы их уже обработали
                    if (($db_events[$i + 2]['change_field'] == 'status_id')
                        && ($db_events[$i + 2]['change_val'] == OrderStatus::STATUS_GET_WORKER
                            || $db_events[$i + 2]['change_val'] == OrderStatus::STATUS_EXECUTION_PRE)
                    ) {
                        unset($db_events[$i + 2]);
                    }

                    if ($db_events[$i + 1]['change_field'] == 'time_to_client') {
                        unset($db_events[$i + 1]);
                    }
                    // worker accepted an pre-order
                } elseif ($db_events[$i]['change_val'] == 'ACCEPT_PREORDER') {
                    $text = t('order', 'Worker {name} accepted an pre-order', [
                        'name' => $arCache['worker'][$db_events[$i]['change_object_id']],
                    ]);
                    if ($db_events[$i + 1]['change_field'] == 'status_id'
                        && $db_events[$i + 1]['change_val'] == OrderStatus::STATUS_PRE_GET_WORKER
                    ) {
                        unset($db_events[$i + 1]);
                    }
                    // worker refused an pre-order
                } elseif ($db_events[$i]['change_val'] == 'REFUSE_PREORDER') {
                    $text = t('order', 'Worker {name} refused an pre-order', [
                        'name' => $arCache['worker'][$db_events[$i]['change_object_id']],
                    ]);
                    if ($db_events[$i + 1]['change_field'] == 'status_id'
                        && $db_events[$i + 1]['change_val'] == OrderStatus::STATUS_PRE_REFUSE_WORKER
                    ) {
                        unset($db_events[$i + 1]);
                    }
                }
            } //Оставлен отзыв к заказу
            elseif ($db_events[$i]['change_object_type'] == 'review') {
                $clientReview = ClientReview::find()->
                with([
                    'client' => function ($query) {
                        $query->select(['client_id', 'last_name', 'name']);
                    },
                ])->
                where(['review_id' => $db_events[$i]['change_object_id']])->
                one();

                $clientName = $clientReview->client->last_name . ' ' . $clientReview->client->name;

                $text = t('order', 'Client {name} add a review about the trip:', [
                    'name' => '<a href="' . Url::to([
                            '/client/base/update',
                            'id' => $clientReview->client->client_id,
                        ]) . '">' . Html::encode($clientName) . '</a>',
                ]);

                $info = [
                    'REVIEW'  => $clientReview->text,
                    'RAITING' => $clientReview->rating,
                ];
            }

            //Формирование результирующего массива
            if (!empty($text)) {
                $time = implode(" ", [
                    $formatter->asDate($city_offset + $db_events[$i]['change_time'], 'shortDate'),
                    t('order', 'on'),
                    $formatter->asTime($city_offset + $db_events[$i]['change_time'], 'medium'),
                ]);
                $events[] = [
                    'TEXT'         => t('status_event', $text),
                    'TIME'         => $time,
                    'TYPE'         => $db_events[$i]['change_object_type'],
                    'CHANGE_FIELD' => $db_events[$i]['change_field'],
                    'INFO'         => $info,
                ];
            }
        }
        $raw_cacl_data = (new \yii\db\Query())
            ->select(['raw_cacl_data'])
            ->from('tbl_raw_order_calc')
            ->where(['order_id' => $order_id])
            ->scalar();

        $raw_cacl_data = !empty($raw_cacl_data) ? json_decode($raw_cacl_data, true) : null;
        $order = Order::findone($order_id);


        $currencyId = ArrayHelper::getValue($order, 'currency_id');
        $currencySymbol = Html::encode(Currency::getCurrencySymbol($currencyId));

        $priceCalculation = (new CalculationService($order, $currencySymbol))->getPriceCalculation();

        return $this->renderAjax('@app/modules/order/views/order/events', [
            'events'        => $events,
            'currency_id'   => empty($order) ? null : $order->currency_id,
            'raw_cacl_data' => $raw_cacl_data,
            'order_id'      => $order_id,

            'priceCalculation' => $priceCalculation
        ]);
    }

    public function actionClientReview($order_id = null)
    {
        $review = ClientReview::findOne(['order_id' => $order_id]);

        return $this->renderAjax('@app/modules/order/views/order/client_review', [
            'review' => $review,
        ]);
    }

    /**
     * Getting position list
     * @return array
     */
    public function actionGetPositionList()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        app()->response->format = Response::FORMAT_JSON;

        $city_id = Yii::$app->request->post('city_id');
        $positions = $this->module->orderService->getPositions($city_id);

        return $positions;
    }

    /**
     * Метод возращает список всех тарифов для определенного города и профессии
     * @return array
     */
    public function actionGetTariffList()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        app()->response->format = Response::FORMAT_JSON;

        $city_id = post('city_id');
        $position_id = post('position_id');
        $clientId = post('client_id');
        $payment = post('payment');

        $companyId = null;
        if (app()->orderService->isCorpBalance($payment)) {
            $company = explode(Order::PAYMENT_CORP . '_', $payment, 2);
            $payment = Order::PAYMENT_CORP;
            $companyId = isset($company[1]) ? $company[1] : null;
        }

        $tariffs = app()->orderService->getTariffs($city_id, $position_id, $clientId, $payment, $companyId, true,
            ['cabinet']);
        $tariffs = array_map(function ($item) {
            $item['description'] = str_replace(PHP_EOL, '<br>', Html::encode($item['description']));

            return $item;
        }, $tariffs);

        return $tariffs;
    }

    /**
     * View order page by id or order number.
     *
     * @param integer $order_number
     *
     * @throws NotFoundHttpException
     * @return mixed
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionView($order_number)
    {

        $orderNumber = filter_var($order_number, FILTER_SANITIZE_NUMBER_INT);

        $orderObj = $this->getOrderViewObject($orderNumber);


        $activeOrder = Order::getOrderFromRedis($orderObj->tenant_id, $orderObj->order_id);

        /** @var $orderService OrderService */
        $orderService = app()->orderService;

        $clientInfo = $orderService->getClientInformationById($orderObj->tenant_id, $orderObj->city_id,
            $orderObj->client_id);
        if ($activeOrder) {
            if (OrderService::isActiveOrderProcessingThroughTheExternalExchange($orderObj->tenant_id, $orderObj->order_id)) {

                $workerInfo = OrderService::getActiveWorkerInformationFromExternalExchange($orderObj->tenant_id, $orderObj->order_id);
            } else {
                $workerInfo = $orderService->getWorkerInformation($orderObj->tenant_id, $orderObj->worker_id, $orderObj->car_id,
                    $orderObj->city_id, $orderObj->position_id);
            }
        } else {
            if ($orderObj->processed_exchange_program_id) {
                $workerInfo = OrderService::getWorkerInformationFromExternalExchange($orderObj->order_id);
            } else {
                $workerInfo = $orderService->getWorkerInformation($orderObj->tenant_id, $orderObj->worker_id, $orderObj->car_id,
                    $orderObj->city_id, $orderObj->position_id);
            }
        }

        $positions = app()->orderService->getPositions($orderObj->city_id);
        $tariffs = app()->orderService->getTariffs($orderObj->city_id, $orderObj->position_id, $orderObj->client_id,
            $orderObj->payment, $orderObj->company_id, false, ['cabinet']);

        $view = '@app/modules/order/views/order/view';
        $params = [
            'order'               => $orderObj,
            'dataForm'            => $orderObj->getFormData(null, $orderObj->status_id),
            'parkingList'         => $orderObj->getAddressParklingList(),
            'positions'           => $positions,
            'tariffs'             => $tariffs,
            'clientInfo'          => $clientInfo,
            'workerInfo'          => $workerInfo,
            'availableAttributes' => [],
            'isClient'            => true,
            'show_phone'          => $this->isShowPhone($orderObj->city_id),
        ];

        // Если заказы другого клиента и мы не администратор
        if ($params['order']->client_id != user()->client_id
            && !user()->isAdmin($params['order']->company_id)
        ) {
            if (app()->request->isAjax) {
                return false;
            }
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if (app()->request->isAjax) {
            return $this->renderAjax($view, $params);
        } else {
            return $this->render($view, $params);
        }
    }

    protected function getOrderViewObject($order_number)
    {
        $order = Order::getOrderInfo($order_number);
        $order->additional_option = ArrayHelper::map($order->options, 'option_id', 'name');

        return $order;
    }

    /**
     * @return WorkerService
     * @throws \yii\base\InvalidConfigException
     */
    protected function getWorkerService()
    {
        return Yii::$app->getModule('employee')->get('worker');
    }

    /**
     * Get worker coords by order id
     * @param $order_id
     * @return bool|string
     */
    public function actionGetWorkerCoordsByOrderId($order_id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $json = ['error' => t('order', 'No data for display')];
        $workerShiftService = $this->getWorkerShiftService();
        if (OrderService::isActiveOrderProcessingThroughTheExternalExchange(user()->tenant_id, $order_id)) {
            $carInfo = OrderService::getCarInfoFromExternalExchangeProcessedOrder(user()->tenant_id, $order_id);
            if ($carInfo) {
                $activeWorker = $workerShiftService->getExternalWorkerLocation($carInfo);
                $json = $activeWorker;
                $json['callsign'] = '';
                $json['error'] = '';
            }
        } else {
            $activeOrder = Order::getOrderFromRedis(user()->tenant_id, $order_id);
            $workerCallsign = ArrayHelper::getValue($activeOrder, 'worker.callsign', null);
            if (!empty($workerCallsign)) {
                $activeWorker = $workerShiftService->getWorkerLocation($workerCallsign);
                if (!empty($activeWorker)) {
                    $json = $activeWorker;
                    $json['callsign'] = $workerCallsign;
                    $json['error'] = '';
                }
            }
        }

        return json_encode($json);
    }


    /**
     * Getting current worker coords by id.
     * @param $worker_id
     * @return bool|string
     * @throws \yii\base\InvalidConfigException
     */
    public function actionGetWorkerCoordsById($worker_id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $json = ['error' => t('order', 'No data for display')];
        $workerService = $this->getWorkerService();
        $workerCallsign = $workerService->getCallsign($worker_id);

        $workerShiftService = $this->getWorkerShiftService();

        if (!empty($workerCallsign)) {
            $activeWorker = $workerShiftService->getWorkerLocation($workerCallsign);

            if (!empty($activeWorker)) {
                $json = $activeWorker;
                $json['callsign'] = $workerCallsign;
                $json['error'] = '';
            }
        }

        return json_encode($json);
    }


    /**
     *  Getting current worker coords by callsign.
     * @param $callsign
     * @return bool|string
     */
    public function actionGetWorkerCoordsByCallsign($callsign)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $workerShiftService = $this->getWorkerShiftService();

        return json_encode($workerShiftService->getWorkerLocation($callsign));
    }

    /**
     * Determining the type of data displayed on the map. Loading view with map.
     *
     * @param integer $order_id Used in the view like GET param
     * @param integer $status_id Order field status_id
     *
     * @return json
     */
    public function actionGetMap($status_id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        return $this->getMap($status_id);
    }

    protected function getMap($status_id)
    {
        $map_data_type = Order::MAP_DATA_TYPE_ADDRESS;
        $status_map = ArrayHelper::map(OrderStatus::getStatusData(), 'status_id', 'status_group');

        if (isset($status_map[$status_id])) {
            if (in_array($status_map[$status_id], OrderStatus::getStatusGroupsForMapWorkerType(), false)) {
                $map_data_type = Order::MAP_DATA_TYPE_WORKER;
            } elseif (in_array($status_map[$status_id], OrderStatus::getStatusGroupsForMapRouteType(), false)) {
                $map_data_type = Order::MAP_DATA_TYPE_ROUTE;
            }
        }

        return $this->renderAjax('@app/modules/order/views/order/tracking',
            ['map_data_type' => $map_data_type, 'isClient' => true]);
    }

    /**
     * Getting order address coords
     *
     * @param int $order_id
     *
     * @return json
     */
    public function actionGetOrderAddress($order_id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $address = Order::getOrderAddress($order_id);

        return $address ? $address : ['error' => t('order', 'No data for display')];
    }

    /**
     * Getting order tracking coords.
     *
     * @param integer $order_id
     *
     * @return json
     * @throws \yii\base\InvalidConfigException
     */
    public function actionGetTracking($order_id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        /* @var $service OrderTrackService */
        $service = \Yii::createObject(OrderTrackService::class);
        $track = $service->getTrack($order_id);

        if (empty($track)) {
            return json_encode(['error' => t('order', 'No data for display')]);
        } else {
            return json_encode([
                'order_route' => $track,
            ]);
        }
    }

    public function actionCancelOrder()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        app()->response->format = Response::FORMAT_JSON;

        /** @var $operatorApi ApiInterface */
        $operatorApi = app()->operatorApi;

        $user = user();
        $tenantId = $user->tenant_id;
        $clientId = $user->client_id;
        $orderId = post('order_id');
        $statusId = OrderStatus::STATUS_REJECTED;
        $lang = $user->lang;

        $order = Order::findOne($orderId);

        if ($order->client_id == $user->client_id || $user->isAdmin($order->company_id)) {
            try {
                $requestId = $operatorApi->rejectOrder($tenantId, $clientId, $orderId, $statusId, $lang);
            } catch (\Exception $ex) {
                $requestId = null;
            }
        } else {
            $requestId = null;
        }

        return ['requestId' => $requestId];
    }

    public function actionGetRejectOrderResult($requestId)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        app()->response->format = Response::FORMAT_JSON;

        /** @var $operatorApi ApiInterface */
        $operatorApi = app()->operatorApi;

        $response = $operatorApi->getRejectOrderResult($requestId);

        $result = isset($response['result']['reject_result']) ? $response['result']['reject_result'] : null;
        $info = empty($response['info']) ? $response['info'] : t('order',
            'An error occurred while canceling an order');

        return [
            'result' => $result,
            'error'  => $result === 0 ? $info : null,
        ];
    }

    /**
     * Getting is-fix field
     *
     * @param int $cityId
     * @param int $positionId
     *
     * @return mixed
     */
    public function actionGetIsFixField($cityId, $positionId)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $isFix = TenantSetting::getSettingValue(
            user()->tenant_id, DefaultSettings::SETTING_IS_FIX, $cityId, $positionId);

        return $this->renderAjax('@app/modules/order/views/order/_isFixField',
            ['order' => new Order(), 'showField' => (int)$isFix === 1]);
    }

    /**
     * Search clients
     *
     * @param string $query
     *
     * @return array JSON
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function actionSearchClients($query)
    {
        app()->response->format = Response::FORMAT_JSON;

        /* @var $service OrderService */
        $service = app()->orderService;

        $tenantId = user()->tenant_id;

        $clientIds = ClientService::getCompanyClientIds($tenantId, user()->client_id);
        $clientIds[] = user()->client_id;

        return array_values(array_filter($service->searchClients($tenantId, $query),
            function ($client) use ($clientIds) {
                return in_array($client['clientId'], $clientIds, false);
            }));
    }

    /**
     * Getting client information
     *
     * @param string $phone
     * @param int $cityId
     *
     * @return array JSON
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionGetClientInfo($phone, $cityId)
    {
        app()->response->format = Response::FORMAT_JSON;

        /* @var $service OrderService */
        $service = app()->orderService;

        $tenantId = user()->tenant_id;

        $info = $service->getClientInformationByPhone($tenantId, $cityId, $phone);

        if ($info === null) {
            throw new NotFoundHttpException('Client not found');
        } else {
            if (user()->getPhone() !== $phone) {
                $companies = user()->getCompanyListIsAdmin();
                $payments = isset($info['payments']) ? $info['payments'] : [];

                $info['payments'] = array_values(array_filter($payments,
                    function ($payment) use ($companies) {
                        $companyId = isset($payment['companyId']) ? $payment['companyId'] : null;

                        return $payment['type'] === 'CORP_BALANCE'
                            && in_array($companyId, $companies, false);
                    }));
            }

            return $info;
        }
    }

    /**
     * Search worker
     *
     * @param string $query
     * @param int $cityId
     * @param int $positionId
     *
     * @return array JSON
     * @throws \yii\base\InvalidConfigException
     */
    public function actionSearchWorkers($query, $cityId, $positionId)
    {
        app()->response->format = Response::FORMAT_JSON;

        /* @var $service OrderService */
        $service = \Yii::createObject(OrderService::className());

        $tenantId = user()->tenant_id;

        return $service->searchWorkers($tenantId, $cityId, $positionId, $query);
    }

    /**
     * Getting worker information
     *
     * @param int $workerId
     * @param int $carId
     * @param int $cityId
     * @param int $positionId
     *
     * @return array JSON
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     *
     */
    public function actionGetWorkerInfo($workerId, $carId, $cityId, $positionId)
    {
        app()->response->format = Response::FORMAT_JSON;

        /* @var $service OrderService */
        $service = app()->orderService;

        $tenantId = user()->tenant_id;

        $info = $service->getWorkerInformation($tenantId, $workerId, $carId, $cityId, $positionId);
        if ($info === null) {
            throw new NotFoundHttpException('Worker not found');
        } else {
            return $info;
        }
    }

    public function actionGetPaymentList($clientId, $tariffId)
    {
        app()->response->format = Response::FORMAT_JSON;

        $payments = [];

        $tariff = TaxiTariff::findOne(['tariff_id' => $tariffId, 'tenant_id' => user()->tenant_id]);
        if (!$tariff) {
            return false;
        }

        $client = Client::findOne(['client_id' => $clientId, 'tenant_id' => user()->tenant_id]);
        if (!$client) {
            $clientId = user()->id;
        }

        if (user()->client_id == $clientId && ($tariff->type === TaxiTariff::TYPE_BASE || $tariff->type === TaxiTariff::TYPE_COMPANY)) {
            $payments[] = ['type' => Order::PAYMENT_CASH, 'name' => t('order', 'Cash')];
            $payments[] = ['type' => Order::PAYMENT_PERSON, 'name' => t('order', 'Personal account')];
            $payments[] = ['type' => Order::PAYMENT_CARD, 'name' => t('order', 'Bank card')];
        }
        if ($tariff->type === TaxiTariff::TYPE_COMPANY || $tariff->type === TaxiTariff::TYPE_COMPANY_CORP_BALANCE) {
            $companyHasClient = ClientHasCompany::find()
                ->where([
                    'client_id' => $clientId,
                ])
                ->all();
            $companyHasClient = ArrayHelper::map($companyHasClient, 'company_id', function ($model) {
                return $model->company->name;
            });

            $companyHasTariff = TaxiTariffHasCompany::find()
                ->select('company_id')
                ->where([
                    'tariff_id' => $tariffId,
                ])
                ->all();
            $companyHasTariff = ArrayHelper::map($companyHasTariff, 'company_id', 'company_id');
            $companies = array_intersect_key($companyHasClient, $companyHasTariff);

            foreach ($companies as $companyId => $company) {
                $payments[] = ['type' => Order::PAYMENT_CORP, 'name' => $company, 'companyId' => $companyId];
            }
        }

        return $payments;
    }

}