<?php

namespace frontend\modules\client\modules\cabinet\assets;

use yii\web\AssetBundle;

class HeaderAsset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/client/modules/cabinet/assets/';
    public $publishOptions = [
        'forceCopy' => true
    ];
    public $css = [
        'css/main.css'
    ];


    public $depends = [
        'frontend\modules\client\modules\cabinet\assets\AppAsset',
    ];

}
