<?php

namespace app\modules\client\models;

use yii\data\ActiveDataProvider;
use \app\modules\balance\models\Account;
use \frontend\modules\tenant\models\Currency;

/**
 * ClientSearch represents the model behind the search form about `frontend\modules\car\models\Car`.
 */
class CompanySearch extends ClientCompany
{
    const PAGE_SIZE = 20;

    public $input_search;
    public $without_client;
    public $formName = null;
    private $accessCityList = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['input_search'], 'safe'],
            [['without_client'], 'integer'],
            [['city_id'], 'in', 'range' => $this->getAccessCityList(), 'allowArray' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'input_search' => t('client', 'Name'),
            'city_id'      => t('user', 'All cities'),
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params Filter data
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $query = parent::find();
        $query->where([
            'tenant_id' => user()->tenant_id,
        ]);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'sort'       => [
                'defaultOrder' => [
                    'name' => SORT_ASC,
                ],
                'attributes'   => [
                    'name',
                    'block',
                    'city_id',
                ],
            ],
            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);

        $this->load($params, $this->formName);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            $query->andWhere(['city_id' => $this->getAccessCityList()]);

            return $dataProvider;
        }

        $query->andWhere(['city_id' => $this->getSearchCityList()]);
        $query->andFilterWhere([
            'or',
            ['like', 'name', $this->input_search],
            ['like', 'full_name', $this->input_search],
        ]);

        if (!empty($this->without_client)) {
            $withoutCompanyIds = ClientHasCompany::find()
                ->where(['client_id' => $this->without_client])
                ->select('company_id')
                ->column();

            $query->andFilterWhere(['not in', 'company_id', $withoutCompanyIds]);
        }

        return $dataProvider;
    }

    public function getAccessCityList()
    {
        return $this->accessCityList;
    }

    public function setAccessCityList($value)
    {
        $this->accessCityList = $value;
    }

    private function getSearchCityList()
    {
        return $this->city_id ? $this->city_id : $this->getAccessCityList();
    }

    public function getBalances()
    {
        $accounts = Account::getCurrencyIdByCity($this->company_id, user()->tenant_id, Account::COMPANY_KIND);

        $total = [];

        foreach ($accounts as $account) {
            $total[] = app()->formatter
                ->asCurrency($account['balance'], Currency::getCurrencyCode($account['currency_id']));
        }

        return $total;
    }
}
