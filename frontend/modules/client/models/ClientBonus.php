<?php

namespace frontend\modules\client\models;

use common\modules\employee\models\position\Position;
use common\modules\tenant\models\Tenant;
use frontend\modules\bonus\models\BonusSystem;
use Yii;
use common\modules\city\models\City;
use frontend\modules\car\models\CarClass;
use app\modules\tariff\models\TaxiTariff;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "{{%client_bonus}}".
 *
 * @property integer                $bonus_id
 * @property string                 $city_id
 * @property integer                $position_id
 * @property integer                $class_id
 * @property string                 $tenant_id
 * @property string                 $name
 * @property integer                $blocked
 * @property integer                $bonus_system_id
 *
 * @property Tenant                 $tenant
 * @property BonusSystem            $bonusSystem
 * @property City                   $city
 * @property CarClass               $class
 * @property ClientBonusHasTariff[] $clientBonusHasTariffs
 * @property ClientBonusGootax      $gootaxBonus
 */
class ClientBonus extends ActiveRecord
{
    const STATUS_NOT_BLOCKED = 0;
    const STATUS_BLOCKED = 1;

    public $choosed_tariffs = [];

    const SCENARIO_READ = 'readonly scenario';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_bonus}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bonus_system_id', 'city_id', 'position_id', 'name', 'blocked'], 'required'],
            [['bonus_system_id', 'city_id', 'position_id', 'class_id', 'blocked'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique', 'targetAttribute' => ['tenant_id', 'city_id', 'name']],
            [['created_at', 'updated_at', 'tenant_id'], 'safe'],
            ['choosed_tariffs', 'required'],
        ];
    }

    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            self::SCENARIO_READ => [],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bonus_id'          => Yii::t('client-bonus', 'Bonus ID'),
            'tenant_id'         => Yii::t('client-bonus', 'Tenant ID'),
            'bonus_system_id'   => Yii::t('client-bonus', 'Bonus System ID'),
            'city_id'           => Yii::t('client-bonus', 'The city, which has a bonus'),
            'position_id'       => Yii::t('employee', 'Profession'),
            'class_id'          => Yii::t('client-bonus', 'Car class'),
            'choosed_tariffs'   => Yii::t('client-bonus', 'Tariffs'),
            'name'              => Yii::t('client-bonus', 'Name'),
            'blocked'           => Yii::t('client-bonus', 'Blocked'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getBonusSystem()
    {
        return $this->hasOne(BonusSystem::className(), ['id' => 'bonus_system_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getClass()
    {
        return $this->hasOne(CarClass::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getClientBonusHasTariffs()
    {
        return $this->hasMany(ClientBonusHasTariff::className(), ['bonus_id' => 'bonus_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTariffs()
    {
        return $this->hasMany(TaxiTariff::className(), ['tariff_id' => 'tariff_id'])
            ->via('clientBonusHasTariffs');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGootaxBonus()
    {
        return $this->hasOne(ClientBonusGootax::className(), ['bonus_id' => 'bonus_id']);
    }

    /**
     * Save choosed tariffs
     */
    protected function saveChoosedTariffs()
    {
        ClientBonusHasTariff::deleteAll(['bonus_id' => $this->bonus_id]);

        if (is_array($this->choosed_tariffs) && count($this->choosed_tariffs) > 0) {
            $values = [];
            foreach ($this->choosed_tariffs as $id) {
                if ($id !== null) {
                    $values[] = [$this->bonus_id, $id, time(), time()];
                }
            }
            self::getDb()->createCommand()
                ->batchInsert(ClientBonusHasTariff::tableName(),
                    ['bonus_id', 'tariff_id', 'created_at', 'updated_at'],
                    $values)->execute();
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if ($this->tenant_id === null) {
            $this->tenant_id = user()->tenant_id;
        }

        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        $this->saveChoosedTariffs();

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return ClientBonusQuery
     */
    public static function find()
    {
        return new ClientBonusQuery(get_called_class());
    }

}

class ClientBonusQuery extends ActiveQuery
{
    /**
     * Filter client bonuses by tenantId
     *
     * @param integer $tenantId
     *
     * @return ActiveQuery
     */
    public function byTenantId($tenantId)
    {
        return $this->andWhere([ClientBonus::tableName() . '.tenant_id' => $tenantId]);
    }

    /**
     * Filter client bonuses by cities
     *
     * @param array $cities
     *
     * @return ActiveQuery
     */
    public function byCities($cities)
    {
        return $this->andWhere([ClientBonus::tableName() . '.city_id' => $cities]);
    }

    public function onlyActive()
    {
        return $this->andWhere(['blocked' => 0]);
    }

    public function onlyBlocked()
    {
        return $this->andWhere(['blocked' => 1]);
    }

    /**
     * Filter client bonuses by status
     *
     * @param integer $blocked
     *
     * @return ActiveQuery
     */
    public function byStatus($blocked)
    {
        return $this->andWhere(['blocked' => $blocked]);
    }

    /**
     * Find by tariff_id
     *
     * @param integer $tariffId
     *
     * @return ActiveQuery
     */
    public function byTariffId($tariffId)
    {
        return $this->andWhere([ClientBonusHasTariff::tableName() . '.tariff_id' => $tariffId]);
    }

}