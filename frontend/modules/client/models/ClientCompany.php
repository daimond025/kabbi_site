<?php

namespace app\modules\client\models;

use app\modules\order\models\Order;
use common\helpers\DateTimeHelper;
use Yii;
use frontend\components\behavior\file\CropFileBehavior;
use app\modules\order\models\OrderStatus;
use app\modules\balance\models\Account;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%client_company}}".
 *
 * @property integer  $company_id
 * @property integer  $tenant_id
 * @property string   $name
 * @property string   $full_name
 * @property string   $legal_address
 * @property string   $post_address
 * @property string   $work_phone
 * @property string   $inn
 * @property string   $bookkeeper
 * @property string   $kpp
 * @property string   $ogrn
 * @property string   $site
 * @property string   $director
 * @property string   $director_post
 * @property string   $email
 * @property string   $contact_last_name
 * @property string   $contact_name
 * @property string   $contact_second_name
 * @property string   $contact_phone
 * @property string   $contact_email
 * @property integer  $block
 * @property string   $logo
 * @property integer  $city_id
 * @property string   $description
 * @property float    $credits
 *
 * @property Client[] $tblClients
 * @property Tenant   $tenant
 */
class ClientCompany extends \yii\db\ActiveRecord
{
    public $tariff_list = [];
    public $cropParams;
    public $contract_start_day;
    public $contract_start_month;
    public $contract_start_year;
    public $contract_end_day;
    public $contract_end_month;
    public $contract_end_year;

    const COUNT_SECONDS_FOR_ACTIVITY = 30*24*3600;
    const BLOCK = 1;
    const NOT_BLOCK = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_company}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['credits', 'default', 'value' => '0'],
            [['name', 'legal_address'], 'required'],
            [['block', 'city_id'], 'integer'],
            [['contract_number'], 'integer', 'max' => 999999999],
            [['name', 'full_name', 'legal_address', 'post_address'], 'string', 'max' => 255],
            [['block', 'tariff_list', 'contract_start_date', 'contract_end_date', 'cropParams'], 'safe'],
            [['contact_last_name', 'contact_name', 'contact_second_name', 'bookkeeper', 'director'], 'trim'],
            [
                ['work_phone', 'contact_phone'],
                'filter',
                'filter' => function ($value) {
                    return preg_replace("/[^0-9]/", '', $value);
                },
            ],
            [['work_phone', 'inn'], 'string', 'max' => 15],
            [
                [
                    'bookkeeper',
                    'site',
                    'director',
                    'director_post',
                    'email',
                    'contact_last_name',
                    'contact_name',
                    'contact_second_name',
                    'contact_phone',
                    'contact_email',
                ],
                'string',
                'max' => 45,
            ],
            [['kpp'], 'string', 'max' => 10],
            [['ogrn'], 'string', 'max' => 20],
            [
                ['logo'],
                'image',
                'extensions' => ['png', 'jpeg', 'jpg'],
                'maxSize'    => getUploadMaxFileSize(),
                'tooBig'     => t('file', 'The file size must be less than {file_size}Mb',
                    ['file_size' => getUploadMaxFileSize(true)]),
                'minWidth'   => 300,
                'minHeight'  => 300,
            ],
            [['email', 'contact_email'], 'email'],
            ['site', 'url'],
            [['description'], 'string'],
            ['credits', 'double'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id'          => Yii::t('client', 'Company ID'),
            'tenant_id'           => Yii::t('client', 'Tenant ID'),
            'name'                => Yii::t('client', 'Short name'),
            'full_name'           => Yii::t('client', 'Full Name'),
            'legal_address'       => Yii::t('client', 'Legal Address'),
            'post_address'        => Yii::t('client', 'Post Address'),
            'work_phone'          => Yii::t('client', 'Organization phone'),
            'inn'                 => Yii::t('client', 'Inn'),
            'bookkeeper'          => Yii::t('client', 'Bookkeeper'),
            'kpp'                 => Yii::t('client', 'Kpp'),
            'ogrn'                => Yii::t('client', 'Ogrn'),
            'site'                => Yii::t('client', 'Site'),
            'director'            => Yii::t('client', 'Director'),
            'director_post'       => Yii::t('client', 'Director Post'),
            'email'               => t('client', 'Email'),
            'contact_name'        => Yii::t('client', 'Contact name'),
            'contact_phone'       => Yii::t('client', 'Contact phone'),
            'contact_email'       => Yii::t('client', 'Contact email'),
            'block'               => Yii::t('client', 'Active'),
            'logo'                => Yii::t('client', 'Logo'),
            'city_id'             => Yii::t('tenant', 'City where the service is working'),
            'tariff_list'         => Yii::t('client', 'Available tariffs'),
            'contact_last_name'   => Yii::t('app', 'Last name'),
            'contact_second_name' => Yii::t('app', 'Second name'),
            'description'         => Yii::t('client', 'Description'),
            'contract_start_date' => Yii::t('client', 'Start Date'),
            'contract_end_date'   => Yii::t('client', 'End Date'),
            'contract_number'     => Yii::t('client', 'Contract number'),
            'credits'             => Yii::t('client', 'Credit'),
        ];
    }

    /**
     * Get client accounts
     * @return ActiveQuery
     */
    public function getAccounts()
    {
        return $this->hasMany(Account::className(), [
            'owner_id'  => 'company_id',
            'tenant_id' => 'tenant_id',
        ])->andOnCondition([
            'acc_kind_id' => Account::COMPANY_KIND,
        ])->andOnCondition([
            'acc_type_id' => Account::PASSIVE_TYPE,
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientHasCompanies()
    {
        return $this->hasMany(ClientHasCompany::className(), ['company_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasMany(Client::className(), ['client_id' => 'client_id'])
            ->viaTable('{{%client_has_company}}', ['company_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(\common\modules\tenant\models\Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(\common\modules\city\models\City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientCompanyHasCities()
    {
        return $this->hasMany(ClientCompanyHasCity::className(), ['company_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientCompanyHasTariffs()
    {
        return $this->hasMany(ClientCompanyHasTariff::className(), ['company_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffs()
    {
        return $this->hasMany(\frontend\modules\car\models\CarClass::className(),
            ['class_id' => 'tariff_id'])->viaTable('{{%client_company_has_tariff}}', ['company_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(\app\modules\order\models\Order::className(), ['company_id' => 'company_id']);
    }

    public function behaviors()
    {
        return [
            'fileBehavior' => [
                'class'      => CropFileBehavior::className(),
                'fileField'  => ['logo'],
                'upload_dir' => app()->params['upload'],
            ],
            'common'       => [
                'class' => \common\components\behaviors\ActiveRecordBehavior::className(),
            ],
        ];
    }

    public function getActiveFieldValue()
    {
        if ($this->block === self::NOT_BLOCK) {
            $lastOrderCreateTime = (int)Order::find()
                ->select('create_time')
                ->where([
                    'tenant_id'  => user()->tenant_id,
                    'company_id' => $this->company_id,
                ])
                ->orderBy(['create_time' => SORT_DESC])
                ->limit(1)
                ->scalar();

            return $lastOrderCreateTime > time() - self::COUNT_SECONDS_FOR_ACTIVITY
                ? t('app', 'Yes') : t('app', 'No');
        }

        return t('client', 'Blocked');
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * Company stuff list.
     *
     * @param integer $company_id
     *
     * @return array
     */
    public static function getStuffList($company_id)
    {
        return Client::find()
            ->where(['tenant_id' => user()->tenant_id])
            ->with([
                'clientPhones',
                'city' => function ($query) {
                    $query->select('city_id, name' . getLanguagePrefix());
                },
            ])
            ->joinWith([
                'clientHasCompanies' => function ($query) use ($company_id) {
                    $query->andWhere(['company_id' => $company_id]);
                },
            ])
            ->all();
    }

    /**
     * Getting client orders with limit 10.
     *
     * @param integer $id Company id
     * @param integer $client_id
     *
     * @return \app\modules\client\models\ClientCompany
     */
    public static function getClientOrdersByLimit($id, $limit = 10, $client_id = null)
    {
        return self::find()
            ->alias('t1')
            ->where([
                't1.company_id' => $id,
                't1.tenant_id'  => user()->tenant_id,
            ])
            ->with([
                'orders'             => function ($query) use ($limit, $client_id) {
                    $arStatuses = array_merge(OrderStatus::getCompletedStatusId(), OrderStatus::getRejectedStatusId());
                    $query->andWhere(['status_id' => $arStatuses]);
                    $query->andFilterWhere(['client_id' => $client_id]);
                    $query->limit($limit);
                    $query->orderBy(['order_id' => SORT_DESC]);
                },
                'orders.userCreated' => function ($query) {
                    $query->select(['user_id', 'last_name', 'name', 'second_name']);
                },
                'orders.clientReview',
                'orders.client'      => function ($query) {
                    $query->select(['client_id', 'last_name', 'name', 'second_name']);
                },
            ])
            ->select(['company_id'])
            ->one();
    }

    /**
     * Getting client orders by period.
     *
     * @param integer $id Company id
     * @param string  $first_date First date of period
     * @param string  $second_date Second date of period
     *
     * @return \app\modules\client\models\ClientCompany
     */
    public static function getClientOrdersByPeriod($id, $first_date = null, $second_date = null, $client_id = null)
    {
        $first  = strtotime($first_date);
        $second = strtotime($second_date) + 24 * 60 * 60;

        return self::find()
            ->where([
                'company_id' => $id,
                'tenant_id'  => user()->tenant_id,
            ])
            ->with([
                'orders'        => function ($query) use ($first, $second, $client_id) {
                    $arStatuses = array_merge(OrderStatus::getCompletedStatusId(), OrderStatus::getRejectedStatusId());
                    $query->andWhere('`create_time` >= ' . $first . ' AND `create_time` < ' . $second);
                    $query->andWhere(['status_id' => $arStatuses]);
                    $query->andFilterWhere(['client_id' => $client_id]);
                    $query->orderBy(['order_id' => SORT_DESC]);
                },
                'orders.clientReview',
                'orders.client' => function ($query) {
                    $query->select(['client_id', 'last_name', 'name', 'second_name']);
                },
            ])
            ->select(['company_id'])
            ->one();
    }

    /**
     * It needs for driver form.
     * @return array
     */
    public function getFormParams()
    {
        //Массив возвращаемых параметров
        $dataForm = [];

        $dataForm['STARTCONTRACT']['DAY'] = DateTimeHelper::getMonthDayNumbers();
        $dataForm['STARTCONTRACT']['DAY'] = [0 => t('app', 'Not selected')] + $dataForm['STARTCONTRACT']['DAY'];

        //Месяц
        $dataForm['STARTCONTRACT']['MONTH'] = DateTimeHelper::getMonthList();
        $dataForm['STARTCONTRACT']['MONTH'] = [0 => t('app', 'Not selected')] + $dataForm['STARTCONTRACT']['MONTH'];

        $dataForm['STARTCONTRACT']['YEAR'] = DateTimeHelper::getYearList(3, 3);
        $dataForm['STARTCONTRACT']['YEAR'] = [0 => t('app', 'Not selected')] + $dataForm['STARTCONTRACT']['YEAR'];

        $dataForm['ENDCONTRACT']['DAY'] = DateTimeHelper::getMonthDayNumbers();
        $dataForm['ENDCONTRACT']['DAY'] = [0 => t('app', 'Not selected')] + $dataForm['ENDCONTRACT']['DAY'];

        //Месяц
        $dataForm['ENDCONTRACT']['MONTH'] = DateTimeHelper::getMonthList();
        $dataForm['ENDCONTRACT']['MONTH'] = [0 => t('app', 'Not selected')] + $dataForm['ENDCONTRACT']['MONTH'];

        $dataForm['ENDCONTRACT']['YEAR'] = DateTimeHelper::getYearList(3, 3);
        $dataForm['ENDCONTRACT']['YEAR'] = [0 => t('app', 'Not selected')] + $dataForm['ENDCONTRACT']['YEAR'];

        return $dataForm;
    }

    public function getDateFieldFormat($day, $month, $year)
    {
        return (!empty($day) && !empty($month) && !empty($year)) ? $year . '-' . $month . '-' . $day : null;
    }

    public function isAdmin($client_id)
    {
        foreach ($this->clientHasCompanies as $client) {
            if ($client->client_id != $client_id) {
                continue;
            }
            if ($client->role == ClientHasCompany::ROLE_JURIDICAL) {
                return true;
            }
        }

        return false;
    }
}
