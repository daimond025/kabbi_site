<?php

namespace frontend\modules\client\models;

use Yii;

/**
 * This is the model class for table "{{%client_bonus_has_tariff}}".
 *
 * @property integer $id
 * @property integer $bonus_id
 * @property string $tariff_id
 *
 * @property TaxiTariff $tariff
 * @property ClientBonus $bonus
 */
class ClientBonusHasTariff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_bonus_has_tariff}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bonus_id', 'tariff_id'], 'required'],
            [['bonus_id', 'tariff_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('client', 'ID'),
            'bonus_id' => Yii::t('client', 'Bonus ID'),
            'tariff_id' => Yii::t('client', 'Tariff ID'),
        ];
    }

    public function behaviors() {
        return [
            \yii\behaviors\TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(TaxiTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonus()
    {
        return $this->hasOne(ClientBonus::className(), ['bonus_id' => 'bonus_id']);
    }
}
