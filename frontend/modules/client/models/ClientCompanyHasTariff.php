<?php

namespace app\modules\client\models;

use Yii;

/**
 * This is the model class for table "{{%client_company_has_tariff}}".
 *
 * @property string $company_id
 * @property integer $tariff_id
 *
 * @property ClientCompany $company
 * @property CarClass $tariff
 */
class ClientCompanyHasTariff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_company_has_tariff}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'tariff_id'], 'required'],
            [['company_id', 'tariff_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'tariff_id' => 'Tariff ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(ClientCompany::className(), ['company_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(CarClass::className(), ['class_id' => 'tariff_id']);
    }
    
    /**
     * Allow to save multiple rows
     * @param array $arTariff
     * @param array $company_id
     * @param bool $deleteAll Preliminary deleting records.
     * @return integer number of rows affected by the execution.
     * @throws Exception execution failed
     */
    public static function manySave($arTariff, $company_id, $deleteAll = false)
    {
        $insertValue = [];
        $connection = app()->db;
        
        if($deleteAll)
            self::deleteAll(['company_id' => $company_id]);
        
        if(empty($arTariff))
            $arTariff = \yii\helpers\ArrayHelper::getColumn(\frontend\modules\car\models\CarClass::find()->all(), 'class_id');

        foreach($arTariff as $tariff_id)
        {
            $insertValue[] = [$company_id, $tariff_id];
        }

        return $connection->createCommand()->batchInsert(self::tableName(), ['company_id', 'tariff_id'], $insertValue)->execute();
    }
}
