<?php

namespace app\modules\client\models;

use yii\data\ActiveDataProvider;

/**
 * ClientSearch represents the model behind the search form about frontend\modules\car\models\Car.
 */
class ClientSearch extends Client
{
    const PAGE_SIZE = 20;

    public $input_search;
    public $formName = null;
    public $workerBlocked;
    private $accessCityList = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['input_search'], 'safe'],
            [['city_id'], 'in', 'range' => $this->getAccessCityList(), 'allowArray' => true],
            ['workerBlocked', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'input_search' => t('client', 'Name'),
            'city_id'      => t('user', 'All cities'),
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params Filter data
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = parent::find()->alias('c');

        $query->where(['c.tenant_id' => user()->tenant_id,]);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'sort'       => [
                'defaultOrder' => [
                    'fullName' => SORT_ASC,
                ],
                'attributes'   => [
                    'fullName'         => [
                        'asc'  => ['c.last_name' => SORT_ASC, 'c.name' => SORT_ASC, 'c.second_name' => SORT_ASC],
                        'desc' => ['c.last_name' => SORT_DESC, 'c.name' => SORT_DESC, 'c.second_name' => SORT_DESC],
                    ],
                    'activeFieldValue' => [
                        'asc'  => ['c.black_list' => SORT_ASC],
                        'desc' => ['c.black_list' => SORT_DESC],
                    ],
                    'city_id',
                ],
            ],
            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);

        $this->load($params, $this->formName);

        if (!$this->validate()) {
            $query->where(['c.city_id' => $this->getSearchAccessCityList()]);
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $searchCityList = $this->getSearchCityList();

        if ($this->hasNullCityCondition()) {
            $query->andWhere(['or', ['c.city_id' => $searchCityList], ['c.city_id' => null]]);
        } else {
            $query->andWhere(['c.city_id' => $searchCityList]);
        }

        $query->andFilterWhere(
            [
                'or',
                ['like', 'c.last_name', $this->input_search],
                ['like', 'c.name', $this->input_search],
                ['like', 'c.second_name', $this->input_search],
                ['like', 'cp.value', $this->input_search],
            ]
        );

        if ($this->workerBlocked) {
            $query->andWhere([
                'like.worker_id' => $this->workerBlocked,
            ]);
            $query->andWhere(['>', 'like.dislike_count', 0]);
            $query->joinWith('clientWorkerLike like');
        }

        $query->joinWith('clientPhones cp');

        $dataProvider->totalCount = $query->count();

        return $dataProvider;
    }

    public function getAccessCityList()
    {
        return $this->accessCityList;
    }

    public function setAccessCityList($value)
    {
        $this->accessCityList = $value;
    }

    public static function searchByPhone($phone)
    {
        return parent::find()
            ->where(['tenant_id' => user()->tenant_id])
            ->joinWith(['clientPhones'])
            ->with([
                'companies' => function ($query) {
                    $query->select(['company_id', 'name']);
                },
            ])
            ->andWhere(['value' => $phone])
            ->select([
                parent::tableName() . '.client_id',
                parent::tableName() . '.tenant_id',
                'last_name',
                'name',
                'second_name',
                'black_list',
                'success_order',
                'fail_worker_order',
                'fail_client_order',
                'fail_dispatcher_order',
            ])
            ->one();
    }

    private function getSearchCityList()
    {
        return $this->city_id ? $this->city_id : $this->getSearchAccessCityList();
    }

    private function getSearchAccessCityList()
    {
        $accessCityList = $this->getAccessCityList();

        $nullKey = array_search('null', $accessCityList);

        if ($nullKey !== false) {
            unset($accessCityList[$nullKey]);
        }

        return $accessCityList;
    }

    /**
     * @return bool
     */
    private function hasNullCityCondition()
    {
        return empty($this->city_id) || $this->city_id[0] === 'null';
    }
}
