<?php

namespace frontend\modules\client\models\tariff;


use app\modules\tariff\models\FixTariff;
use yii\db\ActiveRecord;

/**
 * Class TariffHasFixRecord
 * @package frontend\modules\client\models\tariff
 *
 * @property integer $id
 * @property integer $type_id
 * @property integer $fix_id
 * @property double  $price_to
 * @property double  $price_back
 */
class TariffHasFixRecord extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%taxi_tariff_has_fix}}';
    }


    public function getTariffTYpe()
    {
        return $this->hasOne(TariffOptionTypeRecord::className(), ['type_id' => 'type_id']);
    }


    public function getFixTariff()
    {
        return $this->hasOne(FixTariff::className(), ['fix_id' => 'fix_id']);
    }
}