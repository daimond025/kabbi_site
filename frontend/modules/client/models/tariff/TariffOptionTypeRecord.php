<?php

namespace frontend\modules\client\models\tariff;


use yii\db\ActiveRecord;

/**
 * Class TariffOptionTypeRecord
 * @package frontend\modules\client\models\tariff
 *
 * @property integer                        $type_id
 * @property integer                        $tariff_id
 * @property integer                        $sort
 * @property string                         $type
 *
 * @property Tariff                         $tariff
 * @property TariffOptionActiveDateRecord[] $tariffOptionActiveDate
 * @property TariffOptionRecord[]           $tariffOption
 */
class TariffOptionTypeRecord extends ActiveRecord
{

    const TYPE_CURRENT = 'CURRENT';
    const TYPE_EXCEPTIONS = 'EXCEPTIONS';
    public static function tableName()
    {
        return '{{%taxi_tariff_option_type}}';
    }

    public function getTariff()
    {
        return $this->hasOne(TariffRecord::className(), ['tariff_id' => 'tariff_id']);
    }

    public function getTariffOptionActiveDate()
    {
        return $this->hasMany(TariffOptionActiveDateRecord::className(), ['type_id' => 'type_id']);
    }

    public function getTariffOption()
    {
        return $this->hasMany(TariffOptionRecord::className(), ['type_id' => 'type_id']);
    }
}