<?php

namespace frontend\modules\client\models\tariff;


use yii\helpers\ArrayHelper;

class TariffOptionActiveDate extends TariffOptionActiveDateRecord
{
    public function getName()
    {
        $arDate   = explode('|', $this->active_date, 2);
        $date     = ArrayHelper::getValue($arDate, 0, '');
        $interval = ArrayHelper::getValue($arDate, 1, '');

        // Каждый день
        if (empty($date)) {
            return t('taxi_tariff', 'Regularly') . ': ' . $interval;
        }

        // Каждый год
        if (preg_match('/^\d{2}.\d{2}$/', $date)) {
            return t('taxi_tariff', 'Regularly') . ': ' . $date . ' ' . $interval;
        }

        // Один раз
        if (preg_match('/^\d{2}.\d{2}.\d{4}$/', $date)) {
            return t('taxi_tariff', 'Once') . ": $date $interval";
        }

        // Каждую неделю
        return t('taxi_tariff', 'Regularly') . ': ' . app()->formatter->asDate($date, 'EEEE') . ' ' . $interval;
    }
}