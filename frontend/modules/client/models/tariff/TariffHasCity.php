<?php

namespace frontend\modules\client\models\tariff;

use yii\db\ActiveRecord;

/**
 * Class TariffHasCity
 * @package frontend\modules\client\models\tariff
 *
 * @property integer $tariff_id
 * @property integer $city_id
 */
class TariffHasCity extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%taxi_tariff_has_city}}';
    }
}