<?php

namespace frontend\modules\client\models\tariff;

use yii\db\ActiveRecord;

/**
 * Class TariffGroup
 * @package frontend\modules\client\models\tariff
 *
 * @property integer $group_id
 * @property integer $tenant_id
 * @property string  $name
 * @property integer $sort
 */
class TariffGroup extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%taxi_tariff_group}}';
    }
}