<?php

namespace frontend\modules\client\models\tariff;

use common\modules\employee\models\position\Position;
use common\modules\tenant\models\Tenant;
use frontend\modules\car\models\CarClass;
use yii\db\ActiveRecord;

/**
 * Class TariffRecord
 * @package app\modules\client\models\tariff
 *
 * @property integer            $tariff_id
 * @property integer            $tenant_id
 * @property integer            $class_id
 * @property integer            $block
 * @property integer            $group_id
 * @property string             $name
 * @property string             $description
 * @property integer            $sort
 * @property integer            $auto_downtime
 * @property integer            $enabled_site
 * @property integer            $enabled_app
 * @property integer            $enabled_operator
 * @property integer            $enabled_bordur
 * @property integer            $enabled_cabinet
 * @property string             $logo
 * @property integer            $position_id
 * @property string             $type
 *
 * @property Tenant             $tenant
 * @property CarClass           $carClass
 * @property Position           $position
 * @property TariffHasCity[]    $cities
 * @property TariffHasCompany[] $companies
 */
class TariffRecord extends ActiveRecord
{

    const TYPE_BASE = 'BASE';
    const TYPE_COMPANY = 'COMPANY';
    const TYPE_COMPANY_CORP_BALANCE = 'COMPANY_CORP_BALANCE';
    const TYPE_ALL = 'ALL';

    public static function tableName()
    {
        return '{{%taxi_tariff}}';
    }

    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    public function getCarClass()
    {
        return $this->hasOne(CarClass::className(), ['class_id' => 'class_id']);
    }

    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }

    public function getCities()
    {
        return $this->hasMany(TariffHasCity::className(), ['tariff_id' => 'tariff_id']);
    }

    public function getCompanies()
    {
        return $this->hasMany(TariffHasCompany::className(), ['tariff_id' => 'tariff_id']);
    }
}
