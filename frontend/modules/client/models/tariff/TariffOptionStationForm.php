<?php

namespace frontend\modules\client\models\tariff;

/**
 * Class TariffOptionStationForm
 * @package frontend\modules\client\models\tariff
 */

class TariffOptionStationForm extends BaseTariffOptionAirportOrStationForm
{
    public static function getAreaType()
    {
        return TariffOption::AREA_RAILWAY;
    }
}