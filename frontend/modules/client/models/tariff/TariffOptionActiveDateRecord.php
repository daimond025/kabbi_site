<?php

namespace frontend\modules\client\models\tariff;


use yii\db\ActiveRecord;

/**
 * Class TariffOptionActiveDateRecord
 * @package frontend\modules\client\models\tariff
 *
 * @property integer $date_id
 * @property integer $type_id
 * @property string $active_date
 *
 * @property TariffOptionTypeRecord $tariffOptionType
 */
class TariffOptionActiveDateRecord extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%taxi_tariff_option_active_date}}';
    }

    public function getTariffOptionType()
    {
        return $this->hasOne(TariffOptionTypeRecord::className(), ['type_id' => 'type_id']);
    }
}