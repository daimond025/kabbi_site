<?php

namespace frontend\modules\client\models\tariff;

/**
 * Class TariffOptionType
 * @package frontend\modules\client\models\tariff
 *
 * @property TariffOptionActiveDate[] $tariffOptionActiveDate
 * @property TariffOption[]           $tariffOption
 * @property boolean                  $isCurrentType
 */
class TariffOptionType extends TariffOptionTypeRecord
{

    public function getTariffOptionActiveDate()
    {
        return $this->hasMany(TariffOptionActiveDate::className(), ['type_id' => 'type_id']);
    }

    public function getTariffOption()
    {
        return $this->hasMany(TariffOption::className(), ['type_id' => 'type_id']);
    }

    public function getIsCurrentType()
    {
        return $this->type === TariffOptionType::TYPE_CURRENT;
    }
}