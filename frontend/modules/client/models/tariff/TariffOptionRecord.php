<?php

namespace frontend\modules\client\models\tariff;


use yii\db\ActiveRecord;

class TariffOptionRecord extends ActiveRecord
{

    const AREA_CITY = 'CITY';
    const AREA_TRACK = 'TRACK';
    const AREA_AIRPORT = 'AIRPORT';
    const AREA_RAILWAY = 'RAILWAY';

    const ACCRUAL_TIME = 'TIME';
    const ACCRUAL_DISTANCE= 'DISTANCE';
    const ACCRUAL_MIXED = 'MIXED';
    const ACCRUAL_FIX = 'FIX';
    const ACCRUAL_INTERVAL = 'INTERVAL';

    const ROUNDING_TYPE_FLOOR = 'FLOOR';
    const ROUNDING_TYPE_ROUND = 'ROUND';
    const ROUNDING_TYPE_CEIL  = 'CEIL';

    public static function tableName()
    {
        return '{{%taxi_tariff_option}}';
    }

}