<?php

namespace frontend\modules\client\models\tariff;

use yii\base\Model;

/**
 * Class TariffOptionAirportForm
 * @package frontend\modules\client\models\tariff
 *
 * @property TariffOptionType $optionType
 * @property TariffOption     $optionModel
 * @property boolean          $isSaved
 * @property boolean          $isActive
 * @property boolean          $isNewRecord
 *
 * @property integer          $wait_time
 * @property double           $wait_price
 * @property double           $rounding
 * @property string           $rounding_type
 */
abstract class BaseTariffOptionAirportOrStationForm extends Model
{
    public $optionType;
    public $optionModel;
    public $isSaved = false;
    public $isActive = false;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'isActive'      => t('taxi_tariff', 'Is active'),
            'wait_time'     => t('taxi_tariff', 'Free waiting time before a trip'),
            'wait_price'    => t('taxi_tariff', 'Cost of 1 minute waiting'),
            'rounding'      => t('taxi_tariff', 'Rounding'),
            'rounding_type' => t('taxi_tariff', 'Rounding rule'),
        ];
    }


    /**
     * @inheritdoc
     */

    public function rules()
    {
        return [

            ['isActive', 'required'],
            ['isActive', 'in', 'range' => [0, 1]],

            //Бесплатное время ожидания до начала поездки
            [
                'wait_time',
                'filter',
                'filter' => function () {
                    return 0;
                },
                'when'   => [$this, 'whenIsNotActive'],
            ],
            ['wait_time', 'required', 'when' => [$this, 'whenIsActive']],
            ['wait_time', 'integer', 'min' => 0, 'when' => [$this, 'whenIsActive']],

            //Стоимость 1 минуты ожидания
            [
                'wait_price',
                'filter',
                'filter' => function () {
                    return 0;
                },
                'when'   => [$this, 'whenIsNotActive'],
            ],
            ['wait_price', 'required', 'when' => [$this, 'whenIsActive']],
            ['wait_price', 'double', 'min' => 0, 'when' => [$this, 'whenIsActive']],

            //Округлять до
            [
                'rounding',
                'filter',
                'filter' => function () {
                    return 0.01;
                },
                'when'   => [$this, 'whenIsNotActive'],
            ],
            ['rounding', 'required', 'when' => [$this, 'whenIsActive']],
            ['rounding', 'double', 'min' => 0, 'when' => [$this, 'whenIsActive']],

            // Правило округления
            ['rounding_type', 'required', 'when' => [$this, 'whenIsActive']],
            [
                'rounding_type',
                'in',
                'range' => array_keys(TariffOption::getRoundingTypeMap()),
                'when'  => [$this, 'whenIsActive'],
            ],
        ];
    }

    public function whenIsActive($model)
    {
        return (bool)$model->isActive;
    }

    public function whenIsNotActive($model)
    {
        return !$this->whenIsActive($model);
    }

    public static function find($typeId)
    {
        $model             = new static();
        $model->optionType = TariffOptionType::findOne($typeId);

        if ($model->optionType) {
            $model->optionModel = TariffOption::find()->where([
                'type_id' => $typeId,
                'area'    => static::getAreaType(),
            ])->one();

            $model->isActive = (boolean)$model->optionModel;

            if (!$model->optionModel) {
                $model->optionModel             = new TariffOption();
                $model->optionModel->area       = static::getAreaType();
                $model->optionModel->type_id    = $typeId;
                $model->optionModel->wait_price = 0;
                $model->optionModel->wait_time  = 0;
                $model->optionModel->rounding   = 0.01;
            }
        }

        return $model;
    }


    public static function getAreaType()
    {
        return null;
    }


    public function getWait_time()
    {
        return $this->optionModel->wait_time;
    }

    public function setWait_time($value)
    {
        $this->optionModel->wait_time = $value;
    }

    public function getWait_price()
    {
        return $this->optionModel->wait_price;
    }

    public function setWait_price($value)
    {
        $this->optionModel->wait_price = $value;
    }

    public function getRounding()
    {
        return $this->optionModel->rounding;
    }

    public function setRounding($value)
    {
        $this->optionModel->rounding = $value;
    }

    public function getRounding_type()
    {
        return $this->optionModel->rounding_type;
    }

    public function setRounding_type($value)
    {
        return $this->optionModel->rounding_type = $value;
    }

    public function getIsNewRecord()
    {
        return $this->optionModel->isNewRecord;
    }

    public function save()
    {

        if ($this->validate()) {

            if (!$this->isActive) {
                TariffOption::deleteAll([
                    'type_id' => $this->optionType->type_id,
                    'area'    => static::getAreaType(),
                ]);
            } else {
                $this->optionModel->save(false);
            }


            $this->isSaved = true;

            return true;
        }

        $this->isSaved = false;

        return false;
    }
}