<?php

namespace frontend\modules\client\models\tariff;


use yii\helpers\ArrayHelper;

class TariffOption extends TariffOptionRecord
{
    public function getTypeName()
    {
        return ArrayHelper::getValue(TariffOption::getTypeMap(), $this->accrual);
    }

    public static function getTypeMap()
    {
        return [
            TariffOptionRecord::ACCRUAL_TIME     => t('taxi_tariff', 'Time'),
            TariffOptionRecord::ACCRUAL_DISTANCE => t('taxi_tariff', 'Distance'),
            TariffOptionRecord::ACCRUAL_FIX      => t('taxi_tariff', 'Fix'),
            TariffOptionRecord::ACCRUAL_MIXED    => t('taxi_tariff', 'For distance and time'),
            TariffOptionRecord::ACCRUAL_INTERVAL => t('taxi_tariff', 'Distance range'),
        ];
    }

    public static function getRoundingTypeMap()
    {
        return [
            TariffOptionRecord::ROUNDING_TYPE_CEIL  => t('taxi_tariff', 'to greater'),
            TariffOptionRecord::ROUNDING_TYPE_ROUND=> t('taxi_tariff', 'to nearest'),
            TariffOptionRecord::ROUNDING_TYPE_FLOOR => t('taxi_tariff', 'to smaller'),
        ];
    }

    public static function getKmPriceMap($currency = '₽')
    {
        return [
            '1_minute' => $currency . '/' . t('app', 'min.'),
            '30_minute' => $currency . '/30 ' . t('app', 'min.'),
            '1_hour' => $currency . '/1 ' . t('app', 'hour'),
        ];
    }
}