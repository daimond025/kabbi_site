<?php

namespace frontend\modules\client\models\tariff;

use common\components\behaviors\ActiveRecordBehavior;
use common\modules\employee\models\position\Position;
use common\modules\tenant\models\Tenant;
use common\modules\tenant\models\TenantCityHasPosition;
use common\modules\tenant\models\TenantHasCity;
use common\modules\tenant\models\TenantHelper;
use frontend\components\behavior\file\CropFileBehavior;
use frontend\modules\car\models\CarClass;
use frontend\modules\client\modules\cabinet\models\ClientCompany;
use yii\helpers\ArrayHelper;

/**
 * Class TariffForm
 * @package frontend\modules\client\models\tariff
 *
 * @property integer $city_id
 * @property boolean $isCompanyRequired
 *
 * @mixin CropFileBehavior
 */
class TariffForm extends Tariff
{
    public $cropParams;
    public $city_id;
    public $company_ids = [];

    protected $old_company_ids = [];

    const SCENARIO_UPDATE = 'update';
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPLOAD_LOGO = 'upload-logo';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['tenant_id', 'required', 'on' => [TariffForm::SCENARIO_CREATE]],
            ['tenant_id', 'exist', 'targetClass' => Tenant::className(), 'on' => [TariffForm::SCENARIO_CREATE]],

            ['class_id', 'required', 'on' => [TariffForm::SCENARIO_CREATE], 'when' => [$this, 'whenRequiredClassId']],
            ['class_id', 'exist', 'targetClass' => CarClass::className(), 'on' => [TariffForm::SCENARIO_CREATE]],

            ['name', 'required'],
            ['name', 'string', 'max' => 255],

            ['city_id', 'required', 'on' => [TariffForm::SCENARIO_CREATE]],
            [
                'city_id',
                'exist',
                'targetClass'     => TenantHasCity::className(),
                'targetAttribute' => ['city_id', 'tenant_id'],
                'on'              => [TariffForm::SCENARIO_CREATE],
            ],

            [
                'company_ids',
                'required',
                'when'                   => [$this, 'whenRequiredCompanies'],
                'enableClientValidation' => false,
            ],
            [
                'company_ids',
                'each',
                'rule' => [
                    'exist',
                    'targetClass'     => ClientCompany::className(),
                    'targetAttribute' => ['company_ids' => 'company_id', 'city_id' => 'city_id'],
                ],
                'when' => [$this, 'whenRequiredCompanies'],
            ],

            ['position_id', 'required', 'on' => [TariffForm::SCENARIO_CREATE]],
            [
                'position_id',
                'exist',
                'targetClass'     => TenantCityHasPosition::className(),
                'targetAttribute' => ['city_id', 'tenant_id', 'position_id'],
                'on'              => [TariffForm::SCENARIO_CREATE],
            ],

            [
                'group_id',
                'exist',
                'targetClass'     => TariffGroup::className(),
                'targetAttribute' => ['tenant_id', 'group_id'],
            ],

            ['description', 'string'],

            [
                ['enabled_site', 'enabled_app', 'enabled_operator', 'enabled_bordur', 'enabled_cabinet', 'enabled_hospital'],
                'in',
                'range' => [0, 1],
            ],

            ['sort', 'integer', 'min' => -999999, 'max' => 999999],

            ['type', 'required'],
            ['type', 'in', 'range' => array_keys(static::getTypeList())],

            ['auto_downtime', 'required'],
            ['auto_downtime', 'in', 'range' => [0, 1]],

            ['block', 'in', 'range' => [0, 1]],

            [
                'logo',
                'image',
                'extensions' => ['png', 'jpeg', 'jpg'],
                'maxSize'    => getUploadMaxFileSize(),
                'tooBig'     => t('file', 'The file size must be less than {file_size}Mb',
                    ['file_size' => getUploadMaxFileSize(true)]),
                'minWidth'   => 360,
                'minHeight'  => 160,
            ],

            ['cropParams', 'safe'],
        ];
    }

    public function whenRequiredClassId(TariffForm $model)
    {
        $position = Position::findOne($model->position_id);

        if ($position) {
            return (boolean) $position->has_car;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'class_id'         => t('taxi_tariff', 'Tariff type'),
            'city_id'          => t('taxi_tariff', 'The city where tariff will work'),
            'block'            => t('app', 'Block'),
            'group_id'         => t('taxi_tariff', 'Group'),
            'name'             => t('taxi_tariff', 'Name'),
            'description'      => t('taxi_tariff', 'Description'),
            'sort'             => t('app', 'Sort'),
            'auto_downtime'    => t('taxi_tariff', 'Enabling standby'),
            'enabled_site'     => t('taxi_tariff', 'Site'),
            'enabled_app'      => t('taxi_tariff', 'Application'),
            'enabled_operator' => t('taxi_tariff', 'Operator'),
            'enabled_bordur'   => t('taxi_tariff', 'Driver (Bordur)'),
            'enabled_cabinet'  => t('taxi_tariff', 'Personal cabinet'),
            'logo'             => t('taxi_tariff', 'Logo'),
            'position_id'      => t('employee', 'Profession'),
            'type'             => t('taxi_tariff', 'Is available'),
            'company_ids'      => t('app', 'Organization'),

            'enabled_hospital' => t('taxi_tariff', 'Hospital'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'common' => [
                'class' => ActiveRecordBehavior::className(),
            ],
            'logo'   => [
                'class'                => CropFileBehavior::className(),
                'fileField'            => ['logo'],
                'upload_dir'           => app()->params['upload'],
                'thumb_width'          => 180,
                'thumb_height'         => 80,
                'small_picture_width'  => 360,
                'small_picture_height' => 160,
                'big_picture_width'    => 360,
                'big_picture_height'   => 160,
            ],
        ];
    }


    public function whenRequiredCompanies($model)
    {
        return in_array($model->type, [static::TYPE_COMPANY, static::TYPE_COMPANY_CORP_BALANCE]);
    }


    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        $cities = $this->cities;
        $city   = reset($cities);
        if ($city) {
            $this->city_id = ArrayHelper::getValue($city, 'city_id');
        }

        $this->company_ids     = ArrayHelper::getColumn($this->companies, 'company_id');
        $this->old_company_ids = $this->company_ids;
    }


    public static function getTypeList()
    {
        return [
            static::TYPE_ALL                  => t('taxi_tariff', self::TYPE_ALL),
            static::TYPE_BASE                 => t('taxi_tariff', self::TYPE_BASE),
            static::TYPE_COMPANY              => t('taxi_tariff', self::TYPE_COMPANY),
            static::TYPE_COMPANY_CORP_BALANCE => t('taxi_tariff', self::TYPE_COMPANY_CORP_BALANCE),
        ];
    }

    public function getIsCompanyRequired()
    {
        return in_array($this->type, [static::TYPE_COMPANY, static::TYPE_COMPANY_CORP_BALANCE]);
    }

    public static function getAutoDownTimeList()
    {
        return [
            1 => t('taxi_tariff', 'Automatically by GPS'),
            0 => t('taxi_tariff', 'Manually via a button'),
        ];
    }


    public function saveForm($params)
    {
        $this->load($params);

        $transaction = app()->db->beginTransaction();

        if (!$this->save()) {
            $transaction->rollBack();

            return false;
        }

        $hasCityModel = new TariffHasCity(['tariff_id' => $this->tariff_id, 'city_id' => $this->city_id]);

        if (!$hasCityModel->save()) {
            $transaction->rollBack();

            return false;
        }

        $modelOptionType = new TariffOptionType([
            'tariff_id' => $this->tariff_id,
            'sort' => 100,
            'type' => TariffOptionType::TYPE_CURRENT
        ]);

        if (!$modelOptionType->save()) {
            $transaction->rollBack();

            return false;
        }

        $cityOption = new TariffOption([
            'type_id'       => $modelOptionType->type_id,
            'accrual'       => TariffOption::ACCRUAL_DISTANCE,
            'area'          => TariffOption::AREA_CITY,
            'next_km_price' => '0',
        ]);
        if (!$cityOption->save(false)) {
            $transaction->rollBack();

            return false;
        }

        $trackOption = new TariffOption([
            'type_id'       => $modelOptionType->type_id,
            'accrual'       => TariffOption::ACCRUAL_DISTANCE,
            'area'          => TariffOption::AREA_TRACK,
            'next_km_price' => '0',
        ]);
        if (!$trackOption->save(false)) {
            $transaction->rollBack();

            return false;
        }

        $this->updateCompanies();
        $this->old_company_ids = $this->company_ids;

        $transaction->commit();

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            TenantHelper::fillHelpItem(TenantHelper::CLIENT_TARIFF_FILL);
        }
    }

    public function updateForm($params)
    {


        $this->load($params);

        $transaction = app()->db->beginTransaction();

        if (!$this->save()) {
            $transaction->rollBack();

            return false;
        }

        if (!$this->isCompanyRequired) {
            $this->company_ids = [];
        }

        $this->updateCompanies();
        $this->old_company_ids = $this->company_ids;

        $transaction->commit();

        return true;
    }

    protected function updateCompanies()
    {
        $companyIds = is_array($this->company_ids) ? $this->company_ids : [];

        $deleteCompanies = array_diff($this->old_company_ids, $companyIds);
        if (!empty($deleteCompanies)) {
            $this->deleteCompanies($deleteCompanies);
        }

        $createCompanies = array_diff($companyIds, $this->old_company_ids);
        if (!empty($createCompanies)) {
            $this->createCompanies($createCompanies);
        }
    }

    protected function deleteCompanies($companies)
    {
        TariffHasCompany::deleteAll(['tariff_id' => $this->tariff_id, 'company_id' => $companies]);
    }

    protected function createCompanies($companies)
    {
        $connection = app()->db;
        $data       = [];
        foreach ($companies as $company) {
            $data[] = [$this->tariff_id, $company];
        }
        $connection->createCommand()->batchInsert(TariffHasCompany::tableName(), ['tariff_id', 'company_id'], $data)->execute();
    }
}