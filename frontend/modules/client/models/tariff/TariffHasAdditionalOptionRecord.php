<?php

namespace frontend\modules\client\models\tariff;


use yii\db\ActiveRecord;

/**
 * Class TariffHasAdditionalOptionRecord
 * @package frontend\modules\client\models\tariff
 *
 * @property integer $id
 * @property integer $type_id
 * @property integer $additional_option_id
 * @property float   $price
 */
class TariffHasAdditionalOptionRecord extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%taxi_tariff_has_additional}}';
    }
}