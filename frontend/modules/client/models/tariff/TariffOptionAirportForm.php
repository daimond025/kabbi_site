<?php

namespace frontend\modules\client\models\tariff;

/**
 * Class TariffOptionAirportForm
 * @package frontend\modules\client\models\tariff
 */

class TariffOptionAirportForm extends BaseTariffOptionAirportOrStationForm
{
    public static function getAreaType()
    {
        return TariffOption::AREA_AIRPORT;
    }


}