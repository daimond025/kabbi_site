<?php

namespace frontend\modules\client\models\tariff;

/**
 * Class Tariff
 * @package app\modules\client\models\tariff
 *
 * @property boolean $isBlocked
 * @property boolean $isEnabledSite
 * @property boolean $isEnabledApp
 * @property boolean $isEnabledOperator
 * @property boolean $isEnabledBordur
 * @property boolean $isEnabledCabinet
 */
class Tariff extends TariffRecord
{
    public function getIsBlocked()
    {
        return (bool)$this->block;
    }

    /**
     * @param boolean $value
     */
    public function setIsBlocked($value)
    {
        $this->block = $value ? 1 : 0;
    }


    public function __get($name)
    {
        $nameArr = explode('isEnabled', $name, 2);

        if (count($nameArr) === 2 && empty(reset($nameArr))) {
            $type = lcfirst(next($nameArr));

            return $this->{'enabled_' . $type};
        }

        return parent::__get($name);
    }

    public function __set($name, $value)
    {
        $nameArr = explode('isEnabled', $name, 2);

        if (count($nameArr) === 2 && empty(reset($nameArr))) {
            $type                       = lcfirst(next($nameArr));
            $this->{'enabled_' . $type} = $value;
        } else {
            parent::__set($name, $value);
        }
    }


    public function getTitle()
    {
        return empty($this->name) ? t('taxi_tariff', 'No name') : $this->name;
    }

    public function getCityId()
    {
        return $this->cities[0]->city_id;
    }
}