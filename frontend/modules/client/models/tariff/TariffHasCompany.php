<?php

namespace frontend\modules\client\models\tariff;

use app\modules\client\models\ClientCompany;
use yii\db\ActiveRecord;

/**
 * Class TariffHasCompany
 * @package frontend\modules\client\models\tariff
 *
 * @property integer       $id
 * @property integer       $tariff_id
 * @property integer       $company_id
 *
 * @property ClientCompany $company
 */
class TariffHasCompany extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%taxi_tariff_has_company}}';
    }

    public function getCompany()
    {
        return $this->hasOne(ClientCompany::className(), ['company_id' => 'company_id']);
    }
}