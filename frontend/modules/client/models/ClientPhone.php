<?php

namespace app\modules\client\models;

use Yii;

/**
 * This is the model class for table "{{%client_phone}}".
 *
 * @property string $phone_id
 * @property string $client_id
 * @property string $value
 *
 * @property Client $client
 * @property OrderChange[] $OrderChanges
 * @property OrderHistory[] $OrderHistories
 */
class ClientPhone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_phone}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'value'], 'required'],
            [['client_id'], 'integer'],
            [['value'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'phone_id' => Yii::t('client', 'Phone ID'),
            'client_id' => Yii::t('client', 'Client ID'),
            'value' => Yii::t('client', 'Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    public function getOrders()
    {
        return $this->hasMany(\app\modules\order\models\Order::className(), ['phone' => 'value']);
    }

    /**
     * Allow to save multiple models
     * @param array $arPhones
     * @param integer $client_id
     * @return boolean
     */
    public static function manySave($arPhones, $client_id)
    {
        if(is_array($arPhones) && !empty($arPhones))
        {
            $insertValue = [];
            $connection = app()->db;
            $tenant_id = user()->tenant_id;

            foreach($arPhones as $phone)
            {
                if(!empty($phone))
                    $insertValue[] = [$client_id, $phone];
            }

            if(!empty($insertValue))
            {
                $connection->createCommand()->batchInsert(self::tableName(), ['client_id', 'value'], $insertValue)->execute();

                return true;
            }
        }

        return false;
    }
}
