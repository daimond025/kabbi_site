<?php

namespace app\modules\client\models;

use app\modules\order\models\Order;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%client_review}}".
 *
 * @property string  $review_id
 * @property string  $client_id
 * @property string  $order_id
 * @property string  $text
 * @property double  $rating
 * @property string  $create_time
 * @property integer $active
 *
 * @property boolean $isActive
 *
 * @property Client  $client
 * @property Order   $order
 */
class ClientReview extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_review}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'order_id'], 'required'],
            [['client_id', 'order_id'], 'integer'],
            [['text'], 'string'],
            [['rating'], 'number'],
            [['create_time'], 'safe'],
            ['active', 'in', 'range' => [0, 1]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'review_id'   => 'Review ID',
            'client_id'   => 'Client ID',
            'order_id'    => 'Order ID',
            'text'        => 'Text',
            'rating'      => 'Rating',
            'create_time' => 'Create Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(\app\modules\order\models\Order::className(), ['order_id' => 'order_id']);
    }

    public function behaviors()
    {
        return [
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_time'],
                ],
            ],
        ];
    }

    public function getIsActive()
    {
        return (bool)$this->active;
    }

    public function setIsActive($value)
    {
        $this->active = $value ? 1 : 0;
    }
}
