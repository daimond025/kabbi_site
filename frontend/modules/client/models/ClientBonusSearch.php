<?php

namespace frontend\modules\client\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * ClientBonusSearch represents the model behind the search form about `frontend\modules\client\models\ClientBonus`.
 */
class ClientBonusSearch extends ClientBonus
{

    private $_accessCityList = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['blocked', 'safe'],
            ['city_id', 'in', 'range' => $this->getAccessCityList(), 'allowArray' => true]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'city_id' => t('user', 'All cities'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param bool  $block
     *
     * @return ActiveQuery
     * @throws \yii\base\InvalidParamException
     */
    public function search($params, $block = false)
    {
        $query = self::find();
        $query->where([
            'tenant_id' => user()->tenant_id,
            'blocked' => (int) $block,
            'city_id' => $this->getAccessCityList(),
        ]);
        $query->orderBy(['name' => SORT_ASC]);
        $query->joinWith('class');
        $query->joinWith('bonusSystem bs');
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $query;
        }

        $query->andWhere(['city_id' => $this->getSearchCityList()]);

        return $query;
    }

    public function getAccessCityList()
    {
        return $this->_accessCityList;
    }

    public function setAccessCityList($value)
    {
        $this->_accessCityList = $value;
    }

    private function getSearchCityList()
    {
        return $this->city_id ? $this->city_id : $this->getAccessCityList();
    }
}
