<?php

namespace app\modules\client\models;

use Yii;

/**
 * This is the model class for table "{{%tbl_client_bonus_client}}".
 *
 * @property string $bonus_id
 * @property string $client_id
 *
 * @property TblClient $client
 * @property TblClientBonus $bonus
 */
class ClientBonusClient extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tbl_client_bonus_client}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bonus_id', 'client_id'], 'required'],
            [['bonus_id', 'client_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bonus_id' => Yii::t('client', 'Bonus ID'),
            'client_id' => Yii::t('client', 'Client ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(TblClient::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonus()
    {
        return $this->hasOne(TblClientBonus::className(), ['bonus_id' => 'bonus_id']);
    }
}
