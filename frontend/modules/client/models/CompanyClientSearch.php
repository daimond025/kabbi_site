<?php

namespace app\modules\client\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * ClientSearch represents the model behind the search form about `frontend\modules\car\models\Car`.
 */
class CompanyClientSearch extends ClientCompany
{
    public $input_search;
    public $sort = 'asc';
    public $sortBy = ['asc' => SORT_ASC, 'desc' => SORT_DESC];
    public $sort_type = 'last_name';
    public $city_id;
    public $company;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sort_type', 'sort', 'input_search', 'city_id', 'company'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params Filter data
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        //Заполнение модели данными из фильтра
        $this->load($params, '');

        $query = Client::find();

        $query->where(['tenant_id' => user()->tenant_id]);

        $query->with([
            'clientPhones',
            'city' => function ($query) {
                $query->select('city_id, name');
            },
        ]);

        //У конкретной компании
        $query->joinWith([
            'clientHasCompanies' => function ($sub_query) {
                $sub_query->andWhere(['company_id' => $this->company]);
            },
        ]);

        //По городу
        $query->andFilterWhere(['IN', 'city_id', $this->city_id]);

        //По телефону и ФИО
        $query->joinWith('clientPhones', false);
        $query->andFilterWhere([
                'or',
                ['like', 'last_name', $this->input_search],
                ['like', 'name', $this->input_search],
                ['like', 'second_name', $this->input_search],
                ['like', 'value', $this->input_search],
            ]
        );
        //---------------------------------------------------------

        //Сортировка
        $query->orderBy([$this->sort_type => $this->sortBy[$this->sort]]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}