<?php

namespace app\modules\client\models;

use common\components\behaviors\ActiveRecordBehavior;
use common\modules\tenant\models\DefaultSettings;
use frontend\modules\client\models\ClientWorkerLike;
use Yii;
use frontend\components\behavior\file\CropFileBehavior;
use common\helpers\DateTimeHelper;
use app\modules\order\models\OrderStatus;
use app\modules\order\models\Order;
use app\modules\balance\models\Account;
use common\modules\city\models\City;
use common\modules\tenant\models\TenantSetting;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%client}}".
 *
 * @property integer             $client_id
 * @property integer             $tenant_id
 * @property string              $last_name
 * @property string              $name
 * @property string              $second_name
 * @property string              $email
 * @property integer             $black_list
 * @property integer             $priority
 * @property string              $create_time
 * @property integer             $active
 * @property integer             $success_order
 * @property integer             $fail_worker_order
 * @property integer             $fail_client_order
 * @property integer             $fail_dispatcher_order
 * @property string              $birth
 * @property integer             $city_id
 * @property string              $photo
 * @property string              $password
 * @property string              $lang
 * @property string              $description
 *
 * @property ClientCompany       $company
 * @property Tenant              $tenant
 * @property ClientBalance[]     $tblClientBalances
 * @property ClientBonusClient[] $tblClientBonusClients
 * @property ClientBonus[]       $bonuses
 * @property ClientPhone[]       $clientPhones
 *
 * @mixin ActiveRecordBehavior
 * @mixin CropFileBehavior
 */
class Client extends ActiveRecord
{
    const SCENARIO_UPLOAD_PHOTO = 'photo';

    public $phone;
    public $cropParams;

    /**
     * Flag
     * @var bool 1|0
     */
    public $new_phone;

    /**
     * Vars for user birth
     */
    public $birth_day;
    public $birth_month;
    public $birth_year;

    const COUNT_SECONDS_FOR_ACTIVITY = 30*24*3600;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client}}';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return ArrayHelper::merge([
            self::SCENARIO_UPLOAD_PHOTO => ['photo', 'cropParams'],
        ], parent::scenarios());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['send_to_email', 'in', 'range' => [0, 1]],
            [['phone'], 'required'],
            [
                ['phone'],
                'filter',
                'filter' => function ($value) {
                    $newValue = [];

                    foreach ($value as $item) {
                        $newValue[] = preg_replace("/[^0-9]/", '', $item);
                    }

                    return $newValue;
                },
            ],
            ['phone', 'uniquePhone'],
            [
                [
                    'tenant_id',
                    'black_list',
                    'priority',
                    'active',
                    'success_order',
                    'fail_worker_order',
                    'fail_client_order',
                    'city_id',
                ],
                'integer',
            ],
            [['new_phone', 'birth_day', 'birth_month', 'birth_year', 'cropParams'], 'safe'],
            [['last_name', 'name', 'second_name'], 'string', 'max' => 45],
            [['email'], 'string', 'max' => 45],
            [['email'], 'email'],
            [
                ['photo'],
                'image',
                'extensions' => ['png', 'jpeg', 'jpg'],
                'maxSize'    => getUploadMaxFileSize(),
                'tooBig'     => t(
                    'file',
                    'The file size must be less than {file_size}Mb',
                    ['file_size' => getUploadMaxFileSize(true)]
                ),
                'minWidth'   => 300,
                'minHeight'  => 300,
            ],
            [['description'], 'string'],
        ];
    }

    public function uniquePhone()
    {
        $query = self::find();

        if ($this->isNewRecord) {
            $query->where(['tenant_id' => user()->tenant_id]);
        } else {
            $query->where(
                'tenant_id = :tenant_id AND ' . self::tableName() . '.client_id != :client_id',
                [':tenant_id' => user()->tenant_id, ':client_id' => $this->client_id]
            );
        }

        $query->joinWith([
            'clientPhones' => function ($sub_query) {
                $sub_query->where(['value' => $this->phone]);
            },
        ]);

        if ($query->exists()) {
            $this->addError('phone', t('validator', 'Client with the phone already exists'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'client_id'         => Yii::t('client', 'Client ID'),
            'tenant_id'         => Yii::t('client', 'Tenant ID'),
            'last_name'         => Yii::t('user', 'Last name'),
            'name'              => Yii::t('user', 'Name'),
            'second_name'       => Yii::t('user', 'Second name'),
            'email'             => Yii::t('client', 'Email'),
            'black_list'        => Yii::t('client', 'Black list'),
            'priority'          => Yii::t('client', 'Priority'),
            'create_time'       => Yii::t('client', 'Create Time'),
            'active'            => Yii::t('client', 'Active'),
            'success_order'     => Yii::t('client', 'Success Order'),
            'fail_worker_order' => Yii::t('client', 'Fail Driver Order'),
            'fail_client_order' => Yii::t('client', 'Fail Client Order'),
            'birth'             => Yii::t('client', 'Birth'),
            'phone'             => Yii::t('client', 'Phone'),
            'city_id'           => Yii::t('client', 'City'),
            'password'          => Yii::t('client', 'Password'),
            'photo'             => Yii::t('client', 'Photo'),
            'description'       => Yii::t('client', 'Description'),
            'send_to_email'     => Yii::t('client', 'Send report about trip to email'),
        ];
    }

    public function getClientWorkerLike()
    {
        return $this->hasMany(ClientWorkerLike::class, ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientHasCompanies()
    {
        return $this->hasMany(ClientHasCompany::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(
            ClientCompany::className(),
            ['company_id' => 'company_id']
        )->viaTable('{{%client_has_company}}', ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(\common\modules\tenant\models\Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * Get client accounts
     * @return \yii\db\ActiveQuery
     */
    public function getAccounts()
    {
        return $this->hasMany(Account::className(), [
            'owner_id'  => 'client_id',
            'tenant_id' => 'tenant_id',
        ])->andOnCondition([
            'acc_kind_id' => Account::CLIENT_KIND,
        ])->andOnCondition([
            'acc_type_id' => Account::PASSIVE_TYPE,
        ]);
    }

    /**
     * Get client bonus accounts
     * @return \yii\db\ActiveQuery
     */
    public function getBonusAccounts()
    {
        return $this->hasMany(Account::className(), [
            'owner_id'  => 'client_id',
            'tenant_id' => 'tenant_id',
        ])->andOnCondition([
            'acc_kind_id' => Account::CLIENT_BONUS_KIND,
        ])->andOnCondition([
            'acc_type_id' => Account::PASSIVE_TYPE,
        ]);
    }

    /**
     * Get client account by currency_id
     * @return Account|null
     */
    public function getAccountByCurrencyId($currencyId)
    {
        return Account::findone([
            'owner_id'    => $this->client_id,
            'tenant_id'   => $this->tenant_id,
            'acc_kind_id' => Account::CLIENT_KIND,
            'acc_type_id' => Account::PASSIVE_TYPE,
            'currency_id' => $currencyId,
        ]);
    }

    /**
     * Get client bonus account by currency_id
     * @return Account|null
     */
    public function getBonusAccountByCurrencyId($currencyId)
    {
        return Account::findone([
            'owner_id'    => $this->client_id,
            'tenant_id'   => $this->tenant_id,
            'acc_kind_id' => Account::CLIENT_BONUS_KIND,
            'acc_type_id' => Account::PASSIVE_TYPE,
            'currency_id' => $currencyId,
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonus()
    {
        return $this->hasMany(ClientBonusClient::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientHasBonuses()
    {
        return $this->hasMany(ClientHasBonus::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonuses()
    {
        return $this->hasMany(ClientBonus::className(), ['bonus_id' => 'bonus_id'])->viaTable(
            '{{%client_has_bonus}}',
            ['client_id' => 'client_id']
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientPhones()
    {
        return $this->hasMany(ClientPhone::className(), ['client_id' => 'client_id']);
    }

    public function getClientPhone()
    {
        return $this->hasOne(ClientPhone::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(ClientReview::className(), ['client_id' => 'client_id']);
    }

    public function getActive()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviewRaiting()
    {
        return $this->hasOne(ClientReviewRaiting::className(), ['client_id' => 'client_id']);
    }

    public static function getActiveFieldData()
    {
        return [
            1 => t('app', 'Yes'),
            0 => t('app', 'No'),
        ];
    }

    public function getActiveFieldValue()
    {
        if ($this->black_list) {
            return t('client', 'Black list');
        }

        $lastOrderCreateTime = (int)Order::find()
            ->select('create_time')
            ->where([
                'tenant_id' => user()->tenant_id,
                'client_id' => $this->client_id,
            ])
            ->orderBy(['create_time' => SORT_DESC])
            ->limit(1)
            ->scalar();

        return $lastOrderCreateTime > time() - self::COUNT_SECONDS_FOR_ACTIVITY
            ? t('app', 'Yes') : t('app', 'No');
    }

    public static function getActiveValueForClientBaseExport($client)
    {
        if ($client['black_list']) {
            return t('client', 'Black list');
        }

        $lastOrderCreateTime = (int)Order::find()
            ->select('create_time')
            ->where([
                'tenant_id' => user()->tenant_id,
                'client_id' => $client['client_id'],
            ])
            ->orderBy(['create_time' => SORT_DESC])
            ->limit(1)
            ->scalar();

        return $lastOrderCreateTime > time() - self::COUNT_SECONDS_FOR_ACTIVITY
            ? t('app', 'Yes') : t('app', 'No');
    }

    public static function getdataForm()
    {
        $dataForm = [];

        //Дата рождения

        //День
        $dataForm['BIRTHDAY']['DAY'] = DateTimeHelper::getMonthDayNumbers();
        $dataForm['BIRTHDAY']['DAY'] = [0 => t('app', 'Not selected')] + $dataForm['BIRTHDAY']['DAY'];

        //Месяц
        $dataForm['BIRTHDAY']['MONTH'] = DateTimeHelper::getMonthList();

        //Год
        $dataForm['BIRTHDAY']['YEAR'] = DateTimeHelper::getOldYearList();

        //Города пользователя
        $dataForm['CITY'] = user()->getUserCityList();

        return $dataForm;
    }

    public function behaviors()
    {
        return [
            'common'       => [
                'class' => ActiveRecordBehavior::className(),
            ],
            'fileBehavior' => [
                'class'      => CropFileBehavior::className(),
                'fileField'  => ['photo'],
                'upload_dir' => app()->params['upload'],
            ],
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($this->scenario == self::SCENARIO_UPLOAD_PHOTO) {
            return false;
        }

        if (!$insert) {
            $this->updatePhones();
        }

        $this->updateClientListCache();
        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->password = rand(100000, 999999);
        }

        if (!empty($this->birth_year) && !empty($this->birth_month) && !empty($this->birth_day)) {
            $this->birth = $this->birth_year . '-' . $this->birth_month . '-' . $this->birth_day;
        } else {
            $this->birth = null;
        }

        if ($insert) {
            $this->lang = TenantSetting::getSettingValue(
                user()->tenant_id,
                DefaultSettings::SETTING_LANGUAGE,
                $this->city_id
            );
        }

        return parent::beforeSave($insert);
    }

    public function getFullName()
    {
        return trim($this->last_name . ' ' . $this->name . ' ' . $this->second_name);
    }

    public function getShortName()
    {
        $name = $this->last_name;

        if (!empty($this->name)) {
            $name .= ' ' . mb_substr($this->name, 0, 1, "UTF-8") . '.';
        }

        if (!empty($this->second_name)) {
            $name .= mb_substr($this->second_name, 0, 1, "UTF-8") . '.';
        }

        return $name;
    }

    /**
     * Getting client orders with limit 10.
     *
     * @param integer $id Client id
     *
     * @return \app\modules\client\models\Client
     */
    public static function getClientOrdersByLimit($id)
    {
        $arStatuses = array_merge(OrderStatus::getCompletedStatusId(), OrderStatus::getRejectedStatusId());

        return Order::find()
            ->where([
                'client_id' => $id,
                'tenant_id' => user()->tenant_id,
                'status_id' => $arStatuses,
            ])
            ->with([
                'clientReview',
                'company'    => function ($query) {
                    $query->select(['company_id', 'name']);
                },
                'status',
                'detailCost' => function ($query) {
                    $query->select(['detail_id', 'order_id', 'summary_cost']);
                },
                'client'     => function ($query) {
                    $query->select(['client_id', 'last_name', 'name', 'second_name']);
                },
            ])
            ->limit(10)
            ->orderBy(['order_id' => SORT_DESC])
            ->all();
    }

    /**
     * Getting client orders by period.
     *
     * @param integer $id Client id
     * @param string  $first_date First date of period
     * @param string  $second_date Second date of period
     *
     * @return \app\modules\client\models\Client
     */
    public static function getClientOrdersByPeriod($id, $first_date, $second_date)
    {
        $first_date  = date('d.m.Y', strtotime($first_date));
        $second_date = date('d.m.Y', strtotime($second_date));
        $arFirstDay  = explode('.', $first_date);
        $arSecondDay = explode('.', $second_date);

        $arStatuses = array_merge(OrderStatus::getCompletedStatusId(), OrderStatus::getRejectedStatusId());

        return Order::find()
            ->where([
                'client_id' => $id,
                'tenant_id' => user()->tenant_id,
                'status_id' => $arStatuses,
            ])
            ->andWhere('`create_time` >= ' . mktime(
                0,
                0,
                0,
                $arFirstDay[1],
                $arFirstDay[0],
                    $arFirstDay[2]
            ) . ' AND `create_time` < ' . mktime(
                        0,
                        0,
                        0,
                        $arSecondDay[1],
                        $arSecondDay[0] + 1,
                    $arSecondDay[2]
                    ))
            ->with([
                'clientReview',
                'company'    => function ($query) {
                    $query->select(['company_id', 'name']);
                },
                'status',
                'detailCost' => function ($query) {
                    $query->select(['detail_id', 'order_id', 'summary_cost']);
                },
                'client'     => function ($query) {
                    $query->select(['client_id', 'last_name', 'name', 'second_name']);
                },
            ])
            ->orderBy(['order_id' => SORT_DESC])
            ->all();
    }

    public static function getOrganizationList($client_id, $sort_type = 'name', $sort = 'asc')
    {
        $sortBy = ['asc' => SORT_ASC, 'desc' => SORT_DESC];

        return self::find()
            ->with([
                'companies'                    => function ($query) use ($sort_type, $sortBy, $sort) {
                    $query->orderBy([$sort_type => $sortBy[$sort]]);
                },
                'companies.clientHasCompanies' => function ($query) use ($client_id) {
                    $query->where(['client_id' => $client_id]);
                },
                'companies.city'               => function ($query) {
                    $query->select([City::tableName() . '.name' . getLanguagePrefix(), City::tableName() . '.city_id']);
                },
            ])
            ->where(['client_id' => $client_id])
            ->one();
    }

    /**
     * Allow to add new client by phone.
     *
     * @param string $phone
     *
     * @return boolean|array ['CLIENT_ID' => client_id, 'PHONE_ID' => phone_id]
     */
    public function addNewClientByPhone($phone)
    {
        $this->phone[] = $phone;

        if ($this->save()) {
            $clientPhone            = new ClientPhone;
            $clientPhone->value     = $this->phone[0];
            $clientPhone->client_id = $this->client_id;

            if ($clientPhone->save()) {
                return ['CLIENT_ID' => $this->client_id, 'PHONE_ID' => $clientPhone->phone_id];
            }

            Yii::error('Ошибка сохранение телефона клиента.', 'order');
        }

        Yii::error($this->getErrors(), 'order');

        return false;
    }

    public static function findByPhoneId($phone_id, $select = [])
    {
        $clientPhone = ClientPhone::find()
            ->where(['phone_id' => $phone_id])
            ->with([
                'client' => function ($query) use ($select) {
                    $query->select($select);
                },
            ])
            ->one();

        return $clientPhone->client;
    }

    public function afterDelete()
    {
        $this->updateClientListCache();
        parent::afterDelete();
    }

    public static function find()
    {
        return new ClientQuery(get_called_class());
    }

    public function isAdmin($company_id)
    {
        foreach ($this->clientHasCompanies as $company) {
            if ($company->company_id != $company_id) {
                continue;
            }
            if ($company->role == ClientHasCompany::ROLE_JURIDICAL) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return int
     */
    public function getFailOrderCount()
    {
        return $this->fail_worker_order + $this->fail_client_order + $this->fail_dispatcher_order;
    }

    private function updateClientListCache()
    {
        app()->cache->delete(domainName() . 'base_client_list');
        app()->cache->delete(app()->user->getId() . 'query');
    }

    private function updatePhones()
    {
        $old_phones = ArrayHelper::getColumn($this->clientPhones, 'value');
        $diff_phones = array_diff($this->phone, $old_phones);

        if (!empty($diff_phones)) {
            ClientPhone::deleteAll(['client_id' => $this->client_id]);
            ClientPhone::manySave($this->phone, $this->client_id);
        }
    }
}
