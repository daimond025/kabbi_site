<?php

namespace app\modules\client\models;

use Yii;

/**
 * This is the model class for table "{{%client_has_company}}".
 *
 * @property string $client_id
 * @property string $company_id
 * @property integer $booking
 * @property integer $driving
 *
 * @property Client $client
 * @property ClientCompany $company
 */
class ClientHasCompany extends \yii\db\ActiveRecord
{

    const ROLE_JURIDICAL = 'juridical'; // юр.лицо
    const ROLE_PHYSICAL = 'physical';  // физ.лицо
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_has_company}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'company_id'], 'required'],
            [['client_id', 'company_id', 'booking', 'driving'], 'integer'],
            [['role'], 'in', 'range' => [self::ROLE_JURIDICAL, self::ROLE_PHYSICAL]]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'client_id' => 'Client ID',
            'company_id' => 'Company ID',
            'booking' => 'Booking',
            'driving' => 'Driving',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(ClientCompany::className(), ['company_id' => 'company_id']);
    }
}
