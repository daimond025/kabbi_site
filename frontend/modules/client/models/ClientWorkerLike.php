<?php

namespace frontend\modules\client\models;

use yii\db\ActiveRecord;

/**
 * Class ClientWorkerLike
 * @package frontend\modules\client\models
 *
 * @property integer $id
 * @property integer $client_id
 * @property integer $worker_id
 * @property integer $position_id
 * @property integer $like_count
 * @property integer $dislike_count
 */
class ClientWorkerLike extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_worker_like}}';
    }
}
