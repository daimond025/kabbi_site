<?php

namespace app\modules\client\models;

use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use yii\data\ActiveDataProvider;

/**
 * ClientSearch represents the model behind the search form about `frontend\modules\car\models\Car`.
 */
class OrderClientSearch extends Order
{

    const PAGE_SIZE = 50;
    const LIMIT = 10;

    public $type;
    public $first_date;
    public $second_date;
    public $formName = null;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'in', 'range' => ['today', 'period']],
            [['first_date', 'second_date'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params Filter data
     *
     * @return ActiveDataProvider
     */
    public function search($params/*, $isNextPage = false*/)
    {
        $arStatuses = array_merge(OrderStatus::getCompletedStatusId(), OrderStatus::getRejectedStatusId());

        $query = self::find()
            ->alias('o')
            ->where([
                'o.tenant_id' => user()->tenant_id,
                'o.client_id' => $this->client_id,
                'o.status_id' => $arStatuses,
            ])
            ->joinWith([
                'client cl',
                'worker w',
                'car car',
                'status st',
                'detailCost dc',
                'clientReview rvw',
            ])
            ->orderBy(['order_id' => SORT_DESC])
            ->groupBy('order_id');


        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'sort'       => [
                'defaultOrder' => [
                    'order_id' => SORT_DESC,
                ],
                'attributes'   => [],
            ],
            'pagination' => [
                'pageSize' => $this->type === 'limit' ? self::LIMIT : false,
            ],
        ]);


        $this->load($params, $this->formName);

        if (!$this->validate()) {
            return $dataProvider;
        }

        switch ($this->type) {
            case 'period':
                $first_date  = (int)app()->formatter->asTimestamp($this->first_date);
                $second_date = (int)app()->formatter->asTimestamp($this->second_date) + 24 * 60 * 60;
                $query->andWhere(['between', 'o.create_time', $first_date, $second_date]);
                break;
            case 'limit':
            default:
                $query->limit(self::LIMIT);
        }


        if (isset($this->order_id)) {
            $query->where(['o.order_id' => $this->order_id]);
        }


        return $dataProvider;
    }

}
