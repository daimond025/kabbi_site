<?php

return [
    'components' => [

        'clientBonusService' => 'frontend\modules\client\components\ClientBonusService',

        'tariffService' => [
            'class' => 'frontend\modules\client\components\TariffService',
        ],

        'parkingService' => [
            'class' => 'frontend\modules\client\components\ParkingService',
        ],

        'companyService' => [
            'class' => 'frontend\modules\client\components\CompanyService',
        ],

        'positionService' => [
            'class' => 'frontend\modules\employee\components\position\PositionService',
        ],

        'carClassService' => [
            'class' => 'frontend\modules\car\components\CarClassService'
        ],
    ],

    'modules' => [
        'cabinet' => 'frontend\modules\client\modules\cabinet\Module',
    ],
];