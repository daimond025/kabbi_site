<?php

namespace app\modules\client\controllers;

use common\modules\tenant\models\TenantSetting;
use frontend\components\behavior\CityListBehavior;
use frontend\modules\car\components\CarClassService;
use frontend\modules\client\components\CompanyService;
use frontend\modules\client\components\ParkingService;
use frontend\modules\client\components\TariffService;
use frontend\modules\client\models\tariff\TariffAdditionalOptionForm;
use frontend\modules\client\models\tariff\TariffForm;
use frontend\modules\client\models\tariff\TariffHasFixForm;
use frontend\modules\client\models\tariff\TariffOptionAirportForm;
use frontend\modules\client\models\tariff\TariffOptionForm;
use frontend\modules\client\models\tariff\TariffOptionRecord;
use frontend\modules\client\models\tariff\TariffOptionStationForm;
use frontend\modules\client\models\tariff\TariffOptionType;
use frontend\modules\employee\components\position\PositionService;
use frontend\modules\tenant\models\Currency;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class TariffController
 * @package app\modules\client\controllers
 *
 * @mixin CityListBehavior
 */
class TariffController extends Controller
{
    public function behaviors()
    {
        return [
            'access'   => [
                'class'  => AccessControl::className(),
                'except' => [],
                'rules'  => [
                    [
                        'actions' => ['list', 'update', 'update-option', 'get-fix', 'get-fix-form'],
                        'allow'   => true,
                        'roles'   => ['read_clientTariff'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['clientTariff'],
                    ],
                ],
            ],
            'cityList' => [
                'class' => CityListBehavior::className(),
            ],
        ];
    }

    public function actionList()
    {
        return 'actionList';
    }

    public function actionListBlocked()
    {
        return 'actionListBlocked';
    }

    public function actionCreate()
    {
        /** @var TariffService $tariffService */
        $tariffService = app()->getModule('client')->get('tariffService');

        /** @var PositionService $positionService */
        $positionService = app()->getModule('client')->get('positionService');
        $positionService->setTenantId(user()->tenant_id);

        /** @var CarClassService $carClassService */
        $carClassService = app()->getModule('client')->get('carClassService');

        $tariff            = new TariffForm();
        $tariff->tenant_id = user()->tenant_id;
        $tariff->scenario  = TariffForm::SCENARIO_CREATE;

        $cityList         = $this->getUserCityList();
        $positionList     = $positionService->getPositionMapByCity($tariff->city_id);
        $companyList      = [];
        $typeList         = TariffForm::getTypeList();
        $autoDownTimeList = TariffForm::getAutoDownTimeList();
        $carPositionList  = $positionService->getPositionIdsHasCar();
        $carClassMap      = $carClassService->getCarClassMap();
        $groupList        = $tariffService->getGroupList(user()->tenant_id);

        if (app()->request->isPost && $tariff->saveForm(post())) {
            return $this->redirect(['/client/tariff/update', 'id' => $tariff->tariff_id]);
        } else {
            $tariff->isNewRecord = true;
        }

        return $this->render('create', compact('tariff', 'cityList', 'positionList', 'groupList',
            'typeList', 'autoDownTimeList', 'companyList', 'carPositionList', 'carClassMap'));
    }

    public function actionUpdate($id)
    {
        $tariffId = $id;

        /** @var TariffService $tariffService */
        $tariffService = app()->getModule('client')->get('tariffService');

        /** @var CompanyService $companyService */
        $companyService = app()->getModule('client')->get('companyService');

        /** @var PositionService $positionService */
        $positionService = app()->getModule('client')->get('positionService');
        $positionService->setTenantId(user()->tenant_id);

        /** @var CarClassService $carClassService */
        $carClassService = app()->getModule('client')->get('carClassService');

        $tariff = $tariffService->getTariffForm(user()->tenant_id, $tariffId);
        //        $tariff->scenario = TariffForm::SCENARIO_UPDATE;

        if (!$tariff || !ArrayHelper::isIn($tariff->getCityId(), $this->getUserCityIds())) {
            return $this->redirect(['/client/tariff/list']);
        }

        $readOnly         = !app()->user->can('clientTariff');
        $cityList         = $this->getUserCityList();
        $positionList     = $positionService->getPositionMapByCity($tariff->city_id);
        $companyList      = $companyService->getCompanyList(user()->tenant_id, $tariff->city_id);
        $typeList         = TariffForm::getTypeList();
        $autoDownTimeList = TariffForm::getAutoDownTimeList();
        $optionTypeList   = $tariffService->getOptionTypeList($tariff->tariff_id);
        $carPositionList  = $positionService->getPositionIdsHasCar();
        $carClassMap      = $carClassService->getCarClassMap();
        $groupList        = $tariffService->getGroupList(user()->tenant_id);

        if (app()->request->isPost && $tariff->updateForm(post())) {
            session()->setFlash('success', t('app', 'Data updated successfully'));
        }


        return $this->render('update',
            compact('tariff', 'readOnly', 'cityList', 'positionList', 'groupList', 'typeList', 'autoDownTimeList',
                'optionTypeList', 'companyList', 'carPositionList', 'carClassMap'));
    }


    public function actionGetCompanyList()
    {
        if (!app()->request->isAjax) {
            return false;
        }

        app()->response->format = Response::FORMAT_JSON;

        $cityId = post('city_id');

        /** @var CompanyService $companyService */
        $companyService = app()->getModule('client')->get('companyService');

        $companyList = $companyService->getCompanyList(user()->tenant_id, $cityId);

        return array_map(function ($item) {
            return Html::encode($item);
        }, $companyList);
    }

    public function actionUploadLogo($id)
    {
        $tariffId = $id;

        app()->response->format = Response::FORMAT_JSON;

        if (!app()->request->isAjax) {
            return false;
        }

        /** @var TariffService $tariffService */
        $tariffService = app()->getModule('client')->get('tariffService');

        $tariff           = $tariffService->getTariffForm(user()->tenant_id, $tariffId);
        $tariff->scenario = TariffForm::SCENARIO_UPLOAD_LOGO;

        if (!$tariff || !ArrayHelper::isIn($tariff->getCityId(), $this->getUserCityIds())) {
            return false;
        }

        $tariff->load(post());

        if ($tariff->save(true, ['logo'])) {
            return [
                'src' => [
                    'big'   => $tariff->getPicturePath($tariff->logo),
                    'thumb' => $tariff->getPicturePath($tariff->logo, false),
                ],
            ];
        }

        return false;
    }

    public function actionUpdateOption($typeId)
    {

        $model = TariffOptionForm::find($typeId, user()->tenant_id);

        if (!$model) {
            return $this->redirect(['/client/tariff/list']);
        }

        /** @var TariffService $tariffService */
        $tariffService = app()->getModule('client')->get('tariffService');
        $tariff        = $tariffService->getTariffForm(user()->tenant_id, $model->optionType->tariff_id);
        if (!$tariff || !ArrayHelper::isIn($tariff->getCityId(), $this->getUserCityIds())) {
            return $this->redirect(['/client/tariff/list']);
        }

       /* if (!app()->request->isAjax) {
            return $this->redirect(['/client/tariff/update', 'id' => $model->optionType->tariff_id]);
        }*/

        $cityId = $model->optionType->tariff->cities[0]->city_id;

        /** @var ParkingService $parkingService */
        $parkingService = app()->getModule('client')->get('parkingService');
        $parkingList    = $parkingService->getParkingList($cityId);

        $readOnly = !app()->user->can('clientTariff');

        $currencyId = TenantSetting::getSettingValue(user()->tenant_id, 'CURRENCY', $cityId);
        $currency = Currency::getCurrencySymbol($currencyId);

        $additionalModel = TariffAdditionalOptionForm::find(user()->tenant_id, $cityId, $typeId);

        $airportModel = TariffOptionAirportForm::find($typeId);
        $stationModel = TariffOptionStationForm::find($typeId);

        if (app()->request->isAjax && !$readOnly) {

            if ($model->load(post())) {
                $model->save();
            }

            if ($additionalModel->load(post())) {
                $additionalModel->save();
            }


            if ($airportModel->load(post())) {
                $airportModel->save();
            }

            if ($stationModel->load(post())) {
                $stationModel->save();
            }
        }

        return $this->renderAjax('_option',
            compact('model', 'additionalModel', 'airportModel', 'stationModel', 'parkingList', 'readOnly', 'currency'));
    }

    public function actionCreateOption($id)
    {
        if (!app()->request->isAjax) {
            return $this->redirect(['/client/tariff/update', 'id' => $id]);
        }

        /** @var TariffService $tariffService */
        $tariffService = app()->getModule('client')->get('tariffService');
        $tariff        = $tariffService->getTariffForm(user()->tenant_id, $id);
        if (!$tariff || !ArrayHelper::isIn($tariff->getCityId(), $this->getUserCityIds())) {
            return $this->redirect(['/client/tariff/list']);
        }

        $model                        = new TariffOptionForm();
        $model->optionType->tariff_id = $id;
        $model->city_accrual          = TariffOptionRecord::ACCRUAL_DISTANCE;
        $model->track_accrual         = TariffOptionRecord::ACCRUAL_DISTANCE;

        $cityId     = $model->optionType->tariff->cities[0]->city_id;
        $currencyId = TenantSetting::getSettingValue(user()->tenant_id, 'CURRENCY', $cityId);
        $currency   = Currency::getCurrencySymbol($currencyId);


        if (app()->request->isAjax) {
            if ($model->load(post())) {
                $model->createForm();
            }
        }

        return $this->renderAjax('_option_create', compact('model', 'currency'));
    }

    public function actionDeleteOption($id)
    {
        $model = TariffOptionForm::find($id, user()->tenant_id);

        if (!$model) {
            return $this->redirect(['/client/tariff/list']);
        }

        /** @var TariffService $tariffService */
        $tariffService = app()->getModule('client')->get('tariffService');
        $tariff        = $tariffService->getTariffForm(user()->tenant_id, $model->optionType->tariff_id);
        if (!$tariff || !ArrayHelper::isIn($tariff->getCityId(), $this->getUserCityIds())) {
            return $this->redirect(['/client/tariff/list']);
        }

        if ($model->deleteForm()) {
            session()->setFlash('success', t('app', 'Data updated successfully'));
        }

        return $this->redirect(['/client/tariff/update', 'id' => $tariff->tariff_id]);
    }

    public function actionGetFix($id, $typeId)
    {
        $tariffType = TariffOptionType::find()
            ->joinWith('tariff')
            ->where([
                'type_id'   => $typeId,
                'tenant_id' => user()->tenant_id,
            ])
            ->one();

        if (!app()->request->isAjax || !$tariffType) {
            return $this->redirect(['client/tariff/list']);
        }

        /** @var TariffService $tariffService */
        $tariffService = app()->getModule('client')->get('tariffService');
        $tariff        = $tariffService->getTariffForm(user()->tenant_id, $tariffType->tariff->tariff_id);
        if (!$tariff || !ArrayHelper::isIn($tariff->getCityId(), $this->getUserCityIds())) {
            return $this->redirect(['/client/tariff/list']);
        }

        $fixModel = TariffHasFixForm::find($id, $typeId);

        $readOnly = !app()->user->can('clientTariff');

        $cityId = $tariff->getCityId();

        $currencyId = TenantSetting::getSettingValue(user()->tenant_id, 'CURRENCY', $cityId);
        $currency   = Currency::getCurrencySymbol($currencyId);

        $view = $readOnly ? '_fix_form_read' : '_fix_form';

        return $this->renderAjax($view, compact('fixModel', 'currency'));
    }

    public function actionSaveFixForm($id, $typeId)
    {
        app()->response->format = Response::FORMAT_JSON;

        $readOnly   = !app()->user->can('clientTariff');
        $tariffType = TariffOptionType::find()
            ->joinWith('tariff')
            ->where([
                'type_id'   => $typeId,
                'tenant_id' => user()->tenant_id,
            ])
            ->one();


        if (!app()->request->isAjax || !$tariffType || $readOnly) {
            return $this->redirect(['client/tariff/list']);
        }

        /** @var TariffService $tariffService */
        $tariffService = app()->getModule('client')->get('tariffService');
        $tariff        = $tariffService->getTariffForm(user()->tenant_id, $tariffType->tariff->tariff_id);
        if (!$tariff || !ArrayHelper::isIn($tariff->getCityId(), $this->getUserCityIds())) {
            return $this->redirect(['/client/tariff/list']);
        }

        $fixModel = TariffHasFixForm::find($id, $typeId);
        $fixModel->loadPriceList(post());

        if ($fixModel->save()) {
            return ['status' => 1, 'message' => t('app', 'Data updated successfully')];
        }

        return ['status' => 0, 'message' => t('app', 'Save error')];
    }
}

