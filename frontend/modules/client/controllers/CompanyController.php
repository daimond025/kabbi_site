<?php

namespace app\modules\client\controllers;

use app\modules\client\models\Client;
use app\modules\client\models\ClientHasCompany;
use app\modules\client\models\ClientPhone;
use app\modules\client\models\ClientSearch;
use app\modules\client\models\CompanyClientSearch;
use app\modules\client\models\OrderClientCompanySearch;
use common\modules\tenant\models\DefaultSettings;
use common\modules\tenant\models\TenantSetting;
use frontend\modules\tenant\models\Currency;
use frontend\widgets\ExcelExport;
use app\modules\order\models\Order;
use Yii;
use yii\filters\AccessControl;
use app\modules\client\models\ClientCompany;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use \yii\helpers\ArrayHelper;
use app\modules\client\models\CompanySearch;
use frontend\components\behavior\CityListBehavior;
use yii\web\Response;

/**
 * ClientCompanyController implements the CRUD actions for ClientCompany model.
 * @mixin CityListBehavior
 */
class CompanyController extends Controller
{

    public function behaviors()
    {
        return [
            'access'   => [
                'class'  => AccessControl::className(),
                'except' => [],
                'rules'  => [
                    [
                        'actions' => [
                            'list',
                            'search',
                            'update',
                            'stuff',
                            'orders',
                            'staff-add-search',
                            'staff-search',
                            'order-search',
                        ],
                        'allow'   => true,
                        'roles'   => ['read_organization'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['organization'],
                    ],
                ],
            ],
            'cityList' => [
                'class' => CityListBehavior::className(),
            ],
        ];
    }

    private function getListData($searchParams)
    {
        $cityList = $this->getUserCityList();

        $searchModel                 = new CompanySearch();
        $searchModel->accessCityList = array_keys($cityList);
        $dataProvider                = $searchModel->search($searchParams);
        $pjaxId                      = 'company';

        return compact('searchModel', 'dataProvider', 'cityList', 'pjaxId');
    }

    public function actionList()
    {
        return $this->render('index', $this->getListData(Yii::$app->request->queryParams));
    }

    /**
     * Creates a new ClientCompany model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $company = new ClientCompany();
        $post    = Yii::$app->request->post();
        if ($company->load($post)) {
            $transaction                  = app()->db->beginTransaction();
            $company->contract_start_date = $company->getDateFieldFormat($post['ClientCompany']['contract_start_day'],
                $post['ClientCompany']['contract_start_month'], $post['ClientCompany']['contract_start_year']);
            $company->contract_end_date   = $company->getDateFieldFormat($post['ClientCompany']['contract_end_day'],
                $post['ClientCompany']['contract_end_month'], $post['ClientCompany']['contract_end_year']);

            if ($company->save()) {
                try {
                    session()->setFlash('success', t('client', 'The organization was successfully added.'));
                    $transaction->commit();

                    return $this->redirect('list');
                } catch (\yii\db\Exception $ex) {
                    session()->setFlash('error',
                        t('client', 'Error saving organization. Notify to administrator, please.'));
                }
            } else {
                displayErrors([$company]);
            }

            $transaction->rollback();
        }
        $currency = '';

        return $this->render('create', [
            'company'        => $company,
            'user_city_list' => user()->getUserCityList(),
            'dataForm'       => $company->getFormParams(),
            'currency'       => $currency,
        ]);
    }

    public function actionUploadLogo($entity_id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $entity = $this->findOne($entity_id);
        $entity->load(post());

        if ($entity->save(true, ['logo'])) {
            return [
                'src' => [
                    'big'   => $entity->getPicturePath($entity->logo),
                    'thumb' => $entity->getPicturePath($entity->logo, false),
                ],
            ];
        }

        return $entity->getFirstErrors();
    }

    protected function findOne($entity_id)
    {
        return ClientCompany::find()
            ->where([
                'company_id' => $entity_id,
                'tenant_id'  => user()->tenant_id,
            ])
            ->one();
    }

    /**
     * Updates an existing ClientCompany model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param string $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $company = ClientCompany::find()
            ->where([
                'company_id' => $id,
                'tenant_id'  => user()->tenant_id,
            ])
            ->with([
                'city',
            ])
            ->one();

        if (!$company) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $post = Yii::$app->request->post();
        if ($company->load($post)) {
            $company->contract_start_date = $company->getDateFieldFormat($post['ClientCompany']['contract_start_day'],
                $post['ClientCompany']['contract_start_month'], $post['ClientCompany']['contract_start_year']);
            $company->contract_end_date   = $company->getDateFieldFormat($post['ClientCompany']['contract_end_day'],
                $post['ClientCompany']['contract_end_month'], $post['ClientCompany']['contract_end_year']);
            if ($company->save()) {
                try {
                    session()->setFlash('success', t('app', 'Data updated successfully'));

                    return $this->refresh();
                } catch (\yii\db\Exception $ex) {
                    session()->setFlash('error',
                        t('client', 'Error saving organization. Notify to administrator, please.'));
                }
            } else {
                displayErrors([$company]);
            }
        }

        list($company->contract_start_year, $company->contract_start_month, $company->contract_start_day) = !empty($company->contract_start_date) ? explode('-',
            $company->contract_start_date) : null;
        list($company->contract_end_year, $company->contract_end_month, $company->contract_end_day) = !empty($company->contract_end_date) ? explode('-',
            $company->contract_end_date) : null;

        $currencyId = TenantSetting::getSettingValue(user()->tenant_id, DefaultSettings::SETTING_CURRENCY,
            $company->city_id);
        $currency   = Currency::getCurrencySymbol($currencyId);

        return $this->render('update', [
            'company'        => $company,
            'user_city_list' => user()->getUserCityList(),
            'dataForm'       => $company->getFormParams(),
            'currency'       => $currency,
        ]);
    }

    /**
     * Ajax company client seacrh from filter data
     *
     * @param integer $company
     *
     * @return html
     */
    public function actionStaffSearch($company)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $clients = $this->stuffSearch();

        return $this->renderAjax('_stuff', ['clients' => $clients, 'company_id' => $company]);
    }

    /**
     * Ajax company client seacrh for add
     * @return mixed
     */
    public function actionStaffAddSearch()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $search_model    = new ClientSearch([
            'formName'       => '',
            'accessCityList' => $this->getUserCityIds(),
        ]);
        $view            = post('view', '_stuff_add_table');
        $clients         = $search_model->search(Yii::$app->request->post())->getModels();
        $company_clients = ClientHasCompany::find()->where(['company_id' => post('company_id')])->all();
        $company_clients = ArrayHelper::getColumn($company_clients, 'client_id');

        return $this->renderAjax($view, [
            'clients'         => $clients,
            'company_clients' => $company_clients,
        ]);
    }

    /**
     * Company stuff list.
     *
     * @param integer $id Company id
     *
     * @return mixed
     */
    public function actionStuff($id)
    {
        return $this->renderAjax(post('view', 'stuff'), [
            'clients'        => ClientCompany::getStuffList($id),
            'user_city_list' => user()->getUserCityList(),
            'id'             => $id,
        ]);
    }

    /**
     * Add client to organization
     *
     * @param integer $client_id Client id
     * @param integer $company_id Company id
     *
     * @return mixed
     */
    public function actionStuffAdd($client_id, $company_id)
    {
        $clientHasCompany = new ClientHasCompany();

        $clientHasCompany->client_id  = $client_id;
        $clientHasCompany->company_id = $company_id;
        $clientHasCompany->save();

        return $this->renderAjax(get('view', '_stuff'), [
            'clients'        => ClientCompany::getStuffList($company_id),
            'user_city_list' => user()->getUserCityList(),
            'id'             => $company_id,
        ]);
    }

    public function actionAddNewClient()
    {
        $client = new Client;

        if ($client->load(Yii::$app->request->post())) {
            $transaction = app()->db->beginTransaction();

            if ($client->save()) {
                ClientPhone::manySave($client->phone, $client->client_id);

                $clientHasCompany             = new ClientHasCompany();
                $clientHasCompany->company_id = get('company_id');
                $client->link('clientHasCompanies', $clientHasCompany);

                $transaction->commit();

                session()->setFlash('success', t('client', 'The client was successfully added.'));

                return $this->redirect(['update', 'id' => $clientHasCompany->company_id]);
            }

            $transaction->rollBack();
        }

        $client->phone = post('phone');

        return $this->renderAjax('/base/_form', [
            'client'   => $client,
            'dataForm' => Client::getdataForm(),
            'jsInit'   => true,
            'withLogo' => false,
            'ajax'     => true,
            'target'   => 'client_form',
        ]);
    }

    /**
     * Remove client from organization.
     *
     * @param integer $client Client id
     * @param integer $company Company id
     *
     * @return json
     */
    public function actionStuffRemove($client, $company)
    {
        $client_has_company = ClientHasCompany::find()
            ->where(['client_id' => $client, 'company_id' => $company])
            ->one();

        $json = !is_null($client_has_company) && $client_has_company->delete() ? 1 : 0;

        return json_encode($json);
    }

    /**
     * Change taxi client rights.
     *
     * @param integer $client Client id
     * @param integer $company Company id
     * @param integer $value (0|1)
     * @param string  $type Type of rights (booking|driving)
     *
     * @return json
     */
    public function actionChangeStuffRights($client, $company, $value, $type)
    {
        $client_has_company = ClientHasCompany::find()
            ->where(['client_id' => $client, 'company_id' => $company])
            ->one();

        if ($type == 'booking') {
            $client_has_company->booking = $value ? 1 : 0;
        } elseif ($type == 'driving') {
            $client_has_company->driving = $value ? 1 : 0;
        }

        $json = $client_has_company->save(false) ? 1 : 0;

        return json_encode($json);
    }

    private function getListOrderData($company_id, $searchParams)
    {
        $searchModel             = new OrderClientCompanySearch();
        $searchModel->type       = 'today';
        $searchModel->company_id = $company_id;
        $dataProvider            = $searchModel->search($searchParams);
        $pjaxId                  = 'order_client_company';

        return compact('searchModel', 'dataProvider', 'pjaxId');
    }

    public function actionOrders($id)
    {
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_grid_orders', $this->getListOrderData($id, Yii::$app->request->queryParams));
        }

        return $this->redirect(['update', '#' => 'orders', 'id' => $id]);
    }

    public function actionDumpCompanyOrders($id)
    {
        $searchModel             = new OrderClientCompanySearch();
        $searchModel->type       = 'today';
        $searchModel->company_id = $id;
        $model                   = $searchModel->search(app()->request->queryParams)->query
            ->orderBy(['order_id' => SORT_DESC])
            ->all();

        $exportData          = [];
        $addressPointColumns = [];

        /** @var Order $order */
        foreach ($model as $order) {
            $exportData[] = $order->getDumpByClientCompany();
        }

        //        $currency = !empty($model[0]) ? Currency::getCurrencySymbol($model[0]->currency_id) : '';
        $headers = ArrayHelper::merge([
            'order_number'     => 'Номер заказа',
            'device'           => t('order', 'Device'),
            'status'           => t('order', 'Status'),
            'address'          => t('order', 'Address'),
            'worker'           => t('employee', 'Worker'),
            'car'              => t('car', 'Car'),
            'review'           => t('order', 'Review'),
            'rating'           => t('client', 'Rating'),
            'wait_time'        => t('order', 'Time waiting') . '(' . t('app', 'min.') . ')',
            'summary_time'     => t('order', 'Summary time') . '(' . t('app', 'min.') . ')',
            'summary_distance' => t('order', 'Summary distance') . '(' . t('app', 'km') . ')',
            'summary_cost'     => t('order', 'Cost'),
            'client'           => t('client', 'From client'),
            'order_time'       => t('order', 'Order time'),
            'client_phone'     => t('client', 'Phone'),
        ], $addressPointColumns);

        ExcelExport::widget([
            'data'     => $exportData,
            'format'   => 'Excel5',
            'headers'  => $headers,
            'fileName' => 'clientCompanyOrders',
        ]);
    }

    public function actionDumpStuff()
    {
        $clients    = $this->stuffSearch(false);
        $exportData = [];

        foreach ($clients as $client) {
            $name = $client->fullName;

            $ar_phone = [];
            foreach ($client->clientPhones as $phone) {
                $ar_phone[] = $phone->value;
            }
            $phone = implode("\n", $ar_phone);

            $is_active = $client->getActiveFieldValue();
            $city      = $client->city->{'name' . getLanguagePrefix()};

            $exportData[] = [
                $name,
                $phone,
                $is_active,
                $city,
            ];
        }

        $headers = [
            t('user', 'Name'),
            t('client', 'Phone'),
            t('client', 'Activity'),
            t('client', 'City'),
        ];

        ExcelExport::widget([
            'data'     => $exportData,
            'format'   => 'Excel5',
            'fileName' => 'client-company',
            'headers'  => $headers,
        ]);
    }

    public function stuffSearch($usePagination = true)
    {
        $clients = [];
        if (get('company')) {
            $search_model = new CompanyClientSearch();
            $dataProvider = $search_model->search($_REQUEST);
            if (!$usePagination) {
                $dataProvider->pagination = false;
            }
            $clients = $dataProvider->getModels();
        }

        return $clients;
    }

}