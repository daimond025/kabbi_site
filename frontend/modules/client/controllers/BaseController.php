<?php

namespace app\modules\client\controllers;


use app\modules\client\models\ClientHasCompany;
use app\modules\client\models\ClientPhone;
use app\modules\client\models\CompanySearch;
use app\modules\client\models\OrderClientSearch;
use app\modules\client\Module;
use frontend\modules\client\components\ClientService;
use frontend\modules\bonus\components\BonusSystemService;
use frontend\widgets\ExcelExport;
use Yii;
use yii\db\ActiveQuery;
use yii\filters\AccessControl;
use app\modules\client\models\Client;
use app\modules\client\models\ClientSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\Html;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use frontend\components\behavior\CityListBehavior;

/**
 * ClientController implements the CRUD actions for Client model.
 * @mixin CityListBehavior
 */
class BaseController extends Controller
{
    private $bonusSystemService;
    private $clientService;

    public function behaviors()
    {
        return [
            'access'   => [
                'class'  => AccessControl::class,
                'except' => [],
                'rules'  => [
                    [
                        'actions' => [
                            'list',
                            'search',
                            'update',
                            'reviews',
                            'show-balance',
                            'orders',
                            'organization',
                            'organization-search',
                            'order-search',
                            'page',
                        ],
                        'allow'   => true,
                        'roles'   => ['read_clients'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['clients'],
                    ],
                ],
            ],
            'cityList' => [
                'class' => CityListBehavior::class,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function __construct(
        $id,
        Module $module,
        BonusSystemService $service,
        ClientService $clientService,
        array $config = []
    ) {
        $this->bonusSystemService = $service;
        $this->clientService      = $clientService;
        parent::__construct($id, $module, $config);
    }

    public function actionList()
    {
        return $this->render('index', $this->getListData(Yii::$app->request->queryParams));
    }

    public function actionClientHasBlocked($id)
    {
        if (Yii::$app->request->isAjax) {
            $queryParams                                  = Yii::$app->request->queryParams;
            $queryParams['ClientSearch']['workerBlocked'] = $id;

            return $this->renderAjax(
                '_grid',
                $this->getListData($queryParams)
            );
        }

        return $this->redirect(['/employee/worker/list', '#' => 'blocking']);
    }

    public function actionCreate()
    {
        $client = new Client();

        if ($client->load(Yii::$app->request->post())) {
            $transaction = app()->db->beginTransaction();

            if ($client->save()) {
                try {
                    ClientPhone::manySave($client->phone, $client->client_id);
                    session()->setFlash('success', t('client', 'The client was successfully added.'));

                    $transaction->commit();

                    return $this->redirect('list');
                } catch (\yii\db\Exception $ex) {
                    session()
                        ->setFlash('error', t('client', 'Error saving the client. Notify to administrator, please.'));
                }
            } else {
                displayErrors([$client]);
            }

            $transaction->rollBack();
        }

        return $this->render('create', [
            'client'   => $client,
            'dataForm' => Client::getdataForm(),
        ]);
    }

    public function actionUploadLogo($entity_id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $entity           = $this->findOne($entity_id);
        $entity->scenario = Client::SCENARIO_UPLOAD_PHOTO;
        $entity->load(post());

        if ($entity->save(true, ['photo'])) {
            return [
                'src' => [
                    'big'   => $entity->getPicturePath($entity->photo),
                    'thumb' => $entity->getPicturePath($entity->photo, false),
                ],
            ];
        }

        return $entity->getFirstErrors();
    }

    /**
     * Updates an existing Client model.
     *
     * @param integer $id
     *
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $client = Client::find()
            ->where([
                'client_id' => $id,
                'tenant_id' => user()->tenant_id,
            ])
            ->with(['clientPhones', 'city'])
            ->one();

        if (!$client) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if ($client->load(Yii::$app->request->post())) {
            if ($client->save()) {
                session()->setFlash('success', t('app', 'Data updated successfully'));

                return $this->refresh();
            } else {
                displayErrors([$client]);
            }
        }

        list($client->birth_year, $client->birth_month, $client->birth_day) =
            !empty($client->birth) ? explode('-', $client->birth) : null;

        $client->phone = ArrayHelper::getColumn($client->clientPhones, 'value');

        $tenantId = user()->tenantId;

        return $this->render('update', [
            'client'           => $client,
            'dataForm'         => Client::getdataForm(),
            'bonusAccountKind' => $this->bonusSystemService->getBonusKindId($tenantId),
        ]);
    }

    private function getListOrderData($client_id, $searchParams)
    {
        $searchModel            = new OrderClientSearch();
        $searchModel->type      = 'today';
        $searchModel->client_id = $client_id;
        $dataProvider           = $searchModel->search($searchParams);
        $pjaxId                 = 'order_client';

        return compact('searchModel', 'dataProvider', 'pjaxId');
    }

    public function actionOrders($id)
    {
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_grid_orders', $this->getListOrderData($id, Yii::$app->request->queryParams));
        }

        return $this->redirect(['update', '#' => 'orders', 'id' => $id]);
    }

    public function actionReviews($id, $notActive = false)
    {
        if (!app()->request->isAjax) {
            return $this->redirect(['update', 'id' => $id, '#' => 'reviews']);
        }

        $client = Client::find()
            ->where([
                'client_id' => $id,
                'tenant_id' => user()->tenant_id,
            ])
            ->with([
                'reviews' => function (ActiveQuery $query) use ($notActive) {
                    $query->with('order')->orderBy(['create_time' => SORT_DESC]);
                    if ($notActive) {
                        $query->andWhere(['active' => 0]);
                    }
                },
                'reviewRaiting',
            ])
            ->select(['client_id', 'create_time'])
            ->one();

        return $this->renderAjax('review', [
            'client'    => $client,
            'clientId'  => $id,
            'notActive' => (boolean)$notActive,
        ]);
    }

    /**
     * Ajax organization seacrh from filter.
     *
     * @param integer $id Client id.
     *
     * @return mixed
     */
    public function actionOrganizationSearch($id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $search_model = new CompanySearch([
            'without_client' => $id,
            'accessCityList' => $this->getUserCityIds(),
            'formName'       => '',
        ]);

        $dataProvider = $search_model->search($_POST);

        return $this->renderAjax(
            post('view', '_organization'),
            ['data' => $dataProvider->getModels(), 'view' => '_organization_tr_add']
        );
    }

    /**
     * Organization tab.
     */
    public function actionOrganization()
    {
        return $this->renderAjax(post('view', 'organization'), [
            'data' => Client::getOrganizationList(get('id'), post('sort_type', 'name'), post('sort', 'asc')),
        ]);
    }

    /**
     * Remove organization from client.
     *
     * @param int $client
     * @param $company
     *
     * @return string
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionOrganizationRemove($client, $company)
    {
        $client_has_company = ClientHasCompany::find()
            ->where(['client_id' => $client, 'company_id' => $company])
            ->one();

        $json = !is_null($client_has_company) && $client_has_company->delete() ? 1 : 0;

        return json_encode($json);
    }

    /**
     * Add organization to client.
     *
     * @param int $client_id
     * @param int $company_id
     *
     * @return string
     */
    public function actionOrganizationAdd($client_id, $company_id)
    {
        $clientHasCompany = new ClientHasCompany();

        $clientHasCompany->client_id  = $client_id;
        $clientHasCompany->company_id = $company_id;
        $clientHasCompany->save();

        return $this->renderAjax('_organization', [
            'data' => Client::getOrganizationList($client_id),
        ]);
    }

    public function actionDumpClientOrders($id)
    {
        $searchModel            = new OrderClientSearch();
        $searchModel->type      = 'today';
        $searchModel->client_id = $id;
        $model                  = $searchModel->search(app()->request->queryParams)->query
            ->orderBy(['order_id' => SORT_DESC])
            ->all();

        $exportData = [];

        foreach ($model as $order) {
            $exportData[] = $order->getDumpByClient();
        }

        $headers = [
            'order_number'     => 'Номер заказа',
            'device'           => t('order', 'Device'),
            'status'           => t('order', 'Status'),
            'address'          => t('order', 'Address'),
            'worker'           => t('employee', 'Worker'),
            'car'              => t('car', 'Car'),
            'review'           => t('order', 'Review'),
            'rating'           => t('client', 'Rating'),
            'wait_time'        => t('order', 'Time waiting') . '(' . t('app', 'min.') . ')',
            'summary_time'     => t('order', 'Summary time') . '(' . t('app', 'min.') . ')',
            'summary_distance' => t('order', 'Summary distance') . '(' . t('app', 'km') . ')',
            'summary_cost'     => t('order', 'Cost'),
            'company'          => t('client', 'From organization'),
        ];

        ExcelExport::widget([
            'data'     => $exportData,
            'format'   => 'Excel5',
            'headers'  => $headers,
            'fileName' => 'clientOrders',
        ]);
    }

    public function actionDump()
    {
        /** @var Client[] $clients */
        $clients = Client::find()
            ->where(['tenant_id' => user()->tenant_id])
            ->with([
                'city'         => function ($query) {
                    $query->select(['name', 'city_id']);
                },
                'clientPhones' => function ($query) {
                    $query->select(['client_id', 'value']);
                },
            ])
            ->select([
                'last_name',
                'name',
                'second_name',
                'birth',
                'email',
                'active',
                'black_list',
                'city_id',
                'client_id',
                'success_order',
                'fail_worker_order',
                'fail_client_order',
                'fail_dispatcher_order',
            ])->asArray();

        if (empty($clients)) {
            $this->goBack();
        }

        $exportData = [];

        foreach ($clients->each(100) as $client) {
            $exportData[] = [
                'name'          => trim(str_replace('  ', ' ', $client['last_name'] . ' ' . $client['name'] . ' ' . $client['second_name'])),
                'phone'         => $client['clientPhones'][0]['value'],
                'birth'         => $client['birth'],
                'email'         => $client['email'],
                'city'          => $client['city']['name'],
                'active'        => Client::getActiveValueForClientBaseExport($client),
                'success_order' => $client['success_order'],
                'fail_orders'   => (int)$client['fail_worker_order'] + (int)$client['fail_client_order'] + (int)$client['fail_dispatcher_order'],
            ];
        }

        $headers = [
            'name'          => t('app', 'Full name'),
            'phone'         => t('client', 'Phone'),
            'birth'         => t('client', 'Birth'),
            'email'         => t('client', 'Email'),
            'city'          => t('client', 'City'),
            'active'        => t('client', 'Active'),
            'success_order' => t('client', 'Successful orders'),
            'fail_orders'   => t('client', 'Unsuccessful orders'),
        ];

        ExcelExport::widget([
            'data'     => $exportData,
            'format'   => 'Excel5',
            'headers'  => $headers,
            'fileName' => 'clients',
        ]);
    }

    /**
     * Ajax validate client phone number.
     * @return boolean
     */
    public function actionValidatePhone()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;

        return (new Client(['phone' => [Html::encode(post('phone'))]]))->validate('phone') ? 1 : 0;
    }

    public function actionSwitchRole($client_id, $company_id, $role)
    {
        $model = ClientHasCompany::find()
            ->alias('t1')
            ->where([
                't1.client_id' => $client_id,
                'company_id'   => $company_id,
                'cl.tenant_id' => user()->tenant_id,
            ])
            ->joinWith('client cl')
            ->one();

        if (!$model) {
            return 0;
        }

        $model->role = $role;

        return (int)$model->save();
    }

    public function actionNotActiveReview($id)
    {
        app()->response->format = Response::FORMAT_JSON;

        return $this->clientService->setNotActiveReview(user()->tenant_id, $id);
    }

    public function actionActiveReview($id)
    {
        app()->response->format = Response::FORMAT_JSON;

        return $this->clientService->setActiveReview(user()->tenant_id, $id);
    }

    protected function findOne($entity_id)
    {
        return Client::find()
            ->where([
                'client_id' => $entity_id,
                'tenant_id' => user()->tenant_id,
            ])
            ->one();
    }

    private function getListData($searchParams)
    {
        $cityList = ['null' => t('client', 'Without city')];
        $cityList += $this->getUserCityList();

        $searchModel  = new ClientSearch();
        $searchModel->setAccessCityList(array_keys($cityList));
        $dataProvider = $searchModel->search($searchParams);
        $pjaxId       = 'clients';

        return compact('searchModel', 'dataProvider', 'cityList', 'pjaxId');
    }
}
