<?php

namespace app\modules\client\controllers;

use frontend\modules\bonus\components\BonusSystemService;
use frontend\modules\bonus\models\BonusSystem;
use frontend\modules\client\components\ClientBonusService;
use frontend\modules\client\models\ClientBonusGootax;
use frontend\modules\client\models\ClientBonusSearch;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use frontend\modules\client\models\ClientBonus;
use yii\web\Response;

/**
 * Class BonusController
 * @package app\modules\client\controllers
 */
class BonusController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'list',
                            'list-blocked',
                            'update',
                            'get-classes',
                            'get-client-tariffs',
                        ],
                        'allow'   => true,
                        'roles'   => ['read_bonus'],
                    ],
                    [
                        'actions' => [],
                        'allow'   => true,
                        'roles'   => ['bonus'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Getting tariffs by bonus_id
     *
     * @param integer $bonusId
     *
     * @return array
     */
    private function getTariffsByBonusId($bonusId)
    {
        $tariffs = ClientBonus::find()
            ->alias('c')
            ->select(['t.tariff_id'])
            ->distinct()
            ->innerJoinWith('tariffs t', false)
            ->andWhere(['c.tenant_id' => user()->tenant_id])
            ->andWhere(['!=', 'c.bonus_id', $bonusId])
            ->byStatus(ClientBonus::STATUS_NOT_BLOCKED)
            ->asArray()
            ->all();

        return $tariffs ? ArrayHelper::getColumn($tariffs, 'tariff_id') : [];
    }

    /**
     * Getting positions by city
     *
     * @param $cityId
     *
     * @return mixed
     */
    public function actionGetPositions($cityId)
    {
        app()->response->format = Response::FORMAT_JSON;

        if (!app()->request->isAjax) {
            return false;
        }

        /* @var $service ClientBonusService */
        $service   = $this->module->clientBonusService;
        $positions = $service->getPositions($cityId);

        return compact('positions');
    }

    /**
     * Getting classes by city and position
     *
     * @param $cityId
     * @param $positionId
     *
     * @return mixed
     */
    public function actionGetClasses($cityId, $positionId)
    {
        app()->response->format = Response::FORMAT_JSON;

        if (!app()->request->isAjax) {
            return false;
        }

        /* @var $service ClientBonusService */
        $service = $this->module->clientBonusService;

        $hasCar  = $service->isPositionHasCar($positionId);
        $classes = $service->getCarClasses($cityId, $positionId);

        return compact('hasCar', 'classes');
    }

    /**
     * Getting client tariffs
     *
     * @param $bonusId
     * @param $cityId
     * @param $positionId
     * @param $classId
     *
     * @return array
     */
    private function getClientTariffs($bonusId, $cityId, $positionId, $classId)
    {
        /* @var $service ClientBonusService */
        $service    = $this->module->clientBonusService;
        $onlyActive = empty($bonusId) ? true : null;
        $hasCar     = $service->isPositionHasCar($positionId);
        $classId    = $hasCar && empty($classId) ? -1 : $classId;
        $tariffs    = ArrayHelper::index($service->getClientTariffs(
            $cityId, $positionId, $classId, $onlyActive), 'tariff_id');

        $alreadyChoosedTariffs = $this->getTariffsByBonusId($bonusId);

        $result = [];

        foreach ($tariffs as $key => $value) {
            $isAlreadyChoosed        = in_array($key, $alreadyChoosedTariffs, false);
            $value['alreadyChoosed'] = (int)$isAlreadyChoosed;
            $value['name']           = (isset($value['name']) ? $value['name'] : t('taxi_tariff', 'No name'))
                . ($value['block'] == 1 ? ' (' . t('client-bonus', 'blocked') . ')' : '');
            $value['name']           .= $isAlreadyChoosed
                ? ' ' . t('client-bonus', '(used in the other bonus system)') : '';
            $result[$key]            = $value;
        }

        return $result;
    }

    /**
     * Getting client tariffs
     *
     * @param integer $bonusId
     * @param integer $cityId
     * @param integer $positionId
     * @param integer $classId
     *
     * @return mixed
     */
    public function actionGetClientTariffs($bonusId, $cityId, $positionId, $classId)
    {
        app()->response->format = Response::FORMAT_JSON;

        if (!app()->request->isAjax) {
            return false;
        }

        $tariffs = $this->getClientTariffs($bonusId, $cityId, $positionId, $classId);

        return compact('tariffs');
    }

    private function getListData($searchParams, $block = false)
    {
        $cityList = user()->getUserCityList();

        $searchModel                 = new ClientBonusSearch();
        $searchModel->accessCityList = array_keys($cityList);
        $dataProvider                = new ArrayDataProvider([
            'allModels'  => $searchModel->search($searchParams, $block)->asArray()->all(),
            'sort'       => [
                'attributes' => ['name'],
            ],
            'pagination' => [
                'pageSize' => 25,
            ],
        ]);

        $pjaxId = $block ? 'client_bonus_blocked' : 'client_bonus';

        return compact('searchModel', 'dataProvider', 'cityList', 'pjaxId');
    }


    public function actionList()
    {
        return $this->render('index', $this->getListData(Yii::$app->request->queryParams));
    }

    public function actionListBlocked()
    {
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_grid', $this->getListData(Yii::$app->request->queryParams, 1));
        }

        return $this->redirect(['list', '#' => 'blocked']);
    }

    /**
     * Save client bonus model in transaction
     *
     * @param ClientBonus       $model
     * @param ClientBonusGootax $gootaxBonus
     *
     * @return boolean
     * @throws \yii\db\Exception
     */
    private function saveModel($model, $gootaxBonus = null)
    {
        $insert = $model->isNewRecord;

        $transaction = app()->db->beginTransaction();
        try {
            if ((int)$model->bonus_system_id === BonusSystemService::BONUS_SYSTEM_ID_UDS_GAME) {
                if ($gootaxBonus->isNewRecord) {
                    $gootaxBonus = null;
                } else {
                    $gootaxBonus->delete();
                    $gootaxBonus = null;
                }
            }

            $isValid = $model->validate();
            $isValid = ($gootaxBonus === null || $gootaxBonus->validate()) && $isValid;

            if ($isValid) {
                $model->save(false);

                if ($gootaxBonus !== null) {
                    $gootaxBonus->bonus_id = $model->bonus_id;
                    $gootaxBonus->save(false);
                }

                $transaction->commit();

                if ($insert) {
                    session()->setFlash('success', t('client-bonus', 'The bonus was successfully added.'));
                } else {
                    session()->setFlash('success', t('client-bonus', 'The bonus was successfully updated.'));
                }

                return true;
            }

            $errors = ArrayHelper::merge(
                $model->getFirstErrors(),
                $gootaxBonus === null ? [] : $gootaxBonus->getFirstErrors());
            session()->setFlash('error', implode(' ', $errors));
            $transaction->rollBack();
        } catch (\Exception $ex) {
            $transaction->rollBack();
            \Yii::error($ex->getMessage());

            session()->setFlash('error', t('client-bonus', 'Error saving bonus. Notify to administrator, please.'));
        }

        return false;
    }

    /**
     * Creates a new ClientBonus model.
     * @return mixed
     * @throws \yii\base\InvalidParamException
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        $model = new ClientBonus([
            'scenario' => app()->user->can('bonus')
                ? ClientBonus::SCENARIO_DEFAULT : ClientBonus::SCENARIO_READ,
        ]);
        $model->loadDefaultValues();

        $gootaxBonus = new ClientBonusGootax();
        $gootaxBonus->loadDefaultValues();

        if ($model->load(app()->request->post())
            && $gootaxBonus->load(app()->request->post())
            && $this->saveModel($model, $gootaxBonus)
        ) {
            return $this->redirect(['bonus/update', 'id' => $model->bonus_id]);
        }

        /* @var $service ClientBonusService */
        $service = $this->module->clientBonusService;

        $cities         = user()->userCityList;
        $model->city_id = isset($cities[$model->city_id]) ? $model->city_id : key($cities);

        $positions          = $service->getPositions($model->city_id);
        $model->position_id = isset($positions[$model->position_id]) ? $model->position_id : key($positions);

        $hasCar          = $service->isPositionHasCar($model->position_id);
        $classes         = $service->getCarClasses($model->city_id, $model->position_id);
        $model->class_id = isset($classes[$model->class_id]) ? $model->class_id : key($classes);

        $tariffs = $this->getClientTariffs(
            $model->bonus_id, $model->city_id, $model->position_id, $model->class_id);

        $bonusSystems = $service->getBonusSystems();

        return $this->render('create',
            compact('model', 'gootaxBonus', 'cities', 'hasCar', 'positions', 'classes', 'tariffs', 'bonusSystems'));
    }


    /**
     * Update ClientBonus model
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidParamException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $gootaxBonus = $model->gootaxBonus;
        if ($gootaxBonus === null) {
            $gootaxBonus = new ClientBonusGootax();
            $gootaxBonus->loadDefaultValues();
        }

        if ($model->load(app()->request->post())
            && $gootaxBonus->load(app()->request->post())
            && $this->saveModel($model, $gootaxBonus)
        ) {
            return $this->redirect(['bonus/update', 'id' => $model->bonus_id]);
        }

        /* @var $service ClientBonusService */
        $service = $this->module->clientBonusService;

        $cities    = user()->userCityList;
        $positions = $service->getPositionsWithCurrent($model->position_id, $model->city_id);

        $hasCar  = $service->isPositionHasCar($model->position_id);
        $classes = $service->getCarClasses($model->city_id, $model->position_id);

        $tariffs = $this->getClientTariffs(
            $model->bonus_id, $model->city_id, $model->position_id, $model->class_id);

        $model->choosed_tariffs = ArrayHelper::getColumn(
            $model->getClientBonusHasTariffs()->all(), 'tariff_id');

        $bonusSystems = $service->getBonusSystems();

        return $this->render('update',
            compact('model', 'gootaxBonus', 'cities', 'hasCar', 'positions', 'classes', 'tariffs', 'bonusSystems'));
    }

    /**
     * Find client bonus model by id
     *
     * @param integer $id
     *
     * @return ClientBonus
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = ClientBonus::findOne($id)) !== null) {
            $model->scenario = app()->user->can('bonus')
                ? ClientBonus::SCENARIO_DEFAULT : ClientBonus::SCENARIO_READ;

            return $model;
        } else {
            throw new NotFoundHttpException(t('app', 'The requested page does not exist.'));
        }
    }

}