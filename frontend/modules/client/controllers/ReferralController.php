<?php

namespace app\modules\client\controllers;

use app\modules\setting\models\TenantSetting;
use common\modules\city\models\City;
use frontend\components\behavior\CityListBehavior;
use frontend\modules\referral\models\DefaultReferral;
use frontend\modules\referral\models\Referral;
use frontend\modules\referral\models\ReferralSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


/**
 * Class ReferralController
 * @package app\modules\client\controllers
 */
class ReferralController extends Controller
{
    public function behaviors()
    {
        return [
            'access'   => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [],
                        'allow'   => true,
                        'roles'   => ['referralSystem'],
                    ],
                    [
                        'actions' => ['list', 'list-blocked', 'update'],
                        'allow'   => true,
                        'roles'   => ['read_referralSystem'],
                    ],
                ],
            ],
            'cityList' => [
                'class' => CityListBehavior::className(),
            ],
        ];
    }


    public function actionList()
    {
        return $this->render('index', $this->getListData(\Yii::$app->request->queryParams, true));
    }

    public function actionListBlocked()
    {
        if (\Yii::$app->request->isAjax) {
            return $this->renderAjax('_grid', $this->getListData(\Yii::$app->request->queryParams, false));
        }

        return $this->redirect(['list', '#' => 'blocked']);
    }


    public function actionCreate()
    {
        $model = $this->createModel();

        if ($model->load(post()) && $model->save()) {
            session()->setFlash('success', t('app', 'Data updated successfully'));
        }

        $cityList      = $this->getUserCityList();
        $isExistActive = $this->getIsExistActive();

        return $this->render('create', [
            'model'         => $model,
            'cityList'      => $cityList,
            'currency'      => '',
            'isExistActive' => $isExistActive,
        ]);
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (!$model) {
            throw new \HttpException();
        }

        if ($model->load(post()) && $model->save()) {
            session()->setFlash('success', t('app', 'Data updated successfully'));
        }

        $cityList      = $this->getUserCityList();
        $isExistActive = $this->getIsExistActive($id, $model->city_id);
        $currency      = getCurrencySymbol($model->city_id);

        return $this->render('update', compact('model', 'isExistActive', 'currency', 'cityList'));
    }


    protected function getIsExistActive($notReferralId = null, $cityId = null)
    {
        $model = Referral::find()
            ->where([
                'tenant_id' => user()->tenant_id,
                'active'    => 1,
            ])
            ->andFilterWhere(['not', ['referral_id' => $notReferralId]])
            ->andFilterWhere(['city_id' => $cityId])
            ->exists();

        return $model;
    }


    protected function getListData($searchParams, $isActive = true)
    {
        $cityList = $this->getUserCityList();

        $searchModel = new ReferralSearch(['isActive' => $isActive]);
        $searchModel->setAccessCityList(array_keys($cityList));
        $dataProvider = $searchModel->search($searchParams);
        $pjaxId       = 'referrals-' . ($isActive ? 'active' : 'blocked');

        return compact('searchModel', 'dataProvider', 'cityList', 'pjaxId');
    }

    /**
     * Получить модель
     *
     * @param $id
     *
     * @return Referral|null
     */
    protected function findModel($id)
    {
        return Referral::find()
            ->where([
                'referral_id' => $id,
                'tenant_id'   => user()->tenant_id,
            ])
            ->one();
    }

    /**
     * Создать модель
     * @return Referral
     */
    protected function createModel()
    {
        $model             = new Referral();
        $default           = DefaultReferral::find()->one();
        $model->attributes = [
            'sort'                       => 100,
            'is_active_subscriber_limit' => 0,
            'active'                     => 1,
            'type'                       => Referral::ON_REGISTER,
            'tenant_id'                  => user()->tenant_id,
            'text_in_client_api'         => $default->text_in_client_api,
            'title'                      => $default->title,
            'text'                       => $default->text,
        ];

        return $model;
    }


}