;(function () {
    'use strict';

    var app = {
        urlCurrency: '/city/city/get-city-currency-symbol',
        selectorCity: '.js-city',
        selectorCurrency: '.js-currency',
        selectorIsLimit: '.js-is-limit',
        selectorOnLimit: '.js-on-limit',
        selectorIsOrder: '.js-is-order',
        selectorOnOrder: '.js-on-order',
        selectorIsRegister: '.js-is-register',
        selectorOnRegister: '.js-on-register',
        selectorIsBlock: '.js-is-block',

        init: function () {
            this.startEvent();

            this.onChangeIsLimit();
            this.onChangeType();
            this.onChangeCity();
        },

        startEvent: function () {
            this.changeIsLimit();
            this.changeType();
        },

        onChangeCity: function () {
            var _this = this;
            $(this.selectorCity).on('change', function () {
                var value = $(this).val();
                _this.changeCity(value);
            })
        },
        changeCity: function (cityId) {
            var _this = this;
            $.get(this.urlCurrency, {
                city_id: cityId
            }, function (currency) {
                $(_this.selectorCurrency).html(currency);
            });
        },

        onChangeIsLimit: function () {
            var _this = this;
            $(this.selectorIsLimit).on('change', function () {
                _this.changeIsLimit();
            });
        },
        changeIsLimit: function () {
            var value = $(this.selectorIsLimit).prop('checked');
            if (value === true) {
                this.showOnLimit();
            } else {
                this.hideOnLimit();
            }
        },
        showOnLimit: function () {
            $(this.selectorOnLimit).show();
        },
        hideOnLimit: function () {
            $(this.selectorOnLimit).hide();
        },

        onChangeType: function () {
            var _this = this;
            $(this.selectorIsOrder).on('change', function () {
                _this.changeType();
            });
            $(this.selectorIsRegister).on('change', function () {
                _this.changeType();
            });
        },
        changeType: function () {
            var isOrder = $(this.selectorIsOrder).prop('checked');
            if (isOrder === true) {
                this.showIsOrder();
                this.hideIsRegister();
            } else {
                var isRegister = $(this.selectorIsRegister).prop('checked');
                if (isRegister === true) {
                    this.hideIsOrder();
                    this.showIsRegister();
                }
            }
        },
        showIsRegister: function () {
            $(this.selectorOnRegister).show();
        },
        hideIsRegister: function () {
            $(this.selectorOnRegister).hide();
        },
        showIsOrder: function () {
            $(this.selectorOnOrder).show();
        },
        hideIsOrder: function () {
            $(this.selectorOnOrder).hide();
        },
    };

    app.init();
})();