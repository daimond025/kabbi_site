(function ($) {
    'use strict';

    var app = {
        bonusIdSelector: '#clientbonus-bonus_id',
        bonusSystemIdSelector: '#clientbonus-bonus_system_id',
        cityIdSelector: '#clientbonus-city_id',
        positionIdSelector: '#clientbonus-position_id',
        classIdSelector: '#clientbonus-class_id',
        tariffsSelector: '#clientbonus-choosed_tariffs',
        currencySelector: '.currency',
        paymentMethodSelector: '#clientbonusgootax-payment_method',
        fieldMaxPaymentSelector: '.field-clientbonusgootax-max_payment',
        maxPaymentSelector: '#clientbonusgootax-max_payment',
        gootaxBonusSelector: '.js-gootax-bonus',

        init: function () {
            this.registerCityIdChangeEvent();
            this.registerBonusSystemIdChangeEvent();
            this.registerPositionIdChangeEvent();
            this.registerPaymentMethodChangeEvent();
            this.registerClassIdChangeEvent();

            $(this.bonusSystemIdSelector).trigger('change');
        },

        registerCityIdChangeEvent: function () {
            var _this = this;

            $(_this.cityIdSelector).on('change', function () {
                _this.loadPositions();
                _this.loadCityCurrency();
            });
        },

        registerBonusSystemIdChangeEvent: function () {
            var _this = this;

            $(_this.bonusSystemIdSelector).on('change', function () {
                var $this = $(this);

                if ($this.val() == $this.data('gootax-id')) {
                    $(_this.gootaxBonusSelector).show();
                } else {
                    $(_this.gootaxBonusSelector).hide();
                }
            });
        },

        registerPositionIdChangeEvent: function () {
            var _this = this;

            $(_this.positionIdSelector).on('change', function () {
                _this.loadClasses();
            });
        },

        registerClassIdChangeEvent: function () {
            var _this = this;

            $(_this.classIdSelector).on('change', function () {
                _this.loadTariffs();
            });
        },

        loadPositions: function () {
            var _this = this,
                cityId = $(this.cityIdSelector).val();

            $.get('/client/bonus/get-positions', {
                cityId: cityId
            })
                .done(function (data) {
                    var $positions = $(_this.positionIdSelector);

                    $positions.find('option').remove();

                    if (data instanceof Object) {
                        var $positionItems = $();
                        Object.keys(data.positions).forEach(function (item) {
                            $positionItems = $positionItems.add(
                                $('<option/>')
                                    .val(item)
                                    .text(data.positions[item]));
                        });

                        $positions.append($positionItems);
                        $positions.trigger('update');
                    }
                })
                .fail(function (xhr) {
                    console.log('fail', xhr);
                })
                .always(function () {
                    _this.loadClasses();
                });
        },

        loadClasses: function () {
            var _this = this,
                cityId = $(this.cityIdSelector).val(),
                positionId = $(this.positionIdSelector).val();

            $.get('/client/bonus/get-classes', {
                cityId: cityId,
                positionId: positionId
            })
                .done(function (data) {
                    var $classes = $(_this.classIdSelector);

                    console.log(123);
                    $(_this.tariffsSelector).empty();

                    $classes.find('option').not(':first').remove();
                    $classes.closest('.row').hide();

                    if (data instanceof Object) {
                        var $classItems = $()
                        if (data.hasCar) {
                            Object.keys(data.classes).forEach(function (item) {
                                $classItems = $classItems.add(
                                    $('<option/>')
                                        .val(item)
                                        .text(data.classes[item]));
                            });

                            $classes.append($classItems);
                            $classes.closest('.row').show();
                        }
                        $classes.trigger('update');
                    }
                })
                .fail(function (xhr) {
                    console.log('fail', xhr);
                })
                .always(function () {
                    _this.loadTariffs();
                });
        },

        loadTariffs: function () {
            var _this = this,
                bonusId = $(this.bonusIdSelector).val(),
                cityId = $(this.cityIdSelector).val(),
                positionId = $(this.positionIdSelector).val(),
                classId = $(this.classIdSelector).val();

            $.get('/client/bonus/get-client-tariffs', {
                bonusId: bonusId,
                cityId: cityId,
                positionId: positionId,
                classId: classId
            })
                .done(function (data) {
                    var $tariffs = $(_this.tariffsSelector),
                        $tariffItems = $();

                    $tariffs.empty();

                    if (data instanceof Object) {
                        var tariffIds = Object.keys(data.tariffs);

                        tariffIds.sort(function (a, b) {
                            return data.tariffs[a].sort - data.tariffs[b].sort;
                        });

                        tariffIds.forEach(function (item) {
                            var $label = $('<label/>').text(data.tariffs[item]['name']);

                            if (data.tariffs[item].block == 1) {
                                $label.addClass('blocked-tariff');
                            }

                            if (data.tariffs[item].alreadyChoosed == 1) {
                                $label.addClass('already-choosed-tariff');
                            }

                            $label.prepend(
                                $('<input type="checkbox" name="ClientBonus[choosed_tariffs][]">').val(item)
                            );

                            $tariffItems = $tariffItems.add($label);
                        });
                        $tariffs.append($tariffItems);
                    }
                })
                .fail(function (xhr) {
                    console.log('fail', xhr);
                });
        },

        loadCityCurrency: function () {
            var _this = this;
            $.get(
                '/city/city/get-city-currency-symbol',
                {city_id: $(_this.cityIdSelector).val()}
            )
                .done(function (data) {
                    $(_this.currencySelector).text(data);
                })
                .fail(function (data) {
                    $(_this.currencySelector).text('');
                    console.log('fail', data);
                });
        },

        registerPaymentMethodChangeEvent: function () {
            var _this = this,
                $paymentMethod = $(_this.paymentMethodSelector);

            _this.setPaymentMethod($paymentMethod.val());

            $paymentMethod.on('change', function () {
                _this.setPaymentMethod($paymentMethod.val());
            });
        },

        setPaymentMethod: function (paymentMethod) {
            switch (paymentMethod) {
                case 'FULL':
                    $(this.fieldMaxPaymentSelector).hide();
                    $(this.maxPaymentSelector).val(null);
                    break;
                case 'PARTIAL':
                    $(this.fieldMaxPaymentSelector).show();
                    break;
            }
        }
    };

    app.init();

})(jQuery);