function calendarPeriodInit() {
    $('.sdp').each(function () {
        var obj = $(this);
        $(this).datepicker({
            altField: $(this).prev('input'),
            onSelect: function (date) {
                $(this).parent().prev('.a_sc').html(date).removeClass('sel_active');
                $(this).parent().hide();
                var form = obj.parents('form');
                orderSearch(form.attr('action'), form.serialize());
                $(this).prev('input').attr('value', date).trigger('change');
            }
        });
        if ($(this).hasClass('first_date')) {
            $(this).datepicker("setDate", '-14');
        }
        obj.closest('.cof_date').find('.a_sc').text(obj.val());
    });

    $('form[name="order_filter"] input[type="radio"]').on('click', function () {
        var form = $(this).parents('form');
        console.log(form.serialize());
        orderSearch(form.attr('action'), form.serialize());
    });
}

function savePdfLink() {
    var href = $('#dump_client_orders_link').attr('href');

    var updateLink = function () {
        var firstDate = $("#first_date_link").val(),
            secondDate = $("#second_date_link").val();

        $('#dump_client_orders_link').attr('href', href + '/' + firstDate + '/' + secondDate);
    };

    function returnLink(){
        $('#dump_client_orders_link').attr('href', href);
    }

    $('#first_date_link, #second_date_link').on('change', function () {
        updateLink();
    });

    $('#cof_time_choser').on('click',function(){
        updateLink();
    });

    $('.cof_first input').on('click',function(){
        returnLink();
    });
}

function orderSearch(url, data) {
    $.post(
        url,
        data,
        function (html) {
            $('.tabs_content div.active .co_list').html(html);
        }
    );
}

function sort_table(click_obj, url, data, table) {
    var all_sort_span = click_obj.parents('tr').find('th a span');
    var sort_span = click_obj.find('span');
    var sort_class = sort_span.attr('class');
    var sort;
    var sort_type = sort_span.data('type');

    all_sort_span.attr('class', '');

    if (sort_class === 'pt_down') {
        sort = 'desc';
        sort_class = 'pt_up';
    }
    else {
        sort = 'asc';
        sort_class = 'pt_down';
    }

    data.sort_type = sort_type;
    data.sort = sort;

    $.post(
        url,
        data,
        function (html) {
            var first_tr = table.find('tr:first');
            first_tr.nextAll('tr').remove();
            first_tr.after(html);
        }
    );

    sort_span.attr('class', sort_class);
}