$(function () {
    $(document).on('change', '.js-role', function () {
        var roles;
        var _this = $(this);
        if (_this.prop("checked")) {
            roles = 'juridical';
        } else {
            roles = 'physical';
        }
        $.ajax({
            url: "/client/base/switch-role",
            type: "GET",
            data: "client_id=" + $(this).data('client_id') + "&company_id=" + $(this).data('company_id') + "&role=" + roles,
            success: function (result) {
                if (result == '0') {
                    if (roles == 'physical') {
                        _this.prop("checked", true);
                    } else {
                        _this.prop("checked", false);
                    }
                }
            }
        });
    });

    //Сортировка списка клиентских организаций
    $('div.tabs_content').on('click', '.ajax .styled_table th a', function (e) {
        e.preventDefault();
        sort_table(
            $(this),
            $('.tabs_links li.active a').data('href'),
            {view: '_organization_tr'},
            $(this).parents('.styled_table')
        );
    });

    //Сортировка поиска организаций
    $('div.tabs_content').on('click', '#seacrh_company_result table th a', function (e) {
        e.preventDefault();
        var input_search = $('#client_company_search');
        var search = input_search.val();

        if (search.length > 1) {
            var all_sort_span = $(this).parents('tr').find('th a span');
            var sort_span = $(this).find('span');
            var sort_class = sort_span.attr('class');
            var sort;
            var sort_type = sort_span.data('type');
            var data = {input_search: search, view: '_organization_tr_add'};

            all_sort_span.attr('class', '');

            if (sort_class === 'pt_down') {
                sort = '-' + sort_type;
                sort_class = 'pt_up';
            } else {
                sort = sort_type;
                sort_class = 'pt_down';
            }

            $.post(
                input_search.data('search') + '?sort=' + sort,
                data,
                function (html) {
                    var first_tr = $('#seacrh_company_result').find('tr:first');
                    first_tr.nextAll('tr').remove();
                    first_tr.after(html);
                }
            );

            sort_span.attr('class', sort_class);
        }
    });

    var search_val_old = '';
    //Поиск организаций
    $('section.cont').on('keyup', '#client_company_search', function (e) {

        var input_search = $(this);

        if (e.which != 13) {
            setTimeout(function () {

                var search = input_search.val();

                if (search_val_old === search)
                    return false;

                search_val_old = search;

                if (search.length > 1) {
                    $.ajax({
                        type: "POST",
                        url: input_search.data('search'),
                        data: {input_search: search},
                        dataType: "html",
                        beforeSend: function () {
                            input_search.addClass('input_preloader');
                        },
                        success: function (html) {
                            input_search.removeClass('input_preloader');
                            $('#seacrh_company_result').html(html);
                        }
                    });
                }
                else
                    $('#seacrh_company_result').text('');
            }, 700);
        }
    });

    //Добавление организации
    $('div.tabs_content').on('click', '.styled_table a.pt_add', function (e) {
        e.preventDefault();
        var client_id = $('#client_company_search').data('client');
        var a = $(this);
        var tr = a.parents('tr');
        var tr_size = a.parents('table').find('tr').size();

        if (a.hasClass('loading_button')) {
            return;
        }

        a.addClass('loading_button');
        $.get(
            a.attr('href'),
            {client_id: client_id},
            function (html) {
                $('.ajax').html(html);

                if ((tr_size - 1) == 1)
                    $('#seacrh_company_result').text('');
                else
                    tr.remove();
            }
        )
            .always(function () {
                a.removeClass('loading_button');
            });
    });

    //ОРГАНИЗАЦИИ

    //Удаление сотрудника и организации
    $('div.tabs_content').on('click', 'table a.pt_del', function (e) {

        e.preventDefault();

        var a = $(this);
        var tr = a.parents('tr');
        var tr_size = a.parents('table').find('tr').size();

        if (a.hasClass('loading_button')) {
            return;
        }

        a.addClass('loading_button');
        $.get(
            a.attr('href'),
            {},
            function (json) {
                if (json == '1') {
                    if ((tr_size - 1) == 1) {
                        $.post(
                            $('.tabs_links li.active a').data('href'),
                            {view: a.data('view')},
                            function (html) {
                                a.parents('div.ajax').html(html);
                            }
                        );
                    }
                    else
                        tr.remove();
                }
            },
            'json'
        )
            .always(function () {
                a.removeClass('loading_button');
            });
    });

    //Поиск сотрудников
    $('section.cont').on('keyup', '#company_stuff_search', function (e) {
        var input_search = $(this);

        if (e.which != 13) {
            setTimeout(function () {

                var phone = input_search.val();

                if (search_val_old === phone)
                    return false;

                search_val_old = phone;

                if (phone.length > 5) {
                    $.ajax({
                        type: "POST",
                        url: input_search.data('search'),
                        data: {input_search: phone, company_id: input_search.data('company')},
                        dataType: "html",
                        beforeSend: function () {
                            input_search.addClass('input_preloader');
                        },
                        success: function (html) {
                            input_search.removeClass('input_preloader');
                            $('#client_add').html(html);
                        }
                    });
                }
                else
                    $('#client_add').text('');
            }, 700);
        }
    });

    //Добавление сотрудников
    $('div.tabs_content').on('click', '#client_add a.pt_add', function (e) {
        e.preventDefault();
        var company_id = $('#company_stuff_search').data('company');
        var a = $(this);

        if (a.hasClass('loading_button')) {
            return;
        }

        a.addClass('loading_button');
        $.get(
            a.attr('href'),
            {company_id: company_id, view: '_company_stuff'},
            function (html) {
                a.parent().text('Выбран');
                $('#company_stuff').html(html);
            }
        )
            .always(function () {
                a.removeClass('loading_button');
            });
    });

    //Сортировка
    $('div.tabs_content').on('click', '#client_add .people_table th a', function (e) {
        e.preventDefault();
        var input_search = $('#company_stuff_search');
        var search = input_search.val();

        if (search.length > 5) {
            var all_sort_span = $(this).parents('tr').find('th a span');
            var sort_span = $(this).find('span');
            var sort_class = sort_span.attr('class');
            var sort;
            var sort_type = sort_span.data('type');
            var data = {input_search: search, company_id: input_search.data('company'), view: '_stuff_add_tr'};

            all_sort_span.attr('class', '');

            if (sort_class === 'pt_down') {
                sort = '-' + sort_type;
                sort_class = 'pt_up';
            } else {
                sort = sort_type;
                sort_class = 'pt_down';
            }

            $.post(
                input_search.data('search') + '?sort=' + sort,
                data,
                function (html) {
                    var first_tr = $('#client_add').find('tr:first');
                    first_tr.nextAll('tr').remove();
                    first_tr.after(html);
                }
            );

            sort_span.attr('class', sort_class);
        }
    });

    //Подгрузка формы добавления клиентов
    $('body').on('click', '#client_add_button', function () {
        var a = $(this);
        if (!a.hasClass('ajax_loaded')) {
            a.addClass('ajax_loaded');
            $.post(
                a.data('href'),
                {phone: $('#company_stuff_search').val()},
                function (html) {
                    $('#' + a.data('spolerid')).html(html);
                }
            );
        }
    });

    //Отправка формы добавления нового сотрудника из вкладки "Сотрудники"
    $('body').on('submit', '.spoler_content #client-form', function () {
        var phone = $('#client-phone');
        var email = $('#client-email');
        var error = false;

        $(this).find('.input_error').removeClass('input_error');

        var phone_number = phone.val();
        if (phone_number == '') {
            phone.addClass('input_error');
            error = true;
        } else {
            $.ajax({
                type: 'POST',
                url: '/client/base/validate-phone',
                async: false,
                timeout: 5000,
                data: {phone: phone_number},
                dataType: "json",
                success: function (json) {
                    if (json === 0) {
                        error = true;
                        phone.addClass('input_error');
                    }
                },
            })
                .fail(function () {
                    error = true;
                    alert("Error validate phone number. Notify to administrator, please.");
                });
        }

        var mail = email.val();
        if (mail != '' && !validMail(mail)) {
            email.addClass('input_error');
            error = true;
        }

        if (error) {
            return false;
        }
    });

    registerOrderOpenInModalEvent('.open_order');

    $(document).on('click', '.js-spoiler', function () {
        $(this).prevAll('.js-full').toggle();
        $(this).prevAll('.js-preview').toggle();
    });

    $('.js-city').on('change', function () {
        var cityId = $(this).val();

        $.get(
            '/city/city/get-city-currency-symbol',
            {city_id: cityId}
        )
            .done(function (data) {
                $('.js-currency').text(data);
            })
            .fail(function (data) {
                $('.js-currency').text('');
                console.log('fail', data);
            });
    });

    $(document).on('click', '.js-active-review', function () {
        var url = $(this).data('url');
        var _this = this;
        $.get(url).done(function (response) {
            if (response) {
                $.pjax.reload({
                    container: "#pjax-reviews-list",
                    url: $(_this).data('reload'),
                    push: false,
                    timeout: 5000,
                });
            }
        });
    });

    $(document).on('click', '.js-not-active-review', function () {
        var url = $(this).data('url');
        var _this = this;
        $.get(url).done(function (response) {
            if (response) {
                $.pjax.reload({
                    container: "#pjax-reviews-list",
                    url: $(_this).data('reload'),
                    push: false,
                    timeout: 5000
                });
            }
        });
    });
});