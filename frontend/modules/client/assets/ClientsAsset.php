<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\client\assets;

use yii\web\AssetBundle;

/**
 * author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ClientsAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/client/assets/js';
    
    public $publishOptions = [
        'forceCopy' => true
    ];

    public $css = [
    ];
    public $js = [
        'functions.js',
        'main.js',
    ];
    public $depends = [
        'app\modules\order\assets\ViewOrderAsset',
    ];
}