(function ($) {
    'use strict';

    var tariffParking = {

        spoilerSelector: '.spoiler',

        init: function () {
            this.registerToggleSpoiler();
            this.registerUpdateOptionList();

            this.submitFixForm();
            this.changeFixForm();


        },

        registerToggleSpoiler: function () {
            this.onToggleSpoiler();
        },

        registerUpdateOptionList: function () {
            this.onUpdateOptionList();
        },

        onToggleSpoiler: function () {
            $(document).off('click', this.spoilerSelector).on('click', this.spoilerSelector, function () {
                if (!$(this).hasClass('locked')) {
                    var spoilerId = $(this).data('spoilerid');
                    var url = $(this).data('url');

                    $('#' + spoilerId).toggle();

                    if (typeof url !== "undefined" && !$(this).hasClass('js-ajax-loaded')) {
                        $(this).addClass('js-ajax-loaded');
                        $.ajax({
                            url: url,
                            success: function (response) {
                                $('#' + spoilerId).html(response);
                            }
                        });
                    }

                }
            });
        },

        onUpdateOptionList: function () {
            $(document).off('pjax:end', '#pjax-AIRPORT').on('pjax:end', '#pjax-AIRPORT', function() {
                $.pjax.reload('#pjax-option-list');
            });

            $(document).off('pjax:end', '#pjax-STATION').on('pjax:end', '#pjax-RAILWAY', function() {
                $.pjax.reload('#pjax-option-list');
            });
        },

        submitFixForm: function () {

            $(document).off('submit', '.js-fix-form').on('submit', '.js-fix-form', function () {

                var form = $(this);

                $.ajax({
                    url: form.attr('action'),
                    type: form.attr('method'),
                    data: form.serialize(),
                    success: function (data) {
                        var selector = form.find('.submit_form').find('.message');

                        if (data['status'] === 1) {
                            selector.attr('style', 'color: green; text-align: left');
                            form.find('.ap_cont').removeClass('input_error');

                            form.find('[type=text]').each(function (index) {
                                var jsClass = $(this).attr('class');
                                $('.' + jsClass).val($(this).val());
                            });

                        } else {
                            selector.attr('style', 'color: red; text-align: left');
                            form.find('.ap_cont').addClass('input_error');
                        }

                        selector.show().text(data['message']);
                    }
                });

                return false;
            });
        },

        changeFixForm: function () {
            $(document).off('change', '.js-fix-form input').on('change', '.js-fix-form input', function () {
                var form = $(this).parents('form').first();
                form.find('.submit_form').find('.message').hide();
                form.find('.ap_cont').removeClass('input_error');

            });
        }
    };

    tariffParking.init();
})(jQuery);
