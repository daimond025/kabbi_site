(function ($) {
    'use strict';

    var tariff = {

        citySelection: '#tariffform-city_id',

        positionSelection: '#tariffform-position_id',
        getPositionListUrl: '/employee/position/get-positions',

        companySelection: '#tariffform-company_ids',
        blockCompanySelection: '.js-row-company',
        getCompanyListUrl: '/client/tariff/get-company-list',

        typeSelector: '#tariffform-type',


        init: function () {
            this.registerChangeCity();
            this.registerChangeType();
            this.initModal();
            this.closeModal();



          window.submitted = false;

          $('#tariff-form').on('beforeSubmit', function () {
            if(window.submitted){
              return false
            }
            $('#tariff-form').find('input[type="submit"]').attr('disabled', 'disabled');
            window.submitted = true;
          });
        },

        registerChangeCity: function () {
            this.onChangeCity();
        },

        registerChangeType: function () {
            this.onChangeType();
        },

        onChangeCity: function () {
            var _this = this;

            $(document).on('change', _this.citySelection, function () {
                var cityId = $(this).val();
                _this.changeCity(cityId);
            });
        },

        onChangeType: function () {
            var _this = this;

            $(document).on('change', _this.typeSelector, function () {
                var type = $(this).val();
                _this.changeTypeVisibility(type);
            });
        },

        changeCity: function (cityId) {
            this.updatePosition(cityId);
            this.updateCompanies(cityId);
        },

        updatePosition: function (cityId) {
            var _this = this;
            $.get(
                _this.getPositionListUrl,
                {city_id: cityId},
                function (json) {
                    if ($.isArray(json)) {
                        var select = $(_this.positionSelection);
                        select.find('option:not(":first")').remove();
                        var options = $();

                        for (var i = 0; i < json.length; i++) {
                            options = options.add($('<option/>').val(json[i]['position_id']).text(json[i]['name']));
                        }

                        select.append(options);

                        select.trigger('update');
                    }
                },
                'json'
            );
        },

        updateCompanies: function (cityId) {
            var _this = this;
            $.ajax({
                url: _this.getCompanyListUrl,
                type: "POST",
                data: {city_id: cityId},
                success: function (result) {
                    var select = $(_this.companySelection);
                    var name = select.prev('input').attr('name');
                    select.empty();

                    for (var key in result) {
                        var currentElement = '<div><label><input type="checkbox" name="' + name + '[]" value="' + key + '">' + result[key] + '</label></div>';
                        select.append(currentElement);
                    }
                }
            });
        },

        changeTypeVisibility: function (type) {
            if (type === 'COMPANY_CORP_BALANCE' || type === 'COMPANY') {
                $(this.blockCompanySelection).show();
            } else {
                $(this.blockCompanySelection).hide();
            }
        },

        initOptionForm: function (formSelector) {
            $('body')
                .off('click', formSelector + ' input[type="submit"]')
                .on('click', formSelector + ' input[type="submit"]', function (e) {
                    e.preventDefault();
                    var form = $(this).parents('form'),
                        id = form.attr('id');

                    var fd = new FormData(form[0]);

                    $.ajax({
                        url: form.attr('action'),
                        type: 'POST',
                        data: fd,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            var htmlData = $.parseHTML(data);
                            var newFrom = $(htmlData).find('#' + id);
                            $('#' + id).html(newFrom.html());
                            select_init();
                            // validyInput.init();
                        }
                    })
                });

            return this;
        },

        initModal: function () {

            var _this = this;

            $('body').on('click', '.js-edit-ex', function (e) {
                e.preventDefault();

                $('.js-modal-ex-hidden').gootaxModal({
                    inline: false,
                    onComplete: function () {
                        select_init();
                        _this.initOptionForm('.js-client-options-form');
                    },
                    onClosed: function () {
                        $(document).find('#tariff-option-form').remove();
                    }
                });
                var link = $(this).attr('href');
                $('.js-modal-ex-hidden').gootaxModal('settings', {url: link}).gootaxModal('open');
            });
        },

        closeModal: function () {
            $(document).on('click', '.close-modal', function (e) {
                $('.js-modal-ex-hidden').gootaxModal('close');
            })
        }
    };

    tariff.init();
})(jQuery);
