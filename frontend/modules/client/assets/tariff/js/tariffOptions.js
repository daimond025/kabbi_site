(function ($) {
    'use strict';

    var tariffOptions = {

        cityAccrualSelector: '#tariffoptionform-city_accrual',
        trackAccrualSelector: '#tariffoptionform-track_accrual',

        init: function () {
            this.registerChangeAccrual();
            this.registerUpdateOptionList()
        },

        registerChangeAccrual: function () {
            this.onChangeAccrual();
        },

        registerUpdateOptionList: function() {
            this.onUpdateOptionList();
        },

        onChangeAccrual: function () {
            var _this = this;

            $(document).off('change', _this.cityAccrualSelector).on('change', _this.cityAccrualSelector, function () {
                _this.changeAccrual('city');
            });

            $(document).off('change', _this.trackAccrualSelector).on('change', _this.trackAccrualSelector, function () {
                _this.changeAccrual('track');
            });
        },

        changeAccrual: function (type) {

            var selector = type === 'city' ? this.cityAccrualSelector : this.trackAccrualSelector;
            var accrual = $(selector).val();


            $(document).find('.' + type)
                .find('.js-accrual-mixed, .js-accrual-interval, .js-accrual-fix, .js-accrual-distance, .js-accrual-time')
                .hide();

            switch (accrual) {
                case 'TIME':
                    $(document).find('.' + type).find('.js-accrual-time').show();
                    break;
                case 'DISTANCE':
                    $(document).find('.' + type).find('.js-accrual-distance').show();
                    break;
                case 'MIXED':
                    $(document).find('.' + type).find('.js-accrual-mixed').show();
                    break;
                case 'FIX':
                    $(document).find('.' + type).find('.js-accrual-fix').show();
                    break;
                case 'INTERVAL':
                    $(document).find('.' + type).find('.js-accrual-interval').show();
                    break;
            }




            $('.ap_cont').each(function(index) {

                var accruals = $(this).data('accruals');

                if(accruals === undefined) {
                    return;
                }

                var accrualsArray = accruals.split(',');
                var exist = false;

                for(var key in accrualsArray) {
                    if(accrualsArray[key] === accrual) {
                        exist = true;
                        break;
                    }
                }

                if (exist) {
                    $(this).find('.' + type).find('input').attr('disabled', false);
                    $(this).removeClass('js-hide-' + type);
                } else {
                    $(this).addClass('js-hide-' + type);
                    $(this).find('.' + type).find('input').attr('disabled', true);
                }


                if ($(this).hasClass('js-hide-city') && ($(this).hasClass('js-hide-track'))) {
                    $(this).hide();
                } else {
                    $(this).show();
                }

            });
        },

        onUpdateOptionList: function() {
            $(document).off('pjax:end', '#pjax-option-form').on('pjax:end', '#pjax-option-form', function() {
                $.pjax.reload('#pjax-option-list');
            });
        }
    };

    tariffOptions.init();
})(jQuery);
