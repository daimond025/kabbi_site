(function ($) {
    'use strict';

    var tariffAdditionalOptions = {

        checkboxSelector: '.check_option [type = checkbox]',
        infoSelector: '.js-additional-option-info',

        init: function () {
            this.registerChangeCheckbox();
            this.registerChangeInput();
        },

        registerChangeCheckbox: function () {
            this.onChangeCheckbox();
        },

        registerChangeInput: function() {
            this.onChangeInput();
        },

        onChangeCheckbox: function () {
            $(document).on('change', this.checkboxSelector, function () {
                var checked = $(this).prop('checked');
                $(this).parents('.check_option').first().find("[type = text]").attr('disabled', !checked);
            });
        },

        onChangeInput: function() {
            var _this = this;
            $(document).on('change', 'input', function () {
                _this.removeInfo();
            });
        },

        removeInfo: function() {
            $(this.infoSelector).remove();
        }
    };

    tariffAdditionalOptions.init();
})(jQuery);
