(function ($) {
    'use strict';

    var tariffInterval = {

        removeItemSelector: '.close_red',
        itemSelector: '.tdad_item',
        itemLastSelector: '.tdad_last',
        numberSelector: '.tdad_number',
        createItemSelector: ".plus_icon",
        firstIntervalSelector: '.first_interval_number',
        secondIntervalSelector: '.second_interval_number',
        plantingIncludeSelector: '.js-planting-include',

        init: function () {
            this.registerRemoveItem();
            this.registerCreateItem();
            this.registerChangeInterval();
        },

        registerRemoveItem: function () {
            this.onRemoveItem();
        },

        registerCreateItem: function() {
            this.onCreateItem();
        },

        registerChangeInterval: function () {
            this.onChangeInterval();
        },

        onRemoveItem: function () {
            var _this = this;
            $(document).off('click', this.removeItemSelector).on('click', this.removeItemSelector, function () {
                var currentItem = $(this).parents(_this.itemSelector).first();
                var prevItem = currentItem.prev(_this.itemSelector);
                var lastItem = currentItem.parent().find(_this.itemLastSelector);
                var lastNumber = lastItem.find(_this.numberSelector).text();

                if(!prevItem.hasClass('first')) {
                    prevItem.find('.tdad_interval').append('<a class="close_red"></a>');
                }
                lastItem.find(_this.firstIntervalSelector).val(currentItem.find(_this.firstIntervalSelector).val());
                lastItem.find(_this.numberSelector).text(Number(lastNumber) - 1);

                currentItem.remove();
            })
        },

        onCreateItem: function() {
            var _this = this;

            $(document).off('click', this.createItemSelector).on('click', this.createItemSelector, function () {

                $(this).parent().find('.close_red').remove();
                var lastItem = $(this).parent().find(_this.itemLastSelector);
                var firstItem = $(this).parent().find('.first');
                var lastNumber = lastItem.find(_this.numberSelector).text();

                var firstItemClone = firstItem.clone();
                firstItemClone.removeClass('first');
                firstItemClone.find('input').val('0');
                firstItemClone.find(_this.firstIntervalSelector).val(lastItem.find(_this.firstIntervalSelector).val());
                firstItemClone.find(_this.secondIntervalSelector).val(lastItem.find(_this.firstIntervalSelector).val());
                firstItemClone.find(_this.numberSelector).text(lastNumber);
                firstItemClone.find('.tdad_interval').append('<a class="close_red"></a>');

                lastItem.find(_this.numberSelector).text(Number(lastNumber) + 1);

                lastItem.before(firstItemClone);
            });
        },

        onChangeInterval: function() {
            var _this = this;

            $(document).on('change', this.firstIntervalSelector, function () {

                var currentItem = $(this).parents(_this.itemSelector).first();

                if (currentItem.hasClass('first')) {
                    var isCity = currentItem.parents('.city').length > 0;
                    var area = isCity ? 'city' : 'track';
                    $(document).find('.' + area).find(_this.plantingIncludeSelector).val($(this).val());
                } else {
                    var prevItem = currentItem.prev(_this.itemSelector);
                    prevItem.find(_this.secondIntervalSelector).val($(this).val());
                }

            });


            $(document).on('change', this.secondIntervalSelector, function () {

                var currentItem = $(this).parents(_this.itemSelector).first();
                currentItem.next(_this.itemSelector).find(_this.firstIntervalSelector).val($(this).val());

            });
        }
    };

    tariffInterval.init();
})(jQuery);
