<?php

namespace frontend\modules\client\assets;

use yii\web\AssetBundle;

class BonusAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/client/assets/bonus';

    public $publishOptions = [
        'forceCopy' => true
    ];

    public $css = [
    ];
    public $js = [
        'js/bonus.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}