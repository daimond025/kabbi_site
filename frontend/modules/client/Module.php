<?php

namespace app\modules\client;

use app\components\behavior\ControllerBehavior;
use yii\helpers\ArrayHelper;

/**
 * Class Module
 * @package app\modules\client
 *
 * @mixin ControllerBehavior
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\client\controllers';

    public function init()
    {
        parent::init();

        \Yii::configure($this, require(__DIR__ . '/config.php'));
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        if($this->id == 'cabinet') {
            return parent::behaviors();
        }

        return ArrayHelper::merge([
            'common' => [
                'class' => ControllerBehavior::className(),
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if($this->id != 'cabinet') {
            $this->commonBeforeAction();
        }

        return parent::beforeAction($action);
    }
}
