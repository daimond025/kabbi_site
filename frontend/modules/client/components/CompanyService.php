<?php

namespace frontend\modules\client\components;

use app\modules\client\models\ClientCompany;
use yii\helpers\ArrayHelper;

class CompanyService
{
    protected $_companyList = [];

    public function getCompanyList($tenantId, $cityId)
    {
        if (!ArrayHelper::getValue($this->_companyList, "$tenantId.$cityId", false)) {
            $companies = ClientCompany::findAll([
                'tenant_id' => $tenantId,
                'city_id'   => $cityId,
            ]);
            $this->_companyList[$tenantId][$cityId] = ArrayHelper::map($companies, 'company_id', 'name');
        }

        return ArrayHelper::getValue($this->_companyList, "$tenantId.$cityId", []);
    }
}