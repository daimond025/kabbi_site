<?php

namespace frontend\modules\client\components;

use common\modules\employee\models\position\Position;
use frontend\modules\car\models\CarClass;
use frontend\modules\client\models\tariff\TariffForm;
use frontend\modules\client\models\tariff\TariffGroup;
use frontend\modules\client\models\tariff\TariffOptionRecord;
use frontend\modules\client\models\tariff\TariffOptionType;
use frontend\modules\client\models\tariff\TariffOptionTypeRecord;
use yii\helpers\ArrayHelper;

class TariffService
{
    /**
     * Вернуть тариф по его идентификатору и арендатору
     *
     * @param integer $tenantId
     * @param integer $tariffId
     *
     * @return TariffForm
     */
    public function getTariffForm($tenantId, $tariffId)
    {
        /** @var TariffForm $tariff */
        $tariff = TariffForm::findOne([
            'tenant_id' => $tenantId,
            'tariff_id' => $tariffId,
        ]);

        return $tariff;
    }

    /**
     * Вернуть профессии арендатора в филиале
     *
     * @param $tenantId
     * @param $cityId
     *
     * @return array
     */
    public function getPositionList($tenantId, $cityId)
    {
        /** @var Position[] $positions */
        $positions = Position::find()
            ->alias('p')
            ->joinWith(['cities c'], false)
            ->where([
                'c.tenant_id' => $tenantId,
                'c.city_id'   => $cityId,
            ])
            ->all();

        return ArrayHelper::map($positions, 'position_id', function (Position $model) {
            return t('employee', $model->name);
        });
    }


    public function getCarClassMap()
    {
        //        ArrayHelper::map(CarClass::find()->all(), 'class_id', function ($array) {
        //            return t('car', $array['class']);

        $carClass = CarClass::find()->all();

        return ArrayHelper::map($carClass, 'class_id', function (CarClass $model) {
            return t('car', $model->class);
        });
    }

    public function getGroupList($tenantId)
    {
        $groups = TariffGroup::find()
            ->where(['tenant_id' => $tenantId])
            ->orderBy([
                'sort' => SORT_ASC,
            ])
            ->all();

        return ArrayHelper::map($groups, 'group_id', 'name');
    }

    public function getOptionTypeList($tariffId)
    {
        $optionTypes = TariffOptionType::find()
            ->alias('type')
            ->joinWith([
                'tariffOptionActiveDate activeDate',
                'tariffOption option',
            ])
            ->where([
                'type.tariff_id' => $tariffId,
                'type.type'      => [TariffOptionTypeRecord::TYPE_CURRENT, TariffOptionTypeRecord::TYPE_EXCEPTIONS],
            ])
            ->orderBy([
                'type.sort'    => SORT_ASC,
                'type.type_id' => SORT_ASC,
            ])
            ->all();

        return ArrayHelper::map($optionTypes, 'type_id', function (TariffOptionType $model) {

            $activeDate = ArrayHelper::getColumn($model->tariffOptionActiveDate, function ($model) {
                return $model->getName();
            });

            $cityTariff  = '';
            $trackTariff = '';
            $isAirport   = false;
            $isStation   = false;

            foreach ($model->tariffOption as $option) {
                switch ($option['area']) {
                    case TariffOptionRecord::AREA_CITY:
                        $cityTariff = $option->getTypeName();
                        break;
                    case TariffOptionRecord::AREA_TRACK:
                        $trackTariff = $option->getTypeName();
                        break;
                    case TariffOptionRecord::AREA_AIRPORT:
                        $isAirport = true;
                        break;
                    case TariffOptionRecord::AREA_RAILWAY:
                        $isStation = true;
                        break;
                }
            }

            return [
                'typeId'      => $model->type_id,
                'sort'        => $model->sort,
                'type'        => $model->type,
                'cityTariff'  => $cityTariff,
                'trackTariff' => $trackTariff,
                'isAirport'   => $isAirport,
                'isStation'   => $isStation,
                'activeDate'  => $activeDate,
            ];
        });
    }

}