<?php

namespace frontend\modules\client\components;

use app\modules\tariff\models\TaxiTariff;
use frontend\modules\bonus\models\BonusSystem;
use frontend\modules\employee\components\position\PositionService;
use frontend\modules\employee\Module;
use yii\base\Object;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class ClientBonusService
 * @package frontend\modules\client\components
 */
class ClientBonusService extends Object
{
    /* @var PositionService */
    private $positionService;

    public function __construct(PositionService $positionService, array $config = [])
    {
        $this->positionService = $positionService;
        $this->positionService->setTenantId(user()->tenant_id);

        parent::__construct($config);
    }

    /**
     * Getting bonus systems
     * @return array
     */
    public function getBonusSystems()
    {
        return ArrayHelper::map(BonusSystem::find()->all(), 'id', 'name');
    }

    /**
     * Getting positions list
     *
     * @param null $cityId
     *
     * @return array
     */
    public function getPositions($cityId = null)
    {
        $positions = $this->positionService->getPositionsByCity($cityId);

        return empty($positions) ? [] : ArrayHelper::map($positions, 'position_id', 'name');
    }

    /**
     * Getting positions list
     *
     * @return array
     */
    public function getAllPositions()
    {
        $positions = $this->positionService->getAllPositions();

        return empty($positions) ? [] : ArrayHelper::map($positions, 'position_id', 'name');
    }

    /**
     * Getting position list with current position
     *
     * @param int $positionId
     * @param int $cityId
     *
     * @return array
     */
    public function getPositionsWithCurrent($positionId, $cityId = null)
    {
        $positions              = $this->getPositions($cityId);
        $all                    = $this->getAllPositions();
        $positions[$positionId] = $all[$positionId];

        return $positions;
    }

    /**
     * Getting positions with car class
     *
     * @param int $positionId
     *
     * @return array
     */
    public function isPositionHasCar($positionId)
    {
        $position = $this->positionService->getPositionById($positionId);

        return !empty($position) && (int)$position->has_car === 1;
    }

    /**
     * Getting car classes
     *
     * @param integer $cityId
     * @param integer $positionId
     *
     * @return array
     */
    public function getCarClasses($cityId, $positionId)
    {
        /* @var Module */
        $module = \Yii::$app->getModule('employee');
        if ($module) {
            $tariffs = $module->clientTariff->getRepository()->getTariffsGroupByClass($cityId, $positionId, -1);

            return ArrayHelper::map($tariffs, 'class_id', function ($model) {
                return t('car', $model['class']);
            });
        } else {
            return [];
        }
    }

    /**
     * Getting client tariff list
     *
     * @param integer $cityId
     * @param integer $positionId
     * @param integer $classId
     * @param bool    $onlyActive
     *
     * @return array
     */
    public function getClientTariffs($cityId, $positionId, $classId = null, $onlyActive = true)
    {
        // replace to client tariff service
        $tariffs = TaxiTariff::find()
            ->select(['t.tariff_id', 't.name', 't.block', 't.sort'])
            ->alias('t')
            ->joinWith([
                'tariffHasCity c' => function (Query $subQuery) use ($cityId) {
                    $subQuery->where(['c.city_id' => $cityId]);
                },
            ], false)
            ->where([
                't.tenant_id'   => user()->tenant_id,
                't.position_id' => $positionId,
            ])
            ->andFilterWhere([
                't.block'    => $onlyActive ? 0 : null,
                't.class_id' => $classId,
            ])
            ->asArray()
            ->orderBy(['t.sort' => SORT_ASC, 't.name' => SORT_ASC])
            ->all();

        return empty($tariffs) ? [] : $tariffs;
    }
}

