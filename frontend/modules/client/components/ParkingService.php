<?php

namespace frontend\modules\client\components;

use app\modules\parking\models\Parking;
use yii\helpers\ArrayHelper;

class ParkingService
{
    protected $_parkingList = [];

    public function getParkingList($cityId)
    {

        if (!ArrayHelper::keyExists($cityId, $this->_parkingList)) {
            $models = Parking::find()
                ->select(['parking_id', 'name', 'type'])
                ->where([
                    'tenant_id' => user()->tenant_id,
                    'city_id'   => $cityId,
                    'type'      => [
                        Parking::TYPE_CITY,
                        Parking::TYPE_OUTCITY,
                        Parking::TYPE_AIRPORT,
                        Parking::TYPE_STATION,
                    ],
                    'is_active' => 1,
                ])
                ->orderBy('sort')
                //            ->limit(100)
                ->all();

            $this->_parkingList[$cityId] = ArrayHelper::map($models, 'parking_id', 'name', 'type');
        }

        return ArrayHelper::getValue($this->_parkingList, $cityId, []);
    }
}