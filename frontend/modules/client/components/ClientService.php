<?php

namespace frontend\modules\client\components;

use app\modules\client\models\ClientReview;
use app\modules\client\models\ClientReviewRaiting;

class ClientService
{
    public function setActiveReview($tenantId, $reviewId)
    {
        return $this->changeNotActiveReview($tenantId, $reviewId, true);
    }

    public function setNotActiveReview($tenantId, $reviewId)
    {
        return $this->changeNotActiveReview($tenantId, $reviewId, false);
    }

    private function changeNotActiveReview($tenantId, $reviewId, $isActive)
    {
        /** @var ClientReview $review */
        $review = ClientReview::find()
            ->alias('review')
            ->joinWith('client client')
            ->where([
                'review.review_id' => $reviewId,
                'client.tenant_id' => $tenantId,
            ])
            ->one();

        if (!$review) {
            return false;
        }

        if ($review->isActive === $isActive) {
            return true;
        }

        $review->isActive = $isActive;

        $transaction = app()->db->beginTransaction();

        if ($review->save()) {
            /** @var ClientReviewRaiting $rating */
            $rating = ClientReviewRaiting::find()
                ->alias('rating')
                ->joinWith('client client')
                ->where([
                    'client.client_id' => $review->client_id,
                    'client.tenant_id' => $tenantId,
                ])
                ->one();

            if ($rating) {
                switch ($review->rating) {
                    case 1:
                        $attribute = 'one';
                        break;
                    case 2:
                        $attribute = 'two';
                        break;
                    case 3:
                        $attribute = 'three';
                        break;
                    case 4:
                        $attribute = 'four';
                        break;
                    case 5:
                        $attribute = 'five';
                        break;
                    default:
                        $attribute = null;
                }

                if ($attribute) {
                    $rating->$attribute = $isActive ? $rating->$attribute + 1 : $rating->$attribute - 1;
                    $rating->count      = $isActive ? $rating->count + 1 : $rating->count - 1;
                }

                if ($rating->save()) {
                    $transaction->commit();

                    return true;
                }
            }
        }
        $transaction->rollBack();

        return false;
    }
}
