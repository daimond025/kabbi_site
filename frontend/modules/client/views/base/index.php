<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var string $pjaxId */
/* @var $this yii\web\View */
/* @var array $cityList */
/* @var array $positionList */
/* @var $dataProvider yii\data\ArrayDataProvider */
/* @var $searchModel \frontend\modules\employee\models\groups\WorkerGroupSearch */

$this->title = Yii::t('app', 'Base of clients');

if (app()->user->can('clients')):?>
    <?= Html::a(t('app', 'Add'), ['create'], ['class' => 'button', 'style' => 'float: right;']); ?>
    <?php if ($dataProvider->getCount()): ?>
        <div style="float: right; position: relative; top: 5px;">
            <div class="context_menu_wrap">
                <a class="context_menu a_sc"><?= t('client', 'Export') ?></a>
                <div class="context_menu_content b_sc">
                    <a href="<?= Url::to('/client/base/dump') ?>"><?= t('client', 'Download to Excel') ?></a>
                </div>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>
<h1><?= Html::encode($this->title) ?></h1>
<section class="main_tabs">
    <?= $this->render('_grid', compact('searchModel', 'dataProvider', 'cityList', 'pjaxId')) ?>
</section>