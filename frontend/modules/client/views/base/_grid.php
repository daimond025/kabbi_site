<?php

/**
 * @var $searchModel ClientSearch
 * @var $cityList array
 * @var $dataProvider ActiveDataProvider
 */

use app\modules\client\models\Client;
use app\modules\client\models\ClientSearch;
use frontend\widgets\filter\Filter;
use frontend\widgets\LinkPager;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

$clientDetailViewUrl = getValue($clientDetailViewUrl, '/client/base/update');

$filter = Filter::begin([
    'pjaxId' => $pjaxId,
    'model'  => $searchModel,
]);
echo $filter->checkboxList('city_id', $cityList);
echo $filter->input('input_search', ['placeHolder' => t('client', 'Filter by name or phone')]);
Filter::end();

Pjax::begin(['id' => $pjaxId]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'       => "{items}\n{pager}",
    'tableOptions' => [
        'class' => 'people_table',
    ],
    'pager'        => [
        'class'         => LinkPager::class,
        'prevPageLabel' => t('app', 'Prev'),
        'nextPageLabel' => t('app', 'Next'),
    ],
    'columns'      => [
        [
            'attribute'      => 'fullName',
            'label'          => t('client', 'Client'),
            'contentOptions' => function ($model, $key, $index, $column) {
                return ['class' => 'pt_fio'];
            },
            'content'        => function (Client $model) use ($clientDetailViewUrl) {
                $innerHtml = Html::tag(
                    'span',
                    $model->isFileExists($model->photo) ?
                        $model->getPictureHtml(
                            Html::encode($model->photo),
                            false
                        ) : '',
                    ['class' => 'pt_photo']
                );
                $name_innerHtml = [];
                if (!empty($model->fullName)) {
                    $name_innerHtml[] = Html::tag('span', Html::encode($model->fullName), ['class' => 'pt_fios']);
                }
                if (!empty($model->clientPhones)) {
                    foreach ($model->clientPhones as $phone) {
                        $name_innerHtml[] = Html::tag('span', Html::encode($phone->value));
                    }
                }
                $innerHtml .= implode('<br>', $name_innerHtml);

                return Html::a($innerHtml, [$clientDetailViewUrl, 'id' => $model->client_id], ['data-pjax' => 0]);
            },
            'headerOptions'  => ['class' => 'pt_fio'],
        ],
        [
            'attribute'     => 'client_id',
            'label'         => t('client', 'Orders'),
            'content'       => function (Client $model) {
                $innerHtml = Html::tag('span', Html::encode($model->success_order), ['class' => 'mo_complete']);

                $fail_innerHtml = Html::tag('span', Html::encode($model->fail_client_order));
                $fail_innerHtml .= Html::tag('b', t('client', 'Failure order by client'));
                $fail_innerHtml .= Html::tag('span', Html::encode($model->fail_worker_order));
                $fail_innerHtml .= Html::tag('b', t('client', 'Failure order by driver'));
                $fail_innerHtml .= Html::tag('span', Html::encode($model->fail_dispatcher_order));
                $fail_innerHtml .= Html::tag('b', t('client', 'Failure order by dispatcher'));

                $error_innerHtml = Html::tag('b', $model->getFailOrderCount());
                $error_innerHtml .= Html::tag('span', $fail_innerHtml, ['class' => 'moe_tt']);

                $innerHtml .= '&nbsp;';

                $innerHtml .= Html::tag('span', $error_innerHtml, ['class' => 'mo_error']);

                return $innerHtml;
            },
            'headerOptions' => ['style' => 'width: 16%'],
        ],
        [
            'attribute'     => 'activeFieldValue',
            'label'         => t('client', 'Activity'),
            'headerOptions' => ['style' => 'width: 16%'],
        ],
        [
            'attribute'     => 'city_id',
            'label'         => t('client', 'City'),
            'content'       => function ($model) use ($cityList) {
                if (!empty($model->clientPhones[0])) {
                    $phone_innerHtml = Html::a(
                        '<span></span>',
                        'tel:+' . $model->clientPhones[0]->value,
                        ['class' => 'pt_call']
                    );
                    $innerHtml = Html::tag('div', $phone_innerHtml, ['class' => 'pt_buttons']);
                } else {
                    $innerHtml = '';
                }
                $innerHtml .= $cityList[$model->city_id];

                return $innerHtml;
            },
            'headerOptions' => ['style' => 'width: 23%'],
        ],
    ],
]);

Pjax::end();
