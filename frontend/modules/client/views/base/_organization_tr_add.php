<?php
/* @var $this yii\web\View */
/* @var $companies app\modules\client\models\ClientCompany */

use yii\helpers\Html;
use yii\helpers\Url;

?>

<? foreach ($data as $company): ?>
    <tr>
        <td>
            <a href="<?= Url::to(['/client/company/update', 'id' => $company->company_id]) ?>">
                <span class="pt_photo"><?= $company->isFileExists($company->logo) ? $company->getPictureHtml(Html::encode($company->logo)) : ''; ?></span>
                <span class="pt_fios"><?= Html::encode($company->name); ?></span>
            </a>
        </td>
        <td><?= $company->getActiveFieldValue($company->block) ?></td>
        <td><?= Html::encode($company->city->{name . getLanguagePrefix()}); ?></td>
        <td>
            <div style="text-align: right">
                <a href="<?= Url::to(['/client/base/organization-add', 'company_id' => $company->company_id]) ?>" class="pt_add"><?= t('client', 'Choose') ?></a>
            </div>
        </td>
    </tr>
<? endforeach; ?>