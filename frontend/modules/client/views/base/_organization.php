<?php
/* @var $this yii\web\View */
$view = isset($view) ? $view : '_organization_tr';

if ((!empty($data) && $view != '_organization_tr') || !empty($data->companies)):
    ?>
    <table class="styled_table" style="margin-bottom: 30px;">
        <tr data-view="<?= $view ?>">
            <th class="pt_fio" style="width: 35%;"><a href="#"><?= t('client', 'Organization') ?> <span data-type="name" class="pt_down"></span></a></th>
            <th style="width: 14%;"><?= t('client', 'Activity') ?></th>
            <th style="width: 12%;"><?= t('client', 'City') ?></th>
            <th style="width: 10%;"></th>
        </tr>
        <?= $this->render($view, ['data' => $data]) ?>
    </table>
<? else: ?>
    <p><?= t('app', 'Empty') ?></p>
<? endif; ?>

