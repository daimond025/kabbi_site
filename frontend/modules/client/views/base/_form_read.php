<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $client app\modules\client\models\Client */
/* @var $form yii\widgets\ActiveForm */
$this->registerJs('phone_mask_init();');
if (isset($jsInit) && $jsInit) {
    $this->registerJs('select_init();');
}
?>
<div class="non-editable">

    <!--Город-->

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($client, 'city_id') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($client->city_id) ? Html::encode($dataForm['CITY'][$client->city_id]) : t('car', 'Choose city') ?></div>
        </div>
    </section>
    <section class="row" id="logo">
        <div class="row_label"></div>
        <div class="row_input" style="background: none;">
            <div class="input_file_logo <!--file_loader-->">
                   <?php if (file_exists(Yii::getAlias('@app') . '/web/' . '/upload/' . user()->tenant_id. '/thumb_' . Html::encode($client->photo))): ?>
                <a class="input_file_logo logo cboxElement" href="<?= '/upload/' . user()->tenant_id . '/' . Html::encode($client->photo) ?>" style="border: none">
                    <div class="row_input" style="background: none"><span><img src="<?= '/upload/' . user()->tenant_id . '/' . Html::encode($client->photo) ?>"></span></div>
                </a>
                <? endif ?>
            </div>
        </div>
    </section>

    <!--ФИО-->
    <section class="row">
        <div class="row_label">
            <div class="row_input">
                <?= Html::encode($client->getAttributeLabel('last_name')); ?>
                <?= Html::encode($client->getAttributeLabel('name')); ?>
                <?= Html::encode($client->getAttributeLabel('second_name')); ?>
            </div>
        </div>
        <div class="row_input">
            <div class="grid_3">
                <div class="row_input"><?= !empty($client->last_name) ? Html::encode($client->last_name) : '<div class="row_input"></div>'?></div>
                <div class="row_input"><?= !empty($client->name) ? Html::encode($client->name) : '<div class="row_input"></div>' ?></div>
                <div class="grid_item"><?= !empty($client->second_name) ? Html::encode($client->second_name) : '<div class="row_input"></div>' ?></div>
            </div>
        </div>
    </section>

    <!--Дата рождения-->
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($client, 'birth') ?></div>
        <div class="row_input">
            <div class="grid_3">
                <div class="grid_item">
                    <div class="row_input"><?= !empty($client->birth_day) ? Html::encode($client->birth_day) : '<div class="row_input"></div>' ?></div>
                </div>
                <div class="grid_item">
                    <div class="row_input"><?= !empty($client->birth_month) ? Html::encode($dataForm['BIRTHDAY']['MONTH'][$client->birth_month]) : '<div class="row_input"></div>' ?></div>
                </div>
                <div class="grid_item">
                    <div class="row_input"><?= !empty($client->birth_year) ? Html::encode($client->birth_year) : '<div class="row_input"></div>' ?></div>
                </div>
            </div>
        </div>
    </section>

    <!--Моб. телефон-->
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($client, 'phone') ?></div>
        <div class="row_input">
            <div class="input_with_icon">
                <div class="input_with_icon"><?= !empty($client->phone) ? Html::encode($client->phone[0]) : '<div class="row_input"></div>' ?></div>
                <span></span>
            </div>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($client, 'email') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($client->email) ? Html::encode($client->email) : '<div class="row_input"></div>' ?></div>
        </div>
    </section>
    <?if(!$client->isNewRecord):?>
        <label><?= Html::activeCheckbox($client, 'black_list', ['label' => null, 'disabled' => 'disabled']) ?>
            <b>
                <?= t('client', 'Add to black list') ?>
            </b>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint', 'Add the customer to the black list and then he will not be able to place an order through the mobile application and site. When ordering through the control room, information appears that the customer is on the black list.'); ?>
                </span>
            </a>
        </label>
    <?endif?>
</div>