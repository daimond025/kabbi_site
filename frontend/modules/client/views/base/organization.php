<?php
/* @var $this yii\web\View */
/* @var $data app\modules\client\models\Client */
?>
<div class="ajax">
    <? if (!empty($data->companies)): ?>
        <?= $this->render('_organization', ['data' => $data]) ?>
    <? else: ?>
        <p><?= t('app', 'Empty') ?></p>
    <? endif ?>
</div>
<?if(app()->user->can('clients')):?>
    <h2><?= t('client', 'Select another company') ?></h2>
    <div class="grid_3" style="overflow: hidden;">
        <div class="grid_item">
            <input data-client="<?= $data->client_id ?>" id="client_company_search" type="text" placeholder="<?= t('app', 'Filter') ?>" data-search="<?= \yii\helpers\Url::to(['/client/base/organization-search/', 'id' => get('id')]) ?>" />
        </div>
        <div class="grid_item" style="height: 50px;">

        </div>
        <div class="grid_item" style="height: 50px;">

        </div>
    </div>
    <div id="seacrh_company_result"></div>
<?endif?>