<?php
/* @var $this yii\web\View */
/* @var $client app\modules\client\Client */
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('client', 'Add client');
?>
<div class="bread"><a href="<?= Url::toRoute('/client/base/list')?>"><?= t('app', 'Base of clients')?></a></div>
<h1><?= Html::encode($this->title) ?></h1>
<section class="main_tabs">
    <div class="tabs_content">
        <div class="active" id="t01">
            <?php
           $form = app()->user->can('clients') ? '_form' : '_form_read';
            echo $this->render($form, [
                'client' => $client,
                'dataForm' => $dataForm,
            ]) ?>
        </div>
    </div>
</section>