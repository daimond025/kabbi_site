<?= \frontend\widgets\transaction\Transaction::widget([
        'action' => '/client/base/transaction-search',
        'transactions' => $transactions,
        'transactionClass' => \app\modules\client\models\ClientTransaction::className(),
        'balance' => $clientBalance,
        'has_transaction' => $has_transaction,
        'time_offset' => $time_offset
]);?>