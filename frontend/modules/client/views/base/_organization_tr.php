<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\client\models\ClientHasCompany;
$client_id = $data->client_id;
?>

<? foreach ($data->companies as $company): ?>
    <tr>
        <td>
            <a href="<?= Url::to(['/client/company/update', 'id' => $company->company_id]) ?>">
                <span class="pt_photo"><?= $company->isFileExists($company->logo) ? $company->getPictureHtml(Html::encode($company->logo)) : ''; ?></span>
                <span class="pt_fios"><?= Html::encode($company->name); ?></span>
            </a>
            <? if(app()->user->can('clients')) : ?>
                <span class="auto_num">
                    <label>
                        <?= Html::checkbox('role', $company->isAdmin($client_id), ['id' => 'role' . $company->company_id, 'class' => 'js-role', 'data-client_id' => $client_id, 'data-company_id' => $company->company_id]); ?><?= t('client', 'Administrartor'); ?>
                    </label>
                </span>
            <? endif; ?>
            <? /*
              <span data-url="<?=Url::to(['/client/company/change-stuff-rights', 'client' => $data->client_id, 'company' => $company->company_id])?>" class="auto_num"><label><input data-type="booking" type="checkbox" <?if($company->clientHasCompanies[0]->booking):?>checked="checked"<?endif?> /> <?=t('client', 'Booking')?></label><label><input <?if($company->clientHasCompanies[0]->driving):?>checked="checked"<?endif?> data-type="driving" type="checkbox"/> <?=t('client', 'Driving')?></label></span> */ ?>
        </td>
        <td><?= $company->getActiveFieldValue($company->block) ?></td>
        <td><?= Html::encode($company->city->{name . getLanguagePrefix()}); ?></td>
        <td>
            <div style="text-align: right">
                <?php
                if(app()->user->can('clients'))
                echo '<a data-view="_organization" href="' . Url::to(['/client/base/organization-remove', 'client' => $data->client_id, 'company' => $company->company_id]) .' " class="pt_del">'. t('client', 'Remove') .'</a>';
                ?>
            </div>
        </td>
    </tr>
<? endforeach; ?>
