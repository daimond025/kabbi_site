<?php
/* @var $this yii\web\View */
/* @var $client app\modules\client\models\Client */
/* @var $bonusAccountKind int */
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\balance\models\Account;

$clientFullName = $client->getFullName();
$this->title    = empty($clientFullName) ? $client->phone[0] : $clientFullName;
app\modules\client\assets\ClientsAsset::register($this);
\frontend\assets\LeafletAsset::register($this);
\frontend\modules\balance\assets\BalanceAsset::register($this);
$this->registerJs('balanceOperationInit();');
?>

<div class="bread"><a href="<?= Url::toRoute('/client/base/list') ?>"><?= t('app', 'Base of clients') ?></a></div>
<div class="pt_buttons" style="position: relative; top: 10px;">
    <a href="tel:+<?= Html::encode($client->phone[0]); ?>" class="pt_call"><span></span></a>
</div>
<h1><?= Html::encode($this->title) ?></h1>
<section class="main_tabs">
    <div class="tabs_links">
        <ul>
            <li class>
                <a class="t01" href="#profile">
                    <?= t('client', 'Profile') ?>
                </a>
            </li>
            <li class>
                <a class="t02" href="#orders"
                   data-href="<?= Url::to(['/client/base/orders', 'id' => $client->client_id]) ?>">
                    <?= t('client', 'Orders') ?>
                </a>
            </li>
            <li>
                <a class="t03" href="#client-bonus" data-href="<?= Url::to([
                    '/balance/balance/show-balance',
                    'id'      => $client->client_id,
                    'kind_id' => $bonusAccountKind,
                    'city_id' => $client->city_id,
                ]) ?>">
                    <?= t('client', 'Bonuses') ?>
                </a>
            </li>
            <li>
                <a class="t04" href="#reviews"
                   data-href="<?= Url::to(['/client/base/reviews', 'id' => $client->client_id]) ?>">
                    <?= t('client', 'Reviews') ?>
                </a>
            </li>
            <li>
                <a href="#balance" data-href="<?= Url::to([
                    '/balance/balance/show-balance',
                    'id'      => $client->client_id,
                    'kind_id' => Account::CLIENT_KIND,
                    'city_id' => $client->city_id,
                ]) ?>" class="t05">
                    <?= t('client', 'Balance') ?>
                </a>
            </li>
            <li>
                <a class="t06" href="#corp_services"
                   data-href="<?= Url::to(['/client/base/organization', 'id' => $client->client_id]) ?>">
                    <?= t('client', 'Corporate Services') ?>
                </a>
            </li>
            <li>
                <a class="t07" href="#promocodes"
                   data-href="<?= Url::to(['/promocode/promo-reports/promo-report-client', 'id' => get('id')]) ?>"><?= t('promo',
                        'Promo codes') ?>
                </a>
            </li>
            <li>
                <a class="t08" href="#blocking"
                   data-href="<?= Url::to(['/employee/worker/blocked-workers', 'id' => get('id')]) ?>"><?= t('client',
                        'Blocking') ?>
                </a>
            </li>
        </ul>
    </div>
    <div class="tabs_content">
        <div id="t01">
            <?php
            $form = app()->user->can('clients') ? '_form' : '_form_read';
            echo $this->render($form, ['client' => $client, 'dataForm' => $dataForm])
            ?>
        </div>
        <div id="t02">
        </div>
        <div id="t03">
        </div>
        <div id="t04">
        </div>
        <div id="t05">
        </div>
        <div id="t06"></div>
        <div id="t07"></div>
        <div id="t08"></div>
    </div>
</section>
