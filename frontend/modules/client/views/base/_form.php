<?php

use frontend\widgets\file\Logo;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $client app\modules\client\models\Client */
/* @var $form yii\widgets\ActiveForm */
$this->registerJs('phone_mask_init();');
if ($jsInit) {
    $this->registerJs('select_init();');
}
?>

<? $form = ActiveForm::begin([
    'id'                     => 'client-form',
    'errorCssClass'          => 'input_error',
    'successCssClass'        => 'input_success',
    'enableClientValidation' => true,
    'validateOnBlur'         => false,
    'options'                => ['enctype' => 'multipart/form-data'],
]); ?>

<!--Город-->
<section class="row">
    <?= $form->field($client, 'city_id')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($client, 'city_id') ?></div>
    <div class="row_input">
        <?= Html::activeDropDownList($client, 'city_id', $dataForm['CITY'], [
            'data-placeholder' => !empty($client->city_id) ? Html::encode($dataForm['CITY'][$client->city_id]) : t('car',
                'Choose city'),
            'class'            => 'default_select',
        ]) ?>
    </div>
    <?= $form->field($client, 'city_id')->end(); ?>
</section>

<!--Фото-->
<? if ($withLogo !== false && !$client->isNewRecord): ?>
    <?= Logo::widget([
        'form'         => $form,
        'model'        => $client,
        'attribute'    => 'photo',
        'ajax'         => true,
        'uploadAction' => Url::to(['/client/base/upload-logo/', 'entity_id' => get('id')]),
    ]) ?>
<? endif ?>

<!--ФИО-->
<section class="row">
    <div class="row_label">
        <label <? if ($client->isAttributeRequired('full_name')): ?>class="required"<? endif ?>>
            <?= t('client', 'Full name'); ?>
        </label>&nbsp;
    </div>
    <div class="row_input">
        <div class="grid_3">
            <?= $form->field($client, 'last_name')->begin(); ?>
            <div class="grid_item"><?= Html::activeTextInput($client, 'last_name', ['dir' => 'auto']) ?></div>
            <?= $form->field($client, 'last_name')->end(); ?>
            <?= $form->field($client, 'name')->begin(); ?>
            <div class="grid_item"><?= Html::activeTextInput($client, 'name', ['dir' => 'auto']) ?></div>
            <?= $form->field($client, 'name')->end(); ?>
            <?= $form->field($client, 'second_name')->begin(); ?>
            <div class="grid_item"><?= Html::activeTextInput($client, 'second_name', ['dir' => 'auto']) ?></div>
            <?= $form->field($client, 'second_name')->end(); ?>
        </div>
    </div>
</section>

<!--Дата рождения-->
<section class="row">
    <div class="row_label"><?= Html::activeLabel($client, 'birth') ?></div>
    <div class="row_input">
        <div class="grid_3">
            <?= $form->field($client, 'birth_day')->begin(); ?>
            <div class="grid_item">
                <?= Html::activeDropDownList($client, 'birth_day', $dataForm['BIRTHDAY']['DAY'], [
                    'data-placeholder' => !empty($client->birth_day) ? Html::encode($client->birth_day) : t('user',
                        'Day'),
                    'class'            => 'default_select',
                ]); ?>
            </div>
            <?= $form->field($client, 'birth_day')->end(); ?>
            <?= $form->field($client, 'birth_month')->begin(); ?>
            <div class="grid_item">
                <?= Html::activeDropDownList($client, 'birth_month', $dataForm['BIRTHDAY']['MONTH'], [
                    'data-placeholder' => !empty($client->birth_month) ? Html::encode($dataForm['BIRTHDAY']['MONTH'][$client->birth_month]) : t('user',
                        'Month'),
                    'class'            => 'default_select',
                ]); ?>
            </div>
            <?= $form->field($client, 'birth_month')->end(); ?>
            <?= $form->field($client, 'birth_year')->begin(); ?>
            <div class="grid_item">
                <?= Html::activeDropDownList($client, 'birth_year', $dataForm['BIRTHDAY']['YEAR'], [
                    'data-placeholder' => !empty($client->birth_year) ? Html::encode($client->birth_year) : t('user',
                        'Year'),
                    'class'            => 'default_select',
                ]); ?>
            </div>
            <?= $form->field($client, 'birth_year')->end(); ?>
        </div>
    </div>
</section>

<!--Моб. телефон-->
<section class="row">
    <?= $form->field($client, 'phone')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($client, 'phone') ?></div>
    <div class="row_input">
        <div class="input_with_icon">
            <a href="tel:+<?= Html::encode($client->phone[0]); ?>" class="call_now"><span></span></a>
            <?= Html::activeTextInput($client, 'phone', [
                'class'     => 'mask_phone',
                'name'      => 'Client[phone][]',
                'data-lang' => mb_substr(app()->language, 0, 2),
                'value'     => isset($_POST['Client']['phone'][0]) ? $_POST['Client']['phone'][0] : getValue($client->phone[0]),
            ]); ?>
            <span></span>
        </div>
    </div>
    <?= $form->field($client, 'phone')->end(); ?>
</section>

<!--E-mail-->
<section class="row">
    <?= $form->field($client, 'email')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($client, 'email') ?></div>
    <div class="row_input">
        <?= Html::activeTextInput($client, 'email') ?>
        <?= Html::error($client, 'email',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($client, 'email')->end(); ?>
</section>

<!-- Описание -->
<section class="row">
    <?= $form->field($client, 'description')->begin(); ?>
    <div class="row_label">
        <?= Html::activeLabel($client, 'description') ?>
        <a data-tooltip="2" class="tooltip">?
            <span style="display: none; opacity: 0;">
                <?= Yii::t('hint',
                    'If necessary, add a comment on the client. Clients do not see this information.'); ?>
            </span>
        </a>
    </div>
    <div class="row_input">
        <?= Html::activeTextarea($client, 'description') ?>
    </div>
    <?= $form->field($client, 'description')->end(); ?>
</section>

<!--Password-->
<? if (!$client->isNewRecord): ?>
    <section class="row">
        <?= $form->field($client, 'password')->begin(); ?>
        <div class="row_label">
            <?= Html::activeLabel($client, 'password') ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint',
                        "To enter the client: <br> - In the mobile application iOS and Android; <br> - My Account."); ?>
                </span>
            </a>
        </div>
        <div class="row_input">
            <?= Html::activeTextInput($client, 'password', ['disabled' => 'disabled']) ?>
        </div>
        <?= $form->field($client, 'password')->end(); ?>
    </section>
<? endif ?>

<?= Html::activeHiddenInput($client, 'new_phone') ?>

<section class="row">
    <?= $form->field($client, 'send_to_email')->begin(); ?>
    <div class="row_label"></div>
    <div class="row_input">
        <?= Html::activeCheckbox($client, 'send_to_email') ?>
    </div>
    <?= $form->field($client, 'send_to_email')->end(); ?>
</section>

<section class="submit_form">
    <?= Html::activeCheckbox($client, 'black_list', ['label' => Html::tag('b', $client->getAttributeLabel('black_list'))]) ?>

    <?= Html::submitInput(t('app', 'Save')) ?>
</section>

<?php ActiveForm::end(); ?>
