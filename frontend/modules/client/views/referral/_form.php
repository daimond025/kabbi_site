<?php
/**
 * @var $this \yii\web\View
 * @var $model \frontend\modules\referral\models\Referral
 * @var $cityList array
 * @var $currency string
 * @var $isExistActive boolean
 */

use frontend\modules\client\assets\ReferralAsset;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

ReferralAsset::register($this);

$form = ActiveForm::begin([
    'id'                     => 'worker-form',
    'errorCssClass'          => 'input_error',
    'successCssClass'        => 'input_success',
    'enableClientValidation' => true,
    'options'                => [
        'enctype' => 'multipart/form-data',
    ],
]); ?>

<!--Название-->
<section class="row">
    <?= $form->field($model, 'name')->begin(); ?>
    <div class="row_label">
        <?= Html::activeLabel($model, 'name') ?>
        <a data-tooltip="2" class="tooltip">?
            <span style="display: none; opacity: 0;">
                <?= Yii::t('hint', 'It is only displayed on the system.'); ?>
            </span>
        </a>
    </div>
    <div class="row_input">
        <?= Html::activeTextInput($model, 'name') ?>
        <?= Html::error($model, 'name',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($model, 'name')->end(); ?>
</section>

<!--Город-->
<section class="row">
    <?= $form->field($model, 'city_id')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($model, 'city_id') ?></div>
    <div class="row_input">
        <?= Html::activeDropDownList($model, 'city_id', $cityList, [
            'data-placeholder' => !empty($model->city_id) ? Html::encode($cityList[$model->city_id]) : t('car',
                'Choose city'),
            'class'            => 'default_select js-city',
        ]) ?>
    </div>
    <?= $form->field($model, 'city_id')->end(); ?>
</section>

<!--Сортировка-->
<section class="row">
    <?= $form->field($model, 'sort')->begin(); ?>
    <div class="row_label">
        <?= Html::activeLabel($model, 'sort') ?>
        <a data-tooltip="2" class="tooltip">?
            <span style="display: none; opacity: 0;">
                <?= Yii::t('hint', 'Sorting determines the order of the location in the list. Made for ease of use. The smaller the digit, the higher the item in the list.'); ?>
            </span>
        </a>
    </div>
    <div class="row_input">
        <?= Html::activeTextInput($model, 'sort') ?>
        <?= Html::error($model, 'sort',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($model, 'sort')->end(); ?>
</section>

<!--Лимит приглашенных-->
<section class="row">
    <?= $form->field($model, 'subscriber_limit')->begin(); ?>
    <div class="row_label">
        <?= Html::activeLabel($model, 'subscriber_limit') ?>
        <a data-tooltip="2" class="tooltip">?
            <span style="display: none; opacity: 0;">
                <?= Yii::t('hint', 'Number of people that can be invited by 1 client.'); ?>
            </span>
        </a>
    </div>
    <div class="row_input">
        <label style="float: left; margin-right: 10px; line-height: 30px;">
            <?= Html::activeCheckbox($model, 'is_active_subscriber_limit',
                ['label' => null, 'class' => 'js-is-limit']) ?>
        </label>
        <span class="js-on-limit">
            <?= Html::activeTextInput($model, 'subscriber_limit',
                ['class' => 'input_number_mini', 'style' => 'float: left; margin-right: 10px;']) ?>
            <label style="float: left; margin-right: 10px; line-height: 30px;"><?= Html::encode(t('app',
                    'person')); ?></label>
        </span>
    </div>
    <?= $form->field($model, 'subscriber_limit')->end(); ?>
</section>

<!--Тип реферальной системы-->
<section class="row">
    <?= $form->field($model, 'type')->begin(); ?>
    <div class="row_label">
        <?= Html::activeLabel($model, 'type') ?>
        <a data-tooltip="2" class="tooltip">?
            <span style="display: none; opacity: 0;">
                <?= Yii::t('hint', 'Bonus for the trip: bonuses are accrued only after the trip. Bonuses for registration: bonuses are credited after successful registration.'); ?>
            </span>
        </a>
    </div>
    <div class="row_input">
        <?= Html::activeRadioList($model, 'type', $model::getTypeList(), [
            'item' => function ($index, $label, $name, $checked, $value) {
                switch ($value) {
                    case \frontend\modules\referral\models\Referral::ON_REGISTER:
                        $class = 'js-is-register';
                        break;
                    case \frontend\modules\referral\models\Referral::ON_ORDER:
                        $class = 'js-is-order';
                        break;
                    default:
                        $class = '';
                }

                return Html::radio($name, $checked, ['label' => $label, 'value' => $value, 'class' => $class]);
            },
        ]) ?>
    </div>
    <?= $form->field($model, 'type')->end(); ?>
</section>

<section class="row">
    <?= $form->field($model, 'referrer_bonus')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($model, 'referrer_bonus') ?></div>
    <div class="row_input">
        <?= Html::activeTextInput($model, 'referrer_bonus',
            ['value' => (float)$model->referrer_bonus, 'class' => 'input_number_mini', 'style' => 'float: left; margin-right: 10px;']) ?>
        <div class="js-on-order" style="float: left; width: 95px; margin-right: 10px;">
            <?= Html::activeDropDownList($model, 'referrer_bonus_type', $model::getBonusTypeList(),
                ['class' => 'default_select']) ?>
        </div>
        <label class="js-on-order" style="float: left; line-height: 30px;"><?= Html::encode(t('referral', 'from trips of the invited')) ?></label>
        <label class="js-on-register" style="float: left; line-height: 30px;"><?= Html::encode(t('referral', 'Bonus')) ?></label></label>
    </div>
    <?= $form->field($model, 'referrer_bonus')->end(); ?>
</section>

<section class="row">
    <?= $form->field($model, 'referral_bonus')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($model, 'referral_bonus') ?></div>
    <div class="row_input">
        <?= Html::activeTextInput($model, 'referral_bonus',
            ['value' => (float)$model->referral_bonus, 'class' => 'input_number_mini', 'style' => 'float: left; margin-right: 10px;']) ?>
        <div class="js-on-order" style="float: left; width: 95px; margin-right: 10px;">
            <?= Html::activeDropDownList($model, 'referral_bonus_type', $model::getBonusTypeList(),
                ['class' => 'default_select']) ?>
        </div>
        <div class="js-on-order" style="float: left; width: 150px; margin-right: 10px;">
            <?= Html::activeDropDownList($model, 'order_count', $model::getOrderCountList(),
                ['class' => 'default_select']) ?>
        </div>
        <label class="js-on-order" style="float: left; line-height: 30px;"><?= Html::encode(t('referral', 'from trip')) ?></label>
        <label class="js-on-register" style="float: left; line-height: 30px;"><?= Html::encode(t('referral', 'Bonus')) ?></label>
    </div>
    <?= $form->field($model, 'referral_bonus')->end(); ?>
</section>

<!--При стоимости поездки от-->
<section class="row js-on-order">
    <?= $form->field($model, 'min_price')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($model, 'min_price') ?></div>
    <div class="row_input">
        <?= Html::activeTextInput($model, 'min_price',
            ['value' => (float)$model->min_price, 'class' => 'input_number_mini', 'style' => 'float: left; margin-right: 10px;']) ?>
        <label class="js-currency" style="float: left; line-height: 30px;"><?= $currency ?></label>
    </div>
    <?= $form->field($model, 'min_price')->end(); ?>
</section>

<h2><?= Html::encode(t('referral', 'Texts')) ?></h2>

<!--Текст в клиентском приложении-->
<section class="row">
    <?= $form->field($model, 'text_in_client_api')->begin(); ?>
    <div class="row_label">
        <?= Html::activeLabel($model, 'text_in_client_api') ?>
    </div>
    <div class="row_input">
        <?= Html::activeTextarea($model, 'text_in_client_api', ['rows' => 3]) ?>
    </div>
    <?= $form->field($model, 'text_in_client_api')->end(); ?>
</section>

<!--Заголовок при отправке-->
<section class="row">
    <?= $form->field($model, 'title')->begin(); ?>
    <div class="row_label">
        <?= Html::activeLabel($model, 'title') ?>
    </div>
    <div class="row_input">
        <?= Html::activeTextInput($model, 'title') ?>
        <?= Html::error($model, 'title',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($model, 'title')->end(); ?>
</section>

<!--Текст при отправке-->
<section class="row">
    <?= $form->field($model, 'text')->begin(); ?>
    <div class="row_label">
        <?= Html::activeLabel($model, 'text') ?>
    </div>
    <div class="row_input">
        <?= Html::activeTextarea($model, 'text', ['rows' => 3]) ?>
    </div>
    <?= $form->field($model, 'text')->end(); ?>
</section>

<section class="row">
    #NAME# - <?= t('referral', 'Name of your company name') ?><br>
    #CODE# - <?= t('referral', 'Referral code') ?><br>
    #REFERRER_BONUS# - <?= t('referral', 'Bonus to the referrer') ?><br>
    #REFERRAL_BONUS# - <?= t('referral', 'Bonus to the referral') ?><br>
    #MIN_PRICE# - <?= t('referral', 'Min price') ?><br>
</section>


<section class="submit_form">
    <div>
            <label><?= Html::activeCheckbox($model, 'block', ['label' => null]) ?>
                <b><?= t('app', 'Block') ?></b></label>
    </div>
    <?= Html::submitInput(t('app', 'Save')) ?>
</section>

<?php ActiveForm::end(); ?>
