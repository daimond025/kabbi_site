<?php

/**
 * @var $this \yii\web\View
 * @var $phaxId string
 * @var $cityList array
 * @var $dataProvider \yii\data\ArrayDataProvider
 * @var $searchModel \frontend\modules\referral\models\ReferralSearch
 */

use yii\grid\GridView;
use frontend\widgets\filter\Filter;
use yii\widgets\Pjax;
use yii\helpers\Html;


$filter = Filter::begin([
    'id'     => 'filter_' . $pjaxId,
    'pjaxId' => $pjaxId,
    'model'  => $searchModel,
]);
echo $filter->checkboxList('cityList', $cityList, ['id' => $pjaxId . '-city_id']);
Filter::end();

Pjax::begin(['id' => $pjaxId]);


echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'       => "{items}",
    'tableOptions' => [
        'class' => 'people_table',
    ],
    'columns'      => [
        [
            'attribute'     => 'name',
            'label' => t('referral', 'Name'),
            'headerOptions' => ['class' => 'pt_wor'],
            'options'       => ['class' => 'pt_fio pt_wor'],
            'content'       => function ($model) {
                return Html::a(Html::encode($model['name']), ['update', 'id' => $model['referral_id']],
                    ['data-pjax' => 0]);
            },
        ],
        [
            'attribute'     => 'sort',
            'label' => t('referral', 'Sort'),
            'headerOptions' => ['class' => 'pt_re'],
            'options'       => ['class' => 'pt_re'],
        ],
        [
            'attribute'     => 'type',
            'label' => t('referral', 'Type'),
            'headerOptions' => ['class' => 'pt_re'],
            'options'       => ['class' => 'pt_re'],
            'content'       => function ($model) {
                return Html::encode(t('referral', $model['type']));
            },
        ],
        [
            'attribute'     => 'city',
            'label' => t('referral', 'City'),
            'headerOptions' => ['class' => 'pt_re'],
            'options'       => ['class' => 'pt_re'],
            'content'       => function ($model) use ($cityList) {
                return $cityList[$model['city_id']];
            },
        ],
    ],
]);

Pjax::end();