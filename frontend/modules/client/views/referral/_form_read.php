<?php
/**
 * @var $this \yii\web\View
 * @var $model \frontend\modules\referral\models\Referral
 * @var $cityList array
 * @var $currency string
 * @var $isExistActive boolean
 */

use yii\helpers\Html;
use frontend\modules\referral\models\Referral;

?>
<div class="non-editable">


    <section class="row">
        <div class="row_label">
            <?= Html::activeLabel($model, 'name') ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint', 'It is only displayed on the system.'); ?>
                </span>
            </a>
        </div>
        <div class="row_input">
            <div class="row_input"><?= !empty($model->name) ? Html::encode($model->name) : '<div class="row_input"></div>' ?></div>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'city_id') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($model->city_id) ? Html::encode($cityList[$model->city_id]) : t('car',
                    'Choose city') ?></div>
        </div>
    </section>

    <section class="row">
        <div class="row_label">
            <?= Html::activeLabel($model, 'sort') ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint', 'Sorting determines the order of the location in the list. Made for ease of use. The smaller the digit, the higher the item in the list.'); ?>
                </span>
            </a>
        </div>
        <div class="row_input">
            <div class="row_input"><?= !empty($model->sort) ? Html::encode($model->sort) : '<div class="row_input"></div>' ?></div>
        </div>
    </section>


    <section class="row">
        <div class="row_label">
            <?= Html::activeLabel($model, 'subscriber_limit') ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                <?= Yii::t('hint', 'Number of people that can be invited by 1 client.'); ?>
            </span>
            </a>
        </div>
        <div class="row_input">
            <div class="row_input">
                <?= Html::activeCheckbox($model, 'is_active_subscriber_limit',
                    ['label' => null, 'disabled' => 'disabled']) ?>
                <? if ($model->is_active_subscriber_limit): ?>
                    <div class="row_input"><?= !empty($model->subscriber_limit) ? Html::encode($model->subscriber_limit)
                            . ' ' . Html::encode(t('app', 'person')) : '<div class="row_input"></div>' ?></div>
                <? endif; ?>
            </div>
    </section>

    <? $typeList = $model::getTypeList(); ?>
    <section class="row">
        <div class="row_label">
            <?= Html::activeLabel($model, 'type') ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint', 'Bonus for the trip: bonuses are accrued only after the trip. Bonuses for registration: bonuses are credited after successful registration.'); ?>
                </span>
            </a>
        </div>
        <div class="row_input">
            <div class="row_input"><?= !empty($model->type) ? Html::encode($typeList[$model->type]) : '<div class="row_input"></div>' ?></div>
        </div>
    </section>

    <? $bonusTypeList = $model::getBonusTypeList(); ?>
    <? $orderCountList = $model::getOrderCountList(); ?>
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'referrer_bonus') ?></div>
        <div class="row_input">
            <div class="row_input">
                <?= (float)$model->referrer_bonus ?> <?= $bonusTypeList[$model->referrer_bonus_type]; ?>
                <? if ($model->type === Referral::ON_ORDER): ?>
                    <?= Html::encode(t('referral', 'from trips of the invited')); ?>
                <? endif; ?>
            </div>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'referral_bonus') ?></div>
        <div class="row_input">
            <div class="row_input">
                <?= (float)$model->referral_bonus ?> <?= $bonusTypeList[$model->referral_bonus_type]; ?>
                <? if ($model->type === Referral::ON_ORDER): ?>
                    <?= $orderCountList[(int)$model->order_count] ?>
                    <?= Html::encode(t('referral', 'from trip')) ?>
                <? endif; ?>
            </div>
        </div>
    </section>

    <h2><?= Html::encode(t('referral', 'Texts')) ?></h2>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'text_in_client_api') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($model->text_in_client_api) ? str_replace(PHP_EOL, '<br>',
                    Html::encode($model->text_in_client_api)) : '<div class="row_input"></div>' ?></div>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'title') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($model->title) ? Html::encode($model->title) : '<div class="row_input"></div>' ?></div>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($model, 'text') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($model->text) ? str_replace(PHP_EOL, '<br>',
                    Html::encode($model->text)) : '<div class="row_input"></div>' ?></div>
        </div>
    </section>

    <section class="row">
        #NAME# - <?= t('referral', 'Name of your company name') ?><br>
        #CODE# - <?= t('referral', 'Referral code') ?><br>
        #REFERRER_BONUS# - <?= t('referral', 'Bonus to the referrer') ?><br>
        #REFERRAL_BONUS# - <?= t('referral', 'Bonus to the referral') ?><br>
        #MIN_PRICE# - <?= t('referral', 'Min price') ?><br>
    </section>


    <? if (!$model->isNewRecord): ?>
        <label><?= Html::activeCheckbox($model, 'block', ['label' => null, 'disabled' => 'disabled']) ?> <b><?= t('app',
                    'Block') ?></b></label>
    <? endif ?>
</div>
