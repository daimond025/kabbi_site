<?php
/**
 * @var $this \yii\web\View
 * @var $model \frontend\modules\referral\models\Referral
 * @var $cityList array
 * @var $currency string
 * @var $isExistActive boolean
 **/

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

$this->title = Yii::t('referral', 'Add referral system');

echo Breadcrumbs::widget([
    'links'        => [
        ['label' => t('app', 'Referral system'), 'url' => ['/client/referral/list']],
    ],
    'itemTemplate' => '{link}',
    'tag'          => 'div',
    'options'      => ['class' => 'bread'],
    'homeLink'     => false,
]);
?>
<h1><?= Html::encode($this->title) ?></h1>

<section class="main_tabs">
    <div class="tabs_content">
        <div class="active" id="t01">
            <?= $this->render('_form',
                compact('model', 'cityList', 'isExistActive')) ?>
        </div>
    </div>
</section>
