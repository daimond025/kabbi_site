<?php
/* @var $this yii\web\View */
/* @var $clients app\modules\client\models\Client*/
if(!empty($clients)):?>
    <table class="people_table" style="table-layout: fixed;">
        <tr>
            <th class="pt_fio" style="width: 45%;"><a href="#"><?=t('client', 'Full name')?> <span data-type="fullName" class="pt_down"></span></a>
            </th>
            <th style="width: 16%;"><?=t('client', 'Orders')?>
            </th>
            <th style="width: 16%;"><?=t('client', 'Activity')?>
            </th>
            <th class="pt_city" style="width: 23%;"><a href="#"><?=t('client', 'City')?> <span data-type="city_id" class=""></span></a>
            </th>
        </tr>
        <?=$this->render('_stuff_add_tr', ['clients' => $clients, 'company_clients' => $company_clients])?>
    </table>
<?else:?>
    <p><?=t('client', 'Clients with this number is not found.')?></p>
    <?php
        if(app()->user->can('organization')) {
            echo '<h3><a id="client_add_button" class="spoler" data-spolerid="id01" data-href="'.yii\helpers\Url::to(['/client/company/add-new-client', 'company_id' => post('company_id')]) .'">'. t('app', 'Add').'</a></h3>';
        }
    ?>

    <section class="spoler_content" id="id01" name="client_form">
    </section>
<?endif;?>