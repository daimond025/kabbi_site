<?php
/* @var $this yii\web\View */
/* @var $clients app\modules\client\models\Client*/
use yii\helpers\Url;
use yii\helpers\Html;
?>

<?foreach($clients as $client):?>
    <tr>
        <td class="pt_fio">
            <a target="_blank" href="<?=Url::to(['/client/base/update', 'id' => $client->client_id])?>">
                <span class="pt_photo"><?= $client->isFileExists($client->photo) ? $client->getPictureHtml(Html::encode($client->photo)) : ''?></span>
                <span class="pt_fios"><?=Html::encode(trim($client->last_name . ' ' . $client->name . ' ' . $client->second_name))?></span>
                <?if(!empty($client->clientPhones)):?>
                    <?foreach($client->clientPhones as $phone):?>
                        <span class="table_tel"><?= Html::encode($phone->value); ?></span>
                    <?endforeach;?>
                <?endif?>
            </a>
        </td>
        <td>
            <div class="man_orders">
                <span class="mo_complete"><?= Html::encode($client->success_order); ?></span>
                <span class="mo_error">
                    <b><?= Html::encode($client->fail_worker_order + $client->fail_client_order); ?></b>
                    <span class="moe_tt">
                        <span><?= Html::encode($client->fail_client_order); ?></span><b><?=t('client', 'Failure order by client')?></b>
                        <span><?= Html::encode($client->fail_worker_order); ?></span><b><?=t('client', 'Failure order by driver')?></b>
                    </span>
                </span>
            </div>
        </td>
        <td><?= Html::encode($client->getActiveFieldValue()); ?></td>
        <td>
            <div class="pt_buttons">
                <?if(in_array($client->client_id, $company_clients)):?>
                    <?=t('client', 'Selected')?>
                <?else:?>
                    <a href="<?=Url::to(['/client/company/stuff-add', 'client_id' => $client->client_id])?>" class="pt_add"><?=t('client', 'Choose')?></a>
                <?endif?>
            </div>
            <?= Html::encode($client->city->{name . getLanguagePrefix()}); ?>
        </td>
    </tr>
<?endforeach;?>