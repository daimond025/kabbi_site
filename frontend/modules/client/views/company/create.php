<?php
/* @var $this yii\web\View */
/* @var $company \app\modules\client\models\ClientCompany */
/* @var $currency string */
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('client', 'Add organization');
$form        = app()->user->can('organization') ? '_form' : '_form_read';
?>
<div class="bread"><a href="<?= Url::toRoute('/client/company/list') ?>"><?= t('app', 'Organization') ?></a></div>
<h1><?= Html::encode($this->title) ?></h1>
<section class="main_tabs">
    <div class="tabs_content">
        <div class="active" id="t01">
            <?php
            echo $this->render($form, [
                'company'        => $company,
                'user_city_list' => $user_city_list,
                'tariff_list'    => $tariff_list,
                'dataForm'       => $dataForm,
                'create'         => $currency,
            ]) ?>
        </div>
    </div>
</section>
