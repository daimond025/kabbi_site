<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $company app\modules\client\models\ClientCompany */
/* @var $form yii\widgets\ActiveForm */
/* @var $currency string */

$this->registerJs('phone_mask_init();');
?>
<div class="non-editable">
    <section class="row" id="logo">


    </section>
    <!--Город, где работает организация-->

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'city_id') ?></div>
        <div class="row_input">
            <div class="row_input"> <?= !empty($company->city_id) ? Html::encode($user_city_list[$company->city_id]) : t('car',
                    'Choose city') ?></div>
        </div>
    </section>
    <section class="row" id="logo">
        <div class="row_label"></div>
        <div class="row_input" style="background: none;">
            <div class="input_file_logo <!--file_loader-->">
                <? if (file_exists(Yii::getAlias('@app') . '/web/' . '/upload/' . user()->tenant_id . '/thumb_' . Html::encode($company->logo))): ?>
                    <a class="input_file_logo logo cboxElement"
                       href="<?= '/upload/' . user()->tenant_id . '/' . Html::encode($company->logo) ?>"
                       style="border: none">
                        <div class="row_input" style="background: none"><span><img
                                        src="<?= '/upload/' . user()->tenant_id . '/' . Html::encode($company->logo) ?>"></span>
                        </div>
                    </a>
                <? endif ?>
            </div>
        </div>
    </section>
    <!--Доступные тарифы-->
    <? /**<section class="row" id="tariff_list">
     * <div class="row_label">
     * <?//= Html::activeLabel($company, 'tariff_list') ?>
     * </div>
     * <div class="row_input">
     * <?foreach ($tariff_list as $tariff_id => $tariff):?>
     * <?
     * $checked = null;
     * if(isset($_POST['ClientCompany']['tariff_list'])
     * && array_key_exists($tariff_id, $_POST['ClientCompany']['tariff_list'])
     * || array_key_exists($tariff_id, $company->tariff_list))
     * {
     * $checked = 'checked="checked"';
     * }
     * ?>
     * <?//php
     * if ($checked == 'checked="checked"') {
     * echo '<div class="row_input">' . $tariff . '</div>';
     * }
     * ?>
     *
     * <?endforeach;?>
     * </div>
     * </section>*/ ?>

    <h2><?= t('client', 'Organization') ?></h2>

    <!--Юридический адрес-->
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'full_name') ?></div>
        <div class="row_input">
            <div class="input_with_text">
                <div class="row_input"><?= !empty($company->full_name) ? Html::encode($company->full_name) : '' ?></div>
            </div>
        </div>
    </section>

    <!--Сокращённое наименование-->
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'name') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($company->name) ? Html::encode($company->name) : '' ?></div>
        </div>
    </section>

    <!--Юридический адрес-->
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'legal_address') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($company->legal_address) ? Html::encode($company->legal_address) : '' ?></div>
        </div>
    </section>

    <!--Почтовый (фактический) адрес-->
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'post_address') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($company->post_address) ? Html::encode($company->post_address) : '' ?></div>
        </div>
    </section>

    <!--Директор-->
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'director') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($company->director) ? Html::encode($company->director) : '' ?></div>
        </div>
    </section>

    <!--Должность директора-->
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'director_post') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($company->director_post) ? Html::encode($company->director_post) : '' ?></div>
        </div>
    </section>

    <!--Главный бухглатер-->
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'bookkeeper') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($company->bookkeeper) ? Html::encode($company->bookkeeper) : '' ?></div>
        </div>
    </section>

    <!--ИНН-->
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'inn') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($company->inn) ? Html::encode($company->inn) : '' ?></div>
        </div>
    </section>

    <!--КПП-->
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'kpp') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($company->kpp) ? Html::encode($company->kpp) : '' ?></div>
        </div>
    </section>

    <!--ОГРН или ОГРНИП-->
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'ogrn') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($company->ogrn) ? Html::encode($company->ogrn) : '' ?></div>
        </div>
    </section>

    <h2><?= t('client', 'Contacts of organization') ?></h2>

    <!--Телефон-->
    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'work_phone') ?></div>
        <div class="row_input">
            <div class="input_with_text input_with_icon">
                <a href="tel:<?= !empty($company->work_phone) ? '+' . $company->work_phone : '' ?>"
                   class="call_now"><span></span></a>
                <div class="row_input"><?= !empty($company->work_phone) ? Html::encode($company->work_phone) : '' ?></div>
                <span></span>
            </div>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'email') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($company->email) ? Html::encode($company->email) : '' ?></div>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'site') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($company->site) ? Html::encode($company->site) : '' ?></div>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'contact_email') ?></div>
        <div class="row_input">
            <div class="row_input"><?= !empty($company->contact_email) ? Html::encode($company->contact_email) : '' ?></div>
        </div>
    </section>

    <h2><?= t('client', 'Contact name') ?></h2>

    <section class="row">
        <div class="row_label">
            <div class="row_input">
                <?= Html::encode($company->getAttributeLabel('contact_last_name')) ?>
                <?= Html::encode($company->getAttributeLabel('contact_name')) ?>
                <?= Html::encode($company->getAttributeLabel('contact_second_name')) ?>
            </div>
        </div>
        <div class="row_input">
            <div class="grid_3">
                <div class="row_input"><?= Html::encode($company->contact_last_name) ?></div>
                <div class="row_input"><?= Html::encode($company->contact_name) ?></div>
                <div class="grid_item"><?= Html::encode($company->contact_second_name) ?></div>
            </div>
        </div>
    </section>

    <section class="row">
        <div class="row_label"><?= Html::activeLabel($company, 'contact_phone') ?></div>
        <div class="row_input">
            <div class="input_with_text input_with_icon">
                <a href="tel:<?= !empty($company->contact_phone) ? '+' . $company->contact_phone : '' ?>"
                   class="call_now"><span></span></a>
                <div class="row_input"><?= !empty($company->contact_phone) ? Html::encode($company->contact_phone) : '' ?></div>
                <span></span>
            </div>
        </div>
    </section>

    <section class="row">
        <div class="row_label">
            <?= Html::activeLabel($company, 'credits') ?>
            <a data-tooltip="2" class="tooltip">?
                <span style="display: none; opacity: 0;">
                    <?= Yii::t('hint', 'Admissible negative balance to which the organization can make orders.'); ?>
                </span>
            </a>
        </div>
        <div class="row_input">
            <div class="row_input input_number_mini">
                <?= !empty($company->credits) ? Html::encode($company->credits) : '0' ?>
                <?= $currency ?>
            </div>
        </div>
    </section>

    <? if (!$company->isNewRecord): ?>
        <label><?= Html::activeCheckbox($company, 'block', ['label' => null, 'disabled' => 'disabled']) ?>
            <b><?= t('client', 'Block') ?></b></label>
    <? endif ?>
</div>