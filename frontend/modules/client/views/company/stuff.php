<?php
/* @var $this yii\web\View */
/* @var $clients app\modules\client\models\Client*/
use yii\helpers\Url;
?>

<div id="company_stuff" class="ajax">
    <?=$this->render('_company_stuff', ['clients' => $clients, 'user_city_list' => $user_city_list, 'id' => $id])?>
</div>
<?php
        if(app()->user->can('organization'))
            echo '
<h2>'.t ('client', 'Add employee').'</h2>

<div class="grid_3">
    <div class="grid_item">
        <input data-company=" '. $id .'" id="company_stuff_search" type="text" placeholder="' .t('client', 'Search by phone number (6 chars required)') .' " data-search="'. Url::to('/client/company/staff-add-search').'" />
    </div>
    <div class="grid_item" style="height: 50px;"></div>
    <div class="grid_item" style="height: 50px;">
    </div>
</div>'?>
<div id="client_add"></div>