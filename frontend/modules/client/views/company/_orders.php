<?php

/* @var string $pjaxId */
/* @var $this yii\web\View */
/* @var $searchModel \app\modules\client\models\OrderClientSearch */
/* @var $dataProvider yii\data\ArrayDataProvider */
?>

<section class="main_tabs">
    <?= $this->render('_grid_orders', compact('dataProvider', 'searchModel', 'pjaxId')) ?>
</section>