<?php

use yii\helpers\Html;

/* @var string $pjaxId */
/* @var $this yii\web\View */
/* @var array $cityList */
/* @var array $positionList */
/* @var $dataProvider yii\data\ArrayDataProvider */
/* @var $searchModel \frontend\modules\employee\models\groups\WorkerGroupSearch */

$this->title = Yii::t('client', 'Organizations');

if ( app()->user->can('organization') )
    echo Html::a(t('app', 'Add'), ['create'], ['class' => 'button', 'style' => 'float: right;']);    
?>

<h1><?= Html::encode($this->title) ?></h1>
<section class="main_tabs">
    <?= $this->render('_grid', compact('searchModel', 'dataProvider', 'cityList', 'pjaxId')) ?>
</section>