<?php
/* @var $this yii\web\View */
/* @var $clients app\modules\client\models\Client*/
use yii\helpers\Url;
use yii\helpers\Html;

if(!empty($clients)):?>
    <?foreach($clients as $client):?>
        <tr>
            <td class="pt_fio">
                <a href="<?=Url::to(['/client/base/update', 'id' => $client->client_id])?>">
                    <span class="pt_photo"><?= $client->isFileExists($client->photo) ? $client->getPictureHtml(Html::encode($client->photo)) : ''?></span>
                    <span class="pt_fios"><?= Html::encode(trim($client->last_name . ' ' . $client->name . ' ' . $client->second_name))?></span>
                    <?if(!empty($client->clientPhones)):?>
                        <?foreach($client->clientPhones as $phone):?>
                            <span class="table_tel"><?= Html::encode($phone->value); ?></span>
                        <?endforeach;?>
                    <?endif?>
                </a>
                <? if(app()->user->can('clients')) : ?>
                    <span class="auto_num">
                        <label>
                            <?= Html::checkbox('role', $client->isAdmin($company_id), ['id' => 'role' . $client->client_id, 'class' => 'js-role', 'data-client_id' => $client->client_id, 'data-company_id' => $company_id]); ?><?= t('client', 'Administrartor'); ?>
                        </label>
                    </span>
                <? endif; ?>
                <?/*<span class="auto_num" data-url="<?=Url::to(['/client/company/change-stuff-rights', 'client' => $client->client_id, 'company' => $client->clientHasCompanies[0]->company_id])?>"><label><input data-type="booking" <?if($client->clientHasCompanies[0]->booking):?>checked="checked"<?endif?> type="checkbox"/> <?=t('client', 'Order')?></label><label><input data-type="driving" <?if($client->clientHasCompanies[0]->driving):?>checked="checked"<?endif?> type="checkbox"/> <?=t('client', 'Driving')?></label></span>*/?>
            </td>
            <!--<td>-->
<!--                <div class="man_orders">
                    <span class="mo_complete"></?=$client->success_order?></span>
                    <span class="mo_error">
                        <b></?=$client->fail_worker_order + $client->fail_client_order?></b>
                        <span class="moe_tt">
                            <span></?=$client->fail_client_order?></span><b></?=t('client', 'Failure order by client')?></b>
                            <span></?=$client->fail_worker_order?></span><b></?=t('client', 'Failure order by driver')?></b>
                        </span>
                    </span>
                </div>-->
            <!--</td>-->
            <td><?=$client->getActiveFieldValue()?></td>
            <td>
                <div class="pt_buttons">
                        <?php
                            if(app()->user->can('organization'))
                            echo '<a data-view="_company_stuff" href="' .Url::to(["/client/company/stuff-remove", "client" => $client->client_id, "company" => $client->clientHasCompanies[0]->company_id]) .'" class="pt_del">' .t('client', 'Remove') .'</a>';
                        ?>

                </div>
                <?= Html::encode($client->city->{name . getLanguagePrefix()}); ?>
            </td>
        </tr>
    <?endforeach;?>
<?else:?>
    <tr>
        <td><?=t('client', 'No employees')?></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
<?endif;?>
