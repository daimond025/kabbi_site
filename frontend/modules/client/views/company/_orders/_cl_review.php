<?php

/* @var $model \app\modules\client\models\OrderClientSearch */
/* @var $this yii\web\View */

use yii\helpers\Html;

if(!empty($model->clientReview)) {
    $rating = [];
    for($i = 0; $i < (int)$model->clientReview->rating; $i++) {
        $rating[] = Html::tag('i','',['class' => 'active']);
    }
    for($i = (int)$model->clientReview->rating; $i < 5; $i++) {
        $rating[] = Html::tag('i','');
    }
    $ratingStr = Html::tag('span', implode(PHP_EOL, $rating), ['class' => 'clir_stars']);

    if(!empty($model->clientReview->text)) {
        $text = $model->clientReview->text;
        if(strlen($text) > 20) {
            $textStr = Html::tag('i', '«' . Html::encode(substr($text, 0, 20)) . '...»', ['class' => 'js-preview'])
                . Html::tag('i', '«' . Html::encode($text) . '»', ['style' => 'display:none;', 'class' => 'js-full'])
                . Html::a(t('client', 'Fully'), null, ['class' => 'js-spoiler review_spoler']);
        } else {
            $textStr = Html::tag('i', '«' . Html::encode($text) . '»');
        }
    }
    echo $ratingStr . Html::tag('div', $textStr, ['class' => 'clir_content']);
}