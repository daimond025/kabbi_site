<?= \frontend\widgets\transaction\Transaction::widget([
        'action' => '/client/company/transaction-search',
        'transactions' => $transactions,
        'transactionClass' => \app\modules\client\models\ClientCompanyTransaction::className(),
        'balance' => $companyBalance,
        'has_transaction' => $has_transaction
]);?>