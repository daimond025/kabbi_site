<?php

/* @var string $pjaxId */
/* @var $this yii\web\View */
/* @var $searchModel \app\modules\client\models\OrderClientSearch */
/* @var $dataProvider yii\data\ArrayDataProvider */

use yii\widgets\Pjax;
use yii\grid\GridView;
use frontend\widgets\filter\Filter;
use frontend\widgets\LinkPager;


$filter = Filter::begin([
    'pjaxId' => $pjaxId,
    'model'  => $searchModel,
    'onSubmit' => false,
    'onChange' => true,
]);

echo $filter->radioList('type',
    [
        'today' => [
            'label' => t('reports', 'Today'),
        ],
        'period' => [
            'label' => t('reports', 'Period'),
            'content' => $filter->datePeriod(['first_date', 'second_date'], [
                date("d.m.Y", mktime(0, 0, 0, date("m")  , date("d")-2, date("Y"))),
                date("d.m.Y", mktime(0, 0, 0, date("m")  , date("d"), date("Y")))
            ], ['class' => 'cof_date']),
        ],
    ], [], ['class' => 'filter_item_2', 'style' => 'width:auto']);

echo $filter->export([
    [
        'label' => t('client', 'List of orders in Excel'),
        'url'   => ["/reports/order/dump-company-orders/excel/".get('id')],
    ],
    [
        'label' => t('client', 'Receipts about trips in PDF'),
        'url'   => ["/reports/order/dump-company-orders/pdf/".get('id')],
    ]
], [], 'Download');

Filter::end();



Pjax::begin(['id' => $pjaxId]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'       => "{items}\n{pager}",
    'tableOptions' => [
        'class' => 'people_table',
    ],
    'pager'        => [
        'class'         => LinkPager::className(),
        'prevPageLabel' => t('app', 'Prev'),
        'nextPageLabel' => t('app', 'Next'),
    ],
    'columns'      => [
        [
            'label' => '',
            'attribute' => 'order_number',
            'content' => function ($model) {
                return $this->render('_orders/_cl_id', ['model' => $model]);
            },
            'contentOptions' => ['class' => 'cl_id'],
            'headerOptions' => ['style' => 'width:10%'],
        ],
        [
            'label' => t('order', 'Address'),
            'attribute' => 'info',
            'content' => function ($model) {
                return $this->render('_orders/_cl_info', ['model' => $model]);
            },
            'contentOptions' => ['class' => 'cl_info'],
//            'headerOptions' => ['style' => 'width:25%'],
        ],
        [
            'label' => t('order', 'Status'),
            'attribute' => 'staus_id',
            'content' => function ($model) {
                return $this->render('_orders/_cl_status', ['model' => $model]);
            },
            'contentOptions' => ['class' => 'cli_details'],
            'headerOptions' => ['style' => 'width:18%'],
        ],
        [
            'label' => t('app', 'Worker'),
            'attribute' => 'worker',
            'content' => function ($model) {
                return $this->render('_orders/_cl_worker', ['model' => $model]);
            },
            'contentOptions' => ['class' => 'cli_details'],
            'headerOptions' => ['style' => 'width:17%'],
        ],
        [
            'label' => '',
            'attribute' => 'review',
            'content' => function ($model) {
                return $this->render('_orders/_cl_review', ['model' => $model]);
            },
            'contentOptions' => ['class' => 'cli_review'],
            'headerOptions' => ['style' => 'width:20%'],
        ],
        [
            'label' => '',
            'attribute' => 'predv_price',
            'content' => function ($model) {
                return $this->render('_orders/_cl_costs', ['model' => $model]);
            },
            'contentOptions' => ['style' => 'text-align: right'],
            'headerOptions' => ['style' => 'width:8%'],
        ],
    ],
]);

Pjax::end();