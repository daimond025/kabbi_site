<?php
/* @var $this yii\web\View */
/* @var $company app\modules\client\models\ClientCompany */
/* @var $currency string */
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\balance\models\Account;

$this->title = $company->name;

$this->registerJs("createCheckboxStringOnReady('#city_list');");
$this->registerJs("createCheckboxStringOnReady('#tariff_list');");
app\modules\client\assets\ClientsAsset::register($this);
\frontend\assets\LeafletAsset::register($this);
\frontend\modules\balance\assets\BalanceAsset::register($this);
$this->registerJs('balanceOperationInit();');
?>
<!--div class="pt_buttons" style="position: relative; top: 10px;">
    <a href="#" class="pt_call"><span></span></a>
</div-->
<div class="bread"><a href="<?= Url::toRoute('/client/company/list') ?>"><?= t('app', 'Organization') ?></a></div>
<h1><?= Html::encode($this->title) ?></h1>
<section class="main_tabs">
    <div class="tabs_links">
        <ul>
            <li><a href="#profile" class="t01"><?= t('client', 'Profile') ?></a>
            </li>
            <li><a href="#stuff" class="t02"
                   data-href="<?= Url::to(['/client/company/stuff', 'id' => $company->company_id]) ?>"><?= t('client',
                        'Stuff') ?></a>
            </li>
            <li><a href="#orders" data-href="<?= Url::to(['/client/company/orders', 'id' => $company->company_id]) ?>"
                   class="t03"><?= t('client', 'Orders') ?></a>
            </li>
            <li><a href="#balance" class="t04" data-href="<?= Url::to([
                    '/balance/balance/show-balance',
                    'id'      => $company->company_id,
                    'kind_id' => Account::COMPANY_KIND,
                    'city_id' => $company->city_id,
                ]) ?>"><?= t('client', 'Balance') ?></a>
            </li>
            <li><a href="#reports" class="t05"
                   data-href="<?= Url::to(['/reports/company/view/', 'id' => $company->company_id]) ?>"><?= t('client',
                        'Reports') ?></a>
            </li>
        </ul>
    </div>
    <div class="tabs_content">
        <div id="t01">
            <?php
            $form = app()->user->can('organization') ? '_form' : '_form_read';
            echo $this->render($form, [
                'company'        => $company,
                'user_city_list' => $user_city_list,
                'tariff_list'    => $tariff_list,
                'dataForm'       => $dataForm,
                'currency'       => $currency,
            ]) ?>
        </div>
        <div id="t02"></div>
        <div id="t03"></div>
        <div id="t04"></div>
        <div id="t05"></div>
    </div>
</section>
