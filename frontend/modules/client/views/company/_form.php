<?php

use frontend\widgets\file\Logo;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $company app\modules\client\models\ClientCompany */
/* @var $form yii\widgets\ActiveForm */
/* @var $currency string */

$this->registerJs('phone_mask_init();');

$form = ActiveForm::begin([
    'id'                     => 'company-form',
    'errorCssClass'          => 'input_error',
    'successCssClass'        => 'input_success',
    'enableClientValidation' => true,
    'validateOnBlur'         => false,
    'options'                => ['enctype' => 'multipart/form-data'],
]); ?>

<!--Логотип-->
<? if (!$company->isNewRecord): ?>
    <?= Logo::widget([
        'form'         => $form,
        'model'        => $company,
        'attribute'    => 'logo',
        'ajax'         => true,
        'uploadAction' => Url::to(['/client/company/upload-logo/', 'entity_id' => get('id')]),
    ]) ?>
<? endif ?>

<!--Город, где работает организация-->
<section class="row">
    <?= $form->field($company, 'city_id')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($company, 'city_id') ?></div>
    <div class="row_input">
        <?= Html::activeDropDownList($company, 'city_id', $user_city_list, [
            'data-placeholder' => !empty($company->city_id) ? Html::encode($user_city_list[$company->city_id]) : t('car',
                'Choose city'),
            'class'            => 'default_select js-city',
        ]); ?>
    </div>
    <?= $form->field($company, 'city_id')->end(); ?>
</section>

<!--Доступные тарифы-->
<? /*
<<section class="row" id="tariff_list">
    <div class="row_label">
        <?//=Html::activeLabel($company, 'tariff_list')?>
    </div>
    <div class="row_input">
        <div class="select_checkbox">
            <a class="a_sc" rel="<?//=t('client', 'All tariffs')?>"><?//=t('client', 'All tariffs')?></a>
            <div class="b_sc">
                <ul>
                    <?foreach ($tariff_list as $tariff_id => $tariff):?>
                        <?
                        $checked = null;
                        if(isset($_POST['ClientCompany']['tariff_list'])
                          && array_key_exists($tariff_id, $_POST['ClientCompany']['tariff_list'])
                          || array_key_exists($tariff_id, $company->tariff_list))
                        {
                            $checked = 'checked="checked"';
                        }
                        ?>
                        <li><label><input <?//=$checked?> name="ClientCompany[tariff_list][]" type="checkbox" value="<?//=$tariff_id?>"/> <?//=$tariff?></label></li>
                    <?endforeach;?>
                </ul>
            </div>
        </div>
    </div>
</section>*/ ?>

<h2><?= t('client', 'Organization') ?></h2>

<!--Юридический адрес-->
<section class="row">
    <?= $form->field($company, 'full_name')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($company, 'full_name') ?></div>
    <div class="row_input">
        <div class="input_with_text">
            <?= Html::activeTextInput($company, 'full_name') ?>
            <span><?=t('tenant','example full company name')?></span>
        </div>
    </div>
    <?= $form->field($company, 'full_name')->end(); ?>
</section>

<!--Сокращённое наименование-->
<section class="row">
    <?= $form->field($company, 'name')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($company, 'name') ?></div>
    <div class="row_input">
        <?= Html::activeTextInput($company, 'name') ?>
    </div>
    <?= $form->field($company, 'name')->end(); ?>
</section>

<!--Юридический адрес-->
<section class="row">
    <?= $form->field($company, 'legal_address')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($company, 'legal_address') ?></div>
    <div class="row_input">
        <?= Html::activeTextInput($company, 'legal_address') ?>
    </div>
    <?= $form->field($company, 'legal_address')->end(); ?>
</section>

<!--Почтовый (фактический) адрес-->
<section class="row">
    <?= $form->field($company, 'post_address')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($company, 'post_address') ?></div>
    <div class="row_input">
        <?= Html::activeTextInput($company, 'post_address') ?>
    </div>
    <?= $form->field($company, 'post_address')->end(); ?>
</section>

<!--Директор-->
<section class="row">
    <?= $form->field($company, 'director')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($company, 'director') ?></div>
    <div class="row_input">
        <?= Html::activeTextInput($company, 'director') ?>
    </div>
    <?= $form->field($company, 'director')->end(); ?>
</section>

<!--Должность директора-->
<section class="row">
    <?= $form->field($company, 'director_post')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($company, 'director_post') ?></div>
    <div class="row_input">
        <?= Html::activeTextInput($company, 'director_post') ?>
    </div>
    <?= $form->field($company, 'director_post')->end(); ?>
</section>

<!--Главный бухглатер-->
<section class="row">
    <?= $form->field($company, 'bookkeeper')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($company, 'bookkeeper') ?></div>
    <div class="row_input">
        <?= Html::activeTextInput($company, 'bookkeeper') ?>
    </div>
    <?= $form->field($company, 'bookkeeper')->end(); ?>
</section>

<!--ИНН-->
<section class="row">
    <?= $form->field($company, 'inn')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($company, 'inn') ?></div>
    <div class="row_input">
        <?= Html::activeTextInput($company, 'inn') ?>
    </div>
    <?= $form->field($company, 'inn')->end(); ?>
</section>

<!--КПП-->
<section class="row">
    <?= $form->field($company, 'kpp')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($company, 'kpp') ?></div>
    <div class="row_input">
        <?= Html::activeTextInput($company, 'kpp') ?>
    </div>
    <?= $form->field($company, 'kpp')->end(); ?>
</section>

<!--ОГРН или ОГРНИП-->
<section class="row">
    <?= $form->field($company, 'ogrn')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($company, 'ogrn') ?></div>
    <div class="row_input">
        <?= Html::activeTextInput($company, 'ogrn') ?>
    </div>
    <?= $form->field($company, 'ogrn')->end(); ?>
</section>

<!-- Описание -->
<section class="row">
    <?= $form->field($company, 'description')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($company, 'description') ?></div>
    <div class="row_input">
        <?= Html::activeTextarea($company, 'description') ?>
    </div>
    <?= $form->field($company, 'description')->end(); ?>
</section>

<h2><?= t('client', 'Contacts of organization') ?></h2>

<!--Телефон-->
<section class="row">
    <?= $form->field($company, 'work_phone')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($company, 'work_phone') ?></div>
    <div class="row_input">
        <div class="input_with_text input_with_icon">
            <a href="tel:<?= !empty($company->work_phone) ? '+' . $company->work_phone : '' ?>"
               class="call_now"><span></span></a>
            <?= Html::activeTextInput($company, 'work_phone', [
                'class'     => 'mask_phone',
                'data-lang' => mb_substr(app()->language, 0, 2),
            ]); ?>
            <span></span>
        </div>
    </div>
    <?= $form->field($company, 'work_phone')->end(); ?>
</section>

<section class="row">
    <?= $form->field($company, 'email')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($company, 'email') ?></div>
    <div class="row_input">
        <?= Html::activeTextInput($company, 'email') ?>
        <?= Html::error($company, 'email',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($company, 'email')->end(); ?>
</section>

<section class="row">
    <?= $form->field($company, 'site')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($company, 'site') ?></div>
    <div class="row_input">
        <?= Html::activeTextInput($company, 'site') ?>
        <?= Html::error($company, 'site',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($company, 'site')->end(); ?>
</section>

<h2><?= t('client', 'Contact name') ?></h2>

<section class="row">
    <div class="row_label"><label><?= Html::activeLabel($company,
                'contact_last_name') ?> <?= Html::activeLabel($company,
                'contact_name') ?> <?= Html::activeLabel($company, 'contact_second_name') ?></label></div>
    <div class="row_input">
        <div class="grid_3">

            <?= $form->field($company, 'contact_last_name')->begin(); ?>
            <div class="grid_item">
                <?= Html::activeTextInput($company, 'contact_last_name') ?>
            </div>
            <?= $form->field($company, 'contact_last_name')->end(); ?>

            <?= $form->field($company, 'contact_name')->begin(); ?>
            <div class="grid_item">
                <?= Html::activeTextInput($company, 'contact_name') ?>
            </div>
            <?= $form->field($company, 'contact_name')->end(); ?>

            <?= $form->field($company, 'contact_second_name')->begin(); ?>
            <div class="grid_item">
                <?= Html::activeTextInput($company, 'contact_second_name') ?>
            </div>
            <?= $form->field($company, 'contact_second_name')->end(); ?>
        </div>
    </div>
</section>

<section class="row">
    <?= $form->field($company, 'contact_phone')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($company, 'contact_phone') ?></div>
    <div class="row_input">
        <div class="input_with_text input_with_icon">
            <a href="tel:<?= !empty($company->contact_phone) ? '+' . $company->contact_phone : '' ?>"
               class="call_now"><span></span></a>
            <?= Html::activeTextInput($company, 'contact_phone', [
                'class'     => 'mask_phone',
                'data-lang' => mb_substr(app()->language, 0, 2),
            ]); ?>
            <span></span>
        </div>
    </div>
    <?= $form->field($company, 'contact_phone')->end(); ?>

</section>

<section class="row">
    <?= $form->field($company, 'contact_email')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($company, 'contact_email') ?></div>
    <div class="row_input">
        <?= Html::activeTextInput($company, 'contact_email') ?>
        <?= Html::error($company, 'contact_email',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($company, 'contact_email')->end(); ?>
</section>

<h2><?= Yii::t('client', 'Contract') ?></h2>

<section class="row">
    <?= $form->field($company, 'contract_number')->begin(); ?>
    <div class="row_label"><?= Html::activeLabel($company, 'contract_number') ?></div>
    <div class="row_input">
        <?= Html::activeTextInput($company, 'contract_number') ?>
        <?= Html::error($company, 'contract_number',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
    </div>
    <?= $form->field($company, 'contract_number')->end(); ?>
</section>

<!--Дата начала-->
<section class="row">
    <div class="row_label"><?= Html::activeLabel($company, 'contract_start_date') ?></div>
    <div class="row_input">
        <div class="grid_3">
            <?= $form->field($company, 'contract_start_day')->begin(); ?>
            <div class="grid_item">
                <?= Html::activeDropDownList($company, 'contract_start_day', $dataForm['STARTCONTRACT']['DAY'], [
                    'data-placeholder' => !empty($company->contract_start_day) ? Html::encode($company->contract_start_day) : t('user',
                        'Day'),
                    'class'            => 'default_select',
                ]); ?>
            </div>
            <?= $form->field($company, 'contract_start_day')->end(); ?>
            <?= $form->field($company, 'contract_start_month')->begin(); ?>
            <div class="grid_item">
                <?= Html::activeDropDownList($company, 'contract_start_month', $dataForm['STARTCONTRACT']['MONTH'], [
                    'data-placeholder' => !empty($company->contract_start_month) ? Html::encode($dataForm['STARTCONTRACT']['MONTH'][$company->contract_start_month]) : t('user',
                        'Month'),
                    'class'            => 'default_select',
                ]); ?>
            </div>
            <?= $form->field($company, 'contract_start_month')->end(); ?>
            <?= $form->field($company, 'contract_start_year')->begin(); ?>
            <div class="grid_item">
                <?= Html::activeDropDownList($company, 'contract_start_year', $dataForm['STARTCONTRACT']['YEAR'], [
                    'data-placeholder' => !empty($company->contract_start_year) ? Html::encode($company->contract_start_year) : t('user',
                        'Year'),
                    'class'            => 'default_select',
                ]); ?>
            </div>
            <?= $form->field($company, 'contract_start_year')->end(); ?>
        </div>
    </div>
</section>

<!--Дата окончания-->
<section class="row">
    <div class="row_label"><?= Html::activeLabel($company, 'contract_end_date') ?></div>
    <div class="row_input">
        <div class="grid_3">
            <?= $form->field($company, 'contract_end_day')->begin(); ?>
            <div class="grid_item">
                <?= Html::activeDropDownList($company, 'contract_end_day', $dataForm['ENDCONTRACT']['DAY'], [
                    'data-placeholder' => !empty($company->contract_end_day) ? Html::encode($company->contract_end_day) : t('user',
                        'Day'),
                    'class'            => 'default_select',
                ]); ?>
            </div>
            <?= $form->field($company, 'contract_end_day')->end(); ?>
            <?= $form->field($company, 'contract_end_month')->begin(); ?>
            <div class="grid_item">
                <?= Html::activeDropDownList($company, 'contract_end_month', $dataForm['ENDCONTRACT']['MONTH'], [
                    'data-placeholder' => !empty($company->contract_end_month) ? Html::encode($dataForm['ENDCONTRACT']['MONTH'][$company->contract_end_month]) : t('user',
                        'Month'),
                    'class'            => 'default_select',
                ]); ?>
            </div>
            <?= $form->field($company, 'contract_end_month')->end(); ?>
            <?= $form->field($company, 'contract_end_year')->begin(); ?>
            <div class="grid_item">
                <?= Html::activeDropDownList($company, 'contract_end_year', $dataForm['ENDCONTRACT']['YEAR'], [
                    'data-placeholder' => !empty($company->contract_end_year) ? Html::encode($company->contract_end_year) : t('user',
                        'Year'),
                    'class'            => 'default_select',
                ]); ?>
            </div>
            <?= $form->field($company, 'contract_end_year')->end(); ?>
        </div>
    </div>
</section>

<section class="row">
    <?= $form->field($company, 'credits')->begin(); ?>
    <div class="row_label">
        <?= Html::activeLabel($company, 'credits') ?>
        <a data-tooltip="2" class="tooltip">?
            <span style="display: none; opacity: 0;">
                <?= Yii::t('hint', 'Admissible negative balance to which the organization can make orders.'); ?>
            </span>
        </a>
    </div>
    <div class="row_input">
        <?= Html::activeTextInput($company, 'credits', ['class' => 'input_number_mini']) ?>
        <?= Html::error($company, 'site',
            ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']); ?>
        <span class="js-currency" style="line-height: 30px"><?= $currency ?></span>
    </div>
    <?= $form->field($company, 'credits')->end(); ?>
</section>

<section class="submit_form">
    <div>
        <? if (!$company->isNewRecord): ?>
            <label><?= Html::activeCheckbox($company, 'block', ['label' => null]) ?> <b><?= t('client', 'Block') ?></b></label>
        <? endif ?>
    </div>
    <?= Html::submitInput(t('app', 'Save')) ?>
</section>

<?php ActiveForm::end(); ?>
