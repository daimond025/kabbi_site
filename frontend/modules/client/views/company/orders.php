<?
/* @var $this yii\web\View */
/* @var $company app\modules\client\ClientCompany */
if(!empty($company->orders)):?>
    <?
    $this->registerJs("calendarPeriodInit();");
    $this->registerJsFile('/js/leaflet-0.7.3/leaflet.js');
    ?>
<div class="client_orders">
    <div class="oor_fil" style="margin-bottom: 15px;">
        <div class="cof_left">
            <?php if (app()->user->can('organization')) : ?>
            <div style="float: right; position: relative; top: 5px;">
                <div class="context_menu_wrap">
                    <a class="context_menu a_sc"><?= t('client', 'Export'); ?></a>
                    <div class="context_menu_content b_sc js-no-stop">
                        <a href="<?= \yii\helpers\Url::to('/client/company/dump-orders/' . $company->company_id); ?>" class="js-export-order"><?= t('client', 'Download to Excel'); ?></a>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <form name="order_filter" action="<?=  \yii\helpers\Url::to(['/client/company/order-search', 'id' => $company->company_id])?>" method="post">
                <div class="cof_first" style="margin-right: 25px;">
                    <label>
                        <input name="filter" type="radio" checked="" value="limit"> <?=t('client', 'Last 10')?>
                    </label>
                </div>
                
                <div class="cof_second disabled_cof">
                    <label><input name="filter" id="cof_time_choser" type="radio" value="period"> <?=t('reports', 'Period')?></label>
                    <div class="cof_date notranslate disabled_select">
                        <a class="s_datepicker a_sc select"><?= date("d.m.Y", mktime(0, 0, 0, date("m")  , date("d")-14, date("Y")));?></a>
                        <div class="s_datepicker_content b_sc" style="display: none;">
                            <input name="first_date" class="sdp_input" type="text" value="<?= date("d.m.Y", mktime(0, 0, 0, date("m")  , date("d")-14, date("Y")));?>">
                            <div class="sdp first_date"></div>
                        </div>
                    </div>
                    <hr>
                    <div class="cof_date notranslate disabled_select">
                        <a class="s_datepicker a_sc select"><?= date("d.m.Y");?></a>
                        <div class="s_datepicker_content b_sc" style="display: none;">
                            <input name="second_date" class="sdp_input" type="text" value="<?= date("d.m.Y");?>">
                            <div class="sdp"></div>
                        </div>
                    </div>
                </div>
                
            </form>
        </div>
    </div>
    <div class="co_list">
        <?=$this->render('_orders', ['company' => $company])?>
    </div>
</div>
<?else:?>
    <p><?=t('app', 'Empty')?></p>
<?endif;?>
