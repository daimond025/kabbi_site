<?php

/* @var $this yii\web\View */
/* @var $clients app\modules\client\models\Client */
use yii\helpers\Url;
use yii\helpers\Html;

if (!empty($clients)):?>

    <form name="user_search" method="post" onsubmit="return false" action="<?= Url::to([
        '/client/company/staff-search/',
        'company' => isset($clients[0]->clientHasCompanies[0]->company_id) ? $clients[0]->clientHasCompanies[0]->company_id : null,
    ]) ?>">
        <div class="grid_3" style="height: 50px;">
            <div class="grid_item">
                <div class="select_checkbox">
                    <a class="a_sc select" rel="<?= t('user', 'All cities') ?>"><?= t('user', 'All cities') ?></a>
                    <div class="b_sc">
                        <ul>
                            <? foreach ($user_city_list as $city_id => $city):?>
                                <li class="li_filter"><label><input name="city_id[]" type="checkbox"
                                                                    value="<?= Html::encode($city_id) ?>"/> <?= Html::encode($city); ?>
                                    </label></li>
                            <? endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="grid_item">
                <input type="text" name="input_search" placeholder="<?= t('client', 'Filter by name or phone') ?>"/>
            </div>
            <div class="grid_item">
                <?php if (app()->user->can('organization')) : ?>
                    <div class="context_menu_wrap export_menu_wrap">
                        <a class="context_menu a_sc select"><?= t('client', 'Export'); ?></a>
                        <div class="context_menu_content b_sc js-no-stop">
                            <a href="<?= Url::to(['/client/company/dump-stuff', 'company' => $id]); ?>"
                               class="js-export-stuff"><?= t('client', 'Download to Excel'); ?></a>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </form>
    <table id="company_clients_list" class="people_table sort" style="table-layout: fixed;">
        <tr>
            <th class="pt_fio" style="width: 35%;"><a href="#"><?= t('client', 'Full name') ?> <span
                            data-type="last_name" class="pt_down"></span></a> <!--45%!-->
            </th>
            <!--<th style="width: 16%;"></?=t('client', 'Orders')?>-->
            <!--</th>-->
            <th style="width: 16%;"><?= t('client', 'Activity') ?>
            </th>
            <th class="pt_city" style="width: 23%;"><a href="#"><?= t('client', 'City') ?> <span data-type="city_id"
                                                                                                 class=""></span></a>
            </th>
        </tr>
        <?= $this->render('_stuff', ['clients' => $clients, 'company_id' => $id]) ?>
    </table>
<?php else: ?>
    <?= t('client', 'No employees') ?>
<?php endif; ?>

