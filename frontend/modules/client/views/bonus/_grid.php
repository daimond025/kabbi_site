<?php

/* @var string $pjaxId */
/* @var array $cityList */
/* @var array $positionList */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\employee\models\worker\WorkerSearch */
/* @var string $workerDetailViewUrl */

use frontend\widgets\filter\Filter;
use frontend\widgets\LinkPager;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

$workerDetailViewUrl = getValue($workerDetailViewUrl, '/employee/worker/update');

$filter = Filter::begin([
    'pjaxId' => $pjaxId,
    'model'  => $searchModel,
]);
echo $filter->checkboxList('city_id', $cityList);
Filter::end();

Pjax::begin(['id' => $pjaxId]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'       => "{items}\n{pager}",
    'tableOptions' => [
        'class' => 'people_table',
    ],
    'pager'        => [
        'class'         => LinkPager::className(),
        'prevPageLabel' => t('app', 'Prev'),
        'nextPageLabel' => t('app', 'Next'),
    ],
    'columns'      => [
        [
            'attribute'     => 'name',
            'label'         => t('client-bonus', 'Name'),
            'headerOptions' => ['class' => 'pt_wor'],
            'content'       => function ($model) {
                return Html::a(Html::encode($model['name']), ['update', 'id' => $model['bonus_id']],
                    ['data-pjax' => 0]);
            },
        ],
        [
            'attribute'     => 'city',
            'label'         => t('bonus-system', 'Bonus system'),
            'headerOptions' => ['class' => 'pt_re'],
            'content'       => function ($model) use ($cityList) {
                return Html::encode($model['bonusSystem']['name']);
            },
        ],
        [
            'attribute'     => 'city',
            'label'         => t('client-bonus', 'City'),
            'headerOptions' => ['class' => 'pt_re'],
            'content'       => function ($model) use ($cityList) {
                return $cityList[$model['city_id']];
            },
        ],
        [
            'attribute'     => 'car_class',
            'label'         => t('client-bonus', 'Car class'),
            'headerOptions' => ['class' => 'pt_re'],
            'content'       => function ($model) {
                return Html::encode(t('car', $model['class']['class']));
            },
        ],
    ],
]);

Pjax::end();