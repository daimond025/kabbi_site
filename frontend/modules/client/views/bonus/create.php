<?php

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Add');

?>

    <div class="bread"><a href="<?= Url::to(['bonus/list']) ?>"><?= t('app', 'Bonus system') ?></a></div>

    <h1><?= Html::encode($this->title) ?></h1>

<?= $this->render('_form',
    compact('model', 'gootaxBonus', 'cities', 'hasCar', 'positions', 'classes', 'tariffs', 'bonusSystems')); ?>