<?php

use yii\helpers\Html;

/* @var $classes array */
$content = '';

if (count($classes)) {
    $firstItem = true;

    foreach ($classes as $key => $value) {
        $content .= '<option value="' . $key
            . '" ' . ($firstItem ? 'selected' : '') . '>'
            . Html::encode($value) . '</option>';
        $firstItem = false;
    }
}

echo $content;