<?php

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = t('app', 'Bonus system');

if (app()->user->can('bonus')) {
    echo Html::a(t('app', 'Add'), ['create'], ['class' => 'button', 'style' => 'float: right; position: relative; top: 5px;']);
}

?>

<h1><?= Html::encode($this->title) ?></h1>


<section class="main_tabs">
    <div class="tabs_links">
        <ul>
            <li><a href="#active" class="t01"><?= t('app', 'Active') ?></a></li>
            <li><a href="#blocked" class="t03" data-href="<?= Url::to('list-blocked') ?>"><?= t('app', 'Blocked') ?></a>
            </li>
        </ul>
    </div>
    <div class="tabs_content">
        <div id="t01" data-view="index">
            <?= $this->render('_grid', compact('searchModel', 'dataProvider', 'cityList', 'pjaxId')) ?>
        </div>
        <div id="t02" data-view="new">
        </div>
        <div id="t03" data-view="blocked">
        </div>
    </div>
</section>
