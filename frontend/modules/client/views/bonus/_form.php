<?php

use frontend\modules\bonus\components\BonusSystemService;
use frontend\modules\client\models\ClientBonusGootax;
use frontend\widgets\position\PositionChooser;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use frontend\modules\client\assets\BonusAsset;
use frontend\modules\client\models\ClientBonus;
use frontend\widgets\validity_input\ValidityInput;

BonusAsset::register($this);

/* @var $model ClientBonus */
/* @var $gootaxBonus ClientBonusGootax */
/* @var $cities array */
/* @var $hasCar boolean */
/* @var $positions array */
/* @var $classes array */
/* @var $tariffs array */
/* @var $bonusSystems array */

$disabled = $model->scenario == ClientBonus::SCENARIO_READ;

?>

<div class="<?= $disabled ? 'non-editable' : ''; ?>">

    <?php $form = ActiveForm::begin([
        'errorCssClass'          => 'input_error',
        'successCssClass'        => 'input_success',
        'enableClientValidation' => false,
        'enableAjaxValidation'   => false,
        'options'                => [
            'data' => !$model->isNewRecord ? ['alltariffs' => 1] : null,
        ],
    ]); ?>

    <?= Html::activeHiddenInput($model, 'bonus_id'); ?>

    <section class="row">
        <?= $form->field($model, 'bonus_system_id')->begin(); ?>
        <div class="row_label">
            <?= Html::activeLabel($model, 'bonus_system_id'); ?>
        </div>
        <div class="row_input">
            <?= Html::activeDropDownList($model, 'bonus_system_id', $bonusSystems, [
                'data-height' => 150,
                'class'       => 'default_select',
                'disabled'    => $disabled,
                'data'        => [
                    'gootax-id' => BonusSystemService::BONUS_SYSTEM_ID_GOOTAX,
                ],
            ]); ?>
            <div class="help-block" style="color: red">
                <?= Html::error($model, 'bonus_system_id'); ?>
            </div>
        </div>
        <?= $form->field($model, 'bonus_system_id')->end(); ?>
    </section>

    <section class="row">
        <?= $form->field($model, 'name')->begin(); ?>
        <div class="row_label">
            <?= Html::activeLabel($model, 'name'); ?>
        </div>
        <div class="row_input">
            <?= Html::activeTextInput($model, 'name', [
                'disabled' => $disabled,
            ]); ?>
            <div class="help-block" style="color: red">
                <?= Html::error($model, 'name') ?>
            </div>
        </div>
        <?= $form->field($model, 'name')->end(); ?>
    </section>


    <section class="row">
        <?= $form->field($model, 'city_id')->begin(); ?>
        <div class="row_label">
            <?= Html::activeLabel($model, 'city_id'); ?>
        </div>
        <div class="row_input">
            <?= Html::activeDropDownList($model, 'city_id', $cities, [
                'data-placeholder' => t('client-bonus', 'Choose city'),
                'data-height'      => 150,
                'class'            => 'default_select',
                'disabled'         => $disabled,
            ]); ?>
            <div class="help-block" style="color: red">
                <?= Html::error($model, 'city_id'); ?>
            </div>
        </div>
        <?= $form->field($model, 'city_id')->end(); ?>
    </section>

    <section class="row">
        <?= $form->field($model, 'position_id')->begin(); ?>
        <div class="row_label">
            <?= Html::activeLabel($model, 'position_id'); ?>
        </div>
        <div class="row_input">
            <?= Html::activeDropDownList($model, 'position_id', $positions, [
                'data-placeholder' => t('employee', 'Choose the position'),
                'data-height'      => 150,
                'class'            => 'default_select',
                'disabled'         => $disabled,
            ]); ?>
            <div class="help-block" style="color: red">
                <?= Html::error($model, 'position_id') ?>
            </div>
        </div>
        <?= $form->field($model, 'position_id')->end(); ?>
    </section>

    <section class="row" <?= !$hasCar ? 'style="display: none"' : '' ?>>
        <?= $form->field($model, 'class_id')->begin(); ?>
        <div class="row_label">
            <?= Html::activeLabel($model, 'class_id'); ?>
        </div>
        <div class="row_input">
            <?= Html::activeDropDownList($model, 'class_id', $classes, [
                'data-placeholder' => t('client-bonus', 'Choose car class'),
                'prompt'           => t('client-bonus', 'Choose car class'),
                'data-height'      => 150,
                'class'            => 'default_select',
                'disabled'         => $disabled,
            ]); ?>
            <div class="help-block" style="color: red">
                <?= Html::error($model, 'class_id') ?>
            </div>
        </div>
        <?= $form->field($model, 'class_id')->end(); ?>
    </section>

    <section class="row">
        <?= $form->field($model, 'choosed_tariffs')->begin(); ?>
        <div class="row_label">
            <?= Html::activeLabel($model, 'choosed_tariffs'); ?>
        </div>
        <div class="row_input">
            <?php
            echo Html::activeCheckboxList($model, 'choosed_tariffs', $tariffs, [
                'class' => 'checbox_list',
                'item'  => function ($index, $label, $name, $checked, $value) use ($disabled) {
                    return Html::checkbox($name, $checked, [
                        'value'        => $value,
                        'disabled'     => $disabled,
                        'label'        => Html::encode($label['name'])
                            . ($value['already-choosed'] ? ' ' . t('client-bonus',
                                    '(used in the other bonus system)') : ''),
                        'labelOptions' => [
                            'class' => ($label['block'] ? ' blocked-tariff' : '')
                                . ($label['alreadyChoosed'] ? ' already-choosed-tariff' : ''),
                        ],
                    ]);
                },
            ]); ?>
            <div class="help-block" style="color: red">
                <?= Html::error($model, 'choosed_tariffs') ?>
            </div>
        </div>
        <?= $form->field($model, 'choosed_tariffs')->end(); ?>
    </section>

    <div class="js-gootax-bonus" style="display: none">
        <h2><?= t('client-bonus', 'Refill bonus'); ?></h2>
        <div class="active">
            <?= ValidityInput::widget([
                'id'                      => 'add_holidays',
                'label'                   => t('client-bonus', 'Actual Date'),
                'data'                    => explode(';', $gootaxBonus->actual_date),
                'input_name'              => $gootaxBonus->formName(),
                'read_view'               => $disabled,
                'show_upload_celebration' => false,
                'model'                   => $gootaxBonus,
                'attr_name'               => 'actual_date',
            ]) ?>
        </div>

        <section class="row">
            <?= $form->field($gootaxBonus, 'bonus')->begin(); ?>
            <div class="row_label">
                <?= Html::activeLabel($gootaxBonus, 'bonus'); ?>
            </div>
            <div class="row_input">
                <?= Html::activeTextInput($gootaxBonus, 'bonus', [
                    'class'    => 'input_number_mini',
                    'style'    => 'float: left; margin-right: 6px;',
                    'disabled' => $disabled,
                ]); ?>

                <?= $form->field($gootaxBonus, 'bonus_type',
                    ['options' => ['style' => 'float: left; width: 85px;']])->begin(); ?>
                <?= Html::activeDropDownList($gootaxBonus, 'bonus_type', $gootaxBonus->getBonusTypes(), [
                    'data-height' => '150',
                    'class'       => 'default_select',
                    'disabled'    => $disabled,
                ]); ?>
                <?= $form->field($gootaxBonus, 'bonus_type')->end(); ?>
            </div>
            <?= $form->field($gootaxBonus, 'bonus')->end(); ?>
        </section>


        <section class="row">
            <?= $form->field($gootaxBonus, 'min_cost')->begin(); ?>
            <div class="row_label">
                <?= Html::activeLabel($gootaxBonus, 'min_cost'); ?>
            </div>
            <div class="row_input">
                <?= Html::activeTextInput($gootaxBonus, 'min_cost', [
                    'class'    => 'input_number_mini',
                    'style'    => 'float: left; margin-right: 6px;',
                    'disabled' => $disabled,
                ]); ?>
                <?= Html::tag('div', getCurrencySymbol($model->city_id), [
                    'style' => 'float: left; line-height: 30px;',
                    'class' => 'currency',
                ]); ?>
            </div>
            <?= $form->field($gootaxBonus, 'min_cost')->end(); ?>
        </section>


        <section class="row">
            <?= $form->field($gootaxBonus, 'bonus_app')->begin(); ?>
            <div class="row_label">
                <?= Html::activeLabel($gootaxBonus, 'bonus_app'); ?>
            </div>
            <div class="row_input">
                <?= Html::activeTextInput($gootaxBonus, 'bonus_app', [
                    'class'    => 'input_number_mini',
                    'style'    => 'float: left; margin-right: 6px;',
                    'disabled' => $disabled,
                ]); ?>

                <?= $form->field($gootaxBonus, 'bonus_app_type',
                    ['options' => ['style' => 'float: left; width: 85px;']])->begin(); ?>
                <?= Html::activeDropDownList($gootaxBonus, 'bonus_app_type', $gootaxBonus->getBonusTypes(), [
                    'data-height' => '150',
                    'class'       => 'default_select',
                    'disabled'    => $disabled,
                ]); ?>
                <?= $form->field($gootaxBonus, 'bonus_app_type')->end(); ?>
            </div>
            <?= $form->field($gootaxBonus, 'bonus_app')->end(); ?>
        </section>

        <h2><?= t('client-bonus', 'Bonus payment'); ?></h2>

        <section class="row">
            <div>
                <div class="row_label">
                    <?= Html::activeLabel($gootaxBonus, 'payment_method'); ?>
                </div>
                <div class="row_input">
                    <div style="float: left; width: 220px; margin-right: 6px;">
                        <?= $form->field($gootaxBonus, 'payment_method')->begin(); ?>
                        <?= Html::activeDropDownList($gootaxBonus, 'payment_method', $gootaxBonus->getPaymentMethods(),
                            [
                                'data-height' => '150',
                                'class'       => 'default_select',
                                'disabled'    => $disabled,
                            ]); ?>
                        <?= $form->field($gootaxBonus, 'payment_method')->end(); ?>
                    </div>

                    <?= $form->field($gootaxBonus, 'max_payment')->begin(); ?>

                    <div style="float: left; line-height: 30px; width: 30px; text-align: center; margin-right: 6px;">
                        <?= t('client-bonus', 'до'); ?>
                    </div>

                    <?= Html::activeTextInput($gootaxBonus, 'max_payment', [
                        'class'    => 'input_number_mini',
                        'style'    => 'float: left; margin-right: 6px;',
                        'disabled' => $disabled,
                    ]); ?>

                    <?= $form->field($gootaxBonus, 'max_payment_type')->begin(); ?>
                    <div style="float: left; width: 85px;">
                        <?= Html::activeDropDownList($gootaxBonus, 'max_payment_type', $gootaxBonus->getBonusTypes(), [
                            'data-height' => '150',
                            'class'       => 'default_select',
                            'disabled'    => $disabled,
                        ]); ?>
                    </div>
                    <?= $form->field($gootaxBonus, 'max_payment_type')->end(); ?>
                    <?= $form->field($gootaxBonus, 'max_payment')->end(); ?>
                </div>
            </div>
        </section>
    </div>


    <section class="submit_form">
        <?php if (!$model->isNewRecord): ?>
            <div>
                <label>
                    <?= Html::activeCheckbox($model, 'blocked', [
                        'label'    => null,
                        'disabled' => $disabled,
                    ]); ?>
                    <b><?= t('app', 'Block'); ?></b>
                </label>
            </div>
        <?php endif; ?>

        <?php if (!$disabled) {
            echo Html::submitInput(t('app', 'Save'));
        } ?>
    </section>


    <?php ActiveForm::end(); ?>

</div>