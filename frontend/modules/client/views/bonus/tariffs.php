<?php

use yii\helpers\Html;

foreach ($tariffs as $key => $value) {
    echo Html::tag('div',
            Html::checkbox('ClientBonus[choosed_tariffs][]', true, [
                'value'        => $key,
                'label'        => Html::encode($value['label'])
                    . ($value['already-choosed'] ? ' ' . t('client-bonus', '(used in the other bonus system)') : ''),
                'labelOptions' => [
                    'class' => ($value['blocked'] ? ' blocked-tariff' : '')
                        . ($value['already-choosed'] ? ' already-choosed-tariff' : ''),
             ]]));
}