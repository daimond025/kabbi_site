<?php
/**
 * @var yii\web\View $this
 * @var array        $parkingList
 * @var boolean      $readOnly
 * @var integer      $typeId
 */

use yii\helpers\Html;
use yii\helpers\Url;

?>

<div>

    <? foreach ($parkingList as $parkingId => $parking): ?>
        <h3>
            <?= Html::a(Html::encode($parking), null, [
                'class' => 'spoiler',
                'data'  => [
                    'spoilerId' => 'fix-' . $parkingId,
                    'url'       => Url::to(['/client/tariff/get-fix', 'id' => $parkingId, 'typeId' => $typeId]),
                ],
            ]); ?>

        </h3>
        <?= Html::tag('section', '', [
            'id'    => 'fix-' . $parkingId,
            'class' => 'spoler_content',
            'style' => 'display: none',
        ]); ?>

    <? endforeach; ?>


</div>