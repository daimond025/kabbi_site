<?php
/**
 * @var yii\web\View $this
 * @var \frontend\modules\client\models\tariff\TariffOptionForm $model
 * @var string $currency
 */


use yii\helpers\Html;
use frontend\modules\client\models\tariff\TariffOption;
use yii\helpers\ArrayHelper;


?>


<!-- Сортировка -->
<section class="row">
    <?= Html::tag('div', Html::activeLabel($model, 'sort'), ['class' => 'row_label']); ?>
    <?= Html::tag('span', $model->sort) ?>
</section>

<!-- Зафиксировать расчет -->
<section class="row">
    <?= Html::tag('div', '', ['class' => 'row_label']); ?>
    <?= Html::tag('div', Html::activeCheckbox($model, 'calculation_fix', ['disabled' => true]),
        ['class' => 'row_input']); ?>
</section>

<!-- Тип расчета -->
<div class="ap_top">
    <div>
        <div class="ap_top_f">
            <h3><?= t('taxi_tariff', 'City'); ?></h3>
            <?= Html::activeDropDownList($model, 'city_accrual', TariffOption::getTypeMap(), [
                'class' => 'default_select',
                'disabled' => true,
            ]) ?>
        </div>
        <div class="ap_top_s">
            <h3><?= t('taxi_tariff', 'Track'); ?></h3>
            <?= Html::activeDropDownList($model, 'track_accrual', TariffOption::getTypeMap(), [
                'class' => 'default_select',
                'disabled' => true,
            ]) ?>
        </div>
    </div>
</div>

<!-- Стоимость посадки -->
<div class="ap_cont">
    <div>
        <?= Html::activeLabel($model, 'city_planting_price') ?>
        <div class="city">
            <?= Html::tag('span', $model->city_planting_price . ' ' . $currency, ['class' => 'currency_symbol']) ?>
        </div>
        <div class="track">
            <?= Html::tag('span', $model->track_planting_price . ' ' . $currency, ['class' => 'currency_symbol']) ?>
        </div>
    </div>
</div>

<!-- Включено в стоимость посадки -->
<div class="ap_cont" data-accruals="DISTANCE,TIME,MIXED,INTERVAL"
     style="display: <?= ($model->city_accrual === TariffOption::ACCRUAL_FIX && $model->track_accrual === TariffOption::ACCRUAL_FIX) ? 'none' : 'block' ?>">
    <div>
        <?= Html::activeLabel($model, 'city_planting_include') ?>
        <div class="city">
            <span>
                <?= $model->city_planting_include ?>
                <? if ($model->city_accrual !== TariffOption::ACCRUAL_TIME): ?>
                    <?= t('app', 'km') ?>
                    <br>
                <? endif; ?>

                <? if ($model->city_accrual === TariffOption::ACCRUAL_MIXED): ?>
                    <?= $model->city_planting_include_time ?>
                <? endif; ?>

                <? if ($model->city_accrual === TariffOption::ACCRUAL_MIXED || $model->city_accrual === TariffOption::ACCRUAL_TIME): ?>
                    <?= t('app', 'min.') ?>
                <? endif; ?>
            </span>
        </div>

        <div class="track">
            <span>
                <?= $model->track_planting_include ?>
                <? if ($model->track_accrual !== TariffOption::ACCRUAL_TIME): ?>
                    <?= t('app', 'km') ?>
                    <br>
                <? endif; ?>

                <? if ($model->track_accrual === TariffOption::ACCRUAL_MIXED): ?>
                    <?= $model->track_planting_include_time ?>
                <? endif; ?>

                <? if ($model->track_accrual === TariffOption::ACCRUAL_MIXED || $model->track_accrual === TariffOption::ACCRUAL_TIME): ?>
                    <?= t('app', 'min.') ?>
                <? endif; ?>
            </span>
        </div>

    </div>
</div>


<!-- Стоимость далее -->
<div class="ap_cont" data-accruals="DISTANCE,TIME,MIXED,INTERVAL">
    <div>
        <?= Html::activeLabel($model, 'city_next_km_price') ?>
        <div class="city">
            <span>
                <? if ($model->city_accrual !== TariffOption::ACCRUAL_INTERVAL): ?>
                    <?= $model->city_next_km_price ?>
                <? endif; ?>

                <? if ($model->city_accrual !== TariffOption::ACCRUAL_TIME && $model->city_accrual !== TariffOption::ACCRUAL_INTERVAL): ?>
                    <?= t('app', 'km') ?>
                    <br>
                <? endif; ?>

                <? if ($model->city_accrual === TariffOption::ACCRUAL_MIXED): ?>
                    <?= $model->city_next_km_price_time ?>
                <? endif; ?>

                <? if ($model->city_accrual === TariffOption::ACCRUAL_MIXED || $model->city_accrual === TariffOption::ACCRUAL_TIME): ?>
                    <?= ArrayHelper::getValue(TariffOption::getKmPriceMap($currency), $model->city_next_cost_unit) ?>
                <? endif; ?>
            </span>

            <div class="js-accrual-interval tar_dist_and_diap"
                 style="display: <?= $model->city_accrual === TariffOption::ACCRUAL_INTERVAL ? 'inline-block' : 'none' ?>">
                <?= $this->render('_intervalForm_read',
                    ['model' => $model, 'currency' => $currency, 'type' => 'city']) ?>
            </div>
        </div>

        <div class="track">
            <span>
                <? if ($model->track_accrual !== TariffOption::ACCRUAL_INTERVAL): ?>
                    <?= $model->track_next_km_price ?>
                <? endif; ?>

                <? if ($model->track_accrual !== TariffOption::ACCRUAL_TIME && $model->track_accrual !== TariffOption::ACCRUAL_INTERVAL): ?>
                    <?= t('app', 'km') ?>
                    <br>
                <? endif; ?>

                <? if ($model->track_accrual === TariffOption::ACCRUAL_MIXED): ?>
                    <?= $model->track_next_km_price_time ?>
                <? endif; ?>

                <? if ($model->track_accrual === TariffOption::ACCRUAL_MIXED || $model->track_accrual === TariffOption::ACCRUAL_TIME): ?>
                    <?= ArrayHelper::getValue(TariffOption::getKmPriceMap($currency), $model->track_next_cost_unit) ?>
                <? endif; ?>
            </span>

            <div class="js-accrual-interval tar_dist_and_diap"
                 style="display: <?= $model->track_accrual === TariffOption::ACCRUAL_INTERVAL ? 'inline-block' : 'none' ?>">
                <?= $this->render('_intervalForm_read',
                    ['model' => $model, 'currency' => $currency, 'type' => 'track']) ?>
            </div>

        </div>

    </div>
</div>


<!-- Минимальная стоимость поездки -->
<div class="ap_cont" data-accruals="DISTANCE,TIME,MIXED,INTERVAL">
    <div>
        <?= Html::activeLabel($model, 'city_min_price') ?>
        <div class="city">
            <?= Html::tag('span', $model->city_min_price . ' ' . $currency, ['class' => 'currency_symbol']) ?>
        </div>
        <div class="track">
            <?= Html::tag('span', $model->track_min_price . ' ' . $currency, ['class' => 'currency_symbol']) ?>

        </div>
    </div>
</div>


<!-- Добавочная стоимость к минимальной за каждую точку, начиная с 3-ей -->
<div class="ap_cont" data-accruals="DISTANCE,TIME,MIXED,INTERVAL">
    <div>
        <?= Html::activeLabel($model, 'city_second_min_price') ?>
        <div class="city">
            <?= Html::tag('span', $model->city_second_min_price . ' ' . $currency, ['class' => 'currency_symbol']) ?>
        </div>
        <div class="track">
            <?= Html::tag('span', $model->track_second_min_price . ' ' . $currency, ['class' => 'currency_symbol']) ?>

        </div>
    </div>
</div>


<!-- Стоимость 1 км до адреса подачи -->
<div class="ap_cont" data-accruals="DISTANCE,TIME,MIXED,INTERVAL">
    <div>
        <?= Html::activeLabel($model, 'city_supply_price') ?>
        <div class="city">
        </div>
        <div class="track">
            <?= Html::tag('span', $model->track_supply_price . ' ' . $currency, ['class' => 'currency_symbol']) ?>
        </div>
    </div>
</div>


<!-- Бесплатное время ожидания до начала поездки -->
<div class="ap_cont">
    <div>
        <?= Html::activeLabel($model, 'city_wait_time') ?>
        <div class="city">
            <?= Html::tag('span', $model->city_wait_time . ' ' . t('app', 'min.'), ['class' => 'currency_symbol']) ?>
        </div>
        <div class="track">
            <?= Html::tag('span', $model->track_wait_time . ' ' . t('app', 'min.'), ['class' => 'currency_symbol']) ?>

        </div>
    </div>
</div>


<!-- Бесплатное время ожидания во время поездки за 1 раз -->
<div class="ap_cont" data-accruals="DISTANCE,TIME,MIXED,INTERVAL">
    <div>
        <?= Html::activeLabel($model, 'city_wait_driving_time') ?>
        <div class="city">
            <?= Html::tag('span', $model->city_wait_driving_time . ' ' . t('app', 'sec.'),
                ['class' => 'currency_symbol']) ?>
        </div>
        <div class="track">
            <?= Html::tag('span', $model->track_wait_driving_time . ' ' . t('app', 'sec.'),
                ['class' => 'currency_symbol']) ?>

        </div>
    </div>
</div>

<!-- Стоимость 1 минуты ожидания -->
<div class="ap_cont">
    <div>
        <?= Html::activeLabel($model, 'city_wait_price') ?>
        <div class="city">
            <?= Html::tag('span', $model->city_wait_price . ' ' . $currency, ['class' => 'currency_symbol']) ?>
        </div>
        <div class="track">
            <?= Html::tag('span', $model->track_wait_price . ' ' . $currency, ['class' => 'currency_symbol']) ?>

        </div>
    </div>
</div>

<!-- Скорость при которой вкл. ожидание -->
<div class="ap_cont" data-accruals="DISTANCE,TIME,MIXED,INTERVAL"
     style="display: <?= ($model->city_accrual === TariffOption::ACCRUAL_FIX && $model->track_accrual === TariffOption::ACCRUAL_FIX) ? 'none' : 'block' ?>">
    <div>
        <?= Html::activeLabel($model, 'city_speed_downtime') ?>
        <div class="city">
            <?= Html::tag('span', $model->city_speed_downtime . ' ' . t('app', 'km/h'),
                ['class' => 'currency_symbol']) ?>
        </div>
        <div class="track">
            <?= Html::tag('span', $model->track_speed_downtime . ' ' . t('app', 'km/h'),
                ['class' => 'currency_symbol']) ?>

        </div>
    </div>
</div>

<!-- Округлять до -->
<div class="ap_cont">
    <div>
        <?= Html::activeLabel($model, 'city_rounding') ?>
        <div class="city">
            <?= Html::tag('span', $model->city_rounding . ' ' . $currency, ['class' => 'currency_symbol']) ?>
        </div>
        <div class="track">
            <?= Html::tag('span', $model->track_rounding . ' ' . $currency, ['class' => 'currency_symbol']) ?>

        </div>
    </div>
</div>

<!-- Правило округления -->
<div class="ap_cont">
    <div>
        <?= Html::activeLabel($model, 'city_rounding_type') ?>
        <div class="city">
            <div style="width: 140px; margin-right: 10px;">
                <?= Html::activeDropDownList($model, 'city_rounding_type', TariffOption::getRoundingTypeMap(), [
                    'class' => 'default_select',
                    'data-additional-classes' => 'normal-select',
                    'disabled' => true,
                ]) ?>
            </div>
        </div>
        <div class="track">
            <div style="width: 140px; margin-right: 10px;">
                <?= Html::activeDropDownList($model, 'track_rounding_type', TariffOption::getRoundingTypeMap(), [
                    'class' => 'default_select',
                    'data-additional-classes' => 'normal-select',
                    'disabled' => true,
                ]) ?>
            </div>
        </div>
    </div>
</div>