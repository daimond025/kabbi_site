<?php
/**
 * @var yii\web\View                                            $this
 * @var \frontend\modules\client\models\tariff\TariffHasFixForm $fixModel
 * @var string                                                  $currency
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin([
    'id'                     => 'parking-form-' . $fixModel->parkingId,
    'errorCssClass'          => 'input_error',
    'successCssClass'        => 'input_success',
    'enableAjaxValidation'   => true,
    'enableClientValidation' => true,
    'validateOnBlur'         => false,
    //    'validateOnChange'     => false,
    'validateOnSubmit'       => true,
    'action'                 => ['/client/tariff/save-fix-form', 'id' => $fixModel->parkingId, 'typeId' => $fixModel->typeId],
    'options'                => [
        'enctype'   => 'multipart/form-data',
        'data-pjax' => true,
        'class'     => 'js-fix-form js-client-options-form',
    ],
]);

?>

    <section>
        <div class="ap_cont js-parking-price">
            <? foreach ($fixModel->priceList as $fixId => $item): ?>
                <? if ($item['from'] === $item['to']): ?>
                    <div>
                        <?= Html::label(t('taxi_tariff', 'Within the area')); ?>
                        <?= Html::activeTextInput($fixModel, "priceList[{$fixId}][to_price]"); ?>
                        <?= Html::tag('span', $currency); ?>
                    </div>
                    <? continue ?>
                <? endif; ?>

                <div>

                    <? $label = $item['to_name']; ?>
                    <? if ($item['from'] == $fixModel->parkingId): ?>
                        <? $fromAttr = "priceList[{$fixId}][to_price]" ?>
                        <? $toAttr = "priceList[{$fixId}][from_price]" ?>
                        <? $fromJs = "js-{$item['from']}-to-{$item['to']}" ?>
                        <? $toJs = "js-{$item['to']}-to-{$item['from']}" ?>
                    <? else: ?>
                        <? $fromAttr = "priceList[{$fixId}][from_price]" ?>
                        <? $toAttr = "priceList[{$fixId}][to_price]" ?>
                        <? $fromJs = "js-{$item['to']}-to-{$item['from']}" ?>
                        <? $toJs = "js-{$item['from']}-to-{$item['to']}" ?>
                    <? endif; ?>

                    <?= Html::label($fixModel->getLabel($fixId)); ?>
                    <?= Html::activeTextInput($fixModel, $fromAttr, ['class' => $fromJs]); ?>
                    <?= Html::tag('span', $currency); ?>

                    <?= Html::tag('span', t('taxi_tariff', 'Back'), ['style' => 'width: 22%; text-align: right;']); ?>
                    <?= Html::activeTextInput($fixModel, $toAttr, ['class' => $toJs]); ?>
                    <?= Html::tag('span', $currency, ['class' => 'currency_symbol']); ?>

                </div>

            <? endforeach; ?>
        </div>
    </section>

    <section class="submit_form">
        <?= Html::tag('div', '', ['class' => 'row_input message', 'style' => 'display:none']); ?>
        <?= Html::submitInput(t('app', 'Save')) ?>
    </section>


<? ActiveForm::end(); ?>