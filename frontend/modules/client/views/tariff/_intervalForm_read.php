<?php

/**
 * @var yii\web\View                                            $this
 * @var \frontend\modules\client\models\tariff\TariffOptionForm $model
 * @var string                                                  $currency
 * @var string                                                  $type
 */

use yii\helpers\Html;

$attribute = $type . '_next_price_interval';
$intervalSize = count($model->{$attribute}['first_interval']);

?>

<? foreach ($model->{$attribute}['first_interval'] as $key => $firstInterval) : ?>

    <? if ($key === 0): ?>
        <div class="tdad_item first">
    <? elseif ($key === $intervalSize - 1): ?>
        <div class="tdad_item tdad_last">
    <? else: ?>
        <div class="tdad_item">
    <? endif; ?>

        <div class="tdad_number"><?= $key + 1 ?></div>

        <div class="tdad_interval">
            <?= Html::activeTextInput($model, $attribute . '[first_interval][]',
                [
                    'class' => 'first_interval_number',
                    'value' => $model->{$attribute}['first_interval'][$key],
                    'disabled' => true,
                ]) ?>



            <? if ($key === $intervalSize - 1): ?>
                <?= t('taxi_tariff', 'km and next'); ?>
            <? else: ?>
                <i>-</i>
                <?= Html::activeTextInput($model, $attribute . '[second_interval][]', [
                    'class' => 'second_interval_number',
                    'value' => $model->{$attribute}['second_interval'][$key],
                    'disabled' => true,
                ]) ?>
                <?= Html::tag('span', t('app', 'km'), ['class' => 'currency_symbol']) ?>
            <? endif; ?>


            <div class="tdad_sec_line">
                <?= Html::activeTextInput($model, $attribute . '[price][]', [
                    'value' => $model->{$attribute}['price'][$key],
                    'disabled' => true,
                ]) ?>
                <?= Html::tag('span', $currency . '/' . t('app', 'km'), ['class' => 'currency_symbol']) ?>
            </div>
        </div>
    </div>

<? endforeach; ?>
