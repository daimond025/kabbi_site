<?php


use frontend\modules\client\models\tariff\TariffForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use frontend\modules\client\assets\TariffAsset;

/**
 * @var yii\web\View $this
 * @var TariffForm   $tariff
 * @var boolean      $readOnly
 * @var array        $cityList
 * @var array        $positionList
 * @var array        $groupList
 * @var array        $typeList
 * @var array        $autoDownTimeList
 * @var array        $optionTypeList
 * @var array        $companyList
 * @var array        $carPositionList
 * @var array        $carClassMap
 */

TariffAsset::register($this);

?>

<div class="non-editable">
    <section class="row">
        <div class="row_label">
            <?= Html::activeLabel($tariff, 'logo') ?>
        </div>
        <div class="row_input">
            <span class="file-input">
                <? if ($tariff->isFileExists($tariff->logo)): ?>
                    <div class="input_file_logo input_file_loaded">
                        <a class="logo" href="<?= $tariff->getPicturePath($tariff->logo) ?>">
                            <?= $tariff->getPictureHtml($tariff->logo, false) ?>
                        </a>
                    </div>
                <? else: ?>
                    <div class="input_file_logo"></div>
                <? endif ?>
            </span>
        </div>
    </section>


    <section class="row">
        <?= Html::tag('div', Html::activeLabel($tariff, 'name'), ['class' => 'row_label']); ?>
        <div class="row_input">
            <?= Html::tag('span', $tariff->name) ?>
        </div>
    </section>


    <section class="row">
        <?= Html::tag('div', Html::activeLabel($tariff, 'city_id'), ['class' => 'row_label']); ?>
        <div class="row_input">
            <?= Html::tag('span', ArrayHelper::getValue($cityList, $tariff->city_id)) ?>
        </div>
    </section>


    <?= \frontend\widgets\position\PositionChooser::widget([
        'model'                 => $tariff,
        'carClassMap'           => $carClassMap,
        'carPositionList'       => $carPositionList,
        'positionMap'           => $positionList,
        'positionDisabled'      => true,
        'carClassDisabled'      => true,
        'positionClassDisabled' => true,
    ]) ?>


    <section class="row">
        <?= Html::tag('div', Html::activeLabel($tariff, 'group_id'), ['class' => 'row_label']); ?>
        <div class="row_input">
            <?= Html::tag('span', ArrayHelper::getValue($groupList, $tariff->group_id)) ?>
        </div>
    </section>


    <section class="row">
        <?= Html::tag('div', Html::activeLabel($tariff, 'description'), ['class' => 'row_label']); ?>
        <div class="row_input">
            <?= Html::tag('span', $tariff->description) ?>
        </div>
    </section>


    <section class="row">
        <?= Html::tag('div', Html::label(t('taxi_tariff', 'Where available the tariff')), ['class' => 'row_label']); ?>
        <div class="row_input">
            <?= Html::activeCheckbox($tariff, 'enabled_site', ['disabled' => true]) ?>
            <br/>
            <?= Html::activeCheckbox($tariff, 'enabled_app', ['disabled' => true]) ?>
            <br/>
            <?= Html::activeCheckbox($tariff, 'enabled_operator', ['disabled' => true]) ?>
            <br/>
            <?= Html::activeCheckbox($tariff, 'enabled_bordur', ['disabled' => true]) ?>
            <br/>
            <?= Html::activeCheckbox($tariff, 'enabled_cabinet', ['disabled' => true]) ?>
            <br/>
            <?= Html::activeCheckbox($tariff, 'enabled_hospital', ['disabled' => true]) ?>
        </div>
    </section>

    <section class="row">
        <?= Html::tag('div', Html::activeLabel($tariff, 'sort'), ['class' => 'row_label']); ?>
        <div class="row_input">
            <?= Html::tag('span', $tariff->sort) ?>
        </div>
    </section>


    <section class="row">
        <?= Html::tag('div', Html::activeLabel($tariff, 'type'), ['class' => 'row_label']); ?>
        <div class="row_input">
            <?= Html::tag('span', ArrayHelper::getValue($typeList, $tariff->type)) ?>
        </div>
    </section>


    <section class="row js-row-company" <?= $tariff->isCompanyRequired ? '' : 'style="display: none'; ?> ">
    <div class="row_label"><?= Html::activeLabel($tariff, 'company_ids') ?></div>
    <div class="row_input">
        <?= Html::activeCheckboxList($tariff, 'company_ids', $companyList, [
            'class' => 'select_checkbox',
            'item'  => function ($index, $label, $name, $checked, $value) use ($tariff, $companyList) {
                $input     = Html::checkbox($name, $checked, ['value' => $value, 'disabled' => true]);
                $label     = Html::label($input . Html::encode($label));
                $innerHtml = Html::tag('div', $label);

                return $innerHtml;
            },
        ]) ?>
    </div>
    </section>


    <section class="row">
        <?= Html::tag('div', Html::activeLabel($tariff, 'auto_downtime'), ['class' => 'row_label']); ?>
        <div class="row_input">
            <?= Html::tag('span', ArrayHelper::getValue($autoDownTimeList, $tariff->auto_downtime)) ?>
        </div>
    </section>


    <?= $this->render('_optionList', compact('tariff', 'readOnly', 'optionTypeList')); ?>


</div>
