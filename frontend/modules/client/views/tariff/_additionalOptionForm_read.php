<?php

/**
 * @var yii\web\View                                                      $this
 * @var \frontend\modules\client\models\tariff\TariffAdditionalOptionForm $additionalModel
 * @var string                                                            $currency
 */

use yii\helpers\Html;

?>

<div class="ap_cont">

    <? foreach ($additionalModel->name as $optionId => $name) : ?>

        <div class="check_option">
            <?= Html::activeCheckbox($additionalModel, "active[{$optionId}]", [
                'label' => $name,
                'disabled' => true
            ]); ?>
            <?= Html::tag('span', $additionalModel->price[$optionId] . ' ' . $currency, ['class' => 'currency_symbol']) ?>
        </div>

    <? endforeach; ?>

</div>




