<?php
/**
 * @var yii\web\View $this
 * @var TariffForm   $tariff
 * @var array        $cityList
 * @var array        $positionList
 * @var array        $groupList
 * @var array        $typeList
 * @var array        $autoDownTimeList
 * @var array        $optionTypeList
 * @var array        $companyList
 * @var array        $carPositionList
 * @var array        $carClassMap
 */

use frontend\modules\client\models\tariff\TariffForm;
use yii\helpers\Html;

$readOnly = false;
$this->title = $tariff->getTitle();
?>

<?= Html::tag('div', Html::a(t('app', 'Tariff for clients'), ['list']), ['class' => 'bread']); ?>
<?= Html::tag('h1', Html::encode($this->title)); ?>
<?= $this->render('_form', compact('tariff', 'readOnly', 'cityList', 'positionList', 'groupList', 'typeList', 'autoDownTimeList', 'optionTypeList', 'companyList', 'carPositionList', 'carClassMap')); ?>

