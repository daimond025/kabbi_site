<?php


use frontend\modules\client\models\tariff\TariffForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use frontend\modules\client\assets\TariffAsset;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var TariffForm   $tariff
 * @var boolean      $readOnly
 * @var array        $cityList
 * @var array        $positionList
 * @var array        $groupList
 * @var array        $typeList
 * @var array        $autoDownTimeList
 * @var array        $optionTypeList
 * @var array        $companyList
 * @var array        $carPositionList
 * @var array        $carClassMap
 */

TariffAsset::register($this);

$form = ActiveForm::begin([
    'id'                     => 'tariff-form',
    'errorCssClass'          => 'input_error',
    'successCssClass'        => 'input_success',
    'enableClientValidation' => false,
    'validateOnBlur'         => false,
    'options'                => ['enctype' => 'multipart/form-data'],
]);

?>

<? if (!$tariff->isNewRecord): ?>
    <?= \frontend\widgets\file\Logo::widget([
        'form'         => $form,
        'model'        => $tariff,
        'attribute'    => 'logo',
        'min_height'   => 160,
        'min_width'    => 360,
        'ajax'         => true,
        'uploadAction' => Url::to(['/client/tariff/upload-logo/', 'id' => $tariff->tariff_id]),
        'suggest'      => $tariff->getAttributeLabel('logo'),
    ]) ?>
<? endif; ?>


<section class="row">
    <?= $form->field($tariff, 'name')->begin(); ?>
    <?= Html::tag('div', Html::activeLabel($tariff, 'name'), ['class' => 'row_label']); ?>
    <?= Html::tag('div', Html::activeTextInput($tariff, 'name', ['dir' => 'auto']), ['class' => 'row_input']); ?>
    <?= $form->field($tariff, 'name')->end(); ?>
</section>


<section class="row">
    <?= $form->field($tariff, 'city_id')->begin(); ?>
    <?= Html::tag('div', Html::activeLabel($tariff, 'city_id'), ['class' => 'row_label']); ?>
    <div class="row_input">
        <?= Html::activeDropDownList($tariff, 'city_id', $cityList, [
            'data-placeholder' => ArrayHelper::getValue($cityList, $tariff->city_id, t('app', 'Choose')),
            'class'            => 'default_select',
            'disabled'         => !$tariff->isNewRecord,
        ]); ?>
    </div>
    <?= $form->field($tariff, 'city_id')->end(); ?>
</section>


<?= \frontend\widgets\position\PositionChooser::widget([
    'form'                  => $form,
    'model'                 => $tariff,
    'carClassMap'           => $carClassMap,
    'carPositionList'       => $carPositionList,
    'positionMap'           => $positionList,
    'positionDisabled'      => !$tariff->isNewRecord,
    'carClassDisabled'      => !$tariff->isNewRecord,
    'positionClassDisabled' => !$tariff->isNewRecord,
]) ?>


<section class="row">
    <?= $form->field($tariff, 'group_id')->begin(); ?>
    <?= Html::tag('div', Html::activeLabel($tariff, 'group_id'), ['class' => 'row_label']); ?>
    <div class="row_input">
        <?= Html::activeDropDownList($tariff, 'group_id', ArrayHelper::merge(['' => t('taxi_tariff', 'No group')], $groupList), [
            'data-placeholder' => ArrayHelper::getValue($groupList, $tariff->group_id, t('taxi_tariff', 'No group')),
            'class'            => 'default_select',
            //            'disabled'         => !$tariff->isNewRecord,
        ]); ?>
    </div>
    <?= $form->field($tariff, 'group_id')->end(); ?>
</section>


<section class="row">
    <?= $form->field($tariff, 'description')->begin(); ?>
    <?= Html::tag('div', Html::activeLabel($tariff, 'description'), ['class' => 'row_label']); ?>
    <?= Html::tag('div',
        Html::activeTextarea($tariff, 'description', ['rows' => 5, 'maxlength' => 1000, 'dir' => 'auto']),
        ['class' => 'row_input']); ?>
    <?= $form->field($tariff, 'description')->end(); ?>
</section>


<section class="row">
    <?= Html::tag('div', Html::label(t('taxi_tariff', 'Where available the tariff')), ['class' => 'row_label']); ?>
    <div class="row_input">
        <?= $form->field($tariff, 'enabled_site')->begin(); ?>
        <?= Html::activeCheckbox($tariff, 'enabled_site') ?>
        <?= $form->field($tariff, 'enabled_site')->end(); ?>

        <?= $form->field($tariff, 'enabled_app')->begin(); ?>
        <?= Html::activeCheckbox($tariff, 'enabled_app') ?>
        <?= $form->field($tariff, 'enabled_app')->end(); ?>

        <?= $form->field($tariff, 'enabled_operator')->begin(); ?>
        <?= Html::activeCheckbox($tariff, 'enabled_operator') ?>
        <?= $form->field($tariff, 'enabled_operator')->end(); ?>

        <?= $form->field($tariff, 'enabled_bordur')->begin(); ?>
        <?= Html::activeCheckbox($tariff, 'enabled_bordur') ?>
        <?= $form->field($tariff, 'enabled_bordur')->end(); ?>

        <?= $form->field($tariff, 'enabled_cabinet')->begin(); ?>
        <?= Html::activeCheckbox($tariff, 'enabled_cabinet') ?>
        <?= $form->field($tariff, 'enabled_cabinet')->end(); ?>

        <?= $form->field($tariff, 'enabled_hospital')->begin(); ?>
        <?= Html::activeCheckbox($tariff, 'enabled_hospital') ?>
        <?= $form->field($tariff, 'enabled_hospital')->end(); ?>
    </div>
</section>


<section class="row">
    <?= $form->field($tariff, 'sort')->begin(); ?>
    <?= Html::tag('div', Html::activeLabel($tariff, 'sort'), ['class' => 'row_label']); ?>
    <?= Html::tag('div', Html::activeTextInput($tariff, 'sort', ['dir' => 'auto']), ['class' => 'row_input']); ?>
    <?= $form->field($tariff, 'sort')->end(); ?>
</section>


<section class="row">
    <?= $form->field($tariff, 'type')->begin(); ?>
    <?= Html::tag('div', Html::activeLabel($tariff, 'type'), ['class' => 'row_label']); ?>
    <div class="row_input">
        <?= Html::activeDropDownList($tariff, 'type', $typeList, [
            'data-placeholder' => ArrayHelper::getValue($typeList, $tariff->type, t('app', 'Choose')),
            'class'            => 'default_select',
        ]); ?>
    </div>
    <?= $form->field($tariff, 'type')->end(); ?>
</section>


<section class="row js-row-company" <?= $tariff->isCompanyRequired ? '' : 'style="display: none'; ?> ">
<?= $form->field($tariff, 'company_ids')->begin(); ?>
<div class="row_label"><?= Html::activeLabel($tariff, 'company_ids') ?></div>
<div class="row_input">
    <?= Html::activeCheckboxList($tariff, 'company_ids', $companyList, [
        'class' => 'select_checkbox',
        'item'  => function ($index, $label, $name, $checked, $value) use ($tariff, $companyList) {
            $input     = Html::checkbox($name, $checked, ['value' => $value]);
            $label     = Html::label($input . Html::encode($label));
            $innerHtml = Html::tag('div', $label);

            return $innerHtml;
        },
    ]) ?>
    <?= Html::error($tariff, 'company_ids',
        ['tag' => 'span', 'class' => 'help-block help-block-error', 'style' => 'color:red']) ?>
</div>
<?= $form->field($tariff, 'company_ids')->end(); ?>
</section>


<section class="row">
    <?= $form->field($tariff, 'auto_downtime')->begin(); ?>
    <?= Html::tag('div', Html::activeLabel($tariff, 'auto_downtime'), ['class' => 'row_label']); ?>
    <?= Html::tag('div', Html::activeRadioList($tariff, 'auto_downtime', $autoDownTimeList),
        ['class' => 'row_input']); ?>
    <?= $form->field($tariff, 'auto_downtime')->end(); ?>
</section>

<? if (!$tariff->isNewRecord): ?>
<?= $this->render('_optionList', compact('tariff', 'readOnly', 'optionTypeList')); ?>
<? endif; ?>


<section class="submit_form">
    <? if (!$tariff->isNewRecord): ?>
        <div>
            <label><?= Html::activeCheckbox($tariff, 'block', ['label' => null]) ?> <b><?= t('app',
                        'Block') ?></b></label>
        </div>
    <? endif ?>
    <?= Html::submitInput($tariff->isNewRecord ? t('taxi_tariff', 'Continue') : t('taxi_tariff', 'Save')) ?>
</section>


<? ActiveForm::end(); ?>
