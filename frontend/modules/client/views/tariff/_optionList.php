<?php

use frontend\modules\client\models\tariff\TariffForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var TariffForm   $tariff
 * @var boolean      $readOnly
 * @var array        $optionTypeList
 */
Pjax::begin([
        'id' => 'pjax-option-list'
]);
?>

<section class="row">
    <?= Html::tag('h4', t('taxi_tariff', 'Main tariff')); ?>
    <table class="styled_table" style="margin-bottom: 30px;">
        <tr>
            <th style="width: 40%"><?= t('taxi_tariff', 'Working hours') ?></th>
            <th style="width: 15%"><?= t('taxi_tariff', 'Accrual (City)') ?></th>
            <th style="width: 15%"><?= t('taxi_tariff', 'Accrual (Track)') ?></th>
            <th style="width: 10%"><?= t('taxi_tariff', 'Is airport') ?></th>
            <th style="width: 10%"><?= t('taxi_tariff', 'Is station') ?></th>
            <th style="10%"></th>
        </tr>



        <? foreach ($optionTypeList as $option): ?>
            <? if ($option['type'] === 'CURRENT'): ?>
                <tr>
                    <?= Html::tag('td', t('taxi_tariff', 'All the time except exceptions')); ?>
                    <?= Html::tag('td', ArrayHelper::getValue($option, 'cityTariff')); ?>
                    <?= Html::tag('td', ArrayHelper::getValue($option, 'trackTariff')); ?>
                    <?= Html::tag('td',
                        ArrayHelper::getValue($option, 'isAirport', false) ? t('app', 'Yes') : t('app', 'No')); ?>
                    <?= Html::tag('td',
                        ArrayHelper::getValue($option, 'isStation', false) ? t('app', 'Yes') : t('app', 'No')); ?>
                    <td style="text-align: right">
                        <a class="ot_edit js-edit-ex" href="<?= Url::to([
                            'update-option',
                            'typeId' => $option['typeId'],
                        ]) ?>">
                        </a>
                    </td>
                </tr>

                <? break; ?>
            <? endif; ?>
        <? endforeach; ?>

    </table>
</section>


<section class="row">
    <?= Html::tag('h4', t('taxi_tariff', 'Other tariff')); ?>
    <table class="styled_table" style="margin-bottom: 30px;">
        <tr>
            <th style="width: 30%"><?= t('taxi_tariff', 'Working hours') ?></th>
            <th style="width: 10%"><?= t('taxi_tariff', 'Sort') ?></th>
            <th style="width: 15%"><?= t('taxi_tariff', 'Accrual (City)') ?></th>
            <th style="width: 15%"><?= t('taxi_tariff', 'Accrual (Track)') ?></th>
            <th style="width: 10%"><?= t('taxi_tariff', 'Is airport') ?></th>
            <th style="width: 10%"><?= t('taxi_tariff', 'Is station') ?></th>
            <th style="10%"></th>
        </tr>
        <? foreach ($optionTypeList as $option): ?>
            <? if ($option['type'] !== 'CURRENT'): ?>
                <tr>
                    <?= Html::tag('td', implode('<br>', $option['activeDate'])); ?>
                    <?= Html::tag('td', $option['sort']); ?>
                    <?= Html::tag('td', ArrayHelper::getValue($option, 'cityTariff')); ?>
                    <?= Html::tag('td', ArrayHelper::getValue($option, 'trackTariff')); ?>
                    <?= Html::tag('td',
                        ArrayHelper::getValue($option, 'isAirport', false) ? t('app', 'Yes') : t('app', 'No')); ?>
                    <?= Html::tag('td',
                        ArrayHelper::getValue($option, 'isStation', false) ? t('app', 'Yes') : t('app', 'No')); ?>
                    <td style="text-align: right">
                        <a class="ot_edit js-edit-ex" href="<?= Url::to([
                            'update-option',
                            'typeId' => $option['typeId'],
                        ]) ?>">
                        </a>
                    </td>
                </tr>
            <? endif; ?>
        <? endforeach; ?>

    </table>
</section>

<? Pjax::end(); ?>

<? if (!$readOnly): ?>
    <?= Html::a(t('taxi_tariff', 'Add an exception'), ['create-option', 'id' => $tariff->tariff_id],
        ['style' => 'margin-bottom: 30px;', 'class' => 'button js-edit-ex']); ?>
<? endif; ?>

<?= Html::a('', null, ['class' => 'js-modal-ex-hidden', 'style' => 'display: none']); ?>

