<?php
/**
 * @var yii\web\View $this
 * @var \frontend\modules\client\models\tariff\BaseTariffOptionAirportOrStationForm $model
 * @var string $currency
 * @var array $parkingList
 */


use yii\helpers\Html;
use frontend\modules\client\models\tariff\TariffOption;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use app\modules\parking\models\Parking;

//Pjax::begin(['enablePushState' => false, 'id' => 'pjax-' . $model::getAreaType()]);
$form = ActiveForm::begin([
    'id' => 'airport-or-station-form-' . rand(1, 999),
    'errorCssClass' => 'input_error',
    'successCssClass' => 'input_success',
    'enableClientValidation' => true,
    'validateOnBlur' => false,
    'options' => [
        'class' => 'js-client-options-form',
        'enctype' => 'multipart/form-data',
        //'data-pjax' => true,
    ],
]);

?>

    <!-- Активность -->
    <div class="ap_cont ap_cont--activity">
        <div>
            <?= $form->field($model, 'isActive')->begin(); ?>
            <?= Html::activeCheckbox($model, 'isActive') ?>
            <?= $form->field($model, 'isActive')->end(); ?>
        </div>
    </div>

    <!-- Бесплатное время ожидания до начала поездки -->
    <div class="ap_cont">
        <div>
            <?= Html::activeLabel($model, 'wait_time') ?>
            <div class="city">
                <?= $form->field($model, 'wait_time')->begin(); ?>
                <?= Html::activeTextInput($model, 'wait_time') ?>
                <?= $form->field($model, 'wait_time')->end(); ?>
                <?= Html::tag('span', t('app', 'min.'), ['class' => 'currency_symbol']) ?>
            </div>
        </div>
    </div>

    <!-- Стоимость 1 минуты ожидания -->
    <div class="ap_cont">
        <div>
            <?= Html::activeLabel($model, 'wait_price') ?>
            <div class="city">
                <?= $form->field($model, 'wait_price')->begin(); ?>
                <?= Html::activeTextInput($model, 'wait_price') ?>
                <?= $form->field($model, 'wait_price')->end(); ?>
                <?= Html::tag('span', $currency, ['class' => 'currency_symbol']) ?>
            </div>
        </div>
    </div>


    <!-- Округлять до -->
    <div class="ap_cont">
        <div>
            <?= Html::activeLabel($model, 'rounding') ?>
            <div class="city">
                <?= $form->field($model, 'rounding')->begin(); ?>
                <?= Html::activeTextInput($model, 'rounding') ?>
                <?= $form->field($model, 'rounding')->end(); ?>
                <?= Html::tag('span', $currency, ['class' => 'currency_symbol']) ?>
            </div>
        </div>
    </div>

    <!-- Правило округления -->
    <div class="ap_cont">
        <div>
            <?= Html::activeLabel($model, 'rounding_type') ?>
            <div class="city">
                <div style="width: 140px; margin-right: 10px;">
                    <?= $form->field($model, 'rounding_type')->begin(); ?>
                    <?= Html::activeDropDownList($model, 'rounding_type', TariffOption::getRoundingTypeMap(), [
                        'class' => 'default_select',
                        'data-additional-classes' => 'normal-select',
                    ]) ?>
                    <?= $form->field($model, 'rounding_type')->end(); ?>
                </div>
            </div>
        </div>
    </div>


    <section class="submit_form">
        <? if (app()->request->isPjax): ?>
            <? if ($model->isSaved): ?>
                <div class="row_input message" style="color: green; text-align: left;">
                    <?= t('app', 'Data updated successfully') ?>
                </div>
            <? else: ?>
                <div class="row_input message" style="color: red; text-align: left;">
                    <?= t('app', 'Save error') ?>
                </div>
            <? endif; ?>
        <? endif; ?>

        <?= Html::submitInput(t('app', 'Save')) ?>
    </section>


    <section class="js-fix-station" style="display: <?= $model->isActive ? 'block' : 'none' ?>">
        <?= $this->render('_fix',
            ['typeId' => $model->optionModel->type_id, 'parkingList' => $parkingList]); ?>
    </section>

<?php
if (app()->request->isAjax) {
    $this->registerJs('select_init();');
}
?>
<? ActiveForm::end(); ?>
<? //Pjax::end() ?>