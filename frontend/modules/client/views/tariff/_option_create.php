<?php
/**
 * @var yii\web\View                                                      $this
 * @var \frontend\modules\client\models\tariff\TariffOptionForm           $model
 * @var \frontend\modules\client\models\tariff\TariffAdditionalOptionForm $additionalModel
 * @var \frontend\modules\client\models\tariff\TariffOptionAirportForm    $airportModel
 * @var \frontend\modules\client\models\tariff\TariffOptionStationForm    $stationModel
 * @var \frontend\modules\client\models\tariff\TariffAdditionalOptionForm $additionalModel
 * @var array                                                             $parkingList
 * @var string                                                            $currency
 * @var boolean                                                           $readOnly
 */

$viewOption = $readOnly ? '_optionForm_read' : '_optionForm';
?>

<div class="worker-tariff-exception client-tariff-exception">
    <section class="main_tabs">
        <div class="tabs_links">
            <ul>
                <li class="active"><a class="t01"><?= t('taxi_tariff', 'Params') ?></a></li>
<!--                <li><a class="t02">--><?//= t('taxi_tariff', 'Additional options') ?><!--</a>-->
<!--                <li><a class="t03">--><?//= t('taxi_tariff', 'Parking') ?><!--</a>-->
                </li>
            </ul>
        </div>
        <div class="tabs_content">
            <div id="t01" class="active" data-view="index">
                <?= $this->render('_optionForm', compact('model', 'currency', 'readOnly')); ?>
            </div>
        </div>
    </section>
</div>
