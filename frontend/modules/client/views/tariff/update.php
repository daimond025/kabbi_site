<?php
/**
 * @var yii\web\View $this
 * @var TariffForm   $tariff
 * @var boolean      $readOnly
 * @var array        $cityList
 * @var array        $positionList
 * @var array        $groupList
 * @var array        $typeList
 * @var array        $autoDownTimeList
 * @var array        $optionTypeList
 * @var array        $companyList
 * @var array        $carPositionList
 * @var array        $carClassMap
 */

use frontend\modules\client\models\tariff\TariffForm;
use yii\helpers\Html;
use frontend\modules\client\assets\TariffOptionsAsset;
use frontend\modules\client\assets\TariffAdditionalOptionsAsset;
use frontend\modules\client\assets\TariffParkingAsset;

TariffAdditionalOptionsAsset::register($this);
TariffOptionsAsset::register($this);
TariffParkingAsset::register($this);


$form = $readOnly ? '_form_read' : '_form';

$this->title = $tariff->getTitle();
?>

<?= Html::tag('div', Html::a(t('app', 'Tariff for clients'), ['list']), ['class' => 'bread']); ?>
<?= Html::tag('h1', Html::encode($this->title)); ?>
<?= $this->render($form, compact('tariff', 'readOnly', 'cityList', 'positionList', 'groupList', 'typeList', 'autoDownTimeList', 'optionTypeList', 'companyList', 'carPositionList', 'carClassMap')); ?>

