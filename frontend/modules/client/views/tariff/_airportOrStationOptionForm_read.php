<?php
/**
 * @var yii\web\View $this
 * @var \frontend\modules\client\models\tariff\BaseTariffOptionAirportOrStationForm $model
 * @var yii\widgets\ActiveForm $form
 * @var string $currency
 */


use yii\helpers\Html;
use frontend\modules\client\models\tariff\TariffOption;


?>

<!-- Активность -->
<div class="ap_cont">
    <div>
        <?= Html::activeCheckbox($model, 'isActive', ['disabled' => true]) ?>
    </div>
</div>

<!-- Бесплатное время ожидания до начала поездки -->
<div class="ap_cont">
    <div>
        <?= Html::activeLabel($model, 'wait_time') ?>
        <div class="city">
            <?= Html::tag('span', $model->wait_time . ' ' . t('app', 'min.'), ['class' => 'currency_symbol']) ?>
        </div>
    </div>
</div>

<!-- Стоимость 1 минуты ожидания -->
<div class="ap_cont">
    <div>
        <?= Html::activeLabel($model, 'wait_price') ?>
        <div class="city">
            <?= Html::tag('span', $model->wait_price . ' ' . $currency, ['class' => 'currency_symbol']) ?>
        </div>
    </div>
</div>


<!-- Округлять до -->
<div class="ap_cont">
    <div>
        <?= Html::activeLabel($model, 'rounding') ?>
        <div class="city">
            <?= Html::tag('span', $model->rounding . ' ' . $currency, ['class' => 'currency_symbol']) ?>
        </div>
    </div>
</div>

<!-- Правило округления -->
<div class="ap_cont">
    <div>
        <?= Html::activeLabel($model, 'rounding_type') ?>
        <div class="city">
            <div style="width: 140px; margin-right: 10px;">
                <?= Html::activeDropDownList($model, 'rounding_type', TariffOption::getRoundingTypeMap(), [
                    'class' => 'default_select',
                    'data-additional-classes' => 'normal-select',
                    'disabled' => true,
                ]) ?>
            </div>
        </div>
    </div>
</div>


