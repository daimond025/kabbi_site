<?php
/**
 * @var yii\web\View                                                   $this
 * @var \frontend\modules\client\models\tariff\TariffOptionAirportForm $airportModel
 * @var \frontend\modules\client\models\tariff\TariffOptionStationForm $stationModel
 * @var array                                                          $parkingList
 * @var string                                                         $currency
 * @var boolean                                                        $readOnly
 */

use yii\helpers\Html;
use app\modules\parking\models\Parking;

$view = $readOnly ? '_airportOrStationOptionForm_read' : '_airportOrStationOptionForm';

?>

<?php if (is_array($parkingList[Parking::TYPE_AIRPORT])) : ?>
    <h2 class="check_spoler">
        <?= Html::a(t('taxi_tariff', 'Airport fix tariffs'), null, [
            'class' => 'spoiler',
            'data'  => [
                'spoilerId' => 'airport',
            ],
        ]) ?>
    </h2>
    <section class="spoiler_content" id="airport" style="display: none">
        <?= $this->render($view, [
            'model'       => $airportModel,
            'currency'    => $currency,
            'parkingList' => $parkingList[Parking::TYPE_AIRPORT],
        ]) ?>
    </section>
<? endif; ?>


<?php if (is_array($parkingList[Parking::TYPE_STATION])) : ?>
<h2 class="check_spoler">
    <?= Html::a(t('taxi_tariff', 'Railway fix tariffs'), null, [
        'class' => 'spoiler',
        'data'  => [
            'spoilerId' => 'station',
        ],
    ]) ?>
</h2>
<section class="spoiler_content" id="station" style="display: none">
    <?= $this->render($view,
        ['model' => $stationModel, 'currency' => $currency, 'parkingList' => $parkingList[Parking::TYPE_STATION]]) ?>
</section>
<? endif; ?>


<?php if (is_array($parkingList[Parking::TYPE_CITY])) : ?>
<h2 class="check_spoler">
    <?= Html::a(t('taxi_tariff', 'City'), null, [
        'class' => 'spoiler',
        'data'  => [
            'spoilerId' => 'city',
        ],
    ]) ?>
</h2>
<section class="spoiler_content" id="city" style="display: none">
    <?= $this->render('_fix',
        ['typeId' => $airportModel->optionModel->type_id, 'parkingList' => $parkingList[Parking::TYPE_CITY]]); ?>
</section>
<? endif; ?>


<?php if (is_array($parkingList[Parking::TYPE_OUTCITY])) : ?>
<h2 class="check_spoler">
    <?= Html::a(t('taxi_tariff', 'Track'), null, [
        'class' => 'spoiler',
        'data'  => [
            'spoilerId' => 'track',
        ],
    ]) ?>
</h2>
<section class="spoiler_content" id="track" style="display: none">
    <?= $this->render('_fix',
        ['typeId' => $airportModel->optionModel->type_id, 'parkingList' => $parkingList[Parking::TYPE_OUTCITY]]); ?>
</section>
<? endif; ?>