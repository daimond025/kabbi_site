<?php

/**
 * @var yii\web\View                                                      $this
 * @var \frontend\modules\client\models\tariff\TariffAdditionalOptionForm $additionalModel
 * @var string                                                            $currency
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

//Pjax::begin(['id' => 'pjax-additional-form', 'enablePushState' => false]);

$form = ActiveForm::begin([
    'id'                     => 'tariff-additional-form',
    'errorCssClass'          => 'input_error',
    'successCssClass'        => 'input_success',
    'enableClientValidation' => true,
    'validateOnBlur'         => false,
    'options'                => [
        'class'     => 'js-client-options-form',
        'enctype'   => 'multipart/form-data',
        //'data-pjax' => true,
    ],
]);

?>

<? if (app()->request->isAjax): ?>

    <section class="row js-additional-option-info">
        <div class="row_label"></div>
        <? if ($additionalModel->isSaved): ?>
            <div class="row_input message" style="color: green; text-align: left;">
                <?= t('app', 'Data updated successfully') ?>
            </div>
        <? elseif ($additionalModel->isSaved === false): ?>
            <div class="row_input message" style="color: red; text-align: left;">
                <?= t('app', 'Save error') ?>
            </div>
        <? endif; ?>
    </section>

<? endif; ?>

<div class="ap_cont">

    <? foreach ($additionalModel->name as $optionId => $name) : ?>

        <div class="check_option">
            <?= Html::activeCheckbox($additionalModel, "active[{$optionId}]", [
                'label' => $name,
            ]); ?>
            <?= Html::activeTextInput($additionalModel, "price[{$optionId}]", [
                    'disabled' => !$additionalModel->active[$optionId]
            ]); ?>
            <?= Html::tag('span', $currency, ['class' => 'currency_symbol']) ?>
        </div>

    <? endforeach; ?>

</div>

<section class="submit_form">
    <?= Html::submitInput(t('app', 'Save')) ?>
</section>

<? ActiveForm::end(); ?>
<? //Pjax::end() ?>



