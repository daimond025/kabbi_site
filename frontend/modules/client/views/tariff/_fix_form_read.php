<?php
/**
 * @var yii\web\View                                            $this
 * @var \frontend\modules\client\models\tariff\TariffHasFixForm $fixModel
 * @var string                                                  $currency
 */

use yii\helpers\Html;

?>

<section>
    <div class="ap_cont js-parking-price">
        <? foreach ($fixModel->priceList as $fixId => $item): ?>
            <? if ($item['from'] === $item['to']): ?>
                <div>
                    <?= Html::label(t('taxi_tariff', 'Within the area')); ?>

                    <? $price = $fixModel->priceList[$fixId]['from_price'] ?>
                    <?= Html::tag('span', "$price $currency", ['class' => 'currency_symbol']); ?>

                    <?= Html::tag('span', $currency); ?>
                </div>
                <? continue ?>
            <? endif; ?>

            <div>

                <? $label = $item['to_name']; ?>
                <? if ($item['from'] == $fixModel->parkingId): ?>
                    <? $fromPrice = $fixModel->priceList[$fixId]['to_price'] ?>
                    <? $toPrice = $fixModel->priceList[$fixId]['from_price'] ?>
                <? else: ?>
                    <? $fromPrice = $fixModel->priceList[$fixId]['from_price'] ?>
                    <? $toPrice = $fixModel->priceList[$fixId]['to_price'] ?>
                <? endif; ?>

                <?= Html::label($fixModel->getLabel($fixId)); ?>
                <?= Html::tag('span', "$fromPrice $currency", ['class' => 'currency_symbol']); ?>

                <?= Html::tag('span', t('taxi_tariff', 'Back')); ?>
                <?= Html::tag('span', "$toPrice $currency", ['class' => 'currency_symbol']); ?>

            </div>

        <? endforeach; ?>
    </div>
</section>
