<?php
/**
 * @var yii\web\View $this
 * @var \frontend\modules\client\models\tariff\TariffOptionForm $model
 * @var string $currency
 */


use yii\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\modules\client\models\tariff\TariffOption;
use yii\widgets\Pjax;
use frontend\widgets\validity_input\ValidityInput;

//Pjax::begin(['id' => 'pjax-option-form', 'options' => ['class' => 'client-option-form-pjax'], 'enablePushState' => false]);
$form = ActiveForm::begin([
    'id' => 'tariff-option-form',
    'errorCssClass' => 'input_error',
    'successCssClass' => 'input_success',
    'validateOnBlur' => false,
    'options' => [
        'class' => 'js-client-options-form',
        'enctype' => 'multipart/form-data',
        //'data-pjax' => true,
    ],
]);

?>


<? if (app()->request->isAjax): ?>

    <section class="row js-additional-option-info">
        <div class="row_label"></div>
        <? if ($model->isSaved): ?>
            <div class="row_input message" style="color: green; text-align: left;">
                <?= t('app', 'Data updated successfully') ?>
            </div>
        <? elseif ($model->isSaved === false): ?>
            <div class="row_input message" style="color: red; text-align: left;">
                <?= t('app', 'Save error') ?>
            </div>
        <? endif; ?>
    </section>

<? endif; ?>


<!-- Сортировка -->
<section class="row">
    <?= $form->field($model, 'sort')->begin(); ?>
    <?= Html::tag('div', Html::activeLabel($model, 'sort'), ['class' => 'row_label']); ?>
    <?= Html::tag('div', Html::activeTextInput($model, 'sort'), ['class' => 'row_input']); ?>
    <?= $form->field($model, 'sort')->end(); ?>
</section>

<? if (!$model->optionType->isCurrentType): ?>
    <section class="row">
        <?= $form->field($model, 'active_dates', ['options' => ['style' => 'position:relative;']])->begin(); ?>
        <?= Html::tag('div', Html::activeLabel($model, 'active_dates'), ['class' => 'row_label']); ?>
        <?= ValidityInput::widget([
            'id' => 'add_exclusions',
            'label' => t('taxi_tariff', 'Exclusions'),
            'data' => $model->ActiveDate,
            'input_name' => $model->formName(),
            'model' => $model,
        ]) ?>
        <?= $form->field($exclusionsCity, 'active_dates')->end(); ?>
    </section>
<? endif; ?>

<!-- Зафиксировать расчет для двух и более точек -->
<section class="row">
    <?= $form->field($model, 'calculation_fix')->begin(); ?>
    <?= Html::tag('div', '', ['class' => 'row_label']); ?>
    <?= Html::tag('div', Html::activeCheckbox($model, 'calculation_fix'),
        ['class' => 'row_input']); ?>
    <?= $form->field($model, 'calculation_fix')->end(); ?>
</section>

<!-- Учитывать поправочный коэффициент районов -->
<section class="row">
    <?= $form->field($model, 'enabled_parking_ratio')->begin(); ?>
    <?= Html::tag('div', '', ['class' => 'row_label']); ?>
    <?= Html::tag('div', Html::activeCheckbox($model, 'enabled_parking_ratio'),
        ['class' => 'row_input']); ?>
    <?= $form->field($model, 'enabled_parking_ratio')->end(); ?>
</section>

<!-- Тип расчета -->
<div class="ap_top">
    <div>
        <div class="ap_top_f">
            <h3><?= t('taxi_tariff', 'City'); ?></h3>
            <?= Html::activeDropDownList($model, 'city_accrual', TariffOption::getTypeMap(), [
                'class' => 'default_select',
            ]) ?>
        </div>
        <div class="ap_top_s">
            <h3><?= t('taxi_tariff', 'Track'); ?></h3>
            <?= Html::activeDropDownList($model, 'track_accrual', TariffOption::getTypeMap(), [
                'class' => 'default_select',
            ]) ?>
        </div>
    </div>
</div>

<!-- Стоимость посадки -->
<div class="ap_cont">
    <div>
        <?= Html::activeLabel($model, 'city_planting_price') ?>
        <div class="city">
            <?= $form->field($model, 'city_planting_price')->begin(); ?>
            <?= Html::tag('div', Html::activeTextInput($model, 'city_planting_price')); ?>
            <?= $form->field($model, 'city_planting_price')->end(); ?>
            <?= Html::tag('span', $currency, ['class' => 'currency_symbol']) ?>
        </div>
        <div class="track">
            <?= $form->field($model, 'track_planting_price')->begin(); ?>
            <?= Html::tag('div', Html::activeTextInput($model, 'track_planting_price')); ?>
            <?= $form->field($model, 'track_planting_price')->end(); ?>
            <?= Html::tag('span', $currency, ['class' => 'currency_symbol']) ?>
        </div>
    </div>
</div>

<!-- Включено в стоимость посадки -->
<div class="ap_cont" data-accruals="DISTANCE,TIME,MIXED,INTERVAL"
     style="display: <?= ($model->city_accrual === TariffOption::ACCRUAL_FIX && $model->track_accrual === TariffOption::ACCRUAL_FIX) ? 'none' : 'block' ?>">
    <div>
        <?= Html::activeLabel($model, 'city_planting_include') ?>
        <div class="city">
            <?= $form->field($model, 'city_planting_include')->begin(); ?>
            <?= Html::activeTextInput($model, 'city_planting_include', [
                'class' => 'js-planting-include',
                'disabled' => $model->city_accrual === TariffOption::ACCRUAL_FIX,
            ]) ?>
            <?= $form->field($model, 'city_planting_include')->end(); ?>

            <?= Html::tag('span', t('app', 'km'), [
                'class' => 'currency_symbol height-fix js-accrual-distance js-accrual-fix js-accrual-mixed js-accrual-interval',
                'style' => 'display:' . ($model->city_accrual === TariffOption::ACCRUAL_TIME ? 'none' : 'block'),
            ]) ?>

            <?= $form->field($model, 'city_planting_include_time')->begin(); ?>
            <?= Html::activeTextInput($model, 'city_planting_include_time', [
                'class' => 'js-accrual-mixed',
                'style' => 'display:' . ($model->city_accrual === TariffOption::ACCRUAL_MIXED ? 'block' : 'none') . '; margin-left: -50%',
                'disabled' => $model->city_accrual !== TariffOption::ACCRUAL_MIXED,
            ]) ?>
            <?= $form->field($model, 'city_planting_include_time')->end(); ?>

            <?= Html::tag('span', t('app', 'min.'), [
                'class' => 'currency_symbol js-accrual-mixed js-accrual-time',
                'style' => 'display:' . ($model->city_accrual === TariffOption::ACCRUAL_MIXED || $model->city_accrual === TariffOption::ACCRUAL_TIME ? 'block' : 'none'),
            ]) ?>
        </div>

        <div class="track">
            <?= $form->field($model, 'track_planting_include')->begin(); ?>
            <?= Html::activeTextInput($model, 'track_planting_include', [
                'class' => 'js-planting-include',
                'disabled' => $model->track_accrual === TariffOption::ACCRUAL_FIX,
            ]); ?>
            <?= $form->field($model, 'track_planting_include')->end(); ?>

            <?= Html::tag('span', t('app', 'km'), [
                'class' => 'currency_symbol height-fix js-accrual-distance js-accrual-fix js-accrual-mixed js-accrual-interval',
                'style' => 'display:' . ($model->track_accrual === TariffOption::ACCRUAL_TIME ? 'none' : 'block'),
            ]) ?>

            <?= $form->field($model, 'track_planting_include_time')->begin(); ?>
            <?= Html::activeTextInput($model, 'track_planting_include_time', [
                'class' => 'js-accrual-mixed',
                'style' => 'display:' . ($model->track_accrual === TariffOption::ACCRUAL_MIXED ? 'block' : 'none') . '; margin-left: -50%',
                'disabled' => $model->track_accrual !== TariffOption::ACCRUAL_MIXED,
            ]) ?>
            <?= $form->field($model, 'track_planting_include_time')->end(); ?>

            <?= Html::tag('span', t('app', 'min.'), [
                'class' => 'currency_symbol js-accrual-mixed js-accrual-time',
                'style' => 'display:' . ($model->track_accrual === TariffOption::ACCRUAL_MIXED || $model->track_accrual === TariffOption::ACCRUAL_TIME ? 'block' : 'none'),
            ]) ?>
        </div>

    </div>
</div>


<!-- Стоимость далее -->
<div class="ap_cont" data-accruals="DISTANCE,TIME,MIXED,INTERVAL">
    <div>
        <?= Html::activeLabel($model, 'city_next_km_price') ?>
        <div class="city">
            <?= $form->field($model, 'city_next_km_price')->begin(); ?>
            <?= Html::activeTextInput($model, 'city_next_km_price', [
                'class' => 'js-accrual-time js-accrual-distance js-accrual-mixed js-accrual-fix',
                'style' => 'display:' . ($model->city_accrual === TariffOption::ACCRUAL_INTERVAL ? 'none' : 'block'),
                'disabled' => $model->track_accrual === TariffOption::ACCRUAL_FIX,
            ]) ?>
            <?= $form->field($model, 'city_next_km_price')->end(); ?>
            <?= Html::tag('span', $currency . '/' . t('app', 'km'), [
                'class' => 'currency_symbol height-fix js-accrual-distance js-accrual-fix js-accrual-mixed',
                'style' => 'display:' . ($model->city_accrual === TariffOption::ACCRUAL_TIME || $model->city_accrual === TariffOption::ACCRUAL_INTERVAL ? 'none' : 'block'),
            ]) ?>

            <?= $form->field($model, 'city_next_km_price_time')->begin(); ?>
            <?= Html::activeTextInput($model, 'city_next_km_price_time', [
                'class' => 'js-accrual-mixed',
                'style' => 'display:' . ($model->city_accrual === TariffOption::ACCRUAL_MIXED ? 'block' : 'none') . '; margin-left: -50%' ,
            ]) ?>
            <?= $form->field($model, 'city_next_km_price_time')->end(); ?>
            <span style="display: <?= $model->city_accrual === TariffOption::ACCRUAL_MIXED || $model->city_accrual === TariffOption::ACCRUAL_TIME ? 'block' : 'none' ?>"
                  class="change_r_km_city select city currency_symbol js-accrual-mixed js-accrual-time">
                <?= Html::activeDropDownList($model, 'city_next_cost_unit', TariffOption::getKmPriceMap($currency),
                    ['class' => 'default_select',]); ?>
            </span>
            <div class="js-accrual-interval tar_dist_and_diap"
                 style="display: <?= $model->city_accrual === TariffOption::ACCRUAL_INTERVAL ? 'inline-block' : 'none' ?>">
                <?= $form->field($model, 'city_next_price_interval')->begin(); ?>
                <?= $this->render('_intervalForm', ['model' => $model, 'currency' => $currency, 'type' => 'city']) ?>
                <?= $form->field($model, 'city_next_price_interval')->end(); ?>
            </div>
        </div>

        <div class="track">
            <?= $form->field($model, 'track_next_km_price')->begin(); ?>
            <?= Html::activeTextInput($model, 'track_next_km_price', [
                'class' => 'js-accrual-time js-accrual-distance js-accrual-mixed js-accrual-fix',
                'style' => 'display:' . ($model->track_accrual === TariffOption::ACCRUAL_INTERVAL ? 'none' : 'block'),
                'disabled' => $model->track_accrual === TariffOption::ACCRUAL_FIX,
            ]) ?>
            <?= $form->field($model, 'track_next_km_price')->end(); ?>
            <?= Html::tag('span', $currency . '/' . t('app', 'km'), [
                'class' => 'currency_symbol height-fix js-accrual-distance js-accrual-fix js-accrual-mixed',
                'style' => 'display:' . ($model->track_accrual === TariffOption::ACCRUAL_TIME || $model->track_accrual === TariffOption::ACCRUAL_INTERVAL ? 'none' : 'block'),
            ]) ?>

            <?= $form->field($model, 'track_next_km_price_time')->begin(); ?>
            <?= Html::activeTextInput($model, 'track_next_km_price_time', [
                'class' => 'js-accrual-mixed',
                'style' => 'display:' . ($model->track_accrual === TariffOption::ACCRUAL_MIXED ? 'block' : 'none'). '; margin-left: -50%',
            ]) ?>
            <?= $form->field($model, 'track_next_km_price_time')->end(); ?>

            <span style="display: <?= $model->track_accrual === TariffOption::ACCRUAL_MIXED || $model->track_accrual === TariffOption::ACCRUAL_TIME ? 'block' : 'none' ?>"
                  class="change_r_km_track select track currency_symbol js-accrual-mixed js-accrual-time">

                <?= Html::activeDropDownList($model, 'track_next_cost_unit', TariffOption::getKmPriceMap($currency),
                    ['class' => 'default_select']); ?>

            </span>

            <div class="js-accrual-interval tar_dist_and_diap"
                 style="display: <?= $model->track_accrual === TariffOption::ACCRUAL_INTERVAL ? 'inline-block' : 'none' ?>">
                <?= $form->field($model, 'track_next_price_interval')->begin(); ?>
                <?= $this->render('_intervalForm', ['model' => $model, 'currency' => $currency, 'type' => 'track']) ?>
                <?= $form->field($model, 'track_next_price_interval')->end(); ?>
            </div>

        </div>

    </div>
</div>


<!-- Минимальная стоимость поездки -->
<div class="ap_cont" data-accruals="DISTANCE,TIME,MIXED,INTERVAL">
    <div>
        <?= Html::activeLabel($model, 'city_min_price') ?>
        <div class="city">
            <?= $form->field($model, 'city_min_price')->begin(); ?>
            <?= Html::activeTextInput($model, 'city_min_price', [
                'disabled' => $model->city_accrual === TariffOption::ACCRUAL_FIX,
            ]) ?>
            <?= $form->field($model, 'city_min_price')->end(); ?>
            <?= Html::tag('span', $currency, ['class' => 'currency_symbol']) ?>
        </div>
        <div class="track">
            <?= $form->field($model, 'track_min_price')->begin(); ?>
            <?= Html::activeTextInput($model, 'track_min_price', [
                'disabled' => $model->track_accrual === TariffOption::ACCRUAL_FIX,
            ]) ?>
            <?= $form->field($model, 'track_min_price')->end(); ?>
            <?= Html::tag('span', $currency, ['class' => 'currency_symbol']) ?>
        </div>
    </div>
</div>


<!-- Добавочная стоимость к минимальной за каждую точку, начиная с 3-ей -->
<div class="ap_cont" data-accruals="DISTANCE,TIME,MIXED,INTERVAL">
    <div>
        <?= Html::activeLabel($model, 'city_second_min_price') ?>
        <div class="city">
            <?= $form->field($model, 'city_second_min_price')->begin(); ?>
            <?= Html::activeTextInput($model, 'city_second_min_price', [
                'disabled' => $model->city_accrual === TariffOption::ACCRUAL_FIX,
            ]) ?>
            <?= $form->field($model, 'city_second_min_price')->end(); ?>
            <?= Html::tag('span', $currency, ['class' => 'currency_symbol']) ?>
        </div>
        <div class="track">
            <?= $form->field($model, 'track_second_min_price')->begin(); ?>
            <?= Html::activeTextInput($model, 'track_second_min_price', [
                'disabled' => $model->track_accrual === TariffOption::ACCRUAL_FIX,
            ]) ?>
            <?= $form->field($model, 'track_second_min_price')->end(); ?>
            <?= Html::tag('span', $currency, ['class' => 'currency_symbol']) ?>
        </div>
    </div>
</div>


<!-- Стоимость 1 км до адреса подачи -->
<div class="ap_cont" data-accruals="DISTANCE,TIME,MIXED,INTERVAL">
    <div>
        <?= Html::activeLabel($model, 'city_supply_price') ?>
        <div class="city">
        </div>
        <div class="track">
            <?= $form->field($model, 'track_supply_price')->begin(); ?>
            <?= Html::activeTextInput($model, 'track_supply_price', [
                'disabled' => $model->track_accrual === TariffOption::ACCRUAL_FIX,
            ]) ?>
            <?= $form->field($model, 'track_supply_price')->end(); ?>
            <?= Html::tag('span', $currency, ['class' => 'currency_symbol']) ?>
        </div>
    </div>
</div>


<!-- Бесплатное время ожидания до начала поездки -->
<div class="ap_cont">
    <div>
        <?= Html::activeLabel($model, 'city_wait_time') ?>
        <div class="city">
            <?= $form->field($model, 'city_wait_time')->begin(); ?>
            <?= Html::activeTextInput($model, 'city_wait_time') ?>
            <?= $form->field($model, 'city_wait_time')->end(); ?>
            <?= Html::tag('span', t('app', 'min.'), ['class' => 'currency_symbol']) ?>
        </div>
        <div class="track">
            <?= $form->field($model, 'track_wait_time')->begin(); ?>
            <?= Html::activeTextInput($model, 'track_wait_time') ?>
            <?= $form->field($model, 'track_wait_time')->end(); ?>
            <?= Html::tag('span', t('app', 'min.'), ['class' => 'currency_symbol']) ?>
        </div>
    </div>
</div>


<!-- Бесплатное время ожидания во время поездки за 1 раз -->
<div class="ap_cont" data-accruals="DISTANCE,TIME,MIXED,INTERVAL">
    <div>
        <?= Html::activeLabel($model, 'city_wait_driving_time') ?>
        <div class="city">
            <?= $form->field($model, 'city_wait_driving_time')->begin(); ?>
            <?= Html::activeTextInput($model, 'city_wait_driving_time', [
                'disabled' => $model->city_accrual === TariffOption::ACCRUAL_FIX,
            ]) ?>
            <?= $form->field($model, 'city_wait_driving_time')->end(); ?>
            <?= Html::tag('span', t('app', 'sec.'), ['class' => 'currency_symbol']) ?>
        </div>
        <div class="track">
            <?= $form->field($model, 'track_wait_driving_time')->begin(); ?>
            <?= Html::activeTextInput($model, 'track_wait_driving_time', [
                'disabled' => $model->track_accrual === TariffOption::ACCRUAL_FIX,
            ]) ?>
            <?= $form->field($model, 'track_wait_driving_time')->end(); ?>
            <?= Html::tag('span', t('app', 'sec.'), ['class' => 'currency_symbol']) ?>
        </div>
    </div>
</div>

<!-- Стоимость 1 минуты ожидания -->
<div class="ap_cont">
    <div>
        <?= Html::activeLabel($model, 'city_wait_price') ?>
        <div class="city">
            <?= $form->field($model, 'city_wait_price')->begin(); ?>
            <?= Html::activeTextInput($model, 'city_wait_price') ?>
            <?= $form->field($model, 'city_wait_price')->end(); ?>
            <?= Html::tag('span', $currency, ['class' => 'currency_symbol']) ?>
        </div>
        <div class="track">
            <?= $form->field($model, 'track_wait_price')->begin(); ?>
            <?= Html::activeTextInput($model, 'track_wait_price') ?>
            <?= $form->field($model, 'track_wait_price')->end(); ?>
            <?= Html::tag('span', $currency, ['class' => 'currency_symbol']) ?>
        </div>
    </div>
</div>

<!-- Скорость при которой вкл. ожидание -->
<div class="ap_cont" data-accruals="DISTANCE,TIME,MIXED,INTERVAL"
     style="display: <?= ($model->city_accrual === TariffOption::ACCRUAL_FIX && $model->track_accrual === TariffOption::ACCRUAL_FIX) ? 'none' : 'block' ?>">
    <div>
        <?= Html::activeLabel($model, 'city_speed_downtime') ?>
        <div class="city">
            <?= $form->field($model, 'city_speed_downtime')->begin(); ?>
            <?= Html::activeTextInput($model, 'city_speed_downtime', [
                'disabled' => $model->city_accrual === TariffOption::ACCRUAL_FIX,
            ]) ?>
            <?= $form->field($model, 'city_speed_downtime')->end(); ?>
            <?= Html::tag('span', t('app', 'km/h'), ['class' => 'currency_symbol']) ?>
        </div>
        <div class="track">
            <?= $form->field($model, 'track_speed_downtime')->begin(); ?>
            <?= Html::activeTextInput($model, 'track_speed_downtime', [
                'disabled' => $model->track_accrual === TariffOption::ACCRUAL_FIX,
            ]) ?>
            <?= $form->field($model, 'track_speed_downtime')->end(); ?>
            <?= Html::tag('span', t('app', 'km/h'), ['class' => 'currency_symbol']) ?>
        </div>
    </div>
</div>

<!-- Округлять до -->
<div class="ap_cont">
    <div>
        <?= Html::activeLabel($model, 'city_rounding') ?>
        <div class="city">
            <?= $form->field($model, 'city_rounding')->begin(); ?>
            <?= Html::activeTextInput($model, 'city_rounding') ?>
            <?= $form->field($model, 'city_rounding')->end(); ?>
            <?= Html::tag('span', $currency, ['class' => 'currency_symbol']) ?>
        </div>
        <div class="track">
            <?= $form->field($model, 'track_rounding')->begin(); ?>
            <?= Html::activeTextInput($model, 'track_rounding') ?>
            <?= $form->field($model, 'track_rounding')->end(); ?>
            <?= Html::tag('span', $currency, ['class' => 'currency_symbol']) ?>
        </div>
    </div>
</div>

<!-- Правило округления -->
<div class="ap_cont">
    <div>
        <?= Html::activeLabel($model, 'city_rounding_type') ?>
        <div class="city">
            <div style="width: 140px; margin-right: 10px;">
                <?= $form->field($model, 'city_rounding_type')->begin(); ?>
                <?= Html::activeDropDownList($model, 'city_rounding_type', TariffOption::getRoundingTypeMap(), [
                    'class' => 'default_select',
                    'data-additional-classes' => 'normal-select',
                ]) ?>
                <?= $form->field($model, 'city_rounding_type')->end(); ?>
            </div>
        </div>
        <div class="track">
            <div style="width: 140px; margin-right: 10px;">
                <?= $form->field($model, 'track_rounding_type')->begin(); ?>
                <?= Html::activeDropDownList($model, 'track_rounding_type', TariffOption::getRoundingTypeMap(), [
                    'class' => 'default_select',
                    'data-additional-classes' => 'normal-select',
                ]) ?>
                <?= $form->field($model, 'track_rounding_type')->end(); ?>
            </div>
        </div>
    </div>
</div>


<section class="submit_form">
    <? if (!$model->optionType->isCurrentType && !$model->optionType->isNewRecord): ?>
        <?= html::tag('b', Html::a(t('app', 'Delete'), ['/client/tariff/delete-option', 'id' => $model->optionType->type_id], [
            'class' => 'link_red js-option-delete',
            'data-pjax' => 0,
        ])) ?>
    <? endif; ?>
    <?= Html::submitInput(t('app', 'Save')) ?>
</section>

<?php
if (app()->request->isPjax) {
    $this->registerJs('select_init();');
}
?>

<? ActiveForm::end(); ?>
<? // Pjax::end() ?>
