<?php

namespace app\modules\address\controllers;

use app\modules\address\models\ClientAddressHistory;
use common\modules\tenant\models\DefaultSettings;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\modules\tenant\models\TenantSetting;
use yii\helpers\Json;
use yii\web\Response;

class StreetController extends Controller
{
    const ACTION_AUTO_COMPLETE = 'autocomplete';
    const ACTION_HISTORY = 'history';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Получение координа адреса по полному адресу через ajax
     * Россия, Ижевск, Советская ул, 12
     *
     * @param array $address
     *
     * @return string
     */
    public function actionGetCoordsByAddress($address)
    {
        if (Yii::$app->request->getIsAjax()) {
            $geocoder = app()->geocoder;
            $coords   = $geocoder->findCoordsByAddress($address);

            return json_encode($coords);
        }
    }

    /**
     * Getting street list data for autocomplite
     *
     * @param string $search String search
     * @param string $city_id City Id of branch
     * @param string $lat Latitude of central searching point
     * @param string $lon Longitude of central searching point
     * @param string $action Search action. Ex.: 'autocomplete', 'search'
     * @param string $client_id Client ID
     *
     * @return array|false
     */
    public function actionGetStreetList($search, $city_id, $lat, $lon, $action, $client_id = null)
    {
        if (!Yii::$app->request->isAjax) {
           // return false;
        }

        app()->response->format = Response::FORMAT_JSON;

        switch ($action) {
            case self::ACTION_HISTORY:
                return $this->getAddressInDatabase($client_id, $city_id);

            case self::ACTION_AUTO_COMPLETE:
            default:
                return $this->getAddressInAutoComplete($search, $city_id, $lat, $lon, $action);
        }

        return false;
    }

    /**
     * Preparing autocomplete data for jquery plugin
     *
     * @param array $dirtyData Autocomplete data
     *
     * @return array
     */
    private function prepareData(array $dirtyData)
    {
        $preparedData = [];

        foreach ($dirtyData as $key => $item) {
            // correct
            if(isset($item['address']['city']) && isset($item['address']['label']) ){
                $item['address']['label'] = $item['address']['city'] . ', ' . $item['address']['label'];
                $item['address']['city'] = '';
            }

            $preparedData[$key] = $item;
            if (!empty($item['address']['city'])) {
                if (isset($item['address']['label']) && !empty($item['address']['label'])) {
                    $preparedData[$key]['label'] = $item['address']['city'] . ', ' . $item['address']['label'];
                } else {
                    $preparedData[$key]['label'] = $item['address']['city'];
                }
            } else {
                $preparedData[$key]['label'] = $item['address']['label'];
            }

            $preparedData[$key]['value'] = $preparedData[$key]['label'] ;
            $preparedData[$key]['address']['label'] = $preparedData[$key]['label'];
            $preparedData[$key]['address']['placeId'] = isset($item['address']['placeId']) ? $item['address']['placeId'] : '';
        }
        return $preparedData;
    }

    /**
     * Получить адреса из автокомплита
     *
     * @param $search
     * @param $city_id
     * @param $lat
     * @param $lon
     * @param $action
     *
     * @return array
     */
    public function getAddressInAutoComplete($search, $city_id, $lat, $lon, $action)
    {
        $autoComplete         = app()->autocomplete;
        $autoComplete->action = $action;


        $lang = TenantSetting::getSettingValue(user()->tenant_id, DefaultSettings::SETTING_LANGUAGE, $city_id);

        $result       = Json::decode($autoComplete->getStreetList($search, $city_id, $lat, $lon, $lang));
        $preparedData = $this->prepareData(getValue($result['results'], []));

        return $preparedData;
    }

    /**
     * Получить адреса из БД
     *
     * @param $client_id
     * @param $city_id
     *
     * @return array
     */
    public function getAddressInDatabase($client_id, $city_id)
    {
        $models = $this->findClientAddressHistoryModel($client_id, $city_id);

        if (!$models) {
            return [];
        }

        $result = [];

        foreach ($models as $model) {
            /** @var $model ClientAddressHistory */
            $result[] = [
                'address'  => [
                    'city'    => $model->city,
                    'comment' => '',
                    'house'   => $model->house,
                    'housing' => '',
                    'label'   => $model->street,
                    'lat'     => $model->lat,
                    'lon'     => $model->lon,
                    'porch'   => '',
                    'street'  => '',
                    'placeId'  => isset($model->placeId) ? $model->placeId : ''
                ],
                'distance' => 0,
                'type'     => '',
                'label'    => $model->getLabel(),
                'value'    => $model->getLabel(),
            ];
        }

        return $result;
    }

    public function findClientAddressHistoryModel($clientId, $cityId)
    {
        return ClientAddressHistory::findLast($clientId, $cityId);
    }

}