<?php

namespace frontend\modules\referral\models;

use app\modules\client\models\Client;


/**
 * This is the model class for table "{{%referral_has_client}}".
 *
 * @property integer $referral_id
 * @property integer $inviting_client_id
 * @property integer $invited_client_id
 *
 * @property Client  $invitingClient
 * @property Client  $invitedClient
 * @property string  $referralCode
 *
 */
class ReferralHasClient extends \yii\db\ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%referral_has_client}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['referral_id', 'inviting_client_id', 'invited_client_id'], 'required'],
            ['referral_id', 'exist', 'targetClass' => Referral::className()],
            [
                ['inviting_client_id', 'invited_client_id'],
                'exist',
                'targetClass'     => Client::className(),
                'targetAttribute' => 'client_id',
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReferral()
    {
        return $this->hasOne(Referral::className(), ['referral_id' => 'referral_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvitingClient()
    {
        return $this->hasOne(Client::className(), ['inviting_client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvitedClient()
    {
        return $this->hasOne(Client::className(), ['invited_client_id' => 'client_id']);
    }

    public function getReferralCode()
    {
        $string = implode('_', [
            $this->referral_id,
            $this->inviting_client_id,
            $this->referral->rand_string
        ]);

        return (string)hash('adler32', $string);
    }
}
