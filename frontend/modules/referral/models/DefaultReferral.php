<?php

namespace frontend\modules\referral\models;

use Yii;

/**
 * This is the model class for table "tbl_default_referral".
 *
 * @property integer $id
 * @property string $text_in_client_api
 * @property string $title
 * @property string $text
 */
class DefaultReferral extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_default_referral';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text_in_client_api', 'title', 'text'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text_in_client_api' => 'Text In Client Api',
            'title' => 'Title',
            'text' => 'Text',
        ];
    }
}
