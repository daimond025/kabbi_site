<?php

namespace frontend\modules\referral\models;

use common\modules\city\models\City;
use common\modules\tenant\models\Tenant;

/**
 * This is the model class for table "{{%referral}}".
 * @property integer $referral_id
 * @property integer $tenant_id
 * @property integer $active
 * @property integer $sort
 * @property string  $name
 * @property integer $city_id
 * @property string  $type
 * @property integer $is_active_subscriber_limit
 * @property integer $subscriber_limit
 * @property float   $referral_bonus
 * @property string  $referral_bonus_type
 * @property float   $referrer_bonus
 * @property string  $referrer_bonus_type
 * @property integer $order_count
 * @property float   $min_price
 * @property string  $text_in_client_api
 * @property string  $title
 * @property string  $text
 * @property string  $rand_string
 *
 * @property integer $block
 * @property boolean $isActive
 * @property Tenant  $tenant
 * @property City    $city
 */
class Referral extends \yii\db\ActiveRecord
{
    const ON_ORDER = 'ON_ORDER';
    const ON_REGISTER = 'ON_REGISTER';

    const PERCENT = 'PERCENT';
    const BONUS = 'BONUS';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%referral}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'tenant_id',
                    'active',
                    'name',
                    'city_id',
                    'is_active_subscriber_limit',
                    'type',
                    'referral_bonus',
                    'referral_bonus_type',
                    'referrer_bonus',
                    'referrer_bonus_type',
                    'text_in_client_api',
                    'title',
                    'text',

                ],
                'required',
            ],
            [['name', 'title'], 'string', 'max' => 255],
            [['text_in_client_api', 'text'], 'string'],
            [['active', 'is_active_subscriber_limit'], 'in', 'range' => array_keys(self::getOrderCountList())],
            ['type', 'in', 'range' => array_keys(self::getTypeList())],
            [['subscriber_limit', 'order_count'], 'integer'],
            ['sort', 'integer', 'max' => 99999],
            [['referral_bonus', 'referrer_bonus', 'min_price'], 'double'],
            [['referral_bonus_type', 'referrer_bonus_type'], 'in', 'range' => array_keys(self::getBonusTypeList())],
            ['tenant_id', 'exist', 'targetClass' => Tenant::className()],
            ['city_id', 'exist', 'targetClass' => City::className()],
            ['block', 'boolean'],
            [
                'subscriber_limit',
                'required',
                'when'       => function ($model) {
                    return (bool)$model->is_active_subscriber_limit;
                },
                'whenClient' => 'function(attr,value){
                    return $(".js-is-limit").prop("checked");
                }',
            ],
            [
                ['referral_bonus_type', 'referrer_bonus_type', 'min_price'],
                'required',
                'when'       => function ($model) {
                    return $model->type === self::ON_ORDER;
                },
                'whenClient' => 'function(attr,value){
                    return $(".js-is-order").prop("checked");
                }',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => t('referral', 'Name'),
            'city_id' => t('referral', 'City'),
            'sort' => t('referral', 'Sort'),
            'subscriber_limit' => t('referral', 'Subscriber Limit'),
            'type' => t('referral', 'Referral system type'),
            'referrer_bonus' => t('referral', 'Referrer'),
            'referral_bonus' => t('referral', 'Referral'),
            'min_price' => t('referral', 'At a cost of travel from'),

            'text_in_client_api' => t('referral', 'Text in client api'),
            'title' => t('referral', 'Title'),
            'text' => t('referral', 'Text'),
        ];
    }

    /**
     * @return bool
     */
    public function getIsActive()
    {
        return (bool)$this->active == '1';
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    public function getBlock()
    {
        return (int)!$this->isActive;
    }

    public function setBlock($value)
    {
        $this->active = (int)!$value;
    }

    /**
     * Массив доступных типов реферальной системы
     *
     * @return array
     */
    public static function getTypeList()
    {
        return [
            self::ON_ORDER    => t('referral', self::ON_ORDER),
            self::ON_REGISTER => t('referral', self::ON_REGISTER),
        ];
    }


    /**
     * Массив доступных единиц измерения значений
     *
     * @return array
     */
    public static function getBonusTypeList()
    {
        return [
            self::PERCENT => '%',
            self::BONUS   => t('referral', 'Bonus'),
        ];
    }

    /**
     * Массив доступных значений количества поездок
     * @return array
     */
    public static function getOrderCountList()
    {
        return [
            0 => 'Каждый раз',
            1 => '1 раз',
        ];
    }


    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if (empty($this->order_count)) {
                $this->order_count = null;
            }

            // Блокируем другие реферальные ссылки в данном филиале
            if ($this->isActive) {
                $models = self::find()
                    ->where([
                        'active'    => 1,
                        'tenant_id' => $this->tenant_id,
                        'city_id'   => $this->city_id,
                    ])
                    ->andFilterWhere(['!=', 'referral_id', $this->referral_id])
                    ->all();

                if ($models) {
                    foreach ($models as $model) {
                        /** @var $model self */
                        $model->active = 0;
                        $model->save();
                    }
                }
            }

            if ($this->type === self::ON_REGISTER) {
                $this->referral_bonus_type = self::BONUS;
                $this->referrer_bonus_type = self::BONUS;
            }

            if (!$this->is_active_subscriber_limit) {
                $this->subscriber_limit = null;
            }

            return true;
        }

        return false;
    }
}
