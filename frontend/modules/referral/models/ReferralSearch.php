<?php

namespace frontend\modules\referral\models;


use yii\data\ArrayDataProvider;

class ReferralSearch extends Referral
{
    public $formName;
    public $cityList;
    public $isActive;
    private $_accessCityList = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['isActive', 'in', 'range' => [0, 1]],
            ['cityList', 'in', 'range' => $this->getAccessCityList(), 'allowArray' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cityList' => t('referral', 'All cities')
        ];
    }

    /**
     * @param $params
     *
     * @return ArrayDataProvider
     */
    public function search($params)
    {
        $query        = self::find()
            ->where([
                'tenant_id' => user()->tenant_id,
                'city_id'   => $this->_accessCityList,
                'active'    => (int)$this->isActive,
            ]);
        $dataProvider = new ArrayDataProvider([
            'allModels'  => $query->asArray()->all(),
            'sort'       => [
                'defaultOrder' => [
                    'sort' => SORT_DESC,
                ],
                'attributes'   => ['sort', 'type'],
            ],
            'pagination' => false,
        ]);


        $this->load($params, $this->formName);

        if (!$this->validate()) {
            $dataProvider->allModels = $query->asArray()->all();

            return $dataProvider;
        }

        $query->andWhere([
            'city_id' => $this->getSearchCityList(),
        ]);

        $dataProvider->allModels = $query->asArray()->all();

        return $dataProvider;
    }


    public function getAccessCityList()
    {
        return $this->_accessCityList;
    }


    public function setAccessCityList($value)
    {
        $this->_accessCityList = $value;
    }


    private function getSearchCityList()
    {
        return $this->cityList ? $this->cityList : $this->getAccessCityList();
    }
}
