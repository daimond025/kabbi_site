<?php
/* @var $isClient bool Если авторизован как клиент */
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use app\modules\balance\models\Operation;
use frontend\modules\tenant\models\Currency;
use app\modules\balance\models\Account;
use app\modules\reports\helpers\TransactionReportHelper;

$currencySymbol = Currency::getCurrencySymbol($currency_id);
$minorUnit = Currency::getMinorUnit($currency_id);

if (in_array($kind_id, [Account::CLIENT_BONUS_KIND, Account::CLIENT_BONUS_UDS_GAME_KIND])) {
    $currencySymbol = t('currency', 'B') . ' (' . $currencySymbol . ')';
}

$formatter = app()->formatter;

if (!empty($operations)):
    ?>
    <tr class="mt_spec">
        <td colspan="2">
            <?
            $date = strtotime($operations[0]->date) + $time_offset;
            echo $formatter->asDate($date, 'shortDate');
            ?>
        </td>
        <td class="mt_itog"><b><?= t('balance', 'Beginning balance') ?></b><a data-tooltip="4" class="tooltip">?<span style="display: none; opacity: 0;"><?= HtmlPurifier::process(Yii::t('hint','Difference between debt and credit')) ?></span></a></td>
        <td class="td_text_center" style="text-align: center;" colspan="2">
            <b>
                <?= app()->formatter->asMoney(round(+$operations[0]->getOpeningBalance(), $minorUnit), $currencySymbol); ?>
            </b>
        </td>
    </tr>
    <?
    $debit = $credit = 0;
    foreach ($operations as $operation):
        ?>
        <tr>
            <td>
                <span style="font-size: 14px">
                    <?
                    $date = strtotime($operation->date) + $time_offset;
                    echo $formatter->asDate($date, 'shortDate')
                        . ", " . $formatter->asTime($date, 'short');
                    ?>
                </span>
            </td>
            <td>
                <? if (!empty($operation->transaction->userCreated)): ?>
            <?= Html::encode(t('company-roles', $operation->transaction->userCreated->getPositionName())) ?><br/>
                    <? if(!$isClient): ?>
                        <a href="<?= Url::to(['/tenant/user/update', 'id' => $operation->transaction->user_created]) ?>">
                    <? endif; ?>
                        <?= Html::encode($operation->transaction->userCreated->getFullName()) ?>
                    <? if(!$isClient): ?>
                        </a>
                    <? endif; ?>
                <? endif ?>
            </td>
            <td><?= TransactionReportHelper::getTextOperation($operation, !(bool)$isClient); ?></td>
            <td style="text-align: right;">
                <?
                if ($operation->type_id == Operation::EXPENSES_TYPE || $operation->type_id == Operation::CASH_OUT_TYPE):
                    $debit += $operation->sum;
                    ?>
                    <?= $formatter->asMoney(+$operation->sum, $currencySymbol); ?>
                <? endif ?>
            </td>
            <td style="text-align: right;">
                <?
                if ($operation->type_id == Operation::INCOME_TYPE):
                    $credit += $operation->sum;
                    ?>
                <?= $formatter->asMoney(+$operation->sum, $currencySymbol); ?>
            <? endif ?>
            </td>
        </tr>
    <? endforeach; ?>
    <tr class="mt_spec">
    <? $last_operation = array_pop($operations) ?>
        <td colspan="2">
            <?
            $date = strtotime($last_operation->date) + $time_offset;
            echo $formatter-> asDate($date, 'shortDate');
            ?>
        </td>
        <td class="mt_itog"><b><?= t('balance', 'Total for the period') ?></b></td>
        <td style="text-align: right;">
            <b><?= $formatter->asMoney(+$debit, $currencySymbol); ?></b>
        </td>
        <td style="text-align: right;">
            <b><?= $formatter->asMoney(+$credit, $currencySymbol); ?></b>
        </td>
    </tr>
    <tr class="mt_spec">
        <td colspan="3" class="mt_itog"><b><?= t('balance', 'The balance for the period') ?></b></td>
        <td colspan="2" class="td_text_center">
            <b><?= $formatter->asMoney(round($credit - $debit, $minorUnit), $currencySymbol); ?></b>
        </td>
    </tr>
    <?
endif?>
