<?php
/* @var $isClient bool Если авторизован как клиент */
use yii\helpers\HtmlPurifier;

?>

<? if (!empty($operations)): ?>
    <table class="people_table money_table transaction_list">
        <tbody>
            <tr>
                <th style="width: 20%"><?= t('balance', 'Operation date') ?></th>
                <th style="width: 30%"><?= t('balance', 'Who') ?></th>
                <th style="width: 30%;"><?= t('balance', 'Operation') ?></th>
                <th style="width: 13%; text-align: right;"><?= t('balance', 'Debet') ?><a data-tooltip="5" class="tooltip">?<span style="display: none; opacity: 0;"><?= HtmlPurifier::process(Yii::t('hint','Expense')) ?></span></a></th>
                <th style="width: 13%; text-align: right;"><?= t('balance', 'Credit') ?><a data-tooltip="6" class="tooltip">?<span style="display: none; opacity: 0;"><?= HtmlPurifier::process(Yii::t('hint','Balance refill')) ?></span></a></th>
            </tr>
            <?= $this->render('_rows', compact('currency_id', 'operations', 'time_offset', 'kind_id', 'isClient')) ?>
        </tbody>
    </table>
<? else: ?>
    <p><?= t('reports', 'No operations') ?></p>
<? endif; ?>
