<?php
/* @var $isClient bool Если авторизован как клиент */
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

use app\modules\balance\models\Account;
use frontend\modules\tenant\models\Currency;

$this->registerJs('transactionFilterInit();');

$currencySymbol = Currency::getCurrencySymbol($currency_id);
if (in_array($kind_id, [Account::CLIENT_BONUS_KIND, Account::CLIENT_BONUS_UDS_GAME_KIND])) {
    $currencySymbol = t('currency', 'B') . ' (' . $currencySymbol . ')';
}

?>

<?php if (count($currencies) > 1): ?>
    <div class="currencies">
        <ul>
            <?php foreach ($currencies as $currency): ?>
                <li>
                    <a class="js-currency-link <?= $currency->currency_id == $currency_id ? 'active' : ''; ?>"
                       href="#"
                       data-href="<?= Url::to([
                           'show-balance',
                           'id'          => $id,
                           'kind_id'     => $kind_id,
                           'city_id'     => $city_id,
                           'currency_id' => $currency->currency_id,
                       ]); ?>">
                        <?= t('currency', $currency->name); ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>

<div style="height: 120px;" class="js-balance-form-<?= Html::encode($kind_id) ?>">
    <div class="current_balance">
        <span><?= t('balance', 'Current balance') ?></span>
        <b class="option_green">
            <span class="entity_balance">
                <?= app()->formatter->asMoney(+$account->balance, $currencySymbol); ?>
            </span>
        </b>
    </div>
    <? if ($isAllowed && !$isClient): ?>
        <?php $form = ActiveForm::begin([
            'id'                     => 'balance_form',
            'errorCssClass'          => 'input_error',
            'enableClientValidation' => false,
            'options'                => ['data-type' => $kind_id],
        ]); ?>
        <div class="balance_controls">
            <div class="bc_first">
                <?= $form->field($account, 'operation_type')->begin(); ?>
                <?= Html::activeDropDownList($account, 'operation_type', $account->getOperationList(),
                    ['class' => 'styled_select']) ?>

                <? if (!in_array($account->acc_kind_id, [Account::CLIENT_BONUS_KIND, Account::CLIENT_BONUS_UDS_GAME_KIND])): ?>
                    <a data-tooltip="3" class="tooltip">?<span
                            style="display: none; opacity: 0;"><?= HtmlPurifier::process(Yii::t('hint',
                                'For cash operations. Operation "Cash out" is not included in report')); ?></span></a>
                <? endif; ?>

                <?= $form->field($account, 'operation_type')->end(); ?>
                <?= $form->field($account, 'sum')->begin(); ?>
                <?= Html::activeTextInput($account, 'sum', ['class' => 'numeric', 'placeholder' => 0]) ?>
                <span><?= Html::encode(Currency::getCurrencySymbol($currency_id)); ?></span>
                <?= $form->field($account, 'sum')->end(); ?>
            </div>
            <div class="bc_second">
                <?= $form->field($account, 'transaction_comment')->begin(); ?>
                <?= Html::activeTextarea($account, 'transaction_comment', ['placeholder' => t('balance', 'Comment')]) ?>
                <?= $form->field($account, 'transaction_comment')->end(); ?>
            </div>
            <div class="bc_third">
                <button><?= t('app', 'Run') ?></button>
                <div class="bc_success" style="display: none"><i></i><?= t('app', 'Done') ?></div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    <? endif ?>
</div>

<h3><?= t('balance', 'History') ?></h3>
<div class="transactions">
    <div class="oor_fil" style="margin-bottom: 15px;">
        <div class="cof_left" style="margin-right: 0;">

            <form name="transaction_filter" action="<?= \yii\helpers\Url::to([
                'transaction-search',
                'account_id' => $account->account_id,
                'city_id'    => $city_id,
            ]) ?>" method="post">
                <div class="cof_first" style="margin-right: 25px;">
                    <label>
                        <input name="filter" type="radio" checked="" value="limit"> <?= t('balance', 'By week') ?>
                    </label>
                </div>
                <div class="cof_second disabled_cof">
                    <label><input name="filter" id="cof_time_choser" class="cof_time_choser" type="radio"
                                  value="period"> <?= t('reports', 'Period') ?></label>
                    <div class="cof_date notranslate disabled_select">
                        <a class="s_datepicker a_sc select"></a>
                        <div class="s_datepicker_content b_sc" style="display: none;">
                            <input name="first_date" class="sdp_input" type="text"
                                   value="<?= date("d.m.Y", mktime(0, 0, 0, date("m"), date("d") - 14, date("Y"))); ?>">
                            <div class="sdp first_date"></div>
                        </div>
                    </div>
                    <hr>
                    <div class="cof_date notranslate disabled_select">
                        <a class="s_datepicker a_sc select"></a>
                        <div class="s_datepicker_content b_sc" style="display: none;">
                            <input name="second_date" class="sdp_input" type="text" value="<?= date("d.m.Y"); ?>">
                            <div class="sdp"></div>
                        </div>
                    </div>

                    <?php if (app()->user->can('clients')) : ?>
                        <div class="context_menu_wrap export_menu_wrap">
                            <a class="context_menu a_sc select"><?= t('client', 'Export'); ?></a>
                            <div class="context_menu_content b_sc js-no-stop">
                                <a href="<?= Url::to([
                                    '/balance/balance/dump-transaction',
                                    'account_id' => $account->account_id,
                                    'city_id'    => $city_id,
                                ]); ?>" class="js-export-transaction"><?= t('client', 'Download to Excel'); ?></a>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </form>
        </div>
    </div>
    <div class="transact_history">
        <?= $this->render('_grid', compact('currency_id', 'operations', 'time_offset', 'kind_id', 'isClient')) ?>
    </div>
</div>
