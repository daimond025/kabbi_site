<?php

namespace app\modules\balance\controllers;

use Yii;
use yii\web\Controller;
use \yii\web\Response;

use app\modules\balance\models\Account;
use common\modules\employee\models\worker\Worker;
use app\modules\balance\models\Transaction;
use app\modules\balance\models\ElecsnetLog;
use frontend\modules\tenant\models\Currency;


/**
 * Elecsnet payment class. It needs for payment by elecsnet terminal.
 * Url for check operation balance/elecsnet/refill?type=1&reqid=maxim123|79226813111
 * Url for operation balance/elecsnet/refill?type=1&reqid=maxim123|79226813111&amount=1000&auth_code=147&currency=810
 */
class ElecsnetController extends Controller
{

    const CHECK_TYPE                 = 1;
    const FINANCIAL_TYPE             = 2;

    const SUCCESS_RESPONSE           = '00';
    const HELD_PAYMENT_RESPONSE      = '01';
    const WRONG_PHONE_RESPONSE       = '46';
    const WRONG_FORMAT_DATA_RESPONSE = '49';
    const TECHNICAL_ERROR_RESPONSE   = '45';
    const SYSTEM_ERROR_RESPONSE      = '62';
    const CURRENCY_VALUE             = '810';

    /**
     * Refill balance.
     * @param integer $type Operation type (1|2)
     * @param string $reqid Terminal data by type "domain|phone"
     */
    public function actionRefill($type, $reqid)
    {
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_RAW;
        $response->headers->set('Content-Type', 'text/plain; charset=windows-1251');
        if ($type == self::CHECK_TYPE) {
            $data = $this->checkOperation($reqid);
        } elseif ($type == self::FINANCIAL_TYPE) {
            $amount = get('amount');
            $auth_code = get('auth_code');
            $currency = get('currency');
            if (empty($amount) || empty($auth_code)) {
                $data = 'ans_code=' . self::WRONG_FORMAT_DATA_RESPONSE;
            } elseif (empty($currency) || $currency != self::CURRENCY_VALUE) {
                $data = 'ans_code=' . self::SYSTEM_ERROR_RESPONSE .
                        '&ansid=Прием_денежных_средств_осуществляется_только_в­_валюте_Российской_Федерации_(рубли)';
            } else {
                $data = $this->doOperation($reqid, $amount / 100, $auth_code, get('date'));
            }
        }

        if (!empty($data)) {
            (new ElecsnetLog([
                'type'      => $type,
                'reqid'     => $reqid,
                'amount'    => $amount,
                'auth_code' => $auth_code,
                'currency'  => $currency,
                'result'    => $data,
                    ]))
            ->save();
        }

        $response->data = mb_convert_encoding($data, "Windows-1251", "UTF-8");
    }

    /**
     * Verifying the operation.
     * @param string $reqid Terminal data by type "domain|phone"
     * @return string
     */
    private function checkOperation($reqid)
    {
        list($domain, $phone) = explode('|', $reqid);

        $worker = $this->getWorkerQuery($phone, $domain)->select(['callsign', 'last_name', 'name', 'second_name'])->one();

        if (empty($worker)) {
            return 'ans_code=' . self::WRONG_PHONE_RESPONSE;
        }

        return 'ans_code=' . self::SUCCESS_RESPONSE . '&ansid=' . $worker->callsign . '_' . $worker->last_name . '_' .
                mb_substr($worker->name, 0, 1) . '.' . mb_substr($worker->second_name, 0, 1);
    }

    private function doOperation($reqid, $amount, $auth_code, $date = null)
    {
        list($domain, $phone) = explode('|', $reqid);
        $worker = $this->getWorkerQuery($phone, $domain)->one();

        $transactionData = [
            'type_id'                  => Transaction::DEPOSIT_TYPE,
            'tenant_id'                => $worker->tenant_id,
            'sender_owner_id'          => $worker->worker_id,
            'sender_acc_kind_id'       => Account::WORKER_KIND,
            'sum'                      => $amount,
            'payment_method'           => Transaction::TERMINAL_ELECSNET_PAYMENT,
            'terminal_transact_number' => $auth_code,
            'terminal_transact_date'   => $date,
            'currency_id'              => Currency::CURRENCY_ID_RUS_RUBLE,
        ];

        $response = Transaction::createTransaction($transactionData);

        return $this->getOperationResponseCode($response);
    }

    private function getOperationResponseCode($transaction_response_code)
    {
        switch ($transaction_response_code) {
            case Transaction::SUCCESS_RESPONSE:
                $code = self::SUCCESS_RESPONSE;
                break;
            case Transaction::SYSTEM_ERROR_RESPONSE:
                $code = self::TECHNICAL_ERROR_RESPONSE;
                break;
            case Transaction::HELD_PAYMENT_RESPONSE:
                $code = self::HELD_PAYMENT_RESPONSE;
                break;
            default :
                $code = self::SYSTEM_ERROR_RESPONSE;
        }

        return 'ans_code=' . $code;
    }

    /**
     * Getting worker ActiveQuery
     * @param string $phone
     * @param string $domain
     * @return ActiveQuery the newly created [[ActiveQuery]] instance.
     */
    private function getWorkerQuery($phone, $domain)
    {
        return Worker::find()
                ->where(['phone' => $phone])
                ->joinWith(['tenant' => function($sub_query) use($domain) {
                        $sub_query->where(['domain' => $domain]);
                    }]);
    }

}
