<?php

namespace app\modules\balance\controllers;

use common\modules\employee\models\worker\Worker;
use common\modules\tenant\models\DefaultSettings;
use frontend\modules\companies\components\services\CompanyCheck;
use frontend\widgets\ExcelExport;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\modules\city\models\City;
use app\modules\balance\models\Transaction;
use app\modules\balance\models\Account;
use app\modules\balance\models\Operation;
use common\modules\tenant\models\TenantSetting;
use frontend\modules\tenant\models\Currency;

class BalanceController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (!\Yii::$app->user->isGuest) {
            user()->updateUserActiveTime();
        }

        return parent::beforeAction($action);
    }

    public function actionTransactionSearch($account_id = null, $city_id = null)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $result = $this->transactionSearch(
            $account_id,
            $city_id,
            post('filter'),
            post('first_date'),
            post('second_date')
        );

        return $this->renderAjax(post('view'), [
            'currency_id' => $result['currency_id'],
            'operations'  => $result['operations'],
            'time_offset' => $result['time_offset'],
        ]);
    }

    /**
     * Display entity balance.
     *
     * @param int      $id
     * @param int      $kind_id
     * @param int|null $city_id
     * @param int|null $currency_id
     *
     * @return mixed
     */
    public function actionShowBalance(
        $id,
        $kind_id,
        $city_id = null,
        $currency_id = null
    ) {
        (new CompanyCheck())->isAccess(Worker::findOne(['worker_id' => $id]));

        $tenant_id      = user()->tenant_id;
        $cityCurrencyId = TenantSetting::getSettingValue(
            user()->tenant_id,
            DefaultSettings::SETTING_CURRENCY,
            $city_id
        );

        if (empty($currency_id)) {
            $currency_id = $cityCurrencyId;
        }

        $currencyIds   = Account::find()
            ->select('currency_id')
            ->andWhere(['owner_id' => $id])
            ->andWhere(['tenant_id' => $tenant_id])
            ->andWhere(['acc_kind_id' => $kind_id])
            ->asArray()
            ->all();
        $currencyIds[] = $cityCurrencyId;

        $currencies = Currency::find()
            ->where(['currency_id' => $currencyIds])->all();

        $account = Account::findOne([
            'owner_id'    => $id,
            'tenant_id'   => $tenant_id,
            'acc_kind_id' => $kind_id,
            'currency_id' => $currency_id,
        ]);

        if (empty($account)) {
            $account = new Account();
            $account->loadDefaultValues();

            $account->owner_id    = $id;
            $account->tenant_id   = $tenant_id;
            $account->acc_kind_id = $kind_id;
            $account->acc_type_id = Account::PASSIVE_TYPE;
            $account->currency_id = $currency_id;
        }

        $isAllowed = Transaction::isAllowed($kind_id);

        if ($account->load(post())) {
            $account->sum = abs($account->sum);

            if (!$account->validate()) {
                session()->setFlash('error', implode("\n", $account->getFirstErrors()));

                return false;
            }

            if ($account->operation_type == Transaction::DEPOSIT_TYPE) {
                $payment_method = Transaction::CASH_PAYMENT;
            } elseif ($account->operation_type == Transaction::WITHDRAWAL_TYPE) {
                $payment_method = Transaction::WITHDRAWAL_PAYMENT;
            } elseif ($account->operation_type == Transaction::WITHDRAWAL_CASH_OUT_TYPE) {
                $account->operation_type = Transaction::WITHDRAWAL_TYPE;
                $payment_method          = Transaction::WITHDRAWAL_CASH_OUT_PAYMENT;
            }

            if (in_array($account->acc_kind_id, [Account::CLIENT_BONUS_KIND, Account::CLIENT_BONUS_UDS_GAME_KIND])) {
                $payment_method = Transaction::BONUS_PAYMENT;
            }

            $transactionData = [
                'type_id'            => $account->operation_type,
                'tenant_id'          => $tenant_id,
                'sender_owner_id'    => $id,
                'sender_acc_kind_id' => $kind_id,
                'currency_id'        => $currency_id,
                'sum'                => $account->sum,
                'comment'            => $account->transaction_comment,
                'user_created'       => user()->id,
                'payment_method'     => $payment_method,
            ];

            if ($isAllowed) {
                $response_code = Transaction::createTransaction($transactionData);
            } else {
                session()->setFlash('error', t('balance', 'Forbbiden for current user'));

                return false;
            }

            if ($response_code == 100) {
                $account = Account::findOne([
                    'owner_id'    => $id,
                    'tenant_id'   => $tenant_id,
                    'acc_kind_id' => $kind_id,
                    'currency_id' => $currency_id,
                ]);
            } else {
                session()->setFlash(
                    'error',
                    t('error', 'Service is temporarily unavailable. Notify to administrator, please.')
                );
                Yii::warning(
                    'Код ответа операции биллинга - ' . $response_code . '. sender_acc_id - ' . $account->account_id,
                    'billing'
                );

                return false;
            }
        }

        $time_offset = City::getTimeOffset($city_id);
        $operations  = Operation::getByWeekForAccount($account->account_id);

        return $this->renderAjax(
            'transaction',
            compact(
                'id',
                'kind_id',
                'city_id',
                'currency_id',
                'currencies',
                'account',
                'operations',
                'time_offset',
                'isAllowed'
            )
        );
    }


    public function actionDumpTransaction($account_id = null, $city_id = null)
    {
        $result      = $this->transactionSearch(
            $account_id,
            $city_id,
            get('filter'),
            get('first_date'),
            get('second_date')
        );
        $currency_id = $result['currency_id'];
        $operations  = $result['operations'];
        $time_offset = $result['time_offset'];


        if (empty($operations)) {
            ExcelExport::widget([
                'data'     => [],
                'format'   => 'Excel5',
                'fileName' => 'transaction',
            ]);
        }
        $currencySymbol = Currency::getCurrencySymbol($currency_id);
        $minorUnit      = Currency::getMinorUnit($currency_id);

        $body = [];

        $body[] = [
            get('filter') == 'period' ? t(
                'reports',
                    'Period'
            ) . ' ' . get('first_date') . '-' . get('second_date') : t('balance', 'By week'),
        ];

        $body[] = [];
        $body[] = [
            t('balance', 'Beginning balance'),
            ($currencySymbol) ? app()->formatter->asMoney(
                round(+$operations[0]->getOpeningBalance(), $minorUnit),
                $currencySymbol
            ) : '0',
        ];

        $debit = $credit = 0;

        $info = [];
        /** @var Operation $operation */
        foreach ($operations as $operation) {
            $date = strtotime($operation->date) + $time_offset;

            $time = app()->formatter->asDate($date, 'shortDate') . ", " . app()->formatter->asTime($date, 'short');

            $who = !empty($operation->transaction->userCreated) ?
                $operation->transaction->userCreated->getFullName() : '';

            $operationTypeName = $operation->getOperationTypeName() .
                " ({$operation->transaction->getPaymentTypeName()})";

            $description = $operation->getDescriptionOreration(false);


            if ($operation->type_id == Operation::EXPENSES_TYPE || $operation->type_id == Operation::CASH_OUT_TYPE) {
                $debit += $operation->sum;
                $debitValue     = app()->formatter->asMoney(+$operation->sum, $currencySymbol);
            } else {
                $debitValue = '';
            }
            if ($operation->type_id == Operation::INCOME_TYPE) {
                $credit += $operation->sum;
                $creditValue     = app()->formatter->asMoney(+$operation->sum, $currencySymbol);
            } else {
                $creditValue = '';
            }
            $info[] = [$time, $who, $operationTypeName, $description, $debitValue, $creditValue];
        }


        $body[] = [];
        $body[] = [
            t('balance', 'Operation date'),
            t('balance', 'Who'),
            t('balance', 'Operation'),
            t('balance', 'Comment'),
            t('balance', 'Debet'),
            t('balance', 'Credit'),
        ];
        $body[] = [];
        $body[] = [
            '',
            '',
            '',
            t('balance', 'Total for the period'),
            app()->formatter->asMoney(+$debit, $currencySymbol),
            app()->formatter->asMoney(+$credit, $currencySymbol),

        ];
        $body[] = [];
        $body   = array_merge($body, $info);


        ExcelExport::widget([
            'data'     => $body,
            'format'   => 'Excel5',
            'fileName' => 'transaction',
        ]);
    }

    public function transactionSearch(
        $account_id = null,
        $city_id = null,
        $filter = 'limit',
        $first_date = null,
        $second_date = null
    ) {
        $time_offset = City::getTimeOffset($city_id);

        $account = Account::findone($account_id);
        if (empty($account)) {
            return false;
        }

        $currency_id = $account->currency_id;
        $operation   = new Operation();
        switch ($filter) {
            case 'limit':
                $operations = $operation->getByWeekForAccount($account_id);
                break;
            case 'period':
                $operations = $operation->getByPeriodForAccount($account_id, $first_date, $second_date);
                break;
            default:
                $operations = null;
        }

        return [
            'time_offset' => $time_offset,
            'currency_id' => $currency_id,
            'operations'  => $operations,
        ];
    }
}
