<?php

namespace app\modules\balance\controllers;

use common\modules\employee\models\worker\Worker;
use Yii;
use yii\web\Controller;
use \yii\web\Response;
use app\modules\balance\models\Account;
use app\modules\balance\models\Million;
use app\modules\balance\models\MillionLog;
use app\modules\balance\models\Transaction;
use frontend\modules\tenant\models\Currency;

/**
 * Elecsnet payment class. It needs for payment by MILLION terminal.
 * Проверка исполнителя по ключу и телефону /balance/million/check?apikey=320&username=994500000000
 * Запрос на проведение платежа /process/?apikey=320&username=994500000000&cost=10&id=123456789&timestamp=27-07-2014T11:36:05
 * Получение статуса платежа /status/?apikey=320&id=123456789
 * @property integer $c     Код ответа
 * @property string $m      Сообщение
 * @property array $d       Массив с данными
 */
class MillionController extends Controller
{

    const CHECK_TYPE = 'check';

    const PROCESS_TYPE = 'process';

    const STATUS_TYPE = 'status';

    const STATUS_UNDERFINED = 'underfined';

    const SUCCESS_RESPONSE              = '00';

    const PHONE_NOT_FOUND_RESPONSE      = '01';

    const HELD_PAYMENT_RESPONSE         = '02';

    const TECHNICAL_ERROR_RESPONSE      = '03';

    const SYSTEM_ERROR_RESPONSE         = '04';

    const NOT_ENOUGH_DATA_RESPONSE      = '99';


    public $log;



    public function __construct($id,$module)
    {
        $this->log = new MillionLog();
        $this->log->attributes = [
            'apikey' => Yii::$app->request->get('apikey'),
            'data' => $this->getGetValue(),
            'create_time' => time(),
        ];

        parent::__construct($id,$module);
    }



    public function init()
    {
        parent::init();
        Yii::$app->errorHandler->errorAction = 'balance/million/error';
        Yii::$app->errorHandler->discardExistingOutput = true;
        $response = Yii::$app->response;
        $response->charset = 'UTF-8';
    }



    public function actionError()
    {
        $this->log->attributes = [
            'type' => self::STATUS_UNDERFINED,
            'result' => self::NOT_ENOUGH_DATA_RESPONSE,
        ];
        $data = new Million();
        return $data->toJson();
    }



    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);
        $this->log->save();
        return $result;
    }



    /**
     * Проверка исполнителя по ключу и телефону
     * @param integer $apikey   Идентификатор службы такси
     * @param integer $phone    Телефон исполнителя
     * @return mix
     */
    public function actionCheck($apikey = null,$username = null)
    {
        $phone = $username;
        $this->log->attributes = [
            'type' => self::CHECK_TYPE,
            'phone' => $phone,
        ];

        $data = new Million();
        if(!$apikey || !$phone){
            $data->m = "Not enough data";
            $this->log->result = self::NOT_ENOUGH_DATA_RESPONSE;
        }
        else{
            $worker = $this->getData($apikey,$phone);
            if(!$worker) {
                $data->m = "User \"$phone\" not found";
                $this->log->result = self::PHONE_NOT_FOUND_RESPONSE;
            }
            else {
                $data->attributes = [
                    'c' => Million::RESPONSE_COMPLETE,
                    'd' => $worker
                ];
                $this->log->attributes = [
                    'result' => self::SUCCESS_RESPONSE,
                    'callsign' => $worker['username'],
                ];
            }
        }
        return $data->toJson();
    }



    /**
     * Запрос на проведение платежа
     * @param integer $apikey   Идентификатор службы такси
     * @param integer $username Телефон исполнителя
     * @param integer $cost     Сумма платежа в AZN
     * @param integer id        Уникальный ИД транзакции
     * @param string $apikey    Дата проведения платежа
     * @return mix
    **/
    public function actionProcess($apikey = null, $username = null, $cost = null, $id = null, $timestamp = null)
    {
        $phone = $username;
        $this->log->attributes = [
            'type' => self::PROCESS_TYPE,
            'sum' => $cost,
            'phone' => $phone,
        ];

        $worker = Worker::find()
            ->select(['worker_id','callsign'])
            ->where(['phone' => $phone, 'tenant_id' => $apikey, 'block' => 0])
            ->one();
        $data = new Million();

        if(!$apikey || !$phone || !$id || !$cost || !$timestamp){
            $this->log->result = self::NOT_ENOUGH_DATA_RESPONSE;
        }
        else {
            if($worker){

                $transactionData = [
                    'type_id'                  => Transaction::DEPOSIT_TYPE,
                    'tenant_id'                => $apikey,                                  // Клиент
                    'sender_owner_id'          => $worker->worker_id,                       // ИД исполнителя
                    'sender_acc_kind_id'       => Account::WORKER_KIND,
                    'sum'                      => $cost,                                    // Сумма
                    'payment_method'           => Transaction::TERMINAL_MILLION_PAYMENT,    // Метод
                    'terminal_transact_number' => $id,                                      // ИД транзакции
                    'terminal_transact_date'   => $timestamp,                               // Время операции
                    'currency_id'              => Currency::CURRENCY_ID_AZN_MANAT,          // Ид валюты
                ];
                $resp = Transaction::createTransaction($transactionData);
                if($resp == Transaction::SUCCESS_RESPONSE){
                    $data->c = Million::RESPONSE_COMPLETE;
                }
                $this->log->attributes = [
                    'result' => $this->getOperationResponseCode($resp),
                    'callsign' => $worker->callsign,
                ];
            }
            else {
                $this->log->result = self::PHONE_NOT_FOUND_RESPONSE;
            }
        }
        return $data->toJson(['c','d']);
    }



    /**
     * Получение статуса платежа
     * @param integer $apikey   Идентификатор службы такси
     * @param integer $id       Уникальный ИД транзакции
     * @return mix
    **/
    public function actionStatus($apikey = null, $id = null)
    {
        $this->log->type = self::STATUS_TYPE;

        $data =  new Million();
        if(!$apikey || !$id) {
            $this->log->result = self::NOT_ENOUGH_DATA_RESPONSE;
        }
        else {
            $transaction = Transaction::find()
                ->select('sender_acc_id')
                ->andWhere(['payment_method'=>Transaction::TERMINAL_MILLION_PAYMENT,'terminal_transact_number'=> $id])
                ->one();

            if(!empty($transaction) && $transaction->senderAcc->tenant_id == $apikey){
                $data-> attributes = [
                    'c' => Million::RESPONSE_COMPLETE,
                    'd' => ['status' => 'complete'],
                ];
                $this->log->result = self::SUCCESS_RESPONSE;
            }
            else{
                $this->log->result = self::HELD_PAYMENT_RESPONSE;
            }
        }
        return $data->toJson(['c','d']);
    }


    /**
     * Получаем данные исполнителя
     *
     * @param integer $apikey Идентификатор службы такси
     * @param integer $phone Телефон исполнителя
     *
     * @return array|null
     */
    public function getData($apikey, $phone)
    {
        $data   = null;
        $worker = Worker::find()
            ->select(['worker_id', 'last_name', 'name', 'callsign'])
            ->where(['phone' => $phone, 'block' => 0, 'tenant_id' => $apikey])
            ->one();

        if (!empty($worker)) {
            $account = Account::find()
                ->select('balance')
                ->andWhere([
                    'tenant_id'   => $apikey,
                    'owner_id'    => $worker->worker_id,
                    'currency_id' => Currency::CURRENCY_ID_AZN_MANAT,
                    'acc_kind_id' => Account::WORKER_KIND,
                    'acc_type_id' => Account::PASSIVE_TYPE,
                ])
                ->one();

            if (!empty($account)) {
                $data = [
                    'username' => $worker->callsign,
                    'fio'      => $worker->last_name . " " . $worker->name,
                    'balance'  => $account->balance,
                ];
            } else {
                $data = [
                    'username' => $worker->callsign,
                    'fio'      => $worker->last_name . " " . $worker->name,
                    'balance'  => 0,
                ];
            }
        }

        return $data;
    }



    private function getOperationResponseCode($transaction_response_code)
    {
        switch ($transaction_response_code) {
            case Transaction::SUCCESS_RESPONSE:
                $code = self::SUCCESS_RESPONSE;
                break;
            case Transaction::SYSTEM_ERROR_RESPONSE:
                $code = self::TECHNICAL_ERROR_RESPONSE;
                break;
            case Transaction::HELD_PAYMENT_RESPONSE:
                $code = self::HELD_PAYMENT_RESPONSE;
                break;
            default :
                $code = self::SYSTEM_ERROR_RESPONSE;
        }
        return $code;
    }



    private function getGetValue()
    {
        $result = [];
        $request = Yii::$app->request->get();
        unset($request['q']); // удаляем route
        foreach($request as $key=>$value){
            $result[] = $key . '=' . $value;
        }
        return implode('&',$result);
    }

}







