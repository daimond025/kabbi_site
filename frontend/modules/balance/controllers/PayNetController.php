<?php

namespace app\modules\balance\controllers;

use frontend\modules\balance\components\payNet\PayNetSoapService;
use yii\web\Controller;


class PayNetController extends Controller
{

    public function actions()
    {
        return [
            'server' => [
                'class'    => 'mongosoft\soapserver\Action',
                'wsdlUrl'  => $this->getWsdlFile(),
                'provider' => new PayNetSoapService(),
            ],
        ];
    }

    protected function getWsdlFile()
    {
        return __DIR__ . '/../components/payNet/ProviderWebService.wsdl';
    }
}



