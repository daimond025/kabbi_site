$(function(){

    $(document).on('click', '.js-export-transaction', function(e) {
        var params = $('form[name="transaction_filter"]').serialize();
        var url = $(this).attr('href');
        url = params != '' ? url + '&' + params : url ;
        window.location.href = url;
        return false;
    });
    
});