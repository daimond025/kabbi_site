function transactinSearch(form)
{
    var $history = form.closest('.transactions').find('.transact_history');
    var table = $history.find('table');
    var view = table.size() > 0 ? '_rows' : '_grid';
    $.post(
        form.attr('action'),
        form.serialize() + '&view=' + view,
        function(html){
            if (view === '_grid') {
                $history.html(html);
            } else {
                var first_tr = table.find('tr:first');
                first_tr.nextAll('tr').remove();
                first_tr.after(html);
            }
        }
    );
}

//Операции с балансом
function balanceOperationInit()
{
    $(document).on('click', '.balance_controls .bc_third button', function(e){
        e.preventDefault();

        var form = $(this).parents('form');
        var error = false;
        var numeric = form.find('.numeric');
        var sum = numeric.val();
        var $this = $(this);

        numeric.parent().removeClass('input_error');

        if(isNaN(sum) || sum == '')
        {
            numeric.parent().addClass('input_error');
            error = true;
        }

        if(!error)
        {
            if ($this.hasClass('loading_button')) {
                return;
            }

            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                dataType: 'html',
                beforeSend: function(){
                    $this.addClass('loading_button');
                },
                success: function(html){
                    if ($.trim(html) == '') {
                        location.reload();
                    }
                    var form_type = form.data('type');
                    $('.tabs_content div.js-balance-form-' + form_type).parent().html(html);
                    $('#account-sum, #account-transaction_comment').val('');
                    $('.bc_third .bc_success').show().hide(2500);
                },
                complete: function(){
                    $this.removeClass('loading_button');
                }
            });
        }
    });

    $(document).on('click', '.js-currency-link', function() {
        var $this= $(this);

        $.get(
                $this.data('href')
                )
                .done(function (data) {
                    $('.tabs_content > div.active').html(data);
                })
                .fail(function (data) {
                    console.log('fail', data);
                });

        return false;
    });

    $(document).on('click', 'form[name="transaction_filter"] .cof_first input, #cof_time_choser', function(){
        transactinSearch($(this).parents('form'));
    });
}

function transactionFilterInit()
{
    $('.sdp').each(function () {
        var obj = $(this);
        var type = $(this).data('type');
        $(this).datepicker({
            altField: $(this).prev('input'),
            onSelect: function (date) {
                $(this).parent().prev('.a_sc').html(date).removeClass('sel_active'); $(this).parent().hide();
                transactinSearch(obj.parents('form'));
            }
        });
        if ($(this).hasClass('first_date')) {
            $(this).datepicker("setDate", '-14');
        }
        obj.closest('.cof_date').find('.a_sc').text(obj.val());
    });

    registerOrderOpenInModalEvent('.open_order');

}