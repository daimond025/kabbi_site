<?php

namespace frontend\modules\balance\assets;

use yii\web\AssetBundle;

class BalanceAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/balance/assets/js';

    public $publishOptions = [
        'forceCopy' => true,
    ];
    public $css = [
    ];
    public $js = [
        'functions.js',
        'main.js',
    ];
    public $depends = [
        'app\modules\order\assets\ViewOrderAsset',
    ];
}