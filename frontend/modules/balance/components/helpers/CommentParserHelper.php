<?php

namespace frontend\modules\balance\components\helpers;


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class CommentParserHelper
{


    const TYPE_BONUS_ACTIVATION_PROMO = 'bonusActivationPromo';

    const TYPES_COMMENT = [
        self::TYPE_BONUS_ACTIVATION_PROMO,
    ];

    public static function parsingComment($comment)
    {

        foreach (self::TYPES_COMMENT as $type) {

            if (preg_match(sprintf('/^%s/', $type), $comment)) {
                $partsComment = explode(';', $comment);
                $comment      = ArrayHelper::getValue($partsComment, 1);
                $namePromo    = Html::a(
                    ArrayHelper::getValue($partsComment, 2),
                    '/promocode/promo/update/' . ArrayHelper::getValue($partsComment, 3),
                    ['target' => '_blank']
                );
                $code         = ArrayHelper::getValue($partsComment, 4);

                return t('balance', $comment, [
                    'promoName' => $namePromo,
                    'code'      => $code
                ]);
            }

        }

        return $comment;

    }
}