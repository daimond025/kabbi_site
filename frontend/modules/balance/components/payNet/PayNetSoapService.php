<?php

namespace frontend\modules\balance\components\payNet;


use app\modules\balance\models\Account;
use app\modules\balance\models\Transaction;
use frontend\modules\balance\models\payNet\CancelTransactionArguments;
use frontend\modules\balance\models\payNet\CancelTransactionResult;
use frontend\modules\balance\models\payNet\ChangePasswordArguments;
use frontend\modules\balance\models\payNet\ChangePasswordResult;
use frontend\modules\balance\models\payNet\CheckTransactionArguments;
use frontend\modules\balance\models\payNet\CheckTransactionResult;
use frontend\modules\balance\models\payNet\GenericParam;
use frontend\modules\balance\models\payNet\GetInformationArguments;
use frontend\modules\balance\models\payNet\GetInformationResult;
use frontend\modules\balance\models\payNet\GetStatementArguments;
use frontend\modules\balance\models\payNet\GetStatementResult;
use frontend\modules\balance\models\payNet\PayNetCancelTransaction;
use frontend\modules\balance\models\payNet\PayNetLog;
use frontend\modules\balance\models\payNet\PerformTransactionArguments;
use frontend\modules\balance\models\payNet\PerformTransactionResult;
use frontend\modules\balance\models\payNet\TransactionStatement;
use frontend\modules\employee\models\worker\Worker;
use frontend\modules\tenant\models\Currency;
use yii\helpers\ArrayHelper;

class PayNetSoapService
{
    protected $username = 'v3GIW4NwSC';
    protected $password = '6EuN4Z66Y_q1Vt4vmLIZbIe4sLJ0kB0Q';


    public function GetInformation($arguments)
    {
        $param  = new GetInformationArguments($arguments);
        $result = new GetInformationResult();

        if (!$this->checkLogin($param->username, $param->password)) {
            $result->status = 412;
        } else {

            $worker = $this->findWorker($param->getParameter('phone'), $param->serviceId);
            if (!$worker) {
                $result->status = 302;


            } else {
                $result->parameters[] = new GenericParam('name', $worker->getShortName());
                $result->parameters[] = new GenericParam('callsign', $worker->callsign);
                $result->parameters[] = new GenericParam('phone', $worker->phone);
            }
        }

        $data = [
            'phone' => $param->getParameter('phone'),
        ];

        $this->log([
            'type'      => PayNetLog::TYPE_GET_INFORMATION,
            'serviceId' => $param->serviceId,
            'data'      => json_encode($data),
            'result'    => $result->status,
        ]);

        $result->errorMsg = PayNetErrors::getErrorMessage($result->status);

        return $result;
    }

    public function PerformTransaction($arguments)
    {
        $param  = new PerformTransactionArguments($arguments);
        $result = new PerformTransactionResult();

        if (!$this->checkLogin($param->username, $param->password)) {
            $result->status = 412;

        } else {
            $worker = $this->findWorker($param->getParameter('phone'), $param->serviceId);

            if (!$worker) {
                $result->status = 302;
            } else {

                $transactionData = [
                    'type_id'                  => Transaction::DEPOSIT_TYPE,
                    'tenant_id'                => $param->serviceId,                       // Клиент
                    'sender_owner_id'          => $worker->worker_id,                      // ИД исполнителя
                    'sender_acc_kind_id'       => Account::WORKER_KIND,
                    'sum'                      => $param->amount / 100,                    // Сумма
                    'payment_method'           => Transaction::TERMINAL_PAYNET_PAYMENT,    // Метод
                    'terminal_transact_number' => (string)$param->transactionId,           // ИД транзакции
                    'terminal_transact_date'   => (string)$param->transactionTime,         // Время операции
                    'currency_id'              => Currency::CURRENCY_ID_UZS,               // Ид валюты
                    'comment'                  => $param->getParameter('comment'),
                ];

                $resp = Transaction::createTransaction($transactionData);

                switch ($resp) {
                    case Transaction::HELD_PAYMENT_RESPONSE:
                        $result->status = 201;
                        break;

                    case Transaction::SUCCESS_RESPONSE:
                        $result->providerTrnId = $param->transactionId;
                        break;

                    default:
                        $result->status = 102;
                }
            }
        }

        $data = [
            'phone'           => $param->getParameter('phone'),
            'amount'          => $param->amount,
            'transactionId'   => $param->transactionId,
            'transactionTime' => $param->transactionTime,
        ];

        $this->log([
            'type'      => PayNetLog::TYPE_PERFORM_TRANSACTION,
            'username'  => $param->username,
            'serviceId' => $param->serviceId,
            'data'      => json_encode($data),
            'result'    => $result->status,
        ]);

        $result->errorMsg = PayNetErrors::getErrorMessage($result->status);

        return $result;
    }

    public function CheckTransaction($arguments)
    {
        $param  = new CheckTransactionArguments($arguments);
        $result = new CheckTransactionResult();

        if (!$this->checkLogin($param->username, $param->password)) {
            $result->status = 412;

        } else {
            $isTransaction = Transaction::find()
                ->where([
                    'payment_method'           => Transaction::TERMINAL_PAYNET_PAYMENT,    // Метод
                    'terminal_transact_number' => (string)$param->transactionId,           // ИД транзакции
                ])
                ->exists();

            if (!$isTransaction) {
                $result->status = 601;
            } else {
                $transaction = $this->findCancelTransaction($param->serviceId, $param->transactionId);
                if ($transaction) {
                    $result->transactionState = 2;
                } else {
                    $result->transactionState = 1;
                }

                $result->providerTrnId               = $param->transactionId;
                $result->transactionStateErrorStatus = 0;
                $result->transactionStateErrorMsg    = 'OK';
            }
        }

        $data = [
            'transactionId'   => $param->transactionId,
            'transactionTime' => $param->transactionTime,
        ];

        $this->log([
            'type'      => PayNetLog::TYPE_CHECK_TRANSACTION,
            'username'  => $param->username,
            'serviceId' => $param->serviceId,
            'data'      => json_encode($data),
            'result'    => $result->status,
        ]);

        $result->errorMsg = PayNetErrors::getErrorMessage($result->status);

        return $result;
    }

    public function CancelTransaction($arguments)
    {
        $param  = new CancelTransactionArguments($arguments);
        $result = new CancelTransactionResult();

        if (!$this->checkLogin($param->username, $param->password)) {
            $result->status = 412;
        } else {
            $isTransaction = Transaction::find()
                ->where([
                    'payment_method'           => Transaction::TERMINAL_PAYNET_PAYMENT,    // Метод
                    'terminal_transact_number' => (string)$param->transactionId,           // ИД транзакции
                ])
                ->exists();

            if (!$isTransaction) {
                $result->status = 601;
            } else {
                $transaction = $this->findCancelTransaction($param->serviceId, $param->transactionId);
                if ($transaction) {
                    $result->status           = 202;
                    $result->transactionState = 2;
                } else {
                    $newTransaction             = new PayNetCancelTransaction();
                    $newTransaction->attributes = [
                        'tenant_id'      => $param->serviceId,
                        'worker_id'      => 1,
                        'transaction_id' => (string)$param->transactionId,
                        'status'         => PayNetCancelTransaction::STATUS_ACTIVE,
                    ];
                    $result->status             = 0;
                    $result->transactionState   = $newTransaction->save() ? 2 : 1;
                }
            }

        }

        $result->errorMsg = PayNetErrors::getErrorMessage($result->status);

        $data = [
            'transactionId' => $param->transactionId,
        ];

        $this->log([
            'type'      => PayNetLog::TYPE_CANCEL_TRANSACTION,
            'username'  => $param->username,
            'serviceId' => $param->serviceId,
            'data'      => json_encode($data),
            'result'    => $result->status,
        ]);

        return $result;
    }

    public function GetStatement($arguments)
    {
        $param  = new GetStatementArguments($arguments);
        $result = new GetStatementResult();

        if (!$this->checkLogin($param->username, $param->password)) {
            $result->status = 412;
        } else {
            $transactions = Transaction::find()
                ->joinWith('senderAcc', false)
                ->where([
                    'payment_method' => Transaction::TERMINAL_PAYNET_PAYMENT,
                    'tenant_id'      => $param->serviceId,
                ])
                ->andWhere(['between', 'terminal_transact_date', $param->dateFrom, $param->dateTo])
                ->indexBy('terminal_transact_number')
                ->all();

            $cancelTransaction = PayNetCancelTransaction::find()
                ->where([
                    'transaction_id' => ArrayHelper::getColumn($transactions, 'terminal_transact_number'),
                ])
                ->indexBy('transaction_id')
                ->all();

            $transactions = array_diff_key($transactions, $cancelTransaction);

            /** @var Transaction $transaction */
            foreach ($transactions as $transaction) {
                $currentStatement                  = new TransactionStatement();
                $currentStatement->transactionId   = $transaction->terminal_transact_number;
                $currentStatement->amount          = $transaction->sum * 100;
                $currentStatement->transactionTime = $transaction->terminal_transact_date;

                $result->statements[] = $currentStatement;
            }
        }

        $data = [
            'dateFrom'          => $param->dateFrom,
            'dateTo'            => $param->dateTo,
            'onlyTransactionId' => $param->onlyTransactionId,
        ];

        $this->log([
            'type'      => PayNetLog::TYPE_GET_STATEMENT,
            'username'  => $param->username,
            'serviceId' => $param->serviceId,
            'data'      => json_encode($data),
            'result'    => $result->status,
        ]);

        $result->errorMsg = PayNetErrors::getErrorMessage($result->status);

        return $result;
    }

    //    public function ChangePassword($arguments)
    //    {
    //        $param  = new ChangePasswordArguments($arguments);
    //        $result = new ChangePasswordResult();
    //
    //        $result->status = 100;
    //
    //        //        $result->errorMsg = PayNetErrors::getErrorMessage($result->status);
    //
    //        return $result;
    //    }

    /**
     * @param $phone
     * @param $tenantId
     *
     * @return array|null|Worker
     */
    protected function findWorker($phone, $tenantId)
    {
        return $worker = Worker::find()
            ->where([
                'phone'     => $phone,
                'tenant_id' => $tenantId,
                'block'     => 0,
            ])
            ->one();
    }

    /**
     * @param $tenantId
     * @param $transactionId
     *
     * @return array|null|PayNetCancelTransaction
     */
    protected function findCancelTransaction($tenantId, $transactionId)
    {
        return PayNetCancelTransaction::find()
            ->where([
                'tenant_id'      => $tenantId,
                'transaction_id' => $transactionId,
            ])
            ->one();
    }


    protected function log($params)
    {
        $log = new PayNetLog($params);

        return $log->save();
    }

    protected function checkLogin($username, $password)
    {
        return $this->username === $username && $this->password === $password;
    }

}