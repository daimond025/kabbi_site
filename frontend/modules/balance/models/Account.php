<?php

namespace app\modules\balance\models;

use Yii;
use frontend\modules\tenant\models\Currency;

/**
 * This is the model class for table "{{%account}}".
 *
 * @property integer       $account_id
 * @property integer       $acc_kind_id
 * @property integer       $acc_type_id
 * @property integer       $owner_id
 * @property integer       $currency_id
 * @property integer       $tenant_id
 * @property double        $balance
 *
 * @property AccountKind   $accKind
 * @property AccountType   $accType
 * @property Currency      $currency
 * @property Tenant        $tenant
 * @property Operation[]   $operations
 * @property Transaction[] $transactions
 * @property string        $fio
 */
class Account extends \yii\db\ActiveRecord
{

    const ACTIVE_TYPE = 1;
    const PASSIVE_TYPE = 2;

    const TENANT_KIND = 1;
    const WORKER_KIND = 2;
    const CLIENT_KIND = 3;
    const COMPANY_KIND = 4;
    const SYSTEM_KIND = 5;
    const CLIENT_BONUS_KIND = 7;
    const SYSTEM_BONUS_KIND = 8;
    const CLIENT_BONUS_UDS_GAME_KIND = 9;
    const SYSTEM_BONUS_UDS_GAME_KIND = 10;

    public $sum;
    public $transaction_comment;
    public $operation_type = Transaction::DEPOSIT_TYPE;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%account}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['acc_kind_id', 'acc_type_id', 'currency_id'], 'required'],
            [['acc_kind_id', 'acc_type_id', 'owner_id', 'currency_id', 'tenant_id'], 'integer'],
            ['transaction_comment', 'string'],
            [['balance', 'operation_type'], 'number'],
            [
                'sum',
                'number',
                'min' => -999999999.99,
                'max' => 999999999.99,
            ],
            [['balance', 'sum'], 'default', 'value' => 0],
            [
                'operation_type',
                'in',
                'range' => [
                    Operation::INCOME_TYPE,
                    Operation::EXPENSES_TYPE,
                ],
                'when'  => function ($model) {
                    return in_array($model->acc_kind_id, [self::CLIENT_BONUS_KIND, self::CLIENT_BONUS_UDS_GAME_KIND]);
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'account_id'  => 'Account ID',
            'acc_kind_id' => 'Acc Kind ID',
            'acc_type_id' => 'Acc Type ID',
            'owner_id'    => 'Owner ID',
            'currency_id' => 'Currency ID',
            'tenant_id'   => 'Tenant ID',
            'balance'     => 'Balance',
            'sum'         => t('balance', 'Sum'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccKind()
    {
        return $this->hasOne(AccountKind::className(), ['kind_id' => 'acc_kind_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccType()
    {
        return $this->hasOne(AccountType::className(), ['type_id' => 'acc_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperations()
    {
        return $this->hasMany(Operation::className(), ['account_id' => 'account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(Transaction::className(), ['sender_acc_id' => 'account_id']);
    }

    /**
     * Getting list of operations
     * @return array
     */
    public function getOperationList()
    {
        $operations = [
            Transaction::DEPOSIT_TYPE    => t('balance', 'Refill'),
            Transaction::WITHDRAWAL_TYPE => t('balance', 'Expenses'),
        ];
        if (!in_array($this->acc_kind_id, [self::CLIENT_BONUS_KIND, self::CLIENT_BONUS_UDS_GAME_KIND])) {
            $operations[Transaction::WITHDRAWAL_CASH_OUT_TYPE] = t('balance', 'Cash out');
        }

        return $operations;
    }

    public static function getCurrencyIdByCity($companyId,$tenantId,$kind){
        return self::find()
            ->select(['currency_id','balance'])
            ->andWhere(['owner_id' => $companyId])
            ->andWhere(['tenant_id' => $tenantId])
            ->andWhere(['acc_kind_id' => $kind])
            ->asArray()
            ->all();
    }


}
