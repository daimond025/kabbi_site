<?php

namespace app\modules\balance\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%elecsnet_log}}".
 *
 * @property integer $log_id
 * @property string $type
 * @property string $reqid
 * @property string $amount
 * @property string $auth_code
 * @property string $currency
 * @property string $result
 * @property string $date
 * @property integer $create_time
 */
class ElecsnetLog extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%elecsnet_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'reqid', 'result'], 'required'],
            [['type'], 'string'],
            [['create_time'], 'integer'],
            [['reqid', 'amount', 'auth_code', 'currency', 'result', 'date'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'log_id' => 'Log ID',
            'type' => 'Type',
            'reqid' => 'Reqid',
            'amount' => 'Amount',
            'auth_code' => 'Auth Code',
            'currency' => 'Currency',
            'result' => 'Result',
            'date' => 'Date',
            'create_time' => 'Create Time',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_time'],
                ],
            ],
        ];
    }
}
