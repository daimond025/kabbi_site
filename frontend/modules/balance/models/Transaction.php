<?php

namespace app\modules\balance\models;

use app\modules\order\models\Order;
use common\helpers\CacheHelper;
use Yii;
use common\components\gearman\Gearman;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%transaction}}".
 *
 * @property integer $transaction_id
 * @property integer $type_id
 * @property integer $sender_acc_id
 * @property integer $receiver_acc_id
 * @property double $sum
 * @property integer $payment_method
 * @property string $date
 * @property string $comment
 * @property integer $user_created
 * @property integer $city_id
 * @property string $terminal_transact_number
 * @property string $terminal_transact_date
 * @property integer $order_id
 *
 * @property Operation[] $operations
 * @property PaymentMethod $paymentMethod
 * @property Account $receiverAcc
 * @property Account $senderAcc
 * @property TransactionType $type
 * @property Order $order
 */
class Transaction extends \yii\db\ActiveRecord
{
    /**
     * Пополнение
     */
    const DEPOSIT_TYPE = 1;
    /**
     * Снятие
     */
    const WITHDRAWAL_TYPE = 2;
    /**
     * Оплата за заказ
     */
    const ORDER_PAYMENT_TYPE = 3;
    /**
     * Оплата за Гутакс
     */
    const GOOTAX_PAYMENT_TYPE = 4;
    /**
     * Обналичивание средств (Не учитывается в БД).
     * Необходим только для выставления соответствующего типа списания.
     */
    const WITHDRAWAL_CASH_OUT_TYPE = 5;

    //Коды ошибок
    const SUCCESS_RESPONSE = 100;
    const NOT_SUPPORTED_RESPONSE = 120;
    const SYSTEM_ERROR_RESPONSE = 140;
    const SYSTEM_LOCAL_ERROR_RESPONSE = 141;
    const NO_MONEY_RESPONSE = 160;
    const NOT_FOUND_RESPONSE = 180;
    const HELD_PAYMENT_RESPONSE = 200;
    const FORBBIDEN_RESPONSE = 403;

    //Методы оплаты
    const BONUS_PAYMENT = 'BONUS';
    const CASH_PAYMENT = 'CASH';
    const CARD_PAYMENT = 'CARD';
    const PERSONAL_ACCOUNT_PAYMENT = 'PERSONAL_ACCOUNT';
    const CORP_BALANCE_PAYMENT = 'CORP_BALANCE';
    const TERMINAL_QIWI_PAYMENT = 'TERMINAL_QIWI';
    const WITHDRAWAL_PAYMENT = 'WITHDRAWAL';
    const WITHDRAWAL_TARIFF_PAYMENT = 'WITHDRAWAL_FOR_TARIFF';
    const WITHDRAWAL_CASH_OUT_PAYMENT = 'WITHDRAWAL_FOR_CASH_OUT';
    const TERMINAL_ELECSNET_PAYMENT = 'TERMINAL_ELECSNET';
    const TERMINAL_MILLION_PAYMENT = 'TERMINAL_MILLION';
    const TERMINAL_PAYNET_PAYMENT = 'TERMINAL_PAYNET';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transaction}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'sender_acc_id', 'receiver_acc_id', 'payment_method', 'user_created', 'order_id'], 'integer'],
            [['receiver_acc_id', 'sum'], 'required'],
            [['sum'], 'number'],
            [['date'], 'safe'],
            [['comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'transaction_id'  => 'Transaction ID',
            'type_id'         => 'Type ID',
            'sender_acc_id'   => 'Sender Acc ID',
            'receiver_acc_id' => 'Receiver Acc ID',
            'sum'             => 'Sum',
            'payment_method'  => 'Payment Method',
            'date'            => 'Date',
            'comment'         => 'Comment',
            'user_created'    => 'Dispatcher',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperations()
    {
        return $this->hasMany(Operation::className(), ['transaction_id' => 'transaction_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentMethod()
    {
        return $this->hasOne(PaymentMethod::className(), ['payment_id' => 'payment_method']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceiverAcc()
    {
        return $this->hasOne(Account::className(), ['account_id' => 'receiver_acc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSenderAcc()
    {
        return $this->hasOne(Account::className(), ['account_id' => 'sender_acc_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(TransactionType::className(), ['type_id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreated()
    {
        return $this->hasOne(\app\modules\tenant\models\User::className(), ['user_id' => 'user_created']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }

    /**
     * Allow to get format transaction time.
     * @return string
     */
    public function getTime($time_offset = 0)
    {
        return app()->formatter->asDateTime($this->date + $time_offset, 'php:d.m.Y ' . t('order', 'on') . ' H:i');
    }

    /**
     * Проведение транзакции
     *
     * @param array $transactionData
     *
     * @return string Код ответа
     * @see taxi-gearman \console\components\billing\Billing
     */
    public static function createTransaction($transactionData)
    {
        try {
            $res = app()->gearman->doHigh(Gearman::BILLING, $transactionData);
        } catch (yii\base\ErrorException $exc) {
            Yii::error($exc->getMessage());
            $res = self::SYSTEM_LOCAL_ERROR_RESPONSE;
        }

        return $res;
    }

    /**
     * Возвращает название типа платежа c учетом локали.
     * @return string
     */
    public function getPaymentTypeName()
    {
        return static::getPaymentMethodName($this->payment_method);
    }

    /**
     * Getting payment method name
     *
     * @param $paymentMethod
     *
     * @return string
     */
    public static function getPaymentMethodName($paymentMethod)
    {
        $payment_map = CacheHelper::getFromCache('payment_type' . __CLASS__, function () {
            $res = (new \yii\db\Query)
                ->from('{{%payment_method}}')
                ->all();

            return ArrayHelper::map($res, 'payment', 'name');
        });

        return t('balance', $payment_map[$paymentMethod]);
    }

    /**
     * Checking accsess for transaction
     * @param integer $accKindId kind_id of tbl_account_kind
     * @return bool
     */
    public static function isAllowed($accKindId)
    {
        if (in_array($accKindId, [Account::CLIENT_KIND, Account::CLIENT_BONUS_KIND, Account::CLIENT_BONUS_UDS_GAME_KIND])) {
            return app()->user->can('clients');
        }

        if ($accKindId == Account::WORKER_KIND) {
            return app()->user->can('drivers');
        }

        if ($accKindId == Account::COMPANY_KIND) {
            return app()->user->can('organization');
        }

        return false;
    }
}
