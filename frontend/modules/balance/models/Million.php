<?php

namespace app\modules\balance\models;

use Yii;
use yii\base\Excaption;

class Million extends \yii\base\Model
{
    const RESPONSE_COMPLETE = 1;
    
    const RESPONSE_FAIL = 0;
    
    public $c = self::RESPONSE_FAIL;
    
    public $m = '';
    
    public $d = [];
    
    public function rules()
    {
        return [
            ['c', 'in', 'range' => [self::RESPONSE_COMPLETE, self::RESPONSE_FAIL]],
            ['c', 'required'],
            ['m', 'string'],
            ['d', 'safe'],
        ];
    }
    
    public function toJson( array $fields = [], array $expand = [])
    {
        if($this->validate()) {
            return json_encode($this->toArray($fields, $expand), JSON_UNESCAPED_UNICODE);
        } else {
            throw new Exception('');
        }
    }
   
}
