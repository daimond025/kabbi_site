<?php

namespace app\modules\balance\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%million_log}}".
 *
 * @property integer $log_id
 * @property string $type
 * @property string $data
 * @property string $apikey
 * @property string $result
 * @property integer $create_time
 * @property integer $callsign
 * @property string $phone
 * @property string $sum
 */
 
class MillionLog extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%million_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['create_time'], 'integer'],
            [['type', 'apikey', 'data', 'result', 'sum'], 'string', 'max' => 255],
            [['callsign'], 'safe'],
            [['phone'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'log_id' => 'Log ID',
            'type' => 'Type',
            'apikey' => 'Apikey',
            'data' => 'Data',
            'result' => 'Result',
            'create_time' => 'Create Time',
            'phone' => 'Phone',
            'callsign' => 'Callsign',
            'sum' => 'Sum',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_time'],
                ],
            ],
        ];
    }
}
