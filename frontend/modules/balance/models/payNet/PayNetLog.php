<?php

namespace frontend\modules\balance\models\payNet;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%paynet_log}}".
 *
 * @property integer $log_id
 * @property string  $type
 * @property string  $username
 * @property integer $serviceId
 * @property integer $callsign
 * @property string  $sum
 * @property string  $data
 * @property string  $result
 * @property integer $create_time
 */
class PayNetLog extends ActiveRecord
{

    const TYPE_GET_INFORMATION = 'get information';
    const TYPE_PERFORM_TRANSACTION = 'perform transaction';
    const TYPE_CHECK_TRANSACTION = 'check transaction';
    const TYPE_CANCEL_TRANSACTION = 'cancel transaction';
    const TYPE_GET_STATEMENT = 'get statement';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%paynet_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['type', 'required'],
            ['type', 'in', 'range' => self::getTypeList()],

            ['username', 'required'],
            ['username', 'string', 'max' => 15],

            ['serviceId', 'integer'],

            ['callsign', 'integer'],

            ['sum', 'string'],

            ['data', 'safe'],

            ['result', 'integer'],

            ['create_time', 'integer'],
        ];
    }


    public function behaviors()
    {
        return [
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_time'],
                ],
            ],
        ];
    }

    public static function getTypeList()
    {
        return [
            self::TYPE_CHECK_TRANSACTION,
            self::TYPE_PERFORM_TRANSACTION,
            self::TYPE_GET_INFORMATION,
            self::TYPE_GET_STATEMENT,
            self::TYPE_CANCEL_TRANSACTION,
        ];
    }
}
