<?php

namespace frontend\modules\balance\models\payNet;

use yii\helpers\ArrayHelper;

abstract class BaseGenericArguments
{
    public $password;
    public $username;

    public function __construct($params = [])
    {
        foreach (get_object_vars($this) as $param => $value) {
            $this->$param = ArrayHelper::getValue($params, $param);
        }
    }
}