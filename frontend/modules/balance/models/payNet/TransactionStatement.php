<?php


namespace frontend\modules\balance\models\payNet;

class TransactionStatement
{
    public $amount;
    public $providerTrnId;
    public $transactionId;
    public $transactionTime;
}