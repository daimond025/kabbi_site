<?php

namespace frontend\modules\balance\models\payNet;

class ChangePasswordArguments extends BaseGenericArguments
{
    public $newPassword;
}