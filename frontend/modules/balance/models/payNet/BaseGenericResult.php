<?php

namespace frontend\modules\balance\models\payNet;

abstract class BaseGenericResult
{
    public $errorMsg;
    public $status;
    public $timeStamp;

    public function __construct()
    {
        $this->status   = 0;
        $now = \DateTime::createFromFormat('U.u', microtime(true));
        $this->timeStamp = $now->format('Y-m-d\TH:i:s.uP');
    }
}