<?php

namespace frontend\modules\balance\models\payNet;


class CancelTransactionResult extends BaseGenericResult
{
    public $transactionState;
}