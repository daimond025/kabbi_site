<?php

namespace frontend\modules\balance\models\payNet;


class GetStatementResult extends GenericResult
{
    public $statements;
    public function __construct()
    {
        parent::__construct();

        $this->parameters = [];
        $this->statements = [];
    }

}