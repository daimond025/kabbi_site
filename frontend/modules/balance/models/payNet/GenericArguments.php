<?php

namespace frontend\modules\balance\models\payNet;

abstract class GenericArguments extends BaseGenericArguments
{
    public $parameters;

    public function getParameter($key)
    {
        $parameters = $this->parameters && !is_array($this->parameters) ? [$this->parameters] : $this->parameters;

        foreach ((array)$parameters as $parameter) {
            if ($parameter->paramKey == $key) {
                return $parameter->paramValue;
            }
        }

        return null;
    }

}