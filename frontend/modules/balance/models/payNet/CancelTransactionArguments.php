<?php

namespace frontend\modules\balance\models\payNet;

class CancelTransactionArguments extends GenericArguments
{
    public $serviceId;
    public $transactionId;
}