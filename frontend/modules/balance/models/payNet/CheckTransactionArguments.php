<?php

namespace frontend\modules\balance\models\payNet;

class CheckTransactionArguments extends GenericArguments
{
    public $serviceId;
    public $transactionId;
    public $transactionTime;
}