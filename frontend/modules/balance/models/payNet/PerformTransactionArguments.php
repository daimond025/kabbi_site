<?php

namespace frontend\modules\balance\models\payNet;

class PerformTransactionArguments extends GenericArguments
{
    public $amount;
    public $serviceId;
    public $transactionId;
    public $transactionTime;

}