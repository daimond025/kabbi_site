<?php

namespace frontend\modules\balance\models\payNet;

class GenericParam
{
    public $paramKey;
    public $paramValue;

    public function __construct($key = null, $value = null)
    {
        $this->paramKey   = $key;
        $this->paramValue = $value;
    }
}