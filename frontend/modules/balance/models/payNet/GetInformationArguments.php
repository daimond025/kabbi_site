<?php

namespace frontend\modules\balance\models\payNet;

class GetInformationArguments extends GenericArguments
{
    public $serviceId;
}