<?php

namespace frontend\modules\balance\models\payNet;

abstract class GenericResult extends BaseGenericResult
{
    public $parameters;

    public function __construct()
    {
        parent::__construct();

        $this->parameters = [];
    }
}