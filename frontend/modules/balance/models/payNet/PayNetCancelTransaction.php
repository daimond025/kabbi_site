<?php

namespace frontend\modules\balance\models\payNet;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%paynet_cancel_transaction}}".
 *
 * @property integer $id
 * @property integer $tenant_id
 * @property integer $status
 * @property string $transaction_id
 * @property integer $create_time
 */
class PayNetCancelTransaction extends ActiveRecord
{

    const STATUS_ACTIVE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%paynet_cancel_transaction}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['tenant_id', 'required'],
            ['tenant_id', 'integer'],

            ['status', 'required'],
            ['status', 'integer', 'min' => 0, 'max' => 99],

            ['transaction_id', 'required'],
            ['transaction_id', 'string', 'max' => 255],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class'      => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_time'],
                ],
            ],
        ];
    }
}
