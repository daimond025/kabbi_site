<?php

namespace frontend\modules\balance\models\payNet;


class CheckTransactionResult extends BaseGenericResult
{
    public $providerTrnId;
    public $transactionState;
    public $transactionStateErrorMsg;
    public $transactionStateErrorStatus;
}