<?php

namespace frontend\modules\balance\models\payNet;

class GetStatementArguments extends GenericArguments
{
    public $serviceId;
    public $dateFrom;
    public $dateTo;
    public $onlyTransactionId;
}