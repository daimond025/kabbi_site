<?php

namespace frontend\modules\balance\models\payNet;

class PerformTransactionResult extends GenericResult
{
    public $providerTrnId;

    public function __construct()
    {
        parent::__construct();

        $this->providerTrnId = 0;
    }
}