<?php

namespace app\modules\balance\models;

use common\helpers\CacheHelper;
use common\modules\billing\models\OperationType;
use frontend\modules\balance\components\helpers\CommentParserHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%operation}}".
 *
 * @property integer       $operation_id
 * @property integer       $type_id
 * @property integer       $account_id
 * @property integer       $transaction_id
 * @property float         $sum
 * @property float         $saldo
 * @property string        $comment
 * @property string        $date
 * @property string        $operation_name
 * @property string        $owner_name
 *
 * @property Account       $account
 * @property Transaction   $transaction
 * @property OperationType $type
 */
class Operation extends \yii\db\ActiveRecord
{
    /**
     * Приход
     */
    const INCOME_TYPE = 1;
    /**
     * Расход
     */
    const EXPENSES_TYPE = 2;
    /**
     * Обналичивание
     */
    const CASH_OUT_TYPE = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%operation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'account_id', 'transaction_id'], 'required'],
            [['type_id', 'account_id', 'transaction_id'], 'integer'],
            [['sum'], 'number'],
            [['date'], 'safe'],
            [['comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'operation_id'   => 'Operation ID',
            'type_id'        => 'Type ID',
            'account_id'     => 'Account ID',
            'transaction_id' => 'Transaction ID',
            'sum'            => 'Sum',
            'saldo'          => 'Saldo',
            'comment'        => 'Comment',
            'date'           => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Account::className(), ['account_id' => 'account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(Transaction::className(), ['transaction_id' => 'transaction_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(OperationType::className(), ['type_id' => 'type_id']);
    }

    /**
     * Getting operations by last week.
     *
     * @param integer $account_id Balance id
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getByWeekForAccount($account_id)
    {
        return self::find()
            ->where(['account_id' => $account_id])
            ->with([
                'transaction'             => function (ActiveQuery $query) {
                    $query->select([
                        'transaction_id',
                        'user_created',
                        'payment_method',
                        'terminal_transact_number',
                        'type_id',
                        'order_id',
                    ]);
                },
                'transaction.userCreated' => function (ActiveQuery $query) {
                    $query->select(['user_id', 'name', 'last_name', 'second_name', 'position_id']);
                },
            ])
            ->andWhere([
                'between',
                'date',
                date('Y-m-d H:i:s', mktime(0, 0, 0, date('n'), date("j") - 7)),
                date('Y-m-d H:i:s'),
            ])
            ->all();
    }

    /**
     * Getting operations by period.
     *
     * @param int    $account_id Balance id
     * @param string $first_date First date of period
     * @param string $second_date Second date of period
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getByPeriodForAccount($account_id, $first_date, $second_date)
    {
        $filterDate = date('d.m.Y', strtotime($first_date));
        $arFirstDay = explode('.', $filterDate);

        $filterDate = date('d.m.Y', strtotime($second_date));
        $arSecondDay = explode('.', $filterDate);

        return self::find()
            ->where(['account_id' => $account_id])
            ->with([
                'transaction'             => function (ActiveQuery $query) {
                    $query->select([
                        'transaction_id',
                        'user_created',
                        'payment_method',
                        'terminal_transact_number',
                        'type_id',
                        'order_id',
                    ]);
                },
                'transaction.userCreated' => function (ActiveQuery $query) {
                    $query->select(['user_id', 'name', 'last_name', 'second_name', 'position_id']);
                },
            ])
            ->andWhere([
                'between',
                'date',
                date('Y-m-d H:i:s', mktime(0, 0, 0, $arFirstDay[1], $arFirstDay[0], $arFirstDay[2])),
                date('Y-m-d H:i:s', mktime(23, 59, 59, $arSecondDay[1], $arSecondDay[0], $arSecondDay[2])),
            ])
            ->all();
    }

    /**
     * Возвращает название операции c учетом локали.
     * @return string
     */
    public function getOperationTypeName()
    {
        $typeMap = app()->cache->getOrSet('operation_type' . __CLASS__, function () {
            $res = (new \yii\db\Query)
                ->from('{{%operation_type}}')
                ->all();

            return ArrayHelper::map($res, 'type_id', 'name');
        });

        return t('balance', getValue($typeMap[$this->type_id]));
    }

    /**
     * Определяем сальдо на начало операции.
     * @return float
     */
    public function getOpeningBalance()
    {
        if ($this->isIncome()) {
            $saldo = $this->saldo - $this->sum;
        } else {
            $saldo = $this->saldo + $this->sum;
        }

        return $saldo;
    }

    /**
     * @return bool
     */
    public function isExpense()
    {
        return \in_array($this->type_id, [self::EXPENSES_TYPE, self::CASH_OUT_TYPE]);
    }

    /**
     * @return bool
     */
    public function isIncome()
    {
        return $this->type_id === self::INCOME_TYPE;
    }
}
