<?php

namespace paymentGate;

use common\modules\tenant\models\DefaultSettings;
use common\modules\tenant\models\TenantSetting;
use frontend\modules\tenant\components\repositories\PaygateProfileRepository;
use frontend\modules\tenant\models\entities\PaygateProfile;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use paymentGate\exceptions\UnknownProfileTypeException;
use paymentGate\models\Alfabank;
use paymentGate\models\BaseProfile;
use paymentGate\models\BePaid;
use paymentGate\models\Disabled;
use paymentGate\models\Kkb;
use paymentGate\models\Paycom;
use paymentGate\models\Payture;
use paymentGate\models\Sberbank;
use paymentGate\models\Stripe;
use paymentGate\models\Vtb;
use paymentGate\models\Yandex;
use yii\base\Object;


/**
 * Class ProfileService
 * @package paymentGate
 */
class ProfileService extends Object
{
    /**
     * @var string
     */
    public $baseUrl;

    /**
     * @var float
     */
    public $timeout;

    /**
     * @var float
     */
    public $connectionTimeout;

    /**
     * @var Client
     */
    private $httpClient;

    /* @var $paygateProfileRepository PaygateProfileRepository */
    protected $paygateProfileRepository;

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();

        $this->paygateProfileRepository = \Yii::$container->get(PaygateProfileRepository::class);

        $this->httpClient = \Yii::createObject(Client::class, [
            [
                'base_uri'          => $this->baseUrl,
                'timeout'           => $this->timeout,
                'connectionTimeout' => $this->connectionTimeout,
            ],
        ]);
    }

    /**
     * Execute request to getting profile types
     * @return array
     */
    private function executeGetProfileTypesRequest()
    {
        $response = $this->httpClient->get('v0/profiles');

        return json_decode($response->getBody(), true);
    }

    /**
     * Execute request to getting profile of payment gate
     *
     * @param $paymentName
     *
     * @return array
     */
    private function executeGetProfileRequest($paymentName)
    {
        $response = $this->httpClient->get("v0/profiles/{$paymentName}");

        return json_decode($response->getBody(), true);
    }

    /**
     * Execute request to create profile of payment gate
     *
     * @param BaseProfile $profile
     */
    private function executeCreateProfileRequest(BaseProfile $profile)
    {
        $this->httpClient->post('v0/profiles', [
            'json' => $profile->toArray(),
        ]);
    }

    /**
     * Execute request to update profile of payment gate
     *
     * @param string $paymentName
     * @param BaseProfile $profile
     */
    private function executeUpdateProfileRequest($paymentName, BaseProfile $profile)
    {
        $this->httpClient->put("v0/profiles/{$paymentName}", [
            'json' => $profile->toArray(),
        ]);
    }

    /**
     * Execute request to delete profile of payment gate
     *
     * @param string $paymentName
     */
    private function executeDeleteProfileRequest($paymentName)
    {
        $this->httpClient->delete("v0/profiles/{$paymentName}");
    }

    /**
     * @param string $paymentName
     * @param int $shopId
     * @param int $fromTime
     * @param int $toTime
     *
     * @return array
     */
    private function executeGetYandexFailedPayments($paymentName, $shopId, $fromTime, $toTime)
    {
        $response = $this->httpClient->get('v0/yandex/order/failed', [
            'query' => [
                'paymentName' => $paymentName,
                'shopId'      => $shopId,
                'fromTime'    => $fromTime,
                'toTime'      => $toTime,
            ],
        ]);

        return json_decode($response->getBody(), true);
    }

    /**
     * Getting profile types
     * @return array
     * @throws \GuzzleHttp\Exception\ClientException
     */
    public function getProfileTypes()
    {
        $result = [];
        try {
            $result = $this->executeGetProfileTypesRequest();
        } catch (ClientException $ex) {
            if ($ex->getCode() !== 404) {
                throw $ex;
            }
        }

        return $result;
    }

    /**
     * Getting profile model
     *
     * @param int $typeId
     *
     * @return BaseProfile
     * @throws UnknownProfileTypeException
     */
    public function getProfileModel($typeId)
    {
        switch ($typeId) {
            case Disabled::TYPE_ID:
                return new Disabled();
            case Alfabank::TYPE_ID:
                return new Alfabank();
            case Sberbank::TYPE_ID:
                return new Sberbank();
            case Vtb::TYPE_ID:
                return new Vtb();
            case Yandex::TYPE_ID:
                return new Yandex();
            case Stripe::TYPE_ID:
                return new Stripe();
            case Paycom::TYPE_ID:
                return new Paycom();
            case Payture::TYPE_ID:
                return new Payture();
            case Kkb::TYPE_ID:
                return new Kkb();
            case BePaid::TYPE_ID:
                return new BePaid();
            default:
                throw new UnknownProfileTypeException("Unknown profile type: \"{$typeId}\"");
        }
    }

    /**
     * Getting profile of payment gate
     *
     * @param string $paymentName
     *
     * @return BaseProfile
     * @throws \GuzzleHttp\Exception\ClientException
     * @throws UnknownProfileTypeException
     */
    public function getProfile($paymentName)
    {
        $data = null;
        try {
            $data = $this->executeGetProfileRequest($paymentName);
        } catch (ClientException $ex) {
            if ($ex->getCode() !== 404) {
                throw $ex;
            }
        }

        $typeId = isset($data['typeId']) ? $data['typeId'] : Disabled::TYPE_ID;
        $model = $this->getProfileModel($typeId);
        $model->paymentName = $paymentName;
        $model->load($data, '');

        return $model;
    }

    /**
     * Create profile of payment gate
     *
     * @param BaseProfile $profile
     */
    public function createProfile(BaseProfile $profile, $cityId)
    {
        $profile->paymentName = $this->savePygateProfile($cityId);

        $this->executeCreateProfileRequest($profile);

    }

    /**
     * Update profile of payment gate
     *
     * @param string $paymentName
     * @param BaseProfile $profile
     */
    public function updateProfile($paymentName, BaseProfile $profile)
    {
        $this->executeUpdateProfileRequest($paymentName, $profile);
    }

    /**
     * Delete profile of payment gate
     *
     * @param string $paymentName
     *
     * @throws \GuzzleHttp\Exception\ClientException
     */
    public function deleteProfile($paymentName)
    {
        try {
            $this->executeDeleteProfileRequest($paymentName);
        } catch (ClientException $ex) {
            if ($ex->getCode() !== 404) {
                throw $ex;
            }
        }

        PaygateProfile::deleteAll(['profile' => $paymentName]);
    }

    /**
     * @param string $paymentName
     * @param int $shopId
     * @param int $fromTime
     * @param int $toTime
     *
     * @return array
     */
    public function getYandexFailedPayments($paymentName, $shopId, $fromTime, $toTime)
    {
        $payments = null;
        try {
            $payments = $this->executeGetYandexFailedPayments($paymentName, $shopId, $fromTime, $toTime);
        } catch (\Exception $ignore) {
        }

        return empty($payments) ? [] : $payments;
    }


    public function getPaymentName($tenantId, $cityId)
    {
        $paygateProfile = $this->paygateProfileRepository->getPaygateProfile($tenantId, $cityId);

        return $paygateProfile ? $paygateProfile->profile : null;
    }

    public function getCurrentCityId($city_id, $post)
    {
        $cities = user()->getUserCityList();
        $currentCityId = empty($city_id) || !array_key_exists($city_id, $cities) ? key($cities) : $city_id;

        if ($post) {
            $currentCityId = $post['Profile']['city_id'];
        }

        return $currentCityId;

    }

    public function loadAdvanceSettings($tenantId, $cityId, $profile)
    {
        if ($profile instanceof Stripe) {
            $useStripeConnect = (string)TenantSetting::getSettingValue(
                    $tenantId, DefaultSettings::SETTING_USE_STRIPE_CONNECT, $cityId) === '1';
            $profile->useStripeConnect = $useStripeConnect;
        }
    }

    public function saveAdvanceSettings($tenantId, $cityId, $profile)
    {
        if ($profile instanceof Stripe) {
            TenantSetting::updateSettingValue(
                DefaultSettings::SETTING_USE_STRIPE_CONNECT, $tenantId, $cityId, null, $profile->useStripeConnect);
        }
    }

    public function isCreateProfile($paymentName, $typeIdFromPost)
    {
        return !$paymentName && $typeIdFromPost !== Disabled::TYPE_ID;
    }

    public function isDeleteProfile($paymentName, $typeIdFromPost)
    {
        return $paymentName && $typeIdFromPost == Disabled::TYPE_ID;
    }

    public function isUpdateCurrentProfile($paymentName, $typeIdFromPost, BaseProfile $profile)
    {
        return $paymentName && $profile->typeId == $typeIdFromPost;
    }

    public function isUpdateTypeProfile($paymentName, $typeIdFromPost, BaseProfile $profile)
    {
        return $paymentName && $profile->typeId != $typeIdFromPost;
    }

    protected function savePygateProfile($cityId)
    {
        $pygateProfile = new PaygateProfile([
            'city_id'   => $cityId,
            'tenant_id' => user()->tenant_id
        ]);
        $pygateProfile->save();

        return $pygateProfile->profile;

    }
}