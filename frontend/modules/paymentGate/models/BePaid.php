<?php

namespace paymentGate\models;

use yii\helpers\ArrayHelper;

/**
 * Class BePaid
 *
 * @package paymentGate\models
 */
class BePaid extends BaseProfile
{
    const TYPE_ID = 10;

    /**
     * @var int
     */
    public $shopId;

    /**
     * @var string
     */
    public $shopKey;

    /**
     * @var int
     */
    public $cardBindingSum;

    /**
     * @var int
     */
    public $isDebug;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->typeId = self::TYPE_ID;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['shopId', 'shopKey'], 'required'],
            [['shopId', 'cardBindingSum', 'isDebug'], 'integer'],
            [['shopKey'], 'string', 'max' => 255],
        ]);
    }
}