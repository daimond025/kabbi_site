<?php

namespace paymentGate\models;

use yii\helpers\ArrayHelper;

/**
 * Class Stripe
 * @package paymentGate
 */
class Stripe extends BaseProfile
{
    const TYPE_ID = 5;

    /**
     * @var string
     */
    public $publicKey;

    /**
     * @var string
     */
    public $privateKey;

    /**
     * @var string
     */
    public $useStripeConnect;

    /**
     * @var string
     */
    public $clientId;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->typeId = self::TYPE_ID;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['publicKey', 'privateKey', 'useStripeConnect'], 'required'],
            [['publicKey', 'privateKey', 'clientId'], 'string', 'max' => 255],
        ]);
    }

}