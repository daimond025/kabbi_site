<?php

namespace paymentGate\models;

use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * Class Kkb
 * @package paymentGate\models
 */
class Kkb extends BaseProfile
{
    const TYPE_ID = 8;

    /**
     * @var string
     */
    public $merchantId;

    /**
     * @var string
     */
    public $merchantName;

    /**
     * @var string
     */
    public $certificateId;

    /**
     * @var string
     */
    public $privateCertificate;

    /**
     * @var string
     */
    public $privateCertificatePass;

    /**
     * @var UploadedFile
     */
    public $privateCertificateFile;

    /**
     * @var string
     */
    public $publicCertificate;

    /**
     * @var UploadedFile
     */
    public $publicCertificateFile;

    /**
     * @var
     */
    public $isDebug;

    /**
     * @var int
     */
    public $cardBindingSum;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->typeId = self::TYPE_ID;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [
                [
                    'merchantId',
                    'merchantName',
                    'certificateId',
                    'privateCertificate',
                    'privateCertificatePass',
                    'publicCertificate',
                    'isDebug',
                    'cardBindingSum',
                ],
                'required',
            ],
            [['merchantId', 'certificateId'], 'string', 'max' => 32],
            [['merchantName', 'privateCertificatePass'], 'string', 'max' => 32],
        ]);
    }

    /**
     * Fill content fields
     */
    public function fillContentFields()
    {
        $privateCertificateFile = UploadedFile::getInstance($this, 'privateCertificateFile');
        if ($privateCertificateFile !== null) {
            $this->privateCertificate = file_get_contents($privateCertificateFile->tempName);
        }

        $publicCertificateFile = UploadedFile::getInstance($this, 'publicCertificateFile');
        if ($publicCertificateFile !== null) {
            $this->publicCertificate = file_get_contents($publicCertificateFile->tempName);
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        $this->fillContentFields();

        return parent::beforeValidate();
    }

}