<?php

namespace paymentGate\models;

use yii\helpers\ArrayHelper;

/**
 * Class Paycom
 * @package paymentGate\models
 */
class Paycom extends BaseProfile
{
    const TYPE_ID = 7;

    /**
     * @var string
     */
    public $apiId;

    /**
     * @var string
     */
    public $password;

    /**
     * @var
     */
    public $isDebug;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->typeId = self::TYPE_ID;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['apiId', 'password', 'isDebug'], 'required'],
            [['apiId', 'password'], 'string', 'max' => 255],
        ]);
    }

}