<?php

namespace paymentGate\models;

use yii\helpers\ArrayHelper;

/**
 * Class Vtb
 * @package paymentGate
 */
class Vtb extends BaseProfile
{
    const TYPE_ID = 3;

    /**
     * @var string
     */
    public $url;

    /**
     * @var string
     */
    public $username;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $returnUrl;

    /**
     * @var string
     */
    public $failUrl;

    /**
     * @var int
     */
    public $cardBindingSum;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->typeId = self::TYPE_ID;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['username', 'password', 'url'], 'required'],
            [['username', 'password'], 'string', 'max' => 32],
            [['url', 'returnUrl', 'failUrl'], 'string', 'max' => 255],
            [['cardBindingSum'], 'integer'],
        ]);
    }

}