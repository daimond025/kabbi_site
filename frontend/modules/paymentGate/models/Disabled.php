<?php

namespace paymentGate\models;

/**
 * Class Disabled
 * @package paymentGate\models
 */
class Disabled extends BaseProfile
{
    const TYPE_ID = 0;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->typeId = self::TYPE_ID;
    }
}