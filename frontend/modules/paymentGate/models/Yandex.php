<?php

namespace paymentGate\models;

use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * Class Yandex
 * @package paymentGate
 */
class Yandex extends BaseProfile
{
    const TYPE_ID = 4;

    /**
     * @var int
     */
    public $shopId;

    /**
     * @var int
     */
    public $scid;

    /**
     * @var int
     */
    public $shopArticleId;

    /**
     * @var string
     */
    public $password;

    /**
     * @var int
     */
    public $isDebug;

    /**
     * @var string
     */
    public $certificateContent;

    /**
     * @var UploadedFile
     */
    public $certificateFile;

    /**
     * @var string
     */
    public $keyContent;

    /**
     * @var UploadedFile
     */
    public $keyFile;

    /**
     * @var string
     */
    public $certificatePassword;

    /**
     * @var int
     */
    public $secureDeal;

    /**
     * @var int
     */
    public $secureDealShopId;

    /**
     * @var int
     */
    public $secureDealScid;

    /**
     * @var int
     */
    public $secureDealShopArticleId;

    /**
     * @var int
     */
    public $cardBindingSum;

    /**
     * @var int
     */
    public $secureDealPaymentShopId;

    /**
     * @var int
     */
    public $secureDealPaymentScid;

    /**
     * @var int
     */
    public $secureDealPaymentShopArticleId;

    /**
     * @var int
     */
    public $useReceipt;

    /**
     * @var string
     */
    public $product;

    /**
     * @var int
     */
    public $vat;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->typeId = self::TYPE_ID;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['shopId', 'scid', 'certificateContent', 'keyContent'], 'required'],
            [['shopId', 'scid', 'shopArticleId', 'isDebug', 'cardBindingSum'], 'integer'],
            [['password', 'certificatePassword'], 'string', 'max' => 255],

            [
                [
                    'secureDeal',
                    'secureDealShopId',
                    'secureDealScid',
                    'secureDealShopArticleId',
                    'secureDealPaymentShopId',
                    'secureDealPaymentScid',
                    'secureDealPaymentShopArticleId',
                ],
                'integer',
            ],
            ['secureDealShopId', 'required', 'when' => [$this, 'requireSecureDeal']],
            ['secureDealScid', 'required', 'when' => [$this, 'requireSecureDeal']],
            ['secureDealShopArticleId', 'required', 'when' => [$this, 'requireSecureDeal']],
            ['secureDealShopArticleId', 'required', 'when' => [$this, 'requireSecureDeal']],
            ['secureDealPaymentShopId', 'required', 'when' => [$this, 'requireSecureDeal']],
            ['secureDealPaymentScid', 'required', 'when' => [$this, 'requireSecureDeal']],
            ['secureDealPaymentShopArticleId', 'required', 'when' => [$this, 'requireSecureDeal']],
            [
                [
                    'secureDealShopId',
                    'secureDealScid',
                    'secureDealShopArticleId',
                    'secureDealPaymentShopId',
                    'secureDealPaymentScid',
                    'secureDealPaymentShopArticleId',
                ],
                'filter',
                'filter' => [$this, 'filterSecureDealFields'],
            ],

            ['useReceipt', 'integer'],
            ['product', 'required', 'when' => [$this, 'requireReceipt']],
            ['vat', 'required', 'when' => [$this, 'requireReceipt']],
            ['vat', 'integer', 'min' => 1, 'max' => 6],
            [['product', 'vat'], 'filter', 'filter' => [$this, 'filterReceiptFields']],

            [
                ['certificateContent', 'keyContent'],
                function ($attribute, $params) {
                    if (!empty($this->certificateContent) || !empty($this->keyContent)) {
                        if (!openssl_x509_check_private_key($this->certificateContent, $this->keyContent)) {
                            $this->addError('keyFile', t('bank-card', 'Incorrect key file'));
                        }
                    }
                },
            ],
            [
                'certificateContent',
                function ($attribute, $params) {
                    if (!empty($this->certificateContent)) {
                        $info = openssl_x509_parse($this->certificateContent);

                        if (empty($info)) {
                            $this->addError('certificateFile', t('bank-card', 'Incorrect certificate file'));
                        } elseif ($info['validTo_time_t'] < time()) {
                            $this->addError('certificateFile',
                                t('bank-card', 'This certificate is not valid for the current date'));
                        }
                    }
                },
            ],
        ]);
    }

    public static function getVatMap()
    {
        return [
            1 => 'без НДС',
            2 => 'НДС по ставке 0%',
            3 => 'НДС чека по ставке 10%',
            4 => 'НДС чека по ставке 18%',
            5 => 'НДС чека по расчетной ставке 10/110',
            6 => 'НДС чека по расчетной ставке 18/118',
        ];
    }

    public function filterSecureDealFields($value)
    {
        return !empty($this->secureDeal) ? $value : null;
    }

    public function requireSecureDeal($model)
    {
        return !empty($model->secureDeal);
    }

    public function filterReceiptFields($value)
    {
        return !empty($this->useReceipt) ? $value : null;
    }

    public function requireReceipt($model)
    {
        return !empty($model->useReceipt);
    }

    /**
     * Fill content fields
     */
    public function fillContentFields()
    {
        $certificateFile = UploadedFile::getInstance($this, 'certificateFile');
        if (!empty($certificateFile)) {
            $this->certificateContent = file_get_contents($certificateFile->tempName);
        }

        $keyFile = UploadedFile::getInstance($this, 'keyFile');
        if (!empty($keyFile)) {
            $this->keyContent = file_get_contents($keyFile->tempName);
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        $this->fillContentFields();

        return parent::beforeValidate();
    }
}