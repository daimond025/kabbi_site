<?php

namespace paymentGate\models;

use yii\helpers\ArrayHelper;

/**
 * Class Payture
 * @package paymentGate\models
 */
class Payture extends BaseProfile
{
    const TYPE_ID = 9;

    /**
     * @var string
     */
    public $key3dsAdd;

    /**
     * @var string
     */
    public $keyPay;

    /**
     * @var string
     */
    public $password;

    /**
     * @var int
     */
    public $isDebug;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->typeId = self::TYPE_ID;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['key3dsAdd', 'keyPay', 'password'], 'required'],
            [['isDebug'], 'integer'],
            [['key3dsAdd', 'keyPay', 'password'], 'string', 'max' => 255],
        ]);
    }
}