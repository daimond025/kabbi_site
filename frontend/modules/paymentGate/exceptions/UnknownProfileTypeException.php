<?php

namespace paymentGate\exceptions;

use yii\base\Exception;

/**
 * Class UnknownProfileTypeException
 * @package paymentGate\exceptions
 */
class UnknownProfileTypeException extends Exception
{

}