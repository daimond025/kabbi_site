<?php

namespace app\modules\reports\controllers;

use app\modules\reports\models\orders\factories\OrderReportDataProvider;
use app\modules\reports\models\orders\factories\OrderReportExporterByReportType;
use app\modules\reports\models\orders\OrderSearch;
use app\modules\reports\models\orders\StatisticSearch;
use app\modules\tenant\models\User;
use frontend\components\behavior\CityListBehavior;
use frontend\modules\companies\components\repositories\TenantCompanyRepository;
use frontend\modules\companies\models\TenantCompany;
use frontend\modules\employee\components\position\PositionService;
use frontend\modules\employee\models\worker\Worker;
use frontend\modules\tenant\models\Currency;
use Yii;
use yii\base\Module;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\AccessControl;
use app\modules\client\models\OrderClientCompanySearch;
use app\modules\client\models\OrderClientSearch;

/**
 * Class OrderController
 * @package app\modules\reports\controllers
 * @mixin CityListBehavior
 */
class OrderController extends Controller
{
    private $positionService;

    public function behaviors()
    {
        return [
            'access'   => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['read_reports'],
                    ],
                ],
            ],
            'cityList' => [
                'class' => CityListBehavior::className(),
            ],
        ];
    }

    public function __construct($id, Module $module, PositionService $positionService, array $config = [])
    {
        $this->positionService = $positionService;
        $this->positionService->setTenantId(user()->tenant_id);

        parent::__construct($id, $module, $config);
    }

    private function getPositionMap($cityId = null)
    {
        $positions = $cityId === null
            ? $this->positionService->getAllPositions()
            : $this->positionService->getPositionsByCity($cityId);

        return empty($positions) ? [] : ArrayHelper::map($positions, 'position_id', 'name');
    }

    public function actionIndex()
    {
        $cityList = $this->getUserCityList();
        $positionMap = $this->getPositionMap();
        $tenantCompanyList = TenantCompanyRepository::selfCreate()->getForForm();

        $searchModel = new StatisticSearch([
            'access_city_list'     => array_keys($cityList),
            'access_position_list' => array_keys($positionMap),
        ]);

        $searchModel->load(Yii::$app->request->queryParams);

        $statistics = $searchModel->search();

        return $this->render(
            'index',
            compact('searchModel', 'cityList', 'statistics', 'positionMap', 'tenantCompanyList')
        );
    }

    public function actionList()
    {
        $cityList = $this->getUserCityList();
        $positionMap = $this->getPositionMap();

        $searchModel = $this->getOrderSearch($cityList, $positionMap);
        $dataProvider = $searchModel->search();

        return $this->renderAjax('_grid_orders', compact('searchModel', 'dataProvider', 'cityList', 'positionMap'));
    }

    /**
     * Dump orders
     * @param $searchModel
     * @param $exportType
     * @param $dataProvider
     * @param $reportType
     */
    private function dumpOrders($searchModel, $exportType, $dataProvider, $reportType)
    {

        $query = $dataProvider->query->limit(3000);

        $filenameExcel = 'report.xls';
        $filenamePdf = 'report.pdf';

        $initData = compact(
            'searchModel',
            'reportType',
            'query',
            'exportType',
            'filenameExcel',
            'filenamePdf',
            'dataProvider'
        );

        $data = (new OrderReportDataProvider($initData))->getData();


        if (isset($data->content)) {
            $data->content = $this->renderPartial('dump_orders', [
                'orders'            => $data->content,
                'currencySymbolMap' => Currency::getSymbolMapIndexedById(),
            ]);
        }


        if (is_callable([$data, 'setExporter'])) {
            $data->setExporter()->export();
        }
    }

    /**
     * Set order_id to searchModel
     * @param $order_id
     * @param $searchModel
     */
    private function setOrderId($order_id, $searchModel)
    {
        if (isset($order_id)) {
            $searchModel->order_id = $order_id;
        }
    }

    /**
     * Dump orders
     * @param $exportType
     * @param null $order_id
     */
    public function actionDumpOrders($exportType, $order_id = null)
    {

        $cityList = $this->getUserCityList();
        $positionMap = $this->getPositionMap();



        $searchModel = $this->getOrderSearch($cityList, $positionMap);

        $this->setOrderId($order_id, $searchModel);

        $dataProvider = $searchModel->search(false, true);

        $this->dumpOrders(
            $searchModel,
            $exportType,
            $dataProvider,
            OrderReportExporterByReportType::REPORT_TYPE_STATISTIC
        );
    }

    private function dumpOrdersKabbi($searchModel, $exportType, $dataProvider, $reportType)
    {

        $query = $dataProvider->query->all();

        $filenameExcel = 'report.xls';
        $filenamePdf = 'report.pdf';

        $initData = compact(
            'searchModel',
            'reportType',
            'query',
            'exportType',
            'filenameExcel',
            'filenamePdf',
            'dataProvider'
        );

        $data = (new OrderReportDataProvider($initData))->getData();

        // доп поля - (только первый )
        if( (!is_array( $searchModel->tenantCompanyIds)) && !is_array( $searchModel->worker_id) ){
            app()->session->setFlash('error',  t('tenant_company', 'Select company or worker'));
            return $this->redirect(['/reports/order/index#order_list']);
        }


        if(count($query) == 0){
            app()->session->setFlash('error',  t('order', 'No data for display'));
            return $this->redirect(['/reports/order/index#order_list']);
        }

        // select company
        $company_id = null;
        if(!is_array( $searchModel->tenantCompanyIds) &&  is_array( $searchModel->worker_id)){
            $worker_id = current($searchModel->worker_id);
            $worker = Worker::find()->where(['worker_id' => $worker_id])->one();
            if(!$worker->tenant_company_id){
                app()->session->setFlash('error',  t('tenant_company', 'Worker has not company'));
                return $this->redirect(['/reports/order/index#order_list']);
            }

            $company_id = $worker->tenant_company_id;

        }
        else if(is_array( $searchModel->tenantCompanyIds)){
            $company_id = current($searchModel->tenantCompanyIds);
        }

        if(is_null($company_id) || empty($company_id)){
            app()->session->setFlash('error',  t('tenant_company', 'No company'));
            return $this->redirect(['/reports/order/index#order_list']);
        }

        $company_report = TenantCompany::find()->where(['tenant_company_id' => $company_id])->one();
        $company_report->updateIdReport($company_report);


        $company = TenantCompany::find()->where(['tenant_company_id' => $company_id])->asArray()->one();
        $User = User::find()->where(['user_id' =>$company['user_contact']])->asArray()->one();


        $data->content = $this->renderPartial('_grid_orders_kabbi_new.php', [
            'orders'            => $data->content,
            'currencySymbolMap' => Currency::getSymbolMapIndexedById(),
          /* 'info' => $searchModel->getDetailCompanyWork(),*/
           'company' => $company,
           'user' => $User,
            'fromDate' =>$searchModel->first_date,
            'toDate' =>$searchModel->second_date,
            'idReport' => $company_report->report_id
        ]);


        if (is_callable([$data, 'setExporter'])) {
            $data->setExporter()->export();
        }
    }
    public function actionKabbiOrdersPdf()
    {
        $cityList = $this->getUserCityList();
        $positionMap = $this->getPositionMap();

        $searchModel = $this->getOrderSearch($cityList, $positionMap);

        $dataProvider = $searchModel->search(false, true);

        $this->dumpOrdersKabbi(
            $searchModel,
            'pdf',
            $dataProvider,
            OrderReportExporterByReportType::REPORT_TYPE_STATISTIC
        );

    }

    /**
     * @param $exportType
     * @param $company_id
     */
    public function actionDumpCompanyOrders($exportType, $company_id, $order_id = null)
    {
        $searchModel = new OrderClientCompanySearch();
        $searchModel->type = 'today';
        $searchModel->company_id = $company_id;

        $this->setOrderId($order_id, $searchModel);

        $dataProvider = $searchModel->search(app()->request->queryParams);

        $this->dumpOrders(
            $searchModel,
            $exportType,
            $dataProvider,
            OrderReportExporterByReportType::REPORT_TYPE_COMPANY
        );
    }

    /**
     * @param $exportType
     * @param $client_id
     * @param $order_id
     * @param $first_date
     * @param $second_date
     */
    public function actionDumpClientOrders($exportType, $client_id, $order_id = null, $first_date = null, $second_date = null) {

        $searchModel = new OrderClientSearch();

        if (!isset($first_date, $second_date)) {
            $searchModel->type = 'limit';
        } else {
            $searchModel->type = 'period';
            $searchModel->first_date = $first_date;
            $searchModel->second_date = $second_date;
        }
        $searchModel->client_id = $client_id;

        $this->setOrderId($order_id, $searchModel);

        $dataProvider = $searchModel->search(app()->request->queryParams);

        $this->dumpOrders(
            $searchModel,
            $exportType,
            $dataProvider,
            OrderReportExporterByReportType::REPORT_TYPE_CLIENT
        );
    }

    /**
     * @param array $cityList
     * @param array $positionMap
     *
     * @return OrderSearch
     */
    private function getOrderSearch(array $cityList, array $positionMap)
    {
        $searchModel = new OrderSearch([
            'access_city_list'     => array_keys($cityList),
            'access_position_list' => array_keys($positionMap),
            'tenant_id'            => user()->tenant_id,
        ]);

        $queryParams = Yii::$app->request->queryParams;

        if (app()->user->can(User::ROLE_STAFF_COMPANY)) {
            $queryParams['OrderSearch']['tenant_company_id'] = user()->tenant_company_id;
        }
        $searchModel->load($queryParams);

        return $searchModel;
    }
}
