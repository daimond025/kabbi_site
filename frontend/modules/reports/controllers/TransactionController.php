<?php

namespace app\modules\reports\controllers;

use app\modules\balance\models\Account;
use app\modules\balance\models\Operation;
use app\modules\client\models\Client;
use app\modules\client\models\ClientSearch;
use app\modules\client\models\CompanySearch;
use app\modules\reports\helpers\TransactionReportHelper;
use app\modules\reports\models\transaction\BonusReport;
use app\modules\reports\models\transaction\InnerOperationSearch;
use app\modules\reports\models\transaction\TransactionReport;
use app\modules\tenant\models\User;
use frontend\components\behavior\CityListBehavior;
use frontend\modules\employee\models\worker\Worker;
use frontend\modules\employee\models\worker\WorkerSearch;
use frontend\modules\tenant\models\Currency;
use frontend\widgets\ExcelExport;
use Yii;
use yii\data\Pagination;
use yii\db\ActiveQuery;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class TransactionController
 * @package app\modules\reports\controllers
 *
 * @mixin CityListBehavior
 */
class TransactionController extends Controller
{
    public function behaviors()
    {
        return [
            'access'   => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => [User::ROLE_STAFF_COMPANY],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['read_reports'],
                    ],
                ],
            ],
            'cityList' => [
                'class' => CityListBehavior::className(),
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index', [
            'userCities'  => $this->getUserCityList(),
            'entityTypes' => [
                Account::WORKER_KIND  => t('employee', 'Worker'),
                Account::CLIENT_KIND  => t('transaction', 'Clients'),
                Account::COMPANY_KIND => t('transaction', 'Organizations'),
            ],
        ]);
    }

    /**
     * Search report data on inner accounts.
     * @return bool|string
     * @throws \Throwable
     */
    public function actionInnerReportSearch()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $arPeriod = $this->getSearchPeriod(post('filter'), post('first_date'), post('second_date'));
        $accKindId = post('entity') === 'all' ? null : post('entity_type');
        $cities = post('city_id', $this->getUserCityIds());

        $innerOperationSearch = (new InnerOperationSearch([
            'firstDate'      => $arPeriod[0],
            'lastDate'       => $arPeriod[1],
            'ownerId'        => post('entity_id'),
            'accKindId'      => $accKindId,
            'cityId'         => $cities,
            'accessCityList' => $this->getUserCityIds(),
            'operationType'  => post('operation_type'),
            'carClass'       => post('car_class'),
        ]));
        $query = $innerOperationSearch->search();

        $isLastFilterDateToday = $this->isLastFilterDateToday($arPeriod[1]);

        $pages = $this->createPaginator($query, $isLastFilterDateToday);
        $pages->route = Url::toRoute('/reports/transaction/report-paginate');

        $operations = $this->getPaginateOperations($pages, $query, $isLastFilterDateToday);

        if (!empty($operations)) {
            $openPeriodSaldo = $this->isReportForAllEntities(post())
                ? TransactionReport::getSystemOpenPeriodSaldo($arPeriod)
                : $this->getOperationOpenBalanceIndexedByCurrencySymbol($operations[0]);
        }

        $balances = $innerOperationSearch->getBalances();
        $totalBalances = TransactionReportHelper::formatTotalBalances($balances);

        return $this->renderAjax(
            post('view'),
            compact(
                'operations',
                'time_offset',
                'openPeriodSaldo',
                'report_type',
                'pages',
                'totalBalances'
            )
        );
    }

    /**
     * @param string $lastDate
     * @return bool
     */
    private function isLastFilterDateToday($lastDate)
    {
        return \date('Y-m-d', \strtotime($lastDate)) === \date('Y-m-d');
    }

    /**
     * @param ActiveQuery $query
     * @param bool $isLastFilterDateToday
     * @return Pagination
     * @throws \Throwable
     */
    private function createPaginator(ActiveQuery $query, $isLastFilterDateToday)
    {
        $countQuery = clone $query;

        $getOperationsCount = function () use ($countQuery) {
            return $countQuery->count();
        };

        return new Pagination([
            'totalCount' => $isLastFilterDateToday ? $getOperationsCount() : app()->db->cache($getOperationsCount),
            'pageSize'   => 20,
        ]);
    }

    /**
     * @param Pagination $paginator
     * @param ActiveQuery $query
     * @param $isLastFilterDateToday
     * @return Operation[]|array|mixed|\yii\db\ActiveRecord[]
     * @throws \Throwable
     */
    private function getPaginateOperations(Pagination $paginator, ActiveQuery $query, $isLastFilterDateToday)
    {
        $getOperations = function () use ($query, $paginator) {
            return $query->offset($paginator->offset)
                ->limit($paginator->limit)
                ->all();
        };

        if ($paginator->getPageCount() === $paginator->getPage() && $isLastFilterDateToday) {
            return $getOperations();
        }

        return app()->db->cache($getOperations);
    }

    public function actionReportPaginate()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;

        $arPeriod = $this->getSearchPeriod(post('filter'), post('first_date'), post('second_date'));
        $accKindId = post('entity') === 'all' ? null : post('entity_type');
        $cities = post('city_id', $this->getUserCityIds());

        $query = $this->getInnerReport(
            $arPeriod,
            $accKindId,
            post('entity_id'),
            $cities,
            post('operation_type'),
            post('car_class')
        );

        $isLastFilterDateToday = $this->isLastFilterDateToday($arPeriod[1]);

        $pages = $this->createPaginator($query, $isLastFilterDateToday);

        $operations = $this->getPaginateOperations($pages, $query, $isLastFilterDateToday);

        $response->data = [
            'content' => $this->renderAjax('_rows', compact('report_type', 'operations')),
            'pager'   => $this->renderAjax('_link_pager', compact('pages')),
        ];

        $response->send();
    }

    /**
     * Search report data on outer accounts.
     * @return mixed
     */
    public function actionOuterReportSearch()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $report_type = 'outer';
        //Пока не используется
        $time_offset = null;

        $arPeriod = $this->getSearchPeriod(post('filter'), post('first_date'), post('second_date'));

        /** @var Operation[] $operations */
        $operations = TransactionReport::getOuterReport($arPeriod);

        if (!empty($operations)) {
            $openPeriodSaldo = $this->getOperationOpenBalanceIndexedByCurrencySymbol($operations[0]);
        }

        return $this->renderAjax(post('view'), compact('operations', 'time_offset', 'openPeriodSaldo', 'report_type'));
    }

    /**
     * Search report data on client bonus accounts.
     * @return mixed
     * @throws \Throwable
     */
    public function actionClientBonusReportSearch()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $openPeriodSaldo = [];
        $arPeriod = $this->getSearchPeriod(post('filter'), post('first_date'), post('second_date'));

        $bonusReport = new BonusReport([
            'firstDate' => $arPeriod[0],
            'lastDate'  => $arPeriod[1],
            'ownerId'   => post('entity_id'),
            'tenantId'  => user()->tenant_id,
        ]);

        $query = $bonusReport->buildOperationsQuery();

        $isLastFilterDateToday = $this->isLastFilterDateToday($arPeriod[1]);

        $pages = $this->createPaginator($query, $isLastFilterDateToday);
        $pages->route = Url::toRoute('/reports/transaction/report-bonus-paginate');

        $operations = $this->getPaginateOperations($pages, $query, $isLastFilterDateToday);

        if (!empty($operations)) {
            $openPeriodSaldo = $this->isReportForAllEntities(post())
                ? TransactionReport::getSystemBonusOpenPeriodSaldo($arPeriod)
                : $this->getOperationOpenBalanceIndexedByCurrencySymbol($operations[0]);
        }

        $balances = $bonusReport->getBalances();
        $totalBalances = TransactionReportHelper::formatTotalBalances($balances);

        return $this->renderAjax(post('view'), [
            'operations'      => $operations,
            'time_offset'     => null,
            'report_type'     => 'client-bonus',
            'openPeriodSaldo' => $openPeriodSaldo,
            'pages'           => $pages,
            'totalBalances'   => $totalBalances,
        ]);
    }

    public function actionReportBonusPaginate()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;

        $arPeriod = $this->getSearchPeriod(post('filter'), post('first_date'), post('second_date'));

        $bonusReport = new BonusReport([
            'firstDate' => $arPeriod[0],
            'lastDate'  => $arPeriod[1],
            'ownerId'   => post('entity_id'),
            'tenantId'  => user()->tenant_id,
        ]);

        $query = $bonusReport->buildOperationsQuery();

        $isLastFilterDateToday = $this->isLastFilterDateToday($arPeriod[1]);

        $pages = $this->createPaginator($query, $isLastFilterDateToday);

        $operations = $this->getPaginateOperations($pages, $query, $isLastFilterDateToday);

        $response->data = [
            'content' => $this->renderAjax('_rows', ['operations' => $operations, 'report_type' => 'client-bonus']),
            'pager'   => $this->renderAjax('_link_pager', compact('pages')),
        ];

        $response->send();
    }

    /**
     * @param array $data
     * @return bool
     */
    private function isReportForAllEntities(array $data)
    {
        return $data['entity'] === 'all' || empty($data['entity_id']);
    }

    /**
     * Placeholder translate.
     * @param integer $entity_type
     * @return
     */
    public function actionGetEntityTranslate($entity_type)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;

        $translate = TransactionReport::getTranslateEntityPlaceholderByType($entity_type);
        $response->data = [
            'translate' => empty($translate) ? t('transaction', 'All') : $translate,
        ];
        $response->send();
    }

    /**
     * Autocomplete.
     * @return mixed
     */
    public function actionGetEntityData()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $entity_type = get('entity_type');
        $input_search = get('entity_name');

        if ($entity_type == Account::WORKER_KIND) {
            $workerSearch = new WorkerSearch([
                'formName'       => '',
                'accessCityList' => $this->getUserCityIds(),
            ]);
            $dataProvider = $workerSearch->search([
                'stringSearch' => $input_search,
            ]);
            $entityModels = $dataProvider->getModels();

            $entityData = array_map(function (Worker $worker) {
                $callsign = t('employee', 'Callsign') . ': ' . $worker->callsign;

                return [
                    'entity_id' => $worker->worker_id,
                    'name'      => implode(' - ', [$worker->getShortName(), $callsign]),
                ];
            }, $entityModels);
        } elseif ($entity_type == Account::CLIENT_KIND) {
            $search_model = new ClientSearch(['formName' => '']);
            $search_model->setAccessCityList($this->getUserCityIds());
            $dataProvider = $search_model->search(['input_search' => $input_search, 'city_id' => get('city_id')]);
            $entityModels = $dataProvider->getModels();

            /** @var Client $client */
            foreach ($entityModels as $client) {
                $entityName = $client->clientPhones[0]->value;
                $clientShortName = $client->getShortName();
                if (!empty($clientShortName)) {
                    $entityName .= ' - ' . $clientShortName;
                }
                $entityData[] = ['entity_id' => $client->client_id, 'name' => $entityName];
            }
        } elseif ($entity_type == Account::COMPANY_KIND) {
            $search_model = new CompanySearch(['formName' => '']);
            $search_model->setAccessCityList($this->getUserCityIds());
            $dataProvider = $search_model->search(['input_search' => $input_search, 'city_id' => get('city_id')]);
            $entityModels = $dataProvider->getModels();
            foreach ($entityModels as $company) {
                $entityName = empty($company->full_name) ? $company->name : $company->full_name;
                $entityData[] = ['entity_id' => $company->company_id, 'name' => $entityName];
            }
        } else {
            $entityData = [];
        }

        return $this->renderAjax('entity_search', compact('entityData'));
    }

    /**
     * @param $filter
     * @param $first_date
     * @param $last_date
     * @return array
     */
    private function getSearchPeriod($filter, $first_date, $last_date)
    {
        if ($filter == 'today') {
            $arPeriod = [
                date('Y-m-d H:i:s', mktime(0, 0, 0)),
                date('Y-m-d H:i:s', mktime(23, 59, 59)),
            ];
        } elseif ($filter == 'month') {
            $arPeriod = [
                date('Y-m-d H:i:s', mktime(0, 0, 0, date("n"), 1)),
                date('Y-m-d H:i:s'),
            ];
        } elseif ($filter == 'period') {
            $arPeriod = [
                date('Y-m-d H:i:s', strtotime($first_date . ' 00:00:00')),
                date('Y-m-d H:i:s', strtotime($last_date . ' 23:59:59')),
            ];
        } else {
            $arPeriod = [];
        }

        return $arPeriod;
    }

    public function actionDumpTransaction()
    {
        $result = $this->getTransaction(get());

        $operations = $result['operations'];
        $openPeriodSaldo = $result['openPeriodSaldo'];

        if (empty($operations)) {
            ExcelExport::widget([
                'data'     => [],
                'format'   => 'Excel5',
                'fileName' => 'transaction',
            ]);
        }

        if (!empty($openPeriodSaldo)) {
            $body[] = [
                t('balance', 'Beginning balance'),
            ];
        }
        foreach ($openPeriodSaldo as $key => $item) {
            $body[] = [
                app()->formatter->asMoney($item, $key),
            ];
        }

        $body[] = [];

        $debit = [];
        $credit = [];

        $body[] = [
            t('balance', 'Operation date'),
            t('balance', 'Account holder'),
            t('balance', 'Operation'),
            t('balance', 'Comment'),
            t('balance', 'Debet'),
            t('balance', 'Credit'),
        ];
        /** @var Operation $operation */
        foreach ($operations as $operation) {
            $currencySymbol = Currency::getCurrencySymbol($operation->account->currency_id);
            $date = strtotime($operation->date);
            $time = implode(", ", [
                app()->formatter->asDate($date, 'shortDate'),
                app()->formatter->asTime($date, 'short'),
            ]);

            $owner = TransactionReport::getEntityName($operation->account->acc_kind_id);
            $owner .= " " . $operation->owner_name;

            $info = $operation->getOperationTypeName() . ' (' . $operation->transaction->getPaymentTypeName() . ')';

            $description = TransactionReportHelper::getDescriptionOfOperation($operation, false);

            if ($operation->isExpense()) {
                $debit[$currencySymbol] += $operation->sum;
                $debitValue = app()->formatter->asMoney(+$operation->sum, $currencySymbol);
                $creditValue = '';
            } else {
                $debitValue = '';
                $credit[$currencySymbol] += $operation->sum;
                $creditValue = app()->formatter->asMoney(+$operation->sum, $currencySymbol);
            }

            $body[] = [$time, $owner, $info, $description, $debitValue, $creditValue];
        }

        ExcelExport::widget([
            'data'     => $body,
            'format'   => 'Excel5',
            'fileName' => 'transaction',
        ]);
    }

    /**
     * @param array $data
     * @return array
     */
    private function getTransaction(array $data)
    {
        $arPeriod = $this->getSearchPeriod($data['filter'], $data['first_date'], $data['second_date']);
        $accKindId = $data['entity'] === 'all' ? null : $data['entity_type'];
        $cities = $data['city_id'] ?: $this->getUserCityIds();

        /** @var Operation[] $operations */
        $operations = $this->getInnerReport(
            $arPeriod,
            $accKindId,
            $data['entity_id'],
            $cities,
            $data['operation_type'],
            $data['car_class']
        )->all();
        $openPeriodSaldo = null;

        if (!empty($operations)) {
            $openPeriodSaldo = $this->isReportForAllEntities($data)
                ? TransactionReport::getSystemOpenPeriodSaldo($arPeriod)
                : $this->getOperationOpenBalanceIndexedByCurrencySymbol($operations[0]);
        }

        return [
            'operations'      => $operations,
            'time_offset'     => null,
            'openPeriodSaldo' => $openPeriodSaldo,
            'report_type'     => 'inner',
        ];
    }

    /**
     * @param array $arPeriod
     * @param int $accKindId
     * @param int $ownerId
     * @param int|array $cityId
     * @param null|int $operationType
     * @param null|int $carClass
     * @return ActiveQuery
     */
    private function getInnerReport(
        array $arPeriod,
        $accKindId,
        $ownerId,
        $cityId,
        $operationType = null,
        $carClass = null
    ) {
        return (new InnerOperationSearch([
            'firstDate'      => $arPeriod[0],
            'lastDate'       => $arPeriod[1],
            'ownerId'        => $ownerId,
            'accKindId'      => $accKindId,
            'cityId'         => $cityId,
            'accessCityList' => $this->getUserCityIds(),
            'operationType'  => $operationType,
            'carClass'       => $carClass,
        ]))->search();
    }

    /**
     * @param Operation $operation
     * @return array
     */
    private function getOperationOpenBalanceIndexedByCurrencySymbol(Operation $operation)
    {
        return [
            Currency::getCurrencySymbol($operation[0]->account->currency_id) => $operation[0]->getOpeningBalance(),
        ];
    }
}
