<?php

namespace app\modules\reports\controllers;

use app\modules\reports\models\OrderByWorkerService;
use app\modules\tenant\models\User;
use frontend\components\behavior\CityListBehavior;
use frontend\modules\companies\components\repositories\TenantCompanyRepository;
use frontend\modules\companies\components\services\CompanyCheck;
use frontend\modules\employee\components\position\PositionService;
use frontend\modules\employee\components\worker\WorkerService;
use frontend\modules\employee\models\worker\Worker;
use frontend\modules\reports\models\WorkerReportSearch;
use frontend\modules\reports\models\WorkerShiftReportSearch;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use frontend\modules\promocode\models\ClientPromoCodeSearch;

/**
 * Class WorkerController
 * @package app\modules\reports\controllers
 * @mixin CityListBehavior
 */
class WorkerController extends Controller
{
    private $positionService;
    private $workerService;

    public function behaviors()
    {
        return [
            'access'   => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [User::ROLE_STAFF_COMPANY],
                        'actions' => ['show-shifts', 'index', 'view']
                    ],
                    [
                        'allow' => false,
                        'roles' => [User::ROLE_STAFF_COMPANY],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['read_drivers'],
                    ],
                ],
            ],
            'cityList' => [
                'class' => CityListBehavior::className(),
            ],
        ];
    }

    public function __construct(
        $id,
        Module $module,
        PositionService $positionService,
        WorkerService $workerService,
        array $config = []
    )
    {
        $this->positionService = $positionService;
        $this->positionService->setTenantId(user()->tenant_id);
        $this->workerService = $workerService;

        parent::__construct($id, $module, $config);
    }

    private function getPositionMap($cityId = null)
    {
        $positions = $cityId === null
            ? $this->positionService->getAllPositions()
            : $this->positionService->getPositionsByCity($cityId);

        return empty($positions) ? [] : ArrayHelper::map($positions, 'position_id', 'name');
    }

    public function actionIndex()
    {
        return $this->render('index', $this->getListData(Yii::$app->request->queryParams));
    }

    /**
     * @param $searchParams
     *
     * @return array
     */
    private function getListData($searchParams)
    {
        $cities = $this->getUserCityList();

        $searchModel  = new WorkerReportSearch([
            'accessCityList' => array_keys($cities),
        ]);
        $dataProvider = $searchModel->search($searchParams);

        $positions = $this->getPositionMap();

        $tenantCompanies = TenantCompanyRepository::selfCreate()
            ->getForForm(['tenant_id' => user()->tenant_id]);

        return compact('searchModel', 'dataProvider', 'cities', 'positions', 'tenantCompanies');
    }

    /**
     * Worker report card.
     *
     * @param integer $id Driver ID
     *
     * @return mixed
     */
    public function actionView($id)
    {
        (new CompanyCheck())->isAccess(Worker::findOne(['worker_id' => $id]));

        $positionMap = $this->workerService->getWorkerPositionMap($id);

        $searchModel              = new OrderByWorkerService();
        $searchModel->worker_id   = $id;
        $searchModel->type        = 'today';
        $searchModel->position_id = current(array_keys($positionMap));
        $searchModel->load(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->search();
        $pjaxId       = ['pjax_order_reports', 'pjax_statistics'];

        $statistics = $searchModel->getStatistic();
        $worker     = $this->findModel($id);

        return $this->render('view',
            compact('searchModel', 'dataProvider', 'pjaxId', 'statistics', 'positionMap', 'worker')
        );
    }

    /**
     * Shift`s statistic.
     *
     * @param integer $id Worker ID
     *
     * @return mixed
     */
    public function actionShowShifts($id)
    {
        (new CompanyCheck())->isAccess(Worker::findOne(['worker_id' => $id]));

        $positions = $this->workerService->getWorkerPositionMap($id);

        $searchModel              = new WorkerShiftReportSearch(['formName' => '']);
        $searchParams             = [
            'workerId'   => $id,
            'tenantId'   => user()->tenantId,
            'positions'  => post('position_id', []),
            'filter'     => post('filter', $searchModel->filter),
            'firstDate'  => post('first_date'),
            'secondDate' => post('second_date'),
        ];
        $dataProviderActiveShifts = $searchModel->search($searchParams, true);
        $dataProviderShifts       = $searchModel->search($searchParams, false);

        $showFilter = !post('hideFilter');

        return $this->renderAjax('shifts',
            compact('showFilter', 'searchModel', 'dataProviderActiveShifts', 'dataProviderShifts', 'positions'));
    }

    /**
     * Finds the Worker model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Worker the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = Worker::findOne($id);

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}