<?php

namespace app\modules\reports\controllers;

use app\modules\tenant\models\User;
use frontend\components\behavior\CityListBehavior;
use frontend\modules\reports\models\CallSearch;
use Yii;
use yii\db\ActiveQuery;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

use frontend\modules\tenant\models\UserDispetcher;
use app\modules\tenant\models\UserWorkingTime;
use app\modules\tenant\models\UserSearch;

class OperatorController extends Controller
{
    public function behaviors()
    {
        return [
            'access'   => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => [User::ROLE_STAFF_COMPANY],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['read_reports'],
                    ],
                ],
            ],
            'cityList' => [
                'class' => CityListBehavior::className(),
            ],
        ];
    }

    public function actionIndex()
    {
        $user_city_list = $this->getUserCityList();
        $user_search = new UserSearch(['positionList' => [6, 8]]);
        $user_search->accessCityList = array_keys($user_city_list);
        $dataProvider = $user_search->search([]);
        $arUsers = $user_search->getGridData($dataProvider->getModels(), false);

        return $this->render('index', [
            'operators' => $arUsers['USERS'],
            'city_list' => $user_city_list,
        ]);
    }

    /**
     * Operator card.
     * @param integer $id Operator ID
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $operator = UserDispetcher::find()
            ->alias('ud')
            ->where(['ud.user_id' => $id])
            ->joinWith([
                'user' => function (ActiveQuery $query) {
                    $query->where(['tenant_id' => user()->tenant_id]);
                },
            ], false)
            ->one();

        $statistics = UserWorkingTime::getShiftsByPeriod($id, $this->getSearchPeriod('week'));

        return $this->render('view', compact('operator', 'statistics'));
    }

    /**
     * Statistics shifts.
     * @param integer $id Operator ID
     * @return mixed
     */
    public function actionShowShifts($id)
    {
        $post_filter = get('filter');

        if (!empty($post_filter)) {
            $filter = $post_filter;
        } else {
            $filter = 'week';
        }
        $operatorShifts = UserWorkingTime::getShiftsByPeriod($id, $this->getSearchPeriod($filter));

        return $this->renderAjax('_shifts', compact('operatorShifts'));
    }

    /**
     * Statistics calls.
     * @param integer $id Operator ID
     * @return mixed
     */
    public function actionShowCalls($id)
    {
        $filter       = $this->getSearchPeriod(get('filter'), 'date');
        $searchModel  = new CallSearch();
        $dataProvider = $searchModel->search(user()->tenant_id, $id, $filter);

        return $this->renderAjax('_grid_calls', compact('dataProvider'));
    }

    public function getUserCityList()
    {
        return user()->getUserCityList();
    }

    public function actionSearch()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $cityList = $this->getUserCityList();
        $user_search = new UserSearch(['accessCityList' => array_keys($cityList)]);
        $sort = Yii::$app->request->post('sort');

        if (!empty($sort)) {
            $sortSymbol = $sort === 'desc' ? '-' : '';
            $_GET['sort'] = $sortSymbol . 'fullName';
        } else {
            $_GET['sort'] = null;
        }

        $searchParams = [
            'UserSearch' => [
                'city_id'      => Yii::$app->request->post('city_id'),
                'positionList' => [6, 8],
                'stringSearch' => Yii::$app->request->post('input_search'),
            ],
        ];
        $dataProvider = $user_search->search($searchParams);
        $arUsers = $user_search->getGridData($dataProvider->getModels(), false);

        return $this->renderAjax('_index', ['operators' => $arUsers['USERS']]);
    }

    private function getSearchPeriod($filter, $type = 'unixtime')
    {
        if ($filter == 'week') {
            $arPeriod = [mktime(0, 0, 0, date("n"), date("j") - 7), mktime(23, 59, 59, date("n"), date("j"))];
        } elseif ($filter == 'period') {
            $arPeriod = [strtotime(get('first_date')), strtotime(get('second_date') . ' 23:59:59')];
        } else {
            $arPeriod = [];
        }

        if ($type != 'unixtime') {
            $arPeriod = array_map(function ($item) {
                return date("Y-m-d H:i:s", $item);
            }, $arPeriod);
        }

        return $arPeriod;
    }
}

