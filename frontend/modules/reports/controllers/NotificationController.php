<?php

namespace app\modules\reports\controllers;

use app\modules\client\models\ClientSearchById;
use app\modules\tenant\models\User;
use frontend\modules\reports\models\notification\ClientSearch;
use frontend\modules\reports\models\NotificationReport;
use frontend\modules\reports\models\NotificationReportSearch;
use MongoDB\BSON\ObjectID;
use yii\filters\AccessControl;
use yii\web\Controller;

class NotificationController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => [User::ROLE_STAFF_COMPANY],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['read_reports'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $pjaxId = 'notification_report';
        $searchModel = new NotificationReportSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index',
            compact('searchModel', 'dataProvider', 'pjaxId')
        );
    }

    public function actionDetail($id)
    {
        $notification = NotificationReport::find()
            ->where([
                'tenant_id' => user()->tenant_id,
                '_id' => new ObjectID($id)
            ])
        ->one();

        $searchModel = new ClientSearch(['type' => $notification->client_type]);
        $dataProvider = $searchModel->search($notification->client_ids);

        $notificationSearchModel = new NotificationReportSearch();
        $notificationDataProvider = $notificationSearchModel->searchById($id);
        $pjaxId = ['notification_report', 'notification_clients'];

        return $this->render('detail', compact('notification', 'dataProvider', 'notificationDataProvider', 'pjaxId'));
    }

}