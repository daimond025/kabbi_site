<?php

namespace app\modules\reports\controllers;

use app\modules\tenant\models\User;
use Yii;
use yii\db\ActiveQuery;
use yii\web\Controller;
use yii\filters\AccessControl;
use app\modules\client\models\ClientCompany;
use app\modules\client\models\CompanySearch;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use yii\web\NotFoundHttpException;
use frontend\components\behavior\CityListBehavior;

/**
 * Class CompanyController
 * @package app\modules\reports\controllers
 * @mixin CityListBehavior
 */
class CompanyController extends Controller
{
    public function behaviors()
    {
        return [
            'access'   => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => [User::ROLE_STAFF_COMPANY],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['read_reports'],
                    ],
                ],
            ],
            'cityList' => [
                'class' => CityListBehavior::className(),
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index', $this->getListData(Yii::$app->request->queryParams));
    }

    private function getListData($searchParams)
    {
        $cityList = $this->getUserCityList();

        $searchModel = new CompanySearch();
        $searchModel->setAccessCityList(array_keys($cityList));
        $dataProvider = $searchModel->search($searchParams);
        $pjaxId = 'company';

        return compact('searchModel', 'dataProvider', 'cityList', 'pjaxId');
    }

    /**
     * Driver card.
     * @param integer $id Driver ID
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $company = $this->getCompany($id);

        if (empty($company)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $orders = $this->getCompanyOrders($id);
        $staff = $this->groupOrderStatByClient($orders);

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('_table', compact('orders', 'staff', 'company'));
        }

        return $this->render('view', compact('company', 'orders', 'staff'));
    }

    private function getCompany($id)
    {
        return ClientCompany::find()
            ->where([
                'company_id' => $id,
                'tenant_id'  => user()->tenant_id,
            ])
            ->select(['name', 'city_id'])
            ->one();
    }

    /**
     * The grouping of orders by client
     * @param array $orders app\modules\order\models\Order
     * @return array
     */
    private function groupOrderStatByClient($orders)
    {
        $orderGroups = [];
        foreach ($orders as $order) {
            $orderGroups[$order->client_id][] = $order;
        }

        return $orderGroups;
    }

    /**
     * Getting comapany orders
     * @param integer $company_id
     * @param string $period "week"|"period"
     * @return array app\modules\order\models\Order
     */
    private function getCompanyOrders($company_id, $period = 'week')
    {
        list($first_date, $last_date) = $this->getSearchPeriod(post('filter', $period));

        return Order::find()
            ->where([
                'tenant_id'  => user()->tenant_id,
                'company_id' => $company_id,
                'status_id'  => OrderStatus::STATUS_COMPLETED_PAID,
            ])
            ->andWhere(['between', 'create_time', $first_date, $last_date])
            ->with([
                'client'     => function (ActiveQuery $query) {
                    $query->select(['client_id', 'last_name', 'name', 'second_name']);
                },
                'client.clientPhones',
                'detailCost' => function (ActiveQuery $query) {
                    $query->select(['detail_id', 'order_id', 'summary_cost']);
                },
            ])
            ->select(['order_id', 'client_id', 'create_time', 'order_number', 'currency_id'])
            ->orderBy('create_time')
            ->all();
    }

    /**
     * Show statistic.
     * @param int $id
     * @return string
     */
    public function actionShowStat($id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $orders = $this->getCompanyOrders($id, post('period'));
        $staff = $this->groupOrderStatByClient($orders);
        $company = $this->getCompany($id);

        return json_encode([
            'orders' => $this->renderAjax('_orders', ['orders' => $orders]),
            'staff'  => $this->renderAjax('_staff', ['staff' => $staff, 'company' => $company]),
        ]);
    }

    private function getSearchPeriod($filter)
    {
        if ($filter == 'week') {
            $arPeriod = [
                mktime(0, 0, 0, date("n"), date("j") - 7),
                mktime(23, 59, 59, date("n"), date("j")),
            ];
        } elseif ($filter == 'period') {
            $firstDate = date('d.m.Y', strtotime(post('first_date')));
            $secondDate = date('d.m.Y', strtotime(post('second_date')));
            $arFirstDay = explode('.', $firstDate);
            $arSecondDay = explode('.', $secondDate);

            $arPeriod = [
                mktime(0, 0, 0, $arFirstDay[1], $arFirstDay[0], $arFirstDay[2]),
                mktime(23, 59, 59, $arSecondDay[1], $arSecondDay[0], $arSecondDay[2]),
            ];
        } else {
            $arPeriod = [];
        }

        return $arPeriod;
    }
}
