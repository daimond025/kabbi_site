<?php

namespace app\modules\reports;

use app\components\behavior\ControllerBehavior;
use yii\helpers\ArrayHelper;

/**
 * Class Module
 * @package app\modules\reports
 *
 * @mixin ControllerBehavior
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\reports\controllers';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'common' => [
                'class' => ControllerBehavior::className(),
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        $this->commonBeforeAction();

        return parent::beforeAction($action);
    }
}
