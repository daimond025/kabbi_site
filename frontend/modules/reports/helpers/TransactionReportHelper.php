<?php

namespace app\modules\reports\helpers;

use app\modules\balance\models\Account;
use app\modules\balance\models\Operation;
use app\modules\balance\models\Transaction;
use common\components\formatter\Formatter;
use frontend\modules\balance\components\helpers\CommentParserHelper;
use frontend\modules\tenant\models\Currency;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class TransactionReportHelper
{
    /**
     * Формирование текста описания операции.
     * @param Operation $operation
     * @param bool $html
     * @return string
     */
    public static function getTextOperation(Operation $operation, $html = true)
    {
        if ($operation->transaction->type_id == Transaction::GOOTAX_PAYMENT_TYPE) {
            if ($operation->type_id == Operation::EXPENSES_TYPE) {
                return t('payment', 'Payment for:') . ' ' . $operation->transaction->comment;
            } else {
                if ($operation->transaction->payment_method == Transaction::CASH_PAYMENT) {
                    return $operation->getOperationTypeName();
                } else {
                    if ($operation->transaction->payment_method == Transaction::PERSONAL_ACCOUNT_PAYMENT) {
                        return t('payment', 'Refund money');
                    }
                }
            }
        } elseif ($operation->account->acc_kind_id == Account::TENANT_KIND
            && $operation->transaction->payment_method != Transaction::TERMINAL_ELECSNET_PAYMENT
        ) {
            $description = !empty($operation->transaction->comment)
                ? ($html ? '<br>' : "\n") . t('balance', 'Comment') . ': ' . $operation->transaction->comment
                : '';

            return $operation->getOperationTypeName() . $description;
        }

        $operation_text = $operation->getOperationTypeName();

        if ($operation->type_id == Operation::INCOME_TYPE
            && $operation->transaction->payment_method == Transaction::CARD_PAYMENT
            && $operation->account->acc_kind_id == Account::WORKER_KIND
        ) {
            if (isset($operation->transaction->order_id)) {
                $operation_text .= ' - ' . Transaction::getPaymentMethodName(Transaction::PERSONAL_ACCOUNT_PAYMENT);
            } else {
                $operation_text .= ' - ' . $operation->transaction->getPaymentTypeName();
            }
        } elseif ($operation->type_id == Operation::INCOME_TYPE
            && !in_array(
                $operation->account->acc_kind_id,
                [Account::CLIENT_BONUS_KIND, Account::CLIENT_BONUS_UDS_GAME_KIND]
            )) {
            if (isset($operation->transaction->order_id)
                && $operation->account->acc_kind_id == Account::WORKER_KIND
            ) {
                $operation_text .= ' - ' . Transaction::getPaymentMethodName(Transaction::PERSONAL_ACCOUNT_PAYMENT);
            } else {
                $operation_text .= ' - ' . $operation->transaction->getPaymentTypeName();
            }
        }

        if ($html) {
            $operation_text = Html::encode($operation_text);
        }
        $description = self::getDescriptionOfOperation($operation, $html);

        return $operation_text . ($html ? '<br>' : "\n") . t('balance', 'Comment') . ': ' . $description;
    }

    /**
     * @param Operation $operation
     * @param bool $html
     * @return string
     */
    public static function getDescriptionOfOperation(Operation $operation, $html = true)
    {
        $operation_text = '';
        if (!empty($operation->comment)) {
            if ($operation->transaction->payment_method == Transaction::WITHDRAWAL_TARIFF_PAYMENT) {
                $arComment = explode('||', $html ? Html::encode($operation->comment) : $operation->comment);
                $arComment[0] = t('balance', $arComment[0]);
                $comment = implode(' ', $arComment);
            } else {
                $arComment = explode(' №', Html::encode($operation->comment));
                $arComment[0] = t('balance', $arComment[0]);

                if ($operation->transaction->type_id == Transaction::ORDER_PAYMENT_TYPE
                    && isset($arComment[1])
                ) {
                    $arComment[1] = $html ? Html::a(
                        $arComment[1],
                        ['/order/order/view', 'order_number' => $arComment[1]],
                        ['class' => 'open_order']
                    ) : $arComment[1];
                }

                $comment = count($arComment) == 1 ? $arComment[0] : implode(' №', $arComment);
            }

            $operation_text .= CommentParserHelper::parsingComment($comment);
        }

        if (!empty($operation->transaction->terminal_transact_number)
            && $operation->transaction->payment_method == Transaction::TERMINAL_ELECSNET_PAYMENT) {
            $operation_text .= ($html ? '<br>' : "\n") . Html::encode(
                t(
                    'balance',
                    'Terminal №{terminal} Check №{check}',
                    [
                        'terminal' => substr($operation->transaction->terminal_transact_number, 0, 8),
                        'check'    => substr($operation->transaction->terminal_transact_number, 8, 4),
                    ]
                )
            );
        }

        return $operation_text;
    }

    /**
     * @param array $balances
     * @return array
     */
    public static function formatTotalBalances(array $balances)
    {
        $balanceMap = ArrayHelper::map($balances, 'type_id', 'total', 'currency_id');

        $formattedMap = [];
        foreach ($balanceMap as $currencyId => $operationTypeMap) {
            foreach ($operationTypeMap as $operationTypeId => $total) {
                if ($operationTypeId === Operation::INCOME_TYPE) {
                    $formattedMap[$currencyId]['income'] = +$total;
                } else {
                    if (!isset($formattedMap[$currencyId]['expenses'])) {
                        $formattedMap[$currencyId]['expenses'] = 0;
                    }

                    $formattedMap[$currencyId]['expenses'] += $total;
                }
            }
        }

        /** @var Formatter $formatter */
        $formatter = Yii::$app->formatter;

        $result = ['income' => '', 'expenses' => '', 'total' => ''];
        foreach ($formattedMap as $currencyId => $data) {
            $currencySumbol = Currency::getCurrencySymbol($currencyId);

            if (!empty($result['income'])) {
                $result['income'] .= ', ';
            }
            $income = ArrayHelper::getValue($data, 'income', 0);
            $result['income'] .= $formatter->asMoney($income, $currencySumbol);

            if (!empty($result['expenses'])) {
                $result['expenses'] .= ', ';
            }
            $expenses = ArrayHelper::getValue($data, 'expenses', 0);
            $result['expenses'] .= $formatter->asMoney($expenses, $currencySumbol);

            if (!empty($result['total'])) {
                $result['total'] .= ', ';
            }
            $result['total'] .= $formatter->asMoney($income - $expenses, $currencySumbol);
        }

        return $result;
    }
}
