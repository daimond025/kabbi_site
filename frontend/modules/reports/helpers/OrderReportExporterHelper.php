<?php
namespace app\modules\reports\helpers;

use \yii\helpers\ArrayHelper;

class OrderReportExporterHelper
{
    public static function getFreighter($model)
    {
        return ArrayHelper::getValue($model, function ($model) {
            $array = ArrayHelper::htmlEncode(array_filter([$model->full_company_name,$model->legal_address,'ИНН' . ' ' . $model->inn], function($v){
               return $v !== '';
            }));
            return implode(', ', $array);
        });
    }

    public static function getAddressStart($array)
    {

            return ArrayHelper::getValue($array, function ($array) {
                $array = ArrayHelper::htmlEncode(array_filter([
                        current($array)['city'],
                        current($array)['street'],
                        current($array)['house']
                    ], function ($v) {
                        return $v !== null && $v !== '';
                    })
                );

                return implode(', ', $array);
            });

    }

    public static function getAddressEnd($array)
    {
        return ArrayHelper::getValue($array, function ($array) {
            $array = ArrayHelper::htmlEncode(array_filter([end($array)['city'], end($array)['street'], end($array)['house']], function($v){
                    return isset($v) && $v !== '';
                })
            );
            return implode(', ', $array);
        });

    }

}