<?php

/* @var $this yii\web\View */
/* @var string $pjaxId yii\web\View */
/* @var $searchModel \app\modules\client\models\OrderClientSearch */
/* @var $dataProvider yii\data\ArrayDataProvider */

use yii\grid\GridView;
use frontend\widgets\LinkPager;
use yii\widgets\Pjax;

Pjax::begin(['id' => $pjaxId]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'       => "{items}\n{pager}",
    'tableOptions' => [
        'class' => 'people_table',
    ],
    'pager'        => [
        'class'         => LinkPager::className(),
        'prevPageLabel' => t('app', 'Prev'),
        'nextPageLabel' => t('app', 'Next'),
    ],
    'columns'      => [
        [
            'label' => '',
            'attribute' => 'order_number',
            'content' => function ($model) {
                return $this->render('_orders/_cl_id', ['model' => $model]);
            },
            'contentOptions' => ['class' => 'cl_id'],
            'headerOptions' => ['style' => 'width:10%'],
        ],
        [
            'label' => t('order', 'Address'),
            'attribute' => 'info',
            'content' => function ($model) {
                return $this->render('_orders/_cl_info', ['model' => $model]);
            },
            'contentOptions' => ['class' => 'cl_info'],
//            'headerOptions' => ['style' => 'width:25%'],
        ],
        [
            'label' => t('order', 'Status'),
            'attribute' => 'staus_id',
            'content' => function ($model) {
                return $this->render('_orders/_cl_status', ['model' => $model]);
            },
            'contentOptions' => ['class' => 'cli_details'],
            'headerOptions' => ['style' => 'width:18%'],
        ],
        [
            'label' => '',
            'attribute' => 'review',
            'content' => function ($model) {
                return $this->render('_orders/_cl_review', ['model' => $model]);
            },
            'contentOptions' => ['class' => 'cli_review'],
            'headerOptions' => ['style' => 'width:20%'],
        ],
        [
            'label' => '',
            'attribute' => 'predv_price',
            'content' => function ($model) {
                return $this->render('_orders/_cl_costs', ['model' => $model]);
            },
            'contentOptions' => ['style' => 'text-align: right'],
            'headerOptions' => ['style' => 'width:8%'],
        ],
    ],
]);

Pjax::end();