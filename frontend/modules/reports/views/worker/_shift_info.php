<?php

use common\helpers\DateTimeHelper;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $id int */
/* @var $startWork int */
/* @var $endWork int */
/* @var $shiftTime int */
/* @var $pauseTime int */
/* @var $blockTime int */
/* @var $timeOffset int */
/* @var $events array */
/* @var $shift_end_reason string */
/* @var $shift_end_event_sender_type string */
/* @var $shift_end_event_sender_id integer */

$forms     = ['history', 'block', 'pause'];
$formatter = app()->formatter;

?>

<div style="display: none">
    <? foreach ($forms as $form):
        switch ($form) {
            case 'block':
                $formId = "dr_block_{$id}";
                $title  = t('reports', 'Blocks');
                break;
            case 'pause':
                $formId = "dr_pause_{$id}";
                $title  = t('reports', 'Pauses');
                break;
            default:
                $formId = "dr_hist_{$id}";
                $title  = t('reports', 'Shift');
        } ?>
        <div id="<?= $formId ?>" class="dr_hist">
            <h2><?= $title ?></h2>
            <ul>
                <li>
                    <span>
                        <?= implode(',', [
                            $formatter->asDate($startWork + $timeOffset, 'shortDate'),
                            $formatter->asTime($startWork + $timeOffset, 'short'),
                        ]) ?>
                    </span>
                    <i><?= t('reports', 'Start shift') ?></i>
                </li>

                <? foreach ($events as $event):
                    if ($form == 'history' || $form == $event['type']): ?>
                        <li>
                        <span>
                            <?= date('H:i', $event['startTime'] + $timeOffset)
                            . '–' . date('H:i', $event['finishTime'] + $timeOffset) ?>
                        </span>

                            <? if ($form == 'history' && $event['type'] == 'block') {
                                $eventTitle = t('reports', 'Block') . ': ';
                            } elseif ($form == 'history' && $event['type'] != 'block') {
                                $eventTitle = t('reports', 'Pause') . ': ';
                            } else {
                                $eventTitle = '';
                            } ?>

                            <i><?= Html::encode($eventTitle . $event['label']) ?></i>
                        </li>
                    <? endif ?>
                <? endforeach ?>

                <li>
                    <span>
                        <?= empty($endWork) ? '' : implode(',', [
                            $formatter->asDate($endWork + $timeOffset, 'shortDate'),
                            $formatter->asTime($endWork + $timeOffset, 'short'),
                        ]) ?>
                    </span>
                    <i><?= t('reports', 'Finish shift') ?></i>
                </li>

                <li>
                    <p>
                        <?php
                        switch ($shift_end_reason) {
                            case 'operator_service':
                                switch ($shift_end_event_sender_type) {
                                    case 'worker':
                                        echo t('reports', 'Shift end by worker');
                                        break;
                                    default:
                                        echo t('reports', 'Shift end by user');
                                }
                                break;
                            case 'worker_service':
                                echo t('reports', 'Shift end by worker');
                                break;
                            case 'timer':
                                echo t('reports', 'Shift end by automatically with the end of time');
                                break;
                            case 'bad_parking':
                                echo t('reports', 'Shift end by automatically so as worker out getting orders');
                                break;
                            case 'bad_coords':
                                echo t('reports', 'Shift end by automatically so as losted coords of worker');
                                break;
                            default:
                                null;
                        }
                        ?>
                    </p>
                    <?php
                    $textLink = \app\modules\tenant\models\User::find()->where([
                        'user_id'   => $shift_end_event_sender_id,
                        'tenant_id' => user()->tenant_id,
                    ])->select("CONCAT_WS(' ',`last_name`,`name`)")->scalar();
                    $hrefLink = \yii\helpers\Url::to("/lib/user/update/$shift_end_event_sender_id");
                    ?>
                    <p><?= Html::a($textLink, $hrefLink) ?></p>
                </li>

                <li>
                    <span>
                        <b><?= DateTimeHelper::getFormatTimeFromSeconds($shiftTime) ?></b><br/>
                        <?= ($form != 'block') ? Html::tag('b',
                                DateTimeHelper::getFormatTimeFromSeconds($pauseTime)) . Html::tag('br') : '' ?>
                        <?= ($form != 'pause') ? Html::tag('b',
                            DateTimeHelper::getFormatTimeFromSeconds($blockTime)) : '' ?>
                    </span>
                    <i>
                        <b><?= t('reports', 'Shift') ?></b><br/>
                        <?= ($form != 'block') ? Html::tag('b', t('reports', 'Pauses')) . Html::tag('br') : '' ?>
                        <?= ($form != 'pause') ? Html::tag('b', t('reports', 'Blocks')) : '' ?>
                    </i>
                </li>
            </ul>
        </div>
    <? endforeach ?>
</div>