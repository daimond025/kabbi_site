<?php
/* @var string $pjaxId */
/* @var $this yii\web\View */
/* @var $searchModel \app\modules\client\models\OrderClientSearch */
/* @var $dataProvider yii\data\ArrayDataProvider */
/* @var array $cityList */
/* @var array $positionMap */

use frontend\widgets\filter\Filter;

$bundle = \app\modules\reports\assets\ReportsAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/workers.js');
\frontend\assets\LeafletAsset::register($this);
//$this->registerJsFile($reportAsset->baseUrl . '/workers.js');

$filter = Filter::begin([
    'pjaxId' => $pjaxId,
    'model'  => $searchModel,
    'onSubmit' => true,
    'onChange' => false,
]);
echo $filter->dropDownList('position_id', $positionMap);
echo $filter->content('', ['class' => 'filter_item_2']);
echo $filter->radioList('type',
    [
        'today' => [
            'label' => t('reports', 'Today'),
        ],
        'yesterday' => [
            'label' => t('reports', 'Yesterday')
        ],
        'month' => [
            'label' => app()->formatter->asDate(time(), 'LLLL'),
        ],
        'period' => [
            'label' => t('reports', 'Period'),
            'content' => $filter->datePeriod(['first_date', 'second_date'], [
                date("d.m.Y", mktime(0, 0, 0, date("m")  , date("d")-2, date("Y"))),
                date("d.m.Y", mktime(0, 0, 0, date("m")  , date("d"), date("Y")))
            ], ['class' => 'cof_date']),
        ],
    ], [], ['class' => 'filter_item_2', 'style' => 'width:auto']);

echo $filter->button('Показать', [], ['style' => 'width:auto']);

Filter::end();
?>

<div class="oor_content">
    <h3><a data-tab="driver_stat" class="active"><?= t('reports', 'Statistics') ?></a></h3>
    <h3><a data-tab="driver_orders"><?= t('reports', 'Order list') ?></a></h3>
</div>
<div class="stat_content">
    <div class="active tab_div" id="driver_stat">
        <?= $this->render('_statistic', ['statistics' => $statistics, 'pjaxId' => $pjaxId[1]]) ?>
    </div>
    <div id="driver_orders" class="tab_div">
        <?= $this->render('_grid_orders', ['dataProvider' => $dataProvider, 'pjaxId' => $pjaxId[0]]) ?>
    </div>
</div>