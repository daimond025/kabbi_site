<?php
/* @var string $pjaxId */
/* @var $this yii\web\View */
/* @var $searchModel \app\modules\client\models\OrderClientSearch */
/* @var $dataProvider yii\data\ArrayDataProvider */
/* @var array $cityList */
/* @var array $positionMap */

use common\modules\employee\models\worker\WorkerHasCity;
use frontend\modules\balance\assets\BalanceAsset;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\balance\models\Account;

BalanceAsset::register($this);
$this->registerJs('balanceOperationInit();');

$reportAsset = app\modules\reports\assets\ReportsAsset::register($this);
$this->registerJsFile($reportAsset->baseUrl . '/workers.js');
$this->registerJs('openDetailRowsInit();');

BalanceAsset::register($this);
$this->title = $worker->fullName;

$cityId = WorkerHasCity::find()
    ->select('city_id')
    ->byWorkerId($worker->worker_id)
    ->orderBy(['last_active' => SORT_DESC, 'city_id' => SORT_ASC])
    ->limit(1)
    ->scalar();

?>
<div class="bread"><a href="<?= Url::toRoute('/reports/worker/index') ?>"><?= t('employee', 'Workers') ?></a></div>
<h1><?= Html::encode($this->title) ?></h1>
<section class="main_tabs">
    <div class="tabs_links">
        <ul>
            <li>
                <a href="#orders" class="t01"><?= t('reports', 'Orders') ?></a>
            </li>
            <li>
                <a href="#balance" class="t02" data-href="<?= Url::to([
                    '/balance/balance/show-balance',
                    'id'      => get('id'),
                    'kind_id' => Account::WORKER_KIND,
                    'city_id' => $cityId,
                ]) ?>"><?= t('employee', 'Balance') ?></a>
            </li>
            <li>
                <a href="#feedback" class="t03"
                   data-href="<?= Url::to(['/employee/worker/show-reviews', 'id' => get('id')]) ?>"><?= t('employee',
                        'Feedback') ?></a>
            </li>
            <li>
                <a href="#shifts" class="t04"
                   data-href="<?= Url::to(['/reports/worker/show-shifts', 'id' => get('id')]) ?>"><?= t('reports',
                        'Shifts') ?></a>
            </li>
            <li>
                <a href="#promocodes" class="t05"
                   data-href="<?= Url::to(['/promocode/promo-reports/promo-report-worker', 'id' => get('id')]) ?>"><?= t('reports',
                        'Promocodes') ?></a>
            </li>
        </ul>
    </div>
    <div class="tabs_content">
        <div id="t01"><?= $this->render('_view',
                compact('searchModel', 'dataProvider', 'pjaxId', 'statistics', 'positionMap')) ?>
        </div>
        <div id="t02"></div>
        <div id="t03"></div>
        <div id="t04" class="js-shifts-tab"></div>
        <div id="t05"></div>
    </div>
</section>