<?php

use app\modules\order\models\OrderStatus;
use common\components\formatter\Formatter;
use common\helpers\DateTimeHelper;
use common\modules\employee\models\worker\WorkerShift;
use frontend\modules\employee\models\worker\Worker;
use frontend\modules\reports\models\WorkerReportSearch;
use frontend\modules\tenant\models\Currency;
use frontend\widgets\filter\Filter;
use frontend\widgets\LinkPager;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Pjax;
use app\modules\tenant\models\User;

/* @var $this View */
/* @var $searchModel WorkerReportSearch */
/* @var $dataProvider ActiveDataProvider */
/* @var $cities array */
/* @var $positions array */
/* @var $tenantCompanies array */

$this->title = Yii::t('reports', 'Report by worker');

?>
    <h1><?= Html::encode($this->title) ?></h1>

<?
$pjaxId = 'worker_report';
$filter = Filter::begin([
    'id'       => 'filter_' . $pjaxId,
    'pjaxId'   => $pjaxId,
    'model'    => $searchModel,
    'onSubmit' => true,
    'onChange' => false,
]);
echo $filter->checkboxList('city_id', $cities, ['id' => $pjaxId . '-city_id']);
echo $filter->checkboxList('positionList', $positions, ['id' => $pjaxId . '-positionList']);

if (!app()->user->can(User::ROLE_STAFF_COMPANY)) {
    echo $filter->checkboxList('tenantCompanyIds', $tenantCompanies, ['id' => $pjaxId . '-tenantCompanyIds']);
}

echo $filter->input('stringSearch', ['id' => $pjaxId . '-stringSearch']);
echo $filter->radioList('filter',
    [
        'today'  => [
            'label' => t('reports', 'Today'),
        ],
        'month'  => [
            'label' => app()->formatter->asDate(time(), 'LLLL'),
        ],
        'period' => [
            'label'   => t('reports', 'Period'),
            'content' => $filter->datePeriod(['first_date', 'second_date'], [
                date("d.m.Y", mktime(0, 0, 0, date("m"), date("d") - 2, date("Y"))),
                date("d.m.Y", mktime(0, 0, 0, date("m"), date("d"), date("Y"))),
            ], ['class' => 'cof_date']),
        ],
    ], [], ['class' => 'filter_item_2', 'style' => 'width:auto']);
echo $filter->button(t('reports', 'Show'), [], ['style' => 'width: auto']);
Filter::end();


Pjax::begin(['id' => $pjaxId]);

$reportTime = time();
$shiftTime  = $pauseTime = $blockTime = $lateTime = $lateCount
    = $orderPaymentTime = $orderPaymentCount = $completedCount
    = $rejectedCount = $acceptedOfferCount = $rejectedOfferCount = 0;
$summary    = $cashSummary = $surcharge = $cashSurcharge
    = $tax = $cashTax = $commission = $cashCommission = $positions = [];
$carMileage = 0;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'       => "{items}\n{pager}",
    'tableOptions' => [
        'class' => 'people_table',
    ],
    'pager'        => [
        'class'         => LinkPager::className(),
        'prevPageLabel' => t('app', 'Prev'),
        'nextPageLabel' => t('app', 'Next'),
    ],
    'beforeRow'    => function ($model) use (
        &$reportTime,
        &$shiftTime,
        &$pauseTime,
        &$blockTime,
        &$lateTime,
        &$lateCount,
        &$orderPaymentTime,
        &$orderPaymentCount,
        &$completedCount,
        &$rejectedCount,
        &$acceptedOfferCount,
        &$rejectedOfferCount,
        &$summary,
        &$cashSummary,
        &$surcharge,
        &$cashSurcharge,
        &$tax,
        &$cashTax,
        &$commission,
        &$cashCommission,
        &$positions,
        &$carMileage
    ) {
        $shiftTime = $pauseTime = $blockTime = $lateTime = $lateCount
            = $orderPaymentTime = $orderPaymentCount = $completedCount
            = $rejectedCount = $acceptedOfferCount = $rejectedOfferCount = 0;
        $summary   = $cashSummary = $surcharge = $cashSurcharge
            = $tax = $cashTax = $commission = $cashCommission = $positions = [];
        $carMileage = 0;

        /* @var $model Worker */
        if (is_array($model->shifts)) {
            foreach ($model->shifts as $shift) {

                /* @var $shift WorkerShift */
                $shiftEndTime = $shift->end_work !== null ? $shift->end_work : $reportTime;
                $shiftTime += $shiftEndTime - $shift->start_work;
                $lateTime += $shift->worker_late_time;
                $lateCount += $shift->worker_late_count;
                $orderPaymentTime += $shift->order_payment_time;
                $orderPaymentCount += $shift->order_payment_count;
                $completedCount += $shift->completed_order_count;
                $rejectedCount += $shift->rejected_order_count;
                $acceptedOfferCount += $shift->accepted_order_offer_count;
                $rejectedOfferCount += $shift->rejected_order_offer_count;

                if ($shift->position !== null
                    && !in_array($shift->position->name, $positions)
                ) {
                    $positions[] = $shift->position->name;
                }

                if (is_array($shift->blocks)) {
                    foreach ($shift->blocks as $block) {
                        $blockTime += $block->end_block - $block->start_block;
                    }
                }

                $pauses = unserialize($shift->pause_data);
                if (is_array($pauses)) {
                    foreach ($pauses as $pause) {
                        $pauseTime += (isset($pause['pause_end']) ? $pause['pause_end'] : $shiftEndTime)
                            - (isset($pause['pause_start']) ? $pause['pause_start'] : $shift->start_work);
                    }
                }

                if (is_array($shift->shiftOrders)) {
                    foreach ($shift->shiftOrders as $shiftOrder) {
                        $order      = $shiftOrder->order;
                        $detailCost = $order->detailCost;

                        $summaryCost     = (float)(!empty($detailCost->summary_cost) ? $detailCost->summary_cost : 0);
                        $surchargeValue  = (float)(!empty($detailCost->surcharge) ? $detailCost->surcharge : 0);
                        $taxValue        = (float)(!empty($detailCost->tax) ? $detailCost->tax : 0);
                        $commissionValue = (float)(!empty($detailCost->commission) ? $detailCost->commission : 0);

                        $summary[$order->currency_id] += $summaryCost;
                        $cashSummary[$order->currency_id] += $order->payment == $order::PAYMENT_CASH ? $summaryCost : 0;

                        if (in_array($order->status_id,
                            [OrderStatus::STATUS_COMPLETED_PAID, OrderStatus::STATUS_COMPLETED_NOT_PAID], false)) {
                            $surcharge[$order->currency_id] += $surchargeValue;
                            $cashSurcharge[$order->currency_id] += $order->payment == $order::PAYMENT_CASH ? $surchargeValue : 0;
                            $tax[$order->currency_id] += $taxValue;
                            $cashTax[$order->currency_id] += $order->payment == $order::PAYMENT_CASH ? $taxValue : 0;
                            $commission[$order->currency_id] += $commissionValue;
                            $cashCommission[$order->currency_id] += $order->payment == $order::PAYMENT_CASH ? $commissionValue : 0;
                        }
                    }
                }

                $carMileage += !empty($shift->carMileage->end_value)
                    ? (float)$shift->carMileage->end_value - (float)$shift->carMileage->begin_value : 0;
            }
        }
    },
    'columns'      => [
        [
            'attribute'      => 'fullName',
            'label'          => t('app', 'Full name'),
            'contentOptions' => function ($model, $key, $index, $column) {
                return ['class' => 'pt_fio'];
            },
            'content'        => function ($model) use (&$positions) {
                $lines   = [];
                $lines[] = Html::tag('span', $model->logo, ['class' => 'pt_photo']);
                $lines[] = Html::tag('span', Html::encode($model->fullName), ['class' => 'pt_fios']);

                $lines[] = Html::tag('span', $model->callsign
                    . ' ' . implode(', ', ArrayHelper::getColumn($positions, function ($position) {
                        return t('employee', $position);
                    })), ['class' => 'table_position']);

                return Html::a(implode('', $lines),
                    ['view', 'id' => $model->worker_id, '#' => 'shifts'],
                    ['data-pjax' => 0]);
            },
            'headerOptions'  => ['class' => 'pt_fio', 'style' => 'width: 28%'],
        ],
        [
            'label'   => t('reports', 'Shifts'),
            'content' => function ($model) use (&$shiftTime, &$pauseTime) {
                $lines   = [];
                $lines[] = Html::tag('i', '', ['class' => 'icon_play'])
                    . ' ' . Html::tag('span', DateTimeHelper::getFormatTimeFromSeconds($shiftTime));
                $lines[] = Html::tag('i', '', ['class' => 'icon_pause'])
                    . ' ' . Html::tag('span', DateTimeHelper::getFormatTimeFromSeconds($pauseTime));

                return implode('<br>', $lines);
            },
        ],
        [
            'label'   => t('reports', 'Blocks (short)'),
            'content' => function ($model) use (&$blockTime) {
                return Html::tag('span', DateTimeHelper::getFormatTimeFromSeconds($blockTime), ['class' => 'link_red']);
            },
        ],
        [
            'label'   => t('reports', 'Lateness'),
            'content' => function ($model) use (&$lateTime, &$lateCount) {
                return Html::tag('span', DateTimeHelper::getFormatTimeFromSeconds($lateTime), ['class' => 'link_red'])
                    . '&nbsp;&nbsp;&nbsp;' . $lateCount;
            },
        ],
        [
            'label'   => t('reports', 'Waiting for payment'),
            'content' => function ($model) use (&$orderPaymentTime, &$orderPaymentCount) {
                return Html::tag('span', DateTimeHelper::getFormatTimeFromSeconds($orderPaymentTime))
                    . '&nbsp;&nbsp;&nbsp;' . $orderPaymentCount;
            },
        ],
        [
            'label'          => t('reports', 'Orders'),
            'contentOptions' => function ($model, $key, $index, $column) {
                return ['style' => 'text-align: right; padding-right: 10px;'];
            },
            'content'        => function ($model) use (&$completedCount, &$rejectedCount) {
                return Html::tag('span', $completedCount, ['class' => 'link_green'])
                    . '&nbsp;&nbsp;' . Html::tag('span', $rejectedCount, ['class' => 'link_red']);
            },
            'headerOptions'  => ['style' => 'text-align: right; padding-right: 10px;'],
        ],
        [
            'label'          => t('reports', 'Offer'),
            'contentOptions' => function ($model, $key, $index, $column) {
                return ['style' => 'text-align: right; padding-right: 10px;'];
            },
            'content'        => function ($model) use (&$acceptedOfferCount, &$rejectedOfferCount) {
                return Html::tag('span', $acceptedOfferCount, ['class' => 'link_green'])
                    . '&nbsp;&nbsp;' . Html::tag('span', $rejectedOfferCount, ['class' => 'link_red']);
            },
            'headerOptions'  => ['style' => 'text-align: right; padding-right: 10px;'],
        ],
        [
            'label'          => t('reports', 'Car mileage'),
            'contentOptions' => function ($model, $key, $index, $column) {
                return ['style' => 'text-align: right; padding-right: 10px;'];
            },
            'content'        => function ($model) use (&$carMileage, $formatter) {
                if ($carMileage !== null) {
                    /* @var $formatter Formatter */
                    $formatter = app()->formatter;

                    return Html::tag('span', $formatter->asDecimal($carMileage, 1));
                }
            },
            'headerOptions'  => ['style' => 'text-align: center; width: 5%'],
        ],
        [
            'label'          => t('reports', 'Summary'),
            'contentOptions' => function ($model, $key, $index, $column) {
                return ['style' => 'text-align: right; padding-right: 10px;'];
            },
            'content'        => function ($model) use (
                &$summary,
                &$cashSummary,
                &$surcharge,
                &$cashSurcharge,
                &$tax,
                &$cashTax
            ) {
                $lines = [];
                if (is_array($summary)) {
                    /* @var Formatter */
                    $formatter = app()->formatter;

                    /* @var $summary array */
                    foreach ($summary as $currencyId => $value) {
                        $currency = Currency::getCurrencySymbol($currencyId);
                        $cash     = isset($cashSummary[$currencyId]) ? $cashSummary[$currencyId] : 0;

                        $surchargeValue     = isset($surcharge[$currencyId]) ? $surcharge[$currencyId] : 0;
                        $cashSurchargeValue = isset($cashSurcharge[$currencyId]) ? $cashSurcharge[$currencyId] : 0;
                        $taxValue           = isset($tax[$currencyId]) ? $tax[$currencyId] : 0;
                        $cashTaxValue       = isset($cashTax[$currencyId]) ? $cashTax[$currencyId] : 0;

                        $lines[] = Html::tag('b', $formatter->asMoney($value + $surchargeValue - $taxValue, $currency));
                        $lines[] = Html::tag('span',
                            Html::tag('i', '', ['class' => 'icon_stat_1'])
                            . ' ' . $formatter->asMoney($cash + $cashSurchargeValue - $cashTaxValue, $currency));
                        $lines[] = Html::tag('span',
                            Html::tag('i', '', ['class' => 'icon_stat_2'])
                            . ' ' . $formatter->asMoney(($value - $cash) + ($surchargeValue - $cashSurchargeValue)
                                - ($taxValue - $cashTaxValue), $currency));
                    }
                }

                return implode('<br>', $lines);
            },
            'headerOptions'  => ['style' => 'text-align: right; padding-right: 10px;'],
        ],
        [
            'label'          => t('reports', 'Income'),
            'contentOptions' => function ($model, $key, $index, $column) {
                return ['style' => 'text-align: right; padding-right: 10px;'];
            },
            'content'        => function ($model) use (
                &$summary,
                &$cashSummary,
                &$surcharge,
                &$cashSurcharge,
                &$tax,
                &$cashTax,
                &$commission,
                &$cashCommission
            ) {
                $lines = [];
                if (is_array($summary)) {
                    /* @var Formatter */
                    $formatter = app()->formatter;

                    /* @var $summary array */
                    foreach ($summary as $currencyId => $value) {
                        $currency = Currency::getCurrencySymbol($currencyId);
                        $cash     = isset($cashSummary[$currencyId]) ? $cashSummary[$currencyId] : 0;

                        $surchargeValue      = isset($surcharge[$currencyId]) ? $surcharge[$currencyId] : 0;
                        $cashSurchargeValue  = isset($cashSurcharge[$currencyId]) ? $cashSurcharge[$currencyId] : 0;
                        $taxValue            = isset($tax[$currencyId]) ? $tax[$currencyId] : 0;
                        $cashTaxValue        = isset($cashTax[$currencyId]) ? $cashTax[$currencyId] : 0;
                        $commissionValue     = isset($commission[$currencyId]) ? $commission[$currencyId] : 0;
                        $cashCommissionValue = isset($cashCommission[$currencyId]) ? $cashCommission[$currencyId] : 0;

                        $lines[] = Html::tag('b',
                            $formatter->asMoney($value + $surchargeValue - $taxValue - $commissionValue, $currency));
                        $lines[] = Html::tag('span',
                            Html::tag('i', '', ['class' => 'icon_stat_1'])
                            . ' ' . $formatter->asMoney($cash + $cashSurchargeValue - $cashTaxValue - $cashCommissionValue,
                                $currency));
                        $lines[] = Html::tag('span',
                            Html::tag('i', '', ['class' => 'icon_stat_2'])
                            . ' ' . $formatter->asMoney(($value - $cash) + ($surchargeValue - $cashSurchargeValue)
                                - ($taxValue - $cashTaxValue) - ($commissionValue - $cashCommissionValue),
                                $currency));
                    }
                }

                return implode('<br>', $lines);
            },
            'headerOptions'  => ['style' => 'text-align: right; padding-right: 10px;'],
        ],
    ],
]);

Pjax::end();