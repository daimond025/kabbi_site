<?php

use app\modules\order\models\OrderStatus;
use common\components\formatter\Formatter;
use common\helpers\DateTimeHelper;
use frontend\modules\tenant\models\Currency;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider ActiveDataProvider */
/* @var $active bool */

echo Html::tag('h2', $active ? t('reports', 'Active shift') : t('reports', 'Finished shifts'));

$reportTime  = time();
$shiftTime   = $pauseTime = $blockTime = $lateTime = $lateCount
    = $orderPaymentTime = $orderPaymentCount = $completedCount
    = $rejectedCount = $acceptedOfferCount = $rejectedOfferCount = 0;
$shiftEvents = $summary = $cashSummary = $surcharge = $cashSurcharge
    = $tax = $cashTax = $commission = $cashCommission = [];
$carMileage  = 0;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'       => "{items}",
    'tableOptions' => [
        'class' => 'people_table',
    ],
    'beforeRow'    => function ($model) use (
        &$reportTime,
        &$shiftTime,
        &$pauseTime,
        &$blockTime,
        &$lateTime,
        &$lateCount,
        &$orderPaymentTime,
        &$orderPaymentCount,
        &$completedCount,
        &$rejectedCount,
        &$acceptedOfferCount,
        &$rejectedOfferCount,
        &$shiftEvents,
        &$summary,
        &$cashSummary,
        &$surcharge,
        &$cashSurcharge,
        &$tax,
        &$cashTax,
        &$commission,
        &$cashCommission,
        &$carMileage
    ) {
        $shiftTime  = $pauseTime = $blockTime = $lateTime = $lateCount
            = $orderPaymentTime = $orderPaymentCount = $completedCount
            = $rejectedCount = $acceptedOfferCount = $rejectedOfferCount = 0;
        $summary    = $cashSummary = $surcharge = $cashSurcharge
            = $tax = $cashTax = $commission = $cashCommission = [];
        $carMileage = 0;

        $shiftTime          += ($model->end_work !== null ? $model->end_work : $reportTime) - $model->start_work;
        $lateTime           += $model->worker_late_time;
        $lateCount          += $model->worker_late_count;
        $orderPaymentTime   += $model->order_payment_time;
        $orderPaymentCount  += $model->order_payment_count;
        $completedCount     += $model->completed_order_count;
        $rejectedCount      += $model->rejected_order_count;
        $acceptedOfferCount += $model->accepted_order_offer_count;
        $rejectedOfferCount += $model->rejected_order_offer_count;

        $pauses   = $model->pause_data;
        $shiftEnd = isset($model->end_work) ? $model->end_work : $reportTime;

        if (is_array($pauses)) {
            foreach ($pauses as $pause) {
                $label = isset($pause['pause_reason']) ? $pause['pause_reason'] : '';

                $pauseStart = (isset($pause['pause_start']) ? $pause['pause_start'] : $shiftEnd);
                $pauseEnd   = (isset($pause['pause_end']) ? $pause['pause_end'] : $shiftEnd);

                $shiftEvents[$pauseStart] = [
                    'type'       => 'pause',
                    'startTime'  => $pauseStart,
                    'finishTime' => $pauseEnd,
                    'label'      => $label,
                ];

                $pauseTime += $pauseEnd - $pauseStart;
            }
        }

        if (is_array($model->shiftOrders)) {
            foreach ($model->shiftOrders as $shiftOrder) {
                $order      = $shiftOrder->order;
                $detailCost = $order->detailCost;

                $summaryCost     = (float)(!empty($detailCost->summary_cost) ? $detailCost->summary_cost : 0);
                $surchargeValue  = (float)(!empty($detailCost->surcharge) ? $detailCost->surcharge : 0);
                $taxValue        = (float)(!empty($detailCost->tax) ? $detailCost->tax : 0);
                $commissionValue = (float)(!empty($detailCost->commission) ? $detailCost->commission : 0);

                $summary[$order->currency_id]     += $summaryCost;
                $cashSummary[$order->currency_id] += $order->payment == $order::PAYMENT_CASH ? $summaryCost : 0;

                if (in_array($order->status_id,
                    [OrderStatus::STATUS_COMPLETED_PAID, OrderStatus::STATUS_COMPLETED_NOT_PAID], false)) {
                    $surcharge[$order->currency_id]      += $surchargeValue;
                    $cashSurcharge[$order->currency_id]  += $order->payment == $order::PAYMENT_CASH ? $surchargeValue : 0;
                    $tax[$order->currency_id]            += $taxValue;
                    $cashTax[$order->currency_id]        += $order->payment == $order::PAYMENT_CASH ? $taxValue : 0;
                    $commission[$order->currency_id]     += $commissionValue;
                    $cashCommission[$order->currency_id] += $order->payment == $order::PAYMENT_CASH ? $commissionValue : 0;
                }
            }
        }

        $carMileage += !empty($model->carMileage->end_value)
            ? (float)$model->carMileage->end_value - (float)$model->carMileage->begin_value : 0;
    },
    'afterRow'     => function ($model) use (&$shiftEvents, &$shiftTime, &$pauseTime, &$blockTime) {
        $timeOffset = empty($model->city->republic->timezone) ? 0 : $model->city->republic->timezone * 3600;

        $pauseTimeClone   = $pauseTime;
        $shiftTimeClone   = $shiftTime;
        $blockTimeClone   = $blockTime;
        $shiftEventsClone = $shiftEvents;

        $pauseTime = $shiftTime = $blockTime = 0;
        $shiftEvents = [];

        return $this->render('_shift_info', [
            'id'                          => $model->id,
            'startWork'                   => $model->start_work,
            'endWork'                     => $model->end_work,
            'shift_end_reason'            => $model->shift_end_reason,
            'shift_end_event_sender_type' => $model->shift_end_event_sender_type,
            'shift_end_event_sender_id'   => $model->shift_end_event_sender_id,
            'shiftTime'                   => $shiftTimeClone,
            'pauseTime'                   => $pauseTimeClone,
            'blockTime'                   => $blockTimeClone,
            'timeOffset'                  => $timeOffset,
            'events'                      => $shiftEventsClone,
        ]);
    },
    'columns'      => [
        [
            'label'          => t('reports', 'Date'),
            'contentOptions' => function ($model, $key, $index, $column) {
                return ['style' => 'font-size: 14px'];
            },
            'content'        => function ($model) use (&$active) {
                /* @var $formatter Formatter */
                $formatter = app()->formatter;

                $timeZone  = empty($model->city->republic->timezone) ? 0 : $model->city->republic->timezone;
                $startWork = $model->start_work + $timeZone * 3600;
                $endWork   = $model->end_work + $timeZone * 3600;

                $lines   = [];
                $lines[] = implode(', ', [
                    $formatter->asDate($startWork, 'shortDate'),
                    $formatter->asTime($startWork, 'short'),
                ]);

                if ($active) {
                    $lines[] = Html::a(t('reports', 'End shift'), [
                    ], [
                        'data-href'      => '/employee/worker/end-work',
                        'data-worker_id' => $model->worker_id,
                        'data-shift_id'  => $model->id,
                        'class'          => 'pt_del js-worker-end-work',
                        'style'          => 'white-space: nowrap',
                        'data-message'   => t('reports', 'Are you sure you want to end shift?'),
                    ]);
                } else {
                    $lines[] = implode(', ', [
                        $formatter->asDate($endWork, 'shortDate'),
                        $formatter->asTime($endWork, 'short'),
                    ]);
                }

                $lines[] = Html::encode($model->city->name) . ' (' . ($timeZone > 0 ? '+' : '') . $timeZone . ')';

                return implode('<br>', $lines);
            },
            'headerOptions'  => ['style' => 'width: 11%'],
        ],
        [
            'label'         => t('reports', 'Tariff and car'),
            'content'       => function ($model) {
                $lines   = [];
                $lines[] = Html::encode($model->tariff->name);
                $lines[] = Html::encode(implode(' ', [$model->car->brand, $model->car->model])) . ' '
                    . Html::tag('span', Html::encode($model->car->gos_number), ['dir' => 'auto']);

                return implode('<br>', $lines);
            },
            'headerOptions' => ['style' => 'width: 15%'],
        ],
        [
            'label'   => t('reports', 'Group and priority'),
            'content' => function ($model) {
                $lines   = [];
                $lines[] = Html::encode(isset($model->group->name) ? $model->group->name : '-');
                $lines[] = Html::encode(t('reports', 'Group priority') . ': ' . (int)$model->group_priority);
                $lines[] = Html::encode(t('reports', 'Personal priority') . ': ' . (int)$model->position_priority);

                return implode('<br>', $lines);
            },
        ],
        [
            'label'   => t('reports', 'Shift'),
            'content' => function ($model) use (&$shiftTime, &$pauseTime) {
                $lines   = [];
                $lines[] = Html::tag('i', '', ['class' => 'icon_play'])
                    . ' ' . Html::tag('span',
                        Html::a(DateTimeHelper::getFormatTimeFromSeconds($shiftTime),
                            '#dr_hist_' . $model->id, ['class' => 'driver_history']));
                $lines[] = Html::tag('i', '', ['class' => 'icon_pause'])
                    . ' ' . Html::tag('span',
                        Html::a(DateTimeHelper::getFormatTimeFromSeconds($pauseTime),
                            '#dr_pause_' . $model->id, ['class' => 'driver_history']));

                return implode('<br>', $lines);
            },
        ],
        [
            'label'   => t('reports', 'Blocks (short)'),
            'content' => function ($model) use (&$shiftEvents, &$blockTime, &$reportTime) {
                $lines = [];

                if (is_array($model->blocks)) {
                    foreach ($model->blocks as $block) {
                        if (empty($block->is_unblocked) && $reportTime < $block->end_block) {
                            $blockTime += $block->end_block - $reportTime;
                        } else {
                            $blockTime += $block->end_block - $block->start_block;
                        }

                        $shiftEvents[$block->start_block] = [
                            'type'       => 'block',
                            'startTime'  => $block->start_block,
                            'finishTime' => $block->end_block,
                            'label'      => $block->type_block === 'order'
                                ? t('reports', 'Refusal to order') : t('reports', 'Refusal to pre-order'),
                        ];

                        if (empty($block->is_unblocked) && $reportTime < $block->end_block) {
                            $label   = $block->type_block == 'order'
                                ? t('reports', 'Unblock') : t('reports', 'Unblock (pre-order)');
                            $lines[] = Html::a($label, ['#'], [
                                'data-href'      => '/employee/worker/unblock',
                                'data-worker_id' => $model->worker_id,
                                'data-block_id'  => $block->block_id,
                                'class'          => 'link_red js-worker-unblock',
                                'data-message'   => t('reports', 'Are you sure you want to unblock?'),
                            ]);
                        }
                    }
                }

                array_unshift($lines, Html::tag('span',
                    Html::a(DateTimeHelper::getFormatTimeFromSeconds($blockTime),
                        '#dr_block_' . $model->id, ['class' => 'driver_history link_red'])));

                return implode('<br>', $lines);
            },
        ],
        [
            'label'   => t('reports', 'Lateness'),
            'content' => function ($model) use (&$lateTime, &$lateCount) {
                return Html::tag('span', DateTimeHelper::getFormatTimeFromSeconds($lateTime), ['class' => 'link_red'])
                    . '&nbsp;&nbsp;&nbsp;' . $lateCount;
            },
        ],
        [
            'label'   => t('reports', 'Waiting for payment'),
            'content' => function ($model) use (&$orderPaymentTime, &$orderPaymentCount) {
                return Html::tag('span', DateTimeHelper::getFormatTimeFromSeconds($orderPaymentTime))
                    . '&nbsp;&nbsp;&nbsp;' . $orderPaymentCount;
            },
        ],
        [
            'label'          => t('reports', 'Orders'),
            'contentOptions' => function ($model, $key, $index, $column) {
                return ['style' => 'text-align: right; padding-right: 10px;'];
            },
            'content'        => function ($model) use (&$completedCount, &$rejectedCount) {
                return Html::tag('span', $completedCount, ['class' => 'link_green'])
                    . '&nbsp;&nbsp;' . Html::tag('span', $rejectedCount, ['class' => 'link_red']);
            },
            'headerOptions'  => ['style' => 'text-align: right; padding-right: 10px;'],
        ],
        [
            'label'          => t('reports', 'Offer'),
            'contentOptions' => function ($model, $key, $index, $column) {
                return ['style' => 'text-align: right; padding-right: 10px;'];
            },
            'content'        => function ($model) use (&$acceptedOfferCount, &$rejectedOfferCount) {
                return Html::tag('span', $acceptedOfferCount, ['class' => 'link_green'])
                    . '&nbsp;&nbsp;' . Html::tag('span', $rejectedOfferCount, ['class' => 'link_red']);
            },
            'headerOptions'  => ['style' => 'text-align: right; padding-right: 10px;'],
        ],
        [
            'label'          => t('reports', 'Car mileage'),
            'contentOptions' => function ($model, $key, $index, $column) {
                return ['style' => 'text-align: right; padding-right: 10px;'];
            },
            'content'        => function ($model) use (&$carMileage, $formatter) {
                if ($carMileage !== null) {
                    /* @var $formatter Formatter */
                    $formatter = app()->formatter;

                    return Html::tag('span', $formatter->asDecimal($carMileage, 1));
                }
            },
            'headerOptions'  => ['style' => 'text-align: center; width: 5%'],
        ],
        [
            'label'          => t('reports', 'Summary'),
            'contentOptions' => function ($model, $key, $index, $column) {
                return ['style' => 'text-align: right; padding-right: 10px;'];
            },
            'content'        => function ($model) use (
                &$summary,
                &$cashSummary,
                &$surcharge,
                &$cashSurcharge,
                &$tax,
                &$cashTax
            ) {
                $lines = [];
                if (is_array($summary)) {
                    /* @var Formatter */
                    $formatter = app()->formatter;

                    /* @var $summary array */
                    foreach ($summary as $currencyId => $value) {
                        $currency = Currency::getCurrencySymbol($currencyId);
                        $cash     = isset($cashSummary[$currencyId]) ? $cashSummary[$currencyId] : 0;

                        $surchargeValue     = isset($surcharge[$currencyId]) ? $surcharge[$currencyId] : 0;
                        $cashSurchargeValue = isset($cashSurcharge[$currencyId]) ? $cashSurcharge[$currencyId] : 0;
                        $taxValue           = isset($tax[$currencyId]) ? $tax[$currencyId] : 0;
                        $cashTaxValue       = isset($cashTax[$currencyId]) ? $cashTax[$currencyId] : 0;

                        $lines[] = Html::tag('b', $formatter->asMoney($value + $surchargeValue - $taxValue, $currency));
                        $lines[] = Html::tag('span',
                            Html::tag('i', '', ['class' => 'icon_stat_1'])
                            . ' ' . $formatter->asMoney($cash + $cashSurchargeValue - $cashTaxValue, $currency));
                        $lines[] = Html::tag('span',
                            Html::tag('i', '', ['class' => 'icon_stat_2'])
                            . ' ' . $formatter->asMoney(($value - $cash) + ($surchargeValue - $cashSurchargeValue)
                                - ($taxValue - $cashTaxValue), $currency));
                    }
                }

                return implode('<br>', $lines);
            },
            'headerOptions'  => ['style' => 'text-align: right; padding-right: 10px;'],
        ],
        [
            'label'          => t('reports', 'Income'),
            'contentOptions' => function ($model, $key, $index, $column) {
                return ['style' => 'text-align: right; padding-right: 10px;'];
            },
            'content'        => function ($model) use (
                &$summary,
                &$cashSummary,
                &$surcharge,
                &$cashSurcharge,
                &$tax,
                &$cashTax,
                &$commission,
                &$cashCommission
            ) {
                $lines = [];
                if (is_array($summary)) {
                    /* @var Formatter */
                    $formatter = app()->formatter;

                    /* @var $summary array */
                    foreach ($summary as $currencyId => $value) {
                        $currency = Currency::getCurrencySymbol($currencyId);
                        $cash     = isset($cashSummary[$currencyId]) ? $cashSummary[$currencyId] : 0;

                        $surchargeValue      = isset($surcharge[$currencyId]) ? $surcharge[$currencyId] : 0;
                        $cashSurchargeValue  = isset($cashSurcharge[$currencyId]) ? $cashSurcharge[$currencyId] : 0;
                        $taxValue            = isset($tax[$currencyId]) ? $tax[$currencyId] : 0;
                        $cashTaxValue        = isset($cashTax[$currencyId]) ? $cashTax[$currencyId] : 0;
                        $commissionValue     = isset($commission[$currencyId]) ? $commission[$currencyId] : 0;
                        $cashCommissionValue = isset($cashCommission[$currencyId]) ? $cashCommission[$currencyId] : 0;

                        $lines[] = Html::tag('b',
                            $formatter->asMoney($value + $surchargeValue - $taxValue - $commissionValue, $currency));
                        $lines[] = Html::tag('span',
                            Html::tag('i', '', ['class' => 'icon_stat_1'])
                            . ' ' . $formatter->asMoney($cash + $cashSurchargeValue - $cashTaxValue - $cashCommissionValue,
                                $currency));
                        $lines[] = Html::tag('span',
                            Html::tag('i', '', ['class' => 'icon_stat_2'])
                            . ' ' . $formatter->asMoney(($value - $cash) + ($surchargeValue - $cashSurchargeValue)
                                - ($taxValue - $cashTaxValue) - ($commissionValue - $cashCommissionValue),
                                $currency));
                    }
                }

                return implode('<br>', $lines);
            },
            'headerOptions'  => ['style' => 'text-align: right; padding-right: 10px;'],
        ],
    ],
]);

?>