<?php

use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
//$bundle = \app\modules\driver\assets\DriverAsset::register($this);
//$this->registerJsFile($bundle->baseUrl . '/main.js');

$this->title = Yii::t('app', 'Operators');
?>
<h1><?= Html::encode($this->title) ?></h1>

<section class="main_tabs">
    <div class="tabs_content">
        <div  id="t01" data-view="index">
            <?if(!empty($operators)):?>
            <form name="user_search" method="post" onsubmit="return false" action="<?=Url::to('/reports/operator/search/')?>">
                <div class="grid_3" data-activity="active">
                    <div class="grid_item">
                        <div class="select_checkbox">
                            <a class="a_sc select" rel="<?=t('user', 'All cities')?>"><?=t('user', 'All cities')?></a>
                            <div class="b_sc">
                                <ul>
                                    <?foreach ($city_list as $city_id => $city):?>
                                        <li><label><input name="city_id[]" type="checkbox" value="<?=$city_id?>"/> <?= Html::encode($city); ?></label></li>
                                    <?endforeach;?>
                                </ul>     
                            </div>    
                        </div>
                    </div>
                    <div class="grid_item">
                        <input name="input_search" type="text" placeholder="<?=t('user', 'Search by name')?>" value=""/>
                    </div>
                </div>
            </form>
            <table class="people_table sort">
                <tr>
                    <th class="pt_fio"><a href="#"><?= t('user', 'Last name') . ' ' . t('user', 'Name') . ' ' . t('user', 'Second name') ?> <span data-type="name" class="pt_down"></span></a></th>
                    <th class="pt_city"><?= t('user', 'City') ?></th>
                </tr>
                <?= $this->render('_index', ['operators' => $operators]); ?>
            </table>
            <?else:?>
            <p><?= t('app', 'Empty') ?></p>
            <?endif?>
        </div>   
        <div data-view="invited" id="t02">
        </div>  
        <div data-view="blocked" id="t03">
        </div>  
    </div>
</section>