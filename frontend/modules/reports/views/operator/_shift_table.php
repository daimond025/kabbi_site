<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use common\helpers\DateTimeHelper;
use frontend\modules\tenant\models\Currency;

$formatter = app()->formatter;


?>

<?php if (!empty($shifts)): ?>

<h2 style="margin-bottom: 10px;">
    <?= $active ? t('reports', 'Active shift') : t('reports', 'Finished shifts'); ?>
</h2>

<table class="driver_history_table order_table">
    <tr>
        <th style="width: 12%; padding-right: 2%;"><?=t('reports', 'Shift')?></th>
        <th style="width: 8%; padding-right: 2%;"><?=t('reports', 'Pauses')?></th>
        <th style="width: 8%; padding-right: 2%; text-align: right;"><?=t('reports', 'Calls')?></th>
        <th style="width: 8%; padding-right: 2%; text-align: right;"><?=t('reports', 'Orders')?></th>
        <th style="width: 8%; padding-right: 2%; text-align: right;"><?=t('reports', 'Money')?></th>
        <th style="width: 13%; padding-right: 2%; text-align: right;"><?=t('reports', 'The total time of calls')?></th>
        <th style="width: 13%; padding-right: 2%; text-align: right;"><?=t('reports', 'Average talk time')?></th>
        <th style="width: 15%; padding-right: 2%; text-align: right;">
        <?php if ($active): ?>
            <?= t('reports', 'Actions'); ?>
        <?php else: ?>
            <?= t('reports', 'Workload'); ?>
            <a data-tooltip="7" class="tooltip">?<span style="display: none; opacity: 0;"><?= HtmlPurifier::process(Yii::t('hint','Показывает отношение времени разговора к времени смены')); ?></span></a>
        <?php endif; ?>
        </th>
    </tr>
    <?php 
    foreach ($shifts as $shift): 
        
        $endWork = $active ? time() : $shift['end_work'];
        $summary_time = $endWork - $shift['start_work'];
        $work_time = DateTimeHelper::getFormatTimeFromSeconds($summary_time);
                
        $summary_call_time = $shift['summ_income_calls_time'] + $shift['summ_outcome_calls_time']; // общее время звонков в секундах
        $count_call = $shift['count_income_calls'] + $shift['count_outcome_calls']; // количество звонков
        $average_call = $count_call == 0 ? 0 : round($summary_call_time / $count_call); // среднее время звонков
        
        $pause = unserialize($shift['pause_data']);
        
        $summary_pause = 0;
        foreach ($pause as $item) {
            $summary_pause += $item['pause_end'] - $item['pause_start'];
        }
        
        $summ_completed = [];
        $summ_completed_arr = is_null($shift['summ_completed_arr']) ? [] : unserialize($shift['summ_completed_arr']);
        foreach ($summ_completed_arr as $item) {
            $summ_completed[ $item['currency_id'] ] += $item['value']; 
        }
        
        $summ_rejected = [];
        $summ_rejected_arr = is_null($shift['summ_rejected_arr']) ? [] : unserialize($shift['summ_rejected_arr']);
        foreach ($summ_rejected_arr as $item) {
            $summ_rejected[ $item['currency_id'] ] += $item['value']; 
        }
    ?>
    
    <tr>
        <td style="padding-right: 2%;">
            
            <div style="display: none">
                <div id="op_hist_<?= $shift['id']; ?>" class="dr_hist">
                    
                    <h2><?=t('reports', 'Shift')?></h2>
                    <ul>
                        <li><span><?=date('d.m.Y,H:i', $shift['start_work'])?></span><i><?=t('reports', 'Start shift')?></i></li>
                        
                        <?php foreach ($pause as $item): 
                            $pauseStart = $item['pause_start'];
                        
                            if (!empty($item['pause_end'])) {
                                $pauseEnd = $item['pause_end'];
                            } elseif (!empty($shift['end_work'])) {
                                $pauseEnd = $shift['end_work'];
                            } else {
                                $pauseEnd = time();
                            } ?>
                        
                            <li>
                                <span> <?=date('H:i', $pauseStart) ?>–<?= date('H:i', $pauseEnd) ?></span>
                                <i><?= Html::encode( t('user_pause', $item['pause_reason']) ); ?></i>
                            </li>
                        <?php endforeach; ?>
                            
                        <li><span><?= $active ? '' : date('d.m.Y,H:i', $shift['end_work'])?></span><i><?=t('reports', 'Finish shift')?></i></li>
                        <li>
                            <span><b><?= Html::encode($work_time); ?></b><br/><b><?= DateTimeHelper::getFormatTimeFromSeconds($summary_pause); ?></b></span>
                            <i><b><?=t('reports', 'Shift')?></b><br/><b><?=t('reports', 'Pauses')?></b></i>
                        </li>
                    </ul>
                    
                    
                    
                </div>
            </div>
            <a href="#op_hist_<?= $shift['id']; ?>" class="dr_hist">
                <?= Html::encode($work_time); ?>
            </a>
            <i>
                <?= implode(", ", [
                        $formatter->asDate($shift['start_work'], 'shortDate'),
                        $formatter->asTime($shift['start_work'], 'short'),
                    ]); ?>
            </i>
            <i>
                <?= $active ? ''
                    : implode(", ", [
                        $formatter->asDate($shift['end_work'], 'shortDate'),
                        $formatter->asTime($shift['end_work'], 'short'),
                    ]);?>
            </i>
        </td>
        <td style="padding-right: 2%;">
            <a href="#op_hist_<?= $shift['id']; ?>" class="dr_hist">
                <?= DateTimeHelper::getFormatTimeFromSeconds($summary_pause);?>
            </a>
        </td>
        <td style="text-align: right; padding-right: 2%;">
            <span class="link_green"><?= intval($shift['count_income_answered_calls'] + $shift['count_outcome_answered_calls']); ?></span><br/><span class="link_red"><?= intval($shift['count_income_noanswered_calls'] + $shift['count_outcome_noanswered_calls']); ?></span>
        </td>
        <td style="text-align: right; padding-right: 2%;">
            <span class="link_green"><?= intval($shift['count_completed_orders']); ?></span><br/><span class="link_red"><?= intval($shift['count_rejected_orders']); ?></span>
        </td>
        <td style="text-align: right; padding-right: 2%;">
            <?php foreach ($summ_completed as $currency_id => $value): ?>
                <span class="link_green">+<?= $value; ?> <?= Currency::getCurrencySymbol($currency_id); ?></span><br/>
            <?php endforeach; ?>
            <?php foreach ($summ_rejected as $currency_id => $value): ?>
                <span class="link_red">-<?= $value; ?> <?= Currency::getCurrencySymbol($currency_id); ?></span><br/>
            <?php endforeach; ?>
        </td>
        <td style="text-align: right; padding-right: 2%;">
            <?= DateTimeHelper::getFormatTimeFromSeconds($summary_call_time); ?>
        </td>
        <td style="text-align: right; padding-right: 2%;">
            <?= DateTimeHelper::getFormatTimeFromSeconds($average_call); ?>
        </td>
        <td style="text-align: right; padding-right: 2%;">
        <?php if ($active): ?>
        
            <?= Html::a(t('reports', 'End shift'), [
                    ], [
                        'data-href' => '/operator/operator/end-work',
                        'data-operator_id' => $shift['user_id'],
                        'class' => 'pt_del js-operator-end-work',
                        'style' => 'white-space: nowrap',
                        'data-message' => t('reports', 'Are you sure you want to end shift?')
                ]); ?>
        <?php else: ?>
            <?= $summary_time !== 0 ? round( 1000 * $summary_call_time / $summary_time ) / 10 : 0 ; ?>%
        <?php endif; ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
<?php endif; ?>
