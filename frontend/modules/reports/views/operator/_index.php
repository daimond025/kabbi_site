<?php

use yii\helpers\Url;
use yii\helpers\Html;
//dd($operators);
?>

<? foreach ($operators as $operator): ?>
    <tr>
        <td class="pt_fio">
            <a href="<?= Url::toRoute(['view', 'id' => $operator['ID']]) ?>">
                <span class="pt_photo">
                    <?= $operator['PHOTO'] ?>
                </span>
                <span class="pt_fios"><?= Html::encode($operator['NAME']); ?></span>
                <span class="pt_<?= Html::encode($operator['ONLINE']); ?>line"></span>
            </a>
        </td>
        <td>
            <div class="pt_buttons">
                <?if ($operator['ID'] !== user()->user_id):?>
                    <a href="#" class="pt_message" data-receivertype="user" data-receiverid="<?= $operator['ID'] ?>" data-cityid="<?= $operator['CHAT_CITY_ID'] ?>"><span></span></a>
                <?endif?>
               <!--a href="#" class="pt_call"><span></span></a-->
            </div>
    <?= Html::encode($operator['CITIES']); ?>
        </td>
    </tr>
<? endforeach; ?>