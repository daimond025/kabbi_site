<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\balance\models\Account;

$reportAsset = app\modules\reports\assets\ReportsAsset::register($this);
$this->registerJsFile($reportAsset->baseUrl . '/operators.js');
//$this->registerJs('openDetailRowsInit();');

\frontend\modules\balance\assets\BalanceAsset::register($this);
//$this->registerJs('balanceOperationInit();');

$this->title = trim($operator->user->last_name . ' ' . $operator->user->name . ' ' . $operator->user->second_name);
?>
<div class="bread"><a href="<?= Url::toRoute('/reports/operator/index') ?>"><?= t('app', 'Operators') ?></a></div>
<h1><?= Html::encode($this->title) ?></h1>
<section class="main_tabs">
    <div class="tabs_links">
        <ul>
            <li>
                <a href="#shifts" class="t01"><?= t('reports', 'Shifts') ?></a>
            </li>
            <li>
                <a href="#calls" class="t02"><?= t('reports', 'Calls') ?></a>
            </li>
        </ul>
    </div>
    
    <div class="tabs_content">
        <div id="t01">
        <?=
        $this->render('_filter', [
              'action' => \yii\helpers\Url::to(['/reports/operator/show-shifts', 'id' => get('id')]),
              'form_name' => 'shift_search'])
        ?>    
            <div id="shift_stat"></div>
        </div>
        <div id="t02">
        <?=
        $this->render('_filter', [
              'action' => \yii\helpers\Url::to(['/reports/operator/show-calls', 'id' => get('id')]),
              'form_name' => 'call_search'])
        ?>
            <div id="call_stat"></div>
        </div>
    </div>
</section>