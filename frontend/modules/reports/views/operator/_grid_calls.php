<?php

use yii\grid\GridView;
use frontend\widgets\LinkPager;
use yii\widgets\Pjax;
use app\modules\order\models\Call;

/**
 * @var yii\web\View                 $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

Pjax::begin(['id' => 'pjax-calls-table', 'enablePushState' => false]);

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'layout'       => "{items}\n{pager}",
        'tableOptions' => [
            'class' => 'people_table',
        ],
        'pager'        => [
            'class'         => LinkPager::className(),
            'prevPageLabel' => t('app', 'Prev'),
            'nextPageLabel' => t('app', 'Next'),
        ],
        'columns'      => [
            [
                'attribute'     => 'client',
                'label'         => t('app', 'Client'),
                'content'       => function (Call $model) {
                    return $this->render('call/_client', compact('model'));
                },
                'headerOptions' => ['class' => 'pt_fio', 'style' => 'width:24%'],
            ],

            [
                'attribute'     => 'phoneLine',
                'label'         => t('app', 'Phone line'),
                'content'       => function (Call $model) {
                    return $model->dispatcher_phone;
                },
                'headerOptions' => ['style' => 'width:13%'],
            ],

            [
                'attribute' => 'type',
                'label'     => t('reports', 'Type'),
                'content'   => function (Call $model) {
                    return t('reports', $model->type);
                },
                'headerOptions' => ['style' => 'width:10%'],
            ],

            [
                'attribute' => 'file',
                'label'     => t('reports', 'Call'),
                'content'   => function (Call $model) {
                    return $this->render('call/_file', compact('model'));
                },
                'headerOptions' => ['style' => 'width:25%'],
            ],

            [
                'attribute' => 'status',
                'label'     => t('reports', 'Status'),
                'content'   => function (Call $model) {
                    return $this->render('call/_status', compact('model'));
                },
                'headerOptions' => ['style' => 'width:12%'],
            ],

            [
                'attribute' => 'order',
                'label'     => t('reports', 'Order'),
                'content'   => function (Call $model) {
                    return $this->render('call/_order', compact('model'));
                },
                'headerOptions' => ['class' => 'pt_city', 'style' => 'width:15%'],
            ],
        ],
    ]);
} catch (Exception $e) {
}

Pjax::end();