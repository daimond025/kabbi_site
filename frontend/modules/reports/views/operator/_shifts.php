<?php

use yii\helpers\Html;
//dd($operatorShifts);
/* @var $this yii\web\View */
//Модальное окно смены
$this->registerJs("$('a.dr_hist').colorbox({inline: true, width: \"300px\"});");
?>

    <?php
    if (empty($operatorShifts) ||
            (empty($operatorShifts['activeShifts']) && empty($operatorShifts['operatorShifts']))) {
        echo Html::tag('p', t('app', 'Empty'));
    } else {
        echo $this->render('_shift_table', [
            'shifts' => $operatorShifts['activeShifts'],
            'active' => true,
        ]);

        echo $this->render('_shift_table', [
            'shifts' => $operatorShifts['operatorShifts']]);
    }
    ?>