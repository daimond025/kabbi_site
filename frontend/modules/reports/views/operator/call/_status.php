<?php

use app\modules\order\models\Call;

/** @var \app\modules\order\models\Call $model */

$status = $model->getStatus();

switch ($status) {
    case Call::STATUS_RECEIVED :
        echo t('call', 'Accepted');
        echo '<i class="incoming_call"></i>';
        break;
    case Call::STATUS_MISSED :
        echo t('call', 'Missed');
        echo '<i class="missed_call"></i>';
        break;
    case Call::STATUS_SUCCESS :
        echo t('call', 'Reached');
        echo '<i class="outgoing_call"></i>';
        break;
    case Call::STATUS_UNSUCCESS :
        echo t('call', 'Can\'t reached');
        echo '<i class="outgoing_call"></i>';
        break;
    case Call::STATUS_BUSY :
        echo t('call', 'Busy');
        echo '<i class="outgoing_call"></i>';
        break;
    default:
        echo t('call', 'Undefined');
}
