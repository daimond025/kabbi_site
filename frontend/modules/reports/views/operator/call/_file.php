<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/** @var \app\modules\order\models\Call $model */

$callArray = $model->getCallAsArray();
?>
<div class="call_record">

    <? if (ArrayHelper::getValue($callArray, 'DURATION', 0) > 0): ?>
        <?= Html::tag('audio', '', [
            'preload'  => 'none',
            'type'     => 'audio/wav',
            'controls' => true,
            'src'      => '/file/call-record/' . $model->id,
        ]); ?>
    <? endif; ?>

    <div>
        <?= ArrayHelper::getValue($callArray, 'DURATION_AS_TIME') ?>
        <span>
        <?= ArrayHelper::getValue($callArray, 'DATE') ?><br>
            <?= ArrayHelper::getValue($callArray, 'START_TIME') ?>
    </span>
    </div>
</div>
