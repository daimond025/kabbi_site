<?php

use frontend\modules\tenant\models\Currency;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/** @var \app\modules\order\models\Call $model */

$order = $model->getOrderAsArray();

if ($order) : ?>
    <span
            class="link_green">+<?= ArrayHelper::getValue($order,
            'PRICE.VALUE'); ?> <?= Currency::getCurrencySymbol(ArrayHelper::getValue($order,
            'PRICE.CURRENCY')) ?></span>
    <br/>
    <a class="open_order" href="<?= Url::to([
        "/order/order/view",
        'order_number' => ArrayHelper::getValue($order, 'NUMBER'),
    ]) ?>"><?= t('reports', 'Order'); ?> №<?= Html::encode(ArrayHelper::getValue($order, 'NUMBER')); ?></a>
<?php endif; ?>
