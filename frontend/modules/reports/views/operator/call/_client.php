<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/** @var \app\modules\order\models\Call $model */

$clientArray = $model->getClientAsArray();

if (ArrayHelper::getValue($clientArray, 'PHONE', false)) {
    if (ArrayHelper::getValue($clientArray, 'ID')) {
        echo Html::a(
            '<span class="pt_photo">' . ArrayHelper::getValue($clientArray, 'PHOTO') . '</span>'
            . '<span class="pt_fios">'
            . (ArrayHelper::getValue($clientArray, 'FULL_NAME', false) ? ArrayHelper::getValue($clientArray,
                    'FULL_NAME', '') . '</span><span class="table_tel">' : '')
            . ArrayHelper::getValue($clientArray, 'PHONE') . '</span>',
            ['/client/base/update/' . ArrayHelper::getValue($clientArray, 'ID')]);
    } else {
        echo Html::tag('span', ArrayHelper::getValue($clientArray, 'PHOTO'), ['class' => 'pt_photo']);
        echo Html::tag('span', ArrayHelper::getValue($clientArray, 'PHONE'), ['class' => 'pt_fios']);
    }
}
