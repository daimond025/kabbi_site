<?php
/* @var $this yii\web\View */
/* @var $company app\modules\client\models\ClientCompany */

use yii\helpers\Url;

\frontend\assets\LeafletAsset::register($this);
$reportAsset = app\modules\reports\assets\ReportsAsset::register($this);
$this->registerJsFile($reportAsset->baseUrl . '/company.js');
?>
<?= $this->render('/company/_filter', ['form_name' => 'stat_filter', 'action' => Url::to(['/reports/company/show-stat', 'id' => get('id')])]) ?>
<section class="main_tabs">
    <div class="tabs_links" data-tabs-id="10">
        <ul>
            <li class="active"><a href="#orders" class="t10"><?= t('reports', 'Orders') ?></a></li>
            <li class=""><a href="#staff" class="t11"><?= t('reports', 'Staff') ?></a></li>
        </ul>
    </div>
    <div class="tabs_content" data-tabs-content-id="10">
        <div id="t10" class="active" data-action="orders">
            <?= $this->render('_orders', compact('orders')) ?>
        </div>
        <div id="t11" class="" data-action="staff">
            <?= $this->render('_staff', compact('staff', 'company')) ?>
        </div>
    </div>
</section>