<?php
$this->registerJs('calendarFilterInit();');
?>
<form name="<?= $form_name ?>" action="<?= $action ?>" method="post">
    <div class="oor_fil">
        <div class="cof_left" style="margin-right: 0;">
            <div class="cof_first">
                <label><input checked="checked" name="filter" type="radio" value="week"> <?= t('reports', 'Week') ?></label>
            </div>

            <div class="cof_second disabled_cof">
                <label><input name="filter" class="cof_time_choser" type="radio" value="period"> <?= t('reports', 'Period') ?></label>
                <div class="cof_date notranslate disabled_select">
                    <a class="s_datepicker a_sc select" id = "first"><?= date("d.m.Y", mktime(0, 0, 0, date("m"), date("d") - 14, date("Y"))); ?></a>
                    <div class="s_datepicker_content b_sc" style="display: none;">
                        <input name="first_date" class="sdp_input" type="text" value="<?= date("d.m.Y", mktime(0, 0, 0, date("m"), date("d") - 14, date("Y"))); ?>">
                        <div class="sdp first_date"></div>
                    </div>
                </div>
                <hr>
                <div class="cof_date notranslate disabled_select">
                    <a class="s_datepicker a_sc select"><?= date("d.m.Y"); ?></a>
                    <div class="s_datepicker_content b_sc" style="display: none;">
                        <input name="second_date" class="sdp_input" type="text" value="<?= date("d.m.Y"); ?>">
                        <div class="sdp"></div>
                    </div>
                </div>
                <a style="margin-left: 15px;" class="button oor_compare" href=""><?= t('reports', 'Show') ?></a>
            </div>
        </div>
    </div>
</form>