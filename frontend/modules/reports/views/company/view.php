<?php
/* @var $this yii\web\View */
/* @var $company app\modules\client\models\ClientCompany */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $company->name;

?>
<div class="bread"><a href="<?= Url::toRoute('/reports/company/index') ?>"><?= t('app', 'Companies') ?></a></div>
<h1><?= Html::encode($this->title) ?></h1>
<?= $this->render('_table', compact('orders', 'staff', 'company'))?>