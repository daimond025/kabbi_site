<?php

use yii\helpers\Html;
use frontend\modules\tenant\models\Currency;

$formatter = app()->formatter;

?>
<? if (!empty($orders)): ?>
    <div class="oor_content">
        <table class="driver_history_table order_table">
            <tr>
                <th style="width: 17%; padding-right: 2%; text-align: left;"><?= t('reports', 'Date and time') ?></th>
                <th style="width: 13%; padding-right: 2%; text-align: left;"><?= t('reports', 'Employee') ?></th>
                <th style="width: 13%; padding-right: 2%;"><?= t('reports', 'Order') ?></th>
                <th style="width: 22%; padding-right: 2%;"><?= t('app', 'Sum') ?></th>
            </tr>
            <?php
            $sum = [];
            foreach ($orders as $order):
                $currencySymbol = Currency::getCurrencySymbol($order->currency_id);
                $sum[$currencySymbol] += $order->detailCost->summary_cost;
                ?>
                <tr>
                    <td style="padding-right: 2%; text-align: left;">
                        <?= $formatter->asDate($order->create_time, 'shortDate') ?>
                        <?= t('app', 'at') ?>
                        <?= $formatter->asTime($order->create_time, 'short') ?>
                    </td>
                    <td style="padding-right: 2%; text-align: left;">
                        <?
                        $clientName = $order->client->getShortName();
                        $clientName = empty($clientName) ? $order->client->clientPhones[0]->value : $clientName;
                        ?>
                        <?= Html::a(Html::encode($clientName), ['/client/base/update', 'id' => $order->client_id], ['style' => ['border-style' => 'solid']])?>
                    </td>
                    <td style="padding-right: 2%;">
                        <?= Html::a('№' . $order->order_number, ['/order/order/view', 'order_number' => $order->order_number], ['class' => 'show_order']) ?>
                    </td>
                    <td style="padding-right: 2%;">
                        <?= $formatter->asMoney(+$order->detailCost->summary_cost, $currencySymbol); ?>
                    </td>
                </tr>
            <? endforeach; ?>
                <tr>
                    <td colspan="3" style="font-weight: bold; padding-right: 2%;">
                        <?= count($orders) . ' ' . t('app', 'pcs.')?>
                    </td>
                    <td style="font-weight: bold; padding-right: 2%;">
                        <?php
                            $summary = null;
                            foreach($sum as $key => $value) {
                                $summary .= (empty($summary) ? '' : ', ')
                                    . $formatter->asMoney(+$value, $key);
                            }
                            echo $summary;
                        ?>
                    </td>
                </tr>
        </table>
    </div>
<? else: ?>
    <p><?= t('app', 'Empty') ?></p>
<? endif; ?>