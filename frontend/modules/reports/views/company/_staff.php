<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use frontend\modules\tenant\models\Currency;

$formatter = app()->formatter;

if (!empty($staff)):
    $spolerid = 1;
    $currencySymbol = getCurrencySymbol($company->city_id);
    ?>
    <div class="oor_content">
        <table class="people_table spoler_table" style="table-layout: fixed;">
            <?foreach($staff as $client_id => $orders):
                $clientName = $orders[0]->client->getShortName();
                $clientName = empty($clientName) ? $orders[0]->client->clientPhones[0]->value : $clientName;

                $sum = [];
                foreach($orders as $order) {
                    $sum[$order->currency_id] += $order->detailCost->summary_cost;
                }
                $summary = null;
                foreach($sum as $key => $value) {
                    $summary .= (empty($summary) ? '' : ', ')
                        . $formatter->asMoney(+$value, Currency::getCurrencySymbol($key));
                }
            ?>
                <tr>
                    <th style="width: 60%; text-align: left;"><a class="open_spoler_tr" data-tr-spoler="<?= $spolerid?>"><?= Html::encode($clientName); ?></a></th>
                    <th style="width: 20%;"><?= count($orders) . ' ' . t('app', 'pcs.')?></th>
                    <th style="width: 20%; text-align: right;"><?= $summary; ?></th>
                </tr>
                <?foreach($orders as $order):?>
                    <tr data-tr-id="<?=$spolerid?>" style="display: none;">
                        <td style="text-align: left;">
                            <?= $formatter->asDate($order->create_time, 'shortDate') ?>
                            <?= t('app', 'at') ?>
                            <?= $formatter->asTime($order->create_time, 'short') ?>
                        </td>
                        <td>
                            <?= Html::a('№' . $order->order_number, ['/order/order/view', 'order_number' => $order->order_number], ['class' => 'show_order', 'style' => 'border-style: dashed;']) ?>
                        </td>
                        <td style="text-align: right;">
                            <?= $formatter->asMoney(+$order->detailCost->summary_cost,
                                    Currency::getCurrencySymbol($order->currency_id)); ?>
                        </td>
                    </tr>
                <?  endforeach;?>
                <?$spolerid++?>
            <?  endforeach;?>
        </table>
    </div>
<? else: ?>
    <p><?= t('app', 'Empty') ?></p>
<? endif; ?>
