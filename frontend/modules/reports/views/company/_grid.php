<?php

use frontend\widgets\filter\Filter;
use frontend\widgets\LinkPager;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

$clientDetailViewUrl = getValue($clientDetailViewUrl, '/client/base/update');

$filter = Filter::begin([
    'pjaxId' => $pjaxId,
    'model'  => $searchModel,
]);
echo $filter->checkboxList('city_id', $cityList);
echo $filter->input('input_search');
Filter::end();

Pjax::begin(['id' => $pjaxId]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'       => "{items}\n{pager}",
    'tableOptions' => [
        'class' => 'people_table',
    ],
    'pager'        => [
        'class'         => LinkPager::className(),
        'prevPageLabel' => t('app', 'Prev'),
        'nextPageLabel' => t('app', 'Next'),
    ],
    'columns'      => [
        [
            'attribute' => 'name',
            'label' => t('client', 'Name'),
            'content' => function ($model) {
                $name_innerHtml = Html::tag('span', $model->isFileExists($model->logo) ? $model->getPictureHtml(Html::encode($model->logo), false) : '', ['class' => 'pt_photo']);
                $name_innerHtml .= Html::tag('span', Html::encode($model->name), ['class' => 'pt_fios']);

                $innerHtml = Html::a($name_innerHtml, ['view', 'id' => $model->company_id], ['data-pjax' => 0]);

                return $innerHtml;
            },
            'headerOptions' => ['class' => 'pt_fio', 'style' => 'width: 35%'],
        ],
        [
            'attribute' => 'block',
            'label' => t('client', 'Activity'),
            'content' => function ($model) {
                return $model->getActiveFieldValue($model->block);
            },
            'headerOptions' => ['style' => 'width: 18%'],
        ],
        [
            'attribute' => 'city_id',
            'label' => t('client', 'City'),
            'content'   => function ($model) use ($cityList) {
                return $cityList[$model->city_id];
            },
            'headerOptions' => ['style' => 'width: 17%'],
        ]
    ],
]);

Pjax::end();
