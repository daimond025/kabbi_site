<?php

/* @var Order[] $orders  */
/* @var array $info */
/* @var array $company */
/* @var array $user */
/* @var integer $fromDate */
/* @var integer $toDate */
/* @var integer $idReport */
/* @var array $currencySymbolMap */

use app\modules\order\models\Order;
use frontend\modules\tenant\models\Currency;
use yii\helpers\ArrayHelper;
use \yii\helpers\Html;
use \app\modules\order\services\OrderService;
use \app\modules\reports\helpers\OrderReportExporterHelper;

?>

<?

$path_icon = Yii::$app->basePath. '/web/images/kabbi/kabbi_icon.png';
$path_icon = file_exists($path_icon) ? $path_icon : '';


$numer_report =  $company['tenant_company_id'].$company['user_contact'] . '00'. $idReport ;


// поезджка отменена
$reject_status =  \app\modules\order\models\OrderStatus::getRejectedStatusId();
$complete_status =  \app\modules\order\models\OrderStatus::getCompletedStatusId();
$complete_not_paid=  [\app\modules\order\models\OrderStatus::STATUS_COMPLETED_NOT_PAID];

// статусы поездок
$orders_hospital = [OrderService::COMPLETED_NOT_PAID];

$orders_other= [OrderService::COMPLETED_PAID];




function RoundValue($number){
    $number = round($number,2,PHP_ROUND_HALF_DOWN) ;

    return number_format((float)$number, 2, '.', '');

}

function FormatValue ($number){
    return number_format((float)$number, 2, '.', '');

}
?>

<!-- todo заготовка  <pagebreak /> -->
<html>
<head>
</head>

<body>

<htmlpageheader name="MyHeader1">
    <div style="font-family: Arial, Helvetica, sans-serif;  padding-left: 0%">
        <table style="width:100%;font-size: 14px;   ">
            <tr>
                <td style="width: 97%"></td>
                <td style="width: 3%; text-align: right; background-image: ">    <img   src="<? echo  $path_icon; ?>"> </td>
            </tr>
        </table>
    </div>
</htmlpageheader>

<htmlpageheader name="MyHeader2">
    <div>
        <table style="width:100%;font-size: 14px;   ">
            <tr>
                <td style="width: 97%"></td>
                <td style="width: 3%; text-align: right"><img   src="<? echo  $path_icon; ?>"></td>
            </tr>
        </table>

        <table style="width:100%;font-size: 14px; margin-top: -45px; border-collapse: collapse;">
            <tr  >
                <th rowspan="2" style="border-bottom: 3px solid #000; text-align: left; vertical-align: bottom; width: 5%">Pos.</th>
                <th rowspan="2" style="border-bottom: 3px solid #000; text-align: left; width: 35%; padding-left: 5px; vertical-align: bottom">Datum, Uhrzeit, Route </th>
                <th></th>
                <th></th>
                <th colspan="2" style="text-align: center; vertical-align: bottom; padding-bottom: -2.5% ">
                    Provisionsbertrag
                </th>
            </tr>
            <tr style="border-bottom: 3px solid #000;">
                <th style="border-bottom: 3px solid #000; text-align: left; vertical-align: bottom"> <p>Fahrpreis</p> <p>Brutto &#8364;</p> </th>
                <th style="border-bottom: 3px solid #000; text-align: left; vertical-align: bottom; padding-left: 2%"> <p>Provision</p> <p>%</p> </th>
                <th style="border-bottom: 3px solid #000; text-align: left; vertical-align: bottom"> <p>Netto in &#8364;</p> </th>
                <th style="border-bottom: 3px solid #000; text-align: left; vertical-align: bottom"> <p>Brutto in &#8364;</p> </th>
            </tr>
        </table>
    </div>
</htmlpageheader>

<htmlpagefooter name="MyFooter1">
    <table style="width:100%; font-size: 11px; margin-top: 5px;  ">
        <tr style="">
            <td style="width: 30%;  border-left: 1px solid #000; text-align: left;  vertical-align: top;padding-left: 7px  ">
                <span style=""> Anschrift </span>
                <p style=" font-weight: bold; "> Meinexpressbus Verwaltungs GmbH </p>
                <p style=" font-weight: bold; "> Fahrtenvermittlung </p>
                <p style=" font-weight: bold; "> Bamberg Strabe 11 </p>
                <p style=" font-weight: bold; "> 96049 Bamberg</p>
            </td>
            <td style="width: 25%;  border-left: 1px solid #000; text-align: left; vertical-align: top; padding-left: 7px ">
                <p style=" margin-left: 5%"> Kommunikation </p>
                <p style="font-weight: bold;"> Tel.: 0951 3 95 95 </p>
                <p style="font-weight: bold; "> Fax.: 0911 527 89 8 87 </p>
                <p style="font-weight: bold;"> Email: info@kabbi.com</p>
            </td>
            <td style="width: 20%;  border-left: 1px solid #000; text-align: left; vertical-align: top; padding-left: 7px">
                <p style=""> StNr </p>
                <p style="font-weight: bold; "> StNr.: 207/204/20008 </p>
            </td>
            <td style="width: 20%;  border-left: 1px solid #000; text-align: left; vertical-align: top; padding-left: 7px ">
                <p style=""> Support </p>
                <p style="font-weight: bold;"> info@kabbi.com </p>
            </td>
        </tr>
    </table>
</htmlpagefooter>

<sethtmlpageheader name="MyHeader1" value="on" show-this-page="1" />
<sethtmlpageheader name="MyHeader2" value="on" />
<sethtmlpagefooter name="MyFooter1" value="on"  />



<div style="font-family: Arial, Helvetica, sans-serif;  padding-left: 0%">
    <table style="width:90%;font-size: 14px;   ">
        <tr>
            <td style="vertical-align: top; width: 40%;">
                <hr align="center" width="500" size="2" color="#000000" />
                <p style="padding-top: 3px; margin-top: -2px; "><?= $company['name']?></p>
                <p style=" margin-bottom: -10px"><?= $company['street']?></p>
                <p style=" margin-bottom: -10px "><?= $company['city_code'] . '  ' . $company['city'] ?></p>
            </td>

            <td style="vertical-align: top; width: 40%; padding-left: 10% ">
                <p style="align-items: right; margin-bottom: -5%; font-weight: bold;">Leistungserbringer: <?= $company['tenant_company_id']?></p>
                <p style="align-items: right; margin-bottom: -5%"> <?= $company['name']?></p>
                <p style="align-items: right; margin-bottom: -5%"> <?= $company['street']?></p>
                <p style="align-items: right; margin-bottom: -5%"> <?= $company['city_code'] . '  ' . $company['city'] ?></p>
                <p style="align-items: right; margin-bottom: -5%">StNr.: <?= $company['tax_number']?></p>

                <? if (!empty($company['site'])) :?>
                    <p style="align-items: right; margin-bottom: -5%"> <?= $company['site']?></p>
                <?endif; ?>
                <p style="align-items: right; margin-bottom: -5%"> <?= $user['email']?></p>

                <p style="align-items: right; margin-bottom: -5%; font-weight: bold; ">Ansprechpartner:</p>
                <p style="align-items: right; margin-bottom: -5%"> <?= $user['name'] . ' ' .$user['last_name'] ?></p>
                <p style="align-items: right; margin-bottom: -5%">Telefon:  <?= $company['phone'] ?></p>
                <p style="align-items: right; margin-bottom: -5%">Fax:  <?= $company['fax'] ?></p>
                <p style="align-items: right; margin-bottom: -5%">Email:  <?= $user['email'] ?></p>

            </td>
        </tr>
    </table>

    <table style="width:100%;font-size: 14px; margin-top: 5px">
        <tr>
            <td style=" width: 70%;font-size: 20px; font-weight: bold; margin-top: 5%; padding-left: 0% ">
                Gutschrift
            </td>
            <td style=" width: 30%; margin-top: 5px; text-align: right">
                Bamberg, den <?= date('d.m.Y') ?>
            </td>

        </tr>
    </table>

    <p>  <?php //Bitte bei Zahlung stets angeben: RG.-Nr. ?> </p>

    <table style="width:100%;font-size: 14px; margin-top: 5px; border-collapse: collapse;">
        <tr >
            <th style="width: 30%; text-align: left; border-bottom: 1px solid #000;"> Leistungsbereich </th>
            <th style="width: 70%; text-align: left; border-bottom: 1px solid #000;"> Leistungserbringer</th>
        </tr>
        <tr>
            <td style="width: 30%;"> <?= date('d.m.Y', $fromDate) . ' - ' .date('d.m.Y', $toDate)  ?> </td>
            <td style="width: 70%;"> <?=$numer_report ?></td>
        </tr>
    </table>

    <table style="width:100%;font-size: 14px; margin-top: 5px; border-collapse: collapse;">
        <tr  >
            <th rowspan="2" style="border-bottom: 3px solid #000; text-align: center; vertical-align: bottom">Pos.</th>
            <th rowspan="2" style="border-bottom: 3px solid #000; text-align: left; width: 35%; padding-left: 5px; vertical-align: bottom">Datum, Uhrzeit, Route </th>
            <th></th>
            <th></th>
            <th colspan="2" style="text-align: center; vertical-align: bottom; padding-bottom: -2.5% ">
                Provisionsbertrag
            </th>
        </tr>
        <tr style="border-bottom: 3px solid #000;">
            <th style="border-bottom: 3px solid #000; text-align: left; vertical-align: bottom"> <p>Fahrpreis</p> <p>Brutto &#8364;</p> </th>
            <th style="border-bottom: 3px solid #000; text-align: left; vertical-align: bottom"> <p>Provision</p> <p>%</p> </th>
            <th style="border-bottom: 3px solid #000; text-align: left; vertical-align: bottom"> <p>Netto in &#8364;</p> </th>
            <th style="border-bottom: 3px solid #000; text-align: left; vertical-align: bottom"> <p>Brutto in &#8364;</p> </th>
        </tr>


        <? $i_pos = 1; ?>
        <?
          // todo суммы налогов
          $summ_all = 0;
          $summ_brutto = 0;
        ?>
        <? foreach ($orders as $order) : ?>
            <?
            // отчет только по выполненным заказам!
            if(!in_array((int)$order->status_id, $complete_status)){
                continue;
            }
            $currencySymbol = ArrayHelper::getValue($currencySymbolMap, $order->currency_id);

            $from = OrderReportExporterHelper::getAddressStart($order->address);
            $to = count($order->address) > 1 ? OrderReportExporterHelper::getAddressEnd($order->address) : '';

            $deteil_cost = 0;
            $cost_netto = 0;

            // расстояние заказа
            $order_distant = 0;

            // по умолчанию - не считаем
            $consider = false;
            if(in_array($order->status_id, $complete_not_paid) || $order->payment == Order::PAYMENT_CARD){
                $consider = true;
            }

            ?>
            <tr>
                <td style="padding-top: 8px; text-align: center"> <?= $i_pos?>. <br> <span style="font-size: 10px">(<?=$order->order_number?>) <span></td>
                <td style="padding-left: 10px;"> <?
                    $date_km = '';
                    if ($time = OrderService::getOrderArriveTime($order)) {
                        $date_km = date('d.m.Y, H:i', $time);
                    }
                    else{
                        $date_km = date('d.m.Y, H:i', $order->order_time);
                    }
                    $date_km .= '/'. ' ' . Html::encode($order->getSummaryDistance()) . ' km';
                    echo $date_km;
                    unset($time);
                    $order_distant = $order->getSummaryDistance();
                    ?>
                </td>
                <td> <?php if(!empty($order->detailCost->summary_cost)) : ?>
                        <?
                        $deteil_cost = $order->detailCost->summary_cost;
                        if($consider){
                            $deteil_cost_show = (-1) * $deteil_cost;
                            $summ_all += $deteil_cost;
                        }else{
                            $deteil_cost_show = $deteil_cost;
                        }
                        ?>
                        <?= FormatValue($deteil_cost_show) ?>

                    <? else: ?>
                        <?
                        $deteil_cost = $order->predv_price;

                        if($consider){
                            $deteil_cost_show = (-1) * $deteil_cost;
                            $summ_all += $deteil_cost;
                        }else{
                            $deteil_cost_show = $deteil_cost;
                        }
                        ?>
                        <?= FormatValue($deteil_cost_show) ?>
                    <?php endif; ?>
                </td>

                <td>
                    <?= number_format((float)$company['percent'], 1, '.', '')  ."%" ?>
                </td>

                <td>
                    <?
                      $percent = (float)$company['percent']/100;
                      $cost_netto =(float)RoundValue($deteil_cost * $percent);
                      echo $cost_netto;
                    ?>
                </td>
                <td>
                    <?
                      $cost_brutto_value =  RoundValue($cost_netto * 1.19);
                      $summ_brutto  += $cost_brutto_value;
                      echo $cost_brutto_value;
                    ?>
                </td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid #000;"></td>
                <td colspan="5" style=" text-align: left; vertical-align: top; padding-left: 10px; border-bottom: 1px solid #000; font-size: 12px">
                    <?
                      echo  OrderReportExporterHelper::getAddressStart($order->address). ' - ' .OrderReportExporterHelper::getAddressEnd($order->address) ;
                    ?>
                </td>
            </tr>
            <? $i_pos += 1;  ?>

        <? endforeach;?>

        <tr style="border-bottom: 3px solid #000;">
            <td colspan="2"  style=" text-align: right; vertical-align: top; padding-right: 10px; font-size: 14px; font-weight: bold; border-bottom: 3px solid #000; padding-top: 13px">
                Summen &#8364;
            </td>
            <td  colspan="2" style=" text-align: left; vertical-align: top; padding-right: 5px; font-size: 14px; font-weight: bold; border-bottom: 3px solid #000; padding-top: 13px">
              <?=  RoundValue($summ_all); ?>
            </td>
            <td style=" text-align: left; vertical-align: top; font-size: 14px;  font-weight: bold; border-bottom: 3px solid #000; padding-top: 13px"> </td>
            <td style=" text-align: left; vertical-align: top; font-size: 14px;  font-weight: bold; border-bottom: 3px solid #000; padding-top: 13px">
                <?=  RoundValue($summ_brutto);  ?>
            </td>
        </tr>

        <tr>
            <td colspan="2"  style=" text-align: right; vertical-align: top; padding-right: 10px; font-size: 14px; font-weight: bold; padding-top: 13px">
                Gesamtbetrag &#8364;
            </td>
            <td> </td>
            <td colspan="3" style=" text-align: center; vertical-align: top; padding-left: 5px;  font-size: 14px;  font-weight: bold; padding-top: 13px">
                <p style="margin-right: -19%"> <?=  RoundValue($summ_all); ?> - <?=  RoundValue($summ_brutto); ?> =  <?=   RoundValue($summ_all - $summ_brutto); ?>    </p>
            </td>

        </tr>

    </table>
</div>
</body>
</html>









