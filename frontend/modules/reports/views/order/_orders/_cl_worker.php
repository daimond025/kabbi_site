<?php

/* @var $model \app\modules\client\models\OrderClientSearch */

/* @var $this yii\web\View */

use yii\helpers\Html;
use frontend\modules\exchange\services\ExchangeProgramService;

if ($model->processed_exchange_program_id) {
    $exchangeWorker = $model->exchangeWorker;
    $exchangeProgram = $model->exchangeProgram;
    if (!empty($exchangeWorker) && !empty($exchangeProgram)) {
        $worker =
            Html::a(
                Html::tag('span', Html::encode($exchangeWorker->name), ['class' => 'pt_fios']));
        $car = '<br/>' . Html::encode($exchangeWorker->car_description)
            . Html::tag('span', Html::encode($exchangeWorker->gos_number), [
                'style' => 'display: block; margin-bottom: 8px',
                'dir'   => 'auto',
            ]);
        echo $worker . HTML::a('', "tel:+{$exchangeWorker->phone}", ['class' => 'order_table_call']) . $car;
        echo ExchangeProgramService::getExchangeProgramNameById($exchangeProgram->exchange_program_id);
    }
} else {
    if (!empty($model->worker)) {
        $worker =
            Html::a(
                Html::tag('span', Html::encode($model->worker->fullName), ['class' => 'pt_fios']),
                ['/employee/worker/update', 'id' => $model->worker_id], ['data-pjax' => 0]);

        if (!empty($model->car)) {
            $car = '<br/>' . Html::a(Html::encode($model->car->name), ['/car/car/update', 'id' => $model->car->car_id])
                . Html::tag('span', Html::encode($model->car->gos_number), [
                    'style' => 'display: block; margin-bottom: 8px',
                    'dir'   => 'auto',
                ]);
        } else {
            $car = '';
        }
        echo $worker . HTML::a('', "tel:+{$model->worker->phone}", ['class' => 'order_table_call']) . $car;
    }
}

