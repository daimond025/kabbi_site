<?php

/* @var Order[] $orders  */
/* @var array $currencySymbolMap */

use app\modules\order\models\Order;
use frontend\modules\tenant\models\Currency;
use yii\helpers\ArrayHelper;
use \yii\helpers\Html;
use \app\modules\order\services\OrderService;
use \app\modules\reports\helpers\OrderReportExporterHelper;

?>

<?php
$freighter = '';
$n = 0;
$len = count($orders);
if (array_key_exists(0, $orders)) {
    $freighter = OrderReportExporterHelper::getFreighter($orders[0]->tenant);
}
foreach ($orders as $order) :
    $currencySymbol = ArrayHelper::getValue($currencySymbolMap, $order->currency_id);
    $currencySymbol = $order->currency_id == Currency::CURRENCY_ID_RUS_RUBLE ? 'руб.' : $currencySymbol;
    if ($n % 2 === 0) : ?>
    <div style="border-width: 1px 1px 0 1px; border-style: solid; border-color: #000; font-family: Arial, Helvetica, sans-serif;">
        <? endif; ?>

        <table style="width: 100%;">
            <tbody>
            <tr>
                <td colspan="2" style="padding: 10px 10px 5px 10px;">
                    <p>
                        Taxi- und Mietwagenunternehmen: <?= $freighter; ?>
                    </p>
                </td>
            </tr>
            <tr>
                <td style="padding: 10px; width: 55%; box-sizing: border-box;">
                    <p style="font-size: 18px; font-weight: bold;">
                        <span>Quittung АА № <?= Html::encode($order->order_number); ?></span>
                    </p>
                    <p>
                        <span>für die Nutzung des Taxis</span>
                    </p>
                    <br>
                    <p>
                            <span>Unternehmen: <?php
                                $charterer = Html::encode(!empty($order->client) ? $order->client->getFullName() : '') . ' ' . Html::encode(!empty($order->client->clientPhones) ? $order->client->clientPhones[0]->value : '');
                                if (trim($charterer) !== '0') {
                                    echo $charterer;
                                }
                                unset($charterer);
                                ?>
                            </span>
                    </p>
                    <p>
                        <span>Abholort: <?= OrderReportExporterHelper::getAddressStart($order->address); ?></span>
                    </p>
                    <p>
                        <span>Zieladresse: <?= count($order->address) > 1 ? OrderReportExporterHelper::getAddressEnd($order->address) : '' ?></span>
                    </p>
                    <p>
                        <span>Wartezeit: <?= $order->getWaitTime() . ' мин' ?></span>
                    </p>
                    <p>
                        <span>Zahlungart: <?= Yii::t('commission', Html::encode($order->payment)); ?></span>
                    </p>
                </td>
                <td style="vertical-align: top; width: 45%; box-sizing: border-box;">
                    <table cellpadding="0" cellspacing="0"
                           style="width: 100%; border-width: 1px 0 1px 1px; border-style: solid; border-color: #000;">
                        <tbody>
                        <tr>
                            <td style="border-width: 0 1px 1px 0; border-style: solid; border-color: #000; padding: 3px 10px;">
                                <p>
                                    <span>Abholzeit</span>
                                </p>
                            </td>
                            <td style="border-width: 0 0 1px 0; border-style: solid; border-color: #000; padding: 3px 10px;">
                                <p>
                                        <span>
                                            <?php
                                            if ($time = OrderService::getOrderArriveTime($order)) {
                                                echo date('d.m.Y в H:i', $time);
                                            }
                                            unset($time);
                                            ?>
                                        </span>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-width: 0 1px 1px 0; border-style: solid; border-color: #000; padding: 3px 10px;">
                                <p>
                                    <span>Abreisezeit </span>
                                </p>
                            </td>
                            <td style="border-width: 0 0 1px 0; border-style: solid; border-color: #000; padding: 3px 10px;">
                                <p>
                                        <span>
                                            <?php
                                            if ($time = OrderService::getOrderDepartureTime($order)) {
                                                echo date('d.m.Y в H:i', $time);
                                            }
                                            unset($time);
                                            ?>
                                        </span>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-width: 0 1px 1px 0; border-style: solid; border-color: #000; padding: 3px 10px;">
                                <p>
                                    <span>Ankunftzeit </span>
                                </p>
                            </td>
                            <td style="border-width: 0 0 1px 0; border-style: solid; border-color: #000; padding: 3px 10px;">
                                <p>
                                        <span><?= date('d.m.Y в H:i',
                                                $order->status_time + $order->getOrderOffset()); ?></span>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-width: 0 1px 1px 0; border-style: solid; border-color: #000; padding: 3px 10px;">
                                <p>
                                    <span>№ Kennzeichen </span>
                                </p>
                            </td>
                            <td style="border-width: 0 0 1px 0; border-style: solid; border-color: #000; padding: 3px 10px;">
                                <p>
                                    <span><?= Html::encode($order->car->gos_number) ?></span>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 3px 10px;">
                                    <span>Wartezeit <?= Html::encode(isset($order->detailCost->city_wait_price) ? $order->detailCost->city_wait_price . " $currencySymbol/min" : null); ?>
                                        <br>
                                        Fahrpreis <?php
                                        if (isset($order->detailCost->city_next_cost_unit, $order->detailCost->city_next_km_price)) {
                                            $cost = null;
                                            switch ($order->detailCost->city_next_cost_unit) {
                                                case '1_minute':
                                                    $cost = "$currencySymbol/min";
                                                    break;
                                                case '30_minute':
                                                    $cost = "$currencySymbol/30min";
                                                    break;
                                                case '1_hour':
                                                    $cost = "$currencySymbol/Uhr";
                                                    break;
                                                case '1_km':
                                                    $cost = "$currencySymbol/km";
                                                    break;
                                                case 'ytn':
                                                    $cost = "$currencySymbol/km";
                                                    break;
                                            }

                                            $text = null;

                                            switch ($order->detailCost->accrual_city) {
                                                case 'DISTANCE':
                                                    $text = Html::encode($order->detailCost->city_next_km_price) . ' ' . $cost;
                                                    break;
                                                case 'TIME':
                                                    $text = Html::encode($order->detailCost->city_next_km_price) . ' ' . $cost;
                                                    break;
                                                case 'MIXED':
                                                    $text = Html::encode($order->detailCost->city_next_km_price) . " $currencySymbol/km" . ' ' . Html::encode($order->detailCost->city_next_km_price_time) . ' ' . $cost;
                                                    break;
                                                case 'INTERVAL':
                                                    $array = unserialize($order->detailCost->city_next_km_price);
                                                    if (is_array($array)) {
                                                        $text = 'vor ' . current($array)['price'] . ' ' . $cost;
                                                    }
                                                    break;
                                                case 'FIX':
                                                    $text = 'тарифная сетка';
                                                    break;
                                            }

                                            echo $text;


                                            unset($cost, $array, $text);
                                        }
                                        ?>
                                    </span>
                                <p>
                                    <span>Taxameter <?= Html::encode($order->getSummaryDistance()) . ' km'; ?></span>
                                </p>
                            </td>

                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
        <p style="padding: 10px 10px 5px 10px;">
                <span>Datum der Ausgabe der Quittung für die Nutzung des Taxis  <?= date('d.m.Y в H:i',
                        $order->status_time + $order->getOrderOffset()); ?></span>
        </p>
        <p style="padding: 0 10px 5px 10px;">
            <span>Kosten für die Taxinutzung
            <?php if (!empty($order->detailCost->summary_cost)) : ?>
                <?php if ($order->currency_id == Currency::CURRENCY_ID_RUS_RUBLE) : ?>
                    <?php $arr = list($rub, $cop) = explode('.', $order->detailCost->summary_cost) ?>
                        <?= Html::encode($rub . ' ' . $currencySymbol) . ' ' .
                    Html::encode(isset($cop) ? $cop . ' коп.' : 0 . ' коп.'); ?>
                    <?php unset($arr); ?>
                <?php else : ?>
                    <?= $order->detailCost->summary_cost . ' ' . $currencySymbol?>
                <?php endif; ?>
            <?php endif; ?>
            </span>
        </p>
        <table>
            <tbody>
            <tr>
                <td style="padding: 0 10px 10px 10px;">
                    <p>
                        <span>Buchhalter<br><?= !empty($order->worker) ? $order->worker->getFullName() : '' ?></span>
                    </p>
                </td>
                <td style="padding: 0 10px 10px 10px;">
                    <? if (!empty($order->client)): ?>
                        <p>
                            <span>Kunde<br><?= $order->client->getFullName(); ?></span>
                        </p>
                    <? endif; ?>
                </td>
            </tr>
            </tbody>
        </table>
        <div style="border-bottom: 1px solid #000;"></div>
        <? if ($n % 2 !== 0 || $n + 1 === $len): ?>
        </div>
            <? if ($n + 1 !== $len): ?>
                <pagebreak>
            <? endif; ?>
        <? endif;
        $n++;
        ?>

<?php endforeach;?>