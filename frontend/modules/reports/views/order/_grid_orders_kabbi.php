<?php

/* @var Order[] $orders  */
/* @var array $info */
/* @var array $company */
/* @var array $user */
/* @var array $currencySymbolMap */

use app\modules\order\models\Order;
use frontend\modules\tenant\models\Currency;
use yii\helpers\ArrayHelper;
use \yii\helpers\Html;
use \app\modules\order\services\OrderService;
use \app\modules\reports\helpers\OrderReportExporterHelper;

?>

<?


$path_icon = Yii::$app->basePath. '/web/images/kabbi/kabbi_icon.png';
$path_icon = file_exists($path_icon) ? $path_icon : '';


$freighter = '';
$n = 0;
$len = count($orders);
if (array_key_exists(0, $orders)) {
    $freighter = OrderReportExporterHelper::getFreighter($orders[0]->tenant);
}
?>
<!-- todo заготовка  <pagebreak /> -->
<html>
<head>
</head>

<body>

<htmlpageheader name="MyHeader1">
</htmlpageheader>

<htmlpageheader name="MyHeader2">
    <div>
        <div style="text-align: center">
            <span>
                Seite {PAGENO} von {nbpg} - Gutschrift Nr. 6
            </span>
        </div>
        <table style="width:100%; border-collapse: collapse;">
            <tr>
                <th style="border-bottom: 3px solid #000; width: 4%; ">Pos</th>
                <th style="border-bottom: 3px solid #000; width: 8%; ">Produkt</th>
                <th style="border-bottom: 3px solid #000; width: 43%; ">Bezeichnung</th>
                <th style="border-bottom: 3px solid #000; width: 10%;" >Steuer</th>
                <th style="border-bottom: 3px solid #000; width: 10%; ">Menge</th>
                <th style="border-bottom: 3px solid #000; width: 10%; ">Einzelpreis</th>
                <th style="border-bottom: 3px solid #000; width: 10%; ">Betrag</th>
            </tr>
        </table>
    </div>
</htmlpageheader>


<htmlpagefooter name="MyFooter1">
    <table width="100%">
        <tr>
            <td width="33%"><span style="font-weight: bold; font-style: italic;">{DATE j-m-Y}</span></td>
            <td width="33%" align="center" style="font-weight: bold; font-style: italic;">{PAGENO}/{nbpg}</td>
            <td width="33%" style="text-align: right; ">My document</td>
        </tr>
    </table>
</htmlpagefooter>


<htmlpagefooter name="MyFooter2">
    <div><img style="float: left" src="https://taxi.kabbi.com/images/kabbi/kabbi_icon.jpg"></div>
</htmlpagefooter>

<sethtmlpageheader name="MyHeader1" value="on" show-this-page="1" />
<sethtmlpageheader name="MyHeader2" value="on" />
<sethtmlpagefooter name="MyFooter1" value="on" show-this-page="1" />
<sethtmlpagefooter name="MyFooter2" value="on" />
</htmlpagefooter>


<div style="font-family: Arial, Helvetica, sans-serif;  padding-left: 0%">
    <table style="width:90%;font-size: 14px;   border-collapse: collapse; ">
        <tr>
            <td style="vertical-align: top; width: 40%">
            <p style="align-items: right; margin-bottom: -4%">MeinExpressBus Verwaltungs GmbH</p>
            <p style="align-items: right; margin-bottom: -4%">Buchhaltung</p>
            <p style="align-items: right; margin-bottom: -4%">Bamberger Straße 11</p>
            <p style="align-items: right; margin-bottom: -4%">96049 Bamberg</p>
            <p style="align-items: right; margin-bottom: -4%">Steuernummer: 207/132/20508</p>
            </td>

            <td style="vertical-align: top;  ">
                <p style="align-items: right; margin-bottom: -5%">Steuernummer: 207/132/20508</p>
                <p style="align-items: right; margin-bottom: -5%">Fax: 0911 527 89 8 87</p>
                <p style="align-items: right; margin-bottom: -5%">Mail: info@kabbi.com</p>

            </td>

            <td style="vertical-align: top; float: left;  ">
                <img  src="https://taxi.kabbi.com/images/kabbi/kabbi_icon_large.jpg">
            </td>
        </tr>
    </table>

    <div style="margin-top: 2%">
        <table style="width:100%;font-size: 14px;   font-weight: 900">
            <tr>
                <td style="vertical-align: top; ">
                    <p style="font-size: 8px; align-items: right; margin-bottom: 5%; text-decoration:underline ">MeinExpressBus Verwaltungs GmbH• Bamberger Straße 11 • 96049 Bamberg</p>

                    <p style="align-items: right; margin-bottom: -3%"><?=  $info['companyName']?></p>
                    <p style="align-items: right; "><?=  $info['nameWorker']?> </p>

                </td>
                <td style="vertical-align: top; ">
                    <p style="align-items: right; margin-bottom: -5%">Gutschrift Nr. __</p>
                    <p style="align-items: right; margin-bottom: -5%">Kunde Nr. <?= $info['workercallsign']?></p>
                    <p style="align-items: right; margin-bottom: -5%">Datum. <?= date("d.m.y")?></p>
                </td>
            </tr>
        </table>
    </div>



    <p style="float: left;font-weight: 900;">Gutschrift </p>


    <div style="margin-top: 5%">
        <table style="width:100%; border-collapse: collapse;">
            <tr  >
                <th style="border-bottom: 3px solid #000;">Pos</th>
                <th style="border-bottom: 3px solid #000;">Produkt</th>
                <th style="border-bottom: 3px solid #000;">Bezeichnung</th>
                <th style="border-bottom: 3px solid #000;" >Steuer</th>
                <th style="border-bottom: 3px solid #000;">Menge</th>
                <th style="border-bottom: 3px solid #000;">Einzelpreis</th>
                <th style="border-bottom: 3px solid #000;">Betrag</th>
            </tr>
            <? $i_pos = 1;  ?>
            <? foreach ($orders as $order) : ?>
                <?
                $i_Produkt = 1;
                $currencySymbol = ArrayHelper::getValue($currencySymbolMap, $order->currency_id);
                $currencySymbol = $order->currency_id == Currency::CURRENCY_ID_RUS_RUBLE ? 'руб.' : $currencySymbol;
                 $from = OrderReportExporterHelper::getAddressStart($order->address);
                 $to = count($order->address) > 1 ? OrderReportExporterHelper::getAddressEnd($order->address) : '';
                ?>

                <tr>
                    <td style="border-bottom: 1px solid #000;"><?= $i_pos ?></td>
                    <td style="border-bottom: 1px solid #000;"><?= $i_Produkt ?></td>
                    <td style="border-bottom: 1px solid #000; width:42%">
                        <p>Auftrag Nr. <?= Html::encode($order->order_number);?>  </p>
                        <span>A) <?= $from?>  (Stadt)</span> <br>
                        <span>B) <?= $to?> (Stadt)</span>
                    </td>
                    <td style="border-bottom: 1px solid #000;">7 %</td>
                    <td style="border-bottom: 1px solid #000;">1 St</td>
                    <td style="border-bottom: 1px solid #000;">
                        <span>
                        <?php if (!empty($order->detailCost->summary_cost)) : ?>
                            <?php if ($order->currency_id == Currency::CURRENCY_ID_RUS_RUBLE) : ?>
                                <?php $arr = list($rub, $cop) = explode('.', $order->detailCost->summary_cost) ?>
                                <?= Html::encode($rub . ' ' . $currencySymbol) . ' ' .
                                Html::encode(isset($cop) ? $cop . ' коп.' : 0 . ' коп.'); ?>
                                <?php unset($arr); ?>
                            <?php else : ?>
                                <?= $order->detailCost->summary_cost . ' ' . $currencySymbol?>
                            <?php endif; ?>
                        <?php endif; ?>
                        </span>
                    </td>
                    <td style="border-bottom: 1px solid #000; text-align: left;">
                        <span>
                        <?php if (!empty($order->detailCost->summary_cost)) : ?>
                            <?php if ($order->currency_id == Currency::CURRENCY_ID_RUS_RUBLE) : ?>
                                <?php $arr = list($rub, $cop) = explode('.', $order->detailCost->summary_cost) ?>
                                <?= Html::encode($rub . ' ' . $currencySymbol) . ' ' .
                                Html::encode(isset($cop) ? $cop . ' коп.' : 0 . ' коп.'); ?>
                                <?php unset($arr); ?>
                            <?php else : ?>
                                <?= $order->detailCost->summary_cost . ' ' . $currencySymbol?>
                            <?php endif; ?>
                        <?php endif; ?>
                        </span>
                    </td>
                </tr>

                <?$i_Produkt = 2; $i_pos +=1;   ?>

                 <tr>
                     <td style="border-bottom: 1px solid #000;"><?= $i_pos ?></td>
                     <td style="border-bottom: 1px solid #000;"><?= $i_Produkt ?></td>
                     <td style="border-bottom: 1px solid #000; width:42%">
                         <span>
                           5% Provision für Auftrag Nr.   <?= Html::encode($order->order_number);?>
                         </span>
                     </td>

                     <td style="border-bottom: 1px solid #000;">19%</td>
                     <td style="border-bottom: 1px solid #000;">-11,50</td>
                     <td style="border-bottom: 1px solid #000;">0,05</td>
                     <td style="text-align: left;border-bottom: 1px solid #000;">-0,58</td>

                 </tr>
            <?php endforeach;?>

            <tr style="margin-top: 1%">
                <td style="border-bottom: 1px solid #000;"></td>
                <td style="border-bottom: 1px solid #000;"></td>
                <td style="border-bottom: 1px solid #000;"></td>
                <td style="border-bottom: 1px solid #000;"></td>
                <td  colspan="2" style="border-bottom: 1px solid #000;" >
                    <p  style="text-align: right; margin-top: 10px;">Nettobetrag </p>
                    <p  style="text-align: right">Umsatzsteuer 19% aus -16,45 </p>
                    <p  style="text-align: right; margin-bottom: 10px">Umsatzsteuer 7% aus 294,60 </p>
                </td>
                <td   colspan="2" style="border-bottom: 1px solid #000;" >
                    <p style="text-align: right; margin-top: 10px">261,51 </p>
                    <p style="text-align: right">-2,63 </p>
                    <p style="text-align: right; margin-bottom: 10px">19,27 </p>
                </td>
            </tr>


            <tr style="margin-top: 1%">
                <td style="border-bottom: 1px solid #000;"></td>
                <td style="border-bottom: 1px solid #000;"></td>
                <td style="border-bottom: 1px solid #000;"></td>
                <td style="border-bottom: 1px solid #000;"></td>
                <td  colspan="2"  style="border-bottom: 1px solid #000;" >
                    <p  style="text-align: right; margin-top: 10px">Endbetrag EUR:</p>
                </td>
                <td style="border-bottom: 1px solid #000;"  >
                    <p style="text-align: left; margin-top: 10px">278,15 </p>
                </td>
            </tr>
        </table>

    </div>

    <div style="margin-top: 10px">
        <p>Betrag wird in den nächsten Tagen auf Ihr Konto überwiesen</p>
        <p>Ihre Bankverbindung: DE33 XXXX XXXX XXXX XX17 90 • Spk Bamberg
        </p>
    </div>

</div>
</body>
</html>









