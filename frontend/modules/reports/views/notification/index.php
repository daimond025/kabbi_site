<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var string $pjaxId */
/* @var $this yii\web\View */
/* @var string $firstDate */
/* @var string $secondDate */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $notificationDataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \frontend\modules\employee\models\groups\WorkerGroupSearch */

$this->title = Yii::t('reports', 'PUSH-notifications');
$isDetailVisible = true;
?>

<h1><?= Html::encode($this->title) ?></h1>
<section class="main_tabs">

    <?php
    $filter = \frontend\widgets\filter\Filter::begin([
        'pjaxId'   => $pjaxId[0],
        'model'    => $searchModel,
        'onSubmit' => true,
        'onChange' => false,
    ]);
    echo $filter->radioList('type',
        [
            'today'     => [
                'label' => t('reports', 'Today'),
            ],
            'yesterday' => [
                'label' => t('reports', 'Yesterday'),
            ],
            'month'     => [
                'label' => app()->formatter->asDate(time(), 'LLLL'),
            ],
            'period'    => [
                'label'   => t('reports', 'Period'),
                'content' => $filter->datePeriod(['first_date', 'second_date'], [
                    date("d.m.Y", mktime(0, 0, 0, date("m"), date("d") - 2, date("Y"))),
                    date("d.m.Y", mktime(0, 0, 0, date("m"), date("d"), date("Y"))),
                ], ['class' => 'cof_date']),
            ],
        ], [], ['class' => 'filter_item_3', 'style' => 'width:auto']);

    echo $filter->button(t('reports','Show'), []);

    \frontend\widgets\filter\Filter::end();
    ?>

    <?= $this->render('_index', compact('dataProvider', 'pjaxId', 'isDetailVisible')) ?>
</section>