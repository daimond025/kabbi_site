<?php
/* @var array $pjaxId
 * @var $this yii\web\View
 * @var $notification \frontend\modules\reports\models\NotificationReport
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

use frontend\widgets\LinkPager;
use yii\grid\GridView;
use yii\widgets\Pjax;

Pjax::begin(['id' => $pjaxId[1]]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'       => "{items}\n{pager}",
    'tableOptions' => [
        'class' => 'people_table push-report-full',
    ],
    'pager'        => [
        'class'         => LinkPager::className(),
        'prevPageLabel' => t('app', 'Prev'),
        'nextPageLabel' => t('app', 'Next'),
    ],

    'columns' => [
        [
            'attribute'     => 'fullName',
            'label' => t('app', 'Client'),
            'headerOptions' => [
                'style' => 'width: 80%',
            ],
            'content'       => function ($model) use ($notification) {
                return $this->render('_fullName', compact('notification', 'model'));
            },
        ],
        [
            'attribute'     => 'status',
            'label' => t('reports', 'Status'),
            'headerOptions' => [
                'style' => 'width: 20%',
            ],
            'content'       => function ($model) use ($notification) {
                /* @var $this yii\web\View */
                return $this->render('_status', compact('notification', 'model'));
            },
        ],
    ],
]);

Pjax::end();