<?php
/* @var string $pjaxId
 * @var $this yii\web\View
 * @var $notification \frontend\modules\reports\models\NotificationReport
 * @var $dataProvider \yii\data\ActiveDataProvider
/* @var $notificationDataProvider yii\data\ActiveDataProvider
 */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

$this->title = $notification->getCreatedAtByDate() . ' ' . $notification->title;

echo Breadcrumbs::widget([
    'links'        => [
        ['label' => t('reports', 'PUSH-notifications'), 'url' => ['index']],
    ],
    'itemTemplate' => '{link}',
    'tag'          => 'div',
    'options'      => ['class' => 'bread'],
    'homeLink'     => false,
]);
?>

<?= Html::tag('h1', $this->title); ?>

<?= $this->render('_index',
    ['dataProvider' => $notificationDataProvider, 'pjaxId' => $pjaxId, 'isDetailVisible' => $isDetailVisible]) ?>

<br>

<section class="row">
    <div class="row_label"><?= Html::activeLabel($notification, 'title') ?></div>
    <div class="row_input">
        <div class="input_with_icon">
            <div class="input_with_icon"><?= Html::encode($notification->title) ?></div>
            <span></span>
        </div>
    </div>
</section>

<section class="row">
    <div class="row_label"><?= Html::activeLabel($notification, 'message') ?></div>
    <div class="row_input">
        <div class="input_with_icon">
            <div class="input_with_icon"><?= Html::encode($notification->message) ?></div>
            <span></span>
        </div>
    </div>
</section>

<?= Html::tag('h2', t('reports', 'Detailed report')); ?>
<section class="main_tabs">
    <?= $this->render('_clients', compact('notification', 'dataProvider', 'pjaxId')); ?>
</section>