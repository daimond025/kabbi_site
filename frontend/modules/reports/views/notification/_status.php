<?php
/* @var $this yii\web\View
 * @var $notification \frontend\modules\reports\models\NotificationReport
 * @var $model \frontend\modules\reports\models\notification\ClientInterface
 */

use yii\helpers\Html;

$device = $notification->getDeviceByClient($model->getClientId());
$status = $notification->getStatusByClient($model->getClientId());

echo Html::tag('i', '', ['class' => 'osi_' . $device]);
echo '&nbsp;';

switch ($status) {
    case 'Delivered':
        echo Html::tag('span', t('reports', $status), ['class' => 'link_green']);
        break;
    default:
        echo Html::tag('span', t('reports', $status), ['class' => 'link_red']);
}


