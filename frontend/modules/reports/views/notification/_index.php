<?php

use frontend\widgets\LinkPager;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Html;


Pjax::begin(['id' => $pjaxId[0]]);

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'       => "{items}\n{pager}",
    'tableOptions' => [
        'class' => 'people_table push-report-table' . (!$isDetailVisible ? '-item' : ''),
    ],
    'pager'        => [
        'class'         => LinkPager::className(),
        'prevPageLabel' => t('app', 'Prev'),
        'nextPageLabel' => t('app', 'Next'),
    ],

    'columns' => [
        [
            'attribute'     => 'created_at',
            'headerOptions' => [
                'style' => 'width: 20%',
            ],
            'content'       => function ($model) {
                return $model->getCreatedAtByDate();
            },
        ],
        [
            'attribute'     => 'title',
            'headerOptions' => [
                'style' => 'width: 18%',
            ],
            'content'       => function ($model) {

                return Html::a(Html::encode($model->notification->name),
                    ['/notifications/push/update-mass-mailing', 'id' => $model->notification_id], ['data-pjax' => 0]);
            },
        ],
        [
            'attribute' => 'client_type',
            'content'   => function ($model) {
                return $model->client_type === 'client' ? t('app', 'Client') : t('app', 'Worker');
            },
        ],
        [
            'attribute' => 'total',
            'content'   => function ($model) {
                return $model->getTotal();
            },
        ],
        [
            'attribute' => 'ios',
            'header'    => '<i class="osi_ios"></i>',
            'content'   => function ($model) {
                $success = Html::tag('span', (int)$model->report['ios']['success'], ['class' => 'link_green']);
                $failure = Html::tag('span', (int)$model->report['ios']['failure'], ['class' => 'link_red']);

                return $success . '&nbsp;&nbsp;&nbsp;&nbsp;' . $failure;
            },
        ],
        [
            'attribute' => 'android',
            'header'    => '<i class="osi_andr"></i>',
            'content'   => function ($model) {
                $success = Html::tag('span', (int)$model->report['android']['success'], ['class' => 'link_green']);
                $failure = Html::tag('span', (int)$model->report['android']['failure'], ['class' => 'link_red']);

                return $success . '&nbsp;&nbsp;&nbsp;&nbsp;' . $failure;
            },
        ],
        [
            'attribute'     => 'report',
            'label'         => '',
            'headerOptions' => [
                'style' => 'width: 10%',
            ],
            'content'       => function ($model) {
                return Html::a(t('reports', 'Report'), ['detail', 'id' => (string)$model->_id], ['data-pjax' => 0]);
            },
            'visible' => $isDetailVisible,
        ],
    ],
]);

Pjax::end();