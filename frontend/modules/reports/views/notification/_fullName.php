<?php
/* @var $this yii\web\View
 * @var $notification \frontend\modules\reports\models\NotificationReport
 * @var $model \frontend\modules\reports\models\notification\ClientInterface
 */
use yii\helpers\Html;
$items = [];

$photo = Html::tag('span', $model->getPhoto(), ['class' => 'pt_photo']);

if (!empty($model->getFullName())) {
    $items[] = Html::tag('span', Html::encode($model->getFullName()), ['class' => 'pt_fios']);
}

foreach ((array) $model->getPhone() as $phone) {
    $items[] = Html::tag('span', Html::encode($phone));
}
$name = $photo . implode('<br>', $items);

$url = null;

switch ($notification->client_type) {
    case 'worker':
        $url = ['/employee/worker/update', 'id' =>$model->getClientId()];
        break;
    case 'client':
        $url = ['/client/base/update', 'id' =>$model->getClientId()];
        break;
    default:
        $url = null;
}

echo Html::a($name, $url, ['data-pjax' => 0]);