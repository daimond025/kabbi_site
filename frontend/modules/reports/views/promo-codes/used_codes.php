<?php
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;

$bundle = \app\modules\reports\assets\ReportsAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/orders.js');


echo GridView::widget([
    'dataProvider' => $usedCodesProvider,
    'tableOptions' => [
        'class' => 'people_table',
    ],
    'columns'      => [

        [
            'attribute' => 'status_time',
            'label'     => 'Дата',
            'format'    => ['date', 'd.m.Y'],
        ],
        [
            'attribute' => 'statusCode.status_label',
            'label'     => 'Статус',

        ],
        [
            'attribute' => 'order.order_id',
            'label'     => 'Заказ',
            'content'   => function ($model) {
                $orderNum = '№' . $model->order->order_id;
                $orderID  = $model->order->order_id;

                return Html::tag('a', $orderNum,
                    ['href' => Url::to("/order/view/$orderID"), 'class' => 'js-order-view',]);
            },
        ],
        [
            'label'   => 'Клиент',
            'content' => function ($model) {
                $client = $model->promoClient->client->last_name . ' ' . $model->promoClient->client->name . ' ' . $model->promoClient->client->second_name;
                $client .= "<br>";
                $client .= $model->promoClient->client->clientPhones[0]->value;

                $client_id = $model->promoClient->client->client_id;

                return Html::tag('a', $client, ['href' => Url::to("/client/base/update/$client_id")]);
            },
        ],
        [
            'label'   => 'Исполнитель',
            'content' => function ($model) {
                $worker = $model->order->worker->last_name . ' ' . $model->order->worker->name . ' ' . $model->order->worker->second_name;
                for ($i = 0; $i <= count($model->order->worker->cars); $i++) {
                    $worker .= "<br>";
                    $worker .= $model->order->worker->cars[$i]->car->name;
                    $worker .= "<br>";
                    $worker .= $model->order->worker->cars[$i]->car->gos_number;
                }
                $worker_id = $model->order->worker->worker_id;

                return Html::tag('a', $worker, ['href' => Url::to("/employee/worker/update/$worker_id")]);
            },
        ],
        [
            'attribute' => 'code',
            'label'     => 'Код',
        ],

    ],
]);






