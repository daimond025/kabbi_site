<?php
use yii\grid\GridView;

echo GridView::widget([
    'dataProvider' => $promoCodesProvider,
    'tableOptions' => [
        'class' => 'people_table',
    ],

    'columns' => [
        [
            'attribute'=>'code',
            'label'=>'Код'
        ]
    ]


]);


