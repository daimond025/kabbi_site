<?php
use yii\jui\Tabs;

?>

<section class="main_tabs">
    <div class="tabs_links">
        <ul>
            <li class=""><a href="#promo" class="t01"><?= t('reports', 'Promo') ?></a></li>
            <li class=""><a href="#promocodes"
                            class="t02"><?= t('reports', 'Promocodes') ?></a></li>
            <li class=""><a href="#used-promo"
                            class="t03"><?= t('reports', 'Used promocodes') ?></a></li>
        </ul>
    </div>
    <div class="tabs_content">
        <div id="t01" data-view="index">
            <?= $this->render('@app/modules/promocode/views/promo/create', ['promoProvider'=>$promoProvider, 'model'=>$model,'dataForm'=>$dataForm,'stringDataTenant'=>$stringDataTenant, 'stringTranslation'=>$stringTranslation]) ?>
        </div>
        <div id="t02">
            <?= $this->render('promo_codes', ['promoCodesProvider'=>$promoCodesProvider]) ?>
        </div>
        <div id="t03">
            <?= $this->render('used_codes', ['usedCodesProvider'=>$usedCodesProvider]) ?>
        </div>
    </div>
</section>
