<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

if (!empty($entityData)):?>
    <?foreach($entityData as $entity) :?>
        <li>
            <a data-entity_id="<?= $entity['entity_id'] ?>"><?= Html::encode($entity['name']); ?></a>
        </li>
    <?endforeach;?>
<?else:?>
    <li class="no_select"><a><?= t ('app', 'No result') ?></a></li>
<? endif;?>
