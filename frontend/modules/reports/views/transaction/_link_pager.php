<?php

use frontend\widgets\LinkPager;
use yii\data\Pagination;

/** @var Pagination $pages */

echo  LinkPager::widget([
    'pagination'    => $pages,
    'prevPageLabel' => t('app', 'Prev'),
    'nextPageLabel' => t('app', 'Next'),
    'options'       => ['class' => 'pagination ajax-report-pagination'],
]);
