<?php

use common\components\formatter\Formatter;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\tenant\models\Currency;
use app\modules\balance\models\Operation;
use app\modules\reports\models\transaction\TransactionReport;
use common\modules\city\models\City;
use app\modules\reports\helpers\TransactionReportHelper;

/**
 * @var Operation[] $operations
 * @var string $report_type
 * @var Formatter $formatter
 */

$isBonusAccount = $report_type == 'client-bonus';

$formatter = app()->formatter;

if (!empty($operations)) :
    foreach ($operations as $operation) :
        $currencySymbol = Currency::getCurrencySymbol($operation->account->currency_id);
        if ($isBonusAccount) {
            $currencySymbol = t('currency', 'B') . ' (' . $currencySymbol . ')';
        } ?>
        <tr>
            <td>
                <span style="font-size: 14px">
                    <?
                    $date = strtotime($operation->date) + City::getTimeOffset((int)$operation->transaction->city_id);
                    echo implode(", ", [
                        $formatter->asDate($date, 'shortDate'),
                        $formatter->asTime($date, 'short')
                    ]);
                    ?>
                </span>
            </td>
            <td>
                <?= TransactionReport::getEntityName($operation->account->acc_kind_id) ?><br>
                <a href="<?= Url::to([TransactionReport::getEntityUrl($operation->account->acc_kind_id), 'id' => $operation->account->owner_id]) ?>"><?= Html::encode($operation->owner_name); ?></a>
            </td>
            <td>
                <?= TransactionReportHelper::getTextOperation($operation) ?>
                <? if (!empty($operation->transaction->userCreated)): ?>
                    <br>
                    <?= Html::encode(t('company-roles', $operation->transaction->userCreated->getPositionName())); ?><br/>
                    <a href="<?= Url::to(['/tenant/user/update', 'id' => $operation->transaction->user_created]) ?>"> <?= Html::encode($operation->transaction->userCreated->getFullName()); ?></a>
                <? endif ?>
            </td>
            <td style="text-align: right;">
                <?
                if ($operation->isExpense()):?>
                    <?= $formatter->asMoney(+$operation->sum, $currencySymbol); ?>
                <? endif ?>
            </td>
            <td style="text-align: right;">
                <?
                if ($operation->isIncome()):?>
                    <?= $formatter->asMoney(+$operation->sum, $currencySymbol); ?>
                <? endif ?>
            </td>
        </tr>
    <? endforeach; ?>
<?endif?>
