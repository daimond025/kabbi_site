<?php

use app\modules\balance\models\Account;
use app\modules\reports\models\transaction\InnerOperationSearch;
use app\modules\reports\models\transaction\InnerOperatonType;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\PjaxAsset;

/* @var $userCities array */
/* @var $entityTypes array */

$this->title = t('order', 'Orders');

$bundle = \app\modules\reports\assets\ReportsAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/transaction.js');
?>
<h1><?= t('app', 'Transactions') ?></h1>
<section class="main_tabs">
    <div class="tabs_links">
        <ul>
            <li><a href="#driver_clients_orgs" class="t01"><?= t('transaction',
                        'Workers, clients, organizations') ?></a></li>
            <li><a href="#external_report" class="t02"><?= t('transaction', 'External report') ?></a></li>
            <li><a href="#client_bonus_report" class="t03"><?= t('transaction', 'Bonus system') ?></a></li>
        </ul>
    </div>
    <div class="tabs_content">
        <div class id="t01">

            <form name="stat_filter" id="form-1" action="<?= Url::to(['/reports/transaction/inner-report-search']) ?>"
                  method="post">
                <div class="filter">
                    <div class="filter_item_1">
                        <div class="select_checkbox">
                            <a style="white-space: nowrap;" data-filter="no" class="a_sc select"
                               rel="<?= t('user', 'All cities') ?>"><?= t('user', 'All cities') ?></a>
                            <div class="b_sc">
                                <ul>
                                    <? foreach ($userCities as $city_id => $city): ?>
                                        <li><label><input name="city_id[]" type="checkbox"
                                                          value="<?= $city_id ?>"/> <?= Html::encode($city); ?></label>
                                        </li>
                                    <? endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="filter_item_1">
                        <div class="select_checkbox">
                            <a style="white-space: nowrap;" data-filter="no" class="a_sc select"
                               rel="<?= t('reports', 'All car classes') ?>"><?= t('reports', 'All car classes') ?></a>
                            <div class="b_sc">
                                <ul>
                                    <?
                                    $carClasses = InnerOperationSearch::getCarClassMap();
                                    foreach ($carClasses as $classId => $className): ?>
                                        <li><label><input name="car_class[]" type="checkbox"
                                                          value="<?= $classId ?>"/> <?= Html::encode($className); ?>
                                            </label>
                                        </li>
                                    <? endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="filter_item_1">
                        <?= Html::dropDownList('operation_type', null, InnerOperatonType::getTypeMap(),
                            [
                                'prompt' => t('transaction', 'All operation types'),
                                'class'  => 'default_select',
                            ]);
                        ?>
                    </div>
                </div>
                <div>
                    <div class="mg_new">
                        <div class="mgn_item">
                            <label>
                                <input type="radio" name="entity" checked value="all"/> <?= t('transaction', 'All') ?>
                            </label>
                        </div>
                        <div class="mgn_item" style="margin-right: 0;">
                            <label>
                                <input type="radio" name="entity" value="one"/> <?= t('transaction', 'Choose') ?>
                            </label>
                            <div class="mgni">
                                <?= Html::dropDownList('entity_type', 2, $entityTypes,
                                    ['class' => 'entity_type default_select', 'disabled' => 'disabled']) ?>
                            </div>
                            <div class="mgni">
                                <input class="entity_name" name="entity_name" type="text" autocomplete="off"
                                       placeholder="<?= t('reports', 'All workers') ?>" disabled value=""/>
                                <ul class="autocomplete_entity driver_auto"></ul>
                                <input class="entity_id" name="entity_id" type="hidden" value=""/>
                            </div>
                        </div>
                    </div>
                </div>
                <?= $this->render('_form') ?>
                <div style="height: 50px; float: right; margin-top: -58px;" class="grid_item">
                    <div class="context_menu_wrap export_menu_wrap">
                        <a class="context_menu a_sc"><?= t('client', 'Export'); ?></a>
                        <div class="context_menu_content b_sc js-no-stop">
                            <a href="<?= Url::to('/reports/transaction/dump-transaction/'); ?>"
                               class="js-export-transaction"><?= t('client', 'Download to Excel'); ?></a>
                        </div>
                    </div>
                </div>
            </form>
            <div class="report_data"></div>
        </div>
        <div id="t02">
            <form name="stat_filter" action="<?= Url::to(['/reports/transaction/outer-report-search']) ?>"
                  method="post">
                <?= $this->render('_form') ?>
                <div class="grid_3 money_grid">
                    <!--div class="grid_item">
                        <label><input name="transaction" value="<? //=  Transaction::GOOTAX_PAYMENT_TYPE?>" type="checkbox"/> <? //=t('transaction', 'Payment for gootax rent')?></label>
                    </div>
                    <div class="grid_item ch_tr">
                        <input type="checkbox"/><div><input type="text" placeholder=<? //=t('transaction', 'All market partners')?> disabled/></div>
                    </div-->
                </div>
            </form>
            <div class="report_data"></div>
        </div>
        <div class id="t03">
            <form name="stat_filter" action="<?= Url::to(['/reports/transaction/client-bonus-report-search']) ?>"
                  method="post">
                <?= $this->render('_form') ?>
                <div>
                    <div class="mg_new">
                        <div class="mgn_item">
                            <label>
                                <input type="radio" name="entity" checked value="all"/> <?= t('transaction', 'All') ?>
                            </label>
                        </div>
                        <div class="mgn_item" style="margin-right: 0;">
                            <label>
                                <input type="radio" name="entity" value="one"/> <?= t('transaction', 'Choose') ?>
                            </label>
                            <input class="entity_type" type="hidden" name="entity_type"
                                   value="<?= Account::CLIENT_KIND ?>">
                            <div class="mgni">
                                <input class="entity_name" name="entity_name" type="text" autocomplete="off"
                                       placeholder="<?= t('reports', 'All workers') ?>" disabled value=""/>
                                <ul class="autocomplete_entity driver_auto"></ul>
                                <input class="entity_id" name="entity_id" type="hidden" value=""/>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="report_data"></div>
        </div>
    </div>
</section>