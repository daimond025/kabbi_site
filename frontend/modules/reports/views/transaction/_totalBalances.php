<?php

/**
 * @var array $totalBalances
 */
?>

<tr class="mt_spec">
    <td colspan="3" class="mt_itog"><b><?= t('balance', 'Total for the period') ?></b></td>
    <td style="text-align: right;">
        <b><?= $totalBalances['expenses']?></b>
    </td>
    <td style="text-align: right;">
        <b><?= $totalBalances['income']?></b>
    </td>
</tr>
<tr class="mt_spec">
    <td colspan="3" class="mt_itog"><b><?= t('balance', 'The balance for the period') ?></b></td>
    <td colspan="2" class="td_text_center">
        <b><?= $totalBalances['total']?></b>
    </td>
</tr>