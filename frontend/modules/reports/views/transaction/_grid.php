<?php

use yii\data\Pagination;
use yii\helpers\HtmlPurifier;

/**
 * @var int $time_offset
 * @var array $openPeriodSaldo
 * @var Pagination $pages
 * @var \common\components\formatter\Formatter $formatter
 * @var array $totalBalances
 */

if (!empty($operations)) :
    $formatter = app()->formatter;
    ?>
    <table class="people_table money_table transaction_list">
        <thead>
            <tr>
                <th style="width: 20%"><?= t('balance', 'Operation date') ?></th>
                <th style="width: 30%"><?= t('balance', 'Account holder') ?></th>
                <th style="width: 30%;"><?= t('balance', 'Operation') ?></th>
                <th style="width: 13%; text-align: right;"><?= t('balance', 'Debet') ?><a data-tooltip="8" class="tooltip">?<span style="display: none; opacity: 0;"><?= HtmlPurifier::process(Yii::t('hint', 'Summary earned in period')); ?></span></a></th>
                <th style="width: 13%; text-align: right;"><?= t('balance', 'Credit') ?><a data-tooltip="9" class="tooltip">?<span style="display: none; opacity: 0;"><?= HtmlPurifier::process(Yii::t('hint', 'Incomes in period. Your obligations')); ?></span></a></th>
            </tr>
            <tr class="mt_spec">
                <td colspan="3" class="mt_itog"><b><?= t('balance', 'Beginning balance') ?><a data-tooltip="10" class="tooltip">?<span style="display: none; opacity: 0;"><?= HtmlPurifier::process(Yii::t('hint','Difference between your debt and credit')); ?></b></td>
                <td class="td_text_center" style="text-align: center;" colspan="2">
                    <b>
                        <?php
                        $summary = [];
                        foreach ($openPeriodSaldo as $currencySymbol => $saldo) {
                            $summary[] = $formatter->asMoney(round(+$saldo, 2), $currencySymbol);
                        }
                        echo implode(', ', $summary);
                        ?>
                    </b>
                </td>
            </tr>
            <?= $this->render('_totalBalances', compact('totalBalances'));?>
        </thead>
        <tbody id="ajax-report-body">
            <?= $this->render('_rows', compact('operations', 'time_offset', 'openPeriodSaldo', 'report_type')) ?>
        </tbody>
        <tfoot>
        <?= $this->render('_totalBalances', compact('totalBalances'));?>
        </tfoot>
    </table>
    <?= $this->render('_link_pager', compact('pages'))?>
<?php else : ?>
    <p><?= t('app', 'Empty') ?></p>
<?php endif; ?>
