<?php

namespace app\modules\reports\models;

use yii\base\Model;

class OrderByWorkerService extends Model
{

    public $formName = null;
    public $position_id;
    public $type;
    public $first_date;
    public $second_date;
    public $worker_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'in', 'range' => ['today', 'yesterday', 'month', 'period']],
            [['first_date', 'second_date', 'city_id'], 'safe'],
            [['position_id'], 'safe'],
        ];
    }

    public function search()
    {
        $model = new OrderByWorkerSearch();
        $model->attributes = $this->attributes;

        return $model->search();
    }

    public function getStatistic()
    {
        switch ($this->type) {
            case 'period':
                break;
            case 'yesterday':
                $this->first_date = $this->second_date = date("d.m.Y", (time() - 24 * 60 * 60));
                break;
            case 'month':
                $this->first_date = mktime(0, 0, 0, date("n"), 1);
                $this->second_date = time();
                break;
            case 'today':
            default:
                $this->first_date = $this->second_date = date("d.m.Y", time());
                break;
        }
        $first_date = (int) app()->formatter->asTimestamp($this->first_date);
        $second_date = (int) app()->formatter->asTimestamp($this->second_date) + 24 * 60 * 60;
        return WorkerReport::getOrderStat($this->worker_id, [$first_date, $second_date], [$this->position_id]);
    }
}
