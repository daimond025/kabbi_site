<?php

namespace frontend\modules\reports\models;

use app\modules\tenant\models\User;
use frontend\modules\companies\components\services\TenantCompanyService;
use frontend\modules\companies\models\TenantCompany;
use frontend\modules\employee\models\worker\Worker;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * Class WorkerReportSearch
 * @package frontend\modules\reports\models
 */
class WorkerReportSearch extends Worker
{
    const PAGE_SIZE = 20;

    public $formName = null;
    public $stringSearch;
    public $positionList;
    public $positionActivity = null;
    private $_accessCityList = [];
    public $filter = 'today';
    public $first_date;
    public $second_date;
    public $city_id;
    public $tenantCompanyIds;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stringSearch'], 'string'],
            [['city_id'], 'in', 'range' => $this->getAccessCityList(), 'allowArray' => true],
            [['filter', 'first_date', 'second_date', 'positionList'], 'safe'],
            [
                ['tenantCompanyIds'],
                'each',
                'rule' => [
                    'match',
                    'pattern' => sprintf('/^\d+|%s$/', TenantCompany::FIELD_NAME_MAIN_COMPANY)
                ]
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'positionList'     => t('employee', 'Profession'),
            'city_id'          => t('user', 'All cities'),
            'stringSearch'     => t('employee', 'Search by name, phone or call sign'),
            'tenantCompanyIds' => t('tenant_company', 'Companies'),
        ];
    }

    /**
     * Search
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params, $this->formName);

        $positions = $this->positionList;
        $cities    = $this->getSearchCityList();
        list($firstDate, $secondDate) = $this->getSearchPeriod();

        /** @var $query ActiveQuery */
        $query = parent::find()->alias('t');

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'sort'       => [
                'defaultOrder' => [
                    'fullName' => SORT_ASC,
                ],
                'attributes'   => [
                    'fullName' => [
                        'asc'  => ['last_name' => SORT_ASC, 'name' => SORT_ASC, 'second_name' => SORT_ASC],
                        'desc' => ['last_name' => SORT_DESC, 'name' => SORT_DESC, 'second_name' => SORT_DESC],
                    ],
                    'callsign',
                ],
            ],
            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);

        $query->joinWith(['workerHasCities wc'])
            ->where([
                't.tenant_id' => user()->tenant_id,
                'wc.city_id'  => $this->getAccessCityList(),
            ]);

        $query->with([
            'shifts'                              => function ($subQuery)
            use ($positions, $cities, $firstDate, $secondDate) {
                $subQuery
                    ->alias('t')
                    ->select([
                        't.id',
                        't.worker_id',
                        't.position_id',
                        't.start_work',
                        't.end_work',
                        't.pause_data',
                        't.worker_late_time',
                        't.worker_late_count',
                        't.completed_order_count',
                        't.rejected_order_count',
                        't.accepted_order_offer_count',
                        't.rejected_order_offer_count',
                        't.order_payment_time',
                        't.order_payment_count',
                    ])->andFilterWhere([
                        't.position_id' => $positions,
                        't.city_id'     => $cities,
                    ]);

                if (!empty($firstDate) && !empty($secondDate)) {
                    $subQuery
                        ->joinWith(['city.republic r'], false)
                        ->andWhere('(t.start_work + IFNULL(r.timezone, 0)*3600) between :first and :second', [
                            ':first'  => $firstDate,
                            ':second' => $secondDate,
                        ]);
                }
            },
            'shifts.position'                     => function ($subQuery) {
                $subQuery->select(['position_id', 'name']);
            },
            'shifts.blocks'                       => function ($subQuery) {
                $subQuery->select([
                    'block_id',
                    'shift_id',
                    'worker_id',
                    'start_block',
                    'end_block',
                    'type_block',
                    'is_unblocked',
                ]);
            },
            'shifts.shiftOrders'                  => function ($subQuery) {
                $subQuery->select(['id', 'order_id', 'shift_id']);
            },
            'shifts.shiftOrders.order'            => function ($subQuery) {
                $subQuery->select(['order_id', 'payment', 'status_id', 'currency_id']);
            },
            'shifts.shiftOrders.order.detailCost' => function ($subQuery) {
                $subQuery->select(['detail_id', 'order_id', 'summary_cost', 'surcharge', 'tax', 'commission']);
            },
            'shifts.carMileage' => function ($subQuery) {
                $subQuery->select(['shift_id', 'begin_value', 'end_value']);
            }
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['wc.city_id' => $cities]);

        $phone = strlen($this->stringSearch) > 5 ? $this->stringSearch : null;
        $query->andFilterWhere([
            'or',
            ['like', 't.last_name', $this->stringSearch],
            ['like', 't.name', $this->stringSearch],
            ['like', 't.second_name', $this->stringSearch],
            ['like', 't.callsign', $this->stringSearch],
            ['like', 't.phone', $phone],
        ]);

        $query->joinWith([
            'shifts s' => function ($subQuery) use ($positions, $firstDate, $secondDate) {
                $subQuery->joinWith('city.republic r');
                $subQuery->andFilterWhere(['s.position_id' => $positions]);
                if (!empty($firstDate) && !empty($secondDate)) {
                    $subQuery->andWhere('(s.start_work + IFNULL(r.timezone, 0)*3600) between :first and :second', [
                        ':first'  => $firstDate,
                        ':second' => $secondDate,
                    ]);
                }
            },
        ], false);

        $query->groupBy('t.worker_id');

        (new TenantCompanyService())
            ->queryChange($query, $this->tenantCompanyIds, 't.tenant_company_id');

        return $dataProvider;
    }

    public function getAccessCityList()
    {
        return $this->_accessCityList;
    }

    public function setAccessCityList($value)
    {
        $this->_accessCityList = $value;
    }

    private function getSearchCityList()
    {
        return $this->city_id ? $this->city_id : $this->getAccessCityList();
    }

    private function getSearchPeriod()
    {
        switch ($this->filter) {
            case 'today':
                return [mktime(0, 0, 0), mktime(23, 59, 59)];
            case 'month':
                return [
                    mktime(0, 0, 0, date('n'), 1),
                    mktime(23, 59, 59),
                ];
            case 'period':
                return [
                    strtotime($this->first_date . ' 00:00:00'),
                    strtotime($this->second_date . ' 23:59:59'),
                ];
            default:
                return [];
        }
    }

}