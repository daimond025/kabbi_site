<?php

namespace app\modules\reports\models;

use app\modules\order\models\Order;
use yii\data\ActiveDataProvider;

/**
 * ClientSearch represents the model behind the search form about `frontend\modules\car\models\Car`.
 */
class OrderByWorkerSearch extends Order
{

    const PAGE_SIZE = 50;

    public $worker_id;
    public $type;
    public $first_date;
    public $second_date;
    public $formName = null;
    public $position_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'in', 'range' => ['today', 'yesterday', 'month', 'period']],
            [['first_date', 'second_date'], 'safe'],
            [['position_id', 'worker_id'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params Filter data
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = self::find()
            ->alias('o')
            ->where([
                'o.tenant_id' => user()->tenant_id,
                'o.position_id' => $this->position_id,
                'o.worker_id' => $this->worker_id,
            ])
            ->joinWith([
                'client cl',
                'car car',
                'status st',
                'detailCost dc',
                'clientReview rvw',
            ]);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'       => [
                'defaultOrder' => [
                    'order_id' => SORT_DESC,
                ],
                'attributes' => []
            ],
            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        switch ($this->type) {
            case 'period':
                break;
            case 'yesterday':
                $this->first_date = $this->second_date = date("d.m.Y", (time() - 24*60*60));
                break;
            case 'month':
                $this->first_date = mktime(0, 0, 0, date("n"), 1);
                $this->second_date = time();
                break;
            case 'today':
            default:
                $this->first_date = $this->second_date = date("d.m.Y", time());
                break;
        }
        $first_date = (int) app()->formatter->asTimestamp($this->first_date);
        $second_date = (int) app()->formatter->asTimestamp($this->second_date) + 24 * 60 * 60;
//        $query->andWhere(['>', 'o.create_time', $first_date]);
//        $query->andWhere(['<=', 'o.create_time', $second_date]);
        $query->andWhere(['between', 'o.create_time', $first_date, $second_date]);

        return $dataProvider;
    }


    public function getAccessCityList()
    {
        return $this->_accessCityList;
    }

    public function setAccessCityList($value)
    {
        $this->_accessCityList = $value;
    }

    private function getSearchCityList()
    {
        return $this->city_id ? $this->city_id : current($this->getAccessCityList());
    }

}
