<?php

namespace frontend\modules\reports\models\notification;

interface ClientInterface
{
    public function getFullName();

    public function getPhone();

    public function getPhoto();

    public function getClientId();

}