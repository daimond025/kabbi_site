<?php

namespace frontend\modules\reports\models\notification;

use yii\base\Object;
use yii\data\ActiveDataProvider;

class ClientSearch extends Object
{

    const PAGE_SIZE = 50;

    public $type;


    public function search($client_ids)
    {
        $query = $this->getQuery($client_ids);

        if (!$query) {
            return null;
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,

            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);

        return $dataProvider;
    }

    public function getQuery($client_ids)
    {
        switch ($this->type)
        {
            case 'client':
                return Client::find()
                    ->where([
                        'client_id' => $client_ids,
                        'tenant_id' => user()->tenant_id,
                    ]);
            case 'worker':
                return Worker::find()
                    ->where([
                        'worker_id' => $client_ids,
                        'tenant_id' => user()->tenant_id,
                    ]);
        }

        return null;
    }
}