<?php

namespace app\modules\reports\models\orders;

use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\order\services\OrderService;
use common\modules\city\models\City;
use common\services\OrderStatusService;
use frontend\components\repositories\ExtrinsicDispatcherRepository;
use frontend\modules\car\models\CarClass;
use frontend\modules\companies\components\repositories\TenantCompanyRepository;
use frontend\modules\companies\components\services\TenantCompanyService;
use frontend\modules\companies\models\TenantCompany;
use frontend\modules\employee\models\worker\Worker;
use frontend\modules\exchange\models\entities\ExchangeProgram;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use app\modules\tenant\models\User;
use yii\helpers\Html;

class OrderSearch extends BaseOrderSearch
{
    const PAGE_SIZE = 50;

    public $payment;
    public $order_number;
    public $callsign;
    public $class_id;
    public $status_id;
    public $device;
    public $order_id;
    public $tenant_company_id;
    public $tenantCompanyIds;
    public $user_create;
    public $tenant_id;
    public $rating;
    public $exchange;
    public $extrinsic_dispatcher;
    public $worker_id ;



    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['user_create', 'in', 'range' => array_keys($this->getUsersMap()), 'allowArray' => true],
            ['rating', 'in', 'range' => array_keys($this->getRatingMap()), 'allowArray' => true],
            ['exchange', 'in', 'range' => array_keys($this->getExchangeMap()), 'allowArray' => true],
            ['device', 'in', 'range' => array_keys($this->getDeviceMap()), 'allowArray' => true],
            ['payment', 'in', 'range' => array_keys($this->getPaymentMap()), 'allowArray' => true],
            ['class_id', 'in', 'range' => array_keys($this->getCarClassMap()), 'allowArray' => true],
            ['status_id', 'in', 'range' => array_keys($this->getStatusMap()), 'allowArray' => true],
            ['worker_id', 'in', 'range' => array_keys($this->getWorkerMap()), 'allowArray' => true],
            [
                'extrinsic_dispatcher',
                'in',
                'range'      => array_keys($this->getExtrinsicDispatcherMap()),
                'allowArray' => true,
            ],
            [['order_number', 'callsign', 'tenant_company_id'], 'integer'],
            [
                ['tenantCompanyIds'],
                'each',
                'rule' => [
                    'match',
                    'pattern' => sprintf('/^\d+|%s$/', TenantCompany::FIELD_NAME_MAIN_COMPANY),
                ],
            ],
        ]);
    }

    public function search($usePagination = true, $forReceipt = false)
    {

        $commonQuery = Order::find()->andWhere([
            'o.tenant_id' => user()->tenant_id,
            'o.city_id'   => $this->access_city_list,
        ]);

        $query = clone $commonQuery;

        $query = $query
            ->alias('o')
            ->groupBy('order_id')
            ->joinWith([
                'client cl',
                'worker w',
                'car car',
                'status st',
                'detailCost dc',
                'client.clientPhones cp',
                'company c',
                'exchangeWorker',
            ])
            ->joinWith([
                'clientReview' => function ($query) {
                    if (is_array($this->rating)) {
                        if (in_array(0, $this->rating)) {
                            $query->andWhere(['rating' => null]);
                            $query->orWhere(['rating' => $this->rating]);
                        } else {
                            $query->andFilterWhere(['rating' => $this->rating]);
                        }
                    }
                },
            ])
            ->joinWith([
                'exchangeProgram' => function ($query) {
                    if (is_array($this->exchange)) {
                        $query->andFilterWhere(['exchange_program_id' => $this->exchange]);
                    }
                },
            ])
            ->leftJoin(
                'tbl_tenant_has_extrinsic_dispatcher thed',
                '`o`.`user_create` = `thed`.`user_id`'
            );


        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'sort'       => [
                'defaultOrder' => [
                    'order_id' => SORT_DESC,
                ],
                'attributes'   => [],
            ],
            'pagination' => $usePagination ? ['pageSize' => self::PAGE_SIZE] : false,
        ]);

        if (!$this->validate()) {
            $dataProvider->totalCount = $commonQuery->count();

            return $dataProvider;
        }

        $query->andFilterWhere([
            'o.city_id'      => $this->getCityId(),
            'o.payment'      => $this->payment,
            'o.order_number' => $this->order_number,
            'car.class_id'   => $this->class_id,
            'w.callsign'     => $this->callsign,

            'o.status_id'    => $this->status_id,
            'o.device'       => $this->device,
            'o.position_id'  => $this->getPositionList(),
        ]);


        $query->andWhere(['and', "o.create_time>=$this->first_date", "o.create_time<=$this->second_date"]);

        if (isset($this->order_id)) {
            $query->where(['o.order_id' => $this->order_id]);
        }


        if ($this->user_create || $this->extrinsic_dispatcher) {
            $this->extrinsic_dispatcher = $this->extrinsic_dispatcher ? $this->extrinsic_dispatcher : [];
            $this->user_create = $this->user_create ? $this->user_create : [];
            $userCreate = array_unique(array_merge($this->user_create, $this->extrinsic_dispatcher));

            $query->andWhere([
                'or',
                ['thed.user_id' => $userCreate],
                ['o.user_create' => $userCreate],
            ]);
        }

        if ($forReceipt) {
            $query->andWhere([
                'IN',
                'o.status_id',
                [OrderService::COMPLETED_PAID, OrderService::COMPLETED_NOT_PAID],
            ])->orderBy('order_id DESC');
        }

        if(is_array($this->worker_id)){
            foreach ( $this->worker_id as $value_id){
                $query->andWhere(['o.worker_id' => $value_id]);
            }
        }


        (new TenantCompanyService([
            'or',
            ['o.tenant_company_id' => user()->tenant_company_id],
            ['w.tenant_company_id' => user()->tenant_company_id],
        ]))
            ->queryChange($query, $this->tenantCompanyIds, 'o.tenant_company_id');

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getDeviceMap()
    {
        return [
            Order::DEVICE_0       => t('order', 'Dispatcher'),
            Order::DEVICE_CABINET => t('order', 'Cabinet'),
            Order::DEVICE_WEB     => t('order', 'Web site'),
            Order::DEVICE_WORKER  => t('order', 'Border'),
            Order::DEVICE_ANDROID => 'Android',
            Order::DEVICE_IOS     => 'IOS',
            Order::DEVICE_YANDEX  => 'Yandex',
        ];
    }

    /**
     * @return array
     */
    public function getPaymentMap()
    {
        return [
            Order::PAYMENT_CASH   => t('order', 'Cash'),
            Order::PAYMENT_CARD   => t('order', 'Bank card'),
            Order::PAYMENT_PERSON => t('order', 'Personal account'),
            Order::PAYMENT_CORP   => t('order', 'Corporate balance'),
        ];
    }

    /**
     * @return array
     */
    public function getStatusMap()
    {
        $finishedStatusIds = OrderStatus::getFinishedStatusId();

        $statuses = OrderStatus::find()
            ->where(['status_id' => $finishedStatusIds])
            ->select(['status_id', 'name'])
            ->all();

        return ArrayHelper::map($statuses, 'status_id', function ($item) {
            $statusName = OrderStatusService::translate($item['status_id']);

            if ($item['status_id'] == OrderStatus::STATUS_NO_CARS_BY_TIMER) {
                $statusName .= ' (' . t('reports', 'by timer') . ')';
            }

            return $statusName;
        });
    }

    public function getTenantCompanies()
    {
        return TenantCompanyRepository::selfCreate()
            ->getForForm(['tenant_id' => user()->tenant_id]);
    }

    /**
     * @return array
     */
    public function getCarClassMap()
    {
        return CarClass::getClasses(true);
    }

    public function getUsersMap()
    {
        $users_sql = User::find()->asArray()->where(['tenant_id' => $this->tenant_id]);
        if (app()->user->can(User::ROLE_STAFF_COMPANY)) {
            $users_sql->andWhere(['tenant_company_id' => user()->tenant_company_id]);
        }
        $users = $users_sql->orderBy('last_name')->all();


        return ArrayHelper::map($users, 'user_id', function ($item) {
            return trim(str_replace(
                '  ',
                '',
                $item['last_name'] . ' ' . $item['name'] . ' ' . $item['second_name']
            ));
        });
    }

    // TODO список водителей
    public function getWorkerMap(){
        $users_sql = \common\modules\employee\models\worker\Worker::find()->asArray()->where(['tenant_id' => $this->tenant_id]);
        if (app()->user->can(User::ROLE_STAFF_COMPANY)) {
            $users_sql->andWhere(['tenant_company_id' => user()->tenant_company_id]);
        }
        $users = $users_sql->orderBy('last_name')->all();

        return ArrayHelper::map($users, 'worker_id', function ($item) {
            return trim(str_replace(
                '  ',
                '',
                $item['last_name'] . ' ' . $item['name'] . ' ' . $item['second_name']
            ));
        });
    }
    // TODO  получение данных по компании (имя компании - имя водителя - адрес)
    public function getDetailCompanyWork(){
        $textInfoWorketCompany = [];
        if(is_array($this->worker_id) && (count($this->worker_id) > 0)){


            foreach ($this->worker_id as $worker_id){
                $works = \common\modules\employee\models\worker\Worker::find()->asArray()->where(['worker_id' => $worker_id])->one();
                if($works){
                    $textInfoWorketCompany['nameWorker'] .= $works['last_name'] . ' ' . $works['name'] . ' ' . $works['second_name'] . "<br>";
                    $textInfoWorketCompany['workercallsign'] = $works['callsign'];

                    // поиск информации об компании
                    $company = TenantCompany::find()->asArray()->where(['tenant_company_id' => $works['tenant_company_id'] ])->one();
                    if($company){
                        $textInfoWorketCompany['companyName'] .=$company['name'] . "<br>";
                    }else{
                        $textInfoWorketCompany['companyName'] .= '<br>' ;
                    }
                }else{
                    $textInfoWorketCompany['nameWorker'] = '';
                    $textInfoWorketCompany['companyName'] = '';
                    $textInfoWorketCompany['workercallsign'] = '';
                }
            }
        }elseif (is_array($this->tenantCompanyIds) && (count($this->tenantCompanyIds) > 0)){
            foreach ($this->tenantCompanyIds as $tenantCompanyIds){

                // поиск водителей с данной компании

                $works = \common\modules\employee\models\worker\Worker::find()->asArray()->where(['tenant_company_id' => $tenantCompanyIds])->all();

                if($works && is_array($works)){

                    foreach ($works as $work){
                        $textInfoWorketCompany['nameWorker'] .= $work['last_name'] . ' ' . $work['name'] . ' ' . $work['second_name'] . "<br>";
                        $textInfoWorketCompany['workercallsign'] .= $work['callsign'] . '<br>';

                        // поиск информации об компании
                        $company = TenantCompany::find()->asArray()->where(['tenant_company_id' => $work['tenant_company_id'] ])->one();
                        if($company){
                            $textInfoWorketCompany['companyName'] .= $company['name'] . "<br>";
                        }else{
                            $textInfoWorketCompany['companyName'] .= '<br>' ;
                        }

                    }
                }else{
                    $textInfoWorketCompany['nameWorker'] = 'Nicht gefunden';
                    $textInfoWorketCompany['companyName'] = 'Nicht gefunden';
                    $textInfoWorketCompany['workercallsign'] = '';
                }
            }
        }else{
            $textInfoWorketCompany['nameWorker'] = 'Nicht gefunden';
            $textInfoWorketCompany['companyName'] = 'Nicht gefunden';
            $textInfoWorketCompany['workercallsign'] = '';
        }
        return $textInfoWorketCompany;
    }





    public function getRatingMap()
    {
        $col = range(1, 5);

        array_unshift($col, t('order', 'Without rating'));

        return $col;
    }

    public function getExchangeMap()
    {
        $exchangePrograms = ExchangeProgram::find()->all();

        return ArrayHelper::map($exchangePrograms, 'exchange_program_id', function ($item) {
            return $item['name'];
        });
    }

    public function getExtrinsicDispatcherMap()
    {
        /* @var $extrinsicDispatcherRepository ExtrinsicDispatcherRepository */
        $extrinsicDispatcherRepository = \Yii::$container->get(ExtrinsicDispatcherRepository::class);

        return ArrayHelper::map(
            $extrinsicDispatcherRepository->getAllExtrinsicDispatchersByTenant(user()->tenant_id),
            'user_id',
            function (User $user) {
                return Html::encode($user->getFullName());
            }
        );
    }
}
