<?php


namespace app\modules\reports\models\orders;


class StatisticSearch extends BaseOrderSearch
{
    /**
     * @return array|bool
     */
    public function search()
    {
        if (!$this->validate()) {
            return false;
        }

        return OrderReport::getOrderStat($this->getCityId(), [$this->first_date, $this->second_date],
            $this->position_id, $this->tenantCompanyIds);
    }
}