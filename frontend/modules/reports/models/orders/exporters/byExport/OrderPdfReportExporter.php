<?php

namespace app\modules\reports\models\orders\exporters\byExport;

use app\modules\reports\models\orders\interfaces\OrderReportExporterByExportType;
use kartik\mpdf\Pdf;



class OrderPdfReportExporter extends OrderReportExporterByExportType
{
    private $content;
    private $file;

    public function __construct($content,$file)
    {
        ini_set('pcre.backtrack_limit', 3000000);

        $this->content = $content;
        $this->file = $file;
    }

    public function export()
    {

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'marginBottom' => 30,
            'marginRight' => 10,
            'marginTop' => 50,
            'mode'        => Pdf::MODE_CORE,
            // A4 paper format
            'format'      => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_DOWNLOAD,
            'filename'    => $this->file,
            // your html content input
            'content'     => $this->content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile'     => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline'   => '.kv-heading-1{font-size:18px}',
            // set mPDF properties on the fly
            'options'     => ['title' => 'Kabbi'],

            'methods' => [
                'SetAuthor' => 'Kabbi',
            ]
            // call mPDF methods on the fly

        ]);

        $data = $pdf->render();

        header('Content-Description: File Transfer');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: public, must-revalidate, max-age=0');
        header('Pragma: public');
        header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Content-Type: application/pdf');

        if (!isset($_SERVER['HTTP_ACCEPT_ENCODING']) || empty($_SERVER['HTTP_ACCEPT_ENCODING'])) {
            // don't use length if server using compression
            header('Content-Length: ' . strlen($data));
        }

        header('Content-Disposition: attachment; filename="' . $this->file . '"');
        echo $data;
        // return the pdf output as per the destination setting
        //return $pdf->render();

    }

}
