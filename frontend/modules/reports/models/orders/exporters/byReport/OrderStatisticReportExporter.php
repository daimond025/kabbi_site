<?php
namespace app\modules\reports\models\orders\exporters\byReport;

use app\modules\reports\models\orders\factories\OrderReportExporterByReportType;
use app\modules\reports\models\orders\helpers\OrderReportExporterHelper;

class OrderStatisticReportExporter extends OrderReportExporterByReportType
{

    public function getColumns(){

        $columns = parent::getColumns();

        $columns = array_merge($columns, [
                [
                    'attribute' => 'active',
                    'label'     => t('order', 'Active'),
                    'value'     => function ($model) {
                        return $model->client->activeFieldValue;
                    },
                ],

            ]
        );

        return $columns;
    }


}