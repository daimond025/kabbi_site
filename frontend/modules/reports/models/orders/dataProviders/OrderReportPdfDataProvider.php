<?php
/**
 * Created by PhpStorm.
 * User: zigfried123
 * Date: 13.02.2018
 * Time: 9:54
 */

namespace app\modules\reports\models\orders\dataProviders;

use app\modules\order\services\OrderService;
use app\modules\reports\models\orders\exporters\byExport\OrderPdfReportExporter;
use app\modules\reports\models\orders\interfaces\OrderReportPdfDataProviderInterface;

class OrderReportPdfDataProvider implements OrderReportPdfDataProviderInterface
{

    public $content;
    public $dataInit;

    public function __construct($dataInit)
    {
        $this->dataInit = $dataInit;
    }

    public function getData($dataInit)
    {
        $content = $this->getContent($dataInit['dataProvider']);
        $this->content = $content;
        return $this;
    }

    public function getContent($dataProvider)
    {
        return OrderService::getModels($dataProvider);
    }

    public function setExporter()
    {
        return new OrderPdfReportExporter($this->content, $this->dataInit['filenamePdf']);
    }
}
