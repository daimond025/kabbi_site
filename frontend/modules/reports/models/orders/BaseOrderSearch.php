<?php


namespace app\modules\reports\models\orders;


use common\modules\city\models\City;
use frontend\modules\companies\models\TenantCompany;
use yii\base\InvalidConfigException;
use yii\base\Model;

abstract class BaseOrderSearch extends Model
{
    public $city_id;
    public $position_id;
    public $type = 'today';
    public $first_date;
    public $second_date;
    public $access_city_list;
    public $access_position_list;
    public $tenantCompanyIds;

    protected $isSetTime = false;

    public function init()
    {
        if (empty($this->access_position_list)) {
            throw new InvalidConfigException('The access position list must not be set');
        }

        if (empty($this->city_id)) {
            $this->city_id = current($this->access_city_list);
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'in', 'range' => ['today', 'yesterday', 'month', 'period']],
            [['first_date', 'second_date'], 'safe'],
            [['city_id'], 'in', 'range' => $this->access_city_list],
            [['position_id'], 'in', 'range' => $this->getPositionList(), 'allowArray' => true],
            [
                ['tenantCompanyIds'],
                'each',
                'rule' => [
                    'match',
                    'pattern' => sprintf('/^\d+|%s$/', TenantCompany::FIELD_NAME_MAIN_COMPANY)
                ]
            ],
        ];
    }

    public function afterValidate()
    {
        parent::afterValidate();
        $this->setPeriod();
    }

    abstract protected function search();

    protected function getCityId()
    {
        return $this->city_id ? $this->city_id : current($this->access_city_list);
    }

    /**
     * @return array
     */
    protected function getPositionList()
    {
        return $this->position_id ? $this->position_id : $this->access_position_list;
    }

    private function setPeriod()
    {
        $timeOffset = City::getTimeOffset($this->getCityId());

        if (!$this->isSetTime) {
            switch ($this->type) {
                case 'period':
                    list($day, $month, $year) = $this->parseDate($this->first_date );
                    $this->first_date  =  mktime(0, 0, 0, $month, $day, $year);

                    list($day, $month, $year) = $this->parseDate($this->second_date );
                    $this->second_date  =  mktime(23, 59, 59, $month, $day, $year);
                    break;
                case 'yesterday':
                    $month =  date("n", (time() - 24 * 60 * 60));
                    $day =  date("j", (time() - 24 * 60 * 60));
                    $year =  date("Y", (time() - 24 * 60 * 60));

                    $this->first_date = mktime(0, 0, 0, $month, $day, $year);
                    $this->second_date = mktime(23, 59, 59, $month,$day, $year);
                    break;
                case 'month':
                    $day =  date("t", time());
                    $month =  date("n", time());
                    $year =  date("Y", time());

                    $this->first_date = mktime(0, 0, 0, $month, 1,  $year);
                    $this->second_date = mktime(23, 59, 59, $month, $day,  $year);

                    break;
                case 'today':
                    $this->first_date  =  mktime(0, 0, 0, date("n"), date("j"), date("Y"));
                    $this->second_date =  mktime(23, 59, 59, date("n"), date("j"), date("Y"));
                default:
                    $this->first_date = $this->second_date = date("d.m.Y", time());
                    break;
            }

            $this->first_date = (int)app()->formatter->asTimestamp($this->first_date) + $timeOffset;
            $this->second_date = (int)app()->formatter->asTimestamp($this->second_date) + $timeOffset;

            $this->isSetTime = true;
        }
    }

    private function parseDate($date){
        $dates = explode('.', $date );

        return array_map(function($item){
            return (int)$item;
        }, $dates );

    }
}