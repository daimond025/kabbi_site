<?php


namespace app\modules\reports\models\transaction;


class InnerOperatonType
{
    const WITHDRAVAL_FOR_SHIFT = 'withdraval_for_shift';
    const WITHDRAVAL_FOR_ORDER = 'withdraval_for_order';
    const WITHDRAVAL = 'manual_withdraval';
    const CASH_OUT = 'cash_out';
    const REFILL_BALANCE = 'refill_balance';
    const REFILL_BONUS_BALANCE = 'refill_bonus_balance';

    /**
     * @return array
     */
    public static function getTypeMap()
    {
        $names = [
            t('reports', 'Manual withdraval'),
            t('reports', 'Withdraval for shift'),
            t('reports', 'Withdraval for order'),
            t('reports', 'Cash out'),
            t('balance', 'Refill balance'),
            t('reports', 'Refill bonus balance'),
        ];

        return array_combine(self::getTypeList(), $names);
    }

    /**
     * @return array
     */
    public static function getTypeList()
    {
        return [
            self::WITHDRAVAL,
            self::WITHDRAVAL_FOR_SHIFT,
            self::WITHDRAVAL_FOR_ORDER,
            self::CASH_OUT,
            self::REFILL_BALANCE,
            self::REFILL_BONUS_BALANCE,
        ];
    }

    /**
     * @param string $type
     * @return bool
     */
    public static function isRefillType($type)
    {
        return in_array($type, [self::REFILL_BALANCE, self::REFILL_BONUS_BALANCE]);
    }
}