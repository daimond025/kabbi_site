<?php

namespace app\modules\reports\models\transaction;

use common\helpers\CacheHelper;
use Yii;
use app\modules\balance\models\Operation;
use app\modules\balance\models\Account;
use app\modules\balance\models\Transaction;
use frontend\modules\tenant\models\Currency;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

class TransactionReport extends Model
{
    /**
     * Включить/выключить кеширование отчета.
     */
    const STAT_CACHE = false;

    public static function getOuterReport(array $arPeriod)
    {
        if (self::STAT_CACHE && strtotime($arPeriod[1]) < time()) {
            $key = 'transactions_report_outer' . user()->tenant_id . '_' . $arPeriod[0] . '_' . $arPeriod[1];

            return CacheHelper::getFromCache($key, function () use ($arPeriod) {
                return self::getOuterOperatios($arPeriod);
            });
        }

        return self::getOuterOperatios($arPeriod);
    }

    private static function getOuterOperatios(array $arPeriod)
    {
        return Operation::find()
            ->where([
                'between',
                Operation::tableName() . '.date',
                $arPeriod[0],
                $arPeriod[1],
            ])
            ->with([
                'account' => function ($query) {
                    $query->select(['account_id', 'owner_id', 'acc_kind_id', 'currency_id']);
                },
            ])
            ->joinWith([
                'account a'     => function ($query) {
                    $query->where(['tenant_id' => user()->tenant_id, 'acc_type_id' => Account::PASSIVE_TYPE]);
                },
                'transaction t' => function ($query) {
                    $query->where([
                        'or',
                        [
                            'or',
                            [
                                'and',
                                ['t.payment_method' => Transaction::TERMINAL_ELECSNET_PAYMENT],
                                ['!=', 'a.acc_kind_id', Account::TENANT_KIND],
                            ],
                            [
                                'and',
                                ['!=', 't.payment_method', Transaction::TERMINAL_ELECSNET_PAYMENT],
                                ['a.acc_kind_id' => Account::TENANT_KIND],
                            ],
                        ],
                        ['t.type_id' => Transaction::GOOTAX_PAYMENT_TYPE],
                    ]);
                },
            ], false)
            ->orderBy('date')
            ->all();
    }

    /**
     * Переводы для плейсхолдера
     *
     * @param integer $entity_type Соответствует полю acc_kind_id app\modules\balance\models\Account
     *
     * @return string
     */
    public static function getTranslateEntityPlaceholderByType($entity_type)
    {
        $translateMap = self::translateEntityMap();

        return getValue($translateMap[$entity_type]['placeholder']);
    }

    /**
     * Карта переводов сущностей
     * @return array
     */
    public static function translateEntityMap()
    {
        return [
            Account::CLIENT_KIND       => [
                'placeholder' => t('reports', 'All clients'),
                'name'        => t('app', 'Client'),
                'url'         => '/client/base/update',
            ],
            Account::CLIENT_BONUS_KIND => [
                'placeholder' => t('reports', 'All clients'),
                'name'        => t('app', 'Client'),
                'url'         => '/client/base/update',
            ],
            Account::COMPANY_KIND      => [
                'placeholder' => t('reports', 'All companies'),
                'name'        => t('app', 'Company'),
                'url'         => '/client/company/update',
            ],
            Account::WORKER_KIND       => [
                'placeholder' => t('reports', 'All workers'),
                'name'        => t('app', 'Worker'),
                'url'         => '/employee/worker/update',
            ],
        ];
    }

    /**
     * Имя сущности
     *
     * @param integer $acc_kind_id
     *
     * @return string
     */
    public static function getEntityName($acc_kind_id)
    {
        $translateMap = self::translateEntityMap();

        return getValue($translateMap[$acc_kind_id]['name']);
    }

    /**
     * Урл сущности
     *
     * @param integer $acc_kind_id
     *
     * @return string
     */
    public static function getEntityUrl($acc_kind_id)
    {
        $translateMap = self::translateEntityMap();

        return getValue($translateMap[$acc_kind_id]['url']);
    }

    /**
     * @param array $arPeriod
     * @return array
     */
    public static function getSystemOpenPeriodSaldo(array $arPeriod)
    {
        return self::getOpenPeriodSaldo($arPeriod, Account::SYSTEM_KIND);
    }

    public static function getSystemBonusOpenPeriodSaldo(array $arPeriod)
    {
        return self::getOpenPeriodSaldo($arPeriod, Account::SYSTEM_BONUS_KIND);
    }

    /**
     * @param array $arPeriod
     * @param int $accKindId
     * @return array|mixed
     */
    private static function getOpenPeriodSaldo(array $arPeriod, $accKindId)
    {
        $getSystemOpenPeriodSaldo = function () use ($arPeriod, $accKindId) {
            $systemSaldoRows = Yii::$app->db->createCommand(
                "SELECT
                  currency_id,
                  IF(type_id = :type_id, saldo - sum, saldo + sum) as saldo
                FROM (SELECT a.currency_id, type_id, saldo, sum
                      FROM {{%operation}} AS o 
                        INNER JOIN {{%account}} AS a ON o.account_id = a.account_id
                      WHERE (o.date BETWEEN STR_TO_DATE(:start_date, '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE(
                          :end_date, '%Y-%m-%d %H:%i:%s')) AND tenant_id = :tenant_id AND acc_kind_id = :acc_kind_id
                      ORDER BY date ASC) AS operations
                GROUP BY currency_id"
            )
                ->bindValue(':start_date', $arPeriod[0])
                ->bindValue(':end_date', $arPeriod[1])
                ->bindValue(':tenant_id', user()->tenant_id)
                ->bindValue(':acc_kind_id', $accKindId)
                ->bindValue(':type_id', Operation::INCOME_TYPE)
                ->queryAll();

            return ArrayHelper::map($systemSaldoRows, function ($item) {
                return Currency::getCurrencySymbol($item['currency_id']);
            }, 'saldo');
        };

        if (date('Y-m-d', strtotime($arPeriod[1])) === date('Y-m-d')) {
            return $getSystemOpenPeriodSaldo();
        }

        return Yii::$app->cache->getOrSet([__CLASS__, $arPeriod, user()->tenant_id], $getSystemOpenPeriodSaldo);
    }
}
