<?php

namespace app\modules\reports\models\transaction;

use app\modules\balance\models\Account;
use app\modules\balance\models\Operation;
use app\modules\balance\models\Transaction;
use frontend\modules\car\models\CarClass;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\db\ActiveQuery;

class InnerOperationSearch extends Model
{
    public $firstDate;
    public $lastDate;
    public $ownerId;
    public $accKindId;
    public $cityId;
    public $positionId;
    public $operationType;
    public $accessCityList;
    public $carClass;

    public function init()
    {
        if (empty($this->accessCityList)) {
            throw new InvalidConfigException('The accessCityList must be set');
        }
    }

    public function rules()
    {
        return [
            [['firstDate', 'lastDate'], 'required'],
            [['firstDate', 'lastDate'], 'date', 'format' => 'php:Y-m-d H:i:s'],
            [['ownerId', 'accKindId', 'positionId'], 'integer'],
            ['cityId', 'in', 'range' => $this->accessCityList, 'allowArray' => true],
            ['operationType', 'in', 'range' => InnerOperatonType::getTypeList()],
            ['carClass', 'in', 'range' => $this->getCarClassList(), 'allowArray' => true],
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function search()
    {
        $query = Operation::find()
            ->alias('op');

        if (!$this->validate()) {
            $query->where('0=1');

            return $query;
        }

        $query->with([
            'transaction.userCreated' => function (ActiveQuery $query) {
                $query->select(['user_id', 'name', 'last_name', 'second_name', 'position_id']);
            },
            'transaction.order'       => function (ActiveQuery $query) {
                $query->select(['order_id', 'position_id']);
                $query->filterWhere(['position_id' => $this->positionId]);
                $query->joinWith([
                    'car' => function (ActiveQuery $query) {
                        $query->andFilterWhere(['class_id' => $this->carClass]);
                    },
                ], false);
            },
        ])
            ->orderBy('op.date');

        $this->addRequiredWhereCondition($query);

        return $query;
    }

    public function getBalances()
    {
        if (!$this->validate()) {
            return null;
        }

        $getPeriodBalances = function () {
            $query = Operation::find()
                ->alias('op')
                ->select([
                    'currency_id',
                    'op.type_id',
                    'SUM(op.sum) as total',
                ])
                ->asArray()
                ->groupBy(['currency_id', 'type_id']);
            $this->addRequiredWhereCondition($query);

            return $query->all();
        };

        if ($this->isLastDateToday()) {
            return $getPeriodBalances();
        }

        return Operation::getDb()->cache($getPeriodBalances);
    }

    private function addRequiredWhereCondition(ActiveQuery $query)
    {
        $query->where([
            'between',
            'op.date',
            $this->firstDate,
            $this->lastDate,
        ])->joinWith([
            'account a'     => function (ActiveQuery $query) {
                $query->select(['a.account_id', 'a.owner_id', 'a.acc_kind_id', 'a.currency_id']);

                $query->where([
                    'a.tenant_id'   => user()->tenant_id,
                    'a.acc_type_id' => Account::PASSIVE_TYPE,
                ]);

                if ($this->operationType == InnerOperatonType::REFILL_BONUS_BALANCE) {
                    $this->accKindId = [Account::CLIENT_BONUS_KIND, Account::CLIENT_BONUS_UDS_GAME_KIND];
                } elseif ($this->operationType == InnerOperatonType::REFILL_BALANCE && empty($this->accKindId)) {
                    $this->accKindId = [Account::CLIENT_KIND, Account::WORKER_KIND, Account::COMPANY_KIND];
                }

                $query->andFilterWhere([
                    'a.owner_id'    => $this->ownerId,
                    'a.acc_kind_id' => $this->accKindId,
                ]);

                $query->andWhere([
                    'not in',
                    'a.acc_kind_id',
                    [Account::TENANT_KIND],
                ]);
            },
            'transaction t' => function (ActiveQuery $query) {
                $query->select([
                    't.transaction_id',
                    't.user_created',
                    't.payment_method',
                    't.terminal_transact_number',
                    't.type_id',
                    't.city_id',
                    't.order_id',
                ]);

                $query->andWhere(['t.city_id' => $this->cityId]);

                if (!empty($this->operationType)) {
                    if ($this->operationType === InnerOperatonType::WITHDRAVAL_FOR_SHIFT) {
                        $query->andWhere([
                            't.payment_method' => Transaction::WITHDRAWAL_TARIFF_PAYMENT,
                            't.type_id'        => Transaction::WITHDRAWAL_TYPE,
                        ]);
                    } elseif ($this->operationType === InnerOperatonType::WITHDRAVAL_FOR_ORDER) {
                        $query->andWhere([
                            't.type_id' => Transaction::ORDER_PAYMENT_TYPE,
                        ]);
                    } elseif ($this->operationType === InnerOperatonType::WITHDRAVAL) {
                        $query->andWhere([
                            't.payment_method' => Transaction::WITHDRAWAL_PAYMENT,
                            't.type_id'        => Transaction::WITHDRAWAL_TYPE,
                        ]);
                    } elseif ($this->operationType === InnerOperatonType::CASH_OUT) {
                        $query->andWhere([
                            't.payment_method' => Transaction::WITHDRAWAL_CASH_OUT_PAYMENT,
                            't.type_id'        => Transaction::WITHDRAWAL_TYPE,
                        ]);
                    }
                }
            },
        ], true, 'INNER JOIN');

        if (InnerOperatonType::isRefillType($this->operationType)) {
            $query->andWhere(['op.type_id' => Operation::INCOME_TYPE]);
        } elseif (InnerOperatonType::WITHDRAVAL_FOR_ORDER === $this->operationType) {
            $query->andWhere(['op.type_id' => Operation::EXPENSES_TYPE]);
        }
    }

    /**
     * @return array
     */
    public static function getCarClassMap()
    {
        return CarClass::getClasses(true);
    }

    private function getCarClassList()
    {
        return CarClass::getClassIdList();
    }

    /**
     * @return bool
     */
    private function isLastDateToday()
    {
        return date('Y-m-d', strtotime($this->lastDate)) === date('Y-m-d');
    }
}
