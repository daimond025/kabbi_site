<?php

namespace app\modules\reports\models\transaction;

use app\modules\balance\models\Account;
use app\modules\balance\models\Operation;
use yii\base\Model;
use yii\db\ActiveQuery;

class BonusReport extends Model
{
    public $firstDate;
    public $lastDate;
    public $ownerId;
    public $tenantId;

    public function rules()
    {
        return [
            [['firstDate', 'lastDate', 'tenantId'], 'required'],
            [['firstDate', 'lastDate'], 'date', 'format' => 'php:Y-m-d H:i:s'],
            [['ownerId', 'tenantId'], 'integer'],
        ];
    }

    public function init()
    {
        if (!$this->validate()) {
            throw new \InvalidArgumentException('Not valid model data');
        }
    }

    /**
     * @return ActiveQuery
     */
    public function buildOperationsQuery()
    {
        $query = Operation::find()
            ->alias('op')
            ->with([
                'transaction.userCreated' => function (ActiveQuery $query) {
                    $query->select(['user_id', 'name', 'last_name', 'second_name', 'position_id']);
                },
            ])
            ->orderBy('op.date');

        $this->addWhereCondition($query);

        return $query;
    }

    public function getBalances()
    {
        $getPeriodBalances = function () {
            $query = Operation::find()
                ->alias('op')
                ->select([
                    'currency_id',
                    'op.type_id',
                    'SUM(op.sum) as total',
                ])
                ->asArray()
                ->groupBy(['currency_id', 'type_id']);

            $this->addWhereCondition($query);

            return $query->all();
        };

        if ($this->isLastDateToday()) {
            return $getPeriodBalances();
        }

        return Operation::getDb()->cache($getPeriodBalances);
    }

    /**
     * @return bool
     */
    private function isLastDateToday()
    {
        return date('Y-m-d', strtotime($this->lastDate)) === date('Y-m-d');
    }

    private function addWhereCondition(ActiveQuery $query)
    {
        $query->where([
            'between',
            'op.date',
            $this->firstDate,
            $this->lastDate,
        ])
            ->joinWith([
                'account a' => function (ActiveQuery $query) {
                    $query->where([
                        'tenant_id'   => $this->tenantId,
                        'acc_type_id' => Account::PASSIVE_TYPE,
                        'acc_kind_id' => Account::CLIENT_BONUS_KIND,
                    ]);
                    $query->andFilterWhere(['owner_id' => $this->ownerId]);
                    $query->select([
                        'a.account_id',
                        'a.owner_id',
                        'a.acc_kind_id',
                        'a.currency_id',
                    ]);
                },
            ]);
    }
}
