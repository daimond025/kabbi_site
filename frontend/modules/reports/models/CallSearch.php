<?php

namespace frontend\modules\reports\models;

use app\modules\order\models\Call;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class CallSearch extends Call
{
    const PAGE_SIZE = 50;


    public function rules()
    {
        return [];
    }

    public function search($tenantId, $dispatcherId, $period)
    {
        $startDate = ArrayHelper::getValue($period, 0);
        $endDate    = ArrayHelper::getValue($period, 1);
        $query     = Call::find()
            ->where([
                'tenant_id'       => $tenantId,
                'user_opertor_id' => $dispatcherId,
            ])
            ->andWhere(['between', 'starttime', $startDate, $endDate])
            ->orderBy(['starttime' => SORT_DESC]);

        return new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);



    }
}