<?php

namespace frontend\modules\reports\models;

use MongoDB\BSON\ObjectID;
use yii\data\ActiveDataProvider;

/**
 * Class NotificationReportSearch
 * @package frontend\modules\reports\models
 *
 * @var $first_date string
 * @var $second_date string
 */
class NotificationReportSearch extends NotificationReport
{

    const PAGE_SIZE = 50;

    public $first_date;
    public $second_date;
    public $type;

    public function attributeLabels()
    {
        return [
            'created_at' => t('reports', 'Date'),
            'title' => t('notification', 'Name'),
            'client_type' => t('notification', 'Addressee'),
            'total' => t('reports', 'Total'),
        ];
    }

    public function rules()
    {
        return [
            [['type'], 'in', 'range' => ['today', 'yesterday', 'month', 'period']],
            [['first_date', 'second_date', 'city_id'], 'safe'],
        ];
    }


    public function search($params)
    {

        $query = self::find()
            ->where(['tenant_id' => user()->tenant_id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'       => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
                'attributes' => []
            ],
            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        switch ($this->type) {
            case 'period':
                break;
            case 'yesterday':
                $this->first_date = $this->second_date = date("d.m.Y", (time() - 24 * 60 * 60));
                break;
            case 'month':
                $this->first_date = date('d.m.Y', mktime(0, 0, 0, date("n"), 1));
                $this->second_date = date('d.m.Y');
                break;
            case 'today':
            default:
                $this->first_date = $this->second_date = date('d.m.Y');
                $this->type = 'today';
                break;
        }

        $firstDate = strtotime($this->first_date);
        $secondDate = strtotime($this->second_date) + 24 * 60 * 60;

        $query->andWhere(['between', 'created_at', $firstDate, $secondDate]);

        return $dataProvider;
    }

    public function searchById($notificationId)
    {
        $query = self::find()
            ->where([
                'tenant_id' => user()->tenant_id,
                '_id' => new ObjectID($notificationId),
            ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'       => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
                'attributes' => []
            ],
            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);

        return $dataProvider;
    }

}