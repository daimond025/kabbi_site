<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\reports\assets;

use yii\web\AssetBundle;

/**
 * author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ReportsAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/reports/assets/js';

    public $publishOptions = [
        'forceCopy' => true,
    ];

    public $css = [
    ];
    public $js = [
        'functions.js',
    ];
    public $depends = [
        'app\modules\order\assets\ViewOrderAsset'
    ];
}