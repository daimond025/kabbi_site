$(function() {
    //Обработка формы поиcка статистики
     $('form[name="stat_filter"] a.oor_compare').on('click', function(e){
        e.preventDefault();

        var $this = $(this);
        var form = $this.parents('form');

        if ($this.hasClass('loading_button')) {
            return;
        }

        $this.addClass('loading_button');
        $.post(
            form.attr('action'),
            form.serialize(),
            function(json){
                $('.tabs_content div[data-action="orders"]').html(json.orders);
                $('.tabs_content div[data-action="staff"]').html(json.staff);
            },
            'json'
        )
        .always(function () {
            $this.removeClass('loading_button');
        });
    });

    //Инициализация открытия карточки заказа
    registerOrderOpenInModalEvent('.show_order');

    //Спойлер сотрудников
    $('body').on('click', '.open_spoler_tr', function(){ $('[data-tr-id="'+$(this).attr('data-tr-spoler')+'"]').toggle(); });
});