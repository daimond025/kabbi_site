$(function(){
    //Табы
    var active_tab = $('#driver_stat');

    function registerWorkerEndWorkEvent() {
        $('.js-worker-end-work').on('click', function (e) {
            'use strict';
            var $this = $(this);
            if (confirm($this.data('message'))) {
                $.post(
                    $this.data('href'),
                    {
                        workerId: $this.data('worker_id'),
                        shiftId: $this.data('shift_id')
                    }
                )
                    .done(function (data) {
                        location.reload(true);
                    })
                    .fail(function (data) {
                        console.log('fail', data);
                    });
            }

            return false;
        });
        $('.js-worker-unblock').on('click', function () {
            'use strict';
            var $this = $(this);
            if (confirm($this.data('message'))) {
                $.post(
                    $this.data('href'),
                    {
                        workerId: $this.data('worker_id'),
                        blockId: $this.data('block_id')
                    }
                )
                    .done(function (data) {

                        location.reload(true);
                    })
                    .fail(function (data) {
                        console.log('fail', data);
                    });
            }
            return false;
        });
    }

    $(document).on('click', '.oor_content h3 a', function(e){
        e.preventDefault();

        if (!$(this).hasClass('active')) {
            $(this).parents('.oor_content').find('a.active').removeClass('active');
            $(this).addClass('active');
            var tab_id = $(this).data('tab');
            active_tab = $('#' + tab_id);
            active_tab.parent().find('div.active').removeClass('active');
            active_tab.addClass('active');
        }
    });
    //----------------------------------------------------------------------

    //Обработка формы поиcка смен
    $(document).on('click', 'form[name="shift_search"] a.oor_compare', function(e){
        e.preventDefault();
        var form = $(this).parents('form');
        var $this = $(this);
        var formData;

        if ($this.hasClass('loading_button')) {
            return;
        }

        formData = form.serializeArray();
        formData.push({name: 'hideFilter', value: '1'});
        formData = formData.reduce(function (obj, item) {
            obj[item.name] = item.value;
            return obj;
        }, {});

        $this.addClass('loading_button');
        $.post(form.attr('action'), formData)
            .done(function (html) {
                $('#shift_stat').html(html);
            })
            .always(function () {
                $this.removeClass('loading_button');
                registerWorkerEndWorkEvent();
            });
    });

    $('.js-shifts-tab').on('tab-loaded', function () {
        registerWorkerEndWorkEvent();
    });

    //Инициализация открытия карточки заказа
    registerOrderOpenInModalEvent('.js-order-view');

    $(document).on('click', '.js-spoiler', function () {
        $(this).prevAll('.js-full').toggle();
        $(this).prevAll('.js-preview').toggle();
    });
});