

$(function() {

    //Инициализация открытия детальных строк статистики
    openDetailRowsInit();


    //Инициализация открытия карточки заказа
    registerOrderOpenInModalEvent('.js-order-view');

    $(document).on('click', '.js-spoiler', function () {
        $(this).prevAll('.js-full').toggle();
        $(this).prevAll('.js-preview').toggle();
    });

});