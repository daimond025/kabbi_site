$(function(){
    
    // запросить список смен
    $(document).on('click', '#t01 a.oor_compare', function () {
        show_shifts();
        return false;
    });
    
    // запросить список звонков
    $(document).on('click', '#t02 a.oor_compare', function () {
        show_calls();
        return false;
    });
    
//    $(document).one('click', 'a.t02', function () {
//        show_calls();
//    });
    
    // закрыть смену.
    $(document).on('click', '.js-operator-end-work', function (e) {
        // временно отключено
        return false;
    });
    
    // ajax запрос. Вывести список смен
    function show_shifts() {
        var form = $('form[name="shift_search"]');
        $.ajax({
            type: "GET",
            url: form.data('action'),
            data: form.serialize(),
            dataType: "html",
            success: function (html) {
                $('#shift_stat').html(html);
            }

        });
    }
    
    function show_calls() {
        var form = $('form[name="call_search"]');
        $.ajax({
            type: "GET",
            url: form.data('action'),
            data: form.serialize(),
            dataType: "html",
            success: function (html) {
                $('#call_stat').html(html);
            }

        });
    }
    
    
    show_shifts();
    show_calls();
    
    //Инициализация открытия карточки заказа
    registerOrderOpenInModalEvent('.open_order');
    
});