$(function () {
    calendarFilterInit();

    $('body').on('click', '.mgn_item label', function () {
        $('.mgn_item').find('input[type="text"]').prop('disabled', true);
        $('.mgn_item').find('select').prop('disabled', true).trigger('update');
        $(this).parent().find('input[type="text"]').prop('disabled', false);
        $(this).parent().find('select').prop('disabled', false).trigger('update');
    });

    //Обработка кнопки "Показать"
    $(document).on('click', '.tabs_content a.report_search', function (e) {
        e.preventDefault();
//        var city_id = $('#report_city_id').val();
//        city_id = city_id == 0 ? '' : city_id;
        var form = $(this).parents('form');
        var table = form.next('div.report_data table.transaction_list');
        var view = table.size() > 0 ? '_rows' : '_grid';
        var $this = $(this);

        if ($this.hasClass('loading_button')) {
            return;
        }

        $this.addClass('loading_button');
        $.post(
            form.attr('action'),
            form.serialize() + '&view=' + view,
            function (html) {
                if (view === '_grid') {
                    form.next('div.report_data').html(html);
                } else {
                    var first_tr = table.find('tr:first');
                    first_tr.nextAll('tr').remove();
                    first_tr.after(html);
                }
            }
        )
            .always(function () {
                $this.removeClass('loading_button');
            });
    });

    $('body').on('click', '.ch_tr input[type="checkbox"]', function () {
        if ($(this).parent().find('input[type="text"]').prop('disabled'))
            $(this).parent().find('input[type="text"]').prop('disabled', false);
        else
            $(this).parent().find('input[type="text"]').prop('disabled', true);
    });

    //Подстановка перевода placeholder
    $(document).on('change', '.entity_type', function (e) {
        var form = $(this).parents('form');
        $.get(
            '/reports/transaction/get-entity-translate',
            {entity_type: $(this).val()},
            function (json) {
                if (json.translate != '') {
                    form.find('input[name="entity_name"]').attr('placeholder', json.translate).val('');
                }
            },
            'json'
        );
    });

    //Поиск cущности отчета
    var search_val_old = '';

    $('body').on('keyup', '.entity_name', function (e) {
        var input_search = $(this);

        if (e.which != 13) {
            var form = input_search.closest('form');

            if (e.which == 8) {
                form.find('.entity_id').val('');
            }

            setTimeout(function () {

                var search = input_search.val();

                if (search_val_old == search) {
                    return false;
                }

                search_val_old = search;

                if (search.length > 0 && $.trim(search) != '') {
                    $.ajax({
                        type: "GET",
                        url: '/reports/transaction/get-entity-data',
                        data: form.serialize(),
                        dataType: "html",
                        beforeSend: function () {
                            input_search.addClass('input_preloader');
                        },
                        success: function (html) {
                            input_search.removeClass('input_preloader');
                            input_search.next().html(html).show();
                            $('body').addClass('entitySearch');
                        }
                    });
                }
            }, 700);
        }
    });

    //Клик по выбранному водителю
    $('body').on('click', '.autocomplete_entity li:not(".no_select") a', function (e) {
        var $form = $(this).closest('form');
        e.preventDefault();
        e.stopPropagation();
        $form.find('.entity_name').val($(this).text());
        $form.find('.entity_id').val($(this).data('entity_id'));
        $(this).parents('ul').hide();
    });

    //Сокрытие поиска
    $('body').on('click', function () {
        if ($(this).hasClass('entitySearch')) {
            $('ul.driver_auto').hide();
            $(this).removeClass('entitySearch');
        }
    });
    //-------------------------------------------------------------------------

    registerOrderOpenInModalEvent('.open_order');


    $(document).on('click', '.js-export-transaction', function (e) {
        e.preventDefault();

        var params = $(this).parents('form').serialize();
        var url = $(this).attr('href');
        url = params != '' ? url + '?' + params : url;
        window.location.href = url;
    });

    $(document).on('click', '.ajax-report-pagination a[data-page]', function (event) {
        event.preventDefault();

        if ($(this).hasClass('disable')) {
            return;
        }

        var activeTab = $(this).parents('div.active')
        var form = activeTab.children('form');
        var contentContainer = activeTab.find('.report_data .people_table tbody');
        var pagerContainer = $(this).closest('.ajax-report-pagination');

        $.ajax({
            method: "POST",
            url: $(this).attr('href'),
            data: form.serialize(),
            beforeSend: function (xhr) {
                pagerContainer.find('a[data-page]').addClass('disable')
                    .css('color', '#797979').find('span').unwrap();
            }
        })
            .done(function (json) {
                $('#' + contentContainer.attr('id')).html(json.content);
                pagerContainer.replaceWith(json.pager);
            });
    })

});