$(function(){
    //Табы
    var active_tab = $('#driver_stat');

    function registerDriverEndWorkEvent() {
        $('.js-driver-end-work').on('click', function (e) {
            'use strict';
            var $this = $(this);
            if (confirm($this.data('message'))) {
		$.post(
                    $this.data('href'),
                    {driverId: $this.data('driver_id')}
                )
                    .done(function (data) {
                        location.reload(true);
                    })
                    .fail(function (data) {
                        console.log('fail', data);
                    });
            }

            return false;
        });
        $('.js-driver-unblock').on('click', function () {
            'use strict';
            var $this = $(this);
            if (confirm($this.data('message'))) {
		$.post(
                    $this.data('href'),
                    {driverId: $this.data('driver_id')}
                )
                    .done(function (data) {
                        $('.js-driver-unblock').remove();
                    })
                    .fail(function (data) {
                        console.log('fail', data);
                    });
            }
            return false;
        });
    }

    $(document).on('click', '.oor_content h3 a', function(e){
        e.preventDefault();

        if (!$(this).hasClass('active')) {
            $(this).parents('.oor_content').find('a.active').removeClass('active');
            $(this).addClass('active');
            var tab_id = $(this).data('tab');
            active_tab = $('#' + tab_id);
            active_tab.parent().find('div.active').removeClass('active');
            active_tab.addClass('active');
        }
    });
    //----------------------------------------------------------------------

    //Инициализация открытия карточки заказа
    registerOrderOpenInModalEvent('.ot_edit');

    //Обработка формы поиcка статистики
    $('form[name="stat_search"] a.oor_compare').on('click', function(e){
        e.preventDefault();
        var form = $(this).parents('form');
        var action = $('.oor_content h3 a.active').attr('href');
        var $this = $(this);

        if ($this.hasClass('loading_button')) {
            return;
        }

        $this.addClass('loading_button');
        $.post(
            action,
            form.serialize(),
            function(html){
                active_tab.html(html);
            }
        )
            .always(function () {
                $this.removeClass('loading_button');
            });
    });

    //Обработка формы поиcка смен
    $(document).on('click', 'form[name="shift_search"] a.oor_compare', function(e){
        e.preventDefault();
        var form = $(this).parents('form');
        var $this = $(this);

        if ($this.hasClass('loading_button')) {
            return;
        }

        $this.addClass('loading_button');
        $.post(
            form.attr('action'),
            form.serialize(),
            function(html){
                $('#shift_stat').html(html);
            }
        )
            .always(function () {
                $this.removeClass('loading_button');
                registerDriverEndWorkEvent();
            });
    });

    $('.js-shifts-tab').on('tab-loaded', function () {
        registerDriverEndWorkEvent();
    });
});