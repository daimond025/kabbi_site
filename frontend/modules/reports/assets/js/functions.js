//Инициализация календаря в фильтре периодов
function calendarFilterInit()
{
    $('.sdp').each(function () {
        $(this).datepicker({
            altField: $(this).prev('input'),
            onSelect: function (date) { 
                $(this).parent().prev('.a_sc').html(date).removeClass('sel_active'); 
                $(this).parent().hide(); 
            }
        });
        if ($(this).hasClass('first_date')) {
            $(this).datepicker("setDate", '-14');
        }
        $(this).closest('.cof_date').find('.a_sc').text($(this).val());
    }); 
}

//Инициализация открытия детальных строк статистики
function openDetailRowsInit()
{
    $('body').on('click', '.oor_thead', function () {
        $('[data-theadc="' + $(this).data('thead') + '"]').toggle();
    });
}
