<?php

return [
    \frontend\modules\sms\components\SmsProviderComponent::class => [
        'providers' => [
            1  => \frontend\modules\sms\models\iqsms\IqSmsProvider::class,
            2  => \frontend\modules\sms\models\ibatele\IbateleProvider::class,
            3  => \frontend\modules\sms\models\sapsan\SapsanProvider::class,
            4  => \frontend\modules\sms\models\eyeline\EyelineSmsProvider::class,
            5  => \frontend\modules\sms\models\qtsms\QtSmsProvider::class,
            6  => \frontend\modules\sms\models\msmsms\MsmSmsProvider::class,
            7  => \frontend\modules\sms\models\bulkSms\BulkSmsProvider::class,
            8  => \frontend\modules\sms\models\gateway\GatewaySmsProvider::class,
            9  => \frontend\modules\sms\models\maraditSms\MaraditProvider::class,
            10 => \frontend\modules\sms\models\ibateleSmpp\IbateleSmppProvider::class,
            11 => \frontend\modules\sms\models\clickatell\ClickatellProvider::class,
            12 => \frontend\modules\sms\models\smsc\SmscProvider::class,
            13 => \frontend\modules\sms\models\gosms\GosmsProvider::class,

            14 => \frontend\modules\sms\models\bsg\BsgSmsProvider::class,

            15 => \frontend\modules\sms\models\magfa\MagfaProvider::class,
            16 => \frontend\modules\sms\models\alphasms\AlphasmsProvider::class,
            17 => \frontend\modules\sms\models\qtsmsKuban\QtSmsKubanProvider::class,
            18 => \frontend\modules\sms\models\promoSms\PromoSmsProvider::class,
            19 => \frontend\modules\sms\models\megafonTjSmpp\MegafonTjSmppProvider::class,
            20 => \frontend\modules\sms\models\smsbroker\SmsBrokerProvider::class,
            21 => \frontend\modules\sms\models\comtass\ComtassProvider::class,
            22 => \frontend\modules\sms\models\mediasend\MediasendProvider::class,
            23 => \frontend\modules\sms\models\nikita\NikitaProvider::class,
            24 => \frontend\modules\sms\models\smsLine\SmsLineProvider::class,
            25 => \frontend\modules\sms\models\nikitaKg\NikitaKgProvider::class,
            26 => \frontend\modules\sms\models\smsOnline\SmsOnlineProvider::class,
            27 => \frontend\modules\sms\models\semysms\SemysmsProvider::class,

            28 => \frontend\modules\sms\models\messagebird\MessagebirdSmsProvider::class,
        ],
    ],

    \frontend\modules\sms\models\maraditSms\Maradit::class => [
        'baseUrl'           => getenv('SMS_MARADIT_URL') . 'sendsmsjm/restful/quicksms/',
        'connectionTimeout' => getenv('CURL_CONNECT_TIMEOUT'),
        'timeout'           => getenv('CURL_TIMEOUT'),
    ],

    \frontend\modules\sms\models\alphasms\Alphasms::class => [
        'baseUrl' => getenv('SMS_ALPHASMS_URL') . 'api/http.php',
        'version' => 'http',
    ],

    \frontend\modules\sms\models\bsg\BsgSms::class => [
        'baseUrl'           => getenv('SMS_BSGSMS_URL') . 'xml',
        'connectionTimeout' => getenv('CURL_CONNECT_TIMEOUT'),
        'timeout'           => getenv('CURL_TIMEOUT'),
    ],

    \frontend\modules\sms\models\ibateleSmpp\IbateleSmppSms::class => [
        'requestBalanceServerAddress'          => getenv('SMS_IBATELE_SMPP_URL') . 'xml/balance.php',
        'connectionTimeout'                    => getenv('CURL_CONNECT_TIMEOUT'),
        'timeout'                              => getenv('CURL_TIMEOUT'),
    ],

    \frontend\modules\sms\models\ibatele\IbateleSms::class => [
        'requestBalanceServerAddress'          => getenv('SMS_IBATELE_LK_URL') . 'xml/balance.php',
        'connectionTimeout'                    => getenv('CURL_CONNECT_TIMEOUT'),
        'timeout'                              => getenv('CURL_TIMEOUT'),
    ],

    \frontend\modules\sms\models\iqsms\IqSms::class => [
        'host'              => getenv('SMS_IQSMS_HTTP_HOST'),
        'host_balance'      => getenv('SMS_IQSMS_BALANCE_HTTP_HOST'),
        'connectionTimeout' => getenv('CURL_CONNECT_TIMEOUT'),
        'timeout'           => getenv('CURL_TIMEOUT'),
    ],

    \frontend\modules\sms\models\msmsms\MsmSms::class => [
        'baseUrl'           => getenv('SMS_MSMSMS_URL'),
        'connectionTimeout' => getenv('CURL_CONNECT_TIMEOUT'),
        'timeout'           => getenv('CURL_TIMEOUT'),
    ],

    \frontend\modules\sms\models\qtsms\QtSms::class => [
        'hostname' => getenv('SMS_QTSMS_SSL_HOST'),
        'timeout'  => getenv('CURL_TIMEOUT'),
    ],

    \frontend\modules\sms\models\qtsmsKuban\QtSmsKuban::class => [
        'hostname' => getenv('SMS_QTSMS_KUBAN_SSL_HOST'),
        'timeout'  => getenv('CURL_TIMEOUT'),
    ],

    \frontend\modules\sms\models\sapsan\SapsanSms::class => [
        'requestBalanceServerAddress' => getenv('SMS_SAPSANSMS_URL') . 'xml/balance.php',
        'connectionTimeout'           => getenv('CURL_CONNECT_TIMEOUT'),
        'timeout'                     => getenv('CURL_TIMEOUT'),
    ],
];
