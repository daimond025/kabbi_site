<?php

namespace app\modules\sms;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\sms\controllers';

    public function init()
    {
        parent::init();

        \Yii::$app->setContainer([
            'definitions' => require __DIR__ . '/smsDefinitions.php',
        ]);
    }
}
