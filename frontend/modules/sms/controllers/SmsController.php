<?php

namespace app\modules\sms\controllers;

use frontend\modules\sms\components\SmsProviderComponent;
use frontend\modules\sms\components\SmsProviderFactory;
use yii\base\Module;
use yii\web\Controller;

class SmsController extends Controller
{
    public $smsProviderComponent;

    public function __construct($id, Module $module, SmsProviderComponent $smsProviderComponent, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->smsProviderComponent = $smsProviderComponent;
    }

    /**
     * @param integer $serverId
     * @param string  $login
     * @param string  $password
     *
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function actionGetBalance($serverId, $login, $password)
    {

        $smsProvider = $this->smsProviderComponent->getProvider($serverId);
        return $smsProvider->requestBalance($login, $password);
    }
}
