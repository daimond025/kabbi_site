<?php

namespace frontend\modules\sms\components;

class InvalidServerIdException extends \RuntimeException
{

    public function getName()
    {
        return 'Invalid provider id';
    }
}
