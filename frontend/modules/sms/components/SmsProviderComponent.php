<?php

namespace frontend\modules\sms\components;

use common\helpers\ArrayHelper;
use frontend\modules\sms\models\BaseSmsProvider;
use yii\base\InvalidConfigException;

class SmsProviderComponent
{
    public $providers;

    /**
     * @param $providerId
     *
     * @return mixed|BaseSmsProvider
     * @throws \yii\base\InvalidConfigException
     */
    public function getProvider($providerId)
    {
        if (!ArrayHelper::keyExists($providerId, $this->providers)) {
            throw new InvalidServerIdException();
        }


        $currentProvider = ArrayHelper::getValue($this->providers, $providerId);



        if (is_string($currentProvider)) {

            $currentProvider              = \Yii::createObject($currentProvider);

            $this->providers[$providerId] = $currentProvider;
        }



        if (!$currentProvider instanceof BaseSmsProvider) {
            throw new InvalidConfigException(gettype($currentProvider) . ' not instance of ' . BaseSmsProvider::class);
        }



        return $currentProvider;
    }
}
