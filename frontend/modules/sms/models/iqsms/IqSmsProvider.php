<?php

namespace frontend\modules\sms\models\iqsms;

use frontend\modules\sms\models\BaseSmsProvider;

/**
 * Class IqSmsProvider
 * @package frontend\modules\sms\models\iqsms
 *
 * @property IqSms $gate
 */
class IqSmsProvider extends BaseSmsProvider
{
    public $gate;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = \Yii::createObject(IqSms::class);
    }

    public function requestBalance($login, $password)
    {
        $this->gate->setApiLogin($login);
        $this->gate->setApiPassword($password);
        $result = $this->gate->credits();
        if ($result['status'] == 'error') {
            $this->error = $result['description'];

            return '';
        } else {
            return $result['balance'][0]['balance'];
        }
    }

    public function getError()
    {
        return $this->error;
    }

}
