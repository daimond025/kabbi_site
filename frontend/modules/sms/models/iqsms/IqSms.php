<?php

namespace frontend\modules\sms\models\iqsms;

use yii\base\Exception;
use yii\base\Object;

class IqSms extends Object
{
    const ERROR_EMPTY_RESPONSE = 'errorEmptyResponse';

    protected $apiLogin = null;
    protected $apiPassword = null;
    protected $packetSize = 200;
    protected $results = [];

    public $host;
    public $host_balance;
    public $connectionTimeout;
    public $timeout;

    public function setApiLogin($apiLogin)
    {
        $this->apiLogin = $apiLogin;
    }

    public function setApiPassword($apiPassword)
    {
        $this->apiPassword = $apiPassword;
    }

    public function setHost($host)
    {
        $this->host = $host;
    }

    public function getHost($uri = '')
    {
        if ($uri == 'messages/v2/balance.json') {
            return $this->host_balance;
        }

        return $this->host;
    }

    private function sendRequest($uri, $params = null)
    {
        $url  = $this->getUrl($uri);
        $data = $this->formPacket($params);

        $client = curl_init($url);
        curl_setopt_array($client, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST           => true,
            CURLOPT_HEADER         => false,
            CURLOPT_HTTPHEADER     => ['Host: ' . $this->getHost()],
            CURLOPT_POSTFIELDS     => $data,
            CURLOPT_CONNECTTIMEOUT => $this->connectionTimeout,
            CURLOPT_TIMEOUT        => $this->timeout,
        ]);

        $body = curl_exec($client);
        curl_close($client);
        if (empty($body)) {
            throw new Exception(self::ERROR_EMPTY_RESPONSE);
        }

        $decodedBody = json_decode($body, true);

        if (is_null($decodedBody)) {
            throw new Exception($body);
        }

        return $decodedBody;
    }

    private function getUrl($uri)
    {
        if ($uri == 'messages/v2/balance.json') {
            return 'http://' . $this->getHost($uri) . '/' . $uri . '/';
        }

        return 'http://' . $this->getHost($uri) . '/' . $uri . '/';
    }

    private function formPacket($params = null)
    {
        $params['login']    = $this->apiLogin;
        $params['password'] = $this->apiPassword;
        foreach ($params as $key => $value) {
            if (empty($value)) {
                unset($params[$key]);
            }
        }
        $packet = json_encode($params);

        return $packet;
    }

    public function credits()
    {
        return $this->sendRequest('messages/v2/balance.json');
    }
}
