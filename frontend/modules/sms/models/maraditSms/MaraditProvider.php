<?php

namespace frontend\modules\sms\models\maraditSms;

use frontend\modules\sms\models\BaseSmsProvider;

/**
 * Class MaraditProvider
 * @package console\modules\sms\models\maraditSms
 * @property Maradit $gate
 */
class MaraditProvider extends BaseSmsProvider
{
    public $gate;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = \Yii::createObject(Maradit::class);
    }

    public function requestBalance($login, $password)
    {
        $response = $this->gate->requestBalance($login, $password);

        if ($response['status'] === Maradit::STATUS_ERROR) {
            return '';
        }

        if (!empty($response['message']['errorMessage'])) {
            $this->error = $response['message']['errorMessage'];
            return '';
        }

        return (string)$response['message']['obj'];
    }
}
