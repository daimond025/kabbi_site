<?php

namespace frontend\modules\sms\models\msmsms;

use common\helpers\ArrayHelper;
use frontend\modules\sms\models\BaseSmsProvider;

/**
 * Class MsmSmsProvider
 * @package frontend\modules\sms\models\msmsms
 *
 * @property MsmSms $gate
 */
class MsmSmsProvider extends BaseSmsProvider
{
    public $gate;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = \Yii::createObject(MsmSms::class);
    }

    public function requestBalance($login, $password)
    {
        $data = $this->gate->requestBalance($login, $password);
        $data = explode('&', $data);

        if ($data[0][0] == 'b') {
            return str_replace('balance=', '', $data[0]);
        }
        $this->error = ArrayHelper::getValue($this->gate->errors, str_replace('errno=', '', $data[0]));

        return '';
    }
}
