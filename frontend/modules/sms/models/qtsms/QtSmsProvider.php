<?php

namespace frontend\modules\sms\models\qtsms;

use frontend\modules\sms\models\BaseSmsProvider;

/**
 * Class QtSmsProvider
 * @package frontend\modules\sms\models\qtsms
 *
 * @property QtSms $gate
 */
class QtSmsProvider extends BaseSmsProvider
{
    public $gate;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = \Yii::createObject(QtSms::class);
    }

    public function requestBalance($login, $password)
    {
        $this->gate->user = $login;
        $this->gate->pass = $password;

        $xmlObj = simplexml_load_string($this->gate->getBalance());
        $xml    = (array)$xmlObj;
        if (empty($xml['errors'])) {
            $balance = $xml['balance']->OVERDRAFT + $xml['balance']->AGT_BALANCE;

            return $balance;
        }
        $this->error = (string)$xml['errors']->error[0];

        return '';
    }
}
