<?php

namespace frontend\modules\sms\models\qtsms;

use yii\base\Object;

class QtSms extends Object
{

    public $user = '';
    public $pass = '';
    public $hostname;
    public $path = '/public/http/';
    public $on_ssl = 0;
    public $post_data = [];
    public $timeout;

    public function getBalance()
    {
        $in = ['action' => 'balance'];

        return $this->getPostRequest($in);
    }

    public function getPostRequest($invars)
    {
        $invars['user']                 = ($this->user);
        $invars['pass']                 = ($this->pass);
        $invars['CLIENTADR']            = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : false;
        $invars['HTTP_ACCEPT_LANGUAGE'] = isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? $_SERVER['HTTP_ACCEPT_LANGUAGE'] : false;
        $PostData                       = http_build_query($invars, '', '&');
        $len                            = strlen($PostData);
        $nn                             = "\r\n";
        $send                           = "POST " . $this->path . " HTTP/1.0" . $nn . "Host: " . $this->hostname . "" . $nn . "Content-Type: application/x-www-form-urlencoded" . $nn . "Content-Length: $len" . $nn . "User-Agent: AISMS PHP class" . $nn . $nn . $PostData;
        flush();
        if (($fp = @fsockopen(($this->on_ssl ? 'ssl://' : '') . $this->hostname, ($this->on_ssl ? '443' : '80'), $errno,
                $errstr, 30)) !== false) {
            fputs($fp, $send);
            $header = '';
            do {
                $header .= fgets($fp, 4096);
            } while (strpos($header, "\r\n\r\n") === false);
            if (get_magic_quotes_runtime()) {
                $header = $this->decodeHeader(stripslashes($header));
            } else {
                $header = $this->decodeHeader($header);
            }

            $body = '';
            while (!feof($fp)) {
                $body .= fread($fp, 8192);
            }
            if (get_magic_quotes_runtime()) {
                $body = $this->decodeBody($header, stripslashes($body));
            } else {
                $body = $this->decodeBody($header, $body);
            }

            fclose($fp);

            return $body;
        } else {
            return 'Невозможно соединиться с сервером.';
        }
    }

    public function decodeHeader($str)
    {
        $part = preg_split("/\r?\n/", $str, -1, PREG_SPLIT_NO_EMPTY);
        $out  = [];
        for ($h = 0; $h < sizeof($part); $h++) {
            if ($h != 0) {
                $pos = strpos($part[$h], ':');
                $k   = strtolower(str_replace(' ', '', substr($part[$h], 0, $pos)));
                $v   = trim(substr($part[$h], ($pos + 1)));
            } else {
                $k = 'status';
                $v = explode(' ', $part[$h]);
                $v = $v[1];
            }
            if ($k == 'set-cookie') {
                $out['cookies'][] = $v;
            } else {
                if ($k == 'content-type') {
                    if (($cs = strpos($v, ';')) !== false) {
                        $out[$k] = substr($v, 0, $cs);
                    } else {
                        $out[$k] = $v;
                    }
                } else {
                    $out[$k] = $v;
                }
            }
        }

        return $out;
    }

    public function decodeBody($info, $str, $eol = "\r\n")
    {
        $tmp = $str;
        $add = strlen($eol);
        if (isset($info['transfer-encoding']) && $info['transfer-encoding'] == 'chunked') {
            $str = '';
            do {
                $tmp = ltrim($tmp);
                $pos = strpos($tmp, $eol);
                $len = hexdec(substr($tmp, 0, $pos));
                if (isset($info['content-encoding'])) {
                    $str .= gzinflate(substr($tmp, ($pos + $add + 10), $len));
                } else {
                    $str .= substr($tmp, ($pos + $add), $len);
                }
                $tmp   = substr($tmp, ($len + $pos + $add));
                $check = trim($tmp);
            } while (!empty($check));
        } elseif (isset($info['content-encoding'])) {
            $str = gzinflate(substr($tmp, 10));
        }

        return $str;
    }
}

