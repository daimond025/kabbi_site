<?php

namespace frontend\modules\sms\models\smsbroker;

use frontend\modules\sms\models\BaseSmsProvider;

/**
 * Class SmsBrokerProvider
 * @package console\modules\sms\models\smsbroker
 */
class SmsBrokerProvider extends BaseSmsProvider
{
}
