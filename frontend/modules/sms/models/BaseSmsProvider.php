<?php

namespace frontend\modules\sms\models;

use yii\base\Component;

abstract class BaseSmsProvider extends Component
{
    protected $error;

    public function requestBalance($login, $password)
    {
        return 'Method does not work';
    }

    public function getError()
    {
        return $this->error;
    }
}
