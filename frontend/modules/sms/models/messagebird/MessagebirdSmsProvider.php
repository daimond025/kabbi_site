<?php

namespace frontend\modules\sms\models\messagebird;


use frontend\modules\sms\models\BaseSmsProvider;


/**
 * Class MessagebirdSmsProvider
 * @package \frontend\modules\sms\models\messagebird\MessagebirdSmsProvider
 *
 */
class MessagebirdSmsProvider extends BaseSmsProvider
{


    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
    }

    public function requestBalance($login, $password)
    {

        // баланс
        $MessageBird = new \MessageBird\Client($password);
        try {
            $Balance = $MessageBird->balance->read();
            return $Balance->amount;
        } catch (\MessageBird\Exceptions\AuthenticateException $e) {
            // That means that your accessKey is unknown
            echo 'wrong login';
            return '';
        } catch (\Exception $e) {
            echo($e->getMessage());
            return '';
        }
    }
}
