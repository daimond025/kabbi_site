<?php

namespace frontend\modules\sms\models\qtsmsKuban;

use frontend\modules\sms\models\qtsms\QtSmsProvider;

class QtSmsKubanProvider extends QtSmsProvider
{
    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();

        $this->gate = \Yii::createObject(QtSmsKuban::class);
    }
}
