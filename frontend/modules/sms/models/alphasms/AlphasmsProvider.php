<?php

namespace frontend\modules\sms\models\alphasms;

use frontend\modules\sms\models\BaseSmsProvider;

/**
 * Class AlphasmsProvider
 * @package frontend\modules\sms\models\alphasms
 *
 * @property Alphasms $gate
 */
class AlphasmsProvider extends BaseSmsProvider
{
    public $gate;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = \Yii::createObject(Alphasms::class);
    }

    public function requestBalance($login, $password)
    {
        $response = $this->gate->getBalance($login, $password);

        if (!$response) {
            $this->error = 'No connect';

            return '';
        }

        $response = explode(':', $response, 2);

        if (count($response) != 2) {
            $this->error = 'Unknown error';

            return '';
        }

        if (in_array($response[0], ['error', 'errors'])) {
            $this->error = $response[1];

            return '';
        }

        if ($response[0] == 'balance') {
            return $response[1];
        }

        $this->error = 'Unknown error';

        return '';
    }
}
