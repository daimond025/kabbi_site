<?php

namespace frontend\modules\sms\models\alphasms;

use yii\base\Object;

class Alphasms extends Object
{
    public $baseUrl;
    public $version;

    private function sendResponse($params)
    {
        $curl = app()->curl;

        return $curl->get($this->baseUrl, $params);
    }

    public function getBalance($login, $password)
    {
        $params = [
            'command'  => 'balance',
            'version'  => $this->version,
            'login'    => $login,
            'password' => $password,
        ];

        return $this->sendResponse($params);
    }
}
