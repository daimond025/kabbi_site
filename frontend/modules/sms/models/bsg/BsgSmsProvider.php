<?php

namespace frontend\modules\sms\models\bsg;


use frontend\modules\sms\models\BaseSmsProvider;
use frontend\modules\sms\models\bsg\BsgSms;


/**
 * Class BsgSmsProvider
 * @package console\modules\sms\models\bsg
 *
 * @property BsgSms $gate
 */
class BsgSmsProvider extends BaseSmsProvider
{
    public $gate;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = \Yii::createObject(BsgSms::class);
    }

    public function requestBalance($login, $password)
    {
        try {

            $response = $this->gate->getBalance($login, $password);


            $xmlObj   = simplexml_load_string($response);
            $xml      = (array)$xmlObj;
            if (!empty($xml['error'])) {
                $this->error = $xml['error'];

                return '';
            }

            return $xml['money'];
        } catch (\Exception $ex) {
            return '';
        }
    }
}
