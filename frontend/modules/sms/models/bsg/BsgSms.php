<?php

namespace frontend\modules\sms\models\bsg;

use GuzzleHttp\Client;
use yii\base\Object;

class BsgSms extends Object
{
    public $baseUrl;
    public $connectionTimeout;
    public $timeout;

    public function getBalance($login, $password)
    {
        $body = '<?xml version="1.0" encoding="utf-8"?>
            <request>
            <security>
            <login value="' . $login . '"/>
            <password value="' . $password . '"/>
            </security>
            </request>';

        return $this->send('balance', $body);
    }

    private function send($method, $body)
    {
        $url     = $this->baseUrl . '/' . $method;
        $headers = [
            'content-type' => 'text/xml; charset=utf-8',
        ];

        $client   = new Client([
            'connectionTimeout' => $this->connectionTimeout,
            'timeout'           => $this->timeout,
        ]);
        $response = $client->request('POST', $url, [
            'headers' => $headers,
            'body'    => $body,
        ]);

        return $response->getBody();
    }

}
