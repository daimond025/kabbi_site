<?php

namespace frontend\modules\sms\models\ibateleSmpp;

use frontend\modules\sms\models\BaseSmsProvider;

/**
 * Class IbateleSmppProvider
 * @package frontend\modules\sms\models\ibateleSmpp
 * @property IbateleSmppSms $gate
 */
class IbateleSmppProvider extends BaseSmsProvider
{
    public $gate;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = \Yii::createObject(IbateleSmppSms::class);
    }

    public function requestBalance($login, $password)
    {
        $request = $this->gate->requestBalance($login, $password);

        if ($request == IbateleSmppSms::EMPTY_BALANCE_RESPONSE) {
            $this->error = 'Invalid login/password';

            return '';
        }
        $balance = trim(explode(':', $request)[2], ' ');
        $balance = trim($balance, '"}');

        return (integer)$balance;
    }
}
