<?php

namespace frontend\modules\sms\models\ibatele;

use yii\base\Object;

class IbateleSms extends Object
{
    //Тексты сообщений об ошибках
    const EMPTY_BALANCE_RESPONSE = '{"money":{"@attributes":{"currency":""}}}';

    //Адреса отправки запросов
    public $requestBalanceServerAddress;
    public $connectionTimeout;
    public $timeout;

    public function requestBalance($login, $password)
    {
        $xml = '<?xml  version="1.0" encoding="utf-8" ?>
        <request>
        <security>
        <login value="' . $login . '" />
        <password value="' . $password . '" />
        </security>
        </request>';
        $result = $this->sendRequest($xml, $this->requestBalanceServerAddress);

        $xmlObj = simplexml_load_string($result);
        $xml = (array) $xmlObj;

        return json_encode($xml);
    }

    /**
     *
     * @param string $xml
     * @param string $server_address remote IP address of server
     * depends on what action you gonna do
     * @return mixed
     * state True on success
     * state False on failure
     * or state CURLOPT_RETURNTRANSFER if incoming data available
     */
    private function sendRequest($xml, $server_address, $balance = false)
    {
        $res = '';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: text/xml; charset=utf-8'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CRLF, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->connectionTimeout);
        curl_setopt($ch, CURLOPT_TIMEOUT,  $this->timeout);
        if ($balance = true) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        }
        curl_setopt($ch, CURLOPT_URL, $server_address);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}
