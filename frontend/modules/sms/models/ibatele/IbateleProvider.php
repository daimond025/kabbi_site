<?php

namespace frontend\modules\sms\models\ibatele;

use frontend\modules\sms\models\BaseSmsProvider;

/**
 * Class IbateleProvider
 * @package frontend\modules\sms\models\ibatele
 *
 * @property IbateleSms $gate
 */
class IbateleProvider extends BaseSmsProvider
{
    public $gate;

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->gate = \Yii::createObject(IbateleSms::class);
    }

    public function requestBalance($login, $password)
    {
        $request = $this->gate->requestBalance($login, $password);
        if ($request == IbateleSms::EMPTY_BALANCE_RESPONSE) {
            $this->error = 'Invalid login/password';

            return '';
        }
        $balance = trim(explode(':', $request)[2], ' ');
        $balance = trim($balance, '"}');

        return (integer)$balance;
    }
}
