<?php

namespace frontend\modules\sms\models\megafonTjSmpp;

use console\modules\sms\models\SmsProviderInterface;
use frontend\modules\sms\models\BaseSmsProvider;
use yii\base\Component;

/**
 * Class MegafonTjSmppProvider
 * @package console\modules\sms\megafonTjSmpp
 *
 * @property MegafonTjSmpp $gate
 */
class MegafonTjSmppProvider extends BaseSmsProvider
{
}
