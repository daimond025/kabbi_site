<?php

namespace frontend\modules\sms\models\semysms;

use console\modules\sms\models\SmsProviderInterface;
use frontend\modules\sms\models\BaseSmsProvider;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Class SemysmsProvider
 * @package console\modules\sms\models\semysms
 */
class SemysmsProvider extends BaseSmsProvider
{
}
