<?php

namespace app\modules\order\services;

use yii\base\InvalidParamException;
use \common\modules\tenant\models\TenantSetting;

class CallsignService
{
    const CALLSIGN_TYPE_IS_CAR = 'car';
    const CALLSIGN_TYPE_IS_WORKER = 'worker';
    const SETTING_WHOSE_CALLSIGN_ON_MAP = 'WHOSE_CALLSIGN_ON_MAP';

    /**
     * Get callsign value for map
     *
     * @param array $worker - worker data from redis
     *
     * @return string
     * @throws InvalidParamException
     */

    public static function getCallsign($worker)
    {
        $tenantId   = $worker['worker']['tenant_id'];
        $cityId     = $worker['worker']['city_id'];
        $positionId = $worker['position']['position_id'];

        if (!isset($tenantId, $cityId, $positionId)) {
            throw new InvalidParamException;
        }

        $whoseCallsignOnMap = TenantSetting::getSettingValue($tenantId, self::SETTING_WHOSE_CALLSIGN_ON_MAP, $cityId,
            $positionId);

        if ($whoseCallsignOnMap) {
            return $whoseCallsignOnMap == self::CALLSIGN_TYPE_IS_CAR ? (string)$worker['car']['callsign'] : (string)$worker['worker']['callsign'];
        }

        return '';

    }

    /**
     * @return array
     */
    public static function getCallsignTypes()
    {
        return ['worker' => 'Worker', 'car' => 'Car'];
    }
}

