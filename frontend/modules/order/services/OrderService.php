<?php

namespace app\modules\order\services;

use app\modules\order\models\Order;
use app\modules\order\models\OrderChangeData;
use app\modules\order\models\OrderStatus;
use yii\data\ActiveDataProvider;

class OrderService
{

    const COMPLETED_PAID = 37;
    const COMPLETED_NOT_PAID = 38;

    public static function getOrderDepartureTime(Order $order)
    {
        $changeTime = OrderChangeData::find()
            ->select('change_time')
            ->where(['order_id' => $order->order_id, 'change_field' => 'status_id', 'change_subject' => 'worker'])
            ->andWhere(['in', 'change_val', [OrderStatus::STATUS_EXECUTING, OrderStatus::STATUS_EXECUTION_PRE]])
            ->orderBy('change_id desc')
            ->all();

        if ($changeTime) {
            return end($changeTime)->change_time + $order->getOrderOffset();
        }

        return false;
    }

    /**
     * Method returns order arrive time with city offset
     *
     * @param Order $order
     *
     * @return false|null|string
     */
    public static function getOrderArriveTime(Order $order)
    {
        if ($order->device === Order::DEVICE_WORKER) {
            return self::getOrderDepartureTime($order);
        }

        $changeTime = OrderChangeData::find()
            ->select('change_time')
            ->where(['order_id' => $order->order_id, 'change_field' => 'status_id', 'change_subject' => 'worker'])
            ->andWhere(['in', 'change_val', [OrderStatus::STATUS_WORKER_WAITING]])
            ->orderBy('change_id desc')
            ->scalar();

        if ($changeTime) {
            return $changeTime + $order->getOrderOffset();
        }

        return false;
    }

    public static function getModels(ActiveDataProvider $dataProvider, $params = null)
    {
        return $dataProvider->getModels();
    }

    public static function isOrderCompleted($order_id)
    {
        return Order::find()->where(['order_id' => $order_id])->andWhere([
            'IN',
            'status_id',
            [self::COMPLETED_PAID, self::COMPLETED_NOT_PAID],
        ])->exists();
    }
}
