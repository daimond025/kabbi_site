<?php

namespace frontend\modules\order\helpers;

use app\modules\tariff\models\TaxiTariff;
use yii\helpers\ArrayHelper;

class TariffHelper
{
    public static function getTariffMap(array $tariffIds)
    {
        $tariffIds = \array_unique($tariffIds);
        $tarrifs = TaxiTariff::find()
            ->select(['tariff_id', 'name'])
            ->where(['tariff_id' => $tariffIds])
            ->asArray()
            ->all();

        return ArrayHelper::map($tarrifs, 'tariff_id', 'name');
    }
}
