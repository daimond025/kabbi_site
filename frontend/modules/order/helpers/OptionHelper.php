<?php

namespace frontend\modules\order\helpers;

use frontend\modules\car\models\CarOption;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

class OptionHelper
{
    public static function getOptionMap(array $orderIds)
    {
        $options = CarOption::find()
            ->alias('o')
            ->select(['o.option_id', 'name', 'order_id'])
            ->joinWith([
                'orderOptions' => function (ActiveQuery $query) use ($orderIds) {
                    $query->where(['order_id' => $orderIds]);
                },
            ], true, 'INNER JOIN')
            ->createCommand()
            ->queryAll();

        return ArrayHelper::map($options, 'option_id', 'name', 'order_id');
    }
}
