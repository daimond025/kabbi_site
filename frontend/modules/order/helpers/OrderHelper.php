<?php

namespace frontend\modules\order\helpers;

use app\modules\order\models\Order;
use yii\helpers\ArrayHelper;

class OrderHelper
{
    /**
     * @param $orders Order[]
     * @return array
     */
    public static function getOrderNumbers($orders)
    {
        $orders = ArrayHelper::getColumn($orders, 'order_number');
        $orders = array_map(function ($value) {
            return (int)$value;
        }, $orders);

        return $orders;
    }
}
