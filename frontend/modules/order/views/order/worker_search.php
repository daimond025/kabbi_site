<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $worker \frontend\modules\employee\models\worker\Worker */

if (!empty($workers)):
    ?>
    <?foreach($workers as $worker) :?>
        <li>
            <a data-worker_id="<?= $worker['worker_id'] ?>">
                <?= Html::encode($worker['name'] . ', ' .
                            $worker['callsign'] . ', ' .
                            $worker['car']['name'] . ', ' .
                            $worker['car']['color'] . ', ' .
                            $worker['car']['gos_number']) ?>
            </a>
        </li>
    <?endforeach;?>
<?else:?>
    <li class="no_select"><a><?= t ('app', 'No result') ?></a></li>
<? endif;?>
