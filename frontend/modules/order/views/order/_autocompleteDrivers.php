<?php

/* @var $value string */
/* @var $class string */
/* @var $options */
/* @var $isClient boolean */

use yii\web\JsExpression;
use yii\jui\AutoComplete;

$bundle_order = \app\modules\order\assets\OrderAsset::register($this);
//$this->registerJsFile($bundle_order->baseUrl . '/autocomplete.js');

echo AutoComplete::widget([
    'id'            => rand(1000, 10000000),
    'name'          => 'autocomplete-drivers',
    'value'         => $value,
    'clientOptions' => [
        'source'    => new JsExpression('autocompleteDrivers.source()'),
        'minLength' => 1,
        'appendTo'  => '#autocomplete-drivers-content',
    ],
    'clientEvents'  => [
        'select' => 'autocompleteDrivers.select()',
        'search' => 'autocompleteDrivers.search()',
        'open'   => 'autocompleteDrivers.open()',
        'change' => 'autocompleteDrivers.change()',
        'create' => 'autocompleteDrivers.create()',
        'focus'  => 'autocompleteDrivers.focus()'
    ],
    'options'       => [
        'class' => $class,
        'disabled' => !empty($options['disabled']),
        'placeholder' => $placeholder,
        'data-km'     => t('app', 'km'),
        'data-m'      => t('app', 'm')
    ],
]);