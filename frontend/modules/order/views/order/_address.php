<?php

use yii\helpers\Html;
use common\helpers\ArrayHelper;

/* @var $this \yii\web\View */
/* @var $address array */

?>
<? if(is_array($address)): ?>
    <? foreach ($address as $key => $value): ?>
        <div class="order_direction">
            <span class="od_d"><?= Html::encode($key); ?></span>
            <div>
            <span>
                <?
                $direction   = [];
                if(isset($value['code_n']) && !empty($value['code_n']) ){
                    $direction[] = ArrayHelper::getValue($value, 'street_n', '') . ' ' .  ArrayHelper::getValue($value, 'house_n', '')  ;
                    $direction[] = ArrayHelper::getValue($value, 'city_n', '') . ' ' .  ArrayHelper::getValue($value, 'code_n', '')  ;
                    $direction[] = '(' .  ArrayHelper::getValue($value, 'name_n', '') . ')' ;
                }else{
                    $direction[] = $value['city'];
                    $direction[] = empty($value['house'])
                        ? $value['street'] : $value['street'] . ' ' . $value['house'];
                    $direction[] = empty($value['housing'])
                        ? null : t('order', 'b. ') . $value['housing'];
                }
                echo Html::encode(implode(', ', array_filter($direction, function ($item) {
                    return !empty(trim($item));
                })));
                ?>
            </span>
                <i><?= Html::encode($value['parking']); ?></i>
            </div>
        </div>
    <? endforeach; ?>
<? endif; ?>
