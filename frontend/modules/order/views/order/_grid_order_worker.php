<?php

/* @var $order app\modules\order\models\Order */

use frontend\modules\car\models\CarColor;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $isClient bool Если авторизован как клиент */
/* @var $show_phone  */

$worker = $order->worker;
$car    = $order->car;
$car_color = '';

if($car){
    if(method_exists($car, 'getColorText' )){
        $car_color = $car->getColorText();
    }
}

$car_color = !empty($car_color) ? $car_color : '';


?>
<? if ($isClient) : ?>
    <? if (!empty($worker['worker_id'])): ?>
        <div class="order_table_client_block">
            <div class="order_table_client_block_big">
                <?= Html::encode(getShortName($worker['last_name'], $worker['name'])); ?>
            </div>
            <div class="order_table_client_block_small">
                <?= Html::encode($worker['callsign']); ?>
            </div>
        </div>
        <? if (!empty($car) && !(empty($car['name']) && empty($car['gos_number']))): ?>
            <div class="order_table_client_block">
                <div class="order_table_client_block_big">
                    <?= Html::encode($car['name'] . ', ' . CarColor::getColorText($car['color'])); ?>
                </div>
                <div class="order_table_client_block_small">
                    <?= Html::tag('span', Html::encode($car['gos_number']), ['dir' => 'auto']); ?>
                </div>
            </div>
        <? endif ?>
    <? endif ?>
<? else : ?>
    <? if (!empty($worker['worker_id'])): ?>
        <div class="order_table_client_block">
            <div class="order_table_client_block_big">
                <a href="<?= Url::to([
                    '/employee/worker/update/',
                    'id' => $worker['worker_id'],
                ]) ?>"><?= Html::encode(getShortName($worker['last_name'],
                        $worker['name'])); ?></a>
                <?php if ($worker['phone'] !== '') { ?>
                    <a href="tel:+<?= $worker['phone'] ?>" class="order_table_call"></a>
                <?php } ?>
            </div>
            <div class="order_table_client_block_small">
                <? if ($show_phone): ?>
                    <a href="<?= Url::to([
                        '/employee/worker/update/',
                        'id' => $worker['worker_id'],
                    ]) ?>">
                        <?= Html::encode($worker['phone']); ?>
                    </a><br>
                <? endif; ?>
                <?= Html::encode($worker['callsign']); ?>
                <a class="pt_message" data-receivertype="worker"
                   data-receiverid="<?= $order->worker->callsign ?>"
                   data-cityid="<?= $order->city_id ?>">
                </a>
            </div>
        </div>
        <? if (!empty($car) && !(empty($car['name']) && empty($car['gos_number']))): ?>
            <div class="order_table_client_block">
                <div class="order_table_client_block_big">
                    <a href="<?= Url::to(['/car/car/update/', 'id' => $car['car_id']]) ?>">
                        <?= Html::encode($car['name'] . ', ' . $car_color); ?>
                    </a>
                </div>
                <div class="order_table_client_block_small">
                    <a href="<?= Url::to(['/car/car/update/', 'id' => $car['car_id']]) ?>">
                        <?= Html::tag('span', Html::encode($car['gos_number']), ['dir' => 'auto']); ?>
                    </a>
                </div>
            </div>
        <? endif ?>
    <? endif ?>
<? endif; ?>
