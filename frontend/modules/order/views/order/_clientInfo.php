<?php

use yii\helpers\Html;

?>
<? if (!empty($clientInfo)): ?>
    <div id="client">
        <div class="line">
            <div class="photo">
                <i style="background-image: url(<?= empty($clientInfo['photo']) ? '/images/no-photo.png' : $clientInfo['photo'] ?>);"></i>
            </div>
            <div class="client-info-content" <? if($hide_client) echo 'style="visibility: hidden"'?> >

            <?php  if((Yii::$app->controller->module->id !== 'cabinet') && !($hide_client) && !isset($edit_user)) : ?>

                <a href="<?= $clientInfo['link'] ?>" class="name" target="_blank">
                    <?= $clientInfo['shortName'] ?></a>

            <?php elseif( isset($edit_user) && ($edit_user)) :?>
                <?= $clientInfo['shortName'] ?>
            <?php else: ?>
            <?= $clientInfo['shortName'] ?>
            <?php endif; ?>

                <a <? if (!$canUpdate || empty($clientInfo)): ?> style="display: none;" <? endif; ?> class="js-change-client change-client"><i></i></a>

                <?php if($hide_client): ?>
                    <div class="phone"><?= substr_replace($clientInfo['phone'],'*****',-5); ?></div>
                    <div class="js-clientId" style="display:none"><?= Html::encode($clientInfo['clientId']) ?></div>
                <? elseif(isset($edit_user) &&  (!$edit_user)):?>
                    <div class="phone">+<?= $clientInfo['phone'] ?></div>
                    <div class="js-clientId" style="display:none"><?= Html::encode($clientInfo['clientId']) ?></div>
                <?php else: ?>
                    <div class="phone">+<?= $clientInfo['phone'] ?></div>
                    <?= $clientInfo['banned']
                    ? '<i class="banned-user" title="' . t('client', 'Black list') . '"></i>' : '' ?>
                    <div class="success-orders"><?= Html::encode($clientInfo['successOrders']); ?></div>
                    <div class="failed-orders"><?= Html::encode($clientInfo['failedOrders']); ?></div>
                    <div class="balance"><?= Html::encode($clientInfo['balance']); ?></div>
                    <div class="balance"><?= Html::encode($clientInfo['bonus']); ?></div>
                    <div class="js-clientId" style="display:none"><?= Html::encode($clientInfo['clientId']) ?></div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<? else: ?>
    <div id="client"></div>
<? endif ?>