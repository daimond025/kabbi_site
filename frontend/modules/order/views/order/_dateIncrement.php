<?php

use yii\helpers\Html;

foreach ([15, 30, 45, 60] as $value) {
    echo Html::tag('a', '+' . $value . ' ' . t('order', 'm.'), [
        'data-time' => $value,
        'class' => 'js-inc-date',
    ]);
}