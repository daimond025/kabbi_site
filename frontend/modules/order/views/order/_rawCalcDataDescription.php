<?php

?>

<a class="js-raw-calc-data-description-toggle gray_link">Показать расшифровку</a>
<table class="people_table js-raw-calc-data-description">
    <tr>
        <th colspan="2">Расшифровка</th>
    </tr>
    <tr>
        <td>timeWaitCityInOrder</td>
        <td>Время ожидания в городе во время поездки</td>
    </tr>
    <tr>
        <td>timeWaitTrackInOrder</td>
        <td>Время ожидания за городом во время поездки</td>
    </tr>
    <tr>
        <td>timeWaitCityClient</td>
        <td>Время ожидания клиента в городе до поездки</td>
    </tr>
    <tr>
        <td>timeWaitTrackClient</td>
        <td>Время ожидания клиента за городом до поездки</td>
    </tr>
    <tr>
        <td>timeCityInOrder</td>
        <td>Время поездки по городу</td>
    </tr>
    <tr>
        <td>timeTrackInOrder</td>
        <td>Время поездки по загороду</td>
    </tr>
    <tr>
        <td>timeCityOutOrder</td>
        <td>Время поездки до начала заказа по городу</td>
    </tr>
    <tr>
        <td>timeTrackOutOrder</td>
        <td>Время поездки до начала заказа по загороду</td>
    </tr>
    <tr>
        <td>timeFreeInOrder</td>
        <td>Бесплатное время во время поездки</td>
    </tr>
    <tr>
        <td>disCityInOrder</td>
        <td>Расстояние по городу</td>
    </tr>
    <tr>
        <td>disTrackInOrder</td>
        <td>Расстояние по загороду</td>
    </tr>
    <tr>
        <td>disCityOutOrder</td>
        <td>Расстояние по городу до начала заказа</td>
    </tr>
    <tr>
        <td>disTrackOutOrder</td>
        <td>Расстояние по загороду до начала заказа</td>
    </tr>
    <tr>
        <td>priceCityInOrder</td>
        <td>Цена поездки по городу</td>
    </tr>
    <tr>
        <td>priceCityOutOrder</td>
        <td>Цена поездки по городу до посадки клиента</td>
    </tr>
    <tr>
        <td>priceTrackInOrder</td>
        <td>Цена поездки по загороду</td>
    </tr>
    <tr>
        <td>priceTrackOutOrder</td>
        <td>Цена поездки по загороду до посадки клиента</td>
    </tr>
    <tr>
        <td>priceCityWaitInOrder</td>
        <td>Цена за ожидание во время поездки по городу</td>
    </tr>
    <tr>
        <td>priceTrackWaitInOrder</td>
        <td>Цена за ожидание во время поездки по загороду</td>
    </tr>
    <tr>
        <td>priceClientWait</td>
        <td>Цена за ожидание клиента</td>
    </tr>
    <tr>
        <td>priceIntervalInOrder</td>
        <td>Цена за интервалы(город+загород)</td>
    </tr>
</table>