<?php

/* @var $order app\modules\order\models\Order */
/* @var $isClient bool Если авторизован как клиент */

use app\modules\order\models\Order;
use app\modules\tenant\models\User;
use yii\helpers\Html;
use yii\helpers\Url;


$client = $order->client;
$userCreated = $order->userCreated;


$hide = false;
$user_staff = false;
if ((app()->user->can(User::ROLE_STAFF) || app()->user->can(User::ROLE_STAFF_COMPANY))) {
    $user_staff = true;
}

// для партнеров - они не видят больничные поездки  - если закза принадлежит не им
if( $user_staff && ($order->device == Order::DEVICE_HOSPITAL) && ((int)$order->company_id != (int)user()->tenant_company_id) ){
    $hide = true;
}

$order_company = (isset($order->order_company) && ($order->order_company == true))
    ? $order->order_company : false;


?>

<? if ($hide): ?>
    <div class="order_table_client_block" style="visibility: hidden">
        <div class="order_table_client_block_big">
            <? if (!empty($client)): ?>
                <? $fullName = getShortName($client['name'], $client['second_name']); ?>
                <?= Html::encode($fullName); ?>
            <? else: ?>
                <?= t('order', 'Client') ?>
            <? endif ?>
        </div>
        <div class="order_table_client_block_small">
            <?=  Html::encode(substr_replace($order->phone,'****',-4)); ?>
        </div>
    </div>
    <div class="order_table_client_block">
        <? if (!empty($order->user_create)): ?>
            <div class="order_table_client_block_big">
                <?= t('order', 'Dispatcher') ?>
            </div>
            <div class="order_table_client_block_small">
                <?= Html::encode(getShortName($userCreated['last_name'], $userCreated['name'])); ?>
            </div>
        <? else: ?>
            <div class="order_table_client_block_big">
                <?= Html::encode($order->getDeviceName()); ?>
            </div>
        <? endif ?>
    </div>
<? elseif ($order_company || $user_staff) : ?>
    <div class="order_table_client_block">
        <div class="order_table_client_block_big">
            <? if (!empty($client) || $order_company): ?>
                <? $fullName = getShortName($client['last_name'], $client['name'],
                    $client['second_name']); ?>
                <?= Html::encode($fullName); ?>
            <? else: ?>
                <?= t('order', 'Client') ?>
            <? endif ?>
        </div>
        <div class="order_table_client_block_small">
            <?= Html::encode($order->phone); ?>
            <? if($isClient):?>
                <a href="tel:+<?= $order->phone ?>" class="order_table_call"></a>
            <?endif;?>
        </div>
    </div>
    <div class="order_table_client_block">
        <? if (!empty($order->user_create)): ?>
            <div class="order_table_client_block_big">
                <?= t('order', 'Dispatcher') ?>
            </div>
            <div class="order_table_client_block_small">
                <?= Html::encode(getShortName($userCreated['last_name'], $userCreated['name'])); ?>
            </div>
        <? else: ?>
            <div class="order_table_client_block_big">
                <?= Html::encode($order->getDeviceName()); ?>
            </div>
        <? endif ?>
    </div>
<? else: ?>
    <div class="order_table_client_block">
        <div class="order_table_client_block_big">
            <? if (!empty($client)): ?>
                <? $fullName = getShortName($client['last_name'], $client['name'],
                    $client['second_name']); ?>
                <a href="<?= Url::to([
                    '/client/base/update/',
                    'id' => $order->client_id,
                ]) ?>"><?= Html::encode($fullName); ?></a>
            <? else: ?>
                <? $fullName = getShortName($client['last_name'], $client['name'],
                    $client['second_name']); ?>
            <? endif ?>
        </div>
        <div class="order_table_client_block_small">
            <a href="/client/base/update/<?= $order->client_id ?>"><?= Html::encode($order->phone); ?></a>
            <a href="tel:+<?= $order->phone ?>" class="order_table_call"></a>
        </div>
    </div>
    <div class="order_table_client_block">
        <? if (!empty($order->user_create)): ?>
            <div class="order_table_client_block_big">
                <?= t('order', 'Dispatcher') ?>
            </div>
            <div class="order_table_client_block_small">
                <a href="<?= Url::to([
                    '/tenant/user/update',
                    'id' => $order->user_create,
                ]) ?>"><?= Html::encode(getShortName($userCreated['last_name'],
                        $userCreated['name'])); ?></a>
            </div>
        <? else: ?>
            <div class="order_table_client_block_big">
                <?= Html::encode($order->getDeviceName()); ?>
            </div>
        <? endif ?>
    </div>
<? endif; ?>
