<?php

use yii\helpers\Url;
use app\modules\order\models\OrderStatus;

/* @var $this yii\web\View */
/* @var $order app\modules\order\models\Order */
/* @var $isClient bool Если авторизован как клиент */
/* @var $tariffMap array */
/* @var $optionMap array */
?>
<? if (!empty($orders)): ?>
    <? $date = get('date') ?>
    <table class="order_table <? if (!is_null($date) && $date != date('d.m.Y')): ?>sortable<? else: ?>redis_sort<? endif ?>">
        <tr>
            <th class="ot_01"><a data-column="0" data-name="order_id" href="<?= Url::to(['order/sort', 'type' => OrderStatus::STATUS_GROUP_6]) ?>">№ <span></span></a></th>
            <th class="ot_02"><a class="sort" data-column="1" data-name="order_time" href="<?= Url::to(['order/sort', 'type' => OrderStatus::STATUS_GROUP_6]) ?>"><?= t('order', 'Order on') ?> <span class="pt_up"></span></a></th>
            <th class="ot_03"> <?=t('order', 'From-To')?></th>
            <th class="ot_04"><?= t('order', 'Client') ?></th>
            <th class="ot_05"><?= t('order', 'Worker + car') ?></th>
            <th class="ot_06"><?= t('order', 'Tariff and additional options') ?></th>
            <th class="ot_07"></th>
        </tr>
        <?= $this->render('_pre_order', [
            'orders'     => $orders,
            'isClient'   => $isClient,
            'show_phone' => $show_phone,
            'tariffMap'  => $tariffMap,
            'optionMap'  => $optionMap,
        ]) ?>
    </table>
<? else: ?>
    <p><?= t('app', 'Empty') ?></p>
<? endif; ?>
