<?php

use common\services\OrderStatusService;
use frontend\modules\car\models\CarColor;
use yii\helpers\Url;
use yii\helpers\Html;
use app\modules\tenant\models\User;
use frontend\modules\exchange\services\ExchangeProgramService;

/* @var $this yii\web\View */
/* @var $order app\modules\order\models\Order */
/* @var $isClient bool Если авторизован как клиент */
/* @var $tariffMap array */
/* @var $optionMap array */

$controller = !empty($controller) ? $controller : $this->context->id;
?>
<? if (!empty($orders)): ?>
    <? foreach ($orders as $key => $order):$order->status_time = $order->getOrderTimestamp($order->status_time); ?>
        <tr id="completed_<?= $key ?>" class="status_<?= $order->status_id ?>">
            <td data-sort="<?= $order->order_number ?>" class="ot_01">
                <? if ($isClient || app()->user->can(User::ROLE_STAFF_COMPANY)) : ?>
                    <a href="<?= yii\helpers\Url::to(['order/view', 'order_number' => $order->order_number]) ?>"
                       class="ot_open">
                        <b><?= $order->order_number ?></b>
                    </a>
                <? else : ?>
                    <a href="<?= yii\helpers\Url::to(['order/update', 'order_number' => $order->order_number]) ?>"
                       class="ot_open">
                        <b><?= $order->order_number ?></b>
                    </a>
                <? endif; ?>
            </td>
            <td data-sort="<?= $order->status_time ?>" class="ot_02">
                <span class="ot_green"><b><?= Html::encode(OrderStatusService::translate($order->status_id,
                            $order->position_id)); ?></b></span><br/>
                <?= date('H:i', $order->status_time) ?>
            </td>
            <td class="ot_03">
                <?= $this->render('_address', ['address' => $order->address]) ?>
            </td>
            <td class="ot_04">
                <? if ($isClient) : ?>
                    <div class="order_table_client_block">
                        <div class="order_table_client_block_big">
                            <? if (!empty($client)): ?>
                                <?= Html::encode(getShortName($order->client->last_name, $order->client->name,
                                    $order->client->second_name)); ?>
                            <? else: ?>
                                <?= t('order', 'Client') ?>
                            <? endif ?>
                        </div>
                        <div class="order_table_client_block_small">
                            <?= Html::encode($order->phone); ?>
                            <a href="tel:+<?= $order->phone ?>" class="order_table_call"></a>
                        </div>
                    </div>
                    <div class="order_table_client_block">
                        <? if (!empty($order->user_create)): ?>
                            <div class="order_table_client_block_big">
                                <?= t('order', 'Dispatcher') ?>
                            </div>
                            <div class="order_table_client_block_small">
                                <?= Html::encode(getShortName($order->userCreated['last_name'],
                                    $order->userCreated['name'])); ?>
                            </div>
                        <? else: ?>
                            <div class="order_table_client_block_big">
                                <?= Html::encode($order->getDeviceName()); ?>
                            </div>
                        <? endif ?>
                    </div>
                <? else: ?>
                    <div class="order_table_client_block">
                        <div class="order_table_client_block_big">
                            <? if (!empty($order->client_id)): ?>
                                <a href="<?= Url::to([
                                    '/client/base/update/',
                                    'id' => $order->client_id,
                                ]) ?>"><?= Html::encode(getShortName($order->client->last_name, $order->client->name,
                                        $order->client->second_name)); ?></a>
                            <? else: ?>
                                <?= t('order', 'Client') ?>
                            <? endif ?>
                        </div>
                        <div class="order_table_client_block_small">
                            <a href="/client/base/update/<?= $order->client_id ?>"><?= Html::encode($order->phone); ?></a>
                            <a href="tel:+<?= $order->phone ?>" class="order_table_call"></a>
                        </div>
                    </div>
                    <div class="order_table_client_block">
                        <? if (!empty($order->user_create)): ?>
                            <div class="order_table_client_block_big">
                                <?= t('order', 'Dispatcher') ?>
                            </div>
                            <div class="order_table_client_block_small">
                                <a href="<?= Url::to([
                                    '/tenant/user/update',
                                    'id' => $order->user_create,
                                ]) ?>"><?= Html::encode(getShortName($order->userCreated['last_name'],
                                        $order->userCreated['name'])); ?></a>
                            </div>
                        <? else: ?>
                            <div class="order_table_client_block_big">
                                <?= Html::encode($order->getDeviceName()); ?>
                            </div>
                        <? endif ?>
                    </div>
                <? endif; ?>
            </td>
            <!---Инфа о тачке -->
            <td class="ot_05">
                <!---Клиентский кабинет -->
                <? if ($isClient) : ?>
                    <!---Заказ выполнен в бирже-->
                    <? if ($order->processed_exchange_program_id): ?>
                        <? if (!empty($order->exchangeWorker)): ?>
                            <div class="order_table_client_block">
                                <div class="order_table_client_block_big">
                                    <?= Html::encode($order->exchangeWorker->name); ?>
                                    <?php if ($order->exchangeWorker->phone !== '') { ?>
                                        <a href="<?= 'tel:+' . $order->exchangeWorker->phone ?>"
                                           class="order_table_call"></a>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="orer_table_client_block">
                                <div class="order_table_client_block_big">
                                    <?= Html::encode($order->exchangeWorker->car_description . ', ' . $order->exchangeWorker->color); ?>
                                    <?= Html::encode($order->exchangeWorker->gos_number) ?>
                                </div>
                            </div>
                        <? endif ?>
                        <!---Заказ выполнен в гутаксе-->
                    <? else: ?>
                        <? if (!empty($order->worker_id)): ?>
                            <div class="order_table_client_block">
                                <div class="order_table_client_block_big">
                                    <?= Html::encode(getShortName($order->worker->last_name, $order->worker->name)); ?>
                                </div>
                                <div class="order_table_client_block_small">
                                    <?= Html::encode($order->worker->callsign); ?>
                                </div>
                            </div>
                            <? if (!empty($order->car) && !(empty($order->car->name) && empty($order->car->gos_number))): ?>
                                <div class="order_table_client_block">
                                    <div class="order_table_client_block_big">
                                        <?= Html::encode($order->car->name . ', ' . $order->car->getColorText()); ?>
                                    </div>
                                    <div class="order_table_client_block_small">
                                        <?= Html::tag('span', Html::encode($order->car->gos_number), ['dir' => 'auto']); ?>
                                    </div>
                                </div>
                            <? endif ?>
                        <? endif ?>
                    <? endif ?>
                <? else : ?>
                    <!---Заказ выполнен в бирже-->
                    <? if ($order->processed_exchange_program_id): ?>
                        <? if (!empty($order->exchangeWorker)): ?>
                            <div class="order_table_client_block">
                                <div class="order_table_client_block_big">
                                    <?= Html::encode($order->exchangeWorker->name); ?>
                                    <?php if ($order->exchangeWorker->phone !== '') { ?>
                                        <a href="<?= 'tel:+' . $order->exchangeWorker->phone ?>"
                                           class="order_table_call"></a>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="orer_table_client_block">
                                <div class="order_table_client_block_big">
                                    <?= Html::encode($order->exchangeWorker->car_description . ', ' . $order->exchangeWorker->color); ?>
                                    <?= Html::encode($order->exchangeWorker->gos_number) ?>
                                </div>
                            </div>
                            <div class="orer_table_client_block">
                                <div class="order_table_client_block_big">
                                    <?= Html::encode(ExchangeProgramService::getExchangeProgramNameById($order->processed_exchange_program_id)) ?>
                                </div>
                            </div>
                        <? endif ?>
                        <!---Заказ выполнен в гутаксе-->
                    <? else: ?>
                        <? if (!empty($order->worker_id)): ?>
                            <div class="order_table_client_block">
                                <div class="order_table_client_block_big">
                                    <a href="<?= Url::to([
                                        '/employee/worker/update/',
                                        'id' => $order->worker_id,
                                    ]) ?>"><?= Html::encode(getShortName($order->worker->last_name,
                                            $order->worker->name)); ?></a>
                                    <?php if ($order->worker->phone !== '') { ?>
                                        <a href="<?= 'tel:+' . $order->worker->phone ?>" class="order_table_call"></a>
                                    <?php } ?>
                                </div>
                                <div class="order_table_client_block_small">
                                    <? if ($show_phone): ?>
                                        <a href="<?= Url::to([
                                            '/employee/worker/update/',
                                            'id' => $order->worker->worker_id,
                                        ]) ?>">
                                            <?= Html::encode($order->worker->phone); ?>
                                        </a><br>
                                    <? endif; ?>
                                    <?= Html::encode($order->worker->callsign); ?>
                                    <a class="pt_message" data-receivertype="worker"
                                       data-receiverid="<?= $order->worker->callsign ?>"
                                       data-cityid="<?= $order->city_id ?>">
                                    </a>
                                </div>
                            </div>
                            <? if (!empty($order->car) && !(empty($order->car->name) && empty($order->car->gos_number))): ?>
                                <div class="order_table_client_block">
                                    <div class="order_table_client_block_big">
                                        <a href="<?= Url::to(['/car/car/update/', 'id' => $order->car->car_id]) ?>">
                                            <?= Html::encode($order->car->name . ', ' . $order->car->getColorText()); ?>
                                            <?= Html::tag('span', Html::encode($order->car->gos_number),
                                                ['dir' => 'auto']); ?>
                                        </a>
                                    </div>
                                </div>
                            <? endif ?>
                        <? endif ?>
                    <? endif; ?>
                <? endif; ?>
            </td>
            <td class="ot_06">
                <div class="order_table_client_block_big">
                    <?= Html::encode($tariffMap[$order->tariff_id]); ?>
                </div>
                <div class="order_table_client_block_small">
                    <?php
                    $options = isset($optionMap[$order->order_id]) ? $optionMap[$order->order_id] : [];
                    foreach ($options as $option) : ?>
                        <?= Html::encode(t('car-options', $option)) . '<br>' ?>
                    <?php endforeach; ?>
                </div>
                <?= Html::encode($order->comment); ?>
            </td>
            <td class="ot_07">
                <? if (!$isClient) : ?>
                    <a class="ot_edit"
                       href="<?= Url::to(["/order/$controller/update", 'order_number' => $order->order_number]) ?>"></a>
                <? endif; ?>
            </td>
        </tr>
    <? endforeach; ?>
<? else: ?>
    <tr>
        <td colspan="5"><?= t('app', 'Empty') ?></td>
    </tr>
<? endif ?>