<?php

/* @var $order app\modules\order\models\Order */

use app\modules\order\models\OrderStatus;
use common\services\OrderStatusService;
use yii\helpers\Html;
$timeToOrder = $order->getTimerData($order->order_time);
$statusClass = in_array($order->status_id, OrderStatus::getWarningStatusId()) ? 'ot_red' : 'ot_green';
$timer_class = $timeToOrder['TYPE'] == 'down' ? 'ot_green' : 'ot_red';

?>

<span class="<?= $statusClass ?>">
    <b><?= Html::encode(OrderStatusService::translate($order->status_id,
            $order->position_id)); ?>
    </b>
</span>
<? if(in_array($order->status_id, OrderStatus::getCountdownSatusList(), false)): ?>
    <br>
    <span class="ot_time">
                        <?= date('d ', $order->order_time).' '.t('order',
                            date('F', $order->order_time)).' '.' '.t('support', 'at').' '.date('H:i',
                            $order->order_time) ?>
                    </span>
    <span class="<?= $timer_class ?> timer" data-type="<?= $timeToOrder['TYPE'] ?>">
            <br>
                        <b>
                            <span class="minutes"><?= $timeToOrder['MINUTES'] ?></span>
                            <span class="time_colon">:</span>
                            <span class="seconds"><?= $timeToOrder['SECONDS'] ?></span>
                        </b>
                    </span>
<? endif ?>
