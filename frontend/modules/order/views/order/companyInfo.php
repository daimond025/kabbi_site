<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

?>

<? if (!empty($companyInfo)): ?>

    <div id="company">
        <div class="line">
            <div style="margin-left: 0px;" class="client-info-content">
                <span style="max-width: 100% " class="name">
                    <?= implode(',  ',
                        array_filter([$companyInfo['name']], function ($value) {
                            return isset($value);
                        })) ?>
                </span>
                <a class="js-change-company change-client"><i></i></a>

                <div class="car"> <?= $companyInfo['userName'] ?></div>
            </div>
        </div>
    </div>
<? else: ?>
    <div id="company"></div>
<? endif ?>
