<?php

use common\services\OrderStatusService;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\tenant\models\Currency;
use app\modules\order\models\OrderStatus;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $order app\modules\order\models\Order */
/* @var $form yii\widgets\ActiveForm */
/* @var $dataForm array */
/* @var $isClient bool Если авторизован как клиент */
/* @var $orderActions array */
/* @var $langCode string */

$isHasSettingsCurrentOrderForCouriers = $order->isHasSettingsCurrentOrderForCouriers();
$settingCurrentOrder                  = $order->getSettingsCurrentOrder();

if ($this->context->id == 'operator') {
    $bundle_order = \app\modules\order\assets\OrderAsset::register($this);
    $this->registerJsFile($bundle_order->baseUrl . '/functions.js');
    $this->registerJsFile($bundle_order->baseUrl . '/main.js');
}

$controller = $this->context->id;
if ($this->context->id == 'operator') {
    echo '<div class="bread"><a href="' . Url::to('/order/operator/') . '">' . t('app', 'Orders') . '</a></div>';

    $bundle_order = \app\modules\order\assets\OrderAsset::register($this);
    $this->registerJsFile($bundle_order->baseUrl . '/functions.js');
    $this->registerJs('init_pac()');

}
if (!app()->request->isAjax) {
    $bundle_order = \app\modules\order\assets\OrderAsset::register($this);
}

$edit = app()->user->can('orders') ? true : false;
$hide_client = true;
if($edit ){
    $hide_client = false;
}
?>

<div id="add-order" style="text-align: left;"
     data-module="<?= $isClient ? '/client/cabinet/' : '/order/'; ?>" data-points="2">
<div class="non-editable-order-cart">
    <div style="text-align: left; padding: 0;">

        <?php $form = ActiveForm::begin([
            'id'                     => 'order_update',
            'enableClientValidation' => false,
            'enableClientScript'     => false,
            'action'                 => ['/order/update', 'order_number' => $order->order_number],
            'options'                => [
                'class' => 'order-form',
                'data'  => [
                    'max-number-of-address-points' =>
                        isset($dataForm['MAX_NUMBER_OF_ADDRESS_POINTS']) ? $dataForm['MAX_NUMBER_OF_ADDRESS_POINTS'] : 0,
                    'show-card-payment'            => !empty($order->cardPayment),
                    'is-client'                    => $isClient ? 1 : 0,
                ],
                'style' => 'display:none'
            ],
        ]);
        ?>
        <?= Html::activeHiddenInput($order, 'order_number') ?>

        <?php ActiveForm::end(); ?>

        <div class="tabs_links" style="padding: 10px 32px 0 32px;">
            <ul>
                <li class="active"><a class="b01"><?= t('order', 'Order') ?></a></li>
                <li><a data-href="<?= Url::to(['get-events', 'order_id' => $order->order_id, 'city_id' => $order->city_id]) ?>" class="b02"><?= t('order', 'Events') ?></a></li>
                <li><a data-href="<?= Url::to(["get-map", 'order_id' => $order->order_id, 'status_id' => $order->status_id]) ?>" class="b03"><?= t('order', 'Route on the map') ?></a></li>
                <!--li><a class="b04"><?= t('order', 'Exchange') ?></a></li-->
                <li><a data-href="<?= Url::to(['client-review', 'order_id' => $order->order_id]) ?>" class="b04"><?= t('order', 'Review') ?></a></li>
            </ul>
        </div>
        <div class="tabs_content">
            <div class="active" id="b01" style="padding-top: 0;">
                <div class="order-window view-order <?if ($this->context->id == 'operator') echo 'operator-order-window';?>">
                    <div class="order-window__header">
                        <div class="city-select" style="font-size: 24px; text-align: right; line-height: 24px; position: relative; font-weight: bold; vertical-align: top;">
                            <?= Html::encode($dataForm['CITY_LIST'][$order->city_id]); ?>
                        </div>
                        <div class="date_order_selector" style="margin-bottom: 8px;">
                            <h2 style="font-size: 24px; line-height: 24px; position: relative; top: 9px; vertical-align: top;">
                                <?= t('order', '#{orderNumber} on', [
                                    'orderNumber' => Html::encode($order->order_number),
                                ]) ?>
                                <?= app()->formatter->asDateTime($order->order_time, 'php:d F Y ') . t('order', 'y.') . date(', H:i', $order->order_time)?>
                            </h2>
                        </div>
                        <div class="current-status">
                            <b><?= Html::encode($order->getAttributeLabel('status_id')); ?></b>
                            <p><?= Html::encode(OrderStatusService::translate($order->status_id, $order->position_id)); ?></p>
                            <p>
                                <? if ($isClient
                                    && in_array($order->status_id, OrderStatus::getCanCancelOrderByCLient(), false)
                                ): ?>
                                    <?= Html::a(t('order', 'Cancel the order'), ['cancel-order'], [
                                        'data-order_id' => $order->order_id,
                                        'class'         => 'pt_del js-cancel-order',
                                    ]); ?>
                                <? endif; ?>
                            </p>
                        </div>
                    </div>
                    <div class="order-window__people">
                        <div class="order-window__client">
                            <div style="position: relative; z-index: 9998;">
                                <a class="title active" data-tab="normal"><?= t('order', 'Client') ?></a>
                                <?php if ($order->client_passenger_phone): ?>
                                    <a class="title" data-tab="other"><?= t('order', 'Who\'s riding') ?></a>
                                <?php endif; ?>
                                <div class="order-window__client__input">
                                    <div class="js-normal-client-tab">
                                        <div class="line">
                                            <div class="photo"><i style="background-image: url('/images/no-photo.png');"></i></div>
                                            <div style="display: inline-block; width: 270px; vertical-align: top">
                                                <? $showAddClient = empty($order->client_id) && !empty($order->phone) ?>
                                                <?= $this->render('_autocompleteClients', [
                                                    'class'       => 'not_empty',
                                                    'placeholder' => t('client', 'Search'),
                                                    'style'       => $showAddClient ? 'display: none' : '',
                                                ]) ?>

                                                <a class="js-add-client add-client"><i></i><?= t('order', 'Add customer') ?></a>

                                                <div style="<?if(!$showAddClient):?>display: none;<?endif;?>" class="js-add-client-inputs">
                                                    <a class="js-hide-add-client hide-add-client"></a>
                                                    <?= Html::activeInput('tel', $order, 'phone', [
                                                        'class'     => 'mask_phone not_empty',
                                                        'data-lang' => $langCode,
                                                        'is-valid'  => $phone_valid ? 1 : 0,
                                                    ]) ?>
                                                    <div class="add-client-cols">
                                                        <div class="col">
                                                            <input type="text" placeholder="<?= t('order', 'Surname') ?>">
                                                        </div>
                                                        <div class="col">
                                                            <input type="text" placeholder="<?= t('order', 'Name') ?>">
                                                        </div>
                                                        <div class="col">
                                                            <input type="text" placeholder="<?= t('order', 'Middle name') ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="client-info" data-black-list-title="<?= t('client', 'Black list') ?>">
                                            <?= $this->render('_clientInfo', [
                                                'clientInfo' => $clientInfo,
                                                'canUpdate'  => false,
                                                'hide_client'  => $hide_client,
                                            ]) ?>
                                            <?= Html::activeHiddenInput($order, 'client_id') ?>
                                        </div>
                                    </div>
                                    <div class="js-other-client-tab" style="display: none;">
                                        <div class="line">
                                            <?php if ($order->client_passenger_phone): ?>
                                                <div class="photo">
                                                    <i style="background-image: url(
                                                        <?= $order->clientPassenger
                                                            ->isFileExists($order->clientPassenger->photo)
                                                                ? $order->clientPassenger
                                                                    ->getPicturePath($order->clientPassenger->photo)
                                                                : '/images/no-photo.png' ?>
                                                    );"></i>
                                                </div>
                                                <div style="display: inline-block; width: 270px; vertical-align: top">
                                                    <input readonly
                                                           name="Order[client_passenger_phone]"
                                                           type="tel" class="mask_phone"
                                                           data-lang="<?=$langCode;?>"
                                                           value="<?= $order->client_passenger_phone ?>">
                                                    <div class="add-client-cols">
                                                        <div class="col">
                                                            <input readonly
                                                                   name="Order[client_passenger_lastname]"
                                                                   type="text"
                                                                   value="<?= $order->clientPassenger->last_name ?>">
                                                        </div>
                                                        <div class="col">
                                                            <input readonly
                                                                   name="Order[client_passenger_name]"
                                                                   type="text"
                                                                   value="<?= $order->clientPassenger->name ?>">
                                                        </div>
                                                        <div class="col">
                                                            <input readonly
                                                                   name="Order[client_passenger_secondname]"
                                                                   type="text"
                                                                   value="<?= $order->clientPassenger->second_name ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="autocomplete-clients-content"></div>
                        </div>
                        <div class="order-window__driver">
                            <?if(!empty($workerInfo)):?>
                                <div style="position: relative; z-index: 9998;">
                                    <b class="title"><?= t('employee', 'Worker') ?></b>
                                    <div class="order-window__client__input">
                                        <div class="client-info">
                                            <?= $this->render('_workerInfo', [
                                                'workerInfo'   => $workerInfo,
                                                'showDistance' => false,
                                                'canUpdate'    => false,
                                                'show_phone'   => $show_phone
                                            ]) ?>
                                            <?= Html::activeHiddenInput($order, 'worker_id') ?>
                                        </div>
                                    </div>
                                </div>
                            <?endif;?>
                        </div>
                    </div>
                    <ul class="order-window__points">

                        <li class="order-window__points__item" data-point-id="0">
                            <div class="order-window__points__item__content">
                                <div class="line">
                                    <div class="number">
                                        <i>A</i>
                                    </div>
                                    <div class="street">
                                        <label><?= t('order', 'Street or place') ?></label>
                                        <div class="disabled-row">
                                            <?= Html::encode($order->getFormatStreet()); ?>
                                        </div>
                                    </div>
                                    <div class="parking">
                                        <? $parking = $order->address['A']['parking'];?>
                                        <label><?= t('order', 'Parking') ?></label>
                                        <div class="disabled-row"><?= !empty($parking) ? Html::encode($parking) : t('order', 'It is not defined') ?></div>
                                    </div>
                                    <div class="house">
                                        <label><?= t('order', 'b.') ?></label>
                                        <div class="disabled-row"><?= Html::encode($order->address['A']['housing']); ?></div>
                                    </div>
                                    <div class="porch">
                                        <label><?= t('order', 'Porch') ?></label>
                                        <div class="disabled-row"><?= Html::encode($order->address['A']['porch']); ?></div>
                                    </div>
                                    <div class="apt">
                                        <label><?= t('order', 'apt.') ?></label>
                                        <div class="disabled-row"><?= Html::encode($order->address['A']['apt']); ?></div>
                                    </div>
                                </div>
                                <div class="line">
                                    <div class="number"></div>
                                    <span class="comment" style="display: none">
                                        <div class="disabled-row street"><?= Html::encode($order->address['A']['comment']); ?></div>
                                    </span>
                                    <span class="phone" style="display: none">
                                        <input readonly
                                               type="text"
                                               value="<?= $order->address['A']['phone'] ?>" <?= $disabledAddress; ?>
                                               class="mask_phone input-view-mode"
                                               maxlength=30
                                               placeholder="+_(___)___-____">
                                    </span>
                                </div>
                            </div>
                        </li>

                        <?
                        $cnt = count($order->address);
                        if($cnt >= 1):
                            $index = 0;
                            foreach($order->address as $key => $item):
                                $index++;
                                if($key == 'A')
                                    continue;
                                ?>

                                <li class="order-window__points__item" data-point-id="<?= $index ?>">
                                    <div class="order-window__points__item__content">
                                        <div class="line">
                                            <div class="number">
                                                <i><?= $key ?></i>
                                            </div>
                                            <div class="street">
                                                <label><?= t('order', 'Street or place') ?></label>
                                                <div class="disabled-row">
                                                    <?= Html::encode($order->getFormatStreet($key)); ?>
                                                </div>
                                            </div>
                                            <div class="parking">
                                                <? $parking = $order->address[$key]['parking'];?>
                                                <label><?= t('order', 'Parking') ?></label>
                                                <div class="disabled-row"><?= !empty($parking) ? Html::encode($parking) : t('order', 'It is not defined') ?></div>
                                            </div>
                                            <div class="house">
                                                <label><?= t('order', 'b.') ?></label>
                                                <div class="disabled-row"><?= Html::encode($order->address[$key]['housing']); ?></div>
                                            </div>
                                            <div class="porch">
                                                <label><?= t('order', 'Porch') ?></label>
                                                <div class="disabled-row"><?= Html::encode($order->address[$key]['porch']); ?></div>
                                            </div>
                                            <div class="apt">
                                                <label><?= t('order', 'apt.') ?></label>
                                                <div class="disabled-row"><?= Html::encode($order->address[$key]['apt']); ?></div>
                                            </div>
                                        </div>
                                        <div class="line">
                                            <div class="number"></div>
                                            <span class="comment" style="display: none">
                                                <div class="disabled-row street"><?= Html::encode($order->address[$key]['comment']); ?></div>
                                            </span>
                                            <span class="phone" style="display: none">
                                                <input readonly
                                                       type="text"
                                                       value="<?= $order->address[$key]['phone'] ?>" <?= $disabledAddress; ?>
                                                       class="mask_phone input-view-mode"
                                                       maxlength=30
                                                       placeholder="+_(___)___-____">
                                            </span>
                                            <span class="code" style="display: none">
                                                <div class="disabled-row code"><?= Html::encode(t('order', 'Code') . ': '
                                                        . $order->address[$key]['confirmation_code']); ?></div>
                                            </span>
                                        </div>
                                    </div>
                                </li>
                            <?endforeach;?>
                        <?endif?>
                    </ul>
                    <?if (!empty( $order->comment)):?>
                    <div class="order-window__comment">
                        <div class="disabled-row"><?= Html::tag('pre', Html::encode($order->comment)); ?></div>
                    </div>
                    <?endif;?>
                    <div class="order-window__cols">
                        <div class="col">
                            <b><?= t('order', 'Tariff and additional options') ?></b>
                            <div class="disabled-row"><?= Html::encode($positions[$order->position_id]) ?></div>
                            <div class="disabled-row"><?= empty($order->tariff->name) ? '' : Html::encode($order->tariff->name) ?></div>
                            <ul class="static_comfort_list">
                                <?php
                                if (!empty($order->options)) {
                                    foreach ($order->options as $option) {
                                        echo Html::tag('li',
                                            Html::label(
                                                Html::encode(t('car-options', $option->name))
                                            ));
                                    }
                                } else {
                                    echo Html::tag('div', t('app', 'No wishes'), [
                                        'class' => 'row_input',
                                    ]);
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="col">
                            <? if (!empty($order->payment)): ?>
                                <b><?= Html::encode($order->getAttributeLabel('payment')); ?></b>
                                <div class="disabled-row"><?= Html::encode($order->getPaymentNameByKey($order->payment)); ?></div>
                            <? endif ?>
                        </div>
                        <div class="col route-info">
                            <? $currencySymbol = Html::encode(Currency::getCurrencySymbol($order->currency_id)) ?>
                            <? if (!empty($order->predv_price)): ?>
                                <div id="routeAnalyzer" class="owbc_t">
                                    <b><?= t('order', 'Preliminary calculation') ?></b>
                                    <div class="row_input">
                                        <? if ($order->predv_price_no_discount AND $order->predv_price_no_discount != $order->predv_price): ?>
                                                <small><s>
                                                    <?= app()->formatter->asMoney(+$order->predv_price_no_discount, $currencySymbol); ?>
                                                </s></small>
                                                &nbsp;
                                        <? endif; ?>

                                        <?= app()->formatter->asMoney(+$order->predv_price, $currencySymbol); ?>
                                    </div>
                                </div>
                            <? endif ?>
                            <? if (isset($order->detailCost)): ?>
                                <div class="owbc_t">
                                    <b><?= t('order', 'Cost') ?></b>
                                    <div class="row_input">
                                        <? if ($order->detailCost->promo_discount_value && $order->detailCost->promo_discount_value != 0): ?>
                                            <small><s><?= app()->formatter->asMoney(
                                                        (float)$order->detailCost->summary_cost + $order->detailCost->promo_discount_value,
                                                        $currencySymbol
                                                    ); ?></s></small>

                                        <? endif; ?>
                                        <?= app()->formatter->asMoney((float)$order->detailCost->summary_cost,
                                            $currencySymbol); ?>
                                    </div>
                                </div>
                            <? endif ?>
                            <? if (!empty($order->detailCost->surcharge)): ?>
                                <div class="owbc_t">
                                    <b><?= t('order', 'Surcharge for worker') ?></b>
                                    <div class="row_input">
                                        <?= app()->formatter->asMoney(+$order->detailCost->surcharge, $currencySymbol); ?>
                                    </div>
                                </div>
                            <? endif ?>
                            <? if (!empty($order->detailCost->tax)): ?>
                                <div class="owbc_t">
                                    <b><?= t('order', 'Tax') ?></b>
                                    <div class="row_input">
                                        <?= app()->formatter->asMoney(+$order->detailCost->tax, $currencySymbol); ?>
                                    </div>
                                </div>
                            <? endif ?>
                        </div>
                    </div>
                    <? if (!empty($orderActions)): ?>
                        <div class="order-window__submit">
                            <?php $form = ActiveForm::begin([
                                'id'                     => 'order_view',
                                'enableClientValidation' => false,
                                'enableClientScript'     => false,
                                'action'                 => ['/order/view', 'order_number' => $order->order_number],
                                'options'                => [
                                    'class' => 'order-form',
                                ],
                            ]);
                            ?>
                            <div class="order-actions">
                                <b><?= t('order', 'Action of the order') ?>:</b>
                                <div class="edit-order-actions">
                                    <?= Html::activeDropDownList($order, 'orderAction', $orderActions,
                                        ['class' => 'default_select']) ?>
                                </div>
                            </div>
                            <div class="order-buttons submit_form">
                                <?= Html::submitInput(t('app', 'Save'), ['class' => 'js-save-order']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    <? endif; ?>
                    <div class="order-window__errors">
                        <p id="order-error" style="color:red; display:none"/>
                    </div>
                </div>
            </div>
            <div id="b02" style="padding: 25px 32px;"></div>
            <div id="b03" style="padding: 25px 32px;"></div>
            <div id="b04" style="padding: 25px 32px;"></div>
            <!--<div id="b05">Биржа</div>-->
            <div style="display: none;" id="order-id" data-order_id="<?= $order->order_id ?>"></div>
        </div>
    </div>
</div>
</div>