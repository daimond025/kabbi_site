<?php

use app\modules\order\assets\OrderAsset;
use app\modules\order\models\OrderSearch;
use frontend\widgets\LinkPager;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\order\models\OrderStatus;
use yii\widgets\Pjax;



/* @var $this yii\web\View */
/* @var $order app\modules\order\models\Order */
/* @var $dataProvider ActiveDataProvider */
/* @var $workers array */
/* @var $isClient bool Если авторизован как клиент */
/* @var $show_phone bool */
/* @var $tariffMap array */
/* @var $optionMap array */

/*
$script = <<< JS
$(document).ready(function() {
    setInterval(function() {     
      $.pjax.reload({container:'#id_grid_pre_orders'});
    }, 10000);
});
JS;
$this->registerJs($script);

$this->registerJs("var Yii = ". json_encode($config).";",View::POS_HEAD);
*/

$pjax = OrderSearch::getPjaxUrl();
?>


<?php Pjax::begin([
    'id' => OrderAsset::$id_pre_orders,
    'enablePushState' => false,
    'timeout' => 10000,
]); ?>

<?= GridView::widget([
    'id' => 'id_grid_pre_orders',
    'dataProvider' => $dataProvider,
    'tableOptions' => [
        'class' => 'order_table'
    ],
    'headerRowOptions' => [
        'data-pjax_url' => http_build_query($pjax),
        'data-total' => $dataProvider->getTotalCount(),
        'class' => 'pjax_url'
    ],
    'emptyText' => t('app', 'Empty'),
    'layout' => "{items}\n{pager}",
    'pager'        => [
        'class'         => LinkPager::className(),
        'prevPageLabel' => t('app', 'Prev'),
        'nextPageLabel' => t('app', 'Next'),
    ],
    'columns' => [
        [
            'attribute'=>'order_number',
            //'header' => '<p data-column="0" data-name="order_id" href="' .  Url::to(['order/sort', 'type' => OrderStatus::STATUS_GROUP_6]).  '"> <span>№</span> </p>',
             'label' => '№',
            'headerOptions' => ['class' => 'ot_01'],

            'contentOptions' =>[
                'class' => 'ot_01',
            ],
            'content'=>function($model) use ($isClient){
                return $this->render('_grid_order_number', [
                    'order'     => $model,
                    'isClient'   => $isClient
                ]);
            }
        ],
        [
            'attribute'=>'order_time',
            'label' => t('order', 'Order on'),
            'headerOptions' => [
                'class' => 'ot_02'
            ],

            'contentOptions' =>[
                'class' => 'ot_02',
            ],
            'content'=>function($model){
                return $this->render('_grid_order_time', [
                    'order'     => $model,
                ]);
            }
        ],
        [
            'attribute'=>'address',
            'header' => t('order', 'From-To'),
            'headerOptions' => [
                'class' => 'ot_03'
            ],

            'contentOptions' =>[
                'class' => 'ot_03',
            ],
            'content'=>function($model){
                return $this->render('_address', [
                    'address' => $model->address,
                ]);
            }
        ],
        [
            'attribute'=>'client_id',
            'header' => t('order', 'Client'),
            'headerOptions' => [
                'class' => 'ot_04'
            ],

            'contentOptions' =>[
                'class' => 'ot_04',
            ],
            'content'=>function($model) use ($isClient){
                return $this->render('_grid_order_client', [
                    'order' => $model,
                    'isClient' => $isClient,
                ]);
            }
        ],
        [
            'attribute'=>'worker_id',
            'header' => t('order', 'Worker + car'),
            'headerOptions' => [
                'class' => 'ot_05'
            ],

            'contentOptions' =>[
                'class' => 'ot_05',
            ],
            'content'=>function($model) use ($show_phone, $isClient){
                return $this->render('_grid_order_worker', [
                    'order' => $model,
                    'isClient' => $isClient,
                    'show_phone' => $show_phone,
                ]);
            }
        ],
        [
            'attribute'=>'tariff_id',
            'header' =>t('order', 'Tariff and additional options'),
            'headerOptions' => [
                'class' => 'ot_06'
            ],

            'contentOptions' =>[
                'class' => 'ot_06',
            ],
            'content'=>function($model) use ($show_phone, $isClient){
                return $this->render('_grid_order_tariff', [
                    'order' => $model,
                    'isClient' => $isClient,
                    'show_phone' => $show_phone,
                ]);
            }
        ],
        [
            'class' => \yii\grid\ActionColumn::className(),
            'headerOptions' => [
                'class' => 'ot_07'
            ],

            'contentOptions' =>[
                'class' => 'ot_07',
            ],

            'template' => '{update}',

            'buttons' => [
                'update' => function ($url, $model, $key) use ($isClient){
                    return $this->render('_grid_order_update', [
                        'order' => $model,
                        'isClient' => $isClient,
                    ]);
                },
            ],
        ]
    ]
]);
$this->render('grid_refresh', [
    'dataProvider' => $dataProvider,
    'workers' => $workers,
]);
Pjax::end();
?>

