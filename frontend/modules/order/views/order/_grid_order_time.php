<?php

/* @var $order app\modules\order\models\Order */

?>
<span class="ot_time">
    <?= date('d.m.Y H:i', $order->order_time) ?>
</span>
