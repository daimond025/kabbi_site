<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $order app\modules\order\models\Order */
/* @var $clid int */

$this->title = Yii::t('order', 'Orders');

$bundle_order = \app\modules\order\assets\OrderAsset::register($this);
$this->registerJsFile($bundle_order->baseUrl . '/Timer.js');
$this->registerJsFile($bundle_order->baseUrl . '/functions.js');
$this->registerJsFile($bundle_order->baseUrl . '/main.js');
$this->registerJsFile($bundle_order->baseUrl . '/bonusPayment.js');
$controller = $this->context->id;
?>
<?= Html::a(t('order', 'New order'), ['create'], ['class' => 'button add_order', 'style' => 'float: right; position: relative; top: 5px;']) ?>
<div class="order_status">
    <span class="s_green"><i></i> <?= Html::encode($workers['free']); ?></span>
    <span class="s_red"><i></i> <?= Html::encode($workers['busy']); ?></span>
</div>
<div class="filter_header">
    <a href="<?=Url::to('/order/operator/')?>" class="link_red"><b><?=t('order', 'Cancel')?></b></a><br>
    <? if (empty($clid)) : ?>
      <h1><?=t('order', 'Incoming call: unknown phone number')?></h1>
    <? else: ?>
      <h1><?=t('order', 'Incoming call: several orders')?></h1>
    <? endif; ?>
</div>

<?=$this->render('warning', ['orders' => $orders, 'call' => $call])?>