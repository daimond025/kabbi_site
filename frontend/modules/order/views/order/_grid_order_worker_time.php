<?php

/* @var $order app\modules\order\models\Order */
/* @var $order app\modules\order\models\Order */

use app\modules\order\models\OrderStatus;
use common\services\OrderStatusService;
use yii\helpers\Html;


$order->status_time = $order->getOrderTimestamp($order->status_time);
$timeToOrder = $order->getTimerData($order->order_time);
$statusClass = in_array($order->status_id, \app\modules\order\models\OrderStatus::getWarningStatusId()) ? 'ot_red' : 'ot_green';
$timer_class = $timeToOrder['TYPE'] == 'down' ? 'ot_green' : 'ot_red';

?>
    <span class="<?= $statusClass ?>"><b><?= Html::encode(OrderStatusService::translate($order->status_id, $order->position_id)); ?></b></span>
<? if (in_array($order->status_id, OrderStatus::getCountdownSatusList())): ?>
    <br>
    <?php
    $time = $order->status_time ;
    if (($order->status_id == OrderStatus::STATUS_GET_WORKER || $order->status_id == OrderStatus::STATUS_EXECUTION_PRE) && !empty($order->time_to_client)):
        $time += $order->time_to_client * 60;
        ?>
        <span class="ot_time"><?= Html::encode($order->time_to_client); ?> <?= t('app', 'min.') ?></span>
    <? elseif ($order->status_id == OrderStatus::STATUS_WORKER_WAITING):
        $time += $order->getWorkerWaitingTime();
        ?>
    <? endif ?>
    <?
    $timeToOrder = $order->getTimerData($time);
    $timer_class = $timeToOrder['TYPE'] == 'down' ? 'ot_green' : 'ot_red';
    $timeToOrder['MINUTES'] = $timeToOrder['TYPE'] == 'down' ? $timeToOrder['MINUTES'] : '-' . $timeToOrder['MINUTES'];
    ?>
    <span class="<?= $timer_class ?> timer" data-type="<?= $timeToOrder['TYPE'] ?>"><b><span
                    class="minutes"><?= Html::encode($timeToOrder['MINUTES']); ?></span><span
                    class="time_colon">:</span><span
                    class="seconds"><?= Html::encode($timeToOrder['SECONDS']); ?></span></b></span>
<? endif ?>