<?php

use app\modules\order\assets\OrderAsset;
use app\modules\order\models\OrderSearch;
use common\modules\tenant\models\TenantSetting;
use common\services\OrderStatusService;
use frontend\widgets\LinkPager;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\order\models\OrderStatus;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $order app\modules\order\models\Order */
/* @var $workers array */
/* @var $orderSearch backend\modules\setting\models\OrderSearch; */
/* @var $dataProvider */
/* @var $isClient bool Если авторизован как клиент */
/* @var $show_phone bool */
/* @var $tariffMap array */
/* @var $optionMap array */

/*
$script = <<< JS
$(document).ready(function() {
    setInterval(function() {
      $.pjax.reload({container:'#id_grid_pre_orders'});
    }, 10000);
});
JS;
$this->registerJs($script);
*/

$pjax = OrderSearch::getPjaxUrl();

?>
<ul
    class="orders_filter">
    <li><label><input id="filter_all" value="0" type="checkbox" checked="checked" /> <?= t('order', 'All')?></label></li>
    <? $status_works = OrderStatus::getFilterStatusList(OrderStatus::STATUS_GROUP_5) ?>
    <? foreach ($status_works as $status_id => $status_name): ?>
        <li>
            <label>
                <input name="status_filter" type="checkbox" value="<?= $status_id ?>"/>
                <?= Html::encode(OrderStatusService::translate($status_id)
                    . ($status_id == OrderStatus::STATUS_NO_CARS_BY_TIMER ? ' (' . t('reports', 'by timer') . ')' : '')); ?>
            </label>
        </li>
    <? endforeach; ?>
</ul>
<?php Pjax::begin([
    'id' =>OrderAsset::$id_rej_orders,
    'enablePushState' => false,
    'timeout' => 10000,
]); ?>

<?= GridView::widget([
    'id' => 'id_grid_worker_orders',
    'dataProvider' => $dataProvider,
    'emptyText' => t('app', 'Empty'),
    'headerRowOptions' => [
        'data-pjax_url' => http_build_query($pjax),
        'class' => 'pjax_url'
    ],
    'tableOptions' => [
        'class' => 'order_table'
    ],
    'rowOptions'=>function($model ){
        return ['class' => 'status_' . $model->status_id];

    },
    'layout' => "{items}\n{pager}",
    'pager'        => [
        'class'         => LinkPager::className(),
        'prevPageLabel' => t('app', 'Prev'),
        'nextPageLabel' => t('app', 'Next'),
    ],
    'columns' => [
        [
            'attribute'=>'order_number',
            'label' => '№',
            'headerOptions' => ['class' => 'ot_01'],

            'contentOptions' =>[
                'class' => 'ot_01',
            ],
            'content'=>function($model) use ($isClient){
                return $this->render('_grid_order_number', [
                    'order'     => $model,
                    'isClient'   => $isClient
                ]);
            }
        ],
        [
            'attribute'=>'order_time',
            'label' => t('order', 'Status and time'),
            'headerOptions' => [
                'class' => 'ot_02'
            ],

            'contentOptions' =>[
                'class' => 'ot_02',
            ],
            'content'=>function($model){
                return $this->render('_grid_order_time_complated', [
                    'order'     => $model,
                ]);
            }
        ],
        [
            'attribute'=>'address',
            'header' => t('order', 'From-To'),
            'headerOptions' => [
                'class' => 'ot_03'
            ],

            'contentOptions' =>[
                'class' => 'ot_03',
            ],
            'content'=>function($model){
                return $this->render('_address', [
                    'address' => $model->address,
                ]);
            }
        ],
        [
            'attribute'=>'client_id',
            'header' => t('order', 'Client'),
            'headerOptions' => [
                'class' => 'ot_04'
            ],

            'contentOptions' =>[
                'class' => 'ot_04',
            ],
            'content'=>function($model) use ($isClient){
                return $this->render('_grid_order_client', [
                    'order' => $model,
                    'isClient' => $isClient,
                ]);
            }
        ],
        [
            'attribute'=>'worker_id',
            'header' => t('order', 'Worker + car'),
            'headerOptions' => [
                'class' => 'ot_05'
            ],

            'contentOptions' =>[
                'class' => 'ot_05',
            ],
            'content'=>function($model) use ($show_phone, $isClient){
                return $this->render('_grid_order_worker', [
                    'order' => $model,
                    'isClient' => $isClient,
                    'show_phone' => $show_phone,
                ]);
            }
        ],
        [
            'attribute'=>'tariff_id',
            'header' =>t('order', 'Tariff and additional options'),
            'headerOptions' => [
                'class' => 'ot_06'
            ],

            'contentOptions' =>[
                'class' => 'ot_06',
            ],
            'content'=>function($model) use ($show_phone, $isClient){
                return $this->render('_grid_order_tariff', [
                    'order' => $model,
                    'isClient' => $isClient,
                    'show_phone' => $show_phone,
                ]);
            }
        ],
        [
            'class' => \yii\grid\ActionColumn::className(),
            'headerOptions' => [
                'class' => 'ot_07'
            ],

            'contentOptions' =>[
                'class' => 'ot_07',
            ],

            'template' => '{update}',

            'buttons' => [
                'update' => function ($url, $model, $key) use ($isClient){
                    return $this->render('_grid_order_update', [
                        'order' => $model,
                        'isClient' => $isClient,
                    ]);
                },
            ],
        ],
    ]
]);
$this->render('grid_refresh', [
    'dataProvider' => $dataProvider,
    'workers' => $workers,
]);
Pjax::end();
?>

