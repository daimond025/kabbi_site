<?php

use app\modules\order\models\Order;
use common\modules\tenant\models\DefaultSettings;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use common\modules\tenant\models\TenantSetting;

/* @var $this yii\web\View */
/* @var $order app\modules\order\models\Order */
/* @var $form yii\widgets\ActiveForm */
/* @var $parkingList array */
/* @var $dataForm array */
/* @var $isClient boolean */
/* @var $isCopyOrder boolean */
/* @var $canBonusPayment bool */

$isHasCommonSettingsForCouriers = $order->isHasCommonSettingsForCouriers();
$commonSetting = $order->getCommonSettingsOrder();
$showAddClient = empty($order->client_id) && !empty($order->phone);

if (empty($order->city_id)) {
    $arCurCity = array_slice($dataForm['CITY_LIST'], 0, 1, true);
    $curCity   = current($arCurCity);
    $curCityId = current(array_keys($arCurCity));
} else {
    $curCity   = $dataForm['CITY_LIST'][$order->city_id];
    $curCityId = $order->city_id;
}

if ($this->context->id == 'operator') {
    \frontend\assets\LeafletAsset::register($this);
    $bundle_order = \app\modules\order\assets\OrderAsset::register($this);
    $this->registerJsFile($bundle_order->baseUrl . '/Timer.js');
    $this->registerJsFile($bundle_order->baseUrl . '/functions.js');
    $this->registerJsFile($bundle_order->baseUrl . '/orderDatepicker.js');
    $this->registerJsFile($bundle_order->baseUrl . '/map/orderMap.js');
    $this->registerJsFile($bundle_order->baseUrl . '/main.js');
    $this->registerJsFile($bundle_order->baseUrl . '/bonusPayment.js');
    $this->registerJs('init_pac()');
    $this->registerJs("initSortable()");
    $this->registerJs('window.orderMap && orderMap.initialize();');
    echo '<div class="bread"><a href="' . Url::to([
            '/order/operator/index',
            'city_id' => get('city_id'),
        ]) . '">' . t('app', 'Orders') . '</a></div>';
}
?>
<div id="add-order" style="text-align: left;"
     data-module="<?= $isClient ? '/client/cabinet/' : '/order/'; ?>">
    <?php $form = ActiveForm::begin([
        'id'                     => 'order_create',
        'enableClientValidation' => false,
        'enableClientScript'     => false,
        'action'                 => [$isClient ? '/client/cabinet/order/create' : '/order/order/create'],
        'options'                => [
            'class' => 'order-form',
            'data'  => [
                'max-number-of-address-points' =>
                    isset($dataForm['MAX_NUMBER_OF_ADDRESS_POINTS']) ? $dataForm['MAX_NUMBER_OF_ADDRESS_POINTS'] : 0,
                'show-card-payment'            => 0,
                'is-client'                    => $isClient ? 1 : 0,
                'is-copy-order'                => !empty($isCopyOrder),
            ],
        ],
    ]);
    echo Html::hiddenInput('call_id', $call_id);
    ?>

    <div class="order-window  <? if ($this->context->id == 'operator') {
        echo 'operator-order-window';
    } ?>"
    ">
    <div class="order-window__header">
        <div class="city-select">
            <? if ($isClient) {
                echo $dataForm['CITY_LIST'][user()->city_id];
                echo Html::activeHiddenInput($order, 'city_id',
                    ['value' => user()->city_id, 'class' => 'js-city_id']);
            } else {
                echo Html::activeDropDownList($order, 'city_id', $dataForm['CITY_LIST'], [
                    'data-placeholder' => current($dataForm['CITY_LIST']) ? Html::encode(current($dataForm['CITY_LIST'])) : t('app',
                        'Choose city'),
                    'class'            => 'default_select js-city_id',
                ]);
            } ?>
            <input class="lat" type="hidden" name="city_lat" value="<?= $dataForm['CUR_CITY_COORDS']['lat'] ?>">
            <input class="lon" type="hidden" name="city_lon" value="<?= $dataForm['CUR_CITY_COORDS']['lon'] ?>">
        </div>
        <div class="date_order_selector" data-order_id="<?= $order->order_id; ?>">
            <h2 style="font-size: 24px; line-height: 24px; position: relative; top: 9px; vertical-align: top;"><?= t('order',
                    'New order to') ?> </h2>
            <div style="display: inline-block; zoom: 1; *display: inline;  vertical-align: top;">
                <a class="ordered_when s_datepicker a_sc"><?= t('order', 'now') ?></a>
                <div class="s_datepicker_content b_sc" style="z-index: 9999;">
                    <div class="short_time_items">
                        <?= $this->render('_dateIncrement'); ?>
                    </div>
                    <?= Html::activeHiddenInput($order, 'order_now', [
                        'class' => 'js-now',
                    ]); ?>
                    <?= Html::activeTextInput($order, 'order_date', [
                        'class' => 'sdp_input js-order_date',
                    ]); ?>
                    <div class="sdp_n"></div>
                    <div class="chose_time_order">
                        <span><?= t('app', 'Hours') ?>:</span>
                        <?= Html::activeTextInput($order, 'order_hours', [
                            'class' => 'js-hours',
                        ]); ?>
                        <span><?= t('app', 'Minutes') ?>:</span>
                        <?= Html::activeTextInput($order, 'order_minutes', [
                            'class' => 'js-minutes',
                        ]); ?>
                    </div>
                    <div class="submit_date_order">
                        <button type="button" class="js-choose-date"><?= t('app', 'Confirm') ?></button>
                        <button type="button" class="js-choose-now" data-title="<?= t('order', 'now') ?>"><?= t('app',
                                'Now') ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="order-window__people">
        <div class="order-window__client">
            <div style="position: relative; z-index: 9998;">
                <label class="other-client js-other-client">
                    <input <?= $order->client_passenger_phone ? 'checked' : '' ?>
                           type="checkbox"
                           value="1"
                           name="Order[isGoPassenger]"> <?= t('order', 'Will go another') ?>
                </label>
                <a class="title active" data-tab="normal"><?= t('order', 'Client') ?></a>
                <a class="title" <?= $order->client_passenger_phone ? '' : 'style="display: none;' ?>"
                    data-tab="other"><?= t('order', 'Who\'s riding') ?></a>

                <div class="order-window__client__input">
                    <div class="js-normal-client-tab">
                        <div class="line">
                            <div class="photo"><i style="background-image: url('/images/no-photo.png');"></i></div>
                            <div style="display: inline-block; width: 270px; vertical-align: top">
                                <?= $this->render('_autocompleteClients', [
                                    'class'       => 'not_empty',
                                    'placeholder' => t('client', 'Search'),
                                    'style'       => $showAddClient ? 'display: none' : '',
                                ]) ?>

                                <a class="js-add-client add-client"><i></i><?= t('order', 'Add customer') ?></a>

                                <div style="<?if(!$showAddClient):?>display: none;<?endif;?>" class="js-add-client-inputs">
                                    <a class="js-hide-add-client hide-add-client"></a>
                                    <?= Html::activeInput('tel', $order, 'phone', [
                                        'class'     => 'mask_phone not_empty',
                                        'data-lang' => mb_substr(app()->language, 0, 2),
                                        'is-valid'  => $phone_valid ? 1 : 0,
                                    ]) ?>
                                    <div class="add-client-cols">
                                        <div class="col">
                                            <input name="Order[client_lastname]"
                                                   type="text"
                                                   placeholder="<?= t('order', 'Surname') ?>">
                                        </div>
                                        <div class="col">
                                            <input name="Order[client_name]"
                                                   type="text"
                                                   placeholder="<?= t('order', 'Name') ?>">
                                        </div>
                                        <div class="col">
                                            <input name="Order[client_secondname]"
                                                   type="text"
                                                   placeholder="<?= t('order', 'Middle name') ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="client-info" data-black-list-title="<?= t('client', 'Black list') ?>">
                            <?= $this->render('_clientInfo', [
                                'clientInfo' => $clientInfo,
                                'canUpdate'  => true,
                            ]) ?>
                            <?= Html::activeHiddenInput($order, 'client_id') ?>
                        </div>
                    </div>
                    <div class="js-other-client-tab" style="display: none;">
                        <div class="line">
                            <div class="photo"><i style="background-image: url('/images/no-photo.png');"></i></div>
                            <div style="display: inline-block; width: 270px; vertical-align: top">
                                <input name="Order[client_passenger_phone]"
                                       type="tel"
                                       class="mask_phone"
                                       data-lang="<?=mb_substr(app()->language, 0, 2);?>"
                                       <?= $order->client_passenger_phone
                                           ? sprintf('value="%s"', $order->client_passenger_phone)
                                           : '' ?>>
                                <div class="add-client-cols">
                                    <div class="col">
                                        <input name="Order[client_passenger_lastname]"
                                               type="text"
                                               placeholder="<?= t('order', 'Surname') ?>"
                                               <?= $order->client_passenger_phone
                                                   ? sprintf('value="%s"', $order->client_passenger_lastname)
                                                   : '' ?>>
                                    </div>
                                    <div class="col">
                                        <input name="Order[client_passenger_name]"
                                               type="text"
                                               placeholder="<?= t('order', 'Name') ?>"
                                               <?= $order->client_passenger_phone
                                                    ? sprintf('value="%s"', $order->client_passenger_name)
                                                    : '' ?>>
                                    </div>
                                    <div class="col">
                                        <input name="Order[client_passenger_secondname]"
                                               type="text"
                                               placeholder="<?= t('order', 'Middle name') ?>"
                                               <?= $order->client_passenger_phone
                                                   ? sprintf('value="%s"', $order->client_passenger_secondname)
                                                   : '' ?>>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="autocomplete-clients-content"></div>
        </div>
    </div>

    <?= $this->render('_addressPattern', [
        'curCity'                        => $curCity,
        'curCityId'                      => $curCityId,
        'parkingList'                    => $parkingList,
        'isHasCommonSettingsForCouriers' => $isHasCommonSettingsForCouriers,
        'commonSetting'                  => $commonSetting
    ]) ?>

    <? if (empty($order->address)): ?>
        <ul class="order-window__points" id="sortable" data-points="2">
            <li class="order-window__points__item js-order-point" data-point-id="0">
                <div class="order-window__points__item__content">
                    <div class="line">
                        <div class="number">
                            <i>A</i>
                        </div>
                        <div class="street">
                            <label><?= t('order', 'Street or place') ?></label>
                            <?= $this->render('_autocomplete', ['class' => 'not_empty']) ?>
                            <a class="order-point-history js-address-history"></a>
                            <input data-type="city" data-name="Order[address][{point}][city]"
                                   name="Order[address][A][city]" type="hidden"
                                   value="<?= $curCity ?>"/>
                            <input type="hidden" class="js-cur-city_id" data-name="Order[address][{point}][city_id]"
                                   name="Order[address][A][city_id]"
                                   value="<?= $curCityId ?>"/>
                            <input data-type="street" autocomplete="off" data-name="Order[address][{point}][street]"
                                   name="Order[address][A][street]"
                                   type="hidden"/>
                            <input data-type="house" autocomplete="off" data-name="Order[address][{point}][house]"
                                   name="Order[address][A][house]"
                                   type="hidden"/>

                            <input data-type="placeId" autocomplete="off" data-name="Order[address][{point}][placeId]"
                                   name="Order[address][A][placeId]"
                                   type="hidden"/>

                            <input class="lat not_empty" type="hidden" data-name="Order[address][{point}][lat]"
                                   name="Order[address][A][lat]" value=""
                                   data-error="<?= t('order',
                                       'Coordinates of {point} point are not defined. Pick a point on the map or enter a valid address.') ?>">
                            <input class="lon not_empty" type="hidden" data-name="Order[address][{point}][lon]"
                                   name="Order[address][A][lon]" value=""
                                   data-error="<?= t('order',
                                       'Coordinates of {point} point are not defined. Pick a point on the map or enter a valid address.') ?>">
                        </div>
                        <div class="parking">
                            <?= $this->render('_parking', [
                                'parkingList' => $parkingList,
                                'currentChar' => 'A',
                            ]) ?>
                        </div>
                        <div class="house">
                            <label><?= t('order', 'b.') ?></label>
                            <input data-type="housing" data-name="Order[address][{point}][housing]"
                                   name="Order[address][A][housing]" type="text"
                                   autocomplete="off"/>
                        </div>
                        <div class="porch">
                            <label><?= t('order', 'Porch') ?></label>
                            <input data-name="Order[address][{point}][porch]" name="Order[address][A][porch]"
                                   type="text" autocomplete="off"/>
                        </div>
                        <div class="apt">
                            <label><?= t('order', 'apt.') ?></label>
                            <input data-name="Order[address][{point}][apt]" name="Order[address][A][apt]" type="text"
                                   autocomplete="off"/>
                        </div>
                    </div>
                    <div class="line">
                        <div class="number"></div>
                        <div class="street comment">
                            <input data-name="Order[address][{point}][comment]"
                                   name="Order[address][A][comment]"
                                   type="text"
                                   placeholder="<?= t('order', 'Comment') ?>">
                        </div>
                        <div class="phone">
                            <input data-name="Order[address][{point}][phone]"
                                   name="Order[address][A][phone]"
                                   type="text"
                                   class="mask_phone_wo_placeholder"
                                   maxlength=30
                                   placeholder="<?= t('order', 'Tel. responsible') ?>">
                        </div>
                    </div>
                </div>
                <div class="actions">
                    <a class="delete-point"><?= t('app', 'Remove') ?></a>
                </div>
                <i class="toggle-actions"><i></i><i></i><i></i></i>
            </li>
            <li class="order-window__points__item js-order-point" data-point-id="1">
                <div class="order-window__points__item__content">
                    <div class="line">
                        <div class="number">
                            <i>B</i>
                        </div>
                        <div class="street">
                            <label><?= t('order', 'Street or place') ?></label>
                            <?= $this->render('_autocomplete') ?>
                            <a class="order-point-history js-address-history"></a>
                            <input data-type="city" data-name="Order[address][{point}][city]"
                                   name="Order[address][B][city]" type="hidden"
                                   value="<?= $curCity ?>"/>
                            <input type="hidden" class="js-cur-city_id" data-name="Order[address][{point}][city_id]"
                                   name="Order[address][B][city_id]"
                                   value="<?= $curCityId ?>"/>
                            <input data-type="street" autocomplete="off" data-name="Order[address][{point}][street]"
                                   name="Order[address][B][street]"
                                   type="hidden"/>
                            <input data-type="house" autocomplete="off" data-name="Order[address][{point}][house]"
                                   name="Order[address][B][house]"
                                   type="hidden"/>

                            <input data-type="placeId" autocomplete="off" data-name="Order[address][{point}][placeId]"
                                   name="Order[address][B][placeId]"
                                   type="hidden"/>

                            <input class="lat not_empty" type="hidden" data-name="Order[address][{point}][lat]"
                                   name="Order[address][B][lat]" value=""
                                   data-error="<?= t('order',
                                       'Coordinates of {point} point are not defined. Pick a point on the map or enter a valid address.') ?>">
                            <input class="lon not_empty" type="hidden" data-name="Order[address][{point}][lon]"
                                   name="Order[address][B][lon]" value=""
                                   data-error="<?= t('order',
                                       'Coordinates of {point} point are not defined. Pick a point on the map or enter a valid address.') ?>">
                        </div>
                        <div class="parking">
                            <?= $this->render('_parking', [
                                'parkingList' => $parkingList,
                                'currentChar' => 'B',
                            ]) ?>
                        </div>
                        <div class="house">
                            <label><?= t('order', 'b.') ?></label>
                            <input data-type="housing" data-name="Order[address][{point}][housing]"
                                   name="Order[address][B][housing]" type="text"
                                   autocomplete="off"/>
                        </div>
                        <div class="porch">
                            <label><?= t('order', 'Porch') ?></label>
                            <input data-name="Order[address][{point}][porch]" name="Order[address][B][porch]"
                                   type="text" autocomplete="off"/>
                        </div>
                        <div class="apt">
                            <label><?= t('order', 'apt.') ?></label>
                            <input data-name="Order[address][{point}][apt]" name="Order[address][B][apt]" type="text"
                                   autocomplete="off"/>
                        </div>
                    </div>
                    <div class="line">
                        <div class="number"></div>
                        <div class="street comment">
                            <input data-name="Order[address][{point}][comment]"
                                   name="Order[address][B][comment]"
                                   type="text"
                                   placeholder="<?= t('order', 'Comment') ?>">
                        </div>
                        <div class="phone">
                            <input data-name="Order[address][{point}][phone]"
                                   name="Order[address][B][phone]"
                                   type="text"
                                   class="mask_phone_wo_placeholder"
                                   maxlength=30
                                   placeholder="<?= t('order', 'Tel. responsible') ?>">
                        </div>
                    </div>
                </div>
                <div class="actions">
                    <a class="delete-point"><?= t('app', 'Remove') ?></a>
                </div>
                <i class="toggle-actions"><i></i><i></i><i></i></i>
            </li>
        </ul>
    <? else: ?>
        <?= $this->render('_addressPoints', [
            'order'           => $order,
            'parkingList'     => $parkingList,
            'disabledAddress' => '',
            'isCopyOrder'     => $isCopyOrder
        ]) ?>
    <? endif ?>

    <div class="order-window__comment">
        <?= Html::activeTextarea($order, 'comment', ['placeholder' => t('order', 'Comment')]) ?>
    </div>
    <div class="order-window__points-actions">
        <? //todo: реализовать выключение кнопок?>
        <a class="add-point js-add-order-point"></a>
        <a class="js-show-map show-on-map"><?= t('order', 'Show map'); ?></a>
    </div>
    <div class="order-window__map">
        <div class="js-order-map" style="display: none">
            <div class="order-window__map-header">
                <ul>
                    <li><label><input name="map_filter[]" type="checkbox" value="free_cars" checked="checked" > <?= t('order', 'Free cars') ?></label></li>
                    <li><label><input name="map_filter[]" type="checkbox" value="busy_cars" checked="checked" > <?= t('order', 'Busy cars') ?></label></li>
                    <li><label><input name="map_filter[]" type="checkbox" value="pause_cars" checked="checked" > <?= t('order', 'Pause cars') ?></label></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="order-window__cols">
        <div class="col">
            <?= $this->render('_payment', [
                'form'            => $form,
                'order'           => $order,
                'payments'        => isset($clientInfo['payments']) ? $clientInfo['payments'] : null,
                'showCardPayment' => false,
                'canUpdate'       => true,
            ]) ?>

            <?
            $order->bonus_payment = $canBonusPayment && $order->bonus_payment;
            $hidden               = $order->payment === Order::PAYMENT_CORP || !$canBonusPayment;
            ?>
            <div class="bonus_payment" style="<?= $hidden ? 'display: none' : '' ?>">
                <?= Html::activeCheckbox($order, 'bonus_payment'); ?>
            </div>
        </div>

        <div class="col">
            <b><?= t('order', 'Tariff and additional options') ?></b>
            <?= $form->field($order, 'position_id')->begin(); ?>
            <?= Html::activeDropDownList($order, 'position_id', $positions, [
                'data-placeholder' => current($positions) ? Html::encode(current($positions)) : t('order',
                    'No professions'),
                'class'            => 'default_select not_empty',
            ]) ?>
            <?= $form->field($order, 'position_id')->end(); ?>
            <div class="order-window__tariff-field">
                <?= $form->field($order, 'tariff_id')->begin(); ?>
                <?
                $tariffDescriptions = ArrayHelper::map($tariffs, 'tariff_id', 'description');
                $tariffDescriptions = array_map(function ($item) {
                    return ['data-description' => str_replace(PHP_EOL, '<br>', Html::encode($item))];
                }, $tariffDescriptions);
                $tariffs            = ArrayHelper::map($tariffs, 'tariff_id', 'name');
                echo Html::activeDropDownList($order, 'tariff_id', $tariffs, [
                    'data-empty'       => t('order', 'No tariffs'),
                    'data-placeholder' => current($tariffs) ? Html::encode(current($tariffs)) : t('order',
                        'No tariffs'),
                    'class'            => 'default_select not_empty',
                    'options'          => $tariffDescriptions,
                ])
                ?>
                <?= $form->field($order, 'tariff_id')->end(); ?>
                <a class="tooltip order-window-tariff-description">i
                    <span>
                            <div class="order-window-tariff-description__content">
                                <p class="js-order-tariff-description"><?= str_replace(PHP_EOL, '<br>',
                                        Html::encode($order->tariff->description)) ?></p>

                            </div>
                        </span>
                </a>
            </div>

            <ul class="comfort_list"
                data-can-update="1"
                data-label-show-all="<?= t('order', 'Show all') ?>">
                <?
                if (is_array($order->additional_option)) {
                    foreach ($order->additional_option as $key => $name) {
                        echo Html::checkbox('Order[additional_option][]', true, [
                            'value' => $key,
                            'style' => 'display: none',
                        ]);
                    }
                }
                ?>
            </ul>
        </div>

        <div class="col route-info">
            <div id="routeAnalyzer">
                <b><?= t('order', 'Preliminary calculation') ?></b>
                <div style="overflow: hidden">
                    <?= Html::activeTextInput($order, 'predv_price', [
                        'data'     => [
                            'can-update' => 1,
                        ],
                        'readonly' => true,
                    ]) ?>
                    <span class="currency_symbol"><?= Html::encode(getCurrencySymbol($curCityId)); ?></span>
                    <div style="display: none;" class="route-info__price">
                        <span style="white-space: nowrap">
                            <span class="distance"></span> <span><?= t('app', 'km') ?></span>
                        </span>
                        <?= Html::activeHiddenInput($order, 'predv_distance') ?>
                        <span style="white-space: nowrap">
                            <span class="time"></span> <span><?= t('app', 'min.') ?></span>
                        </span>
                        <?= Html::activeHiddenInput($order, 'predv_time') ?>
                    </div>
                    <div style="display: none;" class="route-info_no_discount">
                        <span class="costNoDiscount"></span>
                        <span class="currency_symbol"><?= Html::encode(getCurrencySymbol($curCityId)); ?></span>
                        <?= Html::activeHiddenInput($order, 'predv_price_no_discount', [
                            'id' => 'input-predv_price_no_discount'
                        ]) ?>
                    </div>
                </div>
                <div class="js-is-fix">
                    <?
                    $showField = true;
                    if ($isClient) {
                        $isFix     = TenantSetting::getSettingValue(
                            user()->tenant_id, DefaultSettings::SETTING_IS_FIX, user()->city_id, $order->position_id);
                        $showField = (int)$isFix === 1;
                    }

                    echo $this->render('_isFixField', compact('order', 'showField'));
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="order-window__submit">
        <div class="order-actions">
            <b><?= t('order', 'Action of the order') ?>:</b>
            <?= Html::activeDropDownList($order, 'orderAction', $orderActions,
                ['class' => 'default_select']) ?>
        </div>
        <div class="order-buttons submit_form">
            <? if ($this->context->id == 'operator'): ?>
                <a href="<?= Url::to('/order/operator/') ?>" class="link_red"><b><?= t('order', 'Cancel') ?></b></a>
            <? endif ?>
            <?= Html::activeHiddenInput($order, 'parking_id') ?>
            <?= Html::activeHiddenInput($order, 'company_id') ?>
            <?= Html::activeHiddenInput($order, 'device', ['value' => Order::DEVICE_0]) ?>
            <?= Html::submitInput(t('app', 'Save'), ['id' => 'save-create-form', 'class' => 'js-save-order']); ?>
        </div>
    </div>
    <div class="order-window__errors">
        <p class="parking_error" style="color: red; margin-bottom: 10px; display: none"><?= t('order',
                'You can`t create order, because the base parking is not created of this branch') ?></p>
        <p id="order-error" style="color:red; display:none"></p>
        <div class="empty-inputs" style="display: none;">
            <div class="empty-client" style="display: none;"><?= t('validator', 'The client is not selected') ?></div>
            <div class="empty-point" style="display: none;"><?= t('validator',
                    'The address point "1" is not selected') ?></div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
</div>
