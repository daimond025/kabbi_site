<?php

use app\modules\order\models\Order;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var ActiveForm $form */
/* @var Order $order */
/* @var array $payments */
/* @var bool $showCardPayment */
/* @var bool $canUpdate */

$allPayments = Order::getPaymentVariants();

if ($order->payment === Order::PAYMENT_CORP) {
    $order->payment = Order::PAYMENT_CORP . '_' . $order->company_id;
}
$payments = empty($payments) ? [['type' => null, 'name' => t('app', 'Not selected')]] : $payments;

$currentBalance = null;
$currentCredit  = null;

$items = $options = [];
foreach ($payments as $payment) {
    $companyId = isset($payment['companyId']) ? $payment['companyId'] : null;
    $balance   = isset($payment['balance']) ? $payment['balance'] : null;
    $credit    = isset($payment['credit']) ? $payment['credit'] : null;

    $key = $payment['type'];
    if (!$showCardPayment && $key === Order::PAYMENT_CARD) {
        continue;
    }

    if ($companyId !== null) {
        $key           .= '_' . $companyId;
        $options[$key] = [
            'data-company' => $companyId,
            'data-balance' => $balance,
            'data-credit'  => $credit,
        ];

        if ($companyId === $order->company_id) {
            $currentBalance = $balance;
            $currentCredit  = $credit;
        }
    }

    $items[$key] = $payment['name'];
}

echo Html::tag('b', Html::encode($order->getAttributeLabel('payment')));

echo $form->field($order, 'payment')->begin();
echo Html::activeDropDownList($order, 'payment', $items, [
    'id'       => 'order_payment',
    'class'    => 'default_select',
    'options'  => $options,
    'disabled' => !$canUpdate,
    'data'     => [
        'default-id'   => Order::PAYMENT_CASH,
        'default-name' => isset($allPayments[Order::PAYMENT_CASH])
            ? $allPayments[Order::PAYMENT_CASH] : '',
        'empty'        => t('app', 'Not selected'),
    ],
]);
echo $form->field($order, 'payment')->end();

echo Html::tag('p', Html::encode(t('balance', 'Balance') . ": {$currentBalance}"), [
    'style'      => empty($order->company_id) ? 'display: none' : '',
    'class'      => 'js-corp-balance',
    'data-label' => t('balance', 'Balance'),
]);

echo Html::tag('p', Html::encode(t('balance', 'Credit') . ": {$currentCredit}"), [
    'style'      => empty($order->company_id) ? 'display: none' : '',
    'class'      => 'js-corp-credit',
    'data-label' => t('balance', 'Credit'),
]);