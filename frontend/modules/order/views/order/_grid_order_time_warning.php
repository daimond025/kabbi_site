<?php

/* @var $order app\modules\order\models\Order */

use app\modules\order\models\OrderStatus;
use common\services\OrderStatusService;
use yii\helpers\Html;
$timeToOrder = $order->getTimerData($order->order_time);
$statusClass = in_array($order->status_id, OrderStatus::getWarningStatusId()) ? 'ot_red' : 'ot_green';
$timer_class = $timeToOrder['TYPE'] == 'down' ? 'ot_green' : 'ot_red';

?>

<span class="ot_red"><b><?= Html::encode(OrderStatusService::translate($order->status_id,
            $order->position_id)); ?></b></span><br>
<span class="ot_time"><?= date('d ', $order->order_time) . ' ' . t('order',
        date('F', $order->order_time)) . ' ' . ' ' . t('support', 'at') . ' ' . date('H:i',
        $order->order_time) ?>
    <? if ($order->status_id == OrderStatus::STATUS_OVERDUE):
        $timeToOrder = $order->getTimerData($order->order_time);
        $timer_class = $timeToOrder['TYPE'] == 'down' ? 'ot_green' : 'ot_red';
        $timeToOrder['MINUTES'] = $timeToOrder['TYPE'] == 'down' ? $timeToOrder['MINUTES'] : '-' . $timeToOrder['MINUTES'];
        ?>
        <span class="<?= $timer_class ?> timer" data-type="<?= $timeToOrder['TYPE'] ?>"><b><span
                    class="minutes"><?= Html::encode($timeToOrder['MINUTES']) ?></span><span
                    class="time_colon">:</span><span
                    class="seconds"><?= Html::encode($timeToOrder['SECONDS']); ?></span></b></span>
    <? endif ?>
