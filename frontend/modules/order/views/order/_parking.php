<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $currentChar string */
/* @var $parkingList array */
/* @var $currentParking string */
/* @var $currentParkingId integer */
/* @var $options array */

?>

<div class="fl_dist">
    <label><?= t('order', 'Parking') ?></label>
    <?
    $disabled = empty($options['disabled']) ? '' : 'disabled="disabled"';
    ?>
    <select data-name="Order[address][{point}][parking]" <?if(!empty($currentChar)):?> name="Order[address][<?= $currentChar ?>][parking]" <?endif;?>
            class="default_select parkings"
            data-placeholder="<?= t('order', 'It is not defined') ?>"
            <?= $disabled ?>
    >
        <option value="">
            <?= t('order', 'It is not defined') ?>
        </option>

        <?
        if (!empty($parkingList)) {
            foreach ($parkingList as $parkingId => $parking) {
                $selected = $parkingId == $currentParkingId ;
                echo Html::tag('option', t('parking', Html::encode($parking)), [
                    'value'    => $parkingId,
                    'selected' => $selected,
                ]);
            }
        }
        ?>
    </select>
    <input class="js-parking" type="hidden" data-name="Order[address][{point}][parking]"
           <?if(!empty($currentChar)):?> name="Order[address][<?= $currentChar ?>][parking]" <?endif;?> value="<?= $currentParking ?>"/>
    <input class="js-parking_id" type="hidden" data-name="Order[address][{point}][parking_id]"
           <?if(!empty($currentChar)):?> name="Order[address][<?= $currentChar ?>][parking_id]" <?endif;?> value="<?= $currentParkingId ?>"/>
</div>