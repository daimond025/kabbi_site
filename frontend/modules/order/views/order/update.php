<?php

use app\modules\order\models\Order;
use common\services\OrderStatusService;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\modules\order\models\OrderStatus;

/* @var $this yii\web\View */
/* @var $order app\modules\order\models\Order */
/* @var $form yii\widgets\ActiveForm */
/* @var $parkingList array */
/* @var $orderActions array */
/* @var $availableAttributes array */
/* @var $canBonusPayment bool */
/* @var $langCode string */

$isHasSettingsForCouriers = $order->isHasSettingsCurrentOrderForCouriers();
$settingOrder             = $order->getSettingsCurrentOrder();

if ($this->context->id == 'operator') {
    \frontend\assets\LeafletAsset::register($this);
    $bundle_order = \app\modules\order\assets\OrderAsset::register($this);
    $this->registerJsFile($bundle_order->baseUrl . '/Timer.js');
    $this->registerJsFile($bundle_order->baseUrl . '/functions.js');
    $this->registerJsFile($bundle_order->baseUrl . '/orderDatepicker.js');
    $this->registerJsFile($bundle_order->baseUrl . '/map/orderMap.js');
    $this->registerJsFile($bundle_order->baseUrl . '/main.js');
    $this->registerJsFile($bundle_order->baseUrl . '/bonusPayment.js');
    $this->registerJs('init_pac()');
    $this->registerJs("initSortable()");
    $this->registerJs('window.orderMap && orderMap.initialize();');

    $call = empty($call) ? get('call') : $call;

    if ($call && in_array($order->status_id, OrderStatus::getWorksStatusId())) {
        if ($order->caller == 'client') {
            $method = 'setDriverPhone';
            $phone  = $order->worker->phone;
        } else {
            $method = 'setClientPhone';
            $phone  = $order->phone;
        }

        $this->registerJs("
            try{
                window.qt_handler.$method($phone);
            } catch(e) {
            }");
    }

    echo '<div class="bread"><a href="' . Url::toRoute([
            '/order/operator/index',
            'city_id' => get('city_id'),
        ]) . '">' . t('app', 'Orders') . '</a></div>';
}

?>
<div id="add-order" style="text-align: left;" data-module="/order/" data-points="<?=count($order->address)?>">
    <?php $form = ActiveForm::begin([
        'id'                     => 'order_update',
        'enableClientValidation' => false,
        'enableClientScript'     => false,
        'action'                 => ['/order/update', 'order_number' => $order->order_number],
        'options'                => [
            'class' => 'order-form',
            'data'  => [
                'max-number-of-address-points' =>
                    isset($dataForm['MAX_NUMBER_OF_ADDRESS_POINTS']) ? $dataForm['MAX_NUMBER_OF_ADDRESS_POINTS'] : 0,
                'show-card-payment'            => !empty($order->cardPayment),
            ],
        ],
    ]);
    ?>

    <?= Html::activeHiddenInput($order, 'hash') ?>
    <?= Html::activeHiddenInput($order, 'update_time') ?>
    <?= Html::activeHiddenInput($order, 'order_number') ?>

    <div class="tabs_links" style="padding: 10px 32px 0 32px;">
        <ul>
            <li class="active"><a class="b01"><?= t('order', 'Order') ?></a></li>
            <li><a data-href="<?= Url::to([
                    '/order/order/get-events',
                    'order_id' => $order->order_id,
                    'city_id'  => $order->city_id,
                ]) ?>" class="b02"><?= t('order', 'Events') ?></a></li>

            <li><a data-href="<?= Url::to([
                    '/order/order/get-map',
                    'order_id'  => $order->order_id,
                    'status_id' => $order->status_id,
                ]) ?>" class="b03"><?= t('order', 'Route on the map') ?></a></li>
            <!--li><a class="b04"><?= t('order', 'Exchange') ?></a></li-->
        </ul>
    </div>
    <div class="tabs_content">
        <div class="active" id="b01" style="padding-top: 0;">
            <div class="order-window <? if ($this->context->id == 'operator') {
                echo 'operator-order-window';
            } ?>">
                <div class="order-window__header">
                    <div class="city-select">
                        <?= Html::activeDropDownList($order, 'city_id', $dataForm['CITY_LIST'], [
                            'data-placeholder' => Html::encode(current($dataForm['CITY_LIST'])),
                            'class'            => 'default_select js-city_id',
                            'disabled'         => !in_array('city_id', $availableAttributes, true),
                        ]) ?>
                        <input class="lat" type="hidden" name="city_lat"
                               value="<?= $dataForm['CUR_CITY_COORDS']['lat'] ?>">
                        <input class="lon" type="hidden" name="city_lon"
                               value="<?= $dataForm['CUR_CITY_COORDS']['lon'] ?>">
                    </div>
                    <div class="date_order_selector">
                        <h2 style="font-size: 24px; line-height: 24px; position: relative; top: 9px; vertical-align: top;">
                            <?= t('order', '#{orderNumber} on', [
                                'orderNumber' => Html::encode($order->order_number),
                            ]) ?>
                        </h2>
                        <div style="display: inline-block; zoom: 1; *display: inline; vertical-align: top;">
                            <a class="ordered_when s_datepicker a_sc"><?= app()->formatter->asDateTime($order->order_time,
                                    'php:d F Y ') . t('order', 'y.') . date(', H:i', $order->order_time) ?></a>
                            <div class="s_datepicker_content b_sc" style="z-index: 9999;">
                                <div class="short_time_items">
                                    <?= $this->render('_dateIncrement'); ?>
                                </div>
                                <?= Html::activeHiddenInput($order, 'order_now', [
                                    'class' => 'js-now',
                                ]); ?>
                                <?= Html::activeTextInput($order, 'order_date', [
                                    'class' => 'sdp_input js-order_date',
                                ]); ?>
                                <div class="sdp_n"></div>
                                <div class="chose_time_order">
                                    <span><?= t('app', 'Hours') ?>:</span>
                                    <?= Html::activeTextInput($order, 'order_hours', [
                                        'class' => 'js-hours',
                                    ]); ?>
                                    <span><?= t('app', 'Minutes') ?>:</span>
                                    <?= Html::activeTextInput($order, 'order_minutes', [
                                        'class' => 'js-minutes',
                                    ]); ?>
                                    <?= Html::activeHiddenInput($order, 'order_seconds', [
                                        'class' => 'js-seconds',
                                    ]); ?>
                                </div>
                                <div class="submit_date_order">
                                    <button type="button" class="js-choose-date"><?= t('app', 'Confirm') ?></button>
                                    <button type="button" class="js-choose-now"
                                            data-title="<?= t('order', 'now') ?>"><?= t('app', 'Now') ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="margin-bottom: 15px" class="current-status">
                        <b><?= Html::encode($order->getAttributeLabel('status_id')); ?></b>
                        <p><?= Html::encode(OrderStatusService::translate($order->status_id,
                                $order->position_id)) ?></p>
                        <? if (in_array($order->status_id, OrderStatus::getCountdownSatusList())): ?>
                            <?php
                            $time = in_array($order->status_id,
                                OrderStatus::getNewStatusId()) ? $order->order_time : $order->getOrderTimestamp($order->status_time);
                            if (($order->status_id == OrderStatus::STATUS_GET_WORKER || $order->status_id == OrderStatus::STATUS_EXECUTION_PRE) && !empty($order->time_to_client)):
                                $time += $order->time_to_client * 60;
                                ?>
                                <span class="ows_time"><?= Html::encode($order->time_to_client) . ' ' . t('app',
                                        'min.') ?></span>
                            <? elseif ($order->status_id == OrderStatus::STATUS_WORKER_WAITING):
                                $time += $order->getWorkerWaitingTime();
                                ?>
                            <? endif ?>
                            <?php
                            $timeToOrder            = $order->getTimerData($time);
                            $timer_class            = $timeToOrder['TYPE'] == 'down' ? 'ot_green' : 'ot_red';
                            $timeToOrder['MINUTES'] = $timeToOrder['TYPE'] == 'down' ? $timeToOrder['MINUTES'] : '-' . $timeToOrder['MINUTES'];
                            ?>
                            <span class="<?= $timer_class ?> timer" data-type="<?= $timeToOrder['TYPE'] ?>"><b><span
                                            class="minutes"><?= Html::encode($timeToOrder['MINUTES']); ?></span><span
                                            class="time_colon">:</span><span
                                            class="seconds"><?= Html::encode($timeToOrder['SECONDS']); ?></span></b></span>
                        <? endif ?>
                    </div>
                </div>
                <div class="order-window__people">
                    <div class="order-window__client">
                        <div style="position: relative; z-index: 9998;">
                            <a class="title active" data-tab="normal"><?= t('order', 'Client') ?></a>
                            <?php if ($order->client_passenger_phone): ?>
                                <a class="title" data-tab="other"><?= t('order', 'Who\'s riding') ?></a>
                            <?php endif; ?>
                            <div class="order-window__client__input">
                                <div class="js-normal-client-tab">
                                    <div class="line">
                                        <div class="photo"><i style="background-image: url('/images/no-photo.png');"></i></div>
                                        <div style="display: inline-block; width: 270px; vertical-align: top">
                                            <? $showAddClient = empty($order->client_id) && !empty($order->phone) ?>
                                            <?= $this->render('_autocompleteClients', [
                                                'class'       => 'not_empty',
                                                'placeholder' => t('client', 'Search'),
                                                'style'       => $showAddClient ? 'display: none' : '',
                                            ]) ?>

                                            <a class="js-add-client add-client"><i></i><?= t('order', 'Add customer') ?></a>

                                            <div style="<?if(!$showAddClient):?>display: none;<?endif;?>" class="js-add-client-inputs">
                                                <a class="js-hide-add-client hide-add-client"></a>
                                                <?= Html::activeInput('tel', $order, 'phone', [
                                                    'class'     => 'mask_phone not_empty',
                                                    'data-lang' => $langCode,
                                                    'is-valid'  => $phone_valid ? 1 : 0,
                                                ]) ?>
                                                <div class="add-client-cols">
                                                    <div class="col">
                                                        <input type="text" placeholder="<?= t('order', 'Surname') ?>">
                                                    </div>
                                                    <div class="col">
                                                        <input type="text" placeholder="<?= t('order', 'Name') ?>">
                                                    </div>
                                                    <div class="col">
                                                        <input type="text" placeholder="<?= t('order', 'Middle name') ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="client-info" data-black-list-title="<?= t('client', 'Black list') ?>">
                                        <?= $this->render('_clientInfo', [
                                            'clientInfo' => $clientInfo,
                                            'canUpdate'  => true,
                                        ]) ?>
                                        <?= Html::activeHiddenInput($order, 'client_id') ?>
                                    </div>
                                </div>
                                <div class="js-other-client-tab" style="display: none;">
                                    <?php if ($order->client_passenger_phone): ?>
                                        <div class="line">
                                            <div class="photo">
                                                <i style="background-image: url(
                                                    <?= $order->clientPassenger
                                                        ->isFileExists($order->clientPassenger->photo)
                                                            ? $order->clientPassenger
                                                                ->getPicturePath($order->clientPassenger->photo)
                                                            : '/images/no-photo.png' ?>
                                                );"></i>
                                            </div>
                                            <div style="display: inline-block; width: 270px; vertical-align: top">
                                                <input name="Order[client_passenger_phone]"
                                                       type="tel" class="mask_phone"
                                                       data-lang="<?=$langCode;?>"
                                                       value="<?= $order->client_passenger_phone ?>"
                                                       readonly>
                                                <div class="add-client-cols">
                                                    <div class="col">
                                                        <input name="Order[client_passenger_lastname]"
                                                               type="text"
                                                               value="<?= $order->clientPassenger->last_name ?>"
                                                               readonly>
                                                    </div>
                                                    <div class="col">
                                                        <input name="Order[client_passenger_name]"
                                                               type="text"
                                                               value="<?= $order->clientPassenger->name ?>"
                                                               readonly>
                                                    </div>
                                                    <div class="col">
                                                        <input name="Order[client_passenger_secondname]"
                                                               type="text"
                                                               value="<?= $order->clientPassenger->second_name ?>"
                                                               readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div id="autocomplete-clients-content"></div>
                    </div>

                    <?php if ($order->device ==  Order::DEVICE_HOSPITAL):?>
                        <style type="text/css">

                            ul.orders_
                            {
                                display:flex;
                                list-style:none;
                            }
                            #autocomplete-company-content{
                                position: absolute;
                                left: 0;
                                top: -1px;
                                width: 100%;
                                background: #fff;
                                box-sizing: border-box;
                                border-radius: 1px;
                                padding: 120px 20px 12px 20px;
                                z-index: 9900;
                                border: 1px solid #e0e0e0;
                            }
                            #autocomplete-company-content > .ui-autocomplete {
                                position: static !important;
                                width: 100% !important;
                                -moz-border-radius: 0;
                                -webkit-border-radius: 0;
                                border-radius: 0;
                                -moz-box-shadow: none;
                                -webkit-box-shadow: none;
                                box-shadow: none;
                                background: none;
                                margin-bottom: 8px;
                            }


                            #wrapper{
                                margin-left:auto;
                                margin-right:auto;
                                height:auto;
                                width:auto;
                                padding: 0;
                            }
                            #inner1{
                                float:left;
                            }
                            #inner2{
                                float:left;
                                clear: left;
                            }

                        </style>

                        <div id="wrapper" class="order-window__driver selected" style="border-top: 1px solid #d7d7d7;">
                        <div id="inner1" class="tabs_links" style="margin-top: -12%; border-bottom: 1px solid #d7d7d7;">
                            <ul class="orders_">
                                <li class="active"><a class="sb01"><?=t('employee', 'Worker') ?></a></li>
                                <li ><a class="sb02"><?=t('car', 'Company') ?></a></li>
                            </ul>
                        </div>

                        <div  id="inner2" class="tabs_content">
                            <div class="active" id="sb01" style="padding-top: 0; margin-top: 5%">
                                <div style="position: relative; z-index: 9998;">
                                    <b class="title"><?= t('employee', 'Worker') ?></b>
                                    <div class="order-window__client__input">
                                        <div class="line">
                                            <div class="photo"><i style="background-image: url('/images/no-photo.png');"></i>
                                            </div>
                                            <div style="display: inline-block; width: 270px; vertical-align: top; position: relative;">
                                                <?= $this->render('_autocompleteDrivers', [
                                                    'class'       => 'not_empty',
                                                    'placeholder' => t('employee', 'Search'),
                                                    'options'     => [
                                                        'disabled' => !in_array('worker_id', $availableAttributes, true),
                                                        'style'    => 'padding-right: 33px',
                                                    ],
                                                ]) ?>
                                                <a class="order-free-drivers <?if(in_array('worker_id', $availableAttributes, true)){echo 'active';}?> js-order-free-drivers"></a>
                                            </div>
                                        </div>
                                        <div class="client-info js-driver-info">
                                            <?= $this->render('_workerInfo', [
                                                'workerInfo'   => $workerInfo,
                                                'showDistance' => true,
                                                'canUpdate'    => in_array('worker_id', $availableAttributes, true),
                                                'show_phone'   => $show_phone,
                                            ]) ?>
                                            <?= Html::activeHiddenInput($order, 'worker_id') ?>
                                            <?= Html::activeHiddenInput($order, 'car_id') ?>
                                        </div>
                                        <div class="js-deny-refuse-order deny-refuse-order" style="display: none">
                                            <?= Html::activeCheckbox($order, 'deny_refuse_order', [
                                                'disabled' => !in_array('deny_refuse_order', $availableAttributes, true),
                                            ]) ?>
                                        </div>
                                    </div>
                                </div>
                                <div style="border: none" id="autocomplete-drivers-content"></div>

                            </div>
                            <div  id="sb02"  style="padding-top: 0; margin-top: 5%">
                                <div style="position: relative; z-index: 9998;">
                                    <b class="title"><?= t('employee', 'Company') ?></b>
                                    <div class="order-window__client__input">
                                        <div class="line">
                                            <div class="photo">
                                            </div>
                                            <div style="display: inline-block; width: 270px; vertical-align: top; position: relative;">
                                                <?= $this->render('_autocompleteCompanies', [
                                                    'class'       => 'not_empty',
                                                    'placeholder' => t('employee', 'Search'),
                                                    'options'     => [
                                                        'disabled' => !in_array('company_id', $availableAttributes, true),
                                                        'style'    => 'padding-right: 33px',
                                                    ],
                                                ]) ?>
                                                <a class="order-free-drivers <?if(in_array('company_id', $availableAttributes, true)){echo 'active';}?> js-order-free-company"></a>
                                            </div>
                                        </div>
                                        <div <?=  (!empty($companyInfo)) ? 'style="height: 120%"' : '';  ?>  class="client-info js-company-info">
                                            <?= $this->render('companyInfo', [
                                                'companyInfo'   => $companyInfo,
                                                'canUpdate'    => in_array('company_id', $availableAttributes, true),
                                                'show_phone'   => $show_phone,
                                            ]) ?>
                                            <?= Html::activeHiddenInput($order, 'company_id') ?>
                                        </div>
                                    </div>
                                </div>
                                <div style="border: none" id="autocomplete-company-content"></div>
                            </div>
                        </div>
                    </div>

                    <?else:?>

                        <?= Html::activeHiddenInput($order, 'company_id') ?>
                        <div class="order-window__driver">
                            <div style="position: relative; z-index: 9998;">
                                <b class="title"><?= t('employee', 'Worker') ?></b>
                                <div class="order-window__client__input">
                                    <div class="line">
                                        <div class="photo"><i style="background-image: url('/images/no-photo.png');"></i>
                                        </div>
                                        <div style="display: inline-block; width: 270px; vertical-align: top; position: relative;">
                                            <?= $this->render('_autocompleteDrivers', [
                                                'class'       => 'not_empty',
                                                'placeholder' => t('employee', 'Search'),
                                                'options'     => [
                                                    'disabled' => !in_array('worker_id', $availableAttributes, true),
                                                    'style'    => 'padding-right: 33px',
                                                ],
                                            ]) ?>
                                            <a class="order-free-drivers <?if(in_array('worker_id', $availableAttributes, true)){echo 'active';}?> js-order-free-drivers"></a>
                                        </div>
                                    </div>
                                    <div class="client-info js-driver-info">
                                        <?= $this->render('_workerInfo', [
                                            'workerInfo'   => $workerInfo,
                                            'showDistance' => true,
                                            'canUpdate'    => in_array('worker_id', $availableAttributes, true),
                                            'show_phone'   => $show_phone,
                                        ]) ?>
                                        <?= Html::activeHiddenInput($order, 'worker_id') ?>
                                        <?= Html::activeHiddenInput($order, 'car_id') ?>
                                    </div>
                                    <div class="js-deny-refuse-order deny-refuse-order" style="display: none">
                                        <?= Html::activeCheckbox($order, 'deny_refuse_order', [
                                            'disabled' => !in_array('deny_refuse_order', $availableAttributes, true),
                                        ]) ?>
                                    </div>
                                </div>
                            </div>
                            <div id="autocomplete-drivers-content"></div>
                        </div>
                    <?php endif;?>

                </div>

                <?= $this->render('_addressPattern', [
                    'curCity'                              => $curCity,
                    'curCityId'                            => $curCityId,
                    'parkingList'                          => $parkingList,
                    'history'                              => true,
                    'settingCurrentOrder'                  => $settingOrder,
                    'isHasSettingsCurrentOrderForCouriers' => $isHasSettingsForCouriers,
                ]) ?>

                <?= $this->render('_addressPoints', [
                    'order'           => $order,
                    'parkingList'     => $parkingList,
                    'disabledAddress' => !in_array('address', $availableAttributes, true) ? 'readonly' : '',
                ]); ?>

                <div class="order-window__comment">
                    <?= Html::activeTextarea($order, 'comment', [
                        'disabled'    => !in_array('comment', $availableAttributes, true),
                        'placeholder' => t('order', 'Comment'),
                    ]) ?>
                </div>
                <? if (empty($disabledAddress)): ?>
                    <div class="order-window__points-actions">
                        <a class="add-point js-add-order-point"></a>
                        <a class="js-show-map show-on-map"><?= t('order', 'Show map'); ?></a>
                    </div>
                    <div class="order-window__map">
                        <div class="js-order-map" style="display: none">
                            <div class="order-window__map-header">
                                    <ul>
                                        <li><label><input name="map_filter[]" type="checkbox" value="free_cars" checked="checked" > <?= t('order', 'Free cars') ?></label></li>
                                        <li><label><input name="map_filter[]" type="checkbox" value="busy_cars" checked="checked" > <?= t('order', 'Busy cars') ?></label></li>
                                        <li><label><input name="map_filter[]" type="checkbox" value="pause_cars" checked="checked" > <?= t('order', 'Pause cars') ?></label></li>
                                    </ul>
                            </div>
                        </div>
                    </div>
                <? endif; ?>
                <div class="order-window__cols">
                    <div class="col">
                        <?= $this->render('_payment', [
                            'form'            => $form,
                            'order'           => $order,
                            'payments'        => isset($clientInfo['payments']) ? $clientInfo['payments'] : null,
                            'showCardPayment' => !empty($order->cardPayment),
                            'canUpdate'       => in_array('payment', $availableAttributes, true),
                        ]) ?>

                        <?
                        $order->bonus_payment = $canBonusPayment && $order->bonus_payment;
                        $hidden               = $order->payment === Order::PAYMENT_CORP || !$canBonusPayment;
                        ?>
                        <div class="bonus_payment" style="<?= $hidden ? 'display: none' : '' ?>">
                            <?= Html::activeCheckbox($order, 'bonus_payment', [
                                'disabled' => !in_array('payment', $availableAttributes, true),
                            ]); ?>
                        </div>
                    </div>

                    <div class="col">
                        <b><?= t('order', 'Tariff and additional options') ?></b>
                        <?= $form->field($order, 'position_id')->begin(); ?>
                        <?= Html::activeDropDownList($order, 'position_id', $positions, [
                            'data-placeholder' => Html::encode(!empty($order->position_id) ? $positions[$order->position_id] : current($positions)),
                            'class'            => 'default_select not_empty',
                            'disabled'         => !in_array('position_id', $availableAttributes, true),
                        ]) ?>
                        <?= $form->field($order, 'position_id')->end(); ?>
                        <div class="order-window__tariff-field">
                            <?= $form->field($order, 'tariff_id')->begin(); ?>
                            <?
                            $tariffDescriptions = ArrayHelper::map($tariffs, 'tariff_id', 'description');
                            $tariffDescriptions = array_map(function ($item) {
                                return ['data-description' => $item];
                            }, $tariffDescriptions);
                            $tariffs            = ArrayHelper::map($tariffs, 'tariff_id', 'name');
                            echo Html::activeDropDownList($order, 'tariff_id', $tariffs, [
                                'data-empty'       => t('order', 'No tariffs'),
                                'data-placeholder' => Html::encode(!empty($order->tariff_id) ? $tariffs[$order->tariff_id] : current($tariffs)),
                                'class'            => 'default_select not_empty',
                                'disabled'         => !in_array('tariff_id', $availableAttributes, true),
                                'options'          => $tariffDescriptions,
                            ]) ?>
                            <?= $form->field($order, 'tariff_id')->end(); ?>
                            <a class="tooltip order-window-tariff-description">i
                                <span>
                                    <div class="order-window-tariff-description__content">
                                        <p class="js-order-tariff-description"><?= str_replace(PHP_EOL, '<br>',
                                                Html::encode($order->tariff->description)) ?></p>

                                    </div>
                                </span>
                            </a>
                        </div>

                        <ul class="comfort_list"
                            data-can-update="<?= in_array('additional_option', $availableAttributes, true) ? 1 : 0; ?>"
                            data-label-show-all="<?= t('order', 'Show all') ?>">
                            <?
                            $new_add_option = [];
                            $add_count      = 0;
                            $hidden_items   = false;
                            foreach ($dataForm['ADD_OPTIONS'] as $option):
                                $add_count++;
                                $checked = array_key_exists($option['additional_option_id'],
                                    $order->additional_option) && $option['tariff_id'] == $order->tariff_id ? true : false;
                                $hidden  = $option['tariff_id'] != $order->tariff_id ? 'style="display: none"' : '';
                                if ($checked) {
                                    $new_add_option[] = $option['additional_option_id'];
                                }
                                if ($add_count === 4) {
                                    echo '<a class="toggle-hidden-items">' . t('order',
                                            'Show all') . '</a><ul style="display: none;" class="hidden-items">';
                                    $hidden_items = true;
                                } ?>
                                <li <?= $hidden ?> data-tariff="<?= $option['tariff_id'] ?>">
                                    <label>
                                        <input
                                            <? if ($checked): ?>checked="checked"<? endif ?>
                                            name="Order[additional_option][]" type="checkbox"
                                            <?= in_array('additional_option', $availableAttributes,
                                                true) ? '' : 'disabled="disabled"'; ?>
                                            value="<?= $option['additional_option_id'] ?>"/>
                                        <?= Html::encode(t('car-options',
                                            $option['option']['name'])); ?>
                                    </label>
                                </li>
                            <? endforeach; ?>
                            <? if ($hidden_items) {
                                echo '</ul>';
                            } ?>
                        </ul>
                    </div>

                    <div class="col route-info">
                        <div id="routeAnalyzer">
                            <?php
                            $currencySymbol = getCurrencySymbol($order->city_id);
                            ?>
                            <b><?= t('order', 'Preliminary calculation') ?></b>
                            <div style="overflow: hidden">
                                <?= Html::activeTextInput($order, 'predv_price', [
                                    'data'     => [
                                        'can-update' => in_array('payment', $availableAttributes, true) ? 1 : 0,
                                    ],
                                    'readonly' => !((int)$order->is_fix === 1
                                        && in_array('payment', $availableAttributes, true)),
                                ]) ?>
                                <span class="currency_symbol"><?= Html::encode($currencySymbol); ?></span>

                                <div style="overflow: hidden;" class="route-info__price">
                                    <span style="white-space: nowrap">
                                        <span class="distance"><?= $order->predv_distance ? Html::encode($order->predv_distance) : 0 ?></span>
                                        <span><?= t('app', 'km') ?></span>
                                    </span>
                                    <?= Html::activeHiddenInput($order, 'predv_distance') ?>
                                    <span style="white-space: nowrap">
                                        <span class="time"><?= $order->predv_time ? Html::encode($order->predv_time) : 0 ?></span>
                                        <span><?= t('app', 'min.') ?></span>
                                    </span>
                                    <?= Html::activeHiddenInput($order, 'predv_time') ?>
                                </div>
                                <? if ($order->predv_price_no_discount && $order->predv_price_no_discount != $order->predv_price): ?>
                                    <div class="route-info_no_discount">
                                        <span class="costNoDiscount"><s><?= $order->predv_price_no_discount ?></s></span>
                                        <span class="currency_symbol"><?= Html::encode(getCurrencySymbol($curCityId)); ?></span>
                                    </div>
                                <? endif; ?>
                            </div>
                            <?= Html::activeCheckbox($order, 'is_fix', [
                                'label'    => t('order', 'offer my price'),
                                'disabled' => !in_array('payment', $availableAttributes, true),
                            ]); ?>
                        </div>
                        <div class="js-summary-cost" style="overflow: hidden; display: none">
                            <b><?= Html::encode($order->getAttributeLabel('summary_cost')); ?></b>
                            <?= Html::activeTextInput($order, 'summary_cost', [
                                'disabled' => !in_array('payment', $availableAttributes, true),
                            ]) ?>
                            <span class="currency_symbol"><?= Html::encode($currencySymbol); ?></span>
                        </div>
                    </div>
                </div>
                <div class="order-window__submit">
                    <div class="order-actions">
                        <b><?= t('order', 'Action of the order') ?>:</b>
                        <div class="edit-order-actions">
                            <?= Html::activeDropDownList($order, 'orderAction', $orderActions,
                                ['class' => 'default_select', 'placeholder' => 'Выберите действие']) ?>

                            <?
                            $arRejectStatus = [];
                            foreach ($dataForm['STATUS'] as $status):
                                if ($status['status_group'] == OrderStatus::STATUS_GROUP_5) {
                                    $rejected_group = OrderStatus::getRejectedStatusGroup($status['status_id']);
                                    if ($rejected_group) {
                                        $arRejectStatus[$rejected_group][] = $status;
                                    }
                                }
                                ?>
                            <? endforeach; ?>
                            <?php array_multisort($arRejectStatus, SORT_ASC, SORT_STRING) ?>
                        </div>
                        <div id="rejected_reason" style="display: none">
                            <b><?= t('order', 'Reason') ?>:</b>
                            <select name="Order[status_reject]" class="default_select add_select"
                                    data-placeholder="<?= Html::encode(OrderStatusService::translateByName(OrderStatus::STATUS_REJECTED,
                                        $order->position_id)) ?>">
                                <?
                                $first_select = false;
                                foreach ($arRejectStatus as $rejected_group => $statuses):?>
                                    <option class="select_group"><?= Html::encode($rejected_group); ?></option>
                                    <? foreach ($statuses as $status):
                                        if ($status['status_id'] == OrderStatus::STATUS_NO_CARS_BY_TIMER) {
                                            continue;
                                        }
                                        if (!$first_select) {
                                            $first_select = true;
                                            $selected     = ' selected="selected"';
                                        } else {
                                            $selected = '';
                                        }
                                        ?>
                                        <option<?= $selected ?> value="<?= $status['status_id'] ?>">
                                            <?= Html::encode(OrderStatusService::translate($status['status_id'], $order->position_id)); ?>
                                        </option>
                                    <? endforeach; ?>
                                <? endforeach; ?>
                            </select>
                        </div>

                    </div>
                    <div class="order-buttons submit_form">
                        <? if ($this->context->id == 'operator'): ?>
                            <a href="<?= Url::to('/order/operator/') ?>" class="link_red">
                                <b><?= t('order', 'Back to list') ?></b>
                            </a>
                        <? endif ?>
                        <?= Html::activeHiddenInput($order, 'parking_id') ?>
                        <?= Html::activeHiddenInput($order, 'new_add_option',
                            ['value' => implode(',', $new_add_option)]) ?>
                        <?= Html::submitInput(t('app', 'Save'), ['class' => 'js-save-order']) ?>
                    </div>
                </div>
                <div class="order-window__errors">
                    <p id="order-error" style="color:red; display:none"/>
                    <div class="empty-inputs" style="display: none;">
                        <div class="empty-client" style="display: none;"><?= t('validator',
                                'The client is not selected') ?></div>
                        <div class="empty-point" style="display: none;"><?= t('validator',
                                'The address point "1" is not selected') ?></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="b02" style="padding: 25px 32px;"></div>
        <div id="b03" style="padding: 25px 32px;"></div>
        <!--<div id="b04">Биржа</div>-->
    </div>
    <?php ActiveForm::end(); ?>
</div>
<div style="display: none;" id="order-id" data-order_id="<?= $order->order_id ?>"></div>