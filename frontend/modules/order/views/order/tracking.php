<?php
/* @var $this yii\web\View */

\frontend\assets\LeafletAsset::register($this);
$bundle = \app\modules\order\assets\OrderAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/map/main.js');
$this->registerJs("$(init_map_data('$map_data_type'));");
if ($isClient) {
    $controller = '/client/cabinet/';
} else {
    $controller = '/order/';
}
?>
<div id="map" style="height: 500px" data-order="<?= get('order_id') ?>" data-controller="<?= $controller; ?>">
    <p class="js-empty-map" style="display:none"><?= t('order', 'No data for display'); ?></p>
</div>
<div id="map_legend" style="display:none">
    <ul class="order_map_legend">
        <li><i>1</i> - <?= t('order', 'Get') ?></li>
        <li><i>2</i> - <?= t('order', 'Arrived') ?></li>
        <li><i>3</i> - <?= t('order', 'Execution') ?></li>
        <li><i>4</i> - <?= t('order', 'Completed') ?></li>
    </ul>
</div>
