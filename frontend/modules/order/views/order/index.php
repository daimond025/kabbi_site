<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\tenant\models\User;
use \yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $order app\modules\order\models\Order */
/* @var $formFilter \app\modules\order\models\forms\FilterMapForm */
/* @var $dataForFilter \frontend\modules\order\components\datacentr\DataForFilterMap */
/* @var $orderSearchByString app\modules\order\models\OrderSearchByString */
/* @var $ordersCount array */
/* @var $tariffMap array */
/* @var $optionMap array */

$this->title = Yii::t('order', 'Orders');

$bundle_order = \app\modules\order\assets\OrderAsset::register($this);
$this->registerJsFile($bundle_order->baseUrl . '/Timer.js');
$this->registerJsFile($bundle_order->baseUrl . '/functions.js');
$this->registerJsFile($bundle_order->baseUrl . '/orderDatepicker.js');
$this->registerJsFile($bundle_order->baseUrl . '/map/orderMap.js');
$this->registerJsFile($bundle_order->baseUrl . '/main.js');
$this->registerJsFile($bundle_order->baseUrl . '/bonusPayment.js');
$this->registerJsFile($bundle_order->baseUrl . '/refresh.js');
$this->registerJs('if (window.qt_handler !== undefined) { window.qt_handler.loadFinished(true); }');

$controller = $this->context->id;

// права доступа
$user_edit = (app()->user->can('orders')) ? true : false;
$user_staff = (app()->user->can(User::ROLE_STAFF) || app()->user->can(User::ROLE_STAFF_COMPANY)) ? true : false;

if ($controller == 'order') {
    $this->registerJs('colorbox_init();');
}
?>
<?php
if (app()->user->can('orders')) {
    echo Html::a(
        t('app', 'Add'),
        ['create', 'city_id' => get('city_id')],
        ['class' => 'button add_order', 'style' => 'float: right; position: relative; top: 5px;']
    );
} ?>
<div class="order_status">
    <b><span><?= Html::encode($workers['free'] + $workers['busy']); ?></span> <?= t('app', 'Cars in work') ?></b>
    <div data-href="<?= Url::to([
        '/order/order/get-workers-on-parkings',
        'status'  => 'free',
        'city_id' => get('city_id'),
    ]) ?>" class="s_green"><span><i></i> <?= Html::encode($workers['free']); ?></span>
        <div style="display: none;"></div>
    </div>
    <div data-href="<?= Url::to([
        '/order/order/get-workers-on-parkings',
        'status'  => 'busy',
        'city_id' => get('city_id'),
    ]) ?>" class="s_red"><span><i></i> <?= Html::encode($workers['busy']); ?></span>
        <div style="display: none;"></div>
    </div>
</div>
<a class="open_map_win" href="#map_win" data-module="/order/"><?= t('order', 'Map') ?></a>
<div class="filter_header">
    <div class="fh_content">
        <?php
        $date = get('date');
        $timestamp = strtotime($date);
        ?>
        <h1><?= t('order', 'Orders') ?></h1>
        <a class="ordered_when a_sc">
            <?= is_null($date) || strtotime(date('d.m.Y')) == $timestamp
                ? t('order', 'today') : app()->formatter->asDate($timestamp, 'shortDate'); ?>
        </a>
        <div class="s_datepicker_content b_sc sdpc_f">
            <div class="sdp_f">
                <input class="date_input" type="hidden" value="<?= $date ?>">
            </div>
        </div>
    </div>
</div>
<?php if (count($user_city_list) > 1): ?>
    <div class="order_cities">
        <ul>
            <li>
                <a href="<?= Url::to([
                    "/order/$controller/index",
                    'date' => $timestamp ? date('Y-m-d', $timestamp) : null,
                ]) ?>" <?php if (is_null(get('city_id'))): ?>class="active"<?php endif ?>>
                    <span><?= t('order', 'All cities') ?></span>
                </a>
            </li>
            <?php
            foreach ($user_city_list as $city):
                $time = time() + $city['republic']['timezone'] * 3600;
                ?>
                <li>
                    <a <?php if (get('city_id') == $city['city_id']): ?>class="active" <?php endif ?>href="<?= Url::to([
                        "/order/$controller/index",
                        'city_id' => $city['city_id'],
                        'date'    => $timestamp ? date('Y-m-d', $timestamp) : null,
                    ]) ?>">
                        <span><?= Html::encode($city['name' . getLanguagePrefix()]); ?></span>
                        <br/>
                        <span class="hours"><?= date('H', $time) ?></span><span class="time_colon">:</span><span
                                class="minutes"><?= date('i', $time) ?></span><span class="seconds hide"><?= date('s',
                                $time) ?></span>
                    </a>
                </li>
            <?php endforeach ?>
        </ul>
    </div>
<?php endif ?>

<?php

$form = ActiveForm::begin();

echo $form->field(
    $orderSearchByString,
    'search_string'
)
    ->textInput([
        'placeholder' => t('orders', 'Search by phone and order'),
        'style'       => 'width:350px',
        'class'       => 'order_search_string',
        'data-url'    => Url::to([
            '/order/get-all',
            'type'    => \app\modules\order\models\OrderStatus::STATUS_GROUP_0,
            'city_id' => get('city_id'),
            'date'    => get('date'),
        ]),
    ])
    ->label(false);

ActiveForm::end();
?>

<div class="tabs_links tnums">
    <ul>
        <?php if ($user_edit || $user_staff ): ?>
            <li data-group="new" class><a href="#new" data-ajax="<?= Url::to([
                    "/order/$controller/get-new",
                    'city_id' => get('city_id'),
                    'date'    => $date,
                ]) ?>" class="t01"><?= t('order', 'New') ?>
                    <sup><?= $ordersCount['new'] > 0 ? $ordersCount['new'] : '' ?></sup></a></li>
        <?php endif; ?>

        <li data-group="works" class><a href="#works" data-ajax="<?= Url::to([
                "/order/$controller/get-works",
                'city_id' => get('city_id'),
                'date'    => $date,
            ]) ?>" class="t02"><?= t('order', 'On the go') ?>
                <sup><?= $ordersCount['works'] > 0 ? $ordersCount['works'] : '' ?></sup></a></li>

        <?php if ($user_edit): ?>
            <li data-group="warning" class><a href="#warning" data-ajax="<?= Url::to([
                    "/order/$controller/get-warning",
                    'city_id' => get('city_id'),
                    'date'    => $date,
                ]) ?>" class="t03"><span class="link_red"><?= t('order', 'Warning') ?></span><sup
                            class="red_sup"><?= $ordersCount['warning'] > 0 ? $ordersCount['warning'] : '' ?></sup></a>
            </li>
        <?php endif; ?>

        <li data-group="pre_order" class><a href="#pre_order" data-ajax="<?= Url::to([
                "/order/$controller/get-pre-orders",
                'city_id' => get('city_id'),
                'date'    => $date,
            ]) ?>" class="t04"><?= t('order', 'Preliminary') ?>
                <sup><?= $ordersCount['pre_order'] > 0 ? $ordersCount['pre_order'] : '' ?></sup></a></li>
        <li data-group="completed" class><a href="#completed" data-ajax="<?= Url::to([
                "/order/$controller/get-completed",
                'city_id' => get('city_id'),
                'date'    => $date,
            ]) ?>" class="t05"><?= t('order', 'Compleated') ?></a></li>

        <?php if ($user_edit): ?>
            <li data-group="rejected" class><a href="#rejected" data-ajax="<?= Url::to([
                    "/order/$controller/get-rejected",
                    'city_id' => get('city_id'),
                    'date'    => $date,
                ]) ?>" class="t06"><span class="link_red"><?= t('order', 'Canceled') ?></span></a></li>
        <?php endif; ?>
    </ul>
</div>
<div class="tabs_content">
    <div id="t01">
        <?php if ($user_edit || $user_staff): ?>
            <?= $this->render('new', [
                    'orders'     => $orders,
                    'show_phone' => $show_phone,
                    'isClient'   => null,
                    'tariffMap'  => $tariffMap,
                    'optionMap'  => $optionMap,
            ]) ?>
        <?php endif; ?>
    </div>
    <div id="t02"></div>
    <div id="t03"></div>
    <div id="t04"></div>
    <div id="t05"></div>
    <div id="t06"></div>
</div>
<?= $this->render('/general-map/map', [
    'formFilter'    => $formFilter,
    'dataForFilter' => $dataForFilter,
    'isClient'      => null,
]) ?>

<audio class="audioSignalNewOrder" style="display:none">
    <source src="/audio/new_order.mp3" type="audio/mpeg">
</audio>
