<?php

use yii\helpers\Html;

?>

<ul>
    <?foreach($workers as $parking):?>
        <li>
            <span><?= Html::encode($parking['cnt']); ?></span>
            <b><?= Html::encode($parking['name']); ?></b>
        </li>
    <?endforeach;?>
</ul>
