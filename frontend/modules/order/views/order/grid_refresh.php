<?php

use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $dataProvider ActiveDataProvider */
/* @var $workers array */

$orders = $dataProvider->query->all();
$order_number = \yii\helpers\Json::encode(ArrayHelper::getColumn($orders, 'order_number'));
$Workers = \yii\helpers\Json::encode($workers);

$js = <<<JS
    orders_refresh = {$order_number};
    workers_refresh = {$Workers};
JS;
$this->registerJs($js, \yii\web\View::POS_READY);
?>

<div class="lsat">   </div>
