<?php

/* @var $value string */
/* @var $class string */
/* @var $options */
/* @var $isClient boolean */
/* @var $style string */

use yii\web\JsExpression;
use yii\jui\AutoComplete;

$bundle_order = \app\modules\order\assets\OrderAsset::register($this);
//$this->registerJsFile($bundle_order->baseUrl . '/autocomplete.js');

echo AutoComplete::widget([
    'id'            => rand(1000, 10000000),
    'name'          => 'autocomplete-clients',
    'value'         => $value,
    'clientOptions' => [
        'source'    => new JsExpression('autocompleteClients.source()'),
        'minLength' => 2,
        'appendTo'  => '#autocomplete-clients-content',
    ],
    'clientEvents'  => [
        'select' => 'autocompleteClients.select()',
        'search' => 'autocompleteClients.search()',
        'open'   => 'autocompleteClients.open()',
        'change' => 'autocompleteClients.change()',
        'create' => 'autocompleteClients.create()',
        'focus'  => 'autocompleteClients.focus()',
    ],
    'options'       => [
        'class'       => $class,
        'disabled'    => !empty($options['disabled']),
        'placeholder' => $placeholder,
        'style'       => $style,
    ],
]);