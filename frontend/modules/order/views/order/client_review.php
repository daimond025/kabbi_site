<?php

use yii\helpers\Html;

$this->title = t('order', 'Review');
?>
        <? if (!empty($review)): ?>
    <span class="rl_content">
        <span class="rl_stars">
            <? for ($i = 1; $i <= 5; $i++): ?>
                <? if ($i <= $review['rating']): ?>
                    <i class="active"></i>
                <? else: ?>
                    <i></i>
        <? endif ?>
    <? endfor; ?>
        </span>
        <i>«<?= Html::encode($review['text']); ?>»</i>
    </span>
<? else: ?>
    <p><?= t('app', 'Empty') ?></p>
<? endif; ?>