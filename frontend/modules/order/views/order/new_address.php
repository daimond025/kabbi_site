<?php
/* @var $this yii\web\View */
/* @var $disabled boolean */
/* @var $isClient */

$this->registerJs("select_init()");

?>

<div class="wlc_right <?= $disabledAddress ? 'js-disabled' : ''?>">
    <a class="close_red" style="display:none"></a>
    <b class="wlc_title"><?= t('order', 'Where') ?></b>
    <div class="form_line">
        <?
        $disabledAddress = empty($disabled) ? '' : 'disabled="disabled"';
        ?>
        <span class="od_d"><?= $char ?></span>
        <div class="fl_inp">
            <div class="fl_city">
                <label><?= t('order', 'Street or place') ?></label>
                <?= $this->render('_autocomplete', [
                    'options' => [
                        'disabled' => !empty($disabledAddress),
                    ],
                    'isClient' => (bool)$isClient
                ])?>
                <input data-type="city" autocomplete="off" name="Order[address][<?= $char ?>][city]"
                       data-city="" type="hidden" value="<?= $city_name ?>"/>
                <input type="hidden" class="js-cur-city_id" name="Order[address][<?= $char ?>][city_id]"
                       value="<?= $city_id ?>"/>
            </div>
            <div class="fl_street">
                <input data-type="street" autocomplete="off" name="Order[address][<?= $char ?>][street]"
                       type="hidden"/>
            </div>
            <div class="fl_mas">
                <div class="fl_minii">
                    <input data-type="house" name="Order[address][<?= $char ?>][house]" type="hidden"
                           autocomplete="off"/>
                </div>
                <div class="fl_minii">
                    <input data-type="housing" name="Order[address][<?= $char ?>][housing]" type="hidden"
                           autocomplete="off"/>
                </div>
                <div class="fl_minii">
                    <label><?= t('order', 'Porch') ?></label>
                    <input <?= $disabledAddress ?> name="Order[address][<?= $char ?>][porch]" type="text" autocomplete="off"/>
                </div>
                <div class="fl_minii">
                    <label><?= t('order', 'apt.') ?></label>
                    <input <?= $disabledAddress ?> name="Order[address][<?= $char ?>][apt]" type="text" autocomplete="off"/>
                </div>
            </div>
            <?= $this->render('_parking', [
                'currentChar' => $char,
                'options' => [
                    'disabled' => !empty($disabledAddress),
                ],
            ]); ?>
        </div>
        <a href="#" class="add_direction plus_icon" style="display:none"><i></i></a>
        <input class="lat not_empty" type="hidden" name="Order[address][<?= $char ?>][lat]" value=""
               data-error="<?= t('order', 'Coordinates of {point} point are not defined. Pick a point on the map or enter a valid address.') ?>">
        <input class="lon not_empty" type="hidden" name="Order[address][<?= $char ?>][lon]" value=""
               data-error="<?= t('order', 'Coordinates of {point} point are not defined. Pick a point on the map or enter a valid address.') ?>">
    </div>
</div>