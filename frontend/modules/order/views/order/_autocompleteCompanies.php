<?php

/* @var $value string */
/* @var $class string */
/* @var $options */
/* @var $isClient boolean */

use yii\web\JsExpression;
use yii\jui\AutoComplete;

$bundle_order = \app\modules\order\assets\OrderAsset::register($this);
//$this->registerJsFile($bundle_order->baseUrl . '/autocomplete.js');

echo AutoComplete::widget([
    'id'            => rand(1000, 10000000),
    'name'          => 'autocomplete-companies',
    'value'         => $value,
    'clientOptions' => [
        'source'    => new JsExpression('autocompleteCompany.source()'),
        'minLength' => 1,
        'appendTo'  => '#autocomplete-company-content',
    ],
    'clientEvents'  => [
        'select' => 'autocompleteCompany.select()',
        'search' => 'autocompleteCompany.search()',
        'open'   => 'autocompleteCompany.open()',
        'change' => 'autocompleteCompany.change()',
        'create' => 'autocompleteCompany.create()',
        'focus'  => 'autocompleteCompany.focus()'
    ],
    'options'       => [
        'class' => $class,
        'disabled' => !empty($options['disabled']),
        'placeholder' => $placeholder,
    ],
]);