<?php

/* @var string $curCity */
/* @var string $curCityId */
/* @var array $parkingList */

/* @var bool | null $isHasCommonSettingsForCouriers */
/* @var \app\modules\order\models\dto\CommonSettingsOrder | null $commonSetting */

/* @var bool | null $isHasSettingsCurrentOrderForCouriers */
/* @var \app\modules\order\models\dto\SettingExistOrderDto | null $settingCurrentOrder */

?>

<div id="order-point-pattern" style="display: none">
    <li class="order-window__points__item" data-point-id="0">
        <div class="order-window__points__item__content">
            <div class="line">
                <div class="number">
                    <i>A</i>
                </div>
                <div class="street">
                    <label><?= t('order', 'Street or place') ?></label>
                    <input type="text" class="new-autocomplete" name="autocomplete" autocomplete="off">
                    <a class="order-point-history js-address-history <? if (!empty($history)): ?>active<? endif; ?>"></a>
                    <input data-type="city" data-name="Order[address][{point}][city]" type="hidden"
                           value="<?= $curCity ?>"/>
                    <input type="hidden" class="js-cur-city_id" data-name="Order[address][{point}][city_id]"
                           value="<?= $curCityId ?>"/>
                    <input data-type="street" autocomplete="off" data-name="Order[address][{point}][street]"
                           type="hidden"/>
                    <input data-type="house" autocomplete="off" data-name="Order[address][{point}][house]"
                           type="hidden"/>

                    <input data-type="placeId" autocomplete="off" data-name="Order[address][{point}][placeId]"
                           type="hidden"/>

                    <input class="lat not_empty" type="hidden" data-name="Order[address][{point}][lat]" value=""
                           data-error="<?= t('order',
                               'Coordinates of {point} point are not defined. Pick a point on the map or enter a valid address.') ?>">
                    <input class="lon not_empty" type="hidden" data-name="Order[address][{point}][lon]" value=""
                           data-error="<?= t('order',
                               'Coordinates of {point} point are not defined. Pick a point on the map or enter a valid address.') ?>">
                </div>
                <div class="parking">
                    <?= $this->render('_parking', [
                        'parkingList' => $parkingList,
                    ]) ?>
                </div>
                <div class="house">
                    <label><?= t('order', 'b.') ?></label>
                    <input data-type="housing" data-name="Order[address][{point}][housing]" type="text"
                           autocomplete="off"/>
                </div>
                <div class="porch">
                    <label><?= t('order', 'Porch') ?></label>
                    <input data-name="Order[address][{point}][porch]" type="text" autocomplete="off"/>
                </div>
                <div class="apt">
                    <label><?= t('order', 'apt.') ?></label>
                    <input data-name="Order[address][{point}][apt]" type="text" autocomplete="off"/>
                </div>
            </div>
            <div class="line">
                <div class="number"></div>
                <div class="street comment">
                    <input data-name="Order[address][{point}][comment]"
                           type="text"
                           placeholder="<?= t('order', 'Comment') ?>">
                </div>
                <div class="phone">
                    <input data-name="Order[address][{point}][phone]"
                           type="text"
                           class="mask_phone_wo_placeholder"
                           maxlength=30
                           placeholder="<?= t('order', 'Tel. responsible') ?>">
                </div>
            </div>
        <div class="actions">
            <a class="delete-point"><?= t('app', 'Remove') ?></a>
        </div>
        <i class="toggle-actions"><i></i><i></i><i></i></i>
    </li>
</div>