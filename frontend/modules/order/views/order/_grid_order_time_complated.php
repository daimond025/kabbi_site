<?php

/* @var $order app\modules\order\models\Order */

use common\services\OrderStatusService;
use yii\helpers\Html; ?>
    <span class="ot_green"><b><?= Html::encode(OrderStatusService::translate($order->status_id,
                $order->position_id)); ?></b></span><br/>
<?= date('H:i', $order->status_time) ?>