<?php
/* @var $order app\modules\order\models\Order */
/* @var $isClient bool Если авторизован как клиент */

$controller = !empty($controller) ? $controller : $this->context->id;
?>

<? use yii\helpers\Url;

if (!$isClient) : ?>
    <a class="ot_edit" href="<?= Url::to(["/order/$controller/update", 'order_number' => $order->order_number]) ?>"></a>
<? endif; ?>
