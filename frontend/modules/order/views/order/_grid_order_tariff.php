<?php


/* @var $order app\modules\order\models\Order */


$tariff = $order->tariff;
$options = $order->options;

use yii\helpers\Html; ?>
<div class="order_table_client_block_big">
    <?= Html::encode($tariff['name']); ?>
</div>
<div class="order_table_client_block_small">
    <?php
    $options = isset($options) ? $options : [];
    foreach ($options as $option) : ?>
        <? $optionName = \common\helpers\ArrayHelper::getValue($option,'name', '' ); ?>
        <?= Html::encode(t('car-options', $optionName)) . '<br>' ?>
    <?php endforeach; ?>
</div>
<?= Html::encode($order->comment); ?>
