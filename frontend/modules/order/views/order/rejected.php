<?php
/* @var $this yii\web\View */
/* @var $order app\modules\order\models\Order */
/* @var $isClient bool Если авторизован как клиент */
/* @var $tariffMap array */
/* @var $optionMap array */

use app\modules\order\models\OrderStatus;
use common\services\OrderStatusService;
use yii\helpers\Url;
use yii\helpers\Html;

?>
<?if(!empty($orders)):?>
    <?$date = get('date')?>
    <ul data-url="<?=  Url::to(['order/sort', 'type' => OrderStatus::STATUS_GROUP_5, 'sourse' => 'db', 'city_id' => get('city_id')])?>" class="orders_filter <?if(!is_null($date) && $date != date('d.m.Y')):?>sortable<?else:?>redis_sort<?endif?>">
        <li><label><input value="0" type="checkbox" checked="checked" /> <?= t('order', 'All')?></label></li>
        <? $status_works = OrderStatus::getFilterStatusList(OrderStatus::STATUS_GROUP_5) ?>
        <? foreach ($status_works as $status_id => $status_name): ?>
            <li>
                <label>
                    <input name="status_filter" type="checkbox" value="<?= $status_id ?>"/>
                    <?= Html::encode(OrderStatusService::translate($status_id)
                        . ($status_id == OrderStatus::STATUS_NO_CARS_BY_TIMER ? ' (' . t('reports', 'by timer') . ')' : '')); ?>
                </label>
            </li>
        <? endforeach; ?>
    </ul>
    <table class="order_table <?if(!is_null($date) && $date != date('d.m.Y')):?>sortable<?else:?>redis_sort<?endif?>">
        <tbody>
            <tr>
                <th class="ot_01"><a data-column="0" href="<?=  Url::to(['order/sort', 'type' => OrderStatus::STATUS_GROUP_5, 'sourse' => 'db', 'city_id' => get('city_id'), 'order' => 'order_id'])?>">№ <span class="pt_down"></span></a>
                </th>
                <th class="ot_02"><a data-column="1" data-orderby="status_time" href="<?=  Url::to(['order/sort', 'sourse' => 'db', 'type' => OrderStatus::STATUS_GROUP_5, 'city_id' => get('city_id'), 'order' => 'status_time'])?>"><?= t('order', 'Status and time') ?> <span></span></a>
                </th>
                <th class="ot_03"> <?=t('order', 'From-To')?></th>
                <th class="ot_04"><?= t('order', 'Client') ?></th>
                <th class="ot_05"><?= t('order', 'Worker + car') ?></th>
                <th class="ot_06"><?= t('order', 'Tariff and additional options') ?></th>
                <th class="ot_07"></th>
            </tr>
            <?= $this->render('_rejected', [
                'orders'     => $orders,
                'isClient'   => $isClient,
                'tariffMap'  => $tariffMap,
                'optionMap'  => $optionMap,
                'show_phone' => $show_phone,
            ]) ?>
        </tbody>
    </table>
<?else:?>
    <p><?=t('app', 'Empty')?><p>
<?endif?>