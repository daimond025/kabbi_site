<?php

use yii\helpers\Url;
use yii\helpers\Html;
use frontend\modules\car\models\CarColor;
use app\modules\tenant\models\User;

/* @var $this yii\web\View */
/* @var $order app\modules\order\models\Order */
/* @var $isClient bool Если авторизован как клиент */
/* @var $tariffMap array */
/* @var $optionMap array */

$controller = !empty($controller) ? $controller : $this->context->id;

?>
<? if (!empty($orders)): ?>
    <? foreach ($orders as $key => $order):
        $client = $order->isActive ? $order->clientRedis : $order->client;
        $userCreated = $order->isActive ? $order->userCreatedRedis : $order->userCreated;
        $worker      = $order->isActive ? $order->workerRedis : $order->worker;
        $car         = $order->isActive ? $order->carRedis : $order->worker->shifts[0]->car;
        $car_color   = !empty($car['color']) ? t('car', $car['color']) : '';

        $hide = isset($order->hide_params_for_order) ? $order->hide_params_for_order : false;
        $user_staff = isset($order->user_staff) ? $order->user_staff : false;

        $order_company = (isset($order->order_company) && ($order->order_company == true))
            ? $order->order_company : false;
        ?>
        <tr id="pre_<?= $key ?>">
            <td data-sort="<?= $order->order_number ?>" class="ot_01">
                <? if ($isClient) : ?>
                    <a href="<?= yii\helpers\Url::to(['order/view', 'order_number' => $order->order_number]) ?>"
                       class="ot_open">
                        <b><?= $order->order_number ?></b>
                    </a>
                <? else : ?>
                    <a href="<?= yii\helpers\Url::to(['order/update', 'order_number' => $order->order_number]) ?>"
                       class="ot_open">
                        <b><?= $order->order_number ?></b>
                    </a>
                <? endif; ?>
            </td>
            <td data-sort="<?= $order->order_time ?>" class="ot_02">
                <span class="ot_time">
                    <?= date('d.m.Y H:i', $order->order_time) ?>
                </span>
            </td>
            <td class="ot_03">
                <?= $this->render('_address', ['address' => $order->address]) ?>
            </td>
            <td class="ot_04">
                <? if ($hide): ?>
                    <div class="order_table_client_block" style="visibility: hidden">
                        <div class="order_table_client_block_big">
                            <? if (!empty($client)): ?>
                                <? $fullName = getShortName($client['name'], $client['second_name']); ?>
                                <?
                                ?>
                                <?= Html::encode($fullName); ?>
                            <? else: ?>
                                <?= t('order', 'Client') ?>
                            <? endif ?>
                        </div>
                        <div class="order_table_client_block_small">
                            <?=  Html::encode(substr_replace($order->phone,'****',-4)); ?>
                        </div>
                    </div>
                    <div class="order_table_client_block">
                        <? if (!empty($order->user_create)): ?>
                            <div class="order_table_client_block_big">
                                <?= t('order', 'Dispatcher') ?>
                            </div>
                            <div class="order_table_client_block_small">
                                <?= Html::encode(getShortName($userCreated['last_name'], $userCreated['name'])); ?>
                            </div>
                        <? else: ?>
                            <div class="order_table_client_block_big">
                                <?= Html::encode($order->getDeviceName()); ?>
                            </div>
                        <? endif ?>
                    </div>
                <? elseif ($isClient || $order_company) : ?>
                    <div class="order_table_client_block">
                        <div class="order_table_client_block_big">
                            <? if (!empty($client) || $order_company): ?>
                                <? $fullName = getShortName($client['last_name'], $client['name'],
                                    $client['second_name']); ?>
                                <?= Html::encode($fullName); ?>
                            <? else: ?>
                                <?= t('order', 'Client') ?>
                            <? endif ?>
                        </div>
                        <div class="order_table_client_block_small">
                            <?= Html::encode($order->phone); ?>
                            <? if($isClient):?>
                                <a href="tel:+<?= $order->phone ?>" class="order_table_call"></a>
                            <?endif;?>
                        </div>
                    </div>
                    <div class="order_table_client_block">
                        <? if (!empty($order->user_create)): ?>
                            <div class="order_table_client_block_big">
                                <?= t('order', 'Dispatcher') ?>
                            </div>
                            <div class="order_table_client_block_small">
                                <?= Html::encode(getShortName($userCreated['last_name'], $userCreated['name'])); ?>
                            </div>
                        <? else: ?>
                            <div class="order_table_client_block_big">
                                <?= Html::encode($order->getDeviceName()); ?>
                            </div>
                        <? endif ?>
                    </div>
                <? else: ?>
                    <div class="order_table_client_block">
                        <div class="order_table_client_block_big">
                            <? if (!empty($client)): ?>
                                <? $fullName = getShortName($client['last_name'], $client['name'],
                                    $client['second_name']); ?>
                                <a href="<?= Url::to([
                                    '/client/base/update/',
                                    'id' => $order->client_id,
                                ]) ?>"><?= Html::encode($fullName); ?></a>
                            <? else: ?>
                                <? $fullName = getShortName($client['last_name'], $client['name'],
                                    $client['second_name']); ?>
                            <? endif ?>
                        </div>
                        <div class="order_table_client_block_small">
                            <a href="/client/base/update/<?= $order->client_id ?>"><?= Html::encode($order->phone); ?></a>
                            <a href="tel:+<?= $order->phone ?>" class="order_table_call"></a>
                        </div>
                    </div>
                    <div class="order_table_client_block">
                        <? if (!empty($order->user_create)): ?>
                            <div class="order_table_client_block_big">
                                <?= t('order', 'Dispatcher') ?>
                            </div>
                            <div class="order_table_client_block_small">
                                <a href="<?= Url::to([
                                    '/tenant/user/update',
                                    'id' => $order->user_create,
                                ]) ?>"><?= Html::encode(getShortName($userCreated['last_name'],
                                        $userCreated['name'])); ?></a>
                            </div>
                        <? else: ?>
                            <div class="order_table_client_block_big">
                                <?= Html::encode($order->getDeviceName()); ?>
                            </div>
                        <? endif ?>
                    </div>
                <? endif; ?>
            </td>
            <td class="ot_05">
                <? if ($isClient) : ?>
                    <? if (!empty($worker['worker_id'])): ?>
                        <div class="order_table_client_block">
                            <div class="order_table_client_block_big">
                                <?= Html::encode(getShortName($worker['last_name'], $worker['name'])); ?>
                            </div>
                            <div class="order_table_client_block_small">
                                <?= Html::encode($worker['callsign']); ?>
                            </div>
                        </div>
                        <? if (!empty($car) && !(empty($car['name']) && empty($car['gos_number']))): ?>
                            <div class="order_table_client_block">
                                <div class="order_table_client_block_big">
                                    <?= Html::encode($car['name'] . ', ' . CarColor::getColorText($car['color'])); ?>
                                </div>
                                <div class="order_table_client_block_small">
                                    <?= Html::tag('span', Html::encode($car['gos_number']), ['dir' => 'auto']); ?>
                                </div>
                            </div>
                        <? endif ?>
                    <? endif ?>
                <? else : ?>
                    <? if (!empty($worker['worker_id'])): ?>
                        <div class="order_table_client_block">
                            <div class="order_table_client_block_big">
                                <a href="<?= Url::to([
                                    '/employee/worker/update/',
                                    'id' => $worker['worker_id'],
                                ]) ?>"><?= Html::encode(getShortName($worker['last_name'],
                                        $worker['name'])); ?></a>
                                <?php if ($worker['phone'] !== '') { ?>
                                    <a href="tel:+<?= $worker['phone'] ?>" class="order_table_call"></a>
                                <?php } ?>
                            </div>
                            <div class="order_table_client_block_small">
                                <? if ($show_phone): ?>
                                    <a href="<?= Url::to([
                                        '/employee/worker/update/',
                                        'id' => $worker['worker_id'],
                                    ]) ?>">
                                        <?= Html::encode($worker['phone']); ?>
                                    </a><br>
                                <? endif; ?>
                                <?= Html::encode($worker['callsign']); ?>
                                <a class="pt_message" data-receivertype="worker"
                                   data-receiverid="<?= $order->worker->callsign ?>"
                                   data-cityid="<?= $order->city_id ?>">
                                </a>
                            </div>
                        </div>
                        <? if (!empty($car) && !(empty($car['name']) && empty($car['gos_number']))): ?>
                            <div class="order_table_client_block">
                                <div class="order_table_client_block_big">
                                    <a href="<?= Url::to(['/car/car/update/', 'id' => $car['car_id']]) ?>">
                                        <?= Html::encode($car['name'] . ', ' . $car_color); ?>
                                    </a>
                                </div>
                                <div class="order_table_client_block_small">
                                    <a href="<?= Url::to(['/car/car/update/', 'id' => $car['car_id']]) ?>">
                                        <?= Html::tag('span', Html::encode($car['gos_number']), ['dir' => 'auto']); ?>
                                    </a>
                                </div>
                            </div>
                        <? endif ?>
                    <? endif ?>
                <? endif; ?>
            </td>
            <td class="ot_06">
                <div class="order_table_client_block_big">
                    <?= Html::encode($tariffMap[$order->tariff_id]); ?>
                </div>
                <div class="order_table_client_block_small">
                    <?php
                    $options = isset($optionMap[$order->order_id]) ? $optionMap[$order->order_id] : [];
                    foreach ($options as $option) : ?>
                        <?= Html::encode(t('car-options', $option)) . '<br>' ?>
                    <?php endforeach; ?>
                </div>
                <?= Html::encode($order->comment); ?>
            </td>
            <td class="ot_07">
                <? if (!$isClient) : ?>
                    <a class="ot_edit"
                       href="<?= Url::to(["/order/$controller/update", 'order_number' => $order->order_number]) ?>"></a>
                <? endif; ?>
            </td>
        </tr>
    <? endforeach; ?>
<? else: ?>
    <tr>
        <td colspan="5"><?= t('app', 'Empty') ?></td>
    </tr>
<? endif ?>