<?php
/* @var $order app\modules\order\models\Order */
/* @var $parkingList array */

use yii\helpers\Html; ?>
<ul class="order-window__points">

    <li class="order-window__points__item" data-point-id="0">
        <div class="order-window__points__item__content">
            <div class="line">
                <div class="number">
                    <i>A</i>
                </div>

                <input data-type="city" data-name="Order[address][{point}][city]" name="Order[address][A][city]"
                       type="hidden"
                       value="<?= $order->address['A']['city'] ?>"/>
                <input type="hidden" class="js-cur-city_id" data-name="Order[address][{point}][city_id]"
                       name="Order[address][A][city_id]"
                       value="<?= $order->address['A']['city_id'] ?>"/>
                <input data-type="street" autocomplete="off" data-name="Order[address][{point}][street]"
                       name="Order[address][A][street]"
                       type="hidden" value="<?= $order->address['A']['street'] ?>"/>
                <input data-type="house" autocomplete="off" data-name="Order[address][{point}][house]"
                       name="Order[address][A][house]"
                       type="hidden" value="<?= $order->address['A']['house'] ?>"/>

                <input data-type="placeId" autocomplete="off" data-name="Order[address][{point}][placeId]"
                       name="Order[address][A][placeId]"
                       type="hidden" value="<?= $order->address['A']['placeId'] ?>"/>

                <input class="lat not_empty" type="hidden" data-name="Order[address][{point}][lat]"
                       name="Order[address][A][lat]" value="<?= $order->address['A']['lat'] ?>"
                       data-error="<?= t('order',
                           'Coordinates of {point} point are not defined. Pick a point on the map or enter a valid address.') ?>">
                <input class="lon not_empty" type="hidden" data-name="Order[address][{point}][lon]"
                       name="Order[address][A][lon]" value="<?= $order->address['A']['lon'] ?>"
                       data-error="<?= t('order',
                           'Coordinates of {point} point are not defined. Pick a point on the map or enter a valid address.') ?>">

                <div class="street">
                    <label><?= t('order', 'Street or place') ?></label>
                    <div class="disabled-row">
                        <?= Html::encode($order->getFormatStreet()); ?>
                    </div>
                </div>
                <div class="parking">
                    <? $parking = $order->address['A']['parking'];?>
                    <label><?= t('order', 'Parking') ?></label>
                    <div class="disabled-row"><?= !empty($parking) ? Html::encode($parking) : t('order', 'It is not defined') ?></div>
                </div>
                <div class="house">
                    <label><?= t('order', 'b.') ?></label>
                    <div class="disabled-row"><?= Html::encode($order->address['A']['housing']); ?></div>
                </div>
                <div class="porch">
                    <label><?= t('order', 'Porch') ?></label>
                    <div class="disabled-row"><?= Html::encode($order->address['A']['porch']); ?></div>
                </div>
                <div class="apt">
                    <label><?= t('order', 'apt.') ?></label>
                    <div class="disabled-row"><?= Html::encode($order->address['A']['apt']); ?></div>
                </div>
            </div>
            <div class="line">
                <div class="number"></div>
                <span class="comment" style="display: none">
                                        <div class="disabled-row street"><?= Html::encode($order->address['A']['comment']); ?></div>
                                    </span>
                <span class="phone" style="display: none">
                                        <input readonly
                                               type="text"
                                               value="<?= $order->address['A']['phone'] ?>" <?= $disabledAddress; ?>
                                               class="mask_phone input-view-mode"
                                               maxlength=30
                                               placeholder="+_(___)___-____">
                                    </span>
            </div>
        </div>
    </li>

    <?
    $cnt = count($order->address);
    if($cnt >= 1):
        $index = 0;
        foreach($order->address as $key => $item):
            $index++;
            if($key == 'A')
                continue;
            ?>

            <li class="order-window__points__item" data-point-id="<?= $index ?>">
                <div class="order-window__points__item__content">
                    <div class="line">
                        <div class="number">
                            <i><?= $key ?></i>
                        </div>

                        <input data-type="city" data-name="Order[address][{point}][city]"
                               name="Order[address][<?= $key ?>][city]" type="hidden"
                               value="<?= $order->address[$key]['city'] ?>"/>
                        <input type="hidden" class="js-cur-city_id" data-name="Order[address][{point}][city_id]"
                               name="Order[address][<?= $key ?>][city_id]"
                               value="<?= $order->address[$key]['city_id'] ?>"/>
                        <input data-type="street" autocomplete="off" data-name="Order[address][{point}][street]"
                               name="Order[address][<?= $key ?>][street]"
                               type="hidden" value="<?= $order->address[$key]['street'] ?>"/>
                        <input data-type="house" autocomplete="off" data-name="Order[address][{point}][house]"
                               name="Order[address][<?= $key ?>][house]"
                               type="hidden" value="<?= $order->address[$key]['house'] ?>"/>

                        <input data-type="placeId" autocomplete="off" data-name="Order[address][{point}][placeId]"
                               name="Order[address][B][placeId]"
                               type="hidden" value="<?= $order->address[$key]['placeId'] ?>"/>

                        <input class="lat not_empty" type="hidden" data-name="Order[address][{point}][lat]"
                               name="Order[address][<?= $key ?>][lat]" value="<?= $order->address[$key]['lat'] ?>"
                               data-error="<?= t('order',
                                   'Coordinates of {point} point are not defined. Pick a point on the map or enter a valid address.') ?>">
                        <input class="lon not_empty" type="hidden" data-name="Order[address][{point}][lon]"
                               name="Order[address][<?= $key ?>][lon]" value="<?= $order->address[$key]['lon'] ?>"
                               data-error="<?= t('order',
                                   'Coordinates of {point} point are not defined. Pick a point on the map or enter a valid address.') ?>">

                        <div class="street">
                            <label><?= t('order', 'Street or place') ?></label>
                            <div class="disabled-row">
                                <?= Html::encode($order->getFormatStreet($key)); ?>
                            </div>
                        </div>
                        <div class="parking">
                            <? $parking = $order->address[$key]['parking'];?>
                            <label><?= t('order', 'Parking') ?></label>
                            <div class="disabled-row"><?= !empty($parking) ? Html::encode($parking) : t('order', 'It is not defined') ?></div>
                        </div>
                        <div class="house">
                            <label><?= t('order', 'b.') ?></label>
                            <div class="disabled-row"><?= Html::encode($order->address[$key]['housing']); ?></div>
                        </div>
                        <div class="porch">
                            <label><?= t('order', 'Porch') ?></label>
                            <div class="disabled-row"><?= Html::encode($order->address[$key]['porch']); ?></div>
                        </div>
                        <div class="apt">
                            <label><?= t('order', 'apt.') ?></label>
                            <div class="disabled-row"><?= Html::encode($order->address[$key]['apt']); ?></div>
                        </div>
                    </div>
                    <div class="line">
                        <div class="number"></div>
                        <span class="comment" style="display: none">
                                                <div class="disabled-row street"><?= Html::encode($order->address[$key]['comment']); ?></div>
                                            </span>
                        <span class="phone" style="display: none">
                                                <input readonly
                                                       type="text"
                                                       value="<?= $order->address[$key]['phone'] ?>" <?= $disabledAddress; ?>
                                                       class="mask_phone input-view-mode"
                                                       maxlength=30
                                                       placeholder="+_(___)___-____">
                                            </span>
                        <span class="code" style="display: none">
                                                <div class="disabled-row code"><?= Html::encode(t('order', 'Code') . ': '
                                                        . $order->address[$key]['confirmation_code']); ?></div>
                                            </span>
                    </div>
                </div>
            </li>
        <?endforeach;?>
    <?endif?>
</ul>
