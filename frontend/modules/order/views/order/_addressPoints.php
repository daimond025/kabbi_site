<?php

use app\modules\order\models\Order;

/* @var $order Order */
/* @var $parkingList array */
/* @var $disabledAddress string */
/* @var $isCopyOrder bool | null */


$count = count($order->address);
?>


<ul class="order-window__points <?= $disabledAddress ?>" id="sortable" data-points="<?= $count ?>">

    <li class="order-window__points__item js-order-point" data-point-id="0">
        <div class="order-window__points__item__content">
            <div class="line">
                <div class="number">
                    <i>A</i>
                </div>
                <div class="street">
                    <label><?= t('order', 'Street or place') ?></label>
                    <?
                    $required = $order->device !== Order::DEVICE_WORKER;
                    echo $this->render('_autocomplete', [
                        'class'   => $required ? 'not_empty' : '',
                        'value'   => $order->getFormatStreet(),
                        'options' => [
                            'disabled' => !empty($disabledAddress),
                        ],
                    ]);

                    ?>
                    <a class="order-point-history js-address-history <?if(!empty($order->getFormatStreet()) && empty($disabledAddress)):?>active<?endif;?>"></a>
                    <input data-type="city" data-name="Order[address][{point}][city]" name="Order[address][A][city]"
                           type="hidden"
                           value="<?= $order->address['A']['city'] ?>"/>
                    <input type="hidden" class="js-cur-city_id" data-name="Order[address][{point}][city_id]"
                           name="Order[address][A][city_id]"
                           value="<?= $order->address['A']['city_id'] ?>"/>
                    <input data-type="street" autocomplete="off" data-name="Order[address][{point}][street]"
                           name="Order[address][A][street]"
                           type="hidden" value="<?= $order->address['A']['street'] ?>"/>
                    <input data-type="house" autocomplete="off" data-name="Order[address][{point}][house]"
                           name="Order[address][A][house]"
                           type="hidden" value="<?= $order->address['A']['house'] ?>"/>

                    <input data-type="placeId" autocomplete="off" data-name="Order[address][{point}][placeId]"
                           name="Order[address][A][placeId]"
                           type="hidden" value="<?= $order->address['A']['placeId'] ?>"/>

                    <input class="lat not_empty" type="hidden" data-name="Order[address][{point}][lat]"
                           name="Order[address][A][lat]" value="<?= $order->address['A']['lat'] ?>"
                           data-error="<?= t('order',
                               'Coordinates of {point} point are not defined. Pick a point on the map or enter a valid address.') ?>">
                    <input class="lon not_empty" type="hidden" data-name="Order[address][{point}][lon]"
                           name="Order[address][A][lon]" value="<?= $order->address['A']['lon'] ?>"
                           data-error="<?= t('order',
                               'Coordinates of {point} point are not defined. Pick a point on the map or enter a valid address.') ?>">

                   <? if(isset($order->address['A']['comment'])): ?>
                       <p class="AddressComment" style="margin-top: -5px"><?= $order->address['A']['comment'] ?> </p>
                    <? endif;?>

                </div>
                <div class="parking">
                    <?= $this->render('_parking', [
                        'currentChar'      => 'A',
                        'parkingList'      => $parkingList,
                        'currentParking'   => $order->address['A']['parking'],
                        'currentParkingId' => $order->address['A']['parking_id'],
                        'options'          => [
                            'disabled' => !empty($disabledAddress),
                        ],
                    ]); ?>
                </div>
                <div class="house">
                    <label><?= t('order', 'b.') ?></label>
                    <input data-type="housing" data-name="Order[address][{point}][housing]"
                           name="Order[address][A][housing]" type="text"
                           autocomplete="off" value="<?= $order->address['A']['housing'] ?>" <?= $disabledAddress; ?>/>
                </div>
                <div class="porch">
                    <label><?= t('order', 'Porch') ?></label>
                    <input data-name="Order[address][{point}][porch]" name="Order[address][A][porch]" type="text"
                           autocomplete="off" value="<?= $order->address['A']['porch'] ?>" <?= $disabledAddress; ?>/>
                </div>
                <div class="apt">
                    <label><?= t('order', 'apt.') ?></label>
                    <input data-name="Order[address][{point}][apt]" name="Order[address][A][apt]" type="text"
                           autocomplete="off" value="<?= $order->address['A']['apt'] ?>" <?= $disabledAddress; ?>/>
                </div>
            </div>
            <div class="line">
                <div class="number"></div>
                <div class="street comment">
                    <input data-name="Order[address][{point}][comment]"
                           name="Order[address][A][comment]"
                           type="text"
                           value="<?= $order->address['A']['comment'] ?>" <?= $disabledAddress; ?>
                           placeholder="<?= t('order', 'Comment') ?>">
                </div>
                <div class="phone">
                    <input data-name="Order[address][{point}][phone]"
                           name="Order[address][A][phone]"
                           type="text"
                           value="<?= $order->address['A']['phone'] ?>" <?= $disabledAddress; ?>
                           class="mask_phone_wo_placeholder"
                           maxlength=30
                           placeholder="<?= t('order', 'Tel. responsible') ?>">
                </div>
            </div>
        </div>
        <div class="actions">
            <a class="delete-point"><?= t('app', 'Remove') ?></a>
        </div>
        <i class="toggle-actions"><i></i><i></i><i></i></i>
    </li>

    <? if ($count > 1):
        $index = 0;
        foreach ($order->address as $key => $item):
            $index++;
            if ($key == 'A') {
                continue;
            }

            ?>

            <li class="order-window__points__item js-order-point" data-point-id="<?= $index ?>">
                <div class="order-window__points__item__content">
                    <div class="line">
                        <div class="number">
                            <i><?= $key ?></i>
                        </div>

                        <div class="street">
                            <label><?= t('order', 'Street or place') ?></label>
                            <?= $this->render('_autocomplete', [
                                'value'   => $order->getFormatStreet($key),
                                'options' => [
                                    'disabled' => !empty($disabledAddress),
                                ],
                            ]) ?>
                            <input data-type="city" data-name="Order[address][{point}][city]"
                                   name="Order[address][<?= $key ?>][city]" type="hidden"
                                   value="<?= $order->address[$key]['city'] ?>"/>
                            <input type="hidden" class="js-cur-city_id" data-name="Order[address][{point}][city_id]"
                                   name="Order[address][<?= $key ?>][city_id]"
                                   value="<?= $order->address[$key]['city_id'] ?>"/>
                            <input data-type="street" autocomplete="off" data-name="Order[address][{point}][street]"
                                   name="Order[address][<?= $key ?>][street]"
                                   type="hidden" value="<?= $order->address[$key]['street'] ?>"/>
                            <input data-type="house" autocomplete="off" data-name="Order[address][{point}][house]"
                                   name="Order[address][<?= $key ?>][house]"
                                   type="hidden" value="<?= $order->address[$key]['house'] ?>"/>

                            <input data-type="placeId" autocomplete="off" data-name="Order[address][{point}][placeId]"
                                   name="Order[address][B][placeId]"
                                   type="hidden" value="<?= $order->address[$key]['placeId'] ?>"/>

                            <input class="lat not_empty" type="hidden" data-name="Order[address][{point}][lat]"
                                   name="Order[address][<?= $key ?>][lat]" value="<?= $order->address[$key]['lat'] ?>"
                                   data-error="<?= t('order',
                                       'Coordinates of {point} point are not defined. Pick a point on the map or enter a valid address.') ?>">
                            <input class="lon not_empty" type="hidden" data-name="Order[address][{point}][lon]"
                                   name="Order[address][<?= $key ?>][lon]" value="<?= $order->address[$key]['lon'] ?>"
                                   data-error="<?= t('order',
                                       'Coordinates of {point} point are not defined. Pick a point on the map or enter a valid address.') ?>">

                            <? if(isset( $order->address[$key]['comment'])): ?>
                                <p class="AddressComment" style="margin-top: -5px"><?=  $order->address[$key]['comment'] ?> </p>

                            <? endif;?>
                        </div>
                        <div class="parking">
                            <?= $this->render('_parking', [
                                'currentChar'      => $key,
                                'parkingList'      => $parkingList,
                                'currentParking'   => $order->address[$key]['parking'],
                                'currentParkingId' => $order->address[$key]['parking_id'],
                                'options'          => [
                                    'disabled' => !empty($disabledAddress),
                                ],
                            ]); ?>
                        </div>
                        <div class="house">
                            <label><?= t('order', 'b.') ?></label>
                            <input data-type="housing" data-name="Order[address][{point}][housing]"
                                   name="Order[address][<?= $key ?>][housing]" type="text"
                                   autocomplete="off"
                                   value="<?= $order->address[$key]['housing'] ?>" <?= $disabledAddress; ?>/>
                        </div>
                        <div class="porch">
                            <label><?= t('order', 'Porch') ?></label>
                            <input data-name="Order[address][{point}][porch]" name="Order[address][<?= $key ?>][porch]"
                                   type="text"
                                   autocomplete="off"
                                   value="<?= $order->address[$key]['porch'] ?>" <?= $disabledAddress; ?>/>
                        </div>
                        <div class="apt">
                            <label><?= t('order', 'apt.') ?></label>
                            <input data-name="Order[address][{point}][apt]" name="Order[address][<?= $key ?>][apt]"
                                   type="text"
                                   autocomplete="off"
                                   value="<?= $order->address[$key]['apt'] ?>" <?= $disabledAddress; ?>/>
                        </div>
                    </div>
                    <div class="line">
                        <div class="number"></div>
                        <div class="street comment">
                            <input data-name="Order[address][{point}][comment]"
                                   name="Order[address][<?= $key ?>][comment]"
                                   type="text"
                                   value="<?= $order->address[$key]['comment'] ?>" <?= $disabledAddress; ?>
                                   placeholder="<?= t('order', 'Comment') ?>">
                        </div>
                        <div class="phone">
                            <input data-name="Order[address][{point}][phone]"
                                   name="Order[address][<?= $key ?>][phone]"
                                   type="text"
                                   value="<?= $order->address[$key]['phone'] ?>" <?= $disabledAddress; ?>
                                   class="mask_phone_wo_placeholder"
                                   maxlength=30
                                   placeholder="<?= t('order', 'Tel. responsible') ?>">
                        </div>
                        <div class="code">
                            <input data-name="Order[address][{point}][confirmation_code]"
                                   name="Order[address][<?= $key ?>][confirmation_code]"
                                   type="text"
                                   readonly
                                   value="<?= t('order', 'Code') . ': '
                                        . $order->address[$key]['confirmation_code'] ?>"/>
                        </div>
                    </div>
                </div>
                <div class="actions">
                    <a class="delete-point"><?= t('app', 'Remove') ?></a>
                </div>
                <i class="toggle-actions"><i></i><i></i><i></i></i>
            </li>
        <? endforeach; ?>
    <? endif ?>
</ul>

<script>
    $(document).ready(function() {
        let ul_object  = $('ul.order-window__points');
        let li_array = ul_object.children('li.order-window__points__item');

        li_array.each(function(i,elem) {
            let comment_address = $(elem).find("p.AddressComment");

            if (comment_address.length > 0) {
                $(elem).height(110);
            }
        });
    });
</script>
