<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

?>

<? if (!empty($workerInfo)): ?>
    <div id="driver">
        <div class="line">
            <div class="photo"><i
                        style="background-image: url(<?= empty($workerInfo['photo']) ? '/images/no-photo.png' : $workerInfo['photo'] ?>);"></i>
            </div>
            <div class="client-info-content">
                <? if ($showDistance && !empty($workerInfo['lat']) && !empty($workerInfo['lon'])): ?>
                    <div class="js-dist" data-lat="<?= $workerInfo['lat']; ?>"
                         data-lon="<?= $workerInfo['lon']; ?>"></div>
                <? endif; ?>
                <span class="name">
                <?php if (Yii::$app->controller->module->id !== 'cabinet'): ?>
                    <a
                        <? if (!empty($workerInfo['link'])): ?>
                            href="<?= $workerInfo['link'] ?>"
                        <? endif ?>
                            class="name" target="_blank">
                        <?= implode(',  ',
                            array_filter([$workerInfo['callsign'], $workerInfo['shortName'], $show_phone ? '<br>' . $workerInfo['phone'] : null], function ($value) {
                                return isset($value);
                            })) ?>
                    </a>
                    <a class="order_table_call" href="tel:+<?= ArrayHelper::getValue($workerInfo, 'phone') ?>"></a>
                <?php else: ?>
                    <?= implode(',  ',
                        array_filter([$workerInfo['callsign'], $workerInfo['shortName'], $show_phone ? '<br>' . $workerInfo['phone'] : null], function ($value) {
                            return isset($value);
                        })) ?>

                <?php endif; ?>
                </span>
                <a <? if (!$canUpdate || empty($workerInfo)): ?> style="display: none;" <? endif; ?>
                        class="js-change-driver change-client"></a>
                <div class="car">
                    <?
                    $car = empty($workerInfo['car']) ? [] : [
                        $workerInfo['car']['name'],
                        $workerInfo['car']['gosNumber'],
                        $workerInfo['car']['color'],
                    ];
                    echo implode(', ', array_filter($car));
                    ?>
                </div>
                <? if (!empty($workerInfo['rating'])): ?>
                    <div class="rating"><?= Html::encode($workerInfo['rating']); ?></div><? endif; ?>
                <?php if (Yii::$app->controller->module->id !== 'cabinet' && !empty($workerInfo['balance'])) : ?>
                    <div class="balance"><?= Html::encode($workerInfo['balance']); ?></div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<? else: ?>
    <div id="driver"></div>
<? endif ?>
