<?php


/* @var $isClient bool Если авторизован как клиент */

use app\modules\tenant\models\User;

/* @var $order app\modules\order\models\Order */


$edit = false;
$form = app()->user->can('orders') ? 'update' : 'view';
$user_staff =  (app()->user->can(User::ROLE_STAFF) || app()->user->can(User::ROLE_STAFF_COMPANY)) ? true : false;
if($form === 'update'){
    $edit = true;
}elseif(($form === 'view') && $user_staff){
    $edit = true;
}

?>
<? if ($isClient || !$edit ) : ?>
    <a href="<?= yii\helpers\Url::to(['order/view', 'order_number' => $order->order_number]) ?>"
       class="ot_open">
        <b><?= $order->order_number ?></b>
    </a>
<? else : ?>
    <a href="<?= yii\helpers\Url::to(['order/update', 'order_number' => $order->order_number]) ?>"
       class="ot_open">
        <b><?= $order->order_number ?></b>
    </a>
<? endif; ?>