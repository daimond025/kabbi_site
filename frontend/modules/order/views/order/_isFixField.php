<?php

use app\modules\order\models\Order;
use yii\helpers\Html;

/* @var $order Order */
/* @var $showField bool */

if ($showField) {
    echo Html::activeCheckbox($order, 'is_fix', [
        'label' => t('order', 'offer my price'),
    ]);
}