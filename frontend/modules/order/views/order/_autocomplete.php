<?php

/* @var $value string */
/* @var $class string */
/* @var $options */
/* @var $isClient boolean */

use yii\web\JsExpression;
use yii\jui\AutoComplete;

$bundle_order = \app\modules\order\assets\OrderAsset::register($this);
//$this->registerJsFile($bundle_order->baseUrl . '/autocomplete.js' );

echo AutoComplete::widget([
    'id'            => rand(1000, 10000000),
    'name'          => 'autocomplete',
    'value'         => $value,
    'clientOptions' => [
        'source'    => new JsExpression('autocomplete.source()'),
        'minLength' => 2,
    ],
    'clientEvents'  => [
        'select' => 'autocomplete.select()',
        'search' => 'autocomplete.search()',
        'open'   => 'autocomplete.open()',
        'change' => 'autocomplete.change()',
        'create' => 'autocomplete.create()',
        'focus'  => 'autocomplete.focus()',
    ],
    'options'       => [
        'class' => $class,
        'disabled' => !empty($options['disabled']),
    ],
]);