<?php

use app\modules\order\assets\OrderAsset;
use app\modules\order\assets\OrderAssignAsset;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $user_city_list array */
/* @var $tenantCompanyList array */

OrderAssignAsset::register($this);
$bundleOrder = $this->getAssetManager()->getBundle(OrderAsset::class);
$this->registerJsFile($bundleOrder->baseUrl . '/Timer.js');
$this->registerJsFile($bundleOrder->baseUrl . '/functions.js');
$this->registerJsFile($bundleOrder->baseUrl . '/orderDatepicker.js');
$this->registerJsFile($bundleOrder->baseUrl . '/map/orderMap.js');
//$this->registerJsFile($bundleOrder->baseUrl . '/main.js');
$this->registerJsFile($bundleOrder->baseUrl . '/bonusPayment.js');
$this->registerJs('if (window.qt_handler !== undefined) { window.qt_handler.loadFinished(true); }');
?>
<section class="kb-toolbar">
    <a id="kb_cancel" class="button kb-btn-dang" href="#"
       style="float: right; position: relative; top: 5px; margin-left: 5px;">
        <?= t('assign', 'Cancel') ?>
    </a>
    <a id="kb_revoke" class="button kb-btn-warn" href="#"
       style="float: right; position: relative; top: 5px; margin-left: 5px;"> <?= t('assign', 'Release') ?></a>
    <a id="kb_assign" class="button" href="#"
       style="float: right; position: relative; top: 5px;">  <?= t('assign', 'Assign') ?></a>

    <h1>
        <span> <?= t('assign', 'Assigning order executors') ?></span>
        <a id="kb_send_schedule_mails" class="button kb-btn-warn" href="#"> <?= t('assign', 'Send schedules') ?></a>
        <a id="kb_download_schedule" class="button kb-btn-warn"
           href=""><?= t('assign', 'Download the schedule for tomorrow') ?></a>
    </h1>

    <div class="filter">
        <div class="filter_item_1">
            <select id="kb_focus_city" class="default_select">
                <?
                $select = true;
                foreach ($user_city_list as $city) {
                    if (isset($city['city_id'])) {
                        echo Html::tag('option', Html::encode($city['name' . getLanguagePrefix()]), [
                            'value' => $city['city_id'],
                            'selected' => $select,
                        ]);
                        $select = false;
                    }

                }
                ?>
            </select>
        </div>
        <div class="filter_item_1">
            <select id="kb_focus_company" class="default_select">
                <?
                $select = true;
                foreach ($tenantCompanyList as $code => $company) {
                    if (is_int($code)) {
                        echo Html::tag('option', Html::encode($company), [
                            'value' => $code,
                            'selected' => $select,
                        ]);
                        $select = false;
                    }
                }
                ?>
            </select>
        </div>

        <div class="filter_item_1">
            <div class="cof_date">
                <div class="cof_date_first">
                    <div class="cof_date">
                        <a class="s_datepicker a_sc select"></a>
                        <div class="s_datepicker_content b_sc" style="display: none;">
                            <input type="text"
                                   id="kb_focus_from"
                                   class="sdp_input"
                                   data-sub-init="<?= date('d.m.Y', mktime(0, 0, 0, date('m'), date('d') + 1, date('Y'))) ?>"
                                   value=""/>
                            <div class="sdp_filter"></div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="cof_date_second">
                    <div class="cof_date">
                        <a class="s_datepicker a_sc select"></a>
                        <div class="s_datepicker_content b_sc" style="display: none;">
                            <input type="text"
                                   id="kb_focus_to"
                                   class="sdp_input"
                                   data-sub-init="<?= date('d.m.Y', mktime(0, 0, 0, date('m'), date('d') + 1, date('Y'))) ?>"
                                   value=""/>
                            <div class="sdp_filter"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>

<section id="order_asgmt" class="kb-order-asgmt">
    <div class="kb-body">
        <div class="kb-scol" data-role="open">
            <div class="kb-scol-head">  <?= t('assign', 'Free orders') ?> </div>
            <div class="kb-scol-body" data-role="cont"></div>
        </div>
        <div class="kb-scol" data-role="worker">
            <div class="kb-scol-head"> <?= t('employee', 'Workers') ?> </div>
            <div class="kb-scol-body" data-role="cont"></div>
        </div>
        <div class="kb-scol" data-role="own">
            <div class="kb-scol-head">
                <span> <?= t('assign', 'Worker orders') ?> </span>
                <div class="kb-checkbox" data-role="select-all">
                    <label>
                        <input type="checkbox" value="1"/>
                        <span> <?= t('assign', 'Free orders') ?> </span>
                    </label>
                </div>
            </div>
            <div class="kb-scol-body" data-role="cont"></div>
        </div>
    </div>
    <div class="kb-overlay" data-role="locker">
        <div class="kb-overlay-cont" data-role="locker-cont"></div>
    </div>
</section>