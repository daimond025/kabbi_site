<?php

use \app\modules\order\models\Order;
use \app\modules\tenant\models\User;
use \frontend\modules\companies\models\TenantCompany;
use yii\helpers\ArrayHelper;
use \yii\helpers\Html;
use \app\modules\reports\helpers\OrderReportExporterHelper;

/* @var $tenantCompany TenantCompany */
/* @var $user User */
/* @var $orders Order[] */
/* @var $from int */
/* @var $to int */

$iconPath = Yii::$app->basePath . '/web/images/kabbi/kabbi_icon.png';

if (!file_exists($iconPath)) {
    $iconPath = '';
}

$reportNo = $tenantCompany->tenant_company_id . $tenantCompany->user_contact . '00' . $tenantCompany->report_id;
?>
<html>
<head></head>
<body>
<htmlpageheader name="MyHeader1">
    <table style="width:100%; border-collapse: collapse;">
        <tr>
            <td style="width: 99%"></td>
            <td style="width: 1%; text-align: right;"><img src="<?= $iconPath ?>" /></td>
        </tr>
    </table>
</htmlpageheader>

<htmlpagefooter name="MyFooter1"></htmlpagefooter>

<sethtmlpageheader name="MyHeader1" value="on" show-this-page="1"/>
<sethtmlpagefooter name="MyFooter1" value="on"/>


<div style="font-family: Arial, Helvetica, sans-serif; padding-left: 0">
    <table style="width:100%;font-size: 14px; margin-top: 5px; border-collapse: collapse;">
        <tr>
            <th style="width: 30%; text-align: left; border-bottom: 1px solid #000;"> Leistungsbereich</th>
            <th style="width: 70%; text-align: left; border-bottom: 1px solid #000;"> Leistungserbringer</th>
        </tr>
        <tr>
            <td style="width: 30%;"><?= date('d.m.Y', $from) . ' - ' . date('d.m.Y', $to) ?> </td>
            <td style="width: 70%;"><?= $tenantCompany->name ?></td>
        </tr>
    </table>

    <table style="width:100%;font-size: 14px; margin-top: 15px; border-collapse: collapse;" repeat_header="1">
        <thead>
        <tr>
            <th style="border-bottom: 3px solid #000; text-align: center; max-width: 60px; vertical-align: bottom">
                Pos.
            </th>
            <th style="border-bottom: 3px solid #000; text-align: left; padding-left: 10px; vertical-align: bottom">
                Datum, Uhrzeit, Route
            </th>
            <th style="border-bottom: 3px solid #000; text-align: left; padding-left: 10px; vertical-align: bottom">
                <p>Arbeiter</p>
            </th>
            <th style="border-bottom: 3px solid #000; text-align: left; padding-left: 10px; vertical-align: bottom">
                Kunde
            </th>
        </tr>
        <tr>
            <th colspan="4" style="height: 8px"></th>
        </tr>
        </thead>
        <tbody>
        <?php
        /* @var $orders Order[] */
        $pos = 1;
        ?>
        <?php foreach ($orders as $order): ?>
            <tr>
                <td style="vertical-align: middle; text-align: center">
                    <?= $order->order_number ?>
                </td>
                <td style="padding-left: 10px;">
                    <?= date('d.m.Y, H:i', $order->order_time) . ' / ' . Html::encode($order->getSummaryDistance()) . ' km' ?>
                </td>
                <td style="padding-left: 10px;">
                    <b><?= Html::encode(getShortName($order->worker->last_name, $order->worker->name, $order->worker->second_name)) ?></b> / <?= Html::encode($order->worker->callsign) ?>
                </td>
                <td style="padding-left: 10px;">
                    <span><?= getShortName($order->client->last_name , $order->client->name) ?></span>
                    <?php if ($phone = ArrayHelper::getValue($order->client->clientPhone,'value', '')): ?>
                        <br />
                        <span><?= $phone ?> </span>
                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid #000;"></td>
                <td colspan="3" style=" text-align: left; vertical-align: top; padding-left: 10px; border-bottom: 1px solid #000; font-size: 12px">
                    <span><?= OrderReportExporterHelper::getAddressStart($order->address) . ' &minus; ' . OrderReportExporterHelper::getAddressEnd($order->address); ?></span>
                    <?php if (trim($order->comment)): ?>
                        <br /><span><?= $order->comment ?></span>
                    <?php endif; ?>
                </td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>
</div>
</body>
</html>
