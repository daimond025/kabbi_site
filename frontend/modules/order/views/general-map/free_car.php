<?php
/* @var $phone Worker phone */
/* @var $worker_name Worker name */
/* @var $callsign Worker callsign */
/* @var $city_id Worker city_id */
/* @var $worker_id Worker ID */
/* @var $car_id Car ID */
/* @var $car_name Car name */
/* @var $car_gos_number Car gos_number */
/* @var $isClient boolean если авторизован как клиент */

use yii\helpers\Url;
use yii\helpers\Html;

?>
<div class="win_map_content">
    <div class="wmc_win">
        <b><?= t('employee', 'Worker') ?></b>
        <div class="wmcw_buttons">
            <a href="call:+<?= $phone ?>" class="call_b"><span></span></a>
            <a class="chat_b pt_message" data-receivertype="worker" data-receiverid="<?= $callsign ?>" data-cityid="<?= $city_id ?>"><span></span></a>
        </div>
        <div class="wmcw_content">
            <? if(!$isClient): ?>
                <a href="<?= Url::to(['/employee/worker/update', 'id' => $worker_id]) ?>"><?= Html::encode($worker_name); ?></a>
            <? else :?>
                <?= Html::encode($worker_name); ?>
            <? endif; ?>
            <span><?= Html::encode($callsign); ?></span>
            <? if(!$isClient): ?>
                <a href="<?= Url::to(['/car/car/update', 'id' => $car_id]) ?>">
                    <?= Html::encode($car_name); ?><?= Html::tag('span', Html::encode($car_gos_number), ['dir' => 'auto']); ?>
                </a>
            <? else :?>
                <?= Html::encode($car_name); ?>
            <? endif; ?>
        </div>
    </div>
</div>