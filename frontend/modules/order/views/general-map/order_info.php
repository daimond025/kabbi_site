<?php
/* @var $phone Client phone */
/* @var $client_id Client ID */
/* @var $client_name Client name */
/* @var $device The device from which the order was created */
/* @var $isClient boolean если авторизован как клиент */

use app\modules\order\models\Order;
use yii\helpers\Url;
use yii\helpers\Html;

?>
<div class="win_map_content">
    <div class="wmc_win">
        <b><?= t('client', 'Client') ?></b>
        <div class="wmcw_buttons">
            <a href="call:+<?= $phone ?>" class="call_b"><span></span></a>
        </div>
        <div class="wmcw_content">
            <span><?= Html::encode($phone); ?></span>
                <? if(!$isClient) : ?>
                    <a href="<?= Url::to(['/client/base/update', 'id' => $client_id]) ?>"><?if (!empty($client_name)):?><?= Html::encode($client_name); ?><?else:?><?= t('order', 'Unknown') ?><?endif?></a>
                <? else : ?>
                    <?if (!empty($client_name)):?><?= Html::encode($client_name); ?><?else:?><?= t('order', 'Unknown') ?><?endif?>
                <? endif; ?>
            <span><i class="<?= Order::getDeviceIconCssClass($device) ?>"></i><?= Html::encode((new Order(['device' => $device]))->getDeviceName()); ?></span>
        </div>
    </div>
</div>