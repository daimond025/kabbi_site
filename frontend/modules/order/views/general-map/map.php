<?php

use frontend\widgets\filter\Filter;
use app\modules\tenant\models\User;

/* @var $formFilter \app\modules\order\models\forms\FilterMapForm */
/* @var $dataForFilter \frontend\modules\order\components\datacentr\DataForFilterMap */

\frontend\assets\LeafletAsset::register($this);
$bundle = \app\modules\order\assets\OrderAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/map/generalMap.js');
?>
<div style="display: none">
    <div id="map_win" style="height: 95%;">
        <div class="win_map_header">
            <h3><?= t('order', 'Map') ?></h3>
            <ul<?= ($isClient) ? ' style="display:none"' : '' ?>>
                <li><label><input name="map_filter[]" type="checkbox" value="orders_free" checked="checked"> <?= t('order',
                            'Orders free') ?></label></li>
                <li><label><input name="map_filter[]" type="checkbox" value="orders_pre" checked="checked"> <?= t('order',
                            'Pre-orders') ?></label></li>
                <li><label><input name="map_filter[]" type="checkbox" value="offer_cars"
                                  checked="checked"> <?= t('order', 'Offer cars') ?></label></li>
                <li><label><input name="map_filter[]" type="checkbox" value="free_cars"
                                  checked="checked"> <?= t('order', 'Free cars') ?></label></li>
                <li><label><input name="map_filter[]" type="checkbox" value="busy_cars"
                                  checked="checked"> <?= t('order', 'Busy cars') ?></label></li>
                <li><label><input name="map_filter[]" type="checkbox" value="pause_cars"
                                  checked="checked"> <?= t('order', 'Pause cars') ?></label></li>
            </ul>
            <?php
            if (!$isClient) {
                $filter = Filter::begin([
                    'id'       => '',
                    'pjaxId'   => true,
                    'model'    => $formFilter,
                    'onChange' => false,
                    'onSubmit' => true,
                    'options'  => [
                        'id'    => 'filter_map',
                        'class' => 'win_map_header__filter filter',
                    ]
                ]);
                echo $filter->checkboxList('cities', $dataForFilter->getCities(), [
                    'id' => 'filter_map__cities',
                ]);
                echo $filter->checkboxList('positions', $dataForFilter->getPositions(), [
                    'id' => 'filter_map__positions',
                ]);

                if (!app()->user->can(User::ROLE_STAFF_COMPANY)) {
                    echo $filter->checkboxList('groups', $dataForFilter->getGroups(), [
                        'id' => 'filter_map__groups',
                    ]);
                }

                if (!app()->user->can(User::ROLE_STAFF_COMPANY)) {
                    echo $filter->checkboxList('tenantCompanies', $dataForFilter->getTenantCompanies(), [
                        'id' => 'filter_map__tenant-companies',
                    ]);
                }

                echo $filter->checkboxList('classes', $dataForFilter->getCarClasses(), [
                    'id' => 'filter_map__classes',
                ]);
                echo $filter->input('searchLine', [
                    'id' => 'filter_map__searchLine',
                ]);
                Filter::end();
            }
            ?>
        </div>
        <div class="win_map_header__wrapper">
            <p style="display: none" data-empty="<?= t('order', 'No data for display'); ?>"></p>
            <div id="orders_map" style="height: 100%"></div>
        </div>
    </div>
</div>