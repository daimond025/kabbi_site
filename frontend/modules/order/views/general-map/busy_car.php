<?php
/* @var $worker_phone Driver phone */
/* @var $worker_name Driver name */
/* @var $callsign Driver callsign */
/* @var $city_id Driver city_id */
/* @var $worker_id Driver ID */
/* @var $car_id Car ID */
/* @var $car_name Car name */
/* @var $car_gos_number Car gos_number */
/* @var $client_phone Client phone */
/* @var $client_id Client ID */
/* @var $client_name Client name */
/* @var $device The device from which the order was created */
/* @var $isClient boolean */

use app\modules\order\models\Order;
use yii\helpers\Url;
use yii\helpers\Html;

?>
<div class="win_map_content">
    <div class="wmc_win">
        <b><?= t('employee', 'Worker') ?></b>
        <? if(!$isClient) : ?>
            <div class="wmcw_buttons">
                <a href="call:+<?= $worker_phone ?>" class="call_b"><span></span></a>
                <a class="chat_b pt_message" data-receivertype="worker" data-receiverid="<?= $callsign ?>"
                   data-cityid="<?= $city_id ?>"><span></span></a>
            </div>
        <? endif; ?>
        <div class="wmcw_content">
            <? if(!$isClient) : ?>
                <a href="<?= Url::to([
                    '/employee/worker/update',
                    'id' => $worker_id,
                ]) ?>"><?= Html::encode($worker_name); ?></a>
            <? else : ?>
                <?= Html::encode($worker_name); ?>
            <? endif; ?>
            <span><?= Html::encode($callsign) ?></span>
            <? if(!$isClient) : ?>
                <a href="<?= Url::to(['/car/car/update', 'id' => $car_id]) ?>">
                    <?= Html::encode($car_name); ?><?= Html::tag('span', Html::encode($car_gos_number), ['dir' => 'auto']); ?>
                </a>
            <? else : ?>
                <?= Html::encode($car_name); ?>
            <? endif; ?>
        </div>
        <? if ($client_id): ?>
            <hr>
            <b><?= t('client', 'Client') ?></b>
            <div class="wmcw_buttons">
                <a href="call:+<?= $client_phone ?>" class="call_b"><span></span></a>
            </div>
            <div class="wmcw_content">
                <span><?= Html::encode($client_phone); ?></span>
                <? if(!$isClient) : ?>
                    <a href="<?= Url::to([
                        '/client/base/update',
                        'id' => $client_id,
                    ]) ?>"><? if (!empty($client_name)): ?><?= Html::encode($client_name); ?><? else: ?><?= t('order',
                            'Unknown') ?><? endif ?></a>
                <? else : ?>
                    <? if (!empty($client_name)): ?><?= Html::encode($client_name); ?><? else: ?><?= t('order',
                            'Unknown') ?><? endif ?>
                <? endif; ?>
                <span><i
                        class="<?= Order::getDeviceIconCssClass($device) ?>"></i><?= Html::encode((new Order(['device' => $device]))->getDeviceName()); ?></span>
            </div>
        <? endif; ?>
    </div>
</div>