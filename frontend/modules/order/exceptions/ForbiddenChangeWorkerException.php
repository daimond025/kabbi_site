<?php

namespace frontend\modules\order\exceptions;

use yii\base\Exception;

class ForbiddenChangeWorkerException extends Exception
{
}