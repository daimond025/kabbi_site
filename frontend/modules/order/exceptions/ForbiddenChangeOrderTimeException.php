<?php

namespace frontend\modules\order\exceptions;

class ForbiddenChangeOrderTimeException extends \yii\base\Exception
{
}