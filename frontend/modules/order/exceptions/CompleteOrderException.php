<?php

namespace frontend\modules\order\exceptions;

/**
 * Class CompleteOrderException
 * @package frontend\modules\order\exceptions
 */
class CompleteOrderException extends \yii\base\Exception
{
}