<?php

namespace app\modules\order\exceptions;

use yii\base\Exception;

/**
 * Class InvalidAttributeValueException
 * @package app\modules\order\exceptions
 */
class InvalidAttributeValueException extends Exception
{

}