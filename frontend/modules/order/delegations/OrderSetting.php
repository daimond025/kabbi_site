<?php

namespace frontend\modules\order\delegations;

use app\modules\order\models\dto\CommonSettingsOrder;
use app\modules\order\models\dto\SettingExistOrderDto;
use app\modules\order\models\Order;
use common\modules\tenant\models\DefaultSettings;
use common\modules\tenant\models\TenantSetting;
use frontend\modules\order\repositories\OrderRepository;

class OrderSetting
{
    protected $order;

    /** @var OrderRepository */
    protected $orderRepository;

    public function __construct(Order $order)
    {
        $this->orderRepository = \Yii::createObject(OrderRepository::class);
        $this->order           = $order;
    }

    /**
     * @return null|SettingExistOrderDto
     */
    public function getSettingsExistOrder()
    {
        return $this->orderRepository->getSettingsExistOrder($this->order);
    }

    /**
     * @param $cityId
     * @param $positionId
     *
     * @return CommonSettingsOrder
     */
    public static function getCommonSettingsOrder($cityId, $positionId)
    {
        $requirePointConfirmationCode = self::getRequirePointConfirmationCode($cityId, $positionId);
        $orderFormAddPointPhone       = self::getOrderFormAddPointPhone($cityId, $positionId);
        $orderFormAddPointComment     = self::getOrderFormAddPointComment($cityId, $positionId);

        $orderSetting                               = new CommonSettingsOrder();
        $orderSetting->requirePointConfirmationCode = $requirePointConfirmationCode;
        $orderSetting->orderFormAddPointPhone       = $orderFormAddPointPhone;
        $orderSetting->orderFormAddPointComment     = $orderFormAddPointComment;

        return $orderSetting;
    }

    public static function getRequirePointConfirmationCode($cityId, $positionId)
    {
        $requirePointConfirmationCode = TenantSetting::getSettingValue(
            user()->tenant_id,
            DefaultSettings::SETTING_REQUIRE_POINT_CONFIRMATION_CODE,
            $cityId,
            $positionId
        );

        if (!isset($requirePointConfirmationCode)) {
            throw new \RuntimeException(
                sprintf(
                    'Отсутствует данные настоек: REQUIRE_POINT_CONFIRMATION_CODE tenantId: %s, cityId: %s, positionId: %s',
                    user()->tenant_id,
                    $cityId,
                    $positionId
                )
            );
        }

        return $requirePointConfirmationCode;
    }

    public static function getOrderFormAddPointPhone($cityId, $positionId)
    {
        $orderFormAddPointPhone = TenantSetting::getSettingValue(
            user()->tenant_id,
            DefaultSettings::SETTING_ORDER_FORM_ADD_POINT_PHONE,
            $cityId,
            $positionId
        );

        if (!isset($orderFormAddPointPhone)) {
            throw new \RuntimeException(
                sprintf(
                    'Отсутствует данные настоек: ORDER_FORM_ADD_POINT_PHONE tenantId: %s, cityId: %s, positionId: %s',
                    user()->tenant_id,
                    $cityId,
                    $positionId
                )
            );
        }

        return $orderFormAddPointPhone;
    }

    public static function getOrderFormAddPointComment($cityId, $positionId)
    {
        $orderFormAddPointComment = TenantSetting::getSettingValue(
            user()->tenant_id,
            DefaultSettings::SETTING_ORDER_FORM_ADD_POINT_COMMENT,
            $cityId,
            $positionId
        );

        if (!isset($orderFormAddPointComment)) {
            throw new \RuntimeException(
                sprintf(
                    'Отсутствует данные настоек: ORDER_FORM_ADD_POINT_COMMENT tenantId: %s, cityId: %s, positionId: %s',
                    user()->tenant_id,
                    $cityId,
                    $positionId
                )
            );
        }

        return $orderFormAddPointComment;
    }
}
