<?php

namespace app\modules\order\controllers;

use app\modules\order\models\forms\FilterMapForm;
use app\modules\order\services\CallsignService;
use app\modules\tenant\models\User;
use frontend\modules\companies\components\services\CompanyCheck;
use frontend\modules\employee\components\worker\WorkerShiftService;
use frontend\modules\order\components\services\filter\FilterOrderMapService;
use frontend\modules\order\components\services\filter\FilterWorkerMapService;
use Yii;
use yii\filters\AccessControl;
use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use yii\web\Controller;
use yii\web\Response;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class GeneralMapController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class'  => AccessControl::className(),
                'except' => [],
                'rules'  => [
                    [
                        'allow' => true,
                        'roles' => ['read_orders'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        return parent::beforeAction($action);
    }

    private function getActiveOrders()
    {
        return Yii::$app->redis_orders_active->executeCommand('hvals', [user()->tenant_id]);
    }

    private function getUserCityList()
    {
        $city_list = user()->getUserCityList();

        return array_keys($city_list);
    }

    /**
     * Free orders
     */
    public function actionGetOrdersFree()
    {
        $city_list = $this->getUserCityList();
        $orders    = $this->getActiveOrders();

        $post   = (new CompanyCheck())->workWithFilterMap(post());
        $orders = (new FilterOrderMapService($orders, new FilterMapForm($post)))->getFilterOrders();

        $city_id                  = post('city_id');
        $filterStatuses           = array_merge(OrderStatus::getNewStatusId(), OrderStatus::getPreOrderStatusId());
        $arResult['markers']      = [];
        $arResult['markers_keys'] = [];
        $arResult['type']         = 'order';
        
        foreach ($orders as $order) {
            $order            = unserialize($order);
            $order['address'] = unserialize($order['address']);

            //Фильтруем данные
            if (!in_array($order['city_id'], $city_list)
                || empty($order['address']['A']['lat'])
                || empty($order['address']['A']['lon'])
                || !in_array($order['status_id'], $filterStatuses)
                || $order['status']['status_group'] == OrderStatus::STATUS_GROUP_6
            ) {
                continue;
            }

            $marker_key_name                            = 'order_' . $order['order_id'];
            $arResult['markers'][]                      = [
                'key_name'    => $marker_key_name,
                'coords'      => [
                    'lat' => $order['address']['A']['lat'],
                    'lon' => $order['address']['A']['lon'],
                ],
                'marker_info' => $this->renderPartial('order_info', [
                    'client_id'   => $order['client_id'],
                    'client_name' => trim($order['client']['last_name'] . ' ' . $order['client']['name']),
                    'phone'       => $order['phone'],
                    'device'      => $order['device'],
                ]),
            ];
            $arResult['markers_keys'][$marker_key_name] = 1;
        }

        $this->createResponseData($arResult);
    }

    /**
     * Free orders
     */
    public function actionGetOrdersPre()
    {
        $city_list = $this->getUserCityList();
        $orders    = $this->getActiveOrders();

        $post   = (new CompanyCheck())->workWithFilterMap(post());
        $orders = (new FilterOrderMapService($orders, new FilterMapForm($post)))->getFilterOrders();

        $city_id                  = post('city_id');
        $filterStatuses           = array_merge(OrderStatus::getNewStatusId(), OrderStatus::getPreOrderStatusId());
        $arResult['markers']      = [];
        $arResult['markers_keys'] = [];
        $arResult['type']         = 'order';

        foreach ($orders as $order) {
            $order            = unserialize($order);
            $order['address'] = unserialize($order['address']);

            //Фильтруем данные
            if (!in_array($order['city_id'], $city_list)
                || empty($order['address']['A']['lat'])
                || empty($order['address']['A']['lon'])
                || !in_array($order['status_id'], $filterStatuses)
                || $order['status']['status_group'] != OrderStatus::STATUS_GROUP_6
            ) {
                continue;
            }

            $marker_key_name                            = 'order_' . $order['order_id'];
            $arResult['markers'][]                      = [
                'key_name'    => $marker_key_name,
                'coords'      => [
                    'lat' => $order['address']['A']['lat'],
                    'lon' => $order['address']['A']['lon'],
                ],
                'marker_info' => $this->renderPartial('order_info', [
                    'client_id'   => $order['client_id'],
                    'client_name' => trim($order['client']['last_name'] . ' ' . $order['client']['name']),
                    'phone'       => $order['phone'],
                    'device'      => $order['device'],
                ]),
            ];
            $arResult['markers_keys'][$marker_key_name] = 1;
        }

        $this->createResponseData($arResult);
    }

    private function getFilteredOnlineWorkers()
    {
        /** @var WorkerShiftService $workerShiftService */
        $workerShiftService = Yii::$app->getModule('employee')->get('workerShift');

        $workers = $workerShiftService->getOnlineWorkers();

        $post = (new CompanyCheck())->workWithFilterMap(post());

        return (new FilterWorkerMapService($workers, new FilterMapForm($post)))->getFilterWorkers();
    }

    /**
     * Free workers
     * @throws \yii\base\InvalidParamException
     */
    public function actionGetFreeCars()
    {
        $city_list                = $this->getUserCityList();
        $workers                  = $this->getFilteredOnlineWorkers();
        $city_id                  = post('city_id');
        $arResult['markers']      = [];
        $arResult['markers_keys'] = [];
        $arResult['type']         = 'free_car';

        foreach ($workers as $cab) {
            $cab   = unserialize($cab);
            $color = !empty($cab['car']['color']) ? t('car', $cab['car']['color']) : '';

            if (!in_array($cab['worker']['city_id'], $city_list)
                || $cab['worker']['status'] != 'FREE'
                || empty($cab['geo']['lat'] || empty($cab['geo']['lon']))
            ) {
                continue;
            }

            //assign value to callsign
            $callsign = CallsignService::getCallsign($cab);

            $marker_key_name                            = 'free_car_' . $cab['worker']['worker_id'];
            $arResult['markers'][]                      = [
                'key_name'    => $marker_key_name,
                'coords'      => [
                    'lat' => $cab['geo']['lat'],
                    'lon' => $cab['geo']['lon'],
                ],
                'marker_info' => $this->renderPartial('free_car', [
                    'worker_id'      => $cab['worker']['worker_id'],
                    'worker_name'    => trim($cab['worker']['last_name'] . ' ' . $cab['worker']['name']),
                    'phone'          => $cab['worker']['phone'],
                    'callsign'       => $cab['worker']['callsign'],
                    'city_id'        => $cab['worker']['city_id'],
                    'car_id'         => isset($cab['car']['car_id']) ? $cab['car']['car_id'] : null,
                    'car_name'       => implode(', ',
                        array_filter([isset($cab['car']['name']) ? trim($cab['car']['name']) : null, $color])),
                    'car_gos_number' => isset($cab['car']['gos_number']) ? $cab['car']['gos_number'] : null,
                ]),
                'iconAngle'   => $cab['geo']['degree'],
                'worker_id'   => $callsign ? $callsign : '',
                //                'iconAngle'   => 0,
                'position_id' => $cab['position']['position_id'],
            ];
            $arResult['markers_keys'][$marker_key_name] = $cab['position']['position_id'];
        }

        $this->createResponseData($arResult);
    }

    /**
     * Workers are working
     */
    public function actionGetBusyCars()
    {
        $city_list                = $this->getUserCityList();
        $workers                  = $this->getFilteredOnlineWorkers();
        $city_id                  = post('city_id');
        $arResult['markers']      = [];
        $arResult['markers_keys'] = [];
        $arResult['type']         = 'busy_car';
        $arBuzyDrieverStatus      = [
            'ON_ORDER',
        ];


        foreach ($workers as $cab) {
            $cab   = unserialize($cab);
            $color = !empty($cab['car']['color']) ? t('car', $cab['car']['color']) : '';
            if (!in_array($cab['worker']['city_id'], $city_list)
                || !in_array($cab['worker']['status'], $arBuzyDrieverStatus)
                || (empty($cab['geo']['lat']) || empty($cab['geo']['lon']))
            ) {
                continue;
            }
            $order           = Order::getOrderFromRedis(user()->tenant_id, $cab['worker']['last_order_id']);
            $marker_key_name = 'busy_car_' . $cab['worker']['worker_id'];

            $isPause = $cab['worker']['status'] == 'ON_BREAK';

            //assign value to callsign
            $callsign = CallsignService::getCallsign($cab);

            $arResult['markers'][]                      = [
                'key_name'    => $marker_key_name,
                'coords'      => [
                    'lat' => $cab['geo']['lat'],
                    'lon' => $cab['geo']['lon'],
                ],
                'marker_info' => $this->renderPartial('busy_car', [
                    'client_id'      => $isPause ? null : $order['client_id'],
                    'client_name'    => $isPause ? null : trim($order['client']['last_name'] . ' ' . $order['client']['name']),
                    'client_phone'   => $isPause ? null : $order['phone'],
                    'device'         => $order['device'],
                    'worker_id'      => $cab['worker']['worker_id'],
                    'worker_name'    => trim($cab['worker']['last_name'] . ' ' . $cab['worker']['name']),
                    'worker_phone'   => $cab['worker']['phone'],
                    'callsign'       => $cab['worker']['callsign'],
                    'city_id'        => $cab['worker']['city_id'],
                    'car_id'         => $cab['car']['car_id'],
                    'car_name'       => trim($cab['car']['name'] . ', ' . $color),
                    'car_gos_number' => $cab['car']['gos_number'],
                ]),
                'iconAngle'   => $cab['geo']['degree'],
                'worker_id'   => $callsign ? $callsign : '',
                'position_id' => $cab['position']['position_id'],
            ];
            $arResult['markers_keys'][$marker_key_name] = $cab['position']['position_id'];
        }

        $this->createResponseData($arResult);
    }


    public function actionGetPauseCars()
    {
        $city_list                = $this->getUserCityList();
        $workers                  = $this->getFilteredOnlineWorkers();
        $city_id                  = post('city_id');
        $arResult['markers']      = [];
        $arResult['markers_keys'] = [];
        $arResult['type']         = 'pause_car';
        $arPauseDriverStatus      = [
            'ON_BREAK',
            'BLOCKED',
        ];

        foreach ($workers as $cab) {
            $cab   = unserialize($cab);
            $color = !empty($cab['car']['color']) ? t('car', $cab['car']['color']) : '';

            if (!in_array($cab['worker']['city_id'], $city_list)
                || !in_array($cab['worker']['status'], $arPauseDriverStatus)
                || empty($cab['geo']['lat']) || empty($cab['geo']['lon'])
            ) {
                continue;
            }

            //assign value to callsign
            $callsign = CallsignService::getCallsign($cab);

            $marker_key_name                            = 'free_car_' . $cab['worker']['worker_id'];
            $arResult['markers'][]                      = [
                'key_name'    => $marker_key_name,
                'coords'      => [
                    'lat' => $cab['geo']['lat'],
                    'lon' => $cab['geo']['lon'],
                ],
                'marker_info' => $this->renderPartial('free_car', [
                    'worker_id'      => $cab['worker']['worker_id'],
                    'worker_name'    => trim($cab['worker']['last_name'] . ' ' . $cab['worker']['name']),
                    'phone'          => $cab['worker']['phone'],
                    'callsign'       => $cab['worker']['callsign'],
                    'city_id'        => $cab['worker']['city_id'],
                    'car_id'         => isset($cab['car']['car_id']) ? $cab['car']['car_id'] : null,
                    'car_name'       => implode(', ',
                        array_filter([isset($cab['car']['name']) ? trim($cab['car']['name']) : null, $color])),
                    'car_gos_number' => isset($cab['car']['gos_number']) ? $cab['car']['gos_number'] : null,
                ]),
                'iconAngle'   => $cab['geo']['degree'],
                'worker_id'   => $callsign ? $callsign : '',
                //                'iconAngle'   => 0,
                'position_id' => $cab['position']['position_id'],
            ];
            $arResult['markers_keys'][$marker_key_name] = $cab['position']['position_id'];
        }

        $this->createResponseData($arResult);
    }

    public function actionGetOfferCars()
    {

        $city_list                = $this->getUserCityList();
        $workers                  = $this->getFilteredOnlineWorkers();
        $city_id                  = post('city_id');
        $arResult['markers']      = [];
        $arResult['markers_keys'] = [];
        $arResult['type']         = 'offer_car';
        $arOfferDriverStatus      = [
            'OFFER_ORDER',
        ];

        foreach ($workers as $cab) {
            $cab   = unserialize($cab);
            $color = !empty($cab['car']['color']) ? t('car', $cab['car']['color']) : '';

            if (!in_array($cab['worker']['city_id'], $city_list)
                || !in_array($cab['worker']['status'], $arOfferDriverStatus)
                || empty($cab['geo']['lat']) || empty($cab['geo']['lon'])
            ) {
                continue;
            }

            //assign value to callsign
            $callsign = CallsignService::getCallsign($cab);

            $marker_key_name                            = 'free_car_' . $cab['worker']['worker_id'];
            $arResult['markers'][]                      = [
                'key_name'    => $marker_key_name,
                'coords'      => [
                    'lat' => $cab['geo']['lat'],
                    'lon' => $cab['geo']['lon'],
                ],
                'marker_info' => $this->renderPartial('free_car', [
                    'worker_id'      => $cab['worker']['worker_id'],
                    'worker_name'    => trim($cab['worker']['last_name'] . ' ' . $cab['worker']['name']),
                    'phone'          => $cab['worker']['phone'],
                    'callsign'       => $cab['worker']['callsign'],
                    'city_id'        => $cab['worker']['city_id'],
                    'car_id'         => isset($cab['car']['car_id']) ? $cab['car']['car_id'] : null,
                    'car_name'       => implode(', ',
                        array_filter([isset($cab['car']['name']) ? trim($cab['car']['name']) : null, $color])),
                    'car_gos_number' => isset($cab['car']['gos_number']) ? $cab['car']['gos_number'] : null,
                ]),
                'iconAngle'   => $cab['geo']['degree'],
                'worker_id'   => $callsign ? $callsign : '',
                //                'iconAngle'   => 0,
                'position_id' => $cab['position']['position_id'],
            ];
            $arResult['markers_keys'][$marker_key_name] = $cab['position']['position_id'];
        }

        $this->createResponseData($arResult);
    }

    /**
     * Generates response data.
     *
     * @param array $data
     */
    private function createResponseData($data)
    {
        if (empty($data['markers'])) {
            $data['message'] = t('order', 'No data for display');
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->data   = $data;
    }
}
