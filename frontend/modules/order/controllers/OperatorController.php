<?php

namespace app\modules\order\controllers;

use app\modules\order\models\OrderStatus;
use frontend\modules\order\components\ActionManager;
use frontend\modules\order\components\OrderRequestResult;
use frontend\modules\order\components\OrderService;
use app\modules\order\models\Order;
use app\modules\tenant\controllers\UserDispetcherController;
use app\modules\client\models\ClientSearch;
use app\modules\tenant\models\PhoneLine;
use yii\helpers\ArrayHelper;
use yii\web\Response;

class OperatorController extends OrderController
{
    public $layout = '//order';

    public function actionIndex()
    {
        //Меняем статус диспетчера на свободен
        $operator_data = unserialize(\Yii::$app->redis_active_dispetcher->executeCommand('get', [user()->user_id]));

        if (!$operator_data['on_pause']) {
            UserDispetcherController::sendDispetcherStateToPhoneApi(user()->user_id, 'idle');
        }

        return parent::actionIndex();
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $city_id
     *
     * @return mixed
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate($city_id = null)
    {
        /* @var $actionManager ActionManager */
        $actionManager = \Yii::createObject(ActionManager::class, [app()->orderApi]);

        /** @var $orderService OrderService */
        $orderService = $this->module->orderService;

        //Меняем статус диспетчера на недоступный
        UserDispetcherController::sendDispetcherStateToPhoneApi(user()->user_id, 'unavailable');
        $order = new Order();
        $order->tenant_id = user()->tenant_id;

        if ($phone = get('phone')) {
            $order->phone = $phone;
        }

        if ($city_id) {
            $order->city_id = $city_id;
        }

        $isCopyOrder = false;
        $orderId = app()->session->get('copyOrder');
        if (!empty($orderId)) {
            $isCopyOrder = true;
            app()->session->remove('copyOrder');
            $order->copyFrom($orderId);
        }

        $order->status_id = OrderStatus::STATUS_NEW;
        $order->orderAction = ActionManager::ACTION_NEW_ORDER;
        $dataForm = $order->getFormData($city_id, $order->status_id);

        if (empty($order->order_date)) {
            $order->city_id = $city_id ? $city_id : key($dataForm['CITY_LIST']);

            $time = time() + $order->getOrderOffset() + $order->getPickUp();
            $order->order_now = 1;
            $order->order_date = date('Y-m-d', $time);
            $order->order_hours = date('H', $time);
            $order->order_minutes = date('i', $time);
        }

        $parkingList = $order->getAddressParklingList();

        $positions = $orderService->getPositions($order->city_id);
        $positionIds = array_keys($positions);
        if (empty($order->position_id) || !in_array($order->position_id, $positionIds, false)) {
            $order->position_id = current($positionIds);
        }

        $tariffs = $orderService->getTariffs($order->city_id, $order->position_id, $order->client_id, $order->payment,
            $order->company_id, true, ['operator']);

        $tariffIds = empty($tariffs) ? [] : ArrayHelper::getColumn($tariffs, 'tariff_id');
        if (empty($order->tariff_id) || !in_array($order->tariff_id, $tariffIds, false)) {
            $order->tariff_id = current($tariffIds);
        }

        $clientInfo = $orderService->getClientInformationById($order->tenant_id, $order->city_id, $order->client_id);

        $orderActions = array_map(function ($value) {
            return [$value => t('order', $value)];
        }, $actionManager->getAvailableActions(null));

        $canBonusPayment = $this->canBonusPayment($order->tenant_id, $order->client_id, $order->city_id,
            $order->tariff_id);

        return $this->render('@app/modules/order/views/order/add', [
            'order'           => $order,
            'dataForm'        => $dataForm,
            'parkingList'     => $parkingList,
            'positions'       => $positions,
            'tariffs'         => $tariffs,
            'orderActions'    => $orderActions,
            'clientInfo'      => $clientInfo,
            'canBonusPayment' => $canBonusPayment,
            'isCopyOrder'     => $isCopyOrder,
        ]);
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $order_number
     *
     * @return mixed
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionUpdate($order_number)
    {
        $orderNumber = filter_var($order_number, FILTER_SANITIZE_NUMBER_INT);

        $order = Order::getOrderInfo($orderNumber);

        if (in_array($order->status->status_group, [
            OrderStatus::STATUS_GROUP_COMPLETED,
            OrderStatus::STATUS_GROUP_REJECTED,
        ], false)) {
            return $this->redirect(['view', 'order_number' => $orderNumber]);
        }

        /* @var $actionManager ActionManager */
        $actionManager = \Yii::createObject(ActionManager::class, [app()->orderApi]);

        /** @var $orderService OrderService */
        $orderService = $this->module->orderService;

        $order->hash = $this->generateOrderHash($order->order_id, $order->status_id);
        $order->order_now = 0;
        $order->order_date = date('Y-m-d', $order->order_time);
        $order->order_hours = date('H', $order->order_time);
        $order->order_minutes = date('i', $order->order_time);
        $order->order_seconds = date('s', $order->order_time);

        $order->additional_option = ArrayHelper::map($order->options, 'option_id', 'name');
        $form = app()->user->can('orders') ? 'update' : 'view';


        $positions = $orderService->getPositions($order->city_id);
        $tariffs = $orderService->getTariffs($order->city_id, $order->position_id, $order->client_id, $order->payment, $order->company_id, true, ['operator']);
        $tariff = $orderService->getTariff($order->tariff_id);
        $tariffs = array_merge([$tariff], $tariffs);

        $orderActions = array_map(function ($value) {
            return [$value => t('order', $value)];
        }, $actionManager->getAvailableActions($order->status_id));
        $availableAttributes = $actionManager->getAvailableAttributes($order->status_id);

        $clientInfo = $orderService->getClientInformationById($order->tenant_id, $order->city_id, $order->client_id);
        if (OrderService::isActiveOrderProcessingThroughTheExternalExchange($order->tenant_id, $order->order_id)) {
            $workerInfo = OrderService::getActiveWorkerInformationFromExternalExchange($order->tenant_id, $order->order_id);
        } else {
            $workerInfo = $orderService->getWorkerInformation($order->tenant_id, $order->worker_id, $order->car_id,
                $order->city_id, $order->position_id);
        }

        $canBonusPayment = $this->canBonusPayment($order->tenant_id, $order->client_id, $order->city_id,
            $order->tariff_id);

        return $this->render('@app/modules/order/views/order/' . $form, [
            'order'               => $order,
            'dataForm'            => $order->getFormData(null, $order->status_id),
            'parkingList'         => $order->getAddressParklingList(),
            'positions'           => $positions,
            'tariffs'             => $tariffs,
            'orderActions'        => $orderActions,
            'availableAttributes' => $availableAttributes,
            'clientInfo'          => $clientInfo,
            'workerInfo'          => $workerInfo,
            'canBonusPayment'     => $canBonusPayment,
        ]);
    }

    /**
     * View order page.
     *
     * @param integer $order_number
     *
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public function actionView($order_number)
    {
        $orderNumber = filter_var($order_number, FILTER_SANITIZE_NUMBER_INT);

        $order = $this->getOrderViewObject($orderNumber);

        /** @var $orderService OrderService */
        $orderService = $this->module->orderService;

        if (app()->request->isPost) {
            app()->response->format = Response::FORMAT_JSON;

            try {
                $order->load(app()->request->post());

                if ($order->orderAction === ActionManager::ACTION_COPY_ORDER) {
                    app()->session->set('copyOrder', $order->order_id);

                    return OrderRequestResult::getResult(
                        OrderRequestResult::ACTION_REDIRECT, '', 'create?city_id=' . $order->city_id);
                }

                return OrderRequestResult::getResult(
                    OrderRequestResult::ACTION_ERROR,
                    t('order', 'Invalid order action.')
                );
            } catch (\Exception $ex) {
                \Yii::error($ex, 'order');

                return OrderRequestResult::getResult(
                    OrderRequestResult::ACTION_ERROR,
                    t('order', 'Error saving order. Notify to administrator, please.')
                );
            }
        }

        $clientInfo = $orderService->getClientInformationById($order->tenant_id, $order->city_id,
            $order->client_id);

        if (OrderService::isActiveOrderProcessingThroughTheExternalExchange($order->tenant_id, $order->order_id)) {
            $workerInfo = OrderService::getActiveWorkerInformationFromExternalExchange($order->tenant_id, $order->order_id);
        } else {
            $workerInfo = $orderService->getWorkerInformation($order->tenant_id, $order->worker_id, $order->car_id,
                $order->city_id, $order->position_id);
        }

        $orderActions = array_map(function ($value) {
            return [$value => t('order', $value)];
        }, [ActionManager::ACTION_COPY_ORDER]);

        $positions = $orderService->getPositions($order->city_id);
        $tariffs = $orderService->getTariffs(
            $order->city_id, $order->position_id, $order->client_id, $order->payment, $order->company_id, true, ['operator']);

        return $this->render('@app/modules/order/views/order/view', [
            'order'               => $order,
            'dataForm'            => $order->getFormData(null, $order->status_id),
            'parkingList'         => $order->getAddressParklingList(),
            'positions'           => $positions,
            'tariffs'             => $tariffs,
            'clientInfo'          => $clientInfo,
            'workerInfo'          => $workerInfo,
            'orderActions'        => $orderActions,
            'availableAttributes' => [],
        ]);
    }

    /**
     * It is calling.
     *
     * @param string $clid Calling phone number
     * @param int $call_id Unique call id.
     *
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\base\InvalidParamException
     */
    public function actionCall($did, $clid, $call_id)
    {
        //Определить новый заказ или текущий.
        $clid = preg_replace("/[^0-9]/", '', $clid);
        $did = trim($did);
        $orders = Order::getCallerOrder($clid);

        /* @var $actionManager ActionManager */
        $actionManager = \Yii::createObject(ActionManager::class, [app()->orderApi]);

        /** @var $orderService OrderService */
        $orderService = $this->module->orderService;

        //Определение тарифа и филиала по телефонной линии
        $phone_line = PhoneLine::findOne(['tenant_id' => user()->tenant_id, 'phone' => $did]);

        if (!empty($orders)) {
            if (empty($clid) || count($orders) > 1) {
                return $this->render('@app/modules/order/views/order/some_orders', [
                    'orders'  => $orders,
                    'call'    => true,
                    'clid'    => $clid,
                    'workers' => \Yii::$app->getModule('employee')->get('workerShift')->getCntWorkersOnShift(getValue($phone_line->city_id)),
                ]);
            }

            $order = $orders[0];
            $order->order_now = 0;
            $order->order_date = date('Y-m-d', $order->order_time);
            $order->order_hours = date('H', $order->order_time);
            $order->order_minutes = date('i', $order->order_time);
            $order->order_seconds = date('s', $order->order_time);
            $order->hash = $this->generateOrderHash($order->order_id, $order->status_id);

            $orderActions = array_map(function ($value) {
                return [$value => t('order', $value)];
            }, $actionManager->getAvailableActions($order->status_id));
            $availableAttributes = $actionManager->getAvailableAttributes($order->status_id);

            $clientInfo = $orderService->getClientInformationById($order->tenant_id, $order->city_id,
                $order->client_id);
            if (OrderService::isActiveOrderProcessingThroughTheExternalExchange($order->tenant_id, $order->order_id)) {
                $workerInfo = OrderService::getActiveWorkerInformationFromExternalExchange($order->tenant_id, $order->order_id);
            } else {
                $workerInfo = $orderService->getWorkerInformation($order->tenant_id, $order->worker_id, $order->car_id,
                    $order->city_id, $order->position_id);
            }

            $positions = $orderService->getPositions($order['city_id']);
            $tariffs = $orderService->getTariffs(
                $order['city_id'], $order['position_id'], $order['client_id'], $order['payment'], $order['company_id'], true, ['operator']);

            $canBonusPayment = $this->canBonusPayment($order->tenant_id, $order->client_id, $order->city_id,
                $order->tariff_id);

            return $this->render('@app/modules/order/views/order/update', [
                'order'               => $order,
                'dataForm'            => $order->getFormData(null, $order['status']['status_id']),
                'parkingList'         => $order->getAddressParklingList(),
                'call'                => true,
                'positions'           => $positions,
                'tariffs'             => $tariffs,
                'orderActions'        => $orderActions,
                'availableAttributes' => $availableAttributes,
                'clientInfo'          => $clientInfo,
                'workerInfo'          => $workerInfo,
                'canBonusPayment'     => $canBonusPayment,
            ]);
        } else {
            //Новый заказ
            $order = new Order();
            $order->phone = $clid;

            $client = ClientSearch::searchByPhone($clid);
            $order->client_id = $client->client_id;
            $order->tenant_id = user()->tenant_id;

            $cities = user()->getUserCityList();
            $order->city_id = empty($phone_line) ? key($cities) : $phone_line->city_id;
            $order->tariff_id = empty($phone_line) ? null : $phone_line->tariff_id;
            $positions = $orderService->getPositions($order['city_id']);
            $order->position_id = empty($order->tariff) || !array_key_exists($order->tariff->position_id, $positions)
                ? key($positions) : $order->tariff->position_id;

            $time = time() + $order->getOrderOffset() + $order->getPickUp();
            $order->order_now = 1;
            $order->order_date = date('Y-m-d', $time);
            $order->order_hours = date('H', $time);
            $order->order_minutes = date('i', $time);
            $order->order_seconds = date('s', $time);
            $clientInfo = $orderService->getClientInformationById($order->tenant_id, $order->city_id,
                $order->client_id);
            $payment = is_array($clientInfo['payments']) ? reset($clientInfo['payments']) : null;
            $order['payment'] = isset($payment['type']) ? $payment['type'] : Order::PAYMENT_CASH;
            $order['company_id'] = isset($payment['companyId']) ? $payment['companyId'] : null;

            $tariffs = $orderService->getTariffs(
                $order['city_id'], $order['position_id'], $order['client_id'], $order['payment'], $order['company_id'], true, ['operator']);

            $order->status_id = OrderStatus::STATUS_NEW;
            $order->orderAction = ActionManager::ACTION_NEW_ORDER;
            $dataForm = $order->getFormData($order->city_id, $order->status_id);

            $parkingList = $order->getAddressParklingList();
            $orderActions = array_map(function ($value) {
                return [$value => t('order', $value)];
            }, $actionManager->getAvailableActions(null));

            $canBonusPayment = $this->canBonusPayment($order->tenant_id, $order->client_id, $order->city_id,
                $order->tariff_id);

            return $this->render('@app/modules/order/views/order/add', [
                'order'           => $order,
                'dataForm'        => $dataForm,
                'parkingList'     => $parkingList,
                'call'            => true,
                'client'          => $client,
                'phone_valid'     => true,
                'call_id'         => $call_id,
                'positions'       => $positions,
                'tariffs'         => $tariffs,
                'orderActions'    => $orderActions,
                'clientInfo'      => $clientInfo,
                'canBonusPayment' => $canBonusPayment,
            ]);
        }
    }

    /**
     * Determining the type of data displayed on the map. Loading view with map.
     *
     * @param integer $order_id Used in the view like GET param
     * @param integer $status_id Order field status_id
     *
     * @return mixed
     */
    public function actionGetMap($order_id, $status_id)
    {
        return $this->getMap($status_id);
    }
}
