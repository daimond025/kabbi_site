<?php

namespace app\modules\order\controllers;

use app\modules\order\models\Order;
use app\modules\tenant\models\User;
use frontend\components\behavior\CityListBehavior;
use frontend\modules\companies\components\repositories\TenantCompanyRepository;
use frontend\modules\companies\models\TenantCompany;
use frontend\modules\order\components\actions\ScheduleReportAction;
use frontend\modules\order\components\OrderService;
use frontend\modules\order\components\OrderAssignWorkerService;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\rbac\Role;
use yii\web\Controller;
use yii\web\View;

/**
 * @mixin CityListBehavior
 */
class OrderAssignController extends Controller
{
    protected $orderService;
    protected $orderAssign;

    public $code_ok = 1;
    public $code_err = 0;
    public $code_info_ok = 'OK';
    public $code_info_err = 'INTERNAL_ERROR';

    public function __construct(
        $id,
        $module,
        OrderService $orderService,
        OrderAssignWorkerService $orderAssign,
        array $config = [])
    {
        $this->orderService = $orderService;
        $this->orderAssign = $orderAssign;
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritDoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['assign-orders', 'list-orders'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['assign-orders', 'list-orders'],
                        'roles' => ['@', '?'],
                    ]
                ],
            ],
            'cityList' => [
                'class' => CityListBehavior::className(),
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function beforeAction($action)
    {
        //Update active user time
        if (!Yii::$app->user->isGuest) {
            user()->updateUserActiveTime();
        }

        return parent::beforeAction($action);
    }

    /**
     * {@inheritDoc}
     */
    public function actions()
    {
        return [
            'schedule-report' => ScheduleReportAction::class,
        ];
    }

    public function actionIndex()
    {
        $tenantCompanyList = TenantCompanyRepository::selfCreate()->getForForm();

        $cityList = user()->getUserCityListWithRepublic();

        /* @var $view View */
        $view = $this->view;
        $view->title = t('app', 'Assign orders');
        $view->registerJs('window.KB = window.KB || {};', View::POS_HEAD);
        $view->registerJs('window.KB.URL = window.KB.URL || {};', View::POS_HEAD);
        $view->registerJs('window.KB.MSG = window.KB.MSG || {};', View::POS_HEAD);

        // Export url patterns
        //=====================

        $exportUrlPatterns = [
            'orderEdit' => [
                Url::to(['order/update', 'order_number' => 9999]),
                ['9999' => '{ORDER_NUMBER}']
            ],
            'scheduleReport' => [
                Url::to(['order-assign/schedule-report', 'company' => 9999]),
                ['9999' => '{COMPANY_ID}']
            ],
        ];

        foreach ($exportUrlPatterns as $name => $pattern) {
            $value = str_replace(array_keys($pattern[1]), $pattern[1], $pattern[0]);
            $view->registerJs("window.KB.URL['{$name}'] = " . Json::encode($value), View::POS_HEAD);
        }

        // Export phrases
        //================

        $exportPhrases = [
            'NO_FREE_ORDERS' => t('assign', 'No free orders!'),
            'PICK_WORKER' => t('assign', 'Choose an workers!'),
            'WORKER_NO_ORDERS' => t('assign', 'Worker has no orders!'),
            'NO_WORKERS' => t('assign', 'There are no workers!'),
            'LOADING' => t('assign', 'Loading ...'),
            'SAVING_STAGE_1' => t('assign', 'Save step 1/2'),
            'SAVING_STAGE_2' => t('assign', 'Save step 2/2'),
            'SUBMITTING' => t('assign', 'Send ...'),
            'CHECKING' => t('assign', 'Check ...'),
            'SEND_SCHEDULE_EMAIL' => t('assign', 'Sending tomorrow schedules by e-mail ...'),
            'LOAD_FAILURE' => t('assign', 'Data loading error!'),
            'SAVE_FAILURE' => t('assign', 'Data saving error!'),
            'MAIL_FAILURE' => t('assign', 'Email newsletter error!'),
            'PLEASE_RELOAD' => t('assign', 'Please refresh the page.'),
            'SAVE' => t('assign', 'Save'),
            'CLEAR' => t('assign', 'Clear'),
            'ASTA_SAVING_FAILURE' => t('assign', 'Saving failure!'),
        ];


        foreach ($exportPhrases as $name => $phrase) {
            $view->registerJs("window.KB.MSG['{$name}'] = " . Json::encode($phrase), View::POS_HEAD);
        }

        return $this->render('@app/modules/order/views/order-assign/index', [
            'user_city_list' => $cityList,
            'tenantCompanyList' => $tenantCompanyList,
        ]);
    }

    public function actionListOrders()
    {
        if (!Yii::$app->request->isAjax) {
            //return false;
        }
        $dataProvider = $this->orderService->getHospitalOrdersData($this->getCityId(), $this->formatDate(get('date_from')), $this->formatDate(get('date_to')));
        $orders = $dataProvider->query->all();

        $orders_out = [];
        foreach ($orders as $order) {
            $order_['id'] = $order->order_id;
            $order_['no'] = $order->order_number;
            $order_['picktime'] = date('d.m.Y H:i', $order->order_time);
            $order_['fromaddr'] = Order::getOrderAddressArr($order->address);

            $order_['workerid'] = !is_null($order->worker_id) ? $order->worker_id : null;
            $order_['companyId'] = isset($order->company_id) ? (int)$order->company_id : 0;

            $client = $order->client;
            $clientName = '';
            if (isset($client)) {
                $clientName = getShortName(
                    ArrayHelper::getValue($client, 'last_name', ''),
                    ArrayHelper::getValue($client, 'name', ''),
                    ArrayHelper::getValue($client, 'second_name', '')
                );
            }
            $order_['clientName'] = $clientName;
            $orders_out[] = $order_;
        }

        $info['code'] = $this->code_ok;
        $info['info'] = $this->code_info_ok;
        $this->getAnswer($info, $orders_out);
    }

    public function actionListWorkers()
    {
        $company_id = get('company_id');
        if (!Yii::$app->request->isAjax || is_null($company_id)) {
            //return false;
        }

        $workers_out = [];
        if (!isset($company_id)) {
            $info['code'] = $this->code_err;
            $info['info'] = $this->code_info_err;
            $this->getAnswer($info, $workers_out);
        }

        $workers = $this->orderAssign->getWorkerCompany($company_id);

        $workers_out = [];
        foreach ($workers as $worker) {
            $worker_item['worker_id'] = $worker->worker_id;
            $worker_item['name'] = $worker->getShortName();
            $worker_item['callsign'] = $worker->callsign;
            $worker_item['note'] = isset($worker->workerShiftNote) && isset($worker->workerShiftNote->note)
                ?$worker->workerShiftNote->note : '';
            $workers_out[] = $worker_item;
        }
        $info['code'] = $this->code_ok;
        $info['info'] = $this->code_info_ok;
        $this->getAnswer($info, $workers_out);
    }

    public function actionWorkerNote(){

        if (!Yii::$app->request->isAjax) {
            //return false;
        }
        $workerId = post('workerId');
        $note = post('note');

        // todo test
        /*$workerId = '11141';
        $note = "sdfsdfsdf sdfsdfs sdfsdfs sdf!";*/

        if(!is_int($workerId)){
            $result = $this->orderAssign->setWorkerNote($workerId, $note) ;
            $info['code'] = $this->code_ok;
            $info['info'] = $this->code_info_ok;
            $this->getAnswer($info, $result == true ? 1 : 0);
        }else{
            $info['code'] = $this->code_err;
            $info['info'] = $this->code_info_err;
            $this->getAnswer($info, 0);
        }
    }

    public function actionAssignOrders()
    {
        if (!Yii::$app->request->isAjax) {
            //return false;
        }
        $orders_assign = [];
        $orders = post('orders');

        //todo test
        /* $orders[686938] = "11141";
         /*$orders[687757] = 11147;
         $orders[2] = 11147;*/

        if (is_null($orders)) {
            $info['code'] = $this->code_err;
            $info['info'] = $this->code_info_err;
            $this->getAnswer($info, $orders_assign);
        } else {

            $orders_ids = array_keys($orders);
            $this->orderAssign->getOrderByIds($orders, $orders_ids);

            $info['code'] = $this->code_ok;
            $info['info'] = $this->code_info_ok;
            $this->getAnswer($info, $orders);
        }
    }

    public function actionGetResult()
    {

        if (!Yii::$app->request->isAjax) {
            //return false;
        }

        $orders = post('orders');

        // TODO test
        /* $orders[688170] = 'a7f33fa9-1570-4616-89e1-32d7f8137ca8';
         $orders[687757] = '13faa189-ccc9-4e3f-8780-829817eabbd7';*/

        $orders_result = [];
        if (is_null($orders)) {
            $info['code'] = $this->code_err;
            $info['info'] = $this->code_info_err;
            $this->getAnswer($info, $orders_result);
        } else {
            $this->orderAssign->getResultRespone($orders);
            $info['code'] = $this->code_ok;
            $info['info'] = $this->code_info_ok;
            $this->getAnswer($info, $orders);
        }
    }

    public function actionRemoveWorkers()
    {
        if (!Yii::$app->request->isAjax) {
            //return false;
        }

        $orders = post('orders');

        // todo test
        /* $orders[687753] = 11096;
         $orders[687757] = 11147;
         $orders[2] = 11147;*/

        $orders_result = [];
        if (is_null($orders)) {
            $info['code'] = $this->code_err;
            $info['info'] = $this->code_info_err;
            $this->getAnswer($info, $orders_result);
        } else {
            $this->orderAssign->removeWorkerFromOrder($orders);

            $info['code'] = $this->code_ok;
            $info['info'] = $this->code_info_ok;
            $this->getAnswer($info, $orders);
        }
    }

    public function actionRemoveOrders()
    {
        if (!Yii::$app->request->isAjax) {
            //return false;
        }

        $orders = post('orders');

        // todo test
        /* $orders[688170] = '';
           $orders[688092] = 11147;
           $orders[2] = 11147;*/

        $orders_result = [];
        if (is_null($orders)) {
            $info['code'] = $this->code_err;
            $info['info'] = $this->code_info_err;
            $this->getAnswer($info, $orders_result);
        } else {
            $orders_ids = array_keys($orders);
            $this->orderAssign->deleteOrders($orders, $orders_ids);

            $info['code'] = $this->code_ok;
            $info['info'] = $this->code_info_ok;
            $this->getAnswer($info, $orders);
        }
    }

    public function actionSendEmail()
    {
        $company_id = get('company_id');

        // todo test
        //$company_id = 10;

        $workers_out = [];
        if (!isset($company_id) || !is_int((int)$company_id)) {
            $info['code'] = $this->code_err;
            $info['info'] = $this->code_info_err;
            $this->getAnswer($info, $workers_out);
        } else {
            $worker_process = $this->orderAssign->processSendEmail($company_id);
            $info['code'] = $this->code_ok;
            $info['info'] = $this->code_info_ok;
            $this->getAnswer($info, $worker_process);
        }
    }

    public function getAnswer($info, $result)
    {
        $response = [
            'code' => $info['code'],
            'info' => $info['info'],
            'result' => $result,
        ];

        return $this->asJson($response);
    }

    public function getCompanyList()
    {
        $user_role = current(app()->authManager->getRolesByUser(app()->user->getId()));
        $user_role_name = ($user_role instanceof Role) ? strtolower(ArrayHelper::getValue($user_role, 'name')) : null;
        $company_user = null;
        if ($user_role_name == User::ROLE_STAFF_COMPANY) {
            $company_user = isset(user()->tenant_company_id) ? user()->tenant_company_id : 0;
        }

        $query = TenantCompany::find();
        $query->where([
            'tenant_id' => user()->tenant_id,
            'block' => 0,
        ])->andFilterWhere([
            'tenant_company_id' => $this->id_current
        ]);
    }


    private function formatDate($date)
    {
        if (empty($date)) {
            return null;
        }
        $filterDate = strtotime($date);
        return $filterDate ? date('d.m.Y', $filterDate) : null;
    }

    /**
     * @return array|mixed|null
     */
    private function getCityId()
    {
        $cityId = get('city_id');

        return ArrayHelper::isIn($cityId, $this->getUserCityIds()) ? $cityId : null;
    }
}