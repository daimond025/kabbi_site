<?php

namespace frontend\modules\order\components;

/**
 * Class OrderRequestResult
 * @package frontend\modules\order\components
 */
class OrderRequestResult
{
    const ACTION_ERROR = 'ACTION_ERROR';
    const ACTION_REDIRECT = 'ACTION_REDIRECT';
    const ACTION_CLOSE = 'ACTION_CLOSE';
    const ACTION_WAIT = 'ACTION_WAIT';

    /**
     * Getting prepared result
     *
     * @param string $action
     * @param string $message
     * @param string $url
     *
     * @return array
     */
    public static function getResult($action, $message = null, $url = null)
    {

        return [
            'action'  => $action,
            'message' => $message,
            'url'     => $url,
        ];
    }
}