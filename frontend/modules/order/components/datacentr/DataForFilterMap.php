<?php

namespace frontend\modules\order\components\datacentr;


use app\modules\tenant\models\User;
use common\modules\car\models\CarClass;
use common\modules\employee\models\position\Position;
use frontend\modules\companies\components\repositories\TenantCompanyRepository;
use frontend\modules\order\components\repositories\CarClassRepositories;
use frontend\modules\order\components\repositories\GroupRepository;
use frontend\modules\order\components\repositories\PositionRepository;
use yii\helpers\ArrayHelper;

class DataForFilterMap
{
    public function getCities()
    {
        return user()->getUserCityList();
    }

    public function getPositions()
    {
        $conditions = [];

        if (app()->user->can(User::ROLE_STAFF_COMPANY)) {
            $conditions = ['w.tenant_company_id' => user()->tenant_company_id];
        }

        return ArrayHelper::map(
            PositionRepository::getAllUniquePositionsTenant($conditions),
            'position_id',
            function (Position $model) {
                return t('employee', $model->name);
            }
        );
    }

    public function getGroups()
    {
        return ArrayHelper::map(
            GroupRepository::getAllGroupsTenant(user()->tenant_id),
            'group_id',
            'name'
        );
    }

    public function getCarClasses()
    {
        $conditions = [];

        if (app()->user->can(User::ROLE_STAFF_COMPANY)) {
            $conditions = ['c.tenant_company_id' => user()->tenant_company_id];
        }

        return ArrayHelper::map(
            CarClassRepositories::getAllCarClassTenant($conditions),
            'class_id',
            function (CarClass $model) {
                return t('car', $model->class);
            }
        );
    }

    public function getTenantCompanies()
    {
        return TenantCompanyRepository::selfCreate()
            ->getForForm(['tenant_id' => user()->tenant_id]);
    }
}