<?php

namespace frontend\modules\order\components;

use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use common\helpers\ArrayHelper;
use orderApi\OrderApi;

/**
 * Class ActionManager
 * @package frontend\modules
 */
class ActionManager
{
    const ACTION_UPDATE_ORDER = 'UPDATE_ORDER';
    const ACTION_COPY_ORDER = 'COPY_ORDER';
    const ACTION_STOP_OFFER = 'STOP_OFFER_ORDER';
    const ACTION_NEW_ORDER = 'NEW_ORDER';
    const ACTION_FREE_ORDER = 'FREE_ORDER';
    const ACTION_FREEZE_ORDER = 'FREEZE_ORDER';
    const ACTION_ASSIGN_ORDER = 'ASSIGN_ORDER';
    const ACTION_TAKE_OFF_ORDER = 'REMOVE_WORKER';
    const ACTION_ASSIGN_COMPANY = 'ASSIGN_COMPANY';
    const ACTION_TAKE_OFF_COMPANY = 'REMOVE_COMPANY';
    const ACTION_COMPLETE_PAY_ORDER = 'COMPLETE_PAY_ORDER';
    const ACTION_COMPLETE_NO_PAY_ORDER = 'COMPLETE_NO_PAY_ORDER';
    const ACTION_REJECT_ORDER = 'REJECT_ORDER';
    const ACTION_SEND_TO_EXCHANGE = 'SEND_TO_EXCHANGE';

    /* @var OrderApi */
    private $orderApi;

    /**
     * ActionManager constructor.
     *
     * @param OrderApi $orderApi
     */
    public function __construct(OrderApi $orderApi)
    {
        $this->orderApi = $orderApi;
    }

    /**
     * Getting attribute information
     *
     * @param int $statusId
     *
     * @return array
     */
    private function getAttributeInformation($statusId)
    {
        try {
            return $this->orderApi->getAttributeInformation($statusId);
        } catch (\Exception $ex) {
            return [];
        }
    }

    /**
     * Getting available actions
     *
     * @param int $statusId
     *
     * @param null $worker
     *
     * @return array
     */
    public static function getAvailableActionsStaff($statusId, $worker = null, $company_id = null, $device = ''){
        // CREATE ORDER
        if ($statusId === null) {
            return [

            ];
        }
        $worker_action = [];
        if(!is_null($worker)){
            $worker_action = [self::ACTION_TAKE_OFF_ORDER];
        }else{
            $worker_action = [self::ACTION_ASSIGN_ORDER];
        }

        //todo company action
        $company_action = [];
        if($device == Order::DEVICE_HOSPITAL && is_null($company_id)){
            $company_action = [
                self::ACTION_ASSIGN_COMPANY
            ];
        }


        switch ($statusId) {
            // MANUAL GROUP
            case OrderStatus::STATUS_MANUAL_MODE:
                return [
                    ];
            // NEW GROUP
            case OrderStatus::STATUS_NEW:
            case OrderStatus::STATUS_NOPARKING:
                return [
                    ];
            // OFFER ORDER GROUP
            case OrderStatus::STATUS_OFFER_ORDER:
            case OrderStatus::STATUS_WORKER_REFUSED:
            case OrderStatus::STATUS_WORKER_IGNORE_ORDER_OFFER:
                return [

                ];

            // FREE GROUP
            case OrderStatus::STATUS_FREE:
            case OrderStatus::STATUS_OVERDUE:
            case OrderStatus::STATUS_REFUSED_ORDER_ASSIGN:
            case OrderStatus::STATUS_FREE_FOR_HOSPITAL:
            case OrderStatus::STATUS_FREE_FOR_COMPANY:
                return array_merge([

                ], $worker_action, $company_action );
            // NEW PRE GROUP
            case OrderStatus::STATUS_PRE:
            case OrderStatus::STATUS_PRE_NOPARKING:
            case OrderStatus::STATUS_PRE_REFUSE_WORKER:
            case OrderStatus::STATUS_PRE_FOR_HOSPITAL:
            case OrderStatus::STATUS_PRE_ORDER_COMPANY:
                return array_merge([

                ], $worker_action, $company_action);
            // ASSIGNED PRE GROUP
            case OrderStatus::STATUS_PRE_GET_WORKER:
            case OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_SOFT:
            case OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_HARD:
            case OrderStatus::STATUS_WAITING_FOR_PRE_ORDER_CONFIRMATION:
            case OrderStatus::STATUS_WORKER_ACCEPTED_PRE_ORDER_CONFIRMATION:
                return array_merge([

                ], $worker_action, $company_action);
            case OrderStatus::STATUS_WORKER_IGNORED_PRE_ORDER_CONFIRMATION:
            case OrderStatus::STATUS_WORKER_REFUSED_PRE_ORDER_CONFIRMATION:
                return [
                ];

            // ASSIGNED GROUP
            case OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT:
            case OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_HARD:
                return array_merge([
                    self::ACTION_COMPLETE_PAY_ORDER,
                    self::ACTION_COMPLETE_NO_PAY_ORDER
                ], $worker_action, $company_action);


            // CAR ASSIGNED GROUP
            case OrderStatus::STATUS_GET_WORKER:
            case OrderStatus::STATUS_WORKER_LATE:
            case OrderStatus::STATUS_EXECUTION_PRE:
                return [
                    self::ACTION_COMPLETE_PAY_ORDER,
                    self::ACTION_COMPLETE_NO_PAY_ORDER,
                    self::ACTION_REJECT_ORDER,
                ];

            // CAR AT PLACE GROUP
            case OrderStatus::STATUS_NONE_FREE_AWAITING:
            case OrderStatus::STATUS_FREE_AWAITING:
            case OrderStatus::STATUS_CLIENT_IS_NOT_COMING_OUT:
            case OrderStatus::STATUS_WORKER_WAITING:
                return [
                    self::ACTION_COMPLETE_PAY_ORDER,
                    self::ACTION_COMPLETE_NO_PAY_ORDER,
                    self::ACTION_REJECT_ORDER,
                ];

            // EXECUTING GROUP
            case OrderStatus::STATUS_EXECUTING:
            case OrderStatus::STATUS_PAYMENT_CONFIRM:
            case OrderStatus::STATUS_WAITING_FOR_PAYMENT:
                return [
                    self::ACTION_COMPLETE_PAY_ORDER,
                    self::ACTION_COMPLETE_NO_PAY_ORDER,
                    self::ACTION_REJECT_ORDER,
                ];
            default:
                return [
                ];
        }
    }

    /**
     * Getting available actions
     *
     * @param int $statusId
     *
     * @param array $user_staff
     *
     * @return array
     */
    public function getAvailableActions($statusId, $user_staff = [])
    {
        $worker_id = ArrayHelper::getValue($user_staff,'worker_id');
        $company_id = ArrayHelper::getValue($user_staff,'company_id');
        $device= ArrayHelper::getValue($user_staff,'device');

        // пользователи партнеров могут только назначать водителей и все
        if((count($user_staff) > 0 && !$user_staff['edit'])){
            return self::getAvailableActionsStaff($statusId, $worker_id, $company_id, $device);
        }

        // CREATE ORDER
        if ($statusId === null) {
            return [
                self::ACTION_NEW_ORDER,
                self::ACTION_FREE_ORDER,
                self::ACTION_FREEZE_ORDER,
            ];
        }

        //todo company action
        $company_action = [];
        if($device == Order::DEVICE_HOSPITAL ){
            if(!is_null($company_id)){
                $company_action = [
                    self::ACTION_TAKE_OFF_COMPANY
                ];
            }else{
                $company_action = [
                    self::ACTION_ASSIGN_COMPANY
                ];
            }
        }

        //todo worker action
        $worker_action = [];
        if(!is_null($worker_id)){
            $worker_action = [self::ACTION_TAKE_OFF_ORDER];
        }else{
            $worker_action = [self::ACTION_ASSIGN_ORDER];
        }

        switch ($statusId) {
            // MANUAL GROUP
            case OrderStatus::STATUS_MANUAL_MODE:
                return [
                    self::ACTION_UPDATE_ORDER,
                    self::ACTION_COPY_ORDER,
                    self::ACTION_NEW_ORDER,
                    self::ACTION_FREE_ORDER,
                    self::ACTION_FREEZE_ORDER,
                    self::ACTION_ASSIGN_ORDER,
                    self::ACTION_REJECT_ORDER,

                ];

            // NEW GROUP
            case OrderStatus::STATUS_NEW:
            case OrderStatus::STATUS_NOPARKING:
                return [
                    self::ACTION_UPDATE_ORDER,
                    self::ACTION_STOP_OFFER,
                    self::ACTION_COPY_ORDER,
                    self::ACTION_FREE_ORDER,
                    self::ACTION_FREEZE_ORDER,
                ];

            // OFFER ORDER GROUP
            case OrderStatus::STATUS_OFFER_ORDER:
            case OrderStatus::STATUS_WORKER_REFUSED:
            case OrderStatus::STATUS_WORKER_IGNORE_ORDER_OFFER:
                return [
                    self::ACTION_UPDATE_ORDER,
                    self::ACTION_COPY_ORDER,
                    self::ACTION_STOP_OFFER,
                ];

            // FREE GROUP
            case OrderStatus::STATUS_FREE:
            case OrderStatus::STATUS_OVERDUE:
            case OrderStatus::STATUS_REFUSED_ORDER_ASSIGN:
                return array_merge( [
                    self::ACTION_UPDATE_ORDER,
                    self::ACTION_COPY_ORDER,
                    self::ACTION_NEW_ORDER,
                    self::ACTION_FREEZE_ORDER,
                    self::ACTION_SEND_TO_EXCHANGE,
                    self::ACTION_REJECT_ORDER,
                ], $worker_action, $company_action);

            // NEW PRE GROUP
            case OrderStatus::STATUS_PRE:
            case OrderStatus::STATUS_PRE_NOPARKING:
            case OrderStatus::STATUS_PRE_FOR_HOSPITAL:
            case OrderStatus::STATUS_PRE_ORDER_COMPANY:
                return array_merge( [
                    self::ACTION_UPDATE_ORDER,
                    self::ACTION_COPY_ORDER,
                    self::ACTION_FREEZE_ORDER,
                    self::ACTION_REJECT_ORDER,
                ], $worker_action, $company_action);

            // ASSIGNED PRE GROUP
            case OrderStatus::STATUS_PRE_GET_WORKER:
            case OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_SOFT:
            case OrderStatus::STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_HARD:
            case OrderStatus::STATUS_WAITING_FOR_PRE_ORDER_CONFIRMATION:
            case OrderStatus::STATUS_WORKER_ACCEPTED_PRE_ORDER_CONFIRMATION:

                return array_merge( [
                    self::ACTION_UPDATE_ORDER,
                    self::ACTION_COPY_ORDER,
                    self::ACTION_FREEZE_ORDER,
                    self::ACTION_COMPLETE_PAY_ORDER,
                    self::ACTION_COMPLETE_NO_PAY_ORDER,
                    self::ACTION_REJECT_ORDER
                ], $worker_action, $company_action);
            case OrderStatus::STATUS_WORKER_IGNORED_PRE_ORDER_CONFIRMATION:
            case OrderStatus::STATUS_WORKER_REFUSED_PRE_ORDER_CONFIRMATION:
                return [
                    self::ACTION_COPY_ORDER,
                    self::ACTION_FREEZE_ORDER,
                    self::ACTION_REJECT_ORDER,
                ];

            // ASSIGNED GROUP
            case OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT:
            case OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_HARD:
                return  array_merge( [
                    self::ACTION_UPDATE_ORDER,
                    self::ACTION_COPY_ORDER,
                    self::ACTION_FREE_ORDER,
                    self::ACTION_FREEZE_ORDER,
                    self::ACTION_COMPLETE_PAY_ORDER,
                    self::ACTION_COMPLETE_NO_PAY_ORDER,
                    self::ACTION_REJECT_ORDER,
                ], $worker_action, $company_action );

            // CAR ASSIGNED GROUP
            case OrderStatus::STATUS_GET_WORKER:
            case OrderStatus::STATUS_WORKER_LATE:
            case OrderStatus::STATUS_EXECUTION_PRE:
                 return  array_merge( [
                    self::ACTION_UPDATE_ORDER,
                    self::ACTION_COPY_ORDER,
                    self::ACTION_FREEZE_ORDER,
                    self::ACTION_COMPLETE_PAY_ORDER,
                    self::ACTION_COMPLETE_NO_PAY_ORDER,
                    self::ACTION_REJECT_ORDER,
                ], []);

            // CAR AT PLACE GROUP
            case OrderStatus::STATUS_NONE_FREE_AWAITING:
            case OrderStatus::STATUS_FREE_AWAITING:
            case OrderStatus::STATUS_CLIENT_IS_NOT_COMING_OUT:
            case OrderStatus::STATUS_WORKER_WAITING:
                return [
                    self::ACTION_UPDATE_ORDER,
                    self::ACTION_COPY_ORDER,
                    self::ACTION_FREEZE_ORDER,
                    self::ACTION_COMPLETE_PAY_ORDER,
                    self::ACTION_COMPLETE_NO_PAY_ORDER,
                    self::ACTION_REJECT_ORDER,
                ];

            // EXECUTING GROUP
            case OrderStatus::STATUS_EXECUTING:
            case OrderStatus::STATUS_PAYMENT_CONFIRM:
            case OrderStatus::STATUS_WAITING_FOR_PAYMENT:
                return [
                    self::ACTION_UPDATE_ORDER,
                    self::ACTION_COPY_ORDER,
                    self::ACTION_FREEZE_ORDER,
                    self::ACTION_COMPLETE_PAY_ORDER,
                    self::ACTION_COMPLETE_NO_PAY_ORDER,
                    self::ACTION_REJECT_ORDER,
                ];

             // FREE HOSPITAL ORDERS
            case OrderStatus::STATUS_FREE_FOR_HOSPITAL:
            case OrderStatus::STATUS_FREE_FOR_COMPANY:
                return array_merge( [
                    self::ACTION_UPDATE_ORDER,
                    self::ACTION_COPY_ORDER,
                    self::ACTION_NEW_ORDER,
                    self::ACTION_FREEZE_ORDER,
                    self::ACTION_SEND_TO_EXCHANGE,
                    self::ACTION_REJECT_ORDER,
                ], $worker_action, $company_action);
            default:
                return [
                    self::ACTION_COPY_ORDER,
                ];
        }
    }

    /**
     * Getting available attributes
     *
     * @param int $statusId
     *
     * @return array
     */
    public function getAvailableAttributes($statusId)
    {
        $attributes = $this->getAttributeInformation($statusId);

        $result = [];
        foreach ($attributes as $key => $value) {
            if ($value) {
                $result[] = $key;
            }
        }

        return $result;
    }

    /**
     * Applying action to order
     *
     * @param Order $order
     */
    public function applyActionToOrder(Order $order)
    {
        switch ($order->orderAction) {
            case self::ACTION_NEW_ORDER:
                $order->status_id = OrderStatus::STATUS_NEW;
                break;
            case self::ACTION_FREE_ORDER:
                $order->status_id = OrderStatus::STATUS_FREE;
                break;
            case self::ACTION_FREEZE_ORDER:
                $order->status_id = OrderStatus::STATUS_MANUAL_MODE;
                break;
            case self::ACTION_ASSIGN_ORDER:
                $order->status_id = (int)$order->deny_refuse_order === 1
                    ? OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_HARD
                    : OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT;
                break;
            case self::ACTION_ASSIGN_COMPANY:
                $order->status_id = $order->isPreOrder()
                    ? OrderStatus::STATUS_PRE_ORDER_COMPANY
                    : OrderStatus::STATUS_FREE_FOR_COMPANY;
                break;
            case self::ACTION_COMPLETE_PAY_ORDER:
                $order->status_id = OrderStatus::STATUS_COMPLETED_PAID;
                break;
            case self::ACTION_COMPLETE_NO_PAY_ORDER:
                $order->status_id = OrderStatus::STATUS_COMPLETED_NOT_PAID;
                break;
            case self::ACTION_REJECT_ORDER:
                $order->status_id = $order->status_reject;
        }
    }
}