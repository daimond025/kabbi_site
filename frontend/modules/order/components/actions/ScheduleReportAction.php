<?php

namespace frontend\modules\order\components\actions;

use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\reports\models\orders\exporters\byExport\OrderPdfReportExporter;
use app\modules\tenant\models\User;
use frontend\modules\companies\models\TenantCompany;
use Yii;
use yii\base\Action;

/**
 * All company orders for tomorrow.
 */
class ScheduleReportAction extends Action
{
    public function run()
    {
        $companyId = (int)Yii::$app->request->get('company');
        $from = strtotime('today +1 day');
        $to = $from + 24 * 60 * 60;

        $orders = Order::find()
            ->alias('o')
            ->with([
                'client',
                'worker',
            ])
            ->where(['o.company_id' => $companyId])
            ->andWhere(['not', ['o.worker_id' => null]])
            ->andWhere(['o.status_id' => OrderStatus::getPreOrderStatusId()])
            ->andWhere(['between', 'o.order_time', $from, $to])
            ->orderBy([
                'o.order_time' => SORT_ASC,
            ])
            ->all();

        $tenantCompany = TenantCompany::findOne(['tenant_company_id' => $companyId]);
        $tenantCompany->updateIdReport($tenantCompany);
        $user = User::findOne(['user_id' => $tenantCompany->user_contact]);

        if ($orders) {
            $content = $this->controller->renderPartial('schedule-report', [
                'tenantCompany' => $tenantCompany,
                'user' => $user,
                'orders' => $orders,
                'from' => $from,
                'to' => $to,
            ]);
        } else {
            $content = '';
        }

        $exporter = new OrderPdfReportExporter(
            $content,
            'schedule-' . date('d.m.Y', $from) . '.pdf'
        );
        $exporter->export();
    }
}