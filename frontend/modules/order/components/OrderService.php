<?php

namespace frontend\modules\order\components;

use app\modules\balance\models\Account;
use app\modules\client\models\Client;
use app\modules\client\models\ClientHasCompany;
use app\modules\order\models\Order;
use app\modules\order\models\OrderHasExchangeWorker;
use app\modules\order\models\OrderStatus;
use app\modules\tenant\models\User;
use common\modules\city\models\City;
use app\modules\tariff\models\TaxiTariff;
use app\modules\tariff\models\TaxiTariffHasCompany;
use bonusSystem\BonusSystem;
use common\modules\tenant\models\DefaultSettings;
use common\modules\tenant\models\TenantSetting;
use common\modules\tenant\modules\tariff\models\Module as ARModule;
use frontend\components\repositories\SettingRepository;
use frontend\modules\car\models\Car;
use frontend\modules\car\models\CarColor;
use frontend\modules\companies\models\TenantCompany;
use frontend\modules\employee\components\position\PositionService;
use frontend\modules\employee\components\review\ReviewService;
use frontend\modules\employee\models\worker\Worker;
use frontend\modules\order\helpers\OrderHelper;
use frontend\modules\tenant\models\Currency;
use operatorApi\ApiInterface;
use operatorApi\exceptions\CreateUpdateEventException;
use operatorApi\exceptions\GetUpdateEventResultException;
use yii\base\Object;
use frontend\modules\employee\Module;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class OrderService
 * @package frontend\modules\order\components
 */
class OrderService extends Object
{
    const ORDER_STATUS_FREEZE = 108;

    /* @var PositionService */
    private $positionService;

    /** @var SettingRepository */
    protected $settingRepository;

    public function __construct(
        PositionService $positionService,
        SettingRepository $settingRepository,
        array $config = []
    ) {
        $this->positionService   = $positionService;
        $this->settingRepository = $settingRepository;
        $this->positionService->setTenantId(user()->tenant_id);

        parent::__construct($config);
    }

    /**
     * @param $cityId integer
     *
     * @return bool
     */
    public function isUseSignalNewOrder($cityId)
    {
        if ($cityId) {
            return (bool)$this->settingRepository->getSettingSignalNewOrder($cityId);
        } else {
            $cityIds = array_keys(user()->getUserCityList());

            foreach ($cityIds as $cityId) {
                if ($this->settingRepository->getSettingSignalNewOrder($cityId)) {
                    return true;
                }
            }

            return false;
        }
    }

    public function getCountOrdersForTabs($arFilter)
    {
        $arrayResult = [];

        $arrayResult[OrderStatus::STATUS_GROUP_0] =
            count(Order::getOrdersFromRedis(OrderStatus::STATUS_GROUP_0, $arFilter));

        $arrayResult[OrderStatus::STATUS_GROUP_7] =
            count(Order::getOrdersFromRedis(OrderStatus::STATUS_GROUP_7, $arFilter));

        $arrayResult[OrderStatus::STATUS_GROUP_6] =
            count(Order::getOrdersFromRedis(OrderStatus::STATUS_GROUP_6, $arFilter));

        $arrayResult[OrderStatus::STATUS_GROUP_8] =
            count(Order::getOrdersFromRedis(OrderStatus::STATUS_GROUP_8, $arFilter));

        return $arrayResult;
    }

    public function getOrderNumbersForTabs($arFilter)
    {
        $arrayResult = [];

        $arrayResult[OrderStatus::STATUS_GROUP_0] =
            OrderHelper::getOrderNumbers(Order::getOrdersFromRedis(OrderStatus::STATUS_GROUP_0, $arFilter));

        $arrayResult[OrderStatus::STATUS_GROUP_7] =
            OrderHelper::getOrderNumbers(Order::getOrdersFromRedis(OrderStatus::STATUS_GROUP_7, $arFilter));

        $arrayResult[OrderStatus::STATUS_GROUP_6] =
            OrderHelper::getOrderNumbers(Order::getOrdersFromRedis(OrderStatus::STATUS_GROUP_6, $arFilter));

        $arrayResult[OrderStatus::STATUS_GROUP_8] =
            OrderHelper::getOrderNumbers(Order::getOrdersFromRedis(OrderStatus::STATUS_GROUP_8, $arFilter));

        return $arrayResult;
    }


    /**
     * Send order to exchange
     * @param $tenantId
     * @param $senderId
     * @param $orderId
     * @param $lastUpdateTime
     * @param $lang
     * @return mixed
     */
    public function sendToExchange($tenantId, $senderId, $orderId, $lastUpdateTime, $lang)
    {
        /** @var $operatorApi ApiInterface */
        $operatorApi = app()->operatorApi;

        return $operatorApi->sendToExchange($tenantId, $senderId, $orderId, $lastUpdateTime, $lang);
    }


    /**
     * Stop offer to worker
     *
     * @param int $tenantId
     * @param int $senderId
     * @param int $orderId
     * @param int $lastUpdateTime
     * @param string $lang
     */
    public function stopOffer($tenantId, $senderId, $orderId, $lastUpdateTime, $lang)
    {
        /** @var $operatorApi ApiInterface */
        $operatorApi = app()->operatorApi;

        $operatorApi->stopOffer($tenantId, $senderId, $orderId, $lastUpdateTime, $lang);
    }

    /**
     * take off from worker
     *
     * @param int $tenantId
     * @param int $senderId
     * @param int $orderId
     * @param int $lastUpdateTime
     * @param string $lang
     */
    public function takeOffFromWorker($tenantId, $senderId, $orderId, $lastUpdateTime, $lang)
    {
        /** @var $operatorApi ApiInterface */
        $operatorApi = app()->operatorApi;

        $operatorApi->takeOffFromWorker($tenantId, $senderId, $orderId, $lastUpdateTime, $lang);
    }

    /**
     * take off from company
     *
     * @param int $tenantId
     * @param int $senderId
     * @param int $orderId
     * @param int $lastUpdateTime
     * @param string $lang
     */
    public function takeOffFromCompany($tenantId, $senderId, $orderId, $lastUpdateTime, $lang)
    {
        /** @var $operatorApi ApiInterface */
        $operatorApi = app()->operatorApi;

        $operatorApi->takeOffFromCompany($tenantId, $senderId, $orderId, $lastUpdateTime, $lang);
    }


    /**
     * Getting result of update event
     *
     * @param string $requestId
     *
     * @return array
     * @throws GetUpdateEventResultException
     */
    public function getUpdateEventResult($requestId)
    {
        /** @var $operatorApi ApiInterface */
        $operatorApi = app()->operatorApi;

        return $operatorApi->getUpdateEventResult($requestId);
    }


    /**
     * Getting update event params
     *
     * @param Order $order
     *
     * @return array
     */
    private function getUpdateEventParams($order)
    {
        $order->order_time = $order->getPreparedOrderTime();

        if (empty($order->status_id)) {
            $order->status_id = $order->status_reject;
        }

        $attributes = $order->getAttributes();
        $attributes['address'] = serialize($order->addressFilter($attributes['address']));
        $attributes['additional_option'] = $order->additional_option;

        $oldAttributes = $order->getOldAttributes();

        $oldAttributes['predv_price'] = (float)$oldAttributes['predv_price'];
        $oldAttributes['predv_distance'] = (float)$oldAttributes['predv_distance'];
        $attributes['predv_price'] = (float)$attributes['predv_price'];
        $attributes['predv_distance'] = (float)$attributes['predv_distance'];

        // проверка измнения адресов
        $address_new = unserialize($attributes['address']);
        $address_old = unserialize($oldAttributes['address']);

        $compare = Order::compareAddress($address_new,$address_old );

        $diff = array_diff_assoc($attributes, $oldAttributes);
        if ($compare) {
            $diff['address'] = unserialize($diff['address']);
        }else{
            unset($diff['address']);
        }

        if (in_array($order->status_id, [
            OrderStatus::STATUS_COMPLETED_PAID,
            OrderStatus::STATUS_COMPLETED_NOT_PAID,
        ], false)) {
            $diff['summary_cost'] = $order->summary_cost;
        }

        $params = [];
        foreach ($diff as $key => $value) {
            $params['fields'][$key] = $value;
        }

        if (!empty($params)) {
            $params['last_update_time'] = $order->update_time;
        }

        return $params;
    }

    /**
     * Create update event
     *
     * @param Order $order
     *
     * @return string|null
     * @throws CreateUpdateEventException
     */
    public function createUpdateEvent($order)
    {
        $params = $this->getUpdateEventParams($order);

        if (empty($params)) {
            return null;
        }

        /** @var $operatorApi ApiInterface */
        $operatorApi = app()->operatorApi;
        $user = user();


        return $operatorApi->createUpdateEvent(
            $user->tenant_id, $user->user_id, $order->order_id, $params, $user->lang);
    }


    /**
     * Getting list of positions
     *
     * @param int $cityId
     *
     * @return array
     */
    public function getPositions($cityId = null)
    {
        $positions = $this->positionService->getPositionsByCity($cityId);

        return empty($positions) ? [] : ArrayHelper::map($positions, 'position_id', 'name');
    }

    /**
     * Getting tariff as array
     *
     * @param int $tariffId
     *
     * @return array
     */
    public function getTariff($tariffId)
    {
        $tariff = TaxiTariff::find()->where(['tariff_id' => $tariffId])->asArray()->one();

        if ($tariff) {
            return [
                'tariff_id'   => $tariff['tariff_id'],
                'name'        => $tariff['name'],
                'description' => $tariff['description'],
            ];
        }

        return [];
    }

    public function isCorpBalance($payment)
    {
        return strpos($payment, Order::PAYMENT_CORP) !== false;
    }

    /**
     * @param int $clientId
     * @param int $cityId
     * @param int $positionId
     * @param int $clientId
     * @param int $payment
     * @param int $companyId
     * @param bool $onlyActive
     * @param array $arVisible ['operator', 'cabinet']
     *
     * @return array
     */
    public function getTariffs(
        $cityId,
        $positionId,
        $clientId = null,
        $payment,
        $companyId = null,
        $onlyActive = true,
        array $arVisible = []
    )
    {
        if ($payment === null) {
            return [];
        }
        $query = TaxiTariff::find()
            ->alias('t')
            ->joinWith([
                'tariffHasCity city' => function (Query $query) use ($cityId) {
                    $query->andWhere(['city.city_id' => $cityId]);
                },
                'hasCompanies com',
                'hasCompanies.company.clientHasCompanies cl',
            ], false)
            ->asArray()
            ->orderBy(['t.sort' => SORT_ASC, 't.name' => SORT_ASC]);

        switch ($payment) {
            case Order::PAYMENT_CORP:
                $query->andWhere([
                    'com.company_id' => $companyId,
                    't.type'         => [TaxiTariff::TYPE_COMPANY, TaxiTariff::TYPE_COMPANY_CORP_BALANCE],
                    'cl.client_id'   => $clientId,
                ])
                    ->orWhere([
                        't.type' => TaxiTariff::TYPE_ALL,
                    ]);
                break;
            case Order::PAYMENT_CASH:
            case Order::PAYMENT_CARD:
            case Order::PAYMENT_PERSON:

                $query->andWhere([
                    't.type' => [TaxiTariff::TYPE_BASE, TaxiTariff::TYPE_ALL],
                ])
                    ->orWhere([
                        't.type'       => TaxiTariff::TYPE_COMPANY,
                        'cl.client_id' => $clientId,
                    ]);
                break;
            default:
                return [];
        }

        if (in_array('operator', $arVisible, false)) {
            $query->andWhere(['t.enabled_operator' => 1]);
        }

        if (in_array('cabinet', $arVisible, false)) {
            $query->andWhere(['t.enabled_cabinet' => 1]);
        }

        $query->andWhere([
            't.tenant_id'   => user()->tenant_id,
            't.position_id' => $positionId,
        ])
            ->andFilterWhere(['t.block' => $onlyActive ? 0 : null]);

        $tariffs = $query->all();

        return empty($tariffs) ? [] : array_map(function ($tariff) {
            return [
                'tariff_id'   => $tariff['tariff_id'],
                'name'        => $tariff['name'],
                'description' => $tariff['description'],
            ];
        }, $tariffs);

    }

    /**
     * Getting bonus balance
     *
     * @param int $clientId
     * @param int $currencyId
     *
     * @return int
     */
    private function getBonusBalance($clientId, $currencyId)
    {
        try {
            $bonusSystem = \Yii::createObject(BonusSystem::class, [user()->tenant_id]);

            return $bonusSystem->getBalance($clientId, $currencyId);
        } catch (\yii\base\Exception $ex) {
            return 0;
        }
    }

    /**
     * Getting URL without host
     *
     * @param string $url
     *
     * @return string
     */
    private function getUrlWithoutHost($url)
    {
        $parsedUrl = parse_url($url);

        return $parsedUrl === false ? $url : $parsedUrl['path'] . '?' . $parsedUrl['query'];
    }

    /**
     * Getting short name
     *
     * @param string $firstName
     * @param string $name
     * @param string $secondName
     *
     * @return string
     */
    public function getShortName($firstName, $name, $secondName)
    {
        $parts = [$firstName];
        if (!empty($name)) {
            $parts[] = mb_substr($name, 0, 1, 'UTF-8') . '.';
        }
        if (!empty($secondName)) {
            $parts[] = mb_substr($secondName, 0, 1, 'UTF-8') . '.';
        }

        return implode(' ', $parts);
    }

    /**
     * Getting phone without country cody
     *
     * @param string $phone
     *
     * @return string
     */
    private function getPhoneWithoutCountryCode($phone)
    {
        return in_array($phone[0], ['7', '8'], true)
            ? mb_substr($phone, 1) : $phone;
    }

    /**
     * Search clients
     *
     * @param int $tenantId
     * @param string $searchString
     *
     * @return array
     */
    public function searchClients($tenantId, $searchString)
    {
        if (empty($searchString)) {
            return [];
        }

        $phone = str_replace('+', '', $searchString);
        $phoneWithoutCountryCode = $this->getPhoneWithoutCountryCode($phone);

        $partsSearchString = preg_split('#\s+#', trim($searchString));
        $countPartsSearchString = count($partsSearchString) > 3 ? 3 : count($partsSearchString);


        $results = [];
        for ($i = 0; $i < $countPartsSearchString; $i++) {
            $query = Client::find()
                ->alias('t')
                ->select([
                    'clientId'   => 't.client_id',
                    'lastName'   => 't.last_name',
                    't.name',
                    'secondName' => 't.second_name',
                    'phone'      => 'c.value',
                    't.photo',
                    't.black_list',
                ])
                ->joinWith('clientPhones c', false)
                ->where(['t.tenant_id' => $tenantId])
                ->andFilterWhere([
                    'OR',
                    ['like', 't.last_name', $partsSearchString[$i]],
                    ['like', 't.name', $partsSearchString[$i]],
                    ['like', 't.second_name', $partsSearchString[$i]],
                    [
                        'or',
                        ['like', 'c.value', $phone],
                        ['like', 'c.value', $phoneWithoutCountryCode],
                    ],
                ])
                ->asArray()
                ->groupBy(['t.client_id'])
                ->orderBy(['c.value' => SORT_ASC, 't.last_name' => SORT_ASC])
                ->limit(50);

            $results[] = ArrayHelper::index($query->all(), 'clientId');
        }

        switch (count($results)) {
            case 1:
                $clients = array_shift($results);
                break;
            case 2:
                $clients = array_intersect_key($results[0], $results[1]);
                break;
            case 3:
                $clients = array_intersect_key($results[0], $results[1], $results[2]);
                break;
        }

        $clients = empty($clients) ? [] : array_values($clients);

        $client = new Client();
        $behavior = $client->getBehavior('fileBehavior');
        if ($behavior !== null) {
            $behavior->action = 'show-external-file';
            $behavior->tenant_id = $tenantId;
        }

        return array_map(function ($model) use ($client) {
            $photo = $client->isFileExists($model['photo'])
                ? $client->getPicturePath(Html::encode($model['photo'])) : null;

            $result = [
                'clientId'   => isset($model['clientId']) ? $model['clientId'] : null,
                'lastName'   => isset($model['lastName']) ? Html::encode($model['lastName']) : null,
                'name'       => isset($model['lastName']) ? Html::encode($model['name']) : null,
                'secondName' => isset($model['secondName']) ? Html::encode($model['secondName']) : null,
                'phone'      => isset($model['phone']) ? Html::encode($model['phone']) : null,
                'photo'      => $photo === null ? null : $this->getUrlWithoutHost($photo),
                'banned'     => isset($model['black_list']) ? (bool)$model['black_list'] : false,
            ];
            $result['shortName'] = $this->getShortName($result['lastName'], $result['name'], $result['secondName']);

            return $result;
        }, $clients);
    }

    /**
     * @param int $companyId
     * @param int $currencyId
     *
     * @return float
     */
    private function getCompanyBalance($companyId, $currencyId)
    {
        $balance = Account::find()
            ->select('balance')
            ->where([
                'acc_kind_id' => Account::COMPANY_KIND,
                'acc_type_id' => Account::PASSIVE_TYPE,
                'owner_id'    => $companyId,
                'currency_id' => $currencyId,
            ])
            ->asArray()
            ->scalar();

        return empty($balance) ? 0 : +$balance;
    }

    /**
     * Getting available payments
     *
     * @param array $companies
     * @param int $currencyId
     * @param string $currencySymbol
     *
     * @return array
     */
    private function getAvailablePayments($companies, $currencyId, $currencySymbol)
    {
        $payments = [];
        if (is_array($companies)) {
            $formatter = \Yii::$app->formatter;
            foreach ($companies as $company) {
                $payments[] = [
                    'type'      => Order::PAYMENT_CORP,
                    'name'      => Html::encode($company->name),
                    'companyId' => $company->company_id,
                    'balance'   => $formatter->asMoney($this->getCompanyBalance($company->company_id, $currencyId),
                        $currencySymbol),
                    'credit'    => $formatter->asMoney($company->credits, $currencySymbol),
                ];
            }
        }

        $payments[] = ['type' => Order::PAYMENT_CASH, 'name' => t('order', 'Cash')];
        $payments[] = ['type' => Order::PAYMENT_PERSON, 'name' => t('order', 'Personal account')];
        $payments[] = ['type' => Order::PAYMENT_CARD, 'name' => t('order', 'Bank card')];

        return $payments;
    }

    /**
     * Getting client information by phone
     *
     * @param int $tenantId
     * @param int $cityId
     * @param string $phone
     *
     * @return array
     */
    public function getClientInformationByPhone($tenantId, $cityId, $phone)
    {
        return $this->getClientInformation($tenantId, $cityId, $phone, null);
    }

    /**
     * Getting client information by id
     *
     * @param int $tenantId
     * @param int $cityId
     * @param int $clientId
     *
     * @return array
     */
    public function getClientInformationById($tenantId, $cityId, $clientId)
    {
        return $this->getClientInformation($tenantId, $cityId, null, $clientId);
    }

    /**
     * Getting client information by id
     *
     * @param int $tenantId
     * @param int $cityId
     * @param string $phone
     * @param int $clientId
     *
     * @return array
     */
    private function getClientInformation($tenantId, $cityId, $phone = null, $clientId = null)
    {
        $query = Client::find()
            ->alias('t')
            ->joinWith(['clientPhones p'])
            ->with([
                'companies' => function ($subQuery) {
                    /** @var ActiveQuery $subQuery */
                    $subQuery->select(['company_id', 'name', 'credits']);
                    $subQuery->where(['block' => 0]);
                },
            ])
            ->where(['t.tenant_id' => $tenantId])
            ->select([
                't.client_id',
                't.tenant_id',
                't.last_name',
                't.name',
                't.second_name',
                't.photo',
                'phone' => 'p.value',
                't.black_list',
                't.success_order',
                't.fail_worker_order',
                't.fail_client_order',
                't.fail_dispatcher_order',
            ]);

        if ($phone !== null) {
            $query->andWhere([
                'or',
                ['p.value' => str_replace('+', '', $phone)],
            ]);
        } else {
            $query->andWhere(['t.client_id' => $clientId]);
        }
        /* @var $client Client */
        $client = $query->one();

        if (empty($client)) {
            return null;
        }

        $behavior = $client->getBehavior('fileBehavior');
        if ($behavior !== null) {
            $behavior->action = 'show-external-file';
            $behavior->tenant_id = $tenantId;
        }

        $photo = $client->isFileExists($client->photo)
            ? $client->getPicturePath(Html::encode($client->photo)) : null;

        $currencyId = TenantSetting::getSettingValue(
            $tenantId, DefaultSettings::SETTING_CURRENCY, $cityId);
        $currencySymbol = Currency::getCurrencySymbol($currencyId);
        $account = $client->getAccountByCurrencyId($currencyId);
        $bonusBalance = $this->getBonusBalance($client->client_id, $currencyId);

        $result['clientId'] = $client->client_id;
        $result['phone'] = $client->phone;
        $result['lastName'] = isset($client->last_name) ? Html::encode($client->last_name) : null;
        $result['name'] = isset($client->name) ? Html::encode($client->name) : null;
        $result['secondName'] = isset($client->second_name) ? Html::encode($client->second_name) : null;
        $result['shortName'] = $this->getShortName($result['lastName'], $result['name'], $result['secondName']);
        $result['banned'] = (bool)$client->black_list;
        $result['photo'] = $photo === null ? null : $this->getUrlWithoutHost($photo);
        $result['link'] = Url::to(['/client/base/update', 'id' => $client->client_id]);
        $result['successOrders'] = +$client->success_order;
        $result['failedOrders'] = $client->fail_worker_order + $client->fail_client_order + $client->fail_dispatcher_order;
        $result['balance'] = \Yii::$app->formatter->asMoney(
            empty($account->balance) ? 0 : +$account->balance, $currencySymbol);
        $result['bonus'] = \Yii::$app->formatter->asMoney($bonusBalance,
            t('currency', 'B') . '(' . $currencySymbol . ')');
        $result['payments'] = $this->getAvailablePayments($client->companies, $currencyId, $currencySymbol);

        return $result;
    }

    /**
     * Search workers
     *
     * @param int $tenantId
     * @param int $cityId
     * @param int $positionId
     * @param string $searchString
     * @param float $latOrder
     * @param float $lonOrder
     *
     * @return array
     */
    public function searchWorkers($tenantId, $cityId, $positionId, $searchString, $latOrder = null, $lonOrder = null)
    {
        $company_id = user()->tenant_company_id;
        $user_edit=  app()->user->can('orders') ? true : false;
        $user_staff =  (app()->user->can(User::ROLE_STAFF) || app()->user->can(User::ROLE_STAFF_COMPANY)) ? true : false;

        $nearWorkersCount = 20;
        $isActionSearch = \Yii::$app->controller->action->id === 'search-workers';
        $isActionNear = \Yii::$app->controller->action->id === 'near-workers';
        if (empty($searchString) && $isActionSearch) {
            return [];
        }
        $workers = \Yii::$app->redis_workers->executeCommand('hvals', [$tenantId]);

        $result = array_filter(array_map(function ($model) use (
            $cityId,
            $positionId,
            $searchString,
            $isActionSearch,
            $isActionNear,
            $latOrder,
            $lonOrder
        ) {
            $worker = unserialize($model);
            $workerCityId = isset($worker['worker']['city_id']) ? $worker['worker']['city_id'] : null;
            $workerCityIds = isset($worker['worker']['cities']) ? ArrayHelper::getColumn($worker['worker']['cities'],
                'city_id') : [$workerCityId];
            $workerStatus = isset($worker['worker']['status']) ? $worker['worker']['status'] : null;
            $positionIds = isset($worker['position']['positions']) ? $worker['position']['positions'] : [];
            $positionIds[] = $positionId;

            if (in_array($cityId, $workerCityIds, false) && in_array($positionId, $positionIds, false)) {
                $workerId = isset($worker['worker']['worker_id']) ? $worker['worker']['worker_id'] : null;
                $callsign = isset($worker['worker']['callsign']) ? $worker['worker']['callsign'] : null;
                $lastName = isset($worker['worker']['last_name']) ? $worker['worker']['last_name'] : null;
                $name = isset($worker['worker']['name']) ? $worker['worker']['name'] : null;
                $secondName = isset($worker['worker']['second_name']) ? $worker['worker']['second_name'] : null;
                $photo = isset($worker['worker']['photo_url']) ? $worker['worker']['photo_url'] : null;
                $carId = isset($worker['car']['car_id']) ? $worker['car']['car_id'] : null;
                $carName = isset($worker['car']['name']) ? $worker['car']['name'] : null;
                $carColor = isset($worker['car']['color']) ? $worker['car']['color'] : null;
                $gosNumber = isset($worker['car']['gos_number']) ? $worker['car']['gos_number'] : null;
                $lat = isset($worker['geo']['lat']) ? (float)$worker['geo']['lat'] : null;
                $lon = isset($worker['geo']['lon']) ? (float)$worker['geo']['lon'] : null;

                $company_id= isset($worker['worker']['tenant_company_id']) ? (int)$worker['worker']['tenant_company_id'] : '';

                $distance = \Yii::$app->geoService->getDistance($latOrder, $lonOrder, $lat, $lon);
                $checkSubstr = false;
                if ($isActionSearch) {
                    $checkSubstr = mb_stripos($callsign, $searchString) !== false
                        || mb_stripos($lastName, $searchString) !== false
                        || mb_stripos($name, $searchString) !== false
                        || mb_stripos($secondName, $searchString) !== false
                        || mb_stripos($carName, $searchString) !== false
                        || mb_stripos($gosNumber, $searchString) !== false;
                }
                if (($isActionSearch && $checkSubstr) || $isActionNear) {
                    $result = [
                        'workerId'   => $workerId,
                        'callsign'   => (int)$callsign,
                        'lastName'   => isset($lastName) ? Html::encode($lastName) : null,
                        'name'       => isset($name) ? Html::encode($name) : null,
                        'secondName' => isset($secondName) ? Html::encode($secondName) : null,
                        'photo'      => $photo === null ? null : $this->getUrlWithoutHost($photo),
                        'car'        => [
                            'carId'     => $carId,
                            'name'      => Html::encode($carName),
                            'color'     => t('car', $carColor),
                            'gosNumber' => Html::tag('span', Html::encode($gosNumber), ['dir' => 'auto']),
                        ],
                        'lat'        => $lat,
                        'lon'        => $lon,
                        'distance'   => $distance,
                        'status'     => $this->getWorkerStatusType($workerStatus),
                        'company_id' => $company_id
                    ];
                    $result['shortName'] = $this->getShortName($result['lastName'], $result['name'],
                        $result['secondName']);

                    return $result;
                }

            }
        }, $workers), function($element) use (
            $company_id,
            $user_edit,
            $user_staff
        ) {
            if(is_null($element)){
                return false;
            }

            if($user_edit){
                return true;
            }
            elseif($user_staff){
                if($element['company_id'] == $company_id ){
                    return true;
                }
            }
            return false;
        });


        $company_id = null;
        if($user_staff && !$user_edit){
            $company_id =user()->tenant_company_id;
        }

        // выборка данных из БД водителей - не берем водителей у которых нет машин - машина по умолчниаю только первая
        $workers =  Worker::find()
            ->alias('w')
            //   ->select(['w.*', 'city.*' , 'position.*' ])
            ->leftJoin('{{%worker_has_city}}', '{{%worker_has_city}}.worker_id = w.worker_id')
            ->leftJoin('{{%worker_has_position}}', '{{%worker_has_position}}.worker_id = w.worker_id')
            ->where(['w.block' => 0])
            ->andWhere(['{{%worker_has_city}}.city_id' =>  $cityId])
            ->andWhere(['{{%worker_has_position}}.position_id' =>  $positionId])
            ->andFilterWhere(['like', 'w.callsign' , $searchString])
            ->andFilterWhere(['w.tenant_company_id' => $company_id])
            ->all();


        foreach ($workers as $worker){

            // смена водителя
            $shiftWorker = $worker->getLastWorkerShift($worker->worker_id);

            if(!$shiftWorker){
                continue;
            }

            $car_number = $this->selectCarShift($worker->cars,$shiftWorker->car_id );

            if(isset($worker->cars[$car_number]->car) && !is_null($worker->cars[$car_number]->car) && isset($worker->worker_id) && isset($worker->callsign) && !$this->WorkerOnShift($worker->callsign,$result )){

                $car = $worker->cars[0]->car;
                $car_value        = [
                    'carId'     => $car->car_id,
                    'name'      => Html::encode($car->name),
                    'color'     => t('car', $car->colorText),
                    'gosNumber' => Html::tag('span', Html::encode($car->gos_number), ['dir' => 'auto']),
                ];


                $result_add = [
                    'workerId'   => $worker->worker_id,
                    'callsign'   => (int)$worker->callsign,
                    'lastName'   => isset($worker->last_name) ? Html::encode($worker->last_name) : null,
                    'name'       => isset($worker->name) ? Html::encode($worker->name) : null,
                    'secondName' => isset($worker->second_name) ? Html::encode($worker->second_name) : null,
                    'photo'      => ($worker->photo === null || empty($worker->photo)) ? null : $this->getUrlWithoutHost($worker->photo),
                    'car'        => $car_value,
                    'lat'        => null,
                    'lon'        => null,
                    'distance'   => '',
                    'status'     => $this->getWorkerStatusType(Worker::STATUS_BLOCKED)
                ];
                $result_add['shortName'] = $this->getShortName($result_add['lastName'], $result_add['name'],$result_add['secondName']);



                $result[] = $result_add;
            }
        }


        if ($isActionNear) {
            usort($result, function ($a, $b) {
                return self::compareWorkers($a, $b, 'distance');
            });
        } else {
            usort($result, function ($a, $b) {
                return self::compareWorkers($a, $b, 'callsign');
            });
        }

        return array_slice($result, 0, $nearWorkersCount);
    }

    /**
     * @param
     * int $WorkerShift
     * @return WorkerHasCar
     */
    public  function selectCarShift($cars, $workerCarShift)
    {
        $car_last_shift = null;

        for($i = 0; $i < count($cars); $i++ ){
            $car = $cars[$i];
            if(intval($car->car_id) == $workerCarShift){
                $car_last_shift = $i;
                break;
            }
        }
        return $car_last_shift;

    }

    public function WorkerOnShift($callsign, $rezults){
        foreach ($rezults as $rezult){
            if($rezult['callsign'] == $callsign){
                return true;
            }
        }

        return false;

    }

    /**
     * @param $workerStatus
     * @return string
     */
    private function getWorkerStatusType($workerStatus)
    {
        switch ($workerStatus) {
            case  Worker::STATUS_FREE:
                $type = 'free_car';
                break;
            case  Worker::STATUS_BLOCKED:
                $type = 'pause_car';
                break;
            case  Worker::STATUS_ON_BREAK:
                $type = 'pause_car';
                break;
            case  Worker::STATUS_OFFER_ORDER:
                $type = 'offer_car';
                break;
            case  Worker::STATUS_ON_ORDER:
                $type = 'busy_car';
                break;
            default:
                $type = 'free_car';
                break;
        }
        return $type;
    }

    private static function compareWorkers($worker1, $worker2, $compareKey)
    {
        if ($worker1[$compareKey] === $worker2[$compareKey]) {
            return 0;
        }

        return ($worker1[$compareKey] < $worker2[$compareKey]) ? -1 : 1;
    }

    /**
     * Is order processed through the external exchange
     * @param $tenantId
     * @param $orderId
     * @return bool
     */
    public static function isActiveOrderProcessingThroughTheExternalExchange($tenantId, $orderId)
    {
        $activeOrder = Order::getOrderFromRedis($tenantId, $orderId);
        return ArrayHelper::getValue($activeOrder, 'exchange_info.order', false) ? true : false;
    }

    /**
     * Get worker info from external exchange processed order
     * @param $tenantId
     * @param $orderId
     * @return array|null
     */
    public static function getWorkerInfoFromExternalExchangeProcessedOrder($tenantId, $orderId)
    {
        if (!self::isActiveOrderProcessingThroughTheExternalExchange($tenantId, $orderId)) {
            return null;
        }
        $activeOrder = Order::getOrderFromRedis($tenantId, $orderId);
        return ArrayHelper::getValue($activeOrder, 'exchange_info.order.worker', null);
    }

    /**
     * Get car info from external exchange processed order
     * @param $tenantId
     * @param $orderId
     * @return array|null
     */
    public static function getCarInfoFromExternalExchangeProcessedOrder($tenantId, $orderId)
    {
        if (!self::isActiveOrderProcessingThroughTheExternalExchange($tenantId, $orderId)) {
            return null;
        }
        $activeOrder = Order::getOrderFromRedis($tenantId, $orderId);
        return ArrayHelper::getValue($activeOrder, 'exchange_info.order.car', null);
    }

    public static function getWorkerInformationFromExternalExchange($orderId)
    {
        $workerData = OrderHasExchangeWorker::find()
            ->where(['order_id' => $orderId])
            ->one();

        if (!$workerData) {
            return null;
        }

        $result = [
            'workerId'   => $workerData['external_worker_id'],
            'lastName'   => null,
            'name'       => $workerData['name'],
            'shortName'  => $workerData['name'],
            'secondName' => null,
            'callsign'   => null,
            'photo'      => null,
            'link'       => null,
            'car'        => [
                'carId'     => $workerData['external_car_id'],
                'name'      => Html::encode($workerData['car_description']),
                'color'     => t('car', $workerData['color']),
                'gosNumber' => Html::tag('span', Html::encode($workerData['gos_number']), ['dir' => 'auto']),
            ],
            'rating'     => 0,
            'balance'    => 0,
            'lat'        => null,
            'lon'        => null,
            'phone'      => $workerData['phone'],
        ];
        return $result;
    }

    public static function getActiveWorkerInformationFromExternalExchange($tenantId, $orderId)
    {
        $worker = self::getWorkerInfoFromExternalExchangeProcessedOrder($tenantId, $orderId);
        $car = self::getCarInfoFromExternalExchangeProcessedOrder($tenantId, $orderId);
        $result = [
            'workerId'   => null,
            'lastName'   => null,
            'name'       => $worker['name'],
            'shortName'  => $worker['name'],
            'secondName' => null,
            'callsign'   => null,
            'photo'      => null,
            'link'       => null,
            'car'        => [
                'carId'     => null,
                'name'      => Html::encode($car['name']),
                'color'     => t('car', $car['color']),
                'gosNumber' => Html::tag('span', Html::encode($car['gos_number']), ['dir' => 'auto']),
            ],
            'rating'     => 0,
            'balance'    => 0,
            'lat'        => $car['lat'],
            'lon'        => $car['lon'],
            'phone'      => $worker['phone'],
        ];
        return $result;
    }

    /**
     * Getting worker information
     *
     * @param int $tenantId
     * @param int $company_id
     *
     * @return array
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\InvalidConfigException
     * */
    public function getCompanyInfo($tenantId,$company_id ){

        if (empty($company_id)) {
            return '';
        }

        $company = TenantCompany::find()
            ->select([
                'tenant_company_id',
                'name',
                'phone',
                'user_contact',
            ])
            ->where([
                'tenant_company_id' => $company_id,
                'block' => 0,
            ])
            ->asArray()
            ->one();

        $userName = '';
        if($company['user_contact']){
            $user = User::find()
                ->where([
                    'user_id' => $company['user_contact']
                ])
                ->select([
                    'last_name',
                    'name',
                    'second_name',
                    ])
                ->asArray()
                ->one();
            $userName =  $this->getShortName($user['last_name'], $user['name'], $user['second_name']);
        }


        $result = [
            'companyId'   => $company['tenant_company_id'],
            'name'       => $company['name'],
            'phone'       => $company['phone'],
            'link'       => Url::to('/companies/company/update/' . $company['tenant_company_id']),
            'userName'       => $userName,
        ];

        return $result;
    }

    /**
     * Getting worker information
     *
     * @param int $tenantId
     * @param int $workerId
     * @param int $carId
     * @param int $cityId
     * @param int $positionId
     *
     * @return array
     * @throws \yii\base\InvalidParamException
     * @throws \yii\base\InvalidConfigException
     */
    public function getWorkerInformation($tenantId, $workerId, $carId, $cityId, $positionId)
    {
        $worker = Worker::find()
            ->select([
                'worker_id',
                'tenant_id',
                'callsign',
                'last_name',
                'name',
                'second_name',
                'photo',
                'phone',
            ])
            ->where([
                'worker_id' => $workerId,
                'tenant_id' => $tenantId,
            ])
            ->one();

        if (empty($worker)) {
            return null;
        }

        $car = Car::find()
            ->select([
                'car_id',
                'name',
                'gos_number',
                'color',
            ])
            ->where([
                'car_id'    => $carId,
                'tenant_id' => $tenantId,
            ])
            ->one();

        $phone = isset($worker['phone']) ? $worker['phone'] : null;
        $callsign = isset($worker['callsign']) ? $worker['callsign'] : null;
        $lastName = isset($worker['last_name']) ? $worker['last_name'] : null;
        $name = isset($worker['name']) ? $worker['name'] : null;
        $secondName = isset($worker['second_name']) ? $worker['second_name'] : null;
        $carName = isset($car['name']) ? $car['name'] : null;
        $carColor = isset($car['colorText']) ? $car['colorText'] : null;
        $gosNumber = isset($car['gos_number']) ? $car['gos_number'] : null;

        $behavior = $worker->getBehavior('fileBehavior');
        if ($behavior !== null) {
            $behavior->action = 'show-external-file';
            $behavior->tenant_id = $tenantId;
        }
        $photo = $worker->isFileExists($worker->photo)
            ? $worker->getPicturePath(Html::encode($worker->photo)) : null;

        /* @var $reviewService ReviewService */
        $reviewService = \Yii::createObject(ReviewService::className());
        $ratingModel = $reviewService->getWorkerReviewRating($workerId, $positionId);
        $rating = !empty($ratingModel) && $ratingModel->count > 0
            ? $reviewService->getAverageValue($ratingModel) : null;

        $currencyId = TenantSetting::getSettingValue($tenantId, DefaultSettings::SETTING_CURRENCY, $cityId);
        $currencySymbol = Currency::getCurrencySymbol($currencyId);
        $account = $worker->getAccountByCurrencyId($currencyId);
        $balance = \Yii::$app->formatter->asMoney(
            empty($account->balance) ? 0 : +$account->balance, $currencySymbol);

        $workerRedis = \Yii::$app->redis_workers->executeCommand('hget', [$tenantId, $callsign]);
        $workerRedis = unserialize($workerRedis);
        $lat = isset($workerRedis['geo']['lat']) ? (float)$workerRedis['geo']['lat'] : null;
        $lon = isset($workerRedis['geo']['lon']) ? (float)$workerRedis['geo']['lon'] : null;

        $result = [
            'workerId'   => $workerId,
            'lastName'   => isset($lastName) ? Html::encode($lastName) : null,
            'name'       => isset($name) ? Html::encode($name) : null,
            'secondName' => isset($secondName) ? Html::encode($secondName) : null,
            'callsign'   => (int)$callsign,
            'photo'      => $photo === null ? null : $this->getUrlWithoutHost($photo),
            'link'       => Url::to(['/employee/worker/update', 'id' => $workerId]),
            'car'        => [
                'carId'     => $carId,
                'name'      => Html::encode($carName),
                'color'     => t('car', $carColor),
                'gosNumber' => Html::tag('span', Html::encode($gosNumber), ['dir' => 'auto']),
            ],
            'rating'     => $rating,
            'balance'    => $balance,
            'lat'        => $lat,
            'lon'        => $lon,
            'phone'      => $phone,
        ];
        $result['shortName'] = $this->getShortName($result['lastName'], $result['name'],
            $result['secondName']);

        return $result;
    }

    public static function getAllOrdersByFields($filter, $date = null, $cityId = null)
    {

        $cityTimeoffset = 0;

        if (empty($cityId)) {
            $cityId = array_keys(user()->getUserCityList());
        } else {
            $cityTimeoffset = City::getTimeOffset($cityId);
        }

        if (empty($date)) {
            $date = date('Y-m-d');
        }

        list($year, $month, $day) = explode('-', $date);
        $first_time = mktime(0, 0, 0, $month, $day, $year);
        $second_time = mktime(23, 59, 59, $month, $day, $year);

        $sql = [];
        $sql[] = "create_time >= $first_time AND create_time <= $second_time";
        if (isset($cityTimeoffset)) {
            $sql[] = "status_time >= $first_time - $cityTimeoffset AND status_time <= $second_time - $cityTimeoffset";
            $sql[] = "order_time >= $first_time + $cityTimeoffset AND order_time <= $second_time + $cityTimeoffset";

        } else {
            $sql[] = "status_time >= $first_time AND status_time <= $second_time";
            $sql[] = "order_time >= $first_time  AND order_time <= $second_time";
        }

        $query = Order::find()->alias('o')->where(['tenant_id' => user()->tenant_id])
            ->joinWith([
                'status s' => function ($query) use ($filter) {
                    if ($filter['tab'] !== '') {
                        if ($filter['tab'] === 'works') {
                            $statusArray = ArrayHelper::merge(OrderStatus::getFilterStatusList(OrderStatus::STATUS_GROUP_1),
                                OrderStatus::getFilterStatusList(OrderStatus::STATUS_GROUP_2),
                                OrderStatus::getFilterStatusList(OrderStatus::STATUS_GROUP_3));
                        } else {
                            $statusArray = OrderStatus::getFilterStatusList($filter['tab']);
                        }
                        $query->andWhere(['IN', 's.name', $statusArray]);
                    }
                },
            ])
            ->andWhere(['OR', ['o.status_id' => self::ORDER_STATUS_FREEZE], $sql[0], $sql[1], $sql[2]])
            ->andWhere(['city_id' => $cityId])
            ->andWhere([
                'OR',
                ['like', 'phone', $filter['searchString']],
                ['like', 'order_number', $filter['searchString']],
            ])
            ->orderBy('order_id DESC');

        if (app()->user->can(User::ROLE_STAFF_COMPANY)) {
            $query->andWhere(['o.tenant_company_id' => user()->tenant_company_id]);
        }

        return $query->all();
    }

    public function getNewOrders($cityId, $dateFilter)
    {
        return $this->getActiveOrders(OrderStatus::STATUS_GROUP_0, $cityId, $dateFilter);
    }

    public function getWorkingOrders($cityId, $dateFilter)
    {
        return $this->getActiveOrders(OrderStatus::STATUS_GROUP_8, $cityId, $dateFilter);
    }

    public function getWarningOrders($cityId, $dateFilter)
    {
        $orders = $this->getActiveOrders(OrderStatus::STATUS_GROUP_7, $cityId, $dateFilter);
        $excludedOrders = ArrayHelper::getColumn($orders, 'order_id');

        return array_merge(
            $orders,
            Order::getByStatusGroup(OrderStatus::STATUS_GROUP_7, $cityId, $dateFilter, [], [], $excludedOrders)
        );
    }

    public function getPreOrders($cityId, $dateFilter)
    {
        return $this->getActiveOrders(OrderStatus::STATUS_GROUP_6, $cityId, $dateFilter, SORT_ASC, 'order_time');
    }

    public function getRejectedOrders($cityId, $dateFilter)
    {
        return Order::getByStatusGroup(OrderStatus::STATUS_GROUP_5, $cityId, $dateFilter);
    }

    private function getActiveOrders($group, $cityId, $dateFilter, $sort = SORT_DESC, $attr = 'order_id')
    {
        return Order::getOrdersFromRedis($group, ['city_id' => $cityId, 'date' => $dateFilter], $sort, $attr);
    }

    public function getCompletedOrders($cityId, $date)
    {
        $cityTimeoffset = City::getTimeOffset($cityId);

        if ($timestamp = strtotime($date)) {
            $date_timestamp = isset($cityTimeoffset) ? $timestamp + $cityTimeoffset : $timestamp;
        }
        $filterDate = isset($date_timestamp) ? date('d.m.Y', $date_timestamp) : null;
        $key = 'or_completed_' . user()->tenant_id . '_' . $cityId . '_' . user()->getId() ;

        //Today orders
        if (is_null($filterDate) || $filterDate == date('d.m.Y')) {

            $key .= 'today';
            $statuses = implode(', ', OrderStatus::getCompletedStatusId());
            $callback = function () use ($cityId) {
                return Order::getByStatusGroup(OrderStatus::STATUS_GROUP_4, $cityId);
            };

            $orders = Order::getTodayNoActiveOrdersFromCache($key, $statuses, $callback, 600, $cityId);

        } else {
            $orders = Order::getByStatusGroup(OrderStatus::STATUS_GROUP_4, $cityId, $filterDate);
        }

        return $orders;
    }

    public function getNewCompletedOrders($cityId, $date)
    {
        $cityTimeoffset = City::getTimeOffset($cityId);

        if ($timestamp = strtotime($date)) {
            $date_timestamp = isset($cityTimeoffset) ? $timestamp + $cityTimeoffset : $timestamp;
        }
        $filterDate = isset($date_timestamp) ? date('d.m.Y', $date_timestamp) : null;

        $orders = Order::getByStatusGroupQuery(OrderStatus::STATUS_GROUP_4, $cityId, $filterDate);

        return $orders;
    }

    public function getPreOrdersData($cityId, $date_from, $date_to = null)
    {
        $cityTimeoffset = City::getTimeOffset($cityId);

        if ($timestamp = strtotime($date_from)) {
            $date_timestamp_from = isset($cityTimeoffset) ? $timestamp + $cityTimeoffset : $timestamp;
        }
        $filterDate['date_from'] = isset($date_timestamp_from) ? date('d.m.Y', $date_timestamp_from) : null;

        if ($timestamp = strtotime($date_to)) {
            $date_timestamp_to = isset($cityTimeoffset) ? $timestamp + $cityTimeoffset : $timestamp;
        }
        $filterDate['date_to'] = isset($date_timestamp_to) ? date('d.m.Y', $date_timestamp_to) : null;

        $filterDate['device_not'] = Order::DEVICE_HOSPITAL;
        $orders = Order::getByStatusGroupQuery(OrderStatus::STATUS_GROUP_6, $cityId, $filterDate);

        return $orders;
    }
    public function getHospitalOrdersData($cityId, $date_from, $date_to = null)
    {
        $cityTimeoffset = City::getTimeOffset($cityId);

        if ($timestamp = strtotime($date_from)) {
            $date_timestamp_from = isset($cityTimeoffset) ? $timestamp + $cityTimeoffset : $timestamp;
        }
        $filterDate['date_from'] = isset($date_timestamp_from) ? date('d.m.Y', $date_timestamp_from) : null;

        if ($timestamp = strtotime($date_to)) {
            $date_timestamp_to = isset($cityTimeoffset) ? $timestamp + $cityTimeoffset : $timestamp;
        }
        $filterDate['date_to'] = isset($date_timestamp_to) ? date('d.m.Y', $date_timestamp_to) : null;
        $filterDate['device'] = Order::DEVICE_HOSPITAL;
        $orders = Order::getByStatusGroupQuery(OrderStatus::STATUS_GROUP_6, $cityId, $filterDate);

        return $orders;
    }

    public function getNewOrdersData($cityId)
    {
        $filterDate['data_no'] = true;
        $status = self::filterByStatus();
        $orders = Order::getByStatusGroupQuery(OrderStatus::STATUS_GROUP_0, $cityId, $filterDate, $status);

        return $orders;
    }

    public function getWorkeOrdersData($cityId )
    {
        $filterDate['data_no'] = true;
        $filterDate['all_data'] = true;
        $status = self::filterByStatus();
        $orders = Order::getByStatusGroupQuery(OrderStatus::STATUS_GROUP_8, $cityId, $filterDate, $status);

        return $orders;
    }

    public function getWarningOrdersData($cityId, $date_from, $date_to = null )
    {
        $cityTimeoffset = City::getTimeOffset($cityId);

        if ($timestamp = strtotime($date_from)) {
            $date_timestamp_from = isset($cityTimeoffset) ? $timestamp + $cityTimeoffset : $timestamp;
        }
        $filterDate['date_from'] = isset($date_timestamp_from) ? date('d.m.Y', $date_timestamp_from) : null;

        if ($timestamp = strtotime($date_to)) {
            $date_timestamp_to = isset($cityTimeoffset) ? $timestamp + $cityTimeoffset : $timestamp;
        }
        $filterDate['date_to'] = isset($date_timestamp_to) ? date('d.m.Y', $date_timestamp_to) : null;
        $status = self::filterByStatus();
        $orders = Order::getByStatusGroupQuery(OrderStatus::STATUS_GROUP_7, $cityId, $filterDate, $status);

        return $orders;
    }

    public function getComplateOrdersData($cityId, $date_from, $date_to = null )
    {
        $cityTimeoffset = City::getTimeOffset($cityId);

        if ($timestamp = strtotime($date_from)) {
            $date_timestamp_from = isset($cityTimeoffset) ? $timestamp + $cityTimeoffset : $timestamp;
        }
        $filterDate['date_from'] = isset($date_timestamp_from) ? date('d.m.Y', $date_timestamp_from) : null;

        if ($timestamp = strtotime($date_to)) {
            $date_timestamp_to = isset($cityTimeoffset) ? $timestamp + $cityTimeoffset : $timestamp;
        }
        $filterDate['date_to'] = isset($date_timestamp_to) ? date('d.m.Y', $date_timestamp_to) : null;
        $status = self::filterByStatus();
        $orders = Order::getByStatusGroupQuery(OrderStatus::STATUS_GROUP_4, $cityId, $filterDate, $status);

        return $orders;
    }

    public function getRejectedOrdersData($cityId, $date_from, $date_to = null )
    {
        $cityTimeoffset = City::getTimeOffset($cityId);

        if ($timestamp = strtotime($date_from)) {
            $date_timestamp_from = isset($cityTimeoffset) ? $timestamp + $cityTimeoffset : $timestamp;
        }
        $filterDate['date_from'] = isset($date_timestamp_from) ? date('d.m.Y', $date_timestamp_from) : null;

        if ($timestamp = strtotime($date_to)) {
            $date_timestamp_to = isset($cityTimeoffset) ? $timestamp + $cityTimeoffset : $timestamp;
        }
        $filterDate['date_to'] = isset($date_timestamp_to) ? date('d.m.Y', $date_timestamp_to) : null;
        $status = self::filterByStatus();
        $orders = Order::getByStatusGroupQuery(OrderStatus::STATUS_GROUP_5, $cityId, $filterDate,$status);

        return $orders;
    }

    public static function filterByStatus(){
        $status = get('filter', null);
        $filter_status = null;
        if(!is_null($status)){
            $filter_status = explode(',',$status);
        }
        return $filter_status;
    }


    public function getCountOrders()
    {
        $arrayResult = [];

        $arrayResult[OrderStatus::STATUS_GROUP_0] = Order::getCountOrder(OrderStatus::STATUS_GROUP_0);

        $arrayResult[OrderStatus::STATUS_GROUP_7] = Order::getCountOrder(OrderStatus::STATUS_GROUP_7);

        $filterDate['device_not'] = Order::DEVICE_HOSPITAL;
        $arrayResult[OrderStatus::STATUS_GROUP_6] =  Order::getCountOrder(OrderStatus::STATUS_GROUP_6, $filterDate);

        $filterHos['device'] = Order::DEVICE_HOSPITAL;
        $arrayResult['hospital'] =  Order::getCountOrder(OrderStatus::STATUS_GROUP_6 , $filterHos);

        $arrayResult[OrderStatus::STATUS_GROUP_8] = Order::getCountOrder(OrderStatus::STATUS_GROUP_8);

       // $arrayResult[OrderStatus::STATUS_GROUP_4] = Order::getCountOrder(OrderStatus::STATUS_GROUP_4);
       // $arrayResult[OrderStatus::STATUS_GROUP_5] = Order::getCountOrder(OrderStatus::STATUS_GROUP_5);

        return $arrayResult;
    }
}
