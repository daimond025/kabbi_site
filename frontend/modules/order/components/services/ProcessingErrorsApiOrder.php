<?php

namespace frontend\modules\order\components\services;

class ProcessingErrorsApiOrder
{

    public $errors422 = [
        OrderError::NO_PASSENGER_PHONE   => 'Indicate passenger\'s phone number',
    ];

    public $errors400 = [
        OrderError::ORDER_TIME_INCORRECT => 'Order time is incorrect',
        OrderError::NO_MONEY             => 'There is not enough money to create an order with the specified type of payment',
    ];

    protected $statusCode;
    protected $message;
    protected $errors;

    public function setResponse($response)
    {
        $this->statusCode = $response['status'];
        $this->message    = $response['message'];
        $this->errors     = $response['errors'];
    }

    public function getError()
    {
        switch ($this->statusCode) {
            case 422:
                return $this->parseError422();

            case 400:
                return $this->parseError400();

            default:
                return $this->getTranslate('Error saving order. Notify to administrator, please.');
        }
    }

    protected function parseError422()
    {
        if (is_array($this->errors) && array_key_exists(current($this->errors)[0], $this->errors422)) {
            $errorKey = $this->errors422[current($this->errors)[0]];

            return $this->getTranslate($errorKey);
        }

        return $this->getTranslate('Error saving order. Notify to administrator, please.');
    }

    protected function parseError400()
    {
        if (array_key_exists((int)$this->message, $this->errors400)) {
            $errorKey = $this->errors400[$this->message];

            return $this->getTranslate($errorKey);
        }

        return $this->getTranslate('Error saving order. Notify to administrator, please.');
    }

    protected function getTranslate($key)
    {
        return t('order', $key);
    }
}
