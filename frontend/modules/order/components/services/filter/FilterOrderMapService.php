<?php

namespace frontend\modules\order\components\services\filter;


use app\modules\order\models\forms\FilterMapForm;
use app\modules\order\models\OrderStatus;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;

class FilterOrderMapService
{

    protected $orders;
    protected $arrOrders = [];

    /* @var $searchByCheckbox SearchByCheckbox */
    protected $searchByCheckbox;

    /* @var $form FilterMapForm */
    protected $form;


    public function __construct($orders, FilterMapForm $form)
    {
        $this->searchByCheckbox = new SearchByCheckbox();

        foreach ($orders as $order) {
            $this->arrOrders[] = unserialize($order);
        }
        $this->orders = $orders;
        $this->form   = $form;
    }

    public function getFilterOrders()
    {

        $idsOrdersbyStatus  = $this->filterStatus();
        $idsOrdersByCity    = $this->filterCities();

        $idsCompany = $this->filterCompanies();

        $intersectOrders = array_intersect(
            $idsOrdersbyStatus,
            $idsOrdersByCity,
            $idsCompany
        );


        $needOrders = [];
        foreach ($intersectOrders as $intersectOrder) {
            $needOrders[] = $this->orders[$intersectOrder];
        }
        return $needOrders;
    }

    protected function filterCities()
    {
        return $this->searchByCheckbox->getFound($this->form->cities, $this->arrOrders, 'city_id');
    }
    protected function filterStatus()
    {
        return $this->searchByCheckbox->notgetFound($this->form->statuses, $this->arrOrders, 'status_id');
    }

    protected function filterCompanies()
    {
        return $this->searchByCheckbox->getFoundOreNull($this->form->tenantCompanies, $this->arrOrders,
            'worker_company.tenant_company_id');
    }

}