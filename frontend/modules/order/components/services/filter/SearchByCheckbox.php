<?php

namespace frontend\modules\order\components\services\filter;


use app\modules\order\models\OrderStatus;
use frontend\modules\companies\models\TenantCompany;
use yii\helpers\ArrayHelper;

class SearchByCheckbox
{
    // проверка что в заказе есть данные параметр
    public function getFound($found, $arrItems, $where)
    {
        if (!$found) {
            return array_keys($arrItems);
        }

        $needItemsIds = [];
        foreach ($arrItems as $key => $item) {
            if (in_array(ArrayHelper::getValue($item, $where), $found)) {
                $needItemsIds[] = $key;
            }
        }

        return $needItemsIds;
    }

    // проверка что в заказе нет данные параметр
    public function notgetFound($found, $arrItems, $where)
    {
        if (!$found) {
            return array_keys($arrItems);
        }

        $needItemsIds = [];
        foreach ($arrItems as $key => $item) {
            if (!in_array(ArrayHelper::getValue($item, $where), $found)) {
                $needItemsIds[] = $key;
            }
        }

        return $needItemsIds;
    }

    public function getFoundOreNull($found, $arrItems, $where){
        if (!$found) {
            return array_keys($arrItems);
        }

        $needItemsIds = [];
        foreach ($arrItems as $key => $item) {
            if (in_array(ArrayHelper::getValue($item, $where), $found)) {
                $needItemsIds[] = $key;
            }elseif(is_null(ArrayHelper::getValue($item, $where))){
                $needItemsIds[] = $key;
            }
        }

        return $needItemsIds;

    }
}