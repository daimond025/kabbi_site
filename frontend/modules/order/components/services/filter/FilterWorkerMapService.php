<?php

namespace frontend\modules\order\components\services\filter;

use app\modules\order\models\forms\FilterMapForm;
use yii\base\InvalidParamException;

class FilterWorkerMapService
{

    protected $arrWorkers = [];
    protected $workers;

    /* @var $form FilterMapForm */
    protected $form;

    /* @var $searchByText SearchByText */
    protected $searchByText;

    /* @var $searchByCheckbox SearchByCheckbox */
    protected $searchByCheckbox;


    public function __construct($workers, FilterMapForm $form)
    {
        $this->searchByText     = new SearchByText();
        $this->searchByCheckbox = new SearchByCheckbox();

        foreach ($workers as $worker) {
            $this->arrWorkers[] = unserialize($worker);
        }
        $this->workers = $workers;
        $this->form    = $form;


        if (!$this->form->validate()) {
            throw new InvalidParamException();
        }
    }

    public function getFilterWorkers()
    {
        $idsWorkersByGroup         = $this->filterGroups();
        $idsWorkersByPosition      = $this->filterPositions();
        $idsWorkersByCity          = $this->filterCities();
        $idsWorkersByCarClass      = $this->filterCarClass();
        $idsWorkersByTenantCompany = $this->filterTenantCompany();
        $idsWorkersBySearchLine    = $this->filterSearchLine();

        $intersectWorkers = array_intersect(
            $idsWorkersByCarClass,
            $idsWorkersByGroup,
            $idsWorkersByPosition,
            $idsWorkersByCity,
            $idsWorkersBySearchLine,
            $idsWorkersByTenantCompany
        );

        $needWorkers = [];
        foreach ($intersectWorkers as $intersectWorker) {
            $needWorkers[] = $this->workers[$intersectWorker];
        }

        return $needWorkers;
    }

    protected function filterSearchLine()
    {
        $whereSearch = [
            'worker.name',
            'worker.second_name',
            'worker.last_name',
            'worker.callsign',
            'car.name',
            'car.gos_number',
        ];

        return $this->searchByText->getFoundWorkers($this->form->searchLine, $this->arrWorkers, $whereSearch);
    }

    protected function filterTenantCompany()
    {
        return $this->searchByCheckbox->getFound($this->form->tenantCompanies, $this->arrWorkers,
            'worker.tenant_company_id');
    }

    protected function filterCarClass()
    {
        return $this->searchByCheckbox->getFound($this->form->classes, $this->arrWorkers, 'car.class_id');
    }

    protected function filterGroups()
    {
        return $this->searchByCheckbox->getFound($this->form->groups, $this->arrWorkers, 'worker.group_id');
    }

    protected function filterPositions()
    {
        return $this->searchByCheckbox->getFound($this->form->positions, $this->arrWorkers,
            'position.position_id');
    }

    protected function filterCities()
    {
        return $this->searchByCheckbox->getFound($this->form->cities, $this->arrWorkers, 'worker.city_id');
    }

}