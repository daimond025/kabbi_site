<?php

namespace frontend\modules\order\components\services\filter;


use yii\helpers\ArrayHelper;

class SearchByText
{

    public function getFoundWorkers($searchLine, $arrItems, array $whereSearch)
    {

        $needWorkersIds = [];
        foreach ($arrItems as $key => $item) {
            for ($i = 0; $i < count($whereSearch); $i++) {
                $value = ArrayHelper::getValue($item, $whereSearch[$i]);
                if (preg_match(sprintf('#%s#iu', $searchLine), $value)) {
                    $needWorkersIds[] = $key;
                    break;
                }
            }
        }

        return $needWorkersIds;
    }

}