<?php

namespace frontend\modules\order\components\services;


use app\modules\order\models\factories\PriceCalculation;
use app\modules\order\models\factories\TariffCost;
use app\modules\order\models\Order;
use app\modules\order\models\OrderDetailCost;
use app\modules\order\models\PriceCalculationStrategy;
use frontend\modules\promocode\models\search\PromoBonusOperation;
use app\modules\order\models\PriceCalculationService;

class CalculationService
{
    /** @var Order */
    protected $order;

    protected $currencySymbol;

    public function __construct(Order $order, $currencySymbol)
    {

        $this->order = $order;

        $this->currencySymbol = $currencySymbol;

    }


    public function getPriceCalculation()
    {
        $priceCalculation = null;

        $info = OrderDetailCost::find()
            ->where(['order_id' => $this->order->order_id])
            ->orderBy(['detail_id' => SORT_DESC])
            ->asArray()
            ->one();

        if (!empty($info)) {

            $getInstanceHelper = function ($byCity) use ($info) {
                return TariffCost::getInstance([
                    'info'           => $info,
                    'currencySymbol' => $this->currencySymbol,
                    $byCity          => $info[$byCity],
                ]);
            };

            $inCity = $getInstanceHelper('accrual_city');

            $outCity = $getInstanceHelper('accrual_out');


            $strategy = new PriceCalculationStrategy();

            $service = new PriceCalculationService($strategy);

            $promoBonusOperation = PromoBonusOperation::find()
                ->where(['order_id' => $info['order_id']])
                ->one();

            $info['refill_promo_bonus'] = $promoBonusOperation->promo_bonus;

            $priceCalculation = new PriceCalculation($inCity,
                $outCity, $info, ['minute' => t('app', 'min.'), 'km' => t('app', 'km')],
                $this->currencySymbol, $service);

            $service->setModel($priceCalculation);

            $strategy->setModel($priceCalculation);


            $priceCalculation->setPriceInCityByAccrual();

            $priceCalculation->setPriceOutCityByAccrual();

        }

        return $priceCalculation;
    }

}