<?php

namespace frontend\modules\order\components\repositories;


use common\modules\car\models\CarClass;

class CarClassRepositories
{
    public static function getAllCarClassTenant($conditions)
    {
        return CarClass::find()->alias('cc')
            ->joinWith([
                'cars c',
            ], false)
            ->where(array_merge([
                'c.tenant_id' => user()->tenant_id,
                'c.active' => 1
            ], $conditions))
            ->all();
    }
}