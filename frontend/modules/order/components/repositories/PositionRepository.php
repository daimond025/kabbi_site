<?php

namespace frontend\modules\order\components\repositories;


use common\modules\employee\models\position\Position;
use common\modules\employee\models\worker\Worker;
use common\modules\employee\models\worker\WorkerHasPosition;
use common\modules\tenant\models\TenantCityHasPosition;

class PositionRepository
{
    public static function getAllUniquePositionsTenant($conditions)
    {
        return Position::find()->alias('p')
            ->distinct()
            ->leftJoin(
                TenantCityHasPosition::tableName() . 'tchp',
                '`p`.`position_id` = `tchp`.`position_id`'
            )
            ->leftJoin(
                WorkerHasPosition::tableName() . 'whp',
                '`p`.`position_id` = `whp`.`position_id`'
            )
            ->leftJoin(
                Worker::tableName() . 'w',
                '`whp`.`worker_id` = `w`.`worker_id`'
            )
            ->where(array_merge([
                'tchp.tenant_id' => user()->tenant_id
            ], $conditions))
            ->all();
    }
}