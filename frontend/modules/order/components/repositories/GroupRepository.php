<?php

namespace frontend\modules\order\components\repositories;


use common\modules\employee\models\worker\WorkerGroup;
use common\modules\tenant\models\TenantHasCity;
use frontend\modules\employee\models\groups\WorkerGroupHasCity;

class GroupRepository
{
    public static function getAllGroupsTenant($tenantId)
    {
        return WorkerGroup::find()->alias('wg')
            ->leftJoin(
                WorkerGroupHasCity::tableName() . ' wghc',
                '`wg`.`group_id` = `wghc`.`group_id`'
            )
            ->leftJoin(
                TenantHasCity::tableName() . ' thc',
                '`wghc`.`city_id` = `thc`.`city_id`'
            )
            ->where([
                'wg.block'      => 0,
                'wg.tenant_id'  => $tenantId,
                'thc.tenant_id' => $tenantId,
                'thc.block'     => 0,
            ])
            ->asArray()
            ->all();
    }
}