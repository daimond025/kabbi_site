<?php

namespace frontend\modules\order\components;

class ApiOrderError422Processing
{

    const NEED_ERRORS = [
        'tariff_id' => [
            'NOT_FOUND'
        ]
    ];

    protected $errors;

    public function __construct($errors)
    {
        $this->errors = $errors;
    }

    public function getErrors()
    {

        $needErrors = [];

        foreach ($this->errors as $error) {
            $field = $error['field'];
            $message = $error['message'];
            if (in_array($field, self::NEED_ERRORS)) {
                foreach (self::NEED_ERRORS[$field] as $needMessage) {
                    if ($needMessage == $message) {
                        $needErrors[] = $message;
                    }
                }
            }
        }

        return $needErrors;
    }
}
