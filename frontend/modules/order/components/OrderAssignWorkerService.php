<?php

namespace frontend\modules\order\components;

use app\modules\order\models\Order;
use app\modules\order\models\OrderStatus;
use app\modules\tenant\models\User;
use common\components\serviceApi\EmailTypes;
use common\components\serviceApi\ServiceApi;
use common\helpers\ArrayHelper;
use common\modules\city\models\City;
use frontend\components\repositories\SettingRepository;
use frontend\modules\employee\components\position\PositionService;
use frontend\modules\order\models\WorkerShiftNote;
use operatorApi\ApiInterface;
use operatorApi\models\OrderEventRepository;
use frontend\modules\employee\models\worker\Worker;
use operatorApi\models\OrderUpdateEvent;
use yii\base\Object;

/**
 * Class OrderService
 * @package frontend\modules\order\components
 */
class OrderAssignWorkerService extends Object
{
    /* @var PositionService */
    private $positionService;

    /** @var SettingRepository */
    protected $settingRepository;

    /** @var $operatorApi ApiInterface */
    protected $operatorApi;

    /** @var $serviceApi ServiceApi */
    protected $serviceApi;

    const OK = true;
    const ERROR = false;

    const OK_RESULT = 1;
    const ERROR_RESULT = 0;
    const ORDER_BAD_STATUS = 10;
    const ORDER_HAS_WORKER = 20;
    const ORDER_NO_WORKER = 25;
    const ORDER_NOT_IN_DB  = 30;
    const ORDER_NOT_IN_RD  = 40;
    const EMAIL_EMPTY = 50;
    const ORDER_HAS_NOT_WOREKRS = 60;
    const ORDER_COMPLATED = 70;
    const ORDER_WORKER = 80;
    public $errorCodeData = [
        self::OK_RESULT  => 'OK',
        self::ORDER_BAD_STATUS  => 'ORDER_BAD_STATUS',
        self::ORDER_HAS_WORKER  => 'ORDER_HAS_WORKER',
        self::ORDER_NO_WORKER  => 'ORDER_NO_WORKER',
        self::ORDER_NOT_IN_DB  => 'ORDER_NOT_IN_DB',
        self::ORDER_NOT_IN_RD  => 'WAIT',
        self::EMAIL_EMPTY  => 'EMAIL_EMPTY',
        self::ORDER_HAS_NOT_WOREKRS  => 'ORDER_HAS_NOT_WOREKRS',
        self::ORDER_COMPLATED  => 'ORDER_COMPLATED',
        self::ORDER_WORKER  => 'ORDER_WORKER',
    ];

    public function __construct(
        PositionService $positionService,
        SettingRepository $settingRepository,
        array $config = []
    ) {
        $this->positionService   = $positionService;
        $this->settingRepository = $settingRepository;
        $this->positionService->setTenantId(user()->tenant_id);

        /** @var $operatorApi ApiInterface */
        $this->operatorApi = app()->operatorApi;

        $this->serviceApi = app()->get('serviceApi');;

        parent::__construct($config);
    }


    public function getPreOrdersData($cityId, $date_from, $date_to )
    {
        $cityTimeoffset = City::getTimeOffset($cityId);

        if ($timestamp = strtotime($date_from)) {
            $date_timestamp_from = isset($cityTimeoffset) ? $timestamp + $cityTimeoffset : $timestamp;
        }
        $filterDate['date_from'] = isset($date_timestamp_from) ? date('d.m.Y', $date_timestamp_from) : null;

        if ($timestamp = strtotime($date_to)) {
            $date_timestamp_to = isset($cityTimeoffset) ? $timestamp + $cityTimeoffset : $timestamp;
        }
        $filterDate['date_to'] = isset($date_timestamp_to) ? date('d.m.Y', $date_timestamp_to) : null;

        $dataProvider = Order::getByStatusGroupQuery(OrderStatus::STATUS_GROUP_PRE_ORDER, $cityId, $filterDate, ['sort' => SORT_ASC, 'order' => 'order_time']);

        return $dataProvider;
    }

    /*
 * get order list by ods
 *
 * @param Array $orders_in
 * @param Array $order_ids
 *
 * @return Order
 * */
    public function getOrderByIds(&$orders_in, $order_ids){

        $query = Order::find()->alias('o')
            ->where(['tenant_id' => user()->tenant_id])
            ->where(['in', 'order_id', $order_ids])
            ->orderBy('order_id DESC');

        if (app()->user->can(User::ROLE_STAFF_COMPANY)) {
            $query->andWhere(['or',
                    ['o.company_id' => user()->tenant_company_id],
                    ['o.company_id' => null]
                ]
            );
        }

        $orders = $query->all();

        foreach($orders as $order)
        {
            list($assign_permit, $reason ) = $this->permitAssignWorker($order);

            if(!$assign_permit){
                $workerId = $orders_in[(int)$order->order_id];
                $order_considered['code'] = self::ERROR_RESULT;
                $order_considered['workerId'] = $workerId;
                $order_considered['reason'] = $reason;
                $order_considered['uuid'] = '';
                $orders_in[(int)$order->order_id] = $order_considered;
            }else{
                $workerId = $orders_in[(int)$order->order_id];
                $params['fields']['worker_id'] = $workerId;
                $params['fields']['status_id'] = OrderStatus::STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT;
                $params['last_update_time'] = $order->update_time;

                $uui_task =  $this->operatorApi->createUpdateEvent( user()->tenant_id,  user()->user_id, $order->order_id, $params, user()->lang);

                $order_considered['code'] = self::OK_RESULT;
                $order_considered['workerId'] = $workerId;
                $order_considered['reason'] = $reason;
                $order_considered['uuid'] = $uui_task;
                $orders_in[(int)$order->order_id] = $order_considered;
            }
        }

        foreach($orders_in as  $order_id => $order){
            if(!is_array($order)){
                $workerId = $orders_in[$order_id];
                $order_considered['code'] = self::ERROR_RESULT;
                $order_considered['workerId'] = $workerId;
                $order_considered['reason'] = $this->errorCodeData[self::ORDER_NOT_IN_DB] ;
                $order_considered['uuid'] = '';
                $orders_in[$order_id] = $order_considered;
            }
        }
    }

    /**
     * permit assign worker to order     *
     * @param Order $order
     * @return array
     */
    public function permitAssignWorker($order)
    {
        if(isset($order->worker_id)){
            return [self::ERROR, $this->errorCodeData[self::ORDER_HAS_WORKER] ];
        }

        if(!in_array((int)$order->status_id,[
            OrderStatus::STATUS_PRE,
            OrderStatus::STATUS_PRE_NOPARKING,
            OrderStatus::STATUS_PRE_REFUSE_WORKER,
            OrderStatus::STATUS_PRE_FOR_HOSPITAL,
            OrderStatus::STATUS_PRE_ORDER_COMPANY,
            OrderStatus::STATUS_FREE_FOR_HOSPITAL,
            OrderStatus::STATUS_FREE_FOR_COMPANY,
            ]))
        {
            return [self::ERROR, $this->errorCodeData[self::ORDER_BAD_STATUS] . '. Current status is ' . $order->status_id];
        }

        return [self::OK, $this->errorCodeData[self::OK_RESULT] ];

    }


    /*
        * get order list by ods
        *
        * @param Array $orders_in
        * @param Array $order_ids
        *
        * @return Order
    * */
    public function deleteOrders(&$orders_in, $order_ids){

        $query = Order::find()->alias('o')
            ->where(['tenant_id' => user()->tenant_id])
            ->where(['in', 'order_id', $order_ids])
            ->orderBy('order_id DESC');

        if (app()->user->can(User::ROLE_STAFF_COMPANY)) {
            $query->andWhere(['or',
                    ['o.company_id' => user()->tenant_company_id],
                    ['o.company_id' => null]
                ]
            );
        }
        $orders = $query->all();

        foreach($orders as $order)
        {
            list($assign_permit, $reason ) = $this->permitRemoveOrder($order);


            if(!$assign_permit){
                $order_considered['code'] = self::ERROR_RESULT;
                $order_considered['reason'] = $reason;
                $order_considered['uuid'] = '';
                $orders_in[(int)$order->order_id] = $order_considered;
            }else{
                $params['fields']['status_id'] = OrderStatus::STATUS_NO_CARS;
                $params['last_update_time'] = $order->update_time;

                $uui_task =  $this->operatorApi->createUpdateEvent( user()->tenant_id,  user()->user_id, $order->order_id, $params, user()->lang);

                $order_considered['code'] = self::OK_RESULT;
                $order_considered['reason'] = $reason;
                $order_considered['uuid'] = $uui_task;
                $orders_in[(int)$order->order_id] = $order_considered;
            }
        }

        foreach($orders_in as  $order_id => $order){
            if(!is_array($order)){
                $order_considered['code'] = self::ERROR_RESULT;
                $order_considered['reason'] = $this->errorCodeData[self::ORDER_NOT_IN_DB] ;
                $order_considered['uuid'] = '';
                $orders_in[$order_id] = $order_considered;
            }
        }
    }

    /**
     * permit assign worker to order     *
     * @param Order $order
     * @return array
     */
    public function permitRemoveOrder($order)
    {
        if(in_array((int)$order->status_id,[
            OrderStatus::getCompletedStatusId()
        ])){
            return [self::ERROR, $this->errorCodeData[self::ORDER_COMPLATED]];
        }
        elseif(in_array((int)$order->status_id,[
            OrderStatus::getWorksStatusId()
        ]))
        {
            return [self::ERROR, $this->errorCodeData[self::ORDER_WORKER]];
        }else{
            return [self::OK, $this->errorCodeData[self::OK_RESULT] ];
        }
    }

    /*
     * get order list by ods
     *
     * @param Array $order_ids
     *
     * @return Order
     * */
    public function getResultRespone(&$orders_in){

       $redisAnswers =  new OrderEventRepository();
       $keys =  $redisAnswers->getAll();

       foreach($orders_in as $order_id => $key){

           if(in_array($key, $keys) ){
              $answer =  $redisAnswers->get($key);
              $answer_object = json_decode($answer,true);
              $code =  ArrayHelper::getValue($answer_object, 'code');
              $info =  ArrayHelper::getValue($answer_object, 'info');
              $order_considered['code'] = (int)$code === 100 ? self::OK_RESULT :  self::ERROR_RESULT ;
              $order_considered['info'] = $info;
              $orders_in[$order_id]  = $order_considered;
           }
           else{
               $order_considered['code'] = self::ERROR_RESULT ;
               $order_considered['info'] = $this->errorCodeData[self::ORDER_NOT_IN_RD];
               $orders_in[$order_id]  = $order_considered;
           }
       }
    }


    /*
     * @param Array $orders_in
     *
     * return array
     * */

    public function removeWorkerFromOrder(&$orders_in){
        $orders_ids = array_keys($orders_in);

        $query = Order::find()->alias('o')
            ->where(['tenant_id' => user()->tenant_id])
            ->where(['in', 'order_id', $orders_ids])
            ->orderBy('order_id DESC');

        if(app()->user->can(User::ROLE_STAFF_COMPANY)){
            $query->andWhere(['or',
                    ['o.company_id' => user()->tenant_company_id],
                    ['o.company_id' =>  null]
                ]
            );
        }
        $orders = $query->all();

        foreach($orders as $order){
            list($assign_remove, $reason) = $this->permitRemoveWorker($order);

            $order_considered = [];
            if($assign_remove){
                $uui_task = $this->operatorApi->takeOffFromWorker($order->tenant_id, user()->id, $order->order_id, $order->update_time, user()->lang);

                $order_considered['code'] = self::OK_RESULT;
                $order_considered['reason'] = $reason;
                $order_considered['uuid'] = $uui_task;
            }else{
                $order_considered['code'] = self::ERROR_RESULT;;
                $order_considered['reason'] = $reason;
                $order_considered['uuid'] = '';
            }
            $orders_in[(int)$order->order_id] = $order_considered;
        }

        foreach($orders_in as $order_id => $order){
            if(!is_array($order)){
                $order_considered['code'] = self::ERROR_RESULT;
                $order_considered['reason'] = $this->errorCodeData[self::ORDER_NOT_IN_DB] ;
                $order_considered['uuid'] = '';
                $orders_in[$order_id] = $order_considered;
            }
        }
    }

    public function getWorkerCompany($company_id){
        self::deleteWorkerShiftNote();

        $workers = Worker::find()
            ->with(['workerShiftNote'])
            ->where([
                'tenant_id' => user()->tenant_id,
                'tenant_company_id' => $company_id,
                'block' => 0,

            ])
            ->all();
        return $workers;
    }

    public static function deleteWorkerShiftNote()
    {
        $date = new \DateTime('-1 days');
        $date->setTime(23, 59, 59);
        $timestamp = $date->getTimestamp() ;

        WorkerShiftNote::deleteAll([
            '<', 'created_at', $timestamp
        ]);

    }
    public function setWorkerNote($workerId, $note){

        $workerNote = WorkerShiftNote::find()
            ->where([
                'worker_id' => $workerId
            ])->one();

        if($workerNote){
            $workerNote->note = $note;
            return $workerNote->save();
        }else{
            $workerNote = new WorkerShiftNote();
            $workerNote->worker_id = $workerId;
            $workerNote->note = $note;
            return $workerNote->save();
        }
    }


    public function getOrdersCompany($companyId){
        list($first_time , $second_time) = self::getDayPlusOne();

        // todo test
        /*$first_time = 1601510400;
        $second_time= 1712720000;*/

        $orders = Order::find()
            ->alias('o')
            ->leftJoin('{{%worker}} w',  'o.worker_id = w.worker_id')
            ->select(['o.worker_id', 'o.order_id',  'o.city_id', 'w.email', 'w.lang'])
            ->where([
                'o.tenant_id' => user()->tenant_id
            ])
            ->andWhere(['o.company_id' => $companyId])
            ->andWhere(['not', ['o.worker_id' => null]])
            ->andWhere(['o.status_id' => OrderStatus::getPreOrderStatusId()])
            ->andWhere("o.order_time >= $first_time AND o.order_time <= $second_time")
            ->groupBy('o.worker_id')
            ->asArray()
            ->all();

        return [ $orders, $first_time, $second_time ];
    }


    public function processSendEmail($companyId)
    {
        list($orderWorders , $first_time , $second_time) = $this->getOrdersCompany($companyId);

        $send_result = [];
        foreach($orderWorders as $order)
        {
            $worker_id = ArrayHelper::getValue($order,'worker_id' );
            $city_id = ArrayHelper::getValue($order, 'city_id' );
            $email = ArrayHelper::getValue($order, 'email' );
            $lang = ArrayHelper::getValue($order, 'lang' );

            //$email = 'daimond025@yandex.ru';

            list($assign_send, $reason) = $this->permitSendEmail($email);

            if($assign_send){
                $worker_considered['code'] = self::OK_RESULT;
                $worker_considered['reason'] = $reason;
                $worker_considered['worker_id'] = $worker_id;
                $send_result[] = $worker_considered;

                // SEND EMAIL
                $this->serviceApi->sendEmail(user()->tenant_id, $city_id, EmailTypes::WORKER_INFO_ORDERS, $lang, $email, [
                    'worker_id'     => $worker_id,
                    'time_start'    => $first_time,
                    'time_end'     => $second_time,
                ]);

            }else{
                $worker_considered['code'] = self::ERROR_RESULT;;
                $worker_considered['reason'] = $reason;
                $worker_considered['worker_id'] = $worker_id;
                $send_result[] = $worker_considered;
            }
        }

        if(count($orderWorders) === 0){
            $worker_considered['code'] = self::ERROR_RESULT;;
            $worker_considered['reason'] = $this->errorCodeData[self::ORDER_HAS_NOT_WOREKRS];
            $send_result[] = $worker_considered;
        }
        return $send_result;
    }

    public static function getDayPlusOne(){
        $NextВay = strtotime("+1 day");

        list($day, $month, $year) = explode('.', date("d.m.Y", $NextВay));
        $first_time = mktime(0, 0, 0, $month, $day, $year);
        $second_time = mktime(23, 59, 59, $month, $day, $year);

        return [$first_time, $second_time ];
    }

    /**
     * permit assign worker to order     *
     * @param Order $order
     * @return array
     */
    public function permitSendEmail($email){
        if(empty($email)){
            return [self::ERROR, $this->errorCodeData[self::EMAIL_EMPTY] ];
        }
        return [self::OK, $this->errorCodeData[self::OK_RESULT] ];
    }

    /**
     * permit assign worker to order     *
     * @param Order $order
     * @return array
     */
    public function permitRemoveWorker($order){
        if(!isset($order->worker_id)){
            return [self::ERROR, $this->errorCodeData[self::ORDER_NO_WORKER] ];
        }
        return [self::OK, $this->errorCodeData[self::OK_RESULT] ];
    }

}
