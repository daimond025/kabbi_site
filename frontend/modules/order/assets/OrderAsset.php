<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\order\assets;

use yii\web\AssetBundle;

class OrderAsset extends AssetBundle
{
    public static $id_pre_orders = 'id_pre_orders';
    public static $id_new_orders = 'id_new_orders';
    public static $id_wor_orders = 'id_worker_orders';
    public static $id_war_orders = 'id_warning_orders';
    public static $id_hos_orders = 'id_hospital_orders';
    public static $id_com_orders = 'id_complate_orders';
    public static $id_rej_orders = 'id_reject_orders';


    public $sourcePath = '@app/modules/order/assets/js';
    public $publishOptions = [
        'forceCopy' => true,
    ];
    public $css = [
        'orders.css'
    ];
    public $js = [
        'refresh.js',
        'main.js',
        'tenant_company.js',
        'Timer.js',
        'autocomplete.js'
    ];
    public $depends = [
        'app\modules\order\assets\ViewOrderAsset',
    ];

}
