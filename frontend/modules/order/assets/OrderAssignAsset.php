<?php

namespace app\modules\order\assets;

use frontend\modules\client\modules\cabinet\assets\AppAsset;
use frontend\widgets\filter\assets\FilterAsset;
use yii\web\AssetBundle;

/**
 * Class OrderAssignAsset
 * @package app\modules\order\assets
 */
class OrderAssignAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/order/assets/orderAssign';

    public $publishOptions = [
        'forceCopy' => true,
    ];
    public $css = [
        'dist/order-assign.min.css',

    ];
    public $js = [
        'dist/order-assign.min.js'
    ];
    public $depends = [
        AppAsset::class,
        FilterAsset::class,
        OrderAsset::class
    ];
}