document.addEventListener("DOMContentLoaded", ready);

function ready() {
    var body = $('body');

    body.on('change', '.js-other-client input', function () {
        if ($(this).is(':checked')) {
            $('[data-tab="other"]').show().trigger('click');

            $('[name="Order[client_passenger_lastname]"]').attr('disabled', false);
            $('[name="Order[client_passenger_name]"]').attr('disabled', false);
            $('[name="Order[client_passenger_secondname]"]').attr('disabled', false);
            $('[name="Order[client_passenger_phone]"]').attr('disabled', false);
        } else {
            $('[data-tab="other"]').hide();
            $('[data-tab="normal"]').trigger('click');

            $('[name="Order[client_passenger_lastname]"]').attr('disabled', true);
            $('[name="Order[client_passenger_name]"]').attr('disabled', true);
            $('[name="Order[client_passenger_secondname]"]').attr('disabled', true);
            $('[name="Order[client_passenger_phone]"]').attr('disabled', true);
        }
    });

    body.on('click', '.order-window__client [data-tab]', function () {
        $('[data-tab]').removeClass('active');
        $(this).addClass('active');

        if ($(this).attr('data-tab') === 'normal') {
            $('.js-normal-client-tab').show();
            $('.js-other-client-tab').hide();
        } else {
            $('.js-normal-client-tab').hide();
            $('.js-other-client-tab').show();
        }
    });
}
