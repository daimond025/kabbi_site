/*global jQuery*/
;
(function ($) {
    'use strict';

    window.autocomplete = {
        preloaderClass: 'input_preloader',
        actions: ['autocomplete', 'search', 'history'],
        _curAction: null,
        _selected: false,

        AUTOCOMPLETE_SUCCESS: 'autocomplete-success',
        AUTOCOMPLETE_FAIL: 'autocomplete-fail',

        source: function () {
            var _this = this;
            return function (request, response) {
                var coords_parent = $('.city-select');
                var lat = coords_parent.find('input.lat').val();
                var lon = coords_parent.find('input.lon').val();
                var city_id = $('#order-city_id').val();
                var isClient = coords_parent.closest('form').data('is-client') == 1;

                $.getJSON(
                    isClient ? 'get-street-list' : '/address/street/get-street-list',
                    {search: request.term, city_id: city_id, lat: lat, lon: lon, action: _this._curAction})
                    .done(function (data) {
                        if (data.length > 0) {
                            response(data);
                        }
                    })
                    .always(function (d) {
                        $('input.' + _this.preloaderClass).removeClass(_this.preloaderClass);
                    });
            };
        },
        historySource: function () {
            var _this = this;
            return function (request, response) {
                var coords_parent = $('.city-select');
                var lat = coords_parent.find('input.lat').val();
                var lon = coords_parent.find('input.lon').val();
                var city_id = $('#order-city_id').val();
                var isClient = coords_parent.closest('form').data('is-client') == 1;
                var clientId = $('#order-client_id').val();
                $.getJSON(
                    isClient ? 'get-street-list' : '/address/street/get-street-list',
                    {
                        search: request.term,
                        city_id: city_id,
                        lat: lat,
                        lon: lon,
                        client_id: clientId,
                        action: _this.actions[2]
                    },
                    function (data) {
                        if (data.length > 0) {
                            var result = [];
                            for (var i = 0; i < data.length; i++) {
                                if (data[i]) result.push(data[i]);
                            }
                            if (result.length > 0) response(result);
                            else $('input.' + _this.preloaderClass).removeClass(_this.preloaderClass);
                        } else {
                            $('input.' + _this.preloaderClass).removeClass(_this.preloaderClass);
                        }
                    }
                );

            };
        },
        search: function () {
            var _this = this;
            return function (event, ui) {
                _this._selected = false;
                $(this).addClass(_this.preloaderClass);
            };
        },
        open: function () {
            var _this = this;
            return function (event, ui) {
                _this._curAction = _this.actions[0];
                $(this).removeClass(_this.preloaderClass);
                _this.attachGlobalSearchEvent($(this));
            };
        },
        change: function () {
            var _this = this;
            return function (event, ui) {
                var parent = $(this).parents('div.line');

                if (!_this._selected) {
                    parent.find('input[data-type="street"]').val($(this).val()).trigger('change');
                    parent.find('input[data-type="house"]').val('');
                    parent.find('input[data-type="housing"]').val('');
                }
            };
        },
        focus: function () {
            var _this = this;
            return function (event, ui) {
                _this.detachGlobalSearchEvent($(this));
            };
        },
        select: function () {
            var _this = this;
            return function (event, ui) {
                _this._selected = true;

                var lat = ui.item.address.lat;
                var lon = ui.item.address.lon;
                var city = ui.item.address.city || null;
                var street = ui.item.address.label;
                var house = ui.item.address.house;
                var housing = ui.item.address.housing;
                var placeId = ui.item.address.placeId;
                var parent = $(this).parents('.js-order-point');
                var form = parent.parents('form');
                var isClient = $(this).closest('form').data('is-client') == 1;
                var autocompleteEvent;

                if (lat && lon) {
                    var city_id;// = parent.find('input.js-cur-city_id').val();
                    var select = parent.find('.parking .default_select');

                    if (!city_id) {
                        city_id = $('#order-city_id').val();
                        /* bug пока автокомплит не передает ключ города по базе Gootax */
                        parent.find('input.js-cur-city_id').val(city_id);
                    }

                    parent.find('input[data-type="street"]').val(street);
                    parent.find('input[data-type="house"]').val(house);
                    parent.find('input[data-type="housing"]').val(housing);
                    parent.find('input[data-type="placeId"]').val(placeId);
                    parent.find('input.lat').val(lat);
                    parent.find('input.lon').val(lon);

                    getParkings(
                        isClient ? 'get-parking-by-coords' : '/parking/parking/get-parking-by-coords',
                        {city_id: city_id, lat: lat, lon: lon}, select);

                    autocompleteEvent = _this.AUTOCOMPLETE_SUCCESS;
                } else {
                    if (check_streets_filled()) {
                        getOrderPriceAnalyz(form);
                    }

                    autocompleteEvent = _this.AUTOCOMPLETE_FAIL;
                }

                var input_city = parent.find('input[data-type="city"]').val();
                if (city != input_city) {
                    parent.find('input[data-type="city"]').val(city);
                }
                _this.attachGlobalSearchEvent($(this));
                parent.trigger(autocompleteEvent);
            };
        },
        create: function () {
            var _this = this;
            return function (event, ui) {
                _this._curAction = _this.actions[0];
                _this.attachGlobalSearchEvent($(this));
            };
        },
        attachGlobalSearchEvent: function (input) {
            var _this = this;
            this.detachGlobalSearchEvent(input);
            input.on('keydown.global', function (e) {
                if (e.keyCode == 13) {
                    e.preventDefault();
                    _this._curAction = _this.actions[1];
                    $(this).autocomplete('search');
                }
            });
        },
        detachGlobalSearchEvent: function (input) {
            input.off('keydown.global');
        }
    };

    window.autocompleteClients = {
        preloaderClass: 'input_preloader',
        actions: ['autocomplete', 'search'],
        _curAction: null,
        _selected: false,

        AUTOCOMPLETE_SUCCESS: 'autocomplete-success',
        AUTOCOMPLETE_FAIL: 'autocomplete-fail',

        source: function () {
            var _this = this;
            return function (request, response) {
                var isClient = $('#order-city_id').closest('form').data('is-client') == 1;
                $.getJSON(
                    isClient ? 'search-clients' : '/order/order/search-clients',
                    {query: request.term, cityId: $('#order-city_id').val()},
                    function (data) {
                        if (data.length > 0) {
                            for (var key in data) {
                                data[key].value = data[key].phone;
                            }
                            response(data);
                        } else {
                            $('input.' + _this.preloaderClass).removeClass(_this.preloaderClass);
                            response(data);
                        }
                    }
                );
            };
        },
        search: function () {
            var _this = this;
            return function (event, ui) {
                _this._selected = false;
                $(this).addClass(_this.preloaderClass);
            };
        },
        open: function () {
            var _this = this;
            return function (event, ui) {
                _this._curAction = _this.actions[0];
                $(this).removeClass(_this.preloaderClass);
                _this.attachGlobalSearchEvent($(this));
            };
        },
        change: function () {
            var _this = this;
            return function (event, ui) {

                if (!_this._selected) {
                    $('#order-phone').val('').trigger('change');
                }
            };
        },
        focus: function () {
            var _this = this;
            return function (event, ui) {
                _this.detachGlobalSearchEvent($(this));
            };
        },
        select: function () {
            var _this = this;
            return function (event, ui) {
                _this._selected = true;

                $('#order-phone').val('+' + ui.item.phone);
                $('#order-client_id').val(ui.item.client_id);

                fillClientInfo(ui.item.phone, $('#order-city_id').val());

                _this.attachGlobalSearchEvent($(this));
            };
        },
        create: function () {
            var _this = this;
            return function (event, ui) {
                _this._curAction = _this.actions[0];
                _this.attachGlobalSearchEvent($(this));

                $('[name="autocomplete-clients"]').autocomplete("instance")._renderItem = function (ul, item) {

                    var name = item.shortName;

                    name = (name !== null) ? ('<div class="name">' + name + '</div>') : '';
                    var photo = (item.photo !== null) ? item.photo : '/images/no-photo.png';
                    var itemContent = '<li><div class="autocomplete-client__item"><div class="photo" style="background-image: url(' + photo + ')"></div><div class="info">' +
                        name + '<div class="phone">+' + item.phone + '</div></div></div></li>';
                    return $(itemContent).appendTo(ul);
                };
            };
        },
        attachGlobalSearchEvent: function (input) {
            var _this = this;
            this.detachGlobalSearchEvent(input);
            input.on('keydown.global', function (e) {
                if (e.keyCode == 13) {
                    e.preventDefault();
                    _this._curAction = _this.actions[1];
                    $(this).autocomplete('search');
                }
            });
        },
        detachGlobalSearchEvent: function (input) {
            input.off('keydown.global');
        }
    };

    window.autocompleteDrivers = $.extend({}, window.autocompleteClients, {
        getAutoComplete: function(shortUrl, url, query, cityId, positionId, lat, lon, response) {
            var isClient = $('#order-city_id').closest('form').data('is-client') == 1;
            var _this = this;
            $.getJSON(
                isClient ? shortUrl : url,
                {
                    query: query,
                    cityId: cityId,
                    positionId: positionId,
                    lat: lat,
                    lon: lon,
                },
                function (data) {

                    if (data.length > 0) {
                        for (var key in data) {
                            data[key].value = data[key].phone;
                        }
                        response(data);
                    } else {
                        $('input.' + _this.preloaderClass).removeClass(_this.preloaderClass);
                    }
                }
            );
        },
        source: function () {
            var _this = this;
            return function (request, response) {

                var query = request.term;
                var cityId = $('#order-city_id').val();
                var positionId = $('#order-position_id').val();

                _this.getAutoComplete('search-workers', '/order/order/search-workers', query, cityId, positionId, null, null, response);

            }
        },
        freeDriversSearch: function () {
            var _this = this;
            return function (request, response) {

                var cityId = $('#order-city_id').val();
                var positionId = $('#order-position_id').val();
                var lat = $('input[name="Order[address][A][lat]"]').val();
                var lon = $('input[name="Order[address][A][lon]"]').val();

                _this.getAutoComplete('near-workers', '/order/order/near-workers', null, cityId, positionId, lat, lon, response);

            }
        },
        create: function () {
            var _this = this;
            return function (event, ui) {
                _this._curAction = _this.actions[0];
                _this.attachGlobalSearchEvent($(this));

                $('[name="autocomplete-drivers"]').autocomplete("instance")._renderItem = function (ul, item) {

                    var name = [item.callsign, item.shortName].filter(function (value) {
                        return value;
                    }).join(',  ');

                    var distStr = '';

                    name = (name !== null) ? ('<div class="name name--driver">' + name + '<i class="status status--' + item.status + '"></i></div>') : '';
                    var photo = (item.photo !== null) ? item.photo : '/images/no-photo.png';
                    var car = [];
                    if (item.car != null) {
                        if (item.car.name) car.push(item.car.name);
                        if (item.car.gosNumber) car.push(item.car.gosNumber);
                        if (item.car.color) car.push(item.car.color);
                    }

                    if (item.lat !== null && item.lon !== null) {
                        var dist = driverDistance(item.lat, item.lon);
                        distStr = '<div class="dist">' + dist + '</div>';
                    }


                    var carStr = '<div class="car">' + car.join(', ') + '</div>';
                    var itemContent = '<li><div class="autocomplete-client__item"><div class="photo" style="background-image: url(' + photo + ')"></div><div class="info">' + distStr +
                        name + carStr + '</div></div></li>';
                    return $(itemContent).appendTo(ul);
                };
            };
        },
        select: function () {
            var _this = this;
            return function (event, ui) {
                _this._selected = true;
                $('#order-worker_id').val(ui.item.workerId);
                fillDriverInfo(
                    ui.item.workerId,
                    ui.item.car ? ui.item.car.carId : '',
                    $('#order-city_id').val(),
                    $('#order-position_id').val()
                );
                _this.attachGlobalSearchEvent($(this));
            };
        },
        change: function () {
            var _this = this;
            return function (event, ui) {
            };
        }
    });

    window.autocompleteCompany = $.extend({}, window.autocompleteDrivers, {
        getAutoComplete: function( url, query, cityId, response) {
            var _this = this;
            $.getJSON(
                url,
                {
                    query_name: query,
                },
                function (data) {

                    if (data.length > 0) {
                        for (var key in data) {
                            data[key].value = data[key].phone;
                        }
                        response(data);
                    } else {
                        $('input.' + _this.preloaderClass).removeClass(_this.preloaderClass);
                    }
                }
            );
        },
        searchCompany: function () {
            var _this = this;
            return function (request, response) {

                var cityId = $('#order-city_id').val();
                var query = request.term;
                _this.getAutoComplete('/order/order/search-companies', query, cityId, response);

            }
        },
        source: function () {
            var _this = this;
            return function (request, response) {

                var query = request.term;
                var cityId = $('#order-city_id').val();

                _this.getAutoComplete('/order/order/search-companies', query, cityId, response);

            }
        },
        create: function () {
            var _this = this;
            return function (event, ui) {
                _this._curAction = _this.actions[0];
                _this.attachGlobalSearchEvent($(this));

                $('[name="autocomplete-companies"]').autocomplete("instance")._renderItem = function (ul, item) {
                    var name = item.name;

                    name = (name !== null) ? ('<div class="name name--driver">' + name + '</div>') : '';

                    var itemContent = '<li><div class="autocomplete-client__item"> <div class="info">' +
                        name +   '</div></div></li>';
                    return $(itemContent).appendTo(ul);
                };
            };
        },
        select: function () {
            var _this = this;
            return function (event, ui) {
                _this._selected = true;

                $('#order-company_id').val(ui.item.companyId);

                fillCompanyInfo(ui.item );
                _this.attachGlobalSearchEvent($(this));

            };
        },
        change: function () {
            var _this = this;
            return function (event, ui) {
            };
        }
    });


    function fillCompanyInfo(company) {
        clearCompanyInfo();

        var name = company.name;

        name = (name !== null) ? (name) : '';
        var carStr = '<div class="car">' + company.userName + '</div>';

        $('#order-company_id').val(company.companyId);
        $('#company').html('<div class="line">' +
            '<div style="margin-left: 0px;" class="client-info-content">' + '<span style="max-width: 100% " class="name">' + name + '</span> <a class="js-change-company change-client"><i></i></a>' + carStr +   '</div></div>');
        $('.js-change-company').show();
        $('.js-company-info').css('height', '120%');
        $('.js-deny-refuse-order').show();
    }

    function clearCompanyInfo() {
        $('.js-change-company, .js-deny-refuse-order').hide();
        $('#company').html('');
        $('#order-company_id').val('');
        $('#order-deny_refuse_order').prop('checked', false);
        $('#company .client_more_info').html('');
        $('.js-company-info').css('height', '');
        $('input[name=autocomplete-companies]').val('');
    }

    $('body').on('click', '.js-change-company', function () {
        clearCompanyInfo();
    });


})(jQuery);
