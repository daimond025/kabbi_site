var OrderDatepicker = (function ($) {
    'use strict';
    var app = {
        init: function () {
            this.datepickerInit();
            this.registerChooseDateEvent();
            this.registerChooseNowEvent();
            this.registerIncrementDateEvent();
        },

        datepickerInit: function () {
            $('.sdp_n').each(function () {
                var $this = $(this),
                    $dateInput = $this.closest('form').find('.js-order_date'),
                    $date = $dateInput.val();
                $this.datepicker({
                    altField: $dateInput,
                    altFormat: 'yy-mm-dd'
                });

                $this.datepicker('setDate', new Date($date));
            });
        },

        registerChooseDateEvent: function () {
            $('.js-choose-date').click($.proxy(function (e) {
                var _this = this,
                    $button = $(e.target),
                    $form = $button.closest('form'),
                    hours = $form.find('.js-hours').val(),
                    minutes = $form.find('.js-minutes').val();

                if (hours >= 0 && hours < 24 && minutes >= 0 && minutes < 60) {
                    hours = hours.length < 2 ? '0' + hours : hours;
                    minutes = minutes.length < 2 ? '0' + minutes : minutes;

                    $form.find('.js-now').val(0);
                    _this.setTitle($form, 
                            $.datepicker.formatDate(
                                $form.find('.sdp_n').datepicker('option', 'dateFormat'),
                                $form.find('.sdp_n').datepicker('getDate'))
                                + ' ' + hours + ':' + minutes);
                    _this.closeDatepicker($form);
                } else {
                    $form.find('.js-hours').addClass('input_error');
                    $form.find('.js-minutes').addClass('input_error');
                }
            }, this));
        },

        registerChooseNowEvent: function () {
            $('.js-choose-now').click($.proxy(function (e) {
                var _this = this,
                    $button = $(e.target),
                    $form = $button.closest('form');

                _this.getCurrentTime(
                    $form.find('.js-city_id').val(),
                    null,
                    $form.data('is-client') == 1
                )
                        .done(function (data) {
                            if (data) {
                                $form.find('.js-now').val(1);
                                _this.setDatetime($form, data);
                                _this.setTitle($form, $button.data('title'));
                                _this.closeDatepicker($form);
                            };
                        })
                        .fail(function (data) {
                            console.log('fail', data);
                        });
            }, this));
        },

        registerIncrementDateEvent: function () {
            $('.js-inc-date').click($.proxy(function (e) {
                var _this = this,
                    $button = $(e.target),
                    $form = $button.closest('form');

                _this.getCurrentTime(
                        $form.find('.js-city_id').val(),
                        $button.data('time'),
                        $form.data('is-client') == 1
                )
                        .done(function (data) {
                            $form.find('.js-now').val(0);
                            _this.setDatetime($form, data);
                            _this.setTitle($form, 
                                    $.datepicker.formatDate(
                                        $form.find('.sdp_n').datepicker('option', 'dateFormat'),
                                        $form.find('.sdp_n').datepicker('getDate'))
                                        + ' ' + data.hours + ':' + data.minutes);
                            _this.closeDatepicker($form);
                        })
                        .fail(function (data) {
                            console.log('fail', data);
                        });
            }, this));
        },

        getCurrentTime: function (city_id, offset, isClient) {
            return $.post(isClient ? 'get-current-time' : '/order/order/get-current-time', {
                city_id: city_id,
                offset: offset || 0
            });
        },

        setDatetime: function ($form, data) {
            $form.find('.sdp_n').datepicker('setDate', new Date(data.date));
            $form.find('.js-hours').val(data.hours);
            $form.find('.js-minutes').val(data.minutes);
            $form.find('.js-seconds').val(0);
        },

        setTitle: function ($form, title) {
            $form.find('.ordered_when').html(title);
        },

        closeDatepicker: function ($form) {
            $form.find('.ordered_when').removeClass('sel_active');
            $form.find('.b_sc').hide();
            $(document).trigger('getPrice');
        }
    };

    return {
        init: function () {
            app.init();
        }
    };
})(jQuery);
