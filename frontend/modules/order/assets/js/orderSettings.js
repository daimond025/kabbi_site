
function refreshFormOrder(cityId, positionId, orderId) {
    var isClient = isUseClient();
    var url = isClient ? '/cabinet/order/get-settings-order' : '/order/get-settings-order';
    $.get(
        url,
        {city_id: cityId, position_id: positionId, order_id: orderId},
        function (json) {
            var settingsOrder = JSON.parse(json);
            changeFieldsCourier(settingsOrder);
        }
    );
}

function refreshFormExistOrder(orderId) {
    var isClient = isUseClient();
    var url = isClient ? '/cabinet/order/get-settings-exist-order' : '/order/get-settings-exist-order';
    $.get(
        url,
        {order_id: orderId},
        function (json) {
            var settingsOrder = JSON.parse(json);
            changeFieldsCourier(settingsOrder);
        }
    );
}

function isUseClient() {
    var isClient = $('#order_create').data('is-client');
    if (isClient === undefined) {
        isClient = $('#order_update').data('is-client');
    }

    return Boolean(isClient);
}

function changeFieldsCourier(settingsOrder) {
    if (settingsOrder !== null && isHasOrderSettingsCourier(settingsOrder)) {
        var isCopyOrder = $('#order_create').data('is-copy-order');
        $('li.order-window__points__item').map(function (key, elem) {
            var $elem = $(elem);
            if (!$elem.hasClass('order-window__points__item--courier')) {
                $elem.addClass('order-window__points__item--courier');
            }

            if (settingsOrder.orderFormAddPointPhone == 1) {
                $elem.find('.phone').show();
            } else {
                $elem.find('.phone').hide();
            }

            if (settingsOrder.orderFormAddPointComment == 1) {
                $elem.find('.comment').show();
            } else {
                $elem.find('.comment').hide();
            }

            if (settingsOrder.requirePointConfirmationCode == 1 && isCopyOrder != 1) {
                $elem.find('.code').show();
            } else {
                $elem.find('.code').hide();
            }
        });
    } else {
        $('li.order-window__points__item').map(function (key, elem) {
            var $elem = $(elem);
            $(elem).removeClass('order-window__points__item--courier');

            $elem.find('.phone').hide();
            $elem.find('.comment').hide();
            $elem.find('.code').hide();
        });
    }
}

function isHasOrderSettingsCourier(settingsOrder) {
    if (settingsOrder.requirePointConfirmationCode == 1
        || settingsOrder.orderFormAddPointPhone == 1
        || settingsOrder.orderFormAddPointComment == 1
    ) {
        return true;
    }

    return false;
}