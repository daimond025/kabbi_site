function registerOrderOpenInModalEvent(selector) {
    $(document).on('click', selector, function (e) {
        var FORM_CLASS = 'edit-order-fake',
            selector = '.' + FORM_CLASS;

        e.preventDefault();

        $orderForm = $(selector);
        if ($orderForm.length == 0) {
            $orderForm = $('<div/>').addClass(FORM_CLASS);
            $orderForm.appendTo(document.body);
            $orderForm.gootaxModal({
                onComplete: function () {
                    select_init();

                    var orderId = $('#order-id').data('order_id');
                    refreshFormExistOrder(orderId);
                },
                onClosed: function () {
                }
            });
        }
        $orderForm.gootaxModal('settings', {url: $(this).attr('href')})
            .gootaxModal('open');
    });
}
