/*global generalMap*/
$(function () {

    _timerId = null;
    _updateWorkerLocation = null;

    _timeoutGetPrice = null; // Отсчет времни, после которого произойдет подсчет


    function getClientInfo() {
        var $phone = $('.mask_phone'),
            $cityId = $('#order-city_id');

        if ($phone.attr('is-valid')) {
            fillClientInfo($phone.val(), $cityId.val());
        } else {
            clearClientInfo();
        }
    }

    var body = $('body');

    //Оплата
    $(document).on('change', '#order_payment', function () {
        var form = $(this).closest('form');
        var $selected = $(this).find(':selected');

        if ($selected.data('company')) {
            $('#order-company_id').val($selected.data('company'));
        } else {
            $('#order-company_id').val('');
        }

        updateCorpBalanceInfo($selected.data('balance'), $selected.data('credit'));
        getTariffs(form);
    });


    //Сокрытие поиска
    body.on('click', function () {
        if ($(this).hasClass('driverSearch')) {
            $('ul.driver_auto').hide();
            $(this).removeClass('driverSearch');
        }
    });


    //Подгрузка парковок
    body.on('change', '.fl_mas .fl_minii:first-child input', function (e) {
        var parent = $(this).parents('.fl_inp');
        var city_input = parent.find('.fl_city input');
        var street_input = parent.find('.fl_street input');
        var city_id = $('#order-city_id').val();
        var street = street_input.val();
        var address = city_input.val() + ', ' + street + ', ' + $(this).val();
        var select = parent.find('.fl_dist .default_select');

        getParkings('/parking/parking/get-parking-by-address', {city_id: city_id, address: address}, select);
    });

    $(document).on('change', '.fl_dist .default_select', function () {
        var $parkingBlock = $(this).closest('.fl_dist'),
            $selected = $(this).find('option:selected');

        $parkingBlock.find('.js-parking').val($selected.val() ? $selected.text() : '');
        $parkingBlock.find('.js-parking_id').val($selected.val());
    });

    //Errors in order window
    function showError(text) {
        $('#order-error').text(text).show();
        $('.order-window__errors').show();
        $('.gootax-modal-wrapper').animate({scrollTop: $('#order-error').offset().top}, 100);

    }

    function clearErrors() {
        $('#order-error').text('').hide();
        $('.order-window__errors').hide();
    }

    //Смена филиала заказа
    $(document).on('change', '#order-city_id', function () {
        var city_id = $(this).find(':selected').val();
        var position_id = $('.order-window #order-position_id :selected').val();
        var name = $(this).find(':selected').text();
        var $form = $(this).closest('form');

        if ($('#order-phone').val() != '') getClientInfo();

        var orderId = $('#order-id').data('order_id');
        refreshFormOrder(city_id, position_id, orderId);

        $('.js-order-point').each(function () {
            getParkings('/parking/parking/get-parking-by-coords', {
                city_id: city_id,
                lat: $(this).find('.lat').val(),
                lon: $(this).find('.lon').val()
            }, $(this).find('.default_select'));
        });

        $.get(
            '/city/city/get-city-currency-symbol',
            {city_id: city_id},
            function (json) {
                $('span.currency_symbol').text(json);
            },
            'json'
        );

        $.post(
            '/order/order/get-position-list',
            {city_id: city_id}
        )
            .done(function (data) {
                var $positionList = $('#order-position_id'),
                    firstId, firstValue, $option;

                $positionList.empty();
                for (position in data) {
                    if (!firstId) {
                        firstId = position;
                        firstValue = data[position];
                    }
                    $option = $('<option/>').val(position).text(data[position]);
                    $positionList.append($option);
                }

                $positionList.attr('data-placeholder', firstValue);
                $positionList.val(firstId);
                $positionList.trigger('update');

                getTariffs();
            })
            .fail(function (data) {
                console.log('fail', data);
            });

        $.post(
            '/city/city/get-city-coords',
            {city_id: city_id},
            function (json) {
                if (json != 'error') {
                    var parent = $('.city-select');
                    parent.find('input.lat').val(json.lat);
                    parent.find('input.lon').val(json.lon);
                }
            },
            'json'
        );

    });

    $(document).on('change', '#order-position_id', function () {
        var cityId = $('#order-city_id').val(),
            positionId = $(this).val();

        if ($(this).closest('form').data('is-client')) {
            isFixFieldReload(cityId, positionId);
        }

        getTariffs();
    });

    function isFixFieldReload(cityId, positionId) {
        $.get(
            'get-is-fix-field',
            {cityId: cityId, positionId: positionId},
            function (html) {
                $('.js-is-fix').html(html);
            }
        );
    }

    function getDateObject(date){
        if(date === undefined)
        {
            return ;
        }
        let data = date.split("-");

        if(data.length <= 1) {
            return new Date();
        }
        return new Date(data[0], data[1] - 1, data[2], 0, 0, 0, 0);
    }


    let dateFromDate =  getDateObject($('.date_input').val());
    let dateToDate =  getDateObject($('.date_input_to').val());

    //РљР°Р»РµРЅРґР°СЂСЊ РЅР° index.php
    $('.sdp_f').datepicker({
        altField: $(this).find('.date_input'),
        altFormat: 'yy-mm-dd',
        maxDate: dateToDate,
        onSelect: function () {
            var date = $(this).find('.date_input').val();
            setAttr('date_from', date);
        }
    });

    var urlVar = getUrlVar();
    var calendar_date = urlVar && urlVar['date_from'] ? urlVar['date_from'] : '';
    var parsedDate = $.datepicker.parseDate('yy-mm-dd', calendar_date);
    $('.sdp_f').datepicker('setDate', parsedDate);


    $('.sdp_f2').datepicker({
        altField: $(this).find('.date_input_to'),
        altFormat: 'yy-mm-dd',
        gotoCurrent: false,
        minDate: dateFromDate,
        onSelect: function () {
            var date = $(this).find('.date_input_to').val();
            setAttr('date_to', date);
        }
    });

    var urlVar2 = getUrlVar();
    var calendar_date2= urlVar2 && urlVar2['date_to'] ? urlVar2['date_to'] : '';
    var parsedDate2 = $.datepicker.parseDate('yy-mm-dd', calendar_date2);
    $('.sdp_f2').datepicker('setDate', parsedDate2);


    var timerForHour = Date.now();
    //РўР°Р№РјРµСЂ
    setInterval(function () {
        let timerForHourNow = Date.now();
        let diffTimerHour  = timerForHourNow - timerForHour;
        if (Math.abs(diffTimerHour) > 100 && Math.abs(diffTimerHour) <= 900){
            return
        }
        timerForHour = timerForHourNow;

        var timer = new Timer();
        timer.colon($('.time_colon')); //Двоеточие

        //Местное время городов
        $('.order_cities li').each(function () {
            var hours = $(this).find('.hours');
            var minutes = $(this).find('.minutes');
            var seconds = $(this).find('.seconds');
            var clock_obj = timer.clock(hours.text(), minutes.text(), seconds.text());

            hours.text(clock_obj.hours);
            minutes.text(clock_obj.minutes);
            seconds.text(clock_obj.seconds);
        });

        //Счетчик заказов
        $('.order_table .timer').each(function () {
            timer.run($(this).find('.minutes'), $(this).find('.seconds'), $(this).data('type'));
        });

    }, 1000);

    //Клик на табы
    $(document).on('click', '.tnums ul li a', function () {
        var url = $(this).data('ajax');
        var div_id = $(this).attr('class');

        if (($(this).data('once') == 1)) {
            return;
        }
        $('#' + div_id).load(url);

    });

    var datatabsid = 0;
    $('body').on('click', '.orders_links li a', function () {
        tabToggle($(this));
    });
    if ($('body .orders_links li a') != undefined && location.hash != '') {
        tabToggle($('.orders_links li a[href=\'' + location.hash + '\']'));
    }

    function tabToggle(tab) {
        var activeTabId = '#' + tab.attr('class');
        setCookie('tab_active_id', tab.attr('class'), '');

        if (!tab.parents('.orders_links').attr('data-tabs')) {
            tab.parents('.orders_links').attr('data-tabs', datatabsid);
            tab.parents('.orders_links').next('.tabs_content:first').attr('data-tabs-content', datatabsid);
            datatabsid++;
        }
        var currenttabs = tab.parents('.orders_links').attr('data-tabs');

        $('[data-tabs="' + currenttabs + '"]').find('.active').removeClass('active');
        $('[data-tabs-content="' + currenttabs + '"]').children('.active').removeClass('active');
        tab.parent().addClass('active');

        if ((tab.attr('data-href') || tab.attr('data-ajax')) && !$(activeTabId).hasClass('ajax_loaded')) {
            var loadType;
            if (tab.data('href') != undefined) {
                loadType = 'href';
            }
            else if (tab.data('ajax') != undefined) {
                loadType = 'ajax';
            }

            if (tab.data(loadType)) {
                $(activeTabId).addClass('active').addClass('file_loader');

                $(activeTabId).load(tab.data(loadType), function () {
                    $(activeTabId).removeClass('file_loader');
                    $(activeTabId).addClass('ajax_loaded');
                    //$(activeTabId).trigger('tab-loaded');
                    $(tab).trigger('tab-loaded');
                })
            }
        }
        else {
            $(activeTabId).addClass('active');
        }
    }

    $('.order_cities li a.active').on('click', function (e) {
        e.preventDefault()
    });

    //Р¤РёР»СЊС‚СЂ СЃС‚Р°С‚СѓСЃРѕРІ todo del
    /* $(document).on('click', 'ul.sortable input', function () {
         var orders = $(this).parents('div.active').find('.order_table tr').not(':first');
         var ul = $(this).parents('ul');
         var input_checked = ul.find('input:checked');

         orders.filter(':hidden').show();

         if ($(this).val() == '0') {
             input_checked.not(':first').removeAttr('checked');
         }
         else if (input_checked.size() > 0) {
             ul.find('input').filter(':first').removeAttr('checked');

             input_checked.each(function () {
                 orders = orders.not('.status_' + $(this).val());
             });

             orders.hide();
         }
     });*/

    //Р¤РёР»СЊС‚СЂ СЃС‚Р°С‚СѓСЃРѕРІ РёР· СЂРµРґРёСЃР° todo del
    /* $(document).on('click', 'ul.redis_sort input[type="checkbox"]', function () {
         var ul = $(this).parents('ul');
         var input_checked = ul.find('input:checked');
         var arr_status_id = [];
         var tbody = ul.next('table').find('tbody');
         var sort_class = tbody.find('tr:first a span').attr('class');
         var sort = null;
         var filter = {status: null};

         if ($(this).val() == '0') {
             input_checked.not(':first').removeAttr('checked');
         }
         else if (input_checked.size() > 0) {
             ul.find('input').filter(':first').removeAttr('checked');

             input_checked.each(function () {
                 arr_status_id.push($(this).val());
             });
         }

         sort = sort_class === 'pt_down' ? 'desc' : 'asc';
         filter.status = arr_status_id;

         $.post(
             $(this).parents('ul').data('url'),
             {sort: sort, filter: filter},
             function (html) {
                 put_search(tbody, html);
             }
         );
     });*/

    var timeout;

    $(document).on('keyup', '.order_search_string', function () {

        clearTimeout(timeout);

        var _this = this;

        var filter = {searchString: null, tab: null};
        var sort = null;
        var tbody = $('.tabs_content div.active');

        filter.searchString = $(_this).val();

        timeout = setTimeout(function () {

            function ajaxQuery(visibility) {

                $.ajax({
                    type: "POST",
                    url: $(_this).data('url'),
                    data: {sort: sort, filter: filter},
                    dataType: "html",
                    beforeSend: function () {
                        tbody.addClass('loading_osn_content');
                    },
                    success: function (html) {
                        tbody.removeClass('loading_osn_content');
                        $('.tabs_content div.active').empty();
                        put_search(tbody, html);
                        $('ul.orders_filter').css('display', visibility);
                        $('.tabs_links').css('display', visibility);
                    }
                });

            }

            if (filter.searchString !== '') {
                ajaxQuery('none');
            } else {
                filter.tab = $('.tabs_links .active').data('group');
                ajaxQuery('block');
            }

        }, 500);

    });


    //РЎРѕСЂС‚РёСЂРѕРІРєР° С‚Р°Р±Р»РёС† todo del
    /* $(document).on('click', 'table.sortable th a', function (e) {
         e.preventDefault();
         $(this).parents('tr').find('a').removeClass('sort');
         $(this).addClass('sort');
         sort_order_table($(this));
     });

     //РЎРѕСЂС‚РёСЂРѕРІРєР° Р·Р°РєР°Р·РѕРІ РЅР° СЃРµРіРѕРґРЅСЏ РёР· Р‘Р”
     $(document).on('click', 'table.redis_sort th a', function (e) {
         e.preventDefault();
         $(this).parents('tr').find('a').removeClass('sort');
         $(this).addClass('sort');
         var sort_span = $(this).find('span');
         var sort_class = sort_span.attr('class');
         $(this).parents('tr').find('a span').attr('class', '');
         var sort = null;
         var tbody = $(this).parents('tbody');
         var status_filter = $(this).parents('table').prev('ul').find('input:gt(0)').filter(':checked');
         var filter = {status: []};
         var attr = $(this).data('name');

         if (status_filter.size() > 0) {
             status_filter.each(function () {
                 filter.status.push($(this).val());
             });
         }

         if (sort_class === 'pt_up') {
             sort = 'desc';
             sort_class = 'pt_down';
         }
         else {
             sort = 'asc';
             sort_class = 'pt_up';
         }

         $.post(
             $(this).attr('href'),
             {sort: sort, filter: filter, attr: attr},
             function (html) {
                 put_search(tbody, html);
             }
         );

         sort_span.attr('class', sort_class);
     });*/

    //Расчет стоимости заказа
    $(document).on('getPrice', function () {
        var $form = $('#order_create, #order_update');

        if ($form) {
            $form.append('<input hidden name="changeOption" value="1" >');
            getAddOptions($form);
            getOrderPriceAnalyz($form);
        }
    });

    function triggerGetPriceEvent() {
        setSubmitStatus('loading');
        clearTimeout(_timeoutGetPrice);
        _timeoutGetPrice = window.setTimeout(function () {
            $(document).trigger('getPrice');
        }, 500);
    }

    $(document).on('change', '#order-tariff_id, .comfort_list input, #order-city_id, select.parkings', function () {
        triggerGetPriceEvent();
    });

    $(document).on('change', '#order-tariff_id', function () {
        getAddOptions($(this).closest('form'));
        autoChangeTariffDescription();
        $(document).trigger('checkCanBonusPayment');
        $.colorbox.resize();
    });

    call_init();

    //Всплывающее окно водителей на парковках
    body.on('click', '.order_status>div', function (e) {
        if ($(this).find('div').css('display') == 'block') {
            $('.order_status>div div').hide();
        } else {
            $('.order_status>div div').hide();
            $(this).find('div').show();
            e.preventDefault();
            e.stopPropagation();
            $(this).find('div').on('click', function (e) {
                e.stopPropagation()
            });
            body.on('click', function () {
                $('.order_status>div div').hide();
            });
            var cur_div = $(this);
            $.get(
                cur_div.data('href'),
                {},
                function (html) {
                    cur_div.find('div').html(html);
                }
            );
        }
    });

    $(".open_map_win").gootaxModal({
        inline: true,
        url: '#map_win',
        windowStyle: 'padding: 20px; left: 5%; top: 5%; width: 90%; height: 90%; margin: 0; box-sizing: border-box;',
        onComplete: function () {
            window.generalMap && generalMap.initialize();
        },
        onClosed: function () {
            window.generalMap && generalMap.finalize();
        }
    });


    $(document).on('updateMap', function () {
        clearErrors();
    });

    $(document).on('initialize-map', '.js-order-map', function () {
        var $form = $(this).closest('form'),
            hash = Math.round(Math.random() * 100000);

        $.ajax({
            type: "POST",
            url: $("#add-order").data('module') + 'order/route-analyzer',
            data: $form.serialize(),
            dataType: "json",
            beforeSend: function () {
                $(document).trigger('disableMap');
                _requests[hash] = true;
            }
        })
            .done(function (data) {
                if (data instanceof Object) {
                    $(document).trigger('updateMap', [data.routeLine]);
                }
            })
            .always(function () {
                $(document).trigger('enableMap');

                delete _requests[hash];
                if (!Object.keys(_requests).length) {
                    $($form).find('.submit_form .js-save-order').removeClass('loading_button');
                }
            });
    });

    $(document).on('update-marker-coords', function (e, $address) {
        var promise = refreshParkings($address);

        promise.then(function () {
            getOrderPriceAnalyz($address.closest('form'));
        });
    });

    $(document).on('click', '.js-stop-offer', function () {
        var orderNumber = $('#order-order_number').val();

        $.post('/order/order/stop-offer', {order_number: orderNumber})
            .done(function (data) {
                var page = location.href,
                    controller;
                if (data instanceof Object && data.code === 1) {
                    controller = page.search('/operator/') !== -1
                        ? 'operator' : 'order';
                    location.href = '/order/' + controller + '/index' + location.search;
                }
            })
            .fail(function (data) {
                console.log('fail', data);
            });

        return false;
    });

    $(document).on('routeAnalyzeComplete', function () {
        var $elem = $('#order-is_fix');

        if ($elem.prop('checked')) {
            $elem.prop('checked', false).val('0');
        }
    });

    // Предложить свою цену
    $(document).on('change', '#order-is_fix', function () {
        var $this = $(this),
            $elem = $('#order-predv_price'),
            checked = $this.prop('checked'),
            canUpdate = $elem.data('can-update');

        $this.val(checked ? '1' : '0');
        $elem.prop('readonly', !(checked && canUpdate == 1));

        if (!checked) {
            triggerGetPriceEvent();
        }
    });

    function getRejectOrderResult(requestId, retryCount) {
        var NOT_RECEIVED_RESULT = null,
            RESULT_OK = 1;

        retryCount--;
        if (!retryCount) {
            location.reload();
            return;
        }

        $.get('get-reject-order-result', {requestId: requestId})
            .done(function (data) {
                if (data instanceof Object) {
                    if (data.result === NOT_RECEIVED_RESULT) {
                        setTimeout(function () {
                            getRejectOrderResult(requestId, retryCount);
                        }, 1000);
                    } else if (data.result === RESULT_OK) {
                        location.reload();
                    } else {
                        showError(data.error);
                    }
                } else {
                    location.reload();
                }
            })
            .fail(function (data) {
                console.log('fail', data);
            });
    }

    // Отмена заказа клиентом
    $(document).on('click', '.js-cancel-order', function () {
        var _this = $(this);

        clearErrors();

        $.post('cancel-order', {order_id: _this.data('order_id')})
            .done(function (data) {
                if (data instanceof Object && data.requestId) {
                    var retryCount = 5;
                    getRejectOrderResult(data.requestId, retryCount);
                } else {
                    location.reload();
                }
            })
            .fail(function (data) {
                console.log('fail', data);
            });
        return false;
    });

    //Показать/скрыть расшифровку данных
    body.on('click', '.js-raw-calc-data-description-toggle', function () {
        $('.js-raw-calc-data-description').toggle(100);
        setTimeout(function () {
            $.colorbox.resize();
        }, 100);
    });


    //Order actions
    body.on('click', '.toggle-actions', function (event) {
        event.stopImmediatePropagation();
        var obj = $(this).parents('.order-window__points__item');

        if ($(obj).hasClass('active-actions')) {
            $('.active-actions').removeClass('active-actions');
            setTimeout(function () {
                $(obj).css('overflow', 'visible')
            }, 300);
        } else {
            $(obj).css('overflow', 'hidden');
            $(obj).addClass('active-actions');
        }
    });

    body.on('click', '.toggle-hidden-items', function () {
        $(this).parent().find('.hidden-items').toggle(100);
        $(this).remove();
    })


    //Add order point

    body.on('click', '.js-add-order-point', function (event) {
        event.stopImmediatePropagation();
        if (Number($('#sortable').attr('data-points')) <= 19) {
            $(this).removeClass('disabled');
            var point = $('#order-point-pattern').html();
            point = point.replace('order-window__points__item', 'order-window__points__item js-order-point');
            $('#sortable').append(point);
            $('#sortable').find('.new-autocomplete')
            .removeClass('new-autocomplete')
            .addClass('autocomplete')
            .autocomplete({
                source: autocomplete.source(),
                minLength: 2,
                select: autocomplete.select(),
                search: autocomplete.search(),
                open: autocomplete.open(),
                change: autocomplete.change(),
                create: autocomplete.create(),
                focus: autocomplete.focus()
            });
            $(document).trigger('update-order-points');
            phone_mask_init()
        } else {
            $(this).addClass('disabled');
        }

    });

    //Delete point
    body.on('click', '.delete-point', function (event) {
        event.stopImmediatePropagation();
        if (Number($('#sortable').attr('data-points')) > 2) {
            $(this).parents('li.js-order-point').toggle(50);
            var _this = this;
            setTimeout(function () {
                $(_this).parents('li.js-order-point').remove();
                $(document).trigger('update-order-points');
            }, 50);
        } else {
            $(this).parents('li.js-order-point').find('input').val('');
            $(this).parents('li.js-order-point').find('.default_select option:selected').removeAttr('selected');
            $(this).parents('li.js-order-point').find('.default_select').trigger('update');
            $(this).parents('li.js-order-point').find('.toggle-actions').click();
            getOrderPriceAnalyz($('#sortable').parents('form'));
        }
    });

    //Change client

    body.on('click', '.js-change-client', function () {
        clearClientInfo();
    });

    //Change worker

    body.on('click', '.js-change-driver', function () {
        clearDriverInfo();
    });

    //Add client

    body.on('click', '.js-add-client', function () {
        $('#order-phone').val($('[name="autocomplete-clients"]').val());
        $('[name="autocomplete-clients"], .js-add-client-inputs').toggle();
        $(this).hide();
        fillPayments(null);
    });

    body.on('click', '.js-hide-add-client', function () {
        // $('#order-phone').val($('[name="autocomplete-clients"]').val());
        $('[name="autocomplete-clients"], .js-add-client-inputs').toggle();
        $('.js-add-client').show();
        fillPayments(null);
    });

    //Remove error from empty inputs on change
    body.on('change', '.input_error', function () {
        if ($(this).val() != '') $(this).removeClass('input_error');
    });

    //Reject order select
    body.on('change', '#order-orderaction', function () {
        var ACTION_COMPLETE_PAY_ORDER = 'COMPLETE_PAY_ORDER';
        var ACTION_REJECT_ORDER = 'REJECT_ORDER';
        var ACTION_COMPLETE_NO_PAY_ORDER = 'COMPLETE_NO_PAY_ORDER';

        if ([ACTION_COMPLETE_PAY_ORDER, ACTION_COMPLETE_NO_PAY_ORDER].indexOf($(this).val()) >= 0) {
            $('.js-summary-cost').show();
        } else {
            $('.js-summary-cost').hide();
        }

        if ($(this).val() == ACTION_REJECT_ORDER) {
            $('#rejected_reason').show();
        } else {
            $('#rejected_reason').hide();
        }
    });

    if ($('.js-add-client input').prop('checked')) {
        fillPayments(null);
    }

    //Address history

    body.on('click', '.js-address-history', function (e) {
        var input = $(this).parent().find('.ui-autocomplete-input');
        $(input).autocomplete('option', 'source', autocomplete.historySource()).autocomplete('option', 'minLength', 0).autocomplete("search", "");
        e.stopImmediatePropagation();
    });

    body.on('autocompletechange focus', '.street .ui-autocomplete-input', function (event, ui) {
        if ($(this).autocomplete('option', 'minLength') == 0) {
            $(this).autocomplete('close').autocomplete('option', 'source', autocomplete.source()).autocomplete('option', 'minLength', 2);
        }
    });

    body.on('click', function (e) {
        var autocomplete = $('.ui-autocomplete, .js-address-history');

        if (!$(autocomplete).is(e.target) && $(autocomplete).has(e.target).length === 0) {
            $('.street .ui-autocomplete-input').autocomplete('close');
        }
    });

    //Free drivers
    body.on('click', '.js-order-free-drivers', function (e) {
        e.stopImmediatePropagation();
        var input = $(this).parent().find('.ui-autocomplete-input');
        $(input).autocomplete('option', 'source', autocompleteDrivers.freeDriversSearch()).autocomplete('option', 'minLength', 0).autocomplete("search", "");
    });

    body.on('autocompletechange focus', '.order-window__driver .ui-autocomplete-input', function (event, ui) {
        if ($(this).autocomplete('option', 'minLength') == 0) {
            $(this).autocomplete('close').autocomplete('option', 'source', autocompleteDrivers.source()).autocomplete('option', 'minLength', 2);
        }
    });

    body.on('click', function (e) {
        var autocomplete = $('.ui-autocomplete, .js-order-free-drivers');
        if (!$(autocomplete).is(e.target) && $(autocomplete).has(e.target).length === 0) {
            $('.order-window__driver .ui-autocomplete-input').autocomplete('close');
        }
    });

    // company
    body.on('click', '.js-order-free-company', function (e) {
        e.stopImmediatePropagation();
        var input = $(this).parent().find('.ui-autocomplete-input');
        $(input).autocomplete('option', 'source', autocompleteCompany.searchCompany()).autocomplete('option', 'minLength', 0).autocomplete("search", "");
    });
});
