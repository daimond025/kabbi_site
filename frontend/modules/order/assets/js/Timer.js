function Timer() {
    var _this = this;

    this.clock = function (hours, minutes, seconds) {
        var clock = {};

        clock.hours = hours;
        clock.minutes = minutes;
        clock.seconds = seconds;

        if (clock.seconds == '59') {
            if (clock.minutes == '59' ) {
                if (clock.hours == '23') {
                    clock.hours = '00';
                } else {
                    clock.hours++;
                    if (clock.hours < 10) {
                        clock.hours = '0' + clock.hours;
                    }
                }
                clock.minutes = '00';
            } else {
                clock.minutes++;
                if (clock.minutes < 10) {
                    clock.minutes = '0' + clock.minutes;
                }
            }
            clock.seconds = 0;
        } else {
            clock.seconds++;
        }

        return clock;
    };

    this.colon = function (selector) {
        if (selector.css('opacity') == 0) {
            selector.css('opacity', 1);
        } else {
            selector.css('opacity', 0);
        }
    };

    this.countup = function (minutes_selector, seconds_selector) {
        var minutes = minutes_selector.text();
        var seconds = Number(seconds_selector.text());

        minutes = Number(minutes.replace(/-*/i, ''));

        seconds++;

        if (seconds > 59) {
            seconds = '00';
            minutes++;

            if (minutes < 10) {
                minutes = '0' + minutes;
            }

            minutes_selector.text('-' + minutes);
        } else if (seconds < 10) {
            seconds = '0' + seconds;
        }

        seconds_selector.text(seconds);
    };

    this.countdown = function (minutes_selector, seconds_selector) {
        var minutes = Number(minutes_selector.text());
        var seconds = Number(seconds_selector.text());

        if (minutes == 0 && seconds == 0) {
            minutes_selector.text('-00').parents('.timer').data('type', 'up').removeClass('ot_green').addClass('ot_red');
            seconds_selector.text('01');

            return false;
        }

        seconds--;

        if (seconds < 0) {
            seconds = 59;
            minutes--;

            if (minutes < 10) {
                minutes = '0' + minutes;
            }

            minutes_selector.text(minutes);
        } else if (seconds < 10) {
            seconds = '0' + seconds;
        }

        seconds_selector.text(seconds);
    };

    this.run = function (minutes_selector, seconds_selector, type) {
        if (type == 'down') {
            _this.countdown(minutes_selector, seconds_selector);
        } else {
            _this.countup(minutes_selector, seconds_selector);
        }
    }
}