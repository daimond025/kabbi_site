/*global document, jQuery*/
(function (document, $) {
    var app = {
        init: function () {
            this.registerEvents();
        },
        checkCanBonusPayment: function () {
            var _this = this,
                    phone, cityId, tariffId;

            if ($('#order_payment').find(':selected').data('company')) {
                _this.hideBonusPayment();
                return;
            }

            phone = $('.mask_phone').val();
            cityId = $('#order-city_id').val();
            tariffId = $('#order-tariff_id').val();

            $.get(
                $("#add-order").data('module') + 'order/can-bonus-payment',
                    {
                        phone: phone,
                        cityId: cityId,
                        tariffId: tariffId
                    }
            )
                    .done(function (data) {
                        if (data) {
                            _this.showBonusPayment();
                        } else {
                            _this.hideBonusPayment();
                        }
                    })
                    .fail(function (data) {
                        console.log('fail', data);
                        _this.hideBonusPayment();
                    });
        },
        hideBonusPayment: function () {
            $('#order-bonus_payment').prop('checked', false);
            $('.bonus_payment').hide();
        },
        showBonusPayment: function () {
            $('.bonus_payment').show();
        },
        registerEvents: function () {
            var _this = this;

            $(document).on('checkCanBonusPayment', function () {
                _this.checkCanBonusPayment();
            });

            $(document).on('change', '#order_payment', function (e) {
                if (!$(e.target).find(':selected').data('company')) {
                    _this.checkCanBonusPayment();
                } else {
                    _this.hideBonusPayment();
                }
            });
        }
    };

    app.init();
})(document, jQuery);
