/**
 * Created by artem on 29.08.16.
 */
$(function () {
    //Обновление заказов
    var date = new Date();
    var urlVar = getUrlVar();
    var calendar_date = urlVar !== false && urlVar['date'] !== undefined ? urlVar['date'] : null;

    if (calendar_date === null || formatDate(date) == calendar_date) {
        var city_id = urlVar !== false && urlVar['city_id'] !== undefined ? urlVar['city_id'] : null;
        setInterval(function () {
            var ul = $('div.tnums ul');
            var div_id = ul.find('li.active a').attr('class');
            var active_div = $('#' + div_id);
            var tbody = active_div.find('tbody');

            //Текущая открытая вкладка заказов и вью для отображения
            var cur_group = ul.find('li.active').data('group');
            var view = tbody.size() > 0 ? '_' + cur_group : cur_group;

            //Сортировка
            var sort_link = tbody.find('th a.sort');
            var sort_class = sort_link.find('span').attr('class');
            var sort = sort_class === 'pt_down' ? 'asc' : 'desc';
            var orderBy = sort_link.data('orderby');

            var attr = tbody.find('th a.sort').data('name');

            //Статус фильтр
            var status_filter = active_div.find('ul.orders_filter').find('input:gt(0)').filter(':checked');
            var filter = {status: []};

            if (status_filter.size() > 0) {
                status_filter.each(function () {
                    filter.status.push($(this).val());
                });
            }

            //Определение контроллера
            var url = location.href;
            var controller = url.search('/operator/') != -1 ? 'operator' : 'order';

            $.ajax({
                url: 'order-refresh?controller=' + controller,
                data: {
                    group: cur_group,
                    city_id: city_id,
                    sort: sort,
                    orderBy: orderBy,
                    filter: filter,
                    view: view,
                    attr: attr
                },
                dataType: 'json',
                timeout: 3000,
                method: 'post',
                success: function (json) {
                    if (json != '') {

                        //Обновление счетчиков
                        if (typeof(json.count) !== 'undefined') {
                            for (var key in json.count) {
                                ul.find('li[data-group="' + key + '"] sup').text(json.count[key] ? json.count[key] : '');
                            }
                        }

                        //Обновление заказов
                        if (tbody.size() > 0) {
                            put_search(tbody, json.html);
                        }
                        else {
                            active_div.html(json.html);
                        }
                    }
                }
            }).fail(function (xhr) {
                if (xhr.status == 403) {
                    window.location.href = '/login';
                }
            });
        }, 5000);
    }
    //-------------------------------------------------------
});