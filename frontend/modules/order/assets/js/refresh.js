$(function(){

    // обновить параметры из grida
    $('.orders_links li a').on('tab-loaded', function(){
        reCountTab();
    });

    function reCountTab(){
        let tab_A = $('.orders_links li.active a');
        let activeTab = '#' + $(tab_A).attr('class');
        let div_grid = $(activeTab).find('.pjax_url');

        let total = $(div_grid).data('total');
        if(total >= 0){
            let current = ($(tab_A).find('#current').length > 0) ? $(tab_A).find('#current') : $(tab_A).find('#all_count');
            if($(current).length === 1){
                if(parseInt(total) === 0){
                    total = '0';
                }
                $(current).text(total);
            }
        }
    }

    $(document).on('pjax:complete', function(){
        let nameBlock = ul.find('li.active a').data('ids');
        playSignalForNewOrder(orders_refresh, nameBlock);
        updateWorkers(workers_refresh);
        reCountTab();
    });

    function updateWorkers(workers){
        if(workers != null){
            //Обновление количества водителей
            let worker_free = typeof workers.free !== undefined ? parseInt(workers.free) : 0;
            let worker_busy = typeof workers.busy !== undefined ? parseInt(workers.busy) : 0;

            $('div.order_status div.s_green > span').html('<i></i> ' + worker_free);
            $('div.order_status div.s_red > span').html('<i></i> ' + worker_busy);
            $('div.order_status > b span').html(worker_free + worker_busy);
        }
    }

    //Обновление заказов
    var date = new Date();
    var urlVar = getUrlVar();
    var calendar_date = urlVar !== false && urlVar['date'] !== undefined ? urlVar['date'] : null;

    var calendar_date_from = urlVar !== false && urlVar['date_from'] !== undefined ? urlVar['date_from'] : null;
    var calendar_date_to = urlVar !== false && urlVar['date_to'] !== undefined ? urlVar['date_to'] : null;

    var currentOrdersActive = [];
    var isFirstQuery = true;
    var tabName = '';

    var ul = $('div.tnums ul');

    try{
        $(".audioSignalNewOrder").trigger('load');
    }catch(e){
        console.error('not support audio: ' + e.message)
    }


    var playSignalForNewOrder = function(object, tab){
        if(tab != tabName){
            isFirstQuery = true;
            currentOrdersActive = [];
            tabName = tab;
        }
        var newOrdersActive = getUniqValues(object);

        if(isFirstQuery){
            currentOrdersActive = newOrdersActive;
            isFirstQuery = false;
        }else{
            if(isNewValue(currentOrdersActive, newOrdersActive)){
                try{
                    $(".audioSignalNewOrder").trigger('play');
                }catch(e){
                    console.error('not support audio: ' + e.message)
                }
            }
            currentOrdersActive = newOrdersActive;
        }
    };

    var getUniqValues = function(object){
        var uniqValues = [];
        object.forEach((number, index) => {
            if(!uniqValues.includes(number)){
                uniqValues.push(number);
            }
        });

        return uniqValues;
    };


    var isNewValue = function(currentValues, newValues){
        for(var i = 0; i < newValues.length; i++){
            if(currentValues.indexOf(newValues[i]) === -1){
                return true;
            }
        }
        return false;
    };


    function setFilterElement(element){
        let div_id = ul.find('li.active a').attr('class');
        let active_div = $('#' + div_id);

        let ul_filter = $(active_div).find('ul.orders_filter');
        let input_checked = $(active_div).find('input:checked');

        if($(element).val() == '0'){
            input_checked.not(':first').removeAttr('checked');
        }else if(input_checked.size() > 0){
            $(ul_filter).find('input#filter_all').removeAttr('checked');
        }

    }

    function getFilterElement(){
        let div_id = ul.find('li.active a').attr('class');
        let active_div = $('#' + div_id);

        let status_filter = active_div.find('ul.orders_filter').find('input:gt(0)').filter(':checked');
        let filter = [];

        let filter_str = '';
        if(status_filter.size() > 0){
            status_filter.each(function(){
                filter.push(parseInt($(this).val()));
            });
        }

        if(filter.length === 0){
            return null;
        }
        filter_str = '&filter=' + filter.join(',');
        return filter_str;
    }

    $(document).on('click', 'ul.orders_filter input[type="checkbox"]', function(){
        setFilterElement($(this));
        updateOrderTab();
    });

    function updateOrderTab(){
        var pjaxId = ul.find('li.active a').data('ids');
        var url = ul.find('li.active a').data('ajax');
        var pjax_elem = $('#' + pjaxId + ' .pjax_url');

        if(ul.length === 0 || url === undefined){
            return;
        }

        let url_get = '';
        if(pjax_elem.length > 0){
            url_get = $(pjax_elem).data('pjax_url');
        }

        // основная логика
        let url_all = '';
        if(url.indexOf('?') > -1){
            url_all = url + '&' + url_get;
        }else{
            if(url_get === ''){
                url_all = url;
            }else{
                url_all = url + '?' + url_get;
            }
        }

        let filter = getFilterElement();
        if(filter !== null){
            if(url_all.indexOf('?') > -1){
                url_all += filter;
            }else{
                url_all += '? ' + filter;
            }
        }

        $.pjax.reload({
            container: '#' + pjaxId,
            url: url_all
        });
    }


    setInterval(function(){
        updateOrderTab();
    }, 6000);

});
