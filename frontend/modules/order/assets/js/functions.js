window.looadedGootaxOrderFunctions = true;

/*global orderMap*/
function fillClientInfo(phoneRaw, cityId) {
    var isClient = $('#add-order').find('form').data('is-client') == 1;

    $.get(
        isClient ? 'get-client-info' : '/order/order/get-client-info',
        {phone: phoneRaw, cityId: cityId},
        function (item) {
            //clearClientInfo();

            var name = item.shortName;
            var blackListTitle = $('.client-info').data('black-list-title');

            var banned = (item.banned) ? '<i class="banned-user" title="' + blackListTitle + '"></i>' : '';

            name = (name !== null && name !== '') ? ('<div><a href="' + item.link + '" class="name" target="_blank">' + name + '</a><a class="js-change-client change-client"><i></i></a></div>') : '';

            var phone = (name !== '') ? ('<div class="phone">+' + item.phone + '</div>') : ('<div><a href="' + item.link + '" target="_blank" class="name">+' + item.phone + '</a><a class="js-change-client change-client"><i></i></a></div>');
            var photo = (item.photo !== null) ? item.photo : '/images/no-photo.png';
            var successOrders = '<div class="success-orders">' + item.successOrders + '</div>';
            var failedOrders = '<div class="failed-orders">' + item.failedOrders + '</div>';
            var balance = '<div class="balance">' + item.balance + '</div>';
            var bonus = '<div class="balance">' + item.bonus + '</div>';

            if (item.banned) {
                $('#order-phone').addClass("in_black_list");
            }
            $('#order-client_id').val(item.clientId);
            $('#order-phone').val(item.phone);
            $('#call_to_client').attr('href', 'call:' + item.phone);

            $('#client').html('<div class="line"><div class="photo"><i style="background-image: url(' + photo + ');"></i></div><div class="client-info-content">' + name + phone + banned + successOrders + failedOrders + balance + bonus + '</div></div>');
            $('.js-change-client').show();
            fillPayments(item.payments);
            showHistoryButton();
        }, 'json')
        .always(function () {
            $(document).trigger('checkCanBonusPayment');
        });
}

function showHistoryButton() {
    $('.js-address-history').addClass('active');
}

function hideHistoryButton() {
    $('.js-address-history').removeClass('active');
}

function fillPayments(payments) {
    var showCardPayment = $('#add-order').find('form').data('show-card-payment') == 1;
    var $select = $('#order_payment');
    var emptyText = $select.data('empty');
    var defaultId = $select.data('default-id');
    var defaultName = $select.data('default-name');
    var selector = '<select id="order_payment" class="default_select" name="Order[payment]" data-empty="' + emptyText + '"'
        + ' data-default-id="' + defaultId + '" data-default-name="' + defaultName + '">';
    var companyId;

    if (Array.isArray(payments) && payments.length > 0) {
        for (var i = 0; i < payments.length; i++) {
            if (!showCardPayment && payments[i].type === 'CARD') {
                continue;
            }
            if (payments[i].type == 'CORP_BALANCE') {
                payments[i].type += '_' + payments[i].companyId;
            }
            selector += '<option value="' + payments[i].type + '"';
            if (payments[i].companyId) {
                selector += ' data-company="' + payments[i].companyId + '"';
                selector += ' data-balance="' + payments[i].balance + '"';
                selector += ' data-credit="' + payments[i].credit + '"';
            }
            selector += '>' + payments[i].name.replace(/</g, "&lt;").replace(/>/g, "&gt;") + '</option>';
        }
    } else if (defaultId) {
        selector += '<option value="' + defaultId + '">' + defaultName + '</option>';
    } else {
        selector += '<option>' + emptyText + '</option>';
    }
    selector += '</select>';
    $select.replaceWith(selector);
    $select = $('#order_payment');
    $selected = $select.find('option').eq(0).attr('selected', 'selected');
    $('#order-company_id').val($selected.data('company'));
    updateCorpBalanceInfo($selected.data('balance'), $selected.data('credit'));

    $select.trigger('update');

    getTariffs();
}

function updateCorpBalanceInfo(balance, credit) {
    var $jsCorpBalance = $('.js-corp-balance');
    var $jsCorpCredit = $('.js-corp-credit');

    if (balance) {
        $jsCorpBalance.text($jsCorpBalance.data('label') + ': ' + balance).show();
    } else {
        $jsCorpBalance.text('').hide();
    }

    if (credit) {
        $jsCorpCredit.text($jsCorpCredit.data('label') + ': ' + credit).show();
    } else {
        $jsCorpCredit.text('').hide();
    }
}

function changeTariffDescription(tariffId) {
    var description = $('option[value="' + tariffId + '"]').data('description');
    $('.js-order-tariff-description').html(description);
}

function autoChangeTariffDescription() {
    var tariffId = $('#order-tariff_id').val();
    changeTariffDescription(tariffId);
}

function clearTariffs() {
    var _this = $('#order-tariff_id');
    _this.empty();
    _this.attr('data-placeholder', _this.data('empty'));
    _this.trigger('update');
    autoChangeTariffDescription();
}

function getTariffs() {
    var selected_tafiff = $('#order-tariff_id').val();
    var city_id = $('#order-city_id').val();
    var position_id = $('#order-position_id').val();
    var clientId = $('#order-client_id').val();
    var payment = $('#order_payment').find(':selected').val();
    var form = $('#order-tariff_id').closest('form');

    $.post($("#add-order").data('module') + 'order/get-tariff-list',
        {
            city_id: city_id,
            position_id: position_id,
            client_id: clientId,
            payment: payment
        }
    )
        .done(function (data) {
            var $tariffList = $('#order-tariff_id'),
                firstId, firstValue, $option;

            clearTariffs();

            if (data.length) {
                for (var i = 0; i < data.length; i++) {
                    if (!firstId) {
                        firstId = data[i]['tariff_id'];
                        firstValue = data[i]['name'];
                    }

                    if (data[i]['tariff_id'] == selected_tafiff) {
                        firstId = data[i]['tariff_id'];
                        firstValue = data[i]['name'];
                    }


                    $option = $('<option/>').val(data[i]['tariff_id']).text(data[i]['name']).attr('data-description', data[i]['description']);
                    $tariffList.append($option);
                }
            } else {
                $tariffList.append($('<option/>').val('').text($tariffList.data('empty')));
            }

            $tariffList.attr('data-placeholder', firstValue ? firstValue : '');
            $tariffList.val(firstId);
            $tariffList.trigger('update').trigger('change');
            changeTariffDescription(firstId);

            var orderId = $('#order-id').data('order_id');
            refreshFormOrder(city_id, position_id, orderId);

            getAddOptions(form);

        })
        .fail(function (data) {
            console.log('fail', data);
        })
        .always(function () {
            $(document).trigger('checkCanBonusPayment');
        });
}


function fillDriverInfo(workerId, carId, cityId, positionId) {
    var isClient = $('#add-order').find('form').data('is-client') == 1;

    $.get(
        isClient ? 'get-worker-info' : '/order/order/get-worker-info',
        {
            workerId: workerId,
            carId: carId,
            cityId: cityId,
            positionId: positionId
        },
        function (item) {
            clearDriverInfo();

            var name = [item.callsign, item.shortName].filter(function (value) {
                return value;
            }).join(',  ');

            name = (name !== null) ? (name) : '';

            var photo = (item.photo !== null) ? item.photo : '/images/no-photo.png';
            var car = [];
            if (item.car != null) {
                if (item.car.name) car.push(item.car.name);
                if (item.car.gosNumber) car.push(item.car.gosNumber);
                if (item.car.color) car.push(item.car.color);
            }
            var carStr = '<div class="car">' + car.join(', ') + '</div>';
            var rating = (item.rating != null) ? '<div class="rating">' + item.rating + '</div>' : '';
            var balance = '<div class="balance">' + item.balance + '</div>';

            var distStr = '';

            if (item.lat !== null && item.lon !== null) {
                var dist = driverDistance(item.lat, item.lon);
                distStr = '<div class="dist">' + dist + '</div>';
            }

            $('#order-worker_id').val(workerId);
            $('#order-car_id').val(carId);
            $('#driver').html('<div class="line"><div class="photo"><i style="background-image: url(' + photo + ');"></i></div><div class="client-info-content">' + distStr + '<span class="name">' + name + '</span><a class="js-change-driver change-client"><i></i></a>' + carStr + rating + balance + '</div></div>');
            $('.js-change-driver').show();
            $('.js-deny-refuse-order').show();
        }, 'json');
}


function clearClientInfo() {
    $('.js-change-client').hide();
    $('#client').html('');
    $('#order-client_id, #order-phone').val('');
    $('#client .client_more_info').html('');
    $('#order-bonus_payment').prop('checked', false);
    $('#order-phone').removeClass("in_black_list");
    $('#order-company_id').val('');
    $('#call_to_client').attr('href', 'call:');
    fillPayments(null);
    hideHistoryButton();
}

function clearDriverInfo() {
    $('.js-change-driver, .js-deny-refuse-order').hide();
    $('#driver').html('');
    $('#order-worker_id').val('');
    $('#order-car_id').val('');
    $('#order-deny_refuse_order').prop('checked', false);
    $('#driver .client_more_info').html('');
}


function timer_init() {
    _timerId = setInterval(function () {

        var timer = new Timer();

        timer.colon($('.time_colon')); //Двоеточие

        var timer_obj = $('form .timer');
        timer.run(timer_obj.find('.minutes'), timer_obj.find('.seconds'), timer_obj.data('type'));

    }, 1000);
}

function driverDistance(lat2, lon2) {
    var R = 6372795;
    var pi = 3.141592;

    var lat1 = Number($('input[name="Order[address][A][lat]"]').val()) * pi / 180;
    var lon1 = Number($('input[name="Order[address][A][lon]"]').val()) * pi / 180;

    lat2 = Number(lat2) * pi / 180;
    lon2 = Number(lon2) * pi / 180;

    var cl1 = Math.cos(lat1);
    var cl2 = Math.cos(lat2);
    var sl1 = Math.sin(lat1);
    var sl2 = Math.sin(lat2);
    var delta = lon2 - lon1;
    var cdelta = Math.cos(delta);
    var sdelta = Math.sin(delta);

    var y = Math.sqrt(Math.pow(cl2 * sdelta, 2) + Math.pow(cl1 * sl2 - sl1 * cl2 * cdelta, 2));
    var x = sl1 * sl2 + cl1 * cl2 * cdelta;

    var ad = Math.atan2(y, x);
    var dist = ad * R;

    if (dist > 1000) {
        dist = dist / 1000;
        dist = Math.round(dist * 100) / 100;
        var km = $('[name=autocomplete-drivers]').attr('data-km');
        dist += ' ' + km;
    } else {
        dist = Math.round(dist);
        var m = $('[name=autocomplete-drivers]').attr('data-m');
        dist += ' ' + m;
    }

    return dist;
}

function init_pac() {
    select_init();
    //phone_mask_init();
    timer_init();
    window.OrderDatepicker && OrderDatepicker.init();

    var $form = $('#order_create');
    var $tariffsForm = $('#order-tariff_id').closest('form');
    var $formUpdate = $('#order_update');
    if ($form.length || $formUpdate.length) {
        if ($form.data('is-copy-order')) {
            getOrderPriceAnalyz($form);
            getAddOptions($tariffsForm);

            var city_id = $('#order-city_id').val();
            var position_id = $('#order-position_id').val();
            refreshFormOrder(city_id, position_id);
        }

        if (!$form.length) {
            getAddOptions($formUpdate);

            var orderId = $('#order-id').data('order_id');
            refreshFormExistOrder(orderId);
        } else {
            if ($('#order-client_id').val() === '') fillPayments(null);
        }
    }

    //textarea autosize

    var $comment = $('#order-comment');
    if ($comment.length) {
        $comment.eq(0).attr('style', 'height:' + ($comment.get(0).scrollHeight) + 'px;overflow-y:hidden;');
    }

    $('#order-comment').on('keyup', function () {
        this.style.height = 'auto';
        this.style.height = (this.scrollHeight) + 'px';
    });

    if ($('#order-client_id').val()) {
        $('.js-address-history').addClass('active');
    }

    if ($('.js-dist').length !== 0) {
        var dist = driverDistance($('.js-dist').attr('data-lat'), $('.js-dist').attr('data-lon'));
        $('.js-dist').removeClass('js-dist').addClass('dist').html(dist);
    }
}

$(document).on('update-order-points', function () {
    var count = 0;
    $('#sortable>li').each(function () {
        var key = String.fromCharCode('A'.charCodeAt(0) + count);

        $(this).attr('data-point-id', count)
            .find('.number i').html(key);
        $(this).find('[data-name]').each(function () {
            var name = $(this).attr('data-name').replace('{point}', key);
            $(this).attr('name', name);
        });
        count++;
    });
    $('#sortable').attr('data-points', count);
    setTimeout(function () {
        getOrderPriceAnalyz($('#sortable').parents('form'));
    }, 10);

});

function showOrder(url) {
    $.colorbox({
        href: url,
        overlayClose: false,
        width: "820px",
        speed: 0,
        transition: 'none',
        onComplete: function () {
            init_pac();
            $(document).trigger('update-address-points');

            $.colorbox.resize();

            window.orderMap && orderMap.initialize();
        },
        onClosed: function () {
            window.orderMap && orderMap.finalize();

            clearTimeout(_timerId);
            clearInterval(_updateWorkerLocation);
        }
    });
}

function initSortable() {
    $("#sortable").sortable({
        handle: '.toggle-actions',
        scroll: true,
        revert: false,
        opacity: 0.5,
        containment: "parent",
        tolerance: "pointer",
        update: function () {
            $(document).trigger('update-order-points');
        }
    });
};

function colorbox_init() {
    registerOrderOpenInModalEvent('.ot_open');

    $(function () {
        $('.add_order').gootaxModal({
            url: $('.add_order').attr('href'),
            onComplete: function () {
                init_pac();
                //$(document).trigger('update-address-points');
                //$(document).trigger('update-order-points');

                window.orderMap && orderMap.initialize();

                initSortable();

                var city_id = $('#order-city_id').val();
                var position_id = $('#order-position_id').val();
                refreshFormOrder(city_id, position_id);
            },
            onClosed: function () {
                window.orderMap && orderMap.finalize();

                clearTimeout(_timerId);
                clearInterval(_updateWorkerLocation);
            }
        });

        $('body').append('<div class="edit-order-fake"></div>');
        $('.edit-order-fake').gootaxModal({
            onComplete: function () {
                init_pac();
                //$(document).trigger('update-address-points');
                //$(document).trigger('update-order-points');

                window.orderMap && orderMap.initialize();

                $(function () {
                    initSortable();
                    if ($('#order-is_fix').is(':checked')) {
                        $('#order-predv_price').prop('disabled', false);
                    }
                });
            },
            onClosed: function () {
                window.orderMap && orderMap.finalize();

                clearTimeout(_timerId);
                clearInterval(_updateWorkerLocation);
            }
        });

        $('body').on('click', '.ot_edit', function (e) {
            e.preventDefault();
            $('.edit-order-fake').gootaxModal('settings', {url: $(this).attr('href')}).gootaxModal('open');
        })
    })
}

function getParkings(url, data, select) {
    var hash = Math.round(Math.random() * 100000);

    $.ajax({
        type: 'GET',
        url: url,
        data: data,
        dataType: "json",
        beforeSend: function () {
            _requests[hash] = true;
        },
        success: function (json) {
            var first_option = select.find('option:first');
            var selected = '';
            var $selectItem;

            first_option.removeAttr('selected');
            select.find('option:gt(0)').remove();

            if (json.parking) {
                for (var i = 0; i < json.parking.length; i++) {
                    selected = json.parking[i].parking_id == json.inside;
                    $selectItem = $('<option/>')
                        .data('parking_id', json.parking[i].parking_id)
                        .val(json.parking[i].parking_id)
                        .text(json.parking[i].name);

                    select.append($selectItem);
                    if (selected) {
                        $selectItem.attr('selected', 'selected');
                        select.closest('.fl_dist').find('.js-parking').val(json.parking[i].name);
                        select.closest('.fl_dist').find('.js-parking_id').val(json.parking[i].parking_id);
                    }
                }
            }

            if (json.error !== null || json.inside === null) {
                first_option.attr('selected', 'selected');
            }

            select.trigger('update');
            getOrderPriceAnalyz(select.parents('form'));
        }
    })
        .always(function () {
            delete _requests[hash];
        });
}

function getAddOptions(form) {
    $.ajax({
        type: 'POST',
        url: $("#add-order").data('module') + 'order/add-options',
        data: form.serialize(),
        dataType: "json",
        success: function (json) {
            var ul = $('ul.comfort_list'),
                $oldCheckboxes = ul.clone();
            ul.empty();
            ul.hide();
            var count = 0;
            var disabled = ul.data('can-update') != '1' ? 'disabled="disabled"' : '';
            var labelShowAll = ul.data('label-show-all');
            for (var i in json) {
                if (count === 3) {
                    ul.append('<a class="toggle-hidden-items">' + labelShowAll + '</a>');
                    ul.append('<ul style="display: none;" class="hidden-items"></ul>');
                    ul = $(ul).find('.hidden-items');
                }

                var checked = json[i]['isChecked'] ? 'checked' : '';

                ul.append('<li style="display: inline-block;"><label><input ' + checked + ' name="Order[additional_option][]" type="checkbox" value="' + i + '" ' + disabled + '> ' + json[i]['option'] + '</label></li>');
                count++;
            }
            if ($oldCheckboxes) {

                if ($($oldCheckboxes).find('.hidden-items').css('display') == 'block') {
                    $('ul.comfort_list').find('.hidden-items').show();
                    $('ul.comfort_list').find('.toggle-hidden-items').remove();
                }

                $($oldCheckboxes).find('[type="checkbox"]:checked').each(function () {
                    $('ul.comfort_list').find('[type=checkbox][value=' + $(this).val() + ']').prop('checked', true);
                });
            }

            $('ul.comfort_list').show();
        }
    });
}

function setSubmitStatus(st) {
    var submit = $('#save-create-form');
    // Проверка на заполнение форм
    if (!check_streets_filled())
        st = '';
    switch (st) {
        case 'loading':
            submit.addClass('loading_button');
            break;
        case '':
            submit.removeClass('loading_button');
    }
}

/**
 * Refresh parkings of address
 *
 * @param $address
 * @returns Promise
 */
function refreshParkings($address) {
    var parkingModule = '/parking/parking/get-parking-by-coords';

    if ($("#add-order").data('module') == '/client/cabinet/') parkingModule = 'get-parking-by-coords';
    return $.get(parkingModule, {
        city_id: $('#order-city_id').val(),
        lat: $address.find('.lat').val(),
        lon: $address.find('.lon').val()
    })
        .done(function (data) {
            var json = JSON.parse(data);

            $parkingList = $address.find('.parkings');
            $parkingList.find('option:gt(0)').remove();

            for (i = 0, l = json.parking.length; i < l; i++) {
                $parkingItem = $('<option/>')
                    .data('parking_id', json.parking[i].parking_id)
                    .val(json.parking[i].parking_id)
                    .text(json.parking[i].name);
                $parkingList.append($parkingItem);

                if (json.parking[i].parking_id == json.inside) {
                    $parkingItem.attr('selected', 'selected');
                    $address.find('.js-parking').val(json.parking[i].name);
                    $address.find('.js-parking_id').val(json.parking[i].parking_id);
                }
            }

            if (json.error !== null || json.inside === null) {
                $parkingList.find('option').eq(0).attr('selected', 'selected');
            }

            $address.find('.parkings option').removeAttr('selected');
            $address.find('.parkings option[value="' + json.inside + '"]').attr('selected', true);
            $address.find('.parkings').trigger('update');
        });
}

lastRequestTime = 0;
_requests = {};

function getOrderPriceAnalyz(form) {
    var parking_error_block = $('section.submit_form p.parking_error');
    parking_error_block.hide();

    var hash = Math.round(Math.random() * 100000);

    lastRequestTime = Date.now();
    $.ajax({
        type: "POST",
        url: $("#add-order").data('module') + 'order/route-analyzer',
        data: form.serialize(),
        dataType: "json",
        headers: {
            'request-time': lastRequestTime,
        },
        beforeSend: function () {
            var analyzerBlock = $('#routeAnalyzer');

            analyzerBlock.find('#order-predv_price').val(null);
            analyzerBlock.find('.distance').css('color', 'red').text('--');
            analyzerBlock.find('.time').css('color', 'red').text('--');
            analyzerBlock.find('.costNoDiscount').css('color', 'red').text('--');

            _requests[hash] = true;
            $(form).find('.submit_form .js-save-order').addClass('loading_button');

            $(document).trigger('disableMap');
        },
        complete: function () {
            $(document).trigger('enableMap');
            $(document).trigger('routeAnalyzeComplete');
        },
        success: function (data, status, request) {
            var analyzerBlock = $('#routeAnalyzer'),
                routePoints = [];

            if (lastRequestTime != request.getResponseHeader('request-time')) {
                return;
            }

            if (data != '') {
                if (data.error != undefined) {
                    if (data.error == 100) {
                        parking_error_block.show();
                        $.colorbox.resize();
                    }

                    return;
                }

                analyzerBlock.find('.distance').text(data.summaryDistance).css('color', '');
                $('#order-predv_distance').val(data.summaryDistance);

                var summaryTime = data.summaryTime;
                analyzerBlock.find('.time').text(summaryTime).css('color', '');
                $('#order-predv_time').val(summaryTime);

                var summaryCostNoDiscount = data.summaryCostNoDiscount;
                var summaryCost = data.summaryCost;

                analyzerBlock.find('#order-predv_price').val(summaryCost);

                analyzerBlock.find('div').show();

                if (summaryCost !== summaryCostNoDiscount) {
                    analyzerBlock.find('.costNoDiscount').html('<s>' + summaryCostNoDiscount + '</s>').css('color', '');
                    analyzerBlock.find('#input-predv_price_no_discount').val(summaryCostNoDiscount);
                    analyzerBlock.find('.route-info_no_discount').show();
                } else {
                    analyzerBlock.find('.route-info_no_discount').hide();
                    analyzerBlock.find('#input-predv_price_no_discount').val(null);
                }

                routePoints = data.routeLine;
            }
            else {
                analyzerBlock.find('.distance').css('color', 'red').text('--');
                analyzerBlock.find('.time').css('color', 'red').text('--');
                analyzerBlock.find('div').show();

                analyzerBlock.find('.route-info_no_discount').hide();
            }

            $(document).trigger('updateMap', [routePoints]);
            setSubmitStatus('');
        }
    })
        .always(function () {
            delete _requests[hash];
            if (!Object.keys(_requests).length) {
                $(form).find('.submit_form .js-save-order').removeClass('loading_button');
            }
        });
}

function sort_order_table(a) {
    var column = a.data('column');
    var sort_span = a.find('span');
    var sort_class = sort_span.attr('class');
    var table = a.parents('table');
    var all_sort_span = table.find('th a span');
    var tr = table.find('tr').not(':first');
    var sortable = [];
    var dir = null;

    all_sort_span.attr('class', '');

    if (sort_class === 'pt_up') {
        dir = -1;
        sort_class = 'pt_down';
    }
    else {
        dir = 1;
        sort_class = 'pt_up';
    }

    tr.each(function () {
        var sort_data = $(this).find('td:eq(' + column + ')').data('sort');
        sortable.push([$(this).attr('id'), sort_data]);
    });

    sortable.sort(function (a, b) {
        if (a[1] < b[1])
            return (-1) * dir;
        else if (a[1] > b[1])
            return dir;
        else
            return 0;
    });

    for (var i = 0; i < sortable.length; i++) {
        $('#' + sortable[i][0]).appendTo(table);
    }
    sort_span.attr('class', sort_class);
}

function call_init() {
    $('div.orders').on('click', '.pt_call', function () {

        var call_type = $(this).data('type');
        var caller_phone = $(this).data('call');

        if (caller_phone != '') {
            try {
                if (call_type == 'client') {
                    window.qt_handler.setDriverPhone(caller_phone);
                }
                else if (call_type == 'worker') {
                    window.qt_handler.setClientPhone(caller_phone);
                }
            } catch (e) {
                console.log('Ошибка');
            }
        }
    });
}

//Проверка заполнения двух улиц для запуска анализатора маршрута
function check_streets_filled() {
    var streets = $('.fl_street input');
    var filled = true;

    streets.each(function () {
        if ($(this).val() == '') {
            filled = false;
        }
    });

    return filled;
}