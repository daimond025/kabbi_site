$(function () {
    //Errors in order window
    function showError(text) {
        $('#order-error').text(text).show();
        $('.order-window__errors').show();
        $('.gootax-modal-wrapper').animate({scrollTop: $('#order-error').offset().top}, 100);
    }

    function clearErrors() {
        $('#order-error').text('').hide();
        $('.order-window__errors').hide();
    }

    /**
     * Getting update result
     * @param requestId
     * @param retryCount
     */
    function getUpdateResult(requestId, retryCount) {
        var $button = $('.submit_form .js-save-order'),
            orderNumber = $('#order-order_number').val();

        retryCount--;
        if (retryCount <= 0) {
            $button.removeClass('loading_button');
            redirectToUrl();
            return;
        }

        $button.addClass('loading_button');
        $.get('/order/order/get-update-event-result', {
            orderNumber: orderNumber,
            requestId: requestId
        })
            .done(function (result) {
                if (result.action === 'ACTION_WAIT') {
                    setTimeout(function () {
                        getUpdateResult(result.message, retryCount);
                    }, 1000);
                } else {
                    processResult(result);
                    $button.removeClass('loading_button');
                }
            })
            .fail(function (data) {
                $button.removeClass('loading_button');
                console.log('fail', data);
            })
    }

    /**
     * Redirecting to index page
     */
    function redirectToIndexPage() {
        var cur_page = location.href;
        var controller = cur_page.search('order/operator/') !== -1 ? 'operator' : 'order';
        location.href = $("#add-order").data('module') + controller + '/index' + location.search;
    }

    /**
     * Redirecting to url
     * @param url
     */
    function redirectToUrl(url) {
        var $modal = $('.edit-order-fake'),
            loadedGootaxOrderFunctions = !!window.looadedGootaxOrderFunctions,
            module = $("#add-order").data('module'),
            controller = location.href.indexOf('order/operator/') !== -1 ? 'operator' : 'order';

        url = url ? module + controller + url : null;
        if ($modal.length && loadedGootaxOrderFunctions) {
            if (url) {
                $modal.gootaxModal('settings', {url: url});
            }
            $modal.gootaxModal('refresh');
        } else {
            if (url) {
                location.href = url;
            } else {
                location.reload();
            }
        }
    }

    /**
     * Result processing
     * @param result
     */
    function processResult(result) {
        var RETRY_COUNT = 10;

        switch (result.action) {
            case 'ACTION_ERROR':
                showError(result.message);
                $('.submit_form .js-save-order').removeClass('loading_button');
                break;
            case 'ACTION_WAIT':
                getUpdateResult(result.message, RETRY_COUNT);
                $('.submit_form .js-save-order').removeClass('loading_button');
                break;
            case 'ACTION_CLOSE':
                redirectToIndexPage();
                break;
            case 'ACTION_REDIRECT':
                redirectToUrl(result.url);
                break;
        }
    }

//Сохранение заказа

    $('body').on('click', '.submit_form .js-save-order', function (e) {
        var form = $(this).parents('form');
        var isViewForm = form.attr('id') == 'order_view';

        e.preventDefault();
        if ($(this).hasClass('loading_button') || $(this).parents('.submit_form').find('p.parking_error').is(':visible')) {
            return false;
        }

        var error = false;
        form.find('.input_error').removeClass('input_error');
        form.find('select.not_empty').parent().prev('b').css({color: 'black'});
        clearErrors();

        if (!isViewForm) {
            form.find('.js-order-point .not_empty, #order-phone').each(function () {
                if ($(this).val() == '' || $(this).val() === null) {
                    if ($(this).hasClass('default_select')) {
                        $(this).parent().prev('b').css({color: 'red'});
                    } else if ($(this).data('error')) {
                        var $point = $(this).parents('.js-order-point'),
                            pointId = $point.data('point-id');

                        if (pointId == 0 || $point.find('[data-type="street"]').val()) {
                            var text = $(this).data('error').replace('{point}',
                                String.fromCharCode('A'.charCodeAt(0) + pointId));
                            showError(text);
                        } else {
                            return;
                        }
                    } else {
                        $(this).addClass('input_error');
                    }

                    error = true;
                }
            });

            var $phone = form.find('.mask_phone');
            if (!$phone.attr('is-valid')) {
                $phone.addClass('input_error');
                error = true;
            }
            console.log(form.find('.mask_phone').val());

            if ($('#order-phone').hasClass('input_error')) {
                $('[name="autocomplete-clients"]').addClass('input_error');
                $('.empty-client').show();
            } else {
                $('[name="autocomplete-clients"]').removeClass('input_error');
                $('.empty-client').hide();
            }

            if (error) {
                $('.empty-inputs, .order-window__errors').show();
                return false;
            }

            $('.empty-inputs, .order-window__errors').hide();
        }


        console.log(form.serialize());
        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize(),
            dataType: 'json',
            beforeSend: function () {
                $('.submit_form .js-save-order').addClass('loading_button');
            },
            success: function (result) {
                processResult(result);
            },
            error: function () {
                $('.submit_form .js-save-order').removeClass('loading_button');
            }
        });


    });

});