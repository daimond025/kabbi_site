(function () {
    var app = {
        init: function () {
            this.enableNeedField();
        },

        enableNeedField: function () {
            if ($('a.t01').length == 0) {
                $('a.t02').trigger('click');
            }
        }

    };

    $(document).ready(function () {
        app.init();
    });
})();