/*global L*/
var controller = $('#map').data('controller');

var coordinatesLoader = {

    routePoints: [],
    routePolyline: null,
    map: null,

    displayError: function (error) {
        $('#b03').html($('<p/>').text(error));
        $.colorbox.resize();
    },

    getAddressIcon: function (key) {
        return L.divIcon({
            iconAnchor: [12, 28],
            html: key,
            className: 'map_icon_point'
        });
    },

    getStatusIcon: function (key) {
        var iconMap = {
            'GET_DRIVER': 'map_icon_point_1',
            'WAITING': 'map_icon_point_2',
            'EXECUTING': 'map_icon_point_3',
            'COMPLETED': 'map_icon_point_4',
        };

        return !iconMap[key]
            ? null : L.divIcon({
                iconAnchor: [8, 8],
                className: iconMap[key]
            });
    },

    getAddressString: function (obj) {
        return [obj.city, obj.street, obj.house, obj.housing, obj.porch, obj.apt]
            .filter(function (value) {
                return value;
            }).join(', ');
    },

    setCenter: function (map, points) {
        var bounds;

        if (!map || !Array.isArray(points)) {
            return;
        }

        bounds = new L.latLngBounds(points);
        map.fitBounds(bounds, {maxZoom: 13});
    },

    getAddressMarkers: function (L, addressJson) {
        var key, marker, point, markers, icon, markerParams, template;

        markers = [];
        for (key in addressJson) {
            if (addressJson.hasOwnProperty(key)) {
                if (addressJson[key].lat && addressJson[key].lon) {
                    point = L.latLng(addressJson[key].lat, addressJson[key].lon);
                } else {
                    continue;
                }

                icon = this.getAddressIcon(key);
                markerParams = icon ? {icon: icon} : {};

                template =
                    '<div class="win_map_content">\
                        <div class="wmc_win">\
                            <div class="wmcw_content">\
                                <span>' + this.getAddressString(addressJson[key]) + '</span>\
                            </div>\
                        </div>\
                    </div>';
                marker = L.marker(point, markerParams).bindPopup(template);
                markers.push(marker);
            }
        }
        return markers;
    },

    addMarkersToMap: function (map, markers) {
        var i, l;
        for (i = 0, l = markers.length; i < l; i++) {
            markers[i].addTo(map);
        }
    },

    getMarkerPoints: function (markers) {
        var i, l, points = [];
        for (i = 0, l = markers.length; i < l; i++) {
            points.push(markers[i].getLatLng());
        }
        return points;
    },

    getDriverSpeedTemplate: function (speed) {
        var currentSpeed = parseFloat(speed) || 0,
            speedCaption = 'Скорость ' + Math.round(currentSpeed) + ' км/ч';

        return '<div class="win_map_content">\
                    <div class="wmc_win">\
                        <div class="wmcw_content">\
                            <span>' + speedCaption + '</span>\
                        </div>\
                    </div>\
                </div>';
    },

    getBusyDriverIcon: function () {
        return L.divIcon({
            iconAnchor: [18, 47],
            className: 'busy_car_icon'
        });
    },

    registerUpdateDriverInterval: function (L, order_id, marker) {
        'use strict';
        var _this = this;

        var _updateWorkerLocation = setInterval(function () {
            $.get(
                controller + 'order/get-worker-coords-by-order-id',
                {order_id: order_id},
                function (json) {
                    var driverCoords;

                    if (json instanceof Object && json.lat && json.lon) {
                        driverCoords = L.latLng([json.lat, json.lon]);
                        marker.setLatLng(driverCoords).setPopupContent(_this.getDriverSpeedTemplate(json.speed));
                        marker.setIconAngle(json.iconAngle);
                    }
                },
                'json'
            );
        }, 3000);
    },

    loadTrackingCoords: function (L) {
        'use strict';
        var _this = this;
        $.get(
            controller + 'order/get-tracking',
            {order_id: $('#map').data('order')},
            function (json) {
                var icon, markerParams;

                if (json.error !== undefined) {
                    _this.displayError(json.error);
                    return;
                }

                var points = [];

                var markers = [];

                for (var i = 0; i < json.order_route.length; i++) {
                    var latlng = L.latLng([json.order_route[i].lat, json.order_route[i].lon]);
                    points.push(latlng);
                    if (typeof markers['WAITING'] == 'undefined' && json.order_route[i].status_id == '26') {
                        markers['WAITING'] = latlng;
                    }
                    else if (typeof markers['EXECUTING'] == 'undefined' && json.order_route[i].status_id == '36') {
                        markers['EXECUTING'] = latlng;
                    }
                }
                if (!points.length) {
                    return;
                }

                var map = init_map(points[0], 13, 'map');

                L.polyline(points, {color: 'red'}).addTo(map);

                var zIndexOffset = 1;

                icon = _this.getStatusIcon('GET_DRIVER');
                markerParams = icon ? {icon: icon, zIndexOffset: zIndexOffset++} : {};
                L.marker(points[0], markerParams).addTo(map);

                for (var key in markers) {
                    icon = _this.getStatusIcon(key);
                    markerParams = icon ? {icon: icon, zIndexOffset: zIndexOffset++} : {};
                    L.marker(markers[key], markerParams).addTo(map);
                }

                icon = _this.getStatusIcon('COMPLETED');
                markerParams = icon ? {icon: icon, zIndexOffset: zIndexOffset++} : {};
                L.marker(points[points.length - 1], markerParams).addTo(map);

//                icon = _this.getAddressIcon('A');
//                markerParams = icon ? { icon: icon, zIndexOffset: zIndexOffset++ } : {};
//                L.marker(points[0], markerParams).addTo(map);
//
//                icon = _this.getAddressIcon('B');
//                markerParams = icon ? { icon: icon, zIndexOffset: zIndexOffset++ } : {};
//                L.marker(points[points.length-1], markerParams).addTo(map);
                $.get(
                    controller + 'order/get-order-address',
                    {order_id: $('#map').data('order')},
                    function (json) {
                        var markers;

                        if (!json) {
                            return;
                        } else if (json.error) {
                            _this.displayError(json.error);
                            return;
                        }

                        markers = _this.getAddressMarkers(L, json);
                        if (!markers || !markers.length) {
                            return;
                        }

                        for (var key in json) {
                            var latlng = L.latLng([json[key].lat, json[key].lon]);
                            icon = _this.getAddressIcon(key);
                            markerParams = icon ? {icon: icon, zIndexOffset: zIndexOffset++} : {};
                            L.marker(latlng, markerParams).addTo(map);
                        }
                    },
                    'json'
                );

                _this.setCenter(map, points);
            },
            'json'
        );
    },

    loadDriverCoords: function (L) {
        'use strict';
        var _this = this;

        $.when(
            $.ajax({
                url: controller + 'order/get-worker-coords-by-order-id',
                data: {order_id: $('#map').data('order')},
                dataType: 'json'
            }),
            $.ajax({
                url: controller + 'order/get-order-address',
                data: {order_id: $('#map').data('order')},
                dataType: 'json'
            })
        )
            .done(function (driverResponse, addressResponse) {
                var marker, markers, driverCoords,
                    driverJson = driverResponse[0],
                    addressJson = addressResponse[0],
                    points = [];

                if (driverJson && !driverJson.error && driverJson.lat && driverJson.lon) {
                    driverCoords = L.latLng([driverJson.lat, driverJson.lon]);
                    marker = L.marker(driverCoords, {
                        icon: _this.getBusyDriverIcon(),
                        iconAngle: driverJson.iconAngle
                    }).bindPopup(_this.getDriverSpeedTemplate(driverJson.speed));
                    _this.registerUpdateDriverInterval(L, $('#map').data('order'), marker);
                }

                if (addressJson && !addressJson.error) {
                    markers = _this.getAddressMarkers(L, addressJson);
                }

                if (!marker && !markers) {
                    if (driverJson.error) {
                        _this.displayError(driverJson.error);
                    }
                    return;
                }

                _this.map = init_map(driverCoords, 16, 'map');

                if (marker) {
                    marker.addTo(_this.map);
                    points.push(driverCoords);
                }

                if (markers) {
                    _this.addMarkersToMap(_this.map, markers);
                    points.push(_this.getMarkerPoints(markers));
                }

                _this.setCenter(_this.map, points);
            })
            .fail(function (data) {
                console.log('fail', data);
            });
    },

    loadAddressCoords: function (L) {
        'use strict';
        var _this = this;
        $.get(
            controller + 'order/get-order-address',
            {order_id: $('#map').data('order')},
            function (json) {
                var markers;

                if (!json) {
                    return;
                } else if (json.error) {
                    _this.displayError(json.error);
                    return;
                }

                markers = _this.getAddressMarkers(L, json);
                if (!markers || !markers.length) {
                    return;
                }

                _this.map = init_map(markers[0].getLatLng(), 13, 'map');
                _this.addMarkersToMap(_this.map, markers);

                _this.setCenter(_this.map, _this.getMarkerPoints(markers));
            },
            'json'
        );
    },

    drawRoute: function (routePoints) {
        var latLngs = [], i, l;
        if (this.routePolyline) {
            this.map.removeLayer(this.routePolyline);
            this.routePolyline = null;
        }

        if (routePoints && Array.isArray(routePoints)) {
            for (i = 0, l = routePoints.length; i < l; i++) {
                latLngs.push(new L.LatLng(routePoints[i][0], routePoints[i][1]));
            }

            this.routePolyline =
                L.polyline(latLngs, {color: "blue"}).addTo(this.map);
        }
    },

    checkPointsLength: function () {
      return Number($('#add-order').attr('data-points'));
    },

    loadAnalyzedRoute: function () {
        if (this.checkPointsLength() < 2)
            return;

        var $form = $('#add-order').find('form.order-form'),
            hash = Math.round(Math.random() * 100000);

        var _this = this;

        $.ajax({
            type: "POST",
            url: $("#add-order").data('module') + 'order/route-analyzer',
            data: $form.serialize(),
            dataType: "json",
            beforeSend: function () {
                _requests[hash] = true;
            }
        })
            .done(function (data) {
                if (data instanceof Object) {
                    _this.drawRoute(data.routeLine);
                }
            })
            .always(function () {
                delete _requests[hash];
            });
    }
};

function init_map_data(map_data_type) {
    if (coordinatesLoader) {
        switch (map_data_type) {
            case 'route':
                coordinatesLoader.loadTrackingCoords(L);
                $('#map_legend').show();
                break;
            case 'worker':
                coordinatesLoader.loadDriverCoords(L);
                coordinatesLoader.loadAnalyzedRoute(L);
                break;
            case 'address':
                coordinatesLoader.loadAddressCoords(L);
                coordinatesLoader.loadAnalyzedRoute(L);
                break;
        }
    }

    setTimeout(function () {
        $('.js-empty-map').show();
    }, 1000);
}