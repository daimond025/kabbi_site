/*global jQuery, L*/
var generalMap = (function ($) {
    'use strict';

    function MarkerCollection() {
        this.markers = null;
        this.deletedMarkers = null;
        this.action = null;
        this.iconClass = null;
        this.filter = null;
        this.active = false;
    }

    MarkerCollection.prototype.addMarkersToMap = function (map) {
        var key;

        if (!map || !this.markers) {
            return;
        }

        this.removeDeletedMarkersFromMap(map);

        for (key in this.markers) {
            if (this.markers.hasOwnProperty(key)) {
                map.addLayer(this.markers[key]);
            }
        }
    };
    MarkerCollection.prototype.removeDeletedMarkersFromMap = function (map) {
        if (!map || !Array.isArray(this.deletedMarkers)) {
            return;
        }

        while (this.deletedMarkers.length) {
            map.removeLayer(this.deletedMarkers.pop());
        }
    };
    MarkerCollection.prototype.removeMarkersFromMap = function (map) {
        var key;

        if (!map || !this.markers) {
            return;
        }

        for (key in this.markers) {
            if (this.markers.hasOwnProperty(key)) {
                map.removeLayer(this.markers[key]);
            }
        }
    };
    MarkerCollection.prototype.addMarkers = function (markers) {
        var _this = this, icon, latLng, iconAngle, key, markerKeys = [], workerId = '', iconSize = L.point(24, 24),
            iconAnchor = L.point(12, 12);

        if (!this.markers) {
            this.markers = [];
        }
        markers.forEach(function (item) {

            switch (_this.action) {
                case 'get-orders-free': _this.iconClass = 'order-map-icon--order';
                    iconSize = L.point(16, 16);
                    iconAnchor = L.point(8, 8);
                    break;
                case 'get-orders-pre': _this.iconClass = 'order-map-icon--preorder';
                    iconSize = L.point(16, 16);
                    iconAnchor = L.point(8, 8);
                    break;
                case 'get-free-cars':
                    switch (item.position_id) {
                        case '1': _this.iconClass = 'order-map-icon--taxi-free';
                            break;
                        case '2': _this.iconClass = 'order-map-icon--cargo-free';
                            break;
                        case '4': _this.iconClass = 'order-map-icon--worker-free';
                            break;
                        default: _this.iconClass = 'order-map-icon--courier-free';
                    }
                    workerId = item.worker_id;
                    break;
                case 'get-busy-cars':
                    switch (item.position_id) {
                        case '1': _this.iconClass = 'order-map-icon--taxi-busy';
                            break;
                        case '2': _this.iconClass = 'order-map-icon--cargo-busy';
                            break;
                        case '4': _this.iconClass = 'order-map-icon--worker-busy';
                            break;
                        default: _this.iconClass = 'order-map-icon--courier-busy';
                    }
                    workerId = item.worker_id;
                    break;
                case 'get-pause-cars':
                    switch (item.position_id) {
                        case '1': _this.iconClass = 'order-map-icon--taxi-inactive';
                            break;
                        case '2': _this.iconClass = 'order-map-icon--cargo-inactive';
                            break;
                        case '4': _this.iconClass = 'order-map-icon--worker-inactive';
                            break;
                        default: _this.iconClass = 'order-map-icon--courier-inactive';
                    }
                    workerId = item.worker_id;
                    break;
                case 'get-offer-cars':
                    switch (item.position_id) {
                        case '1': _this.iconClass = 'order-map-icon--taxi-offer';
                            break;
                        case '2': _this.iconClass = 'order-map-icon--cargo-offer';
                            break;
                        case '4': _this.iconClass = 'order-map-icon--worker-offer';
                            break;
                        default: _this.iconClass = 'order-map-icon--courier-offer';
                    }
                    workerId = item.worker_id;
                    break;
            }

            iconAngle = item.iconAngle ? Number(item.iconAngle) : 0;

            if(iconAngle>0){
                if(iconAngle > 120 && iconAngle < 240){
                    _this.iconClass += ' up-id';
                }
            } else{
                _this.iconClass += ' no-arrow';
            }

            icon = L.divIcon({
                iconAnchor: iconAnchor,
                className: _this.iconClass,
                iconSize: iconSize,
                html: '<i style="transform: rotate('+iconAngle+'deg);"></i><span>'+workerId+'</span>'
            });

            if (!item.coords || !item.coords.lat || !item.coords.lon) {
                return;
            }
            latLng = L.latLng([ item.coords.lat, item.coords.lon ]);


            markerKeys.push(item.key_name);
            if (_this.markers[item.key_name]) {
                _this.markers[item.key_name].setLatLng(latLng);
                _this.markers[item.key_name].setIcon(icon);
            } else {
                _this.markers[item.key_name] =
                        L.marker(latLng, { icon: icon });
                _this.markers[item.key_name].bindPopup(item.marker_info);
            }
        });

        for (key in _this.markers) {
            if (_this.markers.hasOwnProperty(key)
                    && markerKeys.indexOf(key) === -1) {
                if (!Array.isArray(_this.deletedMarkers)) {
                    _this.deletedMarkers = [];
                }
                _this.deletedMarkers.push(_this.markers[key]);
                delete _this.markers[key];
            }
        }
    };
    MarkerCollection.prototype.sendRequest = function (cityId, optionsFilter) {
        return $.post(
                $('.open_map_win').data('module') + 'general-map/' + this.action,
                {
                    city_id: cityId,
                    cities: optionsFilter.cities,
                    positions: optionsFilter.positions,
                    groups: optionsFilter.groups,
                    classes: optionsFilter.classes,
                    searchLine: optionsFilter.searchLine,
                    tenantCompanies: optionsFilter.tenantCompanies
                }
        );
    };


    function OrderFreeCollection() {
        MarkerCollection.apply(this, arguments);
        this.filter = 'orders_free';
        this.action = 'get-orders-free';
        this.iconClass = 'order-map-icon--order';
    }
    OrderFreeCollection.prototype = Object.create(MarkerCollection.prototype);
    OrderFreeCollection.prototype.constructor = OrderFreeCollection;

    function OrderPreCollection() {
        MarkerCollection.apply(this, arguments);
        this.filter = 'orders_pre';
        this.action = 'get-orders-pre';
        this.iconClass = 'order-map-icon--order';
    }
    OrderPreCollection.prototype = Object.create(MarkerCollection.prototype);
    OrderPreCollection.prototype.constructor = OrderPreCollection;

    function FreeCarCollection() {
        MarkerCollection.apply(this, arguments);
        this.filter = 'free_cars';
        this.action = 'get-free-cars';
        this.iconClass = 'order-map-icon--taxi-free';
    }
    FreeCarCollection.prototype = Object.create(MarkerCollection.prototype);
    FreeCarCollection.prototype.constructor = FreeCarCollection;

    function BusyCarCollection() {
        MarkerCollection.apply(this, arguments);
        this.filter = 'busy_cars';
        this.action = 'get-busy-cars';
        this.iconClass = 'order-map-icon--taxi-busy';
    }
    BusyCarCollection.prototype = Object.create(MarkerCollection.prototype);
    BusyCarCollection.prototype.constructor = BusyCarCollection;

    function PauseCarCollection() {
        MarkerCollection.apply(this, arguments);
        this.filter = 'pause_cars';
        this.action = 'get-pause-cars';
        this.iconClass = 'order-map-icon--taxi-inactive';
    }
    PauseCarCollection.prototype = Object.create(MarkerCollection.prototype);
    PauseCarCollection.prototype.constructor = PauseCarCollection;

    function OfferCarCollection() {
        MarkerCollection.apply(this, arguments);
        this.filter = 'offer_cars';
        this.action = 'get-offer-cars';
        this.iconClass = 'order-map-icon--taxi-offer';
    }
    OfferCarCollection.prototype = Object.create(MarkerCollection.prototype);
    OfferCarCollection.prototype.constructor = OfferCarCollection;

    function OptionsFilter() {
        this.cities = [];
        this.positions = [];
        this.groups = [];
        this.classes = [];
        this.tenantCompanies = [];
        this.searchLine = '';
    }
    OptionsFilter.prototype.refreshOptions = function () {
        this.emptyOptions();

        this.cities = this.getValuesInput('#filter_map__cities input');
        this.positions = this.getValuesInput('#filter_map__positions input');
        this.groups = this.getValuesInput('#filter_map__groups input');
        this.classes = this.getValuesInput('#filter_map__classes input');
        this.tenantCompanies = this.getValuesInput('#filter_map__tenant-companies input');

        this.searchLine = $('#filter_map__searchLine').val();
    };
    OptionsFilter.prototype.getValuesInput = function (inputsSelector) {
        var $items = $(inputsSelector);

        var items = [];
        $.each($items, function (index, value) {
            if (value.checked) {
                items.push(value.value);
            }
        });
        return items;
    };
    OptionsFilter.prototype.emptyOptions = function () {
        this.cities = [];
        this.positions = [];
        this.groups = [];
        this.classes = [];
        this.tenantCompanies = [];
        this.searchLine = '';
    };
    OptionsFilter.prototype.registerOptionsEvent = function () {
        $('#filter_map input').on('change', function () {
            app.refreshMap(true);
        });
        $('#filter_map__searchLine').on('input', function () {
            app.refreshMap(true);
        });
    };
    OptionsFilter.prototype.viewSelectedCity = function () {
        var $inputs = $('#filter_map__cities input');
        var labelSelected = '';
        $.each($inputs, function (index, value) {
            if (value.checked) {
                labelSelected += value.parentNode.textContent + ', ';
                return true;
            }
        });
        if (labelSelected !== '') {
            var label = $('#filter_map__cities').parent().parent().find('.a_sc');
            label.text(labelSelected.substring(0, labelSelected.length - 2));
        }
    };
    OptionsFilter.prototype.initialization = function () {
        this.viewSelectedCity();
        this.registerOptionsEvent();
    };

    var app = {
        map: null,
        cityId: null,
        $filterElems: null,
        markerCollections: null,
        updateMapInterval: null,

        optionsFilter: null,

        initialize: function () {
            var _this = this;
            _this.cityId = _this.getCityId();

            _this.$filterElems = $('.win_map_header input[name="map_filter[]"]');
            _this.registerFilterEvent();

            _this.optionsFilter = new OptionsFilter();
            _this.optionsFilter.initialization();

            _this.initMap();
            _this.refreshMap(true);

            _this.updateMapInterval = setInterval(function () {
                _this.refreshMap();
            }, 5000);
        },

        finalize: function () {
            this.cityId = null;
            this.$filterElems = null;
            clearInterval(this.updateMapInterval);

            this.removeMap();
        },

        initMap: function () {
            // bugfix
            // When initializing the map html container must be visible
            var $mapContainer = $('#orders_map');
            $mapContainer.show();
            this.map = init_map([0, 0], 0, 'orders_map');
            $mapContainer.hide();

            this.markerCollections = [
                new OrderFreeCollection(),
                new OrderPreCollection(),
                new FreeCarCollection(),
                new BusyCarCollection(),
                new PauseCarCollection(),
                new OfferCarCollection()
            ];
        },

        removeMap: function () {
            this.markerCollections = null;

            if (this.map) {
                this.map.remove();
                this.map = null;
            }
        },

        refreshMap: function (center) {
            var _this = this,
                filters = _this.getFilters(),
                markerCollections = [],
                responses = [],
                response;

            if (!Array.isArray(_this.markerCollections) || _this.map === null) {
                return;
            }

            _this.optionsFilter.refreshOptions();

            _this.markerCollections.forEach(function (item) {
                item.active = false;
                item.error = null;

                if (filters.indexOf(item.filter) >= 0) {
                    markerCollections.push(item);
                    response = item.sendRequest(_this.cityId, _this.optionsFilter);
                } else {
                    item.removeMarkersFromMap(_this.map);
                    response = null;
                }
                responses.push(response);
            });

            $.when.apply($, responses)
                .done(function () {
                    if (_this.map === null)
                        return;

                    var args = Array.prototype.slice.call(arguments),
                        countActiveCollections = 0,
                        $mapContainer, $errorContainer, errorMessage;

                    if (responses.length === 1) {
                        args = [args];
                    }

                    args.forEach(function (item, index) {
                        if (!item || !item[0] || item[1] !== 'success') {
                            return;
                        }

                        if (item[0].message) {
                            errorMessage = item[0].message;
                            _this.markerCollections[index].removeMarkersFromMap(_this.map);
                        } else if (Array.isArray(item[0].markers) && item[0].markers.length) {
                            _this.markerCollections[index].active = true;
                            _this.markerCollections[index].addMarkers(item[0].markers);
                            _this.markerCollections[index].addMarkersToMap(_this.map);

                            countActiveCollections++;
                        } else {
                            _this.markerCollections[index].removeMarkersFromMap(_this.map);
                        }
                    });

                    $mapContainer = $(_this.map.getContainer());
                    $errorContainer = $mapContainer.parent().find('p');

                    if (!countActiveCollections) {
                        $mapContainer.hide();
                        $errorContainer.text(
                            errorMessage ? errorMessage : $errorContainer.data('empty'));
                        $errorContainer.show();
                    } else {
                        $errorContainer.hide();
                        $mapContainer.show();
                        if (center) {
                            _this.setCenter();
                        }
                    }
                })
                .fail(function (data) {
                    console.log('fail', data);
                });
        },

        setCenter: function () {
            var _this = this,
                points = [],
                bounds,
                i, l, key;

            if (!Array.isArray(_this.markerCollections)) {
                return;
            }

            for (i = 0, l = _this.markerCollections.length; i < l; i++) {
                for (key in _this.markerCollections[i].markers) {
                    if (_this.markerCollections[i].markers.hasOwnProperty(key)) {
                        points.push(_this.markerCollections[i].markers[key].getLatLng());
                    }
                }
            }

            if (!points) {
                return;
            }

            bounds = new L.latLngBounds(points);
            _this.map.fitBounds(bounds, { maxZoom: 13 });
        },

        getCityId: function () {
            var urlVars = getUrlVar();
            return urlVars && urlVars.city_id ? urlVars.city_id : null;
        },

        registerFilterEvent: function () {
            var _this = this;

            if (_this.$filterElems) {
                _this.$filterElems.on('change', function () {
                    _this.refreshMap(true);
                });
            }
        },

        getFilters: function () {
            var filters = [];

            this.$filterElems.each(function () {
                var $this = $(this);

                if ($this.prop('checked')) {
                    filters.push($this.val());
                }
            });
            return filters;
        }
    };

    return {
        initialize: function () {
            app.initialize();
        },
        finalize: function () {
            app.finalize();
        }
    };

})(jQuery);