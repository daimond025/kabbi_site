/*global jQuery, L, getOrderPriceAnalyz*/
var orderMap = (function ($) {
    'use strict';

    function MarkerCollection() {
        this.markers = null;
        this.deletedMarkers = null;
        this.action = null;
        this.iconClass = null;
        this.filter = null;
        this.active = false;
    }
    MarkerCollection.prototype.addMarkersToMap = function (map) {
        var key;

        if (!map || !this.markers) {
            return;
        }

        this.removeDeletedMarkersFromMap(map);

        for (key in this.markers) {
            if (this.markers.hasOwnProperty(key)) {
                map.addLayer(this.markers[key]);
            }
        }
    };
    MarkerCollection.prototype.removeDeletedMarkersFromMap = function (map) {
        if (!map || !Array.isArray(this.deletedMarkers)) {
            return;
        }

        while (this.deletedMarkers.length) {
            map.removeLayer(this.deletedMarkers.pop());
        }
    };
    MarkerCollection.prototype.removeMarkersFromMap = function (map) {
        var key;

        if (!map || !this.markers) {
            return;
        }

        for (key in this.markers) {
            if (this.markers.hasOwnProperty(key)) {
                map.removeLayer(this.markers[key]);
            }
        }
    };
    MarkerCollection.prototype.addMarkers = function (markers) {
        var _this = this, icon, latLng, iconAngle, key, markerKeys = [], workerId = '', iconSize = L.point(24, 24), iconAnchor = L.point(12, 12);

        if (!this.markers) {
            this.markers = [];
        }
        markers.forEach(function (item) {

            switch (_this.action) {
                case 'get-orders': _this.iconClass = 'order-map-icon--order';
                    iconSize = L.point(16, 16);
                    iconAnchor = L.point(8, 8);
                    break;
                case 'get-free-cars':
                    switch (item.position_id) {
                        case '1': _this.iconClass = 'order-map-icon--taxi-free';
                            break;
                        case '2': _this.iconClass = 'order-map-icon--cargo-free';
                            break;
                        case '4': _this.iconClass = 'order-map-icon--worker-free';
                            break;
                        default: _this.iconClass = 'order-map-icon--courier-free';
                    }
                    workerId = item.worker_id;
                    break;
                case 'get-busy-cars':
                    switch (item.position_id) {
                        case '1': _this.iconClass = 'order-map-icon--taxi-busy';
                            break;
                        case '2': _this.iconClass = 'order-map-icon--cargo-busy';
                            break;
                        case '4': _this.iconClass = 'order-map-icon--worker-busy';
                            break;
                        default: _this.iconClass = 'order-map-icon--courier-busy';
                    }
                    workerId = item.worker_id;
                    break;
                case 'get-pause-cars':
                    switch (item.position_id) {
                        case '1': _this.iconClass = 'order-map-icon--taxi-inactive';
                            break;
                        case '2': _this.iconClass = 'order-map-icon--cargo-inactive';
                            break;
                        case '4': _this.iconClass = 'order-map-icon--worker-inactive';
                            break;
                        default: _this.iconClass = 'order-map-icon--courier-inactive';
                    }
                    workerId = item.worker_id;
                    break;
            }

            iconAngle = item.iconAngle ? Number(item.iconAngle) : 0;

            if(iconAngle>0){
                if(iconAngle > 120 && iconAngle < 240){
                    _this.iconClass += ' up-id';
                }
            } else{
                _this.iconClass += ' no-arrow';
            }

            icon = L.divIcon({
                iconAnchor: iconAnchor,
                className: _this.iconClass,
                iconSize: iconSize,
                html: '<i style="transform: rotate('+iconAngle+'deg);"></i><span>'+workerId+'</span>'
            });

            if (!item.coords || !item.coords.lat || !item.coords.lon) {
                return;
            }
            latLng = L.latLng([ item.coords.lat, item.coords.lon ]);


            markerKeys.push(item.key_name);
            if (_this.markers[item.key_name]) {
                _this.markers[item.key_name].setLatLng(latLng);
                _this.markers[item.key_name].setIcon(icon);
            } else {
                _this.markers[item.key_name] =
                    L.marker(latLng, { icon: icon });
                _this.markers[item.key_name].bindPopup(item.marker_info);
            }
        });

        for (key in _this.markers) {
            if (_this.markers.hasOwnProperty(key)
                && markerKeys.indexOf(key) === -1) {
                if (!Array.isArray(_this.deletedMarkers)) {
                    _this.deletedMarkers = [];
                }
                _this.deletedMarkers.push(_this.markers[key]);
                delete _this.markers[key];
            }
        }
    };
    MarkerCollection.prototype.getRequest = function (cityId) {
        if(!$('.main_wrapper').hasClass('operator_orders')){
            return $.post(
                $('.open_map_win').data('module') + 'general-map/' + this.action,
                {city_id: cityId}
            );
        } else{
            return $.post(
                '/order/general-map/' + this.action,
                {city_id: cityId}
            );
        }
    };

    function FreeCarCollection() {
        MarkerCollection.apply(this, arguments);
        this.filter = 'free_cars';
        this.action = 'get-free-cars';
        this.iconClass = 'free_car_icon';
    }
    FreeCarCollection.prototype = Object.create(MarkerCollection.prototype);
    FreeCarCollection.prototype.constructor = FreeCarCollection;

    function BusyCarCollection() {
        MarkerCollection.apply(this, arguments);
        this.filter = 'busy_cars';
        this.action = 'get-busy-cars';
        this.iconClass = 'busy_car_icon';
    }
    BusyCarCollection.prototype = Object.create(MarkerCollection.prototype);
    BusyCarCollection.prototype.constructor = BusyCarCollection;

    function PauseCarCollection() {
        MarkerCollection.apply(this, arguments);
        this.filter = 'pause_cars';
        this.action = 'get-pause-cars';
        this.iconClass = 'order-map-icon--taxi-inactive';
    }
    PauseCarCollection.prototype = Object.create(MarkerCollection.prototype);
    PauseCarCollection.prototype.constructor = PauseCarCollection;

    var app = {
        map: null,
        markers: [],
        markers2: [],
        routePoints: [],
        routePolyline: null,
        filterElems: '.order-window__map-header input[type="checkbox"]',
        markerCollections: null,
        updateMapInterval: null,

        initialize: function () {
            this.registerShowMapEvent();
            this.registerMapEvents();

        },

        finalize: function () {
            this.finalizeMap();
        },

        registerShowMapEvent: function () {
            var _this = this;

            $('.js-show-map').click(function () {
                var $elem = $('.js-order-map');
                $elem.toggle();

                if ($elem.not(':hidden').length) {

                    if (!_this.map) {
                        if(!$('.main_wrapper').hasClass('cabinet-page')){
                            _this.cityId = _this.getCityId();

                            _this.registerFilterEvent();

                            _this.markerCollections = [
                                new FreeCarCollection(),
                                new BusyCarCollection(),
                                new PauseCarCollection()
                            ];

                            _this.refreshMap(true);

                            _this.updateMapInterval = setInterval(function () {
                                _this.refreshMap();
                            }, 5000);
                        } else{
                            $('.order-window__map-header').hide();
                        }

                        _this.initializeMap();
                    } else {
                        if(!$('.main_wrapper').hasClass('cabinet-page')){
                            _this.refreshMap(true);

                            _this.updateMapInterval = setInterval(function () {
                                _this.refreshMap();
                            }, 5000);
                        }

                        _this.refreshMarkers();
                        _this.refreshRoutes(_this.routePoints);
                    }
                } else{
                    clearInterval(_this.updateMapInterval);
                }

                return false;
            });
        },

        registerMapEvents: function () {
            var _this = this;

            $(document).on('updateMap', function (e, routePoints) {
                _this.routePoints = routePoints;
                if (_this.map
                        && $('.js-order-map').not(':hidden').length) {
                    _this.refreshMarkers();
                    _this.refreshRoutes(routePoints);
                }
            });

            $(document).on('disableMap', function () {
                if (_this.map) {
                    _this.disableMap();
                }
            });

            $(document).on('enableMap', function () {
                if (_this.map) {
                    _this.enableMap();
                }
            });
        },

        finalizeMap: function () {
            if (this.map) {
                this.map.remove();
                this.map = null;
            }

            this.markers = [];
            this.routePolyline = null;
            this.routePoints = [];

            $('.js-order-map').empty().trigger('finalize-map');
        },

        initializeMap: function () {
            $('.js-order-map')
                    .append($('<div id="order-map" style="height: 500px"/>'))
                    .trigger('initialize-map');

            this.map = init_map(['0', '0'], 0, 'order-map');
            this.refreshMarkers();

            if (this.routePoints && this.routePoints.length) {
                this.refreshRoutes(this.routePoints);
            }
        },

        disableMap: function () {
            this.map.dragging.disable();
            this.map.touchZoom.disable();
            this.map.doubleClickZoom.disable();
            this.map.scrollWheelZoom.disable();
            this.map.boxZoom.disable();
            this.map.keyboard.disable();
            if (this.map.tap) {
                this.map.tap.disable();
            }
        },

        enableMap: function () {
            this.map.dragging.enable();
            this.map.touchZoom.enable();
            this.map.doubleClickZoom.enable();
            this.map.scrollWheelZoom.enable();
            this.map.boxZoom.enable();
            this.map.keyboard.enable();
            if (this.map.tap) {
                this.map.tap.enable();
            }
        },

        getRequestCityCoords: function (cityId) {
            return $.post(
                    '/city/city/get-city-coords',
                    { city_id: cityId}
                );
        },

        getWorkerCoords: function (cityId) {
            return $.post(
                '/city/city/get-worker-coords',
                { city_id: cityId}
            );
         },

        removeMarkers: function () {
            var i;

            for (i = 0; i < this.markers.length; i++) {
                this.map.removeLayer(this.markers[i]);
            }
            this.markers = [];
        },

        markerDragEndEvent: function () {
            var latLng = this.getLatLng();
            this.formElement.find('.lat').val(latLng.lat);
            this.formElement.find('.lon').val(latLng.lng);

            $(document).trigger('update-marker-coords', [this.formElement]);
        },

        getAddressIcon: function (key) {
            return L.divIcon({
                iconAnchor: [12, 28],
                html: key,
                className: 'map_icon_point'
            });
        },

        getAddressString: function ($elem) {
            return [
                $elem.find('input[name$="[city]"]').val(),
                $elem.find('input[name$="[street]"]').val(),
                $elem.find('input[name$="[house]"]').val(),
                $elem.find('input[name$="[housing]"]').val(),
                $elem.find('input[name$="[porch]"]').val(),
                $elem.find('input[name$="[apt]"]').val()
            ].filter(function (value) {
                return value;
            }).join(', ');
        },

        refreshMarkers: function () {
            var _this = this,
                template,
                deferredObjs = [
                    _this.getRequestCityCoords($('.js-city_id').val())
                ];

            _this.removeMarkers();

            $('.js-order-point').each(function (index) {
                var $elem = $(this),
                    icon,
                    marker,
                    markerParams,
                    title = String.fromCharCode(65+index),
                    lat = $elem.find('.lat').val(),
                    lon = $elem.find('.lon').val(),
                    cityId = $elem.find('.js-cur-city_id').val(),
                    autocomplete = $elem.find('.ui-autocomplete-input').val();

                if (index && !lat && !lon && !autocomplete) {
                    return;
                } else {
                    template =
                        '<div class="win_map_content">\
                            <div class="wmc_win">\
                                <div class="wmcw_content">\
                                    <span>' + _this.getAddressString($elem) + '</span>\
                                </div>\
                            </div>\
                        </div>';

                    markerParams = {
                        title: title,
                        draggable: true
                    };

                    icon = _this.getAddressIcon(title);
                    if (icon) {
                        markerParams.icon = icon;
                        markerParams.iconAnchor = ['52', '88'];
                    }

                    marker = L.marker([0, 0], markerParams)
                        .bindPopup(template)
                        .addTo(_this.map);

                    marker.cityId = cityId;
                    marker.formElement = $elem;
                    marker.on('dragend', _this.markerDragEndEvent);

                    _this.markers.push(marker);


                    if (lat && lon) {
                        deferredObjs.push([{ lat: lat, lon: lon }]);
                    } else {
                        deferredObjs.push(_this.getRequestCityCoords(cityId));
                        deferredObjs.push(_this.getWorkerCoords(cityId));
                    }
                }
            });



            $.when.apply($, deferredObjs)
                    .done(function () {
                        var i, markerCoords;

                        for (i = 0; i < _this.markers.length; i++) {
                            markerCoords = arguments[i+1][0] === 'error'
                                ? arguments[0][0] : arguments[i+1][0];
                            _this.markers[i].setLatLng(
                                    new L.LatLng(markerCoords.lat, markerCoords.lon));

                        }


                        _this.setCenter();
                    })
                    .fail(function (data) {
                        console.log('fail', data);
                    });
        },

        refreshMap: function (center) {

            var _this = this,
                filters = _this.getFilters(),
                markerCollections = [],
                requests = [],
                request;

            if (!Array.isArray(_this.markerCollections)) {
                return;
            }

            _this.markerCollections.forEach(function (item) {
                item.active = false;
                item.error = null;

                if (filters.indexOf(item.filter) >= 0) {
                    markerCollections.push(item);
                    request = item.getRequest(_this.cityId);
                } else {
                    item.removeMarkersFromMap(_this.map);
                    request = null;
                }
                requests.push(request);
            });

            $.when.apply($, requests)
                .done(function () {
                    var args = Array.prototype.slice.call(arguments),
                        countActiveCollections = 0,
                        errorMessage;

                    if (requests.length === 1) {
                        args = [args];
                    }

                    args.forEach(function (item, index) {
                        if (!item || !item[0] || item[1] !== 'success') {
                            return;
                        }

                        if (item[0].message) {
                            errorMessage = item[0].message;
                            _this.markerCollections[index].removeMarkersFromMap(_this.map);
                        } else if (Array.isArray(item[0].markers) && item[0].markers.length){
                            _this.markerCollections[index].active = true;
                            _this.markerCollections[index].addMarkers(item[0].markers);
                            _this.markerCollections[index].addMarkersToMap(_this.map);

                            countActiveCollections++;
                        } else {
                            _this.markerCollections[index].removeMarkersFromMap(_this.map);
                        }
                    });

                })
                .fail(function (data) {
                    console.log('fail', data);
                });
        },

        refreshRoutes: function (routePoints) {
            var latLngs = [], i, l;
            if (this.routePolyline) {
                this.map.removeLayer(this.routePolyline);
                this.routePolyline = null;
            }

            if (routePoints && Array.isArray(routePoints)) {
                for (i = 0, l = routePoints.length; i < l; i++) {
                    latLngs.push(new L.LatLng(routePoints[i][0], routePoints[i][1]));
                }

                this.routePolyline =
                    L.polyline(latLngs, {color: "blue"}).addTo(this.map);
            }
        },

        setCenter: function () {
            var _this = this,
                points = [],
                bounds,
                i;

            for (i = 0; i < _this.markers.length; i++) {
                points.push(_this.markers[i].getLatLng());
            }

            for (i = 0; i < _this.markers2.length; i++) {
                points.push(_this.markers2[i].getLatLng());
            }

            if (!points) {
                return;
            }

            bounds = new L.latLngBounds(points);
            _this.map.fitBounds(bounds, { maxZoom: 13 });

            // Bugfix
            // After re-opening the modal window doesn't work 
            // centering map (invalidate map tiles and objects)
            setTimeout(function () {
                _this.map.panBy(new L.Point(1, 1));
            }, 0);
        },

        getCityId: function () {
            var urlVars = getUrlVar();
            return urlVars && urlVars.city_id ? urlVars.city_id : null;
        },

        registerFilterEvent: function () {
            var _this = this;

            if ($(_this.filterElems)) {
                $(_this.filterElems).on('change', function () {
                    _this.refreshMap(true);
                });
            }
        },

        getFilters: function () {

            var filters = [];

            $(this.filterElems).each(function () {
                var $this = $(this);

                if ($this.prop('checked')) {
                    filters.push($this.val());
                }
            });
            return filters;
        }
    };

    return {
        initialize: function () {
            app.initialize();
        },
        finalize: function () {
            app.finalize();
        }
    };
})(jQuery);