<?php

namespace app\modules\order\assets;

use yii\web\AssetBundle;

/**
 * Class ViewOrderAsset
 * @package app\modules\order\assets
 */
class ViewOrderAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/order/assets/js';

    public $publishOptions = [
        'forceCopy' => true,
        'only'      => [
            'submit.js',
            'orderOpenInModalEvent.js',
            'orderSettings.js'
        ],
    ];

    public $js = [
        'submit.js',
        'orderOpenInModalEvent.js',
        'orderSettings.js',
        'passenger.js'
    ];

    public $depends = [
    ];

}
