(function($) {
    window.KB = window.KB || {};
    var URL = window.KB.URL || {};
    var MESSAGES = window.KB.MSG || {};

    /*********************************************************
     * KApi
     ********************************************************/

    function KApi() {
        this.timeout = 10000;
        this.baseUrl = '/order/order-assign/';
    }

    KApi.prototype = {
        constructor: KApi,
        _send: function (type, method, params) {
            var self = this,
                defer = $.Deferred();

            $.ajax({
                type: type,
                dataType: 'JSON',
                async: true,
                url: self.baseUrl + method,
                timeout: self.timeout,
                data: params,
                success: function (data) {
                    if (data.hasOwnProperty('code') && data.code === 1) {
                        defer.resolve(data.result, method);
                    } else {
                        defer.reject(['API', data]);
                    }
                },
                error: function (xhr) {
                    if (xhr.statusText !== 'abort') {
                        defer.reject(['XHR', xhr]);
                    }
                },
            });
            return defer;
        },
        getWorkers: function (params) {
            return this._send('GET', 'list-workers', params);
        },
        getOrders: function (params) {
            return this._send('GET', 'list-orders', params);
        },
        assign: function (params) {
            return this._send('POST', 'assign-orders', params);
        },
        revoke: function (params) {
            return this._send('POST', 'remove-workers', params);
        },
        cancel: function (params) {
            return this._send('POST', 'remove-orders', params);
        },
        getTaskResult: function (params) {
            return this._send('POST', 'get-result', params);
        },
        watchTaskResult: function(queue) {
            var _queue = $.extend({}, queue),
                self = this,
                defer = $.Deferred(),
                nextCheck = function () {
                    if (Object.keys(_queue).length) {
                        setTimeout(function () {
                            $.when(self.getTaskResult({orders: _queue})).then(
                                function (items) {
                                    for (var id in items) {
                                        var item = items[id];

                                        if (item.code === 1) {
                                            delete _queue[id];
                                        } else if (item.code === 0 && item.info !== 'WAIT') {
                                            defer.reject();
                                            return;
                                        }
                                    }

                                    nextCheck();
                                },
                                function () {
                                    defer.reject();
                                }
                            );
                        }, 1500);
                    } else {
                        defer.resolve();
                    }
                };
            nextCheck();
            return defer;
        },
        sendScheduleMails: function (companyId) {
            return this._send('GET', 'send-email', {
                company_id: companyId
            });
        },
        saveNote: function (workerId, text) {
            return this._send('POST', 'worker-note', {
                workerId: workerId,
                note: text
            });
        }
    };

    /*********************************************************
     * Реестр заказов
     ********************************************************/

    function Orders() {
        var self = this;
        self.list = [];
        self.workerCountMap = {};
    }

    Orders.prototype = {
        constructor: Orders,
        setList: function (list) {
            var self = this;

            self.list = list.sort(function compare(a, b) {
                var aVal = a.picktime.toUpperCase(),
                    bVal = b.picktime.toUpperCase();

                if (aVal > bVal) return 1;
                if (bVal > aVal) return -1;

                if (a.id > b.id) return 1;
                if (b.id > a.id) return -1;

                return 0;
            });

            self.workerCountMap = {};

            for (var n in list) {
                var item = list[n],
                    workerKey = item.workerid + '';

                if (workerKey in self.workerCountMap) {
                    self.workerCountMap[workerKey]++;
                } else {
                    self.workerCountMap[workerKey] = 1;
                }
            }
        },
        getOpen: function (companyId) {
            var self = this,
                list = [];

            for (var n in self.list) {
                var item = self.list[n];

                if ((!item.companyId || item.companyId === companyId) && !item.workerid) {
                    list.push(item);
                }
            }

            return list;
        },
        getAssign: function (companyId, workerId) {
            if (workerId === null) {
                return null;
            } else {
                var self = this,
                    list = [];

                for (var n in self.list) {
                    var item = self.list[n];

                    if ((!item.companyId || item.companyId === companyId) && item.workerid === workerId) {
                        list.push(item);
                    }
                }

                return list;
            }
        },
        getItemById: function (id) {
            var self = this;

            for (var n in self.list) {
                var item = self.list[n];

                if (item.id === id) {
                    return item;
                }
            }

            return null;
        }
    };

    /*********************************************************
     * Реестр исполнителей
     ********************************************************/

    function Workers() {
        var self = this;
        self.list = [];
    }

    Workers.prototype = {
        constructor: Workers,
        setList: function (list) {
            this.list = list.sort(function compare(a, b) {
                var aVal = a.name.toUpperCase(),
                    bVal = b.name.toUpperCase();

                if (aVal > bVal) return 1;
                if (bVal > aVal) return -1;

                aVal = (a.callsign + '').toUpperCase();
                bVal = (b.callsign + '').toUpperCase();

                if (aVal > bVal) return 1;
                if (bVal > aVal) return -1;

                if (a.worker_id > b.worker_id) return 1;
                if (b.worker_id > a.worker_id) return -1;

                return 0;
            });
        },
        getItemById: function (id) {
            var self = this;

            for (var n in self.list) {
                var item = self.list[n];

                if (item.worker_id === id) {
                    return item;
                }
            }

            return null;
        },
        updateNote: function (id, text) {
            var self = this;

            for (var n in self.list) {
                var item = self.list[n];

                if (item.worker_id === id) {
                    item.note = text;
                    break;
                }
            }
        }
    };

    /*********************************************************
     * Реестр статуса отправки E-mail рассылки водителям
     ********************************************************/

    function WkrNtfRes() {
        var self = this;
        self.list = [];
    }

    WkrNtfRes.prototype = {
        constructor: WkrNtfRes,
        setStatus: function (id, status) {
            var self = this;

            if (status === null) {
                delete self.list[id];
            } else {
                self.list[id] = status;
            }
        }
    };

    /*********************************************************
     * Базовый объект колонки.
     ********************************************************/

    function BaseCol(options) {
        var self = this;
        self.$el = $('[data-role=' + options.role + ']');
        self.$cont = self.$el.find('[data-role=cont]');
        self.onSelect = $.Callbacks();
        self._bindSelect(options);
    }

    BaseCol.prototype = {
        constructor: BaseCol,
        _bindSelect: function (options) {
            var self = this;

            switch (options.mode) {
                // Radiobutton (without input).
                case 'radio':
                    self.$cont.on('click', '[data-role=item]', function (e) {
                        var $target = $(e.currentTarget);

                        if (!$target.hasClass('kb-active')) {
                            self.$cont.find('.kb-active[data-role=item]').removeClass('kb-active');
                            $target.addClass('kb-active');
                            self.onSelect.fire($target);
                        }
                    });
                    break;

                // Checkbox (with input).
                case 'icheck':
                    self.$cont.on('click', '[data-role=item]', function (e) {
                        if (!$(e.target).closest('.Spectuator').length) {
                            var $target = $(e.currentTarget);

                            if ($target.hasClass('kb-active')) {
                                $target.removeClass('kb-active');
                                $target.find('input').prop('checked', false);
                            } else {
                                $target.addClass('kb-active');
                                $target.find('input').prop('checked', true);
                            }

                            self.onSelect.fire();
                        }
                    });
                    break;
            }
        },
        hasChecked: function () {
            return this.$cont.find('.kb-active').length > 0;
        },
        getChecked: function (type) {
            var items = [];

            switch (type) {
                case 'id':
                    this.$cont.find('.kb-active').each(function() {
                        items.push(parseInt($(this).data('id'), 10));
                    });
                    break;

                default:
                    this.$cont.find('.kb-active').each(function() {
                        items.push($(this));
                    });
            }

            return items;
        },
        clearCont: function () {
            this.$cont.children().remove();
        },
        appendEmpty: function (msg) {
            this.$cont.append($('<div class="kb-scol-empty">' + msg + '</div>'));
        }
    };

    /*********************************************************
     * Базовый объект колонки заказов.
     ********************************************************/

    function ColOrder(options) {
        BaseCol.call(this, options);
    }

    ColOrder.prototype = Object.create(BaseCol.prototype);
    ColOrder.prototype.constructor = ColOrder;

    ColOrder.prototype.appendItems = function (items, checkIds) {
        var self = this,
            prevDate = '';

        for (var n in items) {
            var item = items[n],
                timeParts = item.picktime.split(' '),
                isCheck = (checkIds.indexOf(item.id) >= 0),
                extClass = '';

            if (prevDate !== timeParts[0]) {
                prevDate = timeParts[0];
                self.$cont.append(
                    $('<div class="kb-timesep">' + prevDate + '</div>')
                );
            }

            if (isCheck) {
                extClass += ' kb-active';
            }

            self.$cont.append($(
                '<div class="kb-order' + extClass + '" data-id="' + item.id + '" data-role="item">' +
                '   <div class="kb-checker">' +
                '       <input type="checkbox" value="1"' + (isCheck ? ' checked' : '') + ' />' +
                '       <a class="ot_edit ot_open Spectuator" href="' + URL['orderEdit'].replace('{ORDER_NUMBER}', item.no) + '"></a>' +
                '   </div>' +
                '   <div class="kb-info">' +
                '       <div class="kb-addr kb-addr-a">' +
                '           <span class="od_d">A</span>' +
                '           <div>' + item.fromaddr.A + '</div>' +
                '       </div>' +
                '       <div class="kb-addr kb-addr-b">' +
                '           <span class="od_d">b</span>' +
                '           <div>' + (item.fromaddr.hasOwnProperty('B') ? item.fromaddr.B : '&minus;') + '</div>' +
                '       </div>' +
                '   </div>' +
                '   <div class="kb-top">' +
                '       <div class="kb-cliname">' + ('clientName' in item ? item.clientName : '&minus;') + '</div>' +
                '       <div class="kb-sign">' +
                '           <div class="kb-no">№ ' + item.no + '</div>' +
                '           <div class="kb-sep"></div>' +
                '           <div class="kb-time">' + timeParts[1] + '</div>' +
                '       </div>' +
                '   </div>' +
                '</div>'
            ));
        }
    };

    /*********************************************************
     * Колонка свободных заказов
     ********************************************************/

    function ColOpen() {
        var self = this;
        ColOrder.call(self, {
            role: 'open',
            mode: 'icheck'
        });
    }

    ColOpen.prototype = Object.create(ColOrder.prototype);
    ColOpen.prototype.constructor = ColOpen;

    ColOpen.prototype.render = function(items) {
        var self = this,
            checkedIds = self.getChecked('id');

        //==== Reset/Clear
        self.clearCont();

        //==== Populate
        if (items.length) {
            self.appendItems(items, checkedIds);
        } else {
            self.appendEmpty(MESSAGES['NO_FREE_ORDERS']);
        }
    };

    /*********************************************************
     * Колонка заказов исполнителя
     ********************************************************/

    function ColOwn() {
        var self = this;
        ColOrder.call(self, {
            role: 'own',
            mode: 'icheck'
        });
        self.$selectAll = self.$el.find('[data-role=select-all]');
        self.$checkAllInput = self.$selectAll.find('input');
        self.$selectAll.hide();
        self._init();
    }

    ColOwn.prototype = Object.create(ColOrder.prototype);
    ColOwn.prototype.constructor = ColOwn;

    ColOwn.prototype._init = function () {
        var self = this;
        self.$checkAllInput.on('click', function() {
            if (self.$checkAllInput.prop('checked')) {
                self.$cont
                    .find('[data-role=item]')
                    .each(function() {
                        var $this = $(this);
                        $this.addClass('kb-active');
                        $this.find('input').prop('checked', true);
                    });
            } else {
                self.$cont
                    .find('.kb-active[data-role=item]')
                    .each(function() {
                        var $this = $(this);
                        $this.removeClass('kb-active');
                        $this.find('input').prop('checked', false);
                    });
            }

            self.onSelect.fire();
        });
        self.onSelect.add(function() {
            self.refreshCheckAll();
        });
    };

    ColOwn.prototype.refreshCheckAll = function () {
        var self = this,
            $items = self.$cont.find('[data-role=item]');

        self.$checkAllInput.prop('checked', ($items.length === $items.filter('.kb-active').length));
    };

    ColOwn.prototype.render = function (items) {
        var self = this,
            checkedIds = self.getChecked('id');

        //==== Reset/Clear
        self.clearCont();
        self.$checkAllInput.prop('checked', false);

        //==== Populate
        if (items === null) {
            self.appendEmpty(MESSAGES['PICK_WORKER']);
        }
        else if (items.length) {
            self.appendItems(items, checkedIds);
        } else {
            self.appendEmpty(MESSAGES['WORKER_NO_ORDERS']);
        }

        if (items && items.length) {
            self.$selectAll.fadeIn();
            self.refreshCheckAll();
        } else {
            self.$selectAll.fadeOut();
        }
    };

    /*********************************************************
     * Колонка исполнителя
     ********************************************************/

    function ColWorker(options) {
        var self = this;
        BaseCol.call(self, {
            role: 'worker',
            mode: 'radio'
        });
        self.kapi = options.kapi;
        /**
         * @type {Workers}
         */
        self.store = options.store;
        self._init();
    }

    ColWorker.prototype = Object.create(BaseCol.prototype);
    ColWorker.prototype.constructor = ColWorker;

    ColWorker.prototype._init = function() {
        var self = this;
        self.onSelect.add(function($target) {
            self.currentId = parseInt($target.data('id'), 10);
        });
        self.$cont.on('keyup', 'textarea', function (e) {
            var $target = $(e.currentTarget);
            var $item = $target.closest('[data-role="item"]');
            $item.find('[data-toggle="k-save"]').prop('disabled', false);
            $item.find('[data-toggle="k-clear"]').prop('disabled', false);
        });
        self.$cont.on('click', '[data-toggle="k-save"]', function (e) {
            var $buttonSave = $(e.currentTarget);
            var $item = $buttonSave.closest('[data-role="item"]');
            var $actionStatus = $item.find('.kb-asta');
            var $buttonClear = $item.find('[data-toggle="k-clear"]');
            var buttonClearDisabled = $buttonClear.prop('disabled')
            var id = $item.data('id');
            var note = $item.find('textarea').val();

            $buttonSave.prop('disabled', true);
            $buttonClear.prop('disabled', true);
            $actionStatus.removeClass('kb-error').html('');

            $.when(self.kapi.saveNote(id, note).then(
                function () {
                    self.store.updateNote(id, note);
                    $buttonClear.prop('disabled', note === '');
                },
                function () {
                    $buttonSave.prop('disabled', false);
                    $buttonClear.prop('disabled', buttonClearDisabled);
                    $actionStatus.addClass('kb-error').html(MESSAGES['ASTA_SAVING_FAILURE'])
                }
            ));
        });
        self.$cont.on('click', '[data-toggle="k-clear"]', function (e) {
            var $buttonClear = $(e.currentTarget);
            var $item = $buttonClear.closest('[data-role="item"]');
            var $actionStatus = $item.find('.kb-asta');
            var $buttonSave = $item.find('[data-toggle="k-save"]');
            var buttonSaveDisabled = $buttonSave.prop('disabled')
            var id = $item.data('id');
            var note = '';

            $buttonSave.prop('disabled', true);
            $buttonClear.prop('disabled', true);
            $actionStatus.removeClass('kb-error').html('');

            $.when(self.kapi.saveNote(id, note).then(
                function () {
                    self.store.updateNote(id, note);
                    $item.find('textarea').val(note);
                },
                function () {
                    $buttonSave.prop('disabled', buttonSaveDisabled);
                    $buttonClear.prop('disabled', false);
                    $actionStatus.addClass('kb-error').html(MESSAGES['ASTA_SAVING_FAILURE'])
                }
            ));
        });
    };

    ColWorker.prototype.render = function(wnrList, countMap) {
        var self = this;
        var origFound = false;
        var list = self.store.list;

        //==== Reset/Clear
        self.clearCont();

        //==== Populate
        if (list.length) {
            for (var n in list) {
                var item = list[n],
                    workerId = item.worker_id,
                    workerKey = item.worker_id + '',
                    extClass = '',
                    countExtClass = '',
                    countText = '0';

                if (workerId === self.currentId) {
                    extClass += ' kb-active';
                    origFound = true;
                }

                if (wnrList.hasOwnProperty(item.worker_id)) {
                    if (wnrList[item.worker_id] === 'SEND') {
                        extClass += ' kb-send';
                    } else {
                        extClass += ' kb-fail';
                    }
                }

                if (workerKey in countMap) {
                    countExtClass += ' kb-busy';
                    countText = countMap[workerKey];
                }

                self.$cont.append($(
                    '<div class="kb-worker' + extClass + '" data-id="' + item.worker_id + '" data-role="item">' +
                    '   <div class="kb-worker-top">' +
                    '       <div class="kb-name">' + item.name + '</div>' +
                    '       <div class="kb-sep">/</div>' +
                    '       <div class="kb-call">' + item.callsign + '</div>' +
                    '       <div class="kb-count' + countExtClass + '">' + countText + '</div>' +
                    '   </div>' +
                    '   <div class="kb-worker-box">' +
                    '       <textarea name="worker">' + item.note + '</textarea>' +
                    '       <div class="kb-actions">' +
                    '           <div class="kb-asta"></div>' +
                    '           <div class="kb-buttons">' +
                    '               <button class="button btn-save" disabled data-toggle="k-save">' + MESSAGES['SAVE'] + '</button>' +
                    '               <button class="button kb-btn-warn btn-clear" ' + (item.note === '' ? 'disabled' : '') + ' data-toggle="k-clear">' + MESSAGES['CLEAR'] + '</button>' +
                    '           </div>' +
                    '       </div>' +
                    '   </div>' +
                    '</div>'
                ));
            }
        } else {
            self.appendEmpty(MESSAGES['NO_WORKERS']);
        }

        if (!origFound) {
            self.currentId = null;
        }
    };

    /*********************************************************
     * Корневой объект виджета.
     ********************************************************/

    function Focus() {
        var self = this;
        self.cityId = null;
        self.companyId = null;
        self.from = '';
        self.to = '';
        self.$city = $('#kb_focus_city');
        self.$company = $('#kb_focus_company');
        self.$from = $('#kb_focus_from');
        self.$to = $('#kb_focus_to');
        self.onCity = $.Callbacks();
        self.onCompany = $.Callbacks();
        self.onDate = $.Callbacks();
        self._init();
    }

    Focus.prototype = {
        constructor: Focus,
        _init: function () {
            var self = this,
                $targetFrom = self.$from.next('.sdp_filter'),
                $targetTo = self.$to.next('.sdp_filter'),
                $targetFromAlt = $targetFrom.prev('input'),
                $targetToAlt = $targetTo.prev('input'),
                targetFromOnSelect =  $targetFrom.datepicker('option' , 'onSelect'),
                targetToOnSelect =  $targetTo.datepicker('option' , 'onSelect');

            //==== Current
            self.cityId = parseInt(self.$city.find(':selected').val(), 10);
            self.companyId = parseInt(self.$company.find(':selected').val(), 10);
            self.from = self.$from.val();
            self.to = self.$to.val();

            //==== Bind
            self.$city.change(function () {
                self.cityId = parseInt(self.$city.find(':selected').val(), 10);
                self.onCity.fire();
            });
            self.$company.change(function () {
                self.companyId = parseInt(self.$company.find(':selected').val(), 10);
                self.onCompany.fire();
            });
            $targetFrom.datepicker('option', 'onSelect', function(strDate) {
                var objDate = $targetFrom.datepicker('getDate');
                self.from = strDate;
                targetFromOnSelect.call(this, strDate);
                // Установка ограничителя для поля "По".
                $targetTo.datepicker('option', 'minDate', new Date(objDate.getFullYear(), objDate.getMonth(),objDate.getDate()));
                // Актуализация и исполнение обратного вызовы "onSelect" для поля "По".
                // Приведёт к исполнению обратных вызовов "self.onDate".
                window.$.datepicker._selectDate($targetTo, null);
            });
            $targetTo.datepicker('option', 'onSelect', function(date) {
                self.to = date;
                targetToOnSelect.call(this, date);
                self.onDate.fire();
            });

            //==== Sub-init
            $targetFrom.datepicker('setDate', $targetFromAlt.data('sub-init'));
            $targetTo.datepicker('setDate', $targetToAlt.data('sub-init'));
            window.$.datepicker._selectDate($targetFrom, null);
            window.$.datepicker._selectDate($targetTo, null);
        },
        disable: function () {
            var self = this;

            self.$city.next('.d_select').addClass('disabled_select');
            self.$company.next('.d_select').addClass('disabled_select');
            self.$from.closest('.cof_date').addClass('disabled_select');
            self.$to.closest('.cof_date').addClass('disabled_select');
        },
        enable: function () {
            var self = this;

            self.$city.next('.d_select').removeClass('disabled_select');
            self.$company.next('.d_select').removeClass('disabled_select');
            self.$from.closest('.cof_date').removeClass('disabled_select');
            self.$to.closest('.cof_date').removeClass('disabled_select');
        }
    }

    /*********************************************************
     * Корневой объект виджета.
     ********************************************************/

    function Widget(options) {
        var self = this;
        self.$el = $(options.selector);
        self.$locker = self.$el.find('[data-role=locker]');
        self.$lockerCont = self.$el.find('[data-role=locker-cont]');
        self.focus = options.focus;
        self.orders = new Orders();
        self.workers = new Workers();
        self.colOpen = new ColOpen();
        self.colWorker = new ColWorker({
            kapi: options.kapi,
            store: self.workers,
        });
        self.colOwn = new ColOwn();
        self.wnr = new WkrNtfRes();
        self._init();
    }

    Widget.prototype = {
        constructor: Widget,
        _init: function () {
            var self = this;
            self.colWorker.onSelect.add(function () {
                self.renderColOwn();
            });
        },
        lock: function (msg) {
            var self = this;
            self.$lockerCont.html(msg);
            self.$locker.fadeIn();
        },
        lockFailure: function (msg) {
            var self = this;
            self.$lockerCont
                .addClass('failure')
                .html(msg);
            self.$locker.fadeIn();
        },
        unlock: function () {
            var self = this;
            self.$locker.stop(true, false).fadeOut();
        },
        assign: function() {
            var self = this,
                workerId = self.colWorker.currentId,
                list = {};

            self.colOpen.$cont.find('.kb-active').each(function() {
                var id = parseInt($(this).data('id'), 10);

                for (var n in self.orders.list) {
                    var item = self.orders.list[n];

                    if (item.id === id) {
                        list[id] = workerId;
                        item.workerid = workerId;
                        break;
                    }
                }
            });
            return list;
        },
        revoke: function () {
            var self = this,
                workerId = self.colWorker.currentId,
                list = {};

            self.colOwn.$cont.find('.kb-active').each(function() {
                var id = parseInt($(this).data('id'), 10);

                for (var n in self.orders.list) {
                    var item = self.orders.list[n];

                    if (item.id === id) {
                        list[id] = workerId;
                        item.workerid = null;
                    }
                }
            });
            return list;
        },
        renderColOpen: function() {
            var self = this;
            self.colOpen.render(
                self.orders.getOpen(self.focus.companyId)
            );
        },
        renderColWorker: function() {
            var self = this;
            self.colWorker.render(self.wnr.list, self.orders.workerCountMap);
        },
        renderColOwn: function() {
            var self = this;
            self.colOwn.render(
                self.orders.getAssign(self.focus.companyId, self.colWorker.currentId)
            );
        },
        render: function () {
            var self = this;
            self.renderColOpen();
            self.renderColWorker();
            self.renderColOwn();
        }
    };


    $(document).ready(function () {
        var kapi = new KApi(),
            focus = new Focus(),
            widget = new Widget({
                selector: '#order_asgmt',
                focus: focus,
                kapi: kapi
            }),
            $btnAssign = $('#kb_assign'),
            $btnRevoke = $('#kb_revoke'),
            $btnCancel = $('#kb_cancel'),
            $btnSendScheduleMails = $('#kb_send_schedule_mails'),
            $btnDownloadSchedule = $('#kb_download_schedule'),
            btnAssignRefresh = function() {
                if (widget.colOpen.hasChecked() && widget.colWorker.currentId) {
                    $btnAssign.fadeIn();
                } else {
                    $btnAssign.fadeOut();
                }
            },
            btnRevokeRefresh = function() {
                if (widget.colOwn.hasChecked() && widget.colWorker.currentId) {
                    $btnRevoke.fadeIn();
                } else {
                    $btnRevoke.fadeOut();
                }
            },
            btnCancelRefresh = function() {
                if (widget.colOpen.hasChecked() || widget.colOwn.hasChecked()) {
                    $btnCancel.fadeIn();
                } else {
                    $btnCancel.fadeOut();
                }
            },
            btnDownloadScheduleRefresh = function() {
                $btnDownloadSchedule.attr('href', URL['scheduleReport'].replace('{COMPANY_ID}', focus.companyId));
            },
            lockLoading = function () {
                focus.disable();
                widget.lock(MESSAGES['LOADING']);
            },
            lockSavingStage1 = function () {
                focus.disable();
                widget.lock('<strong>' + MESSAGES['SAVING_STAGE_1'] + ':</strong> ' + MESSAGES['SUBMITTING']);
            },
            lockSavingStage2 = function () {
                widget.lock('<strong>' + MESSAGES['SAVING_STAGE_2'] + ':</strong> ' + MESSAGES['CHECKING']);
            },
            lockSendingScheduleMails = function () {
                focus.disable();
                widget.lock(MESSAGES['SEND_SCHEDULE_EMAIL']);
                $btnSendScheduleMails.prop('disabled', true);
            },
            lockLoadFailure = function () {
                widget.lockFailure(MESSAGES['LOAD_FAILURE'] + '<br/ >' + MESSAGES['PLEASE_RELOAD']);
            },
            lockSaveFailure = function () {
                widget.lockFailure(MESSAGES['SAVE_FAILURE'] + '<br/ >' + MESSAGES['PLEASE_RELOAD']);
            },
            lockSendScheduleMailsFailure = function () {
                widget.lockFailure(MESSAGES['MAIL_FAILURE'] + '<br/ >' + MESSAGES['PLEASE_RELOAD']);
            },
            kapiGetWorkers = function () {
                return kapi.getWorkers({
                    company_id: focus.companyId
                });
            },
            kapiGetOrders = function () {
                return kapi.getOrders({
                    city_id: focus.cityId,
                    date_from: focus.from,
                    date_to: focus.to
                })
            },
            kapiDoneLoad = function () {
                widget.render();
                widget.unlock();
                focus.enable();
                btnAssignRefresh();
                btnRevokeRefresh();
                btnCancelRefresh();
            },
            kapiDoneSubmit = function (data, method) {
                var queue = {};

                for (var id in data) {
                    var item = data[id];

                    if (item.code === 1 && item.uuid) {
                        queue[id] = item.uuid;
                    } else {
                        var isFailure = true;

                        switch (method) {
                            case 'remove-orders':
                                isFailure = (item.reason !== 'ORDER_COMPLATED' && item.reason !== 'ORDER_NOT_IN_DB');
                                break;
                        }

                        if (isFailure) {
                            lockSaveFailure();
                            return;
                        }
                    }
                }

                widget.wnr.setStatus(widget.colWorker.currentId, null);
                lockSavingStage2();
                kapi.watchTaskResult(queue).then(refreshOrders, lockSaveFailure);
            },
            refreshOrders = function () {
                lockLoading();
                $.when(kapiGetOrders()).then(
                    function (orders) {
                        widget.orders.setList(orders);
                        kapiDoneLoad();
                    },
                    lockLoadFailure
                );
            };

        widget.colOpen.onSelect.add(btnAssignRefresh);
        widget.colOpen.onSelect.add(btnCancelRefresh);
        widget.colWorker.onSelect.add(btnAssignRefresh);
        widget.colWorker.onSelect.add(btnRevokeRefresh);
        widget.colOwn.onSelect.add(btnRevokeRefresh);
        widget.colOwn.onSelect.add(btnCancelRefresh);
        btnDownloadScheduleRefresh();
        $btnAssign.hide();
        $btnRevoke.hide();
        $btnCancel.hide();

        focus.onCity.add(function() {
            refreshOrders();
        });
        focus.onCompany.add(function () {
            lockLoading();
            btnDownloadScheduleRefresh();
            $.when(kapiGetWorkers()).then(
                function (workers) {
                    widget.workers.setList(workers);
                    kapiDoneLoad();
                },
                lockLoadFailure
            );
        });
        focus.onDate.add(function() {
            refreshOrders();
        });

        $btnAssign.click(function() {
            var newly = widget.assign();
            btnAssignRefresh();
            btnRevokeRefresh();
            lockSavingStage1();
            $.when(kapi.assign({orders: newly})).then(kapiDoneSubmit, lockSaveFailure);
        });
        $btnRevoke.click(function() {
            var newly = widget.revoke();
            btnAssignRefresh();
            btnRevokeRefresh();
            lockSavingStage1();
            $.when(kapi.revoke({orders: newly})).then(kapiDoneSubmit, lockSaveFailure);
        });
        $btnCancel.click(function() {
            var items = {},
                openCheckedIds = widget.colOpen.getChecked('id'),
                ownCheckedIds = widget.colOwn.getChecked('id'),
                checkedIds = openCheckedIds.concat(ownCheckedIds);

            lockSavingStage1();

            for (var n in checkedIds) {
                var id = checkedIds[n],
                    order = widget.orders.getItemById(id),
                    worker = widget.workers.getItemById(order.workerid);

                items[id] = worker ? worker.callsign : '';
            }

            $.when(kapi.cancel({orders: items})).then(kapiDoneSubmit, lockSaveFailure);
        });
        $btnSendScheduleMails.click(function() {
            lockSendingScheduleMails();
            $.when(kapi.sendScheduleMails(focus.companyId).then(
                function (data) {
                    for (var n in data) {
                        var item = data[n];

                        if (item.code === 1) {
                            widget.wnr.setStatus(item.worker_id, 'SEND');
                        }
                        else if (item.code === 0
                            && item.reason !== 'EMAIL_EMPTY'
                            && item.reason !== 'ORDER_HAS_NOT_WOREKRS')
                        {
                            widget.wnr.setStatus(item.worker_id, null);
                        } else {
                            widget.wnr.setStatus(item.worker_id, 'FAIL');
                        }
                    }

                    widget.renderColWorker();
                    widget.unlock();
                    focus.enable();
                    $btnSendScheduleMails.prop('disabled', false);
                },
                lockSendScheduleMailsFailure
            ));
        });

        lockLoading();
        $.when(kapiGetWorkers(), kapiGetOrders()).then(
            function (workers, orders) {
                widget.workers.setList(workers[0]);
                widget.orders.setList(orders[0]);
                kapiDoneLoad();
            },
            lockLoadFailure
        );

        colorbox_init();
        
        $(function () {
            var orderModalId = $('.edit-order-fake').data('gootax-modal'),
                targetOrderModalOnClosed = $.fn.gootaxModal.settings[orderModalId].onClosed;

            $.fn.gootaxModal.settings[orderModalId].onClosed = function() {
                targetOrderModalOnClosed();
                refreshOrders();
            };
        });
    });
})(jQuery);

