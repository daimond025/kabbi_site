'use strict';

let gulp = require('gulp'),
    taskListing = require('gulp-task-listing');

// ----------------------------------------------------------
//  Build tasks
// ----------------------------------------------------------

gulp.task('css:build', function () {
    let processor = function (src, dist) {
            let gp_plumber = require('gulp-plumber'),
                gp_sass = require('gulp-sass'),
                gp_csscomb = require('gulp-csscomb'),
                gp_postcss = require('gulp-postcss'),
                gp_rename = require('gulp-rename'),
                gp_autoprefixer = require('gulp-autoprefixer'),
                gp_csso = require('gulp-csso'),
                stream = gulp.src(src);

            stream = stream
                .pipe(gp_plumber())
                // sass still require catch errors (prevent watch interrupt)
                .pipe(gp_sass({
                    outputStyle: 'expanded',
                    errLogToConsole: true
                })
                    .on('error', gp_sass.logError))
                .pipe(gp_postcss([
                    require('postcss-opacity')
                ]))
                .pipe(gp_autoprefixer({
                    overrideBrowserslist: [
                        'last 2 versions',
                        'last 10 Chrome versions',
                        'last 10 Firefox versions',
                        'ie > 8',
                        'android > 4.1',
                        'ios > 4.3'
                    ],
                    cascade: false
                }))
                .pipe(gp_csscomb())
                .pipe(gp_csso());

            return stream
                .pipe(gp_plumber.stop())
                .pipe(gp_rename(function (path) {
                    path.basename += '.min';
                }))
                .pipe(gulp.dest(dist));
        },
        jobs = [
            processor(
                './src/sass/*.scss',
                './dist/'
            )
        ];
    return Promise.all(jobs);
});
gulp.task('js:build', function () {
    let processor = function (src, dist) {
            let gp_rename = require('gulp-rename'),
                gp_uglify = require('gulp-uglify-es').default,
                stream = gulp.src(src);

            return stream
                .pipe(gp_uglify())
                .pipe(gp_rename(function (path) {
                    path.basename += '.min';
                }))
                .pipe(gulp.dest(dist));
        },
        jobs = [
            processor(
                './src/js/*.js',
                './dist/'
            )
        ];
    return Promise.all(jobs);
});
gulp.task('watch', function () {
    gulp.watch('./src/sass/*.scss', gulp.series('css:build'));
    gulp.watch('./src/js/*.js', gulp.series('js:build'));
});
gulp.task('help', taskListing);