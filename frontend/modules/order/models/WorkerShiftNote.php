<?php
namespace frontend\modules\order\models;

use app\modules\tenant\models\PhoneLine;
use common\modules\employee\models\worker\Worker;

/**
 * This is the model class for table "{{%call}}".
 *
 * @property integer   $id
 * @property integer   $worker_id
 * @property string    $note
 * @property integer   $created_at
 */
class WorkerShiftNote extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_shift_note}}';
    }

    public function beforeSave($insert)
    {
        $this->created_at = time();
        return parent::beforeSave($insert);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }
}