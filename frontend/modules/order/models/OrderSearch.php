<?php
namespace app\modules\order\models;

use app\modules\tenant\models\User;
use common\modules\city\models\City;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\order\models\Order;

class OrderSearch extends Model
{
    public $status_id;
    public $status;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_id'], 'required'],
            [['status_id'], 'string'],
            ['status', 'validateFilter']
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateFilter($attribute, $params)
    {
        return true;
    }
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public static function getPjaxUrl(){
        $params =  app()->request->queryParams;

        if(isset($params['date_to'])){
            unset($params['date_to']);
        }
        if(isset($params['date_from'])){
            unset($params['date_from']);
        }

        if(isset($params['city_id'])){
            unset($params['city_id']);
        }
        if(isset($params['filter'])){
            unset($params['filter']);
        }
        if(isset($params['_pjax'])){
            unset($params['_pjax']);
        }
        return $params;
    }

}
