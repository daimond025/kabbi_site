<?php

namespace app\modules\order\models\forms;


use app\modules\tariff\models\TaxiTariff;
use orderApi\interfaces\OptionsSuitablePromoCode;
use yii\base\Model;

class OptionsSuitableCode extends Model implements OptionsSuitablePromoCode
{

    public $client_id;
    public $city_id;
    public $position_id;
    public $tariff_id;

    public $order_date;
    public $order_hours;
    public $order_minutes;



    public function getTenantId()
    {
        return user()->tenant_id;
    }

    public function getClientId()
    {
        return $this->client_id;
    }

    public function getOrderTime()
    {
        return sprintf('%s %s:%s', $this->order_date, $this->order_hours, $this->order_minutes);
    }

    public function getCityId()
    {
        return $this->city_id;
    }

    public function getPositionId()
    {
        return $this->position_id;
    }

    public function getCarClassId()
    {
        return TaxiTariff::find()
            ->select('class_id')
            ->where(['tariff_id' => $this->tariff_id])
            ->scalar();
    }

}