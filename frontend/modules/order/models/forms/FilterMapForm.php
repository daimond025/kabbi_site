<?php

namespace app\modules\order\models\forms;

use frontend\modules\companies\models\TenantCompany;
use yii\base\Model;

class FilterMapForm extends Model
{

    public $cities;
    public $statuses;
    public $positions;
    public $groups;
    public $classes;
    public $tenantCompanies;

    public $city_id;

    public $searchLine;

    public function rules()
    {
        return [
            [['cities', 'statuses', 'positions', 'groups', 'classes'], 'each', 'rule' => ['integer']],
            ['searchLine', 'string'],
            [
                ['tenantCompanies'],
                'each',
                'rule' => [
                    'match',
                    'pattern' => sprintf('/^\d+|%s$/', TenantCompany::FIELD_NAME_MAIN_COMPANY)
                ]
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'cities'          => t('user', 'All cities'),
            'positions'       => t('employee', 'Profession'),
            'groups'          => t('employee', 'Worker group'),
            'classes'         => t('employee', 'Car class'),
            'searchLine'      => t('employee', 'Search by name, callsign and car'),
            'tenantCompanies' => t('tenant_company', 'Companies'),
        ];
    }

}