<?php

namespace app\modules\order\models;

class OrderSearchByString extends \yii\base\Model
{
    public $search_string;

    public function rules()
    {
        return [
            ['search_string', 'safe'],
        ];
    }
}