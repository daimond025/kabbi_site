<?php

namespace app\modules\order\models;

use common\modules\employee\models\worker\Worker;
use Yii;
use app\modules\order\models\Order;

/**
 * This is the model class for table "{{%card_payment}}".

 * @property integer $refuse_id
 * @property integer $shift_id
 * @property integer $order_id
 * @property integer $refuse_time
 * @property integer $worker_id *
 * @property string $refuse_subject

 *
 * @property Order $order
 */
class WorkerRefuseOrder extends \yii\db\ActiveRecord
{

    const timer     = 'timer';
    const driver = 'driver';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_refuse_order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['refuse_id', 'shift_id', 'order_id','worker_id', 'refuse_time' ], 'required'],
            [['refuse_id', 'shift_id', 'order_id','worker_id' ], 'integer'],
            ['refuse_subject', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'refuse_id'               => 'Refuse Id',
            'shift_id'         => 'Shift ID',
            'order_id'              => 'Order Id',
            'refuse_time'      => 'Refuse Time',
            'refuse_subject' => 'Refuse Subject',
            'worker_id'   => 'Worker Id',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }
}
