<?php

namespace app\modules\order\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%order_status}}".
 *
 * @property integer           $status_id
 * @property string            $name
 * @property string            $status_group
 * @property integer           $dispatcher_sees
 *
 * @property Order[]           $orders
 * @property OrderStatusTime[] $orderStatusTimes
 */
class OrderStatus extends \yii\db\ActiveRecord
{

    const STATUS_GROUP_0 = 'new';
    const STATUS_GROUP_1 = 'car_assigned';
    const STATUS_GROUP_2 = 'car_at_place';
    const STATUS_GROUP_3 = 'executing';
    const STATUS_GROUP_4 = 'completed';
    const STATUS_GROUP_5 = 'rejected';
    const STATUS_GROUP_6 = 'pre_order';
    const STATUS_GROUP_7 = 'warning';
    const STATUS_GROUP_8 = 'works';
    const STATUS_GROUP_9 = 'all';

    const STATUS_GROUP_NEW = 'new';
    const STATUS_GROUP_PRE_ORDER = 'pre_order';
    const STATUS_GROUP_EXECUTING = 'executing';
    const STATUS_GROUP_COMPLETED = 'completed';
    const STATUS_GROUP_REJECTED = 'rejected';
    const STATUS_GROUP_CAR_ASSIGNED = 'car_assigned';
    const STATUS_GROUP_CAR_AT_PLACE = 'car_at_place';

    const STATUS_NEW = 1;
    const STATUS_OFFER_ORDER = 2;
    const STATUS_WORKER_REFUSED = 3;
    const STATUS_FREE = 4;
    const STATUS_NOPARKING = 5;
    const STATUS_PRE = 6;
    const STATUS_PRE_GET_WORKER = 7;
    const STATUS_PRE_REFUSE_WORKER = 10;
    const STATUS_PRE_NOPARKING = 16;
    const STATUS_GET_WORKER = 17;
    const STATUS_WORKER_WAITING = 26;
    const STATUS_CLIENT_IS_NOT_COMING_OUT = 27;
    const STATUS_FREE_AWAITING = 29;
    const STATUS_NONE_FREE_AWAITING = 30;
    const STATUS_EXECUTING = 36;
    const STATUS_COMPLETED_PAID = 37;
    const STATUS_COMPLETED_NOT_PAID = 38;
    const STATUS_REJECTED = 39;
    const STATUS_NO_CARS = 40;
    const STATUS_OVERDUE = 52;
    const STATUS_WORKER_LATE = 54;
    const STATUS_EXECUTION_PRE = 55;
    const STATUS_PAYMENT_CONFIRM = 106;
    const STATUS_NO_CARS_BY_TIMER = 107;
    const STATUS_MANUAL_MODE = 108;
    const STATUS_WORKER_IGNORE_ORDER_OFFER = 109;
    const STATUS_WAITING_FOR_PAYMENT = 110;
    const STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_SOFT = 111;
    const STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_HARD = 112;
    const STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT = 113;
    const STATUS_WORKER_ASSIGNED_AT_ORDER_HARD = 114;
    const STATUS_REFUSED_ORDER_ASSIGN = 115;
    const STATUS_WAITING_FOR_PRE_ORDER_CONFIRMATION = 116;
    const STATUS_WORKER_IGNORED_PRE_ORDER_CONFIRMATION = 117;
    const STATUS_WORKER_REFUSED_PRE_ORDER_CONFIRMATION = 118;
    const STATUS_WORKER_ACCEPTED_PRE_ORDER_CONFIRMATION = 119;
    const STATUS_CANCELED_BY_WORKER = 120;



    const STATUS_PRE_FOR_HOSPITAL = 203;
    const STATUS_FREE_FOR_HOSPITAL = 204;

    const STATUS_PRE_ORDER_COMPANY  = 205;
    const STATUS_FREE_FOR_COMPANY = 206;


    const CACHE_KEY = 'tbl_order_status';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_status}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['dispatcher_sees'], 'integer'],
            [['name', 'status_group'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'status_id'       => 'Status ID',
            'name'            => 'Name',
            'status_group'    => 'Status Group',
            'dispatcher_sees' => 'Dispatcher Sees',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['status_id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatusTimes()
    {
        return $this->hasMany(OrderStatusTime::className(), ['status_id' => 'status_id']);
    }

    public static function getDispatcherStatusList($status_id)
    {
        $arStatus = self::find()
            ->where(['status_id' => self::getStatuses($status_id)])
            ->orderBy(['status_id' => SORT_ASC])
            ->asArray()
            ->all();

        return ArrayHelper::index($arStatus, 'status_id');
    }

    /**
     * Getting status set by status_id
     *
     * @param integer $status_id
     *
     * @return array
     */
    public static function getStatuses($status_id)
    {
        switch ($status_id) {
            case self::STATUS_NEW;
                $arStatus = [self::STATUS_NEW, self::STATUS_FREE, self::STATUS_MANUAL_MODE];
                break;
            case self::STATUS_OFFER_ORDER;
                $arStatus = [self::STATUS_OFFER_ORDER, self::STATUS_FREE];
                break;
            case self::STATUS_FREE;
                $arStatus = [self::STATUS_NEW, self::STATUS_FREE, self::STATUS_MANUAL_MODE];
                break;
            case self::STATUS_MANUAL_MODE;
                $arStatus = [self::STATUS_NEW, self::STATUS_FREE, self::STATUS_MANUAL_MODE];
                break;
            case self::STATUS_PRE_GET_WORKER:
                $arStatus = [self::STATUS_PRE, self::STATUS_PRE_GET_WORKER];
                break;
            case self::STATUS_OVERDUE;
                $arStatus = [self::STATUS_NEW, self::STATUS_FREE, self::STATUS_OVERDUE, self::STATUS_MANUAL_MODE];
                break;
            case self::STATUS_GET_WORKER;
                $arStatus = [self::STATUS_FREE, self::STATUS_GET_WORKER, self::STATUS_COMPLETED_PAID];
                break;
            case self::STATUS_WORKER_WAITING;
                $arStatus = [self::STATUS_FREE, self::STATUS_WORKER_WAITING, self::STATUS_COMPLETED_PAID];
                break;
            case self::STATUS_CLIENT_IS_NOT_COMING_OUT;
                $arStatus = [self::STATUS_FREE, self::STATUS_CLIENT_IS_NOT_COMING_OUT, self::STATUS_COMPLETED_PAID];
                break;
            case self::STATUS_FREE_AWAITING;
                $arStatus = [self::STATUS_FREE, self::STATUS_FREE_AWAITING, self::STATUS_COMPLETED_PAID];
                break;
            case self::STATUS_NONE_FREE_AWAITING;
                $arStatus = [self::STATUS_FREE, self::STATUS_NONE_FREE_AWAITING, self::STATUS_COMPLETED_PAID];
                break;
            case self::STATUS_EXECUTING;
                $arStatus = [self::STATUS_FREE, self::STATUS_EXECUTING, self::STATUS_COMPLETED_PAID];
                break;
            case self::STATUS_WAITING_FOR_PAYMENT;
                $arStatus = [self::STATUS_FREE, self::STATUS_WAITING_FOR_PAYMENT, self::STATUS_COMPLETED_PAID];
                break;
            case self::STATUS_WORKER_LATE;
                $arStatus = [self::STATUS_FREE, self::STATUS_WORKER_LATE, self::STATUS_COMPLETED_PAID];
                break;
            case self::STATUS_EXECUTION_PRE;
                $arStatus = [
                    self::STATUS_NEW,
                    self::STATUS_EXECUTION_PRE,
                    self::STATUS_COMPLETED_PAID,
                    self::STATUS_MANUAL_MODE,
                ];
                break;
            default :
                $arStatus = [$status_id];
        }

        return $status_id != 37 && $status_id != 38 ? array_unique(array_merge($arStatus,
            self::getRejectedStatusId())) : $arStatus;
    }

    public static function getFilterStatusList($filter_group)
    {
        $status_data = ArrayHelper::map(self::getStatusData(), 'status_id', 'name');

        switch ($filter_group) {
            case self::STATUS_GROUP_0:
                $filterStatusList = [
                    '113' => $status_data[113],
                    '114' => $status_data[114],
                    '4'   => $status_data[4],
                    '5'   => $status_data[5],
                    '52'  => $status_data[52],
                    '108' => $status_data[108],
                ];
                break;
            case self::STATUS_GROUP_1:
                $filterStatusList = [
                    '17' => $status_data[17],
                    '54' => $status_data[54],
                    '55'   => $status_data[55],
                ];
                break;
            case self::STATUS_GROUP_2:
                $filterStatusList = [
                    '26' => $status_data[26],
                    '27' => $status_data[27],
                    '29'   => $status_data[29],
                    '30'   => $status_data[30],
                ];
                break;
            case self::STATUS_GROUP_3:
                $filterStatusList = [
                    '17'  => $status_data[17],
                    '26'  => $status_data[26],
                    '43'  => $status_data[43],
                    '36'  => $status_data[36],
                    '110' => $status_data[110],
                ];
                break;
            case self::STATUS_GROUP_4:
                $filterStatusList = [
                    '37' => $status_data[37],
                    '38' => $status_data[38],
                ];
                break;
            case self::STATUS_GROUP_5:
                $filterStatusList = [
                    '39'  => $status_data[39],
                    '40'  => $status_data[40],
                    '41'  => $status_data[41],
                    '42'  => $status_data[42],
                    '43'  => $status_data[43],
                    '44'  => $status_data[44],
                    '45'  => $status_data[45],
                    '46'  => $status_data[46],
                    '47'  => $status_data[47],
                    '48'  => $status_data[48],
                    '49'  => $status_data[49],
                    '50'  => $status_data[50],
                    '51'  => $status_data[51],
                    '107' => $status_data[107],
                    '120' => $status_data[120],
                ];
                break;
            case self::STATUS_GROUP_7:
                $warning_statuses = self::getWarningStatusId();
                foreach ($warning_statuses as $warning_status_id) {
                    $filterStatusList[$warning_status_id] = $status_data[$warning_status_id];
                }
                break;
            default:
                $filterStatusList = [];
        }

        return $filterStatusList;
    }

    public static function getNewStatusId()
    {
        return [1, 2, 3, 4, 5, 52, 108, 109, 113, 114, 115,  204,  206];
    }

    public static function getWorksStatusId()
    {
        return [17, 26, 27, 29, 30, 36, 54, 55, 106, 110];
    }

    public static function getWarningStatusId()
    {
        return [5, 16, 27, 30, 38, 45, 46, 47, 48, 52, 54, 203, 204, 205, 206];
    }

    public static function getPreOrderStatusId()
    {
        return [6, 7, 16, 111, 112, 116, 117, 118, 119, 203, 205];
    }

    public static function getCompletedStatusId()
    {
        return [37, 38];
    }

    public static function getRejectedStatusId()
    {
        return [39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 107, 120];
    }

    public static function getRejectedStatusGroup($status_id)
    {
        if (in_array($status_id, [39, 41, 42])) {
            return t('status_event', 'Rejected by the client');
        } elseif (in_array($status_id, [43, 44, 45, 46, 47, 48, 49, 50, 51, 107, 40])) {
            return t('status_event', 'Rejected by the dispatcher');
        };

        return null;
    }

    public static function getFinishedStatusId()
    {
        return array_merge(self::getCompletedStatusId(), self::getRejectedStatusId());
    }

    public static function getRejectedStatusIdByClient()
    {
        return [39, 40, 41, 42, 43, 44];
    }

    public static function getRejectedStatusIdByWorker()
    {
        return [45, 46, 47, 48];
    }

    public static function getStatusWithPickUpTime()
    {
        return [self::STATUS_NEW, self::STATUS_FREE, self::STATUS_NOPARKING];
    }

    public static function getBlockEditStatuses()
    {
        return [1, 2, 3, 109];
    }

    /**
     * For order update card.
     * @return array
     */
    public static function getCountdownSatusList()
    {
        return [1, 4, 17, 26, 27, 52, 54, 113, 114,  204,  206];
    }

    /**
     * Can cancel the order by the client
     * @return array
     */
    public static function getCanCancelOrderByCLient()
    {
        return [1, 2, 3, 4, 5, 6, 7, 10, 16, 17, 52, 54, 55, 108, 109, 111, 112, 113, 114, 115, 116, 117, 118, 119];
    }

    /**
     * Allow to know order status group.
     *
     * @param integer $status_id
     *
     * @return array Statuses
     */
    public static function getStatusGroup($status_id)
    {
        $arStatusGroup = [];

        if (in_array($status_id, self::getWarningStatusId())) {
            $arStatusGroup[] = 'warning';
        }
        if (in_array($status_id, self::getNewStatusId())) {
            $arStatusGroup[] = 'new';
        }
        if (in_array($status_id, self::getWorksStatusId())) {
            $arStatusGroup[] = 'works';
        }
        if (in_array($status_id, self::getPreOrderStatusId())) {
            $arStatusGroup[] = 'pre_order';
        }
        if (in_array($status_id, self::getCompletedStatusId())) {
            $arStatusGroup[] = 'completed';
        }
        if (in_array($status_id, self::getRejectedStatusId())) {
            $arStatusGroup[] = 'rejected';
        }

        return $arStatusGroup;
    }

    /**
     * Группы, которые входят в раздел "В работе"
     * @return array
     */
    public static function getSubWorkGroups()
    {
        return [
            self::STATUS_GROUP_1,
            self::STATUS_GROUP_2,
            self::STATUS_GROUP_3,
        ];
    }

    /**
     * Группы, у которых необходимо выводить счетчик новых заказов
     * @return array
     */
    public static function getGroupsForCalculating()
    {
        return [
            self::STATUS_GROUP_0,
            self::STATUS_GROUP_8,
            self::STATUS_GROUP_7,
            self::STATUS_GROUP_6,
        ];
    }

    /**
     * Группы, которые хранятся в редисе
     * @return array
     */
    public static function getGroupsFromRedis()
    {
        return [
            self::STATUS_GROUP_0,
            self::STATUS_GROUP_8,
            self::STATUS_GROUP_7,
            self::STATUS_GROUP_6,
        ];
    }

    /**
     * Список статусов из БД (Кешируется).
     * @return array
     */
    public static function getStatusData()
    {
        $cache = Yii::$app->cache;
        $data  = $cache->get(self::CACHE_KEY);

        if ($data === false) {
            $data = self::find()->asArray()->all();
            $cache->set(self::CACHE_KEY, $data);
        }

        return $data;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        //При обновлении чистим кеш
        $cache = Yii::$app->cache;
        $cache->delete(self::CACHE_KEY);
    }

    /**
     * Группы статусов при которых на карте отображается водитель.
     * @return array
     */
    public static function getStatusGroupsForMapWorkerType()
    {
        return ['car_assigned', 'car_at_place', 'executing'];
    }

    /**
     * Группы статусов при которых на карте отображается маршрут заказа.
     * @return array
     */
    public static function getStatusGroupsForMapRouteType()
    {
        return ['completed'];
    }

}
