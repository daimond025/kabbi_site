<?php

namespace app\modules\order\models;

use Yii;

/**
 * This is the model class for table "{{%order_views}}".
 *
 * @property integer $view_id
 * @property integer $user_id
 * @property integer $tenant_id
 * @property integer $city_id
 * @property integer $new
 * @property integer $works
 * @property integer $warning
 * @property integer $pre_order
 *
 * @property City $city
 * @property Tenant $tenant
 * @property User $user
 */
class OrderViews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_views}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'tenant_id'], 'required'],
            [['new', 'works', 'warning', 'pre_order'], 'default', 'value' => 0],
            [['view_id', 'user_id', 'tenant_id', 'city_id', 'new', 'works', 'warning', 'pre_order'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'view_id' => 'View ID',
            'user_id' => 'User ID',
            'tenant_id' => 'Tenant ID',
            'city_id' => 'City ID',
            'new' => 'New',
            'works' => 'Works',
            'warning' => 'Warning',
            'pre_order' => 'Pre Order',
            'completed' => 'Completed',
            'rejected' => 'Rejected',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_id']);
    }

    /**
     * Allow to save multiple rows
     * @param array $arCity
     * @param array $user_id
     * @param array $tenant_id
     * @return boolean
     */
    public static function batchInsert($arCity, $user_id, $tenant_id)
    {
        if(is_array($arCity) && !empty($arCity))
        {
            $insertValue = [];
            $connection = app()->db;

            foreach($arCity as $city_id)
            {
                if($city_id > 0)
                    $insertValue[] = [$user_id, $tenant_id, $city_id];
            }

            if(!empty($insertValue))
            {
                $connection->createCommand()->batchInsert(self::tableName(), ['user_id', 'tenant_id', 'city_id'], $insertValue)->execute();

                return true;
            }
        }

        return false;
    }

    public static function refreshRows($user_id, $tenant_id, $user_city_list)
    {
        $order_views = self::find()
                    ->where(['user_id' => $user_id])
                    ->select(['view_id', 'user_id', 'city_id'])
                    ->all();

        if(!is_null($order_views))
        {
            //Очистка от старых запией
            foreach($order_views as $order_view)
            {
                if(!in_array($order_view->city_id, $user_city_list))
                {
                    $order_view->delete();
                }
            }

            //Поиск новых
            $order_views_city = \yii\helpers\ArrayHelper::getColumn($order_views, 'city_id');
            $user_city_list = array_diff($user_city_list, $order_views_city);
        }

        self::batchInsert ($user_city_list, $user_id, $tenant_id);
    }
}
