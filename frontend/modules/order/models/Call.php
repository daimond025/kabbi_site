<?php

namespace app\modules\order\models;

use app\modules\tenant\models\PhoneLine;
use common\modules\city\models\City;
use Yii;
use yii\helpers\Html;
use app\modules\client\models\ClientPhone;

/**
 * This is the model class for table "{{%call}}".
 *
 * @property integer   $id
 * @property string    $uniqueid
 * @property integer   $tenant_id
 * @property integer   $order_id
 * @property string    $type
 * @property string    $source
 * @property string    $destination
 * @property integer   $phone_line_id
 * @property string    $channel
 * @property string    $destinationchannel
 * @property string    $lastapplication
 * @property integer   $starttime
 * @property integer   $answertime
 * @property integer   $endtime
 * @property integer   $duration
 * @property integer   $billableseconds
 * @property string    $disposition
 * @property string    $amaflags
 * @property string    $userfield
 * @property integer   $create_time
 *
 * @property string   $clientPhone
 * @property string   $dispatcher_phone
 *
 * @property PhoneLine $phoneLine
 * @property Tenant    $tenant
 */
class Call extends \yii\db\ActiveRecord
{

    const STATUS_RECEIVED = 1; // принят

    const STATUS_MISSED = 2; // пропущен

    const STATUS_SUCCESS = 3; // мы дозвонились

    const STATUS_UNSUCCESS = 4; // мы не дозвонились

    const STATUS_BUSY = 5; // занято

    const STATUS_UNKNOWN = 6; // неизвестный статус

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%call}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'uniqueid',
                    'tenant_id',
                    'type',
                    'source',
                    'destination',
                    'channel',
                    'destinationchannel',
                    'lastapplication',
                    'duration',
                    'billableseconds',
                    'disposition',
                    'create_time',
                ],
                'required',
            ],
            [['tenant_id', 'order_id', 'phone_line_id', 'duration', 'billableseconds', 'create_time'], 'integer'],
            [['type'], 'string'],
            [['starttime', 'answertime', 'endtime'], 'safe'],
            [['uniqueid'], 'string', 'max' => 60],
            [['source', 'destination', 'lastapplication'], 'string', 'max' => 20],
            [['channel', 'destinationchannel', 'disposition', 'amaflags'], 'string', 'max' => 50],
            [['userfield'], 'string', 'max' => 30],
            [
                ['phone_line_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => PhoneLine::className(),
                'targetAttribute' => ['phone_line_id' => 'line_id'],
            ],
            [
                ['tenant_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Tenant::className(),
                'targetAttribute' => ['tenant_id' => 'tenant_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                 => Yii::t('employee', 'ID'),
            'uniqueid'           => Yii::t('employee', 'Uniqueid'),
            'tenant_id'          => Yii::t('employee', 'Tenant ID'),
            'order_id'           => Yii::t('employee', 'Order ID'),
            'type'               => Yii::t('employee', 'Type'),
            'source'             => Yii::t('employee', 'Source'),
            'destination'        => Yii::t('employee', 'Destination'),
            'phone_line_id'      => Yii::t('employee', 'Phone Line ID'),
            'channel'            => Yii::t('employee', 'Channel'),
            'destinationchannel' => Yii::t('employee', 'Destinationchannel'),
            'lastapplication'    => Yii::t('employee', 'Lastapplication'),
            'starttime'          => Yii::t('employee', 'Starttime'),
            'answertime'         => Yii::t('employee', 'Answertime'),
            'endtime'            => Yii::t('employee', 'Endtime'),
            'duration'           => Yii::t('employee', 'Duration'),
            'billableseconds'    => Yii::t('employee', 'Billableseconds'),
            'disposition'        => Yii::t('employee', 'Disposition'),
            'amaflags'           => Yii::t('employee', 'Amaflags'),
            'userfield'          => Yii::t('employee', 'Userfield'),
            'create_time'        => Yii::t('employee', 'Create Time'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoneLine()
    {
        return $this->hasOne(PhoneLine::className(), ['line_id' => 'phone_line_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    public function getClientPhone()
    {
        if ($this->type == 'outcome' || $this->type == 'notify') {
            $this->destination;
        }

        return $this->source;
    }

    public function setClient_phone($value)
    {
        if ($this->type == 'outcome' || $this->type == 'notify') {
            $this->destination = $value;
        } else {
            $this->source = $value;
        }
    }

    public function getDispatcher_phone()
    {
        if ($this->type == 'outcome' || $this->type == 'notify') {
            return $this->source;
        }

        return $this->destination;
    }

    public function setDispatcher_phone($value)
    {
        if ($this->type == 'outcome' || $this->type == 'notify') {
            $this->source = $value;
        } else {
            $this->destination = $value;
        }
    }

    public static function getCallsByPeriod($operator_id, $arPeriod)
    {
        list($first_date, $last_date) = $arPeriod;

        return self::getCallData($operator_id, $first_date, $last_date);
    }

    public static function getCallData($operator_id, $first_date, $last_date)
    {
        $operatorCalls = self::find()
            ->where(['user_opertor_id' => $operator_id])
            ->andWhere(['between', 'starttime', $first_date, $last_date])
            ->orderBy(['starttime' => SORT_DESC])
            ->all();

        $result = [];

        foreach ($operatorCalls as $call) {
            /** @var $call $this */
            $result[] = [
                'ID'         => $call->id,
                'TYPE'       => $call->type,
                'CLIENT'     => $call->getClientAsArray(),
                'DISPATCHER' => $call->getDispatcherAsArray(),
                'CALL'       => $call->getCallAsArray(),
                'STATUS'     => $call->getStatus(),
                'ORDER'      => $call->getOrderAsArray(),
            ];
        }

        return $result;
    }

    public function getStatus()
    {
        // если исходящий
        if ($this->type == 'outcome') {

            // если дозвонились
            if ($this->disposition == 'ANSWERED') {
                return self::STATUS_SUCCESS;
            }

            // если занято
            if ($this->disposition == 'BUSY') {
                return self::STATUS_BUSY;
            }

            // не зозвонились
            return self::STATUS_UNSUCCESS;
        }

        // если входящий
        if ($this->type == 'income') {

            // если приняли
            if ($this->disposition == 'ANSWERED') {
                return self::STATUS_RECEIVED;
            }

            // пропущен
            return self::STATUS_MISSED;
        }

        if ($this->type == 'notify') {
            // если дозвонились
            if ($this->disposition == 'ANSWERED') {
                return self::STATUS_SUCCESS;
            }

            // если занято
            if ($this->disposition == 'BUSY') {
                return self::STATUS_BUSY;
            }

            // не зозвонились
            return self::STATUS_UNSUCCESS;
        }

        return self::STATUS_UNKNOWN;
    }


    public function getOrderAsArray()
    {
        if (!$this->order_id) {
            return [];
        }

        $order = Order::find()
            ->where('order_id = :order_id', ['order_id' => $this->order_id])
            ->with('detailCost')
            ->one();

        if (!$order) {
            return [];
        }

        $result = [
            'NUMBER' => $order->order_number,
            'PRICE'  => [
                'VALUE'    => !isset($order->detailCost->summary_cost) ? 0 : $order->detailCost->summary_cost,
                'CURRENCY' => $order->currency_id,
            ],
        ];

        return $result;
    }

    public function getCallAsArray()
    {
        $city_id    = PhoneLine::find()
            ->select('city_id')
            ->where([
                'phone' => $this->clientPhone,
            ])
            ->scalar();
        $timeOffset = !empty($city_id) ? City::getTimeOffset($city_id) : 0;

        $startTime = strtotime($this->starttime) + $timeOffset;
        $result    = [
            'DATE'        => date('d.m.Y', $startTime),
            'START_TIME'  => date('H:i:s', $startTime),
            'ANSWER_TIME' => strtotime($this->starttime),
            'END_TIME'    => '',
            'DURATION'    => empty($this->billableseconds) ? 0 : $this->billableseconds,
        ];
        $result['DURATION_AS_TIME'] = app()->formatter->asTime($result['DURATION']);

        return $result;
    }

    public function getClientAsArray()
    {
        if ($this->type == 'outcome' || $this->type == 'notify') {
            $phone = $this->destination;
        } else {
            if ($this->type == 'income') {
                $phone = $this->source;
            } else {
                return [];
            }
        }

        $clientPhone = ClientPhone::find()
            ->where('value = :phone', ['phone' => $phone])
            ->joinWith([
                'client' => function ($query) {
                    $query->andWhere(['tenant_id' => user()->tenant_id]);
                },
            ])
            ->one();

        if (empty($clientPhone)) {
            $result = [
                'PHONE' => $phone,
            ];
        } else {
            $client = $clientPhone->client;
            $result = [
                'FULL_NAME' => $client->getFullName(),
                'ID'        => $client->client_id,
                'PHONE'     => $phone,
                'PHOTO'     => $client->isFileExists($client->photo) ? $client->getPictureHtml(Html::encode($client->photo),
                    false) : '',
            ];
        }

        return $result;
    }

    public function getDispatcherAsArray()
    {
        $result = [
            'PHONE' => $this->dispatcher_phone,
        ];

        return $result;
    }
}
