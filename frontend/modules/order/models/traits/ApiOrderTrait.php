<?php
namespace frontend\modules\order\models\traits;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property integer $order_id
 * @property integer $tenant_id
 * @property integer $worker_id
 * @property integer $city_id
 * @property integer $tariff_id
 * @property integer $client_id
 * @property string $phone
 * @property integer $user_create
 * @property integer $status_id
 * @property integer $user_modifed
 * @property integer $company_id
 * @property integer $parking_id
 * @property string $address
 * @property string $comment
 * @property string $predv_price
 * @property string $predv_distance
 * @property string $predv_time
 * @property string $device
 * @property string $order_number
 * @property string $payment
 * @property integer $show_phone
 * @property string $create_time
 * @property string $updated_time
 * @property string $order_time
 * @property string $time_to_client
 * @property integer $time_offset
 * @property integer $position_id
 * @property integer $is_fix
 * @property integer $update_time
 * @property integer $deny_refuse_order
 * @property integer $promo_code_id
 * @property float $predv_price_no_discount
 * @property integer $processed_exchange_program_id
 * @property integer $bonus_payment
 * @property integer $client_passenger_id
 * @property integer $client_passenger_phone
 */
trait ApiOrderTrait
{

    public function getCityId()
    {
        return $this->city_id;
    }

    public function getOrderNow()
    {
        return $this->order_now;
    }

    public function getOrderDate()
    {
        return $this->order_date;
    }

    public function getOrderHours()
    {
        return $this->order_hours;
    }

    public function getOrderMinutes()
    {
        return $this->order_minutes;
    }

    public function getPhone()
    {
        $this->phone = floatval( str_replace('+', "", $this->phone));
        $this->phone = floatval( str_replace('(', "", $this->phone));
        $this->phone = floatval( str_replace(')', "", $this->phone));
        $this->phone = floatval( str_replace(' ', "", $this->phone));
        $this->phone = floatval( str_replace('-', "", $this->phone));
        return $this->phone;
    }

    public function getClientId()
    {
        return $this->client_id;
    }

    public function getComment()
    {
        return $this->comment;
    }

    public function getPayment()
    {
        return $this->payment;
    }

    public function getBonusPayment()
    {
        return $this->bonus_payment;
    }

    public function getPositionId()
    {
        return $this->position_id;
    }

    public function getTariffId()
    {
        return $this->tariff_id;
    }

    public function getPredvPrice()
    {
        $this->predv_price = floatval( str_replace(',', ".", $this->predv_price));
        return $this->predv_price;
    }

    public function getPredvDistance()
    {
        return $this->predv_distance;
    }

    public function getPredvTime()
    {
        return $this->predv_time;
    }

    public function getPredvPriceNoDiscount()
    {
        return $this->predv_price_no_discount;
    }

    public function getIsFix()
    {
        return $this->is_fix;
    }

    public function getParkingId()
    {
        return $this->parking_id;
    }

    public function getCompanyId()
    {
        return $this->company_id;
    }

    public function getDevice()
    {
        return $this->device;
    }

    public function getOrderAction()
    {
        return $this->orderAction;
    }

    public function getAddressOriginal()
    {
        return $this->address;
    }

    public function getTenantId()
    {
        return user()->tenant_id;
    }

    public function getUserCreatedId()
    {
        return user()->id;
    }

    public function getUserModifedId()
    {
        return user()->id;
    }


    public function getCallId()
    {
        return $this->call_id;
    }

    public function getWorkerId()
    {
        return null;
    }

    public function getCarId()
    {
        return null;
    }

    public function getAdditionalOption()
    {
        return $this->additional_option;
    }

    public function getLang()
    {
        return \Yii::$app->language;
    }

    public function getClientName()
    {
        return $this->client_name;
    }

    public function getClientLastName()
    {
        return $this->client_lastname;
    }

    public function getClientSecondName()
    {
        return $this->client_secondname;
    }


    public function getClientPassengerPhone()
    {
        return $this->client_passenger_phone;
    }

    public function getClientPassengerId()
    {
        return $this->client_passenger_id;
    }

    public function getClientPassengerName()
    {
        return $this->client_passenger_name;
    }

    public function getClientPassengerSecondName()
    {
        return $this->client_passenger_secondname;
    }

    public function getClientPassengerLastName()
    {
        return $this->client_passenger_lastname;
    }
}
