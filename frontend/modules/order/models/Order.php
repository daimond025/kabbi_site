<?php

namespace app\modules\order\models;

use app\modules\order\models\forms\FilterMapForm;
use frontend\components\serviceEngine\ServiceEngine;
use app\modules\address\models\ClientAddressHistory;
use app\modules\client\models\ClientCompany;
use app\modules\client\models\ClientHasCompany;
use app\modules\setting\models\Tenant;
use app\modules\tariff\models\TaxiTariffHasCompany;
use common\components\behaviors\ActiveRecordBehavior;
use common\helpers\CacheHelper;
use common\modules\tenant\models\DefaultSettings;
use common\services\OrderStatusService;
use frontend\modules\companies\components\services\CompanyCheck;
use frontend\modules\employee\components\worker\WorkerShiftService;
use frontend\modules\employee\models\worker\Worker;
use frontend\modules\exchange\models\entities\ExchangeProgram;
use frontend\modules\order\components\OrderService;
use frontend\modules\order\components\services\filter\FilterOrderMapService;
use frontend\modules\order\delegations\OrderSetting;
use frontend\modules\car\models\Car;
use frontend\modules\order\models\traits\ApiOrderTrait;
use orderApi\interfaces\createOrder\ApiOrderInterface;
use phpDocumentor\Reflection\Types\This;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\behaviors\TimestampBehavior;
use common\modules\tenant\models\TenantSetting;
use common\modules\city\models\City;
use app\modules\client\models\Client;
use app\modules\client\models\ClientPhone;
use \app\modules\tariff\models\TaxiTariff;
use yii\caching\DbDependency;
use frontend\modules\car\models\CarOption;
use app\modules\tariff\models\AdditionalOption;
use yii\base\ErrorException;
use yii\helpers\ArrayHelper;
use app\modules\parking\models\Parking;
use app\modules\tenant\models\User;
use common\helpers\DateTimeHelper;
use frontend\modules\order\exceptions\InvalidOrderTimeException;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property integer $order_id
 * @property integer $tenant_id
 * @property integer $worker_id
 * @property integer $city_id
 * @property integer $tariff_id
 * @property integer $client_id
 * @property string $phone
 * @property integer $user_create
 * @property integer $status_id
 * @property integer $user_modifed
 * @property integer $company_id
 * @property integer $parking_id
 * @property string $address
 * @property string $comment
 * @property string $predv_price
 * @property string $predv_distance
 * @property string $predv_time
 * @property string $device
 * @property string $order_number
 * @property string $payment
 * @property integer $show_phone
 * @property string $create_time
 * @property string $updated_time
 * @property string $order_time
 * @property string $time_to_client
 * @property integer $time_offset
 * @property integer $position_id
 * @property integer $is_fix
 * @property integer $update_time
 * @property integer $deny_refuse_order
 * @property integer $promo_code_id
 * @property float $predv_price_no_discount
 * @property integer $processed_exchange_program_id
 * @property integer $client_passenger_id
 * @property integer $client_passenger_phone
 *
 * @property ClientReview[] $clientReviews
 * @property Exchange[] $exchanges
 * @property City $city
 * @property ClientCompany $company
 * @property Worker $worker
 * @property OrderStatus $status
 * @property Parking $parking
 * @property TaxiTariff $tariff
 * @property Tenant $tenant
 * @property User $userCreate
 * @property User $userModifed
 * @property OrderChange[] $orderChanges
 * @property OrderHasOption[] $orderHasOptions
 * @property CarOption[] $options
 * @property OrderStatusTime[] $orderStatusTimes
 * @property ParkingOrder[] $parkingOrders
 * @property Client $clientPassenger
 */
class Order extends \yii\db\ActiveRecord implements ApiOrderInterface
{

    use ApiOrderTrait;

    const DEVICE_0 = 'DISPATCHER';
    const DEVICE_WORKER = 'WORKER';
    const DEVICE_WEB = 'WEB';
    const DEVICE_CABINET = 'CABINET';
    const DEVICE_YANDEX = 'YANDEX';
    const DEVICE_HOSPITAL = 'HOSPITAL';

    const IgnoreStatusForStaff = [1, 2, 3, 108, 109];


    const PRE_ODRER_TIME = 1800;
    const PICK_UP_TIME = 300;
    const PAYMENT_CARD = 'CARD';
    const PAYMENT_PERSON = 'PERSONAL_ACCOUNT';
    const PAYMENT_CASH = 'CASH';
    const PAYMENT_CORP = 'CORP_BALANCE';
    const MAP_DATA_TYPE_ADDRESS = 'address';
    const MAP_DATA_TYPE_WORKER = 'worker';
    const MAP_DATA_TYPE_ROUTE = 'route';

    public $additional_option = [];
    public $new_add_option;
    const DEVICE_IOS = 'IOS';
    const DEVICE_ANDROID = 'ANDROID';

    /**
     * Who is calling.
     * @var string
     */
    public $caller;
    public $order_now;
    public $order_date;
    public $order_hours;
    public $order_minutes;
    public $order_seconds;
    public $status_reject;

    public $call_id;

    //Обрабаытвается ли заказ через внешний обменник
    public $isOrderProcessedThroughTheExternalExchange;
    /**
     * Активный заказ или нет.
     * @var bool
     */
    public $isActive = false;
    public $statusRedis;
    public $clientRedis;
    public $cityRedis;
    public $workerRedis;
    public $workerFromExternalExchange;
    public $carRedis;
    public $carFromExternalExchange;
    public $userCreatedRedis;

    public $client_name;
    public $client_lastname;
    public $client_secondname;

    public $hide_params_for_order;
    public $user_staff;
    public $order_company;

    /**
     * Cost data from redis
     * @var array
     */
    public $costData;
    private $pickUp;
    private $orderOffset;
    private $arOldAttributes;

    /**
     * @var string
     */
    public $hash;

    /**
     * @var float
     */
    public $summary_cost;

    /* @var string */
    public $orderAction;

    /* @var int */
    public $originally_preorder;

    public $client_passenger_name;
    public $client_passenger_secondname;
    public $client_passenger_lastname;

    /** @var OrderSetting */
    protected $settings;

    // pageSize
    public static $pageSize = 20;

    public function init()
    {
        parent::init();

        $this->settings = Yii::createObject(OrderSetting::class, [$this]);
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id', 'status_id', 'position_id'], 'required'],
            [
                [
                    'tenant_id',
                    'worker_id',
                    'city_id',
                    'tariff_id',
                    'phone',
                    'user_create',
                    'status_id',
                    'user_modifed',
                    'company_id',
                    'parking_id',
                    'order_number',
                    'show_phone',
                    'create_time',
                    'status_time',
                    'order_time',
                    'time_to_client',
                    'predv_time',
                    'client_id',
                    'bonus_payment',
                    'update_time',
                    'car_id',
                ],
                'integer',
            ],
            [['address', 'comment', 'payment', 'new_add_option'], 'string'],
            [
                [
                    'show_phone',
                    'additional_option',
                    'order_now',
                    'order_date',
                    'order_hours',
                    'order_minutes',
                    'order_seconds',
                    'status_reject',
                ],
                'safe',
            ],
            [['predv_price', 'predv_distance', 'summary_cost', 'predv_price_no_discount'], 'number'],
            [['device'], 'string', 'max' => 10],
            ['hash', 'safe'],
            [['is_fix', 'originally_preorder'], 'in', 'range' => [0, 1]],
            ['is_fix', 'default', 'value' => '0'],
            ['company_id', 'exist', 'targetClass' => ClientCompany::className(), 'filter' => ['block' => '0']],
            [
                'company_id',
                'exist',
                'targetAttribute' => ['client_id' => 'client_id', 'company_id'],
                'targetClass' => ClientHasCompany::className(),
            ],
            [
                'company_id',
                'exist',
                'targetAttribute' => ['tariff_id' => 'tariff_id', 'company_id'],
                'targetClass' => TaxiTariffHasCompany::className(),
            ],
            [
                'tariff_id',
                'exist',
                'targetAttribute' => ['tenant_id' => 'tenant_id', 'tariff_id'],
                'targetClass' => TaxiTariff::className(),
            ],
            ['deny_refuse_order', 'integer'],
            ['deny_refuse_order', 'default', 'value' => 0],
            ['orderAction', 'required'],
            [['client_passenger_phone', 'client_passenger_id'], 'integer'],
            [
                [
                    'client_passenger_name',
                    'client_passenger_lastname',
                    'client_passenger_secondname',
                    'client_name',
                    'client_lastname',
                    'client_secondname',
                ],
                'string',
                'max' => 45,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => Yii::t('order', 'Order ID'),
            'tenant_id' => Yii::t('order', 'Tenant ID'),
            'worker_id' => Yii::t('order', 'Worker'),
            'city_id' => Yii::t('order', 'Branch'),
            'tariff_id' => Yii::t('order', 'Tariff'),
            'client_id' => Yii::t('order', 'Client ID'),
            'user_create' => Yii::t('order', 'User Create'),
            'user_modifed' => Yii::t('order', 'User modifed'),
            'status_id' => Yii::t('order', 'Status'),
            'address' => Yii::t('order', 'Address'),
            'comment' => Yii::t('order', 'Comment'),
            'predv_price' => Yii::t('order', 'Price'),
            'predv_distance' => Yii::t('order', 'Preliminary distance'),
            'predv_time' => Yii::t('order', 'Preliminary time'),
            'create_time' => Yii::t('order', 'Create Time'),
            'device' => Yii::t('order', 'Device'),
            'payment' => Yii::t('order', 'Payment'),
            'show_phone' => Yii::t('order', 'Show client phone to worker'),
            'parking_id' => t('parking', 'Parking'),
            'preliminary_calc' => t('parking', 'Preliminary calculation'),
            'additional_option' => t('order', 'Additional options'),
            'order_time' => t('order', 'Order time'),
            'status_time' => t('order', 'Status time'),
            'phone' => t('order', 'Client phone'),
            'bonus_payment' => t('order', 'Bonus payment'),
            'position_id' => t('employee', 'Profession'),
            'summary_cost' => t('order', 'Summary cost'),
            'update_time' => t('order', 'Update time'),
            'deny_refuse_order' => t('order', 'Without the possibility of failure'),
            'orderAction' => t('order', 'Action with the order'),
            'car_id' => t('order', 'Car Id'),
            'company_id' => Yii::t('order', 'Company ID'),
            'is_fix' => Yii::t('order', 'offer my price'),
            'processed_exchange_program_id' => Yii::t('order', 'Exchange program id'),
        ];
    }

    public function getExchangeProgram()
    {
        return $this->hasOne(ExchangeProgram::className(), ['exchange_program_id' => 'processed_exchange_program_id']);
    }


    public function getExchangeWorker()
    {
        return $this->hasOne(OrderHasExchangeWorker::className(), ['order_id' => 'order_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(\app\modules\client\models\ClientCompany::className(), ['company_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientReview()
    {
        return $this->hasOne(\app\modules\client\models\ClientReview::className(), ['client_id' => 'client_id', 'order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExchanges()
    {
        return $this->hasMany(Exchange::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParking()
    {
        return $this->hasOne(\app\modules\parking\models\Parking::className(), ['parking_id' => 'parking_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['status_id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(TaxiTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(\common\modules\tenant\models\Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreated()
    {
        return $this->hasOne(\app\modules\tenant\models\User::className(), ['user_id' => 'user_create']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserModifed()
    {
        return $this->hasOne(\app\modules\tenant\models\User::className(), ['user_id' => 'user_modifed']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderChanges()
    {
        return $this->hasMany(OrderChangeData::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderHasOptions()
    {
        return $this->hasMany(OrderHasOption::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptions()
    {
        return $this->hasMany(CarOption::className(), ['option_id' => 'option_id'])->viaTable(
            '{{%order_has_option}}',
            ['order_id' => 'order_id']
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::className(), ['car_id' => 'car_id']);
    }

    public function getDetailCost()
    {
        return $this->hasOne(OrderDetailCost::className(), ['order_id' => 'order_id'])
            ->orderBy(['detail_id' => SORT_DESC]);
    }

    public function getClientPassenger()
    {
        return $this->hasOne(Client::className(), ['client_id' => 'client_passenger_id']);
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_time',
                'updatedAtAttribute' => 'update_time',
            ],
            [
                'class' => ActiveRecordBehavior::className(),
            ],
        ];
    }

    /**
     * Getting local time.
     *
     * @param integer $time
     *
     * @return integer
     */
    public function getOrderTimestamp($time = null)
    {
        if(is_null($time)){
            $time = time();
        }

        $offset = $this->getOrderOffset();

        return $offset + $time;
    }

    /**
     * Getting local time offset.
     * @return integer
     */
    public function getOrderOffset()
    {
        return City::getTimeOffset($this->city_id);
    }

    public function getFormData($city_id = null, $status_id = null)
    {
        $arData['CITY_LIST'] = user()->getUserCityList();
        $arData['STATUS'] = !empty($status_id) ? translateAssocArray(
            'status_event',
            OrderStatus::getDispatcherStatusList($status_id),
            true,
            'name'
        ) : null;

        if($this->isNewRecord){
            if(!empty($city_id)){
                $curCityId = $city_id;
            }else{
                $arCurCity = array_slice($arData['CITY_LIST'], 0, 1, true);
                $curCityId = key($arCurCity);
            }
        }else{
            $curCityId = $this->city_id;
        }

        $arData['CUR_CITY_COORDS'] = City::find()->where(['city_id' => $curCityId])->select([
            'lat',
            'lon',
        ])->asArray()->one();

        //        $tariffListData   = TaxiTariff::getTenantTariffList($curCityId, ['operator']);
        //        $arData['TARIFF'] = ArrayHelper::map($tariffListData, 'tariff_id', 'name');

        $arData['ADD_OPTIONS'] = [];
        if(!empty($this->tariff_id)){
            $arData['ADD_OPTIONS'] = AdditionalOption::find()
                ->select(['tariff_id', 'additional_option_id'])
                ->where(['tariff_id' => $this->tariff_id])
                ->with('option')
                ->asArray()
                ->all();
        }

        $arData['MAX_NUMBER_OF_ADDRESS_POINTS'] = DefaultSettings::getValue(
            DefaultSettings::SETTING_MAX_NUMBER_OF_ADDRESS_POINTS,
            0
        );

        return $arData;
    }

    public static function getPaymentVariants()
    {
        return [
            self::PAYMENT_CASH => t('order', 'Cash'),
            self::PAYMENT_CARD => t('order', 'Bank card'),
            self::PAYMENT_PERSON => t('order', 'Personal account'),
            self::PAYMENT_CORP => t('order', 'Corporate balance'),
        ];
    }

    /**
     * Order payment for select in order card.
     * @return array
     */
    public static function getPaymentList($showCardPayment = false)
    {
        $payments = [
            self::PAYMENT_CASH => t('order', 'Cash'),
            self::PAYMENT_PERSON => t('order', 'Personal account'),
        ];
        if($showCardPayment){
            $payments[self::PAYMENT_CARD] = t('order', 'Bank card');
        }

        return $payments;
    }

    public function getPaymentNameByKey($key)
    {
        $paymentList = self::getPaymentList(true);

        if($key === self::PAYMENT_CORP){
            $paymentName = t('order', 'Balance{name}', ['name' => " ({$this->company->name})"]);
        }elseif(isset($paymentList[$key])){
            $paymentName = $paymentList[$key];
        }else{
            $paymentName = '';
        }

        return $paymentName;
    }

    public function getPaymentName()
    {
        return $this->getPaymentNameByKey($this->payment);
    }

    public function getPaymentHtmlSelect(
        $companies = [],
        $readOnly = false,
        $showCardPayment = false,
        $onlyCompanies = false
    )
    {
        $items = [];
        $options = [];
        $placeholder = null;

        if(!$this->isNewRecord && $this->payment == self::PAYMENT_CORP){
            $this->payment = self::PAYMENT_CORP.'_'.$this->company_id;
        }

        if(!$onlyCompanies){
            $paymentList = $this->getPaymentList($showCardPayment);
            foreach($paymentList as $key => $payment){
                $items[$key] = $payment;
                if(is_null($placeholder)){
                    $placeholder = $payment;
                }
            }
        }else{
            $placeholder = t('app', 'Not selected');
        }
        if(is_array($companies)){
            foreach($companies as $company){
                if(is_null($placeholder)){
                    $placeholder = Html::encode($company->name);
                }
                $key = self::PAYMENT_CORP.'_'.$company->company_id;
                $items[$key] = $company->name;
                $options[$key] = ['data-company' => $company->company_id];
            }
        }

        if($readOnly == true){
            return isset($items[$this->payment]) ? $items[$this->payment] : null;
        }

        return Html::activeDropDownList($this, 'payment', $items, [
            'id' => 'order_payment',
            'data-placeholder' => $placeholder,
            'class' => 'default_select',
            'data-show-card-payment' => (int)$showCardPayment,
            'options' => $options,
        ]);
    }

    public function isNeedCheckOrderTime()
    {
        return in_array($this->status->status_group, [
                OrderStatus::STATUS_GROUP_0,
                OrderStatus::STATUS_GROUP_6,
            ], false)
            && $this->status_id != OrderStatus::STATUS_OVERDUE
            && $this->status_id != OrderStatus::STATUS_MANUAL_MODE;
    }

    /**
     * Update order status
     * @throws InvalidOrderTimeException
     */
    protected function updateOrderStatus()
    {
        if($this->status_id == OrderStatus::STATUS_MANUAL_MODE){
            return;
        }elseif(in_array(
            $this->status_id,
            [OrderStatus::STATUS_PRE_GET_WORKER, OrderStatus::STATUS_PRE_REFUSE_WORKER]
        )){
            return;
        }elseif($this->isPreOrder()){
            if($this->status->status_group != OrderStatus::STATUS_GROUP_6){
                $this->status_id = empty($this->parking_id)
                    ? OrderStatus::STATUS_PRE_NOPARKING : OrderStatus::STATUS_PRE;
            }
        }elseif($this->status->status_group == OrderStatus::STATUS_GROUP_6){
            $this->status_id = empty($this->parking_id)
                ? OrderStatus::STATUS_NOPARKING : OrderStatus::STATUS_NEW;
            $this->order_time = $this->getOrderTime();
        }
    }

    /**
     * Determine parking and address coordinates
     * return array
     */
    protected function determineParkingAndAddressCoords($address)
    {
        $result = [];
        foreach($address as $key => $value){
            if(empty($value['lat']) || empty($value['lon'])){
                $coords = app()->geocoder->findCoordsByAddress(implode(', ', [
                    $value['city'],
                    $value['street'],
                    $value['house'],
                ]));
                $value['lat'] = $coords['lat'];
                $value['lon'] = $coords['lon'];
            }

            if(empty($value['parking_id'])){
                $parkingInfo = app()->routeAnalyzer->getParkingByCoords(
                    $this->tenant_id,
                    $this->city_id,
                    $value['lat'],
                    $value['lon'],
                    false
                );

                if(empty($parkingInfo['error'])){
                    $value['parking_id'] = (int)$parkingInfo['inside'];
                    $value['parking'] = Parking::find()
                        ->where(['parking_id' => $value['parking_id']])
                        ->select('name')
                        ->scalar();

                    if($key == 'A'){
                        $this->parking_id = $value['parking_id'];
                    }
                }
            }elseif($key == 'A'){
                $this->parking_id = $value['parking_id'];
            }
            $result[$key] = $value;
        }

        return $result;
    }


    public function isCorpBalance()
    {
        return strpos($this->payment, self::PAYMENT_CORP) !== false;
    }


    public function saveAddressToHistory()
    {
        $address_array = unserialize($this->address);

        foreach($address_array as $address){
            $this->updateOrCreateClientAddressHistoryModel($address);
        }
    }

    public function updateOrCreateClientAddressHistoryModel($address)
    {
        $model = ClientAddressHistory::findOne([
            'client_id' => $this->client_id,
            'city_id' => $this->city_id,
            'city' => $address['city'],
            'street' => $address['street'],
            'house' => $address['house'],
            'lat' => $address['lat'],
            'lon' => $address['lon'],
        ]);

        if($model){
            return $model->updateTime();
        }

        $model = new ClientAddressHistory();
        $model->attributes = [
            'client_id' => $this->client_id,
            'city_id' => $this->city_id,
            'city' => $address['city'],
            'street' => $address['street'],
            'house' => $address['house'],
            'lat' => $address['lat'],
            'lon' => $address['lon'],
        ];

        return $model->save();
    }

    public function getOrderCostData()
    {
        $taxiRouteAnalyzer = app()->routeAnalyzer;
        $routeCostInfo = $taxiRouteAnalyzer->analyzeRoute(
            $this->tenant_id,
            $this->city_id,
            unserialize($this->address),
            $this->additional_option,
            $this->tariff_id,
            date('d.m.Y H:i:s', $this->order_time),
            $this->client_id
        );

        if(empty($routeCostInfo)){
            throw new ErrorException('No $routeCostInfo in Order::getOrderCostData()');
        }

        $costData = [];
        $result = (array)$routeCostInfo;

        if(!isset($result['tariffInfo']) || empty($result['tariffInfo'])){
            throw new ErrorException('No tariffInfo in Order::getOrderCostData()');
        }

        $costData['additionals_cost'] = isset($result['additionalCost']) ? (string)$result['additionalCost'] : null;
        $costData['summary_time'] = isset($result['summaryTime']) ? (string)$result['summaryTime'] : null;
        $costData['summary_distance'] = isset($result['summaryDistance']) ? (string)$result['summaryDistance'] : null;
        $costData['city_time'] = isset($result['cityTime']) ? (string)$result['cityTime'] : null;
        $costData['city_distance'] = isset($result['cityDistance']) ? (string)$result['cityDistance'] : null;
        $costData['city_cost'] = isset($result['cityCost']) ? (string)$result['cityCost'] : null;
        $costData['out_city_time'] = isset($result['outCityTime']) ? (string)$result['outCityTime'] : null;
        $costData['out_city_distance'] = isset($result['outCityDistance']) ? (string)$result['outCityDistance'] : null;
        $costData['out_city_cost'] = isset($result['outCityCost']) ? (string)$result['outCityCost'] : null;

        if($this->is_fix == 1){
            $costData['summary_cost'] = $this->predv_price;
            $costData['is_fix'] = $this->is_fix;
        }else{
            $costData['is_fix'] = empty($result['isFix']) ? 0 : 1;
            $costData['summary_cost'] = isset($result['summaryCost']) ? (string)$result['summaryCost'] : null;
        }

        $costData['summary_cost_no_discount'] = isset($result['summaryCostNoDiscount']) ? (string)$result['summaryCostNoDiscount'] : null;
        $costData['tariffInfo'] = $result['tariffInfo'];
        $costData['start_point_location'] = isset($result['startPointLocation']) ? (string)$result['startPointLocation'] : null;

        return $costData;
    }

    public function afterFind()
    {
        parent::afterFind();

        $this->address = unserialize($this->address);
        if($this->address === false){
            $this->address = [
                'A' => [
                    'city' => '',
                    'city_id' => '',
                    'street' => '',
                    'house' => '',
                    'housing' => '',
                    'porch' => '',
                    'apt' => '',
                    'parking_id' => '',
                    'lat' => '',
                    'lon' => '',
                ],
            ];
        }
    }

    /**
     * Find client by phone number and create new client if not found
     *
     * @param string $phone
     * @param int $tenantId
     * @param int $cityId
     *
     * @return Client
     */
    public function findClientByPhoneAndCreateIfNotExists($phone, $tenantId, $cityId)
    {
        $client = Client::find()
            ->joinWith('clientPhones', false)
            ->byPhone($phone)
            ->byTenantId($tenantId)
            ->one();

        if(empty($client)){
            $transaction = app()->db->beginTransaction();
            try{
                $client = new Client();
                $client->tenant_id = $tenantId;
                $client->city_id = $cityId;
                $client->phone[] = $phone;
                $client->save();

                $clientPhone = new ClientPhone();
                $clientPhone->client_id = $client->client_id;
                $clientPhone->value = $phone;

                if($clientPhone->save()){
                    $transaction->commit();
                }else{
                    $transaction->rollBack();

                    return false;
                }
            }catch(\Exception $exc){
                $transaction->rollback();

                return false;
            }
        }

        return $client;
    }

    /**
     * Add new client to base.
     *
     * @param string $phone
     *
     * @return boolean|integer
     */
    public function addNewClient($phone)
    {
        $client = new Client();
        $client->city_id = $this->city_id;
        $res = $client->addNewClientByPhone($phone);

        if($res === false){
            \Yii::error('Ошибка добавления нового клиента по телефону', 'order');

            return false;
        }

        return $res['CLIENT_ID'];
    }

    public function isPreOrder()
    {
        $pre_order_time = TenantSetting::getSettingValue(
            user()->tenant_id,
            'PRE_ORDER',
            $this->city_id,
            $this->position_id
        );
        $pre_order_time = $pre_order_time * 60;

        if(empty($pre_order_time)){
            $pre_order_time = self::PRE_ODRER_TIME;
        }

        $now = $this->getOrderTimestamp();

        return $this->getOrderTime() - $now >= $pre_order_time ? true : false;
    }

    /**
     * Getting last order number.
     * @return integer
     */
    public function getLastOrderNumber()
    {
        $connection = app()->db;
        $command = $connection->createCommand('SELECT MAX(order_number) FROM '.$this->tableName().'WHERE `tenant_id`='.user()->tenant_id);

        return $command->queryScalar();
    }

    public function addressFilter(array $address)
    {
        $index = 0;
        $filteredAddress = [];
        $currentKey = 'A';

        $maxNumberOfAddressPoints = DefaultSettings::getValue(
            DefaultSettings::SETTING_MAX_NUMBER_OF_ADDRESS_POINTS,
            0
        );

        foreach($address as $key => $value){
            if($index > $maxNumberOfAddressPoints - 1){
                break;
            }

            if($value['lat'] && $value['lon']){
                $filteredAddress[$currentKey] = $address[$key];
                $currentKey++;
                $index++;
            }
        }

        return $filteredAddress;
    }

    public static function compareAddress($newAddress, $oldAddress){
        if(count($newAddress) != count($oldAddress) ){
            return true;
        }

        $compare = false;
        foreach ($newAddress as $key => $item){
            $newItem = $item;
            $newPlaceId= ArrayHelper::getValue($newItem, 'placeId', '');
            $newLat = ArrayHelper::getValue($newItem, 'lat');
            $newLon = ArrayHelper::getValue($newItem, 'lon');

            if(!isset($oldAddress[$key]) || is_null($newLat) || is_null($newLon) ){
                $compare  = true;
                break;
            }else{
                $oldItem = $oldAddress[$key];
                $oldPlaceId = ArrayHelper::getValue($oldItem, 'placeId', '');
                $oldLat = ArrayHelper::getValue($oldItem, 'lat');
                $oldLon = ArrayHelper::getValue($oldItem, 'lon');

                if(( ($oldPlaceId != $newPlaceId) && !empty($newPlaceId))
                    || ($newLat != $oldLat)
                    || ($newLon != $oldLon)){
                    $compare  = true;
                    break;
                }
            }
        }

        return $compare;
    }

    /**
     * Getting orders.
     *
     * @param string|array $status_group
     * @param integer $city_id
     * @param integer $date 'd.m.Y'
     * @param array $arSort ['sort' => 'asc', 'order' => 'status_id']
     * @param array $filterStatus [1, 2, 3]
     * @param array $arOrderExclusion [1, 2, 3]
     *
     * @return array
     */
    public static function getByStatusGroup(
        $status_group,
        $city_id = null,
        $date = null,
        $arSort = [],
        $filterStatus = [],
        $arOrderExclusion = []
    )
    {
        $cur_date = date('d.m.Y');

        //Массив групп статусов для обновления счетчиков
        $arUpdateCount = OrderStatus::getGroupsForCalculating();

        //Обнуление счетчика
        if((is_null($date) || $date == $cur_date) && in_array($status_group, $arUpdateCount)){
            $arFilter = is_null($city_id) ? ['user_id' => user()->id] : [
                'user_id' => user()->id,
                'city_id' => $city_id,
            ];

            OrderViews::updateAll([$status_group => 0], $arFilter);
        }
        //--------------------------------------------------------------------------------------

        if(empty($city_id)){
            $city_id = array_keys(user()->getUserCityList());
        }else{
            $cityTimeoffset = City::getTimeOffset($city_id);
        }

        if($status_group == OrderStatus::STATUS_GROUP_8){
            $status_group = OrderStatus::getSubWorkGroups();
        }

        $query = self::find();
        $query->where([
            'tenant_id' => user()->tenant_id,
        ]);

        $query->andFilterwhere(['not in', 'order_id', $arOrderExclusion]);


        //Выборка по дате
        if(is_null($date)){
            $date = $cur_date;
        }

        list($day, $month, $year) = explode('.', $date);
        $first_time = mktime(0, 0, 0, $month, $day, $year);
        $second_time = mktime(23, 59, 59, $month, $day, $year);

        if($status_group == OrderStatus::STATUS_GROUP_6){
            $query->andWhere("((create_time >= $first_time AND create_time <= $second_time) OR (status_time >= $first_time AND status_time <= $second_time))");
        }else{
            if(isset($cityTimeoffset)){
                $query->andWhere("status_time >= $first_time - $cityTimeoffset AND status_time <= $second_time - $cityTimeoffset");
            }else{
                $query->andWhere("status_time >= $first_time AND status_time <= $second_time");
            }
        }
        //--------------------------------------------------------------------------------

        $query->with([
            'userCreated' => function($sub_query){
                $sub_query->select(['user_id', 'last_name', 'name']);
            },
            'client' => function($sub_query){
                $sub_query->select(['client_id', 'last_name', 'name']);
            },
            'status',
            'worker' => function($sub_query){
                $sub_query->select(['worker_id', 'last_name', 'name', 'callsign', 'phone']);
            },
            'car' => function($sub_query){
                $sub_query->select(['car_id', 'name', 'gos_number', 'color']);
            },
            'tariff' => function($sub_query){
                $sub_query->select(['tariff_id', 'name']);
            },
            'options' => function($sub_query){
                $sub_query->select(['option_id', 'name']);
            },
            'exchangeProgram',
            'exchangeWorker',
        ]);

        if($status_group === OrderStatus::STATUS_GROUP_7){
            $warning_statuses = !empty($filterStatus) ? $filterStatus : OrderStatus::getWarningStatusId();
            $query->andWhere(['status_id' => $warning_statuses]);
        }else{
            $query->joinWith([
                'status' => function($sub_query) use ($status_group, $filterStatus){
                    $sub_query->andWhere(['status_group' => $status_group]);

                    if(!empty($filterStatus)){
                        $sub_query->andWhere([self::tableName().'.status_id' => $filterStatus]);
                    }
                },
            ], false);
        }

        $query->andFilterWhere(['city_id' => $city_id]);

        //Сортировка
        if(!isset($arSort['sort'])){
            $sort = SORT_DESC;
        }else{
            $sort = ($arSort['sort'] === SORT_DESC || $arSort['sort'] === 'desc') ? SORT_DESC : SORT_ASC;
        }

        $order = isset($arSort['order']) ? $arSort['order'] : 'order_id';
        $orderBy = [$order => $sort];

        $query->orderBy($orderBy);

        if(app()->user->can(User::ROLE_STAFF_COMPANY)){
            $query->andFilterWhere(['tenant_company_id' => user()->tenant_company_id]);
        }

        return $query->all();
    }

    /**
     * Getting query orders.
     *
     * @param string|array $status_group
     * @param integer $city_id
     * @param array $date
     * @param array $filterStatus [1, 2, 3]
     *
     * @retur
     * */
    public static function getByStatusGroupQuery(
        $status_group,
        $city_id = null,
        $date = null,
        $filterStatus = []
    )
    {

        if(empty($city_id)){
            $city_id = array_keys(user()->getUserCityList());
        }else{
            $cityTimeoffset = City::getTimeOffset($city_id);
        }

        if($status_group == OrderStatus::STATUS_GROUP_8){
            $status_group = OrderStatus::getSubWorkGroups();
        }

        $query = self::find();
        $query->where([
            'tenant_id' => user()->tenant_id,
        ]);

        if(!isset($date['data_no']) || !$date['data_no']){

            $date_from = isset($date['date_from']) ? $date['date_from'] : null;
            $date_to = isset($date['date_to']) ? $date['date_to'] : null;

            list($first_time, $second_time) = self::getOrderDateRange($date_from, $date_to);

            if(isset($cityTimeoffset)){
                $query->andWhere("order_time >= $first_time - $cityTimeoffset AND order_time <= $second_time - $cityTimeoffset");
            }else{
                $query->andWhere("order_time >= $first_time AND order_time <= $second_time");
            }
        }

        // по типу устройства больничный / простой
        if(isset($date['device'])){
            $query->andWhere(["device" => $date['device']]);
        }
        if(isset($date['device_not'])){
            $query->andWhere(['<>', 'device', $date['device_not']]);
        }

        $query->with([
            'userCreated' => function($sub_query){
                $sub_query->select(['user_id', 'last_name', 'name']);
            },
            'client' => function($sub_query){
                $sub_query->select(['client_id', 'last_name', 'name']);
            },
            'status',
            'worker' => function($sub_query){
                $sub_query->select(['worker_id', 'last_name', 'name', 'callsign', 'phone']);
            },
            'car' => function($sub_query){
                $sub_query->select(['car_id', 'name', 'gos_number', 'color']);
            },
            'tariff' => function($sub_query){
                $sub_query->select(['tariff_id', 'name']);
            },
            'options' => function($sub_query){
                $sub_query->select(['option_id', 'name']);
            },
            'exchangeProgram',
            'exchangeWorker',
        ]);


        if($status_group === OrderStatus::STATUS_GROUP_7){
            $warning_statuses = !empty($filterStatus) ? $filterStatus : OrderStatus::getWarningStatusId();
            $query->andWhere(['in', 'status_id', $warning_statuses]);

            if(!empty($filterStatus)){
                $query->andWhere([self::tableName().'.status_id' => $filterStatus]);
            }
        }elseif($status_group === OrderStatus::STATUS_GROUP_8){
            $query->andWhere(['in', self::tableName().'status_id', OrderStatus::getWorksStatusId()]);
        }else{
            $query->joinWith([
                'status' => function($sub_query) use ($status_group, $filterStatus){
                    $sub_query->andWhere(['status_group' => $status_group]);

                    if(!empty($filterStatus)){
                        $sub_query->andWhere([self::tableName().'.status_id' => $filterStatus]);
                    }
                },
            ], false);
        }


        $query->andFilterWhere(['city_id' => $city_id]);

        if(app()->user->can(User::ROLE_STAFF_COMPANY)){

            $query->andWhere(['or',
                ['company_id' => user()->tenant_company_id],
                ['company_id' => null],
            ]);

            if($status_group == OrderStatus::STATUS_GROUP_0){
                $query->andWhere(['not in', self::tableName().'.status_id', self::IgnoreStatusForStaff]);
            }
        }

        $pagePer = null;
        if($date['all_data']){
            $count_all = count($query->all());
            if($count_all > 0){
                $pagePer = $count_all;
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => (is_null($pagePer)) ? self::$pageSize : $pagePer,
            ],
            'sort' => [
                'attributes' => [
                    'order_number' => [
                        'asc' => ['order_number' => SORT_ASC],
                        'desc' => ['order_number' => SORT_DESC],
                        'default' => SORT_ASC
                    ],
                    'order_time' => [
                        'asc' => ['order_time' => SORT_ASC],
                        'desc' => ['order_time' => SORT_DESC],
                        'default' => SORT_ASC,
                    ],
                ],
            ],
        ]);

        return $dataProvider;
    }

    /**
     * Getting order online for tub
     *
     * @param integer $Status Order number.
     * @param array $data
     *
     * @return object app\modules\order\models\Order
     */
    public static function getCountOrder($status_group, $data = [])
    {
        $query = self::find();
        $query->where([
            'tenant_id' => user()->tenant_id,
        ]);
        if($status_group === OrderStatus::STATUS_GROUP_7){
            $warning_statuses = !empty($filterStatus) ? $filterStatus : OrderStatus::getWarningStatusId();
            $query->andWhere(['in', 'tbl_order.status_id', $warning_statuses]);
        }elseif($status_group === OrderStatus::STATUS_GROUP_8){
            $query->andWhere(['in', 'tbl_order.status_id', OrderStatus::getWorksStatusId()]);
        }else{
            $query->joinWith([
                'status' => function($sub_query) use ($status_group){
                    $sub_query->andWhere(['status_group' => $status_group]);
                },
            ], false);
        }

        // отсечка только на заказы на текущи момент
        list($first_time, $second_time) = self::getOrderDateRange(null, null);
        $query->andWhere("order_time >= $first_time");

        // по типу устройства больничный / простой
        if(isset($data['device'])){
            $query->andWhere(["device" => $data['device']]);
        }
        if(isset($data['device_not'])){
            $query->andWhere(['<>', 'device', $data['device_not']]);
        }

        if(app()->user->can(User::ROLE_STAFF_COMPANY)){
            $query->andWhere(['or',
                ['company_id' => user()->tenant_company_id],
                ['company_id' => null],
            ]);


            $query->andWhere(['in', 'city_id', array_keys(user()->getUserCityList())]);

            if($status_group == OrderStatus::STATUS_GROUP_0){
                $query->andWhere(['not in', 'tbl_order.status_id', self::IgnoreStatusForStaff]);
            }
        }

        return $query->count();

    }


    /**
     * Getting order date range search.
     *
     * @param integer $date_from Order number.
     * @param integer $date_to Order number.
     *
     * @return array
     */
    public static function getOrderDateRange($date_from, $date_to)
    {
        $first_time = null;
        $second_time = null;

        $cur_date = date('d.m.Y');
        list($curDay, $curMonth, $curYear) = explode('.', $cur_date);
        if(is_null($date_from) && is_null($date_to)){
            $first_time = mktime(0, 0, 0, $curMonth, $curDay, $curYear);
            $second_time = mktime(23, 59, 59, $curMonth, $curDay, $curYear);
            return self::compareData($first_time, $second_time);
        }

        if(!is_null($date_from)){
            list($day, $month, $year) = explode('.', $date_from);
            $first_time = mktime(0, 0, 0, $month, $day, $year);
            $second_time = mktime(23, 59, 59, $month, $day, $year);
        }
        if(!is_null($date_to)){
            list($day, $month, $year) = explode('.', $date_to);

            if(is_null($first_time)){
                $first_time = mktime(0, 0, 0, $curMonth, $curDay, $curYear);
            }
            $second_time = mktime(23, 59, 59, $month, $day, $year);
        }

        return self::compareData($first_time, $second_time);
    }

    /**
     * Getting order date range search.
     *
     * @param integer $first_time Order number.
     * @param integer $second_time Order number.
     *
     * @return array $datarange
     */
    public static function compareData($first_time, $second_time)
    {
        if((int)$first_time > (int)$second_time){
            return [$second_time, $first_time];
        }else{
            return [$first_time, $second_time];
        }

    }


    public static function getOrderAddressArr($addresses)
    {
        $addressStr = [];
        foreach($addresses as $key => $value){
            $direction = [];
            $direction[] = $value['city'];
            $direction[] = empty($value['house'])
                ? $value['street'] : $value['street'].' '.$value['house'];
            $direction[] = empty($value['housing'])
                ? null : t('order', 'b. ').$value['housing'];

            $addressStr[$key] = implode(', ', array_filter($direction, function($item){
                return !empty(trim($item));
            }));
        }
        return $addressStr;

    }


    /**
     * Getting order info.
     *
     * @param integer $order_number Order number.
     *
     * @return object app\modules\order\models\Order
     */
    public static function getOrderInfo($order_number)
    {
        $query = self::find()->where(['tenant_id' => user()->tenant_id]);
        $query->andWhere(['order_number' => $order_number]);
        $query->with([
            'client' => function($query){
                $query->select([
                    'client_id',
                    'last_name',
                    'name',
                    'second_name',
                    'black_list',
                    'success_order',
                    'fail_worker_order',
                    'fail_client_order',
                    'fail_dispatcher_order',
                    'tenant_id',
                ]);
                $query->with([
                    'companies' => function($sub_query){
                        $sub_query->select(['company_id', 'name']);
                    },
                ]);
            },
            'userCreated' => function($query){
                $query->select(['user_id', 'last_name', 'name', 'second_name']);
            },
            'options',
            'city' => function($query){
                $query->select(['city_id', 'republic_id']);
            },
            'city.republic' => function($query){
                $query->select(['republic_id', 'timezone']);
            },
            'worker',
            'exchangeWorker',
        ]);

        return $query->one();
    }

    /**
     * Getting list of parking from all address point.
     * @return array
     */
    public function getAddressParklingList()
    {
        $parkings = \app\modules\parking\models\Parking::find()
            ->where(['tenant_id' => user()->tenant_id])
            ->andWhere(['city_id' => $this->city_id])
            ->select(['parking_id', 'name', 'city_id'])
            ->orderBy('sort DESC')
            ->asArray()
            ->all();

        return ArrayHelper::map($parkings, 'parking_id', 'name');
    }

    /**
     * Getting prepared order time
     *
     * @return int|null
     */
    public function getPreparedOrderTime()
    {
        if(empty($this->order_now)){
            list($year, $month, $day) = explode('-', $this->order_date);

            return mktime($this->order_hours, $this->order_minutes, $this->order_seconds, $month, $day, $year);
        }else{
            return null;
        }
    }

    public function getOrderTime()
    {
        if(empty($this->order_now) && !empty($this->order_date)){
            list($year, $month, $day) = explode('-', $this->order_date);

            return mktime($this->order_hours, $this->order_minutes, $this->order_seconds, $month, $day, $year);
        }elseif(!empty($this->order_time)){
            $order_time = date('Y-m-d-H-i-s', $this->order_time);

            list($year, $month, $day, $hour, $minute, $second) = explode('-', $order_time);
            return mktime($hour, $minute, $second, $month, $day, $year);
        }

        if(in_array($this->status_id, OrderStatus::getStatusWithPickUpTime())){
            return $this->getOrderTimestamp() + $this->getPickUp();
        }

        return $this->getOrderTimestamp();
    }

    /**
     * Get timer data.
     *
     * @param integer $time Time from countdown.
     *
     * @return array ['MINUTES' => '', 'SECONDS' => '', 'TYPE' => '']
     */
    public function getTimerData($time)
    {
        $arTime = [
            'MINUTES' => '00',
            'SECONDS' => '00',
            'TYPE' => 'down',
        ];

        if($this->status_id == OrderStatus::STATUS_NEW ||
            $this->status_id == OrderStatus::STATUS_FREE_FOR_HOSPITAL ||
            $this->status_id == OrderStatus::STATUS_FREE_FOR_COMPANY){
            $arTime['MINUTES'] = $this->getPickUp() / 60;
        }

        $duration = $time - $this->getOrderTimestamp();

        if($duration > $arTime['MINUTES']){
            $parse_time = DateTimeHelper::parse_seconds($duration);
        }elseif($duration < 0){
            $duration = $this->getOrderTimestamp() - $time;
            $parse_time = DateTimeHelper::parse_seconds($duration);
            $arTime['TYPE'] = 'up';
        }

        if(!empty($parse_time)){
            $arTime['MINUTES'] = $parse_time['MINUTES'] + $parse_time['HOURS'] * 60;
            $arTime['SECONDS'] = $parse_time['SECONDS'];
        }

        $arTime['MINUTES'] = $arTime['MINUTES'] < 10 ? '0'.$arTime['MINUTES'] : $arTime['MINUTES'];

        return $arTime;
    }

    /**
     * Время подачи авто.
     * @return integer Time in seconds.
     */
    public function getPickUp()
    {
        if(empty($this->pickUp)){
            $this->pickUp = TenantSetting::getSettingValue(
                user()->tenant_id,
                'PICK_UP',
                $this->city_id,
                $this->position_id
            );

            if(empty($this->pickUp)){
                $this->pickUp = self::PICK_UP_TIME;
            }
        }

        return $this->pickUp;
    }

    public static function getOrderCount($group_name, $city_id)
    {
        $arResult = [
            'new' => 0,
            'works' => 0,
            'warning' => 0,
            'pre_order' => 0,
        ];

        switch($group_name){
            case OrderStatus::STATUS_GROUP_0:
                $arCountSelect = [
                    OrderStatus::STATUS_GROUP_8,
                    OrderStatus::STATUS_GROUP_7,
                    OrderStatus::STATUS_GROUP_6,
                ];
                break;
            case OrderStatus::STATUS_GROUP_8:
                $arCountSelect = [
                    OrderStatus::STATUS_GROUP_0,
                    OrderStatus::STATUS_GROUP_7,
                    OrderStatus::STATUS_GROUP_6,
                ];
                break;
            case OrderStatus::STATUS_GROUP_7:
                $arCountSelect = [
                    OrderStatus::STATUS_GROUP_0,
                    OrderStatus::STATUS_GROUP_8,
                    OrderStatus::STATUS_GROUP_6,
                ];
                break;
            case OrderStatus::STATUS_GROUP_6:
                $arCountSelect = [
                    OrderStatus::STATUS_GROUP_0,
                    OrderStatus::STATUS_GROUP_7,
                    OrderStatus::STATUS_GROUP_8,
                ];
                break;
            default:
                $arCountSelect = [
                    OrderStatus::STATUS_GROUP_0,
                    OrderStatus::STATUS_GROUP_6,
                    OrderStatus::STATUS_GROUP_7,
                    OrderStatus::STATUS_GROUP_8,
                ];
        }

        $order_views = OrderViews::find()
            ->where(['user_id' => user()->id])
            ->andFilterWhere(['city_id' => $city_id])
            ->select($arCountSelect)
            ->asArray()
            ->all();

        if(!empty($order_views)){
            if(count($order_views) > 1){
                foreach($order_views as $key => $order_view){
                    foreach($order_view as $group => $value){
                        $arResult[$group] += $value;
                    }
                }
            }else{
                $arResult = $order_views[0];
            }
        }else{
            OrderViews::batchInsert(User::getCurUserCityIds(), user()->id, user()->tenant_id);
        }

        return $arResult;
    }

    /**
     * Saving order to Redis
     *
     * @param      $orderId
     * @param      $costData
     * @param bool $isNewRecord
     *
     * @return mixed
     */
    public static function saveOrderToRedis($orderId, $costData, $isNewRecord = true)
    {
        $tariffId = isset($costData["tariffInfo"]["tariffDataCity"]["tariff_id"]) ? $costData["tariffInfo"]["tariffDataCity"]["tariff_id"] : null;
        $order = self::find()
            ->where([
                'order_id' => $orderId,
            ])
            ->with([
                'status',
                'tariff',
                'tariff.class' => function($query){
                    $query->select(['class_id', 'class']);
                },
                'client' => function($query){
                    $query->select(['client_id', 'last_name', 'name', 'second_name', 'lang', 'email', 'send_to_email']);
                },
                'userCreated' => function($query){
                    $query->select(['user_id', 'last_name', 'name', 'second_name']);
                },
                'options.additionalOptions' => function($query) use ($tariffId){
                    $query->select(['id', 'additional_option_id', 'price'])->where([
                        'tariff_id' => $tariffId,
                        'tariff_type' => "CURRENT",
                    ]);
                },
                'tenant' => function($query){
                    $query->select(['domain', 'company_name', 'contact_phone', 'contact_email', 'site']);
                },
                'worker',
            ])
            ->asArray()
            ->one();

        $order['tenant_login'] = $order['tenant']['domain'];
        $order['costData'] = $costData;

        if(!empty($order['worker'])){
            /** @var WorkerShiftService $workerShiftService */
            $workerShiftService = Yii::$app->getModule('employee')->get('workerShift');
            $workerData = $workerShiftService->getActiveWorker($order['worker']['callsign']);
            $order["car_id"] = $workerData["car"]["car_id"];
            $order["worker"] = $workerData["worker"];
            $order["car"] = $workerData["car"];
        }

        $orderSerialized = serialize($order);

        return \Yii::$app->redis_orders_active->executeCommand(
            'hset',
            [$order['tenant_id'], $orderId, $orderSerialized]
        );
    }

    /**
     * For getting active orders from redis
     *
     * @param string $status_group
     * @param array $arFilter Filter by any field ['city_id' => $city_id, 'date' => get('date')]
     * @param integer $sort
     *
     * @return Order[]
     */
    public static function getOrdersFromRedis($status_group, $arFilter = [], $sort = SORT_DESC, $attr = 'order_id')
    {
        $orders = [];
        $db_orders = \Yii::$app->redis_orders_active->executeCommand('hvals', [user()->tenant_id]);

        $tenantCompanyFilterData = (new CompanyCheck())->workWithFilterMap();
        $citiesFilterData = (new CompanyCheck())->workWithCityFilterMap();
        $statusFilterData = (new CompanyCheck())->workFilterByStatus();


        $filter = new FilterMapForm([
            'cities' => $citiesFilterData,
            'statuses' => $statusFilterData,
            'tenantCompanies' => \common\helpers\ArrayHelper::getValue($tenantCompanyFilterData, 'tenantCompanies', [])
        ]);

        $db_orders = (new FilterOrderMapService($db_orders, $filter))
            ->getFilterOrders();

        if(!isset($arFilter['city_id'])){
            $user_city_list = user()->getUserCityList();
        }

        $user_staff = false;
        if((app()->user->can(User::ROLE_STAFF) || app()->user->can(User::ROLE_STAFF_COMPANY))){
            $user_staff = true;
        }

        foreach($db_orders as $order){
            $order = unserialize($order);

            if(in_array(
                    $status_group,
                    OrderStatus::getStatusGroup($order['status_id'])
                ) ||
                (!empty($order['call_warning_id']) && $status_group == OrderStatus::STATUS_GROUP_7)
            ){
                if((!empty($user_city_list) && !isset($user_city_list[$order['city_id']]))
                    || (empty($user_city_list) && !isset($arFilter['city_id']))
                ){
                    continue;
                }

                if(!empty($arFilter) && is_array($arFilter)){
                    $next = false;
                    foreach($arFilter as $key => $value){
                        if($key === 'status'){
                            if(!in_array($order['status_id'], $value)){
                                $next = true;
                            }
                        }elseif($key === 'query'){
                            if($value !== '' && $order['order_number'] !== $value && $order['phone'] !== $value){
                                $next = true;
                            }
                        }elseif($key == 'date' && !empty($value) && date('d.m.Y') != $value){
                            if(date('d.m.Y', $order['order_time']) != $value){
                                $next = true;
                            }
                        }elseif(isset($order[$key]) && !empty($value) && $order[$key] != $value){
                            $next = true;
                        }
                    }

                    if($next){
                        continue;
                    }
                }

                $order_obj = new Order();
                $order_obj->load($order, '');


                $order_obj->order_id = $order['order_id'];
                $order_obj->order_time = $order['order_time'];
                $order_obj->client_id = $order['client_id'];
                $order_obj->phone = $order['phone'];
                $order_obj->address = unserialize($order_obj->address);
                $order_obj->statusRedis = $order['status'];
                $order_obj->costData = $order['costData'];
                $order_obj->clientRedis = $order['client'];
                $order_obj->workerRedis = $order['worker'];
                $order_obj->carRedis = $order['car'];
                $order_obj->cityRedis = $order['city'];
                $order_obj->userCreatedRedis = $order['userCreated'];
                $order_obj->isActive = true;
                $order_obj->orderOffset = $order['city']['republic']['timezone'] * 3600;
                $order_obj->hide_params_for_order = false;

                if(OrderService::isActiveOrderProcessingThroughTheExternalExchange($order['tenant_id'], $order['order_id'])){
                    $order_obj->isOrderProcessedThroughTheExternalExchange = true;
                    if(isset($order['exchange_info']['order']['worker'])){
                        $order_obj->workerFromExternalExchange = OrderService::getWorkerInfoFromExternalExchangeProcessedOrder($order['tenant_id'], $order['order_id']);
                    }
                    if(isset($order['exchange_info']['order']['car'])){
                        $order_obj->carFromExternalExchange = OrderService::getCarInfoFromExternalExchangeProcessedOrder($order['tenant_id'], $order['order_id']);
                    }
                }

                // для партнеров - они не видят больничные поездки  - если закза принадлежит не им
                if($user_staff && ($order_obj->device == Order::DEVICE_HOSPITAL) && ((int)$order_obj->company_id != (int)user()->tenant_company_id)){
                    $order_obj->address = Order::getDefaultAddress($order_obj->city_id, $order_obj->address);
                    $order_obj->hide_params_for_order = true;
                }

                $order_company = false;
                if(($order_obj->device == Order::DEVICE_HOSPITAL) && ((int)$order_obj->company_id == (int)user()->tenant_company_id) && $user_staff){
                    $order_company = true;
                }
                $order_obj->order_company = $order_company;
                $orders[] = $order_obj;
            }
        }


        $sort = $sort == SORT_DESC || $sort == 'desc' ? SORT_DESC : SORT_ASC;
        ArrayHelper::multisort($orders, $attr, $sort);

        return $orders;
    }

    /**
     * Getting active order by phone.
     *
     * @param string $phone Caller phone.
     *
     * @return \app\modules\order\models\Order
     */
    public static function getCallerOrder($phone)
    {
        $db_orders = \Yii::$app->redis_orders_active->executeCommand('hvals', [user()->tenant_id]);
        $orders = [];

        if(is_array($db_orders)){
            foreach($db_orders as $order){
                $order = unserialize($order);
                $additionalOptions = isset($order['options']) ? $order['options'] : [];

                if(!in_array($order['status']['status_group'], [
                        OrderStatus::STATUS_GROUP_COMPLETED,
                        OrderStatus::STATUS_GROUP_REJECTED,
                    ], false)
                    && (empty($phone) || $order['phone'] == $phone || $order['worker']['phone'] == $phone)
                ){
                    $order_obj = new Order();
                    $order_obj->load($order, '');
                    $order_obj->order_id = $order['order_id'];
                    $order_obj->address = unserialize($order_obj->address);
                    $order_obj->statusRedis = $order['status'];
                    $order_obj->costData = $order['costData'];
                    $order_obj->phone = $order['phone'];
                    $order_obj->caller = empty($phone) || $order['phone'] == $phone ? 'client' : 'worker';
                    $order_obj->isActive = true;
                    $order_obj->clientRedis = $order['client'];
                    $order_obj->workerRedis = $order['worker'];
                    $order_obj->carRedis = $order['car'];
                    $order_obj->cityRedis = $order['city'];
                    $order_obj->userCreatedRedis = $order['userCreated'];
                    $order_obj->additional_option = ArrayHelper::map($additionalOptions, 'option_id', 'name');

                    $orders[] = $order_obj;

                    //Водила звонит, у него может быть только один активный заказ.
                    if(!empty($phone) && $order['worker']['phone'] == $phone){
                        break;
                    }
                }
            }
        }

        return $orders;
    }

    public static function getDeviceIconCssClass($device)
    {
        switch($device){
            case 'DISPATCHER':
                $cssIcon = 'osi_dis';
                break;
            case self::DEVICE_IOS:
                $cssIcon = 'osi_ios';
                break;
            case self::DEVICE_ANDROID:
                $cssIcon = 'osi_andr';
                break;
            case 'WEB':
                $cssIcon = 'osi_web';
                break;
            case 'JAVA':
                $cssIcon = 'osi_java';
                break;
            case 'WINFONE':
                $cssIcon = 'osi_wp';
                break;
            case 'EXC':
                $cssIcon = 'osi_exc';
                break;
            case 'WORKER':
                $cssIcon = 'osi_dr';
                break;
            case 'CABINET':
                $cssIcon = 'osi_cab';
                break;
            default:
                $cssIcon = '';
        }

        return $cssIcon;
    }

    /**
     * Return waiting time, when worker wait for free.
     * @return integer Time in seconds.
     */
    public function getWorkerWaitingTime()
    {
        $waiting_time = 0;

        if(empty($this->costData)){
            $order = self::getOrderFromRedis($this->tenant_id, $this->order_id);

            if(!empty($order['costData'])){
                $this->costData = $order['costData'];
            }
        }

        $tariffAreaKey = isset($this->costData['start_point_location']) && $this->costData['start_point_location'] == 'in' ?
            'tariffDataCity' : 'tariffDataTrack';
        $tariffWaitingTimeKey = isset($this->costData['tariffInfo']['isDay']) && $this->costData['tariffInfo']['isDay'] ?
            'wait_time_day' : 'wait_time_night';

        if(isset($this->costData['tariffInfo'][$tariffAreaKey][$tariffWaitingTimeKey])){
            $waiting_time = $this->costData['tariffInfo'][$tariffAreaKey][$tariffWaitingTimeKey];
        }

        return $waiting_time * 60;
    }

    /**
     * Return active order from redis.
     *
     * @param integer $tenant_id
     * @param integer $order_id
     *
     * @return array
     */
    public static function getOrderFromRedis($tenant_id, $order_id)
    {
        $order = \Yii::$app->redis_orders_active->executeCommand('hget', [$tenant_id, $order_id]);

        return !empty($order) ? unserialize($order) : null;
    }

    public static function getAllOrdersFromRedis()
    {
        $orders = \Yii::$app->redis_orders_active->executeCommand('hvals', [user()->tenant_id]);

        $arOrders = [];
        foreach($orders as $order){
            $arOrders[] = unserialize($order);
        }

        return $arOrders;
    }

    public function getDeviceName()
    {
        switch($this->device){
            case self::DEVICE_0:
                $name = t('order', 'Dispatcher');
                break;
            case self::DEVICE_IOS:
                $name = 'IOS';
                break;
            case self::DEVICE_ANDROID:
                $name = 'Android';
                break;
            case self::DEVICE_WORKER:
                $name = t('order', 'Border');
                break;
            case self::DEVICE_CABINET:
                $name = t('order', 'Cabinet');
                break;
            case self::DEVICE_WEB:
                $name = t('order', 'Web site');
                break;
            case self::DEVICE_HOSPITAL:
                $name = t('order', 'Hospital');
                break;
            default:
                $name = '';
        }

        return $name;
    }

    /**
     * Берем неактивные заказы из кеша или его формируем.
     *
     * @param string $key Ключ кеша
     * @param string $statuses Массив статусов группы
     * @param callable $callback Функция, возвращающая данные для кеша
     * @param integer $duration Продолжительность кеша
     * @param integer $city_id
     *
     * @return array app\modules\order\models\Order
     */
    public static function getTodayNoActiveOrdersFromCache(
        $key,
        $statuses,
        $callback,
        $duration = 3600,
        $city_id = null
    )
    {
        $sql = 'SELECT MAX(status_time) FROM '.self::tableName().' WHERE status_id IN('.$statuses.') AND tenant_id='.user()->tenant_id;

        if(!empty($city_id)){
            $sql .= ' AND city_id='.$city_id;
        }

        if(app()->user->can(User::ROLE_STAFF_COMPANY)){
            $sql .= ' AND tenant_company_id = '.user()->tenant_company_id;
        }

        $dependency = new DbDependency(['sql' => $sql]);

        return CacheHelper::getFromCache($key, $callback, $duration, $dependency);
    }

    /**
     * Количество заказов в работе за сегодня.
     * @return integer
     */
    public static function getTodayWorkingOrders()
    {
        try{
            $finishedStatusesId = OrderStatus::getFinishedStatusId();
            $orders = $db_orders = \Yii::$app->redis_orders_active->executeCommand(
                'hvals',
                [user()->tenant_id]
            );

            $filterData = (new CompanyCheck())->workWithFilterMap();
            $orders = (new FilterOrderMapService($orders, new FilterMapForm($filterData)))->getFilterOrders();

            $count = 0;

            $curUserCityIds = User::getCurUserCityIds();
            foreach($orders as $order){
                $order = unserialize($order);
                if(!in_array($order['status_id'], $finishedStatusesId) && in_array(
                        $order['city_id'],
                        $curUserCityIds
                    )
                ){
                    $count++;
                }
            }

            $result = $count;
        }catch(\yii\db\Exception $ex){
            session()->setFlash('error', t('error', 'Temporarily does not work order creation. Soon we will fix it.'));
            Yii::error('Ошибка работы с редисом');
            $result = null;
        }

        return $result;
    }

    /**
     * Статистика выполненных заказов за сегодня.
     * @return int
     */
    public static function getTodayCntCompleatedOrders($conditions = [])
    {
        return self::find()
            ->where(array_merge([
                'status_id' => OrderStatus::STATUS_COMPLETED_PAID,
                'city_id' => User::getCurUserCityIds(),
                'tenant_id' => user()->tenant_id,
            ], $conditions))
            ->andWhere(['between', 'status_time', mktime(0, 0, 0), time()])
            ->count();
    }

    /**
     * Статистика отмененных заказов за сегодня.
     * @return int
     */
    public static function getTodayCntRejectedOrders($conditions = [])
    {
        return self::find()
            ->joinWith([
                'status' => function($query){
                    $query->where(['status_group' => OrderStatus::STATUS_GROUP_5]);
                },
            ])
            ->andWhere(array_merge([
                'tenant_id' => user()->tenant_id,
                'city_id' => User::getCurUserCityIds(),
            ], $conditions))
            ->andWhere(['between', 'status_time', mktime(0, 0, 0), time()])
            ->count();
    }

    /**
     * Getting order address
     *
     * @param int $orderId
     *
     * @return array
     */
    public static function getOrderAddress($orderId)
    {
        return Order::findOne($orderId)->address;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCardPayment()
    {
        return $this->hasOne(CardPayment::className(), ['order_id' => 'order_id']);
    }

    public function getFormatStreet($point = 'A')
    {
        $city_name = '';
        if(isset($this->address[$point]['city']) || isset($this->address[$point]['city_n'])){
            if(!empty($this->address[$point]['code_n'])){
                $code = isset($this->address[$point]['code_n']) ? $this->address[$point]['code_n'] : '';
                $city_name =  $this->address[$point]['city_n'] .' ' . $code ;
            }else{
                $city_name .= ''.$this->address[$point]['city'];
            }
        }

        $street_name = $this->address[$point]['street'].' ';
        if(isset($this->address[$point]['code_n']) && !empty($this->address[$point]['code_n'])){
            $street_name = $this->address[$point]['street_n'];
        }

        if(isset($this->address[$point]['house']) || isset($this->address[$point]['house_n'])){
            if(!empty($this->address[$point]['code_n'])){
                $street_name .= ' ' . $this->address[$point]['house_n'];
            }else{
                $street_name .= '' .$this->address[$point]['house'];
            }
        }

        if(isset($this->address[$point]['name_n']) && !empty($this->address[$point]['name_n'])){
            if(strlen($street_name) == 0){
                $street_name .= ' ' . $this->address[$point]['name_n']. ' ';
            }else{
                $street_name .= ' ( ' . $this->address[$point]['name_n']. ')';
            }
        }

        $street_name = isset($city_name) ? ($street_name .', '.$city_name) : $street_name;
        return trim($street_name);
    }

    public function getWaitTime()
    {
        if(!empty($this->detailCost)){
            $wait_time = $this->detailCost->city_time_wait + $this->detailCost->out_time_wait;
            $wait_time_by_minutes = round($wait_time / 60, 2);

            return $wait_time_by_minutes + $this->detailCost->before_time_wait;
        }

        return 0;
    }

    public function getSummaryTime()
    {
        $time = !empty($this->detailCost) ? $this->detailCost->summary_time : $this->predv_time;

        return $time > 0 ? $time : 0;
    }

    public function getSummaryDistance()
    {
        $distance = !empty($this->detailCost) ? $this->detailCost->summary_distance : $this->predv_distance;

        return $distance > 0 ? $distance : 0;
    }

    public function getSummaryCost()
    {
        $cost = !empty($this->detailCost) ? $this->detailCost->summary_cost : $this->predv_price;

        return $cost > 0 ? $cost : 0;
    }

    public function getAddress()
    {
        $result = [];
        if(is_array($this->address)){
            foreach($this->address as $key => $address){
                $item = [];
                $item[] = $address['city'];
                if(!empty($address['street'])){
                    $item[] = $address['street'];
                }
                if(!empty($address['house'])){
                    $item[] = $address['house'];
                }
                if(!empty($address['housing'])){
                    $item[] = $address['housing'];
                }
                if(!empty($address['porch'])){
                    $item[] = $address['porch'];
                }
                if(!empty($address['apt'])){
                    $item[] = $address['apt'];
                }
                if(!empty($address['parking'])){
                    $item[] = $address['parking'];
                }
                $result[] = $key.') '.implode(', ', $item);
            }
        }

        return implode(PHP_EOL, $result);
    }

    public function getDump()
    {
        $dump = [];
        $dump['order_number'] = (string)$this->order_number;
        $dump['device'] = $this->getDeviceName();
        $dump['status'] = OrderStatusService::translate($this->status_id, $this->position_id);
        $dump['address'] = $this->getAddress();
        $dump['worker'] = $this->worker->fullName ?: '';
        $dump['car'] = !empty($this->car) ? $this->car->name.' '.$this->car->gos_number : '';
        $dump['review'] = $this->clientReview->text ?: '';
        $dump['rating'] = $this->clientReview->rating ?: '';
        $dump['wait_time'] = (string)$this->getWaitTime();
        $dump['summary_time'] = (string)$this->getSummaryTime();
        $dump['summary_distance'] = (string)$this->getSummaryDistance();
        $dump['summary_cost'] = (string)$this->getSummaryCost();

        return $dump;
    }

    public function getDumpByClient()
    {
        $dump = [];
        $dump['company'] = !empty($this->company) ? $this->company->name : '';

        return ArrayHelper::merge($this->getDump(), $dump);
    }

    public function getDumpByClientCompany()
    {
        $dump = [];
        $dump['client'] = $this->client->getFullName();
        $dump['order_time'] = app()->formatter->asDateTime($this->order_time, 'short');
        $dump['client_phone'] = getValue($this->client->clientPhones[0]->value);

        return ArrayHelper::merge($this->getDump(), $dump);
    }

    /**
     * Copy attributes from over order
     *
     * @param int $orderId
     */
    public function copyFrom($orderId)
    {
        $order = Order::findOne($orderId);
        if(empty($order)){
            return;
        }

        $this->phone = $order->phone;
        $this->client_id = $order->client_id;
        $this->position_id = $order->position_id;
        $this->tariff_id = $order->tariff_id;
        $this->payment = $order->payment;
        //$this->company_id = $order->company_id; // remove param
        $this->address = $order->address;
        $this->comment = $order->comment;
        $this->bonus_payment = $order->bonus_payment;

        $this->client_passenger_phone = $order->client_passenger_phone;
        $this->client_passenger_name = $order->clientPassenger->name;
        $this->client_passenger_lastname = $order->clientPassenger->last_name;
        $this->client_passenger_secondname = $order->clientPassenger->second_name;

        $this->additional_option = ArrayHelper::map($order->options, 'option_id', 'name');
    }

    public function checkActive()
    {
        return $this->isActive ? $this->userCreatedRedis : $this->userCreated;
    }

    public function isOriginallyPreorder()
    {
        return $this->originally_preorder === 1;
    }

    /**
     * @return dto\SettingExistOrderDto|null
     */
    public function getSettingsCurrentOrder()
    {
        return $this->settings->getSettingsExistOrder();
    }

    public function isHasSettingsCurrentOrderForCouriers()
    {
        $settings = $this->settings->getSettingsExistOrder();

        if($settings->orderFormAddPointPhone
            || $settings->orderFormAddPointComment
            || $settings->requirePointConfirmationCode
        ){
            return true;
        }

        return false;
    }

    /**
     * @return dto\CommonSettingsOrder
     */
    public function getCommonSettingsOrder()
    {
        return OrderSetting::getCommonSettingsOrder($this->city_id, $this->position_id);
    }

    public function isHasCommonSettingsForCouriers()
    {
        $commonSettingsOrder = $this->getCommonSettingsOrder();

        if($commonSettingsOrder->orderFormAddPointPhone
            || $commonSettingsOrder->orderFormAddPointComment
            || $commonSettingsOrder->requirePointConfirmationCode
        ){
            return true;
        }

        return false;
    }

    public function isPaid()
    {
        return $this->status_id == OrderStatus::STATUS_COMPLETED_PAID;
    }

    public static function getDefaultAddress($city_id, $addreses = [])
    {
       /* $city_info = City::find()
            ->where(['city_id' => $city_id])
            ->one();*/

        $address_correct = [];
        foreach($addreses as $key => $item){
            $address_add['city_id'] = isset($item['city_id']) ? $item['city_id'] : '';
            $address_add['city'] = isset($item['city']) ? $item['city'] : '';
            $address_add['street'] = isset($item['street']) ? $item['street'] : '';
            $address_add['housing'] = '';
            $address_add['house'] = '';
            $address_add['porch'] = '';
            $address_add['apt'] = '';
            $address_add['parking'] = '';
            $address_add['placeId'] =  isset($item['placeId']) ? $item['placeId'] : '';
            $address_add['lat'] = \common\helpers\ArrayHelper::getValue($item, 'lat', '');
            $address_add['lon'] = \common\helpers\ArrayHelper::getValue($item, 'lon', '');   //$city_info->lon;

            $address_correct[$key] = $address_add;
        }

        return $address_correct;

    }
}
