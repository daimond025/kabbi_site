<?php

namespace app\modules\order\models\dto;

class CommonSettingsOrder
{
    public $requirePointConfirmationCode;
    public $orderFormAddPointPhone;
    public $orderFormAddPointComment;
}
