<?php

namespace app\modules\order\models;

use Yii;

/**
 * This is the model class for table "{{%order_track}}".
 *
 * @property integer $track
 * @property string $tracking
 * @property integer $order_id
 * @property integer $time
 *
 * @property Order $order
 */
class OrderTrack extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_track}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tracking', 'order_id'], 'required'],
            [['tracking'], 'string'],
            [['order_id', 'time'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'track' => 'Track',
            'tracking' => 'Tracking',
            'order_id' => 'Order ID',
            'time' => 'Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }

    public static function getOrderTrack($order_id)
    {
        $cache_key = 'track_' . $order_id;
        $cache = Yii::$app->cache;
        $data = $cache->get($cache_key);

        if ($data === false)
        {
            $order_track = self::findOne(['order_id' => $order_id]);
            $data = getValue($order_track->tracking);

            if(!is_null($data))
                $cache->set($cache_key, $data);
        }

        return $data;
    }
}
