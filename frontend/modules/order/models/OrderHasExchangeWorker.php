<?php

namespace app\modules\order\models;

use Yii;

/**
 * This is the model class for table "{{%order_has_exchange_worker}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $name
 * @property string $phone
 * @property string $car_description
 * @property string $color
 * @property string $gos_number
 * @property string $external_car_id
 * @property string $external_worker_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Order $order
 */
class OrderHasExchangeWorker extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_has_exchange_worker}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id'], 'required'],
            [['order_id', 'created_at', 'updated_at'], 'integer'],
            [['name', 'car_description'], 'string', 'max' => 255],
            [['phone', 'color', 'gos_number', 'external_car_id', 'external_worker_id'], 'string', 'max' => 100],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'order_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'name' => 'Name',
            'phone' => 'Phone',
            'car_description' => 'Car Description',
            'color' => 'Color',
            'gos_number' => 'Gos Number',
            'external_car_id' => 'External Car ID',
            'external_worker_id' => 'External Worker ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }
}
