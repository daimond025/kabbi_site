<?php

namespace app\modules\order\models;

use MongoDB\BSON\ObjectId;
use MongoDB\BSON\UTCDateTime;
use yii\mongodb\ActiveRecord;

/**
 *
 * @property ObjectId    $_id
 * @property int         $tenant_id
 * @property int         $order_id
 * @property array       $settings
 * @property UTCDateTime $updatedAt
 * @property UTCDateTime $createdAt
 * @property UTCDateTime $__v
 */
class SettingFinishedOrder extends ActiveRecord
{
    /**
     * @return string the name of the index associated with this ActiveRecord class.
     */
    public static function collectionName()
    {
        return 'order_settings';
    }

    /**
     * @return array list of attribute names.
     */
    public function attributes()
    {
        return ['_id', 'tenant_id', 'order_id', 'settings', 'updatedAt', 'createdAt', '__v'];
    }
}
