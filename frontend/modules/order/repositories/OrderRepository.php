<?php

namespace frontend\modules\order\repositories;

use app\modules\order\models\dto\SettingExistOrderDto;
use app\modules\order\models\Order;
use app\modules\order\models\SettingFinishedOrder;

class OrderRepository
{

    /**
     * @param Order $order
     *
     * @return SettingExistOrderDto|null
     */
    public function getSettingsExistOrder(Order $order)
    {
        $settingActiveOrder = \Yii::$app->redis_orders_active
            ->executeCommand('hget', [$order->tenant_id, $order->order_id]);

        if ($settingActiveOrder) {
            $settingActiveOrder = unserialize($settingActiveOrder);

            if (!$this->isHasSettingActiveOrder($settingActiveOrder)) {
                return null;
            }

            $setting = $settingActiveOrder['settings'];
        } else {

            /** @var SettingFinishedOrder $settingFinishedOrder */
            $settingFinishedOrder = SettingFinishedOrder::find()
                ->where([
                    'tenant_id' => $order->tenant_id,
                    'order_id'  => $order->order_id,
                ])
                ->one();

            if (!$this->isHasSettingFinishedOrder($settingFinishedOrder)) {
                return null;
            }

            $setting = $settingFinishedOrder->settings;
        }

        $orderSetting = new SettingExistOrderDto();
        $orderSetting->loadSettings($setting);

        return $orderSetting;
    }


    protected function isHasSettingActiveOrder(array $settingActiveOrder)
    {
        return array_key_exists('settings', $settingActiveOrder)
            && is_array($settingActiveOrder['settings']);
    }

    protected function isHasSettingFinishedOrder($settingFinishedOrder)
    {
        return $settingFinishedOrder instanceof SettingFinishedOrder
            && is_array($settingFinishedOrder->settings);
    }
}
