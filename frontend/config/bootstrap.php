<?php

\Yii::setAlias('bonusSystem', '@frontend/components/bonusSystem');
\Yii::setAlias('operatorApi', '@frontend/components/operatorApi');
\Yii::setAlias('orderApi', '@frontend/components/orderApi');
\Yii::setAlias('paymentGate', '@frontend/modules/paymentGate');
\Yii::setAlias('chat', '@frontend/components/chat');
\Yii::setAlias('logger', '@frontend/components/logger');
\Yii::setAlias('healthCheck', '@common/components/healthCheck');


\Yii::$container->set('yii\widgets\Pjax', ['timeout' => 15000]);