<?php

return [
    'upload'           => getenv('FRONTEND_UPLOAD_DIR'),
    'uploadCallRecord' => getenv('FRONTEND_UPLOAD_CALL_RECORD'),
    'uploadScanDir'    => getenv('FRONTEND_UPLOAD_SCAN_DIR'),

    'user.online.ttlSec' => 15 * 60,

    'settings.orderDistributionTypes' => [
        '1' => 'Distance',
        '2' => 'Parking',
    ],
    'settings.workerBlockTypes'       => [
        'IN_SHIFT' => 'Waivers for change',
        'IN_ROW'   => 'Failures in a row',
    ],

    'settings.offerOrderTypes'               => [
        'queue' => 'One by one',
        'batch' => 'Multiple'
    ],
    'settings.offerOrderWorkerBatchCountMax' => 20,

    'softphone.updateSIPUserUrl'    => getenv('SOFTPHONE_UPDATE_SIP_USER_URL'),
    'softphone.updateSIPDomainsUrl' => getenv('SOFTPHONE_UPDATE_SIP_DOMAIN_URL'),

    'bonusSystem.UDSGame.baseUrl'           => 'https://udsgame.com/v1/partner/',
    'bonusSystem.UDSGame.connectionTimeout' => 15,
    'bonusSystem.UDSGame.timeout'           => 15,

    'order.save.retryCount' => 5,

    'frontend.layout.showFooterUrls'                => (bool)getenv('FRONTEND_LAYOUT_SHOW_FOOTER_URLS'),
    'frontend.layout.images.includeAppleTouchIcons' => (bool)getenv('FRONTEND_LAYOUT_IMAGES_INCLUDE_APPLE_TOUCH_ICONS'),
    'frontend.layout.images.logo'                   => getenv('FRONTEND_LAYOUT_IMAGES_LOGO'),
    'frontend.layout.images.favicon'                => getenv('FRONTEND_LAYOUT_IMAGES_FAVICON'),

    'version' => require __DIR__ . '/version.php',
];
