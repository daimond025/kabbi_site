<?php

\Yii::$container->set(\yii\web\View::className(), function ($container, $params, $config) {
    return new \yii\web\View([
        'params' => [
            'partnerCompanyName' => user() ? user()->getPartnerCompanyName() : '',
            'tenantCompanyName'  => user() ? user()->getCompanyName() : '',
        ],
    ]);
});