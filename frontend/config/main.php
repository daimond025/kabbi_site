<?php

use common\modules\tenant\models\DefaultSettings;
use frontend\components\onlineCashbox\OnlineCashboxApi;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/params.php'
);

$logVars = array_merge(
    ['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION', '_SERVER'],
    require __DIR__ . '/../../common/config/logVarsFilter.php'
);

$config = [
    'id'                  => 'app-frontend',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log', 'bootstrapLanguage'],
    'controllerNamespace' => 'frontend\controllers',
    'aliases'             => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components'          => [
        // Yii2 components
        'assetManager' => [
            'appendTimestamp' => true,
            'basePath'        => '@webroot/assets',
            'baseUrl'         => '@web/assets',
            'bundles'         => [
                'yii\bootstrap\BootstrapAsset' => [
                    'sourcePath' => null,
                    'css'        => [],
                ],
                'yii\jui\JuiAsset'             => [
                    'css' => [],
                ],
            ],
        ],

        'authManager' => [
            'class' => 'frontend\components\rbac\DbManager',
            'cache' => 'cache',
        ],

        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'log' => [
            'traceLevel' => 3,
            'targets'    => [
                [
                    'class'   => 'yii\log\FileTarget',
                    'except'  => ['app-log'],
                    'levels'  => ['error', 'warning'],
                    'logVars' => $logVars,
                ],
                [
                    'class'   => 'yii\log\EmailTarget',
                    'mailer'  => 'mailer_log',
                    'levels'  => ['error', 'warning'],
                    'except'  => ['app-log'],
                    'logVars' => $logVars,
                    'message' => [
                        'from'    => [getenv('MAIL_SUPPORT_USERNAME')],
                        'to'      => [getenv('MAIL_SUPPORT_USERNAME')],
                        'subject' => getenv('MAIL_SUPPORT_SUBJECT_FRONTEND'),
                    ],
                ],
                [
                    'class'          => 'logger\CustomSyslogTarget',
                    'identity'       => 'frontend-' . require __DIR__ . '/version.php',
                    'exportInterval' => 1,
                    'logVars'        => [],
                    'categories'     => ['app-log'],
                ],
                [
                    'class'         => 'notamedia\sentry\SentryTarget',
                    'dsn'           => getenv('SENTRY_DSN'),
                    'levels'        => ['error', 'warning'],
                    'logVars'       => $logVars,
                    'except'        => [
                        'yii\web\HttpException:400',
                        'yii\web\HttpException:401',
                        'yii\web\HttpException:403',
                        'yii\web\HttpException:404',
                    ],
                    'clientOptions' => [
                        'environment' => getenv('ENVIRONMENT_NAME'),
                    ],
                ],
            ],
        ],

        'request' => [
            'baseUrl'             => '',
            'cookieValidationKey' => getenv('FRONTEND_COOKIE_VALIDATION_KEY'),
            'csrfCookie'          => ['httpOnly' => true, 'secure' => YII_ENV_PROD],
            'enableCsrfValidation' => false,
        ],

        'urlManager' => [
            'baseUrl'         => '/',
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => require __DIR__ . '/routes.php',
        ],

        'user' => [
            'identityClass'   => 'app\modules\tenant\models\User',
            'enableAutoLogin' => true,
            'loginUrl'        => ['/tenant/user/login/'],
        ],


        // Others components
        'amqp' => [
            'class'    => 'app\components\Amqp',
            'host'     => getenv('RABBITMQ_MAIN_HOST'),
            'port'     => getenv('RABBITMQ_MAIN_PORT'),
            'user'     => getenv('RABBITMQ_MAIN_USER'),
            'password' => getenv('RABBITMQ_MAIN_PASSWORD'),
        ],

        'autocomplete' => [
            'class'  => 'frontend\components\autocomplete\Autocomplete',
            'apiKey' => function () {
                return DefaultSettings::getValue(DefaultSettings::SETTING_GEOSERVICE_API_KEY);
            },
            'url'    => getenv('API_AUTOCOMPLETE_URL'),
        ],

        'operatorApi' => '\operatorApi\LocalOperatorApi',

        'orderApi' => [
            'class'   => '\orderApi\OrderApi',
            'baseUrl' => getenv('API_ORDER_URL'),
        ],

        'profileService' => [
            'class'             => '\paymentGate\ProfileService',
            'baseUrl'           => getenv('API_PAYGATE_URL'),
            'timeout'           => 15,
            'connectionTimeout' => 15,
        ],

        'onlineСashboxApi' => [
            'class'             => OnlineCashboxApi::class,
            'baseUrl'           => getenv('API_CASHDESK_URL'),
            'timeout'           => 15,
            'connectionTimeout' => 15,
        ],

        'pushApi' => [
            'class' => 'frontend\components\pushApi\PushApi',
            'url'   => getenv('API_PUSH_URL'),
        ],

        'routeAnalyzer' => [
            'class' => 'app\components\routeAnalyzer\TaxiRouteAnalyzer',
            'url'   => getenv('API_SERVICE_URL'),
        ],

        'workerApi' => [
            'class' => 'frontend\components\workerApi\WorkerApi',
            'url'   => getenv('API_WORKER_URL'),
        ],

        'consulService' => [
            'class'           => 'frontend\components\consul\ConsulService',
            'consulAgentHost' => getenv('CONSUL_MAIN_HOST'),
            'consulAgentPort' => getenv('CONSUL_MAIN_PORT'),
        ],
    ],

    'modules' => [
        'address'       => 'app\modules\address\Module',
        'balance'       => 'app\modules\balance\Module',
        'car'           => 'frontend\modules\car\Module',
        'city'          => 'app\modules\city\Module',
        'client'        => 'app\modules\client\Module',
        'employee'      => 'frontend\modules\employee\Module',
        'notifications' => 'app\modules\notifications\Module',
        'order'         => 'app\modules\order\Module',
        'parking'       => 'app\modules\parking\Module',
        'reports'       => 'app\modules\reports\Module',
        'sms'           => 'app\modules\sms\Module',
        'support'       => 'app\modules\support\Module',
        'tariff'        => 'app\modules\tariff\Module',
        'tenant'        => 'app\modules\tenant\Module',
        'upload'        => 'frontend\modules\upload\Module',
        'promocode'     => 'frontend\modules\promocode\Module',
        'companies'     => 'frontend\modules\companies\Module',
        'onlinecashbox' => 'frontend\modules\onlinecashbox\Module',
        'exchange'      => 'frontend\modules\exchange\Module',
    ],

    'params' => $params,
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class'      => 'yii\debug\Module',
        'allowedIPs' => ['192.168.1.*', '127.0.0.1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class'      => 'yii\gii\Module',
        'allowedIPs' => ['192.168.1.*', '127.0.0.1'],
    ];
}

return $config;