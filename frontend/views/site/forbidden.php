<?php

use yii\helpers\Html;

$this->title = $name;

?>

<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title><?= Html::encode($this->title) ?></title>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic&subset=all' rel='stylesheet'
          type='text/css'>
    <link rel="stylesheet" href="/error/css/main.css"/>
    <script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
</head>
<body>
<div class="wrapper">
    <i class="cars"></i>
    <div class="login_form2">
        <i class="cloud01"></i>
        <i class="cloud02"></i>
        <i class="cloud03"></i>
    </div>
    <div class="error_text">
        <h1><?= t('access-forbidden', 'You use the tariff') . ' ' . Html::encode($tariff) ?></h1>
        <p>
            <?= t('access-forbidden', 'Your tariff has a limit on the count of concurrent users.') ?>
            <?= t('access-forbidden', 'Now the limit is reached.') ?>
            <br>
            <?= t('access-forbidden', 'Try later.') ?>
        </p>
        <h2><a href="<?= $url ?>"><?= t('access-forbidden', 'Sign in to your account') ?></a></h2>
    </div>
</div>
</body>
</html>