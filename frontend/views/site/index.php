<?php

use app\modules\order\models\Order;
use app\modules\tenant\models\User;
use common\modules\tenant\models\Tenant;
use frontend\modules\employee\components\worker\WorkerShiftService;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var WorkerShiftService $workerShiftService */
/* @var $clientApps array */
/* @var $driverAndroidLink string */

$this->title = t('app', 'Home');


$workerShiftService = Yii::$app->getModule('employee')->get('workerShift');
?>
<h1><?= Html::encode(ArrayHelper::getValue($this->params, 'partnerCompanyName')); ?></h1>
<div class="index_stat">
    <div class="is_logo">
        <?
        $logo_path = Tenant::getLogoPath();
        if (!empty($logo_path)):?>
            <?= Html::img($logo_path, ['alt' => 'logo']) ?>
        <? endif ?>
    </div>
    <?
    $todayWorkingOrders = Order::getTodayWorkingOrders();
    if ($todayWorkingOrders !== null):
        ?>
        <div class="is_orders">
            <b><?= t('home', 'Now in working') ?></b>
            <i><?= Html::encode($todayWorkingOrders); ?></i>
            <span><?= t('home', 'Orders') ?></span>
        </div>
    <? endif ?>
    <div class="is_stat">
        <b><?= t('home', 'Orders from 00:00 to the current time') ?></b>
        <ul>
            <li style="color: rgb(8, 129, 66);">
                <?php
                    $conditions = [];
                    if (app()->user->can(User::ROLE_STAFF_COMPANY)) {
                        $conditions = ['tenant_company_id' => user()->tenant_company_id];
                    }
                ?>
                <i><?= Html::encode(Order::getTodayCntCompleatedOrders($conditions)); ?></i><span><?= t('home',
                        'Completed and paid') ?></span></li>
            <li style="color: #cc1919;">
                <i><?= Html::encode(Order::getTodayCntRejectedOrders($conditions)); ?></i><span><?= t('home', 'Rejected') ?></span>
            </li>
        </ul>
    </div>
    <div class="is_online" <? if (app()->user->can(User::ROLE_STAFF_COMPANY)): ?>style="width: 160px;" <? endif; ?>>
        <b><?= t('home', 'Online') ?></b>
        <ul>
            <li><i><?= Html::encode($workerShiftService->getQuantityWorkersOnShift()); ?></i><span><?= t('employee',
                        'Workers') ?></span></li>
            <? $arOnlineStat = User::getOnlineStat($conditions) ?>

            <? if (!app()->user->can(User::ROLE_STAFF_COMPANY)): ?>
            <li><i><?= Html::encode($arOnlineStat['dispatchers']); ?></i><span><?= t('home', 'Dispatchers') ?></span></li>
            <? endif; ?>
            <li><i><?= Html::encode($arOnlineStat['others']); ?></i><span><?= t('home', 'Staff') ?></span></li>
        </ul>
    </div>
</div>
<h2><?= t('home', 'Programms for working') ?></h2>

<div class="home_apps">
    <ul>
        <li class="driver_app"><i></i><b><?= t('home', 'Driver app') ?></b>
            <?= Html::a('', $driverAndroidLink, [
                'rel'    => 'nofollow noopener',
                'target' => '_blank',
                'class'  => 'google_play',
            ]); ?>
            <!--            <a rel="nofollow noopener" target="_blank" href="-->
            <? //=DefaultSettings::getSetting('APP_STORE_DRIVER_APP_LINK')?><!--" class="app_store"></a>-->
        </li>

        <?
        foreach ($clientApps as $app) {
            /** @var $app \frontend\modules\tenant\models\mobileApp\MobileApp */
            if ($app->getIsLink()) {
                echo '<li class="user_app"><i></i>';

                if (count($clientApps) === 1) {
                    echo '<b>' . t('home', 'Client app') . '</b>';
                } else {
                    echo '<b>' . Html::encode($app->name) . '</b>';
                }

                if ($app->getIsAndroidLink()) {
                    echo Html::a('', $app->link_android,
                        ['class' => 'google_play', 'target' => '_blank', 'rel' => 'nofollow noopener']);
                }
                if ($app->getIsIosLink()) {
                    echo Html::a('', $app->link_ios,
                        ['class' => 'app_store', 'target' => '_blank', 'rel' => 'nofollow noopener']);
                }

                echo '</li>';
            }
        }
        ?>

    </ul>
</div>

<h2><a href="/cabinet/login"><?= t('home', 'Cabinet link for clients') ?></a></h2>