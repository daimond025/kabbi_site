<?php

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = $name;

?>

<!DOCTYPE html>

<html lang="ru">
<head>
    <meta charset="utf-8"/>
    <title><?= Html::encode($this->title) ?></title>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,400italic&subset=cyrillic' rel='stylesheet'
          type='text/css'>
    <link rel="stylesheet" href="/error/css/main.css"/>
    <link rel="shortcut icon" href="/images/favicon.ico">
</head>
<body>
<div class="wrapper">
    <div class="error_text">
        <h1>:-(</h1>
        <div>
            <b><?= $name ?></b><span><?= $message ?></span>
            <h2><a href="<?= app()->params['landingUrl']; ?>"><?= t('app', 'Go to Gootax home page',[],$lang) ?></a></h2>
        </div>
    </div>
</div>
</body>
</html>