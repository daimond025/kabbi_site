<?php

use app\modules\tenant\models\User;
use common\modules\tenant\models\TenantHelper;
use frontend\assets\AppAsset;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode(app()->name . ': ' . $this->title) ?></title>
    <?php $this->head() ?>

    <? if (app()->params['frontend.layout.images.includeAppleTouchIcons']): ?>
        <link rel="apple-touch-icon" href="/images/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="/images/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="/images/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="/images/icon180.png" sizes="180x180">
    <? endif ?>

    <link rel="shortcut icon" href="<?= app()->params['frontend.layout.images.favicon'] ?>">
</head>
<body data-notifications="0">
<?php $this->beginBody() ?>
<?= $this->render('_header') ?>
<div class="main_wrapper">
    <nav class="main_nav">
        <?
        echo yii\widgets\Menu::widget([
            // Important: you need to specify url as 'controller/action',
            // not just as 'controller' even if default action is used.
            'activateParents' => true,
            'hideEmptyItems'  => true,
            'options'         => ['class' => 'first_nav'],
            'items'           => [
                [
                    'template' => '<a class="mni00" href="{url}">{label}</a>',
                    'label'    => t('app', 'Home'),
                    'url'      => ['/site/index'],
                ],
                [
                    'label'    => t('app', 'Orders'),
                    'template' => '<a class="mni01">{label}</a>',
                    'items'    => [
                        [
                            'label'   => t('app', 'Orders'),
                            'url'     => [\app\modules\tenant\models\User::POSITION_OPERATOR == user()->position_id ? '/order/operator/index' : '/order/order/index'],
                            'visible'  => app()->authManager->checkAccess(user()->id, 'read_orders'),
                        ],
                        [
                            'label'   => t('app', 'Assign orders'),
                            'url'     => ['/order/order-assign/index'],
                            'visible'  => app()->authManager->checkAccess(user()->id, 'read_orders'),
                        ]
                    ]
                ],
                // todo OLD values
               /* [
                    'template' => '<a class="mni01" href="{url}">{label}</a>',
                    'label'    => t('app', 'Orders'),
                    'url'      => [\app\modules\tenant\models\User::POSITION_OPERATOR == user()->position_id ? '/order/operator/index' : '/order/order/index'],
                    'visible'  => app()->authManager->checkAccess(user()->id, 'read_orders'),
                ],*/
                [
                    'label'    => t('app', 'Clients'),
                    'template' => '<a class="mni03">{label}</a>',
                    'items'    => [
                        [
                            'label'   => t('app', 'Base of clients'),
                            'url'     => ['/client/base/list'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_clients'),
                        ],
                        [
                            'label'   => t('app', 'Organization'),
                            'url'     => ['/client/company/list'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_organization'),
                        ],
                        [
                            'label'   => t('app', 'Tariff for clients'),
                            'url'     => ['/tariff/tariff-client/list'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_clientTariff'),
                        ],
                        [
                            'label'   => t('app', 'Tariff groups'),
                            'url'     => ['/tariff/tariff-group/list'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_clientTariff'),
                        ],
                        [
                            'label'   => t('app', 'Bonus system'),
                            'url'     => ['/client/bonus/list'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_bonus'),
                        ],
                        [
                            'label'   => t('app', 'Referral system'),
                            'url'     => ['/client/referral/list'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_referralSystem'),
                        ],
                        [
                            'label'   => t('promo', 'Promo system'),
                            'url'     => ['/promocode/promo/list'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_promoSystem'),
                        ],
                    ],
                    'visible'  => app()->authManager->checkAccess(user()->id, 'read_clients')
                        || app()->authManager->checkAccess(user()->id, 'read_organizationon')
                        || app()->authManager->checkAccess(user()->id, 'read_clientTariffntTariff')
                        || app()->authManager->checkAccess(user()->id, 'read_bonus'),
                ],
                [
                    'label'    => t('employee', 'Workers'),
                    'template' => '<a class="mni08">{label}</a>',
                    'items'    => [
                        [
                            'label'   => t('employee', 'Tariffs for employee'),
                            'url'     => ['/employee/tariff/list'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_driverTariff'),
                        ],
                        [
                            'label' => t('employee', 'Worker groups'),
                            'url' => ['/employee/group/list'],
                            'visible' => !app()->authManager->checkAccess(user()->id, User::ROLE_STAFF_COMPANY),
                        ],
                        [
                            'label'   => t('car', 'Cars'),
                            'url'     => ['/car/car/list'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_cars'),
                        ],
                        [
                            'label' => t('employee', 'Workers'),
                            'url' => ['/employee/worker/list']
                        ],

                        [
                            'label'   => t('car', 'Bind Workers Vs Cars'),
                            'url'     => ['/employee/worker/worker-bond-car'],
                            'visible' => app()->authManager->checkAccess(user()->id, User::ROLE_STAFF) ||
                                app()->authManager->checkAccess(user()->id, User::ROLE_STAFF_COMPANY)  ||
                                app()->authManager->checkAccess(user()->id, User::ROLE_EXTRINSIC_DISPATCHER) ||
                                app()->authManager->checkAccess(user()->id, User::ROLE_BRANCH_DIRECTOR) ,
                        ],
                    ],
                    'visible'  => app()->authManager->checkAccess(user()->id, 'read_drivers'),
                ],
                [
                    'label'    => t('app', 'Reports'),
                    'template' => '<a class="mni05">{label}</a>',
                    'items'    => [
                        ['label' => t('app', 'Orders'), 'url' => ['/reports/order/index']],
                        [
                            'label'   => t('employee', 'Workers'),
                            'url'     => ['/reports/worker/index'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_drivers'),
                        ],
                        [
                            'label' => t('app', 'Operators'),
                            'url' => ['/reports/operator/index'],
                            'visible' => !app()->authManager->checkAccess(user()->id, User::ROLE_STAFF_COMPANY),
                        ],
                        [
                            'label' => t('app', 'Transactions'),
                            'url' => ['/reports/transaction/index'],
                            'visible' => !app()->authManager->checkAccess(user()->id, User::ROLE_STAFF_COMPANY),
                        ],
                        [
                            'label' => t('app', 'Companies'),
                            'url' => ['/reports/company/index'],
                            'visible' => !app()->authManager->checkAccess(user()->id, User::ROLE_STAFF_COMPANY),
                        ],
                        [
                            'label' => t('reports', 'PUSH-notifications'),
                            'url' => ['/reports/notification/index'],
                            'visible' => !app()->authManager->checkAccess(user()->id, User::ROLE_STAFF_COMPANY),
                        ],
                    ],
                    'visible'  => app()->authManager->checkAccess(user()->id, 'read_reports'),
                ],
                [
                    'label'    => t('app', 'Directories'),
                    'template' => '<a class="mni06">{label}</a>',
                    'items'    => [
                        [
                            'label'   => t('app', 'Cities'),
                            'url'     => ['/tenant/city/list'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_cities'),
                        ],
                        [
                            'label'   => t('tenant_company', 'Companies'),
                            'url'     => ['/companies/company/list'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_cities') ||
                                app()->authManager->checkAccess(user()->id, User::ROLE_STAFF_COMPANY),
                        ],
                        [
                            'label'   => t('app', 'Staff'),
                            'url'     => ['/tenant/user/list'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_users'),
                        ],
                        [
                            'label'   => t('app', 'Parking'),
                            'url'     => ['/parking/parking/list'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_parkings'),
                        ],
                        [
                            'label'   => t('app', 'Phone line'),
                            'url'     => ['/tenant/phone-line/list'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_phoneLine'),
                        ],
                        [
                            'label' => t('setting','Push-notification delivery'),
                            'url' => ['/notifications/push/mass-mailing'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_notifications'),
                        ],
                        [
                            'label' => t('exchange','Exchange of orders'),
                            'url' => ['/exchange/exchange-connection/list'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_exchange'),
                        ],
                    ],
                ],
                [
                    'label'    => t('app', 'Settings'),
                    'template' => '<a class="mni07">{label}</a>',
                    'items'    => [
                        [
                            'label'   => t('app', 'Company data'),
                            'url'     => ['/tenant/tenant/info'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'tenant'),
                        ],
                        [
                            'label'   => t('setting', 'Tariff and payment history'),
                            'url'     => ['/tenant/tariff/index'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_gootaxTariff'),
                        ],
                        //['label' => t('setting', 'General'), 'url' => ['/tenant/setting/list', 'type' => 'general'], 'visible' => !Yii::$app->user->isGuest],
                        [
                            'label'   => t('setting', 'Orders'),
                            'url'     => ['/tenant/setting/list', 'type' => 'orders'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_settings'),
                        ],
                        [
                            'label'   => t('employee', 'Workers'),
                            'url'     => ['/tenant/setting/list', 'type' => 'drivers'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_settings'),
                        ],
                        [
                            'label'   => t('setting', 'Car options'),
                            'url'     => ['/tenant/car-setting/car-option'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_settings'),
                        ],
                        //['label' => t('setting', 'Telephony'), 'url' => ['/tenant/setting/telephony'], 'visible' => !Yii::$app->user->isGuest],
                        [
                            'label'   => t('setting', 'Sms'),
                            'url'     => ['/tenant/sms-server/list'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_settings'),
                        ],
                        [
                            'label'   => t('setting', 'Email'),
                            'url'     => ['/tenant/email-setting/index'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_emailSetting'),
                        ],
                        [
                            'label'   => t('setting', 'Client notification'),
                            'url'     => ['/tenant/status-event/list'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_settings'),
                        ],
                        [
                            'label'   => t('setting', 'Message templates'),
                            'url'     => ['/tenant/template/list'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_settings'),
                        ],
                        [
                            'label'   => t('setting', 'Bonus system'),
                            'url'     => ['/tenant/bonus-system/update'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_bonus'),
                        ],
                        [
                            'label'   => t('setting', 'Bank cards'),
                            'url'     => ['/tenant/bank-card/update'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_bankCard'),
                        ],
                        [
                            'label'   => t('setting', 'Online cashbox'),
                            'url'     => ['/onlinecashbox/base/list'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_onlineCashbox'),
                        ],
                        [
                            'label'   => t('setting', 'Maps and routes'),
                            'url'     => ['/tenant/geo-service/list'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_settings'),
                        ],
                        [
                            'label'   => t('setting', 'Custom fields'),
                            'url'     => ['/tenant/field/list'],
                            'visible' => app()->authManager->checkAccess(user()->id, 'read_settings'),
                        ],
                    ],
                    'visible'  => app()->authManager->checkAccess(user()->id, 'read_settings')
                        || app()->authManager->checkAccess(user()->id, 'tenant')
                        || app()->authManager->checkAccess(user()->id, 'read_bonus')
                        || app()->authManager->checkAccess(user()->id, 'read_gootaxTariff')
                        || app()->authManager->checkAccess(user()->id, 'read_bankCard'),
                ],
                [
                    'items' => [
                        [
                            'label'    => t('app', 'About'),
                            'url'      => app()->params['landingUrl'],
                            'template' => '<a rel="nofollow noopener" href="{url}" target="_blank">{label}</a>',
                        ],
                        [
                            'label'    => t('app', 'Tariffs and prices'),
                            'url'      => app()->params['landingUrl'] . '/prices',
                            'template' => '<a rel="nofollow noopener" href="{url}" target="_blank">{label}</a>',
                        ],
                        ['label' => t('app', 'Support'), 'url' => ['/support/support/list']],
                    ],
                ],
            ],
        ]);
        ?>
    </nav>
    <section class="gray_wrapper">
        <section class="content">
            <?= \frontend\widgets\Alert::widget(); ?>
            <div>
                <header class="header_notification may_close" style="display:none;">
                    <a class="close_header"><i></i></a>
                    <div id="errorBlock"></div>
                </header>
            </div>
            <section class="cont">
                <?= $content ?>
            </section>
        </section>
    </section>
</div>
<?= $this->render('_footer') ?>
<!--помощь-->
<?php
if (TenantHelper::checkNeedsHelper()):
    $helper_items = TenantHelper::getHelperItems();
    ?>
    <a id="tenant_helper" class="hw_title">
        <span><?= t('helper', 'Configure your system to start') ?></span>
    </a>
    <div class="hw_content">
        <div class="hwc_header">
            <i></i>
            <h3><?= t('helper', 'Hello') ?>, <?= Html::encode(trim(user()->name . ' ' . user()->second_name)); ?>!
                <br/><?= t('helper', 'I will help you in first steps') ?> <?= t('helper', 'of system configuration') ?>
            </h3>
        </div>
        <div class="hwc_content">
            <ul>
                <li><i <? if ($helper_items[TenantHelper::CITY_FILL]): ?>class="ok"<? endif ?>></i><?= t('helper',
                        'fill') ?> <a
                        href="<?= app()->urlManager->createUrl('/tenant/city/list') ?>"><?= t('helper',
                            'cities for working') ?></a></li>
                <li><i <? if ($helper_items[TenantHelper::PARKING_FILL]): ?>class="ok"<? endif ?>></i><?= t('helper',
                        'draw city borders in') ?> <a
                        href="<?= app()->urlManager->createUrl('/parking/parking/list') ?>"><?= t('helper',
                            'parking') ?></a></li>
                <li>
                    <i <? if ($helper_items[TenantHelper::WORKER_TARIFF_FILL]): ?>class="ok"<? endif ?>></i><?= t('helper',
                        'fill') ?> <a
                        href="<?= app()->urlManager->createUrl('/employee/tariff/list') ?>"><?= t('employee',
                            'Tariffs for employee') ?></a></li>
                <li>
                    <i <? if ($helper_items[TenantHelper::CLIENT_TARIFF_FILL]): ?>class="ok"<? endif ?>></i><?= t('helper',
                        'fill') ?> <a
                        href="<?= app()->urlManager->createUrl('/tariff/tariff-client/list') ?>"><?= t('helper',
                            'tariffs for clients') ?></a></li>
                <li>
                    <i <? if ($helper_items[TenantHelper::CAR_FILL] && $helper_items[TenantHelper::WORKER_FILL]): ?>class="ok"<? endif ?>></i><?= t('helper',
                        'fill') ?> <a href="<?= app()->urlManager->createUrl('/car/car/list') ?>"><?= t('helper',
                            'cars') ?></a> <?= t('helper', 'and') ?> <a
                        href="<?= app()->urlManager->createUrl('/employee/worker/list') ?>"><?= t('employee',
                            'Workers') ?></a></li>
            </ul>
        </div>
        <div class="hwc_footer">
            <h3><?= t('helper', 'a few steps left') ?></h3>
        </div>
    </div>
    <div class="help_overflow"></div>
<? endif ?>
<?php $this->endBody() /*?>
        <!-- Yandex.Metrika counter --><script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter30670468 = new Ya.Metrika({ id:30670468, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch (e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="https://mc.yandex.ru/watch/30670468" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->*/ ?>
</body>
</html>
<?php $this->endPage() ?>
