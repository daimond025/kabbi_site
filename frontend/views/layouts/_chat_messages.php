<?php

use yii\helpers\Html;
?>

<div class = "mw_messages">
    <div class = "mwm_header" style="display: none">
        <h2 id="chat_header"><?= t('chat', 'CHAT'); ?></h2>
    </div>
    <div class = "mwm_content" >
        <?php foreach ($chatData as $data): ?>
            <div data-type="chat_window" data-owner="main_city_chat" data-ownerid="<?= $data["city_id"] ?>" data-cityid="<?= $data["city_id"] ?>" style="display: none">
                <ul>
                </ul>
            </div>
            <?php foreach ($data['worker_arr'] as $worker):
                ?>
                <div data-type="chat_window" data-owner="worker" data-ownerid="<?= $worker["worker_callsign"] ?>" data-cityid="<?= $worker["city_id"] ?>" style="display: none">
                    <ul>
                    </ul>
                </div>
            <?php endforeach; ?>
            <?php foreach ($data['user_arr'] as $user):
                ?>
                <div data-type="chat_window" data-owner="user" data-ownerid="<?= $user["user_id"] ?>" data-cityid="<?= $user["city_id"] ?>" style="display: none">
                    <ul>
                    </ul>
                </div>
            <?php endforeach; ?>
        <?php endforeach; ?>
    </div>
    <div class = "mwm_footer" style="display: none">
        <textarea id="message_area" placeholder = "<?= t('chat', 'Message'); ?>"></textarea>
        <a class = "test_fitch"><i></i><span><?= t('chat', 'Enter'); ?></span></a>
    </div>
</div>




