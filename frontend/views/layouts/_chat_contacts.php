<?php

use yii\helpers\Html;

?>

<div class="mw_contacts">
    <div class="mwc_content">
        <?php foreach ($chatData as $data): ?>
            <a class="chat_user_city" data-cityid="<?= $data['city_id'] ?>" rel="<?= $data['city_name'] ?>"><?= Html::encode($data['city_name']); ?><i></i></a>
            <div class="users_spoler" data-user-city="<?= $data['city_name'] ?>" style="display: none">
                <div class="mwc_search">
                    <input type="text" data-cityid="<?= $data['city_id'] ?>" class="search_chat_user" placeholder="<?= t('chat', 'Search for workers'); ?>"/>
                </div>
                <ul data-cityid="<?= $data['city_id'] ?>">
                    <li data-cityid="<?= $data['city_id'] ?>" class="shared_chat">
                        <a>
                            <i></i>
                            <div style="display: none" class="new_messages" id="city_message_counter_<?= $data['city_id'] ?>_<?= $data['city_id'] ?>"></div>
                            <span data-type="main_chat_selector" data-cityid="<?= $data['city_id'] ?>" data-cityname="<?= $data['city_name'] ?>"><?= Html::encode($data['city_name']) . ' : ' . t('chat', 'general chat'); ?>
                            </span>
                        </a>
                    </li>
                    <?php foreach ($data['user_arr'] as $user): ?>

                        <li data-cityid="<?= $data['city_id'] ?>"><a>
                                <i><span><?= $user["photo_url"] ?></span></i>
                                <div style="display: none" class="new_messages" id="user_message_counter_<?= $user['user_id'] ?>_<?= $user['city_id'] ?>"></div>
                                <span data-type="user_chat_selector" data-userid="<?= $user['user_id'] ?>" data-cityid="<?= $data['city_id'] ?>">
                                    <span><?= Html::encode($user['last_name']); ?> <?= Html::encode($user['name']); ?> <?= Html::encode($user['second_name']); ?></span>
                                    <b><?= Html::encode($user['user_role']); ?></b>
                                </span>
                            </a>
                        </li>

                    <?php endforeach; ?>
                    <?php foreach ($data['worker_arr'] as $worker): ?>

                        <li data-cityid="<?= $data['city_id'] ?>"><a>
                                <i><span><?= $worker["photo_url"] ?></span></i>
                                <div style="display: none" class="new_messages" id="worker_message_counter_<?= $worker['worker_callsign'] ?>_<?= $worker['city_id'] ?>"></div>
                                <span data-type="worker_chat_selector" data-workercallsign="<?= $worker['worker_callsign'] ?>" data-cityid="<?= $data['city_id'] ?>" data-callsign="<?= Html::encode($worker['worker_callsign']); ?>">
                                    <span><?= Html::encode($worker['last_name']); ?> <?= Html::encode($worker['name']); ?> <?= Html::encode($worker['second_name']); ?> </span>
                                    <b><?= Html::encode($worker['worker_callsign']); ?></b>
                                </span>
                            </a>
                        </li>

                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endforeach; ?>
    </div>
</div>