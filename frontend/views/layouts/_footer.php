<?php

use \app\modules\tenant\models\User;
use \yii\helpers\Html;
use yii\helpers\ArrayHelper;

?>
<? if (app()->params['frontend.layout.showFooterUrls']): ?>
    <span>
    <div id="helpdesk_widget"></div>
    <script type="text/javascript" src="https://red.gootax.pro/helpdesk_widget/widget.js"></script>
    <script>
      RedmineHelpdeskWidget.config({
          color: '#15af82',
          translation: {
              nameLabel: 'Ваше имя',
              emailLabel: 'Ваш email (на него придёт ответ)',
              descriptionLabel: 'Пожалуйста, напишите ваш вопрос максимально подробно. Это позволит нам быстрее найти ответ.',
              createSuccessDescription: 'Спасибо за ваше обращение! Мы вам ответим на email, который вы указали.',
              createErrorLabel: 'Произошла ошибка, скоро исправим...',
              createButtonLabel: 'Отправить',
              subjectLabel: 'Тема обращения',
              attachmentLinkLabel: 'Прикрепить файл',
              createSuccessLabel: 'Обращение создано'
          },
          identify: {
              trackerValue: 'Поддержка',
              projectValue: '01 Гутакс: Поддержка клиентов',
          },
          attachment: true,
          title: '<h2>Написать в тех. поддержку</h2><p>Напишите нам, мы ответим вам в течение нескольких часов.<br><br>Напишите как можно подробнее:<br>- Номер заказа<br>- Позывной водителя<br>- Номер клиента<br>- или другую важную информацию для решения вопроса'
      });
    </script>
</span>
<? endif ?>

<footer class="main_footer">
    <?php if (!User::isCurrentPartnerEmployee()) { ?>
        <? if (app()->params['frontend.layout.showFooterUrls']): ?>
            <ul>
                <li><a rel="sn08 nofollow noopener" target="_blank"
                       href="<?= app()->params['landingUrl'] ?>"><? echo t('app',
                            'About') ?></a></li>
                <li><a rel="sn08 nofollow noopener" target="_blank"
                       href="<?= app()->params['landingUrl'] . '/prices' ?>"><? echo t('app',
                            'Tariffs and prices') ?></a></li>
                <li><a title="<?= t('app', 'Support') ?>" class="js-open-helpdesk-widget"><?= t('app',
                            'Support') ?></a></li>
            </ul>
        <? endif ?>
    <?php } ?>
    <?php $copyright = '&copy ';
    $copyright .= date('Y') . ', ';
    if (User::isCurrentPartnerEmployee()) {
        $copyright .= Html::encode(ArrayHelper::getValue($this->params, 'tenantCompanyName'));
    } else {
        $copyright .= t('app', Html::encode(app()->name));
    }
    ?>
    <p><?= $copyright ?></p>
</footer>