<?php

use yii\helpers\Html;
use frontend\modules\tenant\models\UserDispetcher;
?>

<div style="display: none">
    <div style="display: none">
        <?php if ($userData = UserDispetcher::getUserData()) {
            echo $userData;
        } ?>
    </div>
    <div id="messenger">
        <div class="mes_tabs_header">
            <!--<a class="mst_alerts" data-mtid="mt01"><i></i><span>0</span></a>-->
            <a class="mst_mes" data-mtid="mt02"><i></i><span>0</span></a>
            <!--<a class="mst_sos" data-mtid="mt03"><i></i><span>0</span></a>-->
        </div>
        <div class="mes_tabs_content">
            <div id="mt01" style="display: none;">
                in developing...
            </div>
            <div id="mt02" style="display: none;">
                <div class="messenger_window">
                    <?php if ($chatData = UserDispetcher::getChatData()) {
                      //  dd($chatData);die();
                        echo $this->render('_chat_contacts', ['chatData' => $chatData]);
                        echo $this->render('_chat_messages', ['chatData' => $chatData]);
                    } ?>
                </div>
            </div>
            <div id="mt03" style="display: none;">
                in developing...
            </div>
        </div>
    </div>
</div>