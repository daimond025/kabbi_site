<?php
use yii\helpers\Html;
use frontend\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode(app()->name . ': ' . $this->title) ?></title>
        <?php $this->head() ?>

        <? if (app()->params['frontend.layout.images.includeAppleTouchIcons']): ?>
            <link rel="apple-touch-icon" href="/images/icon76.png" sizes="76x76">
            <link rel="apple-touch-icon" href="/images/icon120.png" sizes="120x120">
            <link rel="apple-touch-icon" href="/images/icon152.png" sizes="152x152">
            <link rel="apple-touch-icon" href="/images/icon180.png" sizes="180x180">
        <? endif ?>

        <link rel="shortcut icon" href="<?= app()->params['frontend.layout.images.favicon'] ?>">
    </head>
    <body data-notifications="0">
        <?php $this->beginBody() ?>
        <?= $this->render('_header') ?>
        <div class="main_wrapper orders <?if(strpos(app()->request->url, 'operator')):?>operator_orders<?endif?>">
            <nav class="main_nav">
                <ul class="first_nav">
                    <li><a href="/" class="mni_b"><?=t('app', 'Home page')?></a></li>
                </ul>
            </nav>
            <section class="gray_wrapper">
                <section class="content">
                    <?= \frontend\widgets\Alert::widget(); ?>
                    <div>
                        <header class="header_notification may_close" style="display:none;">
                            <a class="close_header"><i></i></a>
                            <div id="errorBlock"></div>
                        </header>
                    </div>

                    <section class="cont">
                        <?=$content?>
                    </section>
                </section>
            </section>
        </div>
        <?=$this->render('_footer')?>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>