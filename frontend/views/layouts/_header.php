<?php

use frontend\widgets\TariffNotification;
use yii\helpers\Html;
use frontend\modules\tenant\models\UserDispetcher;
use \app\modules\tenant\models\User;
use yii\helpers\ArrayHelper;
?>

<div style="display: none">
    <div id="dispetcher_p">
        <h2><?= t('user', 'Reason') ?></h2>
        <ul class="pause_list">
            <?
            $reasons = UserDispetcher::getPauseReason();
            foreach ($reasons as $key => $value) {
                echo '<li><input type="radio" name="pause" id="' . $key . '"/><label for="' . $key . '">' . Html::encode($value) . '</label></li>';
            }
            ?>
        </ul>
        <div style="text-align: center">
            <button id="confirm_pause" disabled><?= t('app', 'Confirm'); ?></button>
        </div>
    </div>
</div>

<header class="main_header">
    <div class="header_content">
        <?php if (User::isCurrentPartnerEmployee()) { ?>
        <div class="company-name"><?=ArrayHelper::getValue($this->params, 'partnerCompanyName')?></div>
        <?php }else{ ?>
        <a class="logo_header" href="/">
            <img src="<?= Yii::getAlias('@web') . app()->params['frontend.layout.images.logo'] ?>" alt="<?= app()->name ?>"/>
        </a>
        <?php } ?>
        <? if (!Yii::$app->user->isGuest): ?>
            <div class="hc_main">
                <div id="dispetcher_buttons" class="hcm_right">

                    <? if (UserDispetcher::isUserDispetcher()): ?>
                        <? if (UserDispetcher::isDispetcherOffline()): ?>
                            <a id="dispetcher_start_work" class="button green_button"><?= t('user', 'Start work') ?></a>
                        <? endif ?>
                        <? if (UserDispetcher::isDispetcherOnline()): ?>
                            <span id="dispetcher_work_time"
                                  class="hcmr_time"><?= Html::encode(UserDispetcher::getDispetcherOnlineTime()); ?></span>
                            <a href="#dispetcher_p" id="dispetcher_pause" class="button green_button"><?= t('user',
                                    'Pause') ?></a>
                            <a id="dispetcher_stop" class="button red_button"><?= t('user', 'End work') ?></a>
                        <? endif ?>
                        <? if (UserDispetcher::isDispetcherOnPause()): ?>
                            <span id="dispetcher_work_time2"
                                  class="hcmr_time"><?= Html::encode(UserDispetcher::getDispetcherOnlineTime()) ?></span>
                            <a id="dispetcher_pause_time"
                               class="button green_button"><?= Html::encode(UserDispetcher::getDispetcherPauseTime()) ?></a>
                            <a id="dispetcher_stop" class="button red_button"><?= t('user', 'End work') ?></a>
                        <? endif ?>
                    <? endif ?>

                    <div class="hcmr_user">
                        <a class="a_sc"><?= Html::encode(getUserLastName()); ?></a>
                        <div class="b_sc">
                            <? if (!Yii::$app->user->can(User::ROLE_EXTRINSIC_DISPATCHER)): ?>
                            <a href="<?= app()->urlManager->createUrl([
                                '/tenant/user/update/',
                                'id' => user()->id,
                            ]) ?>"><?= t('user', 'Profile') ?></a>
                            <? endif ?>
                            <a id="logout" href="<?= app()->urlManager->createUrl('/tenant/user/logout') ?>"
                               data-dispatcher="<?= user()->is_dispatcher() ? 1 : 0 ?>"><?= t('user', 'Logout') ?></a>
                        </div>
                    </div>

                </div>
                <!--<a href="#" class="open_mes header_alerts cboxElement" data-mtid="mt01"><i></i><span>20</span></a>-->
                <?php if (!User::isCurrentPartnerEmployee()) { ?>
                  <a href="#messenger" class="open_mes header_mes cboxElement" data-mtid="mt02"><i></i><span>0</span></a>
                <?php } ?>
                <!--<a href="#" class="open_mes header_sos cboxElement" data-mtid="mt03"><i></i><span>20</span></a>-->
                        <?= TariffNotification::widget(); ?>
                <!--<a href="#messenger" class="open_mes header_alerts" data-mtid="mt01"><i></i><span>0</span></a> -->
                <!--<a href="#messenger" class="open_mes header_sos" data-mtid="mt03"><i></i><span>0</span></a> -->
                <? // if (!UserDispetcher::isUserDispetcher()): ?>
                <? // endif ?>
                <?= $this->render('chat') ?>
            </div>
        <? endif ?>
    </div>
</header>

