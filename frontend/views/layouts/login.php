<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

frontend\modules\tenant\assets\LoginAsset::register($this);
$this->registerJsFile('/js/functions.js');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= app()->language ?>">
    <head>
        <?php $this->head() ?>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>

        <? if (app()->params['frontend.layout.images.includeAppleTouchIcons']): ?>
            <link rel="apple-touch-icon" href="/images/icon76.png" sizes="76x76">
            <link rel="apple-touch-icon" href="/images/icon120.png" sizes="120x120">
            <link rel="apple-touch-icon" href="/images/icon152.png" sizes="152x152">
            <link rel="apple-touch-icon" href="/images/icon180.png" sizes="180x180">
        <? endif ?>

        <link rel="shortcut icon" href="<?= app()->params['frontend.layout.images.favicon'] ?>">
    </head>
    <body>
        <?php $this->beginBody() ?>
        <div class="wrapper">
            <div class="scroll-wrapper">
                <div class="table-wrapper">
                    <?= $content ?>
                </div>
            </div>
        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
