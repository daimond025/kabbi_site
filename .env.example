# Framework
# ----------
YII_DEBUG=1
YII_ENV="dev"

APPLICATION_NAME="name"

ENVIRONMENT_NAME="name"

# Databases
# ----------
DB_MAIN_USERNAME="username"
DB_MAIN_PASSWORD="password"
DB_MAIN_DSN="mysql:host=localhost;dbname=db"
DB_MAIN_SCHEMA_CACHE_ENABLE=1
DB_MAIN_SCHEMA_CACHE_DURATION=3600


DB_PAYGATE_DSN="mysql:host=localhost;dbname=db"
DB_PAYGATE_USERNAME="username"
DB_PAYGATE_PASSWORD="password"


DB_MAIN_SLAVE_ENABLE=0
DB_MAIN_SLAVE_USERNAME="username"
DB_MAIN_SLAVE_PASSWORD="password"
DB_MAIN_SLAVE_DSN="mysql:host=localhost;dbname=db"

DB_MIGRATION_HOST="localhost"
DB_MIGRATION_PORT=3306
DB_MIGRATION_NAME="dbname"
DB_MIGRATION_USERNAME="user"
DB_MIGRATION_PASSWORD="******"

DB_KAMAILIO_USERNAME="username"
DB_KAMAILIO_PASSWORD="password"
DB_KAMAILIO_DSN="mysql:host=localhost;dbname=db"

ELASTICSEARCH_HTTP_ADDRESS="localhost:9200"

MONGODB_MAIN_DSN="mongodb://localhost/db"

REDIS_MAIN_HOST="localhost"
REDIS_MAIN_PORT=6379
REDIS_MAIN_DATABASE_WORKERS=0
REDIS_MAIN_DATABASE_ORDERS_ACTIVE=1
REDIS_MAIN_DATABASE_PHONE_SCENARIO=2
REDIS_MAIN_DATABASE_ACTIVE_CALL=3
REDIS_MAIN_DATABASE_QUEUE_CALL=4
REDIS_MAIN_DATABASE_ACTIVE_DISPATCHER=5
REDIS_MAIN_DATABASE_FREE_DISPATCHER_QUEUE=6
REDIS_MAIN_DATABASE_WORKER_SHIFT=7
REDIS_MAIN_DATABASE_PARKING_HASH_TABLE=8
REDIS_MAIN_DATABASE_SESSION=10
REDIS_MAIN_DATABASE_DISPATCHER_LIVE_NOTICE=11
REDIS_MAIN_DATABASE_ORDER_EVENT=12
REDIS_MAIN_DATABASE_WORKER_ORDERS=13
REDIS_MAIN_DATABASE_CHECK_STATUS=15

REDIS_CACHE_HOST="localhost"
REDIS_CACHE_PORT=6379
REDIS_CACHE_DATABASE_MAIN=0
REDIS_CACHE_DATABASE_CHECK_STATUS=15
REDIS_CACHE_KEY_PREFIX="site"

# Gearman
# --------
GEARMAN_MAIN_HOST="localhost"
GEARMAN_MAIN_PORT=4730

# RabbitMQ
# ---------
RABBITMQ_MAIN_HOST="localhost"
RABBITMQ_MAIN_PORT=5672
RABBITMQ_MAIN_USER="user"
RABBITMQ_MAIN_PASSWORD="password"

# Consul
# ---------
CONSUL_MAIN_HOST="localhost"
CONSUL_MAIN_PORT=8500

# TODO Необходимо провести рефакторинг конфигураций common, frontend, landing, backend (содержимое переменных не соответствует их типам)

# Common
# -------
COMMON_UPLOAD_DIR="upload"
COMMON_UPLOAD_SCAN_DIR="scan"
COMMON_ADMIN_EMAIL="email"
COMMON_INFO_EMAIL="email"
COMMON_SUPPORT_PHONE="phone"
COMMON_SUPPORT_EMAIL="email"
COMMON_SUPPORT_EMAIL2="email"
COMMON_SALES_PHONE="phone"
COMMON_SALES_EMAIL="email"
COMMON_LANDING_URL="http://localhost"
COMMON_SITE_HOST="localhost"
COMMON_SITE_PROTOCOL="https"
COMMON_SITE_DOMAIN="localhost"
COMMON_DEMO_TARIFF_VALIDITY=5

# Frontend
# ---------
FRONTEND_COOKIE_VALIDATION_KEY="key"
FRONTEND_SERVICE_CHAT_EXTERNAL_URL="https://locahost:3003/"
FRONTEND_UPLOAD_DIR="upload"
FRONTEND_UPLOAD_CALL_RECORD="http://localhost:80/"
FRONTEND_UPLOAD_SCAN_DIR="scan"
FRONTEND_LAYOUT_SHOW_FOOTER_URLS=1
FRONTEND_LAYOUT_IMAGES_INCLUDE_APPLE_TOUCH_ICONS=1
FRONTEND_LAYOUT_IMAGES_LOGO="/images/logo.png"
FRONTEND_LAYOUT_IMAGES_FAVICON="/images/favicon.ico"

# Landing
# --------
LANDING_COOKIE_VALIDATION_KEY="key"
LANDING_UPLOAD_DIR="/upload/"
LANDING_CONTACTS_SKYPE="skype"
LANDING_CONTACTS_PHONE="phone"
LANDING_CONTACTS_EMAIL="email"
LANDING_CONTACTS_PARTNERS_EMAIL="email"

# Backend
# --------
BACKEND_COOKIE_VALIDATION_KEY="key"
BACKEND_UPLOAD_DIR="/upload/"

# API Autocomplete
# -----------------
API_AUTOCOMPLETE_URL="http://localhost:3100/"

# API Order
# ----------
API_ORDER_URL="http://localhost:80/"

# API Paygate
# ------------
API_PAYGATE_URL="http://localhost:80/"

# API Phone
# ----------
API_PHONE_URL="http://localhost:80/"

# API Push
# ---------
API_PUSH_URL="http://localhost:8083/"

# API Service
# ------------
API_SERVICE_URL="http://localhost:80/"

# API Voip
# ---------
API_VOIP_URL="http://localhost:8088/"

# API Worker
# -----------
API_WORKER_URL="http://localhost:80/"

# API Cashdesk
# -----------
API_CASHDESK_URL="http://localhost:80/"

# Chat service
# -------------
SERVICE_CHAT_URL="http://localhost:3003/"

# Engine service
# ---------------
SERVICE_ENGINE_URL="http://localhost:8087/"

# Softphone
# ----------
SOFTPHONE_PROTOCOL="https"
SOFTPHONE_DOMAIN="localhost"
SOFTPHONE_SUB_DOMAIN="sub"
SOFTPHONE_PORT="5060"
SOFTPHONE_CALL_URI="/order/operator/call/?did=%CALLDST%&clid=%CALLSRC%&call_id=%CALLID%"
SOFTPHONE_ASTERISK_HOST="localhost"
SOFTPHONE_UPDATE_SIP_USER_URL="${API_VOIP_URL}updatesipuser/"
SOFTPHONE_UPDATE_SIP_DOMAIN_URL="${API_VOIP_URL}updatesipdomains/"

# Support mail
# -------------
MAIL_SUPPORT_SUBJECT_BACKEND="BACKEND ERROR (LOCAL)"
MAIL_SUPPORT_SUBJECT_FRONTEND="FRONTEND ERROR (LOCAL)"
MAIL_SUPPORT_SUBJECT_LANDING="LANDING ERROR (LOCAL)"

MAIL_SUPPORT_HOST="localhost"
MAIL_SUPPORT_PORT=465
MAIL_SUPPORT_USERNAME="${COMMON_ADMIN_EMAIL}"
MAIL_SUPPORT_PASSWORD="password"
MAIL_SUPPORT_ENCRYPTION="ssl"

# Dispatch mail
# --------------
MAIL_DISPATCH_HOST="localhost"
MAIL_DISPATCH_PORT=465
MAIL_DISPATCH_USERNAME="${COMMON_INFO_EMAIL}"
MAIL_DISPATCH_PASSWORD="password"
MAIL_DISPATCH_ENCRYPTION="ssl"

# CURL
# -----
CURL_CONNECT_TIMEOUT=15
CURL_TIMEOUT=30

# SMS providers
# --------------
SMS_ALPHASMS_URL="https://alphasms.ua/"
SMS_BSGSMS_URL="http://app.bsg.hk/"
SMS_BULKSMS_URL="http://gw1.unifun.com:11480/"
SMS_CLICKATELL_URL="https://platform.clickatell.com/"
SMS_COMTASS_URL="http://196.202.134.90/"
SMS_GATEWAYSMS_URL="http://91.212.89.137/"
SMS_GOSMS_URL="https://app.gosms.cz/"
SMS_IBATELE_LK_URL="https://lk.ibatele.com/"
SMS_IBATELE_SMPP_URL="http://smpp.ibatele.com/"
SMS_MAGFA_URL="http://sms.magfa.com/"
SMS_MARADIT_URL="http://apps.lsim.az/"
SMS_MEDIASEND_URL="http://188.225.75.178/"
SMS_MEGAFON_TJ_SMPP_HOST="10.241.201.184"
SMS_MEGAFON_TJ_SMPP_PORT=2775
SMS_NIKITA_URL="http://31.47.195.66:80/"
SMS_NIKITA_KG_URL="https://smspro.nikita.kg/"
SMS_PROMOSMS_URL="http://sms.promosms.ru:26676/"
SMS_SMSBROKER_URL="http://91.204.239.42:8081/"
SMS_SMSC_URL="http://smsc.ru/"
SMS_SMSLINE_URL="https://api.smsline.by/"
SMS_SMSONLINE_URL="https://bulk.sms-online.com/"
SMS_EYELINESMS_URL="https://sads.whoisd-blr.eyelinecom.com/"
SMS_IQSMS_HTTP_HOST="json.gate.iqsms.ru"
SMS_IQSMS_HTTP_PORT=80
SMS_IQSMS_BALANCE_HTTP_HOST="api.iqsms.ru"
SMS_IQSMS_BALANCE_HTTP_PORT=80
SMS_MSMSMS_URL="https://api.msm.az/"
SMS_QTSMS_SSL_HOST="go.qtelecom.ru"
SMS_QTSMS_SSL_PORT=443
SMS_QTSMS_KUBAN_SSL_HOST="kuban.qtelecom.ru"
SMS_QTSMS_KUBAN_SSL_PORT=443
SMS_SAPSANSMS_URL="http://lk.sms-sapsan.ru/"
SMS_SEMYSMS_NET_URL="https://semysms.net/"

# Sentry
# --------------
SENTRY_DSN="https://<key>:<secret>@sentry.io/<project>"