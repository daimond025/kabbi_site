<?php

namespace console\models;

use yii\mongodb\ActiveRecord;

/**
 * Class OrderStat
 * @package console\models
 *
 * @property array $statistics
 * @property string $city_id
 * @property string $date
 * @property string $tenant_id
 * @property string $position_id
 * @property int $timestamp
 */
class OrderStat extends ActiveRecord
{

    /**
     * @return string the name of the index associated with this ActiveRecord class.
     */
    public static function collectionName()
    {
        return 'order_stat';
    }

    /**
     * @return array list of attribute names.
     */
    public function attributes()
    {
        return ['_id', 'statistics', 'city_id', 'timestamp', 'tenant_id', 'date', 'currency_id', 'position_id'];
    }

}
