<?php

use yii\db\Schema;
use yii\db\Migration;

class m150604_080333_tbl_default_settings extends Migration
{
    public function up()
    {
        $this->insert('{{%default_settings}}', ['name' => 'CLIENT_APP_PAGE', 'type' => 'system']);
    }

    public function down()
    {
        $this->delete('{{%default_settings}}', ['name' => 'CLIENT_APP_PAGE']);
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
