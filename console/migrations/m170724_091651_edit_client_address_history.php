<?php

use yii\db\Migration;

class m170724_091651_edit_client_address_history extends Migration
{
    const TABLE_NAME = '{{%client_address_history}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'street', $this->string(255)->notNull());
        $this->addColumn(self::TABLE_NAME, 'lat', $this->float()->notNull());
        $this->addColumn(self::TABLE_NAME, 'lon', $this->float()->notNull());

        $this->dataFill();
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'street');
        $this->dropColumn(self::TABLE_NAME, 'lat');
        $this->dropColumn(self::TABLE_NAME, 'lon');
    }

    protected function dataFill()
    {
        $address = $this->findAddress();

        foreach ($address as $item) {
            $data = $this->parseAddress($item['address']);
            if ($data) {
                $this->update(self::TABLE_NAME, $data, ['history_id' => $item['history_id']]);
            }
        }
    }

    protected function parseAddress($address)
    {
        $address = json_decode($address, true);

        if (!$address) {
            return null;
        }

        return [
            'street' => $address['address']['label'],
            'lat'    => $address['address']['lat'],
            'lon'    => $address['address']['lon'],
        ];
    }

    protected function findAddress()
    {
        return (new \yii\db\Query())->from(self::TABLE_NAME)->all();

    }
}
