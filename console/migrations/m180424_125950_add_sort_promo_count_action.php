<?php

use yii\db\Migration;

class m180424_125950_add_sort_promo_count_action extends Migration
{

    const TABLE_NAME = '{{%promo_action_count}}';

    const COLUMN_NAME = 'sort';

    const COLUMN_ACTION_COUNT_ID = 'action_count_id';

    public function safeUp()
    {

        $this->update(self::TABLE_NAME, [self::COLUMN_NAME => 1], [self::COLUMN_ACTION_COUNT_ID => 1]);
        $this->update(self::TABLE_NAME, [self::COLUMN_NAME => 2], [self::COLUMN_ACTION_COUNT_ID => 3]);
        $this->update(self::TABLE_NAME, [self::COLUMN_NAME => 3], [self::COLUMN_ACTION_COUNT_ID => 2]);


    }

    public function safeDown()
    {
        return true;
    }
}
