<?php

use yii\db\Schema;
use yii\db\Migration;

class m150729_113551_tbl_operation extends Migration
{
    public function up()
    {
        $this->addColumn('{{%operation}}', 'owner_name', Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('{{%operation}}', 'owner_name');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
