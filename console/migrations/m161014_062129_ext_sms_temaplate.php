<?php

use yii\db\Migration;

class m161014_062129_ext_sms_temaplate extends Migration
{
    const TABLE_NAME = '{{%default_sms_template}}';

    private $temaplateList = [
        2 => [
            'old' => '#TAXI_NAME# - Название службы такси<br>#BRAND# - Марка<br>#MODEL# - Модель<br>'
                . '#COLOR# - Цвет<br>#NUMBER# - Гос. номер<br>#TIME# - Время',
            'new' => '#TAXI_NAME# - Название службы такси<br>#BRAND# - Марка<br>#MODEL# - Модель<br>'
                . '#PHONE# - Телефон исполнителя<br>#COLOR# - Цвет<br>#NUMBER# - Гос. номер<br>#TIME# - Время',
        ],
        3 => [
            'old' => '#TAXI_NAME# - Название службы такси<br>#BRAND# - Марка<br>#MODEL# - Модель<br>#COLOR# - Цвет<br>'
                . '#NUMBER# - Гос. номер',
            'new' => '#TAXI_NAME# - Название службы такси<br>#BRAND# - Марка<br>#MODEL# - Модель<br>#COLOR# - Цвет<br>'
                . '#NUMBER# - Гос. номер<br>#PHONE# - Телефон исполнителя',
        ],
        10 => [
            'old' => '#TAXI_NAME# - Название службы такси <br>#BRAND# - Марка<br>#MODEL# - Модель',
            'new' => '#TAXI_NAME# - Название службы такси <br>#BRAND# - Марка<br>#MODEL# - Модель<br>#PHONE# - Телефон исполнителя',
        ],
        11 => [
            'old' => '#TAXI_NAME# - Название службы такси<br>#BRAND# - Марка<br>#MODEL# - Модель<br>#COLOR# - Цвет'
                . '<br>#NUMBER# - Гос. номер',
            'new' => '#TAXI_NAME# - Название службы такси<br>#BRAND# - Марка<br>#MODEL# - Модель<br>#COLOR# - Цвет'
                . '<br>#NUMBER# - Гос. номер<br>#PHONE# - Телефон исполнителя',
        ],
    ];

    public function up()
    {
        foreach ($this->temaplateList as $template_id => $item) {
            $this->update(self::TABLE_NAME, ['description' => str_replace('<br>', '<br>' . PHP_EOL, $item['new'])],
                ['template_id' => $template_id]);
        }
    }

    public function down()
    {
        foreach ($this->temaplateList as $template_id => $item) {
            $this->update(self::TABLE_NAME, ['description' => str_replace('<br>', '<br>' . PHP_EOL, $item['old'])],
                ['template_id' => $template_id]);
        }
    }
}
