<?php

use yii\db\Schema;
use yii\db\Migration;

class m151029_094215_create_client_bonus_tables extends Migration
{
    const BONUS_TABLE = '{{%client_bonus}}';
    const BONUS_HAS_TARIFF_TABLE = '{{%client_bonus_has_tariff}}';
    const CLIENT_HAS_BONUS_TABLE = '{{%client_has_bonus}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        if ($this->db->schema->getTableSchema(self::CLIENT_HAS_BONUS_TABLE, true)) {
            $this->dropTable(self::CLIENT_HAS_BONUS_TABLE);
        }

        if ($this->db->schema->getTableSchema(self::BONUS_TABLE, true)) {
            $this->dropTable(self::BONUS_TABLE);
        }


        $this->createTable(self::BONUS_TABLE, [
            'bonus_id' => $this->primaryKey(),
            'city_id' => 'int(11) UNSIGNED NOT NULL',
            'class_id' => 'tinyint(3) UNSIGNED NOT NULL',
            'tenant_id' => 'int(11) UNSIGNED NOT NULL',
            'name' => $this->string()->notNull(),
            'actual_date' => $this->text(),
            'bonus_type' => "ENUM('BONUS', 'PERCENT') NOT NULL DEFAULT 'PERCENT'",
            'bonus' => $this->decimal(10, 2)->notNull(),
            'min_cost' => $this->decimal(10, 2)->notNull()->defaultValue(0),
            'payment_method' => "ENUM('FULL', 'PARTIAL') NOT NULL DEFAULT 'FULL'",
            'max_payment_type' => "ENUM('BONUS', 'PERCENT') NOT NULL DEFAULT 'PERCENT'",
            'max_payment' => $this->decimal(10, 2)->defaultValue(0),
            'bonus_app_type' => "ENUM('BONUS', 'PERCENT') NOT NULL DEFAULT 'PERCENT'",
            'bonus_app' => $this->decimal(10, 2)->notNull()->defaultValue(0),
            'blocked' => $this->integer()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
        $this->createIndex('idx_client_bonus__name', self::BONUS_TABLE, 'name', true);
        $this->addForeignKey('fk_client_bonus__city_id', self::BONUS_TABLE, 'city_id',
                '{{%city}}', 'city_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_client_bonus__class_id', self::BONUS_TABLE, 'class_id',
                '{{%car_class}}', 'class_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_client_bonus__tenant_id', self::BONUS_TABLE, 'tenant_id',
                '{{%tenant}}', 'tenant_id', 'CASCADE', 'CASCADE');

        $this->createTable(self::BONUS_HAS_TARIFF_TABLE, [
            'id' => $this->primaryKey(),
            'bonus_id' => $this->integer()->notNull(),
            'tariff_id' => 'int(11) UNSIGNED NOT NULL',
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey('fk_client_bonus_has_tariff__bonus_id', self::BONUS_HAS_TARIFF_TABLE, 'bonus_id',
                self::BONUS_TABLE, 'bonus_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_client_bonus_has_tariff__tariff_id', self::BONUS_HAS_TARIFF_TABLE, 'tariff_id',
                '{{%taxi_tariff}}', 'tariff_id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable(self::BONUS_HAS_TARIFF_TABLE);
        $this->dropTable(self::BONUS_TABLE);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
