<?php

use yii\db\Migration;

class m160908_061925_add_default_settings_for_worker extends Migration
{
    public function up()
    {
        $this->update('{{%default_settings}}', ['name' => 'WORKER_MIN_BALANCE'], ['name' => 'WORKER_MIN_BAlANCE']);
        $this->batchInsert('{{%default_settings}}', ['name', 'value', 'type'], [
            ['SHOW_CHAT_DISPATCHER_EXIT', 1, 'drivers'],
            ['SHOW_CHAT_DISPATCHER_SHIFT', 1, 'drivers'],
            ['SHOW_GENERAL_CHAT_SHIFT', 1, 'drivers'],
            ['ALLOW_USE_BANK_CARD', 1, 'drivers'],
        ]);
    }

    public function down()
    {
        $this->update('{{%default_settings}}', ['name' => 'WORKER_MIN_BAlANCE'], ['name' => 'WORKER_MIN_BALANCE']);
        $this->delete('{{%default_settings}}', [
            'name' => [
                'SHOW_CHAT_DISPATCHER_EXIT',
                'SHOW_CHAT_DISPATCHER_SHIFT',
                'SHOW_GENERAL_CHAT_SHIFT',
                'ALLOW_USE_BANK_CARD',
            ],
            'value' => 1,
            'type' => 'drivers',
        ]);
    }

}
