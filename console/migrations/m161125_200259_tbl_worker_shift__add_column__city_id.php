<?php

use yii\db\Migration;

class m161125_200259_tbl_worker_shift__add_column__city_id extends Migration
{
    const TABLE_WORKER_SHIFT = '{{%worker_shift}}';
    const TABLE_CITY = '{{%city}}';

    const COLUMN_CITY_ID = 'city_id';
    const FOREIGN_KEY_CITY_ID = 'fk_worker_shift__city_id';

    const SQL_FILL_TABLE = <<<SQL
update tbl_worker_shift s, tbl_worker w
set s.city_id = w.city_id
where s.worker_id = w.worker_id;
SQL;

    public function up()
    {
        $this->addColumn(
            self::TABLE_WORKER_SHIFT,
            self::COLUMN_CITY_ID,
            $this->integer(11)->unsigned()
        );

        $this->db->createCommand(self::SQL_FILL_TABLE)->execute();

        $this->addForeignKey(
            self::FOREIGN_KEY_CITY_ID,
            self::TABLE_WORKER_SHIFT,
            self::COLUMN_CITY_ID,
            self::TABLE_CITY,
            self::COLUMN_CITY_ID,
            'CASCADE',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(self::FOREIGN_KEY_CITY_ID, self::TABLE_WORKER_SHIFT);
        $this->dropColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_CITY_ID);
    }

}
