<?php

use yii\rbac\DbManager;
use yii\db\Migration;
use yii\di\Instance;

class m170714_111851_reports_rbac_init extends Migration
{
    /** @var \yii\rbac\DbManager */
    public $authManager;

    public function init()
    {
        parent::init();

        $this->authManager = Instance::ensure([
            'class'           => DbManager::className(),
            'itemTable'       => '{{%admin_auth_item}}',
            'itemChildTable'  => '{{%admin_auth_item_child}}',
            'assignmentTable' => '{{%admin_auth_assignment}}',
            'ruleTable'       => '{{%admin_auth_rule}}',
        ]);
    }

    public function up()
    {
        $this->createPermission('tenantPaymentsReport');
        $this->createPermission('tenantTariffsReport');
    }

    public function down()
    {
        $this->deleteComplexPermission('tenantPaymentsReport');
        $this->deleteComplexPermission('tenantTariffsReport');
    }

    protected function deleteComplexPermission($name)
    {
        $this->deletePermission($name);
        $this->deleteReadPermission($name);
    }

    protected function deleteReadPermission($name)
    {
        $name = 'read_' . $name;

        return $this->deletePermission($name);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {

    }

    public function safeDown()
    {
    }
    */

    /**
     * Creates new permission.
     *
     * @param  string $name The name of the permission
     * @param  string $description The description of the permission
     * @param  string|null $ruleName The rule associated with the permission
     * @param  mixed|null $data The additional data associated with the permission
     * @return \yii\rbac\Permission
     */
    protected function createPermission($name, $description = '', $ruleName = null, $data = null)
    {
        $permission = $this->authManager->createPermission($name);

        $permission->description = $description;
        $permission->ruleName = $ruleName;
        $permission->data = $data;

        $this->authManager->add($permission);

        return $permission;
    }

    protected function deletePermission($name){
        $permission = $this->authManager->createPermission($name);
        return $this->authManager->remove($permission);
    }


}
