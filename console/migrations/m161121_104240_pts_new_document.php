<?php

use yii\db\Migration;

class m161121_104240_pts_new_document extends Migration
{
    const TABLE_DOCUMENTS = '{{%document}}';
    const TABLE_PTS = '{{%worker_pts}}';
    const TABLE_HAS_POSITION  ='{{%position_has_document}}';
    const TABLE_HAS_DOCUMENT = '{{%worker_has_document}}';

    const CODE = 8;

    public function up()
    {
        $this->insert(self::TABLE_DOCUMENTS, [
            'document_id' => self::CODE,
            'name' => 'PTS',
            'code' => self::CODE,
        ]);

        $this->createTable(self::TABLE_PTS, [
            'pts_id' => $this->primaryKey(),
            'worker_document_id' => $this->integer(11),
            'series' => $this->string(5),
            'number' => $this->string(10),
        ]);
        $this->addForeignKey(
            'fk_tbl_worker_pts_tbl_worker_has_document8_idx',
            self::TABLE_PTS,
            'worker_document_id',
            self::TABLE_HAS_DOCUMENT,
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->insert(self::TABLE_HAS_POSITION, [
            'position_id' => 1,
            'document_id' => self::CODE,
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_HAS_POSITION, [
            'document_id' => self::CODE
        ]);

        $this->dropTable(self::TABLE_PTS);

        $this->delete(self::TABLE_DOCUMENTS, [
            'document_id' => self::CODE
        ]);
    }

}
