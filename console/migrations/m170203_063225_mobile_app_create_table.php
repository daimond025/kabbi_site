<?php

use yii\db\Migration;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class m170203_063225_mobile_app_create_table extends Migration
{

    const TABLE_NAME = '{{%mobile_app}}';
    const TABLE_NAME_TENANT = '{{%tenant}}';

    const TABLE_NAME_DEFAULT_SETTING = '{{%default_settings}}';
    const TABLE_NAME_TENANT_SETTING = '{{%tenant_setting}}';


    const API_KEY = 'CLIENT_API_KEY';
    const PUSH_KEY_ANDROID = 'PUSH_ANDROID_CLIENT_API_KEY';
    const PUSH_KEY_IOS = 'PUSH_IOS_CLIENT_API_KEY';
    const LINK_IOS = 'APP_STORE_CLIENT_APP_LINK';
    const LINK_ANDROID = 'GOOGLE_PLAY_CLIENT_APP_LINK';
    const PAGE_INFO = 'CLIENT_APP_PAGE';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'app_id'           => $this->primaryKey()->unsigned(),
            'tenant_id'        => $this->integer(11)->unsigned()->notNull(),
            'name'             => $this->string(100)->notNull(),
            'sort'             => $this->integer(6)->notNull()->defaultValue(100),
            'active'           => $this->integer('1')->notNull()->defaultValue(0),
            'api_key'          => $this->string(250),
            'push_key_android' => $this->text(),
            'push_key_ios'     => $this->text(),
            'link_android'     => $this->string(250),
            'link_ios'         => $this->string(250),
            'page_info'        => $this->text(),
        ]);

        $this->addForeignKey('fk_mobile_app__tenant_id', self::TABLE_NAME, 'tenant_id',
            '{{%tenant}}', 'tenant_id', 'CASCADE', 'CASCADE');

        $this->dataFill();
    }

    public function down()
    {
        $this->dropForeignKey('fk_mobile_app__tenant_id', self::TABLE_NAME);
        $this->dropTable(self::TABLE_NAME);
    }


    /**
     * Получить настройки по-умолчанию
     *
     * @return array
     */
    private function getDefaultSetting()
    {
        $default = (array)(new Query())->select(['name', 'value'])
            ->from(self::TABLE_NAME_DEFAULT_SETTING)
            ->where([
                'name' => [
                    self::API_KEY,
                    self::PUSH_KEY_ANDROID,
                    self::PUSH_KEY_IOS,
                    self::LINK_ANDROID,
                    self::LINK_IOS,
                    self::PAGE_INFO,
                ],
            ])
            ->all();

        return ArrayHelper::map($default, 'name', 'value');
    }

    /**
     * Получить настройки арендатора
     *
     * @param string $tenantId
     *
     * @return array
     */
    private function getTenantSetting($tenantId)
    {
        $setting = (array)(new Query())->select(['name', 'value'])
            ->from(self::TABLE_NAME_TENANT_SETTING)
            ->where([
                'name'      => [
                    self::API_KEY,
                    self::PUSH_KEY_ANDROID,
                    self::PUSH_KEY_IOS,
                    self::LINK_ANDROID,
                    self::LINK_IOS,
                    self::PAGE_INFO,
                ],
                'tenant_id' => $tenantId,
            ])
            ->all();

        return ArrayHelper::map($setting, 'name', 'value');
    }

    /**
     * Получить список идентификаторов всех арендаторов
     *
     * @return array
     */
    private function getTenantList()
    {
        $tenantList = (array)(new Query())->select('tenant_id')
            ->from(self::TABLE_NAME_TENANT)
            ->all();

        return ArrayHelper::getColumn($tenantList, 'tenant_id');
    }

    // Заполнить данныех арендатора
    private function dataFillByTenant($tenantId, $defaultSetting)
    {
        $tenantSetting = $this->getTenantSetting($tenantId);

        $newSetting = [];

        foreach ($defaultSetting as $key => $item) {
            $newSetting[$key] = empty($tenantSetting[$key]) ? $item : $tenantSetting[$key];
        }

        $this->insert(self::TABLE_NAME, [
            'tenant_id'        => $tenantId,
            'name'             => 'Default',
            'active'           => 1,
            'api_key'          => $newSetting[self::API_KEY],
            'push_key_android' => $newSetting[self::PUSH_KEY_ANDROID],
            'push_key_ios'     => $newSetting[self::PUSH_KEY_IOS],
            'link_android'     => $newSetting[self::LINK_ANDROID],
            'link_ios'         => $newSetting[self::LINK_IOS],
            'page_info'        => $newSetting[self::PAGE_INFO],
        ]);
    }

    /**
     * Заполнить данные
     */
    private function dataFill()
    {
        $tenantList     = $this->getTenantList();
        $defaultSetting = $this->getDefaultSetting();

        foreach ($tenantList as $tenantId) {
            $this->dataFillByTenant($tenantId, $defaultSetting);
        }
    }
}
