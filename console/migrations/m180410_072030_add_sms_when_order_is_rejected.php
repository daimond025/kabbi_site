<?php

use yii\db\Migration;
use common\modules\tenant\models\DefaultClientPushNotification;

class m180410_072030_add_sms_when_order_is_rejected extends Migration
{
    const TABLE_DEFAULT = '{{%default_client_push_notification}}';
    const BASE_TABLE = '{{%client_push_notification}}';
    const STATUS_ID = '120';
    const TEXT = 'Извините, ваш заказ отменен';

    private $positionsId = [];

    public function safeUp()
    {
        $this->positionsId = \yii\helpers\ArrayHelper::getColumn(\common\modules\employee\models\position\Position::find()->all(),'position_id');

        foreach($this->positionsId as $positionId) {

            $model = new DefaultClientPushNotification(['text'=>self::TEXT,'type'=>self::STATUS_ID,'position_id'=>$positionId, 'description'=>'#TAXI_NAME# - Название вашей службы', 'params'=>'#TAXI_NAME#']);

            if(!$model->save()){
                dd($model->errors);
            }

        }

        return true;
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_DEFAULT,['type'=>self::STATUS_ID]);
        $this->delete(self::BASE_TABLE,['type'=>self::STATUS_ID]);

        return true;
    }
}
