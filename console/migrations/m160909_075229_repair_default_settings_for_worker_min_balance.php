<?php

use yii\db\Migration;

class m160909_075229_repair_default_settings_for_worker_min_balance extends Migration
{
    public function up()
    {
        $this->update('{{%default_settings}}', ['name' => 'WORKER_MIN_BAlANCE'], ['name' => 'WORKER_MIN_BALANCE']);
    }

    public function down()
    {
        $this->update('{{%default_settings}}', ['name' => 'WORKER_MIN_BALANCE'], ['name' => 'WORKER_MIN_BAlANCE']);
    }
    
}
