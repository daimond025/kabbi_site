<?php

use yii\db\Migration;

class m170127_085815_edit_parking_columns extends Migration
{
    const TABLE_NAME = '{{%parking}}';

    public function up()
    {
        $this->alterColumn(self::TABLE_NAME, 'in_district', $this->integer(10));
        $this->alterColumn(self::TABLE_NAME, 'out_district', $this->integer(10));
        $this->alterColumn(self::TABLE_NAME, 'in_distr_coefficient_type', 'ENUM("PERCENT", "MONEY")');
        $this->alterColumn(self::TABLE_NAME, 'out_distr_coefficient_type', 'ENUM("PERCENT", "MONEY")');
    }

    public function down()
    {
        $this->alterColumn(self::TABLE_NAME, 'out_distr_coefficient_type', $this->string(45));
        $this->alterColumn(self::TABLE_NAME, 'in_distr_coefficient_type', $this->string(45));
        $this->alterColumn(self::TABLE_NAME, 'out_district', $this->integer(3));
        $this->alterColumn(self::TABLE_NAME, 'in_district', $this->integer(3));
    }
}
