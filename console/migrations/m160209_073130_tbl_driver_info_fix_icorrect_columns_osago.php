<?php

use yii\db\Schema;
use yii\db\Migration;

class m160209_073130_tbl_driver_info_fix_icorrect_columns_osago extends Migration
{
    
    const TABLE_NAME = '{{%driver_info}}';
    
    public function up()
    {
        $this->dropColumn(self::TABLE_NAME, 'osago_series');
        $this->dropColumn(self::TABLE_NAME, 'osago_number');
        $this->dropColumn(self::TABLE_NAME, 'osago_start_date');
        $this->dropColumn(self::TABLE_NAME, 'osago_end_date');
        
        $this->addColumn(self::TABLE_NAME,'osago_series','VARCHAR(255)');
        $this->addColumn(self::TABLE_NAME,'osago_number','VARCHAR(255)');
        $this->addColumn(self::TABLE_NAME,'osago_start_date','DATE');
        $this->addColumn(self::TABLE_NAME,'osago_end_date','DATE');
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'osago_series');
        $this->dropColumn(self::TABLE_NAME, 'osago_number');
        $this->dropColumn(self::TABLE_NAME, 'osago_start_date');
        $this->dropColumn(self::TABLE_NAME, 'osago_end_date');
        
        $this->addColumn(self::TABLE_NAME,'osago_series','VARCHAR(3)');
        $this->addColumn(self::TABLE_NAME,'osago_number','INTEGER(11)');
        $this->addColumn(self::TABLE_NAME,'osago_start_date','TIMESTAMP');
        $this->addColumn(self::TABLE_NAME,'osago_end_date','TIMESTAMP');

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
