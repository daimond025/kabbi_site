<?php

use yii\db\Migration;

class m171027_124009_create_promo_code extends Migration
{

    const TABLE_NAME = '{{%promo_code}}';

    const COLUMN_PROMO_ID = 'promo_id';
    const COLUMN_CODE = 'code';

    const COLUMN_WORKER_ID = 'worker_id';
    const COLUMN_CAR_CLASS_ID = 'car_class_id';

    const FK_PROMO_CODE__WORKER_ID = 'fk_promo_code__worker_id';
    const FK_PROMO_CODE__CAR_CLASS_ID = 'fk_promo_code__car_class_id';

    const FK_PROMO_CODE__PROMO_ID = 'fk_promo_code__promo_id';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'code_id'                   => $this->primaryKey()->unsigned(),
            self::COLUMN_PROMO_ID       => $this->integer()->unsigned()->notNull(),
            self::COLUMN_CODE           => $this->string(255),
            'created_at'                => $this->integer()->unsigned()->notNull(),
            'updated_at'                => $this->integer()->unsigned()->notNull(),
            self::COLUMN_WORKER_ID      => $this->integer(10),
            self::COLUMN_CAR_CLASS_ID   => 'tinyint(3) UNSIGNED NULL',
        ]);

        $this->addForeignKey(self::FK_PROMO_CODE__PROMO_ID, self::TABLE_NAME,
            self::COLUMN_PROMO_ID, '{{%promo}}', self::COLUMN_PROMO_ID, 'CASCADE', 'CASCADE');

        $this->addForeignKey(self::FK_PROMO_CODE__WORKER_ID, self::TABLE_NAME, self::COLUMN_WORKER_ID,
            '{{%worker}}', self::COLUMN_WORKER_ID, 'CASCADE', 'CASCADE');

        $this->addForeignKey(self::FK_PROMO_CODE__CAR_CLASS_ID, self::TABLE_NAME, self::COLUMN_CAR_CLASS_ID, '{{%car_class}}', 'class_id', 'CASCADE', 'CASCADE');

        $this->createIndex('idx_promo_code__code', self::TABLE_NAME, self::COLUMN_CODE);

        $this->createIndex('idx_promo_code__worker_id', self::TABLE_NAME, self::COLUMN_WORKER_ID);
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_PROMO_CODE__CAR_CLASS_ID, self::TABLE_NAME);
        $this->dropForeignKey(self::FK_PROMO_CODE__WORKER_ID, self::TABLE_NAME);
        $this->dropForeignKey(self::FK_PROMO_CODE__PROMO_ID, self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
    }
}
