<?php

use yii\db\Migration;

class m180327_203543_create_table__paygate_profile extends Migration
{
    const TABLE_PAYGATE_PROFILE = '{{%paygate_profile}}';
    const TABLE_TENANT = '{{%tenant}}';
    const UNIQUE_INDEX = 'ui_paygate_profile';
    const FOREIGN_KEY_TENANT_ID = 'fk_paygate_profile_tenant_id';

    public function safeUp()
    {
        $this->createTable(self::TABLE_PAYGATE_PROFILE, [
            'id'         => $this->primaryKey(),
            'tenant_id'  => $this->integer(10)->unsigned()->notNull(),
            'profile'    => $this->string(32)->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createIndex(self::UNIQUE_INDEX, self::TABLE_PAYGATE_PROFILE, ['profile'], true);
        $this->addForeignKey(self::FOREIGN_KEY_TENANT_ID, self::TABLE_PAYGATE_PROFILE, 'tenant_id', self::TABLE_TENANT,
            'tenant_id', 'RESTRICT', 'RESTRICT');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_PAYGATE_PROFILE);
    }
}
