<?php

use yii\db\Migration;

class m170212_123506_chat_email_notify extends Migration
{
    const TABLE_USER = '{{%user}}';
    const COLUMN = 'chat_email_notify';

    public function up()
    {
        $this->addColumn(self::TABLE_USER, self::COLUMN, $this->integer());
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_USER, self::COLUMN);
    }


}
