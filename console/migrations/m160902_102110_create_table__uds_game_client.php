<?php

use yii\db\Migration;

class m160902_102110_create_table__uds_game_client extends Migration
{
    const TABLE_UDS_GAME_CLIENT = '{{%uds_game_client}}';
    const TABLE_CLIENT = '{{%client}}';
    const COLUMN_CLIENT_ID = 'client_id';
    const FOREIGN_KEY_CLIENT_ID = 'fk_uds_game_client__client_id';

    public function up()
    {
        $this->createTable(self::TABLE_UDS_GAME_CLIENT, [
            'id'         => $this->primaryKey(),
            'client_id'  => $this->integer(10)->unsigned()->notNull()->unique(),
            'profile_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(
            self::FOREIGN_KEY_CLIENT_ID,
            self::TABLE_UDS_GAME_CLIENT,
            self::COLUMN_CLIENT_ID,
            self::TABLE_CLIENT,
            self::COLUMN_CLIENT_ID,
            'CASCADE',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable(self::TABLE_UDS_GAME_CLIENT);
    }

}
