<?php

use yii\db\Migration;

class m170516_093247_tbl_referral__rename_columns extends Migration
{
    const TABLE_REFERRAL = '{{%referral}}';

    const COLUMN_INVITING_VALUE = 'inviting_value';
    const COLUMN_INVITING_TYPE = 'inviting_type';
    const COLUMN_INVITED_VALUE = 'invited_value';
    const COLUMN_INVITED_TYPE = 'invited_type';

    const COLUMN_REFERRER_BONUS = 'referrer_bonus';
    const COLUMN_REFERRER_BONUS_TYPE = 'referrer_bonus_type';
    const COLUMN_REFERRAL_BONUS = 'referral_bonus';
    const COLUMN_REFERRAL_BONUS_TYPE = 'referral_bonus_type';

    public function up()
    {
        $this->renameColumn(self::TABLE_REFERRAL, self::COLUMN_INVITING_VALUE, self::COLUMN_REFERRAL_BONUS);
        $this->renameColumn(self::TABLE_REFERRAL, self::COLUMN_INVITING_TYPE, self::COLUMN_REFERRAL_BONUS_TYPE);
        $this->renameColumn(self::TABLE_REFERRAL, self::COLUMN_INVITED_VALUE, self::COLUMN_REFERRER_BONUS);
        $this->renameColumn(self::TABLE_REFERRAL, self::COLUMN_INVITED_TYPE, self::COLUMN_REFERRER_BONUS_TYPE);
    }

    public function down()
    {
        $this->renameColumn(self::TABLE_REFERRAL, self::COLUMN_REFERRAL_BONUS, self::COLUMN_INVITING_VALUE);
        $this->renameColumn(self::TABLE_REFERRAL, self::COLUMN_REFERRAL_BONUS_TYPE, self::COLUMN_INVITING_TYPE);
        $this->renameColumn(self::TABLE_REFERRAL, self::COLUMN_REFERRER_BONUS, self::COLUMN_INVITED_VALUE);
        $this->renameColumn(self::TABLE_REFERRAL, self::COLUMN_REFERRER_BONUS_TYPE, self::COLUMN_INVITED_TYPE);
    }

}
