<?php

use yii\db\Migration;

class m170421_110011_tbl_default_client_push_notifications__add_unique_index extends Migration
{
    const TABLE_DEFAULT_CLIENT_PUSH_NOTIFICATIONS = '{{%default_client_push_notifications}}';
    const UNIQUE_INDEX = 'ui_default_client_push_notifications';

    public function up()
    {
        $this->createIndex(
            self::UNIQUE_INDEX,
            self::TABLE_DEFAULT_CLIENT_PUSH_NOTIFICATIONS,
            ['type', 'position_id'],
            true);
    }

    public function down()
    {
        $this->dropIndex(self::UNIQUE_INDEX, self::TABLE_DEFAULT_CLIENT_PUSH_NOTIFICATIONS);
    }

}
