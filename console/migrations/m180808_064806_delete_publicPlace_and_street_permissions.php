<?php

use yii\db\Migration;

class m180808_064806_delete_publicPlace_and_street_permissions extends Migration
{
    const TABLE_NAME = '{{%user_default_right}}';

    /** @var \yii\rbac\DbManager */
    public $authManager;
    public $permissions;

    public function init()
    {
        parent::init();

        $this->authManager = \yii\di\Instance::ensure([
            'class'           => \yii\rbac\DbManager::className(),
            'itemTable'       => '{{%auth_item}}',
            'itemChildTable'  => '{{%auth_item_child}}',
            'assignmentTable' => '{{%auth_assignment}}',
            'ruleTable'       => '{{%auth_rule}}',
        ]);
        $this->permissions = ['streets', 'publicPlace'];

    }

    public function safeUp()
    {
        $this->delete(self::TABLE_NAME, ['permission' => $this->permissions]);

        foreach ($this->permissions as $permission) {
            $this->deleteComplexPermission($permission);
        }
    }

    public function safeDown()
    {
        foreach ($this->permissions as $permission) {
            $this->createComplexPermission($permission);
        }
    }


    private function createComplexPermission($name, $description = '', $ruleName = null, $data = null)
    {
        $permission     = $this->createPermission($name, $description, $ruleName, $data);
        $permissionRead = $this->createReadPermission($name, $description, $ruleName, $data);

        return $this->authManager->addChild($permission, $permissionRead);
    }

    private function createReadPermission($name, $description = '', $ruleName = null, $data = null)
    {
        $name        = 'read_' . $name;
        $description = empty($description) ? '' : $description . ' (read)';

        return $this->createPermission($name, $description, $ruleName, $data);
    }

    private function createPermission($name, $description = '', $ruleName = null, $data = null)
    {
        $permission              = $this->authManager->createPermission($name);
        $permission->description = $description;
        $permission->ruleName    = $ruleName;
        $permission->data        = $data;
        $this->authManager->add($permission);

        return $permission;
    }


    private function deleteComplexPermission($name)
    {
        $this->deletePermission($name);
        $this->deleteReadPermission($name);
    }

    private function deleteReadPermission($name)
    {
        $name = 'read_' . $name;

        return $this->deletePermission($name);
    }

    private function deletePermission($name)
    {
        $permission = $this->authManager->createPermission($name);

        return $this->authManager->remove($permission);
    }
}
