<?php

use yii\db\Schema;
use yii\db\Migration;

class m150605_131758_transaction extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%client_transaction}}', 'value', 'FLOAT NOT NULL');
        $this->alterColumn('{{%client_company_transaction}}', 'value', 'FLOAT NOT NULL');
        $this->alterColumn('{{%driver_transaction}}', 'value', 'FLOAT NOT NULL');
        $this->alterColumn('{{%tenant_transaction}}', 'value', 'FLOAT NOT NULL');
    }

    public function down()
    {
        echo "m150605_131758_transaction cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
