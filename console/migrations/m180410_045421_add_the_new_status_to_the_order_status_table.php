<?php

use yii\db\Migration;

class m180410_045421_add_the_new_status_to_the_order_status_table extends Migration
{
    const TABLE_ORDER_STATUS = '{{%order_status}}';
    const NAME               = 'Canceled by worker';
    const STATUS_GROUP       = 'rejected';
    const DISPATCHER_SEES    = 1;
    const STATUS_ID          = 120;

    public function safeUp()
    {
        $this->insert(self::TABLE_ORDER_STATUS, [
            'status_id'       => self::STATUS_ID,
            'name'            => self::NAME,
            'status_group'    => self::STATUS_GROUP,
            'dispatcher_sees' => self::DISPATCHER_SEES
        ]);

        return true;
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_ORDER_STATUS, ['status_id' => self::STATUS_ID]);

        return true;
    }

}
