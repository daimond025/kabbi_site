<?php

use yii\db\Migration;

class m171027_123632_create_promo_period_type extends Migration
{
    const TABLE_NAME = '{{%promo_period_type}}';

    const COLUMN_PERIOD_TYPE = 'period_type_id';
    const COLUMN_NAME = 'name';

    const DEFAULT_VALUE = [
        ['No time limit'],
        ['Limited'],
    ];

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            self::COLUMN_PERIOD_TYPE => $this->primaryKey(),
            self::COLUMN_NAME        => $this->string(255)->notNull(),
        ]);

        $this->batchInsert(
            self::TABLE_NAME,
            [self::COLUMN_NAME],
            self::DEFAULT_VALUE
        );
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }

}
