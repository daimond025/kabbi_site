<?php

use yii\db\Schema;
use yii\db\Migration;

class m150916_103922_tbl_client_push_notifications extends Migration {

    const TABLE_NAME = '{{%client_push_notifications}}';

    public function up() {
        $this->createTable(self::TABLE_NAME, [
            'push_id' => $this->primaryKey(),
            'text' => $this->string(255)->notNull(),
            'type' => $this->text(45)->notNull(),
            'tenant_id' => $this->integer(5)->notNull(),
        ]);
    }

    public function down() {
        $this->dropTable(self::TABLE_NAME);
        echo self::TABLE_NAME . ' has been successfully dropped';
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
