<?php

use yii\db\Migration;

class m171006_121519_new_auto_geo_type_az extends Migration
{
    const TABLE_NAME = '{{%auto_geo_service_type}}';

    public function up()
    {
        $this->insert(self::TABLE_NAME, [
            'id'            => 8,
            'service_label' => 'Geobase of Azerbaijan in 3 languages (not updated)',
            'active'        => 1,
        ]);

    }

    public function down()
    {
        $this->delete(self::TABLE_NAME, ['id' => 8]);
        return true;
    }


}
