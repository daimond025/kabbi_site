<?php

use yii\db\Migration;

class m160906_132650_tbl_uds_game_order__add_column extends Migration
{
    const TABLE_UDS_GAME_ORDER = '{{%uds_game_order}}';
    const COLUMN_PROMO_CODE = 'promo_code';
    const COLUMN_CUSTOMER_ID = 'customer_id';

    public function up()
    {
        $this->alterColumn(self::TABLE_UDS_GAME_ORDER, self::COLUMN_PROMO_CODE, $this->string());
        $this->addColumn(self::TABLE_UDS_GAME_ORDER, self::COLUMN_CUSTOMER_ID, $this->string(20)->notNull());
    }

    public function down()
    {
        $this->alterColumn(self::TABLE_UDS_GAME_ORDER, self::COLUMN_PROMO_CODE, $this->string()->notNull());
        $this->dropColumn(self::TABLE_UDS_GAME_ORDER, self::COLUMN_CUSTOMER_ID);
    }

}
