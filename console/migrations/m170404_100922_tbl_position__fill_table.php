<?php

use common\modules\employee\models\position\Position;
use common\modules\tenant\modules\tariff\models\Module;
use yii\db\Migration;

class m170404_100922_tbl_position__fill_table extends Migration
{
    const TABLE_POSITION = '{{%position}}';
    const TABLE_MODULE = '{{%module}}';
    const TABLE_ORDER = '{{%order}}';

    const MODULE_TAXI_ID = 1;
    const MODULE_TRACK_ID = 2;
    const MODULE_COURIER_ID = 3;
    const MODULE_MASTER_ID = 4;

    const POSITION_TAXI_ID = 1;
    const POSITION_TRACK_ID = 2;
    const POSITION_COURIER_ID = 3;
    const POSITION_MASTER_ID = 4;

    const POSITION_OLD_TAXI_ID = 1;
    const POSITION_OLD_UAT_TRACK_ID = 2;
    const POSITION_OLD_UAT_COURIER_ID = 3;
    const POSITION_OLD_UAT_SUB_COURIER_ID = 4;

    const MODULES = [
        [
            'id'     => self::MODULE_TAXI_ID,
            'name'   => 'Taxi',
            'active' => 1,
            'code'   => 1,
        ],
        [
            'id'     => self::MODULE_TRACK_ID,
            'name'   => 'Truck taxi',
            'active' => 1,
            'code'   => 2,
        ],
        [
            'id'     => self::MODULE_COURIER_ID,
            'name'   => 'Courier',
            'active' => 1,
            'code'   => 3,
        ],
        [
            'id'     => self::MODULE_MASTER_ID,
            'name'   => 'Master',
            'active' => 1,
            'code'   => 4,
        ],
    ];

    const POSITIONS = [
        [
            'position_id'       => self::POSITION_TAXI_ID,
            'module_id'         => self::MODULE_TAXI_ID,
            'name'              => 'Taxi driver',
            'has_car'           => 1,
            'transport_type_id' => 1,
        ],
        [
            'position_id'       => self::POSITION_TRACK_ID,
            'module_id'         => self::MODULE_TRACK_ID,
            'name'              => 'Truck driver',
            'has_car'           => 1,
            'transport_type_id' => 2,
        ],
        [
            'position_id'       => self::POSITION_COURIER_ID,
            'module_id'         => self::MODULE_COURIER_ID,
            'name'              => 'Courier',
            'has_car'           => 0,
            'transport_type_id' => null,
        ],
        [
            'position_id'       => self::POSITION_MASTER_ID,
            'module_id'         => self::MODULE_MASTER_ID,
            'name'              => 'Master',
            'has_car'           => 0,
            'transport_type_id' => null,
        ],
    ];

    const TABLES_WITH_POSITION_AND_CLASS = [
        [
            'table' => 'tbl_client_bonus',
            'key'   => 'fk_client_bonus__position_class_id',
        ],
        [
            'table' => 'tbl_taxi_tariff',
            'key'   => 'fk_taxi_tariff_has_position_class',
        ],
        [
            'table' => 'tbl_worker_group',
            'key'   => 'fk_worker_group_has_position_class',
        ],
        [
            'table' => 'tbl_worker_has_position',
            'key'   => 'fk_worker_has_position_position_class_id',
        ],
        [
            'table' => 'tbl_worker_tariff',
            'key'   => 'fk_worker_tariff_has_position_class',
        ],
    ];

    const TABLES_WITH_POSITION = [
        'tbl_client_push_notifications',
        'tbl_client_status_event',
        'tbl_default_client_push_notifications',
        'tbl_default_client_status_event',
        'tbl_default_sms_template',
        'tbl_notification_has_position',
        'tbl_position_has_document',
        'tbl_position_has_field',
        'tbl_sms_template',
        'tbl_tenant_city_has_position',
        'tbl_worker_shift',
        'tbl_worker_review_rating',
    ];


    /**
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->dropForeignKey('FK_tbl_position_tbl_position', self::TABLE_POSITION);
        $this->dropColumn(self::TABLE_POSITION, 'parent_id');

        foreach (self::MODULES as $module) {
            $model = Module::findOne($module['id']);
            if (empty($model)) {
                $model = new Module(['id' => $module['id']]);
            }

            $model['name']   = $module['name'];
            $model['active'] = $module['active'];
            $model['code']   = (string)$module['id'];

            if (!$model->save()) {
                throw new \yii\base\Exception(implode('; ', $model->getFirstErrors()));
            }
        }

        foreach (self::POSITIONS as $position) {
            $model = Position::findOne($position['position_id']);
            if (empty($model)) {
                $model = new Position(['position_id' => $position['position_id']]);
            }

            $model['code']              = $position['module_id'] . $position['position_id'];
            $model['module_id']         = $position['module_id'];
            $model['name']              = $position['name'];
            $model['has_car']           = $position['has_car'];
            $model['transport_type_id'] = $position['transport_type_id'];

            if (!$model->save()) {
                throw new \yii\base\Exception(implode('; ', $model->getFirstErrors()));
            }
        }
        $this->db->createCommand('update tbl_position set code = position_id')->execute();

        $this->addColumn(self::TABLE_ORDER, 'module_id', $this->integer());
        $this->update(self::TABLE_ORDER, ['module_id' => self::MODULE_COURIER_ID],
            ['position_id' => [self::POSITION_OLD_UAT_COURIER_ID, self::POSITION_OLD_UAT_SUB_COURIER_ID]]);
        $this->update(self::TABLE_ORDER, ['module_id' => self::MODULE_TRACK_ID],
            ['position_id' => self::POSITION_OLD_UAT_TRACK_ID]);
        $this->update(self::TABLE_ORDER, ['module_id' => self::MODULE_TAXI_ID],
            ['position_id' => self::POSITION_OLD_TAXI_ID]);
        $this->alterColumn(self::TABLE_ORDER, 'module_id', $this->integer()->notNull());
        $this->dropColumn(self::TABLE_ORDER, 'position_id');

        foreach (self::TABLES_WITH_POSITION as $table) {
            $this->update($table, ['position_id' => self::POSITION_COURIER_ID],
                ['position_id' => [self::POSITION_OLD_UAT_COURIER_ID, self::POSITION_OLD_UAT_SUB_COURIER_ID]]);
            $this->update($table, ['position_id' => self::POSITION_TRACK_ID],
                ['position_id' => self::POSITION_OLD_UAT_TRACK_ID]);
            $this->update($table, ['position_id' => self::POSITION_TAXI_ID],
                ['position_id' => self::POSITION_OLD_TAXI_ID]);
        }

        foreach (self::TABLES_WITH_POSITION_AND_CLASS as $tableInfo) {
            $this->dropForeignKey($tableInfo['key'], $tableInfo['table']);
            $this->dropColumn($tableInfo['table'], 'position_class_id');
            $this->update($tableInfo['table'],
                ['position_id' => self::POSITION_COURIER_ID],
                ['position_id' => [self::POSITION_OLD_UAT_COURIER_ID, self::POSITION_OLD_UAT_SUB_COURIER_ID]]);
            $this->update($tableInfo['table'],
                ['position_id' => self::POSITION_TRACK_ID],
                ['position_id' => self::POSITION_OLD_UAT_TRACK_ID]);
            $this->update($tableInfo['table'],
                ['position_id' => self::POSITION_TAXI_ID],
                ['position_id' => self::POSITION_OLD_TAXI_ID]);
        }
    }

    public function down()
    {
        return false;
    }

}
