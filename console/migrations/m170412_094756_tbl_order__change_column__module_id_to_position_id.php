<?php

use yii\db\Migration;

class m170412_094756_tbl_order__change_column__module_id_to_position_id extends Migration
{
    const TABLE_ORDER = '{{%order}}';
    const COLUMN_MODULE_ID = 'module_id';
    const COLUMN_POSITION_ID = 'position_id';

    public function up()
    {
        $this->renameColumn(self::TABLE_ORDER, self::COLUMN_MODULE_ID, self::COLUMN_POSITION_ID);
    }

    public function down()
    {
        $this->renameColumn(self::TABLE_ORDER, self::COLUMN_POSITION_ID, self::COLUMN_MODULE_ID);
    }

}
