<?php

use yii\db\Migration;

class m170111_130331_notification_has_city_table extends Migration
{
    const TABLE_NAME = '{{%notification_has_city}}';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'              => $this->primaryKey(),
            'notification_id' => $this->integer(11)->unsigned()->notNull(),
            'city_id'         => $this->integer(11)->unsigned()->notNull(),
        ]);

        $this->addForeignKey('fk_notification_has_city__notification_id', self::TABLE_NAME, 'notification_id',
            '{{%notification}}', 'notification_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_notification_has_city__city_id', self::TABLE_NAME, 'city_id',
            '{{%city}}', 'city_id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_notification_has_city__city_id', self::TABLE_NAME);
        $this->dropForeignKey('fk_notification_has_city__notification_id', self::TABLE_NAME);
        $this->dropTable(self::TABLE_NAME);
    }
}
