<?php

use yii\db\Migration;

class m160610_114851_add_migrate_currency_gel extends Migration
{
    const TABLE_NAME = '{{%currency}}';
    
    public function up()
    {
        $this->insert(self::TABLE_NAME,[
            'type_id' => 1,
            'name' => 'Lari',
            'code' => 'GEL',
            'symbol' => '₾',
            'numeric_code' => 981,
            'minor_unit' => 2,
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME,['code' => 'GEL']);
    }
}
