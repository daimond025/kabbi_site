<?php

use yii\db\Migration;

class m170514_193109_group_prioity extends Migration
{
    const TABLE_WORKER_GROUP = '{{%worker_group}}';
    const COLUMN_PRIORITY  = 'priority';

    public function up()
    {
        $this->addColumn(self::TABLE_WORKER_GROUP, self::COLUMN_PRIORITY, $this->integer(10)->notNull()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_WORKER_GROUP, self::COLUMN_PRIORITY);
    }


}
