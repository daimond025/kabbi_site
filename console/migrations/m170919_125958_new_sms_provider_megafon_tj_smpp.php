<?php

use yii\db\Migration;

class m170919_125958_new_sms_provider_megafon_tj_smpp extends Migration
{
    const TABLE_NAME = '{{%sms_server}}';
    const PROVIDER_ID = 19;

    public function up()
    {
        $this->insert(self::TABLE_NAME, [
            'server_id' => self::PROVIDER_ID,
            'name'      => 'Megafon.tj (SMPP)',
            'host'      => '10.241.201.184:2775',
            'active'    => '1',
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME, ['server_id' => self::PROVIDER_ID]);
    }
}
