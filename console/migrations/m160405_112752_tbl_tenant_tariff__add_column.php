<?php

use yii\db\Migration;

class m160405_112752_tbl_tenant_tariff__add_column extends Migration
{
    const TABLE_TENANT_TARIFF = '{{%tenant_tariff}}';
    const COLUMN_STARTED_AT = 'started_at';

    public function up()
    {
        $this->addColumn(self::TABLE_TENANT_TARIFF, self::COLUMN_STARTED_AT, $this->integer()->notNull());
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_TENANT_TARIFF, self::COLUMN_STARTED_AT);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
