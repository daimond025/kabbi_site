<?php

use yii\db\Migration;

class m171212_104255_add_default_rights__partner_employee extends Migration
{

    const POSITION_PARTNER_EMPLOYEE_ID = 13;
    const TABLE_NAME = '{{%user_default_right}}';

    const RIGHTS = [
        ['bankCard', 'off', self::POSITION_PARTNER_EMPLOYEE_ID],
        ['bonus', 'off', self::POSITION_PARTNER_EMPLOYEE_ID],
        ['car', 'write', self::POSITION_PARTNER_EMPLOYEE_ID],
        ['cars', 'write', self::POSITION_PARTNER_EMPLOYEE_ID],
        ['cities', 'off', self::POSITION_PARTNER_EMPLOYEE_ID],
        ['clients', 'off', self::POSITION_PARTNER_EMPLOYEE_ID],
        ['clientTariff', 'off', self::POSITION_PARTNER_EMPLOYEE_ID],
        ['drivers', 'write', self::POSITION_PARTNER_EMPLOYEE_ID],
        ['driverTariff', 'off', self::POSITION_PARTNER_EMPLOYEE_ID],
        ['exchange', 'off', self::POSITION_PARTNER_EMPLOYEE_ID],
        ['gootaxTariff', 'off', self::POSITION_PARTNER_EMPLOYEE_ID],
        ['notifications', 'off', self::POSITION_PARTNER_EMPLOYEE_ID],
        ['orders', 'read', self::POSITION_PARTNER_EMPLOYEE_ID],
        ['organization', 'off', self::POSITION_PARTNER_EMPLOYEE_ID],
        ['parkings', 'off', self::POSITION_PARTNER_EMPLOYEE_ID],
        ['phoneLine', 'off', self::POSITION_PARTNER_EMPLOYEE_ID],
        ['promoSystem', 'off', self::POSITION_PARTNER_EMPLOYEE_ID],
        ['publicPlace', 'off', self::POSITION_PARTNER_EMPLOYEE_ID],
        ['referralSystem', 'off', self::POSITION_PARTNER_EMPLOYEE_ID],
        ['reports', 'write', self::POSITION_PARTNER_EMPLOYEE_ID],
        ['settings', 'off', self::POSITION_PARTNER_EMPLOYEE_ID],
        ['streets', 'off', self::POSITION_PARTNER_EMPLOYEE_ID],
        ['tenant', 'off', self::POSITION_PARTNER_EMPLOYEE_ID],
        ['users', 'write', self::POSITION_PARTNER_EMPLOYEE_ID],
    ];

    public function safeUp()
    {
        $this->batchInsert(self::TABLE_NAME, ['permission', 'rights', 'position_id'], self::RIGHTS);
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_NAME, ['position_id' => self::POSITION_PARTNER_EMPLOYEE_ID]);
    }
}
