<?php

use yii\db\Migration;

class m190619_150136_add_fix_for_one_point_taxi_tariff_option extends Migration
{
    const TABLE_NAME = '{{%taxi_tariff_option}}';
    const COLUMN_NAME = 'calculation_fix_point';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME,  self::COLUMN_NAME, $this->integer(1)->notNull()->defaultValue(0)->after('calculation_fix') );

    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);
    }
}
