<?php

use yii\db\Schema;
use yii\db\Migration;

class m151024_122937_tbl_tenant_has_admin_tariff_additional_option extends Migration {

    const TABLE_NAME = 'tbl_tenant_has_admin_tariff_additional_option';

    public function up() {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'tenant_id' => $this->integer(5)->notNull(),
            'option_id' => $this->integer(5)->notNull(),
            'create_date' => $this->dateTime()->notNull(), 
        ]);
    }

    public function down() {
        $this->dropTable(self::TABLE_NAME);
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
