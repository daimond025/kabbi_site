<?php

use yii\db\Schema;
use yii\db\Migration;

class m160128_064609_tbl_sms_log_fix_incorrect_fkey extends Migration
{

    const SMS_LOG = '{{%sms_log}}';
    const SMS_SERVER = '{{%sms_server}}';
    const TENANT = '{{%tenant}}';

    public function up()
    {
        $this->dropForeignKey('fk_sms_log_tenant_id', self::SMS_LOG);
        $this->alterColumn(self::SMS_LOG, 'tenant_id', 'INT(10) UNSIGNED');
        $this->alterColumn(self::SMS_LOG, 'server_id', 'TINYINT(3) UNSIGNED NOT NULL');
        $this->addForeignKey('fk_sms_log_tenant_id', self::SMS_LOG, ['tenant_id'], self::TENANT, ['tenant_id']);
        $this->addForeignKey('fk_sms_log_server_id', self::SMS_LOG, ['server_id'], self::SMS_SERVER, ['server_id']);
    }

    public function down()
    {
        $this->dropForeignKey('fk_sms_log_server_id', self::SMS_LOG);
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
