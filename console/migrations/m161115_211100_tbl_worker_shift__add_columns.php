<?php

use yii\db\Migration;

class m161115_211100_tbl_worker_shift__add_columns extends Migration
{
    const TABLE_WORKER_SHIFT = '{{%worker_shift}}';

    const COLUMN_SHIFT_END_REASON = 'shift_end_reason';
    const COLUMN_SHIFT_END_EVENT_SENDER_TYPE = 'shift_end_event_sender_type';
    const COLUMN_SHIFT_END_EVENT_SENDER_ID = 'shift_end_event_sender_id';

    public function up()
    {
        $this->addColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_SHIFT_END_REASON,
            "ENUM('timer', 'bad_coords', 'bad_parking', 'operator_service', 'worker_service') NULL DEFAULT NULL");
        $this->addColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_SHIFT_END_EVENT_SENDER_TYPE,
            "ENUM('system', 'user', 'worker') NULL DEFAULT NULL");
        $this->addColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_SHIFT_END_EVENT_SENDER_ID, $this->integer());
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_SHIFT_END_REASON);
        $this->dropColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_SHIFT_END_EVENT_SENDER_TYPE);
        $this->dropColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_SHIFT_END_EVENT_SENDER_ID);
    }

}
