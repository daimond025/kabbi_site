<?php

use yii\db\Schema;
use yii\db\Migration;

class m150827_115936_delete_old_billing_tables extends Migration
{
    public function up()
    {
        $this->dropTable('{{%client_transaction}}');
        $this->dropTable('{{%client_balance}}');
        $this->dropTable('{{%client_company_transaction}}');
        $this->dropTable('{{%client_company_balance}}');
        $this->dropTable('{{%driver_transaction}}');
        $this->dropTable('{{%driver_balance}}');
        $this->dropTable('{{%payment_tn_dr}}');
        $this->dropTable('{{%payment_tn_st}}');
        $this->dropTable('{{%payment_tn_tn}}');
        $this->dropTable('{{%tenant_transaction}}');
        $this->dropTable('{{%tenant_balance}}');

    }

    public function down()
    {
        echo "m150827_115936_delete_old_billing_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
