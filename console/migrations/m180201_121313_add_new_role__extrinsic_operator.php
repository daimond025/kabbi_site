<?php

use yii\db\Migration;

class m180201_121313_add_new_role__extrinsic_operator extends Migration
{
    const ROLE_NAME = 'extrinsic_dispatcher';

    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        $role = $auth->createRole(self::ROLE_NAME);
        $role->description = 'Extrinsic dispatcher';
        $auth->add($role);
    }

    public function safeDown()
    {
        $this->delete('{{%auth_assignment}}', ['item_name' => self::ROLE_NAME]);
        $this->delete('{{%auth_item}}', ['name' => self::ROLE_NAME]);
    }
}
