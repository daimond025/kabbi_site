<?php

use yii\db\Migration;
use frontend\modules\tenant\models\GeoServiceProvider;
use frontend\modules\tenant\models\Tenant;

class m160610_074402_geoservice_add_default_data extends Migration
{
    const AUTO_GEO_SERVICE_TYPE = 5; // gootax
    const GEOCODE_SERVICE_TYPE = 1; //osm

    public function up()
    {
        $tenantCityArr = (new \yii\db\Query())
            ->select(['tenant_id', 'city_id'])
            ->from('tbl_tenant_has_city')
            ->all();
        foreach ($tenantCityArr as $tenantCityData) {
            $tenantId = $tenantCityData['tenant_id'];
            $cityId = $tenantCityData['city_id'];
            $tenantDomain = Yii::$app->db->createCommand('SELECT domain FROM tbl_tenant WHERE tenant_id=' . $tenantId)->queryScalar();
            $result = GeoServiceProvider::createProvidersWithCustomType($tenantId, $tenantDomain, $cityId, self::AUTO_GEO_SERVICE_TYPE, self::GEOCODE_SERVICE_TYPE);
        }
        return true;
    }

    public function down()
    {

    }
}