<?php

use yii\db\Schema;
use yii\db\Migration;

class m151024_124737_tbl_admin__one_time_job extends Migration
{
    const TABLE_NAME = '{{%admin_one_time_job}}';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'job_id' => $this->primaryKey(),
            'name' => $this->string(48)->notNull(),
            'price' => $this->integer()->notNull(),
            'active' => $this->boolean(),
            'create_date' => $this->dateTime(),
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
        echo 'Table ' . self::TABLE_NAME . ' has been dropped';
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
