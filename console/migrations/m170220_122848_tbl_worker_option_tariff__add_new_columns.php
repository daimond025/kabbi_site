<?php

use yii\db\Migration;

class m170220_122848_tbl_worker_option_tariff__add_new_columns extends Migration
{
    const TABLE_NAME = '{{%worker_option_tariff}}';

    public function up()
    {
        $this->dropColumn(self::TABLE_NAME, 'increase_sum_limit_type');

        $this->addColumn(self::TABLE_NAME, 'increase_sum_fix', $this->decimal(10, 2));
        $this->addColumn(self::TABLE_NAME, 'increase_sum_fix_type',
            "ENUM('PERCENT','MONEY') NOT NULL DEFAULT 'PERCENT'");
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'increase_sum_fix');
        $this->dropColumn(self::TABLE_NAME, 'increase_sum_fix_type');
        $this->addColumn(self::TABLE_NAME, 'increase_sum_limit_type',
            "ENUM('PERCENT','MONEY') NOT NULL DEFAULT 'PERCENT'");
    }

}
