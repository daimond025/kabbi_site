<?php

use yii\db\Migration;

class m180713_092131_add_table__tbl_client_worker_like extends Migration
{

    const TABLE_NAME = '{{%client_worker_like}}';
    const FK_CLIENT_ID = 'fk_client_worker_like__client_id';
    const FK_WORKER_ID = 'fk_client_worker_like__worker_id';
    const FK_POSITION_ID = 'fk_client_worker_like__position_id';
    const CLIENT_ID = 'client_id';
    const WORKER_ID = 'worker_id';
    const POSITION_ID = 'position_id';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'             => $this->primaryKey()->unsigned(),
            'client_id'      => $this->integer()->unsigned(),
            'worker_id'      => $this->integer(),
            'position_id'    => $this->integer(),
            'like_count'     => $this->integer()->defaultValue(0),
            'dislike_count'  => $this->integer()->defaultValue(0),
            'created_at'     => $this->integer()->unsigned(),
            'updated_at'     => $this->integer()->unsigned(),
        ]);

        $this->addForeignKey(self::FK_CLIENT_ID, self::TABLE_NAME, self::CLIENT_ID, '{{%client}}', self::CLIENT_ID, 'CASCADE', 'CASCADE');
        $this->addForeignKey(self::FK_WORKER_ID, self::TABLE_NAME, self::WORKER_ID, '{{%worker}}', self::WORKER_ID, 'CASCADE', 'CASCADE');
        $this->addForeignKey(self::FK_POSITION_ID, self::TABLE_NAME, self::POSITION_ID, '{{%position}}', self::POSITION_ID, 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_POSITION_ID, self::TABLE_NAME);
        $this->dropForeignKey(self::FK_WORKER_ID, self::TABLE_NAME);
        $this->dropForeignKey(self::FK_CLIENT_ID, self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
    }
}
