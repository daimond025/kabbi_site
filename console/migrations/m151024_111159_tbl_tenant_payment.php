<?php

use yii\db\Schema;
use yii\db\Migration;

class m151024_111159_tbl_tenant_payment extends Migration {

    const TABLE_NAME = '{{%tenant_payment}}';

    public function up() {
        $this->createTable(self::TABLE_NAME, [
            'payment_id' => $this->primaryKey(),
            'amount' => $this->integer(6),
            'tenant_id' => $this->integer(5),
            'create_date' => $this->dateTime(),
        ]);
    }

    public function down() {
        $this->dropTable(self::TABLE_NAME);
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
