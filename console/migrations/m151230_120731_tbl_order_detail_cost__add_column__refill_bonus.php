<?php

use yii\db\Schema;
use yii\db\Migration;

class m151230_120731_tbl_order_detail_cost__add_column__refill_bonus extends Migration
{
    const TABLE_NAME = '{{%order_detail_cost}}';

    const REFILL_BONUS_COLUMN = 'refill_bonus';


    public function up()
    {
        $this->addColumn(self::TABLE_NAME, self::REFILL_BONUS_COLUMN, 'varchar(45)');
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, self::REFILL_BONUS_COLUMN);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
