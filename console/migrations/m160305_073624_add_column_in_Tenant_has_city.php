<?php

use yii\db\Migration;

class m160305_073624_add_column_in_Tenant_has_city extends Migration
{
    
    const TABLE_NAME = '{{%tenant_has_city}}';
    
    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'phone', $this->string(20));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME,'phone');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
