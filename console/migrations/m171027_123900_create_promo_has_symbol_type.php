<?php

use yii\db\Migration;

class m171027_123900_create_promo_has_symbol_type extends Migration
{

    const TABLE_NAME = '{{%promo_has_symbol_type}}';

    const COLUMN_PROMO_ID = 'promo_id';
    const COLUMN_SYMBOL_TYPE_ID = 'symbol_type_id';

    const FK_PROMO_HAS_SYMBOL_TYPE__PROMO_ID = 'fk_promo_has_symbol_type__promo_id';
    const FK_PROMO_HAS_SYMBOL_TYPE__SYMBOL_TYPE_ID = 'fk_promo_has_symbol_type__symbol_type_id';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            self::COLUMN_PROMO_ID       => $this->integer()->unsigned(),
            self::COLUMN_SYMBOL_TYPE_ID => $this->integer(),
        ]);

        $this->addPrimaryKey(
            'pk_promo_has_symbol_type', self::TABLE_NAME,
            [self::COLUMN_PROMO_ID, self::COLUMN_SYMBOL_TYPE_ID]
        );

        $this->addForeignKey(self::FK_PROMO_HAS_SYMBOL_TYPE__PROMO_ID, self::TABLE_NAME,
            self::COLUMN_PROMO_ID, '{{%promo}}', self::COLUMN_PROMO_ID, 'CASCADE', 'CASCADE');

        $this->addForeignKey(self::FK_PROMO_HAS_SYMBOL_TYPE__SYMBOL_TYPE_ID, self::TABLE_NAME,
            self::COLUMN_SYMBOL_TYPE_ID, '{{%promo_symbol_type}}', self::COLUMN_SYMBOL_TYPE_ID, 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_PROMO_HAS_SYMBOL_TYPE__SYMBOL_TYPE_ID, self::TABLE_NAME);
        $this->dropForeignKey(self::FK_PROMO_HAS_SYMBOL_TYPE__PROMO_ID, self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
    }

}
