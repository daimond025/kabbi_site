<?php

use yii\db\Migration;

class m170301_054708_create_bgn_currency extends Migration
{
    const TABLE_NAME = '{{%currency}}';

    public function up()
    {
        $this->insert(self::TABLE_NAME,[
            'type_id' => 1,
            'name' => 'Bulgarian Lev',
            'code' => 'BGN',
            'symbol' => 'лв',
            'numeric_code' => 975,
            'minor_unit' => 2,
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME,['code' => 'BGN']);
    }
}
