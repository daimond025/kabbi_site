<?php

use yii\db\Migration;

class m160412_053119_add_UZS_currency extends Migration
{
    
    const TABLE_NAME = '{{%currency}}';
    public function up()
    {
        $this->insert(self::TABLE_NAME,[
            'type_id' => 1,
            'name' => 'Uzbekistan Sum',
            'code' => 'UZS',
            'symbol' => 'UZS',
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME,['code' => 'UZS']);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
