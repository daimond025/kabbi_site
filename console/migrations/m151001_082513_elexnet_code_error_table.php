<?php

use yii\db\Schema;
use yii\db\Migration;

class m151001_082513_elexnet_code_error_table extends Migration {

    const TABLE_NAME = '{{%elecsnet_error_code}}';

    public function up() {
        $this->createTable(self::TABLE_NAME, [
            'error_id' => $this->primaryKey(),
            'code' => $this->string(4)->notNull(),
            'description' => $this->text(255)->notNull(),
        ]);
    }

    public function down() {
        $this->dropTable(self::TABLE_NAME);
        echo self::TABLE_NAME . ' successfully dropped';
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
