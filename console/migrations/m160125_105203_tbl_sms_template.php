<?php

use yii\db\Schema;
use yii\db\Migration;

class m160125_105203_tbl_sms_template extends Migration
{
    public function up()
    {
        $this->execute('SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0');
        $this->execute('SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0');

        $this->addColumn('{{%sms_template}}', 'city_id', 'INT(11) UNSIGNED NULL DEFAULT NULL');
        $this->createIndex('FK_sms_template_idx', '{{%sms_template}}', 'city_id');
        $this->addForeignKey('FK_sms_template', '{{%sms_template}}', 'city_id', '{{%city}}', 'city_id');

        $this->execute('SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS');
        $this->execute('SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS');
    }

    public function down()
    {
        $this->dropForeignKey('FK_sms_template', '{{%sms_template}}');
        $this->dropIndex('FK_sms_template_idx', '{{%sms_template}}');
        $this->dropColumn('{{%sms_template}}', 'city_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
