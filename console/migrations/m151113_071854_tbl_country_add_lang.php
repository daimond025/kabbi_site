<?php

use yii\db\Schema;
use yii\db\Migration;

class m151113_071854_tbl_country_add_lang extends Migration
{
    public function up()
    {
        $this->addColumn('{{%country}}', 'name_en', 'VARCHAR(45) DEFAULT  NULL');
        $this->addColumn('{{%country}}', 'name_az', 'VARCHAR(45) DEFAULT  NULL');
        $this->addColumn('{{%country}}', 'name_de', 'VARCHAR(45) DEFAULT  NULL');
        $this->addColumn('{{%country}}', 'name_se', 'VARCHAR(45) DEFAULT  NULL');

    }

    public function down()
    {
        $this->dropColumn('{{%country}}', 'name_en');
        $this->dropColumn('{{%country}}', 'name_az');
        $this->dropColumn('{{%country}}', 'name_de');
        $this->dropColumn('{{%country}}', 'name_se');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
