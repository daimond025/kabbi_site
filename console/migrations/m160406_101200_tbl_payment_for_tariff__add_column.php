<?php

use yii\db\Migration;

class m160406_101200_tbl_payment_for_tariff__add_column extends Migration
{
    const TABLE_PAYMENT_FOR_TARIFF = '{{%payment_for_tariff}}';
    const COLUMN_STARTED_AT = 'started_at';

    public function up()
    {
        $this->addColumn(self::TABLE_PAYMENT_FOR_TARIFF, self::COLUMN_STARTED_AT, $this->integer()->notNull());
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_PAYMENT_FOR_TARIFF, self::COLUMN_STARTED_AT);;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
