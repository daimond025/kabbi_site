<?php

use yii\db\Migration;

class m160323_091303_add_image_in_client_tariff extends Migration
{
    const TABLE_NAME = '{{%taxi_tariff}}';
    
    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'logo', $this->string(255)->defaultValue(''));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'logo');
    }
}
