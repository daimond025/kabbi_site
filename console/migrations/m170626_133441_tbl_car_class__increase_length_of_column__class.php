<?php

use yii\db\Migration;

class m170626_133441_tbl_car_class__increase_length_of_column__class extends Migration
{
    const TABLE_NAME = '{{%car_class}}';
    const COLUMN_CLASS = 'class';

    public function safeUp()
    {
        $this->alterColumn(self::TABLE_NAME, self::COLUMN_CLASS, $this->string(50)->notNull());
    }

    public function safeDown()
    {
        $this->alterColumn(self::TABLE_NAME, self::COLUMN_CLASS, $this->string(30)->notNull());
    }

}
