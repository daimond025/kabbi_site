<?php

use yii\db\Migration;

class m170703_121701_tbl_order_status__add_new_preorder_statuses extends Migration
{
    const TABLE_NAME = '{{%order_status}}';

    const NEW_STATUSES = [
        [116, 'Waiting for pre-order confirmation', 'pre_order', 1],
        [117, 'Worker ignored pre-order confirmation', 'pre_order', 1],
        [118, 'Worker refused pre-order confirmation', 'pre_order', 1],
        [119, 'Worker accepted pre-order confirmation', 'pre_order', 1],
    ];

    public function safeUp()
    {
        $this->batchInsert(self::TABLE_NAME, ['status_id', 'name', 'status_group', 'dispatcher_sees'],
            self::NEW_STATUSES);
    }

    public function safeDown()
    {
        $statusIds = array_map(function ($item) {
            return $item[0];
        }, self::NEW_STATUSES);

        $this->delete(self::TABLE_NAME, ['status_id' => $statusIds]);
    }

}
