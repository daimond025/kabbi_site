<?php

use yii\db\Schema;
use yii\db\Migration;

class m150722_145128_tbl_operation_type extends Migration
{
    public function up()
    {
        $this->insert('{{%operation_type}}', ['name' => 'Cash out']);
    }

    public function down()
    {
        echo "m150722_145128_tbl_operation_type cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
