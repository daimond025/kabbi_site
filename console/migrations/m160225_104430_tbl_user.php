<?php

use yii\db\Migration;

class m160225_104430_tbl_user extends Migration
{
    const TABLE_NAME = '{{%user}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'lang', $this->string(10)->defaultValue('ru'));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'lang');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
