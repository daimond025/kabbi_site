<?php

use yii\db\Schema;
use yii\db\Migration;

class m151222_125744_tbl_sms_log extends Migration
{
    const SMS_LOG_TABLE = '{{%sms_log}}';

    public function up()
    {
        $this->alterColumn('{{%sms_log}}', 'tenant_id', $this->integer(10));
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $this->addForeignKey('fk_sms_log_tenant_id', self::SMS_LOG_TABLE, 'tenant_id', '{{%tenant_id}}', 'tenant_id', 'CASCADE', 'CASCADE');
        $this->execute('SET FOREIGN_KEY_CHECKS=1');
    }

    public function down()
    {
        echo "m151222_125744_tbl_sms_log cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
