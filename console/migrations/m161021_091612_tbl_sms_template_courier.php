<?php

use yii\db\Migration;

class m161021_091612_tbl_sms_template_courier extends Migration
{
    const TABLE_NAME = '{{%sms_template_courier}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable(self::TABLE_NAME, [
            'template_id' => $this->primaryKey()->unsigned(),
            'tenant_id'   => $this->integer(10)->notNull()->unsigned(),
            'type'        => $this->string(45)->notNull(),
            'text'        => $this->text()->notNull(),
            'city_id'     => $this->integer()->unsigned(),
            'position_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('FK_sms_template_courier_city_id', self::TABLE_NAME, 'city_id', '{{%city}}', 'city_id');
        $this->addForeignKey('fk_sms_template_courier_position_id', self::TABLE_NAME, 'position_id',
            '{{%position}}', 'position_id');
    }

    public function down()
    {
        $this->dropForeignKey('FK_sms_template_courier_city_id', self::TABLE_NAME);
        $this->dropForeignKey('fk_sms_template_courier_position_id', self::TABLE_NAME);
        $this->dropTable(self::TABLE_NAME);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
