<?php

use yii\db\Schema;
use yii\db\Migration;

class m151112_102820_tbl_payment_method extends Migration
{
    const TABLE_NAME = '{{%payment_method}}';
    const BONUS_PAYMENT = 'BONUS';

    public function up()
    {
        $this->insert(self::TABLE_NAME, [
            'payment' => self::BONUS_PAYMENT,
            'name' => 'Bonus',
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME, 'payment = :payment', [
            'payment' => self::BONUS_PAYMENT,
        ]);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
