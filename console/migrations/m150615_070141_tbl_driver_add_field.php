<?php

use yii\db\Schema;
use yii\db\Migration;

class m150615_070141_tbl_driver_add_field extends Migration
{

    public function up()
    {
        $this->addColumn('{{%driver}}', 'driver_endtime_of_block', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        $this->dropColumn('{{%driver}}', 'driver_endtime_of_block');
    }
}
