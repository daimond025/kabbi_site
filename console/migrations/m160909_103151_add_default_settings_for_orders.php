<?php

use yii\db\Migration;

class m160909_103151_add_default_settings_for_orders extends Migration
{
    public function up()
    {
        $this->batchInsert('{{%default_settings}}', ['name', 'value', 'type'], [
            ['ALLOW_WORKER_TO_CANCEL_ORDER', 1, 'orders'],
            ['TIME_ALLOW_WORKER_TO_CANCEL_ORDER', 20, 'orders'],
        ]);
    }

    public function down()
    {
        $this->delete('{{%default_settings}}', [
            'name' => [
                'ALLOW_WORKER_TO_CANCEL_ORDER',
                'TIME_ALLOW_WORKER_TO_CANCEL_ORDER',
            ],
            'type' => 'orders',
        ]);
    }
    
}
