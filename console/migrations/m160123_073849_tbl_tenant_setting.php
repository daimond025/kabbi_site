<?php

use yii\db\Schema;
use yii\db\Migration;

class m160123_073849_tbl_tenant_setting extends Migration
{
    public function up()
    {
        $currency = \frontend\modules\tenant\models\Currency::find()->where(['type_id' => 1])->all();
        $currency_map = \yii\helpers\ArrayHelper::map($currency, 'code', 'currency_id');

        $tenant_setting = \common\modules\tenant\models\TenantSetting::find()->where(['name' => 'CURRENCY'])->all();
        foreach ($tenant_setting as $setting) {
            if (array_key_exists($setting->value, $currency_map)) {
                $setting->value = $currency_map[$setting->value];
                $setting->save(false, ['value']);
            }
        }
    }

    public function down()
    {
        echo "m160123_073849_tbl_tenant_setting cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
