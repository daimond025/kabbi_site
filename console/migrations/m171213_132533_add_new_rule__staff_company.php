<?php

use yii\db\Migration;
use frontend\components\rbac\rules\StaffCompanyEditRule;

class m171213_132533_add_new_rule__staff_company extends Migration
{

    const ROLE_NAME = 'staff_company';

    /* @var $rule StaffCompanyEditRule */
    protected $rule;

    public function init()
    {
        $this->rule = new StaffCompanyEditRule();
        parent::init();
    }

    public function safeUp()
    {
        $auth = Yii::$app->authManager;
        $auth->add($this->rule);

        $staffCompanyRole = $auth->getRole(self::ROLE_NAME);
        $staffCompanyRole->ruleName = $this->rule->name;
        $auth->update(self::ROLE_NAME, $staffCompanyRole);
    }

    public function safeDown()
    {
        $this->delete('{{%auth_rule}}', ['name' => $this->rule->name]);
        $this->delete('{{%auth_item}}', ['rule_name' => $this->rule->name]);
    }
}
