<?php

use yii\db\Migration;

class m160928_103559_new_currency_tmt extends Migration
{
    const TABLE_NAME = '{{%currency}}';

    public function up()
    {
        $this->insert(self::TABLE_NAME,[
            'type_id' => 1,
            'name' => 'Turkmenistan New Manat',
            'code' => 'TMT',
            'symbol' => 'm',
            'numeric_code' => 934,
            'minor_unit' => 2,
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME,['code' => 'TMT']);
    }
}
