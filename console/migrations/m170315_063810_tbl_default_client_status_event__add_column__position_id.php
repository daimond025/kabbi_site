<?php

use yii\db\Migration;

class m170315_063810_tbl_default_client_status_event__add_column__position_id extends Migration
{
    const TABLE_NAME = '{{%default_client_status_event}}';
    const COLUMN_NAME = 'position_id';

    const TABLE_POSITION = '{{%position}}';
    const FOREIGN_KEY_NAME = 'tbl_default_client_status_event__position_id';

    const DEFAULT_VALUE = 1;

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_NAME, $this->integer(10));
        $this->update(self::TABLE_NAME, [
            self::COLUMN_NAME => self::DEFAULT_VALUE,
        ]);
        $this->alterColumn(self::TABLE_NAME, self::COLUMN_NAME, $this->integer(10)->notNull());

        $this->addForeignKey(
            self::FOREIGN_KEY_NAME, self::TABLE_NAME, self::COLUMN_NAME,
            self::TABLE_POSITION, self::COLUMN_NAME, 'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
        $this->dropForeignKey(self::FOREIGN_KEY_NAME, self::TABLE_NAME);

        $this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);
    }
}
