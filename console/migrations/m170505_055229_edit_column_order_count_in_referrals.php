<?php

use yii\db\Migration;

class m170505_055229_edit_column_order_count_in_referrals extends Migration
{
    const TABLE_SUBSCRIBER = '{{%referral_system_subscribers}}';
    const TABLE_REFERRAL = '{{%referral}}';

    public function up()
    {
        $this->addColumn(self::TABLE_SUBSCRIBER, 'order_count',
            $this->integer(11)->notNull()->defaultValue(0)->comment('Кол-во поездок'));

        $this->alterColumn(self::TABLE_REFERRAL, 'order_count',
            $this->integer(11)->comment('Кол-во поездок. NULL - безлимитно'));
    }

    public function down()
    {
        $this->alterColumn(self::TABLE_REFERRAL, 'order_count',
            $this->integer(11)->defaultValue(0)->comment('Кол-во поездок. 0 - безлимитно'));
        $this->dropColumn(self::TABLE_SUBSCRIBER, 'order_count');
    }
}
