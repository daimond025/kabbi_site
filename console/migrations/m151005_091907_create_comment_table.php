<?php

use yii\db\Schema;
use yii\db\Migration;

class m151005_091907_create_comment_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%comment}}', [
            'comment_id' => $this->primaryKey(),
            'post_id' => $this->integer()->notNull(),
            'comment' => $this->text()->notNull(),
            'author_id' => $this->integer()->notNull(),
            'publication_date' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            ], $tableOptions);
        $this->addForeignKey('fk_comment__post_id', '{{%comment}}', 'post_id',
                '{{%post}}', 'post_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_comment__author_id', '{{%comment}}', 'author_id',
                '{{%community_user}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%comment}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
