<?php

use yii\db\Migration;

class m171212_101511_add_new_role extends Migration
{

    const TABLE_NAME = '{{%user_position}}';

    const ROLE = 'Partner employee';

    public function safeUp()
    {
        $this->insert(self::TABLE_NAME, [
            'name' => self::ROLE
        ]);
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_NAME, [
            'name' => self::ROLE
        ]);
    }
}
