<?php

use yii\db\Migration;

class m180720_110719_add_active_column_in_client_review_table extends Migration
{
    const TABLE_NAME = '{{%client_review}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'active', $this->integer(1)->notNull()->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'active');
    }
}
