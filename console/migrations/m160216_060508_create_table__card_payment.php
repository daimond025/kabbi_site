<?php

use yii\db\Schema;
use yii\db\Migration;

class m160216_060508_create_table__card_payment extends Migration
{
    const TABLE_CARD_PAYMENT = '{{%card_payment}}';
    const TABLE_ORDER = '{{%order}}';

    public function up()
    {
        $this->createTable(self::TABLE_CARD_PAYMENT, [
            'id'               => $this->primaryKey(),
            'order_id'         => 'INT(10) UNSIGNED NOT NULL',
            'pan'              => $this->string(25)->notNull(),
            'payment_sum'      => $this->decimal(12, 2),
            'payment_order_id' => $this->string(36),
            'payment_status'   => "ENUM('PAID', 'REFUNDED')",
            'created_at'       => $this->integer()->notNull(),
            'updated_at'       => $this->integer()->notNull(),
        ]);

        $this->createIndex('ui_card_payment__order_id',
            self::TABLE_CARD_PAYMENT, 'order_id');

        $this->addForeignKey('fk_card_payment__order_id',
                self::TABLE_CARD_PAYMENT, 'order_id',
                self::TABLE_ORDER, 'order_id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        $this->dropTable(self::TABLE_CARD_PAYMENT);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
