<?php

use yii\db\Migration;

class m180716_102020_tbl_user_add_idx extends Migration
{
    public function safeUp()
    {
        $this->createIndex('tbi_user_tenant_id_position_id_idx', '{{%user}}', [
            'tenant_id',
            'position_id',
        ]);
    }

    public function safeDown()
    {
        $this->dropIndex('tbi_user_tenant_id_position_id_idx', '{{%user}}');
    }

}
