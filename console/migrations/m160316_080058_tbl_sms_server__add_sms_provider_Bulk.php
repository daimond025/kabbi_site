<?php
use yii\db\Migration;

class m160316_080058_tbl_sms_server__add_sms_provider_Bulk extends Migration
{

    const TABLE_NAME = '{{%sms_server}}';
    const ID         = 7;

    public function up()
    {
        $this->insert(self::TABLE_NAME,
            [
            'server_id' => self::ID,
            'name'      => 'Bulk (Romania)',
            'host'      => 'http://gw1.unifun.com:11480/BulkSMSAPI/Unifun',
            'active'    => 1,
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME, 'server_id = :id', ['id' => self::ID]);
    }
    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
