<?php

use yii\db\Schema;
use yii\db\Migration;

class m151204_100106_tbl_option_tariff extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%option_tariff}}', 'planting_include_day', Schema::TYPE_FLOAT . ' UNSIGNED DEFAULT 0');
        $this->alterColumn('{{%option_tariff}}', 'planting_include_night', Schema::TYPE_FLOAT . ' UNSIGNED DEFAULT 0');
        $this->alterColumn('{{%option_tariff}}', 'min_price_day', Schema::TYPE_FLOAT . ' UNSIGNED DEFAULT 0');
        $this->alterColumn('{{%option_tariff}}', 'min_price_night', Schema::TYPE_FLOAT . ' UNSIGNED DEFAULT 0');
    }

    public function down()
    {
        $this->alterColumn('{{%option_tariff}}', 'planting_include_day', Schema::TYPE_INTEGER . ' UNSIGNED DEFAULT 0');
        $this->alterColumn('{{%option_tariff}}', 'planting_include_night', Schema::TYPE_INTEGER . ' UNSIGNED DEFAULT 0');
        $this->alterColumn('{{%option_tariff}}', 'min_price_day', Schema::TYPE_INTEGER . ' UNSIGNED DEFAULT 0');
        $this->alterColumn('{{%option_tariff}}', 'min_price_night', Schema::TYPE_INTEGER . ' UNSIGNED DEFAULT 0');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
