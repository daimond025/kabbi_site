<?php

use yii\db\Migration;
use common\modules\tenant\models\DefaultSettings;

class m180720_111558_add__min_worker_review_rating__setting extends Migration
{
    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const SETTING_NAME = 'WORKER_MIN_REVIEW_RATING';

    public function safeUp()
    {
        $printCheck = new DefaultSettings([
            'name'  => self::SETTING_NAME,
            'value' => '0',
            'type'  => 'drivers',
        ]);

        return $printCheck->save();
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::SETTING_NAME,
            'type' => 'drivers',
        ]);
    }
}
