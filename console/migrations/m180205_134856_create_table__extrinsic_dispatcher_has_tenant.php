<?php

use yii\db\Migration;

class m180205_134856_create_table__extrinsic_dispatcher_has_tenant extends Migration
{

    const TABLE_NAME = '{{%tenant_has_extrinsic_dispatcher}}';

    const COLUMN_NAME_USER_ID = 'user_id';
    const COLUMN_NAME_TENANT_ID = 'tenant_id';

    const FK_USER = 'fk_tenant_has_extrinsic_dispatcher__user';
    const FK_TENANT = 'fk_tenant_has_extrinsic_dispatcher__tenant';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            self::COLUMN_NAME_USER_ID => $this->integer()->unsigned(),
            self::COLUMN_NAME_TENANT_ID => $this->integer()->unsigned(),
        ]);

        $this->addPrimaryKey('idx_tenant_has_extrinsic_dispatcher', self::TABLE_NAME, [
            self::COLUMN_NAME_USER_ID, self::COLUMN_NAME_TENANT_ID
        ]);

        $this->addForeignKey(self::FK_USER, self::TABLE_NAME, self::COLUMN_NAME_USER_ID, '{{%user}}', self::COLUMN_NAME_USER_ID, 'CASCADE', 'CASCADE');
        $this->addForeignKey(self::FK_TENANT, self::TABLE_NAME, self::COLUMN_NAME_TENANT_ID, '{{%tenant}}', self::COLUMN_NAME_TENANT_ID, 'CASCADE', 'CASCADE');

    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_TENANT, self::TABLE_NAME);
        $this->dropForeignKey(self::FK_USER, self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
    }
}
