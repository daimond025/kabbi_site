<?php

use yii\db\Schema;
use yii\db\Migration;

class m151224_110324_tbl_street extends Migration
{
    const TABLE_NAME = '{{%street}}';

    public function up()
    {
        $this->execute("UPDATE" . self::TABLE_NAME ." SET fulladdress = REPLACE(fulladdress, ' п,', ',')");
        $this->execute("UPDATE" . self::TABLE_NAME ." SET fulladdress = REPLACE(fulladdress, ' пгт,', ',')");
        $this->execute("UPDATE" . self::TABLE_NAME ." SET fulladdress = REPLACE(fulladdress, ' х,', ',')");
    }

    public function down()
    {
        echo "m151224_110324_tbl_street cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
