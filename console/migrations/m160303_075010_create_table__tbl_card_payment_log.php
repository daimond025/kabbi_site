<?php
use yii\db\Migration;

class m160303_075010_create_table__tbl_card_payment_log extends Migration
{

    const TABLE_NAME  = '{{%card_payment_log}}';
    const TABLE_TENANT = '{{%tenant}}';
    const TABLE_CLIENT = '{{%client}}';
    const TABLE_ORDER = '{{%order}}';

    public function up()
    {
        $this->createTable(self::TABLE_NAME,
            [
            'id'         => $this->primaryKey(),
            'tenant_id'  => 'INT(10) UNSIGNED NOT NULL',
            'client_id'  => 'INT(10) UNSIGNED NOT NULL',
            'order_id'   => 'INT(10) UNSIGNED',
            'type'       => "ENUM('PAY', 'REFUND', 'ADD CARD', 'CHECK CARD', 'REMOVE CARD') NOT NULL",
            'params'     => $this->text(),
            'response'   => $this->text(),
            'result'     => $this->integer(1)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk_card_payment_log__tenant_id', self::TABLE_NAME,
            'tenant_id', self::TABLE_TENANT, 'tenant_id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk_card_payment_log__client_id', self::TABLE_NAME,
            'client_id', self::TABLE_CLIENT, 'client_id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk_card_payment_log__order_id', self::TABLE_NAME,
            'order_id', self::TABLE_ORDER, 'order_id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
