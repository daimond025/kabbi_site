<?php

use yii\db\Migration;

class m180529_090905_detault_value_on_taxi_tariff_options extends Migration
{
    const TABLE_NAME = '{{%taxi_tariff_option}}';

    public function safeUp()
    {
        $this->alterColumn(self::TABLE_NAME, 'wait_time', $this->integer(3)->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->alterColumn(self::TABLE_NAME, 'wait_time', $this->integer(3));
    }
}
