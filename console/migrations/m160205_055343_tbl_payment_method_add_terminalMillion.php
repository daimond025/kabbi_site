<?php

use yii\db\Schema;
use yii\db\Migration;

class m160205_055343_tbl_payment_method_add_terminalMillion extends Migration
{
    
    const TABLE_NAME = '{{%payment_method}}';
    const PAYMENT = 'TERMINAL_MILLION';
    
    public function up()
    {

        
        $this->insert(self::TABLE_NAME, [
            'payment' => self::PAYMENT,
            'name' => 'Million',
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME, 'payment = :payment', [
            'payment' => self::PAYMENT,
        ]);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
