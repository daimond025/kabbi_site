<?php

use yii\db\Migration;

class m160424_092724_tbl_tenant_tariff__add_column extends Migration
{
    const TABLE_TENANT_TARIFF = '{{%tenant_tariff}}';
    const TABLE_PAYMENT_FOR_TARIFF = '{{%payment_for_tariff}}';
    const COLUMN_PAYMENT_FOR_TARIFF_ID = 'payment_for_tariff_id';
    const FOREIGN_KEY_NAME = 'fk_tenant_tariff__payment_for_tariff_id';

    public function up()
    {
        $this->addColumn(self::TABLE_TENANT_TARIFF, self::COLUMN_PAYMENT_FOR_TARIFF_ID, $this->integer());
        $this->addForeignKey(self::FOREIGN_KEY_NAME, self::TABLE_TENANT_TARIFF, self::COLUMN_PAYMENT_FOR_TARIFF_ID,
            self::TABLE_PAYMENT_FOR_TARIFF, 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey(self::FOREIGN_KEY_NAME, self::TABLE_TENANT_TARIFF);
        $this->dropColumn(self::TABLE_TENANT_TARIFF, self::COLUMN_PAYMENT_FOR_TARIFF_ID);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
