<?php

use yii\db\Schema;
use yii\db\Migration;

class m150915_065852_truncate_incorrect_street_names extends Migration
{
    const TABLE_NAME = '{{%street}}';
    public function up()
    {
        $this->execute("UPDATE" . self::TABLE_NAME ." SET street_name = REPLACE(street_name, 'им ', '')");
    }

    public function down()
    {
        echo "m150915_065852_truncate_incorrect_street_names cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
