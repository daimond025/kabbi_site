<?php

use yii\db\Migration;

class m170111_124523_notification_table extends Migration
{
    const TABLE_NAME = '{{%notification}}';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'notification_id' => $this->primaryKey()->unsigned(),
            'tenant_id'       => $this->integer(11)->unsigned()->notNull(),
            'name'            => $this->string(100)->notNull(),
            'addressee'       => "ENUM('WORKER', 'CLIENT') NOT NULL",
            'addressee_id'    => $this->integer(11)->comment('Если пустое, то отправляет всем'),
            'title'           => $this->string(100)->notNull(),
            'text'            => $this->string(200)->notNull(),
        ]);

        $this->addForeignKey('fk_notification__tenant_id', self::TABLE_NAME, 'tenant_id',
            '{{%tenant}}', 'tenant_id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_notification__tenant_id', self::TABLE_NAME);
        $this->dropTable(self::TABLE_NAME);
    }
}
