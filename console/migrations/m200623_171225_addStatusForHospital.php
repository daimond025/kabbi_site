<?php

use yii\db\Migration;

class m200623_171225_addStatusForHospital extends Migration
{
    const TABLE_ORDER_STATUS = '{{%order_status}}';

    const STATUS_HOSPITAL_PRE_ORDER_FOR_ALL = 203;
    const STATUS_HOSPITAL_FREE_ORDER_FOR_ALL = 204;

    const STATUS_HOSPITAL_PRE_ORDER_FOR_COMPANY = 205;
    const STATUS_HOSPITAL_FREE_ORDER__COMPANY= 206;

    public function up()
    {
        $this->batchInsert(self::TABLE_ORDER_STATUS,
            ['status_id', 'name', 'status_group', 'dispatcher_sees'],
            [
                [self::STATUS_HOSPITAL_PRE_ORDER_FOR_ALL, 'Hospital pre-order status for all', 'new', 1],
                [self::STATUS_HOSPITAL_FREE_ORDER_FOR_ALL, 'Hospital free status for all', 'new', 1],

                [self::STATUS_HOSPITAL_PRE_ORDER_FOR_COMPANY, 'Hospital pre-order status for company', 'new', 1],
                [self::STATUS_HOSPITAL_FREE_ORDER__COMPANY, 'Hospital free status for company', 'new', 1],
            ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_ORDER_STATUS, [
            'status_id' => [
                self::STATUS_HOSPITAL_PRE_ORDER_FOR_ALL,
                self::STATUS_HOSPITAL_FREE_ORDER_FOR_ALL,
                self::STATUS_HOSPITAL_PRE_ORDER_FOR_COMPANY,
                self::STATUS_HOSPITAL_FREE_ORDER__COMPANY
            ],
        ]);
    }
}
