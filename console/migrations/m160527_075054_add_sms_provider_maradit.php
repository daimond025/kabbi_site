<?php

use yii\db\Migration;

class m160527_075054_add_sms_provider_maradit extends Migration
{
    
    const TABLE_NAME = '{{%sms_server}}';
    
    const COLUMN_SERVER_ID = 9;
    
    public function up()
    {
        $this->insert(self::TABLE_NAME,[
            'server_id' => self::COLUMN_SERVER_ID,
            'name' =>  'Motive.az (Азербайджан)',
            'host' => 'http://motive.az/',
            'active' => '1',
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME,['server_id' => self::COLUMN_SERVER_ID]);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
