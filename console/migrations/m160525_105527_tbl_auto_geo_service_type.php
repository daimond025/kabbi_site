<?php

use yii\db\Migration;
use yii\db\Schema;

class m160525_105527_tbl_auto_geo_service_type extends Migration
{
    const TABLE_NAME = '{{%auto_geo_service_type}}';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'            => $this->primaryKey(),
            'service_label' => $this->text(),
            'active'        => Schema::TYPE_BOOLEAN . ' DEFAULT 0'
        ]);
    }

    function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
