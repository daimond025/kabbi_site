<?php

use yii\db\Schema;
use yii\db\Migration;

class m150903_115437_tbl_elecsnet_log extends Migration
{

    const TABLE_NAME = '{{%elecsnet_log}}';

    public function up()
    {
        $tableOptions = $this->db->driverName === 'mysql' ? 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB' : null;

        $this->createTable(self::TABLE_NAME, [
            'log_id'      => Schema::TYPE_PK,
            'type'        => 'ENUM("1", "2") NOT NULL',
            'reqid'       => Schema::TYPE_STRING . ' NOT NULL',
            'amount'      => Schema::TYPE_STRING,
            'auth_code'   => Schema::TYPE_STRING,
            'currency'    => Schema::TYPE_STRING,
            'result'      => Schema::TYPE_STRING . ' NOT NULL',
            'date'        => Schema::TYPE_STRING,
            'create_time' => Schema::TYPE_INTEGER,
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
