<?php

use yii\db\Migration;
use yii\db\Query;

class m160906_133050__tbl_client_bonus__drop_old_columns extends Migration
{
    const TABLE_CLIENT_BONUS_GOOTAX = '{{%client_bonus_gootax}}';
    const TABLE_CLIENT_BONUS = '{{%client_bonus}}';

    public function up()
    {
        $this->dropColumn(self::TABLE_CLIENT_BONUS, 'actual_date');
        $this->dropColumn(self::TABLE_CLIENT_BONUS, 'bonus_type');
        $this->dropColumn(self::TABLE_CLIENT_BONUS, 'bonus');
        $this->dropColumn(self::TABLE_CLIENT_BONUS, 'min_cost');
        $this->dropColumn(self::TABLE_CLIENT_BONUS, 'payment_method');
        $this->dropColumn(self::TABLE_CLIENT_BONUS, 'max_payment_type');
        $this->dropColumn(self::TABLE_CLIENT_BONUS, 'max_payment');
        $this->dropColumn(self::TABLE_CLIENT_BONUS, 'bonus_app_type');
        $this->dropColumn(self::TABLE_CLIENT_BONUS, 'bonus_app');
    }

    public function down()
    {
        $this->addColumn(self::TABLE_CLIENT_BONUS, 'actual_date', $this->text());
        $this->addColumn(self::TABLE_CLIENT_BONUS, 'bonus_type', "ENUM('BONUS', 'PERCENT') NOT NULL DEFAULT 'PERCENT'");
        $this->addColumn(self::TABLE_CLIENT_BONUS, 'bonus', $this->decimal(10, 2)->notNull());
        $this->addColumn(self::TABLE_CLIENT_BONUS, 'min_cost', $this->decimal(10, 2)->notNull()->defaultValue(0));
        $this->addColumn(self::TABLE_CLIENT_BONUS, 'payment_method', "ENUM('FULL', 'PARTIAL') NOT NULL DEFAULT 'FULL'");
        $this->addColumn(self::TABLE_CLIENT_BONUS, 'max_payment_type',
            "ENUM('BONUS', 'PERCENT') NOT NULL DEFAULT 'PERCENT'");
        $this->addColumn(self::TABLE_CLIENT_BONUS, 'max_payment', $this->decimal(10, 2)->defaultValue(0));
        $this->addColumn(self::TABLE_CLIENT_BONUS, 'bonus_app_type',
            "ENUM('BONUS', 'PERCENT') NOT NULL DEFAULT 'PERCENT'");
        $this->addColumn(self::TABLE_CLIENT_BONUS, 'bonus_app', $this->decimal(10, 2)->notNull()->defaultValue(0));

        $rows = (new Query())
            ->from(self::TABLE_CLIENT_BONUS_GOOTAX)
            ->all();
        foreach ($rows as $row) {
            $this->update(self::TABLE_CLIENT_BONUS, [
                'actual_date'      => $row['actual_date'],
                'bonus_type'       => $row['bonus_type'],
                'bonus'            => $row['bonus'],
                'min_cost'         => $row['min_cost'],
                'payment_method'   => $row['payment_method'],
                'max_payment_type' => $row['max_payment_type'],
                'max_payment'      => $row['max_payment'],
                'bonus_app_type'   => $row['bonus_app_type'],
                'bonus_app'        => $row['bonus_app'],
            ], [
                'bonus_id' => $row['bonus_id'],
            ]);
        }
    }

}
