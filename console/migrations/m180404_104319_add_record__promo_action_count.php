<?php

use yii\db\Migration;

class m180404_104319_add_record__promo_action_count extends Migration
{

    const TABLE_NAME = '{{%promo_action_count}}';

    const COLUMN_NAME = 'name';

    const RECORD_NAME = '1 time immediately after the promotion code is activated';

    public function safeUp()
    {
        $this->insert(self::TABLE_NAME, [self::COLUMN_NAME => self::RECORD_NAME]);
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_NAME, [self::COLUMN_NAME => self::RECORD_NAME]);
        $this->execute('ALTER TABLE tbl_promo_action_count AUTO_INCREMENT=3;');
    }
}
