<?php

use yii\db\Migration;

/**
 * Handles the creation of table `phone_line_outbound_routing__has_taxi_tariff`.
 */
class m170815_111828_create_phone_line_outbound_routing__has_taxi_tariff_table extends Migration
{
    const TABLE_NAME = '{{%phone_line_outbound_routing_has_taxi_tariff}}';
    const TABLE_ROUTING = '{{%phone_line_outbound_routing}}';
    const TABLE_TAXI_TARIFF = '{{%taxi_tariff}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'         => $this->primaryKey()->unsigned(),
            'routing_id' => $this->integer(10)->unsigned()->notNull(),
            'tariff_id'  => $this->integer(10)->unsigned()->notNull(),
        ]);

        $this->addForeignKey('FK_routing_id__phone_line_outbound_routing_has_taxi_tariff', self::TABLE_NAME,
            'routing_id', self::TABLE_ROUTING, 'routing_id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('FK_tariff_id__phone_line_outbound_routing_has_taxi_tariff', self::TABLE_NAME, 'tariff_id',
            self::TABLE_TAXI_TARIFF, 'tariff_id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('FK_tariff_id__phone_line_outbound_routing_has_taxi_tariff', self::TABLE_NAME);
        $this->dropForeignKey('FK_routing_id__phone_line_outbound_routing_has_taxi_tariff', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
    }
}
