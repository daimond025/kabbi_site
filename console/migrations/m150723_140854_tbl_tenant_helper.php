<?php

use yii\db\Schema;
use yii\db\Migration;

class m150723_140854_tbl_tenant_helper extends Migration
{
    public function up()
    {
        $this->execute('SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0');
        $this->execute('SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0');   
        $this->execute("SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES'");
        
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%tenant_helper}}', [
            'helper_id' => Schema::TYPE_PK,
            'tenant_id' => Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL',
            'info' => Schema::TYPE_BOOLEAN . ' DEFAULT 0',
            'parking' => Schema::TYPE_BOOLEAN . ' DEFAULT 0',
            'driver_tariff' => Schema::TYPE_BOOLEAN . ' DEFAULT 0',
            'client_tariff' => Schema::TYPE_BOOLEAN . ' DEFAULT 0',
            'car' => Schema::TYPE_BOOLEAN . ' DEFAULT 0',
            'driver' => Schema::TYPE_BOOLEAN . ' DEFAULT 0',
        ], $tableOptions);
        
        $this->createIndex('FK_tenant_helper_idx', '{{%tenant_helper}}', 'tenant_id');
        $this->addForeignKey('FK_tenant_helper_tenant_id', '{{%tenant_helper}}', 'tenant_id', '{{%tenant}}', 'tenant_id', 'CASCADE', 'CASCADE');
        
        $this->execute('SET SQL_MODE=@OLD_SQL_MODE');
        $this->execute('SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS');
        $this->execute('SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS');
    }

    public function down()
    {
        $this->dropForeignKey('FK_tenant_helper_tenant_id', '{{%tenant_helper}}');
        $this->dropIndex('FK_tenant_helper_idx', '{{%tenant_helper}}');
        $this->dropTable('{{%tenant_helper}}');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
