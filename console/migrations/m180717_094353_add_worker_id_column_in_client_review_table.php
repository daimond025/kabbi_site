<?php

use yii\db\Migration;
use yii\helpers\ArrayHelper;

class m180717_094353_add_worker_id_column_in_client_review_table extends Migration
{
    const TABLE_NAME = '{{%client_review}}';

    public function safeUp()
    {
                $this->addColumn(self::TABLE_NAME, 'worker_id', $this->integer(10)->after('order_id'));
                $this->addColumn(self::TABLE_NAME, 'position_id', $this->integer(10)->after('worker_id'));

                $this->addForeignKey(
                    'fk_worker_id__client_review',
                    self::TABLE_NAME,
                    'worker_id',
                    '{{%worker}}',
                    'worker_id',
                    'CASCADE',
                    'CASCADE'
                );

                $this->addForeignKey(
                    'fk_position_id__client_review',
                    self::TABLE_NAME,
                    'position_id',
                    '{{%position}}',
                    'position_id',
                    'CASCADE',
                    'CASCADE'
                );

        $this->dataFill();
    }

    public function safeDown()
    {
                $this->dropForeignKey('fk_position_id__client_review', self::TABLE_NAME);
                $this->dropForeignKey('fk_worker_id__client_review', self::TABLE_NAME);

                $this->dropColumn(self::TABLE_NAME, 'position_id');
                $this->dropColumn(self::TABLE_NAME, 'worker_id');
    }

    private function dataFill()
    {
        $clientReviewsQuery = (new \yii\db\Query())->from(self::TABLE_NAME);

        foreach ($clientReviewsQuery->batch(1000) as $rows) {
            $orderIds = ArrayHelper::map($rows, 'review_id', 'order_id');
            $workers  = $this->getWorkerIdsInOrders($orderIds);
            $workers  = ArrayHelper::map($workers, 'order_id', function ($item) {
                return [
                    'worker_id'   => ArrayHelper::getValue($item, 'worker_id'),
                    'position_id' => ArrayHelper::getValue($item, 'position_id'),
                ];
            });

            foreach ($rows as $row) {
                $orderId = ArrayHelper::getValue($row, 'order_id');

                try {
                    $this->update(
                        self::TABLE_NAME,
                        [
                            'worker_id'   => ArrayHelper::getValue($workers, "$orderId.worker_id"),
                            'position_id' => ArrayHelper::getValue($workers, "$orderId.position_id"),
                        ],
                        ['review_id' => ArrayHelper::getValue($row, 'review_id')]
                    );
                } catch (\yii\db\IntegrityException $exception) {
                    echo ' exception ' . get_class($exception) . PHP_EOL;
                }
            }

        }
    }

    private function getWorkerIdsInOrders($orderIds)
    {
        return (new \yii\db\Query())
            ->select(['order_id', 'worker_id', 'position_id'])
            ->from('{{%order}}')
            ->where(['order_id' => $orderIds])
            ->orderBy('order_id')
            ->all();
    }
}
