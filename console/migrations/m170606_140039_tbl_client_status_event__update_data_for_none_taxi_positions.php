<?php

use yii\db\Migration;

class m170606_140039_tbl_client_status_event__update_data_for_none_taxi_positions extends Migration
{
    const SQL_UPDATE_DATA_NONE_TAXI = <<<SQL
UPDATE `tbl_client_status_event` AS `t`
JOIN `tbl_default_client_status_event` AS `d` ON `t`.`status_id` = `d`.`status_id` AND `t`.`group` = `d`.`group` AND `t`.`position_id` = `d`.`position_id`
SET `t`.`notice` = `d`.`notice`
WHERE `t`.`position_id` > 1
SQL;

    public function safeUp()
    {
        $this->execute(self::SQL_UPDATE_DATA_NONE_TAXI);
    }

    public function safeDown()
    {
    }

}
