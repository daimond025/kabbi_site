<?php

use yii\db\Migration;
use yii\db\Query;

class m160418_081725_courier_notify extends Migration
{
    const SMS_TABLE = '{{%default_sms_template}}';
    const PUSH_TABLE = '{{%default_client_push_notifications}}';
    const SMS_TABLE_CR = '{{%default_sms_template_courier}}';
    const PUSH_TABLE_CR = '{{%default_client_push_notifications_courier}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable(
            self::SMS_TABLE_CR,
            [
                'template_id' => $this->primaryKey(),
                'type'        => $this->string(45)->notNull(),
                'text'        => $this->text()->notNull(),
                'description' => $this->text()->notNull(),
            ],
            $tableOptions
        );

        $this->createIndex('idx_default_sms_template_courier', self::SMS_TABLE_CR, 'type', true);

        $sms_data = (new Query())
            ->from(self::SMS_TABLE)
            ->select(['type', 'text', 'description'])
            ->all();

        foreach ($sms_data as $index => $row) {
            $sms_data[$index] = array_values($row);
        }

        $this->batchInsert(self::SMS_TABLE_CR, ['type', 'text', 'description'], $sms_data);

        $this->createTable(
            self::PUSH_TABLE_CR,
            [
                'template_id' => $this->primaryKey(),
                'type'        => $this->string(45)->notNull(),
                'text'        => $this->text()->notNull(),
                'description' => $this->text()->notNull(),
            ],
            $tableOptions
        );

        $this->createIndex('idx_default_client_push_notifications_courier', self::PUSH_TABLE_CR, 'type', true);

        $push_data = (new Query())
            ->from(self::PUSH_TABLE)
            ->select(['type', 'text', 'description'])
            ->all();

        foreach ($push_data as $index => $row) {
            $push_data[$index] = array_values($row);
        }

        $this->batchInsert(self::PUSH_TABLE_CR, ['type', 'text', 'description'], $push_data);
    }


    public function down()
    {
        $this->dropTable(self::SMS_TABLE_CR);
        $this->dropTable(self::PUSH_TABLE_CR);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
