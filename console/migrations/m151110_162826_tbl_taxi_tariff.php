<?php

use yii\db\Schema;
use yii\db\Migration;

class m151110_162826_tbl_taxi_tariff extends Migration
{
    public function up()
    {
        $this->addColumn('{{%taxi_tariff}}', 'enabled_bordur', Schema::TYPE_BOOLEAN . ' DEFAULT 1');
    }

    public function down()
    {
        $this->dropColumn('{{%taxi_tariff}}', 'enabled_bordur');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
