<?php

use yii\db\Migration;
use common\modules\tenant\models\DefaultSettings;

class m180417_083431_add_setting__max_count_reserve_urgent_orders extends Migration
{


    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const TABLE_TENANT_SETTING = '{{%tenant_setting}}';
    const SETTING_NAME = 'MAX_COUNT_RESERVE_URGENT_ORDERS';
    const SETTING_TYPE = 'drivers';

    public function safeUp()
    {
        $model = new DefaultSettings([
            'name'  => self::SETTING_NAME,
            'type'  => self::SETTING_TYPE,
        ]);

        $model->save();

        return true;
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_DEFAULT_SETTINGS, ['name' => self::SETTING_NAME, 'type' => self::SETTING_TYPE]);

        $this->delete(self::TABLE_TENANT_SETTING, ['name' => self::SETTING_NAME, 'type' => self::SETTING_TYPE]);


        return true;
    }
}
