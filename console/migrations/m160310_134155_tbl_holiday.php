<?php

use yii\db\Migration;

class m160310_134155_tbl_holiday extends Migration
{
    const TABLE_NAME = '{{%holiday}}';

    public function up()
    {
        $this->update(self::TABLE_NAME, ['date' => 'Sunday'], ['date' => 'Воскресенье']);
        $this->update(self::TABLE_NAME, ['date' => 'Saturday'], ['date' => 'Суббота']);
    }

    public function down()
    {
        echo "m160310_134155_tbl_holidays cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
