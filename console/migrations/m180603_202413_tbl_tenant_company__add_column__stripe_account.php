<?php

use yii\db\Migration;

class m180603_202413_tbl_tenant_company__add_column__stripe_account extends Migration
{
    const TABLE_TENANT_COMPANY = '{{%tenant_company}}';
    const COLUMN_STRIPE_ACCOUNT = 'stripe_account';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_TENANT_COMPANY, self::COLUMN_STRIPE_ACCOUNT, $this->string(255));
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_TENANT_COMPANY, self::COLUMN_STRIPE_ACCOUNT);
    }
}
