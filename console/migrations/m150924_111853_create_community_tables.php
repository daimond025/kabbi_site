<?php

use yii\db\Schema;
use yii\db\Migration;

class m150924_111853_create_community_tables extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%section}}', [
            'section_id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
                ], $tableOptions);

        $this->batchInsert('{{%section}}', ['name'], [
            ['Блог Гутакса'],
            ['Предложения'],
            ['Общение'],
            ['Документация']]);

        $this->createTable('{{%post}}', [
            'post_id' => $this->primaryKey(),
            'section_id' => $this->integer()->notNull(),
            'title' => $this->string()->notNull(),
            'anons' => $this->text()->notNull(),
            'content' => $this->text(),
            'publication_date' => $this->integer()->notNull(),
            'status' => "ENUM('draft', 'published', 'in archive') NOT NULL DEFAULT 'draft'",
            'author_id' => $this->integer()->notNull(),
            'count_comments' => $this->integer()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
                ], $tableOptions);
        $this->addForeignKey('fk_post__section_id', '{{%post}}', 'section_id',
                '{{%section}}', 'section_id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%tag}}', [
            'tag_id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
                ], $tableOptions);
        $this->batchInsert('{{%tag}}', ['name'],
                [['О Гутаксе'], ['Работа в такси'], ['Новости такси']]);

        $this->createTable('{{%post_tag}}', [
            'post_tag_id' => $this->primaryKey(),
            'post_id' => $this->integer()->notNull(),
            'tag_id' => $this->integer()->notNull(),
                ], $tableOptions);
        $this->addForeignKey('fk_post_tag__post_id', '{{%post_tag}}', 'post_id',
                '{{%post}}', 'post_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_post_tag__tag_id', '{{%post_tag}}', 'tag_id', 
                '{{%tag}}', 'tag_id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%offer_info}}', [
            'offer_info_id' => $this->primaryKey(),
            'post_id' => $this->integer()->notNull(),
            'status' => "ENUM('voting', 'in work', 'introduced', 'deferred') NOT NULL DEFAULT 'voting'",
            'plus' => $this->integer()->notNull()->defaultValue(0),
            'minus' => $this->integer()->notNull()->defaultValue(0),
        ], $tableOptions);
        $this->createIndex('idx_offer_info__status', '{{%offer_info}}', 'status');
        $this->addForeignKey('fk_offer_info__post_id', '{{%offer_info}}', 'post_id',
                '{{%post}}', 'post_id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%offer_info}}');
        $this->dropTable('{{%post_tag}}');
        $this->dropTable('{{%tag}}');
        $this->dropTable('{{%post}}');
        $this->dropTable('{{%section}}');
    }
    
}
