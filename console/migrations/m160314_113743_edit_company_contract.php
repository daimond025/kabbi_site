<?php

use yii\db\Migration;

class m160314_113743_edit_company_contract extends Migration
{
    
    public function up()
    {
        $this->db->createCommand('ALTER TABLE `tbl_client_company` CHANGE `contract_start_date` `contract_start_date` DATE NULL')->execute();
        $this->db->createCommand('ALTER TABLE `tbl_client_company` CHANGE `contract_end_date` `contract_end_date` DATE NULL')->execute();
        
        $this->update('tbl_client_company',['contract_start_date' => null],'contract_start_date = "0000-00-00"');
        $this->update('tbl_client_company',['contract_end_date' => null],'contract_end_date = "0000-00-00"');
    }

    public function down()
    {
        $this->db->createCommand("ALTER TABLE `tbl_client_company` CHANGE `contract_start_date` `contract_start_date` DATE NULL DEFAULT '0000-00-00'")->execute();
        $this->db->createCommand("ALTER TABLE `tbl_client_company` CHANGE `contract_end_date` `contract_end_date` DATE NULL DEFAULT '0000-00-00'")->execute();
    }


}
