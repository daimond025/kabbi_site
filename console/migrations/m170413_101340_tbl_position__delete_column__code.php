<?php

use yii\db\Migration;

class m170413_101340_tbl_position__delete_column__code extends Migration
{
    const TABLE_POSITION = '{{%position}}';
    const COLUMN_CODE = 'code';
    const UNIQUE_INDEX = 'idx_tbl_position_code';

    public function up()
    {
        $this->dropIndex(self::UNIQUE_INDEX, self::TABLE_POSITION);
        $this->dropColumn(self::TABLE_POSITION, self::COLUMN_CODE);
    }

    public function down()
    {
        $this->addColumn(self::TABLE_POSITION, self::COLUMN_CODE, $this->string(45));
        $this->createIndex(self::UNIQUE_INDEX, self::TABLE_POSITION, self::COLUMN_CODE, true);
        $this->db->createCommand('update tbl_position set code = position_id')->execute();
        $this->alterColumn(self::TABLE_POSITION, self::COLUMN_CODE, $this->string(45)->notNull());
    }

}
