<?php

use yii\db\Migration;

class m160329_122158_add_code_column_to_tenant_tariff_tables extends Migration
{
    const TABLE_MODULE = '{{%module}}';
    const TABLE_PERMISSION = '{{%permission}}';
    const TABLE_TARIFF_ADDITIONAL_OPTION = '{{%tariff_additional_option}}';
    const TABLE_ONE_TIME_SERVICE = '{{%one_time_service}}';
    const TABLE_TARIFF_ENTITY_TYPE = '{{%tariff_entity_type}}';

    const COLUMN_CODE = 'code';

    public $tables = [
        self::TABLE_MODULE,
        self::TABLE_PERMISSION,
        self::TABLE_TARIFF_ADDITIONAL_OPTION,
        self::TABLE_ONE_TIME_SERVICE,
        self::TABLE_TARIFF_ENTITY_TYPE,
    ];

    public function up()
    {
        foreach ($this->tables as $table) {
            $this->addColumn($table, self::COLUMN_CODE,
                $this->integer()->notNull()->unique());
        }
    }

    public function down()
    {
        foreach ($this->tables as $table) {
            $this->dropColumn($table, self::COLUMN_CODE);
        }
    }
}
