<?php

use yii\db\Schema;
use yii\db\Migration;

class m151110_080155_tbl_street_new_column extends Migration
{

    public function up()
    {
        $this->addColumn('{{%street}}', 'city_name_en', 'VARCHAR(100) DEFAULT  NULL');
        $this->addColumn('{{%street}}', 'city_name_az', 'VARCHAR(100) DEFAULT  NULL');
        $this->addColumn('{{%street}}', 'city_name_de', 'VARCHAR(100) DEFAULT  NULL');
        $this->addColumn('{{%street}}', 'city_name_se', 'VARCHAR(100) DEFAULT  NULL');
        $this->addColumn('{{%street}}', 'street_name_az', 'VARCHAR(100) DEFAULT  NULL');
        $this->addColumn('{{%street}}', 'street_name_en', 'VARCHAR(100) DEFAULT  NULL');
        $this->addColumn('{{%street}}', 'street_name_de', 'VARCHAR(100) DEFAULT  NULL');
        $this->addColumn('{{%street}}', 'street_name_se', 'VARCHAR(100) DEFAULT  NULL');
        $this->addColumn('{{%street}}', 'street_shortname_az', 'VARCHAR(100) DEFAULT  NULL');
        $this->addColumn('{{%street}}', 'street_shortname_en', 'VARCHAR(100) DEFAULT  NULL');
        $this->addColumn('{{%street}}', 'street_shortname_de', 'VARCHAR(100) DEFAULT  NULL');
        $this->addColumn('{{%street}}', 'street_shortname_se', 'VARCHAR(100) DEFAULT  NULL');
        $this->addColumn('{{%street}}', 'fulladdress_az', 'VARCHAR(200) DEFAULT  NULL');
        $this->addColumn('{{%street}}', 'fulladdress_en', 'VARCHAR(200) DEFAULT  NULL');
        $this->addColumn('{{%street}}', 'fulladdress_de', 'VARCHAR(200) DEFAULT  NULL');
        $this->addColumn('{{%street}}', 'fulladdress_se', 'VARCHAR(200) DEFAULT  NULL');
    }

    public function down()
    {
        $this->dropColumn('{{%street}}', 'city_name_en');
        $this->dropColumn('{{%street}}', 'city_name_az');
        $this->dropColumn('{{%street}}', 'city_name_de');
        $this->dropColumn('{{%street}}', 'city_name_se');
        $this->dropColumn('{{%street}}', 'street_name_az');
        $this->dropColumn('{{%street}}', 'street_name_en');
        $this->dropColumn('{{%street}}', 'street_name_de');
        $this->dropColumn('{{%street}}', 'street_name_se');
        $this->dropColumn('{{%street}}', 'street_shortname_az');
        $this->dropColumn('{{%street}}', 'street_shortname_en');
        $this->dropColumn('{{%street}}', 'street_shortname_de');
        $this->dropColumn('{{%street}}', 'street_shortname_se');
        $this->dropColumn('{{%street}}', 'fulladdress_az');
        $this->dropColumn('{{%street}}', 'fulladdress_en');
        $this->dropColumn('{{%street}}', 'fulladdress_de');
        $this->dropColumn('{{%street}}', 'fulladdress_se');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
