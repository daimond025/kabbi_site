<?php

use yii\db\Migration;

class m160928_045724_add_field_to_tbl_order extends Migration
{
    const TABLE_NAME = '{{%order}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'update_time', $this->integer());
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'update_time');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
