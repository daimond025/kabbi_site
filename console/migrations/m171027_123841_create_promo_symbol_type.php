<?php

use yii\db\Migration;

class m171027_123841_create_promo_symbol_type extends Migration
{

    const TABLE_NAME = '{{%promo_symbol_type}}';

    const COLUMN_SYMBOL_TYPE_ID = 'symbol_type_id';
    const COLUMN_NAME = 'name';

    const DEFAULT_VALUE = [
        ['Lowercase latin letters'],
        ['Digits'],
    ];

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            self::COLUMN_SYMBOL_TYPE_ID => $this->primaryKey(),
            self::COLUMN_NAME           => $this->string(255),
        ]);

        $this->batchInsert(
            self::TABLE_NAME,
            [self::COLUMN_NAME],
            self::DEFAULT_VALUE
        );
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }

}
