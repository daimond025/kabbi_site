<?php

use yii\db\Schema;
use yii\db\Migration;

class m151005_080552_create_community_user_tables extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%community_user}}', [
            'id' => $this->primaryKey(),
            'firstname' => $this->string()->notNull(),
            'lastname' => $this->string()->notNull(),
            'secondname' => $this->string(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string(),
            'email' => $this->string()->notNull(),
            'email_confirm_token' => $this->string(),
            'status' => $this->smallInteger()->notNull()->defaultValue(20),
            'user_id' => 'int(10) unsigned DEFAULT NULL',
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
                ], $tableOptions);
        $this->addForeignKey('fk_post__author_id', '{{%post}}', 'author_id',
                '{{%community_user}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_community_user__user_id', '{{%community_user}}', 'user_id',
                '{{%user}}', 'user_id', 'SET NULL', 'CASCADE');
        $this->createIndex('idx_community_user__id__user_id',
                '{{%community_user}}', ['id', 'user_id'], true);

        $this->createTable('{{%community_auth}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'source' => $this->string()->notNull(),
            'source_id' => $this->string()->notNull(),
            ], $tableOptions);
        $this->addForeignKey('fk_community_auth__user_id', '{{%community_auth}}', 'user_id',
                '{{%community_user}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%community_auth}}');
        $this->dropForeignKey('fk_post__author_id', '{{%post}}');
        $this->dropTable('{{%community_user}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
