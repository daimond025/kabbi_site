<?php

use yii\db\Migration;
use common\modules\tenant\models\DefaultSettings;

class m180109_105516_add_setting_to_default_setting_table extends Migration
{

    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const TABLE_TENANT_SETTING = '{{%tenant_setting}}';
    const SETTING_NAME = 'ALLOW_EDIT_COST_ORDER';
    const SETTING_VALUE = '0';
    const SETTING_TYPE = 'drivers';

    public function safeUp()
    {
        $model = new DefaultSettings([
            'name'  => self::SETTING_NAME,
            'value' => self::SETTING_VALUE,
            'type'  => self::SETTING_TYPE,
        ]);

        $model->save();

        return true;
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_DEFAULT_SETTINGS, ['name' => self::SETTING_NAME, 'value' => self::SETTING_VALUE, 'type' => self::SETTING_TYPE]);

        $this->delete(self::TABLE_TENANT_SETTING, ['name' => self::SETTING_NAME, 'type' => self::SETTING_TYPE]);

        return true;
    }


}
