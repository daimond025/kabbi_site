<?php

use yii\db\Migration;

class m170710_093517_add_columns_in_phone_line_table extends Migration
{
    const TABLE_NAME = '{{%phone_line}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'r_username', $this->string(50)->notNull());
        $this->addColumn(self::TABLE_NAME, 'r_domain', $this->string(50)->notNull());
        $this->addColumn(self::TABLE_NAME, 'sip_server_local', $this->string(255)->notNull());
        $this->addColumn(self::TABLE_NAME, 'auth_proxy', $this->string(255)->notNull());
        $this->addColumn(self::TABLE_NAME, 'port', $this->integer(5)->notNull());
        $this->addColumn(self::TABLE_NAME, 'realm', $this->string(50)->notNull());
        $this->addColumn(self::TABLE_NAME, 'typeServer', 'ENUM("kamailio", "asterisk") NOT NULL DEFAULT "kamailio"');
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'r_username');
        $this->dropColumn(self::TABLE_NAME,'r_domain');
        $this->dropColumn(self::TABLE_NAME, 'sip_server_local');
        $this->dropColumn(self::TABLE_NAME, 'auth_proxy');
        $this->dropColumn(self::TABLE_NAME, 'port');
        $this->dropColumn(self::TABLE_NAME, 'realm');
        $this->dropColumn(self::TABLE_NAME, 'typeServer');
    }
}
