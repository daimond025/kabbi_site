<?php

use yii\db\Migration;

class m171123_094139_add_index_to_order_change_data extends Migration
{
    const TABLE_NAME = '{{%order_change_data}}';
    const INDEX_NAME = 'index_order_id_from_order_change_data_table';
    const COLUMN_NAME = 'order_id';

    public function safeUp()
    {
        $this->createIndex(self::INDEX_NAME,self::TABLE_NAME,self::COLUMN_NAME);

        return true;

    }

    public function safeDown()
    {
        $this->dropIndex(self::INDEX_NAME,self::TABLE_NAME);

        return true;
    }

}
