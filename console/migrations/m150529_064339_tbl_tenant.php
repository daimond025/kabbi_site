<?php

use yii\db\Schema;
use yii\db\Migration;

class m150529_064339_tbl_tenant extends Migration
{
    public function up()
    {
        $this->dropColumn('{{%tenant}}', 'email_confirm');
        $this->dropColumn('{{%tenant}}', 'sms_code');
        $this->dropColumn('{{%tenant}}', 'sms_code_expire');
    }

    public function down()
    {
        $this->addColumn('{{%tenant}}', 'email_confirm', Schema::TYPE_STRING);
        $this->addColumn('{{%tenant}}', 'sms_code', Schema::TYPE_SMALLINT);
        $this->addColumn('{{%tenant}}', 'sms_code_expire', Schema::TYPE_INTEGER);
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
