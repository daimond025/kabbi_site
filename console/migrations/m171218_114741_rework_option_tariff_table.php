<?php

use yii\db\Migration;

class m171218_114741_rework_option_tariff_table extends Migration
{

    const TABLE_NAME_TARIFF = '{{%taxi_tariff}}';

    const TABLE_NAME_OPTION_TYPE = '{{%taxi_tariff_option_type}}';

    const TABLE_NAME_OPTION = '{{%option_tariff}}';
    const TABLE_NAME_OPTION_NEW = '{{%taxi_tariff_option}}';

    const TABLE_NAME_OPTION_ACTIVE_DATE = '{{%option_active_date}}';
    const TABLE_NAME_OPTION_ACTIVE_DATE_NEW = '{{%taxi_tariff_option_active_date}}';

    const TABLE_NAME_OPTION_TYPE_HAS_ADDITION_NEW = '{{%taxi_tariff_has_additional}}';
    const TABLE_NAME_OPTION_TYPE_HAS_ADDITION = '{{%additional_option}}';

    const TABLE_NAME_FIX_HAS_OPTION_NEW = '{{%taxi_tariff_has_fix}}';
    const TABLE_NAME_FIX_HAS_OPTION = '{{%fix_has_option}}';

    public function safeUp()
    {
        $this->createOptionTypeTable();
        $this->createOptionTable();
        $this->createOptionActiveDateTable();
        $this->createTariffTypeHasAdditionalOption();
        $this->createTariffHasFix();

        $this->dataFill();
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME_FIX_HAS_OPTION_NEW);
        $this->dropTable(self::TABLE_NAME_OPTION_TYPE_HAS_ADDITION_NEW);
        $this->dropTable(self::TABLE_NAME_OPTION_ACTIVE_DATE_NEW);
        $this->dropTable(self::TABLE_NAME_OPTION_NEW);
        $this->dropTable(self::TABLE_NAME_OPTION_TYPE);
    }


    protected function createOptionTypeTable()
    {
        $this->createTable(self::TABLE_NAME_OPTION_TYPE, [
            'type_id'   => $this->primaryKey(10)->unsigned(),
            'tariff_id' => $this->integer(10)->unsigned(),
            'type'      => 'ENUM("CURRENT","EXCEPTIONS") NOT NULL',
            'sort'      => $this->integer(6)->unsigned()->notNull()->defaultValue(100),
        ]);

        $this->addForeignKey('FK_tariff_id__taxi_tariff_option_type', self::TABLE_NAME_OPTION_TYPE, 'tariff_id',
            self::TABLE_NAME_TARIFF, 'tariff_id', 'CASCADE', 'CASCADE');
    }

    protected function createOptionTable()
    {
        $this->createTable(self::TABLE_NAME_OPTION_NEW, [
            'option_id'             => $this->primaryKey(10)->unsigned(),
            'type_id'               => $this->integer(10)->unsigned()->notNull(),
            'accrual'               => 'ENUM("DISTANCE","TIME","FIX","MIXED","INTERVAL") NOT NULL',
            'area'                  => 'ENUM("CITY","TRACK","RAILWAY","AIRPORT") NOT NULL',
            'planting_price'        => $this->float()->unsigned()->defaultValue(0)->comment('Стоимость посадки'),
            'planting_include'      => $this->float()->unsigned()->defaultValue(0),
            'next_km_price'         => $this->text(),
            'min_price'             => $this->float()->unsigned()->defaultValue(0),
            'second_min_price'      => $this->float()->unsigned()->defaultValue(0),
            'supply_price'          => $this->float()->unsigned()->defaultValue(0)->comment('Стоимость 1 км до адреса подачи'),
            'wait_time'             => $this->integer(3)->unsigned()->comment('Бесплатное время ожидания до начала поездки'),
            'wait_driving_time'     => $this->integer(11)->unsigned()->defaultValue(0)->comment('Бесплатное время ожидания во время поездки за 1 раз'),
            'wait_price'            => $this->float()->unsigned()->defaultValue(0)->comment('Стоимость 1 минуты ожидания'),
            'speed_downtime'        => $this->float()->unsigned()->defaultValue(0)->comment('Скорость при которой вкл. ожидание'),
            'rounding'              => $this->float()->unsigned()->defaultValue(0)->comment('Округление'),
            'rounding_type'         => 'ENUM("FLOOR","ROUND","CEIL") NOT NULL DEFAULT "ROUND"',
            'enabled_parking_ratio' => $this->integer(1)->defaultValue(1)->comment('Учитывать поправочный коэффицент районов'),
            'calculation_fix'       => $this->integer(1)->defaultValue(0)->comment('Зафиксировать расчет'),
            'next_cost_unit'        => $this->string(255)->defaultValue(null)->comment('Единица измерения "Стоимость далее"'),
            'next_km_price_time'    => $this->float()->unsigned()->defaultValue(0),
            'planting_include_time' => $this->integer(11)->unsigned()->defaultValue(0),
        ]);

        $this->addForeignKey('FK_type_id__taxi_tariff_option', self::TABLE_NAME_OPTION_NEW, 'type_id',
            self::TABLE_NAME_OPTION_TYPE, 'type_id', 'CASCADE', 'CASCADE');
    }

    protected function createOptionActiveDateTable()
    {
        $this->createTable(self::TABLE_NAME_OPTION_ACTIVE_DATE_NEW, [
            'date_id'     => $this->primaryKey()->unsigned(),
            'type_id'     => $this->integer(10)->unsigned()->notNull(),
            'active_date' => $this->string(35)->notNull(),
        ]);

        $this->addForeignKey('FK_type_id__option_tariff_active_date', self::TABLE_NAME_OPTION_ACTIVE_DATE_NEW,
            'type_id', self::TABLE_NAME_OPTION_TYPE, 'type_id', 'CASCADE', 'CASCADE');
    }

    protected function createTariffTypeHasAdditionalOption()
    {
        $this->createTable(self::TABLE_NAME_OPTION_TYPE_HAS_ADDITION_NEW, [
            'id'                   => $this->primaryKey(10)->unsigned(),
            'type_id'              => $this->integer(10)->unsigned()->notNull(),
            'additional_option_id' => 'TINYINT(3) UNSIGNED NOT NULL',
            'price'                => $this->decimal(10, 2)->notNull(),
        ]);

        $this->addForeignKey('FK_type_id__tariff_option_has_additional', self::TABLE_NAME_OPTION_TYPE_HAS_ADDITION_NEW,
            'type_id', self::TABLE_NAME_OPTION_TYPE, 'type_id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('FK_additional_option_id__tariff_option_has_additional',
            self::TABLE_NAME_OPTION_TYPE_HAS_ADDITION_NEW,
            'additional_option_id', '{{%car_option}}', 'option_id', 'CASCADE', 'CASCADE');
    }

    protected function createTariffHasFix()
    {
        $this->createTable(self::TABLE_NAME_FIX_HAS_OPTION_NEW, [
            'id'         => $this->primaryKey()->unsigned(),
            'type_id'    => $this->integer(10)->unsigned()->notNull(),
            'fix_id'     => $this->integer(10)->unsigned()->notNull(),
            'price_to'   => $this->decimal(10, 2)->notNull(),
            'price_back' => $this->decimal(10, 2)->notNull(),
        ]);

        $this->addForeignKey('FK_type_id__tariff_has_fix', self::TABLE_NAME_FIX_HAS_OPTION_NEW,
            'type_id', self::TABLE_NAME_OPTION_TYPE, 'type_id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('FK_fix_id__tariff_has_fix', self::TABLE_NAME_FIX_HAS_OPTION_NEW,
            'fix_id', '{{%fix_tariff}}', 'fix_id', 'CASCADE', 'CASCADE');
    }


    protected function dataFill()
    {
        $tariffs = $this->getTariffs();

        foreach ($tariffs as $tariff) {

            $options = $this->getOptionsByTariff($tariff['tariff_id']);

            $currentDayTypeId      = null;
            $currentNightTypeId    = null;
            $holidaysDayTypeId     = null;
            $holidaysNightTypeId   = null;
            $exceptionsDayTypeId   = null;
            $exceptionsNightTypeId = null;

            foreach ($options as $option) {

                if ($option['tariff_type'] === 'CURRENT') {

                    $additionalOptions = $this->getAdditionalOption($tariff['tariff_id'], $option['tariff_type']);
                    $tariffHasFix      = $this->getTariffHasFix($option['option_id']);


                    if ($currentDayTypeId === null) {
                        $this->insert(self::TABLE_NAME_OPTION_TYPE, [
                            'tariff_id' => $tariff['tariff_id'],
                            'type'      => 'CURRENT',
                        ]);

                        $currentDayTypeId = $this->db->getLastInsertID();

                        $insertAdditionalOptions = array_map(function ($item) use ($currentDayTypeId) {
                            return [$currentDayTypeId, $item['additional_option_id'], $item['price']];
                        }, $additionalOptions);

                        if (!empty($insertAdditionalOptions)) {
                            $this->batchInsert(self::TABLE_NAME_OPTION_TYPE_HAS_ADDITION_NEW,
                                ['type_id', 'additional_option_id', 'price'],
                                $insertAdditionalOptions);
                        }
                    }

                    $this->insert(self::TABLE_NAME_OPTION_NEW, [
                        'type_id'               => $currentDayTypeId,
                        'accrual'               => $option['accrual'],
                        'area'                  => $option['area'],
                        'planting_price'        => $option['planting_price_day'],
                        'planting_include'      => $option['planting_include_day'],
                        'next_km_price'         => $option['next_km_price_day'],
                        'min_price'             => $option['min_price_day'],
                        'second_min_price'      => $option['second_min_price_day'],
                        'supply_price'          => $option['supply_price_day'],
                        'wait_time'             => $option['wait_time_day'],
                        'wait_driving_time'     => $option['wait_driving_time_day'],
                        'wait_price'            => $option['wait_price_day'],
                        'speed_downtime'        => $option['speed_downtime_day'],
                        'rounding'              => $option['rounding_day'],
                        'rounding_type'         => $option['rounding_type_day'],
                        'enabled_parking_ratio' => $option['enabled_parking_ratio'],
                        'calculation_fix'       => $option['calculation_fix'],
                        'next_cost_unit'        => $option['next_cost_unit_day'],
                        'next_km_price_time'    => $option['next_km_price_day_time'],
                        'planting_include_time' => $option['planting_include_day_time'],
                    ]);

                    $insertTariffHasFix = array_map(function ($item) use ($currentDayTypeId) {
                        return [$currentDayTypeId, $item['fix_id'], $item['price_to'], $item['price_back']];
                    }, $tariffHasFix);

                    if (!empty($insertTariffHasFix)) {
                        $this->batchInsert(self::TABLE_NAME_FIX_HAS_OPTION_NEW,
                            ['type_id', 'fix_id', 'price_to', 'price_back'],
                            $insertTariffHasFix);
                    }

                    if ($option['allow_day_night'] == 1 && !empty($option['start_day']) && !empty($option['end_day'])) {

                        if ($currentNightTypeId === null) {
                            $this->insert(self::TABLE_NAME_OPTION_TYPE, [
                                'tariff_id' => $tariff['tariff_id'],
                                'type'      => 'EXCEPTIONS',
                                'sort'      => 300,
                            ]);

                            $currentNightTypeId = $this->db->getLastInsertID();

                            $this->insert(self::TABLE_NAME_OPTION_ACTIVE_DATE_NEW, [
                                'type_id'     => $currentNightTypeId,
                                'active_date' => "|{$option['end_day']}-{$option['start_day']}",
                            ]);

                            $insertAdditionalOptions = array_map(function ($item) use ($currentNightTypeId) {
                                return [$currentNightTypeId, $item['additional_option_id'], $item['price']];
                            }, $additionalOptions);

                            if (!empty($insertAdditionalOptions)) {
                                $this->batchInsert(self::TABLE_NAME_OPTION_TYPE_HAS_ADDITION_NEW,
                                    ['type_id', 'additional_option_id', 'price'],
                                    $insertAdditionalOptions);
                            }
                        }

                        $this->insert(self::TABLE_NAME_OPTION_NEW, [
                            'type_id'               => $currentNightTypeId,
                            'accrual'               => $option['accrual'],
                            'area'                  => $option['area'],
                            'planting_price'        => $option['planting_price_night'],
                            'planting_include'      => $option['planting_include_night'],
                            'next_km_price'         => $option['next_km_price_night'],
                            'min_price'             => $option['min_price_night'],
                            'second_min_price'      => $option['second_min_price_night'],
                            'supply_price'          => $option['supply_price_night'],
                            'wait_time'             => $option['wait_time_night'],
                            'wait_driving_time'     => $option['wait_driving_time_night'],
                            'wait_price'            => $option['wait_price_night'],
                            'speed_downtime'        => $option['speed_downtime_night'],
                            'rounding'              => $option['rounding_night'],
                            'rounding_type'         => $option['rounding_type_night'],
                            'enabled_parking_ratio' => $option['enabled_parking_ratio'],
                            'calculation_fix'       => $option['calculation_fix'],
                            'next_cost_unit'        => $option['next_cost_unit_night'],
                            'next_km_price_time'    => $option['next_km_price_night_time'],
                            'planting_include_time' => $option['planting_include_night_time'],
                        ]);


                        $insertTariffHasFix = array_map(function ($item) use ($currentNightTypeId) {
                            return [$currentNightTypeId, $item['fix_id'], $item['price_to'], $item['price_back']];
                        }, $tariffHasFix);

                        if (!empty($insertTariffHasFix)) {
                            $this->batchInsert(self::TABLE_NAME_FIX_HAS_OPTION_NEW,
                                ['type_id', 'fix_id', 'price_to', 'price_back'],
                                $insertTariffHasFix);
                        }
                    }
                }


                if ($option['tariff_type'] === 'HOLIDAYS') {

                    $activeDates       = $this->getOptionActiveDateByOption($tariff['tariff_id'],
                        $option['tariff_type']);
                    $additionalOptions = $this->getAdditionalOption($tariff['tariff_id'], $option['tariff_type']);
                    $tariffHasFix      = $this->getTariffHasFix($option['option_id']);

                    if ($option['allow_day_night'] == 1 && !empty($option['start_day']) && !empty($option['end_day'])) {
                        if ($holidaysNightTypeId === null) {
                            $this->insert(self::TABLE_NAME_OPTION_TYPE, [
                                'tariff_id' => $tariff['tariff_id'],
                                'type'      => 'EXCEPTIONS',
                                'sort'      => 250,
                            ]);

                            $holidaysNightTypeId = $this->db->getLastInsertID();

                            $insert = array_map(function ($item) use ($holidaysNightTypeId) {
                                return [$holidaysNightTypeId, $item['active_date']];
                            }, $activeDates);

                            if (!empty($insert)) {
                                $this->batchInsert(self::TABLE_NAME_OPTION_ACTIVE_DATE_NEW, ['type_id', 'active_date'],
                                    $insert);
                            }
                        }

                        $this->insert(self::TABLE_NAME_OPTION_NEW, [
                            'type_id'               => $holidaysNightTypeId,
                            'accrual'               => $option['accrual'],
                            'area'                  => $option['area'],
                            'planting_price'        => $option['planting_price_night'],
                            'planting_include'      => $option['planting_include_night'],
                            'next_km_price'         => $option['next_km_price_night'],
                            'min_price'             => $option['min_price_night'],
                            'second_min_price'      => $option['second_min_price_night'],
                            'supply_price'          => $option['supply_price_night'],
                            'wait_time'             => $option['wait_time_night'],
                            'wait_driving_time'     => $option['wait_driving_time_night'],
                            'wait_price'            => $option['wait_price_night'],
                            'speed_downtime'        => $option['speed_downtime_night'],
                            'rounding'              => $option['rounding_night'],
                            'rounding_type'         => $option['rounding_type_night'],
                            'enabled_parking_ratio' => $option['enabled_parking_ratio'],
                            'calculation_fix'       => $option['calculation_fix'],
                            'next_cost_unit'        => $option['next_cost_unit_night'],
                            'next_km_price_time'    => $option['next_km_price_night_time'],
                            'planting_include_time' => $option['planting_include_night_time'],
                        ]);


                        $insertAdditionalOptions = array_map(function ($item) use ($holidaysNightTypeId) {
                            return [$holidaysNightTypeId, $item['additional_option_id'], $item['price']];
                        }, $additionalOptions);

                        if (!empty($insertAdditionalOptions)) {
                            $this->batchInsert(self::TABLE_NAME_OPTION_TYPE_HAS_ADDITION_NEW,
                                ['type_id', 'additional_option_id', 'price'],
                                $insertAdditionalOptions);
                        }

                    }

                    if ($holidaysDayTypeId === null) {
                        $this->insert(self::TABLE_NAME_OPTION_TYPE, [
                            'tariff_id' => $tariff['tariff_id'],
                            'type'      => 'EXCEPTIONS',
                            'sort'      => 200,
                        ]);

                        $holidaysDayTypeId = $this->db->getLastInsertID();

                        $insert = array_map(function ($item) use ($holidaysDayTypeId, $option) {
                            if ($option['allow_day_night'] == 1 && !empty($option['start_day']) && !empty($option['end_day'])) {

                                $startDay = $option['start_day'];
                                $endDay   = $option['end_day'];

                                $date = explode('|', $item['active_date'], 2);
                                if (count($date) === 2) {
                                    $interval      = explode('-', $date[1], 2);
                                    $startInterval = $interval[0];
                                    $endInterval   = $interval[1];
                                    $startDay      = max($startInterval, $option['start_day']);
                                    $endDay        = min($endInterval, $option['end_day']);
                                }

                                return [$holidaysDayTypeId, "{$date[0]}|{$startDay}-{$endDay}"];
                            }

                            return [$holidaysDayTypeId, $item['active_date']];
                        }, $activeDates);

                        if (!empty($insert)) {
                            $this->batchInsert(self::TABLE_NAME_OPTION_ACTIVE_DATE_NEW, ['type_id', 'active_date'],
                                $insert);
                        }

                        $insertAdditionalOptions = array_map(function ($item) use ($holidaysDayTypeId) {
                            return [$holidaysDayTypeId, $item['additional_option_id'], $item['price']];
                        }, $additionalOptions);

                        if (!empty($insertAdditionalOptions)) {
                            $this->batchInsert(self::TABLE_NAME_OPTION_TYPE_HAS_ADDITION_NEW,
                                ['type_id', 'additional_option_id', 'price'],
                                $insertAdditionalOptions);
                        }

                    }

                    $this->insert(self::TABLE_NAME_OPTION_NEW, [
                        'type_id'               => $holidaysDayTypeId,
                        'accrual'               => $option['accrual'],
                        'area'                  => $option['area'],
                        'planting_price'        => $option['planting_price_day'],
                        'planting_include'      => $option['planting_include_day'],
                        'next_km_price'         => $option['next_km_price_day'],
                        'min_price'             => $option['min_price_day'],
                        'second_min_price'      => $option['second_min_price_day'],
                        'supply_price'          => $option['supply_price_day'],
                        'wait_time'             => $option['wait_time_day'],
                        'wait_driving_time'     => $option['wait_driving_time_day'],
                        'wait_price'            => $option['wait_price_day'],
                        'speed_downtime'        => $option['speed_downtime_day'],
                        'rounding'              => $option['rounding_day'],
                        'rounding_type'         => $option['rounding_type_day'],
                        'enabled_parking_ratio' => $option['enabled_parking_ratio'],
                        'calculation_fix'       => $option['calculation_fix'],
                        'next_cost_unit'        => $option['next_cost_unit_day'],
                        'next_km_price_time'    => $option['next_km_price_day_time'],
                        'planting_include_time' => $option['planting_include_day_time'],
                    ]);


                    $insertTariffHasFix = array_map(function ($item) use ($holidaysDayTypeId) {
                        return [$holidaysDayTypeId, $item['fix_id'], $item['price_to'], $item['price_back']];
                    }, $tariffHasFix);

                    if (!empty($insertTariffHasFix)) {
                        $this->batchInsert(self::TABLE_NAME_FIX_HAS_OPTION_NEW,
                            ['type_id', 'fix_id', 'price_to', 'price_back'],
                            $insertTariffHasFix);
                    }
                }


                if ($option['tariff_type'] === 'EXCEPTIONS') {

                    $activeDates       = $this->getOptionActiveDateByOption($tariff['tariff_id'],
                        $option['tariff_type']);
                    $additionalOptions = $this->getAdditionalOption($tariff['tariff_id'], $option['tariff_type']);
                    $tariffHasFix      = $this->getTariffHasFix($option['option_id']);

                    if ($option['allow_day_night'] == 1 && !empty($option['start_day']) && !empty($option['end_day'])) {
                        if ($exceptionsNightTypeId === null) {
                            $this->insert(self::TABLE_NAME_OPTION_TYPE, [
                                'tariff_id' => $tariff['tariff_id'],
                                'type'      => 'EXCEPTIONS',
                                'sort'      => 150,
                            ]);

                            $exceptionsNightTypeId = $this->db->getLastInsertID();

                            $insertActiveDates = array_map(function ($item) use ($exceptionsNightTypeId) {
                                return [$exceptionsNightTypeId, $item['active_date']];
                            }, $activeDates);

                            if (!empty($insertActiveDates)) {
                                $this->batchInsert(self::TABLE_NAME_OPTION_ACTIVE_DATE_NEW, ['type_id', 'active_date'],
                                    $insertActiveDates);
                            }

                            $insertAdditionalOptions = array_map(function ($item) use ($exceptionsNightTypeId) {
                                return [$exceptionsNightTypeId, $item['additional_option_id'], $item['price']];
                            }, $additionalOptions);

                            if (!empty($insertAdditionalOptions)) {
                                $this->batchInsert(self::TABLE_NAME_OPTION_TYPE_HAS_ADDITION_NEW,
                                    ['type_id', 'additional_option_id', 'price'],
                                    $insertAdditionalOptions);
                            }
                        }

                        $this->insert(self::TABLE_NAME_OPTION_NEW, [
                            'type_id'               => $exceptionsNightTypeId,
                            'accrual'               => $option['accrual'],
                            'area'                  => $option['area'],
                            'planting_price'        => $option['planting_price_night'],
                            'planting_include'      => $option['planting_include_night'],
                            'next_km_price'         => $option['next_km_price_night'],
                            'min_price'             => $option['min_price_night'],
                            'second_min_price'      => $option['second_min_price_night'],
                            'supply_price'          => $option['supply_price_night'],
                            'wait_time'             => $option['wait_time_night'],
                            'wait_driving_time'     => $option['wait_driving_time_night'],
                            'wait_price'            => $option['wait_price_night'],
                            'speed_downtime'        => $option['speed_downtime_night'],
                            'rounding'              => $option['rounding_night'],
                            'rounding_type'         => $option['rounding_type_night'],
                            'enabled_parking_ratio' => $option['enabled_parking_ratio'],
                            'calculation_fix'       => $option['calculation_fix'],
                            'next_cost_unit'        => $option['next_cost_unit_night'],
                            'next_km_price_time'    => $option['next_km_price_night_time'],
                            'planting_include_time' => $option['planting_include_night_time'],
                        ]);


                        $insertTariffHasFix = array_map(function ($item) use ($exceptionsNightTypeId) {
                            return [$exceptionsNightTypeId, $item['fix_id'], $item['price_to'], $item['price_back']];
                        }, $tariffHasFix);

                        if (!empty($insertTariffHasFix)) {
                            $this->batchInsert(self::TABLE_NAME_FIX_HAS_OPTION_NEW,
                                ['type_id', 'fix_id', 'price_to', 'price_back'],
                                $insertTariffHasFix);
                        }
                    }

                    if ($exceptionsDayTypeId === null) {
                        $this->insert(self::TABLE_NAME_OPTION_TYPE, [
                            'tariff_id' => $tariff['tariff_id'],
                            'type'      => 'EXCEPTIONS',
                            'sort'      => 100,
                        ]);

                        $exceptionsDayTypeId = $this->db->getLastInsertID();

                        $insert = array_map(function ($item) use ($exceptionsDayTypeId, $option) {
                            if ($option['allow_day_night'] == 1 && !empty($option['start_day']) && !empty($option['end_day'])) {

                                $startDay = $option['start_day'];
                                $endDay   = $option['end_day'];

                                $date = explode('|', $item['active_date'], 2);
                                if (count($date) === 2) {
                                    $interval      = explode('-', $date[1], 2);
                                    $startInterval = $interval[0];
                                    $endInterval   = $interval[1];
                                    $startDay      = max($startInterval, $option['start_day']);
                                    $endDay        = min($endInterval, $option['end_day']);
                                }

                                return [$exceptionsDayTypeId, "{$date[0]}|{$startDay}-{$endDay}"];
                            }

                            return [$exceptionsDayTypeId, $item['active_date']];
                        }, $activeDates);

                        if (!empty($insert)) {
                            $this->batchInsert(self::TABLE_NAME_OPTION_ACTIVE_DATE_NEW, ['type_id', 'active_date'],
                                $insert);
                        }

                        $insertAdditionalOptions = array_map(function ($item) use ($exceptionsDayTypeId) {
                            return [$exceptionsDayTypeId, $item['additional_option_id'], $item['price']];
                        }, $additionalOptions);

                        if (!empty($insertAdditionalOptions)) {
                            $this->batchInsert(self::TABLE_NAME_OPTION_TYPE_HAS_ADDITION_NEW,
                                ['type_id', 'additional_option_id', 'price'],
                                $insertAdditionalOptions);
                        }
                    }

                    $this->insert(self::TABLE_NAME_OPTION_NEW, [
                        'type_id'               => $exceptionsDayTypeId,
                        'accrual'               => $option['accrual'],
                        'area'                  => $option['area'],
                        'planting_price'        => $option['planting_price_day'],
                        'planting_include'      => $option['planting_include_day'],
                        'next_km_price'         => $option['next_km_price_day'],
                        'min_price'             => $option['min_price_day'],
                        'second_min_price'      => $option['second_min_price_day'],
                        'supply_price'          => $option['supply_price_day'],
                        'wait_time'             => $option['wait_time_day'],
                        'wait_driving_time'     => $option['wait_driving_time_day'],
                        'wait_price'            => $option['wait_price_day'],
                        'speed_downtime'        => $option['speed_downtime_day'],
                        'rounding'              => $option['rounding_day'],
                        'rounding_type'         => $option['rounding_type_day'],
                        'enabled_parking_ratio' => $option['enabled_parking_ratio'],
                        'calculation_fix'       => $option['calculation_fix'],
                        'next_cost_unit'        => $option['next_cost_unit_day'],
                        'next_km_price_time'    => $option['next_km_price_day_time'],
                        'planting_include_time' => $option['planting_include_day_time'],
                    ]);

                    $insertTariffHasFix = array_map(function ($item) use ($exceptionsDayTypeId) {
                        return [$exceptionsDayTypeId, $item['fix_id'], $item['price_to'], $item['price_back']];
                    }, $tariffHasFix);

                    if (!empty($insertTariffHasFix)) {
                        $this->batchInsert(self::TABLE_NAME_FIX_HAS_OPTION_NEW,
                            ['type_id', 'fix_id', 'price_to', 'price_back'],
                            $insertTariffHasFix);
                    }
                }


            }

        }
    }


    protected function getTariffs()
    {
        return (array)(new \yii\db\Query())->from(self::TABLE_NAME_TARIFF)
            //            ->limit(1)
            //            ->offset(1)
            ->all();
    }

    protected function getOptionsByTariff($tariffId)
    {
        return (array)(new \yii\db\Query())->from(self::TABLE_NAME_OPTION)->where(['tariff_id' => $tariffId])->all();
    }

    protected function getOptionActiveDateByOption($tariffId, $type)
    {
        return (array)(new \yii\db\Query())->from(self::TABLE_NAME_OPTION_ACTIVE_DATE)
            ->where([
                'tariff_id'   => $tariffId,
                'tariff_type' => $type,
            ])
            ->all();
    }

    protected function getAdditionalOption($tariffId, $type)
    {
        return (new \yii\db\Query())->from(self::TABLE_NAME_OPTION_TYPE_HAS_ADDITION)->where([
            'tariff_id'   => $tariffId,
            'tariff_type' => $type,
        ])->all();
    }


    protected function getTariffHasFix($optionId)
    {
        return (new \yii\db\Query())->from(self::TABLE_NAME_FIX_HAS_OPTION)
            ->where(['option_id' => $optionId])
            ->all();
    }
}
