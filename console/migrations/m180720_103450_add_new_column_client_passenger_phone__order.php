<?php

use yii\db\Migration;

class m180720_103450_add_new_column_client_passenger_phone__order extends Migration
{

    const TABLE_NAME = 'tbl_order';
    const COLUMN_NAME = 'client_passenger_phone';

    public function up()
    {
        /**
         * @var $changer \console\components\mysqlOnlineSchemaChange\MysqlOnlineSchemaChange
         */
        $changer = \Yii::$app->mysqlOnlineSchemaChange;
        try{
            $changer->run(self::TABLE_NAME, sprintf('ADD COLUMN %s VARCHAR(255)', self::COLUMN_NAME), [
                'alterForeignKeysMethod' => $changer::ALTER_FOREIGN_KEY_DROP_SWAP,
                'execute'                => true,
                'debug'                  => false
            ]);
            return true;
        } catch (RuntimeException $ex){
            echo $ex->getMessage() . PHP_EOL;
            return false;
        }
    }

    public function down()
    {
        /**
         * @var $changer \console\components\mysqlOnlineSchemaChange\MysqlOnlineSchemaChange
         */
        $changer = \Yii::$app->mysqlOnlineSchemaChange;
        try{
            $changer->run(self::TABLE_NAME, sprintf('DROP COLUMN %s', self::COLUMN_NAME), [
                'alterForeignKeysMethod' => $changer::ALTER_FOREIGN_KEY_DROP_SWAP,
                'execute'                => true,
                'debug'                  => false
            ]);
            return true;
        } catch (RuntimeException $ex){
            echo $ex->getMessage() . PHP_EOL;
            return false;
        }
    }
}
