<?php

use yii\db\Schema;
use yii\db\Migration;

class m160208_123554_db_messsage_source extends Migration
{
    const TBL_SOURCE = '{{%source_message}}';
    const TBL_TRANSLATE = '{{%message}}';

    public function up()
    {
        $this->createTable(self::TBL_SOURCE, [
            'id'       => $this->primaryKey(),
            'category' => $this->string(32)->notNull(),
            'message'  => $this->text()->notNull(),
        ]);

        $this->createTable(self::TBL_TRANSLATE, [
            'id'          => $this->integer(11)->notNull(),
            'language'    => $this->string(16)->notNull(),
            'translation' => $this->text()->notNull(),
            'PRIMARY KEY (id, language)'
        ]);

        $this->addForeignKey('fk_message_source_message', self::TBL_TRANSLATE, 'id', self::TBL_SOURCE, 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_message_source_message', self::TBL_TRANSLATE);
        $this->dropTable(self::TBL_TRANSLATE);
        $this->dropTable(self::TBL_SOURCE);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
