<?php

use yii\db\Migration;

class m160324_063627_fix_for_lenght_nextKmPrice_in_order_detail_cost extends Migration
{
    const TABLE_NAME = '{{%order_detail_cost}}';
    
    public function up()
    {
        $this->alterColumn(self::TABLE_NAME,'city_next_km_price',$this->text());
        $this->alterColumn(self::TABLE_NAME,'out_next_km_price',$this->text());
    }

    public function down()
    {
        $this->alterColumn(self::TABLE_NAME,'city_next_km_price',$this->string(150));
        $this->alterColumn(self::TABLE_NAME,'out_next_km_price',$this->string(150));
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
