<?php

use yii\db\Migration;

class m170208_085859_add_column_city_autocompletion_in_mobile_app extends Migration
{

    const TABLE_NAME = '{{%mobile_app}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'city_autocompletion', $this->integer(1)->defaultValue(1)->notNull());
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'city_autocompletion');
    }
}
