<?php

use yii\db\Migration;

class m171027_123806_create_promo extends Migration
{

    const TABLE_NAME = '{{%promo}}';

    const COLUMN_TENANT_ID = 'tenant_id';
    const COLUMN_POSITION_ID = 'position_id';
    const COLUMN_TYPE_ID = 'type_id';
    const COLUMN_PERIOD_TYPE_ID = 'period_type_id';
    const COLUMN_ACTION_COUNT_ID = 'action_count_id';
    const COLUMN_ACTION_CLIENT_ID = 'action_clients_id';
    const COLUMN_ACTIVATION_TYPE_ID = 'activation_type_id';

    const FK_PROMO__TENANT_ID = 'fk_promo__tenant_id';
    const FK_PROMO__TYPE_ID = 'fk_promo__type_id';
    const FK_PROMO__POSITION_ID = 'fk_promo__position_id';
    const FK_PROMO__PERIOD_TYPE_ID = 'fk_promo__period_type_id';
    const FK_PROMO__ACTION_COUNT_ID = 'fk_promo__action_count_id';
    const FK_PROMO__ACTION_CLIENT_ID = 'fk_promo__action_clients_id';
    const FK_PROMO__ACTIVATION_TYPE_ID = 'fk_promo__activation_type_id';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'promo_id'                      => $this->primaryKey()->unsigned(),
            self::COLUMN_TENANT_ID          => $this->integer()->unsigned()->notNull(),
            self::COLUMN_TYPE_ID            => $this->integer()->notNull(),
            self::COLUMN_POSITION_ID        => $this->integer()->notNull(),
            'name'                          => $this->string(255)->notNull(),
            self::COLUMN_PERIOD_TYPE_ID     => $this->integer()->notNull(),
            'period_start'                  => $this->integer()->unsigned(),
            'period_end'                    => $this->integer()->unsigned(),
            self::COLUMN_ACTION_COUNT_ID    => $this->integer(),
            self::COLUMN_ACTION_CLIENT_ID   => $this->integer(),
            'action_no_other_codes'         => $this->boolean()->notNull(),
            self::COLUMN_ACTIVATION_TYPE_ID => $this->integer()->notNull(),
            'client_bonuses'                => $this->integer(),
            'client_discount'               => $this->integer(),
            'worker_commission'             => $this->integer(),
            'worker_bonuses'                => $this->integer(),
            'blocked'                       => $this->boolean()->notNull(),
            'created_at'                    => $this->boolean()->unsigned()->notNull(),
            'updated_at'                    => $this->boolean()->unsigned()->notNull(),
            'count_codes'                   => $this->integer(),
            'count_symbols_in_code'         => $this->integer(),
        ]);

        $this->addForeignKey(self::FK_PROMO__TENANT_ID, self::TABLE_NAME,
            self::COLUMN_TENANT_ID, '{{%tenant}}', self::COLUMN_TENANT_ID, 'CASCADE', 'CASCADE');

        $this->addForeignKey(self::FK_PROMO__TYPE_ID, self::TABLE_NAME,
            self::COLUMN_TYPE_ID, '{{%promo_type}}', self::COLUMN_TYPE_ID, 'CASCADE', 'CASCADE');

        $this->addForeignKey(self::FK_PROMO__POSITION_ID, self::TABLE_NAME,
            self::COLUMN_POSITION_ID, '{{%position}}', self::COLUMN_POSITION_ID, 'CASCADE', 'CASCADE');

        $this->addForeignKey(self::FK_PROMO__PERIOD_TYPE_ID, self::TABLE_NAME,
            self::COLUMN_PERIOD_TYPE_ID, '{{%promo_period_type}}', self::COLUMN_PERIOD_TYPE_ID, 'CASCADE', 'CASCADE');

        $this->addForeignKey(self::FK_PROMO__ACTION_COUNT_ID, self::TABLE_NAME,
            self::COLUMN_ACTION_COUNT_ID, '{{%promo_action_count}}', self::COLUMN_ACTION_COUNT_ID, 'CASCADE',
            'CASCADE');

        $this->addForeignKey(self::FK_PROMO__ACTION_CLIENT_ID, self::TABLE_NAME,
            self::COLUMN_ACTION_CLIENT_ID, '{{%promo_action_clients}}', self::COLUMN_ACTION_CLIENT_ID, 'CASCADE',
            'CASCADE');

        $this->addForeignKey(self::FK_PROMO__ACTIVATION_TYPE_ID, self::TABLE_NAME,
            self::COLUMN_ACTIVATION_TYPE_ID, '{{%promo_activation_type}}', self::COLUMN_ACTIVATION_TYPE_ID, 'CASCADE',
            'CASCADE');



        $this->createIndex(sprintf('idx_promo__%s', self::COLUMN_TENANT_ID ), self::TABLE_NAME, self::COLUMN_TENANT_ID);


    }



    public function safeDown()
    {
        $this->dropForeignKey(self::FK_PROMO__ACTIVATION_TYPE_ID, self::TABLE_NAME);
        $this->dropForeignKey(self::FK_PROMO__ACTION_CLIENT_ID, self::TABLE_NAME);
        $this->dropForeignKey(self::FK_PROMO__ACTION_COUNT_ID, self::TABLE_NAME);
        $this->dropForeignKey(self::FK_PROMO__PERIOD_TYPE_ID, self::TABLE_NAME);
        $this->dropForeignKey(self::FK_PROMO__POSITION_ID, self::TABLE_NAME);
        $this->dropForeignKey(self::FK_PROMO__TYPE_ID, self::TABLE_NAME);
        $this->dropForeignKey(self::FK_PROMO__TENANT_ID, self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
    }

}
