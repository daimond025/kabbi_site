<?php

use yii\db\Schema;
use yii\db\Migration;

class m160122_083125_tbl_default_setting extends Migration
{
    public function up()
    {
        $default_currency_id = (new yii\db\Query)
                ->select('currency_id')
                ->from('{{%currency}}')
                ->where(['code' => 'RUB'])
                ->scalar();

        $this->update('{{%default_settings}}', ['value' => $default_currency_id], ['name' => 'CURRENCY']);
    }

    public function down()
    {
        echo "m160122_083125_tbl_default_setting cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
