<?php

use yii\db\Migration;

class m160718_111104_tbl_sms_service__add_sms_provider_ibatele_smpp extends Migration
{
    const TABLE_SMS_SERVER = '{{%sms_server}}';
    const SERVER_ID = 10;

    public function up()
    {
        $this->insert(self::TABLE_SMS_SERVER, [
            'server_id' => self::SERVER_ID,
            'name'      => 'Ibatele (smpp)',
            'host'      => 'http://ibatele.com',
            'active'    => 1,
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_SMS_SERVER, [
            'server_id' => self::SERVER_ID,
        ]);
    }

}
