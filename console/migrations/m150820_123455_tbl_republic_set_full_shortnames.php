<?php

use yii\db\Schema;
use yii\db\Migration;

class m150820_123455_tbl_republic_set_full_shortnames extends Migration
{

    public function up()
    {
        $this->execute("update tbl_republic 
set shortname = 'область'
where shortname = 'обл'");
        $this->execute("update tbl_republic 
set shortname = 'республика'
where shortname = 'респ'");
    }

    public function down()
    {
        echo "m150820_123455_tbl_republic_set_full_shortnames cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
