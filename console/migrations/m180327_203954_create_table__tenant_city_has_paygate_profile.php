<?php

use yii\db\Migration;

class m180327_203954_create_table__tenant_city_has_paygate_profile extends Migration
{
// необходимо будет перенести текущие профили из api_paygate
//
//SELECT cast(paymentName AS UNSIGNED INTEGER)
//FROM yandex_payment_gate_profile
//UNION
//SELECT cast(paymentName AS UNSIGNED INTEGER)
//FROM sberbank_payment_gate_profile
//UNION
//SELECT cast(paymentName AS UNSIGNED INTEGER)
//FROM stripe_payment_gate_profile
//UNION
//SELECT cast(paymentName AS UNSIGNED INTEGER)
//FROM paycom_profile
//UNION
//SELECT cast(paymentName AS UNSIGNED INTEGER)
//FROM payment_gate_profile;
// + kkb_profile

    const TABLE_TENANT_CITY_HAS_PAYGATE_PROFILE = '{{%tenant_city_has_paygate_profile}}';
    const TABLE_TENANT = '{{%tenant}}';
    const TABLE_CITY = '{{%city}}';
    const TABLE_PAYGATE_PROFILE = '{{%paygate_profile}}';
    const UNIQUE_INDEX = 'ui_tenant_has_paygate_profile';
    const FOREIGN_KEY_TENANT_ID = 'fk_tenant_has_paygate_profile_tenant_id';
    const FOREIGN_KEY_CITY_ID = 'fk_tenant_has_paygate_profile_city_id';
    const FOREIGN_KEY_PROFILE_ID = 'fk_tenant_has_paygate_profile_profile_id';

    public function safeUp()
    {
        $this->createTable(self::TABLE_TENANT_CITY_HAS_PAYGATE_PROFILE, [
            'id'         => $this->primaryKey(),
            'tenant_id'  => $this->integer(10)->unsigned()->notNull(),
            'city_id'    => $this->integer(11)->unsigned()->notNull(),
            'profile_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createIndex(self::UNIQUE_INDEX, self::TABLE_TENANT_CITY_HAS_PAYGATE_PROFILE, ['tenant_id', 'city_id'],
            true);
        $this->addForeignKey(self::FOREIGN_KEY_TENANT_ID, self::TABLE_TENANT_CITY_HAS_PAYGATE_PROFILE, 'tenant_id',
            self::TABLE_TENANT, 'tenant_id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey(self::FOREIGN_KEY_CITY_ID, self::TABLE_TENANT_CITY_HAS_PAYGATE_PROFILE, 'city_id',
            self::TABLE_CITY, 'city_id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey(self::FOREIGN_KEY_PROFILE_ID, self::TABLE_TENANT_CITY_HAS_PAYGATE_PROFILE, 'profile_id',
            self::TABLE_PAYGATE_PROFILE, 'id', 'CASCADE', 'CASCADE');

    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_TENANT_CITY_HAS_PAYGATE_PROFILE);
    }
}

