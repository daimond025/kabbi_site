<?php

use yii\db\Migration;

class m171007_211309_create_table__car_mileage extends Migration
{
    const TABLE_CAR_MILEAGE = '{{%car_mileage}}';
    const TABLE_WORKER_SHIFT = '{{%worker_shift}}';
    const FOREIGN_KEY = 'fk_car_mileage__shift_id';

    public function safeUp()
    {
        $this->createTable(self::TABLE_CAR_MILEAGE, [
            'id'          => $this->primaryKey(),
            'car_id'      => $this->integer(10)->unsigned()->notNull(),
            'shift_id'    => $this->integer(10)->unsigned()->notNull()->unique(),
            'begin_value' => $this->double()->notNull(),
            'end_value'   => $this->double(),
            'created_at'  => $this->integer(),
            'updated_at'  => $this->integer(),
        ]);

        $this->addForeignKey(self::FOREIGN_KEY, self::TABLE_CAR_MILEAGE, 'shift_id', self::TABLE_WORKER_SHIFT, 'id',
            'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_CAR_MILEAGE);
    }

}
