<?php

use yii\db\Migration;

class m171027_123750_create_promo_activation_type extends Migration
{

    const TABLE_NAME = '{{%promo_activation_type}}';

    const COLUMN_ACTIVATION_TYPE = 'activation_type_id';
    const COLUMN_NAME = 'name';

    const DEFAULT_VALUE = [
        ['Bonuses to account'],
        ['Discount when ordering'],
    ];

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            self::COLUMN_ACTIVATION_TYPE => $this->primaryKey(),
            self::COLUMN_NAME            => $this->string(255)->notNull(),
        ]);

        $this->batchInsert(
            self::TABLE_NAME,
            [self::COLUMN_NAME],
            self::DEFAULT_VALUE
        );
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }


}
