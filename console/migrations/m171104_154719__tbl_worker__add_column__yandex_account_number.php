<?php

use yii\db\Migration;

class m171104_154719__tbl_worker__add_column__yandex_account_number extends Migration
{
    const TABLE_WORKER = '{{%worker}}';
    const COLUMN_YANDEX_ACCOUNT_NUMBER = 'yandex_account_number';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_WORKER, self::COLUMN_YANDEX_ACCOUNT_NUMBER, $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_WORKER, self::COLUMN_YANDEX_ACCOUNT_NUMBER);
    }

}
