<?php

use yii\db\Migration;

class m171027_123824_create_promo_has_city extends Migration
{

    const TABLE_NAME = '{{%promo_has_city}}';

    const COLUMN_PROMO_ID = 'promo_id';
    const COLUMN_CITY_ID = 'city_id';

    const FK_PROMO_HAS_CITY__PROMO_ID = 'fk_promo_has_city__promo_id';
    const FK_PROMO_HAS_CITY__CITY_ID = 'fk_promo_has_city__city_id';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            self::COLUMN_PROMO_ID => $this->integer()->unsigned()->notNull(),
            self::COLUMN_CITY_ID  => $this->integer()->unsigned()->notNull(),
        ]);

        $this->addPrimaryKey(
            'pk_promo_has_city', self::TABLE_NAME,
            [self::COLUMN_PROMO_ID, self::COLUMN_CITY_ID]
        );

        $this->addForeignKey(self::FK_PROMO_HAS_CITY__PROMO_ID, self::TABLE_NAME,
            self::COLUMN_PROMO_ID, '{{%promo}}', self::COLUMN_PROMO_ID, 'CASCADE','CASCADE');

        $this->addForeignKey(self::FK_PROMO_HAS_CITY__CITY_ID, self::TABLE_NAME,
            self::COLUMN_CITY_ID, '{{%city}}', self::COLUMN_CITY_ID, 'CASCADE','CASCADE');
    }

    public function safeDown()
    {

        $this->dropForeignKey(self::FK_PROMO_HAS_CITY__CITY_ID, self::TABLE_NAME);
        $this->dropForeignKey(self::FK_PROMO_HAS_CITY__PROMO_ID, self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
    }
}
