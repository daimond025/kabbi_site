<?php

use yii\db\Migration;

class m170904_053623_add_templates_to_client_sms extends Migration
{
    const TABLE_PUSH = '{{%default_sms_template}}';

    const TABLE_PUSH2 = '{{%default_sms_template_courier}}';

    public function up()
    {

        $this->addColumn(self::TABLE_PUSH, 'params', 'text');

        $this->addColumn(self::TABLE_PUSH2, 'params', 'text');

        $texts = $this->getTextPushes();

        foreach ($texts as $text) {

            preg_match_all('/#\w+#/', $text['description'], $tpls);

            $tpls = implode('; ', $tpls[0]);

            $this->update(self::TABLE_PUSH, ['params' => $tpls], ['template_id' => $text['template_id']]);

        }

        $texts2 = $this->getTextPushesCourier();

        foreach ($texts2 as $text2) {

            preg_match_all('/#\w+#/', $text2['description'], $tpls);

            $tpls = implode('; ', $tpls[0]);

            $this->update(self::TABLE_PUSH2, ['params' => $tpls], ['template_id' => $text2['template_id']]);

        }
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_PUSH, 'params');

        $this->dropColumn(self::TABLE_PUSH2, 'params');
    }

    public function getTextPushes()
    {

        return $this->getDb()->createCommand('SELECT * FROM ' . self::TABLE_PUSH)->queryAll();

    }

    public function getTextPushesCourier()
    {

        return $this->getDb()->createCommand('SELECT * FROM ' . self::TABLE_PUSH2)->queryAll();

    }
}
