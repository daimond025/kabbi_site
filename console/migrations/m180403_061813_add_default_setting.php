<?php

use yii\db\Migration;

class m180403_061813_add_default_setting extends Migration
{
    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const TABLE_TENANT_SETTING = '{{%tenant_setting}}';
    const SETTING_NAME = 'AUTOCOMPLETE_RADIUS';
    const SETTING_VALUE = '50';
    const SETTING_TYPE = 'general';

    public function safeUp()
    {
        $model = new \common\modules\tenant\models\DefaultSettings([
            'name'  => self::SETTING_NAME,
            'value' => self::SETTING_VALUE,
            'type'  => self::SETTING_TYPE,
        ]);

        $model->save();

        return true;
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_DEFAULT_SETTINGS, ['name' => self::SETTING_NAME, 'type' => self::SETTING_TYPE]);

        $this->delete(self::TABLE_TENANT_SETTING, ['name' => self::SETTING_NAME, 'type' => self::SETTING_TYPE]);

        return true;
    }

}
