<?php

use yii\db\Migration;

class m160901_074042_tbl_worker_option_tariff__remove_old_commission_columns extends Migration
{
    const TABLE_TARIFF_COMMISSION = '{{%tariff_commission}}';
    const TABLE_WORKER_OPTION_TARIFF = '{{%worker_option_tariff}}';

    const COLUMN_COMMISSION = 'commission';
    const COLUMN_COMMISSION_TYPE = 'commission_type';

    public function up()
    {
        $this->dropColumn(self::TABLE_WORKER_OPTION_TARIFF, self::COLUMN_COMMISSION);
        $this->dropColumn(self::TABLE_WORKER_OPTION_TARIFF, self::COLUMN_COMMISSION_TYPE);
    }

    public function down()
    {
        $this->addColumn(self::TABLE_WORKER_OPTION_TARIFF, self::COLUMN_COMMISSION,
            "DECIMAL(10,0) NULL DEFAULT '0'");
        $this->addColumn(self::TABLE_WORKER_OPTION_TARIFF, self::COLUMN_COMMISSION_TYPE,
            "ENUM('PERCENT','MONEY') NULL DEFAULT 'PERCENT'");

        $rows = (new \yii\db\Query())
            ->from(self::TABLE_TARIFF_COMMISSION)
            ->groupBy('option_id')
            ->all();

        foreach ($rows as $row) {
            $this->update(self::TABLE_WORKER_OPTION_TARIFF, [
                self::COLUMN_COMMISSION_TYPE => $row['type'],
                self::COLUMN_COMMISSION      => $row['value'],
            ], [
                'option_id' => $row['option_id'],
            ]);
        }
    }

}
