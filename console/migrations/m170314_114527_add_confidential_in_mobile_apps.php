<?php

use yii\db\Migration;

class m170314_114527_add_confidential_in_mobile_apps extends Migration
{
    const TABLE_NAME = '{{%mobile_app}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'confidential', $this->text()->after('page_info'));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'confidential');
    }
}
