<?php

use yii\db\Schema;
use yii\db\Migration;

class m151216_103230_tbl_order_detail_cost__add_column_bonus extends Migration
{
    const TABLE_NAME = '{{%order_detail_cost}}';

    const WRITEOFF_KEY_NAME = 'fk_order_detail_cost__writeoff_bonus_id';
    const WRITEOFF_BONUS_COLUMN = 'writeoff_bonus_id';

    const REFILL_KEY_NAME = 'fk_order_detail_cost__refill_bonus_id';
    const REFILL_BONUS_COLUMN = 'refill_bonus_id';


    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'bonus', 'varchar(45)');

        $this->addColumn(self::TABLE_NAME, self::WRITEOFF_BONUS_COLUMN, Schema::TYPE_INTEGER);
        $this->addForeignKey(self::WRITEOFF_KEY_NAME, self::TABLE_NAME, self::WRITEOFF_BONUS_COLUMN,
                '{{%client_bonus}}', 'bonus_id', 'CASCADE', 'CASCADE');

        $this->addColumn(self::TABLE_NAME, self::REFILL_BONUS_COLUMN, Schema::TYPE_INTEGER);
        $this->addForeignKey(self::REFILL_KEY_NAME, self::TABLE_NAME, self::REFILL_BONUS_COLUMN,
                '{{%client_bonus}}', 'bonus_id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'bonus');

        $this->dropForeignKey(self::WRITEOFF_KEY_NAME, self::TABLE_NAME);
        $this->dropColumn(self::TABLE_NAME, self::WRITEOFF_BONUS_COLUMN);

        $this->dropForeignKey(self::REFILL_KEY_NAME, self::TABLE_NAME);
        $this->dropColumn(self::TABLE_NAME, self::REFILL_BONUS_COLUMN);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
