<?php

use yii\db\Schema;
use yii\db\Migration;

class m151217_052814_tbl_street extends Migration
{
    public function up()
    {
        $this->addColumn('{{%street}}', 'street_area_en', $this->string());
        $this->addColumn('{{%street}}', 'street_area_az', $this->string());
        $this->addColumn('{{%street}}', 'street_area_de', $this->string());
        $this->addColumn('{{%street}}', 'street_area_se', $this->string());
    }

    public function down()
    {
        $this->dropColumn('{{%street}}', 'street_area_en');
        $this->dropColumn('{{%street}}', 'street_area_az');
        $this->dropColumn('{{%street}}', 'street_area_se');
        $this->dropColumn('{{%street}}', 'street_area_de');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
