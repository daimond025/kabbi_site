<?php

use yii\db\Migration;

class m170204_064551_mobile_app_has_city_create_table extends Migration
{
    const TABLE_NAME = '{{%mobile_app_has_city}}';
    const TABLE_NAME_APP = '{{%mobile_app}}';
    const TABLE_NAME_TENANT_HAS_CITY = '{{%tenant_has_city}}';

    public function up()
    {
                $this->createTable(self::TABLE_NAME, [
                    'id'              => $this->primaryKey(),
                    'app_id' => $this->integer(11)->unsigned()->notNull(),
                    'city_id'         => $this->integer(11)->unsigned()->notNull(),
                ]);

                $this->addForeignKey('fk_mobile_app_has_city__app_id', self::TABLE_NAME, 'app_id',
                    '{{%mobile_app}}', 'app_id', 'CASCADE', 'CASCADE');
                $this->addForeignKey('fk_mobile_app_has_city__city_id', self::TABLE_NAME, 'city_id',
                    '{{%city}}', 'city_id', 'CASCADE', 'CASCADE');

        $this->dataFill();
    }

    public function down()
    {
                $this->dropForeignKey('fk_mobile_app_has_city__city_id', self::TABLE_NAME);
                $this->dropForeignKey('fk_mobile_app_has_city__app_id', self::TABLE_NAME);
                $this->dropTable(self::TABLE_NAME);
    }

    private function dataFill()
    {
        $data = (new \yii\db\Query())->select(['tenant_id', 'city_id'])
            ->from(self::TABLE_NAME_TENANT_HAS_CITY)
            ->where(['block' => 0])
            ->all();

        $apps = (new \yii\db\Query())->select(['app_id', 'tenant_id'])
            ->from(self::TABLE_NAME_APP)
            ->all();

        foreach ($apps as $app) {
            foreach ($data as $item) {
                if ($item['tenant_id'] === $app['tenant_id']) {
                    $this->insert(self::TABLE_NAME, [
                        'app_id'  => $app['app_id'],
                        'city_id' => $item['city_id'],
                    ]);
                }
            }
        }

    }
}
