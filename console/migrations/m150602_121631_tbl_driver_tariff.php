<?php

use yii\db\Schema;
use yii\db\Migration;

class m150602_121631_tbl_driver_tariff extends Migration
{
    public function up()
    {
        $this->addColumn('{{%driver_tariff}}', 'description', Schema::TYPE_TEXT);
    }

    public function down()
    {
        $this->dropColumn('{{%driver_tariff}}', 'description');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
