<?php

use yii\db\Schema;
use yii\db\Migration;

class m151116_055752_tbl_admin_user_has_admin_sms_server extends Migration {

    const TABLE_NAME = '{{%admin_user_has_admin_sms_server}}';
    public function up() {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(5)->notNull(),
            'server_id' => $this->integer(5)->notNull(),
            'create_date' => $this->dateTime()->notNull(),
        ]);
    }

    public function down() {
        if($this->dropTable(self::TABLE_NAME))
            echo 'admin_user_has_admin_sms_server successfully dropped';
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
