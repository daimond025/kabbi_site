<?php

use yii\db\Migration;

class m170529_134036_tbl_worker_option_tariff__add_column__sort extends Migration
{
    const TABLE_NAME = '{{%worker_option_tariff}}';
    const COLUMN_NAME = 'sort';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_NAME, $this->integer()->notNull()->defaultValue(100));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);
    }

}
