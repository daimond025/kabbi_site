<?php

use yii\db\Migration;
use common\modules\tenant\models\DefaultSettings;

class m180621_140146_add_general_setting__signal_new_order extends Migration
{
    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const TABLE_TENANT_SETTING = '{{%tenant_setting}}';

    const SETTING_NAME = 'SIGNAL_NEW_ORDER';
    const SETTING_VALUE = '0';
    const SETTING_TYPE = 'general';

    public function safeUp()
    {
        $model = new DefaultSettings([
            'name'  => self::SETTING_NAME,
            'value' => self::SETTING_VALUE,
            'type'  => self::SETTING_TYPE,
        ]);

        $model->save();

        return true;
    }

    public function safeDown()
    {
        $this->delete(
            self::TABLE_DEFAULT_SETTINGS,
            ['name' => self::SETTING_NAME, 'value' => self::SETTING_VALUE, 'type' => self::SETTING_TYPE]
        );

        $this->delete(
            self::TABLE_TENANT_SETTING,
            ['name' => self::SETTING_NAME, 'type' => self::SETTING_TYPE]
        );

        return true;
    }
}
