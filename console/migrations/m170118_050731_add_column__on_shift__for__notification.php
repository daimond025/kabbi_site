<?php

use yii\db\Migration;

class m170118_050731_add_column__on_shift__for__notification extends Migration
{
    const TABLE_NAME = '{{%notification}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'on_shift',
            $this->integer(1)->notNull()->defaultValue(0)->comment('0-все. 1-на смене. 2-не на смене'));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'on_shift');
    }
}
