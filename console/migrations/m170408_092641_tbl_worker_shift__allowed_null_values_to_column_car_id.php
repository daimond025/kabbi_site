<?php

use yii\db\Migration;

class m170408_092641_tbl_worker_shift__allowed_null_values_to_column_car_id extends Migration
{
    const TABLE_WORKER_SHIFT = '{{%worker_shift}}';
    const COLUMN_CAR_ID = 'car_id';

    public function up()
    {
        $this->alterColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_CAR_ID, $this->integer(10)->unsigned());
    }

    public function down()
    {
        $this->alterColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_CAR_ID, $this->integer(10)->unsigned()->notNull());
    }

}
