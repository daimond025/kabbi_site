<?php

use yii\db\Migration;

class m170916_165921_tbl_worker_active_aboniment__add_column__valid_to extends Migration
{
    const TABLE_NAME = '{{%worker_active_aboniment}}';
    const COLUMN_NAME = 'valid_to';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_NAME, $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);
    }

}
