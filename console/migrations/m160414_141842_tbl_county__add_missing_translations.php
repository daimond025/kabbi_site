<?php

use yii\db\Migration;

class m160414_141842_tbl_county__add_missing_translations extends Migration
{
    const TABLE_NAME = '{{%country}}';

    const ID_SERBIA = 7;
    const ID_UZBEKISTAN = 8;
    const ID_MOLDOVA = 9;

    const NAME_SERBIA = 'Serbia';
    const NAME_UZBEKISTAN = 'Uzbekistan';
    const NAME_MOLDOVA = 'Moldova';

    public function renameColumns($id, $name)
    {
        $this->update(self::TABLE_NAME, [
            'name_en' => $name,
            'name_az' => $name,
            'name_de' => $name,
            'name_sr' => $name,
            'name_ro' => $name,
        ], 'country_id = :id', ['id' => $id]);
    }

    public function up()
    {
        $this->renameColumns(self::ID_SERBIA, self::NAME_SERBIA);
        $this->renameColumns(self::ID_UZBEKISTAN, self::NAME_UZBEKISTAN);
        $this->renameColumns(self::ID_MOLDOVA, self::NAME_MOLDOVA);
    }

    public function down()
    {
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
