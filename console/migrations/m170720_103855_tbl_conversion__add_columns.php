<?php

use yii\db\Migration;

class m170720_103855_tbl_conversion__add_columns extends Migration
{
    const TABLE_CONVERSION = '{{%conversion}}';
    const COLUMN_ACTION = 'action';
    const COLUMN_EMAIL = 'email';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_CONVERSION, self::COLUMN_ACTION, $this->string());
        $this->addColumn(self::TABLE_CONVERSION, self::COLUMN_EMAIL, $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_CONVERSION, self::COLUMN_ACTION);
        $this->dropColumn(self::TABLE_CONVERSION, self::COLUMN_EMAIL);
    }

}
