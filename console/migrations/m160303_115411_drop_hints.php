<?php

use yii\db\Migration;

class m160303_115411_drop_hints extends Migration
{
    
    const HINTS = '{{%hints}}';
    
    const HINTS_AZ = '{{%hints_az}}';
    
    const HINTS_DE = '{{%hints_de}}';
    
    const HINTS_EN = '{{%hints_en}}';
    
    const HINTS_SE = '{{%hints_se}}';
    
    public function up()
    {
        $this->dropTable(self::HINTS);
        $this->dropTable(self::HINTS_AZ);
        $this->dropTable(self::HINTS_DE);
        $this->dropTable(self::HINTS_EN);
        $this->dropTable(self::HINTS_SE);
    }

    public function down()
    {
        echo "m160303_115411_drop_hints cannot be reverted.\n";

        return false;
    }
}
