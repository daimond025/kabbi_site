<?php

use yii\db\Schema;
use yii\db\Migration;

class m150610_122633_tbl_order_detail_cost extends Migration
{
    public function up()
    {
        $this->addColumn('{{%order_detail_cost}}', 'before_time_wait_cost', Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('{{%order_detail_cost}}', 'before_time_wait_cost');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
