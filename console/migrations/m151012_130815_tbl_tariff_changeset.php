<?php

use yii\db\Schema;
use yii\db\Migration;
use yii\db\Expression;

class m151012_130815_tbl_tariff_changeset extends Migration
{
    const TABLE_NAME = '{{%tenant_admin_tariff_changeset}}';
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'tenant_id' => $this->integer(10)->notNull(),
            'tariff_id_old' => $this->integer(5)->notNull(),
            'tariif_id_new' => $this->integer(5)->notNull(),
            'option_id_old' => $this->integer(8)->notNull(),
            'option_id_new' => $this->integer(8)->notNull(), 
            'create_date' => $this->dateTime(),
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
