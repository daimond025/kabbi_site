<?php

use yii\db\Migration;

class m160828_154817_create_table__client_bonus_gootax extends Migration
{
    const TABLE_CLIENT_BONUS_GOOTAX = '{{%client_bonus_gootax}}';
    const TABLE_CLIENT_BONUS = '{{%client_bonus}}';
    const COLUMN_BONUS_ID = 'bonus_id';
    const FOREIGN_KEY_BONUS_ID = 'fk_client_bonus_gootax__bonus_id';

    public function up()
    {
        $this->createTable(self::TABLE_CLIENT_BONUS_GOOTAX, [
            'id'               => $this->primaryKey(),
            'bonus_id'         => $this->integer()->notNull()->unique(),
            'actual_date'      => $this->text(),
            'bonus_type'       => "ENUM('BONUS', 'PERCENT') NOT NULL DEFAULT 'PERCENT'",
            'bonus'            => $this->decimal(10, 2)->notNull(),
            'min_cost'         => $this->decimal(10, 2)->notNull()->defaultValue(0),
            'payment_method'   => "ENUM('FULL', 'PARTIAL') NOT NULL DEFAULT 'FULL'",
            'max_payment_type' => "ENUM('BONUS', 'PERCENT') NOT NULL DEFAULT 'PERCENT'",
            'max_payment'      => $this->decimal(10, 2)->defaultValue(0),
            'bonus_app_type'   => "ENUM('BONUS', 'PERCENT') NOT NULL DEFAULT 'PERCENT'",
            'bonus_app'        => $this->decimal(10, 2)->notNull()->defaultValue(0),
            'created_at'       => $this->integer(),
            'updated_at'       => $this->integer(),
        ]);
        $this->addForeignKey(self::FOREIGN_KEY_BONUS_ID,
            self::TABLE_CLIENT_BONUS_GOOTAX, self::COLUMN_BONUS_ID,
            self::TABLE_CLIENT_BONUS, self::COLUMN_BONUS_ID, 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable(self::TABLE_CLIENT_BONUS_GOOTAX);
    }
}
