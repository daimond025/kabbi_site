<?php

use yii\db\Migration;

class m161005_092343_add_column_enabled_cabinet_in_taxi_tariff extends Migration
{
    const TABLE_NAME = '{{%taxi_tariff}}';
    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'enabled_cabinet', $this->integer(1)->defaultValue(1)->after('enabled_bordur'));
        $this->execute('UPDATE tbl_taxi_tariff SET `enabled_cabinet` = `enabled_app`');
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'enabled_cabinet');
    }
}
