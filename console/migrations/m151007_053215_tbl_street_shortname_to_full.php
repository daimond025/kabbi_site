<?php

use yii\db\Schema;
use yii\db\Migration;

class m151007_053215_tbl_street_shortname_to_full extends Migration
{
    const TABLE_NAME = '{{%street}}';
    
    public function up()
    {
        $this->execute("UPDATE" . self::TABLE_NAME ." SET street_shortname = REPLACE(street_shortname, 'ул', 'улица') where street_shortname='ул'");
    }

    public function down()
    {
        $this->execute("UPDATE" . self::TABLE_NAME ." SET street_shortname = REPLACE(street_shortname, 'улица', 'ул') where street_shortname='улица'");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
