<?php

use yii\db\Migration;
use yii\db\Query;

class m160828_160059_copy_client_bonus_params_to_gootax_table extends Migration
{
    const TABLE_CLIENT_BONUS_GOOTAX = '{{%client_bonus_gootax}}';
    const TABLE_CLIENT_BONUS = '{{%client_bonus}}';

    public function safeUp()
    {
        $rows = (new Query())
            ->from(self::TABLE_CLIENT_BONUS)
            ->all();

        $now = time();
        foreach ($rows as $row) {
            $this->insert(self::TABLE_CLIENT_BONUS_GOOTAX, [
                'bonus_id'         => $row['bonus_id'],
                'actual_date'      => $row['actual_date'],
                'bonus_type'       => $row['bonus_type'],
                'bonus'            => $row['bonus'],
                'min_cost'         => $row['min_cost'],
                'payment_method'   => $row['payment_method'],
                'max_payment_type' => $row['max_payment_type'],
                'max_payment'      => $row['max_payment'],
                'bonus_app_type'   => $row['bonus_app_type'],
                'bonus_app'        => $row['bonus_app'],
                'created_at'       => $now,
                'updated_at'       => $now,
            ]);
        }
    }

    public function safeDown()
    {
        $this->truncateTable(self::TABLE_CLIENT_BONUS_GOOTAX);
    }

}
