<?php

use yii\db\Schema;
use yii\db\Migration;

class m160126_063915_tbl_street_is_city_flag extends Migration
{

    const TABLE_NAME = '{{%city}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'search', $this->boolean()->defaultValue(0));
        $this->update(self::TABLE_NAME, ['search' => 1], ['shortname' => 'г']);
        $this->update(self::TABLE_NAME, ['search' => 1], ['name' => 'Динская']);
        $this->createIndex('search', self::TABLE_NAME, ['search', 'name']);
    }

    public function down()
    {
        $this->dropIndex('search', self::TABLE_NAME);
        $this->dropColumn('search');
    }

}
