<?php

use yii\db\Migration;

class m170201_102226_tbl_order_status__rename_new_pre_order extends Migration
{
    const TABLE_ORDER_STATUS = '{{%order_status}}';
    const STATUS_ID = 6;

    public function up()
    {
        $this->update(self::TABLE_ORDER_STATUS,
            ['name' => 'New pre-order'], ['status_id' => self::STATUS_ID]);
    }

    public function down()
    {
        $this->update(self::TABLE_ORDER_STATUS,
            ['name' => 'New order'], ['status_id' => self::STATUS_ID]);
    }

}
