<?php

use yii\db\Schema;
use yii\db\Migration;

class m151217_051937_tbl_street extends Migration
{
    public function up()
    {
        $this->addColumn('{{%street}}', 'street_area', $this->string());
    }

    public function down()
    {
        $this->dropColumn('{{%street}}', 'street_area');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
