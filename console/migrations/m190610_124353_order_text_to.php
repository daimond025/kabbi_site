<?php

use yii\db\Migration;
use common\modules\tenant\models\DefaultSettings;

class m190610_124353_order_text_to extends Migration
{
    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const TABLE_TENANT_SETTING = '{{%tenant_setting}}';

    const SETTING_NAME_ORDER_SOUND = 'WORKER_ORDER_SOUND';


    const SETTING_TYPE = 'drivers';

    public function safeUp()
    {
        $time_calculate_value = new DefaultSettings([
            'name'  => self::SETTING_NAME_ORDER_SOUND,
            'value' => '0',
            'type'  => self::SETTING_TYPE,
        ]);

        return $time_calculate_value->save();
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_DEFAULT_SETTINGS, [TariffOptionForm
            'name' => self::SETTING_NAME_ORDER_SOUND,
            'type' => 'drivers',
        ]);

        $this->delete(self::TABLE_TENANT_SETTING, ['name' => self::SETTING_NAME_ORDER_SOUND, 'type' => self::SETTING_TYPE]);
    }
}
