<?php

use yii\db\Migration;

class m160526_104252_tbl_auto_geo_service_type_add_fields extends Migration
{
    const TABLE_NAME = '{{%auto_geo_service_type}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'has_app_code', $this->integer(1)->defaultValue(0));
        $this->addColumn(self::TABLE_NAME, 'has_app_id', $this->integer(1)->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'has_app_code');
        $this->dropColumn(self::TABLE_NAME, 'has_app_id');
        return true;
    }
}