<?php

use yii\db\Schema;
use yii\db\Migration;

class m151021_204820_create_subscribe_tables extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%subscribe_offer}}', [
            'id' => $this->primaryKey(),
            'community_user_id' => $this->integer()->notNull(),
            'offer_info_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk_subscribe_offer__community_user_id',
                '{{%subscribe_offer}}', 'community_user_id',
                '{{%community_user}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_subscribe_offer__offer_info_id',
                '{{%subscribe_offer}}', 'offer_info_id',
                '{{%offer_info}}', 'offer_info_id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%subscribe_comment}}', [
            'id' => $this->primaryKey(),
            'community_user_id' => $this->integer()->notNull(),
            'post_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk_subscribe_comment__community_user_id',
                '{{%subscribe_comment}}', 'community_user_id',
                '{{%community_user}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_subscribe_comment__post_id',
                '{{%subscribe_comment}}', 'post_id',
                '{{%post}}', 'post_id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%subscribe_offer}}');
        $this->dropTable('{{%subscribe_comment}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
