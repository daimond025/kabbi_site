<?php

use yii\db\Migration;

class m170816_170815_add_new_worker_settings extends Migration
{
    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const TABLE_TENANT_SETTING = '{{%tenant_setting}}';

    const SETTING_NAME__ALLOW_WORKER_TO_CREATE_ORDER = 'ALLOW_WORKER_TO_CREATE_ORDER';
    const SETTING_VALUE__ALLOW_WORKER_TO_CREATE_ORDER = '1';
    const SETTING_TYPE__ALLOW_WORKER_TO_CREATE_ORDER = 'drivers';

    const SETTING_NAME__DENY_FAKEGPS = 'DENY_FAKEGPS';
    const SETTING_VALUE__DENY_FAKEGPS = '0';
    const SETTING_TYPE__DENY_FAKEGPS = 'drivers';

    const SETTING_NAME__DENY_SETTING_ARRIVAL_STATUS_EARLY = 'DENY_SETTING_ARRIVAL_STATUS_EARLY';
    const SETTING_VALUE__DENY_SETTING_ARRIVAL_STATUS_EARLY = '0';
    const SETTING_TYPE__DENY_SETTING_ARRIVAL_STATUS_EARLY = 'drivers';

    const SETTING_NAME__MIN_DISTANCE_TO_SET_ARRIVAL_STATUS = 'MIN_DISTANCE_TO_SET_ARRIVAL_STATUS';
    const SETTING_VALUE__MIN_DISTANCE_TO_SET_ARRIVAL_STATUS = '0';
    const SETTING_TYPE__MIN_DISTANCE_TO_SET_ARRIVAL_STATUS = 'drivers';

    const SQL_ADD_SETTINGS_WITH_CITY_AND_POSITION = <<<SQL
INSERT INTO `tbl_tenant_setting`
            (`tenant_id`, `city_id`, `position_id`, `name`, `value`, `type`)
SELECT `p`.`tenant_id`, `p`.`city_id`, `p`.`position_id`, :name `name`, :value `value`, :type `type`
FROM `tbl_tenant_has_city` AS `c`
JOIN `tbl_tenant_city_has_position` AS `p` ON `c`.`tenant_id` = `p`.`tenant_id` AND `c`.`city_id` = `p`.`city_id`
LEFT JOIN `tbl_tenant_setting` AS `t` ON `p`.`tenant_id` = `t`.`tenant_id` AND `p`.`city_id` = `t`.`city_id` AND `p`.`position_id` = `t`.`position_id` AND `t`.`name` = :name
WHERE `t`.`setting_id` IS NULL
SQL;

    public function safeUp()
    {
        $this->insert(self::TABLE_DEFAULT_SETTINGS, [
            'name'  => self::SETTING_NAME__ALLOW_WORKER_TO_CREATE_ORDER,
            'value' => self::SETTING_VALUE__ALLOW_WORKER_TO_CREATE_ORDER,
            'type'  => self::SETTING_TYPE__ALLOW_WORKER_TO_CREATE_ORDER,
        ]);

        $this->getDb()->createCommand(self::SQL_ADD_SETTINGS_WITH_CITY_AND_POSITION, [
            'name'  => self::SETTING_NAME__ALLOW_WORKER_TO_CREATE_ORDER,
            'value' => self::SETTING_VALUE__ALLOW_WORKER_TO_CREATE_ORDER,
            'type'  => self::SETTING_TYPE__ALLOW_WORKER_TO_CREATE_ORDER,
        ])->execute();

        $this->insert(self::TABLE_DEFAULT_SETTINGS, [
            'name'  => self::SETTING_NAME__DENY_FAKEGPS,
            'value' => self::SETTING_VALUE__DENY_FAKEGPS,
            'type'  => self::SETTING_TYPE__DENY_FAKEGPS,
        ]);

        $this->getDb()->createCommand(self::SQL_ADD_SETTINGS_WITH_CITY_AND_POSITION, [
            'name'  => self::SETTING_NAME__DENY_FAKEGPS,
            'value' => self::SETTING_VALUE__DENY_FAKEGPS,
            'type'  => self::SETTING_TYPE__DENY_FAKEGPS,
        ])->execute();

        $this->insert(self::TABLE_DEFAULT_SETTINGS, [
            'name'  => self::SETTING_NAME__DENY_SETTING_ARRIVAL_STATUS_EARLY,
            'value' => self::SETTING_VALUE__DENY_SETTING_ARRIVAL_STATUS_EARLY,
            'type'  => self::SETTING_TYPE__DENY_SETTING_ARRIVAL_STATUS_EARLY,
        ]);

        $this->getDb()->createCommand(self::SQL_ADD_SETTINGS_WITH_CITY_AND_POSITION, [
            'name'  => self::SETTING_NAME__DENY_SETTING_ARRIVAL_STATUS_EARLY,
            'value' => self::SETTING_VALUE__DENY_SETTING_ARRIVAL_STATUS_EARLY,
            'type'  => self::SETTING_TYPE__DENY_SETTING_ARRIVAL_STATUS_EARLY,
        ])->execute();

        $this->insert(self::TABLE_DEFAULT_SETTINGS, [
            'name'  => self::SETTING_NAME__MIN_DISTANCE_TO_SET_ARRIVAL_STATUS,
            'value' => self::SETTING_VALUE__MIN_DISTANCE_TO_SET_ARRIVAL_STATUS,
            'type'  => self::SETTING_TYPE__MIN_DISTANCE_TO_SET_ARRIVAL_STATUS,
        ]);

        $this->getDb()->createCommand(self::SQL_ADD_SETTINGS_WITH_CITY_AND_POSITION, [
            'name'  => self::SETTING_NAME__MIN_DISTANCE_TO_SET_ARRIVAL_STATUS,
            'value' => self::SETTING_VALUE__MIN_DISTANCE_TO_SET_ARRIVAL_STATUS,
            'type'  => self::SETTING_TYPE__MIN_DISTANCE_TO_SET_ARRIVAL_STATUS,
        ])->execute();
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::SETTING_NAME__ALLOW_WORKER_TO_CREATE_ORDER,
            'type' => self::SETTING_TYPE__ALLOW_WORKER_TO_CREATE_ORDER,
        ]);

        $this->delete(self::TABLE_TENANT_SETTING, [
            'name' => self::SETTING_NAME__ALLOW_WORKER_TO_CREATE_ORDER,
            'type' => self::SETTING_TYPE__ALLOW_WORKER_TO_CREATE_ORDER,
        ]);

        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::SETTING_NAME__DENY_FAKEGPS,
            'type' => self::SETTING_TYPE__DENY_FAKEGPS,
        ]);

        $this->delete(self::TABLE_TENANT_SETTING, [
            'name' => self::SETTING_NAME__DENY_FAKEGPS,
            'type' => self::SETTING_TYPE__DENY_FAKEGPS,
        ]);

        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::SETTING_NAME__DENY_SETTING_ARRIVAL_STATUS_EARLY,
            'type' => self::SETTING_TYPE__DENY_SETTING_ARRIVAL_STATUS_EARLY,
        ]);

        $this->delete(self::TABLE_TENANT_SETTING, [
            'name' => self::SETTING_NAME__DENY_SETTING_ARRIVAL_STATUS_EARLY,
            'type' => self::SETTING_TYPE__DENY_SETTING_ARRIVAL_STATUS_EARLY,
        ]);

        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::SETTING_NAME__MIN_DISTANCE_TO_SET_ARRIVAL_STATUS,
            'type' => self::SETTING_TYPE__MIN_DISTANCE_TO_SET_ARRIVAL_STATUS,
        ]);

        $this->delete(self::TABLE_TENANT_SETTING, [
            'name' => self::SETTING_NAME__MIN_DISTANCE_TO_SET_ARRIVAL_STATUS,
            'type' => self::SETTING_TYPE__MIN_DISTANCE_TO_SET_ARRIVAL_STATUS,
        ]);
    }

}
