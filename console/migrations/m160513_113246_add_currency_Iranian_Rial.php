<?php

use yii\db\Migration;

class m160513_113246_add_currency_Iranian_Rial extends Migration
{
    
    const TABLE_NAME = '{{%currency}}';
    
    public function up()
    {
        $this->insert(self::TABLE_NAME,[
            'type_id' => 1,
            'name' => 'Iranian Rial',
            'code' => 'IRR',
            'symbol' => 'IR',
            'numeric_code' => 364,
            'minor_unit' => 2,
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME,['code' => 'IRR']);
    }

}
