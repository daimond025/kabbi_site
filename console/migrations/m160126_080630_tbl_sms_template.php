<?php

use yii\db\Schema;
use yii\db\Migration;

class m160126_080630_tbl_sms_template extends Migration
{
    public function up()
    {
        $sms_template = (new \yii\db\Query())
            ->from('{{%sms_template}}')
            ->all();

        $arCity = [];
        $arDefaultCity = [];

        foreach ($sms_template as $item) {
            if (!isset($arCity[$item['tenant_id']])) {
                $tenantHasCity = (new \yii\db\Query())
                    ->select('city_id')
                    ->from('{{%tenant_has_city}}')
                    ->where(['tenant_id' => $item['tenant_id']])
                    ->all();
                $tenantHasCity = \yii\helpers\ArrayHelper::getColumn($tenantHasCity, 'city_id');
                $arCity[$item['tenant_id']] = $tenantHasCity;
                $arDefaultCity[$item['tenant_id']] = array_shift($arCity[$item['tenant_id']]);
            }

            if (empty($item['city_id'])) {
                Yii::$app->db->createCommand()->update('{{%sms_template}}', ['city_id' => $arDefaultCity[$item['tenant_id']]], ['template_id' => $item['template_id']])->execute();
            }

            foreach ($arCity[$item['tenant_id']] as $cityItem) {
                Yii::$app->db->createCommand()->insert('{{%sms_template}}', [
                    'tenant_id' => $item['tenant_id'],
                    'type'     => $item['type'],
                    'text'    => $item['text'],
                    'city_id'   => $cityItem,
                ])->execute();
            }
        }
    }

    public function down()
    {
        echo "m160126_080630_tbl_sms_template cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
