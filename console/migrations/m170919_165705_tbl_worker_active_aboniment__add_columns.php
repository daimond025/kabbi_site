<?php

use yii\db\Migration;

class m170919_165705_tbl_worker_active_aboniment__add_columns extends Migration
{
    const TABLE_NAME = '{{%worker_active_aboniment}}';
    const COLUMN_VALID_TO = 'valid_to';
    const COLUMN_SHIFT_VALID_TO = 'shift_valid_to';
    const COLUMN_EXPIRED_AT = 'expired_at';
    const COLUMN_COUNT_ACTIVE = 'count_active';

    public function safeUp()
    {
        $this->renameColumn(self::TABLE_NAME, self::COLUMN_VALID_TO, self::COLUMN_SHIFT_VALID_TO);
        $this->addColumn(self::TABLE_NAME, self::COLUMN_EXPIRED_AT, $this->integer());
        $this->alterColumn(self::TABLE_NAME, self::COLUMN_COUNT_ACTIVE, $this->integer(10));
    }

    public function safeDown()
    {
        $this->renameColumn(self::TABLE_NAME, self::COLUMN_SHIFT_VALID_TO, self::COLUMN_VALID_TO);
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_EXPIRED_AT);
        $this->alterColumn(self::TABLE_NAME, self::COLUMN_COUNT_ACTIVE, $this->integer(10)->notNull());
    }

}
