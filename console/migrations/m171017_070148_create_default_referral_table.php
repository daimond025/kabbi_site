<?php

use yii\db\Migration;

/**
 * Handles the creation of table `default_referral`.
 */
class m171017_070148_create_default_referral_table extends Migration
{
    const TABLE_NAME = '{{%default_referral}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'                 => $this->primaryKey(),
            'text_in_client_api' => $this->text(),
            'title'              => $this->text(),
            'text'               => $this->text(),
        ]);

        $this->insert(self::TABLE_NAME, [
            'text_in_client_api' => 'Поделитесь промо-кодом с друзьями! Как только приглашенный друг совершит поездку, вы получите #REFERRER_BONUS# ₽ на бонусный баланс, а друг получит #REFERRAL_BONUS# ₽. Чем больше друзей вы пригласите, тем больше бонусов! Удачи!',
            'title'              => 'Вам прислали код со скидкой на поездку в #NAME#.',
            'text'               => 'Привет! Вам прислали код со скидкой на поездку в #NAME#. 1. Скачай приложение #NAME# - “введите ссылку” 2. Введи мой код #CODE# и после первой поездки получите #REFERRAL_BONUS# ₽ на бонусный баланс.',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}


