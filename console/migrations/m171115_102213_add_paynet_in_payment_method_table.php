<?php

use yii\db\Migration;

class m171115_102213_add_paynet_in_payment_method_table extends Migration
{
    const TABLE_NAME = '{{%payment_method}}';
    const PAYMENT = 'TERMINAL_PAYNET';

    public function up()
    {


        $this->insert(self::TABLE_NAME, [
            'payment' => self::PAYMENT,
            'name'    => 'PayNet',
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME, 'payment = :payment', [
            'payment' => self::PAYMENT,
        ]);
    }


}
