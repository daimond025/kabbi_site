<?php

use yii\db\Schema;
use yii\db\Migration;

class m160123_125400_tbl_tenant_has_car_option extends Migration
{
    public function up()
    {
        $this->addColumn('{{%tenant_has_car_option}}', 'city_id', 'INT(11) UNSIGNED NULL DEFAULT NULL');
        $this->createIndex('tenant_has_car_option_idx', '{{%tenant_has_car_option}}', ['tenant_id', 'city_id']);
    }

    public function down()
    {
        $this->dropIndex('tenant_has_car_option_idx', '{{%tenant_has_car_option}}');
        $this->dropColumn('{{%tenant_has_car_option}}', 'city_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
