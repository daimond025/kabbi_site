<?php

use yii\db\Migration;

class m161122_062159_update_data_type_of_billing_sum_columns extends Migration
{
    const TABLE_CURRENCY = '{{%currency}}';
    const CURRENCY_ID_BYN = 6;

    const TABLE_TRANSACTION = '{{%transaction}}';
    const TABLE_OPERATION = '{{%operation}}';
    const TABLE_ACCOUNT = '{{%account}}';

    const COLUMN_TRANSACTION_SUM = 'sum';
    const COLUMN_TRANSACTION_SUM_OLD = 'sum_old';

    const COLUMN_OPERATION_SUM = 'sum';
    const COLUMN_OPERATION_SUM_OLD = 'sum_old';

    const COLUMN_OPERATION_SALDO = 'saldo';
    const COLUMN_OPERATION_SALDO_OLD = 'saldo_old';

    const COLUMN_ACCOUNT_BALANCE = 'balance';
    const COLUMN_ACCOUNT_BALANCE_NEW = 'balance_new';
    const COLUMN_ACCOUNT_BALANCE_OLD = 'balance_old';

    const SQL_FILL_TRANSACTION_SUM = <<<SQL
UPDATE tbl_transaction AS t
LEFT JOIN tbl_account AS r ON t.receiver_acc_id = r.account_id
LEFT JOIN tbl_account AS s ON t.sender_acc_id = s.account_id
LEFT JOIN tbl_currency AS c ON IFNULL(r.currency_id, s.currency_id) = c.currency_id
SET t.sum = ROUND(IFNULL(t.sum_old, 0) * POW(10, IFNULL(c.minor_unit, 2))) / POW(10, IFNULL(c.minor_unit, 2))
SQL;

    const SQL_FILL_OPERATION_SUM = <<<SQL
UPDATE tbl_operation AS o
LEFT JOIN tbl_account AS a ON o.account_id = a.account_id
LEFT JOIN tbl_currency AS c ON a.currency_id = c.currency_id
SET o.sum = ROUND(IFNULL(o.sum_old, 0) * POW(10, IFNULL(c.minor_unit, 2))) / POW(10, IFNULL(c.minor_unit, 2))
SQL;

    const SQL_FILL_ACCOUNT_BALANCE = <<<SQL
UPDATE tbl_account AS t
LEFT JOIN (
SELECT a.account_id, SUM(IFNULL(o.sum, 0) * IF(o.type_id=1,1,-1)) balance
FROM tbl_account AS a
JOIN tbl_operation AS o ON a.account_id = o.account_id
GROUP BY a.account_id) AS a ON t.account_id = a.account_id
SET t.balance = IFNULL(a.balance, 0)
  , t.balance_new = IFNULL(a.balance, 0)
SQL;

    const SQL_FILL_OPERATION_SALDO = <<<SQL
UPDATE tbl_operation t,
(SELECT o.account_id, o.operation_id,
IF(@a = o.account_id, 1, @s := 0),
IF(@a = o.account_id, 1, @a := o.account_id),
@s := @s + (IF(o.type_id=1,1,-1) * IFNULL(o.sum, 0)) saldo
FROM tbl_operation o,
(SELECT @a:=0, @s:=0) t
ORDER BY o.account_id, o.operation_id) t2
SET t.saldo = t2.saldo
WHERE t.operation_id = t2.operation_id
SQL;


    private function fillTransactionTable()
    {
        $time = microtime(true);
        echo '    > fill `' . self::COLUMN_TRANSACTION_SUM . '` of table ' . self::TABLE_TRANSACTION . ' ... ';
        $this->db->createCommand(self::SQL_FILL_TRANSACTION_SUM)->execute();
        echo ' done (time: ' . sprintf('%.3f', microtime(true) - $time) . "s)\n";
    }

    private function fillOperationTable()
    {
        $time = microtime(true);
        echo '    > fill `' . self::COLUMN_OPERATION_SUM . '` of table ' . self::TABLE_OPERATION . ' ... ';
        $this->db->createCommand(self::SQL_FILL_OPERATION_SUM)->execute();
        echo ' done (time: ' . sprintf('%.3f', microtime(true) - $time) . "s)\n";
    }

    private function fillAccountBalance()
    {
        $time = microtime(true);
        echo '    > fill `' . self::COLUMN_ACCOUNT_BALANCE . '` of table ' . self::TABLE_ACCOUNT . ' ... ';
        $this->db->createCommand(self::SQL_FILL_ACCOUNT_BALANCE)->execute();
        echo ' done (time: ' . sprintf('%.3f', microtime(true) - $time) . "s)\n";
    }

    private function fillOperationSaldo()
    {
        $time = microtime(true);
        echo '    > fill `' . self::COLUMN_OPERATION_SALDO . '` of table ' . self::TABLE_OPERATION . ' ... ';
        $this->db->createCommand(self::SQL_FILL_OPERATION_SALDO)->execute();
        echo ' done (time: ' . sprintf('%.3f', microtime(true) - $time) . "s)\n";
    }

    public function up()
    {
        $this->update(self::TABLE_CURRENCY, [
            'numeric_code' => 933,
            'minor_unit'   => 2,
        ], ['currency_id' => self::CURRENCY_ID_BYN]);

        $this->renameColumn(self::TABLE_TRANSACTION, self::COLUMN_TRANSACTION_SUM, self::COLUMN_TRANSACTION_SUM_OLD);
        $this->renameColumn(self::TABLE_OPERATION, self::COLUMN_OPERATION_SUM, self::COLUMN_OPERATION_SUM_OLD);
        $this->renameColumn(self::TABLE_OPERATION, self::COLUMN_OPERATION_SALDO, self::COLUMN_OPERATION_SALDO_OLD);
        $this->renameColumn(self::TABLE_ACCOUNT, self::COLUMN_ACCOUNT_BALANCE, self::COLUMN_ACCOUNT_BALANCE_OLD);

        $this->addColumn(self::TABLE_ACCOUNT, self::COLUMN_ACCOUNT_BALANCE,
            $this->decimal(20, 5)->notNull()->defaultValue(0));
        $this->addColumn(self::TABLE_ACCOUNT, self::COLUMN_ACCOUNT_BALANCE_NEW,
            $this->decimal(20, 5)->notNull()->defaultValue(0));
        $this->addColumn(self::TABLE_TRANSACTION, self::COLUMN_TRANSACTION_SUM,
            $this->decimal(20, 5)->notNull()->defaultValue(0));
        $this->addColumn(self::TABLE_OPERATION, self::COLUMN_OPERATION_SUM,
            $this->decimal(20, 5)->notNull()->defaultValue(0));
        $this->addColumn(self::TABLE_OPERATION, self::COLUMN_OPERATION_SALDO,
            $this->decimal(20, 5)->notNull()->defaultValue(0));

        $this->fillTransactionTable();
        $this->fillOperationTable();
        $this->fillAccountBalance();
        $this->fillOperationSaldo();
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_TRANSACTION, self::COLUMN_TRANSACTION_SUM);
        $this->dropColumn(self::TABLE_OPERATION, self::COLUMN_OPERATION_SUM);
        $this->dropColumn(self::TABLE_OPERATION, self::COLUMN_OPERATION_SALDO);
        $this->dropColumn(self::TABLE_ACCOUNT, self::COLUMN_ACCOUNT_BALANCE);
        $this->dropColumn(self::TABLE_ACCOUNT, self::COLUMN_ACCOUNT_BALANCE_NEW);

        $this->renameColumn(self::TABLE_TRANSACTION, self::COLUMN_TRANSACTION_SUM_OLD, self::COLUMN_TRANSACTION_SUM);
        $this->renameColumn(self::TABLE_OPERATION, self::COLUMN_OPERATION_SUM_OLD, self::COLUMN_OPERATION_SUM);
        $this->renameColumn(self::TABLE_OPERATION, self::COLUMN_OPERATION_SALDO_OLD, self::COLUMN_OPERATION_SALDO);
        $this->renameColumn(self::TABLE_ACCOUNT, self::COLUMN_ACCOUNT_BALANCE_OLD, self::COLUMN_ACCOUNT_BALANCE);
    }

}
