<?php

use yii\db\Migration;

class m161124_175827_create_table__worker_has_city extends Migration
{
    const TABLE_WORKER_HAS_CITY = '{{%worker_has_city}}';
    const TABLE_WORKER = '{{%worker}}';
    const TABLE_CITY = '{{%city}}';

    const INDEX_UNIQUE = 'idx_worker_has_city__worker_id__city_id';
    const FOREIGN_KEY_WORKER_ID = 'fk_worker_has_city__worker_id';
    const FOREIGN_KEY_CITY_ID = 'fk_worker_has_city__city_id';

    const SQL_FILL_TABLE = <<<SQL
insert into tbl_worker_has_city (worker_id, city_id, last_active) select worker_id, city_id, 1 last_active from tbl_worker;
SQL;


    public function up()
    {
        $this->createTable(self::TABLE_WORKER_HAS_CITY, [
            'id'          => $this->primaryKey(),
            'worker_id'   => $this->integer()->notNull(),
            'city_id'     => $this->integer(11)->unsigned()->notNull(),
            'last_active' => $this->integer(1)->notNull()->defaultValue(0),
        ]);

        $this->createIndex(
            self::INDEX_UNIQUE,
            self::TABLE_WORKER_HAS_CITY,
            ['worker_id', 'city_id']
        );

        $this->addForeignKey(
            self::FOREIGN_KEY_WORKER_ID,
            self::TABLE_WORKER_HAS_CITY,
            'worker_id',
            self::TABLE_WORKER,
            'worker_id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            self::FOREIGN_KEY_CITY_ID,
            self::TABLE_WORKER_HAS_CITY,
            'city_id',
            self::TABLE_CITY,
            'city_id',
            'CASCADE',
            'CASCADE'
        );

        $this->db->createCommand(self::SQL_FILL_TABLE)->execute();
    }

    public function down()
    {
        $this->dropTable(self::TABLE_WORKER_HAS_CITY);
    }

}
