<?php

use yii\db\Migration;

class m170113_113106_add_column_device_in_notification extends Migration
{
    const TABLE_NAME = '{{%notification}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'device', $this->string(20)->defaultValue('')->notNull());
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'device');
    }
}
