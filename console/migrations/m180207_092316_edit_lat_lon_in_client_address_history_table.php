<?php

use yii\db\Migration;

class m180207_092316_edit_lat_lon_in_client_address_history_table extends Migration
{
    const TABLE_NAME = '{{%client_address_history}}';

    public function safeUp()
    {
        $this->alterColumn(self::TABLE_NAME, 'lat', $this->string(100)->notNull());
        $this->alterColumn(self::TABLE_NAME, 'lon', $this->string(100)->notNull());
    }

    public function safeDown()
    {
        $this->alterColumn(self::TABLE_NAME, 'lat', $this->float()->notNull());
        $this->alterColumn(self::TABLE_NAME, 'lon', $this->float()->notNull());
    }
}
