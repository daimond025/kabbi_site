<?php

use yii\db\Migration;

class m170425_093701_tbl_tenant_setting__actualize_records extends Migration
{
    const SQL_ADD_MISSING_SETTINGS = <<<SQL
INSERT `tbl_tenant_setting`
 (`tenant_id`, `name`, `value`, `type`)
SELECT `t`.`tenant_id`, `d`.`name`, `d`.`value`, `d`.`type`
FROM `tbl_tenant` AS `t`
CROSS JOIN `tbl_default_settings` AS `d`
LEFT JOIN `tbl_tenant_setting` AS `s` ON `t`.`tenant_id` = `s`.`tenant_id` AND `d`.`name` = `s`.`name` AND `s`.`city_id` IS NULL AND `s`.`position_id` IS NULL
WHERE `d`.`type` = 'system' AND `s`.`setting_id` IS NULL
SQL;

    const SQL_ADD_MISSING_SETTINGS_WITH_CITY = <<<SQL
INSERT `tbl_tenant_setting`
 (`tenant_id`, `city_id`, `name`, `value`, `type`)
SELECT `t`.`tenant_id`, `t`.`city_id`, `d`.`name`, `d`.`value`, `d`.`type`
FROM `tbl_tenant_has_city` AS `t`
CROSS JOIN `tbl_default_settings` AS `d`
LEFT JOIN `tbl_tenant_setting` AS `s` ON `t`.`tenant_id` = `s`.`tenant_id` AND `t`.`city_id` = `s`.`city_id` AND `d`.`name` = `s`.`name` AND `s`.`position_id` IS NULL
WHERE `d`.`type` = 'general' AND `s`.`setting_id` IS NULL
SQL;

    const SQL_ADD_MISSING_SETTINGS_WITH_CITY_AND_POSITION = <<<SQL
INSERT `tbl_tenant_setting`
 (`tenant_id`, `city_id`, `position_id`, `name`, `value`, `type`)
SELECT `p`.`tenant_id`, `p`.`city_id`, `p`.`position_id`, `d`.`name`, `d`.`value`, `d`.`type`
FROM `tbl_tenant_has_city` AS `t`
JOIN `tbl_tenant_city_has_position` AS `p` ON `t`.`tenant_id` = `p`.`tenant_id` AND `t`.`city_id` = `p`.`city_id`
CROSS JOIN `tbl_default_settings` AS `d`
LEFT JOIN `tbl_tenant_setting` AS `s` ON `p`.`tenant_id` = `s`.`tenant_id` AND `p`.`city_id` = `s`.`city_id` AND `p`.`position_id` = `s`.`position_id` AND `d`.`name` = `s`.`name`
WHERE `d`.`type` IN ('orders', 'drivers') AND `s`.`setting_id` IS NULL
SQL;

    public function up()
    {
        $db = \Yii::$app->getDb();
        $db->createCommand(self::SQL_ADD_MISSING_SETTINGS)->execute();
        $db->createCommand(self::SQL_ADD_MISSING_SETTINGS_WITH_CITY)->execute();
        $db->createCommand(self::SQL_ADD_MISSING_SETTINGS_WITH_CITY_AND_POSITION)->execute();
    }

    public function down()
    {
    }

}
