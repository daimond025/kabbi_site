<?php

use yii\db\Migration;

class m160406_060145_edit_tbl_admin_busines_plan extends Migration
{
    const TABLE_NAME = '{{%admin_business_plan}}';
    public function up()
    {
        
        $this->alterColumn(self::TABLE_NAME,'create_time','TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
        $this->addColumn(self::TABLE_NAME,'is_registered',$this->integer(1)->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME,'is_registered');
    }

}
