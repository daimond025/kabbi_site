<?php

use yii\db\Migration;

class m160610_061745_add_auto_geo_service_labels extends Migration
{
    const TABLE_NAME = '{{%auto_geo_service_type}}';

    public function up()
    {
        //HERE.COM
        $this->execute("UPDATE" . self::TABLE_NAME . " SET key_1_label = 'APP CODE' where id = 2");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET key_2_label = 'APP ID' where id = 2");
        //Google
        $this->execute("UPDATE" . self::TABLE_NAME . " SET key_1_label = 'Google Places API key' where id = 3");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET key_2_label = 'Google Geocoding API key' where id = 3");
        //2GIS
        $this->execute("UPDATE" . self::TABLE_NAME . " SET key_1_label = '2GIS API key' where id = 4");
        //Yandex
        $this->execute("UPDATE" . self::TABLE_NAME . " SET key_1_label = 'Yandex maps API key' where id = 6");
    }

    public function down()
    {
        //HERE.COM
        $this->execute("UPDATE" . self::TABLE_NAME . " SET key_1_label = NULL where id = 2");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET key_2_label = NULL where id = 2");
        //Google
        $this->execute("UPDATE" . self::TABLE_NAME . " SET key_1_label = NULL where id = 3");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET key_2_label = NULL where id = 3");
        //2GIS
        $this->execute("UPDATE" . self::TABLE_NAME . " SET key_1_label = NULL where id = 4");
        //Yandex
        $this->execute("UPDATE" . self::TABLE_NAME . " SET key_1_label = NULL where id = 6");

        return false;
    }
}