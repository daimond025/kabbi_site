<?php

use yii\db\Migration;

class m170319_200500_tbl_worker__rename_column_city_id extends Migration
{
    const TABLE_WORKER = '{{%worker}}';
    const TABLE_CITY = '{{%city}}';
    const COLUMN_CITY_ID = 'city_id';
    const COLUMN_CITY_ID_OLD = 'city_id_old';
    const FOREIGN_KEY_CITY_ID = 'fk_tbl_worker_tbl_city1';

    public function up()
    {
        $this->dropForeignKey(self::FOREIGN_KEY_CITY_ID, self::TABLE_WORKER);
        $this->renameColumn(self::TABLE_WORKER, self::COLUMN_CITY_ID, self::COLUMN_CITY_ID_OLD);
        $this->alterColumn(self::TABLE_WORKER, self::COLUMN_CITY_ID_OLD, $this->integer()->unsigned());
    }

    public function down()
    {
        $this->renameColumn(self::TABLE_WORKER, self::COLUMN_CITY_ID_OLD, self::COLUMN_CITY_ID);
        $this->addForeignKey(self::FOREIGN_KEY_CITY_ID,
            self::TABLE_WORKER, self::COLUMN_CITY_ID,
            self::TABLE_CITY, self::COLUMN_CITY_ID,
            'RESTRICT', 'RESTRICT');
    }

}
