<?php

use yii\db\Migration;

class m170726_070412_remove_unused_tables_and_columns extends Migration
{
    const UNUSED_TABLES = [
        'tbl_passport_scan',

        // uat
        'tbl_driver_has_car_old',

        'tbl_driver_info_old',
        'tbl_driver_old',

        // prod
        'tbl_public_place_moderation_add',
        'tbl_public_place_moderation',
        'tbl_public_place',

        'tbl_street_moderation_add',
        'tbl_street_moderation',
        'tbl_street_short_name',
        'tbl_street',

        // uat
        'tbl_public_place_moderation_add_old',
        'tbl_public_place_moderation_old',
        'tbl_public_place_old',

        'tbl_street_moderation_add_old',
        'tbl_street_moderation_old',
        'tbl_street_short_name_old',
        'tbl_street_old',
    ];

    const TABLE_ACCOUNT = '{{%account}}';
    const COLUMN_BALANCE_OLD = 'balance_old';
    const COLUMN_BALANCE_NEW = 'balance_new';

    const TABLE_OPERATION = '{{%operation}}';
    const COLUMN_OPERATION_SUM_OLD = 'sum_old';
    const COLUMN_OPERATION_SALDO_OLD = 'saldo_old';

    const TABLE_TRANSACTION = '{{%transaction}}';
    const COLUMN_SUM_OLD = 'sum_old';

    const TABLE_WORKER = '{{%worker}}';
    const COLUMN_CITY_ID_OLD = 'city_id_old';

    const TABLE_WORKER_OPTION_TARIFF = '{{%worker_option_tariff}}';
    const COLUMN_PERIOD = 'period';
    const COLUMN_PERIOD_TYPE = 'period_type';
    const COLUMN_COST = 'cost';


    public function safeUp()
    {
        $tables = $this->db->schema->getTableNames();
        foreach (self::UNUSED_TABLES as $table) {
            if (in_array($table, $tables, true)) {
                $this->dropTable($table);
            }
        }

        $this->dropColumn(self::TABLE_ACCOUNT, self::COLUMN_BALANCE_OLD);
        $this->dropColumn(self::TABLE_ACCOUNT, self::COLUMN_BALANCE_NEW);

        $this->dropColumn(self::TABLE_OPERATION, self::COLUMN_OPERATION_SUM_OLD);
        $this->dropColumn(self::TABLE_OPERATION, self::COLUMN_OPERATION_SALDO_OLD);

        $this->dropColumn(self::TABLE_WORKER, self::COLUMN_CITY_ID_OLD);

        $this->dropColumn(self::TABLE_WORKER_OPTION_TARIFF, self::COLUMN_PERIOD);
        $this->dropColumn(self::TABLE_WORKER_OPTION_TARIFF, self::COLUMN_PERIOD_TYPE);
        $this->dropColumn(self::TABLE_WORKER_OPTION_TARIFF, self::COLUMN_COST);
    }

    public function safeDown()
    {
        // migration cannot reverted
        return false;
    }

}