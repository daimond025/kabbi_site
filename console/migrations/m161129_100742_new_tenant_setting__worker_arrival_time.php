<?php

use yii\db\Migration;

class m161129_100742_new_tenant_setting__worker_arrival_time extends Migration
{

    const DEFAULT_SETTING_TABLE = '{{%default_settings}}';
    const TENANT_SETTING_TABLE = '{{%tenant_setting}}';
    const CITY_TABLE_NAME = "{{%tenant_has_city}}";

    const NAME = 'WORKER_ARRIVAL_TIME';
    const TYPE = 'drivers';
    const DEFAULT_VALUE = '1;3;5;7;10;15';



    public function up()
    {
        $this->insert(self::DEFAULT_SETTING_TABLE, [
            'name' => self::NAME,
            'value' => self::DEFAULT_VALUE,
            'type' => self::TYPE
        ]);

        $this->updateTenantSettings();
    }

    public function down()
    {
        $this->delete(self::DEFAULT_SETTING_TABLE, ['name' => self::NAME]);
    }



    private function updateTenantSettings()
    {
        $time = microtime(true);

        $tenantCityList = (new \yii\db\Query())
            ->from('{{%tenant_has_city}}')
            ->all();

        foreach ($tenantCityList as $city) {
            Yii::$app->db->createCommand()->insert(self::TENANT_SETTING_TABLE, [
                'tenant_id' => $city['tenant_id'],
                'name' => self::NAME,
                'value' => self::DEFAULT_VALUE,
                'type' => self::TYPE,
                'city_id' => $city['city_id'],
            ])->execute();
        }

        echo '    > insert ' . count($tenantCityList) . ' records  from ' . self::TENANT_SETTING_TABLE;
        echo ' done (time: ' . sprintf('%.3f', microtime(true) - $time) . 's)' . PHP_EOL;

    }

}
