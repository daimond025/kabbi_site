<?php

use yii\db\Migration;
use common\modules\tenant\models\DefaultSettings;

class m180214_132924_add_setting__exchange_enable_sale_orders extends Migration
{

    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const TABLE_TENANT_SETTING = '{{%tenant_setting}}';

    const SETTING_NAME = 'EXCHANGE_ENABLE_SALE_ORDERS';
    const SETTING_TYPE = 'orders';
    const SETTING_VALUE = '0';

    public function safeUp()
    {
        $defaultSettings = new DefaultSettings();
        $defaultSettings->name = self::SETTING_NAME;
        $defaultSettings->type = self::SETTING_TYPE;
        $defaultSettings->value = self::SETTING_VALUE;


        $defaultSettings->save();

    }

    public function safeDown()
    {
        $this->delete(self::TABLE_TENANT_SETTING, [
            'name' => self::SETTING_NAME,
            'type' => self::SETTING_TYPE,
        ]);

        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::SETTING_NAME,
            'type' => self::SETTING_TYPE,
        ]);
    }
}
