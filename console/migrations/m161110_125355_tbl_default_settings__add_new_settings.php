<?php

use yii\db\Migration;
use yii\helpers\ArrayHelper;

class m161110_125355_tbl_default_settings__add_new_settings extends Migration
{
    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const TABLE_TENANT_SETTING = '{{%tenant_setting}}';
    const TABLE_TENANT_HAS_CITY = '{{%tenant_has_city}}';

    const NEW_SETTINGS = [
        [
            'name'  => 'SEND_PUSH_FREE_ORDER_FOR_ALL',
            'value' => 0,
            'type'  => 'orders',
        ],
        [
            'name'  => 'SEND_PUSH_PRE_ORDER_FOR_ALL',
            'value' => 0,
            'type'  => 'orders',
        ],
    ];

    public function up()
    {
        $newSettings = array_map(function ($setting) {
            return array_values($setting);
        }, self::NEW_SETTINGS);

        $this->batchInsert(self::TABLE_DEFAULT_SETTINGS, ['name', 'value', 'type'], $newSettings);

        $tenantCities = (new \yii\db\Query())
            ->from(self::TABLE_TENANT_HAS_CITY)
            ->all();

        foreach ($tenantCities as $item) {
            $tenantId = $item['tenant_id'];
            $cityId   = $item['city_id'];

            echo "tenant: {$tenantId}, city: {$cityId}";

            $settings = array_map(function ($setting) use ($tenantId, $cityId) {
                return array_merge([$tenantId, $cityId], $setting);
            }, $newSettings);

            $this->batchInsert(self::TABLE_TENANT_SETTING,
                ['tenant_id', 'city_id', 'name', 'value', 'type'],
                $settings
            );
        }
    }

    public function down()
    {
        $settings = ArrayHelper::getColumn(self::NEW_SETTINGS, 'name');

        $this->delete(self::TABLE_DEFAULT_SETTINGS, ['name' => $settings]);
        $this->delete(self::TABLE_TENANT_SETTING, ['name' => $settings]);
    }
}
