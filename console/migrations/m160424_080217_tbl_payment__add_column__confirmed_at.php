<?php

use yii\db\Migration;

class m160424_080217_tbl_payment__add_column__confirmed_at extends Migration
{
    const TABLE_PAYMENT = '{{%payment}}';
    const COLUMN_CONFIRMED_AT = 'confirmed_at';

    public function up()
    {
        $this->addColumn(self::TABLE_PAYMENT, self::COLUMN_CONFIRMED_AT, $this->integer());
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_PAYMENT, self::COLUMN_CONFIRMED_AT);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
