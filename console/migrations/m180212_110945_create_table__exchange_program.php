<?php

use yii\db\Migration;

class m180212_110945_create_table__exchange_program extends Migration
{

    const TABLE_NAME = '{{%exchange_program}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'exchange_program_id' => $this->primaryKey()->unsigned(),
            'name'                => $this->string()->notNull(),
            'created_at'          => $this->integer()->unsigned()->notNull(),
            'updated_at'          => $this->integer()->unsigned()->notNull(),
            'sort'                => $this->integer(),
            'block'               => 'ENUM("0", "1") NOT NULL',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
