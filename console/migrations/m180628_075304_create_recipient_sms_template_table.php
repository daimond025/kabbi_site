<?php

use yii\db\Migration;

/**
 * Handles the creation of table `recipient_sms_template`.
 */
class m180628_075304_create_recipient_sms_template_table extends Migration
{
    const TABLE_NAME = '{{%recipient_sms_template}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'template_id' => $this->primaryKey(),
            'tenant_id'   => $this->integer(10)->unsigned()->notNull(),
            'type'        => $this->string(45)->notNull(),
            'text'        => $this->text(),
            'city_id'     => $this->integer(11)->unsigned(),
            'position_id' => $this->integer(10)->notNull(),
            'updated_at'  => $this->integer(),
            'created_at'  => $this->integer(),
        ]);

        $this->addForeignKey('fk_recipient_sms_template__tenant_id', self::TABLE_NAME, 'tenant_id',
            '{{%tenant}}', 'tenant_id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk_recipient_sms_template__city_id', self::TABLE_NAME, 'city_id',
            '{{%city}}', 'city_id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk_recipient_sms_template__position_id', self::TABLE_NAME, 'position_id',
            '{{%position}}', 'position_id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
