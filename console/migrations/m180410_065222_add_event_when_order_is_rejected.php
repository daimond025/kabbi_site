<?php

use yii\db\Migration;
use common\modules\tenant\models\DefaultClientStatusEvent;

class m180410_065222_add_event_when_order_is_rejected extends Migration
{
    const TABLE_DEFAULT_CLIENT_STATUS_EVENT = '{{%default_client_status_event}}';
    const TABLE_CLIENT_STATUS_EVENT = '{{%client_status_event}}';
    const STATUS_ID = 120;

    private $positionsId = [];

    public function safeUp()
    {
        $this->positionsId = \yii\helpers\ArrayHelper::getColumn(\common\modules\employee\models\position\Position::find()->all(),'position_id');

        foreach($this->positionsId as $positionId) {

            $model = new DefaultClientStatusEvent(['status_id'=>self::STATUS_ID,'position_id'=>$positionId, 'group'=>'CALL', 'notice'=>'nothing']);

            $model->save();

            $model = new DefaultClientStatusEvent(['status_id'=>self::STATUS_ID,'position_id'=>$positionId, 'group'=>'APP', 'notice'=>'push']);

            $model->save();

            $model = new DefaultClientStatusEvent(['status_id'=>self::STATUS_ID,'position_id'=>$positionId, 'group'=>'WEB', 'notice'=>'nothing']);

            $model->save();

        }

        return true;
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_DEFAULT_CLIENT_STATUS_EVENT,['status_id'=>self::STATUS_ID]);
        $this->delete(self::TABLE_CLIENT_STATUS_EVENT,['status_id'=>self::STATUS_ID]);

        return true;
    }
}
