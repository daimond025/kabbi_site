<?php

use yii\db\Migration;

class m161102_185247_tbl_worker_shift__add_new_columns extends Migration
{
    const TABLE_WORKER_SHIFT = '{{%worker_shift}}';

    const COLUMN_ORDER_PAYMENT_TIME = 'order_payment_time';
    const COLUMN_ORDER_PAYMENT_COUNT = 'order_payment_count';

    public function up()
    {
        $this->addColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_ORDER_PAYMENT_TIME,
            $this->integer()->notNull()->defaultValue(0));
        $this->addColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_ORDER_PAYMENT_COUNT,
            $this->integer()->notNull()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_ORDER_PAYMENT_TIME);
        $this->dropColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_ORDER_PAYMENT_COUNT);
    }

}
