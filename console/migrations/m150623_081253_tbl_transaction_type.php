<?php

use yii\db\Schema;
use yii\db\Migration;

class m150623_081253_tbl_transaction_type extends Migration
{
    public function up()
    {
        $this->batchInsert('{{%transaction_type}}', ['name'], [['DEPOSIT'], ['WITHDRAWAL'], ['ORDER_PAYMENT']]);
    }

    public function down()
    {
        echo "m150623_081253_tbl_transaction_type cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
