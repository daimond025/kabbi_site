<?php

use yii\db\Migration;

class m160407_101944_tbl_tenant_additional_option__add_column extends Migration
{
    const TABLE_TENANT_ADDITIONAL_OPTION = '{{%tenant_additional_option}}';
    const COLUMN_STARTED_AT = 'started_at';
    const COLUMN_ACTIVE = 'active';

    public function up()
    {
        $this->addColumn(self::TABLE_TENANT_ADDITIONAL_OPTION, self::COLUMN_STARTED_AT, $this->integer()->notNull());
        $this->addColumn(self::TABLE_TENANT_ADDITIONAL_OPTION, self::COLUMN_ACTIVE,
            $this->integer()->notNull()->defaultValue(1));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_TENANT_ADDITIONAL_OPTION, self::COLUMN_STARTED_AT);
        $this->dropColumn(self::TABLE_TENANT_ADDITIONAL_OPTION, self::COLUMN_ACTIVE);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
