<?php

use yii\db\Migration;

class m160922_085324_add_column_in_car_option extends Migration
{

    const TABLE_NAME = '{{%car_option}}';
    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'sort', $this->integer()->notNull()->defaultValue(100));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'sort');
    }
}
