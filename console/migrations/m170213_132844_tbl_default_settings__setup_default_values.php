<?php

use yii\db\Migration;

class m170213_132844_tbl_default_settings__setup_default_values extends Migration
{
    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const SETTINGS = ['SHOW_URGENT_ORDER_TIME', 'SHOW_ESTIMATION', 'ALLOW_EDIT_ORDER'];

    public function up()
    {
        $this->update(self::TABLE_DEFAULT_SETTINGS, ['value' => 1], ['name' => self::SETTINGS]);
    }

    public function down()
    {
    }
}
