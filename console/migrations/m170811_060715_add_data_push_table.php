<?php

use yii\db\Migration;

class m170811_060715_add_data_push_table extends Migration
{

    const TABLE_PUSH = '{{%default_client_push_notifications}}';

    public $arr = [
        1 => [
            'ru' => 'Ваш заказ принят. Ожидайте мастера',
            'en' => 'Thank you for placing order. please wait for the vehicle',
        ],
        4 => [
            'ru' => 'Счастливого пути',
            'en' => 'Have a safe trip',
        ],
        5 => [
            'ru' => 'Спасибо за поездку. Сумма заказа #PRICE# #CURRENCY#',
            'en' => 'Thank you. your order cost is #PRICE# #CURRENCY#',
        ],
        6 => [
            'ru' => 'Заказ отменён',
            'en' => 'Order cancelled / Too bad. See you next time.',
        ],
        7 => [
            'ru' => '#TAXI_NAME#. Ваш код: #CODE#',
            'en' => '#TAXI_NAME# Receiving authorization code for website and application. #CODE#',
        ],
        8 => [
            'ru' => 'Извините, водитель опаздывает',
            'en' => 'We are sorry, the driver is late',
        ],
        9 => [
            'ru' => 'Спасибо, Ваш заказ принят.',
            'en' => 'Thank You for placing order',
        ],
        10 => [
            'ru' => 'К вам подъезжает #BRAND# #MODEL# #COLOR# #NUMBER# через #TIME# мин.',
            'en' => '#BRAND# #MODEL# #COLOR# #NUMBER# will get to you in #TIME# мин.',
        ],
        11 => [
            'ru' => 'Ваш водитель #BRAND# #MODEL# #COLOR# #NUMBER#',
            'en' => 'Your driver #BRAND# #MODEL# #COLOR# #NUMBER#',
        ],
        12 => [
            'ru' => 'Водитель снят с вашего заказа. Ожидайте назначения другого водителя',
            'en' => 'Driver was dismissed from your order. please wait for another driver'
        ],
        13 => [
            'ru' => 'извините, в вашем районе нет машин',
            'en' => 'We are sorry, no vehicles in your vicinity',
        ]
    ];

    public function up()
    {
        foreach ($this->arr as $key => $val) {

            $this->update(self::TABLE_PUSH, [
                'text' => $val['en'],
            ], ['push_id' => $key]);

        }
    }

    public function down()
    {

        foreach ($this->arr as $key => $val) {

            $this->update(self::TABLE_PUSH, [
                'text' => $val['ru'],
            ], ['push_id' => $key]);

        }
    }


}
