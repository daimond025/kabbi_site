<?php

use yii\db\Migration;

class m170727_061553_add_city_and_house_in_client_address_history extends Migration
{
    const TABLE_NAME = '{{%client_address_history}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'city', $this->string(255)->notNull()->after('address'));
        $this->addColumn(self::TABLE_NAME, 'house', $this->string(255)->notNull()->after('street'));
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'city');
        $this->dropColumn(self::TABLE_NAME, 'house');
    }
}
