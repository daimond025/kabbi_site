<?php

use yii\db\Migration;

class m160828_161221_tbl_account_kind__add_new_kind_for_uds_game extends Migration
{
    const TABLE_ACCOUNT_KIND = '{{%account_kind}}';

    public function safeUp()
    {
        $this->batchInsert(self::TABLE_ACCOUNT_KIND, ['kind_id', 'name'], [
            [9, 'CLIENT_BONUS_UDS_GAME'],
            [10, 'SYSTEM_BONUS_UDS_GAME'],
        ]);
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_ACCOUNT_KIND, [
            'kind_id' => [9, 10],
        ]);
    }

}
