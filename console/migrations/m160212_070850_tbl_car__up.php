<?php

use yii\db\Schema;
use yii\db\Migration;
use yii\db\Query;

class m160212_070850_tbl_car__up extends Migration
{
    const TABLE_CAR = '{{%car}}';
    const TABLE_COLOR = '{{%car_color}}';

    public function up()
    {
        $colors = (new Query())
                ->from(self::TABLE_COLOR)
                ->all();

        foreach ($colors as $color) {
            $this->update(self::TABLE_CAR, ['color' => $color['color_id']], ['color' => $color['name']]);
        }

        $this->alterColumn(self::TABLE_CAR, 'color', $this->integer(11));
        $this->addForeignKey('fr_car_car_color_1', self::TABLE_CAR, 'color', self::TABLE_COLOR, 'color_id', 'SET NULL');
    }

    public function down()
    {
        echo "m160212_070850_tbl_car__up cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
