<?php

use yii\db\Schema;
use yii\db\Migration;

class m151002_135336_car_driver_groups extends Migration
{
    public function up()
    {
        $this->execute('SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0');
        $this->execute('SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0');
        $this->execute("SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES'");

        $this->createTable('{{%car_driver_group_has_class}}', [
            'car_id' => Schema::TYPE_INTEGER . '(10) UNSIGNED NOT NULL',
            'class_id' => 'tinyint(3) UNSIGNED NOT NULL',
        ]);

        $this->addForeignKey('FK_car_driver_group_has_class_car_id', '{{%car_driver_group_has_class}}', 'car_id', '{{%car}}', 'car_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_car_driver_group_has_class_class_id', '{{%car_driver_group_has_class}}', 'class_id', '{{%car_class}}', 'class_id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%car_driver_group_has_tariff}}', [
            'car_id' => Schema::TYPE_INTEGER . '(10) UNSIGNED NOT NULL',
            'tariff_id' => Schema::TYPE_INTEGER . '(10) UNSIGNED NOT NULL',
        ]);

        $this->addForeignKey('FK_car_driver_group_has_tariff_car_id', '{{%car_driver_group_has_tariff}}', 'car_id', '{{%car}}', 'car_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_car_driver_group_has_tariff_tariff_id', '{{%car_driver_group_has_tariff}}', 'tariff_id', '{{%driver_tariff}}', 'tariff_id', 'CASCADE', 'CASCADE');

        $this->execute('SET SQL_MODE=@OLD_SQL_MODE');
        $this->execute('SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS');
        $this->execute('SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS');
    }

    public function down()
    {
        $this->dropForeignKey('FK_car_driver_group_has_class_car_id', '{{%car_driver_group_has_class}}');
        $this->dropForeignKey('FK_car_driver_group_has_class_class_id', '{{%car_driver_group_has_class}}');
        $this->dropForeignKey('FK_car_driver_group_has_tariff_car_id', '{{%car_driver_group_has_tariff}}');
        $this->dropForeignKey('FK_car_driver_group_has_tariff_tariff_id', '{{%car_driver_group_has_tariff}}');

        $this->dropTable('{{%car_driver_group_has_class}}');
        $this->dropTable('{{%car_driver_group_has_tariff}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
