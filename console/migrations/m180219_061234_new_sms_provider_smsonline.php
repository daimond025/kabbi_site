<?php

use yii\db\Migration;

class m180219_061234_new_sms_provider_smsonline extends Migration
{
    const TABLE_NAME = '{{%sms_server}}';
    const PROVIDER_ID = 26;

    public function up()
    {
        $this->insert(self::TABLE_NAME, [
            'server_id' => self::PROVIDER_ID,
            'name'      => 'Sms-online',
            'host'      => 'https://en.sms-online.com/',
            'active'    => '1',
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME, ['server_id' => self::PROVIDER_ID]);
    }
}
