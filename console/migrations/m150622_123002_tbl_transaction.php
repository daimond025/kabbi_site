<?php

use yii\db\Schema;
use yii\db\Migration;

class m150622_123002_tbl_transaction extends Migration
{
    public function up()
    {
        $this->addColumn('{{%transaction}}', 'comment', Schema::TYPE_TEXT);
    }

    public function down()
    {
        $this->dropColumn('{{%transaction}}', 'comment');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
