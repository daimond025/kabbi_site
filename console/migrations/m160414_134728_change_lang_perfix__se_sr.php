<?php

use yii\db\Migration;

class m160414_134728_change_lang_perfix__se_sr extends Migration
{
    const OLD_PREFIX = 'se';
    const NEW_PREFIX = 'sr';

    const TABLE_COUNTRY = '{{%country}}';
    const TABLE_REPUBLIC = '{{%republic}}';
    const TABLE_CITY = '{{%city}}';
    const TABLE_STREET = '{{%street}}';
    const TABLE_PUBLIC_PLACE = '{{%public_place}}';

    const COLUMN_NAME = 'name';
    const COLUMN_SHORTNAME = 'shortname';
    const COLUMN_FULLADDRESS = 'fulladdress';
    const COLUMN_CITY_NAME = 'city_name';
    const COLUMN_STREET_NAME = 'street_name';
    const COLUMN_STREET_SHORTNAME = 'street_shortname';
    const COLUMN_STREET_AREA = 'street_area';
    const COLUMN_LABEL = 'label';

    public function up()
    {
        $this->renameColumn(self::TABLE_COUNTRY, self::COLUMN_NAME . '_' . self::OLD_PREFIX,
            self::COLUMN_NAME . '_' . self::NEW_PREFIX);

        $this->renameColumn(self::TABLE_REPUBLIC, self::COLUMN_NAME . '_' . self::OLD_PREFIX,
            self::COLUMN_NAME . '_' . self::NEW_PREFIX);
        $this->renameColumn(self::TABLE_REPUBLIC, self::COLUMN_SHORTNAME . '_' . self::OLD_PREFIX,
            self::COLUMN_SHORTNAME . '_' . self::NEW_PREFIX);

        $this->renameColumn(self::TABLE_CITY, self::COLUMN_NAME . '_' . self::OLD_PREFIX,
            self::COLUMN_NAME . '_' . self::NEW_PREFIX);
        $this->renameColumn(self::TABLE_CITY, self::COLUMN_FULLADDRESS . '_' . self::OLD_PREFIX,
            self::COLUMN_FULLADDRESS . '_' . self::NEW_PREFIX);

        $this->renameColumn(self::TABLE_STREET, self::COLUMN_CITY_NAME . '_' . self::OLD_PREFIX,
            self::COLUMN_CITY_NAME . '_' . self::NEW_PREFIX);
        $this->renameColumn(self::TABLE_STREET, self::COLUMN_STREET_NAME . '_' . self::OLD_PREFIX,
            self::COLUMN_STREET_NAME . '_' . self::NEW_PREFIX);
        $this->renameColumn(self::TABLE_STREET, self::COLUMN_STREET_SHORTNAME . '_' . self::OLD_PREFIX,
            self::COLUMN_STREET_SHORTNAME . '_' . self::NEW_PREFIX);
        $this->renameColumn(self::TABLE_STREET, self::COLUMN_FULLADDRESS . '_' . self::OLD_PREFIX,
            self::COLUMN_FULLADDRESS . '_' . self::NEW_PREFIX);
        $this->renameColumn(self::TABLE_STREET, self::COLUMN_STREET_AREA . '_' . self::OLD_PREFIX,
            self::COLUMN_STREET_AREA . '_' . self::NEW_PREFIX);

        $this->renameColumn(self::TABLE_PUBLIC_PLACE, self::COLUMN_LABEL . '_' . self::OLD_PREFIX,
            self::COLUMN_LABEL . '_' . self::NEW_PREFIX);
    }

    public function down()
    {
        $this->renameColumn(self::TABLE_COUNTRY, self::COLUMN_NAME . '_' . self::NEW_PREFIX,
            self::COLUMN_NAME . '_' . self::OLD_PREFIX);

        $this->renameColumn(self::TABLE_REPUBLIC, self::COLUMN_NAME . '_' . self::NEW_PREFIX,
            self::COLUMN_NAME . '_' . self::OLD_PREFIX);
        $this->renameColumn(self::TABLE_REPUBLIC, self::COLUMN_SHORTNAME . '_' . self::NEW_PREFIX,
            self::COLUMN_SHORTNAME . '_' . self::OLD_PREFIX);

        $this->renameColumn(self::TABLE_CITY, self::COLUMN_NAME . '_' . self::NEW_PREFIX,
            self::COLUMN_NAME . '_' . self::OLD_PREFIX);
        $this->renameColumn(self::TABLE_CITY, self::COLUMN_FULLADDRESS . '_' . self::NEW_PREFIX,
            self::COLUMN_FULLADDRESS . '_' . self::OLD_PREFIX);

        $this->renameColumn(self::TABLE_STREET, self::COLUMN_CITY_NAME . '_' . self::NEW_PREFIX,
            self::COLUMN_CITY_NAME . '_' . self::OLD_PREFIX);
        $this->renameColumn(self::TABLE_STREET, self::COLUMN_STREET_NAME . '_' . self::NEW_PREFIX,
            self::COLUMN_STREET_NAME . '_' . self::OLD_PREFIX);
        $this->renameColumn(self::TABLE_STREET, self::COLUMN_STREET_SHORTNAME . '_' . self::NEW_PREFIX,
            self::COLUMN_STREET_SHORTNAME . '_' . self::OLD_PREFIX);
        $this->renameColumn(self::TABLE_STREET, self::COLUMN_FULLADDRESS . '_' . self::NEW_PREFIX,
            self::COLUMN_FULLADDRESS . '_' . self::OLD_PREFIX);
        $this->renameColumn(self::TABLE_STREET, self::COLUMN_STREET_AREA . '_' . self::NEW_PREFIX,
            self::COLUMN_STREET_AREA . '_' . self::OLD_PREFIX);

        $this->renameColumn(self::TABLE_PUBLIC_PLACE, self::COLUMN_LABEL . '_' . self::NEW_PREFIX,
            self::COLUMN_LABEL . '_' . self::OLD_PREFIX);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
