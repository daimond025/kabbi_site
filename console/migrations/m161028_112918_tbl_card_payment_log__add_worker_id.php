<?php

use yii\db\Migration;

class m161028_112918_tbl_card_payment_log__add_worker_id extends Migration
{
    const TABLE_CARD_PAYMENT_LOG = '{{%card_payment_log}}';
    const TABLE_WORKER = '{{%worker}}';

    const COLUMN_WORKER_ID = 'worker_id';
    const COLUMN_CLIENT_ID = 'client_id';
    const FOREIGN_KEY_WORKER_ID = 'fk_tbl_card_payment_log__worker_id';

    public function up()
    {
        $this->addColumn(self::TABLE_CARD_PAYMENT_LOG, self::COLUMN_WORKER_ID, $this->integer(10));
        $this->alterColumn(self::TABLE_CARD_PAYMENT_LOG, self::COLUMN_CLIENT_ID, 'INT(10) UNSIGNED NULL');

        $this->addForeignKey(self::FOREIGN_KEY_WORKER_ID, self::TABLE_CARD_PAYMENT_LOG, self::COLUMN_WORKER_ID,
            self::TABLE_WORKER, self::COLUMN_WORKER_ID, 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey(self::FOREIGN_KEY_WORKER_ID, self::TABLE_CARD_PAYMENT_LOG);
        $this->dropColumn(self::TABLE_CARD_PAYMENT_LOG, self::COLUMN_WORKER_ID);
    }
}
