<?php

use yii\db\Migration;

class m161005_085258_new_default_setting_visible_worker_phone extends Migration
{
    const TABLE_NAME = '{{%default_settings}}';
    const SETTING_NAME = 'VISIBLE_WORKER_PHONE';
    public function up()
    {
        $this->insert(self::TABLE_NAME, [
            'name' => self::SETTING_NAME,
            'value' => '0',
            'type' => 'orders'
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME, [
            'name' => self::SETTING_NAME,
        ]);
    }
}
