<?php

use yii\db\Migration;

class m170220_091757_add_app_id_column_in_clients_and_orders extends Migration
{
    const TABLE_NAME_CLIENTS = '{{%client}}';
    const TABLE_NAME_ORDERS = '{{%order}}';

    protected $_apps = [];


    public function up()
    {
        $this->addColumn(self::TABLE_NAME_CLIENTS, 'app_id', $this->integer(10)->unsigned()->after('device_token'));
        $this->addColumn(self::TABLE_NAME_ORDERS, 'app_id',
            $this->integer(10)->unsigned()->after('client_device_token'));

        $this->dataFill();
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME_CLIENTS, 'app_id');
        $this->dropColumn(self::TABLE_NAME_ORDERS, 'app_id');
    }


    protected function getAppIdByTenantId($tenantId)
    {
        if (!array_key_exists($tenantId, $this->_apps)) {
            $this->_apps[$tenantId] = (new \yii\db\Query())->select('app_id')
                ->from('{{%mobile_app}}')
                ->where([
                    'tenant_id' => $tenantId,
                ])
                ->limit(1)
                ->scalar();
        }

        return $this->_apps[$tenantId];
    }

    protected function dataFill()
    {
        $clients = (new \yii\db\Query())->from(self::TABLE_NAME_CLIENTS)->select(['client_id', 'tenant_id'])->all();

        echo '    > update clients ...';
        $time = microtime(true);
        foreach ($clients as $client) {
            $tenantId = $client['tenant_id'];
            $appId    = $this->getAppIdByTenantId($tenantId);
            if (!empty($appId)) {
                $this->db->createCommand()->update(self::TABLE_NAME_CLIENTS, ['app_id' => $appId],
                    ['client_id' => $client['client_id']])->execute();
            }
        }
        echo ' done (time: ' . sprintf('%.3f', microtime(true) - $time) . "s)\n";
    }

}
