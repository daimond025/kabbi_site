<?php

use yii\db\Migration;

class m180321_071552_add_field_order__predv_price_no_discount extends Migration
{

    const TABLE_NAME = '{{%order}}';

    const COLUMN_NAME = 'predv_price_no_discount';

    public function safeUp()
    {
        $this->addColumn(
            self::TABLE_NAME,
            self::COLUMN_NAME,
            $this->decimal(10, 2)->after('predv_price')
        );
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);
    }

}
