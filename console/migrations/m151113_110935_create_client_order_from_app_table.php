<?php

use yii\db\Schema;
use yii\db\Migration;

class m151113_110935_create_client_order_from_app_table extends Migration
{
    const TABLE_NAME = '{{%client_order_from_app}}';
    const CLIENT_TABLE_NAME = '{{%client}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'client_id' => 'int(10) UNSIGNED NOT NULL',
            'count' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk_client_order_from_app__client_id',
                self::TABLE_NAME, 'client_id',
                self::CLIENT_TABLE_NAME, 'client_id',
                'CASCADE', 'CASCADE');
        $this->createIndex('ui_client_order_from_app', self::TABLE_NAME, 'client_id', true);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
