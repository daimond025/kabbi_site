<?php

use yii\db\Migration;

class m160422_094917_fix_birthday_in_driver_info extends Migration
{
    const TABLE_NAME = '{{%driver_info}}';
    
    public function up()
    {
        $this->update(self::TABLE_NAME, ['birthday' => null], 'birthday REGEXP "^(.{8}00)|(.{5}00.{3})|(0000.{8})$"');
    }

    public function down()
    {
        echo "m160422_094917_fix_birthday_in_driver_info cannot be reverted.\n";
    }
}
