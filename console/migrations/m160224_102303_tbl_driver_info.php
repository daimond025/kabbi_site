<?php

use yii\db\Migration;

class m160224_102303_tbl_driver_info extends Migration
{
    const TABLE_NAME = '{{%driver_info}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'osago_scan', $this->string());
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'osago_scan');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
