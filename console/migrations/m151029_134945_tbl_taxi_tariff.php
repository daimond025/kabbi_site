<?php

use yii\db\Schema;
use yii\db\Migration;

class m151029_134945_tbl_taxi_tariff extends Migration
{

    public function up()
    {
        $this->execute('SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0');
        $this->execute('SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0');
        $this->execute("SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES'");

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%taxi_tariff_group}}', [
            'group_id'  => $this->primaryKey(),
            'tenant_id' => Schema::TYPE_INTEGER . ' UNSIGNED',
            'name'      => $this->string()->notNull(),
            'sort'      => $this->smallInteger()->defaultValue(100),
                ], $tableOptions);

        $this->addForeignKey('FK_taxi_tariff_group_tenant_id', '{{%taxi_tariff_group}}', 'tenant_id', '{{%tenant}}', 'tenant_id', 'CASCADE', 'CASCADE');

        $this->addColumn('{{%taxi_tariff}}', 'group_id', Schema::TYPE_INTEGER);
        $this->addColumn('{{%taxi_tariff}}', 'name', Schema::TYPE_STRING);
        $this->addColumn('{{%taxi_tariff}}', 'description', Schema::TYPE_STRING);
        $this->addColumn('{{%taxi_tariff}}', 'sort', Schema::TYPE_SMALLINT . ' DEFAULT 100');
        $this->addColumn('{{%taxi_tariff}}', 'enabled_site', Schema::TYPE_BOOLEAN . ' DEFAULT 1');
        $this->addColumn('{{%taxi_tariff}}', 'enabled_app', Schema::TYPE_BOOLEAN . ' DEFAULT 1');
        $this->addColumn('{{%taxi_tariff}}', 'enabled_operator', Schema::TYPE_BOOLEAN . ' DEFAULT 1');

        $this->addColumn('{{%option_tariff}}', 'enabled_parking_ratio', Schema::TYPE_BOOLEAN . ' DEFAULT 1');
        $this->addColumn('{{%option_tariff}}', 'calculation_fix', Schema::TYPE_BOOLEAN . ' DEFAULT 0');
        $this->addColumn('{{%option_tariff}}', 'next_cost_unit_day', Schema::TYPE_STRING);
        $this->addColumn('{{%option_tariff}}', 'next_cost_unit_night', Schema::TYPE_STRING);

        $this->alterColumn('{{%option_tariff}}', 'accrual', 'ENUM("DISTANCE","TIME","FIX","MIXED") NOT NULL');

        $this->addForeignKey('FK_taxi_tariff_group_id', '{{%taxi_tariff}}', 'group_id', '{{%taxi_tariff_group}}', 'group_id', 'SET NULL', 'CASCADE');

        $this->execute('SET SQL_MODE=@OLD_SQL_MODE');
        $this->execute('SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS');
        $this->execute('SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS');
    }

    public function down()
    {
        $this->dropForeignKey('FK_taxi_tariff_group_tenant_id', '{{%taxi_tariff_group}}');
        $this->dropForeignKey('FK_taxi_tariff_group_id', '{{%taxi_tariff}}');

        $this->dropTable('{{%taxi_tariff_group}}');

        $this->dropColumn('{{%taxi_tariff}}', 'group_id');
        $this->dropColumn('{{%taxi_tariff}}', 'name');
        $this->dropColumn('{{%taxi_tariff}}', 'description');
        $this->dropColumn('{{%taxi_tariff}}', 'sort');
        $this->dropColumn('{{%taxi_tariff}}', 'enabled_site');
        $this->dropColumn('{{%taxi_tariff}}', 'enabled_app');
        $this->dropColumn('{{%taxi_tariff}}', 'enabled_operator');

        $this->dropColumn('{{%option_tariff}}', 'enabled_parking_ratio');
        $this->dropColumn('{{%option_tariff}}', 'calculation_fix');
        $this->dropColumn('{{%option_tariff}}', 'next_cost_unit_day');
        $this->dropColumn('{{%option_tariff}}', 'next_cost_unit_night');

        $this->alterColumn('{{%option_tariff}}', 'accrual', 'ENUM("DISTANCE","TIME","FIX") NOT NULL');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
