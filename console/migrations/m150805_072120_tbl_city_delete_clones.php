<?php

use yii\db\Schema;
use yii\db\Migration;

class m150805_072120_tbl_city_delete_clones extends Migration
{
    public function up()
    {
        $this->execute('DELETE t1
        FROM tbl_city t1
        JOIN (SELECT MIN(city_id) city_id, name, lat, lon FROM tbl_city GROUP BY name, lat, lon) t2
        ON t1.city_id <> t2.city_id AND t1.name = t2.name AND t1.lat = t2.lat AND t1.lon = t2.lon');
    }

    public function down()
    {
        echo "m150805_072120_tbl_city_delete_clones cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
