<?php

use yii\db\Schema;
use yii\db\Migration;

class m150629_054949_tbl_car extends Migration
{
    public function up()
    {
        $this->addColumn('{{%car}}', 'brand', Schema::TYPE_STRING);
        $this->addColumn('{{%car}}', 'model', Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('{{%car}}', 'brand');
        $this->dropColumn('{{%car}}', 'model');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
