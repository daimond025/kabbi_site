<?php

use yii\db\Migration;

class m160511_093101_tbl_republic extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%republic}}', 'timezone', $this->float());
    }

    public function down()
    {
        echo "m160511_093101_tbl_republic cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
