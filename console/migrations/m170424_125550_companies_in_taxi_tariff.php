<?php

use yii\db\Migration;

class m170424_125550_companies_in_taxi_tariff extends Migration
{
    const TABLE_TARIFF = '{{%taxi_tariff}}';
    const TABLE_TARIFF_HAS_COMPANY = '{{%taxi_tariff_has_company}}';

    public function up()
    {
        $this->addColumn(self::TABLE_TARIFF, 'type',
            'ENUM("BASE", "COMPANY", "COMPANY_CORP_BALANCE") NOT NULL DEFAULT "BASE"');
//        $this->addColumn(self::TABLE_TARIFF, 'company_id', $this->integer(10)->unsigned()->after('group_id'));
//
//        $this->addForeignKey('fk_tbl_taxi_tariff__company_id',
//            self::TABLE_TARIFF, 'company_id',
//            'tbl_client_company', 'company_id',
//            'SET NULL', 'CASCADE');

        $this->createTable(self::TABLE_TARIFF_HAS_COMPANY, [
            'id' => $this->primaryKey(10)->unsigned(),
            'tariff_id' => $this->integer(10)->unsigned()->notNull(),
            'company_id' => $this->integer(10)->unsigned()->notNull(),
        ]);
        $this->addForeignKey('fk_tbl_taxi_tariff_has_company__company_id',
            self::TABLE_TARIFF_HAS_COMPANY, 'company_id',
            'tbl_client_company', 'company_id',
            'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_tbl_taxi_tariff_has_company__tariff_id',
            self::TABLE_TARIFF_HAS_COMPANY, 'tariff_id',
            self::TABLE_TARIFF, 'tariff_id',
            'CASCADE', 'CASCADE');

    }

    public function down()
    {
        $this->dropForeignKey('fk_tbl_taxi_tariff_has_company__company_id', self::TABLE_TARIFF_HAS_COMPANY);
        $this->dropForeignKey('fk_tbl_taxi_tariff_has_company__tariff_id', self::TABLE_TARIFF_HAS_COMPANY);
        $this->dropTable(self::TABLE_TARIFF_HAS_COMPANY);
        $this->dropColumn(self::TABLE_TARIFF, 'type');
    }
}
