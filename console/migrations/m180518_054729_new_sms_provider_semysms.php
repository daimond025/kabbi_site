<?php

use yii\db\Migration;

class m180518_054729_new_sms_provider_semysms extends Migration
{
    const TABLE_NAME = '{{%sms_server}}';

    const COLUMN_SERVER_ID = 27;

    public function up()
    {
        $this->insert(self::TABLE_NAME,[
            'server_id' => self::COLUMN_SERVER_ID,
            'name' =>  'SemySMS.net',
            'host' => 'https://semysms.net/',
            'active' => '1',
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME,['server_id' => self::COLUMN_SERVER_ID]);
    }
}
