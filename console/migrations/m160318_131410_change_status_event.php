<?php

use yii\db\Migration;

class m160318_131410_change_status_event extends Migration
{
    const NEW_STATUS_EVENT = 107;
    const OLD_STATUS_EVENT = 40;
    const DEFAULT_PUSH_TABLE = '{{%default_client_push_notifications}}';
    const DEFAULT_SMS_TABLE = '{{%default_sms_template}}';
    const PUSH_TABLE = '{{%client_push_notifications}}';
    const SMS_TABLE = '{{%sms_template}}';
    const DEFAULT_STATUS_EVENT_TABLE = '{{%default_client_status_event}}';
    const STATUS_EVENT_TABLE = '{{%client_status_event}}';

    public function up()
    {
        $this->update(self::DEFAULT_PUSH_TABLE, ['type' => self::NEW_STATUS_EVENT], ['type' => self::OLD_STATUS_EVENT]);
        $this->update(self::DEFAULT_SMS_TABLE, ['type' => self::NEW_STATUS_EVENT], ['type' => self::OLD_STATUS_EVENT]);

        $this->update(self::PUSH_TABLE, ['type' => self::NEW_STATUS_EVENT], ['type' => self::OLD_STATUS_EVENT]);
        $this->update(self::SMS_TABLE, ['type' => self::NEW_STATUS_EVENT], ['type' => self::OLD_STATUS_EVENT]);

        $this->update(self::DEFAULT_STATUS_EVENT_TABLE, ['status_id' => self::NEW_STATUS_EVENT], ['status_id' => self::OLD_STATUS_EVENT]);
        $this->update(self::STATUS_EVENT_TABLE, ['status_id' => self::NEW_STATUS_EVENT], ['status_id' => self::OLD_STATUS_EVENT]);
    }

    public function down()
    {
        $this->update(self::DEFAULT_PUSH_TABLE, ['type' => self::OLD_STATUS_EVENT], ['type' => self::NEW_STATUS_EVENT]);
        $this->update(self::DEFAULT_SMS_TABLE, ['type' => self::OLD_STATUS_EVENT], ['type' => self::NEW_STATUS_EVENT]);

        $this->update(self::PUSH_TABLE, ['type' => self::OLD_STATUS_EVENT], ['type' => self::NEW_STATUS_EVENT]);
        $this->update(self::SMS_TABLE, ['type' => self::OLD_STATUS_EVENT], ['type' => self::NEW_STATUS_EVENT]);

        $this->update(self::DEFAULT_STATUS_EVENT_TABLE, ['status_id' => self::OLD_STATUS_EVENT], ['status_id' => self::NEW_STATUS_EVENT]);
        $this->update(self::STATUS_EVENT_TABLE, ['status_id' => self::OLD_STATUS_EVENT], ['status_id' => self::NEW_STATUS_EVENT]);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
