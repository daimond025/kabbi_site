<?php

use yii\db\Migration;

class m160329_123407_tbl_tariff_entity_type__fill_table extends Migration
{
    const TABLE_NAME = '{{%tariff_entity_type}}';

    public function up()
    {
        $this->batchInsert(self::TABLE_NAME, ['id', 'code', 'name'], [
            [1, 1, 'Tariff'],
            [2, 2, 'Additional option'],
            [3, 3, 'One-time service'],
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME, '1');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
