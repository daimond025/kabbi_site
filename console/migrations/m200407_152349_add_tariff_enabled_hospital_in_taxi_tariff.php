<?php

use yii\db\Migration;

class m200407_152349_add_tariff_enabled_hospital_in_taxi_tariff extends Migration
{
    const TABLE_NAME = '{{%taxi_tariff}}';
    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'enabled_hospital', $this->integer(1)->defaultValue(0)->after('enabled_cabinet'));
        $this->execute('UPDATE tbl_taxi_tariff SET `enabled_hospital` = `enabled_site`');
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'enabled_cabinet');
    }
}
