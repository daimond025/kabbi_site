<?php

use yii\db\Schema;
use yii\db\Migration;

class m151005_121339_user_position extends Migration
{
    public function up()
    {
        $this->insert('{{%user_position}}', ['name' => 'Branch manager']);
    }

    public function down()
    {
        echo "m151005_121339_user_position cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
