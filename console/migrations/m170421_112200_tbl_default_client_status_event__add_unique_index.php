<?php

use yii\db\Migration;

class m170421_112200_tbl_default_client_status_event__add_unique_index extends Migration
{
    const TABLE_DEFAULT_CLIENT_STATUS_EVENT = '{{%default_client_status_event}}';
    const UNIQUE_INDEX = 'ui_default_client_status_event';

    public function up()
    {
        $this->createIndex(
            self::UNIQUE_INDEX,
            self::TABLE_DEFAULT_CLIENT_STATUS_EVENT,
            ['status_id', 'group', 'position_id'],
            true);
    }

    public function down()
    {
        $this->dropIndex(self::UNIQUE_INDEX, self::TABLE_DEFAULT_CLIENT_STATUS_EVENT);
    }

}
