<?php

use yii\db\Migration;

class m200817_153405_order_table_add_edit_param extends Migration
{
    const TABLE_NAME = '{{%order}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'edit',
            $this->integer()->defaultValue(0)->after('order_number')
        );

        return true;
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'edit');
        return true;
    }
}
