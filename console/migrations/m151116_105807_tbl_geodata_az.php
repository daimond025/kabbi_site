<?php

use yii\db\Schema;
use yii\db\Migration;

class m151116_105807_tbl_geodata_az extends Migration
{

    const TABLE_NAME = '{{%geodata_az}}';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'        => $this->primaryKey(),
            'city_az'   => $this->text(50),
            'city_en'   => $this->text(50),
            'city_ru'   => $this->text(50),
            'street_az' => $this->text(100),
            'street_en' => $this->text(100),
            'street_ru' => $this->text(100),
            'label_az'  => $this->text(100),
            'label_en'  => $this->text(100),
            'label_ru'  => $this->text(100),
            'type'      => $this->text(50),
            'lat'       => $this->double(0),
            'lon'       => $this->double(0),
            'weight'    => $this->integer(10),
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
        echo self::TABLE_NAME . ' successfully dropped';
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
