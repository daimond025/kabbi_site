<?php

use yii\db\Migration;

class m170228_062747_tbl_tariff_commission__change_table_structure extends Migration
{
    const KIND_FIXED = 'FIXED';
    const KIND_CASH = 'CASH';
    const KIND_CARD = 'CARD';
    const KIND_PERSONAL_ACCOUNT = 'PERSONAL_ACCOUNT';
    const KIND_CORP_BALANCE = 'CORP_BALANCE';
    const KIND_BORDER = 'BORDER';

    const NEW_KINDS = [
        self::KIND_CASH,
        self::KIND_CARD,
        self::KIND_PERSONAL_ACCOUNT,
        self::KIND_CORP_BALANCE,
        self::KIND_BORDER,
    ];

    const TABLE_TARIFF_COMMISSION = '{{%tariff_commission}}';
    const TABLE_WORKER_OPTION_TARIFF = '{{%worker_option_tariff}}';

    const COLUMN_ACTIVE_DATE = 'active_date';
    const COLUMN_INTERVAL_COMMISSION = 'interval_commission';
    const COLUMN_VALUE = 'value';
    const COLUMN_KIND = 'kind';
    const COLUMN_TAX = 'tax';

    const SQL_TARIFF_OPTIONS_WITH_INTERVAL_COMMISSION =
        'select `option_id` from `tbl_tariff_commission` group by `option_id` having count(*) > 1';

    const SQL_TARIFF_COMMISSION_BY_OPTION_ID =
        'select `option_id`, `type`, `value`, `from`, `to` from `tbl_tariff_commission` where `option_id` = :optionId';

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    private function getTariffOptionIdsWithIntervalCommission()
    {
        $optionIds = $this->getDb()
            ->createCommand(self::SQL_TARIFF_OPTIONS_WITH_INTERVAL_COMMISSION)->queryColumn();

        return empty($optionIds) ? [] : $optionIds;
    }

    private function getTariffCommissions($optionId)
    {
        return $this->getDb()
            ->createCommand(self::SQL_TARIFF_COMMISSION_BY_OPTION_ID, ['optionId' => $optionId])->queryAll();
    }

    private function removeIntervalCommissionFromNewKinds()
    {
        $optionIds = $this->getTariffOptionIdsWithIntervalCommission();

        foreach ($optionIds as $optionId) {
            $this->update(self::TABLE_TARIFF_COMMISSION,
                ['kind' => self::KIND_FIXED],
                ['option_id' => $optionId, 'kind' => self::KIND_CASH]);
            $this->delete(self::TABLE_TARIFF_COMMISSION,
                ['option_id' => $optionId, 'kind' => self::NEW_KINDS]);
        }
    }

    private function setupIntervalCommissionForNewKinds()
    {
        $optionIds = $this->getTariffOptionIdsWithIntervalCommission();

        foreach ($optionIds as $optionId) {
            $commissions = $this->getTariffCommissions($optionId);
            foreach (self::NEW_KINDS as $kind) {
                $this->batchInsert(self::TABLE_TARIFF_COMMISSION,
                    ['option_id', 'type', 'value', 'from', 'to', 'kind'],
                    array_map(function ($commission) use ($kind) {
                        $result   = array_values($commission);
                        $result[] = $kind;

                        return $result;
                    }, $commissions));
            }
            $this->delete(self::TABLE_TARIFF_COMMISSION,
                ['option_id' => $optionId, 'kind' => self::KIND_FIXED]);
        }
    }


    public function up()
    {
        $this->alterColumn(self::TABLE_TARIFF_COMMISSION, self::COLUMN_VALUE, $this->decimal(12, 2));
        $this->addColumn(self::TABLE_TARIFF_COMMISSION, self::COLUMN_KIND,
            "ENUM('FIXED','CASH','CARD','PERSONAL_ACCOUNT','CORP_BALANCE','BORDER') NOT NULL DEFAULT 'FIXED'");
        $this->addColumn(self::TABLE_TARIFF_COMMISSION, self::COLUMN_TAX, $this->decimal(12, 2));

        $this->dropColumn(self::TABLE_WORKER_OPTION_TARIFF, self::COLUMN_INTERVAL_COMMISSION);

        $this->setupIntervalCommissionForNewKinds();
    }


    public function down()
    {
        $this->removeIntervalCommissionFromNewKinds();

        $this->dropColumn(self::TABLE_TARIFF_COMMISSION, self::COLUMN_KIND);
        $this->dropColumn(self::TABLE_TARIFF_COMMISSION, self::COLUMN_TAX);
        $this->alterColumn(self::TABLE_TARIFF_COMMISSION, self::COLUMN_VALUE,
            $this->decimal(12, 2)->notNull()->defaultValue(0));

        $this->addColumn(
            self::TABLE_WORKER_OPTION_TARIFF,
            self::COLUMN_INTERVAL_COMMISSION,
            $this->integer()->after(self::COLUMN_ACTIVE_DATE)->defaultValue(0)
        );

        $this->update(
            self::TABLE_WORKER_OPTION_TARIFF,
            [self::COLUMN_INTERVAL_COMMISSION => 1],
            ['option_id' => $this->getTariffOptionIdsWithIntervalCommission()]
        );
    }

}
