<?php

use yii\db\Migration;

class m180319_121809_pk_import_city extends Migration
{
    const TABLE_NAME = '{{%worker_group_can_view_client_tariff}}';
    const PK_NAME    = 'PK1';

    public function safeUp()
    {
        $this->addColumn(
            self::TABLE_NAME,
            'id',
            'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY'
        );
        return true;
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'id');
        return true;
    }


}
