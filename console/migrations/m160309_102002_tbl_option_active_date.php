<?php

use yii\db\Migration;

class m160309_102002_tbl_option_active_date extends Migration
{
    const TABLE_NAME = '{{%option_active_date}}';

    public function up()
    {
        $this->alterColumn(self::TABLE_NAME, 'active_date', $this->string(35));
    }

    public function down()
    {
        $this->alterColumn(self::TABLE_NAME, 'active_date', $this->string(15));
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
