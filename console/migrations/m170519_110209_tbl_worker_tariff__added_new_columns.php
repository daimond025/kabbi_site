<?php

use yii\db\Migration;

class m170519_110209_tbl_worker_tariff__added_new_columns extends Migration
{
    const TABLE_WORKER_TARIFF = '{{%worker_tariff}}';
    const COLUMN_PERIOD_TYPE = 'period_type';
    const COLUMN_PERIOD = 'period';
    const COLUMN_COST = 'cost';

    const SQL_FILL_COLUMNS = <<<SQL
UPDATE tbl_worker_tariff t
JOIN
(SELECT tariff_id, period_type, period, cost
FROM tbl_worker_option_tariff
WHERE tariff_type = 'CURRENT'
GROUP BY tariff_id) o ON t.tariff_id = o.tariff_id
SET t.period_type = o.period_type, t.period = o.period, t.cost = o.cost
SQL;

    public function up()
    {
        $this->addColumn(self::TABLE_WORKER_TARIFF, self::COLUMN_PERIOD_TYPE, "ENUM('INTERVAL','HOURS')");
        $this->addColumn(self::TABLE_WORKER_TARIFF, self::COLUMN_PERIOD, $this->string(45));
        $this->addColumn(self::TABLE_WORKER_TARIFF, self::COLUMN_COST,
            $this->decimal(12, 2)->defaultValue(0)->notNull());

        $this->getDb()->createCommand(self::SQL_FILL_COLUMNS)->execute();

        $this->alterColumn(self::TABLE_WORKER_TARIFF, self::COLUMN_PERIOD_TYPE, "ENUM('INTERVAL','HOURS') NOT NULL");
        $this->alterColumn(self::TABLE_WORKER_TARIFF, self::COLUMN_PERIOD, $this->string(45)->notNull());
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_WORKER_TARIFF, self::COLUMN_PERIOD_TYPE);
        $this->dropColumn(self::TABLE_WORKER_TARIFF, self::COLUMN_PERIOD);
        $this->dropColumn(self::TABLE_WORKER_TARIFF, self::COLUMN_COST);
    }

}
