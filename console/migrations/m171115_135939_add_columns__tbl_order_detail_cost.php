<?php

use yii\db\Migration;

class m171115_135939_add_columns__tbl_order_detail_cost extends Migration
{

    const TABLE_NAME = '{{%order_detail_cost}}';

    const COLUMN_PROMO_DISCOUNT_VALUE = 'promo_discount_value';
    const COLUMN_PROMO_DISCOUNT_PERCENT = 'promo_discount_percent';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_PROMO_DISCOUNT_VALUE, $this->string(255));

        $this->addColumn(self::TABLE_NAME, self::COLUMN_PROMO_DISCOUNT_PERCENT, $this->string(255));
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_PROMO_DISCOUNT_PERCENT);

        $this->dropColumn(self::TABLE_NAME, self::COLUMN_PROMO_DISCOUNT_VALUE);
    }
}
