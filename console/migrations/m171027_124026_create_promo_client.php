<?php

use yii\db\Migration;

class m171027_124026_create_promo_client extends Migration
{
    const TABLE_NAME = '{{%promo_client}}';

    const COLUMN_CLIENT_ID = 'client_id';
    const COLUMN_CODE_ID = 'code_id';
    const COLUMN_STATUS_CODE_ID = 'status_code_id';

    const FK_PROMO_CLIENT__STATUS_CODE_ID = 'fk_promo_client__status_code_id';
    const FK_PROMO_CLIENT__CLIENT_ID = 'fk_promo_client__client_id';
    const FK_PROMO_CLIENT__CODE_ID = 'fk_promo_client__code_id';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            self::COLUMN_CLIENT_ID      => $this->integer()->unsigned(),
            self::COLUMN_CODE_ID        => $this->integer()->unsigned(),
            self::COLUMN_STATUS_CODE_ID => $this->integer()->notNull(),
            'status_time'               => $this->integer()->unsigned(),
        ]);


        $this->addPrimaryKey(
            'pk_promo_client', self::TABLE_NAME,
            [self::COLUMN_CLIENT_ID, self::COLUMN_CODE_ID]
        );

        $this->addForeignKey(self::FK_PROMO_CLIENT__CLIENT_ID, self::TABLE_NAME,
            self::COLUMN_CLIENT_ID, '{{%client}}', self::COLUMN_CLIENT_ID, 'CASCADE', 'CASCADE');

        $this->addForeignKey(self::FK_PROMO_CLIENT__CODE_ID, self::TABLE_NAME,
            self::COLUMN_CODE_ID, '{{%promo_code}}', self::COLUMN_CODE_ID, 'CASCADE', 'CASCADE');

        $this->addForeignKey(self::FK_PROMO_CLIENT__STATUS_CODE_ID, self::TABLE_NAME,
            self::COLUMN_STATUS_CODE_ID, '{{%promo_status_code}}', self::COLUMN_STATUS_CODE_ID, 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_PROMO_CLIENT__STATUS_CODE_ID, self::TABLE_NAME);
        $this->dropForeignKey(self::FK_PROMO_CLIENT__CODE_ID, self::TABLE_NAME);
        $this->dropForeignKey(self::FK_PROMO_CLIENT__CLIENT_ID, self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
    }

}
