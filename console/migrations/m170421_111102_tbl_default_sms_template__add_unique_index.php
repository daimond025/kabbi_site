<?php

use yii\db\Migration;

class m170421_111102_tbl_default_sms_template__add_unique_index extends Migration
{
    const TABLE_DEFAULT_SMS_TEMPLATE = '{{%default_sms_template}}';
    const UNIQUE_INDEX = 'ui_default_sms_template';

    public function up()
    {
        $this->createIndex(
            self::UNIQUE_INDEX,
            self::TABLE_DEFAULT_SMS_TEMPLATE,
            ['type', 'position_id'],
            true);
    }

    public function down()
    {
        $this->dropIndex(self::UNIQUE_INDEX, self::TABLE_DEFAULT_SMS_TEMPLATE);
    }

}
