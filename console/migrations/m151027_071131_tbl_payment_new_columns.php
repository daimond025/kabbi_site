<?php

use yii\db\Schema;
use yii\db\Migration;

class m151027_071131_tbl_payment_new_columns extends Migration
{
    const TABLE_NAME = '{{%tenant_payment}}';
    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'period', $this->integer(2)->notNull());
        $this->addColumn(self::TABLE_NAME, 'payment_item', $this->string(48)->notNull());
        $this->addColumn(self::TABLE_NAME, 'expiration_date', $this->dateTime()->notNull());
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'period');
        $this->dropColumn(self::TABLE_NAME, 'payment_item');
        $this->dropColumn(self::TABLE_NAME, 'expiration_date');
        echo 'Columns "period", "payment_item" and "expiration_date" have dropped from "tenant_payment" table';
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
