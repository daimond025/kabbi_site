<?php

use yii\db\Migration;

class m180219_112212_addUpUpExchange extends Migration
{
    const TABLE_NAME = '{{%exchange_program}}';


    public function safeUp()
    {
        $this->insert(self::TABLE_NAME, [
            'exchange_program_id' => '1',
            'name'                => 'UP&UP',
            'created_at'          => time(),
            'updated_at'          => time(),
            'sort'                => '100',
            'block'               => '0',
        ]);

        return true;
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_NAME, 'exchange_program_id = :exchange_program_id', [
            'exchange_program_id' => 1,
        ]);

        return true;
    }


}
