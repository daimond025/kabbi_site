<?php

use yii\db\Migration;

class m180116_104046_change_table__mail_not_uniq extends Migration
{

    const TABLE_NAME = '{{%user}}';

    const COLUMN_NAME = 'email';

    const INDEX_UNIQUE_NAME = 'email_UNIQUE';

    protected $counterEmails = [];

    public function safeUp()
    {
        $this->dropIndex(self::INDEX_UNIQUE_NAME, self::TABLE_NAME);
        $this->rollbackRenameDuplicatedEmails();
        $this->changeLengthEmail(255);
    }

    protected function rollbackRenameDuplicatedEmails()
    {
        $changedEmails = $this->getChangedEmails();

        foreach ($changedEmails as $email) {
            $this->saveEmail($this->createdNormalEmail($email));
        }
    }


    public function safeDown()
    {
        $this->renameDuplicatedEmails();
        $this->createIndex(self::INDEX_UNIQUE_NAME, self::TABLE_NAME, self::COLUMN_NAME, true);
    }

    protected function renameDuplicatedEmails()
    {
        $duplicatedEmails = $this->getDuplicatedEmails();

        foreach($duplicatedEmails as $email) {
            $this->saveEmail($this->createNewNameEmail($email));
        }

    }


    protected function changeLengthEmail($length)
    {
        $sql = sprintf("ALTER TABLE `tbl_user` MODIFY `email` VARCHAR(%d);", $length);

        $this->getDb()->createCommand($sql)->execute();
    }


    protected function createdNormalEmail(array $email)
    {
        $email['newEmail'] = preg_replace('/\(\d+\)/', '', $email['email']);

        return $email;
    }

    protected function getChangedEmails()
    {
        $sql = 'SELECT email, tenant_id FROM tbl_user WHERE email LIKE "(%"';

        return $this->getDb()->createCommand($sql)->queryAll();
    }


    protected function saveEmail(array $email)
    {
        $this->update(self::TABLE_NAME, [
            self::COLUMN_NAME => $email['newEmail']
        ], [
            self::COLUMN_NAME => $email['email'],
            'tenant_id' => $email['tenant_id']
        ]);
    }

    protected function createNewNameEmail(array $email)
    {
        if(array_key_exists($email['email'], $this->counterEmails)) {
            $this->counterEmails[$email['email']] = $this->counterEmails[$email['email']] + 1;
        } else {
            $this->counterEmails[$email['email']] = 2;
        }

        $email['newEmail'] = sprintf('(%d)%s', $this->counterEmails[$email['email']], $email['email']);

        return $email;

    }

    protected function getDuplicatedEmails()
    {
        $sql = <<<SQL
SELECT `email`, `tenant_id` FROM `tbl_user` WHERE `email` IN (
    SELECT `email` FROM `tbl_user` GROUP BY `email` HAVING count(*)>1
);
SQL;

        $allDuplicateEmails = $this->getDb()->createCommand($sql)->queryAll();

        return $this->prepareUniqueEmails($allDuplicateEmails);

    }

    protected function prepareUniqueEmails($allDuplicateEmails)
    {
        $arrayUniqueEmails = [];
        $duplicatedEmails = [];
        foreach ($allDuplicateEmails as $email) {
            if (!in_array($email['email'], $arrayUniqueEmails)) {
                $arrayUniqueEmails[] = $email['email'];
            } else {
                $duplicatedEmails[] = $email;
            }
        }

        return $duplicatedEmails;
    }
}
