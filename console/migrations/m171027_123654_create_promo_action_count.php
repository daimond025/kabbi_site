<?php

use yii\db\Migration;

class m171027_123654_create_promo_action_count extends Migration
{
    const TABLE_NAME = '{{%promo_action_count}}';

    const COLUMN_ACTION_COUNT_ID = 'action_count_id';
    const COLUMN_NAME = 'name';

    const DEFAULT_VALUE = [
        ['Once'],
        ['For each order'],
    ];

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            self::COLUMN_ACTION_COUNT_ID => $this->primaryKey(),
            self::COLUMN_NAME            => $this->string(255)->notNull(),
        ]);

        $this->batchInsert(
            self::TABLE_NAME,
            [self::COLUMN_NAME],
            self::DEFAULT_VALUE
        );
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }

}
