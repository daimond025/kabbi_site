<?php

use yii\db\Migration;

class m170515_175340_tbl_referral__fixed_column_types extends Migration
{
    const TABLE_REFERRAL = '{{%referral}}';
    const COLUMN_INVITING_VALUE = 'inviting_value';
    const COLUMN_INVITED_VALUE = 'invited_value';
    const COLUMN_MIN_PRICE = 'min_price';

    public function up()
    {
        $this->alterColumn(self::TABLE_REFERRAL, self::COLUMN_INVITING_VALUE,
            $this->decimal(20, 5)->notNull()->defaultValue(0)->comment('Бонус рефералу (кого пригласили)'));
        $this->alterColumn(self::TABLE_REFERRAL, self::COLUMN_INVITED_VALUE,
            $this->decimal(20, 5)->notNull()->defaultValue(0)->comment('Бонус рефереру (кто пригласил)'));
        $this->alterColumn(self::TABLE_REFERRAL, self::COLUMN_MIN_PRICE, $this->decimal(20, 5));
    }

    public function down()
    {
        $this->alterColumn(self::TABLE_REFERRAL, self::COLUMN_INVITING_VALUE,
            $this->integer()->notNull()->defaultValue(0)->comment('Бонус рефералу (кого пригласили)'));
        $this->alterColumn(self::TABLE_REFERRAL, self::COLUMN_INVITED_VALUE,
            $this->integer()->notNull()->defaultValue(0)->comment('Бонус рефереру (кто пригласил)'));
        $this->alterColumn(self::TABLE_REFERRAL, self::COLUMN_MIN_PRICE, $this->float());
    }

}
