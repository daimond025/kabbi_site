<?php

use yii\db\Schema;
use yii\db\Migration;

class m160205_062405_tbl_post__remove_columns extends Migration
{
    const TABLE_NAME = '{{%post}}';

    public function up()
    {
        $this->dropColumn(self::TABLE_NAME, 'meta_title');
        $this->dropColumn(self::TABLE_NAME, 'meta_keywords');
    }

    public function down()
    {
        $this->addColumn(self::TABLE_NAME, 'meta_title', 'VARCHAR(255)');
        $this->addColumn(self::TABLE_NAME, 'meta_keywords', 'VARCHAR(255)');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
