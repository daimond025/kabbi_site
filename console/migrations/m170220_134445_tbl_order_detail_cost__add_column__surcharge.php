<?php

use yii\db\Migration;

class m170220_134445_tbl_order_detail_cost__add_column__surcharge extends Migration
{
    const TABLE_ORDER_DETAIL_COST = '{{%order_detail_cost}}';
    const COLUMN_SURCHARGE = 'surcharge';

    public function up()
    {
        $this->addColumn(self::TABLE_ORDER_DETAIL_COST, self::COLUMN_SURCHARGE,
            $this->string(45)->comment('Доплата за заказ'));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_ORDER_DETAIL_COST, self::COLUMN_SURCHARGE);
    }

}
