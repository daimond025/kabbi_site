<?php

use console\components\mysqlOnlineSchemaChange\MysqlOnlineSchemaChangeTrait;
use yii\db\Migration;

class m180705_084650_tbl_order_add_index extends Migration
{
    use MysqlOnlineSchemaChangeTrait;

    public function up()
    {
        return $this->schemaChange('tbl_order',
            'ADD INDEX tbi_order_tenant_id_client_id_create_time_idx (tenant_id, client_id, create_time)');
    }

    public function down()
    {
        return $this->schemaChange('tbl_order', 'DROP INDEX tbi_order_tenant_id_client_id_create_time_idx');
    }

}
