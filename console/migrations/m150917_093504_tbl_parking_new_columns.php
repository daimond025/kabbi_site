<?php

use yii\db\Schema;
use yii\db\Migration;

class m150917_093504_tbl_parking_new_columns extends Migration
{
    const TABLE_NAME = '{{%parking}}';
    const IN_DISTR = 'in_district';
    const OUT_DISTR = 'out_district';
    const IN_DISTR_TYPE = 'in_distr_coefficient_type';
    const OUT_DISTR_TYPE = 'out_distr_coefficient_type';
    
    public function up()
    {
        $this->addColumn(self::TABLE_NAME, self::IN_DISTR, Schema::TYPE_SMALLINT . '(3)');
        $this->addColumn(self::TABLE_NAME, self::OUT_DISTR, Schema::TYPE_SMALLINT . '(3)');
        $this->addColumn(self::TABLE_NAME, self::IN_DISTR_TYPE, Schema::TYPE_STRING . '(45)');
        $this->addColumn(self::TABLE_NAME, self::OUT_DISTR_TYPE, Schema::TYPE_STRING . '(45)');
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, self::IN_DISTR);
        $this->dropColumn(self::TABLE_NAME, self::OUT_DISTR);
        $this->dropColumn(self::TABLE_NAME, self::IN_DISTR_TYPE);
        $this->dropColumn(self::TABLE_NAME, self::OUT_DISTR_TYPE);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
