<?php

use yii\db\Schema;
use yii\db\Migration;

class m151207_061325_tbl_currency_new_currencies extends Migration
{
    const TABLE_NAME = '{{%currency}}';

    public function up() {
        $this->insert(self::TABLE_NAME, ['name' => 'Euro', 'code' => 'EUR', 'symbol' => '‎€', 'type_id' => 1]);
        $this->insert(self::TABLE_NAME, ['name' => 'Kazakhstan tenge', 'code' => 'KZT', 'symbol' => '₸', 'type_id' => 1]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME, 'code = "EUR"');
        $this->delete(self::TABLE_NAME, 'code = "KZT"');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
