<?php

use yii\db\Migration;

class m160423_164420_tbl_user__add_column extends Migration
{
    const TABLE_USER = '{{%user}}';
    const COLUMN_LAST_SESSION_ID = 'last_session_id';

    public function up()
    {
        $this->addColumn(self::TABLE_USER, self::COLUMN_LAST_SESSION_ID, $this->string());
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_USER, self::COLUMN_LAST_SESSION_ID);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
