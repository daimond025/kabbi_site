<?php

use yii\db\Migration;

class m171027_123953_create_promo_status_code extends Migration
{

    const TABLE_NAME = '{{%promo_status_code}}';

    const COLUMN_STATUS_CODE_ID = 'status_code_id';
    const COLUMN_STATUS_LABEL = 'status_label';

    const DEFAULT_VALUE = [
        ['New'],
        ['Activated'],
        ['Used'],
        ['Expired'],
    ];


    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            self::COLUMN_STATUS_CODE_ID => $this->primaryKey(),
            self::COLUMN_STATUS_LABEL   => $this->string(255)->notNull(),
        ]);


        $this->batchInsert(
            self::TABLE_NAME,
            [self::COLUMN_STATUS_LABEL],
            self::DEFAULT_VALUE
        );
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
