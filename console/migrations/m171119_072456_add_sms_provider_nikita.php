<?php

use yii\db\Migration;

class m171119_072456_add_sms_provider_nikita extends Migration
{
    const TABLE_NAME = '{{%sms_server}}';

    const COLUMN_SERVER_ID = 23;

    public function up()
    {
        $this->insert(self::TABLE_NAME,[
            'server_id' => self::COLUMN_SERVER_ID,
            'name' =>  'Nikita.am',
            'host' => 'http://nikita.am/',
            'active' => '1',
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME,['server_id' => self::COLUMN_SERVER_ID]);
    }
}
