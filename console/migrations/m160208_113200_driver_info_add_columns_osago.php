<?php

use yii\db\Schema;
use yii\db\Migration;

class m160208_113200_driver_info_add_columns_osago extends Migration
{
    
    const TABLE_NAME = '{{%driver_info}}';
    
    public function up()
    {
        $this->addColumn(self::TABLE_NAME,'osago_series','VARCHAR(3)');
        $this->addColumn(self::TABLE_NAME,'osago_number','INTEGER(11)');
        $this->addColumn(self::TABLE_NAME,'osago_start_date','TIMESTAMP');
        $this->addColumn(self::TABLE_NAME,'osago_end_date','TIMESTAMP');
        
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'osago_series');
        $this->dropColumn(self::TABLE_NAME, 'osago_number');
        $this->dropColumn(self::TABLE_NAME, 'osago_start_date');
        $this->dropColumn(self::TABLE_NAME, 'osago_end_date');
        
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
