<?php

use yii\db\Migration;
use common\modules\tenant\models\DefaultSettings;

class m171026_115643_add_default_setting_whose_callsign_on_map extends Migration
{
    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const TABLE_TENANT_SETTING = '{{%tenant_setting}}';

    const SETTING_NAME_WHOSE_CALLSIGN_ON_MAP = 'WHOSE_CALLSIGN_ON_MAP';
    const SETTING_VALUE_WHOSE_CALLSIGN_ON_MAP = 'worker';
    const SETTING_TYPE_WHOSE_CALLSIGN_ON_MAP = 'orders';

    public function safeUp()
    {
        $requirePassword = new DefaultSettings([
            'name'  => self::SETTING_NAME_WHOSE_CALLSIGN_ON_MAP,
            'value' => self::SETTING_VALUE_WHOSE_CALLSIGN_ON_MAP,
            'type'  => self::SETTING_TYPE_WHOSE_CALLSIGN_ON_MAP,
        ]);

        $requirePassword->save();

        return true;
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::SETTING_NAME_WHOSE_CALLSIGN_ON_MAP,
            'type' => self::SETTING_TYPE_WHOSE_CALLSIGN_ON_MAP,
        ]);

        $this->delete(self::TABLE_TENANT_SETTING, [
            'name' => self::SETTING_NAME_WHOSE_CALLSIGN_ON_MAP,
            'type' => self::SETTING_TYPE_WHOSE_CALLSIGN_ON_MAP,
        ]);

        return true;

    }

}
