<?php

use yii\db\Schema;
use yii\db\Migration;

class m160216_182939_tbl_currency__add_columns extends Migration
{
    const TABLE_NAME = '{{%currency}}';
    const COLUMN_NUMBER_CODE = 'numeric_code';
    const COLUMN_MINOR_UNIT  = 'minor_unit';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_NUMBER_CODE, 'INT(3)');
        $this->addColumn(self::TABLE_NAME, self::COLUMN_MINOR_UNIT, 'INT(2)');

        $this->update(self::TABLE_NAME, [
                self::COLUMN_NUMBER_CODE => 810,
                self::COLUMN_MINOR_UNIT  => 2,
            ], 'code = "RUB"');
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_NUMBER_CODE);
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_MINOR_UNIT);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
