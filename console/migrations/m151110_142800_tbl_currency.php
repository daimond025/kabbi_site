<?php

use yii\db\Schema;
use yii\db\Migration;

class m151110_142800_tbl_currency extends Migration
{
    const TABLE_NAME = '{{%currency}}';
    const BONUS_CURRENCY_ID = 5;

    public function up()
    {
        $this->insert(self::TABLE_NAME, [
            'currency_id' => self::BONUS_CURRENCY_ID,
            'type_id' => 2,
            'name' => 'Bonus',
            'code' => 'BONUS',
            'symbol' => 'B',
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME, 'currency_id = :id', [
            'id' => self::BONUS_CURRENCY_ID,
        ]);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
