<?php

use yii\db\Migration;

class m170524_092927_change_type_value_in_taxi_tariff_table extends Migration
{
    const TABLE_NAME = '{{%taxi_tariff}}';
    const TYPE_BASE = 'BASE';
    const TYPE_ALL = 'ALL';

    public function up()
    {
        $this->update(self::TABLE_NAME, ['type' => self::TYPE_ALL], ['type' => self::TYPE_BASE]);

    }

    public function down()
    {

    }
}
