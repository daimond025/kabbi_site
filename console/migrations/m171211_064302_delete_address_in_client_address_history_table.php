<?php

use yii\db\Migration;

class m171211_064302_delete_address_in_client_address_history_table extends Migration
{
    const TABLE_NAME = '{{%client_address_history}}';
    const COLUMN_NAME = 'address';

    public function safeUp()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);
    }

    public function safeDown()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_NAME, $this->text()->notNull());
    }
}
