<?php

use common\modules\tenant\models\TenantHasCity;
use frontend\modules\tenant\models\ClientStatusEvent;
use yii\db\Migration;
use yii\helpers\ArrayHelper;

class m170315_065943_tbl_client_status_event__add_columns extends Migration
{
    const TABLE_DEFAULT_CLIENT_STATUS_EVENT = '{{%default_client_status_event}}';
    const TABLE_CLIENT_STATUS_EVENT = '{{%client_status_event}}';
    const TABLE_POSITION = '{{%position}}';
    const TABLE_CITY = '{{%city}}';

    const COLUMN_POSITION_ID = 'position_id';
    const COLUMN_CITY_ID = 'city_id';

    const FOREIGN_KEY_POSITION_ID = 'tbl_client_status_event__position_id';
    const FOREIGN_KEY_CITY_ID = 'tbl_client_status_event__city_id';

    const DEFAULT_POSITION_ID_VALUE = 1;


    private function getClientStatusEventRecordsGroupedByTenant()
    {
        $records = ClientStatusEvent::find()
            ->asArray()
            ->all();

        return empty($records) ? [] : ArrayHelper::index($records, null, 'tenant_id');
    }

    private function getTenantCityIds($tenantId)
    {
        $cityIds = TenantHasCity::find()
            ->select('city_id')
            ->where(['tenant_id' => $tenantId])
            ->column();

        return empty($cityIds) ? [] : $cityIds;
    }

    private function getClientStatusEventCityIdsGroupedByTenant()
    {
        $records = ClientStatusEvent::find()
            ->select(['tenant_id', 'city_id'])
            ->distinct()
            ->asArray()
            ->all();

        return empty($records) ? [] : ArrayHelper::index($records, null, 'tenant_id');
    }

    public function up()
    {
        $this->addColumn(self::TABLE_CLIENT_STATUS_EVENT, self::COLUMN_POSITION_ID, $this->integer());
        $this->addColumn(self::TABLE_CLIENT_STATUS_EVENT, self::COLUMN_CITY_ID, $this->integer(10)->unsigned());

        $this->update(self::TABLE_CLIENT_STATUS_EVENT, [
            'position_id' => self::DEFAULT_POSITION_ID_VALUE,
        ]);
        $this->alterColumn(self::TABLE_CLIENT_STATUS_EVENT, self::COLUMN_POSITION_ID, $this->integer()->notNull());

        foreach ($this->getClientStatusEventRecordsGroupedByTenant() as $tenantId => $records) {
            $cityIds = $this->getTenantCityIds($tenantId);

            if (!empty($cityIds)) {
                $defaultCityId = array_shift($cityIds);

                if (!empty($records)) {
                    foreach ($records as $record) {
                        $this->update(self::TABLE_CLIENT_STATUS_EVENT, [
                            'city_id' => $defaultCityId,
                        ], [
                            'event_id' => $record['event_id'],
                        ]);

                        foreach ($cityIds as $cityId) {
                            $this->insert(self::TABLE_CLIENT_STATUS_EVENT, [
                                'status_id'   => $record['status_id'],
                                'tenant_id'   => $record['tenant_id'],
                                'group'       => $record['group'],
                                'notice'      => $record['notice'],
                                'position_id' => self::DEFAULT_POSITION_ID_VALUE,
                                'city_id'     => $cityId,
                            ]);
                        }
                    }
                }
            }
        }

        $this->addForeignKey(
            self::FOREIGN_KEY_POSITION_ID, self::TABLE_CLIENT_STATUS_EVENT, self::COLUMN_POSITION_ID,
            self::TABLE_POSITION, self::COLUMN_POSITION_ID, 'RESTRICT', 'RESTRICT');

        $this->addForeignKey(
            self::FOREIGN_KEY_CITY_ID, self::TABLE_CLIENT_STATUS_EVENT, self::COLUMN_CITY_ID,
            self::TABLE_CITY, self::COLUMN_CITY_ID, 'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
        foreach ($this->getClientStatusEventCityIdsGroupedByTenant() as $tenantId => $cities) {
            array_shift($cities);
            $this->delete(self::TABLE_CLIENT_STATUS_EVENT, [
                'tenant_id' => $tenantId,
                'city_id'   => ArrayHelper::getColumn($cities, 'city_id'),
            ]);
        }

        $this->dropForeignKey(self::FOREIGN_KEY_POSITION_ID, self::TABLE_CLIENT_STATUS_EVENT);
        $this->dropForeignKey(self::FOREIGN_KEY_CITY_ID, self::TABLE_CLIENT_STATUS_EVENT);

        $this->dropColumn(self::TABLE_CLIENT_STATUS_EVENT, self::COLUMN_POSITION_ID);
        $this->dropColumn(self::TABLE_CLIENT_STATUS_EVENT, self::COLUMN_CITY_ID);
    }

}
