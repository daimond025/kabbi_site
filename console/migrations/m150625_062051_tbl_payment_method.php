<?php

use yii\db\Schema;
use yii\db\Migration;

class m150625_062051_tbl_payment_method extends Migration
{
    public function up()
    {
        $this->insert('{{%payment_method}}', ['payment' => 'WITHDRAWAL', 'name' => 'Withdrawal']);
    }

    public function down()
    {
        echo "m150625_062051_tbl_payment_method cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
