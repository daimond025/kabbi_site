<?php

use yii\db\Migration;

class m160229_140902_tbl_default_client_status_event extends Migration
{
    public function up()
    {
        $this->insert('{{%default_client_status_event}}', ['status_id' => 40, 'group' => 'CALL', 'notice' => 'nothing']);
        $this->insert('{{%default_client_status_event}}', ['status_id' => 40, 'group' => 'APP', 'notice' => 'nothing']);
        $this->insert('{{%default_client_status_event}}', ['status_id' => 40, 'group' => 'WEB', 'notice' => 'nothing']);
    }

    public function down()
    {
        echo "m160229_140902_tbl_default_client_status_event cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
