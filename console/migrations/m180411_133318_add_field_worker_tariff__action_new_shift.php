<?php

use yii\db\Migration;

class m180411_133318_add_field_worker_tariff__action_new_shift extends Migration
{

    const TABLE_NAME = '{{%worker_tariff}}';

    const COLUMN_NAME = 'action_new_shift';

    public function safeUp()
    {
        $this->addColumn(
            self::TABLE_NAME,
            self::COLUMN_NAME,
            'ENUM("CONTINUE_PERIOD", "NEW_PERIOD")
            COMMENT "Действие при открытии новой смены: 
            CONTINUE PERIOD - продолжить незавершенный период 
            NEW PERIOD - начинается новый период"'
        );
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);
    }
}
