<?php

use yii\db\Migration;

class m170213_102539_tbl_tenant_setting__fill_default_setting_values extends Migration
{
    const TABLE_TENANT_SETTING = '{{%tenant_setting}}';
    const SETTINGS = ['SHOW_URGENT_ORDER_TIME', 'SHOW_ESTIMATION', 'ALLOW_EDIT_ORDER'];

    public function up()
    {
        $this->update(self::TABLE_TENANT_SETTING, ['value' => 1], ['name' => self::SETTINGS]);
    }

    public function down()
    {
    }

}
