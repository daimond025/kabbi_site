<?php

use yii\db\Schema;
use yii\db\Migration;

class m151015_100637_tbl_tenant_tariff extends Migration
{
    const TABLE_NAME = '{{%tenant_admin_tariff}}';
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'tariff_id' => $this->primaryKey()->notNull(),
            'name' => $this->string(32)->notNull(),
            'block' => $this->integer(1),
            'price' => $this->integer(5)->notNull(),
            'create_date' => $this->dateTime()->notNull(),
            'stuff_quantity' => $this->integer(4)->notNull(),
            'driver_quantity' => $this->integer(5)->notNull(),
        ]);
    }

    public function down()
    {
        echo "m151015_100637_tbl_tenant_tariff cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
