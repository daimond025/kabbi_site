<?php

use yii\db\Migration;

class m160927_130633_ext_columns_on_tenant_has_sms extends Migration
{
    const TABLE_NAME = '{{%tenant_has_sms}}';

    public function up()
    {
        $this->alterColumn(self::TABLE_NAME, 'login', $this->string(100)->notNull());
        $this->alterColumn(self::TABLE_NAME, 'password', $this->string(100)->notNull());
    }

    public function down()
    {
        $this->alterColumn(self::TABLE_NAME, 'login', $this->string(45)->notNull());
        $this->alterColumn(self::TABLE_NAME, 'password', $this->string(45)->notNull());
    }
}
