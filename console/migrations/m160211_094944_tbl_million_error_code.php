<?php

use yii\db\Schema;
use yii\db\Migration;

class m160211_094944_tbl_million_error_code extends Migration
{
    
    const TABLE_NAME = '{{%million_error_code}}';
    
    
    public function up()
    {
        $this->createTable(self::TABLE_NAME,[
            'log_id' => $this->primaryKey(),
            'code' => $this->string(4),
            'description' => $this->text(),
        ]);
        
        $this->insert(self::TABLE_NAME,[
            'code' => '00',
            'description' => 'SUCCESS_RESPONSE'
        ]);
        $this->insert(self::TABLE_NAME,[
            'code' => '01',
            'description' => 'PHONE_NOT_FOUND_RESPONSE'
        ]);
        $this->insert(self::TABLE_NAME,[
            'code' => '02',
            'description' => 'HELD_PAYMENT_RESPONSE'
        ]);
        $this->insert(self::TABLE_NAME,[
            'code' => '03',
            'description' => 'TECHNICAL_ERROR_RESPONSE'
        ]);
        $this->insert(self::TABLE_NAME,[
            'code' => '04',
            'description' => 'SYSTEM_ERROR_RESPONSE'
        ]);
        $this->insert(self::TABLE_NAME,[
            'code' => '99',
            'description' => 'NOT_ENOUGH_DATA_RESPONSE'
        ]);
        
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}

