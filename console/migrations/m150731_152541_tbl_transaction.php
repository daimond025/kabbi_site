<?php

use yii\db\Schema;
use yii\db\Migration;

class m150731_152541_tbl_transaction extends Migration
{
    public function up()
    {
        $this->addColumn('{{%transaction}}', 'terminal_transact_number', Schema::TYPE_STRING);
        $this->addColumn('{{%transaction}}', 'terminal_transact_date', Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('{{%transaction}}', 'terminal_transact_number');
        $this->dropColumn('{{%transaction}}', 'terminal_transact_date');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
