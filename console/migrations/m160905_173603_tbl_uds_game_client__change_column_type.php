<?php

use yii\db\Migration;

class m160905_173603_tbl_uds_game_client__change_column_type extends Migration
{
    const TABLE_UDS_GAME_CLIENT = '{{%uds_game_client}}';
    const COLUMN_PROFILE_ID = 'profile_id';

    public function up()
    {
        $this->alterColumn(self::TABLE_UDS_GAME_CLIENT, self::COLUMN_PROFILE_ID, 'VARCHAR(20) NOT NULL');
    }

    public function down()
    {
    }

}
