<?php

use yii\db\Migration;

class m160330_133908_remove_cascading_updates_from_tenant_tariff_tables extends Migration
{
    public $paymentSql = <<<SQL
ALTER TABLE `tbl_payment` DROP FOREIGN KEY `fk_tbl_payment__currency_id`;
ALTER TABLE `tbl_payment` ADD CONSTRAINT `fk_tbl_payment__currency_id` FOREIGN KEY (`currency_id`) REFERENCES `tbl_currency`(`currency_id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `tbl_payment` DROP FOREIGN KEY `fk_tbl_payment__tenant_id`;
ALTER TABLE `tbl_payment` ADD CONSTRAINT `fk_tbl_payment__tenant_id` FOREIGN KEY (`tenant_id`) REFERENCES `tbl_tenant`(`tenant_id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
SQL;

    public $paymentForTariffSql = <<<SQL
ALTER TABLE `tbl_payment_for_tariff` DROP FOREIGN KEY `fk_tbl_payment_for_tariff__currency_id`;
ALTER TABLE `tbl_payment_for_tariff` ADD CONSTRAINT `fk_tbl_payment_for_tariff__currency_id` FOREIGN KEY (`currency_id`) REFERENCES `tbl_currency`(`currency_id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `tbl_payment_for_tariff` DROP FOREIGN KEY `fk_tbl_payment_for_tariff__tariff_id`;
ALTER TABLE `tbl_payment_for_tariff` ADD CONSTRAINT `fk_tbl_payment_for_tariff__tariff_id` FOREIGN KEY (`tariff_id`) REFERENCES `tbl_tariff`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
SQL;

    public $paymentForAdditionalOptionSql = <<<SQL
ALTER TABLE `tbl_payment_for_additional_option` DROP FOREIGN KEY `fk_tbl_payment_for_additional_option__currency_id`;
ALTER TABLE `tbl_payment_for_additional_option` ADD CONSTRAINT `fk_tbl_payment_for_additional_option__currency_id` FOREIGN KEY (`currency_id`) REFERENCES `tbl_currency`(`currency_id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `tbl_payment_for_additional_option` DROP FOREIGN KEY `fk_tbl_payment_for_additional_option__option_id`;
ALTER TABLE `tbl_payment_for_additional_option` ADD CONSTRAINT `fk_tbl_payment_for_additional_option__option_id` FOREIGN KEY (`option_id`) REFERENCES `tbl_tariff_additional_option`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
SQL;

    public $paymentForOneTimeServiceSql = <<<SQL
ALTER TABLE `tbl_payment_for_one_time_service` DROP FOREIGN KEY `fk_tbl_payment_for_one_time_service__currency_id`;
ALTER TABLE `tbl_payment_for_one_time_service` ADD CONSTRAINT `fk_tbl_payment_for_one_time_service__currency_id` FOREIGN KEY (`currency_id`) REFERENCES `tbl_currency`(`currency_id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `tbl_payment_for_one_time_service` DROP FOREIGN KEY `fk_tbl_payment_for_one_time_service__service_id`;
ALTER TABLE `tbl_payment_for_one_time_service` ADD CONSTRAINT `fk_tbl_payment_for_one_time_service__service_id` FOREIGN KEY (`service_id`) REFERENCES `tbl_one_time_service`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
SQL;

    public function up()
    {
        $this->execute($this->paymentSql);
        $this->execute($this->paymentForTariffSql);
        $this->execute($this->paymentForAdditionalOptionSql);
        $this->execute($this->paymentForOneTimeServiceSql);
    }

    public function down()
    {
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
