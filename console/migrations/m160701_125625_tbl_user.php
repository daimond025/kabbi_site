<?php

use yii\db\Migration;

class m160701_125625_tbl_user extends Migration
{
    const TABLE_NAME = '{{%user}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'md5_password', $this->string(32));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'md5_password');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
