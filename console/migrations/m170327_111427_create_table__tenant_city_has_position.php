<?php

use yii\db\Migration;

class m170327_111427_create_table__tenant_city_has_position extends Migration
{
    const TABLE_TENANT_CITY_HAS_POSITION = '{{%tenant_city_has_position}}';
    const TABLE_TENANT = '{{%tenant}}';
    const TABLE_CITY = '{{%city}}';
    const TABLE_POSITION = '{{%position}}';

    const COLUMN_TENANT_ID = 'tenant_id';
    const COLUMN_CITY_ID = 'city_id';
    const COLUMN_POSITION_ID = 'position_id';

    const UNIQUE_INDEX = 'ui_tenant_city_has_position';

    const FOREIGN_KEY_TENANT_ID = 'fk_tenant_city_has_position__tenant_id';
    const FOREIGN_KEY_CITY_ID = 'fk_tenant_city_has_position__city_id';
    const FOREIGN_KEY_POSITION_ID = 'fk_tenant_city_has_position__position_id';


    public function up()
    {
        $this->createTable(self::TABLE_TENANT_CITY_HAS_POSITION, [
            'id'          => $this->primaryKey(),
            'tenant_id'   => $this->integer(10)->unsigned()->notNull(),
            'city_id'     => $this->integer(10)->unsigned()->notNull(),
            'position_id' => $this->integer()->notNull(),
        ]);

        $this->createIndex(self::UNIQUE_INDEX, self::TABLE_TENANT_CITY_HAS_POSITION,
            ['tenant_id', 'city_id', 'position_id'], true);

        $this->addForeignKey(self::FOREIGN_KEY_TENANT_ID, self::TABLE_TENANT_CITY_HAS_POSITION, self::COLUMN_TENANT_ID,
            self::TABLE_TENANT, self::COLUMN_TENANT_ID, 'CASCADE', 'CASCADE');
        $this->addForeignKey(self::FOREIGN_KEY_CITY_ID, self::TABLE_TENANT_CITY_HAS_POSITION, self::COLUMN_CITY_ID,
            self::TABLE_CITY, self::COLUMN_CITY_ID, 'CASCADE', 'CASCADE');
        $this->addForeignKey(self::FOREIGN_KEY_POSITION_ID, self::TABLE_TENANT_CITY_HAS_POSITION,
            self::COLUMN_POSITION_ID,
            self::TABLE_POSITION, self::COLUMN_POSITION_ID, 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey(self::FOREIGN_KEY_TENANT_ID, self::TABLE_TENANT_CITY_HAS_POSITION);
        $this->dropForeignKey(self::FOREIGN_KEY_CITY_ID, self::TABLE_TENANT_CITY_HAS_POSITION);
        $this->dropForeignKey(self::FOREIGN_KEY_POSITION_ID, self::TABLE_TENANT_CITY_HAS_POSITION);

        $this->dropIndex(self::UNIQUE_INDEX, self::TABLE_TENANT_CITY_HAS_POSITION);
        $this->dropTable(self::TABLE_TENANT_CITY_HAS_POSITION);

    }

}
