<?php

use yii\db\Migration;

class m160407_074651_tbl_tenant_tariff__add_column extends Migration
{
    const TABLE_TENANT_TARIFF = '{{%tenant_tariff}}';
    const COLUMN_ACTIVE = 'active';

    public function up()
    {
        $this->addColumn(self::TABLE_TENANT_TARIFF, self::COLUMN_ACTIVE, $this->integer()->notNull()->defaultValue(1));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_TENANT_TARIFF, self::COLUMN_ACTIVE);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
