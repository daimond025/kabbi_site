<?php

use yii\db\Migration;

class m180116_101205_change_type_in_default_settings extends Migration
{
    const TABLE_NAME = '{{%default_settings}}';

    const COLUMN_TYPE = 'type';
    const COLUMN_NAME = 'name';

    const VALUE_COLUMN_NAME__IOS_WORKER_APP_VERSION = 'IOS_WORKER_APP_VERSION';
    const VALUE_COLUMN_NAME__ANDROID_WORKER_APP_VERSION = 'ANDROID_WORKER_APP_VERSION';

    const NEW_VALUE = 'general';
    const OLD_VALUE = 'system';

    public function safeUp()
    {
        $this->update(self::TABLE_NAME, [
            self::COLUMN_TYPE => self::NEW_VALUE
        ], [
            self::COLUMN_NAME => self::VALUE_COLUMN_NAME__IOS_WORKER_APP_VERSION,
        ]);

        $this->update(self::TABLE_NAME, [
            self::COLUMN_TYPE => self::NEW_VALUE
        ], [
            self::COLUMN_NAME => self::VALUE_COLUMN_NAME__ANDROID_WORKER_APP_VERSION,
        ]);
    }

    public function safeDown()
    {
        $this->update(self::TABLE_NAME, [
            self::COLUMN_TYPE => self::OLD_VALUE
        ], [
            self::COLUMN_NAME => self::VALUE_COLUMN_NAME__ANDROID_WORKER_APP_VERSION,
        ]);

        $this->update(self::TABLE_NAME, [
            self::COLUMN_TYPE => self::OLD_VALUE
        ], [
            self::COLUMN_NAME => self::VALUE_COLUMN_NAME__IOS_WORKER_APP_VERSION,
        ]);
    }
}
