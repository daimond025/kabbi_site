<?php

use yii\db\Migration;

class m170123_110841_remove_field_admin_user extends Migration
{
    public function up()
    {
        $this->dropColumn('{{%admin_user}}', 'role');
    }

    public function down()
    {
        echo "m170123_110841_remove_field_admin_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
