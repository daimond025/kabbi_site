<?php

use yii\db\Migration;

class m161111_131011_tbl_order_detail_cost__add_index_for_order_id extends Migration
{
    const TABLE_NAME = '{{%order_detail_cost}}';
    const COLUMN_NAME = 'order_id';
    const INDEX_NAME = 'idx_order_detail_cost__order_id';

    public function up()
    {
        $this->createIndex(self::INDEX_NAME, self::TABLE_NAME, self::COLUMN_NAME);
    }

    public function down()
    {
        $this->dropIndex(self::INDEX_NAME, self::TABLE_NAME);
    }
}
