<?php

use yii\db\Migration;

class m160217_094624_tbl_car_class extends Migration
{
    const TABLE_NAME = '{{%car_class}}';

    public function up()
    {
        $this->alterColumn(self::TABLE_NAME, 'class', $this->string(15)->notNull());
    }

    public function down()
    {
        $this->alterColumn(self::TABLE_NAME, 'class', $this->string(10));
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
