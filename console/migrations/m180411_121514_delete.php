<?php

use yii\db\Migration;

class m180411_121514_delete extends Migration
{
    public function safeUp()
    {

        $this->dropForeignKey('fk_order__tenant_company', 'tbl_order');
    }

    public function safeDown()
    {
        $this->addForeignKey('fk_order__tenant_company', 'tbl_order', 'tenant_company_id',
            'tbl_tenant_company', 'tenant_company_id', 'CASCADE', 'CASCADE');

        return true;
    }


}
