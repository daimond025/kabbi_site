<?php

use yii\db\Migration;

class m160822_103726_add_columns_in_clients extends Migration
{

    const TABLE_NAME = '{{%client}}';

    public function up()
    {

        $this->addColumn(self::TABLE_NAME, 'auth_key', $this->string(255)->notNull());
        $this->addColumn(self::TABLE_NAME, 'active_time', $this->integer(11)->notNull());
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'auth_key');
        $this->dropColumn(self::TABLE_NAME, 'active_time');
    }
}
