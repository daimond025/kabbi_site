<?php

use yii\db\Migration;

class m170405_064228_tbl_tenant_city_has_position__fill_table extends Migration
{
    const TABLE_TENANT_CITY_HAS_POSITION = '{{%tenant_city_has_position}}';

    const SQL_FILL_TABLE_TENANT_CITY_HAS_POSITION = <<<SQL
INSERT INTO `tbl_tenant_city_has_position` (`tenant_id`, `city_id`, `position_id`)
SELECT `tenant_id`, `city_id`, 1 `position_id`
FROM `tbl_tenant_has_city`
WHERE `block` = 0
SQL;

    public function up()
    {
        $this->db->createCommand(self::SQL_FILL_TABLE_TENANT_CITY_HAS_POSITION)->execute();
    }

    public function down()
    {
        $this->delete(self::TABLE_TENANT_CITY_HAS_POSITION);
    }

}
