<?php

use yii\db\Schema;
use yii\db\Migration;

class m150825_063545_tbl_option_tariff extends Migration
{
    public function up()
    {
        $this->addColumn('{{%option_tariff}}', 'active', Schema::TYPE_BOOLEAN);
    }

    public function down()
    {
        $this->dropColumn('{{%option_tariff}}', 'active');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
