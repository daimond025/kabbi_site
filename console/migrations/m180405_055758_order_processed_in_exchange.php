<?php

use yii\db\Migration;

class m180405_055758_order_processed_in_exchange extends Migration
{

    const TABLE_NAME     = '{{%order}}';
    const REF_TABLE_NAME = '{{%exchange_program}}';
    const COLUMN_NAME    = 'processed_exchange_program_id';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_NAME, $this->integer(10)->unsigned());
        return true;
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);
        return true;
    }
}
