<?php

use yii\db\Schema;
use yii\db\Migration;

class m151027_075540_add_new_notification_templates_for_all_drivers extends Migration
{

    public function up()
    {
        $query = new yii\db\Query();
        $data = $query->select(['tenant_id'])
                ->from('tbl_tenant')
                ->distinct()
                ->all();
        if ($data) {
            foreach ($data as $row) {
                $tenantId = $row['tenant_id'];
                //Добавляем шаблоны пушей
                $this->insert('{{%client_push_notifications}}', ['text' => 'Ваш водитель #BRAND# #MODEL# #COLOR# #NUMBER#', 'type' => '7', 'tenant_id' => $tenantId]);
                $this->insert('{{%client_push_notifications}}', ['text' => 'Водитель снят с вашего заказа. Ожидайте назначения другого водителя.', 'type' => '10', 'tenant_id' => $tenantId]);
                //Добавляем шаблоны смс
                $this->insert('{{%sms_template}}', ['text' => '#TAXI_NAME#. Ваш водитель #BRAND# #MODEL# #COLOR# #NUMBER#', 'type' => '7', 'tenant_id' => $tenantId]);
                $this->insert('{{%sms_template}}', ['text' => '#TAXI_NAME#. Водитель снят с вашего заказа. Ожидайте назначения другого водителя.', 'type' => '10', 'tenant_id' => $tenantId]);
            }
        }
    }

    public function down()
    {
        $query = new yii\db\Query();
        $data = $query->select(['tenant_id'])
                ->from('tbl_tenant')
                ->distinct()
                ->all();
        if ($data) {
            foreach ($data as $row) {
                $tenantId = $row['tenant_id'];
                //Добавляем шаблоны пушей
                $this->delete('{{%client_push_notifications}}', ['text' => 'Ваш водитель #BRAND# #MODEL# #COLOR# #NUMBER#', 'type' => '7', 'tenant_id' => $tenantId]);
                $this->delete('{{%client_push_notifications}}', ['text' => 'Водитель снят с вашего заказа. Ожидайте назначения другого водителя.', 'type' => '10', 'tenant_id' => $tenantId]);
                //Добавляем шаблоны смс
                $this->delete('{{%sms_template}}', ['text' => '#TAXI_NAME#. Ваш водитель #BRAND# #MODEL# #COLOR# #NUMBER#', 'type' => '7', 'tenant_id' => $tenantId]);
                $this->delete('{{%sms_template}}', ['text' => '#TAXI_NAME#. Водитель снят с вашего заказа. Ожидайте назначения другого водителя.', 'type' => '10', 'tenant_id' => $tenantId]);
            }
        }
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
