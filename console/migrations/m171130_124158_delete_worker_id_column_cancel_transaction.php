<?php

use yii\db\Migration;

class m171130_124158_delete_worker_id_column_cancel_transaction extends Migration
{

    const TABLE_NAME = '{{%paynet_cancel_transaction}}';

    public function safeUp()
    {
        $this->dropColumn(self::TABLE_NAME, 'worker_id');
    }

    public function safeDown()
    {
        $this->addColumn(self::TABLE_NAME, 'worker_id', $this->integer(11)->notNull());
    }
}
