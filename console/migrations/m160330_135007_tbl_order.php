<?php

use yii\db\Migration;

class m160330_135007_tbl_order extends Migration
{
    const TABLE_NAME = '{{%order}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'time_offset', $this->smallInteger());
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'time_offset');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
