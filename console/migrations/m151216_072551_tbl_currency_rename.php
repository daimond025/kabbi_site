<?php

use yii\db\Schema;
use yii\db\Migration;

class m151216_072551_tbl_currency_rename extends Migration
{
    const TABLE_NAME = '{{%currency}}';
    
    public function up()
    {
        $this->execute("UPDATE" . self::TABLE_NAME . " SET name = 'American Dollar' where code='USD'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET name = 'Serbian Dinar' where code='RSD'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET name = 'Azerbaijani Manat' where code='AZN'");
    }

    public function down()
    {
        echo "m151216_072551_tbl_currency_rename cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
