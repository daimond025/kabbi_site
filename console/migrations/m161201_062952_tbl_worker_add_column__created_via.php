<?php

use yii\db\Migration;

class m161201_062952_tbl_worker_add_column__created_via extends Migration
{
    const TABLE_NAME = '{{%worker}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'created_via', 'ENUM("SITE", "DISPATCHER") NOT NULL DEFAULT "DISPATCHER"');
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'created_via');
    }
}
