<?php

use yii\db\Migration;

class m171115_141709_add_table__tbl_promo_bonus_operation extends Migration
{

    const TABLE_NAME = '{{%promo_bonus_operation}}';

    const FK_PROMO_BONUS_OPERATION__CLIENT = 'fk_promo_bonus_operation__client';
    const FK_PROMO_BONUS_OPERATION__WORKER = 'fk_promo_bonus_operation__worker';
    const FK_PROMO_BONUS_OPERATION__ORDER = 'fk_promo_bonus_operation__order';

    const COLUMN_CLIENT_ID = 'client_id';
    const COLUMN_WORKER_ID = 'worker_id';
    const COLUMN_ORDER_ID = 'order_id';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'bonus_operation_id'   => $this->primaryKey()->unsigned(),
            self::COLUMN_ORDER_ID  => $this->integer()->unsigned()->notNull(),
            self::COLUMN_CLIENT_ID => $this->integer()->unsigned(),
            self::COLUMN_WORKER_ID => $this->integer(),
            'bonus_subject'        => 'ENUM("client", "worker") NOT NULL',
            'completed'            => $this->integer()->defaultValue(0),
            'promo_bonus'          => $this->decimal(20, 5)->notNull(),
            'created_at'           => $this->integer()->unsigned(),
            'updated_at'           => $this->integer()->unsigned(),
        ]);

        $this->createIndex('idx_promo_bonus_operation__order_id', self::TABLE_NAME, self::COLUMN_ORDER_ID);
        $this->createIndex('idx_promo_bonus_operation__client_id', self::TABLE_NAME, self::COLUMN_CLIENT_ID);
        $this->createIndex('idx_promo_bonus_operation__worker_id', self::TABLE_NAME, self::COLUMN_WORKER_ID);

        $this->addForeignKey(self::FK_PROMO_BONUS_OPERATION__ORDER, self::TABLE_NAME, self::COLUMN_ORDER_ID,
            '{{%order}}', self::COLUMN_ORDER_ID, 'CASCADE', 'CASCADE');

        $this->addForeignKey(self::FK_PROMO_BONUS_OPERATION__CLIENT, self::TABLE_NAME, self::COLUMN_CLIENT_ID,
            '{{%client}}', self::COLUMN_CLIENT_ID, 'CASCADE', 'CASCADE');

        $this->addForeignKey(self::FK_PROMO_BONUS_OPERATION__WORKER, self::TABLE_NAME, self::COLUMN_WORKER_ID,
            '{{%worker}}', self::COLUMN_WORKER_ID, 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_PROMO_BONUS_OPERATION__WORKER, self::TABLE_NAME);
        $this->dropForeignKey(self::FK_PROMO_BONUS_OPERATION__CLIENT, self::TABLE_NAME);
        $this->dropForeignKey(self::FK_PROMO_BONUS_OPERATION__ORDER, self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
    }
}
