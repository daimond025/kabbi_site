<?php

use yii\db\Migration;

class m160324_131812_create_tenant_tariff_tables extends Migration
{
    const TABLE_TENANT = '{{%tenant}}';
    const TABLE_TENANT_TARIFF = '{{%tenant_tariff}}';
    const TABLE_TENANT_PERMISSION = '{{%tenant_permission}}';
    const TABLE_TARIFF_USER_PERMISSION = '{{%tariff_user_permission}}';
    const TABLE_TARIFF_PERMISSION = '{{%tariff_permission}}';
    const TABLE_TARIFF_PERMISSION_HISTORY = '{{%tariff_permission_history}}';
    const TABLE_USER_PERMISSION = '{{%user_permission}}';
    const TABLE_TARIFF = '{{%tariff}}';
    const TABLE_TARIFF_ENTITY_TYPE = '{{%tariff_entity_type}}';
    const TABLE_MODULE = '{{%module}}';
    const TABLE_TARIFF_ADDITIONAL_OPTION = '{{%tariff_additional_option}}';
    const TABLE_ONE_TIME_SERVICE = '{{%one_time_service}}';
    const TABLE_TENANT_ADDITIONAL_OPTION = '{{%tenant_additional_option}}';
    const TABLE_PAYMENT = '{{%payment}}';
    const TABLE_PAYMENT_FOR_TARIFF = '{{%payment_for_tariff}}';
    const TABLE_PAYMENT_FOR_ADDITIONAL_OPTION = '{{%payment_for_additional_option}}';
    const TABLE_PAYMENT_FOR_ONE_TIME_SERVICE = '{{%payment_for_one_time_service}}';
    const TABLE_PRICE_HISTORY = '{{%price_history}}';
    const TABLE_CURRENCY = '{{%currency}}';

    const COLUMN_MODULE_ID = 'module_id';
    const COLUMN_PAYMENT_ID = 'payment_id';
    const COLUMN_TENANT_ID = 'tenant_id';
    const COLUMN_TARIFF_ID = 'tariff_id';
    const COLUMN_TARIFF_PERMISSION_ID = 'permission_id';
    const COLUMN_TARIFF_ADDITIONAL_OPTION_ID = 'option_id';
    const COLUMN_ONE_TIME_SERVICE_ID = 'service_id';
    const COLUMN_TARIFF_ENTITY_TYPE_ID = 'entity_type_id';
    const COLUMN_CURRENCY_ID = 'currency_id';

//    public function tableExists($tableName)
//    {
//        $tableName = $this->db->schema->getRawTableName($tableName);
//
//        return in_array($tableName, Yii::$app->db->schema->tableNames);
//    }
//
//    public function dropTable($tableName)
//    {
//        if ($this->tableExists($tableName)) {
//            $this->dropTable($tableName);
//        }
//    }

    public function generateForeignKeyName($tableName, $columnName)
    {
        return 'fk_' . $this->db->schema->getRawTableName($tableName)
        . '__' . $columnName;
    }

    public function dropTables()
    {
        $this->dropTable(self::TABLE_PAYMENT_FOR_ONE_TIME_SERVICE);
        $this->dropTable(self::TABLE_PAYMENT_FOR_ADDITIONAL_OPTION);
        $this->dropTable(self::TABLE_PAYMENT_FOR_TARIFF);
        $this->dropTable(self::TABLE_PAYMENT);
        $this->dropTable(self::TABLE_TENANT_ADDITIONAL_OPTION);
        $this->dropTable(self::TABLE_TENANT_PERMISSION);
        $this->dropTable(self::TABLE_TENANT_TARIFF);
        $this->dropTable(self::TABLE_PRICE_HISTORY);
        $this->dropTable(self::TABLE_TARIFF_ENTITY_TYPE);
        $this->dropTable(self::TABLE_ONE_TIME_SERVICE);
        $this->dropTable(self::TABLE_TARIFF_ADDITIONAL_OPTION);
        $this->dropTable(self::TABLE_TARIFF_PERMISSION_HISTORY);
        $this->dropTable(self::TABLE_TARIFF_PERMISSION);
        $this->dropTable(self::TABLE_TARIFF);
        $this->dropTable(self::TABLE_MODULE);
    }

    public function createTables()
    {
        $this->createTable(self::TABLE_MODULE, [
            'id'          => $this->primaryKey(),
            'name'        => $this->string()->notNull(),
            'description' => $this->text(),
            'active'      => $this->smallInteger(1)->notNull()->defaultValue(1),
            'created_at'  => $this->integer(),
            'updated_at'  => $this->integer(),
            'created_by'  => $this->integer(),
            'updated_by'  => $this->integer(),
        ]);

        $this->createTable(self::TABLE_TARIFF, [
            'id'                   => $this->primaryKey(),
            'name'                 => $this->string()->notNull(),
            'description'          => $this->text(),
            self::COLUMN_MODULE_ID => $this->integer()->notNull(),
            'active'               => $this->smallInteger(1)->notNull()->defaultValue(1),
            'default'              => $this->smallInteger(1)->notNull()->defaultValue(0),
            'created_at'           => $this->integer(),
            'updated_at'           => $this->integer(),
            'created_by'           => $this->integer(),
            'updated_by'           => $this->integer(),
        ]);
        $this->addForeignKey($this->generateForeignKeyName(self::TABLE_TARIFF, self::COLUMN_MODULE_ID),
            self::TABLE_TARIFF, self::COLUMN_MODULE_ID, self::TABLE_MODULE, 'id',
            'CASCADE', 'CASCADE');

        $this->createTable(self::TABLE_TARIFF_PERMISSION, [
            'id'                   => $this->primaryKey(),
            self::COLUMN_TARIFF_ID => $this->integer()->notNull(),
            'name'                 => $this->string()->notNull(),
            'description'          => $this->text(),
            'active'               => $this->smallInteger(1)->notNull()->defaultValue(1),
            'created_at'           => $this->integer(),
            'updated_at'           => $this->integer(),
            'created_by'           => $this->integer(),
            'updated_by'           => $this->integer(),
        ]);
        $this->addForeignKey($this->generateForeignKeyName(self::TABLE_TARIFF_PERMISSION, self::COLUMN_TARIFF_ID),
            self::TABLE_TARIFF_PERMISSION, self::COLUMN_TARIFF_ID, self::TABLE_TARIFF, 'id',
            'CASCADE', 'CASCADE');

        $this->createTable(self::TABLE_TARIFF_PERMISSION_HISTORY, [
            'id'                              => $this->primaryKey(),
            self::COLUMN_TARIFF_PERMISSION_ID => $this->integer()->notNull(),
            'value'                           => $this->integer()->notNull(),
            'activated_at'                    => $this->integer()->notNull(),
            'deactivated_at'                  => $this->integer(),
            'created_at'                      => $this->integer(),
            'updated_at'                      => $this->integer(),
            'created_by'                      => $this->integer(),
            'updated_by'                      => $this->integer(),
        ]);
        $this->addForeignKey($this->generateForeignKeyName(self::TABLE_TARIFF_PERMISSION_HISTORY,
            self::COLUMN_TARIFF_PERMISSION_ID),
            self::TABLE_TARIFF_PERMISSION_HISTORY, self::COLUMN_TARIFF_PERMISSION_ID,
            self::TABLE_TARIFF_PERMISSION, 'id', 'CASCADE', 'CASCADE');

        $this->createTable(self::TABLE_TARIFF_ADDITIONAL_OPTION, [
            'id'          => $this->primaryKey(),
            'name'        => $this->string()->notNull(),
            'description' => $this->text(),
            'active'      => $this->smallInteger(1)->notNull()->defaultValue(1),
            'created_at'  => $this->integer(),
            'updated_at'  => $this->integer(),
            'created_by'  => $this->integer(),
            'updated_by'  => $this->integer(),
        ]);

        $this->createTable(self::TABLE_ONE_TIME_SERVICE, [
            'id'          => $this->primaryKey(),
            'name'        => $this->string()->notNull(),
            'description' => $this->text(),
            'active'      => $this->smallInteger(1)->notNull()->defaultValue(1),
            'created_at'  => $this->integer(),
            'updated_at'  => $this->integer(),
            'created_by'  => $this->integer(),
            'updated_by'  => $this->integer(),
        ]);

        $this->createTable(self::TABLE_TARIFF_ENTITY_TYPE, [
            'id'          => $this->primaryKey(),
            'name'        => $this->string()->notNull(),
            'description' => $this->text(),
            'created_at'  => $this->integer(),
            'updated_at'  => $this->integer(),
            'created_by'  => $this->integer(),
            'updated_by'  => $this->integer(),
        ]);

        $this->createTable(self::TABLE_PRICE_HISTORY, [
            'id'                               => $this->primaryKey(),
            self::COLUMN_TARIFF_ENTITY_TYPE_ID => $this->integer()->notNull(),
            'price'                            => $this->money(12, 2)->notNull()->defaultValue(0),
            self::COLUMN_CURRENCY_ID           => $this->integer()->notNull(),
            'activated_at'                     => $this->integer()->notNull(),
            'deactivated_at'                   => $this->integer(),
            'created_at'                       => $this->integer(),
            'updated_at'                       => $this->integer(),
            'created_by'                       => $this->integer(),
            'updated_by'                       => $this->integer(),
        ]);
        $this->addForeignKey($this->generateForeignKeyName(self::TABLE_PRICE_HISTORY, self::COLUMN_CURRENCY_ID),
            self::TABLE_PRICE_HISTORY, self::COLUMN_CURRENCY_ID, self::TABLE_CURRENCY, self::COLUMN_CURRENCY_ID,
            'CASCADE', 'CASCADE');

        $this->createTable(self::TABLE_TENANT_TARIFF, [
            'id'                   => $this->primaryKey(),
            self::COLUMN_TENANT_ID => 'INT(10) UNSIGNED NOT NULL',
            self::COLUMN_TARIFF_ID => $this->integer()->notNull(),
            'expiry_date'          => $this->integer()->notNull(),
            'created_at'           => $this->integer(),
            'updated_at'           => $this->integer(),
            'created_by'           => $this->integer(),
            'updated_by'           => $this->integer(),
        ]);
        $this->addForeignKey($this->generateForeignKeyName(self::TABLE_TENANT_TARIFF, self::COLUMN_TENANT_ID),
            self::TABLE_TENANT_TARIFF, self::COLUMN_TENANT_ID, self::TABLE_TENANT, self::COLUMN_TENANT_ID,
            'CASCADE', 'CASCADE');
        $this->addForeignKey($this->generateForeignKeyName(self::TABLE_TENANT_TARIFF, self::COLUMN_TARIFF_ID),
            self::TABLE_TENANT_TARIFF, self::COLUMN_TARIFF_ID, self::TABLE_TARIFF, 'id',
            'CASCADE', 'CASCADE');

        $this->createTable(self::TABLE_TENANT_PERMISSION, [
            'id'                              => $this->primaryKey(),
            self::COLUMN_TARIFF_PERMISSION_ID => $this->integer()->notNull(),
            'value'                           => $this->integer()->notNull(),
            'created_at'                      => $this->integer(),
            'updated_at'                      => $this->integer(),
            'created_by'                      => $this->integer(),
            'updated_by'                      => $this->integer(),
        ]);
        $this->addForeignKey($this->generateForeignKeyName(self::TABLE_TENANT_PERMISSION,
            self::COLUMN_TARIFF_PERMISSION_ID),
            self::TABLE_TENANT_PERMISSION, self::COLUMN_TARIFF_PERMISSION_ID, self::TABLE_TARIFF_PERMISSION, 'id',
            'CASCADE', 'CASCADE');

        $this->createTable(self::TABLE_TENANT_ADDITIONAL_OPTION, [
            'id'                                     => $this->primaryKey(),
            self::COLUMN_TENANT_ID                   => 'INT(10) UNSIGNED NOT NULL',
            self::COLUMN_TARIFF_ADDITIONAL_OPTION_ID => $this->integer()->notNull(),
            'expiry_date'                            => $this->integer()->notNull(),
            'created_at'                             => $this->integer(),
            'updated_at'                             => $this->integer(),
            'created_by'                             => $this->integer(),
            'updated_by'                             => $this->integer(),
        ]);
        $this->addForeignKey($this->generateForeignKeyName(self::TABLE_TENANT_ADDITIONAL_OPTION,
            self::COLUMN_TENANT_ID),
            self::TABLE_TENANT_ADDITIONAL_OPTION, self::COLUMN_TENANT_ID, self::TABLE_TENANT, self::COLUMN_TENANT_ID,
            'CASCADE', 'CASCADE');
        $this->addForeignKey($this->generateForeignKeyName(self::TABLE_TENANT_ADDITIONAL_OPTION,
            self::COLUMN_TARIFF_ADDITIONAL_OPTION_ID),
            self::TABLE_TENANT_ADDITIONAL_OPTION, self::COLUMN_TARIFF_ADDITIONAL_OPTION_ID,
            self::TABLE_TARIFF_ADDITIONAL_OPTION, 'id', 'CASCADE', 'CASCADE');

        $this->createTable(self::TABLE_PAYMENT, [
            'id'                     => $this->primaryKey(),
            self::COLUMN_TENANT_ID   => 'INT(10) UNSIGNED NOT NULL',
            'description'            => $this->text(),
            'purchased_at'           => $this->integer(),
            'payment_sum'            => $this->money(12, 2),
            self::COLUMN_CURRENCY_ID => $this->integer()->notNull(),
            'created_at'             => $this->integer(),
            'updated_at'             => $this->integer(),
            'created_by'             => $this->integer(),
            'updated_by'             => $this->integer(),
        ]);
        $this->addForeignKey($this->generateForeignKeyName(self::TABLE_PAYMENT, self::COLUMN_TENANT_ID),
            self::TABLE_PAYMENT, self::COLUMN_TENANT_ID, self::TABLE_TENANT, self::COLUMN_TENANT_ID,
            'CASCADE', 'CASCADE');
        $this->addForeignKey($this->generateForeignKeyName(self::TABLE_PAYMENT, self::COLUMN_CURRENCY_ID),
            self::TABLE_PAYMENT, self::COLUMN_CURRENCY_ID, self::TABLE_CURRENCY, self::COLUMN_CURRENCY_ID,
            'CASCADE', 'CASCADE');

        $this->createTable(self::TABLE_PAYMENT_FOR_TARIFF, [
            'id'                     => $this->primaryKey(),
            self::COLUMN_PAYMENT_ID  => $this->integer()->notNull(),
            self::COLUMN_TARIFF_ID   => $this->integer()->notNull(),
            'period'                 => $this->integer()->notNull(),
            'price'                  => $this->money(12, 2),
            'payment_sum'            => $this->money(12, 2),
            self::COLUMN_CURRENCY_ID => $this->integer()->notNull(),
            'created_at'             => $this->integer(),
            'updated_at'             => $this->integer(),
            'created_by'             => $this->integer(),
            'updated_by'             => $this->integer(),
        ]);
        $this->addForeignKey($this->generateForeignKeyName(self::TABLE_PAYMENT_FOR_TARIFF, self::COLUMN_PAYMENT_ID),
            self::TABLE_PAYMENT_FOR_TARIFF, self::COLUMN_PAYMENT_ID, self::TABLE_PAYMENT, 'id',
            'CASCADE', 'CASCADE');
        $this->addForeignKey($this->generateForeignKeyName(self::TABLE_PAYMENT_FOR_TARIFF, self::COLUMN_TARIFF_ID),
            self::TABLE_PAYMENT_FOR_TARIFF, self::COLUMN_TARIFF_ID, self::TABLE_TARIFF, 'id',
            'CASCADE', 'CASCADE');
        $this->addForeignKey($this->generateForeignKeyName(self::TABLE_PAYMENT_FOR_TARIFF, self::COLUMN_CURRENCY_ID),
            self::TABLE_PAYMENT_FOR_TARIFF, self::COLUMN_CURRENCY_ID, self::TABLE_CURRENCY, self::COLUMN_CURRENCY_ID,
            'CASCADE', 'CASCADE');

        $this->createTable(self::TABLE_PAYMENT_FOR_ADDITIONAL_OPTION, [
            'id'                                     => $this->primaryKey(),
            self::COLUMN_PAYMENT_ID                  => $this->integer()->notNull(),
            self::COLUMN_TARIFF_ADDITIONAL_OPTION_ID => $this->integer()->notNull(),
            'period'                                 => $this->integer()->notNull(),
            'price'                                  => $this->money(12, 2),
            'payment_sum'                            => $this->money(12, 2),
            self::COLUMN_CURRENCY_ID                 => $this->integer()->notNull(),
            'created_at'                             => $this->integer(),
            'updated_at'                             => $this->integer(),
            'created_by'                             => $this->integer(),
            'updated_by'                             => $this->integer(),
        ]);
        $this->addForeignKey($this->generateForeignKeyName(self::TABLE_PAYMENT_FOR_ADDITIONAL_OPTION,
            self::COLUMN_PAYMENT_ID),
            self::TABLE_PAYMENT_FOR_ADDITIONAL_OPTION, self::COLUMN_PAYMENT_ID, self::TABLE_PAYMENT, 'id',
            'CASCADE', 'CASCADE');
        $this->addForeignKey($this->generateForeignKeyName(self::TABLE_PAYMENT_FOR_ADDITIONAL_OPTION,
            self::COLUMN_TARIFF_ADDITIONAL_OPTION_ID),
            self::TABLE_PAYMENT_FOR_ADDITIONAL_OPTION, self::COLUMN_TARIFF_ADDITIONAL_OPTION_ID,
            self::TABLE_TARIFF_ADDITIONAL_OPTION, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey($this->generateForeignKeyName(self::TABLE_PAYMENT_FOR_ADDITIONAL_OPTION,
            self::COLUMN_CURRENCY_ID),
            self::TABLE_PAYMENT_FOR_ADDITIONAL_OPTION, self::COLUMN_CURRENCY_ID,
            self::TABLE_CURRENCY, self::COLUMN_CURRENCY_ID, 'CASCADE', 'CASCADE');

        $this->createTable(self::TABLE_PAYMENT_FOR_ONE_TIME_SERVICE, [
            'id'                             => $this->primaryKey(),
            self::COLUMN_PAYMENT_ID          => $this->integer()->notNull(),
            self::COLUMN_ONE_TIME_SERVICE_ID => $this->integer()->notNull(),
            'price'                          => $this->money(12, 2),
            'payment_sum'                    => $this->money(12, 2),
            self::COLUMN_CURRENCY_ID         => $this->integer()->notNull(),
            'executed_at'                    => $this->integer(),
            'created_at'                     => $this->integer(),
            'updated_at'                     => $this->integer(),
            'created_by'                     => $this->integer(),
            'updated_by'                     => $this->integer(),
        ]);
        $this->addForeignKey($this->generateForeignKeyName(self::TABLE_PAYMENT_FOR_ONE_TIME_SERVICE,
            self::COLUMN_PAYMENT_ID),
            self::TABLE_PAYMENT_FOR_ONE_TIME_SERVICE, self::COLUMN_PAYMENT_ID, self::TABLE_PAYMENT, 'id',
            'CASCADE', 'CASCADE');
        $this->addForeignKey($this->generateForeignKeyName(self::TABLE_PAYMENT_FOR_ONE_TIME_SERVICE,
            self::COLUMN_ONE_TIME_SERVICE_ID),
            self::TABLE_PAYMENT_FOR_ONE_TIME_SERVICE, self::COLUMN_ONE_TIME_SERVICE_ID, self::TABLE_ONE_TIME_SERVICE,
            'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey($this->generateForeignKeyName(self::TABLE_PAYMENT_FOR_ONE_TIME_SERVICE,
            self::COLUMN_CURRENCY_ID),
            self::TABLE_PAYMENT_FOR_ONE_TIME_SERVICE, self::COLUMN_CURRENCY_ID,
            self::TABLE_CURRENCY, self::COLUMN_CURRENCY_ID,
            'CASCADE', 'CASCADE');
    }

    public function up()
    {
//        $this->dropTables();
        $this->createTables();
    }

    public function down()
    {
        $this->dropTables();
    }
}
