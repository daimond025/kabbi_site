<?php

use yii\db\Migration;

class m180206_094027_add_tenant_companies_in_sms_temaplate_courier_table extends Migration
{
    const TABLE_NAME = '{{%default_sms_template_courier}}';

    const PARAM_TENANT_COMPANY_NAME = '#COMPANY_NAME#';
    const PARAM_TENANT_COMPANY_PHONE = '#COMPANY_PHONE#';

    public function safeUp()
    {
        $templates = $this->getTemplates();

        foreach ($templates as $template) {
            $templateId = $template['template_id'];
            $params = $template['params'];

            $arrParams = explode(';', $params);
            $arrParams = array_map(function ($param) {
                return trim($param);
            }, $arrParams);

            $arrParams[] = self::PARAM_TENANT_COMPANY_NAME;
            $arrParams[] = self::PARAM_TENANT_COMPANY_PHONE;

            $arrParams = array_unique($arrParams);

            $params = implode('; ', $arrParams);

            $this->update(self::TABLE_NAME, ['params' => $params], ['template_id' => $templateId]);
        }
    }

    public function safeDown()
    {
        $templates = $this->getTemplates();

        foreach ($templates as $template) {
            $templateId = $template['template_id'];
            $params = $template['params'];
            $delete = [self::PARAM_TENANT_COMPANY_PHONE, self::PARAM_TENANT_COMPANY_NAME];

            $arrParams = explode(';', $params);
            $arrParams = array_map(function ($param) {
                return trim($param);
            }, $arrParams);
            $arrParams = array_diff($arrParams, $delete);

            $params = implode('; ', $arrParams);

            $this->update(self::TABLE_NAME, ['params' => $params], ['template_id' => $templateId]);
        }
    }

    protected function getTemplates()
    {
        return (new \yii\db\Query())->from(self::TABLE_NAME)->all();
    }
}
