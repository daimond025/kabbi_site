<?php

use yii\db\Migration;

class m170512_043303_sms_provider_promosms extends Migration
{
    const TABLE_NAME = '{{%sms_server}}';
    const ID         = 18;

    public function up()
    {
        $this->insert(self::TABLE_NAME,
            [
                'server_id' => self::ID,
                'name'      => 'PromoSMS',
                'host'      => 'http://promosms.ru/',
                'active'    => 1,
            ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME, 'server_id = :id', ['id' => self::ID]);
    }
}
