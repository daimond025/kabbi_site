<?php

use yii\db\Migration;

class m171021_144014_create_table__worker_balance_notification extends Migration
{
    const TABLE_WORKER_BALANCE_NOTIFICATION = '{{%worker_balance_notification}}';
    const TABLE_TENANT = '{{%tenant}}';
    const FOREIGN_KEY = 'fk_worker_balance_notification__tenant_id';

    public function safeUp()
    {
        $this->createTable(self::TABLE_WORKER_BALANCE_NOTIFICATION, [
            'id'              => $this->primaryKey(),
            'tenant_id'       => $this->integer(10)->unsigned()->notNull(),
            'name'            => $this->string()->notNull(),
            'url'             => $this->string()->notNull(),
            'token'           => $this->string()->notNull(),
            'writeoff_notify' => $this->integer()->notNull()->defaultValue(0),
            'deposit_notify'  => $this->integer()->notNull()->defaultValue(0),
            'created_at'      => $this->integer(),
            'updated_at'      => $this->integer(),
        ]);

        $this->addForeignKey(self::FOREIGN_KEY, self::TABLE_WORKER_BALANCE_NOTIFICATION, 'tenant_id',
            self::TABLE_TENANT, 'tenant_id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_WORKER_BALANCE_NOTIFICATION);
    }

}
