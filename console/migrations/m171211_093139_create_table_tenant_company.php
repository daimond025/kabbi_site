<?php

use yii\db\Migration;

class m171211_093139_create_table_tenant_company extends Migration
{

    const TABLE_NAME = '{{%tenant_company}}';

    const FK_TENANT_COMPANY__TENANT = 'fk_tenant_company__tenant';

    const COLUMN_TENANT_ID = 'tenant_id';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'tenant_company_id'    => $this->primaryKey()->unsigned(),
            self::COLUMN_TENANT_ID => $this->integer()->unsigned(),
            'name'                 => $this->string(255),
            'sort'                 => $this->integer(),
            'block'                => $this->integer(),
        ]);

        $this->addForeignKey(self::FK_TENANT_COMPANY__TENANT, self::TABLE_NAME, self::COLUMN_TENANT_ID, '{{%tenant}}', self::COLUMN_TENANT_ID, 'CASCADE', 'CASCADE');

    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_TENANT_COMPANY__TENANT, self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
    }
}
