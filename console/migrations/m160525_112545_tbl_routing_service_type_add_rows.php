<?php

use yii\db\Migration;

class m160525_112545_tbl_routing_service_type_add_rows extends Migration
{
    const TABLE_NAME = '{{%routing_service_type}}';

    public function up()
    {
        $this->insert(self::TABLE_NAME, [
            'id'            => 1,
            'service_label' => 'OSM',
            'active'        => 1
        ]);
        $this->insert(self::TABLE_NAME, [
            'id'            => 2,
            'service_label' => 'Google (free version)',
            'active'        => 1
        ]);
        $this->insert(self::TABLE_NAME, [
            'id'            => 3,
            'service_label' => 'Google',
            'active'        => 0
        ]);
        $this->insert(self::TABLE_NAME, [
            'id'            => 4,
            'service_label' => 'Here.com',
            'active'        => 0
        ]);
        $this->insert(self::TABLE_NAME, [
            'id'            => 5,
            'service_label' => '2GIS',
            'active'        => 0
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME, ['id' => 1]);
        $this->delete(self::TABLE_NAME, ['id' => 2]);
        $this->delete(self::TABLE_NAME, ['id' => 3]);
        $this->delete(self::TABLE_NAME, ['id' => 4]);
        $this->delete(self::TABLE_NAME, ['id' => 5]);
        return true;
    }
}