<?php

use yii\db\Schema;
use yii\db\Migration;

class m151116_111618_tbl_geodata_az_add_row extends Migration
{
    public function up()
    {
          $this->addColumn('{{%geodata_az}}', 'house', 'VARCHAR(20) DEFAULT  NULL');
    }

    public function down()
    {
         $this->dropColumn('{{%geodata_az}}', 'house');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
