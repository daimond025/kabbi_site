<?php

use yii\db\Migration;

class m160412_124115_tbl_source_message extends Migration
{
    const TABLE_NAME = '{{%source_message}}';

    public function up()
    {
        $this->update(self::TABLE_NAME, ['category' => 'employee'], ['category' => 'driver']);
    }

    public function down()
    {
        $this->update(self::TABLE_NAME, ['category' => 'driver'], ['category' => 'employee']);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
