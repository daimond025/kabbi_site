<?php

use yii\db\Migration;

class m160525_110445_tbl_auto_geo_service_type_add_rows extends Migration
{
    const TABLE_NAME = '{{%auto_geo_service_type}}';

    public function up()
    {
        $this->insert(self::TABLE_NAME, [
            'id'            => 1,
            'service_label' => 'Yandex (free version)',
            'active'        => 1
        ]);
        $this->insert(self::TABLE_NAME, [
            'id'            => 2,
            'service_label' => 'Here.com',
            'active'        => 1
        ]);
        $this->insert(self::TABLE_NAME, [
            'id'            => 3,
            'service_label' => 'Google',
            'active'        => 0
        ]);
        $this->insert(self::TABLE_NAME, [
            'id'            => 4,
            'service_label' => '2GiS',
            'active'        => 0
        ]);
        $this->insert(self::TABLE_NAME, [
            'id'            => 5,
            'service_label' => 'Gootax',
            'active'        => 1
        ]);
        $this->insert(self::TABLE_NAME, [
            'id'            => 6,
            'service_label' => 'Yandex',
            'active'        => 0
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME, ['id' => 1]);
        $this->delete(self::TABLE_NAME, ['id' => 2]);
        $this->delete(self::TABLE_NAME, ['id' => 3]);
        $this->delete(self::TABLE_NAME, ['id' => 4]);
        $this->delete(self::TABLE_NAME, ['id' => 5]);
        $this->delete(self::TABLE_NAME, ['id' => 6]);

        return true;
    }
    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}