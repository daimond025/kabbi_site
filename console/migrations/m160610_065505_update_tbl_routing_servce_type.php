<?php

use yii\db\Migration;

class m160610_065505_update_tbl_routing_servce_type extends Migration
{
    const TABLE_NAME = '{{%routing_service_type}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'key_1_label', $this->string(50));
        $this->addColumn(self::TABLE_NAME, 'key_2_label', $this->string(50));
        $this->renameColumn(self::TABLE_NAME, 'has_app_code', 'has_key_1');
        $this->renameColumn(self::TABLE_NAME, 'has_app_id', 'has_key_2');
        //HERE.COM
        $this->execute("UPDATE" . self::TABLE_NAME . " SET key_1_label = 'APP CODE' where id = 4");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET key_2_label = 'APP ID' where id = 4");
        //Google
        $this->execute("UPDATE" . self::TABLE_NAME . " SET key_1_label = 'Google Maps Directions API key' where id = 3");
        //2GIS
        $this->execute("UPDATE" . self::TABLE_NAME . " SET key_1_label = '2GIS API key' where id = 5");
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'key_1_label');
        $this->dropColumn(self::TABLE_NAME, 'key_2_label');
        $this->renameColumn(self::TABLE_NAME, 'has_key_1', 'has_app_code');
        $this->renameColumn(self::TABLE_NAME, 'has_key_2', 'has_app_id');
        //HERE.COM
        $this->execute("UPDATE" . self::TABLE_NAME . " SET key_1_label = NULL where id = 4");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET key_2_label = NULL where id = 4");
        //Google
        $this->execute("UPDATE" . self::TABLE_NAME . " SET key_1_label = NULL where id = 3");
        //Yandex
        $this->execute("UPDATE" . self::TABLE_NAME . " SET key_1_label = NULL where id = 5");
        return true;
    }
}