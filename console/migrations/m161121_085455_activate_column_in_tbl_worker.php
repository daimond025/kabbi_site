<?php

use yii\db\Migration;

class m161121_085455_activate_column_in_tbl_worker extends Migration
{
    const TABLE_NAME = '{{%worker}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'activate', $this->integer(1)
            ->defaultValue(1)
            ->notNull()
            ->after('block')
        );
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'activate');
    }
}
