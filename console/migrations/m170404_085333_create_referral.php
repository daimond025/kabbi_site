<?php

use yii\db\Migration;

class m170404_085333_create_referral extends Migration
{
    const TABLE_NAME = '{{%referral}}';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'referral_id'                => $this->primaryKey(),
            'sort'                       => $this->integer(5)->notNull()->defaultValue(100),
            'tenant_id'                  => $this->integer(10)->unsigned()->notNull(),
            'active'                     => $this->integer(1)->notNull()->defaultValue(0),
            'name'                       => $this->string(100)->notNull(),
            'city_id'                    => $this->integer(11)->unsigned()->notNull(),
            'is_active_subscriber_limit' => $this->integer(1)->notNull()->defaultValue(0)
                ->comment('Включить лимит приглашенных?'),
            'subscriber_limit'           => $this->integer(11)->comment('Лимит приглашенных'),
            'type'                       => 'ENUM("ON_ORDER", "ON_REGISTER") NOT NULL',
            'inviting_value'             => $this->integer(11)->notNull()->defaultValue(0)
                ->comment('Бонус приглашаемому'),
            'inviting_type'              => 'ENUM("PERCENT", "BONUS") NOT NULL',
            'invited_value'              => $this->integer(11)->notNull()->defaultValue(0)
                ->comment('Бонус приглашенному'),
            'invited_type'               => 'ENUM("PERCENT", "BONUS") NOT NULL',
            'order_count'                => $this->integer(11)->defaultValue(0)
                ->comment('Кол-во поездок. 0 - безлимитно'),
            'min_price'                  => $this->float(6),
            'text_in_client_api'         => $this->text()->notNull()->comment('Текст в клиентском прил.'),
            'title'                      => $this->string(100)->notNull(),
            'text'                       => $this->text()->notNull(),
            'rand_string'                => $this->string(255)->notNull(),
        ]);

        $this->addForeignKey('fk_referral__tenant_id', self::TABLE_NAME, 'tenant_id',
            '{{%tenant}}', 'tenant_id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('fk_referral__city_id', self::TABLE_NAME, 'city_id',
            '{{%city}}', 'city_id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
