<?php

use yii\db\Schema;
use yii\db\Migration;

class m151026_115457_tbl_default_client_status_event extends Migration
{

    public function up()
    {
        $this->insert('{{%default_client_status_event}}', ['status_id' => '7', 'group' => 'CALL', 'notice' => 'nothing']);
        $this->insert('{{%default_client_status_event}}', ['status_id' => '7', 'group' => 'APP', 'notice' => 'push']);
        $this->insert('{{%default_client_status_event}}', ['status_id' => '7', 'group' => 'WEB', 'notice' => 'nothing']);
        $this->insert('{{%default_client_status_event}}', ['status_id' => '10', 'group' => 'CALL', 'notice' => 'nothing']);
        $this->insert('{{%default_client_status_event}}', ['status_id' => '10', 'group' => 'APP', 'notice' => 'push']);
        $this->insert('{{%default_client_status_event}}', ['status_id' => '10', 'group' => 'WEB', 'notice' => 'nothing']);
    }

    public function down()
    {
        $this->delete('{{%default_client_status_event}}', ['status_id' => '7', 'group' => 'CALL', 'notice' => 'nothing']);
        $this->delete('{{%default_client_status_event}}', ['status_id' => '7', 'group' => 'APP', 'notice' => 'push']);
        $this->delete('{{%default_client_status_event}}', ['status_id' => '7', 'group' => 'WEB', 'notice' => 'nothing']);
        $this->delete('{{%default_client_status_event}}', ['status_id' => '10', 'group' => 'CALL', 'notice' => 'nothing']);
        $this->delete('{{%default_client_status_event}}', ['status_id' => '10', 'group' => 'APP', 'notice' => 'push']);
        $this->delete('{{%default_client_status_event}}', ['status_id' => '10', 'group' => 'WEB', 'notice' => 'nothing']);
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
