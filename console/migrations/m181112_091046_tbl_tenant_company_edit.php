<?php

use yii\db\Schema;
use yii\db\Migration;

class m181112_091046_tbl_tenant_company_edit extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%tenant_company}}', 'percent', Schema::TYPE_FLOAT);
        $this->addColumn('{{%tenant_company}}', 'street', Schema::TYPE_STRING);
        $this->addColumn('{{%tenant_company}}', 'city', Schema::TYPE_STRING);
        $this->addColumn('{{%tenant_company}}', 'city_code', Schema::TYPE_STRING);
        $this->addColumn('{{%tenant_company}}', 'tax_number', Schema::TYPE_STRING);
        $this->addColumn('{{%tenant_company}}', 'fax', Schema::TYPE_STRING);
        $this->addColumn('{{%tenant_company}}', 'site', Schema::TYPE_STRING);
        $this->addColumn('{{%tenant_company}}', 'user_contact', Schema::TYPE_INTEGER);


    }

    public function safeDown()
    {

        $this->dropColumn('{{%tenant_company}}', 'percent', Schema::TYPE_FLOAT);
        $this->dropColumn('{{%tenant_company}}', 'street', Schema::TYPE_STRING);
        $this->dropColumn('{{%tenant_company}}', 'city', Schema::TYPE_STRING);
        $this->dropColumn('{{%tenant_company}}', 'city_code', Schema::TYPE_STRING);
        $this->dropColumn('{{%tenant_company}}', 'tax_number', Schema::TYPE_STRING);
        $this->dropColumn('{{%tenant_company}}', 'fax', Schema::TYPE_STRING);
        $this->dropColumn('{{%tenant_company}}', 'site', Schema::TYPE_STRING);
        $this->dropColumn('{{%tenant_company}}', 'user_contact', Schema::TYPE_INTEGER);
        $this->dropColumn('{{%tenant_company}}', 'report_id', Schema::TYPE_INTEGER);

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181112_091046_tbl_tenant_company_edit cannot be reverted.\n";

        return false;
    }
    */
}
