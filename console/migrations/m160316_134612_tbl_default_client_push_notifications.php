<?php

use yii\db\Migration;

class m160316_134612_tbl_default_client_push_notifications extends Migration
{
    public function up()
    {
        $this->insert('{{%default_client_push_notifications}}', ['text' => 'Извините, в вашем районе нет машин', 'type' => '40']);
    }

    public function down()
    {
        echo "m160316_134612_tbl_default_client_push_notifications cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
