<?php

use yii\db\Migration;

class m170408_082133_tbl_order_status__rename_statuses extends Migration
{
    const TABLE_ORDER_STATUS = '{{%order_status}}';

    public function up()
    {
        $this->update(self::TABLE_ORDER_STATUS, ['name' => 'Offer to a worker'], ['status_id' => 2]);
        $this->update(self::TABLE_ORDER_STATUS, ['name' => 'Worker has refused'], ['status_id' => 3]);
        $this->update(self::TABLE_ORDER_STATUS, ['name' => 'Worker has accepted an order'], ['status_id' => 7]);
        $this->update(self::TABLE_ORDER_STATUS, ['name' => 'Worker order refusing'], ['status_id' => 10]);
        $this->update(self::TABLE_ORDER_STATUS, ['name' => 'Worker has accepted an order'], ['status_id' => 17]);
        $this->update(self::TABLE_ORDER_STATUS, ['name' => 'Worker has arrived'], ['status_id' => 26]);
        $this->update(self::TABLE_ORDER_STATUS, ['name' => 'A conflict with a worker'], ['status_id' => 41]);
        $this->update(self::TABLE_ORDER_STATUS, ['name' => 'Worker arrived late'], ['status_id' => 45]);
        $this->update(self::TABLE_ORDER_STATUS, ['name' => 'Worker is coming later'], ['status_id' => 54]);
        $this->update(self::TABLE_ORDER_STATUS, ['name' => 'Connection with a worker'], ['status_id' => 104]);
        $this->update(self::TABLE_ORDER_STATUS, ['name' => 'Worker ignored order offer'], ['status_id' => 109]);
        $this->update(self::TABLE_ORDER_STATUS, ['name' => 'Worker assigned at pre-order(soft)'], ['status_id' => 111]);
        $this->update(self::TABLE_ORDER_STATUS, ['name' => 'Worker assigned at pre-order(hard)'], ['status_id' => 112]);
        $this->update(self::TABLE_ORDER_STATUS, ['name' => 'Worker assigned at order(soft)'], ['status_id' => 113]);
        $this->update(self::TABLE_ORDER_STATUS, ['name' => 'Worker assigned at order(hard)'], ['status_id' => 114]);
        $this->update(self::TABLE_ORDER_STATUS, ['name' => 'Worker refused assignment of order'], ['status_id' => 115]);
    }

    public function down()
    {
    }

}
