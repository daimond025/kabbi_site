<?php

use yii\db\Migration;

class m180331_111521_tbl_card_payment_log__add_column__profile extends Migration
{
    const TABLE_CARD_PAYMENT_LOG = '{{%card_payment_log}}';
    const COLUMN_PROFILE = 'profile';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_CARD_PAYMENT_LOG, self::COLUMN_PROFILE, $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_CARD_PAYMENT_LOG, self::COLUMN_PROFILE);
    }

}
