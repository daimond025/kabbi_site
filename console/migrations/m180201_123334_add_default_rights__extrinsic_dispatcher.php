<?php

use yii\db\Migration;

class m180201_123334_add_default_rights__extrinsic_dispatcher extends Migration
{

    const POSITION_EXTRINSIC_DISPATCHER_ID = 14;
    const TABLE_NAME = '{{%user_default_right}}';

    const RIGHTS = [
        ['bankCard', 'write', self::POSITION_EXTRINSIC_DISPATCHER_ID],
        ['bonus', 'write', self::POSITION_EXTRINSIC_DISPATCHER_ID],
        ['car', 'read', self::POSITION_EXTRINSIC_DISPATCHER_ID],
        ['cars', 'off', self::POSITION_EXTRINSIC_DISPATCHER_ID],
        ['cities', 'write', self::POSITION_EXTRINSIC_DISPATCHER_ID],
        ['clients', 'off', self::POSITION_EXTRINSIC_DISPATCHER_ID],
        ['clientTariff', 'read', self::POSITION_EXTRINSIC_DISPATCHER_ID],
        ['drivers', 'off', self::POSITION_EXTRINSIC_DISPATCHER_ID],
        ['driverTariff', 'off', self::POSITION_EXTRINSIC_DISPATCHER_ID],
        ['exchange', 'write', self::POSITION_EXTRINSIC_DISPATCHER_ID],
        ['gootaxTariff', 'write', self::POSITION_EXTRINSIC_DISPATCHER_ID],
        ['notifications', 'off', self::POSITION_EXTRINSIC_DISPATCHER_ID],
        ['orders', 'write', self::POSITION_EXTRINSIC_DISPATCHER_ID],
        ['organization', 'off', self::POSITION_EXTRINSIC_DISPATCHER_ID],
        ['parkings', 'write', self::POSITION_EXTRINSIC_DISPATCHER_ID],
        ['phoneLine', 'write', self::POSITION_EXTRINSIC_DISPATCHER_ID],
        ['promoSystem', 'read', self::POSITION_EXTRINSIC_DISPATCHER_ID],
        ['publicPlace', 'write', self::POSITION_EXTRINSIC_DISPATCHER_ID],
        ['referralSystem', 'off', self::POSITION_EXTRINSIC_DISPATCHER_ID],
        ['reports', 'off', self::POSITION_EXTRINSIC_DISPATCHER_ID],
        ['settings', 'write', self::POSITION_EXTRINSIC_DISPATCHER_ID],
        ['streets', 'write', self::POSITION_EXTRINSIC_DISPATCHER_ID],
        ['tenant', 'write', self::POSITION_EXTRINSIC_DISPATCHER_ID],
        ['users', 'read', self::POSITION_EXTRINSIC_DISPATCHER_ID],
    ];

    public function safeUp()
    {
        $this->batchInsert(self::TABLE_NAME, ['permission', 'rights', 'position_id'], self::RIGHTS);
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_NAME, ['position_id' => self::POSITION_EXTRINSIC_DISPATCHER_ID]);
    }
}
