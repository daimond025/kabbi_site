<?php

use yii\db\Migration;

class m170828_110650_add_draft_column_in_phone_line_table extends Migration
{
    const TABLE_NAME = '{{%phone_line}}';
    const COLUMN_NAME = 'draft';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_NAME, $this->integer(1)->notNull()->defaultValue(1));

        $this->fillData();
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);
    }

    protected function fillData()
    {
        $this->update(self::TABLE_NAME, [self::COLUMN_NAME => 0], ['not', ['r_username' => '']]);
    }
}
