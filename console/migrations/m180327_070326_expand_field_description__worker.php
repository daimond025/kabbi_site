<?php

use yii\db\Migration;

class m180327_070326_expand_field_description__worker extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%worker}}', 'description', $this->string(1000));
    }

    public function safeDown()
    {
        return true;
    }

}
