<?php

use console\components\mysqlOnlineSchemaChange\MysqlOnlineSchemaChangeTrait;
use yii\db\Migration;

class m180718_062203_add_idx_tbl_operation extends Migration
{
    use MysqlOnlineSchemaChangeTrait;

    public function up()
    {
        return $this->schemaChange('tbl_operation', 'ADD INDEX tbl_operation_date_idx (date)');
    }

    public function down()
    {
        return $this->schemaChange('tbl_operation', 'DROP INDEX tbl_operation_date_idx');
    }
}
