<?php

use yii\db\Migration;

class m161004_063747_add_column_in_tenant_has_sms extends Migration
{

    const TABLE_NAME = "{{%tenant_has_sms}}";

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'default', $this->integer(1)->notNull()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'default');
    }

}
