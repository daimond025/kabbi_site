<?php

use yii\db\Schema;
use yii\db\Migration;

class m151031_094651_tbl_tenant_payment_extra_column extends Migration {

    const ADMIN_CHANGESET = '{{%tenant_admin_tariff_changeset}}';
    const ADMIN_TARIFF = '{{%tenant_admin_tariff}}';
    const TABLE_NAME = '{{%tenant_payment}}';

    public function up() {
        $this->addColumn(self::TABLE_NAME, 'payment_activity_begin_date', $this->datetime()
        );
        $this->createIndex( 'tariff_id_new', self::ADMIN_CHANGESET, 'tariff_id_new', false);
        $this->createIndex( 'tariff_id_old', self::ADMIN_CHANGESET, 'tariff_id_old', false);
        $this->addForeignKey('fk_new_tariff', self::ADMIN_CHANGESET, 'tariff_id_new', self::ADMIN_TARIFF, 'tariff_id');
        $this->addForeignKey('fk_old_tariff', self::ADMIN_CHANGESET, 'tariff_id_old', self::ADMIN_TARIFF, 'tariff_id');
    }

    public function down() {
        $this->dropColumn(self::TABLE_NAME, 'payment_activity_begin_date');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
