<?php

use yii\db\Migration;

class m160905_071727_add_user_position extends Migration
{
    const TABLE_NAME = '{{%user_position}}';

    public function up()
    {
        $this->insert(self::TABLE_NAME, ['name' => 'Manager']);
    }

    public function down()
    {
        echo "m160905_071727_add_user_position cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
