<?php

use yii\db\Migration;

class m160404_122900_create_tbl_admin_business_plan extends Migration
{
    
    const TABLE_NAME = '{{%admin_business_plan}}';
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'email' => $this->string(255),
            'phone' => $this->string(15),
            'create_time' => $this->dateTime()
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
