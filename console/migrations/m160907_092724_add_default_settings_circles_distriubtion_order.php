<?php

use yii\db\Migration;

class m160907_092724_add_default_settings_circles_distriubtion_order extends Migration
{
    public function up()
    {
        $this->insert('{{%default_settings}}', [
            'name' => 'CIRCLES_DISTRIBUTION_ORDER',
            'value' => 2,
            'type' => 'orders'
        ]);
    }

    public function down()
    {
        $this->delete('{{%default_settings}}', ['name' => 'CIRCLES_DISTRIBUTION_ORDER']);
    }
}
