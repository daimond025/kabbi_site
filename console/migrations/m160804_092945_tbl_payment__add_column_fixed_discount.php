<?php

use yii\db\Migration;

class m160804_092945_tbl_payment__add_column_fixed_discount extends Migration
{
    const TABLE_PAYMENT = '{{%payment}}';
    const COLUMN_FIXED_DISCOUNT = 'fixed_discount';

    public function up()
    {
        $this->addColumn(self::TABLE_PAYMENT, self::COLUMN_FIXED_DISCOUNT,
            $this->integer(1)->notNull()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_PAYMENT, self::COLUMN_FIXED_DISCOUNT);
    }

}
