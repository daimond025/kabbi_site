<?php

use yii\db\Schema;
use yii\db\Migration;
use common\modules\tenant\models\TenantSetting;

class m160122_113400_tbl_tenant_setting extends Migration
{
    public function up()
    {
        $tenant_settings = (new \yii\db\Query())
                ->from('{{%tenant_setting}}')
                ->where('type != "system"')
                ->all();
        $arCity = [];
        $arDefaultCity = [];
        foreach ($tenant_settings as $item) {
            if (!isset($arCity[$item['tenant_id']])) {
                $tenantHasCity = (new \yii\db\Query())
                    ->select('city_id')
                    ->from('{{%tenant_has_city}}')
                    ->where(['tenant_id' => $item['tenant_id']])
                    ->all();
                $tenantHasCity = \yii\helpers\ArrayHelper::getColumn($tenantHasCity, 'city_id');
                $arCity[$item['tenant_id']] = $tenantHasCity;
                $arDefaultCity[$item['tenant_id']] = array_shift($arCity[$item['tenant_id']]);
            }

            if (empty($item['city_id'])) {
                Yii::$app->db->createCommand()->update('{{%tenant_setting}}', ['city_id' => $arDefaultCity[$item['tenant_id']]], ['setting_id' => $item['setting_id']])->execute();
            }

            foreach ($arCity[$item['tenant_id']] as $cityItem) {
                Yii::$app->db->createCommand()->insert('{{%tenant_setting}}', [
                    'tenant_id' => $item['tenant_id'],
                    'name'      => $item['name'],
                    'value'     => $item['value'],
                    'type'      => $item['type'],
                    'city_id'   => $cityItem,
                ])->execute();
            }
        }
    }

    public function down()
    {
        echo "m160122_113400_tbl_tenant_setting cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
