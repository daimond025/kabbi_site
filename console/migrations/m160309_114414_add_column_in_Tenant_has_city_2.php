<?php


use yii\db\Migration;

class m160309_114414_add_column_in_Tenant_has_city_2 extends Migration
{
    
    const TABLE_TENANT_HAS_CITY = '{{%tenant_has_city}}';
    
    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    
    const TABLE_TENANT_SETTING = '{{%tenant_setting}}';
    
    
    public function up()
    {

        $this->dropColumn(self::TABLE_TENANT_HAS_CITY,'phone');
        $this->insert(self::TABLE_DEFAULT_SETTINGS,[
            'name' => 'PHONE',
            'type' => 'general',
        ]);

        
        $tenant_settings = (new \yii\db\Query())
                ->from('{{%tenant_has_city}}')
                ->all();
        
        foreach ($tenant_settings as $item){
            echo $item['city_id'] .PHP_EOL;
            $this->insert(self::TABLE_TENANT_SETTING,[
                'tenant_id' => $item['tenant_id'],
                'city_id' => $item['city_id'],
                'name' => 'PHONE',
                'type' => 'general',
            ]);
        }
        
        
        
        
    }

    public function down()
    {
        $this->delete(self::TABLE_TENANT_SETTING, ['name' => 'PHONE']);

        $this->delete(self::TABLE_DEFAULT_SETTINGS, ['name' => 'PHONE']);
        $this->addColumn(self::TABLE_TENANT_HAS_CITY, 'phone', $this->string(20));

    }

}
