<?php

use yii\db\Schema;
use yii\db\Migration;

class m150624_113857_tbl_transaction extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%transaction}}', 'comment', Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('{{%transaction}}', 'comment');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
