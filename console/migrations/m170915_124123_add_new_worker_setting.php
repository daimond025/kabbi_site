<?php

use yii\db\Migration;
use common\modules\tenant\models\DefaultSettings;

class m170915_124123_add_new_worker_setting extends Migration
{

    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const TABLE_TENANT_SETTING = '{{%tenant_setting}}';

    const SETTING_NAME_REQUIRE_PASSWORD_EVERY_TIME_LOG_IN_APPLICATION = 'REQUIRE_PASSWORD_EVERY_TIME_LOG_IN_APPLICATION';
    const SETTING_VALUE_REQUIRE_PASSWORD_EVERY_TIME_LOG_IN_APPLICATION = '0';
    const SETTING_TYPE_REQUIRE_PASSWORD_EVERY_TIME_LOG_IN_APPLICATION = 'drivers';

    public function safeUp()
    {
        $requirePassword = new DefaultSettings([
            'name'  => self::SETTING_NAME_REQUIRE_PASSWORD_EVERY_TIME_LOG_IN_APPLICATION,
            'value' => self::SETTING_VALUE_REQUIRE_PASSWORD_EVERY_TIME_LOG_IN_APPLICATION,
            'type'  => self::SETTING_TYPE_REQUIRE_PASSWORD_EVERY_TIME_LOG_IN_APPLICATION
        ]);

        $requirePassword->save();
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::SETTING_NAME_REQUIRE_PASSWORD_EVERY_TIME_LOG_IN_APPLICATION,
            'type' => self::SETTING_TYPE_REQUIRE_PASSWORD_EVERY_TIME_LOG_IN_APPLICATION,
        ]);

        $this->delete(self::TABLE_TENANT_SETTING, [
            'name' => self::SETTING_NAME_REQUIRE_PASSWORD_EVERY_TIME_LOG_IN_APPLICATION,
            'type' => self::SETTING_TYPE_REQUIRE_PASSWORD_EVERY_TIME_LOG_IN_APPLICATION,
        ]);
    }

}
