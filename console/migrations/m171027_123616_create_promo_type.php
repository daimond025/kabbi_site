<?php

use yii\db\Migration;

class m171027_123616_create_promo_type extends Migration
{

    const TABLE_NAME = '{{%promo_type}}';

    const COLUMN_TYPE_ID = 'type_id';
    const COLUMN_NAME = 'name';

    const DEFAULT_VALUE = [
        ['Manually'],
        ['Generation of different codes'],
        ['Codes for drivers'],
    ];

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            self::COLUMN_TYPE_ID => $this->primaryKey(),
            self::COLUMN_NAME    => $this->string(255)->notNull(),
        ]);


        $this->batchInsert(
            self::TABLE_NAME,
            [self::COLUMN_NAME],
            self::DEFAULT_VALUE
        );

    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }


}
