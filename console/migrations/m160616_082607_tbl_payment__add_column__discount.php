<?php

use yii\db\Migration;

class m160616_082607_tbl_payment__add_column__discount extends Migration
{
    const TABLE_PAYMENT = '{{%payment}}';
    const COLUMN_DISCOUNT = 'discount';

    public function up()
    {
        $this->addColumn(self::TABLE_PAYMENT, self::COLUMN_DISCOUNT, $this->decimal(12, 2));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_PAYMENT, self::COLUMN_DISCOUNT);
    }

}
