<?php

use yii\db\Schema;
use yii\db\Migration;

class m150618_053221_tbl_user extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%user}}', 'password_reset_token', Schema::TYPE_STRING . ' null');
    }

    public function down()
    {
        $this->alterColumn('{{%user}}', 'password_reset_token', Schema::TYPE_STRING . ' not null');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
