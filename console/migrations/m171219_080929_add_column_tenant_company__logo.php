<?php

use yii\db\Migration;

class m171219_080929_add_column_tenant_company__logo extends Migration
{

    const TABLE_NAME = '{{%tenant_company}}';

    const COLUMN = 'logo';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN, $this->string(255));
    }

    public function safeDown()
    {
       $this->dropColumn(self::TABLE_NAME, self::COLUMN);
    }

}
