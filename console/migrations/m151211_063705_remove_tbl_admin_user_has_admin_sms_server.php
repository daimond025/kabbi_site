<?php

use yii\db\Schema;
use yii\db\Migration;

class m151211_063705_remove_tbl_admin_user_has_admin_sms_server extends Migration
{
    const TABLE_NAME = '{{%admin_user_has_admin_sms_server}}';
    
    public function up()
    {
        $this->dropTable(self::TABLE_NAME);
    }

    public function down()
    {
        echo "m151211_063705_remove_tbl_admin_user_has_admin_sms_server cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
