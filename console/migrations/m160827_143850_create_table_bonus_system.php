<?php

use yii\db\Migration;

class m160827_143850_create_table_bonus_system extends Migration
{
    const TABLE_BONUS_SYSTEM = '{{%bonus_system}}';

    public function up()
    {
        $this->createTable(self::TABLE_BONUS_SYSTEM, [
            'id'         => $this->primaryKey(),
            'name'       => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $now = time();
        $this->batchInsert(self::TABLE_BONUS_SYSTEM, ['id', 'name', 'created_at', 'updated_at'], [
            [1, 'Gootax', $now, $now],
            [2, 'UDS Game', $now, $now],
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_BONUS_SYSTEM);
    }
}
