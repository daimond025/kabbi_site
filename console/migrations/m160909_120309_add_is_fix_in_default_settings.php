<?php

use yii\db\Migration;

class m160909_120309_add_is_fix_in_default_settings extends Migration
{
    const TABLE_NAME = '{{%default_settings}}';
    public function up()
    {
        $this->insert(self::TABLE_NAME, [
            'name' => 'IS_FIX',
            'value' => '0',
            'type' => 'orders'
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME, [
            'name' => 'IS_FIX'
        ]);
    }
}
