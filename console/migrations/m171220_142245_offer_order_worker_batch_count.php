<?php

use yii\db\Migration;
use common\modules\tenant\models\DefaultSettings;

class m171220_142245_offer_order_worker_batch_count extends Migration
{
    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const TABLE_TENANT_SETTING = '{{%tenant_setting}}';

    const SETTING_NAME_OFFER_ORDER_WORKER_BATCH_COUNT = 'OFFER_ORDER_WORKER_BATCH_COUNT';
    const SETTING_VALUE_OFFER_ORDER_WORKER_BATCH_COUNT = '2';
    const SETTING_TYPE_OFFER_ORDER_WORKER_BATCH_COUNT = DefaultSettings::TYPE_ORDERS;

    public function safeUp()
    {
        $requirePassword = new DefaultSettings([
            'name'  => self::SETTING_NAME_OFFER_ORDER_WORKER_BATCH_COUNT,
            'value' => self::SETTING_VALUE_OFFER_ORDER_WORKER_BATCH_COUNT,
            'type'  => self::SETTING_TYPE_OFFER_ORDER_WORKER_BATCH_COUNT
        ]);

        $requirePassword->save();
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::SETTING_NAME_OFFER_ORDER_WORKER_BATCH_COUNT,
            'type' => self::SETTING_TYPE_OFFER_ORDER_WORKER_BATCH_COUNT,
        ]);

        $this->delete(self::TABLE_TENANT_SETTING, [
            'name' => self::SETTING_NAME_OFFER_ORDER_WORKER_BATCH_COUNT,
            'type' => self::SETTING_TYPE_OFFER_ORDER_WORKER_BATCH_COUNT,
        ]);
    }
}
