<?php

use yii\db\Migration;

class m180212_110946_create_table__exchange_connection extends Migration
{

    const TABLE_NAME = '{{%exchange_connection}}';

    const FK_CITY = 'fk_exchange_connection__city';
    const FK_POSITION = 'fk_exchange_connection__position';
    const FK_PROGRAM = 'fk_exchange_connection__exchange_program';
    const FK_TENANT = 'fk_exchange_connection__tenant';

    const COLUMN_CITY_ID = 'city_id';
    const COLUMN_POSITION_ID = 'position_id';
    const COLUMN_PROGRAM_ID = 'exchange_program_id';
    const COLUMN_TENANT_ID = 'tenant_id';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'exchange_connection_id' => $this->primaryKey()->unsigned(),
            'name'                   => $this->string()->notNull(),
            self::COLUMN_PROGRAM_ID  => $this->integer()->unsigned()->notNull(),
            self::COLUMN_CITY_ID     => $this->integer()->unsigned()->notNull(),
            self::COLUMN_POSITION_ID => $this->integer()->notNull(),
            'block'                  => 'ENUM("0", "1") NOT NULL',
            'clid'                   => $this->string()->notNull(),
            'key_api'                => $this->string()->notNull(),
            'rate'                   => $this->integer()->notNull(),
            'is_show_automate'       => 'ENUM("0", "1") NOT NULL',
            'sort'                   => $this->integer(),
            self::COLUMN_TENANT_ID   => $this->integer()->unsigned()->notNull(),
        ]);

        $this->addForeignKey(self::FK_CITY, self::TABLE_NAME, self::COLUMN_CITY_ID, '{{%city}}', self::COLUMN_CITY_ID, 'CASCADE', 'CASCADE');

        $this->addForeignKey(self::FK_POSITION, self::TABLE_NAME, self::COLUMN_POSITION_ID, '{{%position}}', self::COLUMN_POSITION_ID, 'CASCADE', 'CASCADE');

        $this->addForeignKey(self::FK_PROGRAM, self::TABLE_NAME, self::COLUMN_PROGRAM_ID, '{{%exchange_program}}', self::COLUMN_PROGRAM_ID, 'CASCADE', 'CASCADE');

        $this->addForeignKey(self::FK_TENANT, self::TABLE_NAME, self::COLUMN_TENANT_ID, '{{%tenant}}', self::COLUMN_TENANT_ID, 'CASCADE', 'CASCADE');

    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_TENANT, self::TABLE_NAME);
        $this->dropForeignKey(self::FK_PROGRAM, self::TABLE_NAME);
        $this->dropForeignKey(self::FK_POSITION, self::TABLE_NAME);
        $this->dropForeignKey(self::FK_CITY, self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
    }

}
