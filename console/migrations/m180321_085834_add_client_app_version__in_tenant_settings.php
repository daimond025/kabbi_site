<?php

use yii\db\Migration;

class m180321_085834_add_client_app_version__in_tenant_settings extends Migration
{
    const TABLE_NAME_DEFAULT_SETTING = '{{%default_settings}}';
    const TABLE_NAME_TENANT_SETTING = '{{%tenant_setting}}';
    const ANDROID_CLIENT_APP_VERSION = 'ANDROID_CLIENT_APP_VERSION';
    const IOS_CLIENT_APP_VERSION = 'IOS_CLIENT_APP_VERSION';

    public function safeUp()
    {
        $this->insert(self::TABLE_NAME_DEFAULT_SETTING, [
            'name'  => self::ANDROID_CLIENT_APP_VERSION,
            'value' => '1.0.0',
            'type'  => 'system',
        ]);

        $this->insert(self::TABLE_NAME_DEFAULT_SETTING, [
            'name'  => self::IOS_CLIENT_APP_VERSION,
            'value' => '1.0.0',
            'type'  => 'system',
        ]);

        $this->fillTenantSettings();
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_NAME_TENANT_SETTING, ['name' => self::ANDROID_CLIENT_APP_VERSION]);
        $this->delete(self::TABLE_NAME_TENANT_SETTING, ['name' => self::IOS_CLIENT_APP_VERSION]);

        $this->delete(self::TABLE_NAME_DEFAULT_SETTING, ['name' => self::ANDROID_CLIENT_APP_VERSION]);
        $this->delete(self::TABLE_NAME_DEFAULT_SETTING, ['name' => self::IOS_CLIENT_APP_VERSION]);
    }


    protected function fillTenantSettings()
    {
        $tenantList = $this->getTenantList();
        $inserts     = [];
        foreach ($tenantList as $tenant) {
            $inserts[] = [
                'tenant_id' => $tenant['tenant_id'],
                'name'      => self::ANDROID_CLIENT_APP_VERSION,
                'value'     => '1.0.0',
                'type'      => 'system',
            ];

            $inserts[] = [
                'tenant_id' => $tenant['tenant_id'],
                'name'      => self::IOS_CLIENT_APP_VERSION,
                'value'     => '1.0.0',
                'type'      => 'system',
            ];
        }

        if (!empty($inserts)) {
            $columns     = array_keys(reset($inserts));
            $insertChunks = array_chunk($inserts, 100);
            foreach ($insertChunks as $array) {
                $this->batchInsert(self::TABLE_NAME_TENANT_SETTING, $columns, $array);
            }
        }
    }


    private function getTenantList()
    {
        return (new \yii\db\Query())->from('{{%tenant}}')->all();
    }
}
