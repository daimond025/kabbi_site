<?php

use yii\db\Migration;

/**
 * Handles the creation of table `phone_line_outbound_routing`.
 */
class m170815_111809_create_phone_line_outbound_routing_table extends Migration
{
    const TABLE_NAME = '{{%phone_line_outbound_routing}}';
    const TABLE_TENANT = '{{%tenant}}';
    const TABLE_CITY = '{{%city}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'routing_id' => $this->primaryKey()->unsigned(),
            'tenant_id'  => $this->integer(10)->unsigned()->notNull(),
            'city_id'    => $this->integer(10)->unsigned()->notNull(),
            'name'       => $this->string(100)->notNull(),
            'sort'       => $this->integer(6)->unsigned()->notNull()->defaultValue(100),
            'active'     => $this->integer(1)->unsigned()->notNull()->defaultValue(0),
        ]);

        $this->addForeignKey('FK_tenant_id__phone_line_outbound_routing', self::TABLE_NAME, 'tenant_id',
            self::TABLE_TENANT, 'tenant_id', 'CASCADE', 'CASCADE');


        $this->addForeignKey('FK_city_id__phone_line_outbound_routing', self::TABLE_NAME, 'city_id',
            self::TABLE_CITY, 'city_id', 'CASCADE', 'CASCADE');

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('FK_city_id__phone_line_outbound_routing', self::TABLE_NAME);
        $this->dropForeignKey('FK_tenant_id__phone_line_outbound_routing', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
    }
}
