<?php

use yii\db\Schema;
use yii\db\Migration;

class m151009_060644_tbl_street_shortname_to_full_additional extends Migration {

    const TABLE_NAME = '{{%street}}';

    public function up() {
        $this->execute("UPDATE" . self::TABLE_NAME . " SET street_shortname = REPLACE(street_shortname, 'пр', 'проезд') where street_shortname='пр'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET street_shortname = REPLACE(street_shortname, 'пер', 'переулок') where street_shortname='пер'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET street_shortname = REPLACE(street_shortname, 'пр-кт', 'проспект') where street_shortname='пр-кт'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET street_shortname = REPLACE(street_shortname, 'пл', 'площадь') where street_shortname='пл'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET street_shortname = REPLACE(street_shortname, 'наб', 'набережная') where street_shortname='наб'");
    }

    public function down() {
        $this->execute("UPDATE" . self::TABLE_NAME . " SET street_shortname = REPLACE(street_shortname, 'переулок', 'пер') where street_shortname='переулок'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET street_shortname = REPLACE(street_shortname, 'проезд', 'пр') where street_shortname='проезд'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET street_shortname = REPLACE(street_shortname, 'проспект', 'пр-кт') where street_shortname='проспект'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET street_shortname = REPLACE(street_shortname, 'площадь', 'пл') where street_shortname='площадь'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET street_shortname = REPLACE(street_shortname, 'набережная', 'наб') where street_shortname='набережная'");
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
