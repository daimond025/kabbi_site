<?php

use yii\db\Schema;
use yii\db\Migration;

class m150618_053222_tbl_order extends Migration
{
    public function up()
    {
        $this->dropColumn('{{%order}}', 'phone_id');
        $this->addColumn('{{%order}}', 'phone', Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('{{%order}}', 'phone');
        $this->addColumn('{{%order}}', 'phone_id', Schema::TYPE_INTEGER);
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
