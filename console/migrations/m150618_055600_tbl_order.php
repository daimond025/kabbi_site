<?php

use yii\db\Schema;
use yii\db\Migration;

class m150618_055600_tbl_order extends Migration
{
    public function up()
    {
        $this->addColumn('{{%order}}', 'client_id', 'INT(10) UNSIGNED NOT NULL');
        $this->createIndex('fk_tbl_order_tbl_client1_idx', '{{%order}}', 'client_id');
    }

    public function down()
    {
        $this->dropIndex('fk_tbl_order_tbl_client1_idx', '{{%order}}');
        $this->dropColumn('{{%order}}', 'client_id');
    }
    

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
