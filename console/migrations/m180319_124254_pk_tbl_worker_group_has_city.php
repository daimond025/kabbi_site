<?php

use yii\db\Migration;

class m180319_124254_pk_tbl_worker_group_has_city extends Migration
{
    const TABLE_NAME = '{{%worker_group_has_city}}';
    const PK_NAME    = 'PK1';

    public function safeUp()
    {
        $this->addColumn(
            self::TABLE_NAME,
            'id',
            'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY'
        );
        return true;
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'id');
        return true;
    }
}
