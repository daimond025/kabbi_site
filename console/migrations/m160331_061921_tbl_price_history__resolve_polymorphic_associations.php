<?php

use yii\db\Migration;

class m160331_061921_tbl_price_history__resolve_polymorphic_associations extends Migration
{
    const TABLE_PRICE_HISTORY = '{{%price_history}}';
    const TABLE_TARIFF_ADDITIONAL_OPTION = '{{%tariff_additional_option}}';
    const TABLE_ONE_TIME_SERVICE = '{{%one_time_service}}';
    const TABLE_TARIFF = '{{%tariff}}';

    const COLUMN_ENTITY_ID = 'entity_id';
    const COLUMN_OPTION_ID = 'option_id';
    const COLUMN_SERVICE_ID = 'service_id';
    const COLUMN_TARIFF_ID = 'tariff_id';

    const FK_ADDITIONAL_OPTION_ID = 'fk_tbl_price_history__option_id';
    const FK_ONE_TIME_SERVICE_ID = 'fk_tbl_price_history__service_id';
    const FK_TARIFF_ID = 'fk_tbl_price_history__tariff_id';

    public function up()
    {
        $this->dropColumn(self::TABLE_PRICE_HISTORY, self::COLUMN_ENTITY_ID);

        $this->addColumn(self::TABLE_PRICE_HISTORY, self::COLUMN_OPTION_ID, $this->integer());
        $this->addColumn(self::TABLE_PRICE_HISTORY, self::COLUMN_SERVICE_ID, $this->integer());
        $this->addColumn(self::TABLE_PRICE_HISTORY, self::COLUMN_TARIFF_ID, $this->integer());

        $this->addForeignKey(self::FK_ADDITIONAL_OPTION_ID, self::TABLE_PRICE_HISTORY, self::COLUMN_OPTION_ID,
            self::TABLE_TARIFF_ADDITIONAL_OPTION, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey(self::FK_ONE_TIME_SERVICE_ID, self::TABLE_PRICE_HISTORY, self::COLUMN_SERVICE_ID,
            self::TABLE_ONE_TIME_SERVICE, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey(self::FK_TARIFF_ID, self::TABLE_PRICE_HISTORY, self::COLUMN_TARIFF_ID,
            self::TABLE_TARIFF, 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey(self::FK_ADDITIONAL_OPTION_ID, self::TABLE_PRICE_HISTORY);
        $this->dropForeignKey(self::FK_ONE_TIME_SERVICE_ID, self::TABLE_PRICE_HISTORY);
        $this->dropForeignKey(self::FK_TARIFF_ID, self::TABLE_PRICE_HISTORY);

        $this->dropColumn(self::TABLE_PRICE_HISTORY, self::COLUMN_OPTION_ID);
        $this->dropColumn(self::TABLE_PRICE_HISTORY, self::COLUMN_SERVICE_ID);
        $this->dropColumn(self::TABLE_PRICE_HISTORY, self::COLUMN_TARIFF_ID);

        $this->addColumn(self::TABLE_PRICE_HISTORY, self::COLUMN_ENTITY_ID, $this->integer()->notNull());
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
