<?php

use yii\db\Migration;

class m160930_130338_start_time_by_order extends Migration
{
    const TABLE_NAME = '{{%default_settings}}';
    const NAME = 'START_TIME_BY_ORDER';

    public function up()
    {
        $this->insert(self::TABLE_NAME, [
            'name' => self::NAME,
            'value' => '30',
            'type' => 'orders'
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME, [
            'name' => self::NAME
        ]);
    }
}
