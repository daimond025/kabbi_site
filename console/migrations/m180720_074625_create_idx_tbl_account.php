<?php

use yii\db\Migration;

class m180720_074625_create_idx_tbl_account extends Migration
{
    public function safeUp()
    {
        $this->createIndex('tbl_account_tenant_id_acc_kind_id_idx', '{{%account}}', [
            'tenant_id',
            'acc_kind_id',
        ]);
    }

    public function safeDown()
    {
        $this->dropIndex('tbl_account_tenant_id_acc_kind_id_idx', '{{%account}}');
    }

}
