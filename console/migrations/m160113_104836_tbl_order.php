<?php

use yii\db\Schema;
use yii\db\Migration;

class m160113_104836_tbl_order extends Migration
{
    public function up()
    {
        $this->addColumn('{{%order}}', 'currency_id', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('{{%order}}', 'currency_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
