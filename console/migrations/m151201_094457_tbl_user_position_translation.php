<?php

use yii\db\Schema;
use yii\db\Migration;

class m151201_094457_tbl_user_position_translation extends Migration
{
    const TABLE_NAME = '{{%user_position}}';

    public function up()
    {
        $this->execute("UPDATE" . self::TABLE_NAME . " SET name = 'Founder' where position_id='1'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET name = 'General director' where position_id='2'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET name = 'Technical director' where position_id='3'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET name = 'Executive director' where position_id='4'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET name = 'Sysadmin' where position_id='5'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET name = 'Dispatcher' where position_id='6'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET name = 'Administrator (manager)' where position_id='7'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET name = 'Chief dispatcher' where position_id='8'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET name = 'Bookkeeper' where position_id='9'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET name = 'Chief bookkeeper' where position_id='10'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET name = 'Branch manager' where position_id='11'");
    }

    public function down()
    {
        echo "m151201_094457_tbl_user_position_translation cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
