<?php

use yii\db\Migration;

class m160407_213639_tbl_payment_for_additional_option__add_column extends Migration
{
    const TABLE_PAYMENT_FOR_ADDITIONAL_OPTION = '{{%payment_for_additional_option}}';
    const COLUMN_STARTED_AT = 'started_at';

    public function up()
    {
        $this->addColumn(self::TABLE_PAYMENT_FOR_ADDITIONAL_OPTION, self::COLUMN_STARTED_AT, $this->integer()->notNull());
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_PAYMENT_FOR_ADDITIONAL_OPTION, self::COLUMN_STARTED_AT);;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
