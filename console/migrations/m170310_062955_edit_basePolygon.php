<?php

use yii\db\Migration;

class m170310_062955_edit_basePolygon extends Migration
{

    const TABLE_NAME = '{{%parking}}';

    private $_translates = [];



    public function up()
    {
        echo '    > update table `tbl_parking` ...';
        $time = microtime(true);


        $sql = "UPDATE `tbl_parking` p
  LEFT JOIN (
    SELECT t.`tenant_id`, t.`city_id`, MIN(`value`) `lang`
      FROM `tbl_tenant_setting` t
      WHERE t.`name` = 'LANGUAGE'
    GROUP BY t.`tenant_id`, t.`city_id`
  ) t ON p.`tenant_id` = t.`tenant_id`
     AND p.`city_id` = t.`city_id`
SET p.`name` = CASE IFNULL(t.`lang`, 'en_US')
            WHEN 'ru' THEN 'Базовая парковка (границы города)'
            WHEN 'en-US' THEN 'Basic parking zone (town outskirts)'
            ELSE 'Basic parking zone (town outskirts)'
         END
WHERE p.`type` = 'basePolygon'";

        app()->db->createCommand($sql)->execute();

        echo ' done (time: ' . sprintf('%.3f', microtime(true) - $time) . 's)' . PHP_EOL;
    }

    public function down()
    {
        echo '    > empty' . PHP_EOL;
    }

    public function getTranslate($lang = 'en')
    {
        if(!array_key_exists($lang, $this->_translates)) {
            $this->_translates[$lang] = t('parking', 'Basic parking zone (outskirts)', [], $lang);
        }

        return $this->_translates[$lang];
    }
}
