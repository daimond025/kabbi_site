<?php

use yii\db\Migration;

class m171026_114008_sms_provider_mediasend extends Migration
{
    const TABLE_NAME = '{{%sms_server}}';

    const COLUMN_SERVER_ID = 22;

    public function up()
    {
        $this->insert(self::TABLE_NAME, [
            'server_id' => self::COLUMN_SERVER_ID,
            'name'      => 'MediaSend.kz',
            'host'      => 'https://mediasend.kz',
            'active'    => '1',
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME, ['server_id' => self::COLUMN_SERVER_ID]);
    }
}
