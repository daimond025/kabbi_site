<?php

use yii\db\Schema;
use yii\db\Migration;

class m151216_103235_tbl_transaction__add_column_order_id extends Migration
{
    const TABLE_NAME = '{{%transaction}}';
    const COLUMN_NAME = 'order_id';
    const INDEX_NAME = 'fk_transaction__order_id';

    public function up()
    {
        $this->execute('SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0');
        $this->execute('SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0');
        $this->addColumn(self::TABLE_NAME, self::COLUMN_NAME, 'INT(10) UNSIGNED');
        $this->addForeignKey(self::INDEX_NAME, self::TABLE_NAME,
                self::COLUMN_NAME, '{{%order}}', self::COLUMN_NAME);
        $this->execute('SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS');
        $this->execute('SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS');
    }

    public function down()
    {
        $this->dropForeignKey(self::INDEX_NAME, self::TABLE_NAME);
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
