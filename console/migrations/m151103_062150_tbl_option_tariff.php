<?php

use yii\db\Schema;
use yii\db\Migration;

class m151103_062150_tbl_option_tariff extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%option_tariff}}', 'next_km_price_night', Schema::TYPE_FLOAT . ' DEFAULT 0');
    }

    public function down()
    {
        echo "m151103_062150_tbl_option_tariff cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
