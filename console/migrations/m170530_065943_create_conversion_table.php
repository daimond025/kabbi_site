<?php

use yii\db\Migration;

/**
 * Handles the creation of table `conversion`.
 */
class m170530_065943_create_conversion_table extends Migration
{
    const TABLE_NAME = '{{%conversion}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'conversion_id' => $this->primaryKey()->unsigned(),
            'name' => $this->string(100)->notNull(),
            'city' => $this->string(100)->notNull(),
            'contact' =>  $this->string(100)->notNull(),
            'create_time' => \yii\db\Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
