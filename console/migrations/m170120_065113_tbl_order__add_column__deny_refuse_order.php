<?php

use yii\db\Migration;

class m170120_065113_tbl_order__add_column__deny_refuse_order extends Migration
{
    const TABLE_ORDER = '{{%order}}';
    const COLUMN_DENY_REFUSE_ORDER = 'deny_refuse_order';

    public function up()
    {
        $this->addColumn(
            self::TABLE_ORDER,
            self::COLUMN_DENY_REFUSE_ORDER,
            $this->integer()->defaultValue(0)
        );
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_ORDER, self::COLUMN_DENY_REFUSE_ORDER);
    }

}
