<?php

use yii\db\Migration;

class m160324_131589_remove_old_tenant_tariff_tables extends Migration
{
    const TABLE_TENANT_TARIFF = '{{%tenant_tariff}}';
    const TABLE_TENANT_PERMISSION = '{{%tenant_permission}}';
    const TABLE_TARIFF_USER_PERMISSION = '{{%tariff_user_permission}}';
    const TABLE_TARIFF_PERMISSION = '{{%tariff_permission}}';
    const TABLE_USER_PERMISSION = '{{%user_permission}}';
    const TABLE_TARIFF = '{{%tariff}}';

    public function tableExists($tableName)
    {
        $tableName = $this->db->schema->getRawTableName($tableName);

        return in_array($tableName, Yii::$app->db->schema->tableNames);
    }

    public function dropTableIfExists($tableName)
    {
        if ($this->tableExists($tableName)) {
            $this->dropTable($tableName);
        }
    }

    public function up()
    {
        $this->dropTableIfExists(self::TABLE_TENANT_TARIFF);
        $this->dropTableIfExists(self::TABLE_TARIFF_PERMISSION);
        $this->dropTableIfExists(self::TABLE_TARIFF_USER_PERMISSION);
        $this->dropTableIfExists(self::TABLE_USER_PERMISSION);
        $this->dropTableIfExists(self::TABLE_TENANT_PERMISSION);
        $this->dropTableIfExists(self::TABLE_TARIFF);
    }

    public function down()
    {
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
