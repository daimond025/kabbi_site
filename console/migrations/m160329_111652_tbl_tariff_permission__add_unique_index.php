<?php

use yii\db\Migration;

class m160329_111652_tbl_tariff_permission__add_unique_index extends Migration
{
    const TABLE_NAME = '{{%tariff_permission}}';
    const INDEX_NAME = 'ui_tbl_tariff_permission__tariff_id__permission_id';

    public function up()
    {
        $this->createIndex(self::INDEX_NAME, self::TABLE_NAME, ['tariff_id', 'permission_id'], true);
    }

    public function down()
    {
        // this index needed for foreign key
        // for removing unique index from the table
        // you need to create the new migration which
        // added new indexes for foreign key constrains
        //$this->dropIndex(self::INDEX_NAME, self::TABLE_NAME);
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
