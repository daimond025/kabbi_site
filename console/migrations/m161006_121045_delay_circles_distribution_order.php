<?php

use yii\db\Migration;

class m161006_121045_delay_circles_distribution_order extends Migration
{
    const TABLE_NAME = '{{%default_settings}}';
    const COLUMN_NAME = 'DELAY_CIRCLES_DISTRIBUTION_ORDER';

    public function up()
    {
        $this->insert(self::TABLE_NAME, [
            'name' => self::COLUMN_NAME,
            'value' => '10',
            'type' => 'orders'
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME, [
            'name' => self::COLUMN_NAME,
        ]);
    }
}
