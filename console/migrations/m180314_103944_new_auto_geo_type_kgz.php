<?php

use yii\db\Migration;

class m180314_103944_new_auto_geo_type_kgz extends Migration
{
    const TABLE_NAME = '{{%auto_geo_service_type}}';

    public function up()
    {
        $this->insert(self::TABLE_NAME, [
            'id'            => 9,
            'service_label' => 'Geobase of Kyrgyzstan',
            'active'        => 1,
        ]);

    }

    public function down()
    {
        $this->delete(self::TABLE_NAME, ['id' => 9]);
        return true;
    }
}
