<?php

use yii\db\Schema;
use yii\db\Migration;

class m151113_074938_tbl_republic_add_lang extends Migration
{

    public function up()
    {

        $this->addColumn('{{%republic}}', 'name_en', 'VARCHAR(100) DEFAULT  NULL');
        $this->addColumn('{{%republic}}', 'name_az', 'VARCHAR(100) DEFAULT  NULL');
        $this->addColumn('{{%republic}}', 'name_de', 'VARCHAR(100) DEFAULT  NULL');
        $this->addColumn('{{%republic}}', 'name_se', 'VARCHAR(100) DEFAULT  NULL');

        $this->addColumn('{{%republic}}', 'shortname_en', 'VARCHAR(50) DEFAULT  NULL');
        $this->addColumn('{{%republic}}', 'shortname_az', 'VARCHAR(50) DEFAULT  NULL');
        $this->addColumn('{{%republic}}', 'shortname_de', 'VARCHAR(50) DEFAULT  NULL');
        $this->addColumn('{{%republic}}', 'shortname_se', 'VARCHAR(50) DEFAULT  NULL');
    }

    public function down()
    {
        $this->dropColumn('{{%republic}}', 'name_en');
        $this->dropColumn('{{%republic}}', 'name_az');
        $this->dropColumn('{{%republic}}', 'name_de');
        $this->dropColumn('{{%republic}}', 'name_se');

        $this->dropColumn('{{%republic}}', 'shortname_en');
        $this->dropColumn('{{%republic}}', 'shortname_az');
        $this->dropColumn('{{%republic}}', 'shortname_de');
        $this->dropColumn('{{%republic}}', 'shortname_se');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
