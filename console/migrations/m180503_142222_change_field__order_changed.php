<?php

use yii\db\Migration;
use console\components\mysqlOnlineSchemaChange\MysqlOnlineSchemaChange;

class m180503_142222_change_field__order_changed extends Migration
{

    const TABLE_NAME = 'tbl_order_change_data';

    const COLUMN_NAME = 'change_val';

    public function up()
    {
        /**
         * @var $changer MysqlOnlineSchemaChange
         */
        $changer = \Yii::$app->mysqlOnlineSchemaChange;

        try {
            $changer->run(self::TABLE_NAME, "CHANGE " . self::COLUMN_NAME . " " . self::COLUMN_NAME . " varchar(2000)",
                [
                    'alterForeignKeysMethod' => $changer::ALTER_FOREIGN_KEY_DROP_SWAP,
                    'execute'                => true,
                    'debug'                  => false
                ]);
            return true;
        } catch (RuntimeException $ex) {
            echo $ex->getMessage() . PHP_EOL;
            return false;
        }
    }

    public function down()
    {
        /**
         * @var $changer MysqlOnlineSchemaChange
         */
        $changer = \Yii::$app->mysqlOnlineSchemaChange;

        try {
            $changer->run(self::TABLE_NAME, "CHANGE " . self::COLUMN_NAME . " " . self::COLUMN_NAME . " varchar(300)",
                [
                    'alterForeignKeysMethod' => $changer::ALTER_FOREIGN_KEY_DROP_SWAP,
                    'execute'                => true,
                    'debug'                  => false
                ]);
            return true;
        } catch (RuntimeException $ex) {
            echo $ex->getMessage() . PHP_EOL;
            return false;
        }
    }

}
