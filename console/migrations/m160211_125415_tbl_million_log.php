<?php

use yii\db\Schema;
use yii\db\Migration;

class m160211_125415_tbl_million_log extends Migration
{
    const TABLE_NAME = '{{%million_log}}';
    
    
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'log_id' => $this->primaryKey(),
            'type' => $this->string(255),
            'apikey' => $this->string(255),
            'data' => $this->string(255),
            'result' => $this->string(255),
            'create_time' => $this->integer(11)
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
