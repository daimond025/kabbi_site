<?php

use yii\db\Migration;

class m170202_115243_worker_option_tariff extends Migration
{
    const TABLE_NAME = '{{%worker_option_tariff}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'increase_order_sum', $this->decimal(10, 2));
        $this->addColumn(self::TABLE_NAME, 'increase_sum_limit', $this->decimal(10, 2));
        $this->addColumn(self::TABLE_NAME, 'increase_sum_limit_type', $this->string());
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'increase_order_sum');
        $this->dropColumn(self::TABLE_NAME, 'increase_sum_limit');
        $this->dropColumn(self::TABLE_NAME, 'increase_sum_limit_type');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
