<?php

use yii\db\Schema;
use yii\db\Migration;

class m160126_094135_tbl_client_push_notifications extends Migration
{
    public function up()
    {
        $this->execute('SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0');
        $this->execute('SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0');

        $this->addColumn('{{%client_push_notifications}}', 'city_id', 'INT(11) UNSIGNED NULL DEFAULT NULL');
        $this->createIndex('FK_client_push_notifications_idx', '{{%client_push_notifications}}', 'city_id');
        $this->addForeignKey('FK_client_push_notifications', '{{%client_push_notifications}}', 'city_id', '{{%city}}', 'city_id');

        $this->execute('SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS');
        $this->execute('SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS');
    }

    public function down()
    {
        $this->dropForeignKey('FK_client_push_notifications', '{{%client_push_notifications}}');
        $this->dropIndex('FK_client_push_notifications_idx', '{{%client_push_notifications}}');
        $this->dropColumn('{{%client_push_notifications}}', 'city_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
