<?php

use yii\db\Migration;

class m180416_071122_add_fields__tbl_worker_active_aboniment extends Migration
{

    const FIELD_TYPE = 'type';
    const FIELD_ACTION = 'action_new_shift';
    const FIELD_PERIOD_TYPE = 'period_type';
    const FIELD_PERIOD = 'period';
    const FIELD_COST = 'cost';
    const FIELD_SUBSCRIPTION = 'subscription_limit_type';
    const FIELD_DAYS = 'days';


    const FIELD_CREATED_AT = 'created_at';
    const FIELD_UPDATED_AT = 'updated_at';

    const TABLE_NAME = 'tbl_worker_active_aboniment';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::FIELD_TYPE, $this->string(255));
        $this->addColumn(self::TABLE_NAME, self::FIELD_ACTION, $this->string(255));
        $this->addColumn(self::TABLE_NAME, self::FIELD_PERIOD_TYPE, $this->string(255));
        $this->addColumn(self::TABLE_NAME, self::FIELD_PERIOD, $this->string(255));
        $this->addColumn(self::TABLE_NAME, self::FIELD_COST, $this->decimal(12, 2));
        $this->addColumn(self::TABLE_NAME, self::FIELD_SUBSCRIPTION, $this->string(255));
        $this->addColumn(self::TABLE_NAME, self::FIELD_DAYS, $this->integer()->unsigned());

        $this->addColumn(self::TABLE_NAME, self::FIELD_CREATED_AT, $this->integer()->unsigned());
        $this->addColumn(self::TABLE_NAME, self::FIELD_UPDATED_AT, $this->integer()->unsigned());
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::FIELD_TYPE);
        $this->dropColumn(self::TABLE_NAME, self::FIELD_ACTION);
        $this->dropColumn(self::TABLE_NAME, self::FIELD_PERIOD_TYPE);
        $this->dropColumn(self::TABLE_NAME, self::FIELD_PERIOD);
        $this->dropColumn(self::TABLE_NAME, self::FIELD_COST);
        $this->dropColumn(self::TABLE_NAME, self::FIELD_SUBSCRIPTION);
        $this->dropColumn(self::TABLE_NAME, self::FIELD_DAYS);

        $this->dropColumn(self::TABLE_NAME, self::FIELD_CREATED_AT);
        $this->dropColumn(self::TABLE_NAME, self::FIELD_UPDATED_AT);
    }
}
