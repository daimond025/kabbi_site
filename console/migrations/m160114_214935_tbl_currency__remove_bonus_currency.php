<?php

use yii\db\Schema;
use yii\db\Migration;

class m160114_214935_tbl_currency__remove_bonus_currency extends Migration
{
    const TABLE_NAME        = '{{%currency}}';
    const CURRENCY_ID_BONUS = 5;

    public function up()
    {
        $this->delete(self::TABLE_NAME, 'currency_id = :id', [
            'id' => self::CURRENCY_ID_BONUS,
        ]);
    }

    public function down()
    {
        $this->insert(self::TABLE_NAME, [
            'currency_id' => self::CURRENCY_ID_BONUS,
            'type_id'     => 2,
            'name'        => 'Bonus',
            'code'        => 'BONUS',
            'symbol'      => 'B',
        ]);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
