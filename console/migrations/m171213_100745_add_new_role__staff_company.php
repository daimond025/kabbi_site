<?php

use yii\db\Migration;

class m171213_100745_add_new_role__staff_company extends Migration
{

    const ROLE_NAME = 'staff_company';

    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        $role = $auth->createRole(self::ROLE_NAME);
        $role->description = 'Staff Company';
        $auth->add($role);
    }

    public function safeDown()
    {
        $this->delete('{{%auth_assignment}}', ['item_name' => self::ROLE_NAME]);
        $this->delete('{{%auth_item}}', ['name' => self::ROLE_NAME]);
    }
}
