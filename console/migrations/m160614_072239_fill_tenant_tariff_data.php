<?php

use yii\db\Migration;

class m160614_072239_fill_tenant_tariff_data extends Migration
{
    const TABLE_TENANT_PERMISSION = '{{%tenant_permission}}';
    const TABLE_TENANT_TARIFF = '{{%tenant_tariff}}';
    const TABLE_TENANT_ADDITIONAL_OPTION = '{{%tenant_additional_option}}';

    const TABLE_PAYMENT_FOR_TARIFF = '{{%payment_for_tariff}}';
    const TABLE_PAYMENT_FOR_ADDITIONAL_OPTION = '{{%payment_for_additional_option}}';
    const TABLE_PAYMENT_FOR_ONE_TIME_SERVICE = '{{%payment_for_one_time_service}}';
    const TABLE_PAYMENT = '{{%payment}}';

    const TABLE_DEMO_TARIFF = '{{%demo_tariff}}';

    const TABLE_PRICE_HISTORY = '{{%price_history}}';
    const TABLE_TARIFF_PERMISSION_HISTORY = '{{%tariff_permission_history}}';

    const TABLE_TARIFF_PERMISSION = '{{%tariff_permission}}';
    const TABLE_TARIFF = '{{%tariff}}';
    const TABLE_TARIFF_ADDITIONAL_OPTION = '{{%tariff_additional_option}}';
    const TABLE_ONE_TIME_SERVICE = '{{%one_time_service}}';
    const TABLE_PERMISSION = '{{%permission}}';

    const TABLE_TARIFF_ENTITY_TYPE = '{{%tariff_entity_type}}';
    const TABLE_MODULE = '{{%module}}';

    const ENTITY_TYPE_TARIFF = 1;
    const ENTITY_TYPE_OPTION = 2;
    const ENTITY_TYPE_SERVICE = 3;

    const DEMO_TARIFF = 5;

    private function clearTables()
    {
        $this->delete(self::TABLE_TENANT_PERMISSION);
        $this->delete(self::TABLE_TENANT_TARIFF);
        $this->delete(self::TABLE_TENANT_ADDITIONAL_OPTION);

        $this->delete(self::TABLE_PAYMENT_FOR_TARIFF);
        $this->delete(self::TABLE_PAYMENT_FOR_ADDITIONAL_OPTION);
        $this->delete(self::TABLE_PAYMENT_FOR_ONE_TIME_SERVICE);
        $this->delete(self::TABLE_PAYMENT);

        $this->delete(self::TABLE_DEMO_TARIFF);

        $this->delete(self::TABLE_PRICE_HISTORY);
        $this->delete(self::TABLE_TARIFF_PERMISSION_HISTORY);

        $this->delete(self::TABLE_TARIFF_PERMISSION);
        $this->delete(self::TABLE_TARIFF);
        $this->delete(self::TABLE_TARIFF_ADDITIONAL_OPTION);
        $this->delete(self::TABLE_ONE_TIME_SERVICE);
        $this->delete(self::TABLE_PERMISSION);

        $this->delete(self::TABLE_TARIFF_ENTITY_TYPE);
        $this->delete(self::TABLE_MODULE);
    }

    private function fillModules()
    {
        $this->batchInsert(self::TABLE_MODULE, ['id', 'code', 'name'],
            [
                [1, 1, 'Такси'],
                [2, 2, 'Курьеры'],
            ]);
    }

    private function fillEntityTypes()
    {
        $this->batchInsert(self::TABLE_TARIFF_ENTITY_TYPE, ['id', 'code', 'name'],
            [
                [1, 1, 'Tariff'],
                [2, 2, 'Additional option'],
                [3, 3, 'One-time service'],
            ]);
    }

    private function fillPermissions()
    {
        $this->batchInsert(self::TABLE_PERMISSION, ['id', 'code', 'name'],
            [
                [1, 1, 'Кол-во исполнителей онлайн'],
                [2, 2, 'Кол-во сотрудников онлайн'],
                [3, 3, 'Кол-во филиалов'],
            ]);
    }

    private function fillTariffs()
    {
        $tariffStart  = 1;
        $tariffOptima = 2;
        $tariffMaster = 3;
        $tariffMaxima = 4;
        $tariffDemo   = self::DEMO_TARIFF;

        $moduleTaxi = 1;

        $permissionWorkers = 1;
        $permissionUsers   = 2;
        $permissionCities  = 3;

        $this->batchInsert(self::TABLE_TARIFF,
            ['id', 'name', 'module_id', 'default', 'price', 'currency_id'],
            [
                [$tariffStart, 'Старт', $moduleTaxi, 1, 0.00, 1],
                [$tariffOptima, 'Оптима', $moduleTaxi, 0, 10000.00, 1],
                [$tariffMaster, 'Мастер', $moduleTaxi, 0, 15000.00, 1],
                [$tariffMaxima, 'Максима', $moduleTaxi, 0, 20000.00, 1],
                [$tariffDemo, 'Демо', $moduleTaxi, 0, 0.00, 1],
            ]);

        $this->batchInsert(self::TABLE_PRICE_HISTORY,
            ['entity_type_id', 'tariff_id', 'option_id', 'service_id', 'price', 'currency_id', 'activated_at'],
            [
                [self::ENTITY_TYPE_TARIFF, $tariffStart, null, null, 0.00, 1, time()],
                [self::ENTITY_TYPE_TARIFF, $tariffOptima, null, null, 10000.00, 1, time()],
                [self::ENTITY_TYPE_TARIFF, $tariffMaster, null, null, 15000.00, 1, time()],
                [self::ENTITY_TYPE_TARIFF, $tariffMaxima, null, null, 20000.00, 1, time()],
                [self::ENTITY_TYPE_TARIFF, $tariffDemo, null, null, 0.00, 1, time()],
            ]);

        $this->batchInsert(self::TABLE_TARIFF_PERMISSION,
            ['id', 'tariff_id', 'permission_id', 'value'],
            [
                [1, $tariffStart, $permissionWorkers, 5],
                [2, $tariffStart, $permissionUsers, 2],
                [3, $tariffStart, $permissionCities, 1],

                [4, $tariffOptima, $permissionWorkers, 20],
                [5, $tariffMaster, $permissionWorkers, 100],
                [6, $tariffMaxima, $permissionWorkers, 300],
            ]);

        $this->batchInsert(self::TABLE_TARIFF_PERMISSION_HISTORY,
            ['permission_id', 'value', 'activated_at'],
            [
                [1, 5, time()],
                [2, 2, time()],
                [3, 1, time()],
                [4, 20, time()],
                [5, 100, time()],
                [6, 300, time()],
            ]);

        $this->batchInsert(self::TABLE_DEMO_TARIFF,
            ['id', 'tariff_id'],
            [
                [1, $tariffDemo],
            ]);
    }

    private function fillAdditionalOptions()
    {
        $mobile = 1;
        $ats    = 2;

        $this->batchInsert(self::TABLE_TARIFF_ADDITIONAL_OPTION,
            ['id', 'code', 'name', 'price', 'currency_id'],
            [
                [1, 1, 'Мобильное приложение для клиентов', 5000.00, 1],
                [2, 2, 'Виртуальная АТС', 5000.00, 1],
            ]);

        $this->batchInsert(self::TABLE_PRICE_HISTORY,
            ['entity_type_id', 'tariff_id', 'option_id', 'service_id', 'price', 'currency_id', 'activated_at'],
            [
                [self::ENTITY_TYPE_OPTION, null, $mobile, null, 5000.00, 1, time()],
                [self::ENTITY_TYPE_OPTION, null, $ats, null, 5000.00, 1, time()],
            ]);
    }

    private function fillOneTimeServices()
    {
        $sms = 1;
        $ats = 2;

        $this->batchInsert(self::TABLE_ONE_TIME_SERVICE,
            ['id', 'code', 'name', 'price', 'currency_id'],
            [
                [1, 1, 'Подключение СМС провайдера', 5000.00, 1],
                [2, 2, 'Подключение виртуальной АТС', 20000.00, 1],
            ]);

        $this->batchInsert(self::TABLE_PRICE_HISTORY,
            ['entity_type_id', 'tariff_id', 'option_id', 'service_id', 'price', 'currency_id', 'activated_at'],
            [
                [self::ENTITY_TYPE_SERVICE, null, null, $sms, 5000.00, 1, time()],
                [self::ENTITY_TYPE_SERVICE, null, null, $ats, 20000.00, 1, time()],
            ]);
    }

    public function giveDemoTariffs()
    {
        $startedAt  = strtotime(date('d.m.Y', time()));
        $expiryDate = strtotime('+13 day', $startedAt);

        $sql = <<<SQL
INSERT INTO tbl_tenant_tariff (tenant_id, tariff_id, started_at, expiry_date)
SELECT tenant_id,
       :tariff_id AS tariff_id,
       :started_at started_at,
       :expiry_date expiry_date
FROM tbl_tenant;
SQL;
        $this->execute($sql, [
            'tariff_id'   => self::DEMO_TARIFF,
            'started_at'  => $startedAt,
            'expiry_date' => $expiryDate,
        ]);
    }

    public function up()
    {
        $this->clearTables();

        $this->fillModules();
        $this->fillEntityTypes();
        $this->fillPermissions();
        $this->fillTariffs();
        $this->fillAdditionalOptions();
        $this->fillOneTimeServices();
        $this->giveDemoTariffs();
    }

    public function down()
    {
    }

}
