<?php

use yii\db\Migration;

class m160330_111625_tbl_payment__add_column extends Migration
{
    const TABLE_NAME = '{{%payment}}';
    const COLUMN_NAME = 'payment_number';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_NAME, $this->integer()->notNull());
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
