<?php

use yii\db\Migration;

class m160316_133549_tbl_default_sms_template extends Migration
{
    public function up()
    {
        $this->insert('{{%default_sms_template}}', ['text' => 'Извините, в вашем районе нет машин', 'type' => '40']);
    }

    public function down()
    {
        echo "m160316_133549_tbl_default_sms_template cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
