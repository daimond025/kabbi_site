<?php

use yii\db\Schema;
use yii\db\Migration;

class m150803_110555_tbl_meta_tags extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%meta_tags}}', [
            'id' => Schema::TYPE_PK,
            'description' => Schema::TYPE_STRING,
            'title' => Schema::TYPE_STRING,
            'keywords' => Schema::TYPE_STRING,
            'page' => Schema::TYPE_STRING,
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%meta_tags}');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
