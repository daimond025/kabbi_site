<?php

use yii\db\Schema;
use yii\db\Migration;

class m160122_065303_tbl_tenant_setting extends Migration
{
    public function up()
    {
        $this->execute('SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0');
        $this->execute('SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0');

        $this->addColumn('{{%tenant_setting}}', 'city_id', 'INT(11) UNSIGNED NULL DEFAULT NULL');
        $this->createIndex('FK_tenant_setting_idx', '{{%tenant_setting}}', 'city_id');
        $this->addForeignKey('FK_tenant_setting', '{{%tenant_setting}}', 'city_id', '{{%city}}', 'city_id');

        $this->execute('SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS');
        $this->execute('SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS');
    }

    public function down()
    {
        $this->dropForeignKey('FK_tenant_setting', '{{%tenant_setting}}');
        $this->dropIndex('FK_tenant_setting_idx', '{{%tenant_setting}}');
        $this->dropColumn('{{%tenant_setting}}', 'city_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
