<?php

use yii\db\Migration;

class m160328_054848_create_table_permission extends Migration
{
    const TABLE_PERMISSION = '{{%permission}}';
    const TABLE_TARIFF_PERMISSION = '{{%tariff_permission}}';

    const COLUMN_PERMISSION_ID = 'permission_id';

    public function generateForeignKeyName($tableName, $columnName)
    {
        return 'fk_' . $this->db->schema->getRawTableName($tableName)
        . '__' . $columnName;
    }

    public function up()
    {
        $this->createTable(self::TABLE_PERMISSION, [
            'id'          => $this->primaryKey(),
            'name'        => $this->string()->notNull(),
            'description' => $this->text(),
            'active'      => $this->smallInteger(1)->notNull()->defaultValue(1),
            'created_at'  => $this->integer(),
            'updated_at'  => $this->integer(),
            'created_by'  => $this->integer(),
            'updated_by'  => $this->integer(),
        ]);

        $this->dropColumn(self::TABLE_TARIFF_PERMISSION, 'name');
        $this->dropColumn(self::TABLE_TARIFF_PERMISSION, 'description');
        $this->addColumn(self::TABLE_TARIFF_PERMISSION, self::COLUMN_PERMISSION_ID, $this->integer()->notNull());
        $this->addForeignKey($this->generateForeignKeyName(self::TABLE_TARIFF_PERMISSION, self::COLUMN_PERMISSION_ID),
            self::TABLE_TARIFF_PERMISSION, self::COLUMN_PERMISSION_ID,
            self::TABLE_PERMISSION, 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey(
            $this->generateForeignKeyName(self::TABLE_TARIFF_PERMISSION, self::COLUMN_PERMISSION_ID),
            self::TABLE_TARIFF_PERMISSION);
        $this->dropColumn(self::TABLE_TARIFF_PERMISSION, self::COLUMN_PERMISSION_ID);
        $this->dropTable(self::TABLE_PERMISSION);
        $this->addColumn(self::TABLE_TARIFF_PERMISSION, 'name', $this->string());
        $this->addColumn(self::TABLE_TARIFF_PERMISSION, 'description', $this->text());
    }
}
