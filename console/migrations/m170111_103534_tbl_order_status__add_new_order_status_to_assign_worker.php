<?php

use yii\db\Migration;

class m170111_103534_tbl_order_status__add_new_order_status_to_assign_worker extends Migration
{
    const TABLE_NAME = '{{%order_status}}';

    private $statuses = [
        [111, 'Driver assigned at pre-order(soft)', 'pre_order', 1],
        [112, 'Driver assigned at pre-order(hard)', 'pre_order', 1,],
        [113, 'Driver assigned at order(soft)', 'new', 1,],
        [114, 'Driver assigned at order(hard)', 'new', 1,],
        [115, 'Driver refused assignment of order', 'new', 0],
    ];

    public function up()
    {
        $this->batchInsert(self::TABLE_NAME, [
            'status_id',
            'name',
            'status_group',
            'dispatcher_sees',
        ], $this->statuses);
    }

    public function down()
    {
        $ids = array_map(function ($status) {
            return $status[0];
        }, $this->statuses);

        $this->delete(self::TABLE_NAME, ['status_id' => $ids]);
    }

}
