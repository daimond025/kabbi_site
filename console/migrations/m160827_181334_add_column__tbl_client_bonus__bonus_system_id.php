<?php

use yii\db\Migration;

class m160827_181334_add_column__tbl_client_bonus__bonus_system_id extends Migration
{
    const TABLE_CLIENT_BONUS = '{{%client_bonus}}';
    const TABLE_BONUS_SYSTEM = '{{%bonus_system}}';
    const COLUMN_BONUS_SYSTEM_ID = 'bonus_system_id';
    const FOREIGN_KEY_BONUS_SYSTEM_ID = 'fk_client_bonus__bonus_system_id';

    public function up()
    {
        $this->addColumn(self::TABLE_CLIENT_BONUS, self::COLUMN_BONUS_SYSTEM_ID,
            $this->integer()->notNull()->defaultValue(1));

        $this->addForeignKey(self::FOREIGN_KEY_BONUS_SYSTEM_ID,
            self::TABLE_CLIENT_BONUS, self::COLUMN_BONUS_SYSTEM_ID,
            self::TABLE_BONUS_SYSTEM, 'id');
    }

    public function down()
    {
        $this->dropForeignKey(self::FOREIGN_KEY_BONUS_SYSTEM_ID, self::TABLE_CLIENT_BONUS);
        $this->dropColumn(self::TABLE_CLIENT_BONUS, self::COLUMN_BONUS_SYSTEM_ID);
    }

}
