<?php

use yii\db\Schema;
use yii\db\Migration;

class m151126_062711_tbl_auth_item_translation extends Migration {

    const TABLE_NAME = '{{%auth_item}}';

    public function up() {
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Branch director' where name='branch_director'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Cars' where name='cars'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Clients' where name='clients'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Client tariffs' where name='clientTariff'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Drivers' where name='drivers'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Driver tariffs' where name='driverTariff'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Exchange' where name='exchange'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Orders' where name='orders'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Organization' where name='organization'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Parkings' where name='parkings'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Phone line' where name='phoneLine'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Public place' where name='publicPlace'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Cars (read)' where name='read_cars'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Clients (read)' where name='read_clients'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Client tariffs (read)' where name='read_clientTariff'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Drivers (read)' where name='read_drivers'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Driver tariffs (read)' where name='read_driverTariff'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Exchange (read)' where name='read_exchange'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Orders (read)' where name='read_orders'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Organization (read)' where name='read_organization'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Parkings (read)' where name='read_parkings'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Phone line (read)' where name='read_phoneLine'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Public place (read)' where name='read_publicPlace'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Reports (read)' where name='read_reports'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Settings (read)' where name='read_settings'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Streets (read)' where name='read_streets'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Tenant (read)' where name='read_tenant'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Users (read)' where name='read_users'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Reports' where name='reports'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Settings' where name='settings'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Staff' where name='staff'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Settings' where name='settings'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Streets' where name='streets'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Tenant' where name='tenant'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Top manager' where name='top_manager'");
        $this->execute("UPDATE" . self::TABLE_NAME . " SET description = 'Users' where name='users'");
    }

    public function down() {
        echo "m151126_062711_tbl_auth_item_translation cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
