<?php

use yii\db\Migration;

class m171020_061335_new_sms_provider_comtass extends Migration
{
    const TABLE_NAME = '{{%sms_server}}';
    const PROVIDER_ID = 21;

    public function up()
    {
        $this->insert(self::TABLE_NAME, [
            'server_id' => self::PROVIDER_ID,
            'name'      => 'Comtass (Sudan - http://comtass.com)',
            'host'      => 'http://comtass.com',
            'active'    => '1',
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME, ['server_id' => self::PROVIDER_ID]);
    }
}
