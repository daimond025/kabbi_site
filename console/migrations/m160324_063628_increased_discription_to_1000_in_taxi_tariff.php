<?php

use yii\db\Migration;

class m160324_063628_increased_discription_to_1000_in_taxi_tariff extends Migration
{
    const TABLE_NAME = '{{%taxi_tariff}}';
    
    public function up()
    {
        $this->alterColumn(self::TABLE_NAME,'description',$this->text());
    }

    public function down()
    {
        $this->alterColumn(self::TABLE_NAME,'description',$this->string(255));
    }
}
