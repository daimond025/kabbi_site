<?php

use yii\db\Migration;

class m170301_054657_create_bg_lang extends Migration
{
    const TABLE_CITY = '{{%city}}';
    const TABLE_COUNTRY = '{{%country}}';
    const TABLE_PLACE = '{{%public_place}}';
    const TABLE_REPUBLIC = '{{%republic}}';
    const TABLE_STREET = '{{%street}}';

    public function up()
    {
        $this->addColumnsByLang('bg');
    }

    public function down()
    {
        $this->dropColumnsByLang('bg');
    }


    private function addColumnsByLang($lang)
    {
        $this->addColumn(self::TABLE_CITY, 'name_' . $lang, $this->string(100));
        $this->addColumn(self::TABLE_CITY, 'fulladdress_' . $lang, $this->string(255));
        $this->addColumn(self::TABLE_COUNTRY, 'name_' . $lang, $this->string(255));
        $this->addColumn(self::TABLE_REPUBLIC, 'name_' . $lang, $this->string(100));
        $this->addColumn(self::TABLE_REPUBLIC, 'shortname_' . $lang, $this->string(255));

    }

    private function dropColumnsByLang($lang)
    {
        $this->dropColumn(self::TABLE_CITY, 'name_' . $lang);
        $this->dropColumn(self::TABLE_CITY, 'fulladdress_' . $lang);
        $this->dropColumn(self::TABLE_COUNTRY, 'name_' . $lang);
        $this->dropColumn(self::TABLE_REPUBLIC, 'name_' . $lang);
        $this->dropColumn(self::TABLE_REPUBLIC, 'shortname_' . $lang);

    }
}
