<?php

use yii\db\Migration;

class m161013_114045_alphasms_newprovider extends Migration
{
    const TABLE_NAME = '{{%sms_server}}';

    const COLUMN_SERVER_ID = 16;

    public function up()
    {
        $this->insert(self::TABLE_NAME,[
            'server_id' => self::COLUMN_SERVER_ID,
            'name' =>  'АльфаSMS',
            'host' => 'https://alphasms.ua/',
            'active' => '1',
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME,['server_id' => self::COLUMN_SERVER_ID]);
    }
}
