<?php

use yii\db\Schema;
use yii\db\Migration;

class m150619_124816_billing extends Migration
{
    public function up()
    {
        $this->execute('SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0');
        $this->execute('SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0');   
        $this->execute("SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES'");
        
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
 
        //Тип счета(Активный, пассивный)
        $this->createTable('{{%account_type}}', [
            'type_id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ], $tableOptions);
        
        $this->batchInsert('{{%account_type}}', ['name'], [['ACTIVE'], ['PASSIVE']]);
        
        //Вид счета(Клиентский, водительский)
        $this->createTable('{{%account_kind}}', [
            'kind_id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ], $tableOptions);
        
        $this->batchInsert('{{%account_kind}}', ['name'], [['TENANT'], ['DRIVER'], ['CLIENT'], ['COMPANY'], ['SYSTEM'], ['GOOTAX']]);
        
        //Типы денежных средств(Валюта, Бонусы)
        $this->createTable('{{%currency_type}}', [
            'type_id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ], $tableOptions);
        
        $this->batchInsert('{{%currency_type}}', ['name'], [['CURRENCY'], ['BONUS']]);
        
        //Виды денежных средств, определенного типа
        $this->createTable('{{%currency}}', [
            'currency_id' => Schema::TYPE_PK,
            'type_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'code' => Schema::TYPE_STRING,
        ], $tableOptions);
 
        $this->addForeignKey('FK_currency_type_currency', '{{%currency}}', 'type_id', '{{%currency_type}}', 'type_id', 'CASCADE', 'CASCADE');
        
        //Счета
        $this->createTable('{{%account}}', [
            'account_id' => Schema::TYPE_PK,
            'acc_kind_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'acc_type_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'owner_id' => Schema::TYPE_INTEGER . ' UNSIGNED',
            'currency_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'tenant_id' => Schema::TYPE_INTEGER . ' UNSIGNED',
            'balance' => Schema::TYPE_FLOAT . ' NOT NULL DEFAULT "0"'
        ], $tableOptions);
        
        $this->createIndex('FK_account_idx', '{{%account}}', ['tenant_id', 'owner_id']);
        $this->createIndex('FK_account_tenant_id_idx', '{{%account}}', 'tenant_id');
        
        $this->addForeignKey('FK_account_acc_kind_id', '{{%account}}', 'acc_kind_id', '{{%account_kind}}', 'kind_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_account_acc_type_id', '{{%account}}', 'acc_type_id', '{{%account_type}}', 'type_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_account_currency_id', '{{%account}}', 'currency_id', '{{%currency}}', 'currency_id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('FK_account_tenant_id', '{{%account}}', 'tenant_id', '{{%tenant}}', 'tenant_id', 'CASCADE', 'CASCADE');
        
        //Добаляем счет гутакса
        $this->insert('{{%account}}', ['acc_kind_id' => 6, 'acc_type_id' => 1, 'currency_id' => 1]);
        
        //Типы транзакций(Пополнение счета, Списание комиссии)
        $this->createTable('{{%transaction_type}}', [
            'type_id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ], $tableOptions);
        
        //Транзакции(Главная книга)
        $this->createTable('{{%transaction}}', [
            'transaction_id' => Schema::TYPE_PK,
            'type_id' => Schema::TYPE_INTEGER,
            'sender_acc_id' => Schema::TYPE_INTEGER,
            'receiver_acc_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'sum' => Schema::TYPE_FLOAT . ' NOT NULL',
            'payment_method' => Schema::TYPE_STRING . '(255)',
            'user_created' => Schema::TYPE_INTEGER . ' UNSIGNED',
            'date' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP'
        ], $tableOptions);
        
        $this->addForeignKey('FK_transaction_author_id', '{{%transaction}}', 'user_created', '{{%user}}', 'user_id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('FK_transaction_transaction_type', '{{%transaction}}', 'type_id', '{{%transaction_type}}', 'type_id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('FK_transaction_sender_acc_id', '{{%transaction}}', 'sender_acc_id', '{{%account}}', 'account_id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('FK_transaction_receiver_acc_id', '{{%transaction}}', 'receiver_acc_id', '{{%account}}', 'account_id', 'RESTRICT', 'RESTRICT');
        
        //Типы операций(Приход, расход)
        $this->createTable('{{%operation_type}}', [
            'type_id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ], $tableOptions);
        
        $this->batchInsert('{{%operation_type}}', ['name'], [['Income'], ['Expenses']]);
        
        //Операции(Журнал проводок)
        $this->createTable('{{%operation}}', [
            'operation_id' => Schema::TYPE_PK,
            'type_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'account_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'sum' => Schema::TYPE_FLOAT . ' NOT NULL',
            'saldo' => Schema::TYPE_FLOAT . ' NOT NULL',
            'transaction_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'comment' => Schema::TYPE_STRING,
            'date' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP'
        ], $tableOptions);
        
        $this->createIndex('FK_operation_type_id_account_id_idx', '{{%operation}}', ['type_id', 'account_id']);
        $this->createIndex('FK_operation_account_id_idx', '{{%operation}}', 'account_id');
        
        $this->addForeignKey('FK_operation_type_id', '{{%operation}}', 'type_id', '{{%operation_type}}', 'type_id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('FK_operation_account', '{{%operation}}', 'account_id', '{{%account}}', 'account_id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('FK_operation_transaction_id', '{{%operation}}', 'transaction_id', '{{%transaction}}', 'transaction_id', 'RESTRICT', 'RESTRICT');
        
        $this->execute('SET SQL_MODE=@OLD_SQL_MODE');
        $this->execute('SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS');
        $this->execute('SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS');
    }

    public function down()
    {
        $this->dropForeignKey('FK_currency_type_currency', '{{%currency}}');
        $this->dropForeignKey('FK_account_acc_kind_id', '{{%account}}');
        $this->dropForeignKey('FK_account_acc_type_id', '{{%account}}');
        $this->dropForeignKey('FK_account_currency_id', '{{%account}}');
        $this->dropForeignKey('FK_account_tenant_id', '{{%account}}');
        $this->dropForeignKey('FK_transaction_transaction_type', '{{%transaction}}');
        $this->dropForeignKey('FK_transaction_sender_acc_id', '{{%transaction}}');
        $this->dropForeignKey('FK_transaction_receiver_acc_id', '{{%transaction}}');
        $this->dropForeignKey('FK_operation_type_id', '{{%operation}}');
        $this->dropForeignKey('FK_operation_account', '{{%operation}}');
        $this->dropForeignKey('FK_operation_transaction_id', '{{%operation}}');
        
        $this->dropIndex('FK_account_idx', '{{%account}}');
        $this->dropIndex('FK_operation_type_id_account_id_idx', '{{%operation}}');
        $this->dropIndex('FK_operation_account_id_idx', '{{%operation}}');
        
        $this->dropTable('{{%account_type}}');
        $this->dropTable('{{%account_kind}}');
        $this->dropTable('{{%currency_type}}');
        $this->dropTable('{{%currency}}');
        $this->dropTable('{{%account}}');
        $this->dropTable('{{%transaction_type}}');
        $this->dropTable('{{%transaction}}');
        $this->dropTable('{{%operation_type}}');
        $this->dropTable('{{%operation}}');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
