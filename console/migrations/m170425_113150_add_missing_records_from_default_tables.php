<?php

use yii\db\Migration;

class m170425_113150_add_missing_records_from_default_tables extends Migration
{
    const SQL_ADD_MISSING_PUSH_NOTIFICATIONS = <<<SQL
INSERT `tbl_client_push_notifications`
 (`tenant_id`, `city_id`, `position_id`, `type`, `text`)
SELECT `c`.`tenant_id`, `c`.`city_id`, `d`.`position_id`, `d`.`type`, `d`.`text`
FROM `tbl_tenant_has_city` AS `c`
CROSS JOIN `tbl_default_client_push_notifications` AS `d`
LEFT JOIN `tbl_client_push_notifications` AS `t` ON `c`.`tenant_id` = `t`.`tenant_id` AND `c`.`city_id` = `t`.`city_id` AND `d`.`type` = `t`.`type` AND `d`.`position_id` = `t`.`position_id`
WHERE `t`.`push_id` IS NULL
SQL;

    const SQL_ADD_MISSING_SMS_TEMPLATES = <<<SQL
INSERT `tbl_sms_template`
 (`tenant_id`, `city_id`, `position_id`, `type`, `text`)
SELECT `c`.`tenant_id`, `c`.`city_id`, `d`.`position_id`, `d`.`type`, `d`.`text`
FROM `tbl_tenant_has_city` AS `c`
CROSS JOIN `tbl_default_sms_template` AS `d`
LEFT JOIN `tbl_sms_template` AS `t` ON `c`.`tenant_id` = `t`.`tenant_id` AND `c`.`city_id` = `t`.`city_id` AND `d`.`type` = `t`.`type` AND `d`.`position_id` = `t`.`position_id`
WHERE `t`.`template_id` IS NULL
SQL;

    const SQL_ADD_MISSING_CLIENT_STATUS_EVENTS = <<<SQL
INSERT `tbl_client_status_event`
 (`tenant_id`, `city_id`, `position_id`, `status_id`, `group`, `notice`)
SELECT `c`.`tenant_id`, `c`.`city_id`, `d`.`position_id`, `d`.`status_id`, `d`.`group`, `d`.`notice`
FROM `tbl_tenant_has_city` AS `c`
CROSS JOIN `tbl_default_client_status_event` AS `d`
LEFT JOIN `tbl_client_status_event` AS `t` ON `c`.`tenant_id` = `t`.`tenant_id` AND `c`.`city_id` = `t`.`city_id` AND `d`.`status_id` = `t`.`status_id` AND `d`.`group` = `t`.`group` AND `d`.`position_id` = `t`.`position_id`
WHERE `t`.`event_id` IS NULL
SQL;

    public function up()
    {
        $db = \Yii::$app->getDb();
        $db->createCommand(self::SQL_ADD_MISSING_PUSH_NOTIFICATIONS)->execute();
        $db->createCommand(self::SQL_ADD_MISSING_SMS_TEMPLATES)->execute();
        $db->createCommand(self::SQL_ADD_MISSING_CLIENT_STATUS_EVENTS)->execute();
    }

    public function down()
    {
    }

}
