<?php

use yii\db\Schema;
use yii\db\Migration;

class m150929_100245_tbl_client extends Migration
{
    public function up()
    {
        $this->addColumn('{{%client}}', 'fail_dispatcher_order', Schema::TYPE_INTEGER.'(10) DEFAULT 0');

    }

    public function down()
    {
        $this->dropColumn('{{%client}}', 'fail_dispatcher_order');
    }



    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
