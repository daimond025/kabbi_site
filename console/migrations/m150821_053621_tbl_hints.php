<?php

use yii\db\Schema;
use yii\db\Migration;

class m150821_053621_tbl_hints extends Migration
{

    public function up()
    {
        $this->createTable('{{%hints}}', [
            'id' => $this->primaryKey(),
            'text' => $this->string(128)->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%hints}}');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
