<?php

use yii\db\Migration;

class m180424_124833_fix_promoaction_count extends Migration
{

    const TABLE_NAME = '{{%promo_action_count}}';

    const COLUMN_NAME = 'name';

    const COLUMN_ID = 'action_count_id';

    const RECORD_NAME = '1 time immediately after the promotion code is activated';

    public function safeUp()
    {
        $this->delete(self::TABLE_NAME, ['>', self::COLUMN_ID, 2]);

        $this->insert(self::TABLE_NAME, [
            self::COLUMN_NAME => self::RECORD_NAME,
            self::COLUMN_ID   => 3
        ]);
    }

    public function safeDown()
    {
        return true;
    }

}
