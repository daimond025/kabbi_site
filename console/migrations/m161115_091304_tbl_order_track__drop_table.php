<?php

use yii\db\Migration;

class m161115_091304_tbl_order_track__drop_table extends Migration
{
    const TABLE_NAME = '{{%order_track}}';
    const INDEX_NAME = 'idx_order_track__order_id';

    public function up()
    {
        $this->dropTable(self::TABLE_NAME);
    }

    public function down()
    {
        $this->createTable(self::TABLE_NAME, [
            'track'    => $this->primaryKey(10),
            'tracking' => 'LONGTEXT NOT NULL',
            'order_id' => $this->integer(10)->notNull(),
            'time'     => $this->integer(20),
        ]);

        $this->createIndex(self::INDEX_NAME, self::TABLE_NAME, 'order_id');
    }

}
