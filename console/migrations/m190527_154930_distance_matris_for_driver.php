<?php

use yii\db\Migration;
use common\modules\tenant\models\DefaultSettings;

class m190527_154930_distance_matris_for_driver extends Migration
{
    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const TABLE_TENANT_SETTING = '{{%tenant_setting}}';

    const SETTING_NAME_VALUE = 'WORKER_TIME_CALCULATE';
    const SETTING_NAME_STEP =  'WORKER_TIME_CALCULATE_STEP';
    const SETTING_NAME_COUNT = 'WORKER_TIME_CALCULATE_STEP_COUNT';

    const SETTING_TYPE = 'drivers';

    public function safeUp()
    {
        $time_calculate_value = new DefaultSettings([
            'name'  => self::SETTING_NAME_VALUE,
            'value' => '0',
            'type'  => self::SETTING_TYPE,
        ]);

        $time_calculate_step = new DefaultSettings([
            'name'  => self::SETTING_NAME_STEP,
            'value' => '0',
            'type'  => self::SETTING_TYPE,
        ]);

        $time_calculate_count = new DefaultSettings([
            'name'  => self::SETTING_NAME_COUNT,
            'value' => '0',
            'type'  => self::SETTING_TYPE,
        ]);

        return $time_calculate_value->save() && $time_calculate_step->save() && $time_calculate_count->save() ;
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::SETTING_NAME_VALUE,
            'type' => 'drivers',
        ]);

        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::SETTING_NAME_STEP,
            'type' => 'drivers',
        ]);

        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::SETTING_NAME_COUNT,
            'type' => 'drivers',
        ]);

       $this->delete(self::TABLE_TENANT_SETTING, ['name' => self::SETTING_NAME_VALUE, 'type' => self::SETTING_TYPE]);
       $this->delete(self::TABLE_TENANT_SETTING, ['name' => self::SETTING_NAME_STEP, 'type' => self::SETTING_TYPE]);
       $this->delete(self::TABLE_TENANT_SETTING, ['name' => self::SETTING_NAME_COUNT, 'type' => self::SETTING_TYPE]);
    }

}
