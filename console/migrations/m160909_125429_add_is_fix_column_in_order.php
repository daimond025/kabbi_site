<?php

use yii\db\Migration;

class m160909_125429_add_is_fix_column_in_order extends Migration
{
    const TABLE_NAME = '{{%order}}';
    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'is_fix', $this->integer(1)->defaultValue(0)->notNull());
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'is_fix');
    }
}
