<?php

use yii\db\Schema;
use yii\db\Migration;
use common\modules\tenant\models\TenantHasCarOption;

class m160123_134944_tbl_tenant_has_car_option extends Migration
{
    public function up()
    {
        $tenantHasCarOption = (new \yii\db\Query())
                ->from('{{%tenant_has_car_option}}')
                ->all();
        $arCity = [];
        $arDefaultCity = [];
        foreach ($tenantHasCarOption as $item) {
            if (!isset($arCity[$item['tenant_id']])) {
                $tenantHasCity = (new \yii\db\Query())
                    ->select('city_id')
                    ->from('{{%tenant_has_city}}')
                    ->where(['tenant_id' => $item['tenant_id']])
                    ->all();
                $tenantHasCity = \yii\helpers\ArrayHelper::getColumn($tenantHasCity, 'city_id');
                $arCity[$item['tenant_id']] = $tenantHasCity;
                $arDefaultCity[$item['tenant_id']] = array_shift($arCity[$item['tenant_id']]);
            }

            if (empty($item['city_id'])) {
                Yii::$app->db->createCommand()->update('{{%tenant_has_car_option}}', ['city_id' => $arDefaultCity[$item['tenant_id']]], ['tenant_id' => $item['tenant_id'], 'option_id' => $item['option_id']])->execute();
            }

            foreach ($arCity[$item['tenant_id']] as $cityItem) {
                Yii::$app->db->createCommand()->insert('{{%tenant_has_car_option}}', [
                    'tenant_id' => $item['tenant_id'],
                    'option_id'      => $item['option_id'],
                    'value'     => $item['value'],
                    'city_id'   => $cityItem,
                ])->execute();
            }
        }
    }

    public function down()
    {
        echo "m160123_134944_tbl_tenant_has_car_option cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
