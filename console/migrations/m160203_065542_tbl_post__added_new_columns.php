<?php

use yii\db\Schema;
use yii\db\Migration;

class m160203_065542_tbl_post__added_new_columns extends Migration
{
    const TABLE_NAME = '{{%post}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'meta_title', 'VARCHAR(255)');
        $this->addColumn(self::TABLE_NAME, 'meta_description', 'VARCHAR(255)');
        $this->addColumn(self::TABLE_NAME, 'meta_keywords', 'VARCHAR(255)');
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'meta_title');
        $this->dropColumn(self::TABLE_NAME, 'meta_description');
        $this->dropColumn(self::TABLE_NAME, 'meta_keywords');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
