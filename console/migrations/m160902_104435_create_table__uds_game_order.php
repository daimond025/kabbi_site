<?php

use yii\db\Migration;

class m160902_104435_create_table__uds_game_order extends Migration
{
    const TABLE_UDS_GAME_ORDER = '{{%uds_game_order}}';
    const TABLE_ORDER = '{{%order}}';
    const COLUMN_ORDER_ID = 'order_id';
    const FOREIGN_KEY_ORDER_ID = 'fk_uds_game_order__order_id';

    public function up()
    {
        $this->createTable(self::TABLE_UDS_GAME_ORDER, [
            'id'         => $this->primaryKey(),
            'order_id'   => $this->integer(10)->unsigned()->notNull()->unique(),
            'promo_code' => $this->string()->notNull(),
            'request_id' => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(
            self::FOREIGN_KEY_ORDER_ID,
            self::TABLE_UDS_GAME_ORDER,
            self::COLUMN_ORDER_ID,
            self::TABLE_ORDER,
            self::COLUMN_ORDER_ID,
            'CASCADE',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable(self::TABLE_UDS_GAME_ORDER);
    }

}

