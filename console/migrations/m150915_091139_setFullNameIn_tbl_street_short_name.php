<?php

use yii\db\Schema;
use yii\db\Migration;

class m150915_091139_setFullNameIn_tbl_street_short_name extends Migration
{
    const TABLE_NAME = '{{%street_short_name}}';
    
    public function up()
    {
        $this->execute("UPDATE" . self::TABLE_NAME ." SET name = 'улица' where id=1");
        $this->execute("UPDATE" . self::TABLE_NAME ." SET name = 'переулок' where id = 3");
        $this->execute("UPDATE" . self::TABLE_NAME ." SET name = 'бульвар' where id = 4");
        $this->execute("UPDATE" . self::TABLE_NAME ." SET name =  'площадь' where id = 5");
        $this->execute("UPDATE" . self::TABLE_NAME ." SET name =  'проспект' where id = 6");
        $this->execute("UPDATE" . self::TABLE_NAME ." SET name =  'шоссе' where id = 9");
        $this->execute("UPDATE" . self::TABLE_NAME ." SET name = 'тупик' where id = 7");
        $this->execute("UPDATE" . self::TABLE_NAME ." SET name = 'участок' where id = 10");
        $this->execute("UPDATE" . self::TABLE_NAME ." SET name = 'квартал' where id = 13");
        $this->execute("UPDATE" . self::TABLE_NAME ." SET name = 'набережная' where id = 14");
        $this->execute("UPDATE" . self::TABLE_NAME ." SET name =  'дом' where id = 20");
    }

    public function down()
    {
        $this->execute("truncate table" . self::TABLE_NAME . "; INSERT INTO" . self::TABLE_NAME .
                "VALUES (1,'ул'),(2,'проезд'),(3,'пер'),(4,'б-р'),(5,'пл'),(6,'пр-кт'),(7,'туп'),(8,'проулок'),(9,'ш'),(10,'уч-к'),(11,'линия'),(12,'аллея'),(13,'кв-л'),(14,'наб'),(15,'кольцо'),(16,'парк'),(17,'сквер'),(18,'тракт'),(19,'заезд'),(20,'д'),(21,'городок'),(22,'остров'),(23,'въезд'),(24,'аул'),(25,'вал');");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
