<?php

use yii\db\Schema;
use yii\db\Migration;

class m160114_202920_tbl_account__change_currency_id extends Migration
{
    const ACCOUNT_TABLE          = '{{%account}}';
    const TENANT_SETTING_TABLE   = '{{%tenant_setting}}';
    const DEFAULT_SETTINGS_TABLE = '{{%default_settings}}';
    const CURRENCY_TABLE         = '{{%currency}}';
    const CURRENCY_ID_BONUS      = 5;

    public function up()
    {
        $accountTable         = self::ACCOUNT_TABLE;
        $tenantSettingTable   = self::TENANT_SETTING_TABLE;
        $defaultSettingsTable = self::DEFAULT_SETTINGS_TABLE;
        $currencyTable        = self::CURRENCY_TABLE;

        $updateSql = <<<SQL
            update {$accountTable} as `a`
              join (select `a`.`account_id`, `c`.`currency_id`
                      from {$accountTable} as `a`
                      left join {$tenantSettingTable} as `t` on `a`.`tenant_id` = `t`.`tenant_id`
                                                            and `t`.`name` = 'CURRENCY'
                      left join {$defaultSettingsTable} as `d` on `d`.`name` = 'CURRENCY'
                      join {$currencyTable} as `c` on ifnull(`t`.`value`, `d`.`value`) = `c`.`code`
                     where `a`.`acc_kind_id` in (7,8)) as `a2`
                on `a`.`account_id` = `a2`.`account_id`
               set `a`.`currency_id` = `a2`.`currency_id`
SQL;
        $this->execute($updateSql);
    }

    public function down()
    {
        $accountTable    = self::ACCOUNT_TABLE;
        $bonusCurrencyId = self::CURRENCY_ID_BONUS;

        $updateSql = <<<SQL
            update {$accountTable}
               set `currency_id` = {$bonusCurrencyId}
             where `acc_kind_id` in (7,8)
SQL;

        $this->execute($updateSql);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
