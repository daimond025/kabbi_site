<?php

use yii\db\Migration;

class m170524_050402_edit_type_column_in_taxi_tariff_table extends Migration
{
    const TABLE_NAME = '{{%taxi_tariff}}';

    public function up()
    {
        $this->alterColumn(self::TABLE_NAME, 'type', 'ENUM("BASE", "COMPANY", "COMPANY_CORP_BALANCE", "ALL") NOT NULL DEFAULT "ALL"');
    }

    public function down()
    {
        $this->alterColumn(self::TABLE_NAME, 'type', 'ENUM("BASE", "COMPANY", "COMPANY_CORP_BALANCE") NOT NULL DEFAULT "BASE"');
    }
}
