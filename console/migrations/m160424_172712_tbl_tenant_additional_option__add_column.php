<?php

use yii\db\Migration;

class m160424_172712_tbl_tenant_additional_option__add_column extends Migration
{
    const TABLE_TENANT_ADDITIONAL_OPTION = '{{%tenant_additional_option}}';
    const TABLE_PAYMENT_FOR_ADDITIONAL_OPTION = '{{%payment_for_additional_option}}';
    const COLUMN_PAYMENT_FOR_OPTION_ID = 'payment_for_option_id';
    const FOREIGN_KEY_NAME = 'fk_tenant_additional_option__payment_for_option_id';

    public function up()
    {
        $this->addColumn(self::TABLE_TENANT_ADDITIONAL_OPTION, self::COLUMN_PAYMENT_FOR_OPTION_ID, $this->integer());
        $this->addForeignKey(self::FOREIGN_KEY_NAME, self::TABLE_TENANT_ADDITIONAL_OPTION,
            self::COLUMN_PAYMENT_FOR_OPTION_ID,
            self::TABLE_PAYMENT_FOR_ADDITIONAL_OPTION, 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey(self::FOREIGN_KEY_NAME, self::TABLE_TENANT_ADDITIONAL_OPTION);
        $this->dropColumn(self::TABLE_TENANT_ADDITIONAL_OPTION, self::COLUMN_PAYMENT_FOR_OPTION_ID);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
