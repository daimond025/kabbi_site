<?php

use yii\db\Migration;

class m170112_114741_notification_has_position__table extends Migration
{
    const TABLE_NAME = '{{%notification_has_position}}';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'              => $this->primaryKey(),
            'notification_id' => $this->integer(11)->unsigned()->notNull(),
            'position_id'     => $this->integer(10)->notNull(),
        ]);

        $this->addForeignKey('fk_notification_has_position__notification_id', self::TABLE_NAME, 'notification_id',
            '{{%notification}}', 'notification_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_notification_has_position__position_id', self::TABLE_NAME, 'position_id',
            '{{%position}}', 'position_id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_notification_has_position__position_id', self::TABLE_NAME);
        $this->dropForeignKey('fk_notification_has_position__notification_id', self::TABLE_NAME);
        $this->dropTable(self::TABLE_NAME);
    }
}
