<?php

use yii\db\Migration;

class m160704_074520_add_new_language_column extends Migration
{
    public function up()
    {
        $this->addColumn('{{%country}}', 'name_uz', $this->string());
        $this->alterColumn('{{%country}}', 'name', $this->string()->notNull());
        $this->alterColumn('{{%country}}', 'name_en', $this->string());
        $this->alterColumn('{{%country}}', 'name_az', $this->string());
        $this->alterColumn('{{%country}}', 'name_de', $this->string());
        $this->alterColumn('{{%country}}', 'name_sr', $this->string());
        $this->alterColumn('{{%country}}', 'name_ro', $this->string());

        $this->addColumn('{{%republic}}', 'name_uz', $this->string());
        $this->addColumn('{{%republic}}', 'shortname_uz', $this->string());

        $this->addColumn('{{%city}}', 'name_uz', $this->string());
        $this->addColumn('{{%city}}', 'fulladdress_uz', $this->string());
    }

    public function down()
    {
        echo "m160704_074520_add_new_language_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
