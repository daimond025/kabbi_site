<?php

use yii\db\Migration;

class m180521_130809_craete_default_email_settings_table extends Migration
{
    const TABLE_NAME = '{{%default_email_settings}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'setting_id'      => $this->primaryKey(10)->unsigned(),
            'provider_server' => $this->string(255)->notNull(),
            'provider_port'   => $this->integer(5)->unsigned()->notNull(),
            'sender_name'     => $this->string(100)->notNull(),
            'sender_email'    => $this->string(255)->notNull(),
            'sender_password' => $this->string(100)->notNull(),
            'template'        => $this->text()->notNull(),
        ]);

        $this->insert(self::TABLE_NAME, [
            'provider_server' => 'smtp.yandex.ru',
            'provider_port'   => 465,
            'sender_name'     => 'info@gootax.pro',
            'sender_email'    => 'info@gootax.pro',
            'sender_password' => 'wiehb37ibrfhurbgjhr1',
            'template'        => $this->getTemplate(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }


    private function getTemplate()
    {
        return <<<TXT
<table cellpadding="0" cellspacing="0" border="0" width="100%">
        <tbody><tr>
            <td>
                <div style="padding:20px 10px 20px 6%;">
                    <font face="tahoma,sans-serif" color="#000000" size="2">
                    
                        #CONTENT#

                    </font>
                    <div style="border-top:1px #e7e7e7 solid;padding:20px 0;margin-top:20px;">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tbody>
                                <tr>
                                    <td>
                                        <font face="tahoma,sans-serif" color="#a8a4a3" size="2">Наш сайт  gootax.pro <br>Возникли вопросы? Пишите на <a href="mailto:sales@gootax.pro" class="ns-action" data-click-action="common.go" data-params="new_window&amp;url=%23compose%3Fmailto%3Dsales%2540gootax.pro">sales@gootax.pro</a></font></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>
TXT;
    }
}
