<?php

use yii\db\Migration;

class m180301_074644_add_access_token_for_extrinsic_dispatcher extends Migration
{

    const TABLE_NAME = 'tbl_user';

    public function safeUp()
    {

        $sql      = 'SELECT user_id FROM ' . self::TABLE_NAME . ' WHERE position_id=14 AND (access_token IS NULL OR access_token="")';
        $security = Yii::$app->security;

        $extrinsicDispatchers = $this->db->createCommand($sql)->queryAll();

        foreach ($extrinsicDispatchers as $extrinsicDispatcher) {
            $this->db->createCommand()
                ->update(
                    self::TABLE_NAME,
                    ['access_token' => $security->generatePasswordHash($security->generateRandomString())],
                    ['user_id' => $extrinsicDispatcher['user_id']]
                )
                ->execute();
        }
    }

    public function safeDown()
    {

    }
}
