<?php

use yii\db\Schema;
use yii\db\Migration;

class m151120_132955_tbl_currency_new_currency extends Migration {

    const TABLE_NAME = '{{%currency}}';

    public function up() {
        $this->insert(self::TABLE_NAME, ['name' => 'Belarusian ruble', 'code' => 'BYR', 'symbol' => 'BYR', 'type_id' => 1]);
        $this->execute("UPDATE" . self::TABLE_NAME . " SET name = 'Russian ruble' where name='Rubl'");
    }

    public function down() {
        echo "m151120_132955_tbl_currency_new_currency cannot be reverted.\n";

        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
