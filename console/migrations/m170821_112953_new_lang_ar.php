<?php

use yii\db\Migration;

class m170821_112953_new_lang_ar extends Migration
{
    const TABLE_CITY = '{{%city}}';
    const TABLE_COUNTRY = '{{%country}}';
    const TABLE_PLACE = '{{%public_place}}';
    const TABLE_REPUBLIC = '{{%republic}}';
    const TABLE_STREET = '{{%street}}';

    public function up()
    {
        $this->addColumnsByLang('ar');
    }

    public function down()
    {
        $this->dropColumnsByLang('ar');
    }


    private function addColumnsByLang($lang)
    {
        $this->addColumn(self::TABLE_CITY, 'name_' . $lang, $this->string(100));
        $this->addColumn(self::TABLE_CITY, 'fulladdress_' . $lang, $this->string(255));
        $this->addColumn(self::TABLE_COUNTRY, 'name_' . $lang, $this->string(255));
        //        $this->addColumn(self::TABLE_PLACE, 'label_' . $lang, $this->string(100));
        $this->addColumn(self::TABLE_REPUBLIC, 'name_' . $lang, $this->string(100));
        $this->addColumn(self::TABLE_REPUBLIC, 'shortname_' . $lang, $this->string(255));

        //        $this->addColumn(self::TABLE_STREET, 'city_name_' . $lang, $this->string(100));
        //        $this->addColumn(self::TABLE_STREET, 'street_name_' . $lang, $this->string(100));
        //        $this->addColumn(self::TABLE_STREET, 'street_shortname_' . $lang, $this->string(100));
        //        $this->addColumn(self::TABLE_STREET, 'fulladdress_' . $lang, $this->string(200));
        //        $this->addColumn(self::TABLE_STREET, 'street_area_' . $lang, $this->string(255));


    }

    private function dropColumnsByLang($lang)
    {
        $this->dropColumn(self::TABLE_CITY, 'name_' . $lang);
        $this->dropColumn(self::TABLE_CITY, 'fulladdress_' . $lang);
        $this->dropColumn(self::TABLE_COUNTRY, 'name_' . $lang);
        //        $this->dropColumn(self::TABLE_PLACE, 'label_' . $lang);
        $this->dropColumn(self::TABLE_REPUBLIC, 'name_' . $lang);
        $this->dropColumn(self::TABLE_REPUBLIC, 'shortname_' . $lang);

        //        $this->dropColumn(self::TABLE_STREET, 'city_name_' . $lang);
        //        $this->dropColumn(self::TABLE_STREET, 'street_name_' . $lang);
        //        $this->dropColumn(self::TABLE_STREET, 'street_shortname_' . $lang);
        //        $this->dropColumn(self::TABLE_STREET, 'fulladdress_' . $lang);
        //        $this->dropColumn(self::TABLE_STREET, 'street_area_' . $lang);

    }
}
