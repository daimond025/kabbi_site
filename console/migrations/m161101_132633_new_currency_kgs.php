<?php

use yii\db\Migration;

class m161101_132633_new_currency_kgs extends Migration
{
    const TABLE_NAME = '{{%currency}}';

    public function up()
    {
        $this->insert(self::TABLE_NAME,[
            'type_id' => 1,
            'name' => 'Som',
            'code' => 'KGS',
            'symbol' => 'с',
            'numeric_code' => 417,
            'minor_unit' => 2,
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME,['code' => 'KGS']);
    }
}