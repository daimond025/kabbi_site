<?php

use yii\db\Schema;
use yii\db\Migration;

class m151015_101955_tbl_tenant_has_admin_tariff extends Migration
{
    const TABLE_NAME = '{{%tenant_has_admin_tariff}}';
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'tariff_id' => $this->primaryKey()->notNull(),
            'tenant_id' => $this->integer(5)->notNull(),
            'create_date' => $this->dateTime()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }


    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
