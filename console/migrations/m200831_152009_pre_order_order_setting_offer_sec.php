<?php

use yii\db\Migration;
use common\modules\tenant\models\DefaultSettings;

class m200831_152009_pre_order_order_setting_offer_sec extends Migration
{
    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const TABLE_TENANT_SETTING = '{{%tenant_setting}}';

    const FREE_ORDER_OFFER_SEC = 'FREE_ORDER_OFFER_SEC';
    const PRE_ORDER_OFFER_SEC =  'PRE_ORDER_OFFER_SEC';

    const SETTING_TYPE = 'orders';
    public function safeUp()
    {
        $FREE_ORDER_OFFER_SEC = new DefaultSettings([
            'name'  => self::FREE_ORDER_OFFER_SEC,
            'value' => '0',
            'type'  => self::SETTING_TYPE,
        ]);

        $PRE_ORDER_OFFER_SEC = new DefaultSettings([
            'name'  => self::PRE_ORDER_OFFER_SEC,
            'value' => '0',
            'type'  => self::SETTING_TYPE,
        ]);

        return $FREE_ORDER_OFFER_SEC->save() && $PRE_ORDER_OFFER_SEC->save()  ;
    }
    public function safeDown()
    {
        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::FREE_ORDER_OFFER_SEC,
            'type' => self::SETTING_TYPE,
        ]);

        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::PRE_ORDER_OFFER_SEC,
            'type' => self::SETTING_TYPE,
        ]);

        $this->delete(self::TABLE_TENANT_SETTING, ['name' => self::FREE_ORDER_OFFER_SEC, 'type' => self::SETTING_TYPE]);
        $this->delete(self::TABLE_TENANT_SETTING, ['name' => self::PRE_ORDER_OFFER_SEC, 'type' => self::SETTING_TYPE]);
    }

}
