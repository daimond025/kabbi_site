<?php

use yii\db\Migration;

class m170210_090127_auto_wait_in_cleint_tariffs extends Migration
{

    const TABLE_NAME = '{{%taxi_tariff}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'auto_downtime', $this->integer(1)->defaultValue(1)->notNull()->after('sort'));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'auto_downtime');
    }
}
