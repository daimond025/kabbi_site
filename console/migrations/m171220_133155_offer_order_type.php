<?php

use yii\db\Migration;
use common\modules\tenant\models\DefaultSettings;

class m171220_133155_offer_order_type extends Migration
{
    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const TABLE_TENANT_SETTING = '{{%tenant_setting}}';

    const SETTING_NAME_OFFER_ORDER_TYPE = 'OFFER_ORDER_TYPE';
    const SETTING_VALUE_OFFER_ORDER_TYPE = 'queue';
    const SETTING_TYPE_OFFER_ORDER_TYPE = DefaultSettings::TYPE_ORDERS;

    public function safeUp()
    {
        $requirePassword = new DefaultSettings([
            'name'  => self::SETTING_NAME_OFFER_ORDER_TYPE,
            'value' => self::SETTING_VALUE_OFFER_ORDER_TYPE,
            'type'  => self::SETTING_TYPE_OFFER_ORDER_TYPE
        ]);

        $requirePassword->save();
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::SETTING_NAME_OFFER_ORDER_TYPE,
            'type' => self::SETTING_TYPE_OFFER_ORDER_TYPE,
        ]);

        $this->delete(self::TABLE_TENANT_SETTING, [
            'name' => self::SETTING_NAME_OFFER_ORDER_TYPE,
            'type' => self::SETTING_TYPE_OFFER_ORDER_TYPE,
        ]);
    }

}
