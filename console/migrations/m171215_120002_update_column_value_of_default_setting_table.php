<?php

use yii\db\Migration;

class m171215_120002_update_column_value_of_default_setting_table extends Migration
{
    const TABLE_NAME = '{{%default_settings}}';
    const TYPE = 'drivers';
    const NAME = 'SHOW_WORKER_CLIENT';

    public function safeUp()
    {
        $this->update(self::TABLE_NAME,['value'=>0],['value'=>1,'type' => self::TYPE, 'name' => self::NAME]);
        return true;
    }

    public function safeDown()
    {
        $this->update(self::TABLE_NAME,['value'=>1],['value'=>0,'type' => self::TYPE, 'name' => self::NAME]);
        return true;
    }

}
