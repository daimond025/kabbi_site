<?php

use yii\db\Migration;

class m160331_121758_tbl_tariff_permission__add_column extends Migration
{
    const TABLE_NAME = '{{%tariff_permission}}';
    const COLUMN_NAME = 'value';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_NAME, $this->integer()->notNull());
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
