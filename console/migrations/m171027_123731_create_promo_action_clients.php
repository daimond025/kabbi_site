<?php

use yii\db\Migration;

class m171027_123731_create_promo_action_clients extends Migration
{
    const TABLE_NAME = '{{%promo_action_clients}}';

    const COLUMN_ACTION_CLIENTS_ID = 'action_clients_id';
    const COLUMN_NAME = 'name';

    const DEFAULT_VALUE = [
        ['Only for new customers'],
        ['For all customers'],
    ];

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            self::COLUMN_ACTION_CLIENTS_ID => $this->primaryKey(),
            self::COLUMN_NAME              => $this->string(255)->notNull(),
        ]);

        $this->batchInsert(
            self::TABLE_NAME,
            [self::COLUMN_NAME],
            self::DEFAULT_VALUE
        );
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }


}
