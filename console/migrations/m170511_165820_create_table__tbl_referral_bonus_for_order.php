<?php

use yii\db\Migration;

class m170511_165820_create_table__tbl_referral_bonus_for_order extends Migration
{
    const TABLE_REFERRAL_BONUS_FOR_ORDER = '{{%referral_bonus_for_order}}';
    const TABLE_CLIENT = '{{%client}}';
    const TABLE_ORDER = '{{%order}}';
    const UNIQUE_INDEX = 'ui_referral_bonus_for_order';
    const FOREIGN_KEY_REFERRER_ID = 'fk_referral_bonus_for_order__referrer_id';
    const FOREIGN_KEY_REFERRAL_ID = 'fk_referral_bonus_for_order__referral_id';
    const FOREIGN_KEY_ORDER_ID = 'fk_referral_bonus_for_order__order_id';

    public function up()
    {
        $this->createTable(self::TABLE_REFERRAL_BONUS_FOR_ORDER, [
            'id'             => $this->primaryKey(),
            'order_id'       => $this->integer(10)->unsigned()->notNull(),
            'referrer_id'    => $this->integer(10)->unsigned()->notNull(),
            'referrer_bonus' => $this->decimal(20, 5)->defaultValue(0)->notNull(),
            'referral_id'    => $this->integer(10)->unsigned()->notNull(),
            'referral_bonus' => $this->decimal(20, 5)->defaultValue(0)->notNull(),
            'completed'      => $this->integer(1)->defaultValue(0)->notNull(),
            'created_at'     => $this->integer(),
            'updated_at'     => $this->integer(),
        ]);

        $this->createIndex(
            self::UNIQUE_INDEX, self::TABLE_REFERRAL_BONUS_FOR_ORDER, 'order_id', true);

        $this->addForeignKey(self::FOREIGN_KEY_REFERRER_ID, self::TABLE_REFERRAL_BONUS_FOR_ORDER, 'referrer_id',
            self::TABLE_CLIENT, 'client_id', 'CASCADE', 'CASCADE');

        $this->addForeignKey(self::FOREIGN_KEY_REFERRAL_ID, self::TABLE_REFERRAL_BONUS_FOR_ORDER, 'referral_id',
            self::TABLE_CLIENT, 'client_id', 'CASCADE', 'CASCADE');

        $this->addForeignKey(self::FOREIGN_KEY_ORDER_ID, self::TABLE_REFERRAL_BONUS_FOR_ORDER, 'order_id',
            self::TABLE_ORDER, 'order_id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable(self::TABLE_REFERRAL_BONUS_FOR_ORDER);
    }

}
