<?php

use yii\db\Migration;

class m171027_123922_create_promo_has_car_class extends Migration
{

    const TABLE_NAME = '{{%promo_has_car_class}}';

    const COLUMN_CLASS_ID = 'car_class_id';
    const COLUMN_PROMO_ID = 'promo_id';

    const FK_PROMO_HAS_CAR__CLASS_ID = 'fk_promo_has_car_class__car_class_id';
    const FK_PROMO_HAS_CAR__PROMO_ID = 'fk_promo_has_car_class__promo_id';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            self::COLUMN_PROMO_ID => $this->integer()->unsigned()->notNull(),
            self::COLUMN_CLASS_ID => 'tinyint(3) UNSIGNED NOT NULL',
        ]);

        $this->addPrimaryKey(
            'pk_promo_has_car_class', self::TABLE_NAME,
            [self::COLUMN_PROMO_ID, self::COLUMN_CLASS_ID]
        );

        $this->addForeignKey(self::FK_PROMO_HAS_CAR__CLASS_ID, self::TABLE_NAME,
            self::COLUMN_CLASS_ID, '{{%car_class}}', 'class_id', 'CASCADE','CASCADE');

        $this->addForeignKey(self::FK_PROMO_HAS_CAR__PROMO_ID, self::TABLE_NAME,
            self::COLUMN_PROMO_ID, '{{%promo}}', self::COLUMN_PROMO_ID, 'CASCADE','CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_PROMO_HAS_CAR__CLASS_ID, self::TABLE_NAME);
        $this->dropForeignKey(self::FK_PROMO_HAS_CAR__PROMO_ID, self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
    }
}
