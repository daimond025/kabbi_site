<?php

use yii\db\Schema;
use yii\db\Migration;

class m181214_122102_add_type_sms_notification extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%notification}}', 'type_notice', \yii\db\Schema::TYPE_INTEGER);
    }

    public function safeDown()
    {

        $this->dropColumn('{{%notification}}', 'type_notice', \yii\db\Schema::TYPE_INTEGER);

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181214_122102_add_type_sms_notification cannot be reverted.\n";

        return false;
    }
    */
}
