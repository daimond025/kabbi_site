<?php

use yii\db\Migration;

class m171009_073831_rename_geo_provider_gootax_to_osm extends Migration
{
    const TABLE_NAME = '{{%auto_geo_service_type}}';

    public function safeUp()
    {
        $this->update(self::TABLE_NAME, [
            'service_label' => 'OSM'
        ], ['id' => 5]);
        return true;
    }

    public function safeDown()
    {
        $this->update(self::TABLE_NAME, [
            'service_label' => 'Gootax'
        ], ['id' => 5]);
        return true;
    }

}
