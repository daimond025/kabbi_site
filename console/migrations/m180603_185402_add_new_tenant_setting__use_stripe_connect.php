<?php

use common\modules\tenant\models\DefaultSettings;
use yii\db\Migration;

class m180603_185402_add_new_tenant_setting__use_stripe_connect extends Migration
{
    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const TABLE_TENANT_SETTING = '{{%tenant_setting}}';
    const SETTING_USE_STRIPE_CONNECT = 'USE_STRIPE_CONNECT';

    public function safeUp()
    {
        $settings = new DefaultSettings();
        $settings->name = self::SETTING_USE_STRIPE_CONNECT;
        $settings->type = DefaultSettings::TYPE_GENERAL;
        $settings->value = '0';

        $settings->save();
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_TENANT_SETTING, ['name' => self::SETTING_USE_STRIPE_CONNECT]);
        $this->delete(self::TABLE_DEFAULT_SETTINGS, ['name' => self::SETTING_USE_STRIPE_CONNECT]);
    }
}
