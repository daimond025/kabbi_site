<?php

use yii\db\Migration;

class m170731_100950_edit_type_geo_coords_in_client_address_history_table extends Migration
{
    const TABLE_NAME = '{{%client_address_history}}';

    public function safeUp()
    {
        $this->alterColumn(self::TABLE_NAME, 'lat', $this->string(100)->notNull());
        $this->alterColumn(self::TABLE_NAME, 'lon', $this->string(100)->notNull());
    }

    public function safeDown()
    {
        $this->alterColumn(self::TABLE_NAME, 'lat', $this->float()->notNull());
        $this->alterColumn(self::TABLE_NAME, 'lon', $this->float()->notNull());
    }
}
