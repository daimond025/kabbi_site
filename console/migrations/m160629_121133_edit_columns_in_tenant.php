<?php

use yii\db\Migration;

class m160629_121133_edit_columns_in_tenant extends Migration
{

    const TABLE_NAME = '{{%tenant}}';

    public function up()
    {
        $this->alterColumn(self::TABLE_NAME,'site', 'VARCHAR(45)');
        $this->alterColumn(self::TABLE_NAME,'ogrn', 'VARCHAR(45)');
    }

    public function down()
    {
        $this->alterColumn(self::TABLE_NAME,'site', 'VARCHAR(20)');
        $this->alterColumn(self::TABLE_NAME,'ogrn', 'VARCHAR(20)');
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
