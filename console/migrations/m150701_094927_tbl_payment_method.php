<?php

use yii\db\Schema;
use yii\db\Migration;

class m150701_094927_tbl_payment_method extends Migration
{
    public function up()
    {
        $this->insert('{{%payment_method}}', ['payment' => 'TERMINAL_ELECSNET', 'name' => 'Elecsnet']);
    }

    public function down()
    {
        echo "m150701_094927_tbl_payment_method cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
