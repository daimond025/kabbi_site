<?php

use yii\db\Schema;
use yii\db\Migration;

class m150529_085513_tbl_user extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'auth_exp', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'auth_exp');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
