<?php

use yii\db\Schema;
use yii\db\Migration;

class m150916_122406_tbl_hints extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%hints}}', 'text', 'varchar(255)');
    }

    public function down()
    {
        echo "m150916_122406_tbl_hints cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
