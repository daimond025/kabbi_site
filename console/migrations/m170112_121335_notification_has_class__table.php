<?php

use yii\db\Migration;

class m170112_121335_notification_has_class__table extends Migration
{
    const TABLE_NAME = '{{%notification_has_class}}';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'              => $this->primaryKey(),
            'notification_id' => $this->integer(11)->unsigned()->notNull(),
            'class_id'        => 'tinyint(3) UNSIGNED NOT NULL',
        ]);

        $this->addForeignKey('fk_notification_has_class__notification_id', self::TABLE_NAME, 'notification_id',
            '{{%notification}}', 'notification_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_notification_has_class__class_id', self::TABLE_NAME, 'class_id',
            '{{%car_class}}', 'class_id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_notification_has_class__class_id', self::TABLE_NAME);
        $this->dropForeignKey('fk_notification_has_class__notification_id', self::TABLE_NAME);
        $this->dropTable(self::TABLE_NAME);
    }
}
