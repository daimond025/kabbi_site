<?php

use yii\db\Schema;
use yii\db\Migration;

class m150909_064123_tbl_street_resp_to_respublica extends Migration
{
    const TABLE_NAME = '{{%street}}';
    public function up()
    {
        $this->execute("UPDATE" . self::TABLE_NAME ." SET fulladdress = REPLACE(fulladdress, 'Респ,', 'Республика,')");
    }

    public function down()
    {
        echo "m150909_064123_tbl_street_resp_to_respublica cannot be reverted.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
