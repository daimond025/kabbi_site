<?php

use yii\db\Schema;
use yii\db\Migration;

class m150605_053238_tbl_default_client_status_event extends Migration
{
    public function up()
    {
        $this->batchInsert('{{%default_client_status_event}}', ['status_id', 'group', 'notice'], [[52, 'CALL', 'autocall'], [52, 'APP', 'push'], [52, 'WEB', 'autocall']]);
    }

    public function down()
    {
        $this->delete('{{%default_client_status_event}}', ['status_id' => 52]);
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
