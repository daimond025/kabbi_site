<?php

use yii\db\Migration;

class m170425_093001_tbl_default_settings__fix_setting__check_worker_rating extends Migration
{
    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const SETTING_NAME = 'CHECK_WORKER_RATING_TO_OFFER_ORDER';
    const SETTING_TYPE_ORDERS = 'orders';

    public function up()
    {
        $this->update(self::TABLE_DEFAULT_SETTINGS,
            ['type' => self::SETTING_TYPE_ORDERS],
            ['name' => self::SETTING_NAME]);
    }

    public function down()
    {
    }

}
