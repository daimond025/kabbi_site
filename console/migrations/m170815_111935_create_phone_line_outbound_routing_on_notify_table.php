<?php

use yii\db\Migration;

/**
 * Handles the creation of table `phone_line_outbound_routing_on_notify`.
 */
class m170815_111935_create_phone_line_outbound_routing_on_notify_table extends Migration
{
    const TABLE_NAME = '{{%phone_line_outbound_routing_on_notify}}';
    const TABLE_ROUTING = '{{%phone_line_outbound_routing}}';
    const TABLE_PHONE = '{{%phone_line}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'                 => $this->primaryKey()->unsigned(),
            'routing_id'         => $this->integer(10)->unsigned()->notNull(),
            'line_id'            => $this->integer(10)->unsigned()->notNull(),
            'sort'               => $this->integer(6)->unsigned()->notNull()->defaultValue(100),
            'timeout'            => $this->integer(10)->unsigned()->notNull()->defaultValue(60),
            'distribution_count' => $this->integer(10)->unsigned()->notNull()->defaultValue(60),
            'distribution_delay' => $this->integer(10)->unsigned()->notNull()->defaultValue(60),
        ]);

        $this->addForeignKey('FK_routing_id__phone_line_outbound_routing_on_notify', self::TABLE_NAME,
            'routing_id', self::TABLE_ROUTING, 'routing_id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('FK_line_id__phone_line_outbound_routing_on_notify', self::TABLE_NAME, 'line_id',
            self::TABLE_PHONE, 'line_id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('FK_line_id__phone_line_outbound_routing_on_notify', self::TABLE_NAME);
        $this->dropForeignKey('FK_routing_id__phone_line_outbound_routing_on_notify', self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
    }
}
