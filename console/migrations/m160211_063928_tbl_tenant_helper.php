<?php

use yii\db\Schema;
use yii\db\Migration;
use yii\db\Query;
use common\modules\tenant\models\TenantHelper;

class m160211_063928_tbl_tenant_helper extends Migration
{
    public function up()
    {
        $tenants = (new Query())
                ->from('{{%tenant_has_city}}')
                ->select('tenant_id')
                ->distinct()
                ->column();
        TenantHelper::updateAll(['city' => 1], ['tenant_id' => $tenants]);
    }

    public function down()
    {
        echo "m160211_063928_tbl_tenant_helper cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
