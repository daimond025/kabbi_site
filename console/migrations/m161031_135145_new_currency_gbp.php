<?php

use yii\db\Migration;

class m161031_135145_new_currency_gbp extends Migration
{
    const TABLE_NAME = '{{%currency}}';

    public function up()
    {
        $this->insert(self::TABLE_NAME,[
            'type_id' => 1,
            'name' => 'Pound Sterling',
            'code' => 'GBP',
            'symbol' => '£',
            'numeric_code' => 826,
            'minor_unit' => 2,
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME,['code' => 'GBP']);
    }
}