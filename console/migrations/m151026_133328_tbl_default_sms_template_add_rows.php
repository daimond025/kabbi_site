<?php

use yii\db\Schema;
use yii\db\Migration;

class m151026_133328_tbl_default_sms_template_add_rows extends Migration
{

    public function up()
    {
        $this->insert('{{%default_sms_template}}', ['text' => '#TAXI_NAME#. Ваш водитель #BRAND# #MODEL# #COLOR# #NUMBER#', 'type' => '7', 'description' => '#TAXI_NAME# - Название службы такси<br>#BRAND# - Марка<br>#MODEL# - Модель<br>#COLOR# - Цвет<br>#NUMBER# - Гос. номер']);
        $this->insert('{{%default_sms_template}}', ['text' => '#TAXI_NAME#. Водитель снят с вашего заказа. Ожидайте назначения другого водителя.', 'type' => '10', 'description' => '#TAXI_NAME# - Название службы такси <br>']);
    }

    public function down()
    {
        $this->delete('{{%default_sms_template}}', ['text' => '#TAXI_NAME#. Ваш водитель #BRAND# #MODEL# #COLOR# #NUMBER#', 'type' => '7', 'description' => '#TAXI_NAME# - Название службы такси<br>#BRAND# - Марка<br>#MODEL# - Модель<br>#COLOR# - Цвет<br>#NUMBER# - Гос. номер']);
        $this->delete('{{%default_sms_template}}', ['text' => '#TAXI_NAME#. Водитель снят с вашего заказа. Ожидайте назначения другого водителя.', 'type' => '10', 'description' => '#TAXI_NAME# - Название службы такси <br>']);
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
