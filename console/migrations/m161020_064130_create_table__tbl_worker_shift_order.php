<?php

use yii\db\Migration;

class m161020_064130_create_table__tbl_worker_shift_order extends Migration
{
    const TABLE_WORKER_SHIFT_ORDER = '{{%worker_shift_order}}';
    const TABLE_ORDER = '{{%order}}';
    const TABLE_WORKER_SHIFT = '{{%worker_shift}}';
    const COLUMN_ORDER_ID = 'order_id';
    const FOREIGN_KEY_ORDER_ID = 'fk_worker_shift_order__order_id';
    const COLUMN_SHIFT_ID = 'shift_id';
    const FOREIGN_KEY_SHIFT_ID = 'fk_worker_shift_order__shift_id';

    public function up()
    {
        $this->createTable(self::TABLE_WORKER_SHIFT_ORDER, [
            'id'         => $this->primaryKey(),
            'shift_id'   => $this->integer(10)->unsigned()->notNull(),
            'order_id'   => $this->integer(10)->unsigned()->notNull()->unique(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(
            self::FOREIGN_KEY_ORDER_ID,
            self::TABLE_WORKER_SHIFT_ORDER,
            self::COLUMN_ORDER_ID,
            self::TABLE_ORDER,
            self::COLUMN_ORDER_ID,
            'CASCADE',
            'CASCADE');

        $this->addForeignKey(
            self::FOREIGN_KEY_SHIFT_ID,
            self::TABLE_WORKER_SHIFT_ORDER,
            self::COLUMN_SHIFT_ID,
            self::TABLE_WORKER_SHIFT,
            'id',
            'CASCADE',
            'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey(self::FOREIGN_KEY_ORDER_ID, self::TABLE_WORKER_SHIFT_ORDER);
        $this->dropForeignKey(self::FOREIGN_KEY_SHIFT_ID, self::TABLE_WORKER_SHIFT_ORDER);
        $this->dropTable(self::TABLE_WORKER_SHIFT_ORDER);
    }

}
