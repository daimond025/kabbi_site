<?php

use yii\db\Migration;

class m160602_055802_create_table__tenant_notification_log extends Migration
{
    const TABLE_TENANT_NOTIFICATION_LOG = '{{%tenant_notification_log}}';
    const TABLE_TENANT = '{{%tenant}}';
    const COLUMN_TENANT_ID = 'tenant_id';
    const FOREIGN_KEY_TENANT_ID = 'fk_tenant_notification_log__tenant_id';

    public function up()
    {
        $this->createTable(self::TABLE_TENANT_NOTIFICATION_LOG, [
            'id'         => $this->primaryKey(),
            'tenant_id'  => $this->integer(10)->unsigned()->notNull(),
            'type'       => 'ENUM("EMAIL", "SMS") NOT NULL',
            'reason'     => 'ENUM("ADVANCE_NOTICE", "TARIFF_EXPIRED_NOTICE") NOT NULL',
            'to'         => $this->string()->notNull(),
            'content'    => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
        $this->addForeignKey(self::FOREIGN_KEY_TENANT_ID, self::TABLE_TENANT_NOTIFICATION_LOG,
            self::COLUMN_TENANT_ID, self::TABLE_TENANT, self::COLUMN_TENANT_ID, 'CASCADE');
    }

    public function down()
    {
        $this->dropTable(self::TABLE_TENANT_NOTIFICATION_LOG);
    }
}
