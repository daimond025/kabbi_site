<?php

use yii\db\Schema;
use yii\db\Migration;

class m150928_073349_driver_groups extends Migration
{
    public function up()
    {
        $this->execute('SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0');
        $this->execute('SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0');
        $this->execute("SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES'");

        $this->addForeignKey('FK_driver_group_can_view_client_tariff_class_id', '{{%driver_group_can_view_client_tariff}}', 'class_id', '{{%car_class}}', 'class_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_driver_group_class_id', '{{%driver_group}}', 'class_id', '{{%car_class}}', 'class_id', 'CASCADE', 'CASCADE');

        $this->execute('SET SQL_MODE=@OLD_SQL_MODE');
        $this->execute('SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS');
        $this->execute('SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS');
    }

    public function down()
    {
        $this->dropForeignKey('FK_driver_group_can_view_client_tariff_group_id', '{{%driver_group_can_view_client_tariff}}');
        $this->dropForeignKey('FK_driver_group_class_id', '{{%driver_group}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
