<?php

use yii\db\Migration;

class m171208_070722_add_column_to_notification_table extends Migration
{
    const TABLE_NAME = '{{%notification}}';
    const COLUMN_NAME = 'blocked';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_NAME, $this->integer()->defaultValue(0));

        return true;
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);

        return true;
    }

}
