<?php

use yii\db\Migration;

class m201116_133438_geoAddressWorkerNote extends Migration
{
    const TABLE_NAME = '{{%geo_address}}';
    const WORKER_SHIFT_NOTE = '{{%worker_shift_note}}';
    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey()->notNull(),
            'provider' => $this->string(20)->defaultValue('google'),
            'place_id' => $this->string()->notNull(),
            'address' => $this->text(),
            'updated_at' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
        ]);
        $this->createIndex('placeIds', self::TABLE_NAME, 'place_id', true);

        $this->createTable(self::WORKER_SHIFT_NOTE, [
            'id' => $this->primaryKey()->notNull(),
            'worker_id' => $this->integer(11)->notNull(),
            'note' => $this->text(),
            'created_at' => $this->integer(),
        ]);
    }

    public function safeDown()
    {
        $this->dropIndex('placeIds', self::TABLE_NAME);
        $this->dropTable(self::TABLE_NAME);
        $this->dropTable(self::WORKER_SHIFT_NOTE);
    }

}
