<?php

use yii\db\Migration;

class m180405_062531_order_has_exchange_worker extends Migration
{
    const TABLE_NAME       = '{{%order_has_exchange_worker}}';
    const TABLE_ORDER_NAME = '{{%order}}';
    const FOREIGN_KEY      = 'fk_order_has_exchange_worker__order_id';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'                 => $this->primaryKey(),
            'order_id'           => $this->integer(10)->unsigned()->notNull(),
            'name'               => $this->string(255),
            'phone'              => $this->string(100),
            'car_description'    => $this->string(255),
            'color'              => $this->string(100),
            'gos_number'         => $this->string(100),
            'external_car_id'    => $this->string(100),
            'external_worker_id' => $this->string(100),
            'created_at'         => $this->integer(),
            'updated_at'         => $this->integer(),
        ]);

        $this->addForeignKey(
            self::FOREIGN_KEY,
            self::TABLE_NAME,
            'order_id',
            self::TABLE_ORDER_NAME,
            'order_id',
            'CASCADE',
            'CASCADE');
        return true;
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FOREIGN_KEY, self::TABLE_NAME);
        $this->dropTable(self::TABLE_NAME);
        return true;
    }


}
