<?php

use yii\db\Migration;

class m161102_183903_tbl_order_status__insert_new_statuses extends Migration
{
    const TABLE_ORDER_STATUS = '{{%order_status}}';

    const STATUS_ID_MANUAL_MODE = 108;
    const STATUS_ID_DRIVER_IGNORE_ORDER_OFFER = 109;
    const STATUS_ID_WAITING_FOR_PAYMENT = 110;

    public function up()
    {
        $this->batchInsert(self::TABLE_ORDER_STATUS,
            ['status_id', 'name', 'status_group', 'dispatcher_sees'],
            [
                [self::STATUS_ID_MANUAL_MODE, 'Manual mode', 'new', 1],
                [self::STATUS_ID_DRIVER_IGNORE_ORDER_OFFER, 'Driver ignored order offer', 'new', 0],
                [self::STATUS_ID_WAITING_FOR_PAYMENT, 'Waiting for payment', 'executing', 1],
            ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_ORDER_STATUS, [
            'status_id' => [
                self::STATUS_ID_MANUAL_MODE,
                self::STATUS_ID_DRIVER_IGNORE_ORDER_OFFER,
                self::STATUS_ID_WAITING_FOR_PAYMENT,
            ],
        ]);
    }

}
