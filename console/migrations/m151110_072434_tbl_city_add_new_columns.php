<?php

use yii\db\Schema;
use yii\db\Migration;

class m151110_072434_tbl_city_add_new_columns extends Migration
{

    public function up()
    {
        $this->addColumn('{{%city}}', 'name_az', 'VARCHAR(100) DEFAULT  NULL');
        $this->addColumn('{{%city}}', 'name_en', 'VARCHAR(100) DEFAULT  NULL');
        $this->addColumn('{{%city}}', 'name_de', 'VARCHAR(100) DEFAULT  NULL');
        $this->addColumn('{{%city}}', 'name_se', 'VARCHAR(100) DEFAULT  NULL');
        $this->addColumn('{{%city}}', 'fulladdress_az', 'VARCHAR(100) DEFAULT  NULL');
        $this->addColumn('{{%city}}', 'fulladdress_en', 'VARCHAR(100) DEFAULT  NULL');
        $this->addColumn('{{%city}}', 'fulladdress_de', 'VARCHAR(100) DEFAULT  NULL');
        $this->addColumn('{{%city}}', 'fulladdress_se', 'VARCHAR(100) DEFAULT  NULL');
    }

    public function down()
    {
        $this->dropColumn('{{%city}}', 'name_az');
        $this->dropColumn('{{%city}}', 'name_en');
        $this->dropColumn('{{%city}}', 'name_de');
        $this->dropColumn('{{%city}}', 'name_se');
        $this->dropColumn('{{%city}}', 'fulladdress_az');
        $this->dropColumn('{{%city}}', 'fulladdress_en');
        $this->dropColumn('{{%city}}', 'fulladdress_de');
        $this->dropColumn('{{%city}}', 'fulladdress_se');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
