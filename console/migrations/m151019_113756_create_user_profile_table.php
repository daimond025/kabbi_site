<?php

use yii\db\Schema;
use yii\db\Migration;

class m151019_113756_create_user_profile_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_profile}}', [
            'user_profile_id' => $this->primaryKey(),
            'community_user_id' => $this->integer()->notNull(),
            'company' => $this->string(),
            'position' => $this->string(),
            'phone' => $this->string(),
            'photo' => $this->string(),
            'automation_program' => $this->string(),
            'community_participation' => $this->string(),
        ], $tableOptions);
        $this->addForeignKey('fk_user_profile__community_user_id',
                '{{%user_profile}}', 'community_user_id',
                '{{%community_user}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%user_profile}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
