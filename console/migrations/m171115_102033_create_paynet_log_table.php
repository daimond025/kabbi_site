<?php

use yii\db\Migration;

/**
 * Handles the creation of table `paynet_log`.
 */
class m171115_102033_create_paynet_log_table extends Migration
{
    const TABLE_NAME = '{{%paynet_log}}';


    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'log_id'      => $this->primaryKey(),
            'type'        => $this->string(255),
            'username'    => $this->string(15),
            'serviceId'   => $this->integer(11),
            'callsign'    => $this->integer(8),
            'sum'         => $this->string(255),
            'data'        => $this->text(),
            'result'      => $this->string(255),
            'create_time' => $this->integer(11),
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
