<?php

use yii\db\Migration;

class m171027_124101_create_new_field_in_table_order extends Migration
{

    const TABLE_NAME = '{{%order}}';

    const COLUMN_PROMO_CODE_ID = 'promo_code_id';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_PROMO_CODE_ID, $this->integer()->unsigned());

        $this->createIndex(sprintf('idx_order__%s', self::COLUMN_PROMO_CODE_ID ), self::TABLE_NAME, self::COLUMN_PROMO_CODE_ID);
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_PROMO_CODE_ID);
    }

}
