<?php

use yii\db\Migration;

class m190808_144400_geo_key_default extends Migration
{
    const TABLE_NAME = '{{%auto_geo_key_default}}';

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable(self::TABLE_NAME, [
            'id'           => $this->primaryKey(),
            'row_id' => $this->string()->unique(),
            'service_type'       => $this->string(),
            'tenant_id'    => $this->string(),
            'tenant_domain' => $this->string(),
            'city_id'   => $this->integer(),
            'service_provider_id'   => $this->string(),
            'key_1'   => $this->string(),
            'key_2'   => $this->string(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }

}
