<?php

use yii\db\Migration;

class m170404_200851_tbl_tenant_setting__add_column__position_id extends Migration
{
    const TABLE_TENANT_SETTING = '{{%tenant_setting}}';
    const TABLE_POSITION = '{{%position}}';
    const COLUMN_POSITION_ID = 'position_id';

    const SETTING_TYPES = ['orders', 'drivers'];

    const POSITION_CLASS_TAXI_ID = 1;

    const FOREIGN_KEY_POSITION_ID = 'fk_tenant_setting__position_id';
    const UNIQUE_INDEX = 'tbl_tenant_setting__index_unique';

    public function up()
    {
        $this->addColumn(self::TABLE_TENANT_SETTING, self::COLUMN_POSITION_ID, $this->integer());
        $this->update(self::TABLE_TENANT_SETTING, [self::COLUMN_POSITION_ID => self::POSITION_CLASS_TAXI_ID],
            ['type' => self::SETTING_TYPES]);

        $this->addForeignKey(self::FOREIGN_KEY_POSITION_ID, self::TABLE_TENANT_SETTING, self::COLUMN_POSITION_ID,
            self::TABLE_POSITION, self::COLUMN_POSITION_ID, 'RESTRICT', 'RESTRICT');

        $this->dropIndex(self::UNIQUE_INDEX, self::TABLE_TENANT_SETTING);
        $this->createIndex(self::UNIQUE_INDEX, self::TABLE_TENANT_SETTING,
            ['tenant_id', 'name', 'city_id', self::COLUMN_POSITION_ID], true);
    }

    public function down()
    {
        $this->delete(self::TABLE_TENANT_SETTING, ['!=', 'position_id', self::POSITION_CLASS_TAXI_ID]);

        $this->dropIndex(self::UNIQUE_INDEX, self::TABLE_TENANT_SETTING);
        $this->createIndex(self::UNIQUE_INDEX, self::TABLE_TENANT_SETTING,
            ['tenant_id', 'name', 'city_id'], true);

        $this->dropForeignKey(self::FOREIGN_KEY_POSITION_ID, self::TABLE_TENANT_SETTING);
        $this->dropColumn(self::TABLE_TENANT_SETTING, self::COLUMN_POSITION_ID);
    }

}
