<?php

use yii\db\Migration;

class m160427_071142_add_sms_provider_smsg_uz extends Migration
{
    
    const TABLE_NAME = '{{%sms_server}}';
    
    public function up()
    {
        $this->insert(self::TABLE_NAME,[
            'server_id' => 8,
            'name' =>  'SMS Gateway (Узбекистан)',
            'host' => 'http://www.smsg.uz/',
            'active' => '1',
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME,['server_id' => 8]);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
