<?php

use yii\db\Migration;

class m170411_105226_tbl_module__rename_records extends Migration
{
    const TABLE_MODULE = '{{%module}}';

    public function up()
    {
        $this->update(self::TABLE_MODULE, ['name' => 'Cargo taxi'], ['id' => 2]);
        $this->update(self::TABLE_MODULE, ['name' => 'Couriers'], ['id' => 3]);
        $this->update(self::TABLE_MODULE, ['name' => 'Services'], ['id' => 4]);
    }

    public function down()
    {
    }

}
