<?php

use yii\db\Migration;

class m180720_091421_add_new_field_in_tbl_order__client_passenger_id extends Migration
{

    const TABLE_NAME = 'tbl_order';
    const COLUMN_NAME = 'client_passenger_id';

    public function up()
    {
        /**
         * @var $changer \console\components\mysqlOnlineSchemaChange\MysqlOnlineSchemaChange
         */
        $changer = \Yii::$app->mysqlOnlineSchemaChange;
        try{
            $changer->run(self::TABLE_NAME, sprintf('ADD COLUMN %s INT(10)', self::COLUMN_NAME), [
                'alterForeignKeysMethod' => $changer::ALTER_FOREIGN_KEY_DROP_SWAP,
                'execute'                => true,
                'debug'                  => false
            ]);
            return true;
        } catch (RuntimeException $ex){
            echo $ex->getMessage() . PHP_EOL;
            return false;
        }
    }

    public function down()
    {
        /**
         * @var $changer \console\components\mysqlOnlineSchemaChange\MysqlOnlineSchemaChange
         */
        $changer = \Yii::$app->mysqlOnlineSchemaChange;
        try{
            $changer->run(self::TABLE_NAME, sprintf('DROP COLUMN %s', self::COLUMN_NAME), [
                'alterForeignKeysMethod' => $changer::ALTER_FOREIGN_KEY_DROP_SWAP,
                'execute'                => true,
                'debug'                  => false
            ]);
            return true;
        } catch (RuntimeException $ex){
            echo $ex->getMessage() . PHP_EOL;
            return false;
        }
    }

}
