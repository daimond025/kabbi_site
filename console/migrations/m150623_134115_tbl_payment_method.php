<?php

use yii\db\Schema;
use yii\db\Migration;

class m150623_134115_tbl_payment_method extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%payment_method}}', [
            'payment' => Schema::TYPE_STRING . '(255) NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'PRIMARY KEY (payment)',
        ], $tableOptions);
        
        $arRows = [
            ['CASH', 'Cash'],
            ['CARD', 'Bank card'],
            ['PERSONAL_ACCOUNT', 'Personal account'],
            ['CORP_BALANCE', 'Company account'],
            ['TERMINAL_QIWI', 'QIWI'],
        ];
        $this->batchInsert('{{%payment_method}}', ['payment', 'name'], $arRows);
    }

    public function down()
    {
        $this->dropTable('{{%payment_method}}');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
