<?php

use yii\db\Migration;

class m161220_120539_tbl_default_settings__add_setting__max_number_of_address_points extends Migration
{
    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const SETTING_MAX_NUMBER_OF_ADDRESS_POINTS = 'MAX_NUMBER_OF_ADDRESS_POINTS';

    public function up()
    {
        $this->insert(self::TABLE_DEFAULT_SETTINGS, [
            'name'  => self::SETTING_MAX_NUMBER_OF_ADDRESS_POINTS,
            'value' => 20,
            'type'  => 'system',
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::SETTING_MAX_NUMBER_OF_ADDRESS_POINTS,
        ]);
    }

}
