<?php

use yii\db\Migration;

class m180201_122223_add_role_extrinsic_dispatcher_in_table_user_position extends Migration
{

    const TABLE_NAME = '{{%user_position}}';

    const ROLE = 'Extrinsic dispatcher';

    public function safeUp()
    {
        $this->insert(self::TABLE_NAME, [
            'name' => self::ROLE
        ]);
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_NAME, [
            'name' => self::ROLE
        ]);
    }
}
