<?php

use yii\db\Migration;

class m160906_114211_tbl_uds_game_order__add_column extends Migration
{
    const TABLE_UDS_GAME_ORDER = '{{%uds_game_order}}';
    const COLUMN_OPERATION_ID = 'operation_id';

    public function up()
    {
        $this->addColumn(
            self::TABLE_UDS_GAME_ORDER,
            self::COLUMN_OPERATION_ID,
            $this->string(20)
        );
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_UDS_GAME_ORDER, self::COLUMN_OPERATION_ID);
    }
}
