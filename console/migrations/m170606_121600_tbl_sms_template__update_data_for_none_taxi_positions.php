<?php

use yii\db\Migration;

class m170606_121600_tbl_sms_template__update_data_for_none_taxi_positions extends Migration
{
    const SQL_UPDATE_DATA_NONE_TAXI = <<<SQL
UPDATE `tbl_sms_template` AS `t`
JOIN `tbl_default_sms_template` AS `d` ON `t`.`type` = `d`.`type` AND `t`.`position_id` = `d`.`position_id`
SET `t`.`text` = `d`.`text`
WHERE `t`.`position_id` > 1
SQL;

    public function safeUp()
    {
        $this->execute(self::SQL_UPDATE_DATA_NONE_TAXI);
    }

    public function safeDown()
    {
    }

}
