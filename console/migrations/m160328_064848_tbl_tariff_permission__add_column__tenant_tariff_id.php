<?php

use yii\db\Migration;

class m160328_064848_tbl_tariff_permission__add_column__tenant_tariff_id extends Migration
{
    const TABLE_TENANT_TARIFF = '{{%tenant_tariff}}';
    const TABLE_TENANT_PERMISSION = '{{%tenant_permission}}';

    const COLUMN_TENANT_TARIFF_ID = 'tenant_tariff_id';

    public function generateForeignKeyName($tableName, $columnName)
    {
        return 'fk_' . $this->db->schema->getRawTableName($tableName)
        . '__' . $columnName;
    }

    public function up()
    {
        $this->addColumn(self::TABLE_TENANT_PERMISSION, self::COLUMN_TENANT_TARIFF_ID, $this->integer()->notNull());
        $this->addForeignKey($this->generateForeignKeyName(self::TABLE_TENANT_PERMISSION,
            self::COLUMN_TENANT_TARIFF_ID),
            self::TABLE_TENANT_PERMISSION, self::COLUMN_TENANT_TARIFF_ID,
            self::TABLE_TENANT_TARIFF, 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey(
            $this->generateForeignKeyName(self::TABLE_TENANT_PERMISSION, self::COLUMN_TENANT_TARIFF_ID),
            self::TABLE_TENANT_PERMISSION);
        $this->dropColumn(self::TABLE_TENANT_PERMISSION, self::COLUMN_TENANT_TARIFF_ID);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
