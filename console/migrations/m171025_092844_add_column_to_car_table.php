<?php

use yii\db\Migration;

class m171025_092844_add_column_to_car_table extends Migration
{

    const TABLE_NAME = '{{%car}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'callsign', $this->string(10));
        return true;
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'callsign');
        return true;
    }

}
