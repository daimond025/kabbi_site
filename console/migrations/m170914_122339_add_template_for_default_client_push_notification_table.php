<?php

use yii\db\Migration;

class m170914_122339_add_template_for_default_client_push_notification_table extends Migration
{
    const TABLE_PUSH = '{{%default_client_push_notifications}}';

    public $arr = [
        2 => [
            'ru' => 'К вам подъезжает #BRAND# #MODEL# #COLOR# #NUMBER# через #TIME# мин.',
            'en' => '#BRAND# #MODEL# #COLOR# #NUMBER# will get to you in #TIME# min.',
        ],
        3 => [
            'ru' => 'Вас ожидает #BRAND# #MODEL# #COLOR# #NUMBER#',
            'en' => 'Is waiting for you #BRAND# #MODEL# #COLOR# #NUMBER#',
        ],
    ];

    public function up()
    {
        foreach ($this->arr as $key => $val) {

            $this->update(self::TABLE_PUSH, [
                'text' => $val['en'],
            ], ['push_id' => $key]);

        }
    }

    public function down()
    {

        foreach ($this->arr as $key => $val) {

            $this->update(self::TABLE_PUSH, [
                'text' => $val['ru'],
            ], ['push_id' => $key]);

        }
    }
}
