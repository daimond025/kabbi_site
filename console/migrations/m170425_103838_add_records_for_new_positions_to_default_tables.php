<?php

use yii\db\Migration;

class m170425_103838_add_records_for_new_positions_to_default_tables extends Migration
{
    const SQL_DEFAULT_SMS_TEMPLATE = <<<SQL
INSERT `tbl_default_sms_template` (`type`, `text`, `description`, `position_id`)
SELECT `d`.`type`, `d`.`text`, `d`.`description`, `p`.`position_id`
FROM `tbl_position` AS `p`
CROSS JOIN `tbl_default_sms_template` AS `d`
WHERE `p`.`position_id` NOT IN (
SELECT DISTINCT `position_id`
FROM `tbl_default_sms_template`)
SQL;

    const SQL_DEFAULT_CLIENT_PUSH_NOTIFICATIONS = <<<SQL
INSERT `tbl_default_client_push_notifications` (`type`, `text`, `description`, `position_id`)
SELECT `d`.`type`, `d`.`text`, `d`.`description`, `p`.`position_id`
FROM `tbl_position` AS `p`
CROSS JOIN `tbl_default_client_push_notifications` AS `d`
WHERE `p`.`position_id` NOT IN (
SELECT DISTINCT `position_id`
FROM `tbl_default_client_push_notifications`)
SQL;

    const SQL_DEFAULT_CLIENT_STATUS_EVENT = <<<SQL
INSERT `tbl_default_client_status_event` (`status_id`, `group`, `notice`, `position_id`)
SELECT `d`.`status_id`, `d`.`group`, `d`.`notice`, `p`.`position_id`
FROM `tbl_position` AS `p`
CROSS JOIN `tbl_default_client_status_event` AS `d`
WHERE `p`.`position_id` NOT IN (
SELECT DISTINCT `position_id`
FROM `tbl_default_client_status_event`)
SQL;

    public function up()
    {
        $db = \Yii::$app->getDb();
        $db->createCommand(self::SQL_DEFAULT_SMS_TEMPLATE)->execute();
        $db->createCommand(self::SQL_DEFAULT_CLIENT_PUSH_NOTIFICATIONS)->execute();
        $db->createCommand(self::SQL_DEFAULT_CLIENT_STATUS_EVENT)->execute();
    }

    public function down()
    {
    }

}
