<?php

use yii\db\Migration;

class m160831_054724_add_column_in_client_has_company extends Migration
{

    const TABLE_NAME = '{{%client_has_company}}';

    public function up()
    {

        $this->addColumn(self::TABLE_NAME, 'role', "ENUM('juridical','physical') NOT NULL DEFAULT 'physical'");
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'role');
    }
}
