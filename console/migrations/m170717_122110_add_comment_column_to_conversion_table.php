<?php

use yii\db\Migration;

/**
 * Handles adding comment to table `conversion`.
 */
class m170717_122110_add_comment_column_to_conversion_table extends Migration
{
    const TABLE_NAME = '{{%conversion}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'comment', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'comment');
    }
}
