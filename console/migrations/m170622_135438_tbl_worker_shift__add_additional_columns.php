<?php

use yii\db\Migration;

class m170622_135438_tbl_worker_shift__add_additional_columns extends Migration
{
    const TABLE_WORKER_SHIFT = '{{%worker_shift}}';
    const TABLE_WORKER_GROUP = '{{%worker_group}}';

    const COLUMN_GROUP_ID = 'group_id';
    const COLUMN_GROUP_PRIORITY = 'group_priority';
    const COLUMN_POSITION_PRIORITY = 'position_priority';

    const FOREIGN_KEY_GROUP_ID = 'fk_worker_shift__group_id';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_GROUP_ID, $this->integer());
        $this->addColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_GROUP_PRIORITY, $this->integer());
        $this->addColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_POSITION_PRIORITY, $this->integer());

        $this->addForeignKey(self::FOREIGN_KEY_GROUP_ID, self::TABLE_WORKER_SHIFT, self::COLUMN_GROUP_ID,
            self::TABLE_WORKER_GROUP, self::COLUMN_GROUP_ID, 'restrict', 'restrict');
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FOREIGN_KEY_GROUP_ID, self::TABLE_WORKER_SHIFT);

        $this->dropColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_GROUP_ID);
        $this->dropColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_GROUP_PRIORITY);
        $this->dropColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_POSITION_PRIORITY);
    }

}
