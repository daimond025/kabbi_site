<?php

use yii\db\Schema;
use yii\db\Migration;

class m151215_070930_tbl_sms_log extends Migration {
    const TABLE_NAME = '{{%sms_log}}';
    public function up() {
        $this->createTable(self::TABLE_NAME, [
            'id'          => $this->primaryKey(),
            'text'        => $this->string(255)->notNull(),
            'status'      => $this->boolean()->notNull(),
            'login'       => $this->string(48),
            'phone'       => $this->string(48)->notNull(),
            'create_time' => $this->dateTime()->notNull(),
            'signature'   => $this->string(48),
            'server_id'   => $this->integer(3)->notNull(),
        ]);
    }

    public function down() {
        $this->dropTable(self::TABLE_NAME);
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
