<?php

use yii\db\Migration;

class m160419_113057_fix_in_tbl_taxi_tariff_has_city extends Migration
{
    
    const TABLE_NAME = '{{%taxi_tariff_has_city}}';
    
    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'first', $this->integer(1)->defaultValue(0));
        $this->db->createCommand('
UPDATE `tbl_taxi_tariff_has_city` t
SET `first` = 1
WHERE EXISTS( 
    SELECT 1
    FROM 
        (
            SELECT `tariff_id`, `city_id`
            FROM `tbl_taxi_tariff_has_city` t2
            GROUP BY `tariff_id`
        ) t3
    WHERE t . `tariff_id` = t3 . `tariff_id`
    AND t . `city_id` = t3 . `city_id`
)')
                ->execute();
        $this->delete(self::TABLE_NAME,'first = 0');
        $this->dropColumn(self::TABLE_NAME, 'first');
    }

    public function down()
    {
        echo "Нельзя откатить.\n";

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
