<?php

use yii\db\Migration;

class m160616_061130_create_table__discount_for_period extends Migration
{
    const TABLE_DISCOUNT_FOR_PERIOD = '{{%discount_for_period}}';
    const TABLE_DISCOUNT_FOR_PERIOD_HISTORY = '{{%discount_for_period_history}}';

    private function fillDiscounts()
    {
        $this->batchInsert(self::TABLE_DISCOUNT_FOR_PERIOD,
            ['id', 'period', 'percent'],
            [
                [1, 3, 5],
                [2, 6, 10],
                [3, 12, 15],
            ]);

        $activatedAt = time();
        $this->batchInsert(self::TABLE_DISCOUNT_FOR_PERIOD_HISTORY,
            ['id', 'period', 'percent', 'activated_at'],
            [
                [1, 3, 5, $activatedAt],
                [2, 6, 10, $activatedAt],
                [3, 12, 15, $activatedAt],
            ]);
    }

    public function up()
    {
        $this->createTable(self::TABLE_DISCOUNT_FOR_PERIOD, [
            'id'         => $this->primaryKey(),
            'period'     => $this->integer(3)->notNull()->unique(),
            'percent'    => $this->integer(5)->notNull(),
            'created_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createTable(self::TABLE_DISCOUNT_FOR_PERIOD_HISTORY, [
            'id'             => $this->primaryKey(),
            'period'         => $this->integer(3)->notNull(),
            'percent'        => $this->integer(5)->notNull(),
            'activated_at'   => $this->integer()->notNull(),
            'deactivated_at' => $this->integer(),
            'created_by'     => $this->integer(),
            'created_at'     => $this->integer(),
            'updated_by'     => $this->integer(),
            'updated_at'     => $this->integer(),
        ]);

        $this->fillDiscounts();
    }

    public function down()
    {
        $this->dropTable(self::TABLE_DISCOUNT_FOR_PERIOD);
        $this->dropTable(self::TABLE_DISCOUNT_FOR_PERIOD_HISTORY);
    }
}
