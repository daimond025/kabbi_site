<?php

use yii\db\Migration;

class m200407_122922_order_device_list_add extends Migration
{
    const TABLE_NAME = '{{%order}}';
    public function up()
    {
        $this->alterColumn(self::TABLE_NAME,'device', "ENUM('DISPATCHER','IOS','WORKER','WINFON','WEB','ANDROID','CABINET','HOSPITAL') "
            ."CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Устройство с которого сделан заказ'");
    }

    public function down()
    {
        $this->alterColumn(self::TABLE_NAME, 'device', "ENUM('DISPATCHER','IOS','WORKER','WINFON','WEB','ANDROID','CABINET' "
            . "CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Устройство с которого сделан заказ'");
    }
}
