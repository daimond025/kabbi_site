<?php

use yii\db\Migration;

class m180205_141008_add_phone_column_in_tenant_company_table extends Migration
{
    const TABLE_NAME = '{{%tenant_company}}';
    const COLUMN_NAME = 'phone';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_NAME, $this->string(20)->after('name'));
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);
    }

}