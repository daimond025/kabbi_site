<?php

use yii\db\Migration;

class m180410_063556_run_migrate_gateways extends Migration
{
    public function safeUp()
    {
        if (Yii::$app->runAction('payment/zot/run')) {
            return true;
        } else {
            return false;
        }
    }

    public function safeDown()
    {
        if (Yii::$app->runAction('payment/zot/rollback')) {
            return true;
        } else {
            return false;
        }
    }
}
