<?php

use yii\db\Schema;
use yii\db\Migration;

class m150825_100228_tbl_client extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%client}}', 'email', 'varchar(45)');
    }

    public function down()
    {
        echo "m150825_100228_tbl_client cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
