<?php

use yii\db\Migration;

class m160905_073126_add_rights_for_user_position extends Migration
{
    const POSITION_MANAGER_ID = 12;
    const TABLE_NAME = '{{%user_default_right}}';

    public function up()
    {
        $data = (new \yii\db\Query())
            ->from(self::TABLE_NAME)
            ->select(['permission', 'rights'])
            ->where(['position_id' => 6])
            ->all();

        $rows = [];

        foreach ($data as $item) {
            $item['position_id'] = self::POSITION_MANAGER_ID;
            $rows[] = $item;
        }

        $this->batchInsert(self::TABLE_NAME, ['permission', 'rights', 'position_id'], $rows);
    }

    public function down()
    {
        echo "m160905_073126_add_rights_for_user_position cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
