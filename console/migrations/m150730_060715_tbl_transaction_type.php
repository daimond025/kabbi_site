<?php

use yii\db\Schema;
use yii\db\Migration;

class m150730_060715_tbl_transaction_type extends Migration
{
    public function up()
    {
        $this->insert('{{%transaction_type}}', ['name' => 'GOOTAX_PAYMENT']);
    }

    public function down()
    {
        echo "m150730_060715_tbl_transaction_type cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
