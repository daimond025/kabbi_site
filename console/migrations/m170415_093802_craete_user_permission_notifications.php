<?php

use yii\db\Migration;
use yii\rbac\DbManager;

class m170415_093802_craete_user_permission_notifications extends Migration
{
    /** @var DbManager */
    public $authManager;

    const NAME = 'notifications';

    const TABLE_DEFAULT_RIGHT = '{{%user_default_right}}';

    public function init()
    {
        parent::init();

        $this->authManager = \yii\di\Instance::ensure([
            'class'           => DbManager::className(),
            'itemTable'       => '{{%auth_item}}',
            'itemChildTable'  => '{{%auth_item_child}}',
            'assignmentTable' => '{{%auth_assignment}}',
            'ruleTable'       => '{{%auth_rule}}',
        ]);
    }

    public function up()
    {
        $this->createComplexPermission(self::NAME, 'PUSH notifications');

        $rules = [
            1 => 'write',
            2 => 'write',
            3 => 'write',
            4 => 'write',
            5 => 'write',
            7 => 'write',
        ];
        $this->addPermissionByUsers(self::NAME, $rules);

        $this->createDefaultRights(self::NAME, $rules);


    }

    public function down()
    {
        $this->deleteComplexPermission(self::NAME);

        $this->delete(self::TABLE_DEFAULT_RIGHT, ['permission' => 'notifications']);
    }


    protected function createComplexPermission($name, $description = '', $ruleName = null, $data = null)
    {
        $permission     = $this->createPermission($name, $description, $ruleName, $data);
        $permissionRead = $this->createReadPermission($name, $description, $ruleName, $data);

        return $this->authManager->addChild($permission, $permissionRead);
    }

    protected function createReadPermission($name, $description = '', $ruleName = null, $data = null)
    {
        $name        = 'read_' . $name;
        $description = empty($description) ? '' : $description . ' (read)';

        return $this->createPermission($name, $description, $ruleName, $data);
    }

    protected function createPermission($name, $description = '', $ruleName = null, $data = null)
    {
        $permission              = $this->authManager->createPermission($name);
        $permission->description = $description;
        $permission->ruleName    = $ruleName;
        $permission->data        = $data;
        $this->authManager->add($permission);

        return $permission;
    }


    protected function deletePermission($name)
    {
        $permission = $this->authManager->createPermission($name);

        return $this->authManager->remove($permission);
    }

    protected function deleteReadPermission($name)
    {
        $name = 'read_' . $name;

        return $this->deletePermission($name);
    }

    protected function deleteComplexPermission($name)
    {
        $this->deletePermission($name);
        $this->deleteReadPermission($name);
    }


    protected function createDefaultRights($permission, $rules)
    {
        $positions    = (new \yii\db\Query())->from('{{%user_position}}')
            ->select('position_id')
            ->indexBy('position_id')
            ->all();
        $positionsArr = array_keys($positions);
        $rulesKeys    = array_keys($rules);

        foreach ($positionsArr as $position) {
            $rights = in_array($position, $rulesKeys) ? $rules[$position] : 'off';

            $this->insert(self::TABLE_DEFAULT_RIGHT, [
                'position_id' => $position,
                'permission'  => $permission,
                'rights'      => $rights,
            ]);
        }
    }


    protected function addPermissionByUsers($name, $rules)
    {
        $rulesKeys = array_keys($rules);

        $users     = (new \yii\db\Query())->from('{{%user}}')
            ->select(['user_id', 'position_id'])
            ->where([
                'position_id' => $rulesKeys,
            ])
            ->all();
        $usersKeys = array_keys($users);

        $permission     = $this->authManager->getPermission($name);
        $permissionRead = $this->authManager->getPermission('read_' . $name);

        foreach ($users as $user) {
            $positionId = $user['position_id'];

            switch ($rules[$positionId]) {
                case 'write':
                    $this->authManager->assign($permission, $user['user_id']);
                    break;
                case 'read':
                    $this->authManager->assign($permissionRead, $user['user_id']);
                    break;
            }
        }
    }
}
