<?php

use yii\db\Schema;
use yii\db\Migration;

class m151006_125248_create_vote_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%vote}}', [
            'vote_id' => $this->primaryKey(),
            'offer_info_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'vote' => "ENUM('plus', 'minus') NOT NULL",
        ], $tableOptions);
        $this->addForeignKey('fk_vote__offer_info_id', '{{%vote}}', 'offer_info_id',
            '{{%offer_info}}', 'offer_info_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_vote__user_id', '{{%vote}}', 'user_id',
            '{{%community_user}}', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('idx_vote__offer_info_id__user_id',
                '{{%vote}}', ['offer_info_id', 'user_id'], true);
    }

    public function down()
    {
        $this->dropTable('{{%vote}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
