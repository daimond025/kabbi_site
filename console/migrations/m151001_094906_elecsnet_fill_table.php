<?php

use yii\db\Schema;
use yii\db\Migration;

class m151001_094906_elecsnet_fill_table extends Migration
{
    const TABLE_NAME = '{{%elecsnet_error_code}}';
    
    public function up()
    {
       $this->execute("INSERT INTO " . self::TABLE_NAME ."(code, description) VALUES ('00', 'Платёж принят успешно ')");
       $this->execute("INSERT INTO " . self::TABLE_NAME ."(code, description) VALUES ('01 ', 'Платёж с таким auth_code уже зарегистрирован ')");
       $this->execute("INSERT INTO " . self::TABLE_NAME ."(code, description) VALUES ('02', 'Учётная дата отличается от текущей более, чем на 24 часа. Платёж не может быть зарегистрирован. ')");
       $this->execute("INSERT INTO " . self::TABLE_NAME ."(code, description) VALUES ('03', 'Неверная подпись ')");
       $this->execute("INSERT INTO " . self::TABLE_NAME ."(code, description) VALUES ('26', 'Сервис временно недоступен по административным причинам ')");
       $this->execute("INSERT INTO " . self::TABLE_NAME ."(code, description) VALUES ('42', 'Неверный номер карты (абонент не найден), в случае идентификации по номеру карты ')");
       $this->execute("INSERT INTO " . self::TABLE_NAME ."(code, description) VALUES ('45', 'Сервис временно недоступен по техническим причинам ')");
       $this->execute("INSERT INTO " . self::TABLE_NAME ."(code, description) VALUES ('46', 'Неверный номер телефона (абонент не найден), в случае идентификации по номеру телефона ')");
       $this->execute("INSERT INTO " . self::TABLE_NAME ."(code, description) VALUES ('48', 'Неверный номер договора (абонент не найден), в случае идентификации по номеру договора ')");
       $this->execute("INSERT INTO " . self::TABLE_NAME ."(code, description) VALUES ('48', 'Системная ошибка (неверные форматы данных и пр.) ')");
       $this->execute("INSERT INTO " . self::TABLE_NAME ."(code, description) VALUES ('60', 'Произвольная ошибка. При получении этого кода на терминале отображается сообщение, содержащееся в первом подполе ansid. По сценарию происходит возврат к вводу реквизитов платежа (предыдущая экранная форма перед запросом на хост) ')");
       $this->execute("INSERT INTO " . self::TABLE_NAME ."(code, description) VALUES ('62', 'Произвольная ошибка. При получении этого кода на терминале отображается сообщение, содержащееся в первом подполе ansid. Далее происходит выход из платёжного сценария (возврат в главное меню). ')");
       
    }

    public function down()
    {
        $this->truncateTable(self::TABLE_NAME);
        echo self::TABLE_NAME.' was truncated';
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
