<?php

use yii\db\Migration;
use yii\db\Schema;

class m160616_135635_add_table_admin_docpack extends Migration
{
    const TABLE_NAME = '{{%admin_docpack}}';
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'email' => $this->string(255),
            'phone' => $this->string(15),
            'is_site' => $this->boolean()->defaultValue(false),
            'create_time' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
