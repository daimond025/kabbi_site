<?php

use yii\db\Schema;
use yii\db\Migration;

class m150728_072426_tbl_transaction extends Migration
{
    public function up()
    {
        $this->execute('SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0');
        $this->execute('SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0');   
        $this->execute("SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES'");
        
        $this->addColumn('{{%transaction}}', 'city_id', Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL');
        $this->createIndex('FK_transaction_city_id_idx', '{{%transaction}}', 'city_id');
        $this->addForeignKey('FK_transaction_city_id', '{{%transaction}}', 'city_id', '{{%city}}', 'city_id');
        
        $this->execute('SET SQL_MODE=@OLD_SQL_MODE');
        $this->execute('SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS');
        $this->execute('SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS');
    }

    public function down()
    {
        $this->dropColumn('{{%transaction}}', 'city_id');
        $this->dropForeignKey('FK_transaction_city_id', '{{%transaction}}');
        $this->dropIndex('FK_transaction_city_id_idx', '{{%transaction}}');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
