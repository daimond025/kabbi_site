<?php

use yii\rbac\DbManager;
use yii\db\Migration;
use yii\di\Instance;

class m170123_112729_admin_rbac_init extends Migration
{
    /** @var DbManager */
    public $authManager;

    /**
     * Initializes the migration.
     * This method will set [[authManager]] to be the 'authManager' application component, if it is `null`.
     */
    public function init()
    {
        parent::init();

        $this->authManager = Instance::ensure([
            'class'           => DbManager::className(),
            'itemTable'       => '{{%admin_auth_item}}',
            'itemChildTable'  => '{{%admin_auth_item_child}}',
            'assignmentTable' => '{{%admin_auth_assignment}}',
            'ruleTable'       => '{{%admin_auth_rule}}',
        ]);
    }

    public function up()
    {
        $this->addComplexPermission('users');
        $this->createPermission('landingPageEdit');
        $this->createPermission('landingDownLoads');
        $this->addComplexPermission('landingCommunity');
        $this->addComplexPermission('tenantInfo');
        $this->addComplexPermission('tenant');
        $this->createPermission('tenantOrders');
        $this->createPermission('logs');
        $this->createPermission('statistics');
        $this->addComplexPermission('support');
        $this->createPermission('serviceCommands');
        $this->addComplexPermission('translations');
        $this->addComplexPermission('tenantRights');
        $this->addComplexPermission('geo');
        $this->addComplexPermission('employee');
        $this->addComplexPermission('transport');
        $this->addComplexPermission('sms');
        $this->addComplexPermission('messageTemplates');
        $this->addComplexPermission('holidays');
        $this->addComplexPermission('orderStatus');
        $this->addComplexPermission('defaultSettings');
        $this->addComplexPermission('tenantSettings');
        $this->addComplexPermission('phoneLines');
        $this->addComplexPermission('geoElastic');
    }

    public function down()
    {
        echo "m170123_112729_admin_rbac_init cannot be reverted.\n";

        return false;
    }

    /**
     * Add permission with read permission
     * @param string $name
     * @return bool
     */
    private function addComplexPermission($name)
    {
        $permission = $this->createPermission($name);
        $permissionRead = $this->createPermission($name . 'Read');

        return $this->authManager->addChild($permission, $permissionRead);
    }

    /**
     * Creates new permission.
     *
     * @param  string $name The name of the permission
     * @param  string $description The description of the permission
     * @param  string|null $ruleName The rule associated with the permission
     * @param  mixed|null $data The additional data associated with the permission
     * @return \yii\rbac\Permission
     */
    protected function createPermission($name, $description = '', $ruleName = null, $data = null)
    {
        $permission = $this->authManager->createPermission($name);

        $permission->description = $description;
        $permission->ruleName = $ruleName;
        $permission->data = $data;

        $this->authManager->add($permission);

        return $permission;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
