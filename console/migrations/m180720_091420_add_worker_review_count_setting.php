<?php

use yii\db\Migration;
use common\modules\tenant\models\DefaultSettings;

class m180720_091420_add_worker_review_count_setting extends Migration
{
    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const SETTING_WORKER_REVIEW_COUNT = 'WORKER_REVIEW_COUNT';

    public function safeUp()
    {
        $printCheck = new DefaultSettings([
            'name'  => self::SETTING_WORKER_REVIEW_COUNT,
            'value' => '100',
            'type'  => 'drivers',
        ]);

        return $printCheck->save();
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::SETTING_WORKER_REVIEW_COUNT,
            'type' => 'drivers',
        ]);
    }
}
