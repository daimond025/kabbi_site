<?php

use yii\db\Schema;
use yii\db\Migration;

class m151024_090240_tbl_TenantAdminTariffChangeset_fixes extends Migration
{

    const TABLE_TARIFF_CHANGESET_NAME = '{{%tenant_admin_tariff_changeset}}';

    public function up()
    {
        $this->alterColumn(self::TABLE_TARIFF_CHANGESET_NAME, 'tariif_id_new', $this->integer(5));
    }

    public function down()
    {
        echo "m151024_090240_tbl_TenantAdminTariffChangeset_fixes cannot be reverted.\n";

        return false;
    }

}
