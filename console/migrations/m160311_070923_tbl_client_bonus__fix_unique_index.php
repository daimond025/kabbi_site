<?php
use yii\db\Migration;

class m160311_070923_tbl_client_bonus__fix_unique_index extends Migration
{

    const TABLE_NAME     = '{{%client_bonus}}';
    const INDEX_NAME_OLD = 'idx_client_bonus__name';
    const INDEX_NAME_NEW = 'ui_client_bonus';

    public function up()
    {
        $newColumns = ['tenant_id', 'city_id', 'name'];
        $this->dropIndex(self::INDEX_NAME_OLD, self::TABLE_NAME);
        $this->createIndex(self::INDEX_NAME_NEW, self::TABLE_NAME, $newColumns,
            true);
    }

    public function down()
    {
        // mysql can't drop unique index
        // because mysql removed foreign key index for table `tbl_tenant`
        // and unique index which was added used as foreign key index
        //$this->dropIndex(self::INDEX_NAME_NEW, self::TABLE_NAME);
        return false;
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
