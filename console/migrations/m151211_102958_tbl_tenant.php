<?php

use yii\db\Schema;
use yii\db\Migration;

class m151211_102958_tbl_tenant extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%tenant}}', 'domain', $this->string(15));
    }

    public function down()
    {
        $this->alterColumn('{{%tenant}}', 'domain', $this->string(10));
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
