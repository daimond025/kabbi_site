<?php

use yii\db\Migration;
use yii\rbac\DbManager;
use yii\di\Instance;

class m180201_135330_add_permission_for_adminki__extrinsic_dispatcher extends Migration
{

    const PERMISSION_NAME = 'extrinsicDispatcher';

    /** @var DbManager */
    public $authManager;

    /**
     * Initializes the migration.
     * This method will set [[authManager]] to be the 'authManager' application component, if it is `null`.
     */
    public function init()
    {
        parent::init();

        $this->authManager = Instance::ensure([
            'class'           => DbManager::className(),
            'itemTable'       => '{{%admin_auth_item}}',
            'itemChildTable'  => '{{%admin_auth_item_child}}',
            'assignmentTable' => '{{%admin_auth_assignment}}',
            'ruleTable'       => '{{%admin_auth_rule}}',
        ]);
    }

    public function up()
    {
        $this->addComplexPermission(self::PERMISSION_NAME);
    }

    public function down()
    {
        $this->deleteComplexPermission(self::PERMISSION_NAME);
    }


    protected function deleteComplexPermission($name)
    {
        $this->deletePermission($name);
        $this->deleteReadPermission($name);
    }


    protected function deletePermission($name)
    {
        $permission = $this->authManager->createPermission($name);

        return $this->authManager->remove($permission);
    }

    protected function deleteReadPermission($name)
    {
        $name = $name . 'Read';

        return $this->deletePermission($name);
    }

    /**
     * Add permission with read permission
     *
     * @param string $name
     *
     * @return bool
     */
    private function addComplexPermission($name)
    {
        $permission     = $this->createPermission($name);
        $permissionRead = $this->createPermission($name . 'Read');

        return $this->authManager->addChild($permission, $permissionRead);
    }

    /**
     * Creates new permission.
     *
     * @param  string      $name The name of the permission
     * @param  string      $description The description of the permission
     * @param  string|null $ruleName The rule associated with the permission
     * @param  mixed|null  $data The additional data associated with the permission
     *
     * @return \yii\rbac\Permission
     */
    protected function createPermission($name, $description = '', $ruleName = null, $data = null)
    {
        $permission = $this->authManager->createPermission($name);

        $permission->description = $description;
        $permission->ruleName    = $ruleName;
        $permission->data        = $data;

        $this->authManager->add($permission);

        return $permission;
    }


}
