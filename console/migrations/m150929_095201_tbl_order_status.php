<?php

use yii\db\Schema;
use yii\db\Migration;

class m150929_095201_tbl_order_status extends Migration
{
    public function up()
    {
        $this->insert('{{%order_status}}', ['name' => 'There are not free cars', 'status_group' => 'rejected']);
    }

    public function down()
    {
        $this->delete('{{%order_status}}', ['name' => 'There are not free cars', 'status_group' => 'rejected']);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
