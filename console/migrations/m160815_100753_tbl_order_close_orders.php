<?php

use yii\db\Migration;

class m160815_100753_tbl_order_close_orders extends Migration
{
    public function up()
    {
        $this->update('{{%order}}', ['status_id' => 51],
            ['not in', 'status_id', [6, 7, 10, 16, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 107]]);
    }

    public function down()
    {
        echo "m160815_100753_tbl_order_close_orders cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
