<?php

use yii\db\Migration;

class m170413_045713_add_second_min_price_in__taxi_tariff extends Migration
{
    const TABLE_NAME = '{{%option_tariff}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'second_min_price_day',
            $this->float()->unsigned()->defaultValue(0)->after('min_price_day'));
        $this->addColumn(self::TABLE_NAME, 'second_min_price_night',
            $this->float()->unsigned()->defaultValue(0)->after('min_price_night'));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'second_min_price_day');
        $this->dropColumn(self::TABLE_NAME, 'second_min_price_night');
    }
}
