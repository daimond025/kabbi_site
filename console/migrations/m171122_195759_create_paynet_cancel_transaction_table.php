<?php

use yii\db\Migration;

/**
 * Handles the creation of table `paynet_cancel_transaction`.
 */
class m171122_195759_create_paynet_cancel_transaction_table extends Migration
{
    const TABLE_NAME = '{{%paynet_cancel_transaction}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'             => $this->primaryKey(),
            'tenant_id'      => $this->integer(11)->notNull(),
            'worker_id'      => $this->integer(11)->notNull(),
            'transaction_id' => $this->string(255)->notNull(),
            'status'         => $this->integer(2)->notNull()->defaultValue(0),
            'create_time'    => $this->integer(11)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
