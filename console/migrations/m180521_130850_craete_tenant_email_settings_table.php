<?php

use yii\db\Migration;

class m180521_130850_craete_tenant_email_settings_table extends Migration
{
    const TABLE_NAME = '{{%tenant_email_settings}}';
    const FOREIGN_KEY = 'fk_tenant_email_settings__tenant_id';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'setting_id'      => $this->primaryKey(10)->unsigned(),
            'tenant_id'       => $this->integer(10)->unsigned()->notNull(),
            'city_id'         => $this->integer(10)->unsigned()->notNull(),
            'active'          => $this->integer(1)->unsigned()->notNull(),
            'provider_server' => $this->string(255)->notNull(),
            'provider_port'   => $this->integer(5)->unsigned()->notNull(),
            'sender_name'     => $this->string(100)->notNull(),
            'sender_email'    => $this->string(255)->notNull(),
            'sender_password' => $this->string(100)->notNull(),
            'template'        => $this->text()->notNull(),
        ]);

        $this->addForeignKey('fk_tenant_email_settings__tenant_id', self::TABLE_NAME, 'tenant_id', '{{%tenant}}',
            'tenant_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_tenant_email_settings__city_id', self::TABLE_NAME, 'city_id', '{{%city}}', 'city_id',
            'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_tenant_email_settings__city_id', self::TABLE_NAME);
        $this->dropForeignKey('fk_tenant_email_settings__tenant_id', self::TABLE_NAME);
        $this->dropTable(self::TABLE_NAME);
    }
}
