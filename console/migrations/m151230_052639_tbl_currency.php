<?php

use yii\db\Schema;
use yii\db\Migration;

class m151230_052639_tbl_currency extends Migration
{

    const TABLE_NAME = '{{%currency}}';

    public function up()
    {
        $this->insert(self::TABLE_NAME, ['name' => 'Moldovan Leu', 'code' => 'MDL', 'symbol' => '‎MDL', 'type_id' => 1]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME, 'code = "MDL"');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
