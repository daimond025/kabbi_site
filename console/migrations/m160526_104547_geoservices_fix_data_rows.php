<?php

use yii\db\Migration;

class m160526_104547_geoservices_fix_data_rows extends Migration
{
    const AUTO_TABLE_NAME = '{{%auto_geo_service_type}}';
    const ROUTE_TABLE_NAME = '{{%routing_service_type}}';

    public function up()
    {
        $this->delete(self::AUTO_TABLE_NAME, ['id' => 1]);
        $this->delete(self::AUTO_TABLE_NAME, ['id' => 2]);
        $this->delete(self::AUTO_TABLE_NAME, ['id' => 3]);
        $this->delete(self::AUTO_TABLE_NAME, ['id' => 4]);
        $this->delete(self::AUTO_TABLE_NAME, ['id' => 5]);
        $this->delete(self::AUTO_TABLE_NAME, ['id' => 6]);

        $this->delete(self::ROUTE_TABLE_NAME, ['id' => 1]);
        $this->delete(self::ROUTE_TABLE_NAME, ['id' => 2]);
        $this->delete(self::ROUTE_TABLE_NAME, ['id' => 3]);
        $this->delete(self::ROUTE_TABLE_NAME, ['id' => 4]);
        $this->delete(self::ROUTE_TABLE_NAME, ['id' => 5]);

        //autogeodata
        $this->insert(self::AUTO_TABLE_NAME, [
            'id'            => 1,
            'service_label' => 'Yandex (free version)',
            'active'        => 1,
            'has_app_code'  => 0,
            'has_app_id'    => 0
        ]);
        $this->insert(self::AUTO_TABLE_NAME, [
            'id'            => 2,
            'service_label' => 'Here.com',
            'active'        => 1,
            'has_app_code'  => 1,
            'has_app_id'    => 1
        ]);
        $this->insert(self::AUTO_TABLE_NAME, [
            'id'            => 3,
            'service_label' => 'Google',
            'active'        => 0,
            'has_app_code'  => 1,
            'has_app_id'    => 0
        ]);
        $this->insert(self::AUTO_TABLE_NAME, [
            'id'            => 4,
            'service_label' => '2GiS',
            'active'        => 0,
            'has_app_code'  => 1,
            'has_app_id'    => 0
        ]);
        $this->insert(self::AUTO_TABLE_NAME, [
            'id'            => 5,
            'service_label' => 'Gootax',
            'active'        => 1,
            'has_app_code'  => 0,
            'has_app_id'    => 0
        ]);
        $this->insert(self::AUTO_TABLE_NAME, [
            'id'            => 6,
            'service_label' => 'Yandex',
            'active'        => 0,
            'has_app_code'  => 1,
            'has_app_id'    => 0
        ]);

        //routedata

        $this->insert(self::ROUTE_TABLE_NAME, [
            'id'            => 1,
            'service_label' => 'OSM',
            'active'        => 1,
            'has_app_code'  => 0,
            'has_app_id'    => 0
        ]);
        $this->insert(self::ROUTE_TABLE_NAME, [
            'id'            => 2,
            'service_label' => 'Google (free version)',
            'active'        => 1,
            'has_app_code'  => 0,
            'has_app_id'    => 0
        ]);
        $this->insert(self::ROUTE_TABLE_NAME, [
            'id'            => 3,
            'service_label' => 'Google',
            'active'        => 0,
            'has_app_code'  => 1,
            'has_app_id'    => 0
        ]);
        $this->insert(self::ROUTE_TABLE_NAME, [
            'id'            => 4,
            'service_label' => 'Here.com',
            'active'        => 0,
            'has_app_code'  => 1,
            'has_app_id'    => 1
        ]);
        $this->insert(self::ROUTE_TABLE_NAME, [
            'id'            => 5,
            'service_label' => '2GIS',
            'active'        => 0,
            'has_app_code'  => 1,
            'has_app_id'    => 0
        ]);
    }

    public function down()
    {
        return false;
    }
}