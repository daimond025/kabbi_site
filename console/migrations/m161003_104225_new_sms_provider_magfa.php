<?php

use yii\db\Migration;

class m161003_104225_new_sms_provider_magfa extends Migration
{
    const TABLE_NAME = '{{%sms_server}}';

    const COLUMN_SERVER_ID = 15;

    public function up()
    {
        $this->insert(self::TABLE_NAME,[
            'server_id' => self::COLUMN_SERVER_ID,
            'name' =>  'Magfa.com',
            'host' => 'https://messaging.magfa.com',
            'active' => '1',
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME,['server_id' => self::COLUMN_SERVER_ID]);
    }
}
