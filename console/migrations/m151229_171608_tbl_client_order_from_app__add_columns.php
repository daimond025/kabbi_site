<?php

use yii\db\Schema;
use yii\db\Migration;

class m151229_171608_tbl_client_order_from_app__add_columns extends Migration
{
    const TABLE_NAME          = '{{%client_order_from_app}}';
    const ORDER_TABLE_NAME    = '{{%order}}';
    const ORDER_ID_COLUMN     = 'order_id';
    const ORDER_ID_KEY        = 'fk_client_order_from_app__order_id';
    const DEVICE_COLUMN       = 'device';
    const DEVICE_TOKEN_COLUMN = 'device_token';
    const DEVICE_TOKEN_INDEX  = 'idx_client_order_from_app__device_token';
    const ORDER_TIME_COLUMN   = 'order_time';
    const COUNT_COLUMN        = 'count';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, self::ORDER_ID_COLUMN, 'INT(10) UNSIGNED');
        $this->addColumn(self::TABLE_NAME, self::DEVICE_COLUMN, Schema::TYPE_STRING);
        $this->addColumn(self::TABLE_NAME, self::DEVICE_TOKEN_COLUMN, Schema::TYPE_STRING);
        $this->addColumn(self::TABLE_NAME, self::ORDER_TIME_COLUMN, Schema::TYPE_INTEGER);
        $this->dropColumn(self::TABLE_NAME, self::COUNT_COLUMN);
        $this->execute('SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0');
        $this->execute('SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0');
        $this->addForeignKey(self::ORDER_ID_KEY, self::TABLE_NAME, self::ORDER_ID_COLUMN,
                self::ORDER_TABLE_NAME, self::ORDER_ID_COLUMN, 'CASCADE', 'CASCADE');
        $this->createIndex(self::DEVICE_TOKEN_INDEX, self::TABLE_NAME, self::DEVICE_TOKEN_COLUMN);
        $this->execute('SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS');
        $this->execute('SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS');
    }

    public function down()
    {
        $this->dropForeignKey(self::ORDER_ID_KEY, self::TABLE_NAME);
        $this->dropIndex(self::DEVICE_TOKEN_INDEX, self::TABLE_NAME);

        $this->dropColumn(self::TABLE_NAME, self::ORDER_ID_COLUMN);
        $this->dropColumn(self::TABLE_NAME, self::DEVICE_COLUMN);
        $this->dropColumn(self::TABLE_NAME, self::DEVICE_TOKEN_COLUMN);
        $this->dropColumn(self::TABLE_NAME, self::ORDER_TIME_COLUMN);
        $this->addColumn(self::TABLE_NAME, self::COUNT_COLUMN, Schema::TYPE_INTEGER);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
