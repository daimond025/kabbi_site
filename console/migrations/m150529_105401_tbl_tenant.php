<?php

use yii\db\Schema;
use yii\db\Migration;

class m150529_105401_tbl_tenant extends Migration
{
    public function up()
    {
        $this->dropColumn('{{%tenant}}', 'contact_phone_confirm');
    }

    public function down()
    {
        $this->addColumn('{{%tenant}}', 'contact_phone_confirm', Schema::TYPE_BOOLEAN);
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
