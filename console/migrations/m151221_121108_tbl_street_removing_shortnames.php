<?php

use yii\db\Schema;
use yii\db\Migration;

class m151221_121108_tbl_street_removing_shortnames extends Migration
{
    const TABLE_NAME = '{{%street}}';
    
    public function up()
    {
        $this->execute("UPDATE" . self::TABLE_NAME ." SET fulladdress = REPLACE(fulladdress, ' ст-ца,', ',')");
        $this->execute("UPDATE" . self::TABLE_NAME ." SET fulladdress = REPLACE(fulladdress, ' снт,', ',')");
    }

    public function down()
    {
        echo "m151221_121108_tbl_street_removing_shortnames cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
