<?php

use yii\db\Schema;
use yii\db\Migration;

class m160210_063628_tbl_car_option_add_smoke extends Migration
{
    
    
    const TABLE_NAME = '{{%car_option}}';
    
    
    public function up()
    {
        $this->insert(self::TABLE_NAME, [
            'name' => 'Smoke',
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME, [
            'name' => 'Smoke',
        ]);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
