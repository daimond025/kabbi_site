<?php

use yii\db\Migration;

class m171221_064520_change_limit_value_field_in_tenant_settings_table extends Migration
{
    const TABLE_NAME = '{{%tenant_setting}}';
    const COLUMN_NAME = 'value';

    public function safeUp()
    {
        $this->alterColumn(self::TABLE_NAME,self::COLUMN_NAME,'LONGTEXT');
        return true;
    }

    public function safeDown()
    {
        $this->alterColumn(self::TABLE_NAME,self::COLUMN_NAME,'TEXT');
        return true;
    }

}
