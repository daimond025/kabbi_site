<?php

use console\components\mysqlOnlineSchemaChange\MysqlOnlineSchemaChangeTrait;
use yii\db\Migration;

class m180824_093318_add_order_idx extends Migration
{
    use MysqlOnlineSchemaChangeTrait;

    public function up()
    {
        return $this->schemaChange('tbl_order',
            'ADD INDEX tbi_order_tenant_id_position_id_city_id_idx (tenant_id, position_id, city_id)');
    }

    public function down()
    {
        return $this->schemaChange('tbl_order', 'DROP INDEX tbi_order_tenant_id_position_id_city_id_idx');
    }

}
