<?php

use yii\db\Schema;
use yii\db\Migration;

class m160123_130047_tbl_tenant_has_car_option extends Migration
{
    public function up()
    {
        $sql = 'ALTER TABLE `tbl_tenant_has_car_option` DROP PRIMARY KEY, ADD PRIMARY KEY(`tenant_id`,`option_id`,`city_id`);';
        $this->execute($sql);
    }

    public function down()
    {
        $sql = 'ALTER TABLE `tbl_tenant_has_car_option` DROP PRIMARY KEY, ADD PRIMARY KEY(`tenant_id`,`option_id`);';
        $this->execute($sql);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
