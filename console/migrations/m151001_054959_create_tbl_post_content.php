<?php

use yii\db\Schema;
use yii\db\Migration;

class m151001_054959_create_tbl_post_content extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=MyISAM';
        }
        $this->createTable('{{%post_content}}', [
            'post_content_id' => $this->primaryKey(),
            'post_id' => $this->integer()->notNull(),
            'title' => $this->string(),
            'anons' => $this->text(),
            'content' => $this->text(),
            'author' => $this->text(),
                ], $tableOptions);
        
        $this->createIndex('idx_post_content__post_id', '{{%post_content}}', 'post_id');
        $this->execute('ALTER TABLE '
            . $this->db->quoteTableName('{{%post_content}}')
            . ' ADD FULLTEXT ('
            . implode(',', [
                $this->db->quoteColumnName('title'),
                $this->db->quoteColumnName('anons'),
                $this->db->quoteColumnName('content'),
                $this->db->quoteColumnName('author'),
                ]) . ')');
    }

    public function down()
    {
        $this->dropTable('{{%post_content}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
