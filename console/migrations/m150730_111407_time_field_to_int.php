<?php

use yii\db\Schema;
use yii\db\Migration;

class m150730_111407_time_field_to_int extends Migration
{

    public function up()
    {
        $this->alterColumn('{{%order_change_data}}', 'change_time', Schema::TYPE_INTEGER . '(10)');
        $this->alterColumn('{{%order_detail_cost}}', 'time', Schema::TYPE_INTEGER . '(10)');
        $this->alterColumn('{{%user_working_time}}', 'start_work', Schema::TYPE_INTEGER . '(10)');
        $this->alterColumn('{{%user_working_time}}', 'end_work', Schema::TYPE_INTEGER . '(10)');
    }

    public function down()
    {
        echo "m150730_111407_time_field_to_int cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
