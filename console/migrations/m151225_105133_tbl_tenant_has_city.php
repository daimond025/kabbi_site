<?php

use yii\db\Schema;
use yii\db\Migration;

class m151225_105133_tbl_tenant_has_city extends Migration
{
    const TABLE_NAME = '{{%tenant_has_city}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'sort', $this->integer() . ' DEFAULT 100');
        $this->addColumn(self::TABLE_NAME, 'block', $this->boolean() . ' DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'sort');
        $this->dropColumn(self::TABLE_NAME, 'block');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
