<?php

use yii\db\Migration;

class m160225_073531_million_log___add_column extends Migration
{
    
    const TABLE_NAME = '{{%million_log}}';
    
    public function up()
    {
        $this->addColumn(self::TABLE_NAME,'phone','VARCHAR(15)');
        $this->addColumn(self::TABLE_NAME,'callsign','INTEGER(8)');
        $this->addColumn(self::TABLE_NAME, 'sum', 'VARCHAR(255)');
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'phone');
        $this->dropColumn(self::TABLE_NAME, 'callsign');
        $this->dropColumn(self::TABLE_NAME, 'sum');
    }

}
