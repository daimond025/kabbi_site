<?php

use yii\db\Migration;

class m160504_093716_create_table_demo_tariff extends Migration
{
    const TABLE_DEMO_TARIFF = '{{%demo_tariff}}';
    const TABLE_TARIFF = '{{%tariff}}';
    const COLUMN_TARIFF_ID = 'tariff_id';
    const FOREIGN_KEY_NAME = 'fk_tbl_demo_tariff__tariff_id';

    public function up()
    {
        $this->createTable(self::TABLE_DEMO_TARIFF, [
            'id'         => $this->primaryKey(),
            'tariff_id'  => $this->integer()->notNull()->unique(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
        $this->addForeignKey(self::FOREIGN_KEY_NAME,
            self::TABLE_DEMO_TARIFF, self::COLUMN_TARIFF_ID, self::TABLE_TARIFF, 'id',
            'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey(self::FOREIGN_KEY_NAME, self::TABLE_DEMO_TARIFF);
        $this->dropTable(self::TABLE_DEMO_TARIFF);
    }
}
