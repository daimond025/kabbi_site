<?php

use yii\db\Migration;

class m160927_125218_new_gosms_cz_sms_provider extends Migration
{
    const TABLE_NAME = '{{%sms_server}}';

    const COLUMN_SERVER_ID = 13;

    public function up()
    {
        $this->insert(self::TABLE_NAME,[
            'server_id' => self::COLUMN_SERVER_ID,
            'name' =>  'GoSMS.cz',
            'host' => 'http://gosms.cz',
            'active' => '1',
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME,['server_id' => self::COLUMN_SERVER_ID]);
    }
}
