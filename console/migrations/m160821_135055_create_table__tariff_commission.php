<?php

use yii\db\Migration;
use yii\db\Query;

class m160821_135055_create_table__tariff_commission extends Migration
{
    const TABLE_TARIFF_COMMISSION = '{{%tariff_commission}}';
    const TABLE_WORKER_OPTION_TARIFF = '{{%worker_option_tariff}}';
    const FOREIGN_KEY_OPTION_ID = 'fk_tariff_commission__option_id';

    public function safeUp()
    {
        $this->createTable(self::TABLE_TARIFF_COMMISSION, [
            'id'        => $this->primaryKey(),
            'option_id' => $this->integer(10)->unsigned()->notNull(),
            'type'      => "ENUM('PERCENT','MONEY') NOT NULL DEFAULT 'PERCENT'",
            'value'     => $this->decimal(12, 2)->notNull(),
            'from'      => $this->decimal(12, 2)->notNull()->defaultValue(0),
            'to'        => $this->decimal(12, 2),
        ]);

        $this->addForeignKey(
            self::FOREIGN_KEY_OPTION_ID,
            self::TABLE_TARIFF_COMMISSION,
            'option_id',
            self::TABLE_WORKER_OPTION_TARIFF,
            'option_id',
            'CASCADE',
            'CASCADE'
        );

        $tariffOptions = (new Query())
            ->from(self::TABLE_WORKER_OPTION_TARIFF)
            ->all();

        if (is_array($tariffOptions)) {
            foreach ($tariffOptions as $option) {
                $this->insert(self::TABLE_TARIFF_COMMISSION, [
                    'option_id' => $option['option_id'],
                    'type'      => $option['commission_type'],
                    'value'     => $option['commission'],
                    'from'      => 0,
                ]);
            }
        }

        $this->addColumn(
            self::TABLE_WORKER_OPTION_TARIFF,
            'interval_commission',
            $this->integer()->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_WORKER_OPTION_TARIFF, 'interval_commission');
        $this->dropTable(self::TABLE_TARIFF_COMMISSION);
    }
}
