<?php

use yii\db\Schema;
use yii\db\Migration;

class m160127_103426_tbl_option_tariff extends Migration
{
    const TABLE_NAME = '{{%option_tariff}}';

    public function up()
    {
        $this->alterColumn(self::TABLE_NAME, 'accrual', 'ENUM("DISTANCE","TIME","FIX","MIXED","INTERVAL") NOT NULL');
        $this->alterColumn(self::TABLE_NAME, 'next_km_price_day', $this->text());
        $this->alterColumn(self::TABLE_NAME, 'next_km_price_night', $this->text());
    }

    public function down()
    {
        $this->alterColumn(self::TABLE_NAME, 'accrual', 'ENUM("DISTANCE","TIME","FIX","MIXED") NOT NULL');
        $this->alterColumn(self::TABLE_NAME, 'next_km_price_day', Schema::TYPE_FLOAT . ' DEFAULT 0');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
