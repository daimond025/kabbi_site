<?php

use yii\db\Migration;

/**
 * Handles the creation of table `default_recipient_sms_notifications`.
 */
class m180627_114301_create_default_recipient_sms_template_table extends Migration
{
    const TABLE_NAME = '{{%default_recipient_sms_template}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'template_id' => $this->primaryKey(),
            'type'        => $this->string(45)->notNull(),
            'text'        => $this->text(),
            'position_id' => $this->integer(10)->notNull(),
            'params'      => $this->text(),
            'updated_at'  => $this->integer(),
            'created_at'  => $this->integer(),
        ]);

        $this->addForeignKey('fk_default_recipient_sms_template__position_id', self::TABLE_NAME, 'position_id',
            '{{%position}}', 'position_id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
