<?php

use yii\db\Migration;

class m171221_062401_change_limit_confidential_field_in_mobile_app extends Migration
{
    const TABLE_NAME = '{{%mobile_app}}';
    const COLUMN_NAME = 'confidential';

    public function safeUp()
    {
        $this->alterColumn(self::TABLE_NAME,self::COLUMN_NAME,'LONGTEXT');
        return true;
    }

    public function safeDown()
    {
        $this->alterColumn(self::TABLE_NAME,self::COLUMN_NAME,'TEXT');
        return true;
    }

}
