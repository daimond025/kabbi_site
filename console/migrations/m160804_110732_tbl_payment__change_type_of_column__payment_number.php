<?php

use yii\db\Migration;

class m160804_110732_tbl_payment__change_type_of_column__payment_number extends Migration
{
    const TABLE_PAYMENT = '{{%payment}}';
    const COLUMN_PAYMENT_NUMBER = 'payment_number';

    public function up()
    {
        $this->alterColumn(self::TABLE_PAYMENT, self::COLUMN_PAYMENT_NUMBER, $this->string()->notNull());
    }

    public function down()
    {
    }

}
