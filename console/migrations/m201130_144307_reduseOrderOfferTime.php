<?php

use common\modules\tenant\models\DefaultSettings;
use yii\db\Migration;

class m201130_144307_reduseOrderOfferTime extends Migration
{
    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const TABLE_TENANT_SETTING = '{{%tenant_setting}}';

    const NEW_ORDER_TIME_ASSIGN = 'NEW_ORDER_TIME_ASSIGN';

    const SETTING_TYPE = 'orders';
    public function safeUp()
    {
        $FREE_ORDER_OFFER_SEC = new DefaultSettings([
            'name'  => self::NEW_ORDER_TIME_ASSIGN,
            'value' => '0',
            'type'  => self::SETTING_TYPE,
        ]);

        return $FREE_ORDER_OFFER_SEC->save() ;
    }
    public function safeDown()
    {
        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::NEW_ORDER_TIME_ASSIGN,
            'type' => self::SETTING_TYPE,
        ]);

        $this->delete(self::TABLE_TENANT_SETTING, ['name' => self::NEW_ORDER_TIME_ASSIGN, 'type' => self::SETTING_TYPE]);
    }
}
