<?php

use yii\db\Migration;

class m161019_103848_tbl_worker_shift__added_statistic_columns extends Migration
{
    const TABLE_WORKER_SHIFT = '{{%worker_shift}}';

    const COLUMN_WORKER_LATE_TIME = 'worker_late_time';
    const COLUMN_WORKER_LATE_COUNT = 'worker_late_count';
    const COLUMN_COMPLETED_ORDER_COUNT = 'completed_order_count';
    const COLUMN_REJECTED_ORDER_COUNT = 'rejected_order_count';
    const COLUMN_ACCEPTED_ORDER_OFFER_COUNT = 'accepted_order_offer_count';
    const COLUMN_REJECTED_ORDER_OFFER_COUNT = 'rejected_order_offer_count';

    public function up()
    {
        $this->addColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_WORKER_LATE_TIME,
            $this->integer()->notNull()->defaultValue(0));
        $this->addColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_WORKER_LATE_COUNT,
            $this->integer()->notNull()->defaultValue(0));
        $this->addColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_COMPLETED_ORDER_COUNT,
            $this->integer()->notNull()->defaultValue(0));
        $this->addColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_REJECTED_ORDER_COUNT,
            $this->integer()->notNull()->defaultValue(0));
        $this->addColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_ACCEPTED_ORDER_OFFER_COUNT,
            $this->integer()->notNull()->defaultValue(0));
        $this->addColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_REJECTED_ORDER_OFFER_COUNT,
            $this->integer()->notNull()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_WORKER_LATE_TIME);
        $this->dropColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_WORKER_LATE_COUNT);
        $this->dropColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_COMPLETED_ORDER_COUNT);
        $this->dropColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_REJECTED_ORDER_COUNT);
        $this->dropColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_ACCEPTED_ORDER_OFFER_COUNT);
        $this->dropColumn(self::TABLE_WORKER_SHIFT, self::COLUMN_REJECTED_ORDER_OFFER_COUNT);
    }

}
