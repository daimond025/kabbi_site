<?php

use yii\db\Migration;

class m180116_101150_change_value_settings extends Migration
{

    const TABLE_NAME = '{{%tenant_setting}}';

    const COLUMN_VALUE = 'value';
    const COLUMN_NAME = 'name';

    const VALUE_COLUMN_NAME__IOS_WORKER_APP_VERSION = 'IOS_WORKER_APP_VERSION';
    const VALUE_COLUMN_NAME__ANDROID_WORKER_APP_VERSION = 'ANDROID_WORKER_APP_VERSION';

    const NEW_VALUE = null;

    const OLD_DEFAULT_VALUE__ANDROID_WORKER_APP_VERSION = '29';
    const OLD_DEFAULT_VALUE__IOS_WORKER_APP_VERSION = '2.05';

    public function safeUp()
    {
        $this->update(self::TABLE_NAME, [
            self::COLUMN_VALUE => self::NEW_VALUE
        ], [
            self::COLUMN_NAME => self::VALUE_COLUMN_NAME__IOS_WORKER_APP_VERSION,
        ]);

        $this->update(self::TABLE_NAME, [
            self::COLUMN_VALUE => self::NEW_VALUE
        ], [
            self::COLUMN_NAME => self::VALUE_COLUMN_NAME__ANDROID_WORKER_APP_VERSION,
        ]);
    }

    public function safeDown()
    {
        $this->update(self::TABLE_NAME, [
            self::COLUMN_VALUE => self::OLD_DEFAULT_VALUE__ANDROID_WORKER_APP_VERSION
        ], [
            self::COLUMN_NAME => self::VALUE_COLUMN_NAME__ANDROID_WORKER_APP_VERSION,
        ]);

        $this->update(self::TABLE_NAME, [
            self::COLUMN_VALUE => self::OLD_DEFAULT_VALUE__IOS_WORKER_APP_VERSION
        ], [
            self::COLUMN_NAME => self::VALUE_COLUMN_NAME__IOS_WORKER_APP_VERSION,
        ]);
    }
}
