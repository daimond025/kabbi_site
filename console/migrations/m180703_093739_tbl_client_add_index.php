<?php

use yii\db\Migration;

class m180703_093739_tbl_client_add_index extends Migration
{
    public function safeUp()
    {
        $this->createIndex(
            'tbi_client_tenant_id_name_idx',
            '{{%client}}',
            ['tenant_id', 'last_name', 'name', 'second_name']
        );
    }

    public function safeDown()
    {
        $this->dropIndex('tbi_client_tenant_id_name_idx', '{{%client}}');
    }
}
