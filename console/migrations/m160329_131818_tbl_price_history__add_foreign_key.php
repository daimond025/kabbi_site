<?php

use yii\db\Migration;

class m160329_131818_tbl_price_history__add_foreign_key extends Migration
{
    const TABLE_NAME = '{{%price_history}}';
    const REF_TABLE_NAME = '{{%tariff_entity_type}}';
    const COLUMN_NAME = 'entity_type_id';
    const KEY_NAME = 'fk_tbl_price_history__entity_type_id';

    public function up()
    {
        $this->addForeignKey(self::KEY_NAME, self::TABLE_NAME, self::COLUMN_NAME,
            self::REF_TABLE_NAME, 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey(self::KEY_NAME, self::TABLE_NAME);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
