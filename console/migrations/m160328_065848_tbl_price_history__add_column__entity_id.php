<?php

use yii\db\Migration;

class m160328_065848_tbl_price_history__add_column__entity_id extends Migration
{
    const TABLE_NAME = '{{%price_history}}';
    const COLUMN_NAME = 'entity_id';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_NAME, $this->integer()->notNull());
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
