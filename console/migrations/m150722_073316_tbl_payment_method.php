<?php

use yii\db\Schema;
use yii\db\Migration;

class m150722_073316_tbl_payment_method extends Migration
{
    public function up()
    {   
        $rows = [
            ['WITHDRAWAL_FOR_TARIFF', 'Withdrawal for tariff'],
            ['WITHDRAWAL_FOR_CASH_OUT', 'Cash out'],
        ];
        $this->batchInsert('{{%payment_method}}', ['payment', 'name'], $rows);
    }

    public function down()
    {
        echo "m150722_073316_tbl_payment_method cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
