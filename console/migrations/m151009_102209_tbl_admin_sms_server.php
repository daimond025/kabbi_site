<?php

use yii\db\Schema;
use yii\db\Migration;

class m151009_102209_tbl_admin_sms_server extends Migration
{
    public function up()
    {
        $this->addColumn('{{%admin_sms_server}}', 'sign', Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('{{%admin_sms_server}}', 'sign');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
