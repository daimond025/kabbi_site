<?php

use yii\db\Migration;

class m161030_201437_create_table__deposit_from_bank_card extends Migration
{
    const TABLE_DEPOSIT_FROM_BANK_CARD = '{{%deposit_from_bank_card}}';
    const TABLE_TRANSACTION = '{{%transaction}}';
    const COLUMN_TRANSACTION_ID = 'transaction_id';
    const FOREIGN_KEY_TRANSACTION_ID = 'fk_deposit_from_bank_card__transaction_id';

    public function up()
    {
        $this->createTable(self::TABLE_DEPOSIT_FROM_BANK_CARD, [
            'id'               => $this->primaryKey(),
            'request_id'       => $this->string()->notNull()->unique(),
            'pan'              => $this->string()->notNull(),
            'transaction_id'   => $this->integer()->notNull(),
            'payment_order_id' => $this->string()->notNull(),
        ]);

        $this->addForeignKey(
            self::FOREIGN_KEY_TRANSACTION_ID,
            self::TABLE_DEPOSIT_FROM_BANK_CARD,
            self::COLUMN_TRANSACTION_ID,
            self::TABLE_TRANSACTION,
            self::COLUMN_TRANSACTION_ID,
            'CASCADE',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable(self::TABLE_DEPOSIT_FROM_BANK_CARD);
    }

}
