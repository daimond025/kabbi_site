<?php

use yii\db\Schema;
use yii\db\Migration;

class m160210_051859_tbl_add_description extends Migration
{
    
    const TABLE_NAME_DRIVER = '{{%driver}}';
    
    const TABLE_NAME_CLIENT = '{{%client}}';
    
    const TABLE_NAME_COMPANY = '{{%client_company}}';
    
    
    
    public function up()
    {
        $this->addColumn(self::TABLE_NAME_DRIVER, 'description', 'VARCHAR(255)');
        $this->addColumn(self::TABLE_NAME_CLIENT, 'description', 'VARCHAR(255)');
        $this->addColumn(self::TABLE_NAME_COMPANY, 'description', 'VARCHAR(255)');
        
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME_DRIVER, 'description');
        $this->dropColumn(self::TABLE_NAME_CLIENT, 'description');
        $this->dropColumn(self::TABLE_NAME_COMPANY, 'description');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
