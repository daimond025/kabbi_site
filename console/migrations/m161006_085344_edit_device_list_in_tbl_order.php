<?php

use yii\db\Migration;

class m161006_085344_edit_device_list_in_tbl_order extends Migration
{
    const TABLE_NAME = '{{%order_upup}}';
    const TABLE_ORDER_NAME = '{{%order}}';
    const FOREIGN_KEY = 'fk_order_upup__order_id';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'         => $this->primaryKey(),
            'order_id'   => $this->integer(10)->unsigned()->notNull(),
            'upup_id'    => $this->bigInteger()->unsigned()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
        $this->addForeignKey(
            self::FOREIGN_KEY, self::TABLE_NAME, 'order_id',
            self::TABLE_ORDER_NAME, 'order_id', 'CASCADE', 'CASCADE');
        return true;
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FOREIGN_KEY, self::TABLE_NAME);
        $this->dropTable(self::TABLE_NAME);
        return true;
    }
}
