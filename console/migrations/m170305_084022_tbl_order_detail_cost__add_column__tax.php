<?php

use yii\db\Migration;

class m170305_084022_tbl_order_detail_cost__add_column__tax extends Migration
{
    const TABLE_ORDER_DETAIL_COST = '{{%order_detail_cost}}';
    const COLUMN_SURCHARGE = 'tax';

    public function up()
    {
        $this->addColumn(self::TABLE_ORDER_DETAIL_COST, self::COLUMN_SURCHARGE,
            $this->string(45)->comment('Налог'));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_ORDER_DETAIL_COST, self::COLUMN_SURCHARGE);
    }
}
