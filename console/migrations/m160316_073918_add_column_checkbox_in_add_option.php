<?php

use yii\db\Migration;

class m160316_073918_add_column_checkbox_in_add_option extends Migration
{
        const TABLE_NAME = '{{%tenant_has_car_option}}';
    public function up()
    {
	$this->addColumn(self::TABLE_NAME, 'is_active', $this->smallInteger(1)->defaultValue(1));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME,'is_active');
    }
}
