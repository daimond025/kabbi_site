<?php

use yii\db\Migration;

/**
 * Handles the creation of table `referral_system_subscribers`.
 */
class m170410_100327_create_referral_system_subscribers_table extends Migration
{
    const TABLE_NAME = '{{%referral_system_subscribers}}';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'subscriber_id' => $this->primaryKey(),
            'client_id' => $this->integer(10)->unsigned()->notNull(),
            'member_id' => $this->integer(11)->notNull(),
        ]);

        $this->addForeignKey('referral_system_subscribers__member_id', self::TABLE_NAME, 'member_id',
            '{{%referral_system_members}}', 'member_id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('referral_system_subscribers__client_id', self::TABLE_NAME, 'client_id',
            '{{%client}}', 'client_id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
