<?php

use yii\db\Schema;
use yii\db\Migration;


class m151230_032811_create_table__bonus_fail_log extends Migration
{

    const TABLE_NAME              = '{{%bonus_fail_log}}';
    const CLIENT_TABLE_NAME       = '{{%client}}';
    const ORDER_TABLE_NAME        = '{{%order}}';
    const CLIENT_BONUS_TABLE_NAME = '{{%client_bonus}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable(self::TABLE_NAME, [
            'id'           => $this->primaryKey(),
            'order_id'     => 'INT(10) UNSIGNED NOT NULL',
            'client_id'    => 'INT(10) UNSIGNED NOT NULL',
            'device'       => $this->string()->notNull(),
            'device_token' => $this->string()->notNull(),
            'order_time'   => $this->integer()->notNull(),
            'bonus_id'     => $this->integer()->notNull(),
            'bonus'        => $this->decimal(12, 2)->notNull(),
                ], $tableOptions);
        
        $this->addForeignKey('fk_bonus_fail_log__client_id', self::TABLE_NAME, 'client_id',
                self::CLIENT_TABLE_NAME, 'client_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_bonus_fail_log__order_id', self::TABLE_NAME, 'order_id',
                self::ORDER_TABLE_NAME, 'order_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_bonus_fail_log__bonus_id', self::TABLE_NAME, 'bonus_id',
                self::CLIENT_BONUS_TABLE_NAME, 'bonus_id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */

}
