<?php

use yii\db\Migration;

class m160229_130640_tbl_message extends Migration
{
    const TABLE_NAME = '{{%message}}';

    public function up()
    {
        $this->update(self::TABLE_NAME, ['language' => 'de'], ['language' => 'de-EU']);
        $this->update(self::TABLE_NAME, ['language' => 'sr'], ['language' => 'se']);
        $this->update(self::TABLE_NAME, ['language' => 'ru'], ['language' => 'ru-RU']);
    }

    public function down()
    {
        echo "m160229_130640_tbl_message cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
