<?php

use yii\db\Schema;
use yii\db\Migration;

class m151119_141650_rename_days_in_option_tariffs extends Migration
{
    const DRIVER_OPTION_TARIFF = '{{%driver_option_tariff}}';
    const OPTION_ACTIVE_DATE = '{{%option_active_date}}';
    const CLIENT_BONUS = '{{%client_bonus}}';
    const ACTIVE_DATE = 'active_date';
    const ACTUAL_DATE = 'actual_date';

    protected function getUpQuery($table, $field)
    {
return <<<SQL
UPDATE $table
    set `$field` = replace(
        replace(
        replace(
        replace(
        replace(
        replace(
        replace(
            `$field`,
            'Понедельник', 'Monday'),
            'Вторник', 'Tuesday'),
            'Среда', 'Wednesday'),
            'Четверг', 'Thursday'),
            'Пятница', 'Friday'),
            'Суббота', 'Saturday'),
            'Воскресенье', 'Sunday')
WHERE
        $field like '%Понедельник%' OR
        $field like '%Вторник%' OR
        $field like '%Среда%' OR
        $field like '%Четверг%' OR
        $field like '%Пятница%' OR
        $field like '%Суббота%' OR
        $field like '%Воскресенье%';
SQL;
    }

    protected function getDownQuery($table, $field)
    {
return <<<SQL
UPDATE $table
    set `$field` = replace(
        replace(
        replace(
        replace(
        replace(
        replace(
        replace(
            `$field`,
            'Monday', 'Понедельник'),
            'Tuesday', 'Вторник'),
            'Wednesday', 'Среда'),
            'Thursday', 'Четверг'),
            'Friday', 'Пятница'),
            'Saturday', 'Суббота'),
            'Sunday', 'Воскресенье')
WHERE
        $field like '%Monday%' OR
        $field like '%Tuesday%' OR
        $field like '%Wednesday%' OR
        $field like '%Thursday%' OR
        $field like '%Friday%' OR
        $field like '%Saturday%' OR
        $field like '%Sunday%'
SQL;
    }

//    public function up()
//    {
//    }
//
//    public function down()
//    {
//    }

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->db->createCommand(
                $this->getUpQuery(self::CLIENT_BONUS, self::ACTUAL_DATE)
            )->execute();
        $this->db->createCommand(
                $this->getUpQuery(self::DRIVER_OPTION_TARIFF, self::ACTIVE_DATE)
            )->execute();
        $this->db->createCommand(
                $this->getUpQuery(self::OPTION_ACTIVE_DATE, self::ACTIVE_DATE)
            )->execute();
    }

    public function safeDown()
    {
        $this->db->createCommand(
                $this->getDownQuery(self::CLIENT_BONUS, self::ACTUAL_DATE)
            )->execute();
        $this->db->createCommand(
                $this->getDownQuery(self::DRIVER_OPTION_TARIFF, self::ACTIVE_DATE)
            )->execute();
        $this->db->createCommand(
                $this->getDownQuery(self::OPTION_ACTIVE_DATE, self::ACTIVE_DATE)
            )->execute();
    }
    
}
