<?php

use yii\db\Schema;
use yii\db\Migration;

class m150727_114520_taxi_tariff_block extends Migration
{
    public function up()
    {
        $this->addColumn('{{%taxi_tariff}}', 'block', Schema::TYPE_BOOLEAN . ' UNSIGNED NULL DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('{{%taxi_tariff}}', 'block');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
