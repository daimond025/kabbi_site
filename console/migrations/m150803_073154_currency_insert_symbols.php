<?php

use yii\db\Schema;
use yii\db\Migration;

class m150803_073154_currency_insert_symbols extends Migration
{
    public function up()
    {
        $this->update('{{%currency}}', ['symbol' => '₽'], ['code' => 'RUB']);
        $this->update('{{%currency}}', ['symbol' => '$'], ['code' => 'USD']);
        $this->update('{{%currency}}', ['symbol' => 'man.'], ['code' => 'AZN']);
        $this->update('{{%currency}}', ['symbol' => 'din.'], ['code' => 'RSD']);
    }

    public function down()
    {
        $this->update('{{%currency}}', ['symbol' => ''], ['code' => 'RUB']);
        $this->update('{{%currency}}', ['symbol' => ''], ['code' => 'USD']);
        $this->update('{{%currency}}', ['symbol' => ''], ['code' => 'AZN']);
        $this->update('{{%currency}}', ['symbol' => ''], ['code' => 'RSD']);
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
