<?php

use yii\db\Schema;
use yii\db\Migration;

class m150728_061631_tbl_client_company extends Migration
{
    public function up()
    {
        $this->execute('SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0');
        $this->execute('SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0');   
        $this->execute("SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES'");
        
        $this->dropTable('{{%client_company_has_city}}');
        $this->addColumn('{{%client_company}}', 'city_id', Schema::TYPE_INTEGER . ' UNSIGNED NOT NULL');
        $this->addForeignKey('FK_client_company_city_id', '{{%client_company}}', 'city_id', '{{%city}}', 'city_id');
        
        $this->execute('SET SQL_MODE=@OLD_SQL_MODE');
        $this->execute('SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS');
        $this->execute('SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS');
    }

    public function down()
    {
        echo "m150728_061631_tbl_client_company cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
