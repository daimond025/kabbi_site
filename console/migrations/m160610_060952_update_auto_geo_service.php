<?php

use yii\db\Migration;

class m160610_060952_update_auto_geo_service extends Migration
{
    const TABLE_NAME = '{{%auto_geo_service_type}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'key_1_label', $this->string(50));
        $this->addColumn(self::TABLE_NAME, 'key_2_label', $this->string(50));
        $this->renameColumn(self::TABLE_NAME, 'has_app_code', 'has_key_1');
        $this->renameColumn(self::TABLE_NAME, 'has_app_id', 'has_key_2');
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'key_1_label');
        $this->dropColumn(self::TABLE_NAME, 'key_2_label');
        $this->renameColumn(self::TABLE_NAME, 'has_key_1', 'has_app_code');
        $this->renameColumn(self::TABLE_NAME, 'has_key_2', 'has_app_id');
        return true;
    }
}