<?php

use yii\db\Migration;
use yii\db\Query;

class m160811_183105_add_setting_to_filter_orders_by_distance extends Migration
{
    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const TABLE_TENANT_SETTING = '{{%tenant_setting}}';
    const TABLE_TENANT_HAS_CITY = '{{%tenant_has_city}}';

    const SETTING_NAME = 'DISTANCE_TO_FILTER_FREE_ORDERS';
    const SETTING_TYPE = 'orders';
    const SETTING_VALUE = 10000;

    public function up()
    {
        $this->insert(self::TABLE_DEFAULT_SETTINGS, [
            'name'  => self::SETTING_NAME,
            'type'  => self::SETTING_TYPE,
            'value' => self::SETTING_VALUE,
        ]);

        $tenantCities = (new Query)
            ->from(self::TABLE_TENANT_HAS_CITY)
            ->all();

        foreach ($tenantCities as $item) {
            echo "tenant: {$item['tenant_id']}, city: {$item['city_id']}" . PHP_EOL;

            $this->insert(self::TABLE_TENANT_SETTING, [
                'tenant_id' => $item['tenant_id'],
                'city_id'   => $item['city_id'],
                'name'      => self::SETTING_NAME,
                'type'      => self::SETTING_TYPE,
                'value'     => self::SETTING_VALUE,
            ]);
        }
    }

    public function down()
    {
        $this->delete(self::TABLE_TENANT_SETTING, [
            'name' => self::SETTING_NAME,
            'type' => self::SETTING_TYPE,
        ]);

        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::SETTING_NAME,
            'type' => self::SETTING_TYPE,
        ]);
    }
}
