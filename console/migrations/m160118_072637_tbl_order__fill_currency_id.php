<?php

use yii\db\Schema;
use yii\db\Migration;

class m160118_072637_tbl_order__fill_currency_id extends Migration
{
    const ORDER_TABLE            = '{{%order}}';
    const CURRENCY_ID_COLUMN     = 'currency_id';
    const TENANT_SETTING_TABLE   = '{{%tenant_setting}}';
    const DEFAULT_SETTINGS_TABLE = '{{%default_settings}}';
    const CURRENCY_TABLE         = '{{%currency}}';

    public function up()
    {
        $orderTable           = self::ORDER_TABLE;
        $tenantSettingTable   = self::TENANT_SETTING_TABLE;
        $defaultSettingsTable = self::DEFAULT_SETTINGS_TABLE;
        $currencyTable        = self::CURRENCY_TABLE;

        $updateSql = <<<SQL
            update {$orderTable} as `o`
              join (select `o`.`order_id`, `c`.`currency_id`
                      from {$orderTable} as `o`
                      left join {$tenantSettingTable} as `t` on `o`.`tenant_id` = `t`.`tenant_id`
                                                            and `t`.`name` = 'CURRENCY'
                      left join {$defaultSettingsTable} as `d` on `d`.`name` = 'CURRENCY'
                      join {$currencyTable} as `c` on ifnull(`t`.`value`, `d`.`value`) = `c`.`code`) as `o2`
                on `o`.`order_id` = `o2`.`order_id`
               set `o`.`currency_id` = `o2`.`currency_id`
SQL;
        $this->execute($updateSql);

        $this->alterColumn($orderTable, self::CURRENCY_ID_COLUMN, 'INT(11) NOT NULL');
    }

    public function down()
    {
        $orderTable = self::ORDER_TABLE;

        $this->alterColumn($orderTable, self::CURRENCY_ID_COLUMN,
                'INT(11) NULL DEFAULT NULL');

        $updateSql = <<<SQL
            update {$orderTable}
               set `currency_id` = null
SQL;

        $this->execute($updateSql);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
