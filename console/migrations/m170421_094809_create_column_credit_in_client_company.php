<?php

use yii\db\Migration;

class m170421_094809_create_column_credit_in_client_company extends Migration
{
    const TABLE_NAME = '{{%client_company}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'credits', $this->float(10)->defaultValue(0)->notNull());
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'credits');
    }
}
