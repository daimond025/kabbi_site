<?php

use yii\db\Schema;
use yii\db\Migration;

class m151030_061850_tbl_city_add_city_polygon_fileld extends Migration
{

    public function up()
    {
        $this->addColumn('{{%city}}', 'city_polygon', Schema::TYPE_TEXT . ' DEFAULT  NULL');
    }

    public function down()
    {
        $this->dropColumn('{{%city}}', 'city_polygon');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
