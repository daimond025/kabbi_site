<?php

use yii\db\Migration;

class m160426_061031_tbl_transaction__modify_column__city_id extends Migration
{
    const TABLE_TRANSACTION = '{{%transaction}}';
    const COLUMN_CITY_ID = 'city_id';

    public function up()
    {
        $this->alterColumn(self::TABLE_TRANSACTION, self::COLUMN_CITY_ID, 'INT(11) UNSIGNED');
    }

    public function down()
    {
        $this->alterColumn(self::TABLE_TRANSACTION, self::COLUMN_CITY_ID, 'INT(11) UNSIGNED NOT NULL');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
