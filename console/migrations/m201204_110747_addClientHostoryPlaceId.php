<?php

use yii\db\Migration;

class m201204_110747_addClientHostoryPlaceId extends Migration
{
    const CLIENT_ADDRESS_HISTORY= '{{%client_address_history}}';
    public function up()
    {
        $this->addColumn(self::CLIENT_ADDRESS_HISTORY, 'placeId', $this->string()->defaultValue('')->after('lon'));
    }

    public function safeDown()
    {
        $this->dropColumn(self::CLIENT_ADDRESS_HISTORY, 'placeId');
    }
}
