<?php

use yii\db\Migration;

class m180716_103617_tbl_messages_add_idx extends Migration
{
    public function safeUp()
    {
        $this->createIndex('tbl_source_message_category', '{{%source_message}}', [
            'category',
        ]);

        $this->createIndex('tbl_message_language', '{{%message}}', [
            'language',
        ]);
    }

    public function safeDown()
    {
        $this->dropIndex('tbl_source_message_category', '{{%source_message}}');
        $this->dropIndex('tbl_message_language', '{{%message}}');
    }

}
