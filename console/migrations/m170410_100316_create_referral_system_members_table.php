<?php

use yii\db\Migration;

/**
 * Handles the creation of table `referral_system_members`.
 */
class m170410_100316_create_referral_system_members_table extends Migration
{

    const TABLE_NAME = '{{%referral_system_members}}';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'member_id' => $this->primaryKey(),
            'client_id' => $this->integer(10)->unsigned()->notNull(),
            'referral_id' => $this->integer(11)->notNull(),
            'code' => $this->string('10')->unique()->notNull(),
        ]);

        $this->addForeignKey('referral_system_members__referral_id', self::TABLE_NAME, 'referral_id',
            '{{%referral}}', 'referral_id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('referral_system_members__client_id', self::TABLE_NAME, 'client_id',
            '{{%client}}', 'client_id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
