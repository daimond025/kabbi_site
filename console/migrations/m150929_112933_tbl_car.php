<?php

use yii\db\Schema;
use yii\db\Migration;

class m150929_112933_tbl_car extends Migration
{
    public function up()
    {
        $this->execute('SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0');
        $this->execute('SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0');
        $this->execute("SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES'");

        $this->addColumn('{{%car}}', 'group_id', Schema::TYPE_INTEGER . '(10)');
        $this->addForeignKey('FK_car_group_id', '{{%car}}', 'group_id', '{{%driver_group}}', 'group_id', 'SET NULL', 'CASCADE');

        $this->execute('SET SQL_MODE=@OLD_SQL_MODE');
        $this->execute('SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS');
        $this->execute('SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS');
    }

    public function down()
    {
        $this->dropForeignKey('FK_car_group_id', '{{%car}}');
        $this->dropColumn('{{%car}}', 'group_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
