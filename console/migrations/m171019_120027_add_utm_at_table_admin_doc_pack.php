<?php

use yii\db\Migration;

class m171019_120027_add_utm_at_table_admin_doc_pack extends Migration
{
    const TABLE_NAME = '{{%admin_docpack}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'utm_source', $this->text());
        $this->addColumn(self::TABLE_NAME, 'utm_medium', $this->text());
        $this->addColumn(self::TABLE_NAME, 'utm_campaign', $this->text());
        $this->addColumn(self::TABLE_NAME, 'utm_content', $this->text());
        $this->addColumn(self::TABLE_NAME, 'utm_term', $this->text());
        $this->addColumn(self::TABLE_NAME, 'client_id_go', $this->text());
        $this->addColumn(self::TABLE_NAME, 'client_id_ya', $this->text());
        $this->addColumn(self::TABLE_NAME, 'ip', $this->text());

    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'utm_source');
        $this->dropColumn(self::TABLE_NAME, 'utm_medium');
        $this->dropColumn(self::TABLE_NAME, 'utm_campaign');
        $this->dropColumn(self::TABLE_NAME, 'utm_content');
        $this->dropColumn(self::TABLE_NAME, 'utm_term');
        $this->dropColumn(self::TABLE_NAME, 'client_id_go');
        $this->dropColumn(self::TABLE_NAME, 'client_id_ya');
        $this->dropColumn(self::TABLE_NAME, 'ip');
        return true;
    }


}
