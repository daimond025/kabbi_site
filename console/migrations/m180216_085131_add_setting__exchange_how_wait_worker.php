<?php

use yii\db\Migration;
use common\modules\tenant\models\DefaultSettings;

class m180216_085131_add_setting__exchange_how_wait_worker extends Migration
{
    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const TABLE_TENANT_SETTING = '{{%tenant_setting}}';

    const SETTING_NAME = 'EXCHANGE_HOW_WAIT_WORKER';
    const SETTING_TYPE = 'orders';

    public function safeUp()
    {
        $defaultSettings = new DefaultSettings();
        $defaultSettings->name = self::SETTING_NAME;
        $defaultSettings->type = self::SETTING_TYPE;


        $defaultSettings->save();

    }

    public function safeDown()
    {
        $this->delete(self::TABLE_TENANT_SETTING, [
            'name' => self::SETTING_NAME,
            'type' => self::SETTING_TYPE,
        ]);

        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::SETTING_NAME,
            'type' => self::SETTING_TYPE,
        ]);
    }
}
