<?php

use yii\db\Migration;

class m161111_130510_tbl_client_review__add_index_for_order_id extends Migration
{
    const TABLE_NAME = '{{%client_review}}';
    const COLUMN_NAME = 'order_id';
    const INDEX_NAME = 'idx_client_review__order_id';

    public function up()
    {
        $this->createIndex(self::INDEX_NAME, self::TABLE_NAME, self::COLUMN_NAME);
    }

    public function down()
    {
        $this->dropIndex(self::INDEX_NAME, self::TABLE_NAME);
    }

}
