<?php

use yii\db\Schema;
use yii\db\Migration;
class m181114_174142_tbl_tenant_company_edit_2 extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%tenant_company}}', 'report_id', Schema::TYPE_INTEGER);

    }

    public function safeDown()
    {
        $this->dropColumn('{{%tenant_company}}', 'report_id', Schema::TYPE_INTEGER);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181114_174142_tbl_tenant_company_edit_2 cannot be reverted.\n";

        return false;
    }
    */
}
