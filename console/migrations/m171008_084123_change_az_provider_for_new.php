<?php

use yii\db\Migration;
use frontend\modules\tenant\models\GeoServiceProvider;

class m171008_084123_change_az_provider_for_new extends Migration
{
    const GEO_GOOTAX_TYPE_ID = 5;
    const GEO_AZ_TYPE_ID = 8;
    const BAKU_CITY_ID = 210863;

    public function safeUp()
    {
        $models = GeoServiceProvider::find()
            ->where([
                'service_type'        => GeoServiceProvider::AUTO_GEO_SERVICE_TYPE,
                'city_id'             => self::BAKU_CITY_ID,
                'service_provider_id' => self::GEO_GOOTAX_TYPE_ID
            ])
            ->all();
        foreach ($models as $model) {
            $model->service_provider_id = self::GEO_AZ_TYPE_ID;
            $model->update(true);
        }
        return true;

    }

    public function safeDown()
    {
        $models = GeoServiceProvider::find()
            ->where([
                'service_type'        => GeoServiceProvider::AUTO_GEO_SERVICE_TYPE,
                'city_id'             => self::BAKU_CITY_ID,
                'service_provider_id' => self::GEO_AZ_TYPE_ID
            ])
            ->all();
        foreach ($models as $model) {
            $model->service_provider_id = self::GEO_GOOTAX_TYPE_ID;
            $model->update(true);
        }
        return true;
    }


}
