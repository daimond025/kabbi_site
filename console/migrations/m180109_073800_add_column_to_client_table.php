<?php

use yii\db\Migration;

class m180109_073800_add_column_to_client_table extends Migration
{
    const TABLE_NAME = '{{%client}}';

    const COLUMN_NAME = 'send_to_email';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME,self::COLUMN_NAME,$this->integer(1)->defaultValue(0));



        return true;
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME,self::COLUMN_NAME);

        return true;
    }

}
