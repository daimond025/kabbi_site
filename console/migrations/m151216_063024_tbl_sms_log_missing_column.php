<?php

use yii\db\Schema;
use yii\db\Migration;

class m151216_063024_tbl_sms_log_missing_column extends Migration {

    const TABLE_NAME = '{{%sms_log}}';

    public function up() {
        $this->addColumn(self::TABLE_NAME, 'tenant_id', $this->integer(5)->notNull());
        $this->addColumn(self::TABLE_NAME, 'response', $this->string(255));
    }

    public function down() {
        $this->dropColumn(self::TABLE_NAME, 'tenant_id');
        $this->dropColumn(self::TABLE_NAME, 'response');
    }

}
