<?php

use yii\db\Migration;

class m160827_145828_create_table__tenant_has_bonus_system extends Migration
{
    const TABLE_TENANT_HAS_BONUS_SYSTEM = '{{%tenant_has_bonus_system}}';
    const TABLE_BONUS_SYSTEM = '{{%bonus_system}}';
    const TABLE_TENANT = '{{%tenant}}';
    const FOREIGN_KEY__TENANT_ID = 'fk_tenant_has_bonus_system__tenant_id';
    const FOREIGN_KEY__BONUS_SYSTEM_ID = 'fk_tenant_has_bonus_system__bonus_system_id';

    public function up()
    {
        $this->createTable(self::TABLE_TENANT_HAS_BONUS_SYSTEM, [
            'id'              => $this->primaryKey(),
            'tenant_id'       => $this->integer(10)->unsigned()->notNull()->unique(),
            'bonus_system_id' => $this->integer()->notNull(),
            'api_key'         => $this->string(),
        ]);

        $this->addForeignKey(self::FOREIGN_KEY__TENANT_ID, self::TABLE_TENANT_HAS_BONUS_SYSTEM, 'tenant_id',
            self::TABLE_TENANT, 'tenant_id', 'CASCADE', 'CASCADE');

        $this->addForeignKey(self::FOREIGN_KEY__BONUS_SYSTEM_ID, self::TABLE_TENANT_HAS_BONUS_SYSTEM, 'bonus_system_id',
            self::TABLE_BONUS_SYSTEM, 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable(self::TABLE_TENANT_HAS_BONUS_SYSTEM);
    }

}
