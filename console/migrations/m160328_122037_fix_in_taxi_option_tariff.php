<?php

use yii\db\Migration;

class m160328_122037_fix_in_taxi_option_tariff extends Migration
{
    const TABLE_NAME = '{{%driver_option_tariff}}';
    
    public function up()
    {
        $this->update(self::TABLE_NAME, ['period' => 12], 'period_type = "HOURS" AND period NOT IN (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24)');
        $this->update(self::TABLE_NAME, ['period' => '08:00-20:00'], 'period_type = "INTERVAL" AND period NOT REGEXP "^([0-1][0-9]|2[0-3]):[0-5][0-9]-([0-1][0-9]|2[0-3]):[0-5][0-9]$"');
    }

    public function down()
    {
        echo "m160328_122037_fix_in_taxi_option_tariff cannot be reverted.\n";

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
