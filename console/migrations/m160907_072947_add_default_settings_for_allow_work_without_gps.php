<?php

use yii\db\Migration;

class m160907_072947_add_default_settings_for_allow_work_without_gps extends Migration
{
    public function up()
    {
        $this->insert('{{%default_settings}}', [
            'name' => 'ALLOW_WORK_WITHOUT_GPS',
            'value' => 0,
            'type' => 'drivers'
        ]);
    }

    public function down()
    {
        $this->delete('{{%default_settings}}', ['name' => 'ALLOW_WORK_WITHOUT_GPS']);
    }
    
}
