<?php

use yii\db\Migration;
use yii\helpers\ArrayHelper;

class m180713_061226_add_model_id_in_cars_table extends Migration
{
    const CARS_TABLE_NAME = '{{%car}}';
    const CAR_MODELS_TABLE_NAME = '{{%car_model}}';

    public function safeUp()
    {
        $this->addColumn(self::CARS_TABLE_NAME, 'model_id', $this->integer(10)->unsigned()->after('model'));

        $this->addForeignKey(
            'fk_model_id__tbl_cars',
            self::CARS_TABLE_NAME,
            'model_id',
            self::CAR_MODELS_TABLE_NAME,
            'model_id',
            'CASCADE',
            'CASCADE'
        );

        $this->dataFill();

    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_model_id__tbl_cars', self::CARS_TABLE_NAME);
        $this->dropColumn(self::CARS_TABLE_NAME, 'model_id');
    }


    private function dataFill()
    {
        $cars      = $this->getCars();
        $carModels = $this->getCarModels();
        $carBrands = $this->getCarBrands();

        $carBrandList = ArrayHelper::map($carBrands, 'brand_id', 'name');

        foreach ($cars as $car) {
            $currentCarId    = ArrayHelper::getValue($car, 'car_id');
            $currentCarModel = ArrayHelper::getValue($car, 'model');
            $currentCarBrand = ArrayHelper::getValue($car, 'brand');

            if (!$currentCarModel || !$currentCarBrand) {
                continue;
            }

            $currentCarBrandId = array_search($currentCarBrand, $carBrandList);

            foreach ($carModels as $carModel) {
                if (ArrayHelper::getValue($carModel, 'brand_id') == $currentCarBrandId
                    && ArrayHelper::getValue($carModel, 'name') == $currentCarModel
                ) {
                    $currentCarModelId = ArrayHelper::getValue($carModel, 'model_id');

                    $this->update(
                        self::CARS_TABLE_NAME,
                        ['model_id' => $currentCarModelId],
                        ['car_id' => $currentCarId]
                    );
                    break;
                }
            }
        }
    }

    private function getCars()
    {
        return (new \yii\db\Query())->from(self::CARS_TABLE_NAME)->all();
    }

    private function getCarBrands()
    {
        return (new \yii\db\Query())->from('{{%car_brand}}')->all();
    }

    private function getCarModels()
    {
        return (new \yii\db\Query())->from(self::CAR_MODELS_TABLE_NAME)->all();
    }
}
