<?php

use yii\db\Schema;
use yii\db\Migration;

class m150929_093401_tbl_default_settings extends Migration
{
    public function up()
    {
        $this->insert('{{%default_settings}}', ['name' => 'ORDER_REJECT_TIME', 'type' => 'orders', 'value' => 60]);
        $this->insert('{{%default_settings}}', ['name' => 'ORDER_OFFER_SEC', 'type' => 'orders', 'value' => 15]);
    }

    public function down()
    {
        $this->delete('{{%default_settings}}', ['name' => ['ORDER_REJECT_TIME', 'ORDER_OFFER_SEC']]);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
