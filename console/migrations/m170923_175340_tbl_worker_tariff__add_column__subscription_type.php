<?php

use yii\db\Migration;

class m170923_175340_tbl_worker_tariff__add_column__subscription_type extends Migration
{
    const TABLE_NAME = '{{%worker_tariff}}';
    const COLUMN_NAME = 'subscription_limit_type';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_NAME, "ENUM('SHIFT_COUNT','DAY_COUNT') NULL");
        $this->update(self::TABLE_NAME, ['subscription_limit_type' => 'SHIFT_COUNT'], ['type' => 'SUBSCRIPTION']);
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);
    }

}
