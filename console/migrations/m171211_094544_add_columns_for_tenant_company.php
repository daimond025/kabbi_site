<?php

use yii\db\Migration;

class m171211_094544_add_columns_for_tenant_company extends Migration
{

    const COLUMN_TENANT_COMPANY_ID = 'tenant_company_id';

    const TABLE_NAME_TENANT_COMPANY = '{{%tenant_company}}';

    const TABLE_NAME_WORKER = '{{%worker}}';
    const TABLE_NAME_CAR = '{{%car}}';
    const TABLE_NAME_WORKER_TARIFF = '{{%worker_tariff}}';
    const TABLE_NAME_ORDER = '{{%order}}';
    const TABLE_NAME_USER = '{{%user}}';

    const FK_WORKER__TENANT_COMPANY = 'fk_worker__tenant_company';
    const FK_CAR__TENANT_COMPANY = 'fk_car__tenant_company';
    const FK_WORKER_TARIFF__TENANT_COMPANY = 'fk_worker_tariff__tenant_company';
    const FK_ORDER__TENANT_COMPANY = 'fk_order__tenant_company';
    const FK_USER__TENANT_COMPANY = 'fk_user__tenant_company';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME_WORKER, self::COLUMN_TENANT_COMPANY_ID, $this->integer()->unsigned());
        $this->addColumn(self::TABLE_NAME_CAR, self::COLUMN_TENANT_COMPANY_ID, $this->integer()->unsigned());
        $this->addColumn(self::TABLE_NAME_WORKER_TARIFF, self::COLUMN_TENANT_COMPANY_ID, $this->integer()->unsigned());
        $this->addColumn(self::TABLE_NAME_ORDER, self::COLUMN_TENANT_COMPANY_ID, $this->integer()->unsigned());
        $this->addColumn(self::TABLE_NAME_USER, self::COLUMN_TENANT_COMPANY_ID, $this->integer()->unsigned());

        $this->addForeignKey(self::FK_WORKER__TENANT_COMPANY, self::TABLE_NAME_WORKER, self::COLUMN_TENANT_COMPANY_ID, self::TABLE_NAME_TENANT_COMPANY, self::COLUMN_TENANT_COMPANY_ID, 'CASCADE', 'CASCADE');

        $this->addForeignKey(self::FK_CAR__TENANT_COMPANY, self::TABLE_NAME_CAR, self::COLUMN_TENANT_COMPANY_ID, self::TABLE_NAME_TENANT_COMPANY, self::COLUMN_TENANT_COMPANY_ID, 'CASCADE', 'CASCADE');

        $this->addForeignKey(self::FK_WORKER_TARIFF__TENANT_COMPANY, self::TABLE_NAME_WORKER_TARIFF, self::COLUMN_TENANT_COMPANY_ID, self::TABLE_NAME_TENANT_COMPANY, self::COLUMN_TENANT_COMPANY_ID, 'CASCADE', 'CASCADE');

        $this->addForeignKey(self::FK_ORDER__TENANT_COMPANY, self::TABLE_NAME_ORDER, self::COLUMN_TENANT_COMPANY_ID, self::TABLE_NAME_TENANT_COMPANY, self::COLUMN_TENANT_COMPANY_ID, 'CASCADE', 'CASCADE');

        $this->addForeignKey(self::FK_USER__TENANT_COMPANY, self::TABLE_NAME_USER, self::COLUMN_TENANT_COMPANY_ID, self::TABLE_NAME_TENANT_COMPANY, self::COLUMN_TENANT_COMPANY_ID, 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {

        $this->dropForeignKey(self::FK_USER__TENANT_COMPANY, self::TABLE_NAME_USER);
        $this->dropForeignKey(self::FK_ORDER__TENANT_COMPANY, self::TABLE_NAME_ORDER);
        $this->dropForeignKey(self::FK_WORKER_TARIFF__TENANT_COMPANY, self::TABLE_NAME_WORKER_TARIFF);
        $this->dropForeignKey(self::FK_CAR__TENANT_COMPANY, self::TABLE_NAME_CAR);
        $this->dropForeignKey(self::FK_WORKER__TENANT_COMPANY, self::TABLE_NAME_WORKER);

        $this->dropColumn(self::TABLE_NAME_USER, self::COLUMN_TENANT_COMPANY_ID);
        $this->dropColumn(self::TABLE_NAME_ORDER, self::COLUMN_TENANT_COMPANY_ID);
        $this->dropColumn(self::TABLE_NAME_WORKER_TARIFF, self::COLUMN_TENANT_COMPANY_ID);
        $this->dropColumn(self::TABLE_NAME_CAR, self::COLUMN_TENANT_COMPANY_ID);
        $this->dropColumn(self::TABLE_NAME_WORKER, self::COLUMN_TENANT_COMPANY_ID);

    }
}
