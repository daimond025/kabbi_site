<?php

use yii\db\Migration;

class m161010_054403_tenant_setting_update extends Migration
{

    const TABLE_NAME = '{{%tenant_setting}}';
    const DEFAULT_TABLE_NAME = '{{%default_settings}}';
    const CITY_TABLE_NAME = "{{%tenant_has_city}}";

    public function up()
    {

        $this->alterColumn(self::TABLE_NAME, 'value', $this->text()->null());
        $this->alterColumn(self::DEFAULT_TABLE_NAME, 'value', $this->text()->null());


        $tenantCityList = (new \yii\db\Query())
            ->from(self::CITY_TABLE_NAME)
            ->all();

        $defaultSetting = (new \yii\db\Query())
            ->from(self::DEFAULT_TABLE_NAME)
            ->where(['not', ['type' => 'system']])
            ->all();
        $defaultSettingValue = \yii\helpers\ArrayHelper::map($defaultSetting, 'name', 'value');
        $defaultSettingType = \yii\helpers\ArrayHelper::map($defaultSetting, 'name', 'type');

        foreach ($tenantCityList as $city) {
            $cityId = $city['city_id'];
            $tenantId = $city['tenant_id'];

            $tenantSetting = (new \yii\db\Query())
                ->from(self::TABLE_NAME)
                ->where(['not', ['type' => 'system']])
                ->andWhere([
                    'city_id' => $cityId,
                    'tenant_id' => $tenantId
                ])
                ->all();
            $tenantSettingValue = \yii\helpers\ArrayHelper::map($tenantSetting, 'name', 'value');

            foreach ($defaultSettingType as $name => $setting) {
                if(!array_key_exists($name, $tenantSettingValue)) {
                    Yii::$app->db->createCommand()->insert(self::TABLE_NAME, [
                        'tenant_id' => $tenantId,
                        'name'      => $name,
                        'value'     => $defaultSettingValue[$name],
                        'type'      => $defaultSettingType[$name],
                        'city_id'   => $cityId,
                    ])->execute();
                }
            }
        }
    }

    public function down()
    {
        echo "m161010_054403_tenant_setting_update cannot be reverted.\n";

        return true;
    }
}
