<?php

use yii\db\Schema;
use yii\db\Migration;

class m160126_094351_tbl_client_push_notifications extends Migration
{
    public function up()
    {
        $push_notifications = (new \yii\db\Query())
            ->from('{{%client_push_notifications}}')
            ->all();

        $arCity = [];
        $arDefaultCity = [];

        foreach ($push_notifications as $item) {
            if (!isset($arCity[$item['tenant_id']])) {
                $tenantHasCity = (new \yii\db\Query())
                    ->select('city_id')
                    ->from('{{%tenant_has_city}}')
                    ->where(['tenant_id' => $item['tenant_id']])
                    ->all();
                $tenantHasCity = \yii\helpers\ArrayHelper::getColumn($tenantHasCity, 'city_id');
                $arCity[$item['tenant_id']] = $tenantHasCity;
                $arDefaultCity[$item['tenant_id']] = array_shift($arCity[$item['tenant_id']]);
            }

            if (empty($item['city_id'])) {
                Yii::$app->db->createCommand()->update('{{%client_push_notifications}}', ['city_id' => $arDefaultCity[$item['tenant_id']]], ['push_id' => $item['push_id']])->execute();
            }

            foreach ($arCity[$item['tenant_id']] as $cityItem) {
                Yii::$app->db->createCommand()->insert('{{%client_push_notifications}}', [
                    'text'    => $item['text'],
                    'type'     => $item['type'],
                    'tenant_id' => $item['tenant_id'],
                    'city_id'   => $cityItem,
                ])->execute();
            }
        }
    }

    public function down()
    {
        echo "m160126_094351_tbl_client_push_notifications cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
