<?php

use yii\db\Migration;
use common\modules\tenant\models\DefaultSettings;

class m190726_101327_offer_free_value_params extends Migration
{
    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const TABLE_TENANT_SETTING = '{{%tenant_setting}}';

    // параметры преложения свободнеого заказа
    const OFFER_FREE_ORDER = 'OFFER_FREE_ORDER';  //  предлагать свободный заказ
    const OFFER_FREE_ORDER_COUNT = 'OFFER_FREE_ORDER_COUNT';  //  количество  предлождений

    /// есливия предлоджения
    const OFFER_FREE_ORDER_WORKER_ON_ORDER = 'OFFER_FREE_ORDER_WORKER_ON_ORDER';   //  предлагать свободный заказ водителям которые на заказе
    const OFFER_FREE_ORDER_WORKER_ON_BREAK = 'OFFER_FREE_ORDER_WORKER_ON_BREAK';   //  предлагать свободный заказ водителям которые на паузе

    const OFFER_FREE_ORDER_DISTANCE = 'OFFER_FREE_ORDER_DISTANCE';   //  расстояние между конечсной точкой заказа и  началом другого заказа

    const SETTING_TYPE = 'orders';

    public function safeUp()
    {
        $OFFER_FREE_ORDER = new DefaultSettings([
            'name'  => self::OFFER_FREE_ORDER,
            'value' => '0',
            'type'  => self::SETTING_TYPE,
        ]);

        $OFFER_FREE_ORDER_COUNT = new DefaultSettings([
            'name'  => self::OFFER_FREE_ORDER_COUNT,
            'value' => '0',
            'type'  => self::SETTING_TYPE,
        ]);

        $OFFER_FREE_ORDER_WORKER_ON_ORDER = new DefaultSettings([
            'name'  => self::OFFER_FREE_ORDER_WORKER_ON_ORDER,
            'value' => '0',
            'type'  => self::SETTING_TYPE,
        ]);

        $OFFER_FREE_ORDER_WORKER_ON_BREAK = new DefaultSettings([
            'name'  => self::OFFER_FREE_ORDER_WORKER_ON_BREAK,
            'value' => '0',
            'type'  => self::SETTING_TYPE,
        ]);

        $OFFER_FREE_ORDER_DISTANCE = new DefaultSettings([
            'name'  => self::OFFER_FREE_ORDER_DISTANCE,
            'value' => '0',
            'type'  => self::SETTING_TYPE,
        ]);


        return $OFFER_FREE_ORDER->save() && $OFFER_FREE_ORDER_COUNT->save() && $OFFER_FREE_ORDER_WORKER_ON_ORDER->save() &&
            $OFFER_FREE_ORDER_WORKER_ON_BREAK->save() &&  $OFFER_FREE_ORDER_DISTANCE->save() ;
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::OFFER_FREE_ORDER,
            'type' => 'drivers',
        ]);

        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::OFFER_FREE_ORDER_COUNT,
            'type' => 'drivers',
        ]);

        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::OFFER_FREE_ORDER_WORKER_ON_ORDER,
            'type' => 'drivers',
        ]);

        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::OFFER_FREE_ORDER_WORKER_ON_BREAK,
            'type' => 'drivers',
        ]);

        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::OFFER_FREE_ORDER_DISTANCE,
            'type' => 'drivers',
        ]);


        $this->delete(self::TABLE_TENANT_SETTING, ['name' => self::OFFER_FREE_ORDER, 'type' => self::SETTING_TYPE]);
        $this->delete(self::TABLE_TENANT_SETTING, ['name' => self::OFFER_FREE_ORDER_COUNT, 'type' => self::SETTING_TYPE]);
        $this->delete(self::TABLE_TENANT_SETTING, ['name' => self::OFFER_FREE_ORDER_WORKER_ON_ORDER, 'type' => self::SETTING_TYPE]);
        $this->delete(self::TABLE_TENANT_SETTING, ['name' => self::OFFER_FREE_ORDER_WORKER_ON_BREAK, 'type' => self::SETTING_TYPE]);
        $this->delete(self::TABLE_TENANT_SETTING, ['name' => self::OFFER_FREE_ORDER_DISTANCE, 'type' => self::SETTING_TYPE]);

    }


}
