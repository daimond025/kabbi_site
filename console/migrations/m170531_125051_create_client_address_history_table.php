<?php

use yii\db\Migration;

/**
 * Handles the creation of table `client_address_history`.
 */
class m170531_125051_create_client_address_history_table extends Migration
{

    const TABLE_NAME = '{{%client_address_history}}';
    const CLIENT_TABLE_NAME = '{{%client}}';
    const CITY_TABLE_NAME = '{{%city}}';

    const FK__CLIENT_ID = 'fk__tbl_client_address_history__client_id';
    const FK__CITY_ID = 'fk__tbl_client_address_history__city_id';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'history_id' => $this->primaryKey()->unsigned(),
            'client_id'  => $this->integer(10)->notNull()->unsigned(),
            'city_id'    => $this->integer(10)->notNull()->unsigned(),
            'address'    => $this->text()->notNull(),
            'create_time' => \yii\db\Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP'
        ]);

        $this->addForeignKey(self::FK__CLIENT_ID,
            self::TABLE_NAME, 'client_id',
            self::CLIENT_TABLE_NAME, 'client_id',
            'CASCADE', 'CASCADE');

        $this->addForeignKey(self::FK__CITY_ID,
            self::TABLE_NAME, 'city_id',
            self::CITY_TABLE_NAME, 'city_id',
            'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(self::FK__CITY_ID, self::TABLE_NAME);
        $this->dropForeignKey(self::FK__CLIENT_ID, self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
    }
}
