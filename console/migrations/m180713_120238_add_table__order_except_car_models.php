<?php

use yii\db\Migration;

class m180713_120238_add_table__order_except_car_models extends Migration
{
    const TABLE_NAME = '{{%order_except_car_models}}';
    const FK_ORDER_ID = 'fk_order_except_car_models__order_id';
    const FK_CAR_MODEL_ID = 'fk_order_except_car_models__car_model_id';
    const ORDER_ID = 'order_id';
    const CAR_MODEL_ID = 'car_model_id';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'               => $this->primaryKey()->unsigned(),
            self::ORDER_ID     => $this->integer()->unsigned(),
            self::CAR_MODEL_ID => $this->integer()->unsigned(),
            'created_at'       => $this->integer()->unsigned(),
            'updated_at'       => $this->integer()->unsigned(),
        ]);

        $this->addForeignKey(self::FK_ORDER_ID, self::TABLE_NAME, self::ORDER_ID, '{{%order}}', self::ORDER_ID, 'CASCADE', 'CASCADE');
        $this->addForeignKey(self::FK_CAR_MODEL_ID, self::TABLE_NAME, self::CAR_MODEL_ID, '{{%car_model}}', 'model_id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey(self::FK_ORDER_ID, self::TABLE_NAME);
        $this->dropForeignKey(self::FK_CAR_MODEL_ID, self::TABLE_NAME);

        $this->dropTable(self::TABLE_NAME);
    }
}
