<?php

use yii\db\Schema;
use yii\db\Migration;

class m151216_103238_tbl_order__add_column_bonus_payment extends Migration
{
    const TABLE_NAME = '{{%order}}';
    const COLUMN_NAME = 'bonus_payment';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_NAME, 'INT(1) DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
