<?php

use yii\db\Schema;
use yii\db\Migration;

class m151110_142609_tbl_account_kind extends Migration
{
    const TABLE_NAME = '{{%account_kind}}';
    const CLIENT_BONUS_ID = 7;
    const SYSTEM_BONUS_ID = 8;

    public function up()
    {
        $this->batchInsert(self::TABLE_NAME, ['kind_id', 'name'], [
            [self::CLIENT_BONUS_ID, 'CLIENT BONUS'],
            [self::SYSTEM_BONUS_ID, 'SYSTEM BONUS'],
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME, 'kind_id in (:rec1, :rec2)', [
            'rec1' => self::CLIENT_BONUS_ID,
            'rec2' => self::SYSTEM_BONUS_ID,
        ]);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
