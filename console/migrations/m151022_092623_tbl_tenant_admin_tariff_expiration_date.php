<?php

use yii\db\Schema;
use yii\db\Migration;

class m151022_092623_tbl_tenant_admin_tariff_expiration_date extends Migration
{
    const TABLE_NAME = '{{%tenant_admin_tariff_expiration_date}}';
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'tenant_id' => $this->primaryKey()->notNull(),
            'expiration_date' => $this->dateTime()->notNull(),
            'tariff_id' => $this->integer(5)->notNull(),
            'create_date' => $this->dateTime()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
