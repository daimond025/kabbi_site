<?php

use yii\db\Migration;

class m180209_110022_create_tenant_fields extends Migration
{

    const TABLE_NAME = '{{%tenant_show_field}}';

    public function safeUp()
    {
        $nameList   = $this->getNameList();
        $nameString = "'" . implode("', '", $nameList) . "'";

        $this->createTable(self::TABLE_NAME, [
            'field_id'    => $this->primaryKey(10)->unsigned(),
            'tenant_id'   => $this->integer(10)->unsigned()->notNull(),
            'city_id'     => $this->integer(11)->unsigned()->notNull(),
            'position_id' => $this->integer(10),
            'type'        => "ENUM('worker', 'car') NOT NULL",
            'name'        => "ENUM($nameString) NOT NULL",
            'value'       => $this->integer(1)->notNull()->defaultValue(1),
        ]);

        $this->addForeignKey('FK_tenant_id__tenant_show_field', self::TABLE_NAME, 'tenant_id',
            '{{%tenant}}', 'tenant_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_city_id__tenant_show_field', self::TABLE_NAME, 'city_id',
            '{{%city}}', 'city_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_position_id__tenant_show_field', self::TABLE_NAME, 'position_id',
            '{{%position}}', 'position_id', 'CASCADE', 'CASCADE');

        $this->dataFill();


    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }


    protected function dataFill()
    {
        $workerNameList         = $this->getWorkerNameList();
        $workerPositionNameList = $this->getWorkerPositionNameList();
        $carNameList            = $this->getCarNameList();
        $tenantHasCity          = $this->getTenantHasCity();
        $tenantHasPosition      = $this->getTenantHasPosition();
        $positionHasDocument    = $this->getPositionHasDocument();
        $positionHasDocument    = \yii\helpers\ArrayHelper::map($positionHasDocument, 'document_id', 'document_id',
            'position_id');

        $data = [];

        foreach ($tenantHasCity as $item) {
            foreach ($workerNameList as $name) {
                $data[] = [
                    $item['tenant_id'],
                    $item['city_id'],
                    null,
                    'worker',
                    $name,
                    1,
                ];
            }

            foreach ($carNameList as $name) {
                $data[] = [
                    $item['tenant_id'],
                    $item['city_id'],
                    null,
                    'car',
                    $name,
                    1,
                ];
            }
        }

        foreach ($tenantHasPosition as $item) {
            foreach ($workerPositionNameList as $documentId => $name) {
                $positionId = $item['position_id'];
                if (!isset($positionHasDocument[$positionId][$documentId])) {
                    continue;
                }
                $data[] = [
                    $item['tenant_id'],
                    $item['city_id'],
                    $item['position_id'],
                    'worker',
                    $name,
                    1,
                ];
            }
        }

        $insert = array_chunk($data, 100);

        //        print_r($insert); return;
        foreach ($insert as $item) {
            $this->batchInsert(self::TABLE_NAME, ['tenant_id', 'city_id', 'position_id', 'type', 'name', 'value'],
                $item);
        }
    }

    protected function getNameList()
    {
        return array_merge($this->getWorkerNameList(), $this->getWorkerPositionNameList(), $this->getCarNameList());
    }

    protected function getWorkerNameList()
    {
        return [
            'workerBirthday',
            'workerEmail',
            'workerCardNumber',
            'workerYandexAccountNumber',
            'workerDescription',
            'workerPassport',
            'workerSNILS',
            'workerINN',
            'workerOGRNIP',
        ];
    }

    protected function getWorkerPositionNameList()
    {
        return [
            6 => 'workerPositionOSAGO',
            7 => 'workerPositionDriverLicense',
            8 => 'workerPositionPTS',
            5 => 'workerPositionMedicalCertificate',
        ];
    }

    protected function getCarNameList()
    {
        return [
            'carCallsign',
            'carLicense',
        ];
    }

    protected function getTenantHasCity()
    {
        return (new \yii\db\Query())->from('{{%tenant_has_city}}')->all();
    }

    protected function getTenantHasPosition()
    {
        return (new \yii\db\Query())->from('{{%tenant_city_has_position}}')->all();
    }

    protected function getPositionHasDocument()
    {
        return (new \yii\db\Query)->from('{{%position_has_document}}')->all();
    }
}
