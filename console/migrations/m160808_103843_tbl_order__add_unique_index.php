<?php

use yii\db\Migration;

class m160808_103843_tbl_order__add_unique_index extends Migration
{
    const TABLE_ORDER = '{{%order}}';
    const UNIQUE_INDEX = 'ui_order__tenant_id__order_number';

    const DUPLICATE_ORDERS = <<<SQL
SELECT `tenant_id`, `order_number`
FROM `tbl_order`
GROUP BY `tenant_id`, `order_number`
HAVING COUNT(*) > 1
SQL;

    const GET_ORDERS = <<<SQL
SELECT `order_id`
FROM `tbl_order`
WHERE `tenant_id` = :tenantId
AND `order_number` = :orderNumber
SQL;

    const GET_FREE_ORDER_NUMBER = <<<SQL
SELECT IFNULL(MAX(`order_number`), 0) + 1 as `order_number`
FROM `tbl_order`
WHERE `tenant_id` = :tenantId
SQL;

    public function safeUp()
    {
        $rows = $this->db->createCommand(self::DUPLICATE_ORDERS)->queryAll();
        if (is_array($rows)) {
            foreach ($rows as $row) {
                $orders = $this->db->createCommand(self::GET_ORDERS, [
                    'tenantId'    => $row['tenant_id'],
                    'orderNumber' => $row['order_number'],
                ])->queryAll();

                if (is_array($orders)) {
                    for ($i = 1; $i < count($orders); $i++) {
                        $orderNumber = $this->db->createCommand(self::GET_FREE_ORDER_NUMBER, [
                            'tenantId' => $row['tenant_id'],
                        ])->queryScalar();

                        $this->update(self::TABLE_ORDER, [
                            'order_number' => $orderNumber,
                        ], [
                            'order_id' => $orders[$i]['order_id'],
                        ]);
                    }
                }
            }
        }

        $this->createIndex(self::UNIQUE_INDEX, self::TABLE_ORDER, ['tenant_id', 'order_number'], true);
    }

    public function safeDown()
    {
        $this->dropIndex(self::UNIQUE_INDEX, self::TABLE_ORDER);
    }

}
