<?php

use yii\db\Migration;

class m180422_134529_edit__client_status_event__table extends Migration
{
    const TABLE_NAME = '{{%client_status_event}}';

    public function safeUp()
    {
        $this->alterColumn(self::TABLE_NAME, 'notice', $this->string(255)->notNull()->defaultValue('nothing'));
    }

    public function safeDown()
    {
        $models     = $this->getClientStatusEvents();
        $noticeList = ['autocall', 'sms', 'push', 'nothing'];

        foreach ($models as $model) {
            $notice = $model['notice'];

            if (!in_array($notice, $noticeList)) {
                $notice = explode(',', $notice);
                $notice = array_intersect($noticeList, $notice);
                $notice = empty($notice) ? 'nothing' : reset($notice);
                $this->update(self::TABLE_NAME, ['notice' => $notice], ['event_id' => $model['event_id']]);
            }
        }

        $this->alterColumn(self::TABLE_NAME, 'notice',
            "ENUM('sms','autocall','push','nothing') NOT NULL DEFAULT 'nothing'");
    }

    private function getClientStatusEvents()
    {
        return (new \yii\db\Query())->from(self::TABLE_NAME)->all();
    }
}
