<?php

use yii\db\Schema;
use yii\db\Migration;

class m151023_120135_create_page_content_tables extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%page_content}}', [
            'id' => $this->primaryKey(),
            'page' => $this->string()->notNull(),
            'content' => $this->text(),
            'title' => $this->string()->notNull(),
            'description' => $this->string()->notNull(),
            'keywords' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->createIndex('idx_page_content__page', '{{%page_content}}', 'page', true);

        $this->createTable('{{%page_content_aud}}', [
            'id' => $this->primaryKey(),
            'page_content_id' => $this->integer()->notNull(),
            'page' => $this->string()->notNull(),
            'content' => $this->text(),
            'title' => $this->string()->notNull(),
            'description' => $this->string()->notNull(),
            'keywords' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk_page_content_aud__page_content_id', '{{%page_content_aud}}', 'page_content_id',
                '{{%page_content}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%page_content_aud}}');
        $this->dropTable('{{%page_content}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
