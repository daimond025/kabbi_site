<?php

use yii\db\Migration;

class m180411_134055_fill_worker_tariff_default_values extends Migration
{

    const TABLE_NAME = '{{%worker_tariff}}';

    public function safeUp()
    {
        $this->update(self::TABLE_NAME, ['action_new_shift' => 'CONTINUE_PERIOD'], [
            'type' => 'ONCE'
        ]);
    }

    public function safeDown()
    {
        $this->update(self::TABLE_NAME, ['action_new_shift' => null], [
            'type' => 'ONCE'
        ]);
    }

}
