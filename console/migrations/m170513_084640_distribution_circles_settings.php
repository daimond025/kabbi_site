<?php

use yii\db\Migration;
use common\modules\tenant\models\DefaultSettings;

class m170513_084640_distribution_circles_settings extends Migration
{
    public function up()
    {
        //1circle
        $d1 = new DefaultSettings([
            'name'  => 'CIRCLE_DISTANCE_1',
            'value' => '3000',
            'type'  => 'orders'
        ]);
        $d1->save();
        $r1 = new DefaultSettings([
            'name'  => 'CIRCLE_REPEAT_1',
            'value' => '2',
            'type'  => 'orders'
        ]);
        $r1->save();

        //2cirlce
        $d2 = new DefaultSettings([
            'name'  => 'CIRCLE_DISTANCE_2',
            'value' => null,
            'type'  => 'orders'
        ]);
        $d2->save();
        $r2 = new DefaultSettings([
            'name'  => 'CIRCLE_REPEAT_2',
            'value' => null,
            'type'  => 'orders'
        ]);
        $r2->save();

        //3cirlce
        $d3 = new DefaultSettings([
            'name'  => 'CIRCLE_DISTANCE_3',
            'value' => null,
            'type'  => 'orders'
        ]);
        $d3->save();
        $r3 = new DefaultSettings([
            'name'  => 'CIRCLE_REPEAT_3',
            'value' => null,
            'type'  => 'orders'
        ]);
        $r3->save();

        //4cirlce
        $d4 = new DefaultSettings([
            'name'  => 'CIRCLE_DISTANCE_4',
            'value' => null,
            'type'  => 'orders'
        ]);
        $d4->save();
        $r4 = new DefaultSettings([
            'name'  => 'CIRCLE_REPEAT_4',
            'value' => null,
            'type'  => 'orders'
        ]);
        $r4->save();

        //5cirlce
        $d5 = new DefaultSettings([
            'name'  => 'CIRCLE_DISTANCE_5',
            'value' => null,
            'type'  => 'orders'
        ]);
        $d5->save();
        $r5 = new DefaultSettings([
            'name'  => 'CIRCLE_REPEAT_5',
            'value' => null,
            'type'  => 'orders'
        ]);
        $r5->save();

    }

    public function down()
    {
        return true;
    }

}
