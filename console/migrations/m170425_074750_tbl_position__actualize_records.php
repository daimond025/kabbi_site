<?php

use yii\db\Migration;

class m170425_074750_tbl_position__actualize_records extends Migration
{
    const TABLE_POSITION = '{{%position}}';

    public function up()
    {
        $this->update(self::TABLE_POSITION, [
            'name' => 'Courier on foot',
        ], ['position_id' => 3]);
        $this->batchInsert(self::TABLE_POSITION,
            ['position_id', 'name', 'module_id', 'has_car'],
            [
                [5, 'Courier on moto', 3, 0],
                [6, 'Courier on auto', 3, 0],
            ]);
    }

    public function down()
    {
    }

}
