<?php

use yii\db\Migration;

class m160628_072121_delete_index_tbl_default_sms_template extends Migration
{
    public function up()
    {
        $this->dropIndex('type_UNIQUE', '{{%default_sms_template}}');
    }

    public function down()
    {
        echo "m160628_072121_delete_index_tbl_default_sms_template cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
