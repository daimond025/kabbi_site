<?php

use yii\db\Migration;
use common\modules\tenant\models\DefaultSettings;

class m180116_101134_delete_records__settings extends Migration
{
    const TABLE_NAME__TENANT_SETTING = '{{%tenant_setting}}';
    const TABLE_NAME__DEFAULT_SETTING = '{{%default_settings}}';

    const SETTING_NAME__WORKER_APP_ANDROID_VERSION = 'WORKER_APP_ANDROID_VERSION';
    const SETTING_NAME__WORKER_APP_IOS_VERSION = 'WORKER_APP_IOS_VERSION';

    const DEFAULT_VALUE = '1';
    const TYPE_SETTING = 'system';

    public function safeUp()
    {
        $this->delete(self::TABLE_NAME__DEFAULT_SETTING, [
            'name' => [
                self::SETTING_NAME__WORKER_APP_ANDROID_VERSION,
                self::SETTING_NAME__WORKER_APP_IOS_VERSION
            ]
        ]);

        $this->delete(self::TABLE_NAME__TENANT_SETTING, [
            'name' => [
                self::SETTING_NAME__WORKER_APP_ANDROID_VERSION,
                self::SETTING_NAME__WORKER_APP_IOS_VERSION
            ]
        ]);
    }

    public function safeDown()
    {
        $workerAppAndroidVersion = new DefaultSettings([
            'name'  => self::SETTING_NAME__WORKER_APP_ANDROID_VERSION,
            'value' => self::DEFAULT_VALUE,
            'type'  => self::TYPE_SETTING,
        ]);
        $workerAppAndroidVersion->save();

        $workerAppIosVersion = new DefaultSettings([
            'name'  => self::SETTING_NAME__WORKER_APP_IOS_VERSION,
            'value' => self::DEFAULT_VALUE,
            'type'  => self::TYPE_SETTING,
        ]);
        $workerAppIosVersion->save();
    }
}
