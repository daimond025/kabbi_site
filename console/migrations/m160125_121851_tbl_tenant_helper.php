<?php

use yii\db\Schema;
use yii\db\Migration;

class m160125_121851_tbl_tenant_helper extends Migration
{
    public function up()
    {
        $this->addColumn('{{%tenant_helper}}', 'city', $this->boolean()->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('{{%tenant_helper}}', 'city');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
