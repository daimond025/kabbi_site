<?php

use yii\db\Migration;

/**
 * Handles the creation of table `mobile_app_demo`.
 */
class m170801_094241_create_mobile_app_demo_table extends Migration
{
    const TABLE_NAME = '{{%mobile_app_demo}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'demo_id'             => $this->primaryKey(),
            'name'                => $this->string(100)->notNull(),
            'sort'                => $this->integer(6)->notNull()->defaultValue(1),
            'active'              => $this->integer(1)->notNull()->defaultValue(1),
            'api_key'             => $this->string(250),
            'push_key_android'    => $this->text(),
            'push_key_ios'        => $this->text(),
            'link_android'        => $this->string(250),
            'link_ios'            => $this->string(250),
            'page_info'           => $this->text(),
            'confidential'        => $this->text(),
            'city_autocompletion' => $this->integer(1)->notNull()->defaultValue(1),
        ]);

        $this->insert(self::TABLE_NAME, [
            'name'             => 'Demo',
            'api_key'          => 'jdshgjksahgjkasdhwehghkajsdgn',
            'push_key_android' => 'AIzaSyDtErekKTTCd7kDMPSQmMMntUvqyRJMI_Q',
            'push_key_ios'     => 'Bag+Attributes%0D%0A++++friendlyName%3A+Apple+Production+IOS+Push+Services%3A+com.gootax.client%0D%0A++++localKeyID%3A+0A+42+4C+0D+C5+C4+7F+C3+AA+F2+89+86+8C+46+54+8D+86+EA+09+71%0D%0Asubject%3D%2FUID%3Dcom.gootax.client%2FCN%3DApple+Production+IOS+Push+Services%3A+com.gootax.client%2FOU%3DG967HA9U2Z%2FC%3DUS%0D%0Aissuer%3D%2FC%3DUS%2FO%3DApple+Inc.%2FOU%3DApple+Worldwide+Developer+Relations%2FCN%3DApple+Worldwide+Developer+Relations+Certification+Authority%0D%0A-----BEGIN+CERTIFICATE-----%0D%0AMIIFhjCCBG6gAwIBAgIIa3cPCNoi6fkwDQYJKoZIhvcNAQEFBQAwgZYxCzAJBgNV%0D%0ABAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3Js%0D%0AZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3%0D%0AaWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkw%0D%0AHhcNMTUwNjA1MTIxNzMyWhcNMTYwNjA0MTIxNzMyWjCBhTEhMB8GCgmSJomT8ixk%0D%0AAQEMEWNvbS5nb290YXguY2xpZW50MT4wPAYDVQQDDDVBcHBsZSBQcm9kdWN0aW9u%0D%0AIElPUyBQdXNoIFNlcnZpY2VzOiBjb20uZ29vdGF4LmNsaWVudDETMBEGA1UECwwK%0D%0ARzk2N0hBOVUyWjELMAkGA1UEBhMCVVMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAw%0D%0AggEKAoIBAQDhkMhvSGYRQ0gJIUXwXTRGE8qmJdonh%2F6TyaNEsJ0Ao6yKBEtsipu6%0D%0A6C1YBFVC4eL7qc4ZShldUNTmc6URUCxwc7eej%2FJhWl4kxtPpyi1x4uRigBy%2FXrWc%0D%0ADxGG5j0b9UrlbG0%2FmuCUKusuoHXLmk2jYhzJiTGwV9Of931GiiOnnvLoFbQop5IP%0D%0Aof2TFOqFQ4x%2FxateL%2B3Bnm7q%2FABJ0VjvL97CEOb88rE7VfIIRBz0LZgaJu%2FQx7VB%0D%0A6cq1a6eu1v4ezFJHE8cg7jHWHO1WlVk0HC3vNzwuBz6bo73z0LrG2gQn9lZp3WrC%0D%0AHrMVl5VicrBRotLI37qEOWnFiEu9FPw%2FAgMBAAGjggHlMIIB4TAdBgNVHQ4EFgQU%0D%0ACkJMDcXEf8Oq8omGjEZUjYbqCXEwCQYDVR0TBAIwADAfBgNVHSMEGDAWgBSIJxcJ%0D%0AqbYYYIvs67r2R1nFUlSjtzCCAQ8GA1UdIASCAQYwggECMIH%2FBgkqhkiG92NkBQEw%0D%0AgfEwgcMGCCsGAQUFBwICMIG2DIGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0%0D%0AZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFw%0D%0AcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBj%0D%0AZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3Rh%0D%0AdGVtZW50cy4wKQYIKwYBBQUHAgEWHWh0dHA6Ly93d3cuYXBwbGUuY29tL2FwcGxl%0D%0AY2EvME0GA1UdHwRGMEQwQqBAoD6GPGh0dHA6Ly9kZXZlbG9wZXIuYXBwbGUuY29t%0D%0AL2NlcnRpZmljYXRpb25hdXRob3JpdHkvd3dkcmNhLmNybDALBgNVHQ8EBAMCB4Aw%0D%0AEwYDVR0lBAwwCgYIKwYBBQUHAwIwEAYKKoZIhvdjZAYDAgQCBQAwDQYJKoZIhvcN%0D%0AAQEFBQADggEBAMCKeeAKgD5vZ6ICMxpMoToBq2rrirPiCbhXHD2kHO76L0acm8TX%0D%0ALoMlEWvobME8kKVeRJx75Y3rahQ3EcYxonhlwohuv1tXsrQoRfzg6rVGZns30vv5%0D%0AVsPykyJjOFvSqDb4gnfD%2FY9bWKJPsyRv%2Fe1INKOJGk9k9L9OOAaZzuSJeAO2r2oD%0D%0AAJf%2Bkz%2BFAzHueK3wqBDUOzslswzPfAsj0vf6qjVLX2mtFbx1bx14T%2BEf8f%2BWPxdz%0D%0AnIRrEvk%2FOlIvUWBjDwRDBFgwoWTQiD1uX6xj4wazmBVxkVusZJF9iL1cKnTI6Wko%0D%0AwC1Hnp3S8Ab8%2FjfJNS3QUalohg4kLgDnkD8%3D%0D%0A-----END+CERTIFICATE-----%0D%0ABag+Attributes%0D%0A++++friendlyName%3A+Dmitry+Kudrin%0D%0A++++localKeyID%3A+0A+42+4C+0D+C5+C4+7F+C3+AA+F2+89+86+8C+46+54+8D+86+EA+09+71%0D%0AKey+Attributes%3A+%3CNo+Attributes%3E%0D%0A-----BEGIN+RSA+PRIVATE+KEY-----%0D%0AMIIEowIBAAKCAQEA4ZDIb0hmEUNICSFF8F00RhPKpiXaJ4f%2Bk8mjRLCdAKOsigRL%0D%0AbIqbuugtWARVQuHi%2B6nOGUoZXVDU5nOlEVAscHO3no%2FyYVpeJMbT6cotceLkYoAc%0D%0Av161nA8RhuY9G%2FVK5WxtP5rglCrrLqB1y5pNo2IcyYkxsFfTn%2Fd9Roojp57y6BW0%0D%0AKKeSD6H9kxTqhUOMf8WrXi%2FtwZ5u6vwASdFY7y%2FewhDm%2FPKxO1XyCEQc9C2YGibv%0D%0A0Me1QenKtWunrtb%2BHsxSRxPHIO4x1hztVpVZNBwt7zc8Lgc%2Bm6O989C6xtoEJ%2FZW%0D%0Aad1qwh6zFZeVYnKwUaLSyN%2B6hDlpxYhLvRT8PwIDAQABAoIBABZ4tw8nFkwH%2BOg3%0D%0A8WDF2ExW8mxk%2FvvLN5h%2FkQ1pzupeY%2FUIAn%2BYu3T%2BZm4LWHcmS9eojgjE8gvy3pLq%0D%0A2HoGE7dMP%2FHDA3GhcBmX2UrEQozC%2Ft%2B1RafnBo66jZNz4oijgkei6qtYgaEhOXbW%0D%0As4WKJMvvHVESoumfhky8ejWJ042CP2liLjJ5vIUssBuVtHNKQq1ZsFz3turhPjFE%0D%0AO7IuaLGZ%2F6LXDFgk7rXpp7PnAkEWQ0hoiuxL%2FNXalmYXJHGf%2Bvd4vKly2je2bzV%2F%0D%0AZ452j5Fzih4QGvHovb3MoiS8RWz2Rfhm%2BAvBzOe0re9JhU%2BOC%2FFYUBSbqOzcvfdz%0D%0AQ7gN85kCgYEA9qnEcTprfq3zuTa1tJxUJl7C899l9wF8TqMaPu9M%2FGvnkQUmZ11X%0D%0A2zqbCrVl0wpyeOw1ajQkOHiDvga9BnR1D5e%2F6%2F%2FdbtORt2W4%2FkWzHkwunIJLXfhD%0D%0AyP7WNZxD79NhuX5s56ulTpLBVfexCnYTXddnRZ%2BDN1UesqCja86TId0CgYEA6hqS%0D%0A%2Fkm2pTSy9gxkOmFH74jw7UPkl3ylq%2FcDym8ZFq4Sf6iACXomE0EjI1T1LlhILwh3%0D%0AQM4f7wzIktTP29ECbtfU%2FbLcpXHXAvhEXjD96TFI%2B5ToEHnsGCeXJqf98hK7V8Vg%0D%0AyzXQkhGU5hPYU0gl8FwH%2BVgcB%2BNnTHJxFPhIissCgYEA8zNVzbIlFKUcniw0IhzU%0D%0A13QtUdidJ1qensYv9YdqZ6NYkWL7h4TAQBZbkcZi3B772QBEdi9s2wTwGCd6rmea%0D%0Al8IuZ9lCEaimjqXMLWC5h5DjkErjVoaXBxFEUV1%2BmnLXctYVOKLDfWIzH7j1JPRg%0D%0AifD9EOeCr8bMWqEKIBxwcz0CgYBXTivNq73y5LFtM3fZkdgbC%2B4V%2FkDtAe5jMI74%0D%0AWvB3VdnORA%2Fj%2BOrkOWGaVp%2FNdd0NtVGMGJMuOo0%2BvSD7sHoAzns%2FVgoVpVvzXpVY%0D%0Ar1qyMdAvCvx00JNs%2Bq7TGsmJEtVNSW3suxgasXXfTD1b6cdEWX4bznE9u%2BAVUYS8%0D%0Atk%2BPewKBgEkEUOclmrvhTfzi%2F%2F9UCJWTXg98xIY%2B3Zpj1d3IAVrf9c8%2B7HCzqmJx%0D%0AZhoQPaXAG5IAEOFMBcVQ0dDAK90PzNFHSkDt4E2a%2B8W%2Fml%2BIUeC9bbRLkh1tPC6f%0D%0AbInD8FntsQhcK5pe8%2BG%2F7%2B8jsunStGHaiX68TlEYCjAlaZ%2B7CPb9%0D%0A-----END+RSA+PRIVATE+KEY-----',
            'link_android'     => 'https://play.google.com/store/apps/details?id=com.gootax.client',
            'link_ios'         => 'https://itunes.apple.com/us/app/gutaks-zakaz-taksi/id998929451?l=ru&ls=1&mt=8',
            'page_info'        => 'This is Page info',
            'confidential'     => 'This is confidential',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
