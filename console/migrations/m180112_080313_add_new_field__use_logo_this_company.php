<?php

use yii\db\Migration;

class m180112_080313_add_new_field__use_logo_this_company extends Migration
{
    const TABLE_NAME = '{{%tenant_company}}';
    const COLUMN_NAME = 'use_logo_company';


    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_NAME, $this->integer()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);
    }
}
