<?php

use yii\db\Schema;
use yii\db\Migration;

class m150530_051844_tbl_deafult_settings extends Migration
{
    public function up()
    {
        $this->update('{{%default_settings}}', ['value' => 1], ['name' => 'TYPE_OF_DISTRIBUTION_ORDER']);
        $this->update('{{%default_settings}}', ['value' => 3000], ['name' => 'DISTANCE_FOR_SEARCH']);
        $this->update('{{%default_settings}}', ['value' => 300], ['name' => 'PICK_UP']);
        $this->update('{{%default_settings}}', ['value' => 'IN_ROW'], ['name' => 'DRIVER_TYPE_BLOCK']);
        $this->update('{{%default_settings}}', ['value' => 3], ['name' => 'DRIVER_COUNT_TO_BLOCK']);
        $this->update('{{%default_settings}}', ['value' => 30], ['name' => 'DRIVER_TIME_OF_BLOCK']);
        $this->update('{{%default_settings}}', ['value' => 30], ['name' => 'PRE_ORDER']);
        $this->update('{{%default_settings}}', ['value' => serialize([30, 20, 10])], ['name' => 'DRIVER_PRE_ORDER_REMINDER']);
        $this->update('{{%default_settings}}', ['value' => 30], ['name' => 'DRIVER_PRE_ORDER_REJECT_MIN']);
        $this->update('{{%default_settings}}', ['value' => 24], ['name' => 'DRIVER_PRE_ORDER_BLOCK_HOUER']);
        $this->update('{{%default_settings}}', ['value' => 5], ['name' => 'DRIVER_LATE_TIME_NOTIFICATION']);
        $this->update('{{%default_settings}}', ['value' => 3], ['name' => 'CALL_COUNT_TRY']);
        $this->update('{{%default_settings}}', ['value' => 5], ['name' => 'CALL_WAIT_TIMEOUT']);
    }

    public function down()
    {
        $this->update('{{%default_settings}}', ['value' => null], ['name' => 'TYPE_OF_DISTRIBUTION_ORDER']);
        $this->update('{{%default_settings}}', ['value' => null], ['name' => 'DISTANCE_FOR_SEARCH']);
        $this->update('{{%default_settings}}', ['value' => null], ['name' => 'PICK_UP']);
        $this->update('{{%default_settings}}', ['value' => null], ['name' => 'DRIVER_TYPE_BLOCK']);
        $this->update('{{%default_settings}}', ['value' => null], ['name' => 'DRIVER_COUNT_TO_BLOCK']);
        $this->update('{{%default_settings}}', ['value' => null], ['name' => 'DRIVER_TIME_OF_BLOCK']);
        $this->update('{{%default_settings}}', ['value' => null], ['name' => 'PRE_ORDER']);
        $this->update('{{%default_settings}}', ['value' => null], ['name' => 'DRIVER_PRE_ORDER_REMINDER']);
        $this->update('{{%default_settings}}', ['value' => null], ['name' => 'DRIVER_PRE_ORDER_REJECT_MIN']);
        $this->update('{{%default_settings}}', ['value' => null], ['name' => 'DRIVER_PRE_ORDER_BLOCK_HOUER']);
        $this->update('{{%default_settings}}', ['value' => null], ['name' => 'DRIVER_LATE_TIME_NOTIFICATION']);
        $this->update('{{%default_settings}}', ['value' => null], ['name' => 'CALL_COUNT_TRY']);
        $this->update('{{%default_settings}}', ['value' => null], ['name' => 'CALL_WAIT_TIMEOUT']);
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
