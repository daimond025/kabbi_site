<?php

use yii\db\Migration;

class m170822_130217_create_executor_push_notifications_tables extends Migration
{
    const TABLE_NOTIFICATIONS = '{{%worker_push_notifications}}';
    const TABLE_DEFAULT_NOTIFICATIONS = '{{%default_worker_push_notifications}}';

    public function safeUp()
    {

        $columns = $columnsByDefault = [
            'push_id'     => $this->primaryKey(),
            'text'        => $this->string(255)->notNull(),
            'type'        => $this->string(255)->notNull(),
            'tenant_id'   => $this->integer(10)->unsigned()->notNull(),
            'city_id'     => $this->integer(11)->unsigned()->notNull(),
            'position_id' => $this->integer(11)->notNull(),
        ];

        $columnsByDefault = $columnsByDefault = [
            'push_id'     => $this->primaryKey(),
            'text'        => $this->string(255)->notNull(),
            'type'        => $this->string(255)->notNull(),
            'params'      => $this->text(),
            'position_id' => $this->integer(11)->notNull(),
        ];


        $this->createTable(self::TABLE_NOTIFICATIONS, $columns);
        $this->createTable(self::TABLE_DEFAULT_NOTIFICATIONS, $columnsByDefault);

        $this->addForeignKey('FK_tenant_id_worker_push_notifications', self::TABLE_NOTIFICATIONS, 'tenant_id',
            '{{%tenant}}', 'tenant_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_city_id_worker_push_notifications', self::TABLE_NOTIFICATIONS, 'city_id', '{{%city}}',
            'city_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_position_id_worker_push_notifications', self::TABLE_NOTIFICATIONS, 'position_id',
            '{{%position}}', 'position_id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('FK_position_id_default_worker_push_notifications', self::TABLE_DEFAULT_NOTIFICATIONS,
            'position_id', '{{%position}}', 'position_id', 'CASCADE', 'CASCADE');


        $this->dataDefaultFill();
        $this->dataTenantFill();
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_position_id_default_worker_push_notifications', self::TABLE_DEFAULT_NOTIFICATIONS);

        $this->dropForeignKey('FK_position_id_worker_push_notifications', self::TABLE_NOTIFICATIONS);
        $this->dropForeignKey('FK_city_id_worker_push_notifications', self::TABLE_NOTIFICATIONS);
        $this->dropForeignKey('FK_tenant_id_worker_push_notifications', self::TABLE_NOTIFICATIONS);

        $this->dropTable(self::TABLE_DEFAULT_NOTIFICATIONS);
        $this->dropTable(self::TABLE_NOTIFICATIONS);
    }

    protected function dataDefaultFill()
    {
        $positions = (array)$this->getPositionIds();
        $keys      = ['text', 'type', 'params', 'position_id'];
        $data      = [];
        foreach ($positions as $position) {
            $data[] = [
                'text'        => 'New order for #PRICE# #CURRENCY#. Address: #ADDRESS#',
                'type'        => 1,
                'params'      => '#ADDRESS#; #DATE#; #TIME#; #TARIFF#; #PRICE#; #CURRENCY#',
                'position_id' => $position,
            ];

            $data[] = [
                'text'        => 'New pre-order for #PRICE# #CURRENCY#. Address: #ADDRESS#. Time: #DATE#',
                'type'        => 6,
                'params'      => '#ADDRESS#; #DATE#; #TIME#; #TARIFF#; #PRICE#; #CURRENCY#',
                'position_id' => $position,
            ];
        }

        if (!empty($data)) {
            $this->batchInsert(self::TABLE_DEFAULT_NOTIFICATIONS, $keys, $data);
        }
    }

    protected function dataTenantFill()
    {
        $keys  = ['text', 'type', 'tenant_id', 'city_id', 'position_id'];
        $data  = [];
        $array = $this->getTenantCityHasPosition();

        foreach ($array as $item) {
            $data[] = [
                'text'        => 'New order for #PRICE# #CURRENCY#. Address: #ADDRESS#',
                'type'        => 1,
                'tenant_id'   => $item['tenant_id'],
                'city_id'     => $item['city_id'],
                'position_id' => $item['position_id'],
            ];

            $data[] = [
                'text'        => 'New pre-order for #PRICE# #CURRENCY#. Address: #ADDRESS#. Time: #DATE#',
                'type'        => 6,
                'tenant_id'   => $item['tenant_id'],
                'city_id'     => $item['city_id'],
                'position_id' => $item['position_id'],
            ];
        }

        if (!empty($data)) {
            $this->batchInsert(self::TABLE_NOTIFICATIONS, $keys, $data);
        }
    }


    protected function getPositionIds()
    {
        return (new \yii\db\Query())->from('{{%position}}')->select('position_id')->column();
    }

    protected function getTenantCityHasPosition()
    {
        return (new \yii\db\Query())->from('{{%tenant_city_has_position}}')->all();
    }
}
