<?php

use yii\db\Schema;
use yii\db\Migration;

class m160113_184418_tbl_account__add_unique_index extends Migration
{
    const TABLE_NAME = '{{%account}}';
    const INDEX_NAME = 'iu_tbl_account';

    public function up()
    {
        $this->createIndex(self::INDEX_NAME, self::TABLE_NAME,
                'owner_id, acc_kind_id, acc_type_id, tenant_id, currency_id', true);
    }

    public function down()
    {
        $this->dropIndex(self::INDEX_NAME, self::TABLE_NAME);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
