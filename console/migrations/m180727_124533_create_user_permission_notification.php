<?php

use yii\db\Migration;
use yii\helpers\ArrayHelper;

class m180727_124533_create_user_permission_notification extends Migration
{
    const NAME = 'notifications';

    const TABLE_DEFAULT_RIGHT = '{{%user_default_right}}';

    public function init()
    {
        parent::init();
    }

    public function safeUp()
    {
        $rules = [
//            1 => 'write',
//            2 => 'write',
//            3 => 'write',
//            4 => 'write',
//            5 => 'write',
            7 => 'write',
        ];

        $this->createDefaultRights(self::NAME, $rules);


    }

    public function safeDown()
    {

    }


    private function createDefaultRights($permission, $rules)
    {
        $positions    = (new \yii\db\Query())->from('{{%user_position}}')
            ->select('position_id')
            ->indexBy('position_id')
            ->all();
        $positionsArr = array_keys($positions);
        $rulesKeys    = array_keys($rules);

        $defaultRights = $this->getDefaultRights('notifications');
        $positions = ArrayHelper::map($defaultRights, 'id', 'position_id');

        foreach ($positionsArr as $position) {
            if (ArrayHelper::isIn($position, $positions)) {
                continue;
            }

            $rights = in_array($position, $rulesKeys) ? $rules[$position] : 'off';
            $this->insert(self::TABLE_DEFAULT_RIGHT, [
                'position_id' => $position,
                'permission'  => $permission,
                'rights'      => $rights,
            ]);
        }
    }

    private function getDefaultRights($permission)
    {
        return (new \yii\db\Query())->from(self::TABLE_DEFAULT_RIGHT)->where(['permission' => $permission])->all();
    }
}
