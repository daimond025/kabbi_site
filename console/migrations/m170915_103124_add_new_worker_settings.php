<?php

use yii\db\Migration;
use common\modules\tenant\models\DefaultSettings;

class m170915_103124_add_new_worker_settings extends Migration
{

    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const TABLE_TENANT_SETTING = '{{%tenant_setting}}';

    const SETTING_NAME_PRINT_CHECK = 'PRINT_CHECK';
    const SETTING_VALUE_PRINT_CHECK = '0';
    const SETTING_TYPE_PRINT_CHECK = 'drivers';

    public function safeUp()
    {
        $printCheck = new DefaultSettings([
            'name'  => self::SETTING_NAME_PRINT_CHECK,
            'value' => self::SETTING_VALUE_PRINT_CHECK,
            'type'  => self::SETTING_TYPE_PRINT_CHECK,
        ]);
        $printCheck->save();

    }

    public function safeDown()
    {
        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::SETTING_NAME_PRINT_CHECK,
            'type' => self::SETTING_TYPE_PRINT_CHECK,
        ]);

        $this->delete(self::TABLE_TENANT_SETTING, [
            'name' => self::SETTING_NAME_PRINT_CHECK,
            'type' => self::SETTING_TYPE_PRINT_CHECK,
        ]);
    }


}
