<?php

use yii\db\Migration;

class m160824_065012_add_column_to_tbl_user extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'access_token', $this->string());
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'access_token');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
