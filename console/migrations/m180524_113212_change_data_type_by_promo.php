<?php

use yii\db\Migration;

class m180524_113212_change_data_type_by_promo extends Migration
{

    const TABLE_NAME = '{{%promo}}';

    const COLUMN_CREATE = 'created_at';
    const COLUMN_UPDATE = 'updated_at';

    public function safeUp()
    {
        $this->alterColumn(self::TABLE_NAME, self::COLUMN_CREATE, $this->integer()->unsigned()->notNull());
        $this->alterColumn(self::TABLE_NAME, self::COLUMN_UPDATE, $this->integer()->unsigned()->notNull());
    }

    public function safeDown()
    {
        $this->alterColumn(self::TABLE_NAME, self::COLUMN_CREATE, 'tinyint UNSIGNED NOT NULL');
        $this->alterColumn(self::TABLE_NAME, self::COLUMN_UPDATE, 'tinyint UNSIGNED NOT NULL');
    }

}
