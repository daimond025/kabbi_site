<?php

use yii\db\Schema;
use yii\db\Migration;

class m150902_053550_tbl_default_settings extends Migration
{
    public function up()
    {
        $this->insert('{{%default_settings}}', ['name' => 'GOOGLE_PLAY_DRIVER_APP_LINK', 'type' => 'system']);
        $this->insert('{{%default_settings}}', ['name' => 'APP_STORE_DRIVER_APP_LINK', 'type' => 'system']);
        $this->insert('{{%default_settings}}', ['name' => 'GOOGLE_PLAY_CLIENT_APP_LINK', 'type' => 'system']);
        $this->insert('{{%default_settings}}', ['name' => 'APP_STORE_CLIENT_APP_LINK', 'type' => 'system']);
        $this->insert('{{%default_settings}}', ['name' => 'SOFTPHONE', 'type' => 'system']);
    }

    public function down()
    {
        $this->delete('{{%default_settings}}', ['name' => 'GOOGLE_PLAY_DRIVER_APP_LINK']);
        $this->delete('{{%default_settings}}', ['name' => 'APP_STORE_DRIVER_APP_LINK']);
        $this->delete('{{%default_settings}}', ['name' => 'GOOGLE_PLAY_CLIENT_APP_LINK']);
        $this->delete('{{%default_settings}}', ['name' => 'APP_STORE_CLIENT_APP_LINK']);
        $this->delete('{{%default_settings}}', ['name' => 'SOFTPHONE']);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
