<?php

use yii\db\Migration;

class m160331_102255_tbl_tariff__add_columns extends Migration
{
    const TABLE_TARIFF = '{{%tariff}}';
    const TABLE_CURRENCY = '{{%currency}}';
    const COLUMN_PRICE = 'price';
    const COLUMN_CURRENCY_ID = 'currency_id';
    const FK_NAME = 'fk_tbl_tariff__currency_id';

    public function up()
    {
        $this->addColumn(self::TABLE_TARIFF, self::COLUMN_PRICE, $this->money(12, 2)->notNull());
        $this->addColumn(self::TABLE_TARIFF, self::COLUMN_CURRENCY_ID, $this->integer()->notNull());
        $this->addForeignKey(self::FK_NAME, self::TABLE_TARIFF, self::COLUMN_CURRENCY_ID,
            self::TABLE_CURRENCY,
            self::COLUMN_CURRENCY_ID, 'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_TARIFF, self::COLUMN_PRICE);
        $this->dropForeignKey(self::FK_NAME, self::TABLE_TARIFF);
        $this->dropColumn(self::TABLE_TARIFF, self::COLUMN_CURRENCY_ID);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
