<?php

use yii\db\Migration;

class m160303_052050_tbl_tenant_setting extends Migration
{
    const TABLE_NAME = '{{%tenant_setting}}';

    public function up()
    {
        $this->update(self::TABLE_NAME, ['value' => 'ru'], ['name' => 'LANGUAGE', 'value' => 'ru-RU']);
        $this->update(self::TABLE_NAME, ['value' => 'az'], ['name' => 'LANGUAGE', 'value' => 'az-AZ']);
        $this->update(self::TABLE_NAME, ['value' => 'de'], ['name' => 'LANGUAGE', 'value' => 'de-EU']);
        $this->update(self::TABLE_NAME, ['value' => 'sr'], ['name' => 'LANGUAGE', 'value' => 'se-RS']);
    }

    public function down()
    {
        echo "m160303_052050_tbl_tenant_setting cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
