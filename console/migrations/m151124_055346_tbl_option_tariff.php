<?php

use yii\db\Schema;
use yii\db\Migration;

class m151124_055346_tbl_option_tariff extends Migration
{
    public function up()
    {
        $this->addColumn('{{%option_tariff}}', 'planting_include_day_time', Schema::TYPE_INTEGER . ' UNSIGNED DEFAULT 0');
        $this->addColumn('{{%option_tariff}}', 'planting_include_night_time', Schema::TYPE_INTEGER . ' UNSIGNED DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('{{%option_tariff}}', 'planting_include_day_time');
        $this->dropColumn('{{%option_tariff}}', 'planting_include_night_time');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
