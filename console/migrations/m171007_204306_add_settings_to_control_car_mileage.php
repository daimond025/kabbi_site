<?php

use yii\db\Migration;

class m171007_204306_add_settings_to_control_car_mileage extends Migration
{
    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const TABLE_TENANT_SETTING = '{{%tenant_setting}}';

    const SETTING_NAME__CONTROL_OWN_CAR_MILEAGE = 'CONTROL_OWN_CAR_MILEAGE';
    const SETTING_VALUE__CONTROL_OWN_CAR_MILEAGE = '0';
    const SETTING_TYPE__CONTROL_OWN_CAR_MILEAGE = 'drivers';

    const SQL_ADD_SETTINGS_WITH_CITY_AND_POSITION = <<<SQL
INSERT INTO `tbl_tenant_setting`
            (`tenant_id`, `city_id`, `position_id`, `name`, `value`, `type`)
SELECT `p`.`tenant_id`, `p`.`city_id`, `p`.`position_id`, :name `name`, :value `value`, :type `type`
FROM `tbl_tenant_has_city` AS `c`
JOIN `tbl_tenant_city_has_position` AS `p` ON `c`.`tenant_id` = `p`.`tenant_id` AND `c`.`city_id` = `p`.`city_id`
LEFT JOIN `tbl_tenant_setting` AS `t` ON `p`.`tenant_id` = `t`.`tenant_id` AND `p`.`city_id` = `t`.`city_id` AND `p`.`position_id` = `t`.`position_id` AND `t`.`name` = :name
WHERE `t`.`setting_id` IS NULL
SQL;

    public function safeUp()
    {
        $this->insert(self::TABLE_DEFAULT_SETTINGS, [
            'name'  => self::SETTING_NAME__CONTROL_OWN_CAR_MILEAGE,
            'value' => self::SETTING_VALUE__CONTROL_OWN_CAR_MILEAGE,
            'type'  => self::SETTING_TYPE__CONTROL_OWN_CAR_MILEAGE,
        ]);

        $this->getDb()->createCommand(self::SQL_ADD_SETTINGS_WITH_CITY_AND_POSITION, [
            'name'  => self::SETTING_NAME__CONTROL_OWN_CAR_MILEAGE,
            'value' => self::SETTING_VALUE__CONTROL_OWN_CAR_MILEAGE,
            'type'  => self::SETTING_TYPE__CONTROL_OWN_CAR_MILEAGE,
        ])->execute();
    }

    public function safeDown()
    {
        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::SETTING_NAME__CONTROL_OWN_CAR_MILEAGE,
            'type' => self::SETTING_TYPE__CONTROL_OWN_CAR_MILEAGE,
        ]);

        $this->delete(self::TABLE_TENANT_SETTING, [
            'name' => self::SETTING_NAME__CONTROL_OWN_CAR_MILEAGE,
            'type' => self::SETTING_TYPE__CONTROL_OWN_CAR_MILEAGE,
        ]);
    }

}
