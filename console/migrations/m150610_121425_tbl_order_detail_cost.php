<?php

use yii\db\Schema;
use yii\db\Migration;

class m150610_121425_tbl_order_detail_cost extends Migration
{
    public function up()
    {
        $this->addColumn('{{%order_detail_cost}}', 'city_wait_cost', Schema::TYPE_STRING);
        $this->addColumn('{{%order_detail_cost}}', 'out_wait_cost', Schema::TYPE_STRING);
        $this->addColumn('{{%order_detail_cost}}', 'distance_for_plant_cost', Schema::TYPE_STRING);
    }

    public function down()
    {
        $this->dropColumn('{{%order_detail_cost}}', 'city_wait_cost');
        $this->dropColumn('{{%order_detail_cost}}', 'out_wait_cost');
        $this->dropColumn('{{%order_detail_cost}}', 'distance_for_plant_cost');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
