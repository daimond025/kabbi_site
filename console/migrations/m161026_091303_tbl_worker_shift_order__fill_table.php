<?php

use yii\db\Migration;

class m161026_091303_tbl_worker_shift_order__fill_table extends Migration
{
    /**
     * Getting worker orders
     * @return array
     */
    private function getWorkerOrders()
    {
        $query = new \yii\db\Query();

        return $query
            ->select(['o.order_id', 'o.worker_id', 'o.status_time'])
            ->from('tbl_order as o')
            ->innerJoin('tbl_order_status as s', 'o.status_id = s.status_id')
            ->where([
                'and',
                ['is not', 'o.worker_id', null],
                ['s.status_group' => ['completed', 'rejected']],
            ])
            ->all();
    }

    /**
     * Getting id of last shift
     *
     * @param int $workerId
     * @param int $orderTime
     * @param int $now
     *
     * @return bool|string
     */
    private function getLastShiftId($workerId, $orderTime, $now)
    {
        $query = new \yii\db\Query();

        return $query
            ->select(['id'])
            ->from('tbl_worker_shift')
            ->where(['worker_id' => $workerId])
            ->andWhere(':orderTime between start_work and IFNULL(end_work, :now)', [
                ':orderTime' => $orderTime,
                ':now'       => $now,
            ])
            ->orderBy(['start_work' => SORT_DESC])
            ->limit(1)
            ->scalar();
    }

    /**
     * Getting new shifts
     * @return array
     */
    private function getNewShifts()
    {
        $query = (new \yii\db\Query());

        return $query
            ->select('shift_id')
            ->distinct()
            ->from('tbl_worker_shift_order')
            ->where(['created_at' => null])
            ->column();
    }

    /**
     * Getting count of shift orders by status group `rejected`, `completed`
     *
     * @param $shiftId
     * @param $status
     *
     * @return int|string
     */
    private function getOrderCount($shiftId, $status)
    {
        $query = (new \yii\db\Query());

        return $query
            ->select('1')
            ->from('tbl_worker_shift_order as w')
            ->innerJoin('tbl_order as o', 'w.order_id = o.order_id')
            ->innerJoin('tbl_order_status as s', 'o.status_id = s.status_id')
            ->where(['w.shift_id' => $shiftId, 's.status_group' => $status])
            ->count();
    }

    /**
     * Updating shift counters
     *
     * @param array $shifts
     */
    private function updateShiftCounters($shifts)
    {
        if (is_array($shifts)) {
            foreach ($shifts as $shift) {
                $completedCount = $this->getOrderCount($shift, 'completed');
                $rejectedCount  = $this->getOrderCount($shift, 'rejected');

                $this->update('tbl_worker_shift', [
                    'completed_order_count' => $completedCount,
                    'rejected_order_count'  => $rejectedCount,
                ], ['id' => $shift]);
            }
        }
    }

    public function safeUp()
    {
        $orders = $this->getWorkerOrders();

        if (is_array($orders)) {
            $now = time();
            foreach ($orders as $order) {
                $shiftId = $this->getLastShiftId(
                    $order['worker_id'], $order['status_time'], $now);

                if (!empty($shiftId)) {
                    $params = [
                        'order_id' => $order['order_id'],
                        'shift_id' => $shiftId,
                    ];

                    $exists = (new \yii\db\Query())
                        ->from('tbl_worker_shift_order')
                        ->where($params)
                        ->exists();

                    if (!$exists) {
                        $this->insert('tbl_worker_shift_order', $params);
                    }
                } else {
                    echo 'Shift not found (order_id=' . $order['order_id'] . ")\n";
                }
            }
        }

        $shifts = $this->getNewShifts();
        $this->updateShiftCounters($shifts);
    }

    public function safeDown()
    {
        $shifts = $this->getNewShifts();

        $this->delete('tbl_worker_shift_order', ['created_at' => null]);

        $this->updateShiftCounters($shifts);
    }
}
