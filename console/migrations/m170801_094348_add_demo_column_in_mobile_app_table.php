<?php

use yii\db\Migration;

class m170801_094348_add_demo_column_in_mobile_app_table extends Migration
{
    const TABLE_NAME = '{{%mobile_app}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'demo', $this->integer(1)->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'demo');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170801_094348_add_demo_column_in_mobile_app_table cannot be reverted.\n";

        return false;
    }
    */
}
