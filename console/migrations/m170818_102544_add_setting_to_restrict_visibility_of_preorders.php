<?php

use yii\db\Migration;

class m170818_102544_add_setting_to_restrict_visibility_of_preorders extends Migration
{
    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const TABLE_TENANT_SETTING = '{{%tenant_setting}}';

    const SETTING_NAME__RESTRICT_VISIBILITY_OF_PRE_ORDER = 'RESTRICT_VISIBILITY_OF_PRE_ORDER';
    const SETTING_VALUE__RESTRICT_VISIBILITY_OF_PRE_ORDER = '0';
    const SETTING_TYPE__RESTRICT_VISIBILITY_OF_PRE_ORDER = 'orders';

    const SETTING_NAME__TIME_OF_PRE_ORDER_VISIBILITY = 'TIME_OF_PRE_ORDER_VISIBILITY';
    const SETTING_VALUE__TIME_OF_PRE_ORDER_VISIBILITY = null;
    const SETTING_TYPE__TIME_OF_PRE_ORDER_VISIBILITY = 'orders';

    const SQL_ADD_SETTINGS_WITH_CITY_AND_POSITION = <<<SQL
INSERT INTO `tbl_tenant_setting`
            (`tenant_id`, `city_id`, `position_id`, `name`, `value`, `type`)
SELECT `p`.`tenant_id`, `p`.`city_id`, `p`.`position_id`, :name `name`, :value `value`, :type `type`
FROM `tbl_tenant_has_city` AS `c`
JOIN `tbl_tenant_city_has_position` AS `p` ON `c`.`tenant_id` = `p`.`tenant_id` AND `c`.`city_id` = `p`.`city_id`
LEFT JOIN `tbl_tenant_setting` AS `t` ON `p`.`tenant_id` = `t`.`tenant_id` AND `p`.`city_id` = `t`.`city_id` AND `p`.`position_id` = `t`.`position_id` AND `t`.`name` = :name
WHERE `t`.`setting_id` IS NULL
SQL;

    public function safeUp()
    {
        $this->insert(self::TABLE_DEFAULT_SETTINGS, [
            'name'  => self::SETTING_NAME__RESTRICT_VISIBILITY_OF_PRE_ORDER,
            'value' => self::SETTING_VALUE__RESTRICT_VISIBILITY_OF_PRE_ORDER,
            'type'  => self::SETTING_TYPE__RESTRICT_VISIBILITY_OF_PRE_ORDER,
        ]);

        $this->getDb()->createCommand(self::SQL_ADD_SETTINGS_WITH_CITY_AND_POSITION, [
            'name'  => self::SETTING_NAME__RESTRICT_VISIBILITY_OF_PRE_ORDER,
            'value' => self::SETTING_VALUE__RESTRICT_VISIBILITY_OF_PRE_ORDER,
            'type'  => self::SETTING_TYPE__RESTRICT_VISIBILITY_OF_PRE_ORDER,
        ])->execute();

        $this->insert(self::TABLE_DEFAULT_SETTINGS, [
            'name'  => self::SETTING_NAME__TIME_OF_PRE_ORDER_VISIBILITY,
            'value' => self::SETTING_VALUE__TIME_OF_PRE_ORDER_VISIBILITY,
            'type'  => self::SETTING_TYPE__TIME_OF_PRE_ORDER_VISIBILITY,
        ]);

        $this->getDb()->createCommand(self::SQL_ADD_SETTINGS_WITH_CITY_AND_POSITION, [
            'name'  => self::SETTING_NAME__TIME_OF_PRE_ORDER_VISIBILITY,
            'value' => self::SETTING_VALUE__TIME_OF_PRE_ORDER_VISIBILITY,
            'type'  => self::SETTING_TYPE__TIME_OF_PRE_ORDER_VISIBILITY,
        ])->execute();

    }

    public function safeDown()
    {
        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::SETTING_NAME__RESTRICT_VISIBILITY_OF_PRE_ORDER,
            'type' => self::SETTING_TYPE__RESTRICT_VISIBILITY_OF_PRE_ORDER,
        ]);

        $this->delete(self::TABLE_TENANT_SETTING, [
            'name' => self::SETTING_NAME__RESTRICT_VISIBILITY_OF_PRE_ORDER,
            'type' => self::SETTING_TYPE__RESTRICT_VISIBILITY_OF_PRE_ORDER,
        ]);

        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::SETTING_NAME__TIME_OF_PRE_ORDER_VISIBILITY,
            'type' => self::SETTING_TYPE__TIME_OF_PRE_ORDER_VISIBILITY,
        ]);

        $this->delete(self::TABLE_TENANT_SETTING, [
            'name' => self::SETTING_NAME__TIME_OF_PRE_ORDER_VISIBILITY,
            'type' => self::SETTING_TYPE__TIME_OF_PRE_ORDER_VISIBILITY,
        ]);
    }

}

