<?php

use yii\db\Migration;

class m171119_111507_add_rounding_type_in_option_tariff extends Migration
{
    const TABLE_NAME = '{{%option_tariff}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, 'rounding_type_day', "ENUM('FLOOR','ROUND', 'CEIL') NOT NULL DEFAULT 'ROUND'");
        $this->addColumn(self::TABLE_NAME, 'rounding_type_night', "ENUM('FLOOR','ROUND', 'CEIL') NOT NULL DEFAULT 'ROUND'");
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, 'rounding_type_day');
        $this->dropColumn(self::TABLE_NAME, 'rounding_type_night');
    }
}
