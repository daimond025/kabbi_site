<?php

use yii\db\Migration;

class m160229_093336_company_contract extends Migration
{
    
    const TABLE_NAME = '{{%client_company}}';
    
    public function up()
    {
        $this->addColumn(self::TABLE_NAME,'contract_number','INTEGER(11)');
        $this->addColumn(self::TABLE_NAME,'contract_start_date','DATE DEFAULT \'0000-00-00\'');
        $this->addColumn(self::TABLE_NAME,'contract_end_date','DATE DEFAULT \'0000-00-00\'');
        
    }

    public function down()
    {
           $this->dropColumn(self::TABLE_NAME,'contract_number');
           $this->dropColumn(self::TABLE_NAME,'contract_start_date');
           $this->dropColumn(self::TABLE_NAME,'contract_end_date');
    }


}
