<?php

use yii\db\Migration;

class m160909_053352_add_smsc_provider extends Migration
{

    const TABLE_NAME = '{{%sms_server}}';

    const COLUMN_SERVER_ID = 12;

    public function up()
    {
        $this->insert(self::TABLE_NAME,[
            'server_id' => self::COLUMN_SERVER_ID,
            'name' =>  'Smsc.ru',
            'host' => 'http://smsc.ru',
            'active' => '1',
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME,['server_id' => self::COLUMN_SERVER_ID]);
    }
}
