<?php

use yii\db\Migration;

class m180628_113847_parking_change_type_sort extends Migration
{
    const TABLE_NAME = '{{%parking}}';

    public function safeUp()
    {
        $this->alterColumn(self::TABLE_NAME, 'sort', $this->integer()->unsigned());
    }

    public function safeDown()
    {
        return true;
    }

}
