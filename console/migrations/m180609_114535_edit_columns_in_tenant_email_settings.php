<?php

use yii\db\Migration;

class m180609_114535_edit_columns_in_tenant_email_settings extends Migration
{
    const TABLE_NAME = '{{%tenant_email_settings}}';

    public function safeUp()
    {
        $this->alterColumn(self::TABLE_NAME, 'provider_server', $this->string(255));
        $this->alterColumn(self::TABLE_NAME, 'provider_port', $this->integer(5)->unsigned());
        $this->alterColumn(self::TABLE_NAME, 'sender_name', $this->string(100));
        $this->alterColumn(self::TABLE_NAME, 'sender_email', $this->string(255));
        $this->alterColumn(self::TABLE_NAME, 'sender_password', $this->string(100));
        $this->alterColumn(self::TABLE_NAME, 'template', $this->text());
    }

    public function safeDown()
    {
        $this->alterColumn(self::TABLE_NAME, 'provider_server', $this->string(255)->notNull());
        $this->alterColumn(self::TABLE_NAME, 'provider_port', $this->integer(5)->unsigned()->notNull());
        $this->alterColumn(self::TABLE_NAME, 'sender_name', $this->string(100)->notNull());
        $this->alterColumn(self::TABLE_NAME, 'sender_email', $this->string(255)->notNull());
        $this->alterColumn(self::TABLE_NAME, 'sender_password', $this->string(100)->notNull());
        $this->alterColumn(self::TABLE_NAME, 'template', $this->text()->notNull());
    }
}
