<?php

use yii\db\Schema;
use yii\db\Migration;

class m150721_120529_tbl_currency extends Migration
{
    public function up()
    {
        $this->addColumn('{{%currency}}', 'symbol', 'string');
    }

    public function down()
    {
        $this->dropColumn('{{%currency}}', 'symbol');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
