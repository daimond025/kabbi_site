<?php

use yii\db\Schema;
use yii\db\Migration;

class m151106_092055_tbl_option_tariff extends Migration
{
    public function up()
    {
        $this->addColumn('{{%option_tariff}}', 'next_km_price_day_time', Schema::TYPE_FLOAT . ' DEFAULT 0 COMMENT "Стоимость далее при при смешанном типе (Время)"');
        $this->addColumn('{{%option_tariff}}', 'next_km_price_night_time', Schema::TYPE_FLOAT . ' DEFAULT 0 COMMENT "Стоимость далее при при смешанном типе (Время)"');
    }

    public function down()
    {
        $this->dropColumn('{{%option_tariff}}', 'next_km_price_day_time');
        $this->dropColumn('{{%option_tariff}}', 'next_km_price_night_time');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
