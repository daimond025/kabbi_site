<?php

use yii\db\Schema;
use yii\db\Migration;

class m151102_054025_tbl_option_tariff extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%option_tariff}}', 'allow_day_night', Schema::TYPE_BOOLEAN . ' DEFAULT 0');
        $this->alterColumn('{{%option_tariff}}', 'start_day', Schema::TYPE_STRING . '(5) DEFAULT "08:00"');
        $this->alterColumn('{{%option_tariff}}', 'end_day', Schema::TYPE_STRING . '(5) DEFAULT "20:00"');
    }

    public function down()
    {
        echo "m151102_054025_tbl_option_tariff cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
