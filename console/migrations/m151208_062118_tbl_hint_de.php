<?php

use yii\db\Schema;
use yii\db\Migration;

class m151208_062118_tbl_hint_de extends Migration
{
    const TABLE_NAME = '{{%hints_de}}';

    public function up() {
        $this->createTable(self::TABLE_NAME, [
            'id'   => $this->primaryKey(),
            'text' => $this->string(255)->notNull(),
        ]);
        $this->insert(self::TABLE_NAME, ['text' => 'Please add branches (cities) where your company will work. You can set different settings for each branch.']);
        $this->insert(self::TABLE_NAME, ['text' => 'Distance: depends on how close car is relatively to a client. Parking zones: cars take a part only being on one parking zone relatively to a client']);
        $this->insert(self::TABLE_NAME, ['text' => 'For cash operations. Operation "Cash out" is not included in report']);
        $this->insert(self::TABLE_NAME, ['text' => 'Difference between debt and credit']);
        $this->insert(self::TABLE_NAME, ['text' => 'Expense']);
        $this->insert(self::TABLE_NAME, ['text' => 'Balance refill']);
        $this->insert(self::TABLE_NAME, ['text' => 'Summary cost of all trips without deductions']);
        $this->insert(self::TABLE_NAME, ['text' => 'Summary earned in period']);
        $this->insert(self::TABLE_NAME, ['text' => 'Incomes in period. Your obligations']);
        $this->insert(self::TABLE_NAME, ['text' => 'Difference between your debt and credit']);
    }

    public function down() {
        $this->dropTable(self::TABLE_NAME);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
