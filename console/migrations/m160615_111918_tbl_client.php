<?php

use yii\db\Migration;

class m160615_111918_tbl_client extends Migration
{
    const TABLE_NAME = '{{%client}}';

    public function up()
    {
        $clients = (new \yii\db\Query())
            ->from(self::TABLE_NAME)
            ->where(['password' => null])
            ->select('client_id')
            ->all();

        foreach ($clients as $client) {
            Yii::$app->db->createCommand()
                ->update(self::TABLE_NAME, ['password' => rand(100000, 999999)], ['client_id' => $client['client_id']])
                ->execute();
        }
    }

    public function down()
    {
        echo "m160615_111918_tbl_client cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
