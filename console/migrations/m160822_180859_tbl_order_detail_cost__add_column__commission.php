<?php

use yii\db\Migration;

class m160822_180859_tbl_order_detail_cost__add_column__commission extends Migration
{
    const TABLE_ORDER_DETAIL_COST = '{{%order_detail_cost}}';
    const COLUMN_COMMISSION = 'commission';

    public function up()
    {
        $this->addColumn(self::TABLE_ORDER_DETAIL_COST, self::COLUMN_COMMISSION,
            $this->string(45)->defaultValue('0'));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_ORDER_DETAIL_COST, self::COLUMN_COMMISSION);
    }

}
