<?php

use yii\db\Migration;

class m170821_094930_tbl_card_payment__add_column__to_pan extends Migration
{
    const TABLE_CARD_PAYMENT = '{{%card_payment}}';
    const COLUMN_TO_PAN = 'to_pan';

    public function safeUp()
    {
        $this->addColumn(self::TABLE_CARD_PAYMENT, self::COLUMN_TO_PAN, $this->string(25));
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE_CARD_PAYMENT, self::COLUMN_TO_PAN);
    }

}
