<?php

use yii\db\Schema;
use yii\db\Migration;

class m150921_151747_driver_groups extends Migration
{
    public function up()
    {
        $this->execute('SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0');
        $this->execute('SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0');
        $this->execute("SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES'");

        $this->createTable('{{%driver_group}}', [
            'group_id' => $this->primaryKey(),
            'tenant_id' => Schema::TYPE_INTEGER . '(10) UNSIGNED NOT NULL',
            'name' => $this->string(255)->notNull(),
            'class_id' => 'tinyint(3) UNSIGNED NOT NULL',
            'block' => $this->boolean()
        ]);

        $this->createIndex('FK_driver_group_tenant_id_idx', '{{%driver_group}}', 'tenant_id');
        $this->addForeignKey('FK_driver_group_tenant_id', '{{%driver_group}}', 'tenant_id', '{{%tenant}}', 'tenant_id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%driver_group_has_city}}', [
            'group_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'city_id' => Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL',
        ]);

        $this->createIndex('FK_driver_group_has_city_idx', '{{%driver_group_has_city}}', ['group_id', 'city_id']);

        $this->addForeignKey('FK_driver_group_has_city_group_id', '{{%driver_group_has_city}}', 'group_id', '{{%driver_group}}', 'group_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_driver_group_has_city_id', '{{%driver_group_has_city}}', 'city_id', '{{%city}}', 'city_id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%driver_group_has_tariff}}', [
            'group_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'tariff_id' => Schema::TYPE_INTEGER . '(10) UNSIGNED NOT NULL',
        ]);

        $this->createIndex('FK_driver_group_has_tariff_idx', '{{%driver_group_has_tariff}}', ['group_id', 'tariff_id']);

        $this->addForeignKey('FK_driver_group_has_tariff_group_id', '{{%driver_group_has_tariff}}', 'group_id', '{{%driver_group}}', 'group_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_driver_group_has_tariff_id', '{{%driver_group_has_tariff}}', 'tariff_id', '{{%driver_tariff}}', 'tariff_id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%driver_group_can_view_client_tariff}}', [
            'group_id' => $this->integer()->notNull(),
            'class_id' => 'tinyint(3) UNSIGNED NOT NULL',
        ]);

        $this->createIndex('FK_driver_group_can_view_client_tariff_idx', '{{%driver_group_can_view_client_tariff}}', ['group_id', 'class_id']);

        $this->addForeignKey('FK_driver_group_can_view_client_tariff_group_id', '{{%driver_group_can_view_client_tariff}}', 'group_id', '{{%driver_group}}', 'group_id', 'CASCADE', 'CASCADE');

        $this->execute('SET SQL_MODE=@OLD_SQL_MODE');
        $this->execute('SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS');
        $this->execute('SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS');
    }

    public function down()
    {
        $this->dropForeignKey('FK_driver_group_tenant_id', '{{%driver_group}}');
        $this->dropForeignKey('FK_driver_group_has_city_group_id', '{{%driver_group_has_city}}');
        $this->dropForeignKey('FK_driver_group_has_city_id', '{{%driver_group_has_city}}');
        $this->dropForeignKey('FK_driver_group_has_tariff_group_id', '{{%driver_group_has_tariff}}');
        $this->dropForeignKey('FK_driver_group_has_tariff_id', '{{%driver_group_has_tariff}}');
        $this->dropForeignKey('FK_driver_group_can_view_client_tariff_group_id', '{{%driver_group_can_view_client_tariff}}');

        $this->dropIndex('FK_driver_group_tenant_id_idx', '{{%driver_group}}');
        $this->dropIndex('FK_driver_group_has_city_idx', '{{%driver_group_has_city}}');
        $this->dropIndex('FK_driver_group_has_tariff_idx', '{{%driver_group_has_tariff}}');
        $this->dropIndex('FK_driver_group_can_view_client_tariff_idx', '{{%driver_group_can_view_client_tariff}}');

        $this->dropTable('{{%driver_group}}');
        $this->dropTable('{{%driver_group_has_city}}');
        $this->dropTable('{{%driver_group_has_tariff}}');
        $this->dropTable('{{%driver_group_can_view_client_tariff}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
