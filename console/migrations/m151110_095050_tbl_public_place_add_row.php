<?php

use yii\db\Schema;
use yii\db\Migration;

class m151110_095050_tbl_public_place_add_row extends Migration
{

    public function up()
    {
        $this->addColumn('{{%public_place}}', 'label_en', 'VARCHAR(100) DEFAULT  NULL');
        $this->addColumn('{{%public_place}}', 'label_az', 'VARCHAR(100) DEFAULT  NULL');
        $this->addColumn('{{%public_place}}', 'label_de', 'VARCHAR(100) DEFAULT  NULL');
        $this->addColumn('{{%public_place}}', 'label_se', 'VARCHAR(100) DEFAULT  NULL');
    }

    public function down()
    {
        $this->dropColumn('{{%public_place}}', 'label_en');
        $this->dropColumn('{{%public_place}}', 'label_az');
        $this->dropColumn('{{%public_place}}', 'label_de');
        $this->dropColumn('{{%public_place}}', 'label_se');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
