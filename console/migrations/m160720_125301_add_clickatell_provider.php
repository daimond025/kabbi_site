<?php

use yii\db\Migration;

class m160720_125301_add_clickatell_provider extends Migration
{
    const TABLE_NAME = '{{%sms_server}}';

    public function up()
    {
        $this->insert(self::TABLE_NAME,[
            'server_id' => 11,
            'name' => 'Clickatell',
            'host' => 'clickatell.com',
            'active' => 1
        ]);
    }

    public function down()
    {
        $this->delete(self::TABLE_NAME,['server_id' => 11]);
    }
}
