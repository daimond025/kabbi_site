<?php

use yii\db\Schema;
use yii\db\Migration;

class m151003_070725_car_driver_groups extends Migration
{
    public function up()
    {
        $this->addPrimaryKey('FK_car_driver_group_has_tariff_tariff_id_car_id', '{{%car_driver_group_has_tariff}}', ['car_id', 'tariff_id']);
        $this->addPrimaryKey('FK_car_driver_group_has_tariff_tariff_id_class_id', '{{%car_driver_group_has_class}}', ['car_id', 'class_id']);
    }

    public function down()
    {
        echo "m151003_070725_car_driver_groups cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
