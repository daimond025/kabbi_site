<?php

use yii\db\Migration;

class m170114_084430_edit_addressee_id__in__notification extends Migration
{
    const TABLE_NAME = '{{%notification}}';

    public function up()
    {
        $this->alterColumn(self::TABLE_NAME, 'addressee_id',
            $this->string(15)->comment('Если пустое, то отправляет всем'));
    }

    public function down()
    {
        $this->alterColumn(self::TABLE_NAME, 'addressee_id',
            $this->integer(11)->comment('Если пустое, то отправляет всем'));
    }
}
