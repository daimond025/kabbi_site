<?php

use yii\db\Migration;

class m170301_092946_add_column_sort_in_car_class extends Migration
{

    const TABLE_NAME = '{{%car_class}}';
    const TABLE_ADDED_COLUMN = 'sort';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, self::TABLE_ADDED_COLUMN,
            $this->integer(10)->notNull()->defaultValue(100)->after('class'));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, self::TABLE_ADDED_COLUMN);
    }
}
