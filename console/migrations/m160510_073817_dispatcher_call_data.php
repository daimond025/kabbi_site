<?php

use yii\db\Migration;

class m160510_073817_dispatcher_call_data extends Migration
{
    const TABLE_NAME = '{{%user_working_time}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'count_income_answered_calls', 'INTEGER(11)');
        $this->addColumn(self::TABLE_NAME, 'count_income_noanswered_calls', 'INTEGER(11)');
        $this->addColumn(self::TABLE_NAME, 'count_outcome_answered_calls', 'INTEGER(11)');
        $this->addColumn(self::TABLE_NAME, 'count_outcome_noanswered_calls', 'INTEGER(11)');
        $this->addColumn(self::TABLE_NAME, 'count_completed_orders', 'INTEGER(11)');
        $this->addColumn(self::TABLE_NAME, 'count_rejected_orders', 'INTEGER(11)');
        $this->addColumn(self::TABLE_NAME, 'summ_completed_arr', 'VARCHAR(255)');
        $this->addColumn(self::TABLE_NAME, 'summ_rejected_arr', 'VARCHAR(255)');
        $this->addColumn(self::TABLE_NAME, 'count_income_calls', 'INTEGER(11)');
        $this->addColumn(self::TABLE_NAME, 'count_outcome_calls', 'INTEGER(11)');
        $this->addColumn(self::TABLE_NAME, 'summ_income_calls_time', 'INTEGER(11)');
        $this->addColumn(self::TABLE_NAME, 'summ_outcome_calls_time', 'INTEGER(11)');
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'count_income_answered_calls');
        $this->dropColumn(self::TABLE_NAME, 'count_income_noanswered_calls');
        $this->dropColumn(self::TABLE_NAME, 'count_outcome_answered_calls');
        $this->dropColumn(self::TABLE_NAME, 'count_outcome_noanswered_calls');
        $this->dropColumn(self::TABLE_NAME, 'count_completed_orders');
        $this->dropColumn(self::TABLE_NAME, 'count_rejected_orders');
        $this->dropColumn(self::TABLE_NAME, 'summ_completed_arr');
        $this->dropColumn(self::TABLE_NAME, 'summ_rejected_arr');
        $this->dropColumn(self::TABLE_NAME, 'count_income_calls');
        $this->dropColumn(self::TABLE_NAME, 'count_outcome_calls');
        $this->dropColumn(self::TABLE_NAME, 'summ_income_calls_time');
        $this->dropColumn(self::TABLE_NAME, 'summ_outcome_calls_time');
        return true;
    }
}