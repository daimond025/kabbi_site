<?php

use yii\db\Schema;
use yii\db\Migration;

class m151024_122245_tbl_admin_tariff_additional_option extends Migration
{
    const TABLE_NAME = '{{%admin_tariff_additional_option}}';
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'option_id' => $this->primaryKey(),
            'name' => $this->string(48)->notNull(),
            'price' => $this->integer(6)->notNull(),
            'active' => $this->boolean()->notNull(), 
            'create_date' => $this->dateTime()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
        echo 'Table' . self::TABLE_NAME . ' has been dropped';
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
