<?php

use yii\db\Schema;
use yii\db\Migration;

class m160126_080224_tbl_tenant_has_sms extends Migration
{
    public function up()
    {
        $tenantHasSms = (new \yii\db\Query())
            ->from('{{%tenant_has_sms}}')
            ->all();

        $arCity = [];
        $arDefaultCity = [];

        foreach ($tenantHasSms as $item) {
            if (!isset($arCity[$item['tenant_id']])) {
                $tenantHasCity = (new \yii\db\Query())
                    ->select('city_id')
                    ->from('{{%tenant_has_city}}')
                    ->where(['tenant_id' => $item['tenant_id']])
                    ->all();
                $tenantHasCity = \yii\helpers\ArrayHelper::getColumn($tenantHasCity, 'city_id');
                $arCity[$item['tenant_id']] = $tenantHasCity;
                $arDefaultCity[$item['tenant_id']] = array_shift($arCity[$item['tenant_id']]);
            }

            if (empty($item['city_id'])) {
                Yii::$app->db->createCommand()->update('{{%tenant_has_sms}}', ['city_id' => $arDefaultCity[$item['tenant_id']]], ['id' => $item['id']])->execute();
            }

            foreach ($arCity[$item['tenant_id']] as $cityItem) {
                Yii::$app->db->createCommand()->insert('{{%tenant_has_sms}}', [
                    'server_id' => $item['server_id'],
                    'tenant_id' => $item['tenant_id'],
                    'login'     => $item['login'],
                    'password'  => $item['password'],
                    'active'    => $item['active'],
                    'sign'      => $item['sign'],
                    'city_id'   => $cityItem,
                ])->execute();
            }
        }
    }

    public function down()
    {
        echo "m160126_080224_tbl_tenant_has_sms cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
