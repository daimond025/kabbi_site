<?php

use yii\db\Schema;
use yii\db\Migration;

class m150622_071016_tbl_currency extends Migration
{
    const CURRENCY_TYPE = 1;
    
    public function up()
    {
        $this->batchInsert('{{%currency}}', ['type_id', 'name', 'code'], [
            ['type_id' => self::CURRENCY_TYPE, 'name' => 'Rubl', 'code' => 'RUB'],
            ['type_id' => self::CURRENCY_TYPE, 'name' => 'Dollar', 'code' => 'USD'],
            ['type_id' => self::CURRENCY_TYPE, 'name' => 'Manat', 'code' => 'AZN'],
            ['type_id' => self::CURRENCY_TYPE, 'name' => 'Dinar', 'code' => 'RSD']
        ]);
    }

    public function down()
    {
        echo "m150622_071016_tbl_currency cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
