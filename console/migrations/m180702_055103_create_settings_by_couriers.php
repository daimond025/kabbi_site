<?php

use common\modules\tenant\models\DefaultSettings;
use yii\db\Migration;

class m180702_055103_create_settings_by_couriers extends Migration
{
    const TABLE_DEFAULT_SETTINGS = '{{%default_settings}}';
    const TABLE_TENANT_SETTING = '{{%tenant_setting}}';

    const SETTING_ORDER_FORM_ADD_POINT_PHONE = 'ORDER_FORM_ADD_POINT_PHONE';
    const SETTING_SMS_NOTIFY_POINT_RECIPIENT_ABOUT_ORDER_EXECUTING = 'SMS_NOTIFY_POINT_RECIPIENT_ABOUT_ORDER_EXECUTING';
    const SETTING_ORDER_FORM_ADD_POINT_COMMENT = 'ORDER_FORM_ADD_POINT_COMMENT';
    const SETTING_REQUIRE_POINT_CONFIRMATION_CODE = 'REQUIRE_POINT_CONFIRMATION_CODE';
    const SETTING_VALUE_PRINT_CHECK = '0';
    const SETTING_TYPE_PRINT_CHECK = 'orders';

    public function safeUp()
    {
        $printCheck = new DefaultSettings([
            'name'  => self::SETTING_ORDER_FORM_ADD_POINT_PHONE,
            'value' => self::SETTING_VALUE_PRINT_CHECK,
            'type'  => self::SETTING_TYPE_PRINT_CHECK,
        ]);
        $printCheck->save();

        $printCheck = new DefaultSettings([
            'name'  => self::SETTING_SMS_NOTIFY_POINT_RECIPIENT_ABOUT_ORDER_EXECUTING,
            'value' => self::SETTING_VALUE_PRINT_CHECK,
            'type'  => self::SETTING_TYPE_PRINT_CHECK,
        ]);
        $printCheck->save();

        $printCheck = new DefaultSettings([
            'name'  => self::SETTING_ORDER_FORM_ADD_POINT_COMMENT,
            'value' => self::SETTING_VALUE_PRINT_CHECK,
            'type'  => self::SETTING_TYPE_PRINT_CHECK,
        ]);
        $printCheck->save();

        $printCheck = new DefaultSettings([
            'name'  => self::SETTING_REQUIRE_POINT_CONFIRMATION_CODE,
            'value' => self::SETTING_VALUE_PRINT_CHECK,
            'type'  => self::SETTING_TYPE_PRINT_CHECK,
        ]);
        $printCheck->save();

    }

    public function safeDown()
    {
        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::SETTING_ORDER_FORM_ADD_POINT_COMMENT,
            'type' => self::SETTING_TYPE_PRINT_CHECK,
        ]);

        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::SETTING_SMS_NOTIFY_POINT_RECIPIENT_ABOUT_ORDER_EXECUTING,
            'type' => self::SETTING_TYPE_PRINT_CHECK,
        ]);

        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::SETTING_ORDER_FORM_ADD_POINT_PHONE,
            'type' => self::SETTING_TYPE_PRINT_CHECK,
        ]);

        $this->delete(self::TABLE_DEFAULT_SETTINGS, [
            'name' => self::SETTING_REQUIRE_POINT_CONFIRMATION_CODE,
            'type' => self::SETTING_TYPE_PRINT_CHECK,
        ]);
    }
}

