<?php

use yii\db\Migration;

class m170306_103356_sorting_sms_provider extends Migration
{
    const TABLE_NAME = '{{%sms_server}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'sort', $this->integer(10)->notNull()->defaultValue(100)->after('name'));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'sort');
    }
}
