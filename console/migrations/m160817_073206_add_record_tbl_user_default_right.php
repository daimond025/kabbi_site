<?php

use yii\db\Migration;

class m160817_073206_add_record_tbl_user_default_right extends Migration
{
    public function up()
    {
        $this->insert('{{%user_default_right}}', ['position_id' => 7, 'permission' => 'cities', 'rights' => 'write']);
    }

    public function down()
    {
        echo "m160817_073206_add_record_tbl_user_default_right cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
