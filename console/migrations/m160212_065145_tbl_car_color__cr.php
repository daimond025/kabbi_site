<?php

use yii\db\Schema;
use yii\db\Migration;

class m160212_065145_tbl_car_color__cr extends Migration
{
    const TABLE_NAME = '{{%car_color}}';

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'color_id' => $this->primaryKey(),
            'name'     => $this->string(255)->notNull(),
        ]);

        $arColor = [
            ['Black'],
            ['Blue'],
            ['Red'],
            ['White'],
            ['Silver'],
            ['Green'],
            ['Grey'],
            ['Brown'],
            ['Beige'],
            ['Yellow'],
            ['Gold'],
            ['Purple'],
            ['Orange'],
            ['Dark'],
        ];

        $this->batchInsert(self::TABLE_NAME, ['name'], $arColor);
    }

    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
