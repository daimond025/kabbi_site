<?php
/**
 * Created by PhpStorm.
 * User: alexey
 * Date: 24.08.18
 * Time: 13:35
 */

namespace console\components\mysqlOnlineSchemaChange;


use RuntimeException;

trait MysqlOnlineSchemaChangeTrait
{
    /**
     * Run query
     * @param string $tableName - data base table name, e.g: tbl_order
     * @param string $command - DML query without 'ALTER TABLE `some table`' expression, e.g: 'ADD COLUMN test000 INT(10)'
     * @param array $options - options to how execute script:
     *                       [
     *                       'alterForeignKeysMethod' => MysqlSchemaChange::ALTER_FOREIGN_KEY_AUTO,
     *                       'debug'                  => false,
     *                       'execute'                => false,
     *                       ]
     *                       if do not pass this parameter, then the default options
     * @return bool
     * @throws RuntimeException
     */
    public function schemaChange($tableName, $command, array $options = null)
    {
        if ($options === null) {
            $options = [
                'alterForeignKeysMethod' => MysqlOnlineSchemaChange::ALTER_FOREIGN_KEY_DROP_SWAP,
                'execute'                => true,
                'debug'                  => false,
            ];
        }

        /** @var MysqlOnlineSchemaChange $changer */
        $changer = \Yii::$app->mysqlOnlineSchemaChange;
        try {
            $changer->run($tableName, $command, $options);

            return true;
        } catch (RuntimeException $ex) {
            echo $ex->getMessage() . PHP_EOL;

            return false;
        }
    }
}