<?php

namespace console\components\tenantTariff;

use common\modules\tenant\modules\tariff\components\TenantTariffService;
use console\components\tenantTariff\models\FreeTariff;
use console\components\tenantTariff\models\Tariff;
use console\components\tenantTariff\models\Tenant;
use yii\base\Object;

/**
 * Class NotificationService
 * @package console\components\tenantTariff
 */
class NotificationService extends Object
{
    /**
     * @var TenantTariffService
     */
    public $tenantTariffService;

    /**
     * @var NotifyInterface[]
     */
    public $notifications = [];

    /**
     * @var int[]
     */
    public $notificationDays = [];

    public function init()
    {
        $this->tenantTariffService = \Yii::createObject($this->tenantTariffService);
        $this->notifications       = array_map(function ($notification) {
            return \Yii::createObject($notification);
        }, $this->notifications);
    }

    /**
     * Notify tenant about tariff expiration
     *
     * @param array $notificationData
     */
    private function notify(array $notificationData)
    {
        foreach ($notificationData as $tenantInfo) {
            $tenant = new Tenant([
                'tenantId'          => $tenantInfo['tenant_id'],
                'domain'            => $tenantInfo['domain'],
                'companyName'       => $tenantInfo['company_name'],
                'contactPhone'      => $tenantInfo['contact_phone'],
                'contactEmail'      => $tenantInfo['contact_email'],
                'contactName'       => $tenantInfo['contact_name'],
                'contactSecondName' => $tenantInfo['contact_second_name'],
                'contactLastName'   => $tenantInfo['contact_last_name'],
            ]);

            if (is_array($tenantInfo['tariffs'])) {
                $tariffs = array_map(function ($value) {
                    $freeTariff = new FreeTariff([
                        'name' => $value['free_tariff'],
                    ]);

                    if (isset($value['workers_online'])) {
                        $freeTariff->workersOnline = $value['workers_online'];
                    }
                    if (isset($value['employees_online'])) {
                        $freeTariff->employeesOnline = $value['employees_online'];
                    }

                    return new Tariff([
                        'name'       => $value['name'],
                        'expiryDate' => $value['expiry_date'],
                        'daysLeft'   => $value['days_left'],
                        'freeTariff' => $freeTariff,
                    ]);
                }, $tenantInfo['tariffs']);

                foreach ($this->notifications as $notification) {
                    $notification->notify($tenant, $tariffs);
                }
            }

        }
    }

    /**
     * Notify tenants about expiration of the tariff
     */
    public function notifyTariffExpiration()
    {
        if (empty($this->notifications) || empty($this->notificationDays)) {
            return;
        }

        $notificationData = $this->tenantTariffService->getNotificationData(
            time(), $this->notificationDays);
        $this->notify($notificationData);
    }

}