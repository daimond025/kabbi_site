<?php

namespace console\components\tenantTariff\models;

use yii\base\Object;

/**
 * Class FreeTariff
 * @package console\components\tenantTariff\models
 */
class FreeTariff extends Object
{
    public $name;
    public $workersOnline;
    public $employeesOnline;
}