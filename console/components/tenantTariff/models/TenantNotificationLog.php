<?php

namespace console\components\tenantTariff\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tbl_tenant_notification_log".
 *
 * @property integer $id
 * @property integer $tenant_id
 * @property string  $type
 * @property string  $reason
 * @property string  $to
 * @property string  $content
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Tenant  $tenant
 */
class TenantNotificationLog extends ActiveRecord
{
    const TYPE_EMAIL = 'EMAIL';
    const TYPE_SMS = 'SMS';

    const REASON_ADVANCE_NOTICE = 'ADVANCE_NOTICE';
    const REASON_TARIFF_EXPIRED_NOTICE = 'TARIFF_EXPIRED_NOTICE';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_tenant_notification_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'created_at', 'updated_at'], 'integer'],
            [['type', 'reason'], 'required'],
            [['type', 'reason', 'content'], 'string'],
            [['to'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'tenant_id'  => 'Tenant ID',
            'type'       => 'Type',
            'reason'     => 'Reason',
            'to'         => 'To',
            'content'    => 'Content',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }
}
