<?php

namespace console\components\tenantTariff\models;

use yii\base\Object;

/**
 * Class Tenant
 * @package console\components\tenantTariff\models
 */
class Tenant extends Object
{
    public $tenantId;
    public $domain;
    public $companyName;
    public $contactPhone;
    public $contactEmail;
    public $contactName;
    public $contactSecondName;
    public $contactLastName;
}