<?php

namespace console\components\tenantTariff\models;

use yii\base\Object;

/**
 * Class Tariff
 * @package console\components\tenantTariff\models
 */
class Tariff extends Object
{
    public $name;
    public $expiryDate;
    public $daysLeft;

    /**
     * @var FreeTariff
     */
    public $freeTariff;
}