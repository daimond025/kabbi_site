<?php

namespace console\components\tenantTariff\models\notifications;

use common\components\gearman\Gearman;
use console\components\tenantTariff\models\Tariff;
use console\components\tenantTariff\models\Tenant;
use console\components\tenantTariff\models\TenantNotificationLog;
use yii\base\Object;

/**
 * Class EmailNotification
 * @package console\components\tenantTariff\models\notifications
 */
class EmailNotification extends Object implements NotifyInterface
{
    public $language = 'ru-RU';
    public $paymentPage = '/tenant/tariff/payment';

    public $tenantService;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->tenantService = \Yii::createObject($this->tenantService);
        parent::init();
    }

    /**
     * Sending email
     *
     * @param $params
     */
    protected function sendEmail($params)
    {
        sendMail($params);
    }

    /**
     * @inheritdoc
     */
    public function notify(Tenant $tenant, array $tariffs)
    {
        foreach ($tariffs as $tariff) {
            /* @var $tariff Tariff */
            if (empty($tariff->daysLeft)) {
                $template = 'tariffExpiredNotice';
                $reason   = TenantNotificationLog::REASON_TARIFF_EXPIRED_NOTICE;
                $subject  = \Yii::t('email',
                    'Your account has been transferred to the tariff {tariff}',
                    ['tariff' => $tariff->freeTariff->name],
                    $this->language);
            } else {
                $template = 'advanceNotice';
                $reason   = TenantNotificationLog::REASON_ADVANCE_NOTICE;
                $subject  = \Yii::t('email',
                    'Time your tariff {tariff} expire after {days_left} days.',
                    [
                        'tariff'    => $tariff->name,
                        'days_left' => $tariff->daysLeft,
                    ], $this->language);
            }

            $params = [
                'LAYOUT'   => 'layouts/base',
                'TEMPLATE' => $template,
                'TO'       => $tenant->contactEmail,
                'SUBJECT'  => $subject,
                'DATA'     => [
                    'TENANT_ID'        => $tenant->tenantId,
                    'PAYMENT_URL'      => $this->tenantService->getUrlByDomain($tenant->domain) . $this->paymentPage,
                    'REASON'           => $reason,
                    'PHONE'            => \Yii::$app->params['supportPhone'],
                    'EMAIL'            => \Yii::$app->params['supportEmail2'],
                    'LANGUAGE'         => $this->language,
                    'USER'             => implode(' ', [
                        $tenant->contactName,
                        $tenant->contactSecondName,
                    ]),
                    'TARIFF'           => $tariff->name,
                    'DAYS_LEFT'        => $tariff->daysLeft,
                    'EXPIRY_DATE'      => $tariff->expiryDate,
                    'FREE_TARIFF'      => $tariff->freeTariff->name,
                    'WORKERS_ONLINE'   => $tariff->freeTariff->workersOnline,
                    'EMPLOYEES_ONLINE' => $tariff->freeTariff->employeesOnline,
                ],
            ];

            $this->sendEmail($params);
        }
    }
}