<?php

namespace console\components\tenantTariff\models\notifications;

use console\components\tenantTariff\models\TenantNotificationLog;
use yii\base\Exception;

/**
 * Class LogEmailNotification
 * @package console\components\tenantTariff\models\notifications
 */
class LogEmailNotification extends EmailNotification
{
    /**
     * @param $params
     */
    private function log($params)
    {
        try {
            $log = \Yii::createObject([
                'class'     => TenantNotificationLog::className(),
                'tenant_id' => $params['DATA']['TENANT_ID'],
                'type'      => TenantNotificationLog::TYPE_EMAIL,
                'reason'    => $params['DATA']['REASON'],
                'to'        => $params['TO'],
                'content'   => json_encode([
                    'user'             => $params['DATA']['USER'],
                    'tariff'           => $params['DATA']['TARIFF'],
                    'days_left'        => $params['DATA']['DAYS_LEFT'],
                    'expiry_date'      => date('d.m.Y', strtotime('+1day', $params['DATA']['EXPIRY_DATE'])),
                    'free_tariff'      => $params['DATA']['FREE_TARIFF'],
                    'workers_online'   => $params['DATA']['WORKERS_ONLINE'],
                    'employees_online' => $params['DATA']['EMPLOYEES_ONLINE'],
                ], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE),
            ]);

            if (!$log->save()) {
                throw new Exception(implode('; ', $log->getFirstErrors()));
            }
        } catch (\Exception $e) {
            \Yii::error($e);
        }
    }

    /**
     * @inheritdoc
     */
    protected function sendEmail($params)
    {
        parent::sendEmail($params);
        $this->log($params);
    }

}