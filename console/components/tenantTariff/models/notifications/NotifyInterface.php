<?php

namespace console\components\tenantTariff\models\notifications;

use console\components\tenantTariff\models\Tariff;
use console\components\tenantTariff\models\Tenant;

/**
 * Interface NotifyInterface
 * @package console\components\tenantTariff\models\notifications
 */
interface NotifyInterface
{
    /**
     * @param Tenant   $tenant
     * @param Tariff[] $tariff
     */
    public function notify(Tenant $tenant, array $tariff);
}