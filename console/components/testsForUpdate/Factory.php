<?php
namespace console\components\testsForUpdate;


class Factory
{


    public function getTest($testName)
    {
        switch ($testName) {
            case 'insert':
                return new Insert();

            case 'at':
                return new InsertWithAlterTable();

            case 'uat':
                return new UpdateWithAlterTable();

            case 'sat':
                return new SelectWithAlterTable();

            case 'ia':
                return new InsertWithUpdate();

            case 'cobs':
                return new CreateOrderBySite();

            default:
                throw new \yii\console\Exception('Не найден тест');
        }
    }


}