<?php

namespace console\components\testsForUpdate;


use common\modules\order\models\Order;

class SelectWithAlterTable implements Test
{


    protected $sleep = 1;

    public function countProcess()
    {
        return 10;
    }


    public function process1()
    {
        echo 'Начало изменения колонок' . PHP_EOL;
        \Yii::$app->db->createCommand()->addColumn('tbl_order', 'test', 'INTEGER')->execute();
        echo 'колонка создана' . PHP_EOL;
        \Yii::$app->db->createCommand()->dropColumn('tbl_order', 'test')->execute();
        echo 'колонка удалена' . PHP_EOL;
    }

    public function process2()
    {
        sleep($this->sleep);

        $order = Order::find()->where([
                'tenant_id' => 68,
                'order_number' => 31230
            ])
            ->one();

        if ($order instanceof Order) {
            echo 'данные пришли' . PHP_EOL;
        } else {
            echo 'данных нет' . PHP_EOL;
        }

    }

    public function process3()
    {
        sleep($this->sleep);

        $order = Order::find()->where([
            'tenant_id' => 68,
            'order_number' => 31466
        ])
            ->one();

        if ($order instanceof Order) {
            echo 'данные пришли' . PHP_EOL;
        } else {
            echo 'данных нет' . PHP_EOL;
        }

    }

    public function process4()
    {
        sleep($this->sleep);

        $order = Order::find()->where([
            'tenant_id' => 68,
            'order_number' => 31768
        ])
            ->one();

        if ($order instanceof Order) {
            echo 'данные пришли' . PHP_EOL;
        } else {
            echo 'данных нет' . PHP_EOL;
        }
    }

    public function process5()
    {
        sleep($this->sleep);
        $order = Order::find()->where([
            'tenant_id' => 68,
            'order_number' => 32026
        ])
            ->one();

        if ($order instanceof Order) {
            echo 'данные пришли' . PHP_EOL;
        } else {
            echo 'данных нет' . PHP_EOL;
        }

    }

    public function process6()
    {
        sleep($this->sleep);

        $order = Order::find()->where([
            'tenant_id' => 68,
            'order_number' => 32283
        ])
            ->one();

        if ($order instanceof Order) {
            echo 'данные пришли' . PHP_EOL;
        } else {
            echo 'данных нет' . PHP_EOL;
        }

    }

    public function process7()
    {
        sleep($this->sleep);

        $order = Order::find()->where([
            'tenant_id' => 68,
            'order_number' => 32528
        ])
            ->one();

        if ($order instanceof Order) {
            echo 'данные пришли' . PHP_EOL;
        } else {
            echo 'данных нет' . PHP_EOL;
        }

    }

    public function process8()
    {
        sleep($this->sleep);

        $order = Order::find()->where([
            'tenant_id' => 68,
            'order_number' => 32706
        ])
            ->one();

        if ($order instanceof Order) {
            echo 'данные пришли' . PHP_EOL;
        } else {
            echo 'данных нет' . PHP_EOL;
        }

    }

    public function process9()
    {
        sleep($this->sleep);

        $order = Order::find()->where([
            'tenant_id' => 68,
            'order_number' => 32917
        ])
            ->one();

        if ($order instanceof Order) {
            echo 'данные пришли' . PHP_EOL;
        } else {
            echo 'данных нет' . PHP_EOL;
        }

    }

    public function process10()
    {
        sleep($this->sleep);
        $order = Order::find()->where([
            'tenant_id' => 68,
            'order_number' => 33112
        ])
            ->one();

        if ($order instanceof Order) {
            echo 'данные пришли' . PHP_EOL;
        } else {
            echo 'данных нет' . PHP_EOL;
        }

    }
}