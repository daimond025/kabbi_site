<?php

namespace console\components\testsForUpdate;


use common\modules\order\models\Order;

class UpdateWithAlterTable
{


    protected $sleep = 1;

    public function countProcess()
    {
        return 10;
    }


    public function process1()
    {
        echo 'Начало изменения колонок' . PHP_EOL;
        \Yii::$app->db->createCommand()->addColumn('tbl_order', 'test', 'INTEGER')->execute();
        echo 'колонка создана' . PHP_EOL;
        \Yii::$app->db->createCommand()->dropColumn('tbl_order', 'test')->execute();
        echo 'колонка удалена' . PHP_EOL;
    }

    public function process2()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand()->update(Order::tableName(), [
            'address' => 'a:2:{s:1:\"A\";a:11:{s:4:\"city\";s:29:\"Санкт-Петербург\";s:7:\"city_id\";s:6:\"210862\";s:6:\"street\";s:39:\"Аптекарский проспект\";s:5:\"house\";s:0:\"\";s:3:\"lat\";s:9:\"59.972406\";s:3:\"lon\";s:8:\"30.32012\";s:7:\"parking\";s:55:\"Базовый район (границы города)\";s:10:\"parking_id\";s:3:\"792\";s:7:\"housing\";s:0:\"\";s:5:\"porch\";s:0:\"\";s:3:\"apt\";s:0:\"\";}s:1:\"B\";a:11:{s:4:\"city\";s:29:\"Санкт-Петербург\";s:7:\"city_id\";s:6:\"210862\";s:6:\"street\";s:33:\"посёлок Парголово\";s:5:\"house\";s:0:\"\";s:3:\"lat\";s:9:\"60.080195\";s:3:\"lon\";s:9:\"30.264393\";s:7:\"parking\";s:0:\"\";s:10:\"parking_id\";s:0:\"\";s:7:\"housing\";s:0:\"\";s:5:\"porch\";s:0:\"\";s:3:\"apt\";s:0:\"\";}}'
        ], [
            'tenant_id' => 68,
            'order_number' => 31230
        ])) {
            echo 'заказ обновлен' . PHP_EOL;
        } else {
            echo 'заказ не обновлен' . PHP_EOL;
        }
    }

    public function process3()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand()->update(Order::tableName(), [
            'address' => 'a:2:{s:1:\"A\";a:11:{s:4:\"city\";s:29:\"Санкт-Петербург\";s:7:\"city_id\";s:6:\"210862\";s:6:\"street\";s:39:\"Аптекарский проспект\";s:5:\"house\";s:0:\"\";s:3:\"lat\";s:9:\"59.972406\";s:3:\"lon\";s:8:\"30.32012\";s:7:\"parking\";s:55:\"Базовый район (границы города)\";s:10:\"parking_id\";s:3:\"792\";s:7:\"housing\";s:0:\"\";s:5:\"porch\";s:0:\"\";s:3:\"apt\";s:0:\"\";}s:1:\"B\";a:11:{s:4:\"city\";s:29:\"Санкт-Петербург\";s:7:\"city_id\";s:6:\"210862\";s:6:\"street\";s:33:\"посёлок Парголово\";s:5:\"house\";s:0:\"\";s:3:\"lat\";s:9:\"60.080195\";s:3:\"lon\";s:9:\"30.264393\";s:7:\"parking\";s:0:\"\";s:10:\"parking_id\";s:0:\"\";s:7:\"housing\";s:0:\"\";s:5:\"porch\";s:0:\"\";s:3:\"apt\";s:0:\"\";}}'
        ], [
            'tenant_id' => 68,
            'order_number' => 31466
        ])) {
            echo 'заказ обновлен' . PHP_EOL;
        } else {
            echo 'заказ не обновлен' . PHP_EOL;
        }
    }

    public function process4()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand()->update(Order::tableName(), [
            'address' => 'a:2:{s:1:\"A\";a:11:{s:4:\"city\";s:29:\"Санкт-Петербург\";s:7:\"city_id\";s:6:\"210862\";s:6:\"street\";s:39:\"Аптекарский проспект\";s:5:\"house\";s:0:\"\";s:3:\"lat\";s:9:\"59.972406\";s:3:\"lon\";s:8:\"30.32012\";s:7:\"parking\";s:55:\"Базовый район (границы города)\";s:10:\"parking_id\";s:3:\"792\";s:7:\"housing\";s:0:\"\";s:5:\"porch\";s:0:\"\";s:3:\"apt\";s:0:\"\";}s:1:\"B\";a:11:{s:4:\"city\";s:29:\"Санкт-Петербург\";s:7:\"city_id\";s:6:\"210862\";s:6:\"street\";s:33:\"посёлок Парголово\";s:5:\"house\";s:0:\"\";s:3:\"lat\";s:9:\"60.080195\";s:3:\"lon\";s:9:\"30.264393\";s:7:\"parking\";s:0:\"\";s:10:\"parking_id\";s:0:\"\";s:7:\"housing\";s:0:\"\";s:5:\"porch\";s:0:\"\";s:3:\"apt\";s:0:\"\";}}'
        ], [
            'tenant_id' => 68,
            'order_number' => 31768
        ])) {
            echo 'заказ обновлен' . PHP_EOL;
        } else {
            echo 'заказ не обновлен' . PHP_EOL;
        }
    }

    public function process5()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand()->update(Order::tableName(), [
            'address' => 'a:2:{s:1:\"A\";a:11:{s:4:\"city\";s:29:\"Санкт-Петербург\";s:7:\"city_id\";s:6:\"210862\";s:6:\"street\";s:39:\"Аптекарский проспект\";s:5:\"house\";s:0:\"\";s:3:\"lat\";s:9:\"59.972406\";s:3:\"lon\";s:8:\"30.32012\";s:7:\"parking\";s:55:\"Базовый район (границы города)\";s:10:\"parking_id\";s:3:\"792\";s:7:\"housing\";s:0:\"\";s:5:\"porch\";s:0:\"\";s:3:\"apt\";s:0:\"\";}s:1:\"B\";a:11:{s:4:\"city\";s:29:\"Санкт-Петербург\";s:7:\"city_id\";s:6:\"210862\";s:6:\"street\";s:33:\"посёлок Парголово\";s:5:\"house\";s:0:\"\";s:3:\"lat\";s:9:\"60.080195\";s:3:\"lon\";s:9:\"30.264393\";s:7:\"parking\";s:0:\"\";s:10:\"parking_id\";s:0:\"\";s:7:\"housing\";s:0:\"\";s:5:\"porch\";s:0:\"\";s:3:\"apt\";s:0:\"\";}}'
        ], [
            'tenant_id' => 68,
            'order_number' => 32026
        ])) {
            echo 'заказ обновлен' . PHP_EOL;
        } else {
            echo 'заказ не обновлен' . PHP_EOL;
        }
    }

    public function process6()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand()->update(Order::tableName(), [
            'address' => 'a:2:{s:1:\"A\";a:11:{s:4:\"city\";s:29:\"Санкт-Петербург\";s:7:\"city_id\";s:6:\"210862\";s:6:\"street\";s:39:\"Аптекарский проспект\";s:5:\"house\";s:0:\"\";s:3:\"lat\";s:9:\"59.972406\";s:3:\"lon\";s:8:\"30.32012\";s:7:\"parking\";s:55:\"Базовый район (границы города)\";s:10:\"parking_id\";s:3:\"792\";s:7:\"housing\";s:0:\"\";s:5:\"porch\";s:0:\"\";s:3:\"apt\";s:0:\"\";}s:1:\"B\";a:11:{s:4:\"city\";s:29:\"Санкт-Петербург\";s:7:\"city_id\";s:6:\"210862\";s:6:\"street\";s:33:\"посёлок Парголово\";s:5:\"house\";s:0:\"\";s:3:\"lat\";s:9:\"60.080195\";s:3:\"lon\";s:9:\"30.264393\";s:7:\"parking\";s:0:\"\";s:10:\"parking_id\";s:0:\"\";s:7:\"housing\";s:0:\"\";s:5:\"porch\";s:0:\"\";s:3:\"apt\";s:0:\"\";}}'
        ], [
            'tenant_id' => 68,
            'order_number' => 32283
        ])) {
            echo 'заказ обновлен' . PHP_EOL;
        } else {
            echo 'заказ не обновлен' . PHP_EOL;
        }
    }

    public function process7()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand()->update(Order::tableName(), [
            'address' => 'a:2:{s:1:\"A\";a:11:{s:4:\"city\";s:29:\"Санкт-Петербург\";s:7:\"city_id\";s:6:\"210862\";s:6:\"street\";s:39:\"Аптекарский проспект\";s:5:\"house\";s:0:\"\";s:3:\"lat\";s:9:\"59.972406\";s:3:\"lon\";s:8:\"30.32012\";s:7:\"parking\";s:55:\"Базовый район (границы города)\";s:10:\"parking_id\";s:3:\"792\";s:7:\"housing\";s:0:\"\";s:5:\"porch\";s:0:\"\";s:3:\"apt\";s:0:\"\";}s:1:\"B\";a:11:{s:4:\"city\";s:29:\"Санкт-Петербург\";s:7:\"city_id\";s:6:\"210862\";s:6:\"street\";s:33:\"посёлок Парголово\";s:5:\"house\";s:0:\"\";s:3:\"lat\";s:9:\"60.080195\";s:3:\"lon\";s:9:\"30.264393\";s:7:\"parking\";s:0:\"\";s:10:\"parking_id\";s:0:\"\";s:7:\"housing\";s:0:\"\";s:5:\"porch\";s:0:\"\";s:3:\"apt\";s:0:\"\";}}'
        ], [
            'tenant_id' => 68,
            'order_number' => 32528
        ])) {
            echo 'заказ обновлен' . PHP_EOL;
        } else {
            echo 'заказ не обновлен' . PHP_EOL;
        }
    }

    public function process8()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand()->update(Order::tableName(), [
            'address' => 'a:2:{s:1:\"A\";a:11:{s:4:\"city\";s:29:\"Санкт-Петербург\";s:7:\"city_id\";s:6:\"210862\";s:6:\"street\";s:39:\"Аптекарский проспект\";s:5:\"house\";s:0:\"\";s:3:\"lat\";s:9:\"59.972406\";s:3:\"lon\";s:8:\"30.32012\";s:7:\"parking\";s:55:\"Базовый район (границы города)\";s:10:\"parking_id\";s:3:\"792\";s:7:\"housing\";s:0:\"\";s:5:\"porch\";s:0:\"\";s:3:\"apt\";s:0:\"\";}s:1:\"B\";a:11:{s:4:\"city\";s:29:\"Санкт-Петербург\";s:7:\"city_id\";s:6:\"210862\";s:6:\"street\";s:33:\"посёлок Парголово\";s:5:\"house\";s:0:\"\";s:3:\"lat\";s:9:\"60.080195\";s:3:\"lon\";s:9:\"30.264393\";s:7:\"parking\";s:0:\"\";s:10:\"parking_id\";s:0:\"\";s:7:\"housing\";s:0:\"\";s:5:\"porch\";s:0:\"\";s:3:\"apt\";s:0:\"\";}}'
        ], [
            'tenant_id' => 68,
            'order_number' => 32706
        ])) {
            echo 'заказ обновлен' . PHP_EOL;
        } else {
            echo 'заказ не обновлен' . PHP_EOL;
        }
    }

    public function process9()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand()->update(Order::tableName(), [
            'address' => 'a:2:{s:1:\"A\";a:11:{s:4:\"city\";s:29:\"Санкт-Петербург\";s:7:\"city_id\";s:6:\"210862\";s:6:\"street\";s:39:\"Аптекарский проспект\";s:5:\"house\";s:0:\"\";s:3:\"lat\";s:9:\"59.972406\";s:3:\"lon\";s:8:\"30.32012\";s:7:\"parking\";s:55:\"Базовый район (границы города)\";s:10:\"parking_id\";s:3:\"792\";s:7:\"housing\";s:0:\"\";s:5:\"porch\";s:0:\"\";s:3:\"apt\";s:0:\"\";}s:1:\"B\";a:11:{s:4:\"city\";s:29:\"Санкт-Петербург\";s:7:\"city_id\";s:6:\"210862\";s:6:\"street\";s:33:\"посёлок Парголово\";s:5:\"house\";s:0:\"\";s:3:\"lat\";s:9:\"60.080195\";s:3:\"lon\";s:9:\"30.264393\";s:7:\"parking\";s:0:\"\";s:10:\"parking_id\";s:0:\"\";s:7:\"housing\";s:0:\"\";s:5:\"porch\";s:0:\"\";s:3:\"apt\";s:0:\"\";}}'
        ], [
            'tenant_id' => 68,
            'order_number' => 32917
        ])) {
            echo 'заказ обновлен' . PHP_EOL;
        } else {
            echo 'заказ не обновлен' . PHP_EOL;
        }
    }

    public function process10()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand()->update(Order::tableName(), [
            'address' => 'a:2:{s:1:\"A\";a:11:{s:4:\"city\";s:29:\"Санкт-Петербург\";s:7:\"city_id\";s:6:\"210862\";s:6:\"street\";s:39:\"Аптекарский проспект\";s:5:\"house\";s:0:\"\";s:3:\"lat\";s:9:\"59.972406\";s:3:\"lon\";s:8:\"30.32012\";s:7:\"parking\";s:55:\"Базовый район (границы города)\";s:10:\"parking_id\";s:3:\"792\";s:7:\"housing\";s:0:\"\";s:5:\"porch\";s:0:\"\";s:3:\"apt\";s:0:\"\";}s:1:\"B\";a:11:{s:4:\"city\";s:29:\"Санкт-Петербург\";s:7:\"city_id\";s:6:\"210862\";s:6:\"street\";s:33:\"посёлок Парголово\";s:5:\"house\";s:0:\"\";s:3:\"lat\";s:9:\"60.080195\";s:3:\"lon\";s:9:\"30.264393\";s:7:\"parking\";s:0:\"\";s:10:\"parking_id\";s:0:\"\";s:7:\"housing\";s:0:\"\";s:5:\"porch\";s:0:\"\";s:3:\"apt\";s:0:\"\";}}'
        ], [
            'tenant_id' => 68,
            'order_number' => 33112
        ])) {
            echo 'заказ обновлен' . PHP_EOL;
        } else {
            echo 'заказ не обновлен' . PHP_EOL;
        }
    }

}