<?php

namespace console\components\testsForUpdate;


class InsertWithAlterTable implements Test
{

    protected $sql = <<<SQL

INSERT INTO `tbl_order` 
(`city_id`, `tenant_id`, `promo_code_id`, `phone`, `client_id`, `address`, `comment`, `payment`, `bonus_payment`, `position_id`, `tariff_id`, `predv_price`, `predv_distance`, `predv_time`, `predv_price_no_discount`, `is_fix`, `parking_id`, `company_id`, `device`, `status_id`, `create_time`, `update_time`, `user_create`, `order_number`, `order_time`, `user_modifed`, `status_time`, `currency_id`, `time_offset`) 
VALUES 
(210862, 68, NULL, '32453463634', 4259, 'a:2:{s:1:\"A\";a:11:{s:4:\"city\";s:29:\"Санкт-Петербург\";s:7:\"city_id\";s:6:\"210862\";s:6:\"street\";s:39:\"Аптекарский проспект\";s:5:\"house\";s:0:\"\";s:3:\"lat\";s:9:\"59.972406\";s:3:\"lon\";s:8:\"30.32012\";s:7:\"parking\";s:55:\"Базовый район (границы города)\";s:10:\"parking_id\";s:3:\"792\";s:7:\"housing\";s:0:\"\";s:5:\"porch\";s:0:\"\";s:3:\"apt\";s:0:\"\";}s:1:\"B\";a:11:{s:4:\"city\";s:29:\"Санкт-Петербург\";s:7:\"city_id\";s:6:\"210862\";s:6:\"street\";s:33:\"посёлок Парголово\";s:5:\"house\";s:0:\"\";s:3:\"lat\";s:9:\"60.080195\";s:3:\"lon\";s:9:\"30.264393\";s:7:\"parking\";s:0:\"\";s:10:\"parking_id\";s:0:\"\";s:7:\"housing\";s:0:\"\";s:5:\"porch\";s:0:\"\";s:3:\"apt\";s:0:\"\";}}', '', 'CASH', 0, 1, 469, '81.72', '13.8', 29, NULL, 0, 792, NULL, 'DISPATCHER', 1, 1524120378, 1524120378, 943, (SELECT * FROM (SELECT MAX(order_number) FROM tbl_order WHERE tenant_id = 68 FOR UPDATE) AS t) + 1, 1524131478, 943, 1524120378, 1, 10800) 

SQL;

    protected $sql2 = <<<SQL

INSERT INTO `tbl_order` 
(`city_id`, `tenant_id`, `promo_code_id`, `phone`, `client_id`, `address`, `comment`, `payment`, `bonus_payment`, `position_id`, `tariff_id`, `predv_price`, `predv_distance`, `predv_time`, `predv_price_no_discount`, `is_fix`, `parking_id`, `company_id`, `device`, `status_id`, `create_time`, `update_time`, `user_create`, `order_number`, `order_time`, `user_modifed`, `status_time`, `currency_id`, `time_offset`) 
VALUES 
(210862, 552, NULL, '32453463634', 4259, 'a:2:{s:1:\"A\";a:11:{s:4:\"city\";s:29:\"Санкт-Петербург\";s:7:\"city_id\";s:6:\"210862\";s:6:\"street\";s:39:\"Аптекарский проспект\";s:5:\"house\";s:0:\"\";s:3:\"lat\";s:9:\"59.972406\";s:3:\"lon\";s:8:\"30.32012\";s:7:\"parking\";s:55:\"Базовый район (границы города)\";s:10:\"parking_id\";s:3:\"792\";s:7:\"housing\";s:0:\"\";s:5:\"porch\";s:0:\"\";s:3:\"apt\";s:0:\"\";}s:1:\"B\";a:11:{s:4:\"city\";s:29:\"Санкт-Петербург\";s:7:\"city_id\";s:6:\"210862\";s:6:\"street\";s:33:\"посёлок Парголово\";s:5:\"house\";s:0:\"\";s:3:\"lat\";s:9:\"60.080195\";s:3:\"lon\";s:9:\"30.264393\";s:7:\"parking\";s:0:\"\";s:10:\"parking_id\";s:0:\"\";s:7:\"housing\";s:0:\"\";s:5:\"porch\";s:0:\"\";s:3:\"apt\";s:0:\"\";}}', '', 'CASH', 0, 1, 469, '81.72', '13.8', 29, NULL, 0, 792, NULL, 'DISPATCHER', 1, 1524120378, 1524120378, 943, (SELECT * FROM (SELECT MAX(order_number) FROM tbl_order WHERE tenant_id = 552 FOR UPDATE) AS t) + 1, 1524131478, 943, 1524120378, 1, 10800) 

SQL;

    protected $sleep = 1;

    public function countProcess()
    {
        return 20;
    }


    public function process1()
    {
        echo 'Начало изменения колонок' . PHP_EOL;
        \Yii::$app->db->createCommand()->addColumn('tbl_order', 'test', 'INTEGER')->execute();
        echo 'колонка создана' . PHP_EOL;
        \Yii::$app->db->createCommand()->dropColumn('tbl_order', 'test')->execute();
        echo 'колонка удалена' . PHP_EOL;
    }

    public function process2()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand($this->sql)->execute()) {
            echo 'заказ создан' . PHP_EOL;
        } else {
            echo 'заказ на создан' . PHP_EOL;
        }
    }

    public function process3()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand($this->sql)->execute()) {
            echo 'заказ создан' . PHP_EOL;
        } else {
            echo 'заказ на создан' . PHP_EOL;
        }
    }

    public function process4()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand($this->sql)->execute()) {
            echo 'заказ создан' . PHP_EOL;
        } else {
            echo 'заказ на создан' . PHP_EOL;
        }
    }

    public function process5()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand($this->sql)->execute()) {
            echo 'заказ создан' . PHP_EOL;
        } else {
            echo 'заказ на создан' . PHP_EOL;
        }
    }

    public function process6()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand($this->sql)->execute()) {
            echo 'заказ создан' . PHP_EOL;
        } else {
            echo 'заказ на создан' . PHP_EOL;
        }
    }

    public function process7()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand($this->sql)->execute()) {
            echo 'заказ создан' . PHP_EOL;
        } else {
            echo 'заказ на создан' . PHP_EOL;
        }
    }

    public function process8()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand($this->sql)->execute()) {
            echo 'заказ создан' . PHP_EOL;
        } else {
            echo 'заказ на создан' . PHP_EOL;
        }
    }

    public function process9()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand($this->sql)->execute()) {
            echo 'заказ создан' . PHP_EOL;
        } else {
            echo 'заказ на создан' . PHP_EOL;
        }
    }

    public function process10()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand($this->sql2)->execute()) {
            echo 'заказ создан' . PHP_EOL;
        } else {
            echo 'заказ на создан' . PHP_EOL;
        }
    }
    public function process11()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand($this->sql2)->execute()) {
            echo 'заказ создан' . PHP_EOL;
        } else {
            echo 'заказ на создан' . PHP_EOL;
        }
    }

    public function process12()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand($this->sql2)->execute()) {
            echo 'заказ создан' . PHP_EOL;
        } else {
            echo 'заказ на создан' . PHP_EOL;
        }
    }

    public function process13()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand($this->sql2)->execute()) {
            echo 'заказ создан' . PHP_EOL;
        } else {
            echo 'заказ на создан' . PHP_EOL;
        }
    }

    public function process14()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand($this->sql2)->execute()) {
            echo 'заказ создан' . PHP_EOL;
        } else {
            echo 'заказ на создан' . PHP_EOL;
        }
    }

    public function process15()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand($this->sql2)->execute()) {
            echo 'заказ создан' . PHP_EOL;
        } else {
            echo 'заказ на создан' . PHP_EOL;
        }
    }

    public function process16()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand($this->sql2)->execute()) {
            echo 'заказ создан' . PHP_EOL;
        } else {
            echo 'заказ на создан' . PHP_EOL;
        }
    }

    public function process17()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand($this->sql2)->execute()) {
            echo 'заказ создан' . PHP_EOL;
        } else {
            echo 'заказ на создан' . PHP_EOL;
        }
    }

    public function process18()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand($this->sql2)->execute()) {
            echo 'заказ создан' . PHP_EOL;
        } else {
            echo 'заказ на создан' . PHP_EOL;
        }
    }

    public function process19()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand($this->sql2)->execute()) {
            echo 'заказ создан' . PHP_EOL;
        } else {
            echo 'заказ на создан' . PHP_EOL;
        }
    }

    public function process20()
    {
        sleep($this->sleep);
        if (\Yii::$app->db->createCommand($this->sql2)->execute()) {
            echo 'заказ создан' . PHP_EOL;
        } else {
            echo 'заказ на создан' . PHP_EOL;
        }
    }
}