<?php

namespace console\components\testsForUpdate;


use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\ClientException;

class CreateOrderBySite implements Test
{

    /* @var $httpClient Client */
    protected $httpClient;


    protected $order = [
        '_csrf' => 'VWI65MOjjS89SroPjfTKad10wmWpCFtwZLAZYlcBia0uUOV9XYumDbYRqqkdRSxHEcYpHJ6uwGBwgRWTGa0rcw==',
        'Order' => [
            'city_id'        => '210862',
            'order_now'      => '1',
            'order_date'     => '2018-04-20',
            'order_hours'    => '14',
            'order_minutes'  => '15',
            'phone'          => '2342424323423',
            'address'        => 'a:2:{s:1:"A";a:11:{s:4:"city";s:29:"Санкт-Петербург";s:7:"city_id";s:6:"210862";s:6:"street";s:39:"Аптекарский проспект";s:5:"house";s:0:"";s:3:"lat";s:9:"59.972406";s:3:"lon";s:8:"30.32012";s:7:"parking";s:55:"Базовый район (границы города)";s:10:"parking_id";s:3:"792";s:7:"housing";s:0:"";s:5:"porch";s:0:"";s:3:"apt";s:0:"";}s:1:"B";a:11:{s:4:"city";s:29:"Санкт-Петербург";s:7:"city_id";s:6:"210862";s:6:"street";s:33:"посёлок Парголово";s:5:"house";s:0:"";s:3:"lat";s:9:"60.080195";s:3:"lon";s:9:"30.264393";s:7:"parking";s:0:"";s:10:"parking_id";s:0:"";s:7:"housing";s:0:"";s:5:"porch";s:0:"";s:3:"apt";s:0:"";}}',
            'payment'        => 'CASH',
            'bonus_payment'  => '0',
            'position_id'    => '1',
            'tariff_id'      => '469',
            'predv_price'    => '50',
            'predv_distance' => '0',
            'predv_time'     => '0',
            'is_fix'         => '0',
            'orderAction'    => 'NEW_ORDER',
            'device'         => 'DISPATCHER',
        ],
    ];

    public function __construct()
    {

        $cookieJar = CookieJar::fromArray([
            'Cookie'           => '_ym_uid=1517293265974226351; _ga=GA1.2.1265043808.1518088799; _csrf=7bc414ef739b0a4e99197194cf4e6ee203deb9ac084562e23ca4e274c9cde38fa%3A2%3A%7Bi%3A0%3Bs%3A5%3A%22_csrf%22%3Bi%3A1%3Bs%3A32%3A%22%7B2%DF%99%9E%28%2B%22%8B%5B%10%A6%90%B1%E6.%CC%B2%EBy7%A6%9B%10%141%0C%F1N%AC%A2%DE%22%3B%7D; PHPSESSID=kn54g6tmf2ui55bjfq40s4lmd4',
            'X-CSRF-Token'     => '0k_ZzVtl11cCpG-Ou-s04t3LaDyr6kKtjxhgHcPuGT6pfQZUxU38dYn_fygrWtLMEXmDRZxM2b2bKWzsjUK74A==',
            'X-Requested-With' => 'XMLHttpRequest',
            'Content-Type'     => 'application/x-www-form-urlencoded; charset=UTF-8',
            'Accept'           => 'application/json, text/javascript, */*; q=0.01',
        ], 'mavellol.mavellol.taxi.lcl');

        $this->httpClient = \Yii::createObject(Client::class, [
            [
                'base_uri'          => 'http://mavellol.mavellol.taxi.lcl',
                'timeout'           => 20,
                'connectionTimeout' => 20,
                'cookies'           => $cookieJar,
            ],
        ]);


    }

    public function countProcess()
    {
        return 20;
    }


    public function process1()
    {
        try {
            $response = $this->httpClient->post('order/create', [
                'form_params' => $this->order
            ]);

            echo $response->getBody()->getContents() . PHP_EOL;
        } catch (ClientException $e) {
            echo $e->getMessage();
        }
    }

    public function process2()
    {
        try {
            $response = $this->httpClient->post('order/create', [
                'form_params' => $this->order
            ]);

            echo $response->getBody()->getContents() . PHP_EOL;
        } catch (ClientException $e) {
            echo $e->getMessage();
        }
    }

    public function process3()
    {
        try {
            $response = $this->httpClient->post('order/create', [
                'form_params' => $this->order
            ]);

            echo $response->getBody()->getContents() . PHP_EOL;
        } catch (ClientException $e) {
            echo $e->getMessage();
        }
    }

    public function process4()
    {
        try {
            $response = $this->httpClient->post('order/create', [
                'form_params' => $this->order
            ]);

            echo $response->getBody()->getContents() . PHP_EOL;
        } catch (ClientException $e) {
            echo $e->getMessage();
        }
    }

    public function process5()
    {
        try {
            $response = $this->httpClient->post('order/create', [
                'form_params' => $this->order
            ]);

            echo $response->getBody()->getContents() . PHP_EOL;
        } catch (ClientException $e) {
            echo $e->getMessage();
        }
    }

    public function process6()
    {
        try {
            $response = $this->httpClient->post('order/create', [
                'form_params' => $this->order
            ]);

            echo $response->getBody()->getContents() . PHP_EOL;
        } catch (ClientException $e) {
            echo $e->getMessage();
        }
    }

    public function process7()
    {
        try {
            $response = $this->httpClient->post('order/create', [
                'form_params' => $this->order
            ]);

            echo $response->getBody()->getContents() . PHP_EOL;
        } catch (ClientException $e) {
            echo $e->getMessage();
        }
    }

    public function process8()
    {
        try {
            $response = $this->httpClient->post('order/create', [
                'form_params' => $this->order
            ]);

            echo $response->getBody()->getContents() . PHP_EOL;
        } catch (ClientException $e) {
            echo $e->getMessage();
        }
    }

    public function process9()
    {
        try {
            $response = $this->httpClient->post('order/create', [
                'form_params' => $this->order
            ]);

            echo $response->getBody()->getContents() . PHP_EOL;
        } catch (ClientException $e) {
            echo $e->getMessage();
        }
    }

    public function process10()
    {
        try {
            $response = $this->httpClient->post('order/create', [
                'form_params' => $this->order
            ]);

            echo $response->getBody()->getContents() . PHP_EOL;
        } catch (ClientException $e) {
            echo $e->getMessage();
        }
    }
    public function process11()
    {
        try {
            $response = $this->httpClient->post('order/create', [
                'form_params' => $this->order
            ]);

            echo $response->getBody()->getContents() . PHP_EOL;
        } catch (ClientException $e) {
            echo $e->getMessage();
        }
    }

    public function process12()
    {
        try {
            $response = $this->httpClient->post('order/create', [
                'form_params' => $this->order
            ]);

            echo $response->getBody()->getContents() . PHP_EOL;
        } catch (ClientException $e) {
            echo $e->getMessage();
        }
    }

    public function process13()
    {
        try {
            $response = $this->httpClient->post('order/create', [
                'form_params' => $this->order
            ]);

            echo $response->getBody()->getContents() . PHP_EOL;
        } catch (ClientException $e) {
            echo $e->getMessage();
        }
    }

    public function process14()
    {
        try {
            $response = $this->httpClient->post('order/create', [
                'form_params' => $this->order
            ]);

            echo $response->getBody()->getContents() . PHP_EOL;
        } catch (ClientException $e) {
            echo $e->getMessage();
        }
    }

    public function process15()
    {
        try {
            $response = $this->httpClient->post('order/create', [
                'form_params' => $this->order
            ]);

            echo $response->getBody()->getContents() . PHP_EOL;
        } catch (ClientException $e) {
            echo $e->getMessage();
        }
    }

    public function process16()
    {
        try {
            $response = $this->httpClient->post('order/create', [
                'form_params' => $this->order
            ]);

            echo $response->getBody()->getContents() . PHP_EOL;
        } catch (ClientException $e) {
            echo $e->getMessage();
        }
    }

    public function process17()
    {
        try {
            $response = $this->httpClient->post('order/create', [
                'form_params' => $this->order
            ]);

            echo $response->getBody()->getContents() . PHP_EOL;
        } catch (ClientException $e) {
            echo $e->getMessage();
        }
    }

    public function process18()
    {
        try {
            $response = $this->httpClient->post('order/create', [
                'form_params' => $this->order
            ]);

            echo $response->getBody()->getContents() . PHP_EOL;
        } catch (ClientException $e) {
            echo $e->getMessage();
        }
    }

    public function process19()
    {
        try {
            $response = $this->httpClient->post('order/create', [
                'form_params' => $this->order
            ]);

            echo $response->getBody()->getContents() . PHP_EOL;
        } catch (ClientException $e) {
            echo $e->getMessage();
        }
    }

    public function process20()
    {
        try {
            $response = $this->httpClient->post('order/create', [
                'form_params' => $this->order
            ]);

            echo $response->getBody()->getContents() . PHP_EOL;
        } catch (ClientException $e) {
            echo $e->getMessage();
        }
    }


}