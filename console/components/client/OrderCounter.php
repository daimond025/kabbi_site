<?php

namespace console\components\client;

use Yii;
use yii\base\Object;
use yii\db\Query;

class OrderCounter extends Object
{
    /**
     * @var array
     */
    public $completedStatuses;
    /**
     * @var array
     */
    public $rejectedStatusesByClient;
    /**
     * @var array
     */
    public $rejectedStatusesByWorker;
    /**
     * @var array
     */
    public $rejectedStatusesByDispatcher;

    /**
     * @var string
     */
    public $clientTblName = '{{%client}}';

    /**
     * @var string
     */
    public $orderTblName = '{{%order}}';

    /**
     * @var int
     */
    public $clientBatchSize = 500;

    public function count($clientId = null, $tenantId = null)
    {
        if (empty($clientId)) {
            $query = (new Query())
                ->select('client_id')
                ->from($this->clientTblName)
                ->filterWhere(['tenant_id' => $tenantId])
                ->orderBy('client_id');

            foreach ($query->each($this->clientBatchSize) as $client) {
                $this->updateCount($client['client_id']);
            }
        } else {
            $this->updateCount($clientId);
        }
    }

    private function updateCount($clientId)
    {
        return Yii::$app->db->createCommand()
            ->update($this->clientTblName, [
                'success_order'         => $this->getCount($clientId, $this->completedStatuses),
                'fail_worker_order'     => $this->getCount($clientId, $this->rejectedStatusesByWorker),
                'fail_client_order'     => $this->getCount($clientId, $this->rejectedStatusesByClient),
                'fail_dispatcher_order' => $this->getCount($clientId, $this->rejectedStatusesByDispatcher),
            ], ['client_id' => $clientId])
            ->execute();
    }

    private function getCount($clientId, array $statuses)
    {
        return (new Query())
            ->from($this->orderTblName)
            ->where(['client_id' => $clientId, 'status_id' => $statuses])
            ->count();
    }
}
