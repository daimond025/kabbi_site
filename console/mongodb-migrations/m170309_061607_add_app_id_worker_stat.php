<?php

use console\models\WorkerStat;
use yii\db\Query;

class m170309_061607_add_app_id_worker_stat extends \yii\mongodb\Migration
{
    protected $_apps = [];

    public function up()
    {
        $orders = WorkerStat::find()->all();

        /** @var WorkerStat $order */
        foreach ($orders as $order) {
            $appId = $this->getAppIdByTenantId($order['tenant_id']);
            if (!empty($appId)) {
                $statistics = $order->statistics;

                $statistics['received']['device']['IOS'] = [$appId => $statistics['received']['device']['IOS']];
                $statistics['received']['device']['ANDROID'] = [$appId => $statistics['received']['device']['ANDROID']];

                $statistics['pre_order']['device']['IOS'] = [$appId => $statistics['pre_order']['device']['IOS']];
                $statistics['pre_order']['device']['ANDROID'] = [$appId => $statistics['pre_order']['device']['ANDROID']];

                $statistics['completed']['device']['IOS'] = [$appId => $statistics['completed']['device']['IOS']];
                $statistics['completed']['device']['ANDROID'] = [$appId => $statistics['completed']['device']['ANDROID']];

                $statistics['rejected']['device']['IOS'] = [$appId => $statistics['rejected']['device']['IOS']];
                $statistics['rejected']['device']['ANDROID'] = [$appId => $statistics['rejected']['device']['ANDROID']];

                $order->statistics = $statistics;

                $order->save(false, ['statistics']);
            }
        }
    }

    public function down()
    {
        echo "m170309_061607_add_app_id_worker_stat cannot be reverted.\n";

        return false;
    }

    protected function getAppIdByTenantId($tenantId)
    {
        if (!array_key_exists($tenantId, $this->_apps)) {
            $this->_apps[$tenantId] = (new Query())
                ->select('app_id')
                ->from('{{%mobile_app}}')
                ->where(['tenant_id' => $tenantId])
                ->limit(1)
                ->scalar();
        }

        return $this->_apps[$tenantId];
    }
}
