<?php

use console\models\OrderStat;

class m160621_045156_order_statistic extends \yii\mongodb\Migration
{
    const POSITION_ID = 1;

    public function up()
    {
        $records = OrderStat::find()->all();

        foreach ($records as $record) {

            $record['position_id'] = self::POSITION_ID;

            $record->save(false);
        }
    }

    public function down()
    {
        $records = OrderStat::find()->all();

        foreach ($records as $record) {
            $record['position_id'] = null;
            $record->save(false);
        }
    }
}
