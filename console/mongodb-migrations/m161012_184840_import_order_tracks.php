<?php

use common\modules\order\models\ar\mongodb\OrderTrack;
use yii\helpers\Console;

class m161012_184840_import_order_tracks extends \yii\mongodb\Migration
{
    const COLLECTION_ORDER_TRACK = 'orderTrack';
    const COLUMN_ORDER_ID = 'order_id';
    const BATCH_SIZE = 100;

    public function up()
    {
        if (isset(\Yii::$app->log->targets[0])) {
            \Yii::$app->log->targets[0]->enabled = false;
        }

        $startTime = time();

        try {
            $this->dropCollection(self::COLLECTION_ORDER_TRACK);
        } catch (\Exception $ex) {
        }

        $this->createCollection(self::COLLECTION_ORDER_TRACK);


        $count = (new \yii\db\Query())
            ->from('tbl_order_track')
            ->count();

        $pageCount = floor($count / self::BATCH_SIZE);

        $invalidTracks = [];

        Console::startProgress(0, $count, 'Import records: ', false);
        for ($i = 0; $i <= $pageCount; $i++) {
            $records = (new \yii\db\Query())
                ->select(['order_id', 'tracking', 'time'])
                ->from('tbl_order_track')
                ->limit(self::BATCH_SIZE)
                ->offset($i * self::BATCH_SIZE)
                ->all();

            foreach ($records as $record) {
                $tracking = json_decode($record['tracking'], true);

                if (is_array($tracking['order_route'])) {
                    $orderRoute = array_map(function ($item) {
                        if (isset($item['lat'])) {
                            $item['lat'] = (string)round($item['lat'], 6);
                        }

                        if (isset($item['lat'])) {
                            $item['lon'] = (string)round($item['lon'], 6);
                        }

                        return $item;
                    }, $tracking['order_route']);

                    $track = new OrderTrack([
                        'order_id' => $record['order_id'],
                        'tracking' => $orderRoute,
                        'time'     => $record['time'],
                    ]);
                    $track->save();
                } else {
                    $invalidTracks[] = $record['order_id'];
                }
            }

            Console::updateProgress($i * self::BATCH_SIZE, $count);
        }
        Console::endProgress('done (' . (time() - $startTime) . ' sec.)' . PHP_EOL);

        Console::output('Invalid order tracks: ' . implode(', ', $invalidTracks));
    }

    public function down()
    {
        $this->dropCollection(self::COLLECTION_ORDER_TRACK);
    }
}
