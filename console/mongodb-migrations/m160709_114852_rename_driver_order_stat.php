<?php

use console\models\OrderStat;

class m160709_114852_rename_driver_order_stat extends \yii\mongodb\Migration
{
    public function up()
    {
        $records = OrderStat::find()->all();

        foreach ($records as $record) {
            $statistics = $record['statistics'];

            $completed = $statistics['completed'];
            $completed['workers'] = $completed['drivers'];
            unset($completed['drivers']);
            $completed['workers']['worker_id'] = $completed['workers']['driver_id'];
            unset($completed['workers']['driver_id']);
            $statistics['completed'] = $completed;

            $rejected = $statistics['rejected'];
            $rejected['workers'] = $rejected['drivers'];
            unset($rejected['drivers']);
            $rejected['workers']['worker_id'] = $rejected['workers']['driver_id'];
            unset($rejected['workers']['driver_id']);
            $statistics['rejected'] = $rejected;

            $record['statistics'] = $statistics;
            $record->save(false);
        }
    }

    public function down()
    {
        echo "m160709_114852_rename_driver_order_stat cannot be reverted.\n";

        return false;
    }
}
