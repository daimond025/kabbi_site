<?php

use console\models\WorkerStat;

class m160622_050806_driver_stat extends \yii\mongodb\Migration
{
    const POSITION_ID = 1;

    public function up()
    {
        $records = WorkerStat::find()->all();
        foreach ($records as $record) {
            $record['position_id'] = self::POSITION_ID;
            $record['worker_id'] = $record['driver_id'];
            $record->save(false);
        }
    }

    public function down()
    {
        $records = WorkerStat::find()->all();

        foreach ($records as $record) {
            $record['position_id'] = null;
            $record['worker_id'] = null;
            $record->save(false);
        }
    }
}
