<?php

use common\modules\tenant\models\DefaultSettings;
use console\models\OrderStat;
use common\modules\tenant\models\TenantSetting;
use frontend\modules\tenant\models\Currency;


class m160118_101849_order_stat__add_column__currency_id extends \yii\mongodb\Migration
{

    public function up()
    {
        $defaultCurrencyCode = DefaultSettings::getValue(DefaultSettings::SETTING_CURRENCY);

        $records = OrderStat::find()->all();

        foreach ($records as $record) {
            $currencyCode = TenantSetting::getSettingValue($record['tenant_id'],
                            DefaultSettings::SETTING_CURRENCY);

            $currencyId = Currency::find()
                    ->select('currency_id')
                    ->where([
                        'code' => empty($currencyCode) ? $defaultCurrencyCode : $currencyCode,
                    ])
                    ->scalar();

            $record['currency_id'] = $currencyId;

            $record->save(false);
        }
    }

    public function down()
    {
        $records = OrderStat::find()->all();

        foreach ($records as $record) {
            $record['currency_id'] = null;
            $record->save(false);
        }
    }

}
