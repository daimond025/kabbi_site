<?php

namespace console\controllers;

use console\components\client\OrderCounter;
use yii\console\Controller;

/**
 * Commands for client manipulations
 * Class ClientController
 * @package console\controllers
 */
class ClientController extends Controller
{
    public $clientId;
    public $tenantId;

    public function options($actionID)
    {
        return ['clientId', 'tenantId'];
    }

    /**
     * Command for updating order counts
     */
    public function actionUpdateOrderCnt()
    {
        /** @var OrderCounter $orderCounter */
        $orderCounter = \Yii::$app->clientOrderCounter;
        $orderCounter->count($this->clientId, $this->tenantId);

        return 1;
    }
}
