<?php

namespace console\controllers;

use console\components\tenantTariff\NotificationService;
use yii\console\Controller;

/**
 * Class TenantTariffController
 * @package console\controllers
 */
class TenantTariffController extends Controller
{
    /**
     * Check tenant tariffs
     */
    public function actionNotifyTariffExpiration()
    {
        /* @var NotificationService */
        $notificationService = \Yii::$app->notificationService;
        $notificationService->notifyTariffExpiration();
    }

}