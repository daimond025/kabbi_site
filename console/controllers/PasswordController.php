<?php
namespace console\controllers;

class PasswordController extends \yii\console\Controller
{
    public function actionGenerate($password)
    {
        echo 'password: ' . \Yii::$app->security->generatePasswordHash($password) . '\n';
        echo 'auth_key: ' . \Yii::$app->security->generateRandomString();
    }
}
