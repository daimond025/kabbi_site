<?php

namespace console\controllers\payment;


use common\modules\tenant\models\TenantHasCity;
use frontend\modules\tenant\models\entities\PaygateProfile;
use yii\base\Module;
use yii\console\Controller;
use yii\db\Connection;
use yii\helpers\ArrayHelper;

class ZotController extends Controller
{

    protected $sberbank_payment_gate_profile = [
        'usernameBinding',
        'passwordBinding',
        'usernamePayment',
        'passwordPayment',
        'url',
        'returnUrl',
        'failUrl',
        'commission',
    ];

    protected $paycom_profile = [
        'apiId',
        'password',
        'isDebug',
        'commission',
    ];

    protected $stripe_payment_gate_profile = [
        'publicKey',
        'privateKey',
        'commission',
    ];

    protected $yandex_payment_gate_profile = [
        'shopId',
        'scid',
        'password',
        'secureDeal',
        'secureDealShopId',
        'secureDealScid',
        'secureDealShopArticleId',
        'secureDealPaymentShopId',
        'secureDealPaymentScid',
        'secureDealPaymentShopArticleId',

        'certificateContent',
        'keyContent',
        'certificatePassword',
        'isDebug',
        'cardBindingSum',
        'commission',
        'useReceipt',
        'product',
        'vat',
    ];

    protected $payment_gate_profile = [
        'username',
        'password',
        'url',
        'returnUrl',
        'failUrl',
        'commission',
        'cardBindingSum',
        'currency',
    ];


    const LIST_TYPES_GATE = [
        'payment_gate_profile',
        'paycom_profile',
        'sberbank_payment_gate_profile',
        'stripe_payment_gate_profile',
        'yandex_payment_gate_profile',
    ];


    protected $profileService;

    public function __construct(
        $id,
        Module $module,
        array $config = []
    ) {
        parent::__construct($id, $module, $config);

        $this->profileService = new ProfileService([
            'baseUrl' => getenv('API_PAYGATE_URL'),
            'timeout' => 15,
            'connectionTimeout' => 15
        ]);

    }


    /**
         * @throws \yii\base\InvalidConfigException
         */
    public function actionRun()
    {

        try {
            app()->set('db2', [
                'class'             => 'yii\db\Connection',
                'charset'           => 'utf8',
                'tablePrefix'       => 'tbl_',
                'dsn'               => getenv('DB_PAYGATE_DSN'),
                'username'          => getenv('DB_PAYGATE_USERNAME'),
                'password'          => getenv('DB_PAYGATE_PASSWORD'),
                'enableSchemaCache' => false,
            ]);
        } catch (\PDOException $e) {
            echo $e->getMessage() . PHP_EOL;
            return false;
        }

        /* @var $dbPaygate Connection */
        $dbPaygate = app()->get('db2');

        try {

            foreach (self::LIST_TYPES_GATE as $type) {

                // Достаем тенантов у которых есть шлюзы
                $tenantIds = $dbPaygate
                    ->createCommand('SELECT paymentName FROM ' . $type)
                    ->queryAll();
                $tenantIds = ArrayHelper::getColumn($tenantIds, 'paymentName');
                $tenantIds = $this->testTenantId($tenantIds);

                // Достаем города у тенантов
                /* @var $tenantHasCity TenantHasCity[] */
                $tenantHasCity = TenantHasCity::find()
                    ->where(['tenant_id' => $tenantIds])
                    ->all();

                // Формируем массив тенантов с массивом их городов
                $dataTenantHasCity = [];
                foreach ($tenantHasCity as $key => $item) {
                    $dataTenantHasCity[$item->tenant_id][] = $item->city_id;
                }

                // Достаем данные шлюзов у тенантов
                $dataProfileTenants = [];
                foreach ($tenantIds as $tenId) {
                    $dataProfileTenants[$tenId] = $dbPaygate
                        ->createCommand("SELECT * FROM $type WHERE paymentName = '$tenId'")
                        ->queryAll();
                }
                $dataProfileTenants = ArrayHelper::getColumn($dataProfileTenants, 0);

                // Сохраняем данные шлюзов
                foreach ($dataTenantHasCity as $tenantId => $cityIds) {

                    $class = $this->getNameClass($type, $dataProfileTenants[$tenantId]);

                    $profile = new $class($this->fillProfileData($dataProfileTenants[$tenantId], $this->$type));

                    foreach ($cityIds as $cityId) {

                        $this->profileService->createProfile($profile, $cityId, $tenantId);

                        echo "Таблица шлюза: $type, тенант: $tenantId город: $cityId сохранен" . PHP_EOL;
                    }
                }
            }
        } catch (\Exception $e) {
            echo $e->getMessage() . PHP_EOL;
            return false;
        }

        return true;
    }


    public function actionRollback()
    {
        try {
            /* @var $paygateProfiles PaygateProfile[] */
            $paygateProfiles = PaygateProfile::find()->all();

            foreach ($paygateProfiles as $paygateProfile) {
                $this->profileService->deleteProfile($paygateProfile->profile);
                echo $paygateProfile->profile . ' удален' . PHP_EOL;
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
            return false;
        }

        return true;

    }

    protected function getNameClass($type, $profile)
    {
        switch ($type) {
            case 'paycom_profile':
                return Paycom::class;

            case 'payment_gate_profile':
                switch ($profile['typeId']) {
                    case Alfabank::TYPE_ID:
                        return Alfabank::class;

                    case Vtb::TYPE_ID:
                        return Vtb::class;
                }

            case 'stripe_payment_gate_profile':
                return Stripe::class;

            case 'yandex_payment_gate_profile':
                return Yandex::class;

            case 'sberbank_payment_gate_profile':
                return Sberbank::class;

        }

        throw new \Exception('Нет данных класса типа: ' . $type);
    }


    protected function testTenantId($tenantIds)
    {
        $newTenantIds = [];
        foreach ($tenantIds as $tenantId) {
            if (ctype_digit($tenantId)) {
                $newTenantIds[] = $tenantId;
            }
        }

        return $newTenantIds;
    }

    protected function fillProfileData($dataProfile, $dataScheme)
    {
        $data = [];

        foreach ($dataScheme as $field) {
            $data[$field] = $dataProfile[$field];
        }

        return $data;
    }


}