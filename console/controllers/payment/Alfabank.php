<?php

namespace console\controllers\payment;

use yii\helpers\ArrayHelper;

/**
 * Class Alfabank
 * @package paymentGate
 */
class Alfabank extends BaseProfile
{
    const TYPE_ID = 1;

    /**
     * @var string
     */
    public $url;

    /**
     * @var string
     */
    public $username;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $returnUrl;

    /**
     * @var string
     */
    public $failUrl;

    /**
     * @var int
     */
    public $cardBindingSum;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->typeId = self::TYPE_ID;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['username', 'password', 'url'], 'required'],
            [['username', 'password'], 'string', 'max' => 32],
            [['url', 'returnUrl', 'failUrl'], 'string', 'max' => 255],
            [['cardBindingSum'], 'integer'],
        ]);
    }

}