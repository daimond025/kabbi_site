<?php

namespace console\controllers\payment;

use common\modules\city\models\City;
use yii\base\Model;

/**
 * Class BaseProfile
 * @package paymentGate\models
 */
abstract class BaseProfile extends Model
{
    /**
     * @var int
     */
    public $typeId;

    /**
     * @var int
     */
    public $city_id;

    /**
     * @var string
     */
    public $paymentName;

    /**
     * @var float
     */
    public $commission;

    /**
     * @var string
     */
    public $currency;

    /**
     * Getting view path
     *
     * @return string
     */
    public function getViewPath()
    {
        $reflector = new \ReflectionClass($this);

        return mb_strtolower($reflector->getShortName());
    }

    /**
     * @inheritdoc
     */
    public function formName()
    {
        return 'Profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['typeId', 'currency'], 'integer'],
            ['commission', 'number', 'min' => 0, 'max' => 100],
            [['paymentName'], 'string', 'max' => 32],
            [
                ['city_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => City::className(),
                'targetAttribute' => ['city_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'typeId'                  => t('bank-card', 'Payment gate type'),
            'paymentName'             => t('bank-card', 'Payment name'),
            'username'                => t('bank-card', 'Username'),
            'password'                => t('bank-card', 'Password'),
            'apiId'                   => t('bank-card', 'ID'),
            'url'                     => t('bank-card', 'Url'),
            'returnUrl'               => t('bank-card', 'Return Url'),
            'failUrl'                 => t('bank-card', 'Fail Url'),
            'commission'              => t('bank-card', 'Commission'),
            'shopId'                  => t('bank-card', 'Shop Id (shopId)'),
            'scid'                    => t('bank-card', 'Showcase Id (scid)'),
            'certificateContent'      => t('bank-card', 'Certificate File'),
            'certificateFile'         => t('bank-card', 'Certificate File'),
            'certificatePassword'     => t('bank-card', 'Certificate Password'),
            'keyContent'              => t('bank-card', 'Key File'),
            'keyFile'                 => t('bank-card', 'Key File'),
            'isDebug'                 => t('bank-card', 'Is Debug'),
            'usernameBinding'         => t('bank-card', 'Username (for binding bank card)'),
            'passwordBinding'         => t('bank-card', 'Password (for binding bank card)'),
            'usernamePayment'         => t('bank-card', 'Username (for payment)'),
            'passwordPayment'         => t('bank-card', 'Password (for payment)'),
            'publicKey'               => t('bank-card', 'Public key'),
            'privateKey'              => t('bank-card', 'Private Key'),
            'secureDeal'              => t('bank-card', 'Secure deal'),
            'agentId'                 => t('bank-card', 'Agent Id for secure deal (agentId)'),
            'secureDealShopId'        => t('bank-card', 'Shop Id for secure deal (shopId)'),
            'secureDealScid'          => t('bank-card', 'Showcase Id for secure deal (scid)'),
            'shopArticleId'           => t('bank-card', 'Product Id (shopArticleId)'),
            'secureDealShopArticleId' => t('bank-card', 'Product Id for secure deal (shopArticleId)'),
            'cardBindingSum'          => t('bank-card', 'Sum of first payment to bind card'),
            'useReceipt'              => t('bank-card', 'Sending data to form a receipt'),
            'product'                 => t('bank-card', 'Product name'),
            'vat'                     => t('bank-card', 'Vat'),
            'currency'                => t('bank-card', 'Currency'),
            'key3dsAdd'               => t('bank-card', 'Key (for binding bank card)'),
            'keyPay'                  => t('bank-card', 'Key (for payment)'),
        ];
    }
}