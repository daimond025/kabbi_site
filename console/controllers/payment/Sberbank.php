<?php

namespace console\controllers\payment;

use yii\helpers\ArrayHelper;

/**
 * Class Sberbank
 * @package paymentGate
 */
class Sberbank extends BaseProfile
{
    const TYPE_ID = 2;

    /**
     * @var string
     */
    public $url;

    /**
     * @var string
     */
    public $returnUrl;

    /**
     * @var string
     */
    public $failUrl;

    /**
     * @var string
     */
    public $usernameBinding;

    /**
     * @var string
     */
    public $passwordBinding;

    /**
     * @var string
     */
    public $usernamePayment;

    /**
     * @var string
     */
    public $passwordPayment;


    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->typeId = self::TYPE_ID;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['usernameBinding', 'passwordBinding', 'usernamePayment', 'passwordPayment', 'url'], 'required'],
            [['usernameBinding', 'usernamePayment'], 'string', 'max' => 32],
            [['passwordBinding', 'passwordPayment'], 'string', 'max' => 255],
            [['url', 'returnUrl', 'failUrl'], 'string', 'max' => 255],
        ]);
    }

}