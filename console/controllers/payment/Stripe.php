<?php

namespace console\controllers\payment;

use yii\helpers\ArrayHelper;

/**
 * Class Stripe
 * @package paymentGate
 */
class Stripe extends BaseProfile
{
    const TYPE_ID = 5;

    /**
     * @var string
     */
    public $publicKey;

    /**
     * @var string
     */
    public $privateKey;


    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->typeId = self::TYPE_ID;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['publicKey', 'privateKey'], 'required'],
            [['publicKey', 'privateKey'], 'string', 'max' => 32],
        ]);
    }

}