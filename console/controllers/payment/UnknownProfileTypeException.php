<?php

namespace console\controllers\payment;

use yii\base\Exception;

/**
 * Class UnknownProfileTypeException
 * @package paymentGate\exceptions
 */
class UnknownProfileTypeException extends Exception
{

}