<?php

namespace console\controllers;


use console\components\testsForUpdate\Factory;
use console\components\testsForUpdate\Test;

use yii\console\Controller;
use yii\db\Exception;

class TestForUpdateController extends Controller
{

    /* @var $test Test */
    protected $test;

    /* @var $factory Factory */
    protected $factory;

    public function init()
    {
        parent::init();

        $this->factory = \Yii::$container->get(Factory::class);
    }


    public function actionRun($testName)
    {

        $this->test = $this->factory->getTest($testName);

        echo PHP_EOL;
        echo PHP_EOL;

        $process = [];
        $mainPid = null;

        foreach (range(0, $this->test->countProcess()) as $num) {

            if ($num === 0) {
                $mainPid = pcntl_fork();
                if ($mainPid) {
                    $mainPid = getmypid();
                }
            } elseif ($mainPid == getmypid()) {
                $pid = pcntl_fork();
                if (!$pid) {
                    $process[$num] = getmypid();
                }
            } elseif (!in_array(getmypid(), $process)) {
                exit;
            }
        }


        foreach ($process as $num => $pid) {
            $action = 'process' . $num;
            try {
                $this->test->$action();
            } catch (\Exception $e) {
                if ($e instanceof Exception) {
                    echo 'Error: ' . $e->errorInfo[2] . ' '. microtime() . PHP_EOL;
                } else {
                    echo $e->getMessage() . PHP_EOL;
                }
            }

            echo 'Exit script ' . getmypid() . ' ' . microtime() . PHP_EOL;
        }


    }





}