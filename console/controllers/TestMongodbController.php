<?php

namespace console\controllers;

use common\modules\order\models\ar\mongodb\OrderTrack;
use yii\console\Controller;
use yii\helpers\Console;


/**
 * Class TestMongodbController
 * @package app\controllers
 */
class TestMongodbController extends Controller
{
    const ORDER_ID = 1;
    const TRACKING = '{"order_route":[{"lat":"56.8447073","lon":"53.2417503","parking":"base","speed":"0.0","status_id":"17","time":"1476085871429"},{"lat":"56.8447073","lon":"53.2417503","parking":"base","speed":"0.0","status_id":"17","time":"1476085872440"},{"lat":"56.8447073","lon":"53.2417503","parking":"base","speed":"0.0","status_id":"26","time":"1476085873431"},{"lat":"56.8447073","lon":"53.2417503","parking":"base","speed":"0.0","status_id":"26","time":"1476085874443"},{"lat":"56.844711","lon":"53.2417502","parking":"base","speed":"0.0","status_id":"36","time":"1476085875431"},{"lat":"56.844711","lon":"53.2417502","parking":"base","speed":"0.0","status_id":"36","time":"1476085876431"}]}';
    const TEST_TIME = 10;

    public function actionCheckRead()
    {
        $endTime = time() + self::TEST_TIME;

        Console::startProgress(
            0, self::TEST_TIME, 'Getting order tracks (during ' . self::TEST_TIME . ' seconds): ', false);
        while (true) {
            OrderTrack::find()->where(['order_id' => self::ORDER_ID])->asArray()->one();

            if ($endTime < time()) {
                break;
            }

            $start = date_create(date('Y-m-d h:i:s', time()));
            $end   = date_create(date('Y-m-d h:i:s', $endTime));
            $diff  = date_diff($start, $end);
            Console::updateProgress(self::TEST_TIME - $diff->s, self::TEST_TIME);
        }
        Console::endProgress('done.' . PHP_EOL);
    }

    public function actionCheckWrite()
    {
        $endTime = time() + self::TEST_TIME;

        Console::startProgress(
            0, self::TEST_TIME, 'Saving order tracks (during ' . self::TEST_TIME . ' seconds): ', false);
        while (true) {
            $track = OrderTrack::find()->where(['order_id' => self::ORDER_ID])->one();
            if (empty($track)) {
                $track = new OrderTrack([
                    'order_id' => self::ORDER_ID,
                ]);
            }
            $track->tracking = self::TRACKING;
            $track->save();

            if ($endTime < time()) {
                break;
            }

            $start = date_create(date('Y-m-d h:i:s', time()));
            $end   = date_create(date('Y-m-d h:i:s', $endTime));
            $diff  = date_diff($start, $end);
            Console::updateProgress(self::TEST_TIME - $diff->s, self::TEST_TIME);
        }
        Console::endProgress('done.' . PHP_EOL);
    }
}