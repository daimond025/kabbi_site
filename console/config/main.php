<?php

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/params.php'
);

$logVars = array_merge(
    ['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION', '_SERVER'],
    require __DIR__ . '/../../common/config/logVarsFilter.php'
);

$config = [
    'id'                  => 'app-console',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'console\controllers',
    'aliases'             => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components'          => [
        'clientOrderCounter' => [
            'class'                        => 'console\components\client\OrderCounter',
            'completedStatuses'            => [37, 38],
            'rejectedStatusesByClient'     => [39, 40, 41, 42, 43, 44],
            'rejectedStatusesByWorker'     => [45, 46, 47, 48, 50, 51, 120],
            'rejectedStatusesByDispatcher' => [107],
        ],
        'log'                => [
            'targets' => [
                [
                    'class'   => 'yii\log\FileTarget',
                    'levels'  => ['error', 'warning', 'info'],
                    'logVars' => $logVars,
                ],
            ],
        ],

        'notificationService' => [
            'class'               => 'console\components\tenantTariff\NotificationService',
            'tenantTariffService' => [
                'class' => 'common\modules\tenant\modules\tariff\components\TenantTariffService',
            ],
            'notifications'       => [
                'email' => [
                    'class'         => 'console\components\tenantTariff\models\notifications\LogEmailNotification',
                    'tenantService' => 'common\modules\tenant\components\TenantService',
                ],
            ],
            'notificationDays'    => [0, 5],
        ],

        'mysqlOnlineSchemaChange' => [
            'class'      => \console\components\mysqlOnlineSchemaChange\MysqlOnlineSchemaChange::class,
            'dbHost'     => getenv('DB_MIGRATION_HOST'),
            'dbPort'     => getenv('DB_MIGRATION_PORT'),
            'dbName'     => getenv('DB_MIGRATION_NAME'),
            'dbUser'     => getenv('DB_MIGRATION_USERNAME'),
            'dbPassword' => getenv('DB_MIGRATION_PASSWORD'),
        ],
    ],

    'modules' => [
        'common-tenant' => [
            'class'   => 'common\modules\tenant\Module',
            'modules' => [
                'tariff' => [
                    'class'         => 'common\modules\tenant\modules\tariff\Module',
                    'currencyClass' => \frontend\modules\tenant\models\Currency::className(),
                    'tenantClass'   => \common\modules\tenant\models\Tenant::className(),
                ],
            ],
        ],
    ],

    'controllerMap' => [
        'migrate'          => [
            'class'          => 'yii\console\controllers\MigrateController',
            'migrationTable' => 'tbl_migration',
        ],
        'mongodb-migrate'  => [
            'class'         => 'yii\mongodb\console\controllers\MigrateController',
            'migrationPath' => '@app/mongodb-migrations',
        ],
        'kamailio-migrate' => [
            'class'         => 'yii\console\controllers\MigrateController',
            'migrationPath' => '@app/kamailio-migrations',
            'db'            => 'kamailio',
        ],
    ],

    'params' => $params,
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class'      => 'yii\gii\Module',
        'allowedIPs' => ['192.168.1.*', '127.0.0.1'],
    ];
}

return $config;