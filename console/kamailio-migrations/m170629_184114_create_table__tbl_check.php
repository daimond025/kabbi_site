<?php

use yii\db\Migration;

class m170629_184114_create_table__tbl_check extends Migration
{
    const TABLE_NAME = 'tbl_check';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id'    => $this->primaryKey(),
            'key'   => $this->string()->notNull(),
            'value' => $this->string()->notNull(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }

}
