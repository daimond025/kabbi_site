#!/bin/sh


DATE=`date +%Y-%m-%d_%H-%M-%S`

DIR=/opt/mysql/

cd $DIR

mysqldump -u daimond --password='123456'  --databases k_taxi > k_taxi.sql

mysqldump -u daimond --password='123456'  --databases k_paygate > k_paygate.sql

mysqldump -u daimond --password='123456'  --databases k_kamailio > k_kamailio.sql


DIR_TEMP=/opt/mysql/$DATE
mkdir $DIR_TEMP

TAXI_SQL=/opt/mysql/k_taxi.sql
PAYGATE_SQL=/opt/mysql/k_paygate.sql
KAMALLIO_SQL=/opt/mysql/k_kamailio.sql

if [ -f "$TAXI_SQL" ]; then
    mv $TAXI_SQL $DIR_TEMP ;
fi

if [ -f "$PAYGATE_SQL" ]; then
    mv $PAYGATE_SQL $DIR_TEMP;
fi

if [ -f "$KAMALLIO_SQL" ]; then
    mv $KAMALLIO_SQL $DIR_TEMP;
fi

tar -cvf ./taxi_$DATE.tar.gz --absolute-names $DIR_TEMP

sshpass -p 't6R1P2wOSHcrU87WaYhE' scp -P 2025 ./taxi_$DATE.tar.gz  root@m1.kabbi.com:/var/spool/asterisk/monitor/

rm -rf $DIR_TEMP
rm -f taxi_$DATE.tar.gz