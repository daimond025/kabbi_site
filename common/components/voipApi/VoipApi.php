<?php

namespace common\components\voipApi;

use app\modules\setting\models\PhoneLine;
use GuzzleHttp\Client;
use Yii;
use yii\base\Object;

class VoipApi extends Object
{
    public $url;

    const TYPE_CREATE = 'CREATE';
    const TYPE_UPDATE = 'UPDATE';
    const TYPE_DELETE = 'DELETE';

    const STATE_OK = 'ok';
    const STATE_ERROR = 'error';


    protected function getUrl()
    {
        return $this->url;
    }

    public function updatesipuser($sip)
    {
        $params = [
            'username' => $sip,
        ];


        $curl       = app()->curl;
        $response   = $curl->get($this->getUrl() . 'updatesipuser', $params);
        $statusCode = $response->headers['Status-Code'];

        if ($statusCode != 200) {
            $error = $curl->error();
            Yii::error('VoipApi. ' . ($error ? $error : $statusCode));
        }

        return (bool)($response->body == 'OK');
    }

    /**
     * @param $model
     * @param $type
     *
     * @return array
     */
    public function setPhoneLine($params, $type)
    {
        $url = $this->getUrl() . 'phoneLine';

        $params['typeQuery'] = $type;

        try {
            $client       = new Client();
            $response     = $client->request('POST', $url, [
                'json' => $params,
            ]);
            $responseBody = json_decode($response->getBody(), true);
            if ($responseBody['state'] !== self::STATE_OK) {
                Yii::error('Ошибка в запросе ' . $url);
                Yii::error($responseBody['message']);
                Yii::error($params);
            }

            return $responseBody;
        } catch (\Exception $err) {
            Yii::error($err);

            return [
                'state'   => 'error',
                'message' => 'Не могу подключиться к VoipApi',
            ];
        }

    }

}