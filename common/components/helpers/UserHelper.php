<?php

namespace common\components\helpers;


use app\modules\tenant\models\User;
use app\modules\support\models\User as UserBack;

class UserHelper
{

    /**
     * Get client name
     * @param $user User | UserBack
     * @return string
     */
    public static function getNameUser($user)
    {
        switch ($user->lang) {
            case 'ru':
                $name = $user->name . ' ' . $user->second_name;
                break;
            default:
                $name = $user->name . ' ' . $user->last_name;
        }
        return $name;
    }


}