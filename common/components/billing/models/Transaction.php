<?php

namespace common\components\billing\models;

use yii\base\Model;

/**
 * Class Transaction
 * @package common\components\billing\models
 */
class Transaction extends Model
{
    const TYPE_ID_DEPOSIT = 1;
    const TYPE_ID_WITHDRAWAL = 2;
    const TYPE_ID_GOOTAX_PAYMENT = 4;

    const PAYMENT_METHOD_CASH = 'CASH';
    const PAYMENT_METHOD_PERSONAL_ACCOUNT = 'PERSONAL_ACCOUNT';
    const PAYMENT_METHOD_TERMINAL_MILLION = 'TERMINAL_MILLION';

}