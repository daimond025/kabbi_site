<?php

namespace common\components\billing;

use common\components\billing\models\Transaction;
use yii\base\Object;

/**
 * Class Billing
 * @package common\components\billing
 */
class Billing extends Object
{
    const TASK = 'Billing';
    const SUCCESS = 100;
    const ACTION_REGISTER = 'register payment';
    const ACTION_REFUND = 'refund payment';

    public $gearman;

    public function depositTenantBalance($tenantId, $sum, $currencyId, $comment)
    {
        $transactionData = [
            'tenant_id'       => $tenantId,
            'type_id'         => Transaction::TYPE_ID_DEPOSIT,
            'isTenantPayment' => true,
            'sum'             => $sum,
            'currency_id'     => $currencyId,
            'comment'         => $comment,
            'payment_method'  => Transaction::PAYMENT_METHOD_CASH,
        ];

        try {
            return $this->gearman->doHigh(static::TASK, $transactionData) == static::SUCCESS;
        } catch (\Exception $ex) {
            Yii::error($ex);

            return false;
        }
    }

    public function withdrawalTenantBalance($tenantId, $sum, $currencyId, $comment)
    {
        $transactionData = [
            'tenant_id'       => $tenantId,
            'type_id'         => Transaction::TYPE_ID_WITHDRAWAL,
            'isTenantPayment' => true,
            'sum'             => $sum,
            'currency_id'     => $currencyId,
            'comment'         => $comment,
            'payment_method'  => Transaction::PAYMENT_METHOD_PERSONAL_ACCOUNT,
        ];

        try {
            return $this->gearman->doHigh(static::TASK, $transactionData) == static::SUCCESS;
        } catch (\Exception $ex) {
            Yii::error($ex);

            return false;
        }
    }

    /**
     * Register payment for Gootax tariff
     *
     * @param integer $tenantId
     * @param integer $paymentId
     * @param float   $paymentSum
     * @param integer $currencyId
     * @param string  $comment
     *
     * @return bool
     */
    public function registerGootaxPayment($tenantId, $paymentId, $paymentSum, $currencyId, $comment)
    {
        $transactionData = [
            'tenant_id'      => $tenantId,
            'type_id'        => Transaction::TYPE_ID_GOOTAX_PAYMENT,
            'action'         => static::ACTION_REGISTER,
            'payment_id'     => $paymentId,
            'sum'            => $paymentSum,
            'currency_id'    => $currencyId,
            'comment'        => $comment,
            'payment_method' => Transaction::PAYMENT_METHOD_PERSONAL_ACCOUNT,
        ];

        try {
            return $this->gearman->doHigh(static::TASK, $transactionData) == static::SUCCESS;
        } catch (\Exception $ex) {
            Yii::error($ex);

            return false;
        }
    }

    /**
     * Refund payment for Gootax tariff
     *
     * @param integer $tenantId
     * @param integer $paymentId
     * @param float   $paymentSum
     * @param integer $currencyId
     * @param string  $comment
     *
     * @return bool
     */
    public function refundGootaxPayment($tenantId, $paymentId, $paymentSum, $currencyId, $comment)
    {
        $refundPaymentData = [
            'tenant_id'      => $tenantId,
            'type_id'        => Transaction::TYPE_ID_GOOTAX_PAYMENT,
            'action'         => static::ACTION_REFUND,
            'payment_id'     => $paymentId,
            'sum'            => $paymentSum,
            'currency_id'    => $currencyId,
            'comment'        => $comment,
            'payment_method' => Transaction::PAYMENT_METHOD_PERSONAL_ACCOUNT,
        ];

        try {
            return $this->gearman->doHigh(static::TASK, $refundPaymentData) == static::SUCCESS;
        } catch (\Exception $ex) {
            Yii::error($ex);

            return false;
        }
    }

}