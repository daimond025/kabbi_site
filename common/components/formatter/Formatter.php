<?php

namespace common\components\formatter;

use IntlDateFormatter;
use Yii;
use NumberFormatter;

class Formatter extends \yii\i18n\Formatter
{
    public function asMoney($sum, $currencySymbol)
    {
        $this->numberFormatterSymbols = [NumberFormatter::CURRENCY_SYMBOL => $currencySymbol];

        return $this->asCurrency($sum);
    }

    public function asDate($value, $format = null)
    {
        if ($format === 'shortDate') {
            $formatter = new IntlDateFormatter(
                Yii::$app->language,
                IntlDateFormatter::SHORT,
                IntlDateFormatter::NONE,
                'UTC',
                IntlDateFormatter::GREGORIAN
            );
            $pattern = $formatter->getPattern();
            $formatter->setPattern(str_replace('yy', 'yyyy', $pattern));

            return $formatter->format($value);
        }

        return parent::asDate($value, $format);
    }
}
