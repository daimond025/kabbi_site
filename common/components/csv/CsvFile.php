<?php
/**
 * Created by PhpStorm.
 * User: zigfried123
 * Date: 16.01.2018
 * Time: 10:54
 */

namespace common\components\csv;

use yii\helpers\FileHelper;
use yii\base\Exception;

class CsvFile extends \yii2tech\csvgrid\CsvFile
{

    /**
     * Opens the related file for writing.
     * @throws Exception on failure.
     * @return boolean success.
     */
    public function open()
    {
        if ($this->fileHandler === null) {
            FileHelper::createDirectory(dirname($this->name));
            $this->fileHandler = fopen($this->name, 'w+');
            fwrite($this->fileHandler, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
            if ($this->fileHandler === false) {
                throw new Exception('Unable to create/open file "' . $this->name . '".');
            }
        }
        return true;
    }


}