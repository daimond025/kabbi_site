<?php
/**
 * Created by PhpStorm.
 * User: zigfried123
 * Date: 16.01.2018
 * Time: 10:46
 */

namespace common\components\csv;

class ExportResult extends \yii2tech\csvgrid\ExportResult
{

    /**
     * Creates new CSV file in result set.
     *
     * @param array $config file instance configuration.
     *
     * @return CsvFile file instance.
     */
    public function newCsvFile($config = [])
    {
        $selfFileName = $this->fileBaseName . '-' . str_pad((count($this->csvFiles) + 1), 3, '0', STR_PAD_LEFT);

        $file       = new CsvFile($config);
        $file->name = $this->getDirName() . DIRECTORY_SEPARATOR . $selfFileName . '.csv';

        $this->csvFiles[] = $file;

        return $file;
    }

}