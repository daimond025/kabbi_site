<?php

namespace common\components\csv;

/**
 * Created by PhpStorm.
 * User: zigfried123
 * Date: 16.01.2018
 * Time: 10:32
 */
class CsvGrid extends \yii2tech\csvgrid\CsvGrid
{

    /**
     * Performs data export.
     * @return ExportResult export result.
     */
    public function export()
    {
        $result = new ExportResult($this->resultConfig);

        $columnsInitialized = false;

        $maxEntriesPerFile = false;
        if (!empty($this->maxEntriesPerFile)) {
            $maxEntriesPerFile = $this->maxEntriesPerFile;
            if ($this->showFooter) {
                $maxEntriesPerFile--;
            }
        }

        $csvFile = null;
        while (($data = $this->batchModels()) !== false) {
            list($models, $keys) = $data;

            if (!$columnsInitialized) {
                $this->initColumns(reset($models));
                $columnsInitialized = true;
            }

            foreach ($models as $index => $model) {
                if (!is_object($csvFile)) {
                    $csvFile = $result->newCsvFile($this->csvFileConfig);
                    if ($this->showHeader) {
                        $csvFile->writeRow($this->composeHeaderRow());
                    }
                }

                $key = isset($keys[$index]) ? $keys[$index] : $index;
                $csvFile->writeRow($this->composeBodyRow($model, $key, $index));

                if ($maxEntriesPerFile !== false && $csvFile->entriesCount >= $maxEntriesPerFile) {
                    if ($this->showFooter) {
                        $csvFile->writeRow($this->composeFooterRow());
                    }
                    $csvFile->close();
                    $csvFile = null;
                }
            }

            $this->gc();
        }

        if (is_object($csvFile)) {
            if ($this->showFooter) {
                $csvFile->writeRow($this->composeFooterRow());
            }
            $csvFile->close();
        }

        if (empty($result->csvFiles)) {
            $csvFile = $result->newCsvFile($this->csvFileConfig);
            if ($this->showHeader) {
                $csvFile->writeRow($this->composeHeaderRow());
            }
            if ($this->showFooter) {
                $csvFile->writeRow($this->composeFooterRow());
            }
            $csvFile->close();
        }

        return $result;
    }

}