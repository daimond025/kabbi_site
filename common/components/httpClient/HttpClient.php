<?php

namespace common\components\httpClient;

use common\components\serviceApi\HttpClientInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException as GuzzleConnectException;
use yii\base\Component;

class HttpClient extends Component implements HttpClientInterface
{
    public $timeout = 5;
    public $connectionTimeout = 5;

    /**
     * @param string   $url
     * @param string[] $getParams
     *
     * @return array
     * @throws ConnectException
     */
    public function get($url, $getParams)
    {
        $options = [
            'query' => $getParams,
        ];

        return $this->send('GET', $url, $options);
    }

    /**
     * @param string   $url
     * @param string[] $postParams
     * @param string[] $getParams
     *
     * @return array
     * @throws ConnectException
     */
    public function post($url, $postParams, $getParams = [])
    {
        $options = [
            'query'       => $getParams,
            'form_params' => $postParams,
        ];

        return $this->send('POST', $url, $options);
    }

    /**
     * @param string $method
     * @param string $url
     * @param array  $options
     *
     * @return array
     * @throws ConnectException
     */
    private function send($method, $url, $options)
    {
        $client = new Client([
            'timeout'         => $this->timeout,
            'connect_timeout' => $this->connectionTimeout,
            'http_errors'     => false,
        ]);


        try {
            $response = $client->request($method, $url, $options);

            return [
                'statusCode' => $response->getStatusCode(),
                'body'       => (string)$response->getBody(),
            ];

        } catch (GuzzleConnectException $exception) {
            throw new ConnectException($exception->getMessage());
        }
    }
}