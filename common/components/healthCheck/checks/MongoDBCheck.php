<?php

namespace healthCheck\checks;

use Ramsey\Uuid\Uuid;
use healthCheck\exceptions\HealthCheckException;
use yii\mongodb\Connection;

/**
 * Class MongoDBCheck
 * @package healthCheck\checks
 */
class MongoDBCheck extends BaseCheck
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * MongoDBCheck constructor.
     *
     * @param string     $name
     * @param Connection $connection
     */
    public function __construct($name, Connection $connection)
    {
        $this->connection = $connection;

        parent::__construct($name);
    }

    public function run()
    {
        try {
            $key = Uuid::uuid4()->toString();

            $value = 'value';
            $this->connection->createCommand()->insert('check',
                ['key' => $key, 'value' => $value]);
            $actualValue = $this->connection->createCommand()->find('check', ['key' => $key])->toArray();
            if (!isset($actualValue[0]['value']) || $value !== (string)$actualValue[0]['value']) {
                throw new HealthCheckException('Incorrect actual value after insert');
            }

            $value = 'new value';
            $this->connection->createCommand()->update('check',
                ['key' => $key],
                ['key' => $key, 'value' => $value]);
            $actualValue = $this->connection->createCommand()->find('check', ['key' => $key])->toArray();
            if (!isset($actualValue[0]['value']) || $value !== (string)$actualValue[0]['value']) {
                throw new HealthCheckException('Incorrect actual value after update');
            }

            $this->connection->createCommand()->delete('check', ['key' => $key]);
            $actualValue = $this->connection->createCommand()->find('check', ['key' => $key])->toArray();
            if (!empty($actualValue)) {
                throw new HealthCheckException('Incorrect actual value after delete');
            }
        } catch (HealthCheckException $ex) {
            throw new HealthCheckException(
                'Check MongoDb exception: error=' . $ex->getMessage() . ', code=' . $ex->getCode(), 0, $ex);
        }
    }
}