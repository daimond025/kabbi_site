<?php

namespace common\components\services\users\community;


use app\modules\tenant\models\User;
use landing\modules\community\models\user\CommunityUser;

class CommunityUserService
{

    public function synchronizationCommunityUserWithUser(User $user)
    {
        $communityUser = CommunityUser::findOne(['user_id' => $user->user_id]);

        if (!$communityUser) {
            return false;
        }

        $communityUser->load($this->returnDataForCommunityUser($user), '');

        if ($communityUser->save()) {
            return $communityUser;
        }

        return false;
    }

    protected function returnDataForCommunityUser(User $user)
    {
        return array_merge($user->getAttributes(), [
            'firstname'  => $user->getAttribute('name'),
            'lastname'   => $user->getAttribute('last_name'),
            'secondname' => $user->getAttribute('second_name'),
        ]);
    }

}