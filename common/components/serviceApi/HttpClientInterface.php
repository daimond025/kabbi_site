<?php

namespace common\components\serviceApi;

interface HttpClientInterface
{
    public function get($url, $params);

    public function post($url, $postParams, $getParams);
}