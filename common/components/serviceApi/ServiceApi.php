<?php

namespace common\components\serviceApi;

use common\components\httpClient\ConnectException;
use common\components\httpClient\HttpClient;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

/**
 * Class ServiceApi
 * @package common\components\serviceApi
 * @property HttpClient $httpClient
 * @property array      $httpClientData
 */
class ServiceApi extends Component
{
    public $baseUrl;
    private $httpClientData;

    const ACTION_SEND_EMAIL = 'v1/email/send';

    const ACTION_SEND_ALL_SMS = 'v1/notification/send_text_for_client';

    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    /**
     * @return HttpClient|object
     * @throws InvalidConfigException
     */
    public function getHttpClient()
    {
        if (!$this->httpClientData instanceof HttpClientInterface) {
            $this->httpClientData = \Yii::createObject($this->httpClientData);
        }

        return $this->httpClientData;
    }

    public function setHttpClient(array $data)
    {
        $this->httpClientData = $data;
    }

    /**
     * @param integer $tenantId
     * @param integer $cityId
     * @param string  $type
     * @param string  $lang
     * @param string  $to
     * @param array   $params
     *
     * @return bool
     */
    public function sendEmail($tenantId, $cityId, $type, $lang, $to, $params = [])
    {
        EmailTypes::check($type);

        $url       = $this->getUrl(self::ACTION_SEND_EMAIL);
        $getParams = [
            'tenant_id' => $tenantId,
            'city_id'   => $cityId,
            'lang'      => $lang,
            'to'        => $to,
            'type'      => $type,
        ];

        try {
            $response           = $this->httpClient->post($url, $params, $getParams);
            $responseStatusCode = ArrayHelper::getValue($response, 'statusCode');

        } catch (ConnectException $exception) {
            \Yii::error('Ошибка соединения: ' . $exception->getMessage());

            return false;
        }

        if ($responseStatusCode === 422) {
            \Yii::error('Невалидные параметры: ' . ArrayHelper::getValue($response, 'body'));

            return false;
        }

        return $responseStatusCode === 200;
    }

    /**
     * @param integer $tenantId
     * @param integer $cityId
     * @param string  $type
     * @param string  $lang
     * @param string  $to
     * @param array   $params
     *
     * @return bool
     */
    public function sendAllSms($params = []){
        $url       = $this->getUrl(self::ACTION_SEND_ALL_SMS);

        $getParams = [
            'tenant_id' => user()->tenant_id,
        ];


        try {
            $response           = $this->httpClient->post($url, $params, $getParams);
            $responseStatusCode = ArrayHelper::getValue($response, 'statusCode');

        } catch (ConnectException $exception) {
            \Yii::error('Ошибка соединения: ' . $exception->getMessage());

            return false;
        }

        if ($responseStatusCode === 422) {
            \Yii::error('Невалидные параметры: ' . ArrayHelper::getValue($response, 'body'));

            return false;
        }

        return $responseStatusCode === 200;
    }



    // метод отправки

    private function getUrl($method)
    {
        return rtrim($this->baseUrl, '/') . '/' . $method;
    }
}