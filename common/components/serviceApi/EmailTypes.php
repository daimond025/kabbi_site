<?php

namespace common\components\serviceApi;

use yii\helpers\ArrayHelper;

class EmailTypes
{
    // Регистрация арендатора
    const REGISTER = 'register';

    // Регистрация арендатора
    const WORKER_INFO_ORDERS = 'WORKER_INFO_ORDERS';

    // Подтверждение регистации сотрудника
    const EMAIL_USER_CONFIRM = 'confirm_user_email';

    // Восстановление пароля сотрудника
    const PASSWORD_RESET_TOKEN = 'password_reset_token';

    // Непрочитанные сообщения в чате сотрудника
    const CHAT_UNREAD_MESSAGE = 'chat_unread_message';

    // Подтверждение получения оплаты от арендатора
    const PAYMENT_NOTICE = 'payment_notice';

    // Отчет о заказе клиенту
    const CLIENT_ORDER_REPORT = 'client_order_report';

    public static function getTypes()
    {
        return [
            self::REGISTER,
            self::WORKER_INFO_ORDERS,
            self::EMAIL_USER_CONFIRM,
            self::PASSWORD_RESET_TOKEN,
            self::CHAT_UNREAD_MESSAGE,
            self::PAYMENT_NOTICE,
            self::CLIENT_ORDER_REPORT,
        ];
    }

    /**
     * @param $type
     * @throws \InvalidArgumentException
     */
    public static function check($type)
    {
        if(!ArrayHelper::isIn($type, self::getTypes())) {
            throw new \InvalidArgumentException();
        }
    }

}