<?php
/* @var $panel \common\components\panels\branch\MercurialBranchPanel */

use yii\helpers\Html;
use yii\web\View;

$branchList = $panel->getBranches();
$this->registerJs('initBranchClickListener();', View::POS_READY);
?>
<div class="yii-debug-toolbar__branch_alert"></div>
<h1>
    Branches
    <small>Current branch: <span class="yii-debug-toolbar__branch"><?= $panel->getBranch() ?></span></small>
</h1>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th>#</th>
        <th>Branch</th>
        <th>Revision</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($branchList as $key => $item) : ?>
        <tr>
            <td><?= $key + 1 ?></td>
            <td class="yii-debug-toolbar__branch_cell"><?= Html::a($item['branch'], ['setBranch', 'name' => $item['branch']]) ?></td>
            <td><?= $item['revision'] ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<script>
    function initBranchClickListener() {
        $('.yii-debug-toolbar__branch_cell a').on('click', function (e) {
            e.preventDefault();

            $.get($(this).attr('href'), function (json) {
                if (json.success === 1) {
                    $('.yii-debug-toolbar__branch').text(json.name);
                    window.parent.$('.yii-debug-toolbar__branch').text(json.name);

                    $('.yii-debug-toolbar__branch_alert').prepend('<div class="alert alert-success alert-dismissible">\n' +
                        '  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>\n' +
                        '  The branch was successfully updated.\n' +
                        '</div>');
                } else {
                    console.log('Error to change the repository branch')
                }
            }, 'json')
        });
    }
</script>