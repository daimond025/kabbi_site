<?php
/* @var $panel \common\components\panels\branch\MercurialBranchPanel */

if (!empty($panel->data)) :
    ?>
    <div class="yii-debug-toolbar__block">
        <a href="<?= $panel->getUrl() ?>" title="Current branch"><?= $panel->name ?>
            <span class="yii-debug-toolbar__branch yii-debug-toolbar__label yii-debug-toolbar__label_info"><?= $panel->data ?></span>
        </a>
    </div>
<?php endif; ?>
