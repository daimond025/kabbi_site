<?php

namespace common\components\panels\branch\actions;

use common\components\panels\branch\MercurialBranchPanel;
use Yii;
use yii\base\Action;
use yii\web\Response;

class SetBranchAction extends Action
{
    /** @var MercurialBranchPanel */
    public $panel;

    public function run($name)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $isSuccess = $this->panel->setBranch($name);

        return ['name' => $name, 'success' => (int) $isSuccess];
    }
}
