<?php

namespace common\components\panels\branch;

use common\components\panels\branch\actions\SetBranchAction;
use Yii;
use yii\base\InvalidConfigException;
use yii\debug\Panel;

class MercurialBranchPanel extends Panel
{
    /**
     * @var string path alias (e.g. "@app/views/site/index");
     */
    public $viewPath;

    public function init()
    {
        parent::init();

        if (empty($this->viewPath)) {
            throw new InvalidConfigException('The field "viewPath" for getting view must be set');
        }

        $this->actions['setBranch'] = [
            'class' => SetBranchAction::class,
            'panel' => $this,
        ];
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Branch';
    }

    /**
     * @inheritdoc
     */
    public function getSummary()
    {
        return Yii::$app->view->render($this->viewPath . '/summary', ['panel' => $this]);
    }

    /**
     * @inheritdoc
     */
    public function getDetail()
    {
        return Yii::$app->view->render($this->viewPath . '/detail', ['panel' => $this]);
    }

    /**
     * Save result for next output
     * @return mixed
     */
    public function save()
    {
        return $this->getBranch();
    }

    public function setBranch($name)
    {
        $this->execute("hg up -C $name", $output, $errorCode);

        return $errorCode == 0;
    }

    public function getBranch()
    {
        $this->execute('hg id --branch 2>/dev/null', $output, $errorCode);

        return $errorCode == 0 ? current($output) : null;
    }

    public function getBranches()
    {
        $this->execute('hg branches 2>/dev/null', $output, $errorCode);

        return array_map(function ($item) {
            $branch = preg_replace("/\s+/", ' ', $item);
            list($branch, $revision) = explode(' ', $branch);

            return compact('branch', 'revision');
        }, $output);
    }

    private function execute($command, &$output = null, &$errorCode = null)
    {
        $rootProjectDir = dirname(Yii::$app->basePath);
        exec("cd $rootProjectDir && $command", $output, $errorCode);
    }
}
