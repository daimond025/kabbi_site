<?php

namespace common\components\behaviors;

use yii\helpers\Html;

class FormattedError extends \yii\base\Behavior
{
    public function getFormattedError()
    {
        $result = [];
        foreach ($this->owner->getErrors() as $attribute => $errors) {
            $result[Html::getInputId($this->owner, $attribute)] = $errors;
        }
        return $result;
    }
}