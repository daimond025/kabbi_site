<?php

namespace common\controllers;
   
class SapsanSmsController extends IbateleSmsController
{
    const SMS_SEND_SERVER_ADDRESS = 'http://lk.sms-sapsan.ru/xml/';
    const SMS_REQUEST_BALANCE_SERVER_ADDRESS = 'http://lk.sms-sapsan.ru/xml/balance.php';
}
