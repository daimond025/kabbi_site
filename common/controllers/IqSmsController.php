<?php

namespace common\controllers;

use common\models\IQSMS;
use yii\web\Controller;

class IqSmsController extends Controller
{
    public function actionBalance($login, $password)
    {
        $gate = new IQSMS($login, $password);
        $credits = $gate->credits()['balance'];
        return $credits; // узнаем текущий баланс
    }
}
