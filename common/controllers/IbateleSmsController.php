<?php

namespace common\controllers;

class IbateleSmsController extends \yii\web\Controller
{

    //Адреса отправки запросов
    const SMS_SEND_SERVER_ADDRESS = 'https://lk.ibatele.com/xml/';
    const SMS_REQUEST_SMS_STATE_SERVER_ADDRESS = 'https://lk.ibatele.com/xml/state.php';
    const SMS_REQUEST_BALANCE_SERVER_ADDRESS = 'https://lk.ibatele.com/xml/balance.php';
    const SMS_REQUEST_SENDER_LIST_SERVER_ADDRESS = 'https://lk.ibatele.com/xml/originator.php';
    const SMS_REQUEST_INCOMING_MESSAGES_SERVER_ADDRESS = 'https://lk.ibatele.com/xml/incoming.php';
    const SMS_REQUEST_INFO_ABOUT_PHONE = 'https://lk.ibatele.com/xml/def.php';
    const SMS_REQUEST_LIST_OF_BASES = 'https://lk.ibatele.com/xml/list_bases.php';
    const SMS_REQUEST_BASES_EDIT_UPDATE = 'https://lk.ibatele.com/xml/bases.php';
    const SMS_REQUEST_GET_CLIENT_BASE = 'https://lk.ibatele.com/xml/list_phones.php';
    //Тексты сообщений об ошибках
    const POST_DATA_MISSING_ERROR_MESSAGE = 'POST данные отсутствуют';
    const INCORRECT_LOGIN_DATA = 'Неправильный логин или пароль';
    const ACCOUNT_IS_BANNED = 'Ваш аккаунт заблокирован';
    const INCORRECT_XML_FORMAT = 'Неправильный формат XML документа';

    /**
     * 
     * @param string $login
     * @param string $password
     * @param string $abonent_phone Phone number where sms will be sent
     * @param string $sms_number SMS counter 1,2,3,etc
     * @param string $sender Sender's name. For example 'Gootax'
     * @param string $text Message body
     * @return mixed json encoded server response
     */
    public function actionSendSms($login, $password, $abonent_phone, $sms_number, $sender, $text)
    {
        $xml = '<?xml version="1.0" encoding="utf-8" ?>
        <request>
        <message type="sms">
        <sender>' . $sender . '</sender>
        <text>' . $text . '</text>
        <abonent phone="' . $abonent_phone . '" number_sms="' .
                $sms_number . '" client_id_sms="' . $client_id . '" time_send="' .
                date('y-m-d') . ' ' . date('H:i') . '" />
        </message>
        <security>
        <login value="' . $login . '" />
        <password value="' . $password . '" />
        </security>
        </request>';
        $result = $this->sendSms($xml, static::SMS_SEND_SERVER_ADDRESS);
        return json_encode($this->parseServerResponse($result));
    }

    /**
     * @return mixed
     * state True on success
     * state False on failure
     * or state CURLOPT_RETURNTRANSFER
     */
    public function actionRequestSmsStatus($login, $password, $sms_id)
    {
        $xml = '<?xml  version="1.0" encoding="utf-8" ?>
        <request>
        <security>
        <login value="' . $login . '" />
        <password value="' . $password . '" />
        </security>
        <get_state>
        <id_sms>' . $sms_id . '</id_sms>
        </get_state>
        </request>';
        return $this->sendSms($xml, self::SMS_REQUEST_SMS_STATE_SERVER_ADDRESS);
    }

    /**
     * 
     * @param type $login
     * @param type $password
     * @return mixed json encoded server response
     */
    public function actionRequestBalance($login, $password)
    {
        $xml = '<?xml  version="1.0" encoding="utf-8" ?>
        <request>
        <security>
        <login value="' . $login . '" />
        <password value="' . $password . '" />
        </security>
        </request>';
        $result = $this->sendSms($xml, static::SMS_REQUEST_BALANCE_SERVER_ADDRESS);
        return json_encode($this->parseServerResponse($result));
    }

    /**
     * @return mixed
     * state True on success
     * state False on failure
     * or state CURLOPT_RETURNTRANSFER
     */
    public function actionRequestSenderList($login, $password)
    {
        $xml = '<?xml  version="1.0" encoding="utf-8" ?>
        <request>
        <security>
        <login value="' . $login . '" />
        <password value="' . $password . '" />
        </security>
        </request>';
        return $this->sendSms($xml, self::SMS_REQUEST_SENDER_LIST_SERVER_ADDRESS);
    }

    /**
     * @return mixed
     * state True on success
     * state False on failure
     * or state CURLOPT_RETURNTRANSFER
     * 
     * PS: time format: YYYY-MM-DD HH:MM on null receive all messages
     */
    public function actionRequestIncomingSms($login, $password, $period_of_time_start = NULL, $period_of_time_end = NULL)
    {
        $xml = '<?xml  version="1.0" encoding="utf-8" ?>
        <request>
        <security>
        <login value="' . $login . '" />
        <password value="' . $password . '" />
        </security>
        <timestart="' . $period_of_time_start . '" ' . $period_of_time_end . '" />
        </request>';
        return $this->sendSms($xml, self::SMS_REQUEST_INCOMING_MESSAGES_SERVER_ADDRESS);
    }

    /**
     * @return mixed
     * state True on success
     * state False on failure
     * or state CURLOPT_RETURNTRANSFER
     */
    public function actionRequestInformationAboutPhones($login, $password, $phones)
    {
        foreach ($phones as $rows) {
            $phoneList .= '<phone>' . $rows['phone'] . '</phone><br/>';
        }
        $xml = '<?xml  version="1.0" encoding="utf-8" ?>
        <request>
        <security>
        <login value="' . $login . '" />
        <password value="' . $password . '" />
        </security>
        <phones>' .
                $phoneList .
                '</phones>
        </request>';
        return $this->sendSms($xml, self::SMS_REQUEST_INFO_ABOUT_PHONE);
    }

    /**
     * $local_time_birth, $doShowMessage bool
     * $base_name array()
     * @return mixed
     * state True on success
     * state False on failure
     * or state CURLOPT_RETURNTRANSFER
     * 
     * time format is : HH:MM
     */
    public function actionBaseParameters($login, $password, $base_name, $birth_time, $doShowMessage, $local_time_birth, $day_before, $success_message_author, $successMessageSendTime, $base_id, $message_on_success)
    {
        if ($doShowMessage == false) {
            $doShowMessage = 'no';
        } else if ($doShowMessage == true) {
            $doShowMessage = 'yes';
        } else {
            return 'doShowMessage must be bool';
        } foreach ($bases as $rows) {
            $baseList .= $rows;
        }
        $xml = '<?xml version="1.0" encoding="utf-8" ?>
        <request>
        <security>
        <login value="логин" />
        <password value="пароль" />
        </security>
        <bases>
        <base id_base="' . $base_id . '" name_base="' . $base_name . '" time_birth="' .
                $birth_time . '" local_time_birth="yes" day_before="' . $day_before . '"originator_birth="' .
                $success_message_author . '" on_birth="yes">"' .
                $message_on_success . '"!</base>
        </bases>
        <delete_bases>
        <base id_base="1235" />
        <base id_base="1236" />
        …
        </delete_bases>
        </request>';
        return $this->sendSms($xml, self::SMS_REQUEST_BASES_EDIT_UPDATE);
    }

    /**
     * 
     * @param type $xml
     * @param type $server_address remote IP address of server
     * depends on what action you gonna do
     * @return mixed
     * state True on success
     * state False on failure
     * or state CURLOPT_RETURNTRANSFER if incoming data available
     */
    public function sendSms($xml, $server_address)
    {
        $res = '';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: text/xml; charset=utf-8'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CRLF, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_URL, $server_address);
        $result = curl_exec($ch);

        curl_close($ch);
        return $result; // отправить на функцию - парсер
    }

    public function parseServerResponse($result)
    {
        $xmlObj = simplexml_load_string($result);
        $xml = (array) $xmlObj;
        return $xml;
    }

    /**
     * Checking sms status.
     * @param mixed json encoded server response
     */
    public function isResultSuccess($result)
    {
        if ($result == IbateleSmsController::ACCOUNT_IS_BANNED || $result == self::INCORRECT_LOGIN_DATA || $result == self::POST_DATA_MISSING_ERROR_MESSAGE || $result == self::INCORRECT_XML_FORMAT) {
            return false;
        }

        return true;
    }

}
