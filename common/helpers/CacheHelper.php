<?php

namespace common\helpers;


class CacheHelper
{
    /**
     * Getting data from cache
     * @param $key
     * @param callable $callback
     * @param int $duration
     * @param null $dependency
     * @return mixed
     * @deprecated
     */
    public static function getFromCache($key, $callback, $duration = 0, $dependency = null)
    {
        // try retrieving $data from cache
        $cache = app()->cache;
        $data = $cache->get($key);

        if ($data === false && is_callable($callback)) {
            // $data is not found in cache, calculate it from scratch
            $data = call_user_func($callback);
            // store $data in cache so that it can be retrieved next time
            $cache->set($key, $data, $duration, $dependency);
        }

        return $data;
    }
}