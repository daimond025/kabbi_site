<?php

namespace common\helpers;

use Yii;

class DateTimeHelper
{

    /**
     * Getting map of days in the locale.
     * @return array ['Monday' => 'Понедельник',...]
     */
    public static function getDayOfWeekList()
    {
        $arDays = [
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
            'Sunday',
        ];
        $arResult = [];

        foreach ($arDays as $day) {
            $arResult[$day] = Yii::$app->formatter->asDate($day, 'cccc');
        }

        return $arResult;
    }

    /**
     * Getting year list.
     * @param integer $start Number years to past
     * @param integer $end Number years to future
     * @return array
     */
    public static function getYearList($start = 15, $end = null)
    {
        $end = $end ? $end : $start;
        $start_day = date("Y") - $start;
        $end_date = date("Y") + $end;
        $arYear = [];

        for ($i = $end_date; $i >= $start_day; $i--) {
            $arYear[$i] = $i;
        }

        return $arYear;
    }

    /**
     * Getting month list
     * @return array
     */
    public static function getMonthList()
    {
        $monthList = [];

        for ($i = 1; $i <= 12; $i++) {
            $key = $i < 10 ? '0' . $i : $i;
            $monthList[$key] = Yii::$app->formatter->asDate(mktime(0, 0, 0, $key, 1), 'LLLL');
        }

        return $monthList;
    }

    /**
     * Return genitive month list
     * @return array
     */
    public static function getGenitiveMonths($lang = null)
    {
        if (empty($lang)) {
            $lang = substr(app()->language, 0, 2);
        }

        $genitiveMonths = [
            'ru' => [
                '01' => 'Января',
                '02' => 'Февраля',
                '03' => 'Марта',
                '04' => 'Апреля',
                '05' => 'Мая',
                '06' => 'Июня',
                '07' => 'Июля',
                '08' => 'Августа',
                '09' => 'Сентября',
                '10' => 'Октября',
                '11' => 'Ноября',
                '12' => 'Декабря',
            ],
        ];

        $months = isset($genitiveMonths[$lang]) ? $genitiveMonths[$lang] : [];

        return array_replace(self::getMonthList(), $months);
    }

    /**
     * Convert date to a different format.
     * @param string $format 'd.m.Y'
     * @param string $date_str '2015-06-29 11:52:12'
     * @param integer $time_offset The time zone offset (s)
     * @return string
     */
    public static function getDateFormatFromDateStr($format, $date_str, $time_offset = 0)
    {
        return date($format, strtotime($date_str) + $time_offset);
    }

    /**
     * Getting hours, minutes and seconds by time duration
     * @param integer $duration Time duration (s)
     * @return array ['HOURS' => 4, 'MINUTES' => 33, 'SECONDS' => 21]
     */
    public static function parse_seconds($duration)
    {
        $hours = floor($duration / 3600);
        $minutes = ($duration / 60) % 60;
        $seconds = $duration % 60;

        if (intval($hours) < 10) {
            $hours = '0' . $hours;
        }
        if (intval($minutes) < 10) {
            $minutes = '0' . $minutes;
        }
        if (intval($seconds) < 10) {
            $seconds = '0' . $seconds;
        }

        return array('HOURS' => $hours, 'MINUTES' => $minutes, 'SECONDS' => $seconds);
    }

    /**
     * Converrt time duration to H:i:s
     * @param integer $duration Time duration (s)
     * @return string H:i:s
     */
    public static function getFormatTimeFromSeconds($duration)
    {
        return implode(':', self::parse_seconds($duration));
    }

    /**
     * Converrt time duration to "5 hours 13 minutes 7 seconds"
     * @param integer $seconds
     * @return string "5 hours 13 minutes 7 seconds"
     */
    public static function secondsToStr($seconds)
    {
        $seconds = $seconds < 0 ? 0 : $seconds;
        $str = '';

        $hours = (int)($seconds / 3600);
        $hours_str = $hours < 10 ? '0' . $hours : $hours;

        if ($hours > 0) {
            $str .= $hours_str . ' ' . t('app', 'h.') . ' ';
        }

        $min = $seconds / 60 % 60;
        $min_str = $min < 10 ? '0' . $min : $min;

        if ($min > 0 || !empty($str)) {
            $str .= $min_str . ' ' . t('app', 'min.') . ' ';
        }

        $sec = $seconds % 60;
        $sec_str = $sec < 10 ? '0' . $sec : $sec;
        $sec_str = $sec_str == '00' ? 0 : $sec_str;
        $str .= $sec_str . ' ' . t('app', 'sec.');

        return $str;
    }

    public static function getMonthDayNumbers()
    {
        $monthDayNumbers = [];

        for ($i = 1; $i <= 31; $i++) {
            $day = $i < 10 ? '0' . $i : $i;
            $monthDayNumbers[$day] = $day;
        }

        return $monthDayNumbers;
    }

    public static function getOldYearList($start = 15, $end = 80)
    {
        $start_day = date("Y") - $start;
        $end_date = date("Y") - $end;
        $arYear = [];

        for ($i = $start_day; $i >= $end_date; $i--) {
            $arYear[$i] = $i;
        }

        return $arYear;
    }

    public static function getYearMonths()
    {
        $arMonth = range(1, 12);

        return array_combine($arMonth, $arMonth);
    }

    public static function getFormattedDateByParts($day, $month, $year, $format = 'yyyy-MM-dd')
    {
        return Yii::$app->formatter->asDate($year . '/' . $month . '/' . $day, $format);
    }

    /**
     * @param string $day Day part
     * @param string $month Month part
     * @param string $year Year part
     * @param string $date Date
     */
    public static function loadDatePartsFromDate(&$day, &$month, &$year, $date)
    {
        if (!empty($date)) {
            $formatedDate = date('d.m.Y', strtotime($date));
            list($day, $month, $year) = explode('.', $formatedDate);
        }
    }

}
