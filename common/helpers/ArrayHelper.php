<?php

namespace common\helpers;

use yii\helpers\ArrayHelper as BaseArrayHelper;

class ArrayHelper extends BaseArrayHelper
{
    public static function trim(array $array, $characterMask = " \t\n\r\0\x0B")
    {
        return array_map(function ($item) use ($characterMask) {
            return trim($item, $characterMask);
        }, $array);
    }
}
