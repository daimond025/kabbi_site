<?php

namespace common\helpers;

class LanguageHelper
{
    public static function getCode($tag)
    {
        $parseTag = self::parseTag($tag);

        return ArrayHelper::getValue($parseTag, 0);
    }

    private static function parseTag($tag)
    {
        return explode('-', $tag, 2);
    }
}
