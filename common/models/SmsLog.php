<?php

namespace common\models;
use app\modules\setting\models\Tenant;
use app\modules\setting\models\SmsServer;

/**
 * This is the model class for table "tbl_sms_log".
 *
 * @property integer $id
 * @property string $text
 * @property integer $status
 * @property string $login
 * @property string $phone
 * @property string $create_time
 * @property string $signature
 * @property integer $server_id
 * @property integer $tenant_id
 * @property string $response
 *
 * @property AdminSmsServer $server
 * @property SmsServer $server0
 * @property Tenant $tenant
 */
class SmsLog extends \yii\db\ActiveRecord
{
    
    public $tenant_domain;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_sms_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'status', 'phone', 'create_time', 'server_id', 'tenant_id'], 'required'],
            [['status', 'server_id', 'tenant_id'], 'integer'],
            [['create_time'], 'safe'],
            [['text', 'response'], 'string', 'max' => 255],
            [['login', 'phone', 'signature'], 'string', 'max' => 48]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Текст',
            'status' => 'Статус',
            'login' => 'Логин',
            'phone' => 'Телефон',
            'create_time' => 'Дата отправки',
            'signature' => 'Подпись',
            'server_id' => 'ID сервера',
            'tenant_id' => 'ID арендатора',
            'response' => 'Ответ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServer()
    {
        return $this->hasOne(SmsServer::className(), ['server_id' => 'server_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }
}
