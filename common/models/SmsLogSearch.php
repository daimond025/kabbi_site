<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SmsLog;

/**
 * SmsLogSearch represents the model behind the search form about `common\models\SmsLog`.
 */
class SmsLogSearch extends SmsLog {

    public $server_name;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'status', 'server_id', 'tenant_id'], 'integer'],
            [['text', 'login', 'phone', 'signature', 'response', 'server_name', 'tenant_domain'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = SmsLog::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'create_time' => SORT_DESC
                ],
            ],
        ]);
        $query->with('tenant');
        $query->with('server');
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if (!empty($this->tenant_domain)) {
            $query->joinWith('tenant t');
            $query->andFilterWhere(['like','t.domain',$this->tenant_domain]);
        }
        if (!empty($this->server_name)) {
            $query->joinWith('server s');
            $query->andFilterWhere(['like','s.name',$this->server_name]);
        }

        $query->andFilterWhere([
            'id'          => $this->id,
            'status'      => $this->status,
            'tenant_id'          => $this->tenant_id,
        ]);

        $query->andFilterWhere(['like', 'text', $this->text])
                ->andFilterWhere(['like', 'login', $this->login])
                ->andFilterWhere(['like', 'phone', $this->phone])
                ->andFilterWhere(['like', 'signature', $this->signature])
                ->andFilterWhere(['like', 'response', $this->response]);
        return $dataProvider;
    }

}
