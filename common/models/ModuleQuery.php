<?php

namespace common\models;

use yii\base\InvalidParamException;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Module]].
 *
 * @see Module
 */
class ModuleQuery extends ActiveQuery
{
    /**
     * @param $active 1|0
     * @return $this
     */
    public function active($active)
    {
        if (!in_array($active, [0, 1])) {
            throw new InvalidParamException('The active param must be in values 1 or 0');
        }

        return $this->andWhere(['active' => $active]);
    }

    /**
     * @inheritdoc
     * @return Module[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Module|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
