<?php
/**
 * Created by PhpStorm.
 * User: zigfried123
 * Date: 14.02.2018
 * Time: 15:58
 */

namespace common\models;

use \Yii;


class LoginMembersOfTenantStrategy
{
    private $lang;

    public function __construct($lang)
    {
        $this->lang = $lang;
    }

    public function getLanguage()
    {
        return $this->getLanguageFromUrl();
    }

    private function getLanguageFromUrl()
    {

        $languages = Yii::$app->bootstrapLanguage->supportedLanguages;

        if ($this->lang !== null && $this->hasLanguage($this->lang,$languages)) {
            return $this->lang;

        }

        return substr(explode(';',Yii::$app->request->headers['accept-language'])[0],0,2);

    }

    private function hasLanguage($lang,$languages)
    {

        foreach($languages as $val){
            if(substr($val,0,2) === $lang){
                return true;
            }
        }

        return false;
    }

}