<?php

use common\modules\tenant\models\DefaultSettings;
use common\modules\tenant\models\TenantSetting;
use frontend\modules\tenant\models\Currency;
use app\modules\tenant\models\User;
use frontend\modules\tenant\models\UserExtrinsicDispatcher;

/**
 * functions.php file.
 * Global shorthand functions for commonly used Yii methods.
 */

/**
 * Returns the application instance.
 * @return \yii\web\Application
 */
function app()
{
    return Yii::$app;
}

/**
 * Dumps the given variable using CVarDumper::dumpAsString().
 *
 * @param mixed $var
 * @param int   $depth
 * @param bool  $highlight
 */
function dump($var, $depth = 10, $highlight = true)
{
    return \yii\helpers\VarDumper::dump($var, $depth, $highlight);
}

/**
 * Debug function with die() after
 * dd($var);
 */
function dd($var, $showDebugCallPath = false, $depth = 10, $highlight = true)
{
    if ($showDebugCallPath) {
        $db   = current(debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 1));
        $dump = [
            'file' => $db['file'],
            'line' => $db['line'],
            'dump' => $var,
        ];
    } else {
        $dump = $var;
    }

    dump($dump, $depth, $highlight);
    die();
}

/**
 * Returns user component.
 * @return \app\modules\tenant\models\User|UserExtrinsicDispatcher
 */
function user()
{

    /** @var \app\modules\tenant\models\User $user */
    $user = app()->getUser()->identity;

    if (Yii::$app->user->can('extrinsic_dispatcher')) {
        $user = User::getUserExtrinsicDispatcher($user);
    }

    return $user;
}

function getUserName()
{
    $user = user();

    return is_object($user) ? $user->name : null;
}

function getUserLastName()
{
    $user = user();

    return is_object($user) ? $user->last_name : null;
}

/**
 * Returns post data
 */
function post($name = null, $defaultValue = null)
{
    return app()->request->post($name, $defaultValue);
}

/**
 * Returns get data
 */
function get($name = null, $defaultValue = null)
{
    return app()->request->get($name, $defaultValue);
}

/**
 * Yii translate
 */
function t($category, $message, $params = [], $language = null)
{
    return Yii::t($category, $message, $params, $language);
}

/**
 * @return string Returns domain name
 */
function domainName()
{
    $host = $_SERVER['HTTP_HOST'];
    $host = explode('.', $host);
    $host = $host[0];

    return $host;
}

function getLanguagePrefix()
{
    $prefix = mb_substr(app()->language, 0, 2);

    return $prefix == 'ru' ? '' : '_' . $prefix;
}

function session()
{
    return app()->getSession();
}

/**
 * Return flash with models error.
 *
 * @param array $arModels
 */
function displayErrors(array $arModels)
{
    $error_mes = '';

    foreach ($arModels as $model) {
        $errors = $model->getErrors();

        foreach ($errors as $error) {
            foreach ($error as $mes) {
                if (!empty($mes)) {
                    $error_mes .= "$mes<br/>";
                }
            }
        }
    }

    session()->setFlash('error', $error_mes);
}

function getValue($var, $return = null)
{
    return isset($var) ? $var : $return;
}

/**
 * Функция перевода первого символа в верхний регистр
 *
 * @param string $str
 *
 * @return string
 */
function mb_ucfirst($str)
{
    $fc = mb_strtoupper(mb_substr($str, 0, 1));

    return $fc . mb_substr($str, 1);
}

/**
 * Конкатенация ФИО
 *
 * @param string $last_name
 * @param string $name
 * @param string $second_name
 *
 * @return string
 */
function getFullName($last_name, $name = '', $second_name = '')
{
    return trim($last_name . ' ' . $name . ' ' . $second_name);
}

function getShortName($last_name, $name = '', $second_name = '')
{
    $result = $last_name;

    if (!empty($name)) {
        $result .= ' ' . mb_substr($name, 0, 1, "UTF-8") . '. ';
    }

    if (!empty($second_name)) {
        $result .= mb_substr($second_name, 0, 1, "UTF-8") . '.';
    }

    return $result;
}

/**
 * Склонение слов
 *
 * @param integer $num
 * @param array   $arWords
 *
 * @return string
 */
function declension_words($num, $arWords)
{
    $number = $num < 21 ? $num : (int)substr($num, -1);

    if ($number == 1) {
        $w = $arWords[0];
    } elseif ($number > 1 && $number < 5) {
        $w = $arWords[1];
    } else {
        $w = $arWords[2];
    }

    return $w;
}

// Generate a random character string
function rand_str($length = 25, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890')
{
    // Length of character list
    $chars_length = (strlen($chars) - 1);

    // Start our string
    $string = $chars{rand(0, $chars_length)};

    // Generate random string
    for ($i = 1; $i < $length; $i = strlen($string)) {
        // Grab a random character from our list
        $r = $chars{rand(0, $chars_length)};

        // Make sure the same two characters don't appear next to each other
        if ($r != $string{$i - 1}) {
            $string .= $r;
        }
    }

    return $string;
}

function phoneFilter($phone)
{
    return preg_replace("/[^0-9]/", '', $phone);
}

function getCurrencySymbol($city_id = null)
{
    $currencyId = TenantSetting::getSettingValue(
        user()->tenant_id, DefaultSettings::SETTING_CURRENCY, $city_id);

    return Currency::getCurrencySymbol($currencyId);
}

/**
 * Return formated sum
 *
 * @param float   $sum
 * @param integer $currencyId
 *
 * @return float
 */
function getFormatedSum($sum, $currencyId)
{
    $symbol = Currency::getCurrencySymbol($currencyId);
    if ($symbol == '$') {
        return $symbol . ' ' . app()->formatter->asDecimal($value);
    }

    return app()->formatter->asDecimal($sum) . ' ' . $symbol;
}

/**
 *
 * @param string $key Name of file which has needed translation
 * @param array  $arrayToTranslate An array to translate
 *
 * @return array Translated array
 */
function translateNumericArray($key, $arrayToTranslate)
{
    $translated = [];
    $length     = count($arrayToTranslate);
    for ($i = 0; $i < $length; $i++) {
        if ($arrayToTranslate[$i] != null) {
            $translated[$i] = t($key, $arrayToTranslate[$i]);
        }
    }

    return $translated;
}

/**
 *
 * @param string $key Name of file which has needed translation
 * @param array  $arrayToTranslate An array to translate
 *
 * @return array Translated array
 */
function translateAssocArray($tkey, $arrayToTranslate, $multiLevel = false, $specialKey = '')
{
    if ($multiLevel == false) {
        $translated = [];
        foreach ($arrayToTranslate as $key => $value) {
            $translated[$key] = t($tkey, $value);
        }

        return $translated;
    } elseif ($multiLevel == true && isset($specialKey) == true) {
        $arrKeys       = array_keys($arrayToTranslate);
        $arrKeysLength = count($arrKeys);
        for ($i = 0; $i < $arrKeysLength; $i++) {
            foreach ($arrayToTranslate as $key => $value) {
                $arrayToTranslate[$arrKeys[$i]][$specialKey] = t($tkey, $arrayToTranslate[$arrKeys[$i]][$specialKey]);
            }
        }

        return $arrayToTranslate;
    }
}

/**
 * Sending email
 *
 * @param array $params
 *
 * @return bool
 */
function sendMail($params)
{
    /* @var $mailer \yii\mail\BaseMailer */
    $mailer = Yii::$app->mailer;

    $defaultLayout      = $mailer->htmlLayout;
    $mailer->htmlLayout = isset($params['LAYOUT']) ? $params['LAYOUT'] : $defaultLayout;

    $message = $mailer->compose($params['TEMPLATE'], ['data' => $params['DATA']])
        ->setFrom([Yii::$app->params['infoEmail'] => Yii::$app->name])
        ->setTo($params['TO'])
        ->setSubject($params['SUBJECT']);
    if (isset($params['DATA']['FILES'])) {
        foreach ($params['DATA']['FILES'] as $file) {
            $message->attach($file);
        }
    }
    $result             = $message->send();
    $mailer->htmlLayout = $defaultLayout;

    return $result;
}

function getUploadMaxFileSize($mb = false)
{
    $upload_max_filesize = (int)ini_get('upload_max_filesize');

    return $mb ? $upload_max_filesize : $upload_max_filesize * 1048576;
}
