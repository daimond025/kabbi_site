<?php

$params = require __DIR__ . '/../../common/config/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'language'   => 'ru-RU',
    'name'       => getenv('APPLICATION_NAME'),
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'aliases'    => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'cache' => 'cache',
        ],

        'bootstrapLanguage' => [
            'class'              => 'frontend\bootstrap\Language',
            'supportedLanguages' => array_keys($params['supportedLanguages']),
        ],

        'cache' => [
            'class'     => 'yii\redis\Cache',
            'redis'     => $db['redisCache'],
            'keyPrefix' => getenv('REDIS_CACHE_KEY_PREFIX'),
        ],

        'formatter' => [
            'class'                  => 'common\components\formatter\Formatter',
            'numberFormatterOptions' => [NumberFormatter::MIN_FRACTION_DIGITS => 0],
        ],

        'i18n' => [
            'translations' => [
                '*'    => [
                    'class'            => 'yii\i18n\DbMessageSource',
                    'forceTranslation' => true,
                    'enableCaching'    => YII_ENV_PROD,
                    'cachingDuration'  => 3600,
                ],
                'app*' => [
                    'class'            => 'yii\i18n\DbMessageSource',
                    'forceTranslation' => true,
                    'enableCaching'    => YII_ENV_PROD,
                    'cachingDuration'  => 3600,
                ],
            ],
        ],

        'mailer' => [
            'class'            => 'yii\swiftmailer\Mailer',
            'viewPath'         => '@common/mail',
            'useFileTransport' => false,
            'transport'        => [
                'class'      => 'Swift_SmtpTransport',
                'host'       => getenv('MAIL_DISPATCH_HOST'),
                'port'       => getenv('MAIL_DISPATCH_PORT'),
                'username'   => getenv('MAIL_DISPATCH_USERNAME'),
                'password'   => getenv('MAIL_DISPATCH_PASSWORD'),
                'encryption' => getenv('MAIL_DISPATCH_ENCRYPTION'),
            ],
        ],

        'mailer_log' => [
            'class'            => 'yii\swiftmailer\Mailer',
            'viewPath'         => '@common/mail',
            'useFileTransport' => false,
            'transport'        => [
                'class'      => 'Swift_SmtpTransport',
                'host'       => getenv('MAIL_SUPPORT_HOST'),
                'port'       => getenv('MAIL_SUPPORT_PORT'),
                'username'   => getenv('MAIL_SUPPORT_USERNAME'),
                'password'   => getenv('MAIL_SUPPORT_PASSWORD'),
                'encryption' => getenv('MAIL_SUPPORT_ENCRYPTION'),
            ],
        ],

        'session' => [
            'class'   => 'yii\redis\Session',
            'timeout' => 60 * 60 * 8,
            'redis'   => $db['redisMainSession'],
        ],

        'urlManager'                   => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
        ],

        // Databases
        'db'                           => $db['dbMain'],
        'elasticsearch'                => $db['elasticSearch'],
        'kamailio'                     => $db['dbKamailio'],
        'mongodb'                      => $db['mongodbMain'],
        'redis_pub_sub'                => $db['redisMainPubSub'],
        'redis_workers'                => $db['redisMainWorkers'],
        'redis_orders_active'          => $db['redisMainOrdersActive'],
        'redis_phone_scenario'         => $db['redisMainPhoneScenario'],
        'redis_active_call'            => $db['redisMainActiveCall'],
        'redis_queue_call'             => $db['redisMainQueueCall'],
        'redis_active_dispetcher'      => $db['redisMainActiveDispatcher'],
        'redis_free_dispetcher_queue'  => $db['redisMainDispatcherQueue'],
        'redis_parking_hashtable'      => $db['redisMainParkingHashTable'],
        'redis_dispetcher_live_notice' => $db['redisMainDispatcherLiveNotice'],
        'redis_order_event'            => $db['redisMainOrderEvent'],
        'redis_check_status'           => $db['redisMainCheckStatus'],
        'redis_cache_check_status'     => $db['redisCacheCheckStatus'],

        // Others
        'curl'                         => 'common\components\curl\Curl',
        'restCurl'                     => 'common\components\curl\RestCurl',
        'geoService'                   => 'common\components\geoService\GeoService',

        'gearman' => [
            'class' => 'common\components\gearman\Gearman',
            'host'  => getenv('GEARMAN_MAIN_HOST'),
            'port'  => getenv('GEARMAN_MAIN_PORT'),
        ],

        'serviceApi' => [
            'class'             => 'common\components\serviceApi\ServiceApi',
            'baseUrl'           => getenv('API_SERVICE_URL'),
            'httpClient' => [
                'class' => 'common\components\httpClient\HttpClient',
                'timeout' => 10,
                'connectionTimeout' => 10
            ],
        ],

        'geocoder' => [
            'class' => 'common\components\taxiGeocoder\TaxiGeocoder',
            'url'   => getenv('API_SERVICE_URL'),
        ],

        'voipApi' => [
            'class' => 'common\components\voipApi\VoipApi',
            'url'   => getenv('API_VOIP_URL'),
        ],
    ],

    'params' => $params,
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class'      => 'yii\debug\Module',
        'allowedIPs' => ['84.201.247.242', '192.168.1.*', '127.0.0.1'],
        'panels'     => [
            'branch' => [
                'class'    => 'common\components\panels\branch\MercurialBranchPanel',
                'viewPath' => '@common/components/panels/branch/views',
            ],
        ],
    ];
}

if (YII_ENV_PROD) {
    $config['components']['assetManager'] = [
        'hashCallback' => function ($path) {
            return hash('md4', $path);
        }
    ];
}

return $config;
