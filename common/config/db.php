<?php

$db = [
    'dbMain' => [
        'class'               => 'yii\db\Connection',
        'charset'             => 'utf8',
        'tablePrefix'         => 'tbl_',
        'dsn'                 => getenv('DB_MAIN_DSN'),
        'username'            => getenv('DB_MAIN_USERNAME'),
        'password'            => getenv('DB_MAIN_PASSWORD'),
        'enableSchemaCache'   => getenv('DB_MAIN_SCHEMA_CACHE_ENABLE') ? true : false,
        'schemaCacheDuration' => (int)getenv('DB_MAIN_SCHEMA_CACHE_DURATION'),
    ],

    'elasticSearch' => [
        'class' => 'yii\elasticsearch\Connection',
        'nodes' => [
            ['http_address' => getenv('ELASTICSEARCH_HTTP_ADDRESS')],
        ],
    ],

    'dbKamailio' => [
        'class'    => 'yii\db\Connection',
        'charset'  => 'utf8',
        'dsn'      => getenv('DB_KAMAILIO_DSN'),
        'username' => getenv('DB_KAMAILIO_USERNAME'),
        'password' => getenv('DB_KAMAILIO_PASSWORD'),
    ],

    'mongodbMain' => [
        'class' => '\yii\mongodb\Connection',
        'dsn'   => getenv('MONGODB_MAIN_DSN'),
    ],

    'redisCache' => [
        'class'    => 'yii\redis\Connection',
        'hostname' => getenv('REDIS_CACHE_HOST'),
        'port'     => getenv('REDIS_CACHE_PORT'),
        'database' => getenv('REDIS_CACHE_DATABASE_MAIN'),
    ],

    'redisMainPubSub' => [
        'class'    => 'yii\redis\Connection',
        'hostname' => getenv('REDIS_MAIN_HOST'),
        'port'     => getenv('REDIS_MAIN_PORT'),
    ],

    'redisMainWorkers' => [
        'class'    => 'yii\redis\Connection',
        'hostname' => getenv('REDIS_MAIN_HOST'),
        'port'     => getenv('REDIS_MAIN_PORT'),
        'database' => getenv('REDIS_MAIN_DATABASE_WORKERS'),
    ],

    'redisMainOrdersActive' => [
        'class'    => 'yii\redis\Connection',
        'hostname' => getenv('REDIS_MAIN_HOST'),
        'port'     => getenv('REDIS_MAIN_PORT'),
        'database' => getenv('REDIS_MAIN_DATABASE_ORDERS_ACTIVE'),
    ],

    'redisMainPhoneScenario' => [
        'class'    => 'yii\redis\Connection',
        'hostname' => getenv('REDIS_MAIN_HOST'),
        'port'     => getenv('REDIS_MAIN_PORT'),
        'database' => getenv('REDIS_MAIN_DATABASE_PHONE_SCENARIO'),
    ],

    'redisMainActiveCall' => [
        'class'    => 'yii\redis\Connection',
        'hostname' => getenv('REDIS_MAIN_HOST'),
        'port'     => getenv('REDIS_MAIN_PORT'),
        'database' => getenv('REDIS_MAIN_DATABASE_ACTIVE_CALL'),
    ],

    'redisMainQueueCall' => [
        'class'    => 'yii\redis\Connection',
        'hostname' => getenv('REDIS_MAIN_HOST'),
        'port'     => getenv('REDIS_MAIN_PORT'),
        'database' => getenv('REDIS_MAIN_DATABASE_QUEUE_CALL'),
    ],

    'redisMainActiveDispatcher' => [
        'class'    => 'yii\redis\Connection',
        'hostname' => getenv('REDIS_MAIN_HOST'),
        'port'     => getenv('REDIS_MAIN_PORT'),
        'database' => getenv('REDIS_MAIN_DATABASE_ACTIVE_DISPATCHER'),
    ],

    'redisMainDispatcherQueue' => [
        'class'    => 'yii\redis\Connection',
        'hostname' => getenv('REDIS_MAIN_HOST'),
        'port'     => getenv('REDIS_MAIN_PORT'),
        'database' => getenv('REDIS_MAIN_DATABASE_FREE_DISPATCHER_QUEUE'),
    ],

    'redisMainParkingHashTable' => [
        'class'    => 'yii\redis\Connection',
        'hostname' => getenv('REDIS_MAIN_HOST'),
        'port'     => getenv('REDIS_MAIN_PORT'),
        'database' => getenv('REDIS_MAIN_DATABASE_PARKING_HASH_TABLE'),
    ],

    'redisMainSession' => [
        'class'    => 'yii\redis\Connection',
        'hostname' => getenv('REDIS_MAIN_HOST'),
        'port'     => getenv('REDIS_MAIN_PORT'),
        'database' => getenv('REDIS_MAIN_DATABASE_SESSION'),
    ],

    'redisMainDispatcherLiveNotice' => [
        'class'    => 'yii\redis\Connection',
        'hostname' => getenv('REDIS_MAIN_HOST'),
        'port'     => getenv('REDIS_MAIN_PORT'),
        'database' => getenv('REDIS_MAIN_DATABASE_DISPATCHER_LIVE_NOTICE'),
    ],

    'redisMainOrderEvent' => [
        'class'    => 'yii\redis\Connection',
        'hostname' => getenv('REDIS_MAIN_HOST'),
        'port'     => getenv('REDIS_MAIN_PORT'),
        'database' => getenv('REDIS_MAIN_DATABASE_ORDER_EVENT'),
    ],

    'redisMainWorkerOrders' => [
        'class'    => 'yii\redis\Connection',
        'hostname' => getenv('REDIS_MAIN_HOST'),
        'port'     => getenv('REDIS_MAIN_PORT'),
        'database' => getenv('REDIS_MAIN_DATABASE_WORKER_ORDERS'),
    ],

    'redisCacheCheckStatus' => [
        'class'    => 'yii\redis\Connection',
        'hostname' => getenv('REDIS_CACHE_HOST'),
        'port'     => getenv('REDIS_CACHE_PORT'),
        'database' => getenv('REDIS_CACHE_DATABASE_CHECK_STATUS'),
    ],

    'redisMainCheckStatus' => [
        'class'    => 'yii\redis\Connection',
        'hostname' => getenv('REDIS_MAIN_HOST'),
        'port'     => getenv('REDIS_MAIN_PORT'),
        'database' => getenv('REDIS_MAIN_DATABASE_CHECK_STATUS'),
    ],
];

if (!(defined('CONSOLE_APPLICATION') && CONSOLE_APPLICATION) && (bool)getenv('DB_MAIN_SLAVE_ENABLE')) {
    $db['dbMain']['slaveConfig'] = [
        'username' => getenv('DB_MAIN_SLAVE_USERNAME'),
        'password' => getenv('DB_MAIN_SLAVE_PASSWORD'),
        'charset'  => 'utf8',
    ];

    $db['dbMain']['slaves'] = [
        ['dsn' => getenv('DB_MAIN_SLAVE_DSN')],
    ];
}

return $db;