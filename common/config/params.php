<?php

return [
    'upload'        => getenv('COMMON_UPLOAD_DIR'),
    'uploadScanDir' => getenv('COMMON_UPLOAD_SCAN_DIR'),

    'adminEmail'    => getenv('COMMON_ADMIN_EMAIL'),
    'infoEmail'     => getenv('COMMON_INFO_EMAIL'),
    'supportPhone'  => getenv('COMMON_SUPPORT_PHONE'),
    'supportEmail'  => getenv('COMMON_SUPPORT_EMAIL'),
    'supportEmail2' => getenv('COMMON_SUPPORT_EMAIL2'),
    'salesPhone'    => getenv('COMMON_SALES_PHONE'),
    'salesEmail'    => getenv('COMMON_SALES_EMAIL'),
    'landingUrl'    => getenv('COMMON_LANDING_URL'),
    // main site in email template
    'siteHost'      => getenv('COMMON_SITE_HOST'),
    'site.protocol' => getenv('COMMON_SITE_PROTOCOL'),
    'site.domain'   => getenv('COMMON_SITE_DOMAIN'),

    'softphone.protocol'     => getenv('SOFTPHONE_PROTOCOL'),
    'softphone.domain'       => getenv('SOFTPHONE_DOMAIN'),
    'softphone.subDomain'    => getenv('SOFTPHONE_SUB_DOMAIN'),
    'softphone.port'         => getenv('SOFTPHONE_PORT'),
    'softphone.callUri'      => getenv('SOFTPHONE_CALL_URI'),
    'softphone.asteriskHost' => getenv('SOFTPHONE_ASTERISK_HOST'),

    'phoneApiUrl'         => getenv('API_PHONE_URL'),
    'nodeApiUrl'          => getenv('SERVICE_ENGINE_URL'),
    'chatSocketServerUrl' => getenv('FRONTEND_SERVICE_CHAT_EXTERNAL_URL'),
    'chatServiceUrl'      => getenv('SERVICE_CHAT_URL'),

    'user.passwordResetTokenExpire' => 3600,
    'rememberMe'                    => 3600 * 24 * 365,
    'supportedLanguages'            => require __DIR__ . '/supportedLanguages.php',
];
