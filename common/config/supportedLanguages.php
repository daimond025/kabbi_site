<?php

return [
    'az'    => 'Azerbaijani',
    'de'    => 'German',
    'en-US' => 'English',
    'ru'    => 'Russian',
    'sr'    => 'Serbian',
    'ro'    => 'Romanian',
    'uz'    => 'Uzbek',
    'bg'    => 'Bulgarian',
    'ar'    => 'Arabic',
    'tg'    => 'Tajik',
    'ky'    => 'Kirghiz',
];