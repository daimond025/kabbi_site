<?php

namespace common\modules\city\models;

use common\modules\city\models\Republic;
use app\modules\city\models\TblRepublic;
use Yii;

/**
 * This is the model class for table "{{%tbl_country}}".
 *
 * @property integer $country_id
 * @property string $name
 *
 * @property TblRepublic[] $tblRepublics
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%country}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'country_id' => Yii::t('city', 'Country ID'),
            'name' => Yii::t('city', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepublics()
    {
        return $this->hasMany(Republic::className(), ['country_id' => 'country_id']);
    }

    public static function getCountryList()
    {
        return  self::find()->asArray()->orderBy('name' . getLanguagePrefix())->all();
    }

}
