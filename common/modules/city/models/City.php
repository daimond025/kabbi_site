<?php

namespace common\modules\city\models;

use common\modules\employee\models\worker\Worker;
use common\modules\employee\models\worker\WorkerHasCity;
use common\modules\tenant\models\TenantHasCity;
use frontend\modules\tenant\models\UserHasCity;
use Yii;

/**
 * This is the model class for table "{{%tbl_city}}".
 *
 * @property string              $city_id
 * @property string              $republic_id
 * @property string              $name
 * @property string              $lat
 * @property string              $lon
 * @property Car[]               $tblCars
 * @property Republic            $republic
 * @property MessageCity[]       $tblMessageCities
 * @property TaxiTariffHasCity[] $tblTaxiTariffHasCities
 * @property TaxiTariff[]        $tariffs
 * @property TenantHasCity[]     $tblTenantHasCities
 * @property Tenant[]            $tenants
 * @property UserHasCity[]       $tblUserHasCities
 * @property User[]              $users
 * @property WorkerHasCity[]     $workerHasCities
 * @property Worker[]            $workers
 */
class City extends \yii\db\ActiveRecord
{
    private static $_timeOffset = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%city}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['search'], 'boolean'],
            [['republic_id', 'name'], 'required'],
            [['republic_id'], 'integer'],
            [['name'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'city_id'     => Yii::t('city', 'City ID'),
            'republic_id' => Yii::t('city', 'Republic ID'),
            'name'        => Yii::t('city', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepublic()
    {
        return $this->hasOne(Republic::className(), ['republic_id' => 'republic_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessageCities()
    {
        return $this->hasMany(MessageCity::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParkings()
    {
        return $this->hasMany(Parking::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxiTariffHasCities()
    {
        return $this->hasMany(TaxiTariffHasCity::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffs()
    {
        return $this->hasMany(TaxiTariff::className(),
            ['tariff_id' => 'tariff_id'])->viaTable('{{%tbl_taxi_tariff_has_city}}', ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantHasCities()
    {
        return $this->hasMany(TenantHasCity::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenants()
    {
        return $this->hasMany(Tenant::className(), ['tenant_id' => 'tenant_id'])->viaTable('{{%tbl_tenant_has_city}}',
            ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserHasCities()
    {
        return $this->hasMany(UserHasCity::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['user_id' => 'user_id'])->viaTable('{{%tbl_user_has_city}}',
            ['city_id' => 'city_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerHasCities()
    {
        return $this->hasMany(WorkerHasCity::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkers()
    {
        return $this->hasMany(WorkerHasCity::className(), ['worker_id' => 'worker_id'])
            ->via('workerHasCities');
    }

    /**
     * City time offset.
     *
     * @param integer $city_id
     *
     * @return integer
     */
    public static function getTimeOffset($city_id)
    {
        if (!isset(self::$_timeOffset[$city_id])) {
            $city = self::find()
                ->where(['city_id' => $city_id])
                ->with([
                    'republic' => function ($query) {
                        $query->select([
                            'republic_id',
                            'timezone',
                        ]);
                    },
                ])
                ->one();

            if (empty($city)) {
                return 0;
            }

            self::$_timeOffset[$city_id] = !empty($city->republic->timezone) ? $city->republic->timezone * 3600 : 0;
        }

        return self::$_timeOffset[$city_id];
    }

    /**
     * Получить данных о городе из кеша
     *
     * @param int $cityId
     *
     * @return array
     */
    public static function getCityDataFromCache($cityId)
    {
        $cityCacheKey = 'city_data_' . $cityId;
        $cityArr      = \common\helpers\CacheHelper::getFromCache($cityCacheKey, function () use ($cityId) {
            return self::Find()
                ->where(["city_id" => $cityId])
                ->asArray()
                ->one();
        });

        return $cityArr;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        app()->cache->delete('city_data_' . $this->city_id);
    }
}
