<?php

namespace app\modules\city;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\city\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    public function beforeAction($action)
    {
        //Update active user time
        if (!\Yii::$app->user->isGuest) {
            user()->updateUserActiveTime();
        }
        return parent::beforeAction($action);
    }
}
