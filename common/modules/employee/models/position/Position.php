<?php

namespace common\modules\employee\models\position;

use common\components\behaviors\CacheInvalidateBehavior;
use common\modules\car\models\transport\TransportType;
use common\modules\employee\models\documents\Document;
use common\modules\employee\models\worker\WorkerHasPosition;
use common\modules\tenant\models\TenantCityHasPosition;
use common\modules\tenant\modules\tariff\models\Module;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%position}}".
 *
 * @property integer             $position_id
 * @property string              $name
 * @property integer             $module_id
 * @property bool                $has_car
 * @property integer             $transport_type_id
 *
 * @property Module              $module
 * @property PositionHasField[]  $positionHasFields
 * @property PositionField[]     $fields
 * @property WorkerHasPosition[] $workerHasPositions
 */
class Position extends ActiveRecord
{
    const CACHE_KEY = 'position_list';

    const TAXI_DRIVER = 1;

    public function behaviors()
    {
        return [
            [
                'class' => CacheInvalidateBehavior::className(),
                'keys'  => [
                    self::CACHE_KEY,
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%position}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'module_id'], 'required'],
            [['module_id', 'transport_type_id'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [
                ['module_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Module::className(),
                'targetAttribute' => ['module_id' => 'id'],
            ],
            [['has_car'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'position_id'       => Yii::t('employee', 'Position ID'),
            'name'              => Yii::t('employee', 'Name'),
            'module_id'         => Yii::t('employee', 'Module ID'),
            'has_car'           => Yii::t('employee', 'Link the car'),
            'transport_type_id' => Yii::t('employee', 'Transport type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModule()
    {
        return $this->hasOne(Module::className(), ['id' => 'module_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositionHasFields()
    {
        return $this->hasMany(PositionHasField::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFields()
    {
        return $this->hasMany(PositionField::className(),
            ['field_id' => 'field_id'])->viaTable('{{%position_has_field}}', ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerHasPositions()
    {
        return $this->hasMany(WorkerHasPosition::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositionHasDocuments()
    {
        return $this->hasMany(PositionHasDocument::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(Document::className(),
            ['document_id' => 'document_id'])->viaTable('tbl_position_has_document', ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransportType()
    {
        return $this->hasOne(TransportType::className(), ['type_id' => 'transport_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(TenantCityHasPosition::className(), ['position_id' => 'position_id']);
    }

    /**
     * @inheritdoc
     * @return PositionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PositionQuery(get_called_class());
    }
}
