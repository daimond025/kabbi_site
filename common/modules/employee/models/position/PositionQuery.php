<?php


namespace common\modules\employee\models\position;


use common\modules\tenant\models\TenantCityHasPosition;
use yii\db\ActiveQuery;

class PositionQuery extends ActiveQuery
{
    /**
     * @param null $db
     * @return array|null|\yii\db\ActiveRecord
     */
    public function one($db = null)
    {
        $this->limit(1);

        return parent::one($db);
    }

    /**
     * @param int|array $cityId
     * @param null $tenantId
     * @return $this
     */
    public function byCity($cityId, $tenantId = null)
    {
        return $this->joinWith([
            'cities' => function (ActiveQuery $query) use ($cityId, $tenantId) {
                $query->where(['city_id' => $cityId]);
                $query->andFilterWhere([TenantCityHasPosition::tableName() . '.tenant_id' => $tenantId]);
            },
        ], false);
    }

    /**
     * @return $this
     */
    public function onlyHasCar()
    {
        return $this->andWhere(['has_car' => 1]);
    }
}