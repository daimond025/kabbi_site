<?php

namespace common\modules\employee\models\position;

use common\modules\employee\models\position\Position;
use common\modules\employee\models\position\PositionField;
use Yii;

/**
 * This is the model class for table "{{%position_has_field}}".
 *
 * @property integer $position_id
 * @property integer $field_id
 *
 * @property Position $position
 * @property PositionField $field
 */
class PositionHasField extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%position_has_field}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position_id', 'field_id'], 'required'],
            [['position_id', 'field_id'], 'integer'],
            [['position_id'], 'exist', 'skipOnError' => true, 'targetClass' => Position::className(), 'targetAttribute' => ['position_id' => 'position_id']],
            [['field_id'], 'exist', 'skipOnError' => true, 'targetClass' => PositionField::className(), 'targetAttribute' => ['field_id' => 'field_id']],
            [['position_id', 'field_id'], 'unique', 'targetAttribute' => ['position_id', 'field_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'position_id' => Yii::t('employee', 'Position ID'),
            'field_id' => Yii::t('employee', 'Field ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getField()
    {
        return $this->hasOne(PositionField::className(), ['field_id' => 'field_id']);
    }
}
