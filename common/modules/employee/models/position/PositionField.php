<?php

namespace common\modules\employee\models\position;

use common\modules\employee\models\worker\WorkerPositionFieldValue;
use Yii;

/**
 * This is the model class for table "{{%position_field}}".
 *
 * @property integer $field_id
 * @property string $name
 * @property string $type
 *
 * @property PositionHasField[] $positionHasFields
 * @property Position[] $positions
 * @property WorkerPositionFieldValue[] $workerPositionFieldValues
 */
class PositionField extends \yii\db\ActiveRecord
{
    const STRING = 'string';
    const INT = 'int';
    const TEXT = 'text';
    const CHECKBOX = 'checkbox';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%position_field}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['type'], 'string'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'field_id' => Yii::t('employee', 'Field ID'),
            'name'     => Yii::t('employee', 'Name'),
            'type'     => Yii::t('employee', 'Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositionHasFields()
    {
        return $this->hasMany(PositionHasField::className(), ['field_id' => 'field_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositions()
    {
        return $this->hasMany(Position::className(),
            ['position_id' => 'position_id'])->viaTable('{{%position_has_field}}', ['field_id' => 'field_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerPositionFieldValues()
    {
        return $this->hasMany(WorkerPositionFieldValue::className(), ['field_id' => 'field_id']);
    }

    public static function getTypeList()
    {
        return [
            self::INT,
            self::STRING,
            self::TEXT,
            self::CHECKBOX,
        ];
    }
}
