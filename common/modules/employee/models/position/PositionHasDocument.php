<?php

namespace common\modules\employee\models\position;

use common\modules\employee\models\documents\Document;
use Yii;

/**
 * This is the model class for table "{{%position_has_document}}".
 *
 * @property integer $position_id
 * @property integer $document_id
 *
 * @property Document $document
 * @property Position $position
 */
class PositionHasDocument extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%position_has_document}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position_id', 'document_id'], 'required'],
            [['position_id', 'document_id'], 'integer'],
            [
                ['document_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Document::className(),
                'targetAttribute' => ['document_id' => 'document_id'],
            ],
            [
                ['position_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Position::className(),
                'targetAttribute' => ['position_id' => 'position_id'],
            ],
            ['document_id', 'unique', 'targetAttribute' => ['document_id', 'position_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'position_id' => Yii::t('employee', 'Position ID'),
            'document_id' => Yii::t('employee', 'Document ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        return $this->hasOne(Document::className(), ['document_id' => 'document_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }
}
