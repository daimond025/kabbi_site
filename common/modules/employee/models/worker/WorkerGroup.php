<?php

namespace common\modules\employee\models\worker;

use common\modules\city\models\City;
use common\modules\tenant\models\Tenant;
use common\modules\employee\models\position\Position;
use frontend\modules\car\models\Car;
use frontend\modules\car\models\CarClass;
use Yii;

/**
 * This is the model class for table "{{%worker_group}}".
 *
 * @property integer $group_id
 * @property integer $tenant_id
 * @property string $name
 * @property integer $class_id
 * @property integer $block
 * @property integer $position_id
 * @property integer $position_class_id
 *
 * @property Car[] $cars
 * @property CarClass $class
 * @property Tenant $tenant
 * @property Position $position
 * @property Position $positionClass
 * @property WorkerGroupCanViewClientTariff[] $workerGroupCanViewClientTariffs
 * @property WorkerGroupHasCity[] $workerGroupHasCities
 * @property City[] $cities
 * @property WorkerGroupHasTariff[] $workerGroupHasTariffs
 * @property WorkerHasPosition[] $workerHasPositions
 */
class WorkerGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'name'], 'required'],
            [['tenant_id', 'class_id', 'block', 'position_id', 'position_class_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'group_id'          => 'Group ID',
            'tenant_id'         => 'Tenant ID',
            'name'              => 'Name',
            'class_id'          => 'Class ID',
            'block'             => 'Block',
            'position_id'       => 'Position ID',
            'position_class_id' => 'Position Class ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::className(), ['group_id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClass()
    {
        return $this->hasOne(CarClass::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositionClass()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerGroupCanViewClientTariffs()
    {
        return $this->hasMany(WorkerGroupCanViewClientTariff::className(), ['group_id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerGroupHasCities()
    {
        return $this->hasMany(WorkerGroupHasCity::className(), ['group_id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['city_id' => 'city_id'])->viaTable('{{%worker_group_has_city}}',
            ['group_id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerGroupHasTariffs()
    {
        return $this->hasMany(WorkerGroupHasTariff::className(), ['group_id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerHasPositions()
    {
        return $this->hasMany(WorkerHasPosition::className(), ['group_id' => 'group_id']);
    }
}
