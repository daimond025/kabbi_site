<?php

namespace common\modules\employee\models\worker;

use Yii;
use common\modules\employee\models\documents\WorkerDocumentScan;
use common\modules\employee\models\documents\WorkerDriverLicense;
use common\modules\employee\models\documents\WorkerInn;
use common\modules\employee\models\documents\WorkerMedicalCertificate;
use common\modules\employee\models\documents\WorkerOgrnip;
use common\modules\employee\models\documents\WorkerOsago;
use common\modules\employee\models\documents\WorkerPassport;
use common\modules\employee\models\documents\WorkerSnils;
use common\modules\employee\models\documents\WorkerPts;
use common\modules\employee\models\documents\Document;

/**
 * This is the model class for table "{{%worker_has_document}}".
 *
 * @property integer $id
 * @property integer $worker_id
 * @property integer $document_id
 * @property integer $has_position_id
 *
 * @property WorkerDocumentScan[] $workerDocumentScans
 * @property WorkerDriverLicense[] $workerDriverLicenses
 * @property Document $document
 * @property Worker $worker
 * @property WorkerInn[] $workerInns
 * @property WorkerMedicalCertificate[] $workerMedicalCertificates
 * @property WorkerOgrnip[] $workerOgrnips
 * @property WorkerOsago[] $workerOsagos
 * @property WorkerPassport[] $workerPassports
 * @property WorkerSnils[] $workerSnils
 * @property WorkerPts[] $workerPts
 */
class WorkerHasDocument extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_has_document}}';
    }

    /**
     *
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['worker_id', 'document_id'], 'required'],
            [['worker_id', 'document_id'], 'integer'],
            [
                ['document_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Document::className(),
                'targetAttribute' => ['document_id' => 'document_id'],
            ],
            [
                ['worker_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Worker::className(),
                'targetAttribute' => ['worker_id' => 'worker_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('employee', 'ID'),
            'worker_id'   => Yii::t('employee', 'Worker ID'),
            'document_id' => Yii::t('employee', 'Document ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentScans()
    {
        return $this->hasMany(WorkerDocumentScan::className(), ['worker_document_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriverLicenses()
    {
        return $this->hasOne(WorkerDriverLicense::className(), ['worker_document_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        return $this->hasOne(Document::className(), ['document_id' => 'document_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInn()
    {
        return $this->hasOne(WorkerInn::className(), ['worker_document_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMedicalCertificate()
    {
        return $this->hasOne(WorkerMedicalCertificate::className(), ['worker_document_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOgrnip()
    {
        return $this->hasOne(WorkerOgrnip::className(), ['worker_document_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOsago()
    {
        return $this->hasOne(WorkerOsago::className(), ['worker_document_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPassport()
    {
        return $this->hasOne(WorkerPassport::className(), ['worker_document_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSnils()
    {
        return $this->hasOne(WorkerSnils::className(), ['worker_document_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPts()
    {
        return $this->hasOne(WorkerPts::className(), ['worker_document_id' => 'id']);
    }
}
