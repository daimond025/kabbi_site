<?php

namespace common\modules\employee\models\worker;

use common\modules\city\models\City;
use yii\caching\TagDependency;

/**
 * This is the model class for table "{{%worker_has_city}}".
 *
 * @property integer $id
 * @property integer $worker_id
 * @property integer $city_id
 *
 * @property City    $city
 * @property Worker  $worker
 */
class WorkerHasCity extends \yii\db\ActiveRecord
{
    const CACHE_TAG = 'worker_has_city';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_has_city}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['worker_id', 'city_id'], 'required'],
            [['worker_id', 'city_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'worker_id' => 'Worker ID',
            'city_id'   => 'City ID',
        ];
    }


    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        TagDependency::invalidate(\Yii::$app->cache, self::CACHE_TAG);;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * Saving many2many
     *
     * @param int   $workerId
     * @param int[] $cityIds
     *
     * @throws \yii\db\Exception
     */
    public static function saveMany($workerId, $cityIds)
    {
        if (!empty($cityIds)) {
            $values = array_map(function ($cityId) use ($workerId) {
                return [$workerId, $cityId];
            }, $cityIds);

            \Yii::$app->getDb()
                ->createCommand()
                ->batchInsert(self::tableName(), ['worker_id', 'city_id'], $values)
                ->execute();
        }
        TagDependency::invalidate(\Yii::$app->cache, self::CACHE_TAG);
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new WorkerHasCityQuery(get_called_class());
    }
}
