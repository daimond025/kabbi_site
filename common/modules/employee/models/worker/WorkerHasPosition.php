<?php

namespace common\modules\employee\models\worker;

use common\modules\employee\models\position\Position;
use common\modules\employee\models\position\PositionField;
use frontend\modules\car\models\Car;
use Yii;

/**
 * This is the model class for table "{{%worker_has_position}}".
 *
 * @property integer $id
 * @property integer $worker_id
 * @property integer $position_id
 * @property integer $active
 * @property integer $group_id
 * @property integer $rating
 *
 * @property WorkerHasCar[] $workerHasCars
 * @property Car[] $cars
 * @property Position $position
 * @property Worker $worker
 * @property WorkerPositionFieldValue[] $workerPositionFieldValues
 * @property PositionField[] $fields
 */
class WorkerHasPosition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_has_position}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['worker_id', 'position_id'], 'required'],
            [['worker_id', 'position_id', 'active', 'group_id'], 'integer'],
            [
                'position_id',
                'unique',
                'targetAttribute' => ['worker_id', 'position_id'],
                'message'         => t('employee', 'The position has already added'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                => Yii::t('employee', 'ID'),
            'worker_id'         => Yii::t('employee', 'Worker ID'),
            'position_id'       => Yii::t('employee', 'Position ID'),
            'active'            => Yii::t('employee', 'Active'),
            'group_id'          => Yii::t('app', 'Group'),
            'rating'            => Yii::t('employee', 'Rating'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerHasCars()
    {
        return $this->hasMany(WorkerHasCar::className(), ['has_position_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::className(), ['car_id' => 'car_id'])->viaTable('{{%worker_has_car}}',
            ['has_position_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerPositionFieldValues()
    {
        return $this->hasMany(WorkerPositionFieldValue::className(), ['has_position_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(WorkerHasDocument::className(), ['has_position_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFields()
    {
        return $this->hasMany(PositionField::className(),
            ['field_id' => 'field_id'])->viaTable('{{%worker_position_field_value}}', ['has_position_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(WorkerGroup::className(), ['group_id' => 'group_id']);
    }
}
