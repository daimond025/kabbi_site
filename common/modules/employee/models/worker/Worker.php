<?php

namespace common\modules\employee\models\worker;

use frontend\modules\order\models\WorkerShiftNote;
use common\modules\employee\models\position\Position;
use frontend\modules\companies\models\TenantCompany;
use Yii;
use common\modules\tenant\models\Tenant;
use common\modules\city\models\City;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%worker}}".
 *
 * @property integer             $worker_id
 * @property integer             $tenant_id
 * @property integer             $callsign
 * @property string              $password
 * @property string              $last_name
 * @property string              $name
 * @property string              $second_name
 * @property string              $phone
 * @property string              $photo
 * @property string              $device
 * @property string              $device_token
 * @property string              $lang
 * @property string              $description
 * @property string              $device_info
 * @property string              $email
 * @property string              $partnership
 * @property string              $birthday
 * @property integer             $block
 * @property integer             $activate
 * @property string              $create_time
 * @property string              $card_number
 * @property string              $yandex_account_number
 * @property integer             $tenant_company_id
 *
 * @property City[]              $cities
 * @property Car[]               $availableCars
 * @property Tenant              $tenant
 * @property WorkerHasPosition[] $workerHasPositions
 * @property WorkerHasCar[]      $cars
 * @property TenantCompany       $tenantCompany
 */
class Worker extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['last_name', 'name', 'second_name'], 'filter', 'filter' => 'trim'],
            [['phone', 'last_name', 'name', 'password', 'partnership'], 'required'],
            [['block', 'activate', 'tenant_company_id'], 'integer'],
            [['device', 'device_token', 'partnership', 'yandex_account_number'], 'string'],
            [['birthday'], 'safe'],
            [['password', 'photo', 'device_info'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 500],
            ['password', 'string', 'min' => 6, 'tooShort' => t('employee', 'at least 6 characters')],
            [['last_name', 'name', 'second_name', 'email', 'card_number'], 'string', 'max' => 45],
            [
                ['phone'],
                'filter',
                'filter' => function ($value) {
                    return preg_replace("/[^0-9]/", '', $value);
                },
            ],
            [['phone'], 'string', 'max' => 15],
            [
                'phone',
                'unique',
                'targetAttribute' => ['phone', 'tenant_id'],
                'message'         => t('validator',
                    'There is already a worker with the specified phone number, check in tabs "Active" and "Blocked"'),
            ],
            [['lang'], 'string', 'max' => 10],
            [['callsign'], 'integer', 'max' => 9999999],
            [
                ['callsign'],
                'unique',
                'targetAttribute' => ['callsign', 'tenant_id'],
                'message'         => t('validator', 'The callsign is busy'),
            ],
            ['email', 'email'],

            [
                ['tenant_company_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => TenantCompany::className(),
                'targetAttribute' => ['tenant_company_id' => 'tenant_company_id'],
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'worker_id'             => Yii::t('employee', 'Worker ID'),
            'tenant_id'             => Yii::t('employee', 'Tenant ID'),
            'callsign'              => Yii::t('employee', 'Callsign'),
            'password'              => Yii::t('employee', 'Password'),
            'last_name'             => Yii::t('user', 'Last name'),
            'name'                  => Yii::t('user', 'Name'),
            'second_name'           => Yii::t('user', 'Second name'),
            'phone'                 => Yii::t('employee', 'Phone'),
            'photo'                 => Yii::t('employee', 'Photo'),
            'device'                => Yii::t('employee', 'Device'),
            'device_token'          => Yii::t('employee', 'Device Token'),
            'lang'                  => Yii::t('employee', 'Lang'),
            'description'           => Yii::t('employee', 'Description'),
            'device_info'           => Yii::t('employee', 'Device Info'),
            'email'                 => Yii::t('employee', 'Email'),
            'partnership'           => Yii::t('employee', 'Collaboration type'),
            'birthday'              => Yii::t('employee', 'Birthday'),
            'block'                 => Yii::t('employee', 'Active'),
            'create_time'           => Yii::t('employee', 'Create Time'),
            'card_number'           => Yii::t('employee', 'Card number'),
            'yandex_account_number' => Yii::t('employee', 'Yandex account number'),
            'tenant_company_id'     => Yii::t('tenant_company', 'Companies'),
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class'      => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'password',
                ],
                'value'      => [$this, 'getPassword'],
            ],
            [
                'class'              => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_time',
                'updatedAtAttribute' => false,
            ],
        ];
    }

    public function getPassword()
    {
        $this->setPassword($this->password);

        return $this->password;
    }

    public function getNextCallsignNumber()
    {
        return $this->getLastCallsign() + 1;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerShiftNote()
    {
        return $this->hasOne(WorkerShiftNote::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShifts()
    {
        return $this->hasMany(WorkerShift::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerHasPositions()
    {
        return $this->hasMany(WorkerHasPosition::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActiveWorkerHasPositions()
    {
        return $this->hasMany(WorkerHasPosition::className(), ['worker_id' => 'worker_id'])
            ->onCondition(['active' => 1]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositions()
    {
        return $this->hasMany(Position::className(),
            ['position_id' => 'position_id'])->viaTable(WorkerHasPosition::tableName(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerHasCities()
    {
        return $this->hasMany(WorkerHasCity::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['city_id' => 'city_id'])->via('workerHasCities');
    }

    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerHasDocuments()
    {
        return $this->hasMany(WorkerHasDocument::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(WorkerHasCar::className(),
            ['has_position_id' => 'id'])->viaTable(WorkerHasPosition::tableName(), ['worker_id' => 'worker_id']);
    }

    public function getTenantCompany()
    {
        return $this->hasOne(TenantCompany::className(), ['tenant_company_id' => 'tenant_company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAvailableCars()
    {
        return $this->hasMany(WorkerHasCar::className(),
            ['has_position_id' => 'id'])
            ->via('activeWorkerHasPositions');
    }

    public function getLastCallsign()
    {
        return Yii::$app->db
            ->createCommand('SELECT MAX(callsign) FROM ' . $this->tableName() . 'WHERE `tenant_id`=' . $this->tenant_id)
            ->queryScalar();
    }

    public function getFullName()
    {
        return trim($this->last_name . ' ' . $this->name . ' ' . $this->second_name);
    }

    public function getIsOnline()
    {
        return in_array($this->worker_id, self::getOnlineWorkersId());
    }

    public static function getOnlineWorkers()
    {
        $redis_data = \Yii::$app->redis_workers->executeCommand('hvals', [user()->tenant_id]);

        return array_map(function ($item) {
            return (array)unserialize($item);
        }, $redis_data);
    }

    public static function getOnlineWorkersId()
    {
        return array_map(function ($item) {
            return (int)$item['worker']['worker_id'];
        }, (array)self::getOnlineWorkers());
    }

    public static function getNotActivatedWorkers($city_list)
    {
        $query = Worker::find();

        // add conditions that should always apply here

        $query
            ->alias('t')
            ->joinWith(['workerHasCities wc'])
            ->where([
                't.tenant_id' => user()->tenant_id,
                'wc.city_id'  => $city_list,
                't.block'     => 1,
                't.activate'  => 0,
            ]);

        return $query->count();
    }

    public static function getCountNotActivatedWorkers($conditions)
    {
        $query = Worker::find();

        // add conditions that should always apply here

        $query
            ->alias('t')
            ->joinWith(['workerHasCities wc'])
            ->where(array_merge([
                't.tenant_id' => user()->tenant_id,
                't.block'     => 1,
                't.activate'  => 0,
            ], $conditions));

        return $query->count();
    }

    public function beforeSave($insert)
    {
        if ((!$insert || $this->activate !== null) && isset($this->dirtyAttributes['activate'])) {
            $this->block = ($this->activate + 1) % 2;
        }

        if (empty($this->callsign)) {
            $this->callsign = $this->getNextCallsignNumber();
        }

        return parent::beforeSave($insert);
    }
}
