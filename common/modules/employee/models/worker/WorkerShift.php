<?php

namespace common\modules\employee\models\worker;

use common\modules\car\models\Car;
use common\modules\city\models\City;
use common\modules\employee\models\position\Position;
use frontend\modules\car\models\CarMileage;
use Yii;

/**
 * This is the model class for table "{{%worker_shift}}".
 *
 * @property integer            $id
 * @property integer            $worker_id
 * @property integer            $car_id
 * @property integer            $city_id
 * @property integer            $start_work
 * @property integer            $end_work
 * @property integer            $tariff_id
 * @property string             $pause_data
 * @property integer            $position_id
 * @property integer            $worker_late_time
 * @property integer            $worker_late_count
 * @property integer            $completed_order_count
 * @property integer            $rejected_order_count
 * @property integer            $accepted_order_offer_count
 * @property integer            $rejected_order_offer_count
 * @property integer            $order_payment_time
 * @property integer            $order_payment_count
 * @property string             $shift_end_reason
 * @property string             $shift_end_event_sender_type
 * @property integer            $shift_end_event_sender_id
 * @property integer            $group_id
 * @property integer            $group_priority
 * @property integer            $position_priority
 *
 * @property Position           $position
 * @property City               $city
 * @property WorkerGroup        $group
 * @property WorkerShiftOrder[] $shiftOrders
 * @property WorkerBlock[]      $blocks
 * @property CarMileage         $carMileage
 */
class WorkerShift extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_shift}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'worker_id',
                    'car_id',
                    'start_work',
                    'end_work',
                    'tariff_id',
                    'position_id',
                    'worker_late_time',
                    'worker_late_count',
                    'completed_order_count',
                    'rejected_order_count',
                    'accepted_order_offer_count',
                    'rejected_order_offer_count',
                    'shift_end_event_sender_id',
                    'order_payment_time',
                    'order_payment_count',
                    'group_id',
                    'group_priority',
                    'position_priority',
                ],
                'integer',
            ],
            [['car_id', 'start_work', 'tariff_id', 'position_id'], 'required'],
            [['pause_data', 'shift_end_reason', 'shift_end_event_sender_type'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'worker_id'   => 'Worker ID',
            'car_id'      => 'Car ID',
            'start_work'  => 'Start Work',
            'end_work'    => 'End Work',
            'tariff_id'   => 'Tariff ID',
            'pause_data'  => 'Pause Data',
            'position_id' => 'Position ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(WorkerTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::className(), ['car_id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShiftOrders()
    {
        return $this->hasMany(WorkerShiftOrder::className(), ['shift_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlocks()
    {
        return $this->hasMany(WorkerBlock::className(), ['shift_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(WorkerGroup::className(), ['group_id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarMileage()
    {
        return $this->hasOne(CarMileage::className(), ['shift_id' => 'id']);
    }
}
