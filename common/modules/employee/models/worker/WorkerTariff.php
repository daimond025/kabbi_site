<?php

namespace common\modules\employee\models\worker;

use common\modules\car\models\Car;
use common\modules\car\models\CarClass;
use common\modules\city\models\City;
use frontend\modules\companies\models\TenantCompany;
use Yii;

/**
 * This is the model class for table "{{%worker_tariff}}".
 *
 * @property integer $tariff_id
 * @property integer $tenant_id
 * @property integer $class_id
 * @property string $name
 * @property string $type
 * @property integer $block
 * @property integer $days
 * @property string $description
 * @property integer $position_id
 * @property integer $position_class_id
 * @property integer $tenant_company_id
 *
 * @property CarHasWorkerGroupTariff[] $carHasWorkerGroupTariffs
 * @property Car[] $cars
 * @property WorkerActiveAboniment[] $workerActiveAboniments
 * @property WorkerGroupHasTariff[] $workerGroupHasTariffs
 * @property WorkerOptionTariff[] $workerOptionTariffs
 * @property CarClass $class
 * @property Tenant $tenant
 * @property CarClass $class0
 * @property Position $position
 * @property Position $positionClass
 * @property WorkerTariffHasCity[] $workerTariffHasCities
 * @property City[] $cities
 * @property TenantCompany $tenantCompany
 */
class WorkerTariff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_tariff}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'position_id'], 'required'],
            [['tenant_id', 'class_id', 'block', 'days', 'position_id', 'position_class_id'], 'integer'],
            [['type', 'description'], 'string'],
            [['name'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tariff_id'         => 'Tariff ID',
            'tenant_id'         => 'Tenant ID',
            'class_id'          => 'Class ID',
            'name'              => 'Name',
            'type'              => 'Type',
            'block'             => 'Block',
            'days'              => 'Days',
            'description'       => 'Description',
            'position_id'       => 'Position ID',
            'position_class_id' => 'Position Class ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarHasWorkerGroupTariffs()
    {
        return $this->hasMany(CarHasWorkerGroupTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::className(), ['car_id' => 'car_id'])->viaTable('{{%car_has_worker_group_tariff}}',
            ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerActiveAboniments()
    {
        return $this->hasMany(WorkerActiveAboniment::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerGroupHasTariffs()
    {
        return $this->hasMany(WorkerGroupHasTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerOptionTariffs()
    {
        return $this->hasMany(WorkerOptionTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClass()
    {
        return $this->hasOne(CarClass::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClass0()
    {
        return $this->hasOne(CarClass::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositionClass()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerTariffHasCities()
    {
        return $this->hasMany(WorkerTariffHasCity::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['city_id' => 'city_id'])
            ->viaTable('{{%worker_tariff_has_city}}', ['tariff_id' => 'tariff_id']);
    }

    public function getTenantCompany()
    {
        return $this->hasOne(TenantCompany::className(), ['tenant_company_id' => 'tenant_company_id']);
    }
}
