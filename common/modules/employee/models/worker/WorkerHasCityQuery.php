<?php

namespace common\modules\employee\models\worker;

use yii\db\ActiveQuery;

/**
 * Class WorkerHasCityQuery
 * @package common\modules\employee\models\worker
 */
class WorkerHasCityQuery extends ActiveQuery
{
    public function byWorkerId($id, $alias = null)
    {
        return $this->andWhere([(!empty($alias) ?: "$alias.") . 'worker_id' => $id]);
    }

    public function lastActive($alias = null)
    {
        return $this->andWhere([(!empty($alias) ?: "$alias.") . 'last_active' => 1]);
    }
}