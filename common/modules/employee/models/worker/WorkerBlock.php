<?php

namespace common\modules\employee\models\worker;

use Yii;

/**
 * This is the model class for table "{{%worker_block}}".
 *
 * @property integer $block_id
 * @property integer $shift_id
 * @property string $type_block
 * @property integer $start_block
 * @property integer $end_block
 * @property integer $is_unblocked
 * @property integer $worker_id
 *
 * @property Worker $worker
 */
class WorkerBlock extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_block}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['shift_id', 'type_block', 'worker_id'], 'required'],
            [['shift_id', 'start_block', 'end_block', 'is_unblocked', 'worker_id'], 'integer'],
            [['type_block'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'block_id' => 'Block ID',
            'shift_id' => 'Shift ID',
            'type_block' => 'Type Block',
            'start_block' => 'Start Block',
            'end_block' => 'End Block',
            'is_unblocked' => 'Is Unblocked',
            'worker_id' => 'Worker ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }
}
