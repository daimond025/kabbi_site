<?php

namespace common\modules\employee\models\worker;

use common\modules\employee\models\position\PositionField;
use Yii;

/**
 * This is the model class for table "{{%worker_position_field_value}}".
 *
 * @property integer $id
 * @property integer $has_position_id
 * @property integer $field_id
 * @property integer $value_int
 * @property string $value_string
 * @property string $value_text
 *
 * @property PositionField $field
 * @property WorkerHasPosition $hasPosition
 */
class WorkerPositionFieldValue extends \yii\db\ActiveRecord
{
    public $value_checkbox;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_position_field_value}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['has_position_id', 'field_id'], 'required'],
            [['has_position_id', 'field_id', 'value_int'], 'integer'],
            [['value_text'], 'string'],
            [['value_checkbox'], 'boolean'],
            [['value_string'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => Yii::t('employee', 'ID'),
            'has_position_id' => Yii::t('employee', 'Has Position ID'),
            'field_id'        => Yii::t('employee', 'Field ID'),
            'value_int'       => Yii::t('employee', 'Value Int'),
            'value_string'    => Yii::t('employee', 'Value String'),
            'value_text'      => Yii::t('employee', 'Value Text'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getField()
    {
        return $this->hasOne(PositionField::className(), ['field_id' => 'field_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHasPosition()
    {
        return $this->hasOne(WorkerHasPosition::className(), ['id' => 'has_position_id']);
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->value_int = (int)$this->value_int;

        if ($this->value_int === 1 || $this->value_int === 0) {
            $this->value_checkbox = $this->value_int;
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (isset($this->value_checkbox)) {
                $this->value_int = $this->value_checkbox;
            }

            return true;
        }

        return false;
    }
}
