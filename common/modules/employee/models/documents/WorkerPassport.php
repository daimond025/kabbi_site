<?php

namespace common\modules\employee\models\documents;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%worker_passport}}".
 *
 * @property integer $passport_id
 * @property integer $worker_document_id
 * @property string $series
 * @property string $number
 * @property string $issued
 * @property string $registration
 * @property string $actual_address
 *
 * @property WorkerHasDocument $workerDocument
 */
class WorkerPassport extends WorkerDocumentBase
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_passport}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge([
            [['issued', 'registration', 'actual_address'], 'string'],
            [['series'], 'string', 'max' => 5],
            [['number'], 'string', 'max' => 10],
        ], parent::rules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'passport_id'        => Yii::t('employee', 'Passport ID'),
            'worker_document_id' => Yii::t('employee', 'Worker Document ID'),
            'series'             => Yii::t('employee', 'Series'),
            'number'             => Yii::t('employee', 'Number'),
            'issued'             => Yii::t('employee', 'Issued'),
            'registration'       => Yii::t('employee', 'Registration'),
            'actual_address'     => Yii::t('employee', 'Actual Address'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerDocument()
    {
        return $this->hasOne(WorkerHasDocument::className(), ['id' => 'worker_document_id']);
    }
}
