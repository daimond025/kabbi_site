<?php

namespace common\modules\employee\models\documents;

use common\modules\employee\models\worker\WorkerHasDocument;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%worker_snils}}".
 *
 * @property integer $id
 * @property integer $worker_document_id
 * @property string $number
 *
 * @property WorkerHasDocument $workerDocument
 */
class WorkerSnils extends WorkerDocumentBase
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_snils}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge([
            [['number'], 'string', 'max' => 15],
        ], parent::rules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                 => Yii::t('employee', 'ID'),
            'worker_document_id' => Yii::t('employee', 'Worker Document ID'),
            'number'             => Yii::t('employee', 'SNILS'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerDocument()
    {
        return $this->hasOne(WorkerHasDocument::className(), ['id' => 'worker_document_id']);
    }
}
