<?php

namespace common\modules\employee\models\documents;

use common\modules\employee\models\worker\WorkerHasDocument;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%worker_inn}}".
 *
 * @property integer $id
 * @property integer $worker_document_id
 * @property integer $number
 *
 * @property WorkerHasDocument $workerDocument
 */
class WorkerInn extends WorkerDocumentBase
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_inn}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge([
            [['number'], 'integer'],
        ], parent::rules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                 => Yii::t('employee', 'ID'),
            'worker_document_id' => Yii::t('employee', 'Worker Document ID'),
            'number'             => Yii::t('employee', 'INN'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerDocument()
    {
        return $this->hasOne(WorkerHasDocument::className(), ['id' => 'worker_document_id']);
    }
}
