<?php

namespace common\modules\employee\models\documents;

use common\modules\employee\models\worker\WorkerHasDocument;
use Yii;

/**
 * This is the model class for table "{{%worker_document_scan}}".
 *
 * @property integer $scan_id
 * @property integer $worker_document_id
 * @property string $filename
 *
 * @property \common\modules\employee\models\worker\WorkerHasDocument $workerDocument
 */
class WorkerDocumentScan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_document_scan}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['worker_document_id', 'filename'], 'required'],
            [['worker_document_id'], 'integer'],
            [['filename'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'scan_id'            => Yii::t('employee', 'Scan ID'),
            'worker_document_id' => Yii::t('employee', 'Worker Document ID'),
            'filename'           => Yii::t('app', 'Scan'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerDocument()
    {
        return $this->hasOne(WorkerHasDocument::className(), ['id' => 'worker_document_id']);
    }
}
