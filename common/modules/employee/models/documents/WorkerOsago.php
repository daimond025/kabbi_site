<?php

namespace common\modules\employee\models\documents;

use common\modules\employee\models\worker\WorkerHasDocument;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%worker_osago}}".
 *
 * @property integer $id
 * @property integer $worker_document_id
 * @property string $series
 * @property string $number
 * @property string $start_date
 * @property string $end_date
 *
 * @property WorkerHasDocument $workerDocument
 */
class WorkerOsago extends WorkerDocumentBase
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_osago}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge([
            [['start_date', 'end_date'], 'safe'],
            [['series'], 'string', 'max' => 255],
            [['number'], 'number'],
        ], parent::rules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                 => Yii::t('employee', 'ID'),
            'worker_document_id' => Yii::t('employee', 'Worker Document ID'),
            'series'             => Yii::t('employee', 'OSAGO Serial'),
            'number'             => Yii::t('employee', 'OSAGO Number'),
            'start_date'         => Yii::t('employee', 'Beginning'),
            'end_date'           => Yii::t('employee', 'Expiration'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerDocument()
    {
        return $this->hasOne(WorkerHasDocument::className(), ['id' => 'worker_document_id']);
    }
}
