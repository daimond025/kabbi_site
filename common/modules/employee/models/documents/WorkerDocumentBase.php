<?php

namespace common\modules\employee\models\documents;

use common\modules\employee\components\document\DocumentLayer;
use common\modules\employee\models\worker\WorkerHasDocument;
use yii\base\Exception;
use \yii\db\ActiveRecord;

class WorkerDocumentBase extends ActiveRecord
{
    public $worker_id;
    public $document_code;
    public $has_position_id;

    public function beforeSave($insert)
    {
        if ($insert) {
            $workerHasDocument = new WorkerHasDocument([
                'worker_id'   => $this->worker_id,
                'document_id' => DocumentLayer::getDccumentIdByCode($this->document_code),
            ]);

            if (!empty($this->has_position_id)) {
                $workerHasDocument->has_position_id = $this->has_position_id;
            }

            if (!$workerHasDocument->save()) {
                throw new Exception('Error to save WorkerHasDocument with entry params: "worker_id" = ' .
                    $this->worker_id . '; "document_id" = ' . $workerHasDocument->document_id);
            }

            $this->worker_document_id = $workerHasDocument->id;
        }

        return parent::beforeSave($insert);
    }
}