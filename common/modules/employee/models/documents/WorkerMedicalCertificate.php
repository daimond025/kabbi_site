<?php

namespace common\modules\employee\models\documents;

use common\modules\employee\models\worker\WorkerHasDocument;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%worker_medical_certificate}}".
 *
 * @property integer $id
 * @property integer $worker_document_id
 * @property string $number
 *
 * @property WorkerHasDocument $workerDocument
 */
class WorkerMedicalCertificate extends WorkerDocumentBase
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_medical_certificate}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge([
            [['number'], 'string', 'max' => 15],
        ], parent::rules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                 => Yii::t('employee', 'ID'),
            'worker_document_id' => Yii::t('employee', 'Worker Document ID'),
            'number'             => Yii::t('employee', 'Medical certificate'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerDocument()
    {
        return $this->hasOne(WorkerHasDocument::className(), ['id' => 'worker_document_id']);
    }
}
