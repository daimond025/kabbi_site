<?php

namespace common\modules\employee\models\documents;

use common\components\behaviors\CacheInvalidateBehavior;
use common\helpers\CacheHelper;
use common\modules\employee\models\position\Position;
use common\modules\employee\models\position\PositionHasDocument;
use common\modules\employee\models\worker\WorkerHasDocument;
use Yii;

/**
 * This is the model class for table "{{%document}}".
 *
 * @property integer $document_id
 * @property string $name
 * @property integer $code
 *
 * @property \common\modules\employee\models\worker\WorkerHasDocument[] $workerHasDocuments
 */
class Document extends \yii\db\ActiveRecord
{
    const CACHE_KEY = 'documentList';
    /**
     * Value is document code
     */
    const PASSPORT = 1;
    const INN = 2;
    const OGRNIP = 3;
    const SNILS = 4;
    const MEDICAL_CERT = 5;
    const OSAGO = 6;
    const DRIVER_LICENSE = 7;
    const PTS = 8;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%document}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code'], 'required'],
            [['code'], 'integer'],
            [['name'], 'string', 'max' => 45],
            [['code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'document_id' => Yii::t('employee', 'Document ID'),
            'name'        => Yii::t('employee', 'Name'),
            'code'        => Yii::t('employee', 'Code'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositionHasDocuments()
    {
        return $this->hasMany(PositionHasDocument::className(), ['document_id' => 'document_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPositions()
    {
        return $this->hasMany(Position::className(),
            ['position_id' => 'position_id'])->viaTable('{{%position_has_document}}', ['document_id' => 'document_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerHasDocuments()
    {
        return $this->hasMany(WorkerHasDocument::className(), ['document_id' => 'document_id']);
    }

    public function behaviors()
    {
        return [
            [
                'class' => CacheInvalidateBehavior::className(),
                'keys'  => [
                    self::CACHE_KEY,
                ],
            ],
        ];
    }
}
