<?php

namespace common\modules\employee\models\documents;

use common\modules\employee\models\worker\WorkerHasDocument;
use Yii;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%worker_driver_license}}".
 *
 * @property integer $id
 * @property integer $worker_document_id
 * @property string $series
 * @property string $number
 * @property string $category
 * @property string $start_date
 *
 * @property WorkerHasDocument $workerDocument
 */
class WorkerDriverLicense extends WorkerDocumentBase
{
    protected $_category = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_driver_license}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge([
            [['start_date', 'categoryMap'], 'safe'],
            [['series'], 'string', 'max' => 20],
            [['number'], 'number'],
        ], parent::rules());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                 => Yii::t('employee', 'ID'),
            'worker_document_id' => Yii::t('employee', 'Worker Document ID'),
            'series'             => Yii::t('employee', 'Series'),
            'number'             => Yii::t('employee', 'Number'),
            'category'           => Yii::t('employee', 'License Category'),
            'start_date'         => Yii::t('app', 'Start date of an experience'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerDocument()
    {
        return $this->hasOne(WorkerHasDocument::className(), ['id' => 'worker_document_id']);
    }

    /**
     * Driver license category map.
     * @return array
     */
    public function getDriverCategoryMap()
    {
        return [
            'A' => 'A',
            'B' => 'B',
            'C' => 'C',
            'D' => 'D',
            'E' => 'E',
        ];
    }

    public function getCategoryMap()
    {
        if (is_null($this->_category)) {
            $categories = explode(', ', $this->category);

            if (!empty($categories)) {
                $this->_category = array_combine($categories, $categories);
            } else {
                $this->_category = [];
            }
        }

        return $this->_category;
    }

    public function setCategoryMap($value)
    {
        $this->_category = is_array($value) ? $value : [];
    }

    public function beforeSave($insert)
    {
        $this->category = implode(', ', $this->categoryMap);

        return parent::beforeSave($insert);
    }
}
