<?php


namespace common\modules\employee\components;


use yii\base\InvalidConfigException;
use yii\base\Object;

class ArRepositoryBase extends Object
{
    public $modelClass;

    public function init()
    {
        parent::init();
        $this->configValidate();
    }

    protected function configValidate()
    {
        if (empty($this->modelClass)) {
            throw new InvalidConfigException('Field "modelClass" must be set');
        }
    }
}