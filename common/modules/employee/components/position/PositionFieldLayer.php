<?php

namespace common\modules\employee\components\position;

use common\modules\employee\models\position\Position;
use yii\base\Object;
use yii\helpers\ArrayHelper;

class PositionFieldLayer extends Object
{
    /**
     * Getting field map ['field_id' => ['name' => $name, 'type' => $type]]
     * @param array $fields List of common\modules\employee\models\position\PositionField
     * @return array
     */
    public static function getFieldMap(array $fields)
    {
        return ArrayHelper::map(
            $fields,
            'field_id',
            function ($array, $defaultValue) {
                return ['name' => $array['name'], 'type' => $array['type']];
            }
        );
    }

    /**
     * Getting field map ['field_id' => ['name' => $name, 'type' => $type]]
     * @param Position $position
     * @return array
     */
    public static function getFieldMapByPosition(Position $position)
    {
        if (is_array($position->fields)) {
            return self::getFieldMap($position->fields);
        }

        return [];
    }
}