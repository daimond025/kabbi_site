<?php

namespace common\modules\employee\components\document\interfaces;


interface DocumentFactoryInterface
{
    /**
     * Creating new document object
     * @param string $documentCode
     * @return mixed
     */
    public static function createDocument($documentCode);

    /**
     * Getting object from db
     * @param $documentCode
     * @param integer $entityDocumentId Primery key of main document table which includes all documents
     * @return mixed
     */
    public static function getDocument($documentCode, $entityDocumentId);
}