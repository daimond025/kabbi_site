<?php

namespace common\modules\employee\components\document;

use common\modules\employee\components\document\interfaces\DocumentFactoryInterface;
use common\modules\employee\models\documents\Document;
use common\modules\employee\models\documents\WorkerDriverLicense;
use common\modules\employee\models\documents\WorkerInn;
use common\modules\employee\models\documents\WorkerMedicalCertificate;
use common\modules\employee\models\documents\WorkerOgrnip;
use common\modules\employee\models\documents\WorkerOsago;
use common\modules\employee\models\documents\WorkerPassport;
use common\modules\employee\models\documents\WorkerPts;
use common\modules\employee\models\documents\WorkerSnils;
use yii\base\NotSupportedException;

class WorkerDocumentFactory implements DocumentFactoryInterface
{
    /**
     * @param $documentCode
     * @return mixed Document AR
     * @throws NotSupportedException
     */
    public static function createDocument($documentCode)
    {
        $className = self::getDocumentClassName($documentCode);

        return new $className(['document_code' => $documentCode]);
    }

    public static function getDocument($documentCode, $entityDocumentId)
    {
        $className = self::getDocumentClassName($documentCode);

        return $className::findOne(['worker_document_id' => $entityDocumentId]);
    }

    /**
     * @param $documentCode
     * @return string
     * @throws NotSupportedException
     */
    private static function getDocumentClassName($documentCode)
    {
        if ($documentCode == Document::PASSPORT) {
            $className = static::getPassport();
        } elseif ($documentCode == Document::INN) {
            $className = static::getInn();
        } elseif ($documentCode == Document::OGRNIP) {
            $className = static::getOgrnip();
        } elseif ($documentCode == Document::SNILS) {
            $className = static::getSnils();
        } elseif ($documentCode == Document::MEDICAL_CERT) {
            $className = static::getMedicalCertificate();
        } elseif ($documentCode == Document::OSAGO) {
            $className = static::getOsago();
        } elseif ($documentCode == Document::DRIVER_LICENSE) {
            $className = static::getDriverLicense();
        }elseif ($documentCode == Document::PTS) {
            $className = static::getPts();
        } else {
            throw new NotSupportedException('Unsupported document type');
        }

        return $className;
    }

    public static function getPassport()
    {
        return WorkerPassport::className();
    }

    public static function getInn()
    {
        return WorkerInn::className();
    }

    public static function getOgrnip()
    {
        return WorkerOgrnip::className();
    }

    public static function getSnils()
    {
        return WorkerSnils::className();
    }

    public static function getMedicalCertificate()
    {
        return WorkerMedicalCertificate::className();
    }

    public static function getOsago()
    {
        return WorkerOsago::className();
    }

    public static function getDriverLicense()
    {
        return WorkerDriverLicense::className();
    }

    public static function getPts()
    {
        return WorkerPts::className();
    }


}