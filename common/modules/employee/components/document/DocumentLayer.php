<?php

namespace common\modules\employee\components\document;

use common\helpers\CacheHelper;
use common\modules\employee\models\documents\Document;
use common\modules\employee\models\documents\WorkerDocumentBase;
use yii\base\Object;
use yii\helpers\ArrayHelper;

class DocumentLayer extends Object
{
    public static function getDocumentList()
    {
        return CacheHelper::getFromCache(Document::CACHE_KEY, function () {
            return Document::find()->indexBy('code')->asArray()->all();
        });
    }

    /**
     * @param integer $code
     * @return integer|null
     */
    public static function getDccumentIdByCode($code)
    {
        $documentList = self::getDocumentList();

        return getValue($documentList[$code]['document_id']);
    }

    /**
     * Getting document map with value as AR document object
     * @param array $codes List of document codes
     * @param array|null $data The data array to load, typically $_POST or $_GET.
     * @return array ['code' => 'AR document model']
     */
    public static function createDocumentMapByCodes(array $codes, $data = null)
    {
        $documentModels = [];

        foreach ($codes as $code) {
            $documentModels[$code] = static::createDocumentObjectsByCode($code);

            if (!empty($data)) {
                $documentModels[$code]->load($data);
            }
        }

        return $documentModels;
    }

    /**
     * Getting document map with value as AR document object
     * @param array $documentObjects List of common\modules\employee\models\document\Document
     * @return array ['code' => 'AR document model']
     */
    public static function createDocumentMapByObjects(array $documentObjects)
    {
        return ArrayHelper::map(
            $documentObjects,
            'code',
            function ($array, $defaultValue) {
                return static::createDocumentObjectsByCode($array['code']);
            }
        );
    }

    /**
     * @param array|string $code Document code
     * @return object|array
     */
    public static function createDocumentObjectsByCode($code)
    {
        if (is_array($code)) {
            $arObjects = [];
            foreach ($code as $item) {
                $arObjects[$item] = WorkerDocumentFactory::createDocument($item);
            }

            return $arObjects;
        }

        return WorkerDocumentFactory::createDocument($code);
    }

    public static function getDocumentObject($code, $worker_document_id)
    {
        return WorkerDocumentFactory::getDocument($code, $worker_document_id);
    }

    public static function saveData(array $documents, $worker_id, $workerHasPositionId = null, $runValidation = false)
    {
        $isSaved = true;

        /** @var $document WorkerDocumentBase */
        foreach ($documents as $document) {
            if (!empty($workerHasPositionId)) {
                $document->has_position_id = $workerHasPositionId;
            }

            $document->worker_id = $worker_id;
            $isSaved = $document->save($runValidation) && $isSaved;
        }

        return $isSaved;
    }

    /**
     * @param $codeMap ['worker_document_id' => 'code']
     * @param array|null $data The data array to load, typically $_POST or $_GET.
     * @return array
     */
    public static function getDocumentMapByCodeMap($codeMap, $data = null)
    {
        $documentModels = [];

        foreach ($codeMap as $worker_document_id => $code) {
            $documentModels[$code] = static::getDocumentObject($code, $worker_document_id);

            if (!empty($data)) {
                $documentModels[$code]->load($data);
            }
        }

        return $documentModels;
    }

    /**
     * Getting document code list by position id
     * @param int $position_id
     * @return array
     */
    public static function getDocumentCodeListByPosition($position_id)
    {
        $documents = Document::find()
            ->joinWith([
                'positionHasDocuments' => function ($query) use ($position_id) {
                    $query->where(['position_id' => $position_id]);
                },
            ], false)
            ->all();

        return ArrayHelper::getColumn($documents, 'code');
    }

    /**
     * Filtering documents form old unlinked documents
     * @param array $documents
     * @param array $oldDocumentCodes
     * @return array Documents after filtering
     */
    public static function cleanOldDocuments(array $documents, array $oldDocumentCodes)
    {
        foreach ($documents as $key => $document) {
            if (in_array($document['document']['code'], $oldDocumentCodes)) {
                unset($documents[$key]);
            }
        }

        return $documents;
    }

}