<?php

namespace common\modules\employee\components\worker;

use frontend\modules\car\models\Car;
use frontend\modules\car\models\CarClass;
use yii\base\Object;

class WorkerHasCarLayer extends Object
{
    public $modelClass;

    public function link($entity_id, $car_id)
    {
        $modelClass = $this->modelClass;
        return (new $modelClass(['has_position_id' => $entity_id, 'car_id' => $car_id]))->save();
    }

    public function unlink($entity_id, $car_id)
    {
        $worker_posotion =  $this->getEntity($entity_id, $car_id); //->delete();
        if($worker_posotion){
            return $worker_posotion->delete();
        }
        return 0;
    }

    public function getEntity($entity_id, $car_id)
    {
        $modelClass = $this->modelClass;
        return $modelClass::findOne(['has_position_id' => $entity_id, 'car_id' => $car_id]);
    }

    public function getCarsByEntityId($entity_id)
    {
        return Car::find()
            ->joinWith([
                'workerHasCars' => function ($query) use ($entity_id) {
                    $query->where(['has_position_id' => $entity_id]);
                }
            ], false)
            ->where([
                'tenant_id' => user()->tenant_id
            ])
            ->orderBy(['name' => SORT_ASC])
            ->all();
    }

    public function getCarClasses()
    {
        return CarClass::getClasses(true);
    }
}