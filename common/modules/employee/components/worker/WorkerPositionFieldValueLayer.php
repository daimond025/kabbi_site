<?php

namespace common\modules\employee\components\worker;


use common\modules\employee\models\worker\WorkerPositionFieldValue;
use yii\base\Object;

class WorkerPositionFieldValueLayer extends Object
{
    public static function createWorkerFieldValueModels($count = 1)
    {
        $fieldModels = [];

        for ($i = 0; $i < $count; $i++) {
            $fieldModels[] = new WorkerPositionFieldValue();
        }

        return $fieldModels;
    }

    public static function saveData(array $fieldModels, $runValidation = false)
    {
        $isSaved = true;

        /** @var $fieldModel WorkerPositionFieldValue */
        foreach ($fieldModels as $fieldModel) {
            $isSaved = $fieldModel->save($runValidation) && $isSaved;
        }

        return $isSaved;
    }
}