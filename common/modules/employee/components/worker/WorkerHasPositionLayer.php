<?php

namespace common\modules\employee\components\worker;


use yii\base\Object;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

class WorkerHasPositionLayer extends Object
{
    /**
     * @var ActiveRecord
     */
    public $modelClass;
    public $modelCacheKey;

    /**
     * @param array|string $where Condition
     *
     * @return bool|string
     */
    public function getWorkerHasPositionId($where)
    {
        $modelClass = $this->modelClass;

        return $modelClass::find()
            ->where($where)
            ->select(['id'])
            ->scalar();
    }

    /**
     * @param $worker_id
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getWorkerPositionsData($worker_id)
    {
        $modelClass = $this->modelClass;

        return $modelClass::find()
            ->alias('whp')
            ->where(['whp.worker_id' => $worker_id])
            ->with([
                'position'  => function (ActiveQuery $query) {
                    $query->select(['position_id', 'name', 'has_car', 'module_id']);
                },
                'documents' => function (ActiveQuery $query) {
                    $query->joinWith([
                        'document d' => function (ActiveQuery $sub_query) {
                            $sub_query->select(['d.document_id', 'd.code']);
                        },
                    ]);
                    $query->with('documentScans');
                },
                'cars',
                'cars.groupHasDriverTariff',
                'cars.groupCanViewClass',
                'workerPositionFieldValues',
            ])
            ->joinWith([
                'worker w' => function (ActiveQuery $query) {
                    $query->where(['tenant_id' => user()->tenant_id])->select(['w.worker_id']);
                },
                'worker.workerHasCities',
            ])
            ->all();
    }

    /**
     * @param $worker_id
     * @param $position_id
     *
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getWorkerPositionData($worker_id, $position_id)
    {
        $modelClass = $this->modelClass;

        return $modelClass::find()
            ->where([
                $modelClass::tableName() . '.worker_id' => $worker_id,
                'position_id'                           => $position_id,
            ])
            ->with([
                'position'  => function (ActiveQuery $query) {
                    $query->select(['position_id', 'name', 'has_car']);
                },
                'documents' => function (ActiveQuery $query) {
                    $query->joinWith([
                        'document d' => function (ActiveQuery $sub_query) {
                            $sub_query->select(['d.document_id', 'd.code']);
                        },
                    ]);
                },
                'workerPositionFieldValues',
            ])
            ->joinWith([
                'worker' => function (ActiveQuery $query) {
                    $query->where(['tenant_id' => user()->tenant_id]);
                },
            ], false)
            ->one();
    }

    /**
     * @param integer $worker_id
     * @param integer $position_id
     * @param array $values Attribute values (name => value) to be assigned to the model.
     *
     * @return bool
     */
    public function changeData($worker_id, $position_id, $values)
    {
        $workerHasPosition = $this->getOneEntity(compact('worker_id', 'position_id'));

        if (!empty($workerHasPosition)) {
            $workerHasPosition->setAttributes($values);

            return $workerHasPosition->save(true, array_keys($values));
        }

        return false;
    }

    /**
     * @param array|string $where Condition
     *
     * @return null|ActiveRecord ActiveRecord instance matching the condition, or `null` if nothing matches.
     */
    public function getOneEntity($where)
    {
        $modelClass = $this->modelClass;

        return $modelClass::findOne($where);
    }

    public function getWorkerCitiesById($id)
    {
        $modelClass = $this->modelClass;

        return $modelClass::find()
            ->alias('t')
            ->where(['t.id' => $id])
            ->joinWith(['worker.workerHasCities w'])
            ->select('w.city_id')
            ->column();
    }

    /**
     * @param int $workerId
     * @param int|array $positionIds
     * @return bool
     */
    public function hasPositions($workerId, $positionIds)
    {
        $modelClass = $this->modelClass;

        return $modelClass::find()
            ->where(['worker_id' => $workerId, 'position_id' => $positionIds])
            ->exists();
    }
}