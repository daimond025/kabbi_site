<?php


namespace common\modules\employee\components;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\Object;

class ServiceBase extends Object
{
    public $modelClass;
    public $repositoryConfig;
    protected $_repository;

    public function init()
    {
        parent::init();

        $this->configValidate();
        $this->setRepository($this->repositoryConfig);
    }

    public function getRepository()
    {
        return $this->_repository;
    }

    public function setRepository(array $repositoryConfig)
    {
        $this->_repository = Yii::createObject($repositoryConfig);
    }

    protected function configValidate()
    {
        if (empty($this->modelClass)) {
            throw new InvalidConfigException('Field "modelClass" must be set');
        }

        if (empty($this->repositoryConfig)) {
            throw new InvalidConfigException('Field "repositoryConfig" must be set');
        }
    }
}