<?php


namespace common\modules\employee\components\groups;

use common\modules\employee\components\ServiceBase;
use yii\helpers\ArrayHelper;

class GroupsLayer extends ServiceBase
{
    public function getMapIndexByPosition(array $groupList)
    {
        return ArrayHelper::index($groupList, null, 'position_id');
    }

    public function getMapGroup(array $groupList, $group = null)
    {
        return ArrayHelper::map($groupList, 'group_id', 'name', $group);
    }
}