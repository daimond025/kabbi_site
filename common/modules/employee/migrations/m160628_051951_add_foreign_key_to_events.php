<?php

use yii\db\Migration;

/**
 * Handles adding foreign_key to table `events`.
 */
class m160628_051951_add_foreign_key_to_events extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addForeignKey('fk_default_client_push_notifications_position_id', '{{%default_client_push_notifications}}', 'position_id',
            '{{%position}}', 'position_id');
        $this->addForeignKey('fk_client_push_notifications_position_id', '{{%client_push_notifications}}', 'position_id',
            '{{%position}}', 'position_id');
        $this->addForeignKey('fk_default_sms_template_position_id', '{{%default_sms_template}}', 'position_id',
            '{{%position}}', 'position_id');
        $this->addForeignKey('fk_sms_template_position_id', '{{%sms_template}}', 'position_id',
            '{{%position}}', 'position_id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        echo "m160628_051951_add_foreign_key_to_events cannot be reverted.\n";

        return false;
    }
}
