<?php

use yii\db\Migration;

class m160525_121321_link_car_models_to_type extends Migration
{
    const MODEL_TYPE = 1;

    public function up()
    {
        $this->update('{{%car_model}}', ['type_id' => self::MODEL_TYPE]);
    }

    public function down()
    {
        echo "m160525_121321_link_car_models_to_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
