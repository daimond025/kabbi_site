<?php

use yii\db\Migration;

class m160627_115001_tbl_client_bonus__column__class_id__remove_not_null extends Migration
{
    const TABLE_CLIENT_BONUS = '{{%client_bonus}}';
    const COLUMN_CLASS_ID = 'class_id';

    public function up()
    {
        $this->alterColumn(self::TABLE_CLIENT_BONUS, self::COLUMN_CLASS_ID, 'TINYINT(3) UNSIGNED');
    }

    public function down()
    {
        $this->alterColumn(self::TABLE_CLIENT_BONUS, self::COLUMN_CLASS_ID, 'TINYINT(3) UNSIGNED NOT NULL');
    }

}
