<?php

use yii\db\Migration;

class m160711_043505_tbl_client extends Migration
{
    public function up()
    {
        $this->renameColumn('{{%client}}', 'fail_driver_order', 'fail_worker_order');
    }

    public function down()
    {
        echo "m160711_043505_tbl_client cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
