-- MySQL Workbench Synchronization
-- Generated: 2016-06-24 11:54
-- Model: Workers
-- Version: 1.0
-- Project: Gootax

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


CREATE TABLE IF NOT EXISTS `tbl_position` (
  `position_id` INT(10) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `parent_id` INT(10) NULL DEFAULT NULL,
  `module_id` INT(11) NOT NULL,
  `code` VARCHAR(45) NOT NULL,
  `has_car` TINYINT(1) NULL DEFAULT 0,
  PRIMARY KEY (`position_id`),
  INDEX `UK_tbl_position_module_id` (`module_id` ASC),
  INDEX `FK_tbl_position_tbl_position_idx` (`parent_id` ASC),
  CONSTRAINT `FK_tbl_position_tbl_module_id`
    FOREIGN KEY (`module_id`)
    REFERENCES `tbl_module` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_tbl_position_tbl_position`
    FOREIGN KEY (`parent_id`)
    REFERENCES `tbl_position` (`position_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tbl_position_field` (
  `field_id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL,
  `type` ENUM('int', 'string', 'text') NOT NULL DEFAULT 'string',
  PRIMARY KEY (`field_id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tbl_worker` (
  `worker_id` INT(10) NOT NULL AUTO_INCREMENT,
  `tenant_id` INT(10) UNSIGNED NOT NULL,
  `city_id` INT(11) UNSIGNED NOT NULL,
  `callsign` MEDIUMINT(8) UNSIGNED NOT NULL,
  `password` VARCHAR(255) NULL DEFAULT NULL,
  `last_name` VARCHAR(45) NULL DEFAULT NULL,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  `second_name` VARCHAR(45) NULL DEFAULT NULL,
  `phone` VARCHAR(15) NOT NULL,
  `photo` VARCHAR(255) NULL DEFAULT NULL,
  `device` ENUM('IOS','ANDROID','WINFON') NULL DEFAULT NULL,
  `device_token` TEXT NULL DEFAULT NULL,
  `lang` VARCHAR(10) NULL DEFAULT NULL,
  `description` VARCHAR(255) NULL DEFAULT NULL,
  `device_info` VARCHAR(255) NULL DEFAULT NULL,
  `email` VARCHAR(45) NULL DEFAULT NULL,
  `partnership` ENUM('PARTNER','STAFF') NULL DEFAULT 'PARTNER',
  `birthday` DATE NULL DEFAULT NULL,
  `block` TINYINT(1) NULL DEFAULT 0,
  `create_time` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`worker_id`),
  INDEX `fk_tbl_worker_tbl_tenant1_idx` (`tenant_id` ASC),
  INDEX `fk_tbl_worker_tbl_city1_idx` (`city_id` ASC),
  CONSTRAINT `fk_tbl_worker_tbl_city1`
    FOREIGN KEY (`city_id`)
    REFERENCES `tbl_city` (`city_id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_worker_tbl_tenant1`
    FOREIGN KEY (`tenant_id`)
    REFERENCES `tbl_tenant` (`tenant_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tbl_position_has_field` (
  `position_id` INT(10) NOT NULL,
  `field_id` INT(11) NOT NULL,
  PRIMARY KEY (`position_id`, `field_id`),
  INDEX `fk_tbl_position_has_tbl_position_field_tbl_position_field1_idx` (`field_id` ASC),
  INDEX `fk_tbl_position_has_tbl_position_field_tbl_position1_idx` (`position_id` ASC),
  CONSTRAINT `fk_tbl_position_has_tbl_position_field_tbl_position1`
    FOREIGN KEY (`position_id`)
    REFERENCES `tbl_position` (`position_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_position_has_tbl_position_field_tbl_position_field1`
    FOREIGN KEY (`field_id`)
    REFERENCES `tbl_position_field` (`field_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tbl_worker_has_car` (
  `has_position_id` INT(11) NOT NULL,
  `car_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`has_position_id`, `car_id`),
  INDEX `fk_tbl_worker_has_positon_has_tbl_car_tbl_car1_idx` (`car_id` ASC),
  INDEX `fk_tbl_worker_has_positon_has_tbl_car_tbl_worker_has_posito_idx` (`has_position_id` ASC),
  CONSTRAINT `fk_tbl_worker_has_positon_has_tbl_car_tbl_car1`
    FOREIGN KEY (`car_id`)
    REFERENCES `tbl_car` (`car_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tbl_worker_has_position` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `worker_id` INT(11) NOT NULL,
  `position_id` INT(11) NOT NULL,
  `active` TINYINT(1) NULL DEFAULT '1',
  `position_class_id` INT(11) NULL DEFAULT NULL,
  `group_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tbl_worker_has_positon_tbl_worker1_idx` (`worker_id` ASC),
  INDEX `fk_tbl_worker_has_positon_tbl_position1_idx` (`position_id` ASC),
  INDEX `fk_worker_has_position_position_class_id` (`position_class_id` ASC),
  INDEX `fk_worker_has_position_group_id` (`group_id` ASC),
  CONSTRAINT `fk_tbl_worker_has_positon_tbl_position1`
    FOREIGN KEY (`position_id`)
    REFERENCES `tbl_position` (`position_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_worker_has_positon_tbl_worker1`
    FOREIGN KEY (`worker_id`)
    REFERENCES `tbl_worker` (`worker_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_worker_has_position_group_id`
    FOREIGN KEY (`group_id`)
    REFERENCES `tbl_worker_group` (`group_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_worker_has_position_position_class_id`
    FOREIGN KEY (`position_class_id`)
    REFERENCES `tbl_position` (`position_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tbl_worker_position_field_value` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `has_position_id` INT(11) NOT NULL,
  `field_id` INT(11) NOT NULL,
  `value_int` INT(11) NULL DEFAULT NULL,
  `value_string` VARCHAR(255) NULL DEFAULT NULL,
  `value_text` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tbl_worker_has_positon_has_tbl_position_field_tbl_positi_idx` (`field_id` ASC),
  INDEX `fk_tbl_worker_has_positon_has_tbl_position_field_tbl_worker_idx` (`has_position_id` ASC),
  CONSTRAINT `fk_tbl_worker_has_positon_has_tbl_position_field_tbl_position1`
    FOREIGN KEY (`field_id`)
    REFERENCES `tbl_position_field` (`field_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tbl_document` (
  `document_id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `code` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`document_id`),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tbl_worker_has_document` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `worker_id` INT(10) NOT NULL,
  `document_id` INT(11) NOT NULL,
  `has_position_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tbl_worker_has_tbl_document_tbl_document1_idx` (`document_id` ASC),
  INDEX `fk_tbl_worker_has_tbl_document_tbl_worker1_idx` (`worker_id` ASC),
  INDEX `fk_tbl_worker_has_document_tbl_worker_has_position1_idx` (`has_position_id` ASC),
  CONSTRAINT `fk_tbl_worker_has_tbl_document_tbl_document1`
    FOREIGN KEY (`document_id`)
    REFERENCES `tbl_document` (`document_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tbl_worker_has_tbl_document_tbl_worker1`
    FOREIGN KEY (`worker_id`)
    REFERENCES `tbl_worker` (`worker_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tbl_worker_document_scan` (
  `scan_id` INT(11) NOT NULL AUTO_INCREMENT,
  `worker_document_id` INT(11) NOT NULL,
  `filename` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`scan_id`),
  INDEX `fk_tbl_worker_document_scan_tbl_worker_has_document1_idx` (`worker_document_id` ASC),
  CONSTRAINT `fk_tbl_worker_document_scan_tbl_worker_has_document1`
    FOREIGN KEY (`worker_document_id`)
    REFERENCES `tbl_worker_has_document` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tbl_worker_passport` (
  `passport_id` INT(11) NOT NULL AUTO_INCREMENT,
  `worker_document_id` INT(11) NOT NULL,
  `series` VARCHAR(5) NULL DEFAULT NULL,
  `number` VARCHAR(10) NULL DEFAULT NULL,
  `issued` TEXT NULL DEFAULT NULL,
  `registration` TEXT NULL DEFAULT NULL,
  `actual_address` TEXT NULL DEFAULT NULL,
  PRIMARY KEY (`passport_id`),
  INDEX `fk_tbl_worker_passport_tbl_worker_has_document1_idx` (`worker_document_id` ASC),
  CONSTRAINT `fk_tbl_worker_passport_tbl_worker_has_document1`
    FOREIGN KEY (`worker_document_id`)
    REFERENCES `tbl_worker_has_document` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tbl_worker_inn` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `worker_document_id` INT(11) NOT NULL,
  `number` INT(15) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tbl_worker_inn_tbl_worker_has_document1_idx` (`worker_document_id` ASC),
  CONSTRAINT `fk_tbl_worker_inn_tbl_worker_has_document1`
    FOREIGN KEY (`worker_document_id`)
    REFERENCES `tbl_worker_has_document` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tbl_worker_ogrnip` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `worker_document_id` INT(11) NOT NULL,
  `number` INT(15) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tbl_worker_ogrnip_tbl_worker_has_document1_idx` (`worker_document_id` ASC),
  CONSTRAINT `fk_tbl_worker_ogrnip_tbl_worker_has_document1`
    FOREIGN KEY (`worker_document_id`)
    REFERENCES `tbl_worker_has_document` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tbl_worker_snils` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `worker_document_id` INT(11) NOT NULL,
  `number` VARCHAR(15) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tbl_worker_inn_tbl_worker_has_document1_idx` (`worker_document_id` ASC),
  CONSTRAINT `fk_tbl_worker_inn_tbl_worker_has_document10`
    FOREIGN KEY (`worker_document_id`)
    REFERENCES `tbl_worker_has_document` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tbl_worker_driver_license` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `worker_document_id` INT(11) NOT NULL,
  `series` VARCHAR(20) NULL DEFAULT NULL,
  `number` VARCHAR(20) NULL DEFAULT NULL,
  `category` VARCHAR(15) NULL DEFAULT NULL,
  `start_date` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tbl_worker_inn_tbl_worker_has_document1_idx` (`worker_document_id` ASC),
  CONSTRAINT `fk_tbl_worker_inn_tbl_worker_has_document11`
    FOREIGN KEY (`worker_document_id`)
    REFERENCES `tbl_worker_has_document` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tbl_worker_medical_certificate` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `worker_document_id` INT(11) NOT NULL,
  `number` VARCHAR(15) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tbl_worker_ogrnip_tbl_worker_has_document1_idx` (`worker_document_id` ASC),
  CONSTRAINT `fk_tbl_worker_ogrnip_tbl_worker_has_document10`
    FOREIGN KEY (`worker_document_id`)
    REFERENCES `tbl_worker_has_document` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tbl_worker_osago` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `worker_document_id` INT(11) NOT NULL,
  `series` VARCHAR(255) NULL DEFAULT NULL,
  `number` VARCHAR(255) NULL DEFAULT NULL,
  `start_date` DATE NULL DEFAULT NULL,
  `end_date` DATE NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tbl_worker_inn_tbl_worker_has_document1_idx` (`worker_document_id` ASC),
  CONSTRAINT `fk_tbl_worker_inn_tbl_worker_has_document100`
    FOREIGN KEY (`worker_document_id`)
    REFERENCES `tbl_worker_has_document` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `tbl_position_has_document` (
  `position_id` INT(10) NOT NULL,
  `document_id` INT(11) NOT NULL,
  PRIMARY KEY (`position_id`, `document_id`),
  INDEX `fk_tbl_position_has_tbl_document_tbl_document1_idx` (`document_id` ASC),
  INDEX `fk_tbl_position_has_tbl_document_tbl_position1_idx` (`position_id` ASC),
  CONSTRAINT `fk_tbl_position_has_tbl_document_tbl_position1`
    FOREIGN KEY (`position_id`)
    REFERENCES `tbl_position` (`position_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbl_position_has_tbl_document_tbl_document1`
    FOREIGN KEY (`document_id`)
    REFERENCES `tbl_document` (`document_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
