<?php

use yii\db\Migration;

class m160709_050451_rename_column_tbl_order extends Migration
{
    public function up()
    {
        $this->renameColumn('{{%order}}', 'driver_id', 'worker_id');
    }

    public function down()
    {
        echo "m160709_050451_rename_column_tbl_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
