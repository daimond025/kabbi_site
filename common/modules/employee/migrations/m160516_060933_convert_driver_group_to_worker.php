<?php

use yii\db\Migration;
use yii\db\Query;
use yii\helpers\Console;

class m160516_060933_convert_driver_group_to_worker extends Migration
{
    public function up()
    {
        $this->renameTable('{{%driver_group}}', '{{%worker_group}}');
        $this->renameTable('{{%driver_group_has_tariff}}', '{{%worker_group_has_tariff}}');
        $this->renameTable('{{%driver_group_has_city}}', '{{%worker_group_has_city}}');
        $this->renameTable('{{%driver_group_can_view_client_tariff}}', '{{%worker_group_can_view_client_tariff}}');
        $this->renameTable('{{%car_driver_group_has_class}}', '{{%car_has_worker_group_class}}');
        $this->renameTable('{{%car_driver_group_has_tariff}}', '{{%car_has_worker_group_tariff}}');

        $this->addForeignKey('fk_car_has_worker_group_class_car_id', '{{%car_has_worker_group_class}}', 'car_id',
            '{{%car}}', 'car_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_car_has_worker_group_class_class_id', '{{%car_has_worker_group_class}}', 'class_id',
            '{{%car_class}}', 'class_id', 'CASCADE', 'CASCADE');

        try {
            $this->addForeignKey('fk_car_has_worker_group_tariff_car_id', '{{%car_has_worker_group_tariff}}', 'car_id',
                '{{%car}}', 'car_id', 'CASCADE', 'CASCADE');
        } catch (yii\db\IntegrityException $e) {
            Console::output('Ощибка добавления ключа "fk_car_has_worker_group_tariff_car_id" для car_id');
            Console::output('Поиск несуществующих car_id');
            $res = (new Query())
                ->select('g.car_id')
                ->from('{{%car_has_worker_group_tariff}} g')
                ->leftJoin('{{%car}} c', 'g.car_id = c.car_id')
                ->where(['c.car_id' => null])
                ->column();

            if ($res) {
                Console::output('Начинаем удаление несуществующих ключей');
                $count = Yii::$app->db->createCommand()->delete('{{%car_has_worker_group_tariff}}',
                    ['car_id' => $res])->execute();
                Console::output("Удалено $count записей");
                Console::output('Дабавляем ключ...');
                $this->addForeignKey('fk_car_has_worker_group_tariff_car_id', '{{%car_has_worker_group_tariff}}', 'car_id',
                    '{{%car}}', 'car_id', 'CASCADE', 'CASCADE');
            } else {
                Console::output('Ключ "fk_car_has_worker_group_tariff_car_id" не добавлен');
            }
        }

        $this->addForeignKey('fk_car_has_worker_group_tariff_tariff_id', '{{%car_has_worker_group_tariff}}', 'tariff_id',
            '{{%worker_tariff}}', 'tariff_id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m160516_060933_convert_driver_group_to_worker cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
