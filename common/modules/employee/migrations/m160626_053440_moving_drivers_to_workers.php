<?php

use common\modules\employee\models\documents\WorkerDocumentScan;
use common\modules\employee\models\documents\WorkerDriverLicense;
use common\modules\employee\models\documents\WorkerInn;
use common\modules\employee\models\documents\WorkerMedicalCertificate;
use common\modules\employee\models\documents\WorkerOgrnip;
use common\modules\employee\models\documents\WorkerOsago;
use common\modules\employee\models\documents\WorkerPassport;
use common\modules\employee\models\documents\WorkerSnils;
use common\modules\employee\models\worker\WorkerHasCar;
use common\modules\employee\models\worker\WorkerHasPosition;
use common\modules\employee\models\worker\WorkerShift;
use frontend\modules\driver\models\Driver;
use common\modules\employee\models\worker\Worker;
use yii\db\Migration;
use yii\helpers\Console;

class m160626_053440_moving_drivers_to_workers extends Migration
{
    const TAXI_DRIVER_POSITION_ID = 1;
    const PASSPORT = 1;
    const INN = 2;
    const OGRNIP = 3;
    const SNILS = 4;
    const MEDICAL_CERT = 5;
    const OSAGO = 6;
    const DRIVER_LICENSE = 7;

    /**
     * @return bool
     */
    public function safeUp()
    {
        $this->addColumn('{{%worker_has_position}}', 'rating', $this->float());
        $this->renameColumn('{{%worker_shift}}', 'driver_id', 'worker_id');

        /** @var Driver $driver */
        foreach (Driver::find()->with(['driverHasCars', 'info'])->groupBy('phone')->each(250) as $driver) {
            if (!is_object($driver->info)) {
                continue;
            }
            //Driver to Worker
            $worker = new Worker([
                'worker_id'   => $driver->driver_id,
                'tenant_id'   => $driver->tenant_id,
                'callsign'    => $driver->callsign,
                'email'       => $driver->info->email,
                'partnership' => $driver->info->partnership,
                'birthday'    => $driver->info->birthday,
                'block'       => (int)!$driver->active,
                'create_time' => strtotime($driver->info->create_time),
            ]);
            $worker->detachBehavior('callsign');
            $worker->setAttributes($driver->getAttributes());

            if (strlen($worker->password) < 6) {
                $worker->password = '123456';
            }

            $isWorkerSaved = $worker->save();

            if (!$isWorkerSaved) {
                $this->outputErrors($worker, 'Ошибка сохранения исполнителя driver_id: ' . $worker->worker_id);
                Console::output('Пропущено');
                continue;
            }

            //Position
            $workerHasPosition = new WorkerHasPosition([
                'worker_id'   => $worker->worker_id,
                'position_id' => self::TAXI_DRIVER_POSITION_ID,
                'rating'      => $driver->raiting,
            ]);

            if ($workerHasPosition->save()) {
                //Cars
                foreach ($driver->driverHasCars as $driverHasCar) {
                    $workerHasCar = new WorkerHasCar([
                        'has_position_id' => $workerHasPosition->id,
                        'car_id'          => $driverHasCar->car_id,
                    ]);

                    if (!$workerHasCar->save()) {
                        $this->outputErrors($workerHasCar,
                            'Ошибка привязки авто driver_id: ' . $worker->worker_id);

                        return false;
                    }
                }

                //General documents

                //passport
                $doccument = new WorkerPassport([
                    'worker_id'       => $worker->worker_id,
                    'document_code'   => self::PASSPORT,
                    'series'          => $driver->info->passport_serial,
                    'number'          => $driver->info->passport_number,
                    'issued'          => $driver->info->passport_issued,
                    'registration'    => $driver->info->address_registr,
                    'actual_address'  => $driver->info->address_fact,
                    'has_position_id' => $workerHasPosition->id,
                ]);

                $doccument->save();

                //scans
                foreach ($driver->info->passportScans as $passportScan) {
                    (new WorkerDocumentScan([
                        'worker_document_id' => $doccument->worker_document_id,
                        'filename'           => $passportScan->filename,
                    ]))
                        ->save();
                }
                //------------------------------------------------------------

                //snils
                $document = new WorkerSnils([
                    'worker_id'       => $worker->worker_id,
                    'document_code'   => self::SNILS,
                    'number'          => $driver->info->snils,
                    'has_position_id' => $workerHasPosition->id,
                ]);

                $isDocumentSaved = $document->save();

                //scan
                if (!empty($driver->info->snils_scan) && $isDocumentSaved) {
                    $this->saveDocumentScan($document->worker_document_id, $driver->info->snils_scan);
                }

                //------------------------------------------------------------

                //inn
                $document = new WorkerInn([
                    'worker_id'       => $worker->worker_id,
                    'document_code'   => self::INN,
                    'number'          => $driver->info->inn,
                    'has_position_id' => $workerHasPosition->id,
                ]);

                $isDocumentSaved = $document->save();

                //scan
                if (!empty($driver->info->inn_scan) && $isDocumentSaved) {
                    $this->saveDocumentScan($document->worker_document_id, $driver->info->inn_scan);
                }

                //------------------------------------------------------------

                //ogrnip
                $document = new WorkerOgrnip([
                    'worker_id'       => $worker->worker_id,
                    'document_code'   => self::OGRNIP,
                    'number'          => $driver->info->ogrnip,
                    'has_position_id' => $workerHasPosition->id,
                ]);

                $isDocumentSaved = $document->save();

                //scan
                if (!empty($driver->info->ogrn_scan) && $isDocumentSaved) {
                    $this->saveDocumentScan($document->worker_document_id, $driver->info->ogrn_scan);
                }

                //------------------------------------------------------------

                //Position documents

                //driver license
                $document = new WorkerDriverLicense([
                    'worker_id'       => $worker->worker_id,
                    'document_code'   => self::DRIVER_LICENSE,
                    'series'          => $driver->info->driver_license_serial,
                    'number'          => $driver->info->driver_license_number,
                    'category'        => $driver->info->license_category,
                    'start_date'      => $driver->info->driver_license_start_date,
                    'has_position_id' => $workerHasPosition->id,
                ]);

                $isDocumentSaved = $document->save();

                //scan
                if (!empty($driver->info->driver_license_scan) && $isDocumentSaved) {
                    $this->saveDocumentScan($document->worker_document_id, $driver->info->driver_license_scan);
                }

                //------------------------------------------------------------

                //osago
                $document = new WorkerOsago([
                    'worker_id'       => $worker->worker_id,
                    'document_code'   => self::OSAGO,
                    'series'          => $driver->info->osago_series,
                    'number'          => $driver->info->osago_number,
                    'start_date'      => $driver->info->osago_start_date,
                    'end_date'        => $driver->info->osago_end_date,
                    'has_position_id' => $workerHasPosition->id,
                ]);

                $isDocumentSaved = $document->save();

                //scan
                if (!empty($driver->info->osago_scan) && $isDocumentSaved) {
                    $this->saveDocumentScan($document->worker_document_id, $driver->info->osago_scan);
                }

                //------------------------------------------------------------
                //medical
                $document = new WorkerMedicalCertificate([
                    'worker_id'       => $worker->worker_id,
                    'document_code'   => self::MEDICAL_CERT,
                    'number'          => $driver->info->med,
                    'has_position_id' => $workerHasPosition->id,
                ]);

                $isDocumentSaved = $document->save();

                //scan
                if (!empty($driver->info->med_scan) && $isDocumentSaved) {
                    $this->saveDocumentScan($document->worker_document_id, $driver->info->med_scan);
                }

                //------------------------------------------------------------
            } else {
                $this->outputErrors($worker,
                    'Ошибка сохранения должности исполнителя driver_id: ' . $worker->worker_id);

                return false;
            }
        }
    }

    public function safeDown()
    {
        echo "m160626_053440_moving_drivers_to_workers cannot be reverted.\n";

        return false;
    }

    private function outputErrors($model, $message = '')
    {
        if (!empty($message)) {
            Console::output($message);
        }

        Console::output(implode('; ', $model->getFirstErrors()));
    }

    /**
     * @param int $workerDocumentId
     * @param string $filename
     * @return bool
     */
    private function saveDocumentScan($workerDocumentId, $filename)
    {
        return (new WorkerDocumentScan([
            'worker_document_id' => $workerDocumentId,
            'filename'           => $filename,
        ]))
            ->save();
    }
}
