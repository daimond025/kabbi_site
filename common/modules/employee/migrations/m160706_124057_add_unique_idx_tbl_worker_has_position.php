<?php

use yii\db\Migration;

class m160706_124057_add_unique_idx_tbl_worker_has_position extends Migration
{
    public function up()
    {
        $this->createIndex('idx_tbl_worker_has_position_worker_id_position_id', '{{%worker_has_position}}',
            ['worker_id', 'position_id'], true);
    }

    public function down()
    {
        echo "m160706_124057_add_unique_idx_tbl_worker_has_position cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
