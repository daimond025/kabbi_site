<?php

use yii\db\Migration;

class m160513_141236_tbl_taxi_tariff extends Migration
{
    const TABLE_NAME = '{{%taxi_tariff}}';
    const DRIVER_POSITION = 'taxi_driver';

    public function up()
    {
        $this->alterColumn(self::TABLE_NAME, 'class_id', 'tinyint(3) unsigned');

        try {
            $this->dropForeignKey('fk_taxi_tariff_has_position_id', self::TABLE_NAME);
        } catch (yii\db\Exception $e) {

        }
    }

    public function down()
    {
        echo "m160513_141236_tbl_taxi_tariff cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
