<?php

use frontend\modules\tenant\models\GeoServiceProvider;
use yii\db\Migration;

class m160815_103959_geo_service_provider extends Migration
{
    public function up()
    {
        GeoServiceProvider::updateAll(['service_provider_id' => 2], ['service_type' => 'routing_service', 'service_provider_id' => 1]);
    }

    public function down()
    {
        echo "m160815_103959_geo_service_provider cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
