<?php

use yii\db\Migration;

class m160804_090354_tbl_call extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%call}}', 'userfield', $this->string(200));
    }

    public function down()
    {
        echo "m160804_090354_tbl_call cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
