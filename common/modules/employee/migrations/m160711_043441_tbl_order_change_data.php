<?php

use yii\db\Migration;

class m160711_043441_tbl_order_change_data extends Migration
{
    const TABLE_NAME = '{{%order_change_data}}';

    public function up()
    {
        $this->update(self::TABLE_NAME, ['change_object_type' => 'worker'], ['change_object_type' => 'driver']);
        $this->update(self::TABLE_NAME, ['change_subject' => 'worker'], ['change_subject' => 'driver']);
    }

    public function down()
    {
        echo "m160711_043441_tbl_order_change_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
