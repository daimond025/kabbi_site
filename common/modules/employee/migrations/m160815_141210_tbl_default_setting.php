<?php

use yii\db\Migration;

class m160815_141210_tbl_default_setting extends Migration
{
    public function up()
    {
        $this->update('{{%default_settings}}', ['value' => 0], ['name' => 'WORKER_MIN_BAlANCE']);
    }

    public function down()
    {
        echo "m160815_141210_tbl_default_setting cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
