<?php

use yii\db\Migration;

class m160525_054003_tbl_driver_review_raiting extends Migration
{
    const TABLE_NAME = '{{%worker_review_rating}}';

    public function up()
    {
        $this->renameTable('{{%driver_review_raiting}}', self::TABLE_NAME);
        $this->addColumn(self::TABLE_NAME, 'position_id', $this->integer()->comment('PK of tbl_position'));
    }

    public function down()
    {
        echo "m160525_054003_tbl_driver_review_raiting cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
