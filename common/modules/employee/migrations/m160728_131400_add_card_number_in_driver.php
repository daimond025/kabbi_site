<?php

use yii\db\Migration;

class m160728_131400_add_card_number_in_driver extends Migration
{
    const TABLE_NAME = '{{%worker}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'card_number', $this->string(45));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'card_number');
    }

}
