<?php

use yii\db\Migration;

class m160705_073653_rename_settings extends Migration
{
    const TABLE_NAME = '{{%default_settings}}';

    public function up()
    {
        $this->execute("UPDATE" . self::TABLE_NAME ." SET name = REPLACE(name, 'DRIVER', 'WORKER')");
    }

    public function down()
    {
        echo "m160705_073653_rename_settings cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
