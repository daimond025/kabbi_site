<?php

use yii\db\Migration;

class m160726_113723_tbl_worker_block__add_columns extends Migration
{
    const TABLE_WORKER_BLOCK = '{{%worker_block}}';
    const COLUMN_PID = 'pid';
    const COLUMN_SERVER_ID = 'server_id';

    public function up()
    {
        $this->addColumn(self::TABLE_WORKER_BLOCK, self::COLUMN_PID, $this->integer());
        $this->addColumn(self::TABLE_WORKER_BLOCK, self::COLUMN_SERVER_ID, $this->integer());
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_WORKER_BLOCK, self::COLUMN_PID);
        $this->dropColumn(self::TABLE_WORKER_BLOCK, self::COLUMN_SERVER_ID);
    }

}
