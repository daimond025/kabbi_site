<?php

use yii\db\Migration;


class m160628_040353_add_position_id_to_event_tbls extends Migration
{
    const POSITION_ID = 1;
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%default_client_push_notifications}}', 'position_id', $this->integer()->notNull());
        $this->update('{{%default_client_push_notifications}}', ['position_id' => self::POSITION_ID]);
        $this->addColumn('{{%client_push_notifications}}', 'position_id', $this->integer()->notNull());
        $this->update('{{%client_push_notifications}}', ['position_id' => self::POSITION_ID]);

        $this->addColumn('{{%default_sms_template}}', 'position_id', $this->integer()->notNull());
        $this->update('{{%default_sms_template}}', ['position_id' => self::POSITION_ID]);
        $this->addColumn('{{%sms_template}}', 'position_id', $this->integer()->notNull());
        $this->update('{{%sms_template}}', ['position_id' => self::POSITION_ID]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
