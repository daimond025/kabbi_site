<?php

use yii\db\Migration;

class m160815_102209_close_worker_shifts extends Migration
{
    public function up()
    {
        $this->update('{{%worker_shift}}', ['end_work' => time()], ['end_work' => null]);
    }

    public function down()
    {
        echo "m160815_102209_close_worker_shifts cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
