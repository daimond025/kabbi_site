<?php

use yii\db\Migration;

class m160815_103031_worker_unblock extends Migration
{
    public function up()
    {
        $this->update('{{%worker_block}}', ['is_unblocked' => 1], 'is_unblocked <> 1');
    }

    public function down()
    {
        echo "m160815_103031_worker_unblock cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
