<?php

use yii\db\Migration;

class m160626_104212_fill_position_id extends Migration
{
    const TAXI_POSITION_ID = 1;

    public function up()
    {
        $this->update('{{%worker_tariff}}', ['position_id' => self::TAXI_POSITION_ID]);
        $this->alterColumn('{{%worker_tariff}}', 'position_id',
            $this->integer(10)->notNull()->comment('PK of table "tbl_position"'));
        $this->addForeignKey('fk_worker_tariff_has_position', '{{%worker_tariff}}', 'position_id',
            '{{%position}}', 'position_id');

        $this->update('{{%taxi_tariff}}', ['position_id' => self::TAXI_POSITION_ID]);
        $this->alterColumn('{{%taxi_tariff}}', 'position_id',
            $this->integer(10)->notNull()->comment('PK of table "tbl_position"'));
        $this->addForeignKey('fk_taxi_tariff_has_position_id', '{{%taxi_tariff}}', 'position_id',
            '{{%position}}', 'position_id');

        $this->update('{{%worker_group}}', ['position_id' => self::TAXI_POSITION_ID]);
        $this->alterColumn('{{%worker_group}}', 'position_id',
            $this->integer(10)->notNull()->comment('PK of table "tbl_position"'));
        $this->addForeignKey('fk_worker_group_has_position', '{{%worker_group}}', 'position_id',
            '{{%position}}', 'position_id');

        $this->update('{{%order}}', ['position_id' => self::TAXI_POSITION_ID]);
        $this->alterColumn('{{%order}}', 'position_id',
            $this->integer(10)->notNull()->comment('PK of table "tbl_position"'));

        $this->update('{{%worker_review_rating}}', ['position_id' => self::TAXI_POSITION_ID]);
        $this->alterColumn('{{%worker_review_rating}}', 'position_id',
            $this->integer(10)->notNull()->comment('PK of table "tbl_position"'));
        $this->addForeignKey('fk_worker_review_rating_position', '{{%worker_review_rating}}', 'position_id',
            '{{%position}}', 'position_id');

        $this->update('{{%worker_shift}}', ['position_id' => self::TAXI_POSITION_ID]);
        $this->alterColumn('{{%worker_shift}}', 'position_id',
            $this->integer(10)->notNull()->comment('PK of table "tbl_position"'));
        $this->addForeignKey('fk_worker_shift_has_position', '{{%worker_shift}}', 'position_id',
            '{{%position}}', 'position_id');

        $this->update('{{%client_bonus}}', ['position_id' => self::TAXI_POSITION_ID]);
    }

    public function down()
    {
        echo "m160626_104212_fill_position_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
