<?php

use yii\db\Migration;

class m160510_050922_tbl_position_add_data extends Migration
{
    public function up()
    {
        $arData = [
            ['Taxi driver', 1, 'taxi_driver', 1],
        ];
        $this->batchInsert('{{%position}}', ['name', 'module_id', 'code', 'has_car'], $arData);
    }

    public function down()
    {
        echo "m160510_050922_tbl_position_add_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
