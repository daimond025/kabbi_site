<?php

use yii\db\Migration;

class m160510_050928_tbl_worker_shift extends Migration
{
    const TABLE_NAME = '{{%driver_shift}}';
    const NEW_TABLE_NAME = '{{%worker_shift}}';
    const FK_NAME = 'fk_worker_shift_has_position';
    const REF_TABLE = '{{%position}}';
    const NEW_COLUMN_NAME = 'position_id';

    public function up()
    {
        $this->alterColumn(self::TABLE_NAME, 'driver_id', $this->integer());
        $this->renameTable(self::TABLE_NAME, self::NEW_TABLE_NAME);
        $this->addColumn(self::NEW_TABLE_NAME, self::NEW_COLUMN_NAME,
            $this->integer()->comment('PK of table "tbl_position"'));
    }

    public function down()
    {
        $this->dropColumn(self::NEW_TABLE_NAME, self::NEW_COLUMN_NAME);
        $this->renameTable(self::NEW_TABLE_NAME, self::TABLE_NAME);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
