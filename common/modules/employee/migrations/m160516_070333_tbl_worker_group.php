<?php

use yii\db\Migration;

class m160516_070333_tbl_worker_group extends Migration
{
    const TABLE_NAME = '{{%worker_group}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'position_id',
            $this->integer(10)->comment('PK of table "tbl_position"'));

        $this->addColumn(self::TABLE_NAME, 'position_class_id',
            $this->integer(10)->comment('PK of table "tbl_position"'));
        $this->addForeignKey('fk_worker_group_has_position_class', self::TABLE_NAME, 'position_class_id',
            '{{%position}}', 'position_id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_worker_group_has_position', self::TABLE_NAME);
        $this->dropForeignKey('fk_worker_group_has_position_class', self::TABLE_NAME);
        $this->dropColumn(self::TABLE_NAME, 'position_id');
        $this->dropColumn(self::TABLE_NAME, 'position_class_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
