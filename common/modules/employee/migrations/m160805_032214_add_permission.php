<?php

use yii\db\Migration;

class m160805_032214_add_permission extends Migration
{
    public function up()
    {
        $auth = Yii::$app->authManager;

        $cities = $auth->createPermission('cities');
        $cities->description = 'Cities';
        $auth->add($cities);

        $readCities = $auth->createPermission('read_cities');
        $readCities->description = 'Cities (read)';
        $auth->add($readCities);

        $auth->addChild($cities, $readCities);

        $bonuses = $auth->createPermission('bonus');
        $bonuses->description = 'Bonus';
        $auth->add($bonuses);

        $readBonuses = $auth->createPermission('read_bonus');
        $readBonuses->description = 'Bonus (read)';
        $auth->add($readBonuses);

        $auth->addChild($bonuses, $readBonuses);

        $bankCard = $auth->createPermission('bankCard');
        $bankCard->description = 'Bank card';
        $auth->add($bankCard);

        $readBankCard = $auth->createPermission('read_bankCard');
        $readBankCard->description = 'Bank card (read)';
        $auth->add($readBankCard);

        $auth->addChild($bankCard, $readBankCard);

        $gootaxTariff = $auth->createPermission('gootaxTariff');
        $gootaxTariff->description = 'Gootax tariff';
        $auth->add($gootaxTariff);

        $readGootaxTariff = $auth->createPermission('read_gootaxTariff');
        $readGootaxTariff->description = 'Gootax tariff (read)';
        $auth->add($readGootaxTariff);

        $auth->addChild($gootaxTariff, $readGootaxTariff);
    }

    public function down()
    {
        echo "m160805_032214_add_permission cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
