<?php

use yii\db\Migration;

class m160510_115936_worker_tariff_changes extends Migration
{
    const DRIVER_POSITION = 'taxi_driver';

    public function up()
    {
        $this->renameTable('{{%driver_option_discount}}', '{{%worker_option_discount}}');
        $this->renameTable('{{%driver_option_tariff}}', '{{%worker_option_tariff}}');
        $this->renameTable('{{%driver_tariff}}', '{{%worker_tariff}}');
        $this->renameTable('{{%driver_tariff_has_city}}', '{{%worker_tariff_has_city}}');
        $this->addColumn('{{%worker_tariff}}', 'position_id',
            $this->integer(10)->comment('PK of table "tbl_position"'));
        $this->alterColumn('{{%worker_tariff}}', 'class_id', 'tinyint(3) unsigned');
        $this->addForeignKey('fk_worker_tariff_car_class', '{{%worker_tariff}}', 'class_id',
            '{{%car_class}}', 'class_id');
    }

    public function down()
    {
        echo "m160510_115936_worker_tariff_changes cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
