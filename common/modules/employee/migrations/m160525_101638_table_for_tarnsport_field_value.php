<?php

use yii\db\Migration;

class m160525_101638_table_for_tarnsport_field_value extends Migration
{
    const TABLE_NAME = '{{%transport_type_field_value}}';

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable(self::TABLE_NAME, [
            'id'           => $this->primaryKey(),
            'has_field_id' => $this->integer()->notNull(),
            'car_id'       => $this->integer(10)->unsigned(),
            'value_int'    => $this->integer(),
            'value_string' => $this->string(),
            'value_text'   => $this->text(),
        ], $tableOptions);

        $this->addForeignKey('fk_transport_type_field_value_field_id', self::TABLE_NAME, 'has_field_id',
            '{{%transport_type_has_field}}', 'id',
            'cascade', 'cascade');

        $this->addForeignKey('fk_transport_type_field_value_car_id', self::TABLE_NAME, 'car_id',
            '{{%car}}', 'car_id',
            'cascade', 'cascade');
    }

    public function down()
    {
        $this->dropForeignKey('fk_transport_type_field_value_field_id', self::TABLE_NAME);
        $this->dropTable(self::TABLE_NAME);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
