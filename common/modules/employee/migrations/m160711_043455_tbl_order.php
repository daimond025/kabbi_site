<?php

use yii\db\Migration;

class m160711_043455_tbl_order extends Migration
{
    const TABLE_NAME = '{{%order}}';

    public function up()
    {
        $orders = (new \yii\db\Query())
            ->select('order_id')
            ->from(self::TABLE_NAME)
            ->where(['device' => 'DRIVER'])
            ->column();

        $this->alterColumn(self::TABLE_NAME, 'device',
            'ENUM("DISPATCHER", "IOS", "WORKER", "WINFON", "WEB", "ANDROID") COMMENT \'Устройство с которого сделан заказ\'');
        $this->update(self::TABLE_NAME, ['device' => 'WORKER'], ['order_id' => $orders]);
    }

    public function down()
    {
        echo "m160711_043455_tbl_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
