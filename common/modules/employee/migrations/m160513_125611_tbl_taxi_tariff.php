<?php

use yii\db\Migration;

class m160513_125611_tbl_taxi_tariff extends Migration
{
    const TABLE_NAME = '{{%taxi_tariff}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'position_id',
            $this->integer(10)->comment('PK of table "tbl_position"'));
        $this->addColumn(self::TABLE_NAME, 'position_class_id',
            $this->integer(10)->comment('PK of table "tbl_position"'));
        $this->addForeignKey('fk_taxi_tariff_has_position_class', self::TABLE_NAME, 'position_class_id',
            '{{%position}}', 'position_id');
    }

    public function down()
    {
        try {
            $this->dropForeignKey('fk_taxi_tariff_has_position_class', self::TABLE_NAME);
        } catch (yii\db\Exception $e) {

        }

        $this->dropColumn(self::TABLE_NAME, 'position_id');
        $this->dropColumn(self::TABLE_NAME, 'position_class_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
