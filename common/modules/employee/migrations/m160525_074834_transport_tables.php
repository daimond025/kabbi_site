<?php

use yii\db\Migration;

class m160525_074834_transport_tables extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%transport_type}}', [
            'type_id' => $this->primaryKey(),
            'name'    => $this->string(),
        ], $tableOptions);

        $this->addColumn('{{%car_model}}', 'type_id', $this->integer());
        $this->addForeignKey('fk_cat_model_type_id', '{{%car_model}}', 'type_id', '{{%transport_type}}', 'type_id',
            'SET NULL', 'cascade');

        $this->createTable('{{%transport_field}}', [
            'field_id' => $this->primaryKey(),
            'name'     => $this->string(),
            'type'     => "ENUM('int', 'string', 'text', 'enum') NOT NULL",
        ], $tableOptions);

        $this->createTable('{{%transport_type_has_field}}', [
            'id'       => $this->primaryKey(),
            'type_id'  => $this->integer()->notNull(),
            'field_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('fk_transport_type_has_field_field_id', '{{%transport_type_has_field}}', 'field_id',
            '{{%transport_field}}', 'field_id',
            'cascade', 'cascade');

        $this->addForeignKey('fk_transport_type_has_field_type_id', '{{%transport_type_has_field}}', 'type_id',
            '{{%transport_type}}', 'type_id',
            'cascade', 'cascade');

        $this->createTable('{{%transport_field_enum}}', [
            'enum_id'           => $this->primaryKey(),
            'type_has_field_id' => $this->integer()->notNull()->comment('PK for tbl_transport_type_has_field'),
            'value'             => $this->string()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('fk_transport_field_enum_field_id', '{{%transport_field_enum}}', 'type_has_field_id',
            '{{%transport_type_has_field}}', 'id',
            'cascade', 'cascade');
    }

    public function down()
    {
        echo "m160525_074834_transport_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
