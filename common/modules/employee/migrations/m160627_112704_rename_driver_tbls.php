<?php

use yii\db\Migration;
use yii\db\Query;
use yii\helpers\Console;

class m160627_112704_rename_driver_tbls extends Migration
{
    public function up()
    {
        $this->renameTable('{{%driver_active_aboniment}}', '{{%worker_active_aboniment}}');
        $this->addColumn('{{%worker_active_aboniment}}', 'worker_id', $this->integer(10)->notNull());
        $this->execute("update `tbl_worker_active_aboniment` set worker_id=`driver_id`");
        try {
            $this->addForeignKey('fk_worker_active_aboniment_worker_id', '{{%worker_active_aboniment}}', 'worker_id',
                '{{%worker}}', 'worker_id', 'CASCADE', 'CASCADE');
        } catch (yii\db\IntegrityException $e) {
            Console::output('Ощибка добавления ключа "fk_worker_active_aboniment_worker_id" для worker_id');
            Console::output('Поиск несуществующих worker_id');
            $res = (new Query())
                ->select('aboniment_id')
                ->from('{{%worker_active_aboniment}} a')
                ->leftJoin('{{%worker}} w', 'a.worker_id = w.worker_id')
                ->where(['w.worker_id' => null])
                ->column();

            if ($res) {
                Console::output('Начинаем удаление несуществующих ключей');
                $count = Yii::$app->db->createCommand()->delete('{{%worker_active_aboniment}}',
                    ['aboniment_id' => $res])->execute();
                Console::output("Удалено $count записей");
                Console::output('Дабавляем ключ...');
                $this->addForeignKey('fk_worker_active_aboniment_worker_id', '{{%worker_active_aboniment}}',
                    'worker_id',
                    '{{%worker}}', 'worker_id', 'CASCADE', 'CASCADE');
            } else {
                Console::output('Ключ "fk_worker_active_aboniment_worker_id" не добавлен');
            }
        }

        $this->dropForeignKey('fk_tbl_driver', '{{%worker_active_aboniment}}');
        $this->dropColumn('{{%worker_active_aboniment}}', 'driver_id');

        $this->renameTable('{{%driver_block}}', '{{%worker_block}}');
        $this->addColumn('{{%worker_block}}', 'worker_id', $this->integer(10)->notNull());
        $this->execute("update `tbl_worker_block` set worker_id=`driver_id`");
        try {
            $this->addForeignKey('fk_worker_block_worker_id', '{{%worker_block}}', 'worker_id',
                '{{%worker}}', 'worker_id', 'CASCADE', 'CASCADE');
        } catch (yii\db\IntegrityException $e) {
            Console::output('Ощибка добавления ключа "fk_worker_block_worker_id" для worker_id');
            Console::output('Поиск несуществующих worker_id');
            $res = (new Query())
                ->select('block_id')
                ->from('{{%worker_block}} b')
                ->leftJoin('{{%worker}} w', 'b.worker_id = w.worker_id')
                ->where(['w.worker_id' => null])
                ->column();

            if ($res) {
                Console::output('Начинаем удаление несуществующих ключей');
                $count = Yii::$app->db->createCommand()->delete('{{%worker_block}}', ['block_id' => $res])->execute();
                Console::output("Удалено $count записей");
                Console::output('Дабавляем ключ...');
                $this->addForeignKey('fk_worker_block_worker_id', '{{%worker_block}}', 'worker_id',
                    '{{%worker}}', 'worker_id', 'CASCADE', 'CASCADE');
            } else {
                Console::output('Ключ "fk_worker_active_aboniment_worker_id" не добавлен');
            }
        }

        $this->dropColumn('{{%worker_block}}', 'driver_id');

        $this->renameTable('{{%driver_refuse_order}}', '{{%worker_refuse_order}}');
        $this->addColumn('{{%worker_refuse_order}}', 'worker_id', $this->integer(10)->notNull());
        $this->execute("update `tbl_worker_refuse_order` set worker_id=`driver_id`");

        try {
            $this->addForeignKey('fk_worker_refuse_order_worker_id', '{{%worker_refuse_order}}', 'worker_id',
                '{{%worker}}', 'worker_id', 'CASCADE', 'CASCADE');
        } catch (yii\db\IntegrityException $e) {
            Console::output('Ощибка добавления ключа "fk_worker_refuse_order_worker_id" для worker_id');
            Console::output('Поиск несуществующих worker_id');
            $res = (new Query())
                ->select('refuse_id')
                ->from('{{%worker_refuse_order}} o')
                ->leftJoin('{{%worker}} w', 'o.worker_id = w.worker_id')
                ->where(['w.worker_id' => null])
                ->column();

            if ($res) {
                Console::output('Начинаем удаление несуществующих ключей');
                $count = Yii::$app->db->createCommand()->delete('{{%worker_refuse_order}}',
                    ['refuse_id' => $res])->execute();
                Console::output("Удалено $count записей");
                Console::output('Дабавляем ключ...');
                $this->addForeignKey('fk_worker_refuse_order_worker_id', '{{%worker_refuse_order}}', 'worker_id',
                    '{{%worker}}', 'worker_id', 'CASCADE', 'CASCADE');
            } else {
                Console::output('Ключ "fk_worker_active_aboniment_worker_id" не добавлен');
            }
        }

        $this->dropColumn('{{%worker_refuse_order}}', 'driver_id');
    }

    public function down()
    {
        echo "m160627_112704_rename_driver_tbls cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
