<?php

use yii\db\Migration;

class m160812_065005_tbl_tenant_helper extends Migration
{
    const TABLE_NAME = '{{%tenant_helper}}';

    public function up()
    {
        $this->renameColumn(self::TABLE_NAME, 'driver_tariff', 'worker_tariff');
        $this->renameColumn(self::TABLE_NAME, 'driver', 'worker');
    }

    public function down()
    {
        echo "m160812_065005_tbl_tenant_helper cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
