<?php

use yii\db\Migration;
use yii\db\Query;
use yii\helpers\Console;

class m160709_051633_rename_column_tbl_worker_review_rating extends Migration
{
    const TABLE_NAME = '{{%worker_review_rating}}';

    public function up()
    {
        $this->dropForeignKey('fk_tbl_driver_review_raiting_tbl_driver1', self::TABLE_NAME);
        $this->renameColumn(self::TABLE_NAME, 'driver_id', 'worker_id');
        $this->alterColumn(self::TABLE_NAME, 'worker_id', $this->integer(10)->notNull());
        $this->renameColumn(self::TABLE_NAME, 'raitng_id', 'rating_id');

        try {
            $this->addForeignKey('fk_tbl_worker_review_rating_worker_id', self::TABLE_NAME, 'worker_id', '{{%worker}}',
                'worker_id', 'cascade', 'cascade');
        } catch (yii\db\IntegrityException $e) {
            Console::output('Ощибка добавления ключа "fk_tbl_worker_review_rating_worker_id" для worker_id');
            Console::output('Поиск несуществующих worker_id');
            $res = (new Query())
                ->select('rating_id')
                ->from(self::TABLE_NAME . ' r')
                ->leftJoin('{{%worker}} w', 'r.worker_id = w.worker_id')
                ->where(['w.worker_id' => null])
                ->column();

            if ($res) {
                Console::output('Начинаем удаление несуществующих ключей');
                $count = Yii::$app->db->createCommand()->delete(self::TABLE_NAME,
                    ['rating_id' => $res])->execute();
                Console::output("Удалено $count записей");
                Console::output('Дабавляем ключ...');
                $this->addForeignKey('fk_tbl_worker_review_rating_worker_id', self::TABLE_NAME, 'worker_id', '{{%worker}}',
                    'worker_id', 'cascade', 'cascade');
            } else {
                Console::output('Ключ "fk_tbl_worker_review_rating_worker_id" не добавлен');
            }
        }
    }

    public function down()
    {
        echo "m160709_051633_rename_column_tbl_worker_review_rating cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
