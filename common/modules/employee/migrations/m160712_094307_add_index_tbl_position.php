<?php

use yii\db\Migration;

class m160712_094307_add_index_tbl_position extends Migration
{
    public function up()
    {
        $this->createIndex('idx_tbl_position_code', '{{%position}}', 'code', true);
    }

    public function down()
    {
        $this->dropIndex('idx_tbl_position_code', '{{%position}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
