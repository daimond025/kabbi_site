<?php

use yii\db\Migration;

class m160526_053502_tbl_transport_type_has_field extends Migration
{
    public function up()
    {
        $this->createIndex('idx_type_id_field_id', '{{%transport_type_has_field}}', ['type_id', 'field_id'], true);
    }

    public function down()
    {
        echo "m160526_053502_tbl_transport_type_has_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
