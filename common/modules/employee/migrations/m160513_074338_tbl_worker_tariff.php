<?php

use yii\db\Migration;

class m160513_074338_tbl_worker_tariff extends Migration
{
    const TABLE_NAME = '{{%worker_tariff}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'position_class_id',
            $this->integer(10)->comment('PK of table "tbl_position"'));
        $this->addForeignKey('fk_worker_tariff_has_position_class', self::TABLE_NAME, 'position_class_id',
            '{{%position}}', 'position_id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_worker_tariff_has_position_class', self::TABLE_NAME);
        $this->dropColumn(self::TABLE_NAME, 'position_class_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
