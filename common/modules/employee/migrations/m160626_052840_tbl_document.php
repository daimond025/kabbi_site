<?php

use yii\db\Migration;

class m160626_052840_tbl_document extends Migration
{
    const TABLE_NAME = '{{%document}}';
    public function up()
    {
        $arData = [
            ['Passport', 1],
            ['INN', 2],
            ['OGRNIP', 3],
            ['SNILS', 4],
            ['Medical certificate', 5],
            ['OSAGO', 6],
            ['Driver license', 7],
        ];

        $this->batchInsert(self::TABLE_NAME, ['name', 'code'], $arData);
    }

    public function down()
    {
        $this->truncateTable(self::TABLE_NAME);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
