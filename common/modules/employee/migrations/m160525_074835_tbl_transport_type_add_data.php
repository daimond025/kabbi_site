<?php

use yii\db\Migration;

class m160525_074835_tbl_transport_type_add_data extends Migration
{
    public function up()
    {
        $arData = [
            ['Passenger'],
            ['Cargo'],
            ['Bus'],
            ['Bus'],
            ['Evacuator'],
            ['Specialized vehicles'],
        ];
        $this->batchInsert('{{%transport_type}}', ['name'], $arData);
    }

    public function down()
    {
        echo "m160510_050921_tbl_transport_type_add_data cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
