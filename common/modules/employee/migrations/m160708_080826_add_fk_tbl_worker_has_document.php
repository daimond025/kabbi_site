<?php

use yii\db\Migration;

class m160708_080826_add_fk_tbl_worker_has_document extends Migration
{
    public function up()
    {
        $this->addForeignKey('fk_tbl_worker_has_document_has_position', '{{%worker_has_document}}', 'has_position_id',
            '{{%worker_has_position}}', 'id', 'cascade', 'cascade');
    }

    public function down()
    {
        echo "m160708_080826_add_fk_tbl_worker_has_document cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
