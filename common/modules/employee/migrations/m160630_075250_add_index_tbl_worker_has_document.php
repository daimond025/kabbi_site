<?php

use yii\db\Migration;

class m160630_075250_add_index_tbl_worker_has_document extends Migration
{
    const INDEX_NAME = 'idx_worker_has_document_worker_id_document_id_has_position_id';
    const TABLE_NAME = '{{%worker_has_document}}';

    public function up()
    {
        $this->createIndex(self::INDEX_NAME, self::TABLE_NAME,
            ['worker_id', 'document_id', 'has_position_id'], true);
    }

    public function down()
    {
        $this->dropIndex(self::INDEX_NAME, self::TABLE_NAME);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
