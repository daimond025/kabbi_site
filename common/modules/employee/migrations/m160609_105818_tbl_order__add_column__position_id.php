<?php

use yii\db\Migration;

class m160609_105818_tbl_order__add_column__position_id extends Migration
{
    const TABLE_ORDER = '{{%order}}';
    const COLUMN_POSITION_ID = 'position_id';

    public function up()
    {
        $this->addColumn(
            self::TABLE_ORDER,
            self::COLUMN_POSITION_ID,
            $this->integer()
        );
    }

    public function down()
    {
        $this->dropColumn(self::TABLE_ORDER, self::COLUMN_POSITION_ID);
    }

}
