<?php

use yii\db\Migration;

class m160803_122625_tbl_call extends Migration
{
    const TABLE_NAME = '{{%call}}';

    public function up()
    {
        $this->truncateTable(self::TABLE_NAME);
        $this->dropForeignKey('fk_driver', self::TABLE_NAME);
        $this->renameColumn(self::TABLE_NAME, 'driver_id', 'worker_id');
        $this->alterColumn(self::TABLE_NAME, 'worker_id', $this->integer(10));
        $this->addForeignKey('fk_call_worker_id', self::TABLE_NAME, 'worker_id', '{{%worker}}', 'worker_id', 'cascade',
            'cascade');
    }

    public function down()
    {
        echo "m160803_122625_tbl_call cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
