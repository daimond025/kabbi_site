<?php

use yii\db\Migration;

class m160622_135127_tbl_client_bonus__add_column__position_id extends Migration
{
    const TABLE_CLIENT_BONUS = '{{%client_bonus}}';
    const TABLE_POSITION = '{{%position}}';
    const COLUMN_POSITION_ID = 'position_id';
    const COLUMN_POSITION_CLASS_ID = 'position_class_id';
    const FOREIGN_KEY__POSITION_ID = 'fk_client_bonus__position_id';
    const FOREIGN_KEY__POSITION_CLASS_ID = 'fk_client_bonus__position_class_id';

    public function up()
    {
        $this->addColumn(self::TABLE_CLIENT_BONUS, self::COLUMN_POSITION_ID, $this->integer());
        $this->addForeignKey(
            self::FOREIGN_KEY__POSITION_ID,
            self::TABLE_CLIENT_BONUS,
            self::COLUMN_POSITION_ID,
            self::TABLE_POSITION,
            self::COLUMN_POSITION_ID,
            'CASCADE', 'CASCADE');

        $this->addColumn(self::TABLE_CLIENT_BONUS, self::COLUMN_POSITION_CLASS_ID, $this->integer());
        $this->addForeignKey(
            self::FOREIGN_KEY__POSITION_CLASS_ID,
            self::TABLE_CLIENT_BONUS,
            self::COLUMN_POSITION_CLASS_ID,
            self::TABLE_POSITION,
            self::COLUMN_POSITION_ID,
            'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey(self::FOREIGN_KEY__POSITION_ID, self::TABLE_CLIENT_BONUS);
        $this->dropColumn(self::TABLE_CLIENT_BONUS, self::COLUMN_POSITION_ID);

        $this->dropForeignKey(self::FOREIGN_KEY__POSITION_CLASS_ID, self::TABLE_CLIENT_BONUS);
        $this->dropColumn(self::TABLE_CLIENT_BONUS, self::COLUMN_POSITION_CLASS_ID);
    }

}
