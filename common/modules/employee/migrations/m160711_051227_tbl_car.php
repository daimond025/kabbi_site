<?php

use yii\db\Migration;

class m160711_051227_tbl_car extends Migration
{
    const TABLE_NAME = '{{%car}}';
    
    public function up()
    {
        $cars = (new \yii\db\Query())
            ->select('car_id')
            ->from(self::TABLE_NAME)
            ->where(['owner' => 'DRIVER'])
            ->column();

        $this->alterColumn(self::TABLE_NAME, 'owner', 'ENUM("COMPANY", "WORKER")');
        $this->update(self::TABLE_NAME, ['owner' => 'WORKER'], ['car_id' => $cars]);
    }

    public function down()
    {
        echo "m160711_051227_tbl_car cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
