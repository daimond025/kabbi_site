<?php

use yii\db\Migration;

class m160622_094218_tbl_position extends Migration
{
    const TABLE_NAME = '{{%position}}';
    const TRANSPORT_TYPE_ID = 1;

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'transport_type_id', $this->integer()->comment('PK of tbl_transport_type'));
        $this->update(self::TABLE_NAME, ['transport_type_id' => self::TRANSPORT_TYPE_ID]);
        $this->addForeignKey('fr_position_transport_type_id', self::TABLE_NAME, 'transport_type_id',
            '{{%transport_type}}', 'type_id',
            'SET NULL', 'cascade');
    }

    public function down()
    {
        $this->dropForeignKey('fr_position_transport_type_id', self::TABLE_NAME);
        $this->dropColumn(self::TABLE_NAME, 'transport_type_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
