<?php

use yii\db\Migration;

class m160809_132315_tbl_worker_tariff__clear_days_column_in_once_tariff_type extends Migration
{
    const TABLE_WORKER_TARIFF = '{{%worker_tariff}}';

    public function safeUp()
    {
        $this->update(self::TABLE_WORKER_TARIFF, [
            'days' => null,
        ], [
            'type' => 'ONCE',
        ]);
    }

    public function safeDown()
    {
    }

}
