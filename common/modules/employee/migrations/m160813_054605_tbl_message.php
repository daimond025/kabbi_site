<?php

use yii\db\Migration;

class m160813_054605_tbl_message extends Migration
{
    public function up()
    {
        $this->update('{{%message}}', ['language' => 'ru'], ['language' => 'ru-RU']);
    }

    public function down()
    {
        echo "m160813_054605_tbl_message cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
