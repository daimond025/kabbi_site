<?php

use yii\db\Migration;
use yii\db\Query;

class m160808_063949_add_rights_to_admin extends Migration
{
    public function up()
    {
        $auth = Yii::$app->authManager;

        $users = (new Query())
            ->select('user_id')
            ->from('{{%user}}')
            ->where(['position_id' => [1, 2, 3, 4, 7]])
            ->column();

        foreach ($users as $user) {
            $cities = $auth->getPermission('cities');
            $auth->assign($cities, $user);

            $bonuses = $auth->getPermission('bonus');
            $auth->assign($bonuses, $user);

            $bankCard = $auth->getPermission('bankCard');
            $auth->assign($bankCard, $user);

            $gootaxTariff = $auth->getPermission('gootaxTariff');
            $auth->assign($gootaxTariff, $user);
        }
    }

    public function down()
    {
        echo "m160808_063949_add_rights_to_admin cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
