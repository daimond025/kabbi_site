<?php

use yii\db\Migration;

class m160518_082708_tbl_worker_group extends Migration
{
    const TABLE_NAME = '{{%worker_group}}';

    public function up()
    {
        $this->alterColumn(self::TABLE_NAME, 'class_id', 'tinyint(3) unsigned');
    }

    public function down()
    {
        $this->alterColumn(self::TABLE_NAME, 'class_id', 'tinyint(3) unsigned NOT NULL');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
