<?php

use yii\db\Migration;

class m160706_123427_remove_fk_tbl_worker_group_can_view_client_tariff extends Migration
{
    public function up()
    {
        $this->dropForeignKey('FK_driver_group_can_view_client_tariff_class_id', '{{%worker_group_can_view_client_tariff}}');
    }

    public function down()
    {
        echo "m160706_123427_remove_fk_tbl_worker_group_can_view_client_tariff cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
