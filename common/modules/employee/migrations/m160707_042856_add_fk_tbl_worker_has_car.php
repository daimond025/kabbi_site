<?php

use yii\db\Migration;

class m160707_042856_add_fk_tbl_worker_has_car extends Migration
{
    public function up()
    {
        $this->addForeignKey('fk_worker_has_car_position', '{{%worker_has_car}}', 'has_position_id',
            '{{%worker_has_position}}', 'id', 'cascade', 'cascade');
    }

    public function down()
    {
        echo "m160707_042856_add_fk_tbl_worker_has_car cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
