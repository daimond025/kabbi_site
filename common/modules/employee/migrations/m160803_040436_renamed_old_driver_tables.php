<?php

use yii\db\Migration;

class m160803_040436_renamed_old_driver_tables extends Migration
{
    public function up()
    {
        $this->renameTable('{{%driver}}', '{{%driver_old}}');
        $this->renameTable('{{%driver_info}}', '{{%driver_info_old}}');
        $this->renameTable('{{%driver_has_car}}', '{{%driver_has_car_old}}');
    }

    public function down()
    {
        echo "m160803_040436_renamed_old_driver_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
