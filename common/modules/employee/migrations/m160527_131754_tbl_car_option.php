<?php

use yii\db\Migration;

class m160527_131754_tbl_car_option extends Migration
{
    const TRANSPORT_TYPE = 1;
    const TABLE_NAME = '{{%car_option}}';

    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'type_id', $this->integer()->comment('PK of tbl_transport_type')->notNull());
        $this->update(self::TABLE_NAME, ['type_id' => self::TRANSPORT_TYPE]);
        $this->addForeignKey('fr_car_option_type_id', self::TABLE_NAME, 'type_id', '{{%transport_type}}', 'type_id',
            'cascade', 'cascade');
    }

    public function down()
    {
        $this->dropForeignKey('fr_car_option_type_id', self::TABLE_NAME);
        $this->dropColumn(self::TABLE_NAME, 'type_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
