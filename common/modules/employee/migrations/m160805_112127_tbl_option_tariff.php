<?php

use yii\db\Migration;

class m160805_112127_tbl_option_tariff extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%option_tariff}}', 'wait_driving_time_day',
            $this->integer()->defaultValue(0)->unsigned()->comment('Бесплатное время ожидания во время поездки за 1 раз'));

        $this->alterColumn('{{%option_tariff}}', 'wait_driving_time_night',
            $this->integer()->defaultValue(0)->unsigned()->comment('Бесплатное время ожидания во время поездки за 1 раз'));
    }

    public function down()
    {
        echo "m160805_112127_tbl_option_tariff cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
