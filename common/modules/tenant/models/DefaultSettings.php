<?php

namespace common\modules\tenant\models;

use backend\modules\setting\models\ElasticAccess;
use common\modules\tenant\exceptions\InvalidSettingTypeException;
use yii\caching\Cache;
use yii\caching\TagDependency;

/**
 * This is the model class for table "tbl_default_settings".
 *
 * @property string $name
 * @property string $value
 * @property string $type
 */
class DefaultSettings extends \yii\db\ActiveRecord
{
    const TYPE_SYSTEM = 'system';
    const TYPE_GENERAL = 'general';
    const TYPE_ORDERS = 'orders';
    const TYPE_DRIVERS = 'drivers';

    const SETTING_GEOCODER_TYPE = 'GEOCODER_TYPE';
    const SETTING_GEOSERVICE_API_KEY = 'GEOSERVICE_API_KEY';
    const SETTING_WORKER_API_KEY = 'WORKER_API_KEY';
    const SETTING_CLIENT_API_KEY = 'CLIENT_API_KEY';
    const SETTING_MAX_NUMBER_OF_ADDRESS_POINTS = 'MAX_NUMBER_OF_ADDRESS_POINTS';
    const SETTING_CLIENT_APP_PAGE = 'CLIENT_APP_PAGE';
    const SETTING_GOOGLE_PLAY_WORKER_APP_LINK = 'GOOGLE_PLAY_WORKER_APP_LINK';
    const SETTING_APP_STORE_WORKER_APP_LINK = 'APP_STORE_WORKER_APP_LINK';
    const SETTING_CURRENCY = 'CURRENCY';
    const SETTING_LANGUAGE = 'LANGUAGE';
    const SETTING_PHONE = 'PHONE';
    const SETTING_WORKER_PRE_ORDER_REMINDER = 'WORKER_PRE_ORDER_REMINDER';
    const SETTING_FREE_STATUS_BETWEEN_DISTRIBUTION_CYCLES = 'FREE_STATUS_BETWEEN_DISTRIBUTION_CYCLES';
    const SETTING_TYPE_OF_DISTRIBUTION_ORDER = 'TYPE_OF_DISTRIBUTION_ORDER';
    const SETTING_ORDER_OFFER_SEC = 'ORDER_OFFER_SEC';
    const SETTING_DISTANCE_FOR_SEARCH = 'DISTANCE_FOR_SEARCH';
    const SETTING_CHECK_WORKER_RATING_TO_OFFER_ORDER = 'CHECK_WORKER_RATING_TO_OFFER_ORDER';
    const SETTING_CIRCLES_DISTRIBUTION_ORDER = 'CIRCLES_DISTRIBUTION_ORDER';
    const SETTING_DELAY_CIRCLES_DISTRIBUTION_ORDER = 'DELAY_CIRCLES_DISTRIBUTION_ORDER';
    const SETTING_SHOW_WORKER_PHONE = 'SHOW_WORKER_PHONE';
    const SETTING_IS_FIX = 'IS_FIX';
    const SETTING_RESTRICT_VISIBILITY_OF_PRE_ORDER = 'RESTRICT_VISIBILITY_OF_PRE_ORDER';
    const SETTING_TIME_OF_PRE_ORDER_VISIBILITY = 'TIME_OF_PRE_ORDER_VISIBILITY';
    const SETTING_VISIBLE_WORKER_PHONE = 'VISIBLE_WORKER_PHONE';
    const SETTING_AUTOCOMPLETE_RADIUS = 'AUTOCOMPLETE_RADIUS';
    const SETTING_ALLOW_RESERVE_URGENT_ORDERS = 'ALLOW_RESERVE_URGENT_ORDERS';
    const SETTING_MAX_COUNT_RESERVE_URGENT_ORDERS = 'MAX_COUNT_RESERVE_URGENT_ORDERS';
    const SETTING_EXCHANGE_ENABLE_SALE_ORDERS = 'EXCHANGE_ENABLE_SALE_ORDERS';
    const SETTING_EXCHANGE_HOW_WAIT_WORKER = 'EXCHANGE_HOW_WAIT_WORKER';
    const SETTING_ADD_INTERMEDIATE_POINT_IN_ORDER_FOR_STOP = 'ADD_INTERMEDIATE_POINT_IN_ORDER_FOR_STOP';
    const SETTING_SIGNAL_NEW_ORDER = 'SIGNAL_NEW_ORDER';
    const SETTING_ORDER_FORM_ADD_POINT_PHONE = 'ORDER_FORM_ADD_POINT_PHONE';
    const SETTING_ORDER_FORM_ADD_POINT_COMMENT = 'ORDER_FORM_ADD_POINT_COMMENT';
    const SETTING_REQUIRE_POINT_CONFIRMATION_CODE = 'REQUIRE_POINT_CONFIRMATION_CODE';
    const SETTING_USE_STRIPE_CONNECT = 'USE_STRIPE_CONNECT';

    const FRONTEND_API_KEY_ELASTIC_ID = 'frontend';

    const CACHE_TAG = 'default_settings';

    const SQL_ADD_SETTINGS = <<<SQL
INSERT INTO `tbl_tenant_setting`
            (`tenant_id`, `name`, `value`, `type`)
SELECT `t`.`tenant_id`, :name `name`, :value `value`, :type `type`
FROM `tbl_tenant` AS `t`
LEFT JOIN `tbl_tenant_setting` AS `s` ON `t`.`tenant_id` = `s`.`tenant_id` AND `s`.`name` = :name
WHERE `s`.`setting_id` IS NULL
SQL;

    const SQL_ADD_SETTINGS_WITH_CITY = <<<SQL
INSERT INTO `tbl_tenant_setting`
            (`tenant_id`, `city_id`, `name`, `value`, `type`)
SELECT `c`.`tenant_id`, `c`.`city_id`, :name `name`, :value `value`, :type `type`
FROM `tbl_tenant_has_city` AS `c`
LEFT JOIN `tbl_tenant_setting` AS `t` ON `c`.`tenant_id` = `t`.`tenant_id` AND `c`.`city_id` = `t`.`city_id` AND `t`.`name` = :name
WHERE `t`.`setting_id` IS NULL
SQL;

    const SQL_ADD_SETTINGS_WITH_CITY_AND_POSITION = <<<SQL
INSERT INTO `tbl_tenant_setting`
            (`tenant_id`, `city_id`, `position_id`, `name`, `value`, `type`)
SELECT `p`.`tenant_id`, `p`.`city_id`, `p`.`position_id`, :name `name`, :value `value`, :type `type`
FROM `tbl_tenant_has_city` AS `c`
JOIN `tbl_tenant_city_has_position` AS `p` ON `c`.`tenant_id` = `p`.`tenant_id` AND `c`.`city_id` = `p`.`city_id`
LEFT JOIN `tbl_tenant_setting` AS `t` ON `p`.`tenant_id` = `t`.`tenant_id` AND `p`.`city_id` = `t`.`city_id` AND `p`.`position_id` = `t`.`position_id` AND `t`.`name` = :name
WHERE `t`.`setting_id` IS NULL
SQL;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%default_settings}}';
    }

    /**
     * @inheritdoc
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['value', 'type'], 'string'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name'  => 'Name',
            'value' => 'Value',
            'type'  => 'Type',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (array_key_exists('value', $changedAttributes) && $this->name == self::SETTING_GEOSERVICE_API_KEY) {
            $elasticRow = ElasticAccess::findOne(self::FRONTEND_API_KEY_ELASTIC_ID);
            if (!empty($elasticRow)) {
                $elasticRow->api_key = $this->value;
                $elasticRow->save(false, ['api_key']);
            }
        }

        if ($insert || array_key_exists('type', $changedAttributes)) {
            static::addMissingSettingAllTenants($this->name, $this->value, $this->type);
        }

        $cache = \Yii::$app->getCache();
        TagDependency::invalidate($cache, self::CACHE_TAG);
    }

    /**
     * Getting default value
     *
     * @param string $name
     * @param string $defaultValue
     *
     * @return string
     */
    public static function getValue($name, $defaultValue = null)
    {
        $cache = \Yii::$app->getCache();

        $key   = 'defaultSetting_' . $name;
        $value = $cache->get($key);

        if ($value === false) {
            $value = self::find()->where(['name' => $name])->select('value')->scalar();

            $cache->set($key, $value, 60 * 60, new TagDependency(['tags' => self::CACHE_TAG]));
        }

        return $value === null ? $defaultValue : $value;
    }

    /**
     * Add missing settings with city and position to all tenants
     *
     * @param string $name
     * @param string $value
     * @param string $type
     *
     * @throws \yii\db\Exception
     * @throws InvalidSettingTypeException
     */
    public static function addMissingSettingAllTenants($name, $value, $type)
    {
        switch ($type) {
            case self::TYPE_SYSTEM:
                $sql = self::SQL_ADD_SETTINGS;
                break;
            case self::TYPE_GENERAL:
                $sql = self::SQL_ADD_SETTINGS_WITH_CITY;
                break;
            case self::TYPE_ORDERS:
            case self::TYPE_DRIVERS:
                $sql = self::SQL_ADD_SETTINGS_WITH_CITY_AND_POSITION;
                break;
            default:
                throw new InvalidSettingTypeException('Invalid type');
        }

        \Yii::$app->getDb()->createCommand($sql, [
            'name'  => $name,
            'value' => $value,
            'type'  => $type,
        ])->execute();
    }

    /**
     * Add missing settings from default to tenant
     *
     * @param int $tenantId
     *
     * @throws \yii\db\Exception
     */
    public static function addMissingSettingsToTenant($tenantId)
    {
        $sql = <<<SQL
INSERT INTO `tbl_tenant_setting`
 (`tenant_id`, `name`, `value`, `type`)
SELECT :tenantId `tenant_id`, `d`.`name`, `d`.`value`, `d`.`type`
FROM `tbl_default_settings` AS `d`
LEFT JOIN `tbl_tenant_setting` AS `t` ON `t`.`tenant_id` = :tenantId AND `t`.`name` = `d`.`name` AND `t`.`city_id` IS NULL AND `t`.`position_id` IS NULL
WHERE `d`.`type` = :systemType AND `t`.`setting_id` IS NULL
SQL;

        \Yii::$app->getDb()->createCommand($sql, [
            'tenantId'   => $tenantId,
            'systemType' => self::TYPE_SYSTEM,
        ])->execute();
    }

    /**
     * Add missing settings from default with city to tenant
     *
     * @param int $tenantId
     * @param int $cityId
     *
     * @throws \yii\db\Exception
     */
    public static function addMissingSettingsWithCityToTenant($tenantId, $cityId)
    {
        $sql = <<<SQL
INSERT `tbl_tenant_setting`
 (`tenant_id`, `city_id`, `name`, `value`, `type`)
SELECT :tenantId `tenant_id`, :cityId `city_id`, `d`.`name`, `d`.`value`, `d`.`type`
FROM `tbl_default_settings` AS `d`
LEFT JOIN `tbl_tenant_setting` AS `t` ON `t`.`tenant_id` = :tenantId AND `t`.`city_id` = :cityId AND `t`.`position_id` IS NULL AND `t`.`name` = `d`.`name`
WHERE `d`.`type` = :generalType AND `t`.`setting_id` IS NULL
SQL;

        \Yii::$app->getDb()->createCommand($sql, [
            'tenantId'    => $tenantId,
            'cityId'      => $cityId,
            'generalType' => self::TYPE_GENERAL,
        ])->execute();
    }

    /**
     * Add missing settings from default wit city and position to tenant
     *
     * @param int $tenantId
     * @param int $cityId
     * @param int $positionId
     *
     * @throws \yii\db\Exception
     */
    public static function addMissingSettingsWithCityAndPositionToTenant($tenantId, $cityId, $positionId)
    {
        $sql = <<<SQL
INSERT `tbl_tenant_setting`
 (`tenant_id`, `city_id`, `position_id`, `name`, `value`, `type`)
SELECT :tenantId `tenant_id`, :cityId `city_id`, :positionId `position_id`, `d`.`name`, `d`.`value`, `d`.`type`
FROM `tbl_default_settings` AS `d`
LEFT JOIN `tbl_tenant_setting` AS `t` ON `t`.`tenant_id` = :tenantId AND `t`.`city_id` = :cityId AND `t`.`position_id` = :positionId AND `t`.`name` = `d`.`name`
WHERE `d`.`type` IN (:ordersType, :driversType) AND `t`.`setting_id` IS NULL
SQL;

        \Yii::$app->getDb()->createCommand($sql, [
            'tenantId'    => $tenantId,
            'cityId'      => $cityId,
            'positionId'  => $positionId,
            'ordersType'  => self::TYPE_ORDERS,
            'driversType' => self::TYPE_DRIVERS,
        ])->execute();
    }
}
