<?php

namespace common\modules\tenant\models;

use common\helpers\CacheHelper;
use frontend\modules\companies\models\TenantCompany;
use Yii;
use common\modules\city\models\City;
use frontend\modules\tenant\models\Currency;
use frontend\components\behavior\file\CropFileBehavior;
use app\modules\balance\models\Account;
use backend\modules\setting\models\ElasticAccess;

/**
 * This is the model class for table "{{%tenant}}".
 *
 * @property string $tenant_id
 * @property string $company_name
 * @property string $full_company_name
 * @property string $legal_address
 * @property string $post_address
 * @property string $contact_name
 * @property string $contact_last_name
 * @property string $contact_second_name
 * @property string $contact_phone
 * @property string $contact_email
 * @property string $company_phone
 * @property string $domain
 * @property string $email
 * @property string $inn
 * @property string $create_time
 * @property string $bookkeeper
 * @property string $kpp
 * @property string $ogrn
 * @property string $site
 * @property string $archive
 * @property string $logo
 * @property string $director
 * @property string $director_position
 * @property string $status
 * @property string $utm_source
 * @property string $utm_medium
 * @property string $utm_campaign
 * @property string $utm_content
 * @property string $utm_term
 * @property string $client_id_go
 * @property string $client_id_ya
 * @property string $ip
 *
 * @property Car[] $tblCars
 * @property Client[] $tblClients
 * @property ClientCompany[] $tblClientCompanies
 * @property Document[] $tblDocuments
 * @property DocumentFieldValue[] $tblDocumentFieldValues
 * @property DocumentTemplate[] $tblDocumentTemplates
 * @property DocumentTemplateType[] $tblDocumentTemplateTypes
 * @property MessageCity[] $tblMessageCities
 * @property Order[] $tblOrders
 * @property OrderChange[] $tblOrderChanges
 * @property OrderHistory[] $tblOrderHistories
 * @property Parking[] $tblParkings
 * @property PaymentTnDr[] $tblPaymentTnDrs
 * @property PaymentTnSt[] $tblPaymentTnSts
 * @property PaymentTnTn[] $tblPaymentTnTns
 * @property Support[] $tblSupports
 * @property TaxiTariff[] $tblTaxiTariffs
 * @property TenantBalance[] $tblTenantBalances
 * @property TenantHaкsCarOption[] $tblTenantHasCarOptions
 * @property CarOption[] $options
 * @property TenantHasCity[] $tblTenantHasCities
 * @property City[] $cities
 * @property TenantHasSms[] $tblTenantHasSms
 * @property TenantTariff[] $tblTenantTariffs
 * @property User[] $tblUsers
 * @property TenantCompany $tenantCompany
 */
class Tenant extends \yii\db\ActiveRecord
{
    /**
     * Const for tenant status
     */
    const ACTIVE = 'ACTIVE';
    const BLOCKED = 'BLOCKED';
    const REMOVED = 'REMOVED';

    /**
     * @var array Tenant city
     */

    public $city_list = [];
    public $cropParams;

    /**
     *
     * @var bool For lock organization
     */
    public $blocked = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'utm_source',
                    'utm_medium',
                    'utm_campaign',
                    'utm_content',
                    'utm_term',
                    'client_id_go',
                    'client_id_ya',
                    'ip',
                ],
                'safe',
            ],
            [['contact_phone', 'email'], 'required'],
            [
                ['contact_phone', 'company_phone'],
                'filter',
                'filter' => function ($value) {
                    return preg_replace("/[^0-9]/", '', $value);
                },
            ],
            [['email', 'contact_email'], 'email'],
            [['company_name', 'full_company_name', 'legal_address', 'post_address'], 'string', 'max' => 255],
            [
                ['contact_name', 'contact_last_name', 'contact_second_name', 'bookkeeper', 'director_position'],
                'string',
                'max' => 45,
            ],
            [['contact_phone', 'company_phone', 'inn'], 'string', 'max' => 15],
            [['contact_email', 'email', 'ogrn', 'site'], 'string', 'max' => 45],
            [['site'], 'url'],
            [['kpp'], 'string', 'max' => 10],
            [['director'], 'string', 'max' => 50],
            [['blocked', 'cropParams'], 'safe'],
            [
                'logo',
                'image',
                'extensions' => ['png', 'jpeg', 'jpg'],
                'maxSize'    => getUploadMaxFileSize(),
                'tooBig'     => t(
                    'file',
                    'The file size must be less than {file_size}Mb',
                    ['file_size' => getUploadMaxFileSize(true)]
                ),
                'minWidth'   => 300,
                'minHeight'  => 300,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_name'        => t('tenant', 'Company Name'),
            'full_company_name'   => t('tenant', 'Full Company Name'),
            'legal_address'       => t('tenant', 'Legal Address'),
            'post_address'        => t('tenant', 'Post Address'),
            'work_phone'          => t('tenant', 'Company phone'),
            'domain'              => t('tenant', 'Domain'),
            'email'               => t('tenant', 'Company email'),
            'inn'                 => t('tenant', 'Inn'),
            'create_time'         => t('tenant', 'Create Time'),
            'bookkeeper'          => t('tenant', 'Bookkeeper name'),
            'kpp'                 => t('tenant', 'Kpp'),
            'ogrn'                => t('tenant', 'Ogrn'),
            'site'                => t('tenant', 'Site'),
            'director'            => t('tenant', 'Director name'),
            'director_position'   => t('tenant', 'Director position'),
            'logo'                => t('tenant', 'Company logo'),
            'contact_name'        => t('tenant', 'Contact name'),
            'contact_phone'       => t('tenant', 'Contact phone'),
            'contact_email'       => t('tenant', 'Contact email'),
            'company_phone'       => t('tenant', 'Company phone'),
            'contact_last_name'   => t('app', 'Last name'),
            'contact_second_name' => t('app', 'Second name'),
            'city_list'           => t('tenant', 'City where the service is working'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasMany(Client::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientCompanies()
    {
        return $this->hasMany(ClientCompany::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(Document::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentFieldValues()
    {
        return $this->hasMany(DocumentFieldValue::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentTemplates()
    {
        return $this->hasMany(DocumentTemplate::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentTemplateTypes()
    {
        return $this->hasMany(DocumentTemplateType::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessageCities()
    {
        return $this->hasMany(MessageCity::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderChanges()
    {
        return $this->hasMany(OrderChange::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderHistories()
    {
        return $this->hasMany(OrderHistory::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParkings()
    {
        return $this->hasMany(Parking::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentTnDrs()
    {
        return $this->hasMany(PaymentTnDr::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentTnSts()
    {
        return $this->hasMany(PaymentTnSt::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentTnTns()
    {
        return $this->hasMany(PaymentTnTn::className(), ['recipient' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupports()
    {
        return $this->hasMany(Support::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxiTariffs()
    {
        return $this->hasMany(TaxiTariff::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantBalances()
    {
        return $this->hasMany(TenantBalance::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantHasCarOptions()
    {
        return $this->hasMany(TenantHasCarOption::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptions()
    {
        return $this->hasMany(
            CarOption::className(),
            ['option_id' => 'option_id']
        )->viaTable('{{%tenant_has_car_option}}', ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantHasCities()
    {
        return $this->hasMany(TenantHasCity::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['city_id' => 'city_id'])->viaTable(
            '{{%tenant_has_city}}',
            ['tenant_id' => 'tenant_id']
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantHasSms()
    {
        return $this->hasMany(TenantHasSms::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantTariffs()
    {
        return $this->hasMany(TenantTariff::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['tenant_id' => 'tenant_id']);
    }

    public function setEmailToken($email)
    {
        $this->email_confirm = md5($email . time());
    }

    public function getTenantCompany()
    {
        return $this->hasOne(TenantCompany::className(), ['tenant_company_id' => 'tenant_company_id']);
    }

    public function behaviors()
    {
        return [
            'fileBehavior' => [
                'class'      => CropFileBehavior::className(),
                'fileField'  => ['logo'],
                'upload_dir' => app()->params['upload'],
            ],
        ];
    }

    public function afterSignup()
    {
        //Создание дефолтных настроек
        $this->createSettings();
    }

    private function createSettings()
    {
        DefaultSettings::addMissingSettingsToTenant($this->tenant_id);

        $clientApiKey = TenantSetting::generateClientApiKey();
        $workerApiKey = TenantSetting::generateWorkerApiKey();

        TenantSetting::updateSettingValue(
            DefaultSettings::SETTING_CLIENT_API_KEY,
            $this->tenant_id,
            null,
            null,
            $clientApiKey
        );
        TenantSetting::updateSettingValue(
            DefaultSettings::SETTING_WORKER_API_KEY,
            $this->tenant_id,
            null,
            null,
            $workerApiKey
        );

        //Добавляем настройки в Elastic
        ElasticAccess::createKey(
            $this->tenant_id . '_client',
            $this->tenant_id,
            $this->domain,
            ElasticAccess::CLIENT_TYPE_APP,
            $clientApiKey
        );
        ElasticAccess::createKey(
            $this->tenant_id . '_worker',
            $this->tenant_id,
            $this->domain,
            ElasticAccess::WORKER_TYPE_APP,
            $workerApiKey
        );
    }

    public function addDomainToPhoneBase()
    {
        app()->curl->get(app()->params['softphone.updateSIPDomainsUrl']);

        return app()->kamailio->createCommand()->insert('domain', [
            'domain'        => $this->domain . '.' .
                app()->params['softphone.subDomain'] . '.' . app()->params['softphone.domain'],
            'last_modified' => date('Y-m-d H:i:s'),
        ])->execute();
    }

    public static function getDomainName($tenantId = null)
    {
        if (Yii::$app->user->isGuest) {
            return false;
        }

        $tenantId = $tenantId === null ? user()->tenant_id : $tenantId;

        return self::getDomainById($tenantId);
    }

    public static function getTenantHostInPhoneBase($domain = null)
    {
        $domain = empty($domain) ? self::getDomainName() : $domain;

        return $domain . '.' . app()->params['softphone.subDomain'] . '.' . app()->params['site.domain'];
    }

    public static function getTenantUrl($domain)
    {
        return app()->params['site.protocol'] . '://' . $domain . '.' . app()->params['site.domain'];
    }

    public static function getNameByDomain($domain)
    {
        return self::find()->where(['domain' => $domain])->select('company_name')->scalar();
    }

    public static function getLogoByDomain($domain)
    {
        return self::find()->where(['domain' => $domain])->select('logo')->scalar();
    }

    public static function getIdByDomain($domain)
    {
        return self::getDb()->cache(function () use ($domain) {
            return self::find()->where(['domain' => $domain])->select('tenant_id')->scalar();
        }, 3600);
    }

    public static function getDomainById($tenantId)
    {
        return self::getDb()->cache(function () use ($tenantId) {
            return self::find()
                ->select('domain')
                ->where([
                    'tenant_id' => $tenantId,
                ])
                ->scalar();
        }, 3600);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (!$insert) {
            //Очищаем кеш логотипа
            if (array_key_exists('logo', $changedAttributes)) {
                app()->cache->delete('logo_' . user()->tenant_id);
            } elseif (array_key_exists('company_name', $changedAttributes)) {
                app()->cache->delete('company_name_' . user()->tenant_id);
            }
        }
    }

    /**
     * Путь до логотипа.
     * @return string
     */
    public static function getLogoPath()
    {
        $logoName = CacheHelper::getFromCache('logo_' . user()->tenant_id, function () {
            return user()->tenant->logo;
        });

        $tenant = new Tenant();

        return $tenant->isFileExists($logoName) ? $tenant->getPicturePath($logoName) : '';
    }

    /**
     * Баланс арендатора
     * @return float
     */
    public static function getBalance()
    {
        return Account::find()->where([
            'acc_kind_id' => Account::TENANT_KIND,
            'tenant_id'   => user()->tenant_id,
            'currency_id' => Currency::CURRENCY_ID_RUS_RUBLE,
        ])->select('balance')->scalar();
    }

    public static function getHasTenantCities()
    {
        $tenantData = Tenant::find()->where(['tenant_id' => user()->tenant_id])->with('cities')->one();

        return count($tenantData->cities) > 0;
    }
}
