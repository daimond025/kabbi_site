<?php

namespace common\modules\tenant\models;

use common\modules\tenant\components\NotificationTemplatesService;
use Yii;
use common\modules\city\models\City;
use common\modules\city\models\Republic;
use common\modules\tenant\models\mobileApp\MobileAppHasCity;
use frontend\modules\tenant\models\GeoServiceProvider;
use frontend\modules\tenant\models\mobileApp\MobileApp;
use app\modules\parking\models\Parking;
use common\components\behaviors\ActiveRecordBehavior;
use frontend\modules\tenant\models\UserHasCity;
use app\modules\tenant\models\User;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%tbl_tenant_has_city}}".
 *
 * @property integer                 $tenant_id
 * @property integer                 $city_id
 * @property integer                 $currency_id
 * @property string                  $phone
 * @property integer                 $sort
 * @property integer                 $block
 *
 * @property City                    $city
 * @property Tenant                  $tenant
 * @property TenantCityHasPosition[] $activePositions
 */
class TenantHasCity extends \yii\db\ActiveRecord
{

    const SCENARIO_INSERT = 'city_insert';

    public $lang;

    public $currency;

    public $phone;

    public $isUseSignalNewOrder;

    private $cityName;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_has_city}}';
    }

    public function beforeValidate()
    {
        $this->tenant_id = user()->tenant_id;

        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id'], 'required', 'on' => self::SCENARIO_INSERT],
            [
                'city_id',
                'unique',
                'targetAttribute' => ['city_id', 'tenant_id'],
                'message'         => t('validator', 'This city is already bound, check in tabs "Active" and "Blocked"'),
            ],
            ['city_id', 'integer', 'message' => t('app', 'City list cannot be blank')],
            [['tenant_id', 'currency', 'sort', 'block'], 'integer'],
            [['sort'], 'default', 'value' => 100],
            ['lang', 'required'],
            ['lang', 'string'],
            ['currency', 'required'],
            [['phone'], 'string', 'max' => 20],
            [['isUseSignalNewOrder'], 'required'],
            [['isUseSignalNewOrder'], 'in', 'range' => [0, 1]],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tenant_id' => Yii::t('app', 'Tenant ID'),
            'city_id'   => t('app', 'City'),
            'currency'  => Yii::t('currency', 'Currency'),
            'sort'      => Yii::t('app', 'Sort'),
            'block'     => Yii::t('app', 'Active'),
            'lang'      => Yii::t('app', 'Language'),
            'phone'     => Yii::t('app', 'Dispatcher phone'),

            'isUseSignalNewOrder' => Yii::t('app', 'To play a sound signal when a new order is received'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivePositions()
    {
        return $this->hasMany(TenantCityHasPosition::className(), [
            'tenant_id' => 'tenant_id',
            'city_id'   => 'city_id',
        ]);
    }

    public function getCityName()
    {
        return $this->city->name;
    }

    public function behaviors()
    {
        return [
            'common' => [
                'class' => ActiveRecordBehavior::className(),
            ],
        ];
    }

    /**
     * Allow to save multiple models
     *
     * @param array $arCity
     *
     * @return boolean
     */
    public static function manySave($arCity)
    {
        if (is_array($arCity) && !empty($arCity)) {
            $insertValue = [];
            $connection  = app()->db;
            $tenant_id   = user()->tenant_id;
            $arCity      = array_unique($arCity);

            //Синхронизация городов тенанта с выбранными пользовательскими городами
            $user_has_city = UserHasCity::find()
                ->joinWith([
                    'user' => function ($query) {
                        $query->andWhere(['tenant_id' => user()->tenant_id]);
                    },
                ], false)
                ->all();

            $user_map         = ArrayHelper::map($user_has_city, 'city_id', 'user_id');
            $user_city_delete = array_diff(array_keys($user_map), $arCity);

            $user_id_delete = [];

            foreach ($user_city_delete as $city_id) {
                $user_id_delete[] = $user_map[$city_id];
            }

            if (!empty($user_city_delete)) {
                UserHasCity::deleteAll([
                    'city_id' => $user_city_delete,
                    'user_id' => $user_id_delete,
                ]);
            }
            //-------------------------------------------------------------------

            foreach ($arCity as $city_id) {
                if ($city_id > 0) {
                    $insertValue[] = [$tenant_id, $city_id];
                }
            }

            if (!empty($insertValue)) {
                $connection->createCommand()->batchInsert(self::tableName(), ['tenant_id', 'city_id'],
                    $insertValue)->execute();

                return true;
            }
        }

        return false;
    }

    /**
     * Get tenant city list
     *
     * @param bool $withRepublic
     *
     * @return array Models
     */
    public static function getCityList($withRepublic = true)
    {
        return self::find()
            ->where(['tenant_id' => user()->tenant_id])
            ->select('city_id')
            ->with([
                'city' => function ($query) use ($withRepublic) {
                    if ($withRepublic) {
                        $query->with([
                            'republic' => function ($query) {
                                $query->select(['republic_id', Republic::tableName() . '.name']);
                            },
                        ]);
                    }
                },
            ])
            ->all();
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            //Заполняем хелпера
            TenantHelper::fillHelpItem(TenantHelper::CITY_FILL);

            //Добавляем городу базовую парковку
            Parking::addBasePolygon($this->city_id, $this->lang);

            //Добавляем город топ-менеджерам
            $top_managers = User::find()
                ->where(['tenant_id' => $this->tenant_id, 'position_id' => User::getTopManagerPositionIdList()])
                ->select(['user_id'])
                ->column();

            if (!empty($top_managers)) {
                $insertValue = [];
                foreach ($top_managers as $maneger_id) {
                    $insertValue[] = [$this->city_id, $maneger_id];
                    //Сброс кеша
                    UserHasCity::deleteCache($maneger_id);
                }

                if (!empty($insertValue)) {
                    app()->db->createCommand()->batchInsert(UserHasCity::tableName(), ['city_id', 'user_id'],
                        $insertValue)->execute();
                }
            }

            // Добавляем город в мобильные приложения
            $mobileApps = MobileApp::find()
                ->where([
                    'tenant_id'           => user()->tenant_id,
                    'city_autocompletion' => 1,
                ])
                ->all();

            if ($mobileApps) {
                foreach ($mobileApps as $app) {
                    /* @var $app MobileApp */
                    $model = new MobileAppHasCity([
                        'app_id'  => $app->app_id,
                        'city_id' => $this->city_id,
                    ]);
                    $model->save();
                }
            }

            //Создание настроек арендатора
            $this->setTenantCitySettings();

            $domain = Tenant::getDomainName();
            GeoServiceProvider::createDefaultGeoProviders(user()->tenant_id, $domain, $this->city_id);
        } else {
            //Блок сброса кеша при смене активности филиала
            if (isset($changedAttributes['block'])) {
                $users_has_city = UserHasCity::getUsersIdByCity($this->city_id);
                foreach ($users_has_city as $user_id) {
                    UserHasCity::deleteCache($user_id);
                }

                User::deleteUserListCache($this->city_id, $this->tenant_id);
            }
            //----------------------------------------------
        }

        $this->addMissingModelsForCityPositions();

        parent::afterSave($insert, $changedAttributes);
    }

    private function addMissingModelsForCityPositions()
    {
        $positions = TenantCityHasPosition::find()
            ->select('position_id')
            ->where([
                'tenant_id' => $this->tenant_id,
                'city_id'   => $this->city_id,
            ])
            ->column();

        if (is_array($positions)) {
            foreach ($positions as $positionId) {
                NotificationTemplatesService::addAllMissingClientSmsNotificationsToTenant(
                    $this->tenant_id, $this->city_id, $positionId);
                NotificationTemplatesService::addAllMissingRecipientSmsNotificationsToTenant(
                    $this->tenant_id, $this->city_id, $positionId);
                NotificationTemplatesService::addAllMissingClientPushNotificationsToTenant(
                    $this->tenant_id, $this->city_id, $positionId);
                NotificationTemplatesService::addAllMissingWorkerPushNotificationsToTenant(
                    $this->tenant_id, $this->city_id, $positionId);
                DefaultClientStatusEvent::addAllMissingClientStatusEventsToTenant(
                    $this->tenant_id, $this->city_id, $positionId);

                DefaultSettings::addMissingSettingsWithCityAndPositionToTenant(
                    $this->tenant_id, $this->city_id, $positionId);
            }
        }
    }

    private function setTenantCitySettings()
    {
        DefaultSettings::addMissingSettingsWithCityToTenant($this->tenant_id, $this->city_id);

        TenantSetting::updateSettingValue(
            DefaultSettings::SETTING_CURRENCY, $this->tenant_id, $this->city_id, null, $this->currency);
        TenantSetting::updateSettingValue(
            DefaultSettings::SETTING_LANGUAGE, $this->tenant_id, $this->city_id, null, $this->lang);
        TenantSetting::updateSettingValue(
            DefaultSettings::SETTING_PHONE, $this->tenant_id, $this->city_id, null, $this->phone);
    }
}
