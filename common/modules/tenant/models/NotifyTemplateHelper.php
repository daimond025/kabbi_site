<?php
/**
 * Created by PhpStorm.
 * User: zigfried123
 * Date: 06.09.2017
 * Time: 11:31
 */

namespace common\modules\tenant\models;

class NotifyTemplateHelper
{

    public static function getParams($data)
    {
        $params = is_array($data) ? $data : explode(';', $data);
        $params = array_map(function ($param) {
            return trim($param);
        }, $params);

        $result = [];

        foreach ($params as $key => $item) {
            $result[$key] = $item . ' - ' . t('template', $item);
        }

        return implode("<br>", $result);
    }

}