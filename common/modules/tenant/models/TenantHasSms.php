<?php

namespace common\modules\tenant\models;

use app\modules\tenant\models\SmsServer;
use app\modules\tenant\models\TblSmsServer;
use app\modules\tenant\models\TblTenant;
use Yii;
use common\modules\tenant\models\Tenant;

/**
 * This is the model class for table "{{%tbl_tenant_has_sms}}".
 *
 * @property string $id
 * @property integer $server_id
 * @property string $tenant_id
 * @property string $login
 * @property string $password
 * @property integer $active
 * @property string $sign
 * @property string $city_id
 * @property integer $default
 *
 * @property TblSmsServer $server
 * @property TblTenant $tenant
 */
class TenantHasSms extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_has_sms}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'password', 'active'], 'required'],
            [['server_id', 'tenant_id', 'active', 'city_id'], 'integer'],
            [['login', 'password', 'sign'], 'string', 'max' => 100],
            [['default'], 'in', 'range' => ['0','1']],
        ];
    }

    public function subRules()
    {
        $rules = [
            [['login', 'password', 'sign'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
        return array_merge(parent::rules(), $rules);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => Yii::t('app', 'ID'),
            'server_id' => Yii::t('setting', 'Server'),
            'tenant_id' => Yii::t('app', 'Tenant ID'),
            'login'     => Yii::t('setting', 'Login'),
            'password'  => Yii::t('app', 'Password'),
            'active'    => Yii::t('setting', 'Active'),
            'sign'      => Yii::t('setting', 'Sign'),
            'city_id'   => Yii::t('app', 'City ID'),
            'default'   => Yii::t('app', 'Use default'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServer()
    {
        return $this->hasOne(SmsServer::className(), ['server_id' => 'server_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(\common\modules\tenant\models\Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(\common\modules\city\models\City::className(), ['city_id' => 'city_id']);
    }

    public function behaviors()
    {
        return [
            'common' => [
                'class' => \common\components\behaviors\ActiveRecordBehavior::className()
            ]
        ];
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if($this->default == 1) {
                $this->deleteOldDefault($insert);
            }

            return true;
        } else {
            return false;
        }
    }


    private function deleteOldDefault($insert)
    {
        if ($insert || $this->oldAttributes['default'] != $this->default) {

            $old = self::find()
                ->where([
                    'tenant_id' => $this->tenant_id,
                    'default' => 1
                ])
                ->one();

            if (!empty($old)) {
                $old->default = 0;
                return $old->save();
            }
        }

        return false;
    }

}
