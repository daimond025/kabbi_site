<?php

namespace common\modules\tenant\models;

use app\modules\tenant\models\User;
use Exception;
use frontend\modules\car\models\Car;
use Yii;
use common\modules\city\models\City;
use frontend\modules\car\models\CarOption;
use yii\base\InvalidParamException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tbl_tenant_has_car_option".
 *
 * @property integer $tenant_id
 * @property integer $option_id
 * @property string $value
 * @property integer $city_id
 * @property integer $is_active
 *
 * @property CarOption $option
 * @property Tenant $tenant
 */
class TenantHasCarOption extends ActiveRecord
{
    /**
     * @var array Car option list
     */
    public $option_list;

    /**
     * @var array Tenant option list
     */
    public $tenant_options;

    /**
     * @var array Tenant option list
     */
    public $tenant_options_active;

    public static function tableName()
    {
        return '{{%tenant_has_car_option}}';
    }

    public function rules()
    {
        return [
            [['value'], 'required'],
            [['tenant_id', 'option_id', 'value', 'city_id', 'is_active'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'tenant_id' => Yii::t('tenant', 'Tenant ID'),
            'option_id' => Yii::t('tenant', 'Option ID'),
            'value'     => Yii::t('tenant', 'Value'),
            'city_id'   => Yii::t('tenant', 'City ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOption()
    {
        return $this->hasOne(CarOption::className(), ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantHasCity()
    {
        return $this->hasMany(TenantHasCity::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * Allow to save multiple rows
     * @param array $arCarOption
     * @param integer $tenant_id
     * @param integer $city_id
     * @return integer number of rows affected by the execution.
     * @throws Exception execution failed
     */
    public static function manySave(array $arCarOption, $tenant_id, $city_id)
    {
        $insertValue = [];
        $connection = app()->db;

        //Удаляем все записи
        self::deleteAll([
            'tenant_id' => $tenant_id,
            'city_id'   => $city_id,
        ]);
        foreach ($arCarOption as $option_id => $item) {

            $insertValue[] = [$tenant_id, $option_id, $item['value'], $city_id, $item['is_active']];
        }

        return $connection->createCommand()->batchInsert(self::tableName(),
            ['tenant_id', 'option_id', 'value', 'city_id', 'is_active'], $insertValue)->execute();
    }

    /**
     * Refresh car raiting
     * @param int $cityId
     */
    public static function refreshCarRating($cityId)
    {
        $userCities = User::getCurUserCityIds();

        if (!in_array($cityId, $userCities)) {
            throw new InvalidParamException('The user does not have a city with id: ' . $cityId);
        }

        $tenantId = user()->tenant_id;
        $activeOptions = self::find()
            ->where([
                'tenant_id' => $tenantId,
                'city_id'   => $cityId,
                'is_active' => 1,
            ])
            ->indexBy('option_id')
            ->all();

        $cars = Car::find()
            ->where(['tenant_id' => $tenantId, 'city_id' => $cityId])
            ->with('carHasOptions')
            ->all();

        if (is_array($cars)) {
            foreach ($cars as $car) {
                $rating = 0;
                foreach ($car->carHasOptions as $carOption) {
                    if (array_key_exists($carOption->option_id, $activeOptions)) {
                        $rating += $activeOptions[$carOption->option_id]->value;
                    }
                }
                $car->raiting = $rating;
                $car->save(false, ['raiting']);
            }
        }
    }
}
