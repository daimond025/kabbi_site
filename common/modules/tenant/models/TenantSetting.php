<?php

namespace common\modules\tenant\models;

use common\helpers\CacheHelper;
use common\modules\employee\models\position\Position;
use common\modules\city\models\City;
use yii\caching\TagDependency;

/**
 * This is the model class for table "{{%tenant_setting}}".
 *
 * @property string   $setting_id
 * @property string   $tenant_id
 * @property string   $name
 * @property string   $value
 * @property string   $type
 * @property integer  $city_id
 * @property integer  $position_id
 *
 * @property Tenant   $tenant
 * @property City     $city
 * @property Position $position
 */
class TenantSetting extends \yii\db\ActiveRecord
{
    const CACHE_TAG = 'tenant_setting';

    const GOOGLE_PLAY_WORKER_APP_LINK = 'GOOGLE_PLAY_WORKER_APP_LINK';
    const SETTING_WORKER_REVIEW_COUNT = 'WORKER_REVIEW_COUNT';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_setting}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'name'], 'required'],
            [['tenant_id', 'city_id', 'position_id'], 'integer'],
            [['name', 'value', 'type'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'setting_id'  => 'Setting ID',
            'tenant_id'   => 'Tenant ID',
            'name'        => 'Name',
            'value'       => 'Value',
            'type'        => 'Type',
            'city_id'     => 'City ID',
            'position_id' => 'Position ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantHasCity()
    {
        return $this->hasMany(TenantHasCity::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefaultSetting()
    {
        return $this->hasOne(DefaultSettings::className(), ['name' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }

    /**
     * Getting cache key
     *
     * @param int    $tenantId
     * @param int    $cityId
     * @param int    $positionId
     * @param string $setting
     *
     * @return string
     */
    protected static function getCacheKey($tenantId, $cityId, $positionId, $setting)
    {
        return implode('_', ['setting', $tenantId, (string)$cityId, (string)$positionId, $setting]);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        app()->cache->delete(static::getCacheKey($this->tenant_id, $this->city_id, $this->position_id, $this->name));

        parent::afterDelete();
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        app()->cache->delete(static::getCacheKey($this->tenant_id, $this->city_id, $this->position_id, $this->name));

        TagDependency::invalidate(app()->cache, 'currency');

        TagDependency::invalidate(app()->cache, self::CACHE_TAG);

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * Getting setting value.
     *
     * @param int    $tenantId
     * @param string $setting
     * @param int    $cityId
     * @param int    $positionId
     *
     * @return string
     */
    public static function getSettingValue($tenantId, $setting, $cityId = null, $positionId = null)
    {

        $cacheKey = static::getCacheKey($tenantId, $cityId, $positionId, $setting);

        return CacheHelper::getFromCache($cacheKey, function () use ($tenantId, $setting, $cityId, $positionId) {

            return self::getSetting($tenantId, $setting, $cityId, $positionId);

        }, 3600, new TagDependency(['tags' => [self::CACHE_TAG, DefaultSettings::CACHE_TAG]]));
    }

    /**
     * Getting setting value
     *
     * @param int    $tenantId
     * @param string $setting
     * @param int    $cityId
     * @param int    $positionId
     *
     * @return false|null|string
     */
    private static function getSetting($tenantId, $setting, $cityId = null, $positionId = null)
    {
        return self::find()
            ->where([
                'tenant_id' => $tenantId,
                'name'      => $setting,
            ])
            ->andFilterWhere([
                'city_id'     => $cityId,
                'position_id' => $positionId,
            ])
            ->select('value')
            ->scalar();
    }

    /**
     * @param string $name
     * @param int    $tenantId
     * @param int    $cityId
     * @param int    $positionId
     * @param string $value
     *
     * @return bool
     */
    public static function updateSettingValue($name, $tenantId, $cityId = null, $positionId = null, $value = null)
    {
        $setting = self::find()
            ->where([
                'name'      => $name,
                'tenant_id' => $tenantId,
            ])
            ->andFilterWhere([
                'city_id'     => $cityId,
                'position_id' => $positionId,
            ])->one();

        $setting->value = $value;

        return $setting->save(false, ['value']);
    }

    /**
     * Getting cached tenant settings
     *
     * @param integer $tenantId
     * @param string  $type
     * @param integer $cityId
     * @param integer $positionId
     *
     * @return array
     */
    public static function getCachedSettings($tenantId, $type, $cityId = null, $positionId = null)
    {
        $cacheKey = self::getCacheKey($tenantId, $cityId, $positionId, "type:{$type}");

        $settings = app()->cache->get($cacheKey);
        if ($settings === false) {
            self::saveSettingsToCache($tenantId, $type, $cityId, $positionId);
            $settings = app()->cache->get($cacheKey);
        }

        return $settings;
    }

    /**
     * Update cached tenant settings
     *
     * @param int    $tenantId
     * @param string $type
     * @param int    $cityId
     * @param int    $positionId
     */
    public static function updateCachedSettings($tenantId, $type, $cityId = null, $positionId = null)
    {
        $cacheKey = self::getCacheKey($tenantId, $cityId, $positionId, "type:{$type}");
        app()->cache->delete($cacheKey);

        self::saveSettingsToCache($tenantId, $type, $cityId, $positionId);
    }

    /**
     * Save tenant settings to cache
     *
     * @param int    $tenantId
     * @param string $type
     * @param int    $cityId
     * @param int    $positionId
     *
     * @return bool
     */
    public static function saveSettingsToCache($tenantId, $type, $cityId, $positionId)
    {
        $settings = self::find()
            ->where([
                'tenant_id' => $tenantId,
                'type'      => $type,
            ])
            ->andFilterWhere([
                'city_id'     => $cityId,
                'position_id' => $positionId,
            ])->all();

        $cacheKey = self::getCacheKey($tenantId, $cityId, $positionId, "type:{$type}");

        return app()->cache->set($cacheKey, $settings, 3600,
            new TagDependency(['tags' => [self::CACHE_TAG, DefaultSettings::CACHE_TAG]]));
    }

    /**
     * @return string
     */
    public static function generateApiKey()
    {
        return (string)mt_rand(1000000000, 9999999999);
    }

    /**
     * @return string
     */
    public static function generateClientApiKey()
    {
        $defaultClientApiKey = DefaultSettings::getValue(DefaultSettings::SETTING_CLIENT_API_KEY);

        return empty($defaultClientApiKey) ? self::generateApiKey() : $defaultClientApiKey;
    }

    /**
     * @return string
     */
    public static function generateWorkerApiKey()
    {
        $defaultWorkerApiKey = DefaultSettings::getValue(DefaultSettings::SETTING_WORKER_API_KEY);

        return empty($defaultWorkerApiKey) ? self::generateApiKey() : $defaultWorkerApiKey;
    }

}
