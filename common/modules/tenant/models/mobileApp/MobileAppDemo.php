<?php

namespace common\modules\tenant\models\mobileApp;

use yii\db\ActiveRecord;

/**
 * Class MobileAppDemo
 *
 * @property int $app_id
 * @property string $name
 * @property int $sort
 * @property int $active
 * @property string $api_key
 * @property string $push_key_android
 * @property string $push_key_ios
 * @property string $link_android
 * @property string $link_ios
 * @property string $page_info
 * @property string $confidential
 * @property integer $city_autocompletion
 *
 * @property bool $isActive
 * @property bool $isCityAutocompletion
 *
 * @package common\modules\tenant\mobileApp
 */
class MobileAppDemo extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%mobile_app_demo}}';
    }

    public function rules()
    {
        return [
            [['tenant_id', 'name', 'active', 'api_key'], 'required'],
            [['name'], 'string', 'max' => 100],
            [['api_key', 'link_android', 'link_ios'], 'string', 'max' => 250],
            [['sort'], 'number', 'min' => 1, 'max' => 999999],
            [['active', 'city_autocompletion'], 'in', 'range' => [0, 1]],
            [['push_key_android', 'push_key_ios', 'page_info', 'confidential', 'cities'], 'safe'],
        ];
    }

    /**
     * @return bool
     */
    public function getIsActive()
    {
        return $this->active === 1;
    }

    public function setIsActive($value)
    {
        $this->active = (int)$value;
    }

    /**
     * @return bool
     */
    public function getIsCityAutocompletion()
    {
        return $this->city_autocompletion === 1;
    }
}