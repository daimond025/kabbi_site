<?php

namespace common\modules\tenant\models\mobileApp;

use yii\db\ActiveRecord;

/**
 * Class MobileApp
 *
 * @property int     $app_id
 * @property int     $tenant_id
 * @property string  $name
 * @property int     $sort
 * @property int     $active
 * @property string  $api_key
 * @property string  $push_key_android
 * @property string  $push_key_ios
 * @property string  $link_android
 * @property string  $link_ios
 * @property string  $page_info
 * @property string  $confidential
 * @property integer $city_autocompletion
 * @property integer $demo
 *
 * @property bool    $isActive
 * @property bool    $isDemo
 * @property bool    $isCityAutocompletion
 *
 * @package common\modules\tenant\mobileApp
 */
class MobileApp extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%mobile_app}}';
    }

    public function rules()
    {
        return [
            [['tenant_id', 'name', 'active', 'api_key'], 'required'],
            [['name'], 'string', 'max' => 100],
            [['api_key', 'link_android', 'link_ios'], 'string', 'max' => 250],
            [['sort'], 'number', 'min' => 1, 'max' => 999999],
            [['active', 'city_autocompletion', 'demo'], 'in', 'range' => [0, 1]],
            [['push_key_android', 'push_key_ios', 'page_info', 'confidential', 'cities'], 'safe'],
        ];
    }

    /**
     * @return bool
     */
    public function getIsActive()
    {
        return $this->active === 1;
    }

    public function setIsActive($value)
    {
        $this->active = (int)$value;
    }


    /**
     * @return bool
     */
    public function getIsDemo()
    {
        return $this->demo === 1;
    }

    public function setIsDemo($value)
    {
        $this->demo = (int)$value;
    }


    /**
     * @return bool
     */
    public function getIsCityAutocompletion()
    {
        return $this->city_autocompletion === 1;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHasCity()
    {
        return $this->hasMany(MobileAppHasCity::className(), ['app_id' => 'app_id']);
    }
}