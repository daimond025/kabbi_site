<?php

namespace common\modules\tenant\models;

use common\modules\employee\models\position\Position;
use common\modules\order\models\OrderStatus;
use common\services\OrderStatusService;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%default_client_status_event}}".
 *
 * @property integer     $event_id
 * @property integer     $status_id
 * @property string      $group
 * @property string      $notice
 * @property integer     $position_id
 *
 * @property OrderStatus $status
 * @property Position    $position
 */
class DefaultClientStatusEvent extends \yii\db\ActiveRecord
{
    const GROUP_CALL = 'CALL';
    const GROUP_WEB = 'WEB';
    const GROUP_APP = 'APP';
    const GROUP_BORDER = 'BORDER';

    const NOTICE_SMS = 'sms';
    const NOTICE_AUTOCALL = 'autocall';
    const NOTICE_PUSH = 'push';
    const NOTICE_NOTHING = 'nothing';

    const AVAILABLE_STATUSES = [
        OrderStatus::STATUS_NEW,
        OrderStatus::STATUS_GET_WORKER,
        OrderStatus::STATUS_WORKER_WAITING,
        OrderStatus::STATUS_WORKER_LATE,
        OrderStatus::STATUS_EXECUTING,
        OrderStatus::STATUS_COMPLETED_PAID,
        OrderStatus::STATUS_REJECTED,
        OrderStatus::STATUS_PRE,
        OrderStatus::STATUS_PRE_GET_WORKER,
        OrderStatus::STATUS_PRE_REFUSE_WORKER,
        OrderStatus::STATUS_EXECUTION_PRE,
        OrderStatus::STATUS_OVERDUE,
        OrderStatus::STATUS_NO_CARS_BY_TIMER,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%default_client_status_event}}';
    }

    /**
     * @inheritdoc
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_id', 'group', 'notice', 'position_id'], 'required'],
            [['status_id', 'position_id'], 'integer'],
            [['group', 'notice'], 'string'],
            [
                'position_id',
                'unique',
                'targetAttribute' => ['status_id', 'group', 'position_id'],
                'message'         => 'Уведомление для данной должности, статуса и группы уже создано',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'event_id'    => 'Event ID',
            'status_id'   => 'Status ID',
            'group'       => 'Group',
            'notice'      => 'Notice',
            'position_id' => 'Position ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['status_id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function beforeSave($insert)
    {
        $oldStatusId   = $this->getOldAttribute('status_id');
        $oldGroup      = $this->getOldAttribute('group');
        $oldPositionId = $this->getOldAttribute('position_id');

        if ($this->status_id != $oldStatusId || $this->group != $oldGroup || $this->position_id != $oldPositionId) {
            static::removeClientStatusEventsFromAllTenants($oldStatusId, $oldGroup, $oldPositionId);
        }

        static::addMissingClientStatusEventsToAllTenants(
            $this->status_id, $this->group, $this->position_id, $this->notice);

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function beforeDelete()
    {
        static::removeClientStatusEventsFromAllTenants($this->status_id, $this->group, $this->position_id);

        return parent::beforeDelete();
    }


    /**
     * Add missing client status events from default status event to all tenants
     *
     * @param int    $statusId
     * @param string $group
     * @param int    $positionId
     * @param string $notice
     *
     * @throws \yii\db\Exception
     */
    public static function addMissingClientStatusEventsToAllTenants($statusId, $group, $positionId, $notice)
    {
        $sql = <<<SQL
INSERT `tbl_client_status_event`
        (`tenant_id`, `city_id`, `status_id`, `group`, `notice`, `position_id`)
SELECT `c`.`tenant_id`, `c`.`city_id`, :statusId `status_id`, :group `group`, :notice `notice`, :positionId `position_id`
FROM `tbl_tenant_has_city` AS `c`
LEFT JOIN `tbl_client_status_event` AS `t` ON `t`.`tenant_id` = `c`.`tenant_id` AND `t`.`city_id` = `c`.`city_id` AND `t`.`status_id` = :statusId AND `t`.`group` = :group AND `t`.`position_id` = :positionId
WHERE `t`.`event_id` IS NULL
SQL;

        \Yii::$app->getDb()->createCommand($sql, [
            'statusId'   => $statusId,
            'group'      => $group,
            'positionId' => $positionId,
            'notice'     => $notice,
        ])->execute();
    }

    /**
     * Add missing client status events from default client status event to tenant
     *
     * @param int $tenantId
     * @param int $cityId
     * @param int $positionId
     *
     * @throws \yii\db\Exception
     */
    public static function addAllMissingClientStatusEventsToTenant($tenantId, $cityId, $positionId)
    {
        $sql = <<<SQL
INSERT `tbl_client_status_event`
        (`tenant_id`, `city_id`, `status_id`, `position_id`, `group`, `notice`)
SELECT :tenantId `tenant_id`, :cityId `city_id`, `d`.`status_id`, `d`.`position_id`, `d`.`group`, `d`.`notice`
FROM `tbl_default_client_status_event` AS `d`
LEFT JOIN `tbl_client_status_event` AS `t` ON `t`.`tenant_id` = :tenantId AND `t`.`city_id` = :cityId AND `t`.`status_id` = `d`.`status_id` AND `t`.`group` = `d`.`group` AND `t`.`position_id` = `d`.`position_id`
WHERE `d`.`position_id` = :positionId AND `t`.`event_id` IS NULL
SQL;

        \Yii::$app->getDb()->createCommand($sql, [
            'tenantId'   => $tenantId,
            'cityId'     => $cityId,
            'positionId' => $positionId,
        ])->execute();
    }

    /**
     * Remove client status events from all tenants
     *
     * @param int    $statusId
     * @param string $group
     * @param int    $positionId
     *
     * @throws \yii\db\Exception
     */
    public static function removeClientStatusEventsFromAllTenants($statusId, $group, $positionId)
    {
        $sql = <<<SQL
DELETE
FROM `tbl_client_status_event`
WHERE `status_id` = :statusId AND `group` = :group AND `position_id` = :positionId
SQL;


        \Yii::$app->getDb()->createCommand($sql, [
            'statusId'   => $statusId,
            'group'      => $group,
            'positionId' => $positionId,
        ])->execute();
    }

    /**
     * @return array
     */
    public static function getGroupMap()
    {
        return [
            self::GROUP_CALL   => t('status_event', 'Order is created through a call to the control room'),
            self::GROUP_APP    => t('status_event', 'Order is created through the application'),
            self::GROUP_WEB    => t('status_event', 'Order is created through the site'),
            self::GROUP_BORDER => t('status_event', 'Order is created on the street'),
        ];
    }


    /**
     * @return array
     */
    public static function getNoticeMap()
    {
        return [
            self::NOTICE_SMS      => 'Sms',
            self::NOTICE_AUTOCALL => t('status_event', 'Autocall'),
            self::NOTICE_PUSH     => 'PUSH',
            self::NOTICE_NOTHING  => t('status_event', 'Nothing'),
        ];
    }


    /**
     * Getting status map
     *
     * @param int $positionId
     *
     * @return mixed
     */
    public static function getStatusMap($positionId = null)
    {
        $statuses = OrderStatus::find()
            ->select(['status_id', 'name'])
            ->where(['status_id' => self::AVAILABLE_STATUSES])
            ->asArray()
            ->all();

        return empty($statuses) ? [] : ArrayHelper::map($statuses, 'status_id', function ($status) use ($positionId) {
            return OrderStatusService::translate($status['status_id'], $positionId);
        });
    }

}
