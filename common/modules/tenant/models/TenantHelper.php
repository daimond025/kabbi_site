<?php

namespace common\modules\tenant\models;

use common\modules\tenant\models\Tenant;
use Yii;
use yii\helpers\ArrayHelper;
use frontend\modules\tenant\models\UserHasCity;
use app\modules\parking\models\Parking;

/**
 * This is the model class for table "{{%tenant_helper}}".
 *
 * @property integer $helper_id
 * @property integer $tenant_id
 * @property integer $info
 * @property integer $parking
 * @property integer $driver_tariff
 * @property integer $client_tariff
 * @property integer $car
 * @property integer $driver
 * @property integer $city
 *
 * @property Tenant $tenant
 */
class TenantHelper extends \yii\db\ActiveRecord
{
    //Константы, соответствующие полям в БД
    const PARKING_FILL = 'parking';
    const WORKER_TARIFF_FILL = 'worker_tariff';
    const CLIENT_TARIFF_FILL = 'client_tariff';
    const CAR_FILL = 'car';
    const WORKER_FILL = 'worker';
    const CITY_FILL = 'city';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_helper}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id'], 'required'],
            [['tenant_id', 'info', 'parking', 'worker_tariff', 'client_tariff', 'car', 'worker', 'city'], 'integer'],
            [['tenant_id', 'info', 'parking', 'worker_tariff', 'client_tariff', 'car', 'worker', 'city'], 'default', 'value' => 0]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'helper_id' => 'Helper ID',
            'tenant_id' => 'Tenant ID',
            'info' => 'Info',
            'parking' => 'Parking',
            'worker_tariff' => 'Driver Tariff',
            'client_tariff' => 'Client Tariff',
            'car' => 'Car',
            'driver' => 'Driver',
            'city' => 'City',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * Значения пунктов хелпера
     * @return array
     */
    public static function getHelperItems()
    {
        $tenant_id = user()->tenant_id;
        $helper_items = self::findOne(['tenant_id' => $tenant_id]);

        if (empty($helper_items)) {
            $helper_items = new self(['tenant_id' => $tenant_id]);
            $helper_items->save();
        }

        return ArrayHelper::toArray($helper_items);
    }

    /**
     * Проверка необходимости хелпера
     * @return bool
     */
    public static function checkNeedsHelper()
    {
        return !self::find()->
                where([
                    'tenant_id' => user()->tenant_id,
                    'info' => 1,
                    'parking' => 1,
                    'worker_tariff' => 1,
                    'client_tariff' => 1,
                    'car' => 1,
                    'worker' => 1,
                    'city' => 1,
                ])->
                exists();
    }

    /**
     * Отметить пункт хелпера
     * @param string $item Название поля хелпера
     * @return bool|null
     */
    public static function fillHelpItem($item, $tenant_id = null)
    {
        if (is_null($tenant_id) && !Yii::$app->user->isGuest) {
            $tenant_id = user()->tenant_id;
        }

        if (!empty($tenant_id)) {
            $filled = self::find()->where(['tenant_id' => $tenant_id, $item => 1])->exists();

            if(!$filled) {
                if($item == self::PARKING_FILL) {
                    $base_parking_cnt = Parking::find()->where(['tenant_id' => $tenant_id, 'type' => 'basePolygon'])->count();

                    if($base_parking_cnt == 0)
                        return false;
                }

                return app()->db->createCommand()->update(self::tableName(), [$item => 1], 'tenant_id = ' . $tenant_id)->execute();
            }
        }
        return null;
    }
    
    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert)) {
            if($insert) {
                $this->info = 1;
            }

            return true;
        }

        return false;
    }

}
