<?php

namespace common\modules\tenant\models;

use common\modules\employee\models\position\Position;
use common\modules\tenant\components\NotificationsService;
use common\modules\tenant\components\NotificationTemplatesService;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "tbl_default_worker_push_notifications".
 *
 * @property integer $push_id
 * @property string  $text
 * @property string  $type
 * @property string  $params
 * @property integer $position_id
 */
class DefaultWorkerPushNotification extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%default_worker_push_notifications}}';
    }

    /**
     * @inheritdoc
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'type'], 'required'],
            [['params', 'type'], 'string'],
            [['text'], 'string', 'max' => 255],
            [['position_id'], 'integer'],
            [
                'position_id',
                'unique',
                'targetAttribute' => ['type', 'position_id'],
                'message'         => 'Шаблон для данной должности и типа уже создан',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'push_id'     => 'Push ID',
            'text'        => 'Текст',
            'type'        => 'Тип',
            'position_id' => 'Профессия',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(Position::className(), ['position_id' => 'position_id']);
    }

    /**
     * Getting default push notification
     *
     * @param string $type
     * @param int    $positionId
     *
     * @return null|array|DefaultWorkerPushNotification
     */
    public static function getPushNotification($type, $positionId)
    {
        return static::find()
            ->select(['text', 'params'])
            ->where([
                'type'        => $type,
                'position_id' => $positionId,
            ])
            ->one();
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function beforeSave($insert)
    {
        $oldType       = $this->getOldAttribute('type');
        $oldPositionId = $this->getOldAttribute('position_id');

        if ($this->type != $oldType || $this->position_id != $oldPositionId) {
            NotificationTemplatesService::removeWorkerPushNotificationsFromAllTenants($oldType, $oldPositionId);

        }

        NotificationTemplatesService::addMissingWorkerPushNotificationsToAllTenants($this->type, $this->position_id, $this->text);

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     * @throws \yii\db\Exception
     */
    public function beforeDelete()
    {
        NotificationTemplatesService::removeWorkerPushNotificationsFromAllTenants($this->type, $this->position_id);

        return parent::beforeDelete();
    }
}
