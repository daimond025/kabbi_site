<?php

namespace common\modules\tenant\components;

use yii\base\Object;
use frontend\modules\tenant\models\Tenant;

/**
 * Class TenantService
 * @package common\modules\tenant\components
 */
class TenantService extends Object
{
    /**
     * @inheritdoc
     */
    public function getUrlByDomain($domain)
    {
        return Tenant::getTenantUrl($domain);
    }

}