<?php

namespace common\modules\tenant\components;

use yii\base\Object;

/**
 * Class TenantService
 * @package common\modules\tenant\components
 */
class NotificationTemplatesService extends Object
{

    protected static function addMissingNotificationsToAllTenants($sql, $type, $positionId, $text)
    {
        \Yii::$app->getDb()->createCommand($sql, [
            'type'       => $type,
            'positionId' => $positionId,
            'text'       => $text,
        ])->execute();
    }

    protected static function addAllMissingNotificationToTenant($sql, $tenantId, $cityId, $positionId)
    {
        \Yii::$app->getDb()->createCommand($sql, [
            'tenantId'   => $tenantId,
            'cityId'     => $cityId,
            'positionId' => $positionId,
        ])->execute();
    }

    protected static function removeNotificationsFromAllTenants($sql, $type, $positionId)
    {
        \Yii::$app->getDb()->createCommand($sql, [
            'type'       => $type,
            'positionId' => $positionId,
        ])->execute();
    }




    public static function addMissingClientSmsNotificationsToAllTenants($type, $positionId, $text)
    {
        $sql = <<<SQL
INSERT `tbl_sms_template`
        (`tenant_id`, `city_id`, `type`, `text`, `position_id`)
SELECT `c`.`tenant_id`, `c`.`city_id`, :type `type`, :text `text`, :positionId `position_id`
FROM `tbl_tenant_has_city` AS `c`
LEFT JOIN `tbl_sms_template` AS `t` ON `t`.`tenant_id` = `c`.`tenant_id` AND `t`.`city_id` = `c`.`city_id` AND `t`.`type` = :type AND `t`.`position_id` = :positionId
WHERE `t`.`template_id` IS NULL
SQL;

        self::addMissingNotificationsToAllTenants($sql, $type, $positionId, $text);
    }


    public static function addMissingClientPushNotificationsToAllTenants($type, $positionId, $text)
    {
        $sql = <<<SQL
INSERT `tbl_client_push_notifications`
        (`tenant_id`, `city_id`, `type`, `text`, `position_id`)
SELECT `c`.`tenant_id`, `c`.`city_id`, :type `type`, :text `text`, :positionId `position_id`
FROM `tbl_tenant_has_city` AS `c`
LEFT JOIN `tbl_client_push_notifications` AS `t` ON `t`.`tenant_id` = `c`.`tenant_id` AND `t`.`city_id` = `c`.`city_id` AND `t`.`type` = :type AND `t`.`position_id` = :positionId
WHERE `t`.`push_id` IS NULL
SQL;

        self::addMissingNotificationsToAllTenants($sql, $type, $positionId, $text);
    }


    public static function addMissingWorkerPushNotificationsToAllTenants($type, $positionId, $text)
    {
        $sql = <<<SQL
INSERT `tbl_worker_push_notifications`
        (`tenant_id`, `city_id`, `type`, `text`, `position_id`)
SELECT `c`.`tenant_id`, `c`.`city_id`, :type `type`, :text `text`, :positionId `position_id`
FROM `tbl_tenant_has_city` AS `c`
LEFT JOIN `tbl_worker_push_notifications` AS `t` ON `t`.`tenant_id` = `c`.`tenant_id` AND `t`.`city_id` = `c`.`city_id` AND `t`.`type` = :type AND `t`.`position_id` = :positionId
WHERE `t`.`push_id` IS NULL
SQL;

        self::addMissingNotificationsToAllTenants($sql, $type, $positionId, $text);
    }


    public static function addAllMissingClientSmsNotificationsToTenant($tenantId, $cityId, $positionId)
    {
        $sql = <<<SQL
INSERT `tbl_sms_template`
        (`tenant_id`, `city_id`, `position_id`, `type`, `text`)
SELECT :tenantId `tenant_id`, :cityId `city_id`, `d`.`position_id`, `d`.`type`, `d`.`text`
FROM `tbl_default_sms_template` AS `d`
LEFT JOIN `tbl_sms_template` AS `t` ON `t`.`tenant_id` = :tenantId AND `t`.`city_id` = :cityId AND `t`.`type` = `d`.`type` AND `t`.`position_id` = `d`.`position_id`
WHERE `d`.`position_id` = :positionId AND `t`.`template_id` IS NULL
SQL;

        self::addAllMissingNotificationToTenant($sql, $tenantId, $cityId, $positionId);
    }


    public static function addAllMissingRecipientSmsNotificationsToTenant($tenantId, $cityId, $positionId)
    {
        $sql = <<<SQL
INSERT `tbl_recipient_sms_template`
        (`tenant_id`, `city_id`, `position_id`, `type`, `text`)
SELECT :tenantId `tenant_id`, :cityId `city_id`, `d`.`position_id`, `d`.`type`, `d`.`text`
FROM `tbl_default_sms_template` AS `d`
LEFT JOIN `tbl_sms_template` AS `t` ON `t`.`tenant_id` = :tenantId AND `t`.`city_id` = :cityId AND `t`.`type` = `d`.`type` AND `t`.`position_id` = `d`.`position_id`
WHERE `d`.`position_id` = :positionId AND `t`.`template_id` IS NULL
SQL;

        self::addAllMissingNotificationToTenant($sql, $tenantId, $cityId, $positionId);
    }


    public static function addAllMissingClientPushNotificationsToTenant($tenantId, $cityId, $positionId)
    {
        $sql = <<<SQL
INSERT `tbl_client_push_notifications`
        (`tenant_id`, `city_id`, `position_id`, `type`, `text`)
SELECT :tenantId `tenant_id`, :cityId `city_id`, `d`.`position_id`, `d`.`type`, `d`.`text`
FROM `tbl_default_client_push_notifications` AS `d`
LEFT JOIN `tbl_client_push_notifications` AS `t` ON `t`.`tenant_id` = :tenantId AND `t`.`city_id` = :cityId AND `t`.`type` = `d`.`type` AND `t`.`position_id` = `d`.`position_id`
WHERE `d`.`position_id` = :positionId AND `t`.`push_id` IS NULL
SQL;

        self::addAllMissingNotificationToTenant($sql, $tenantId, $cityId, $positionId);
    }


    public static function addAllMissingWorkerPushNotificationsToTenant($tenantId, $cityId, $positionId)
    {
        $sql = <<<SQL
INSERT `tbl_worker_push_notifications`
        (`tenant_id`, `city_id`, `position_id`, `type`, `text`)
SELECT :tenantId `tenant_id`, :cityId `city_id`, `d`.`position_id`, `d`.`type`, `d`.`text`
FROM `tbl_default_worker_push_notifications` AS `d`
LEFT JOIN `tbl_worker_push_notifications` AS `t` ON `t`.`tenant_id` = :tenantId AND `t`.`city_id` = :cityId AND `t`.`type` = `d`.`type` AND `t`.`position_id` = `d`.`position_id`
WHERE `d`.`position_id` = :positionId AND `t`.`push_id` IS NULL
SQL;

        self::addAllMissingNotificationToTenant($sql, $tenantId, $cityId, $positionId);
    }


    public static function removeClientSmsNotificationsFromAllTenants($type, $positionId)
    {
        $sql = <<<SQL
DELETE
FROM `tbl_sms_template`
WHERE `type` = :type AND `position_id` = :positionId
SQL;

        self::removeNotificationsFromAllTenants($sql, $type, $positionId);
    }


    public static function removeClientPushNotificationsFromAllTenants($type, $positionId)
    {
        $sql = <<<SQL
DELETE
FROM `tbl_client_push_notifications`
WHERE `type` = :type AND `position_id` = :positionId
SQL;

        self::removeNotificationsFromAllTenants($sql, $type, $positionId);
    }


    public static function removeWorkerPushNotificationsFromAllTenants($type, $positionId)
    {
        $sql = <<<SQL
DELETE
FROM `tbl_worker_push_notifications`
WHERE `type` = :type AND `position_id` = :positionId
SQL;

        self::removeNotificationsFromAllTenants($sql, $type, $positionId);
    }

}