<?php

namespace common\modules\tenant\exceptions;

use yii\base\Exception;

/**
 * Class InvalidSettingTypeException
 * @package common\modules\tenant\exceptions
 */
class InvalidSettingTypeException extends Exception
{

}