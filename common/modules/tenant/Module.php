<?php

namespace common\modules\tenant;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'common\modules\tenant\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
