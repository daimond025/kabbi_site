<?php

namespace common\modules\tenant\modules\tariff\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%demo_tariff}}".
 *
 * @property integer $id
 * @property integer $tariff_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Tariff  $tariff
 */
class DemoTariff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%demo_tariff}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id'], 'required'],
            [['tariff_id', 'created_at', 'updated_at'], 'integer'],
            [['tariff_id'], 'unique'],
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'tariff_id'  => Yii::t('app', 'Tariff ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(Tariff::className(), ['id' => 'tariff_id']);
    }

    /**
     * @inheritdoc
     * @return DemoTariffQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DemoTariffQuery(get_called_class());
    }
}
