<?php

namespace common\modules\tenant\modules\tariff\models;

/**
 * This is the ActiveQuery class for [[TariffPermissionHistory]].
 *
 * @see TariffPermissionHistory
 */
class TariffPermissionQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        $this->andWhere('[[active]]=1');

        return $this;
    }

    public function byPermissionId($permissionId)
    {
        $this->andWhere(['[[permission_id]]' => $permissionId]);

        return $this;
    }

    /**
     * @inheritdoc
     * @return TariffPermissionHistory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TariffPermissionHistory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}