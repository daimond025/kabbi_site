<?php

namespace common\modules\tenant\modules\tariff\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%discount_for_period}}".
 *
 * @property integer $id
 * @property integer $period
 * @property integer $percent
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 */
class DiscountForPeriod extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%discount_for_period}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['period', 'percent'], 'required'],
            [['created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            ['period', 'integer', 'min' => 1, 'max' => 999],
            ['percent', 'integer', 'min' => 0, 'max' => 99999],
            [['period'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('discount-for-period', 'ID'),
            'period'     => Yii::t('discount-for-period', 'Period'),
            'percent'    => Yii::t('discount-for-period', 'Percent'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert
            || (isset($changedAttributes['percent'])
                && $this->percent != $changedAttributes['percent'])
        ) {
            DiscountForPeriodHistory::changeDiscount($this->period, $this->percent);
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        DiscountForPeriodHistory::changeDiscount($this->period, 0);

        parent::afterDelete();
    }

    /**
     * @inheritdoc
     * @return DiscountForPeriodQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DiscountForPeriodQuery(get_called_class());
    }
}
