<?php

namespace common\modules\tenant\modules\tariff\models;

use common\components\behaviors\CacheInvalidateBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use common\modules\tenant\modules\tariff\components\behaviors\DatetimeBehavior;
use yii\caching\Cache;
use yii\caching\TagDependency;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%tenant_tariff}}".
 *
 * @property integer            $id
 * @property integer            $tenant_id
 * @property integer            $tariff_id
 * @property integer            $started_at
 * @property integer            $expiry_date
 * @property integer            $created_at
 * @property integer            $updated_at
 * @property integer            $created_by
 * @property integer            $updated_by
 *
 * @property TenantPermission[] $tenantPermissions
 * @property PaymentForTariff   $paymentForTariff
 * @property Tariff             $tariff
 * @property Tenant             $tenant
 */
class TenantTariff extends \yii\db\ActiveRecord
{
    const CACHE_TAG = 'tenant-tariffs';
    const CACHE_DEFAULT_DURATION = 6 * 3600;
    const CACHE_MIN_DURATION = 10 * 60;
    public $tname;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_tariff}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'tariff_id', 'started_at', 'expiry_date'], 'required'],
            [
                [
                    'tenant_id',
                    'tariff_id',
                    'created_at',
                    'updated_at',
                    'created_by',
                    'updated_by',
                ],
                'integer',
            ],
            [['startedAt', 'expiryDate'], 'date', 'format' => 'php:d.m.Y'],
            ['started_at', 'compare', 'compareAttribute' => 'expiry_date', 'operator' => '<'],
            [['started_at'], 'checkIntersection',],
        ];
    }

    /**
     * Validator which check tenant tariff intersection
     */
    public function checkIntersection($attribute, $params)
    {
        if (TenantTariff::find()
            ->alias('tt')
            ->active('tt')
            ->byTenantId($this->tenant_id)
            ->joinWith('tariff t', true)
            ->andWhere(['t.module_id' => $this->tariff->module_id])
            ->andWhere(['not', ['tt.id' => $this->id]])
            ->andWhere([
                'or',
                [
                    'and',
                    ['<=', 'tt.started_at', $this->started_at],
                    ['>=', 'tt.expiry_date', $this->started_at],
                ],
                [
                    'and',
                    ['>=', 'tt.started_at', $this->started_at],
                    ['<=', 'tt.started_at', $this->expiry_date],
                ],
            ])
            ->exists()
        ) {
            $this->addError(null, t('tenant-tariff', 'Active tariff already exists on this date period',
                ['attribute' => t('tenant-tariff', $this->attributeLabels()[$attribute])]));
        }
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
            [
                'class'   => DatetimeBehavior::className(),
                'options' => [
                    'startedAt'  => [
                        'field'  => 'started_at',
                        'format' => 'd.m.Y',
                    ],
                    'expiryDate' => [
                        'field'  => 'expiry_date',
                        'format' => 'd.m.Y',
                    ],
                ],
            ],
            [
                'class' => CacheInvalidateBehavior::className(),
                'tags'  => [
                    self::CACHE_TAG,
                    function ($model) {
                        return self::CACHE_TAG . ' id=' . $model->id;
                    },
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('tenant-tariff', 'ID'),
            'tenant_id'   => Yii::t('tenant-tariff', 'Tenant ID'),
            'tariff_id'   => Yii::t('tenant-tariff', 'Tariff ID'),
            'started_at'  => Yii::t('tenant-tariff', 'Started At'),
            'startedAt'   => Yii::t('tenant-tariff', 'Started At'),
            'expiry_date' => Yii::t('tenant-tariff', 'Expiry Date'),
            'expiryDate'  => Yii::t('tenant-tariff', 'Expiry Date'),
            'created_at'  => Yii::t('app', 'Created At'),
            'updated_at'  => Yii::t('app', 'Updated At'),
            'created_by'  => Yii::t('app', 'Created By'),
            'updated_by'  => Yii::t('app', 'Updated By'),
        ];
    }

    public static function getTenantClass()
    {
        $module = \Yii::$app->getModule('common-tenant/tariff');

        return $module->tenantClass;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantPermissions()
    {
        return $this->hasMany(TenantPermission::className(), ['tenant_tariff_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(Tariff::className(), ['id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(static::getTenantClass(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentForTariff()
    {
        return $this->hasOne(PaymentForTariff::className(), ['id' => 'payment_for_tariff_id']);
    }

    /**
     * @inheritdoc
     * @return TenantTariffQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TenantTariffQuery(get_called_class());
    }

    /**
     * Getting tenant tariffs
     *
     * @param integer $date
     * @param integer $tenantId
     * @param Cache   $cache
     *
     * @return array
     */
    public static function getTenantTariffs($date, $tenantId, $cache = null)
    {
        $date = strtotime(date('d.m.Y', $date));

        $cache    = isset($cache) ? $cache : Yii::$app->getCache();
        $cacheKey = 'tenant-tariffs #' . $tenantId . ' ' . date('d.m.Y', $date);

        $result = $cache->get($cacheKey);

        if ($result !== false) {
            return $result;
        } else {
            $result = Module::find()->select([
                'id',
                'code',
                'name',
                'description',
            ])->active()->indexBy('code')->asArray()->all();

            $tenantTariffs = ArrayHelper::map(static::find()
                ->alias('tt')
                ->innerJoinWith([
                    'tariff t',
                    'tariff.module m' => function ($query) {
                        /* @var $query ModuleQuery */
                        $query->active('m');
                    },
                ], true)
                ->joinWith([
                    'tenantPermissions tp',
                    'tenantPermissions.tariffPermission',
                    'tenantPermissions.tariffPermission.permission p',
                ], true)
                ->byTenantId($tenantId)
                ->byDate($date)
                ->active('tt')
                ->groupBy('m.code')
                ->orderBy(['m.code' => SORT_ASC, 'tt.started_at' => SORT_DESC, 'p.code' => SORT_ASC])
                ->asArray()
                ->all(),
                function ($model) {
                    return $model['tariff']['module']['code'];
                },
                function ($model) {
                    return [
                        'name'        => $model['tariff']['name'],
                        'description' => $model['tariff']['description'],
                        'started_at'  => $model['started_at'],
                        'expiry_date' => $model['expiry_date'],
                        'permissions' => ArrayHelper::map($model['tenantPermissions'],
                            function ($model) {
                                return $model['tariffPermission']['permission']['code'];
                            },
                            function ($model) {
                                return [
                                    'code'        => $model['tariffPermission']['permission']['code'],
                                    'name'        => $model['tariffPermission']['permission']['name'],
                                    'description' => $model['tariffPermission']['permission']['description'],
                                    'value'       => $model['value'],
                                ];
                            }
                        ),
                    ];
                });

            $defaultTariffs = ArrayHelper::map(Tariff::find()
                ->alias('t')
                ->innerJoinWith([
                    'module m' => function ($query) {
                        $query->active('m');
                    },
                    'tariffPermissions tp',
                    'tariffPermissions.permission p',
                ], true)
                ->byDefault(true)
                ->active('t')
                ->groupBy('m.code')
                ->orderBy(['m.code' => SORT_ASC, 'p.code' => SORT_ASC])
                ->asArray()
                ->all(),
                function ($model) {
                    return $model['module']['code'];
                },
                function ($model) {
                    return [
                        'name'        => $model['name'],
                        'description' => $model['description'],
                        'permissions' => ArrayHelper::map($model['tariffPermissions'],
                            function ($model) {
                                return $model['permission']['code'];
                            },
                            function ($model) {
                                return [
                                    'code'        => $model['permission']['code'],
                                    'name'        => $model['permission']['name'],
                                    'description' => $model['permission']['description'],
                                    'value'       => $model['value'],
                                ];
                            }),
                    ];
                });

            foreach ($defaultTariffs as $key => $value) {
                $result[$key]['tariff']      = $value;
                $result[$key]['permissions'] = $value['permissions'];
            }

            foreach ($tenantTariffs as $key => $value) {
                $result[$key]['tariff']      = $value;
                $result[$key]['permissions'] = $value['permissions'];
            }

            foreach ($result as $key => $value) {
                $nextTariff  = null;
                $expiry_date = isset($value['tariff']['expiry_date']) ? $value['tariff']['expiry_date'] : null;
                if (isset($value['tariff'])) {
                    $nextTariff = TenantTariff::find()
                        ->byModuleId($value['id'])
                        ->byTenantId($tenantId)
                        ->byDate(strtotime('+1 day', $expiry_date))
                        ->active()
                        ->one();
                }
                $result[$key]['isNextPaid']     = !empty($nextTariff);
                $result[$key]['nextExpiryDate'] = isset($nextTariff->expiry_date) ? $nextTariff->expiry_date : null;
            }

            $duration = min(strtotime('+1day', $date) - time(), self::CACHE_DEFAULT_DURATION);
            $cache->set($cacheKey, $result, $duration <= 0 ? self::CACHE_MIN_DURATION : $duration,
                new TagDependency([
                    'tags' => [
                        static::CACHE_TAG,
                        Module::CACHE_TAG,
                        Tariff::CACHE_TAG,
                        Permission::CACHE_TAG,
                        TariffPermission::CACHE_TAG,
                        TenantPermission::CACHE_TAG,
                    ],
                ]));

            return $result;
        }
    }

}
