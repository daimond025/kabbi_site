<?php

namespace common\modules\tenant\modules\tariff\models;

/**
 * This is the ActiveQuery class for [[TenantTariff]].
 *
 * @see TenantTariff
 */
class TenantTariffQuery extends \yii\db\ActiveQuery
{
    public function active($alias = null)
    {
        $field = $alias ? $alias . '.active' : 'active';
        $this->andWhere([$field => 1]);

        return $this;
    }

    public function byTenantId($tenantId)
    {
        $this->andWhere(['[[tenant_id]]' => $tenantId]);

        return $this;
    }

    public function byModuleId($moduleId)
    {
        $this->andWhere([
            'in',
            '[[tariff_id]]',
            Tariff::find()->select('id')->byModuleId($moduleId),
        ]);

        return $this;
    }

    public function byDate($date, $alias = null)
    {
        $alias = isset($alias) ? $alias . '.' : '';

        $this->andWhere(['<=', $alias . 'started_at', $date])
            ->andWhere(['>=', $alias . 'expiry_date', $date]);

        return $this;
    }

    public function expiredAt($date, $alias = null)
    {
        $alias = isset($alias) ? $alias . '.' : '';

        $this->andWhere(['=', $alias . 'expiry_date', $date]);

        return $this;
    }

    /**
     * @inheritdoc
     * @return TenantTariff[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TenantTariff|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}