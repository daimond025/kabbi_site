<?php

namespace common\modules\tenant\modules\tariff\models;

use common\components\behaviors\CacheInvalidateBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%permission}}".
 *
 * @property integer            $id
 * @property integer            $code
 * @property string             $name
 * @property string             $description
 * @property integer            $active
 * @property integer            $created_at
 * @property integer            $updated_at
 * @property integer            $created_by
 * @property integer            $updated_by
 *
 * @property TariffPermission[] $tariffPermissions
 */
class Permission extends \yii\db\ActiveRecord
{
    const CACHE_TAG = 'permissions';

    const CODE_WORKERS_ONLINE = 1;
    const CODE_USERS_ONLINE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%permission}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name'], 'required'],
            [['description'], 'string'],
            [['code', 'active', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 255],
            ['code', 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('permission', 'ID'),
            'code'        => Yii::t('permission', 'Code'),
            'name'        => Yii::t('permission', 'Name'),
            'description' => Yii::t('permission', 'Description'),
            'active'      => Yii::t('permission', 'Active'),
            'created_at'  => Yii::t('app', 'Created At'),
            'updated_at'  => Yii::t('app', 'Updated At'),
            'created_by'  => Yii::t('app', 'Created By'),
            'updated_by'  => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
            [
                'class' => CacheInvalidateBehavior::className(),
                'tags'  => [
                    self::CACHE_TAG,
                    function ($model) {
                        return self::CACHE_TAG . ' id=' . $model->id;
                    },
                ],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffPermissions()
    {
        return $this->hasMany(TariffPermission::className(), ['permission_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return PermissionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PermissionQuery(get_called_class());
    }
}
