<?php

namespace common\modules\tenant\modules\tariff\models;

/**
 * This is the ActiveQuery class for [[Tariff]].
 *
 * @see Tariff
 */
class TariffQuery extends \yii\db\ActiveQuery
{
    public function active($alias = null)
    {
        $field = $alias ? $alias . '.active' : 'active';
        $this->andWhere([$field => 1]);

        return $this;
    }

    public function byDefault($default)
    {
        $this->andWhere(['[[default]]' => $default ? 1 : 0]);

        return $this;
    }

    public function byDemo($demo, $alias = null)
    {
        $operator = empty($demo) ? 'not in' : 'in';
        $field    = isset($alias) ? $alias . '.id' : 'id';

        $this->andWhere([
            $operator,
            $field,
            DemoTariff::find()->select('tariff_id'),
        ]);

        return $this;
    }

    public function byModuleId($moduleId)
    {
        $this->andWhere(['[[module_id]]' => $moduleId]);

        return $this;
    }

    /**
     * @inheritdoc
     * @return Tariff[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Tariff|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}