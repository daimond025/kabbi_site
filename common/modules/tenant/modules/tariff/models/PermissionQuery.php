<?php

namespace common\modules\tenant\modules\tariff\models;

/**
 * This is the ActiveQuery class for [[Permission]].
 *
 * @see Permission
 */
class PermissionQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        $this->andWhere('[[status]]=1');

        return $this;
    }

    public function byCode($code)
    {
        $this->andWhere(['[[code]]' => $code]);

        return $this;
    }

    /**
     * @inheritdoc
     * @return Permission[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Permission|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}