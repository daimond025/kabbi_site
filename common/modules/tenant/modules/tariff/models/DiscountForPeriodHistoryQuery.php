<?php

namespace common\modules\tenant\modules\tariff\models;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[DiscountForPeriodHistory]].
 *
 * @see DiscountForPeriodHistory
 */
class DiscountForPeriodHistoryQuery extends ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['is', 'deactivated_at', null]);
    }

    /**
     * @param integer $period
     *
     * @return $this
     */
    public function byPeriod($period)
    {
        return $this->andWhere(['period' => $period]);
    }


    /**
     * @inheritdoc
     * @return DiscountForPeriodHistory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return DiscountForPeriodHistory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
