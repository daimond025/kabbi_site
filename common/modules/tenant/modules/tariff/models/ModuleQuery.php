<?php

namespace common\modules\tenant\modules\tariff\models;

/**
 * This is the ActiveQuery class for [[Module]].
 *
 * @see Module
 */
class ModuleQuery extends \yii\db\ActiveQuery
{
    public function active($alias = null)
    {
        $field = $alias ? $alias . '.active' : 'active';
        $this->andWhere([$field => 1]);

        return $this;
    }

    public function byCode($code)
    {
        $this->andWhere(['[[code]]' => $code]);

        return $this;
    }

    /**
     * @inheritdoc
     * @return Module[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Module|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}