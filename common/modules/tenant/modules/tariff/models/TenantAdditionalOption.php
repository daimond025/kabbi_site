<?php

namespace common\modules\tenant\modules\tariff\models;

use common\components\behaviors\CacheInvalidateBehavior;
use common\modules\tenant\modules\tariff\components\behaviors\DatetimeBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\caching\TagDependency;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%tenant_additional_option}}".
 *
 * @property integer                    $id
 * @property integer                    $tenant_id
 * @property integer                    $option_id
 * @property integer                    $started_at
 * @property integer                    $expiry_date
 * @property integer                    $created_at
 * @property integer                    $updated_at
 * @property integer                    $created_by
 * @property integer                    $updated_by
 *
 * @property TariffAdditionalOption     $additionalOption
 * @property PaymentForAdditionalOption $paymentForAdditionalOption
 * @property Tenant                     $tenant
 */
class TenantAdditionalOption extends \yii\db\ActiveRecord
{
    const CACHE_TAG = 'tenant-additional-options';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_additional_option}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'option_id', 'started_at', 'expiry_date'], 'required'],
            [
                [
                    'tenant_id',
                    'option_id',
                    'created_at',
                    'updated_at',
                    'created_by',
                    'updated_by',
                ],
                'integer',
            ],
            [['startedAt', 'expiryDate'], 'date', 'format' => 'php:d.m.Y'],
            ['started_at', 'compare', 'compareAttribute' => 'expiry_date', 'operator' => '<'],
            [['started_at'], 'checkIntersection',],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('tenant-additional-option', 'ID'),
            'tenant_id'   => Yii::t('tenant-additional-option', 'Tenant ID'),
            'option_id'   => Yii::t('tenant-additional-option', 'Option ID'),
            'started_at'  => Yii::t('tenant-additional-option', 'Started At'),
            'startedAt'   => Yii::t('tenant-additional-option', 'Started At'),
            'expiry_date' => Yii::t('tenant-additional-option', 'Expiry Date'),
            'expiryDate'  => Yii::t('tenant-additional-option', 'Expiry Date'),
            'created_at'  => Yii::t('app', 'Created At'),
            'updated_at'  => Yii::t('app', 'Updated At'),
            'created_by'  => Yii::t('app', 'Created By'),
            'updated_by'  => Yii::t('app', 'Updated By'),
        ];
    }

    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
            [
                'class'   => DatetimeBehavior::className(),
                'options' => [
                    'startedAt'  => [
                        'field'  => 'started_at',
                        'format' => 'd.m.Y',
                    ],
                    'expiryDate' => [
                        'field'  => 'expiry_date',
                        'format' => 'd.m.Y',
                    ],
                ],
            ],
            [
                'class' => CacheInvalidateBehavior::className(),
                'tags'  => [
                    self::CACHE_TAG,
                    function ($model) {
                        return self::CACHE_TAG . ' id=' . $model->id;
                    },
                ],
            ],
        ];
    }

    /**
     * Validator which check tenant tariff intersection
     */
    public function checkIntersection($attribute, $params)
    {
        if (TenantAdditionalOption::find()
            ->active()
            ->byTenantId($this->tenant_id)
            ->andWhere(['option_id' => $this->option_id])
            ->andWhere(['not', ['id' => $this->id]])
            ->andWhere([
                'or',
                [
                    'and',
                    ['<=', 'started_at', $this->started_at],
                    ['>=', 'expiry_date', $this->started_at],
                ],
                [
                    'and',
                    ['>=', 'started_at', $this->started_at],
                    ['<=', 'started_at', $this->expiry_date],
                ],
            ])
            ->exists()
        ) {
            $this->addError(null,
                t('tenant-additional-option', 'Active additional option already exists on this date period',
                    ['attribute' => t('tenant-additional-option', $this->attributeLabels()[$attribute])]));
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdditionalOption()
    {
        return $this->hasOne(TariffAdditionalOption::className(), ['id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentForAdditionalOption()
    {
        return $this->hasOne(PaymentForAdditionalOption::className(), ['id' => 'payment_for_option_id']);
    }

    public static function getTenantClass()
    {
        $module = \Yii::$app->getModule('common-tenant/tariff');

        return $module->tenantClass;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(static::getTenantClass(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @inheritdoc
     * @return TenantAdditionalOptionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TenantAdditionalOptionQuery(get_called_class());
    }

    /**
     * Getting tenant tariffs
     *
     * @param integer $date
     * @param integer $tenantId
     * @param Cache   $cache
     *
     * @return array
     */
    public static function getTenantAdditionalOptions($date, $tenantId, $cache = null)
    {
        $date = strtotime(date('d.m.Y', $date));

        $cache    = isset($cache) ? $cache : Yii::$app->getCache();
        $cacheKey = 'tenant-additional-options #' . $tenantId . ' ' . date('d.m.Y', $date);

        $result = $cache->get($cacheKey);

        if ($result !== false) {
            return $result;
        } else {
            $result = ArrayHelper::map(static::find()
                ->alias('t')
                ->innerJoinWith(['additionalOption'], true)
                ->byTenantId($tenantId)
                ->byDate($date)
                ->active('t')
                ->addOrderBy(['started_at' => SORT_DESC])
                ->asArray()
                ->all(),
                function ($model) {
                    return $model['additionalOption']['code'];
                }, function ($model) {
                    return [
                        'code'        => $model['additionalOption']['code'],
                        'name'        => $model['additionalOption']['name'],
                        'description' => $model['additionalOption']['description'],
                        'started_at'  => $model['started_at'],
                        'expiry_date' => $model['expiry_date'],
                    ];
                });

            $duration = strtotime('+1day', $date) - time();
            $cache->set($cacheKey, $result, $duration, new TagDependency([
                'tags' => [
                    static::CACHE_TAG,
                    TariffAdditionalOption::CACHE_TAG,
                ],
            ]));

            return $result;
        }
    }

}
