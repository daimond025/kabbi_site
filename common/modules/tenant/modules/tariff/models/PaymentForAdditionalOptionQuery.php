<?php

namespace common\modules\tenant\modules\tariff\models;

/**
 * This is the ActiveQuery class for [[PaymentForAdditionalOption]].
 *
 * @see PaymentForAdditionalOption
 */
class PaymentForAdditionalOptionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PaymentForAdditionalOption[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PaymentForAdditionalOption|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}