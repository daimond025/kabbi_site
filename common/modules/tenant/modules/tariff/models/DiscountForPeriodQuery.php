<?php

namespace common\modules\tenant\modules\tariff\models;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[DiscountForPeriod]].
 *
 * @see DiscountForPeriod
 */
class DiscountForPeriodQuery extends ActiveQuery
{
    /**
     * @param integer $period
     *
     * @return $this
     */
    public function byPeriod($period)
    {
        return $this->andWhere(['period' => $period]);
    }

    /**
     * @inheritdoc
     * @return DiscountForPeriod[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return DiscountForPeriod|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
