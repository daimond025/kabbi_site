<?php

namespace common\modules\tenant\modules\tariff\models;

use common\modules\tenant\modules\tariff\components\behaviors\DatetimeBehavior;
use Yii;
use yii\base\Exception;
use yii\base\InvalidParamException;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%price_history}}".
 *
 * @property integer          $id
 * @property integer          $entity_type_id
 * @property integer          $option_id
 * @property integer          $service_id
 * @property integer          $tariff_id
 * @property string           $price
 * @property integer          $currency_id
 * @property integer          $activated_at
 * @property integer          $deactivated_at
 * @property integer          $created_at
 * @property integer          $updated_at
 * @property integer          $created_by
 * @property integer          $updated_by
 * @property integer          $entity_id
 *
 * @property TariffEntityType $tariffEntityType
 * @property Currency         $currency
 * @property AdditionalOption $additionalOption
 * @property OneTimeService   $oneTimeService
 * @property Tariff           $tariff
 */
class PriceHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%price_history}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity_type_id', 'currency_id', 'activated_at'], 'required'],
            [
                [
                    'entity_type_id',
                    'option_id',
                    'service_id',
                    'tariff_id',
                    'currency_id',
                    'created_at',
                    'updated_at',
                    'created_by',
                    'updated_by',
                ],
                'integer',
            ],
            [['price'], 'number'],
            [['activatedAt', 'deactivatedAt'], 'date', 'format' => 'php:d.m.Y'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'             => Yii::t('price-history', 'ID'),
            'entity_type_id' => Yii::t('price-history', 'Entity Type ID'),
            'option_id'      => Yii::t('price-history', 'Option ID'),
            'service_id'     => Yii::t('price-history', 'Service ID'),
            'tariff_id'      => Yii::t('price-history', 'Tariff ID'),
            'price'          => Yii::t('price-history', 'Price'),
            'currency_id'    => Yii::t('price-history', 'Currency ID'),
            'activated_at'   => Yii::t('price-history', 'Activated At'),
            'deactivated_at' => Yii::t('price-history', 'Deactivated At'),
            'activatedAt'    => Yii::t('price-history', 'Activated At'),
            'deactivatedAt'  => Yii::t('price-history', 'Deactivated At'),
            'created_at'     => Yii::t('app', 'Created At'),
            'updated_at'     => Yii::t('app', 'Updated At'),
            'created_by'     => Yii::t('app', 'Created By'),
            'updated_by'     => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
            [
                'class'   => DatetimeBehavior::className(),
                'options' => [
                    'activatedAt'   => [
                        'field'  => 'activated_at',
                        'format' => 'd.m.Y',
                    ],
                    'deactivatedAt' => [
                        'field'  => 'deactivated_at',
                        'format' => 'd.m.Y',
                    ],
                ],
            ],
        ];
    }

    /**
     * Saving price changes history
     *
     * @param $entityTypeId
     * @param $optionId
     * @param $serviceId
     * @param $tariffId
     * @param $price
     * @param $currencyId
     *
     * @throws InvalidParamException|Exception
     */
    protected static function addPriceChange($entityTypeId, $optionId, $serviceId, $tariffId, $price, $currencyId)
    {
        switch ($entityTypeId) {
            case TariffEntityType::CODE_ADDITIONAL_OPTION:
                $lastPrice = static::find()->active()->byOptionId($optionId)->one();
                break;
            case TariffEntityType::CODE_ONE_TIME_SERVICE:
                $lastPrice = static::find()->active()->byserviceId($serviceId)->one();
                break;
            case TariffEntityType::CODE_TARIFF:
                $lastPrice = static::find()->active()->byTariffId($tariffId)->one();
                break;
            default:
                throw new InvalidParamException("Incorrect entity type");
        }

        $deactivatedAt = time();
        if (!empty($lastPrice)) {
            $lastPrice->deactivated_at = $deactivatedAt;
            $lastPrice->save();
        }

        $actualPrice = new static([
            'entity_type_id' => $entityTypeId,
            'option_id'      => $optionId,
            'service_id'     => $serviceId,
            'tariff_id'      => $tariffId,
            'price'          => $price,
            'currency_id'    => $currencyId,
            'activated_at'   => strtotime('+1 sec', $deactivatedAt),
        ]);

        if (!$actualPrice->save()) {
            throw new Exception(implode("\n", $actualPrice->getFirstErrors()));
        }
    }

    /**
     * Saving price changes history of additional option
     *
     * @param $optionId
     * @param $price
     * @param $currencyId
     *
     * @throws InvalidParamException
     */
    public static function addAdditionalOptionPriceChange($optionId, $price, $currencyId)
    {
        static::addPriceChange(TariffEntityType::CODE_ADDITIONAL_OPTION, $optionId, null, null, $price, $currencyId);
    }

    /**
     * Saving price changes history of one-time service
     *
     * @param $serviceId
     * @param $price
     * @param $currencyId
     */
    public static function addOneTimeServicePriceChange($serviceId, $price, $currencyId)
    {
        static::addPriceChange(TariffEntityType::CODE_ONE_TIME_SERVICE, null, $serviceId, null, $price, $currencyId);
    }

    /**
     * Saving price changes history of tariff
     *
     * @param $tariffId
     * @param $price
     * @param $currencyId
     */
    public static function addTariffPriceChange($tariffId, $price, $currencyId)
    {
        static::addPriceChange(TariffEntityType::CODE_TARIFF, null, null, $tariffId, $price, $currencyId);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffEntityType()
    {
        return $this->hasOne(TariffEntityType::className(), ['id' => 'entity_type_id']);
    }

    /**
     * Getting currency class
     * @return string
     */
    public static function getCurrencyClass()
    {
        $module = Yii::$app->getModule('common-tenant/tariff');

        return $module->currencyClass;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(static::getCurrencyClass(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdditionalOption()
    {
        return $this->hasOne(TariffAdditionalOption::className(), ['id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOneTimeService()
    {
        return $this->hasOne(OneTimeService::className(), ['id' => 'service_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(Tariff::className(), ['id' => 'tariff_id']);
    }

    /**
     * @inheritdoc
     * @return PriceHistoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PriceHistoryQuery(get_called_class());
    }
}
