<?php

namespace common\modules\tenant\modules\tariff\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%payment_for_one_time_service}}".
 *
 * @property integer        $id
 * @property integer        $payment_id
 * @property integer        $service_id
 * @property string         $price
 * @property string         $payment_sum
 * @property integer        $currency_id
 * @property integer        $executed_at
 * @property integer        $created_at
 * @property integer        $updated_at
 * @property integer        $created_by
 * @property integer        $updated_by
 *
 * @property Currency       $currency
 * @property Payment        $payment
 * @property OneTimeService $service
 */
class PaymentForOneTimeService extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment_for_one_time_service}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_id', 'service_id', 'currency_id'], 'required'],
            [
                [
                    'payment_id',
                    'service_id',
                    'currency_id',
                    'executed_at',
                    'created_at',
                    'updated_at',
                    'created_by',
                    'updated_by',
                ],
                'integer',
            ],
            [['price', 'payment_sum'], 'number'],
            ['executedAt', 'date', 'format' => 'php:d.m.Y'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'payment_id'  => 'Payment ID',
            'service_id'  => 'Service ID',
            'price'       => 'Price',
            'payment_sum' => 'Payment Sum',
            'currency_id' => 'Currency ID',
            'executed_at' => 'Executed At',
            'executedAt'  => 'Executed At',
            'created_at'  => 'Created At',
            'updated_at'  => 'Updated At',
            'created_by'  => 'Created By',
            'updated_by'  => 'Updated By',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
        ];
    }

    public static function getCurrencyClass()
    {
        $module = Yii::$app->getModule('common-tenant/tariff');

        return $module->currencyClass;
    }

    public function getExecutedAt()
    {
        return empty($this->executed_at)
            ? null : date('d.m.Y', $this->executed_at);
    }

    public function setExecutedAt($value)
    {
        $this->executed_at = empty($value) ? null : strtotime($value);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(static::getCurrencyClass(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::className(), ['id' => 'payment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(OneTimeService::className(), ['id' => 'service_id']);
    }

    /**
     * @inheritdoc
     * @return PaymentForOneTimeServiceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PaymentForOneTimeServiceQuery(get_called_class());
    }
}
