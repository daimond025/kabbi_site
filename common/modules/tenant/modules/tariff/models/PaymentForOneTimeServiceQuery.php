<?php

namespace common\modules\tenant\modules\tariff\models;

/**
 * This is the ActiveQuery class for [[PaymentForOneTimeService]].
 *
 * @see PaymentForOneTimeService
 */
class PaymentForOneTimeServiceQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PaymentForOneTimeService[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PaymentForOneTimeService|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}