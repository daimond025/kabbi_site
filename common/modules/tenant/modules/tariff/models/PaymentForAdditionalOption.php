<?php

namespace common\modules\tenant\modules\tariff\models;

use common\modules\tenant\modules\tariff\components\behaviors\DatetimeBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%payment_for_additional_option}}".
 *
 * @property integer                      $id
 * @property integer                      $payment_id
 * @property integer                      $option_id
 * @property integer                      $started_at
 * @property integer                      $period
 * @property string                       $price
 * @property string                       $payment_sum
 * @property integer                      $currency_id
 * @property integer                      $created_at
 * @property integer                      $updated_at
 * @property integer                      $created_by
 * @property integer                      $updated_by
 *
 * @property Currency                     $currency
 * @property TariffAdditionalOption       $additionalOption
 * @property TenantTariffAdditionalOption $tenantAdditionalOption
 * @property Payment                      $payment
 */
class PaymentForAdditionalOption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment_for_additional_option}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_id', 'option_id', 'period', 'currency_id'], 'required'],
            [
                [
                    'payment_id',
                    'option_id',
                    'started_at',
                    'period',
                    'currency_id',
                    'created_at',
                    'updated_at',
                    'created_by',
                    'updated_by',
                ],
                'integer',
            ],
            [['price', 'payment_sum'], 'number'],
            ['startedAt', 'date', 'format' => 'php:d.m.Y'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('payment-for-additional-option', 'ID'),
            'payment_id'  => Yii::t('payment-for-additional-option', 'Payment ID'),
            'option_id'   => Yii::t('payment-for-additional-option', 'Option ID'),
            'started_at'  => Yii::t('payment-for-additional-option', 'Started At'),
            'startedAt'   => Yii::t('payment-for-additional-option', 'Started At'),
            'period'      => Yii::t('payment-for-additional-option', 'Period'),
            'price'       => Yii::t('payment-for-additional-option', 'Price'),
            'payment_sum' => Yii::t('payment-for-additional-option', 'Payment Sum'),
            'currency_id' => Yii::t('payment-for-additional-option', 'Currency ID'),
            'created_at'  => Yii::t('app', 'Created At'),
            'updated_at'  => Yii::t('app', 'Updated At'),
            'created_by'  => Yii::t('app', 'Created By'),
            'updated_by'  => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
            [
                'class'   => DatetimeBehavior::className(),
                'options' => [
                    'startedAt' => [
                        'field'  => 'started_at',
                        'format' => 'd.m.Y',
                    ],
                ],
            ],
        ];
    }

    public static function getCurrencyClass()
    {
        $module = Yii::$app->getModule('common-tenant/tariff');

        return $module->currencyClass;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(static::getCurrencyClass(), ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdditionalOption()
    {
        return $this->hasOne(TariffAdditionalOption::className(), ['id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantAdditionalOption()
    {
        return $this->hasOne(TenantAdditionalOption::className(), ['payment_for_option_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::className(), ['id' => 'payment_id']);
    }

    /**
     * @inheritdoc
     * @return PaymentForAdditionalOptionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PaymentForAdditionalOptionQuery(get_called_class());
    }
}
