<?php

namespace common\modules\tenant\modules\tariff\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%one_time_service}}".
 *
 * @property integer                    $id
 * @property integer                    $code
 * @property string                     $name
 * @property string                     $description
 * @property integer                    $active
 * @property float                      $price
 * @property integer                    $currency_id
 * @property integer                    $created_at
 * @property integer                    $updated_at
 * @property integer                    $created_by
 * @property integer                    $updated_by
 *
 * @property PaymentForOneTimeService[] $paymentForOneTimeServices
 * @property Currency                   $currency
 */
class OneTimeService extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%one_time_service}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name', 'price', 'currency_id'], 'required'],
            [['description'], 'string'],
            [['code', 'active', 'currency_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            ['price', 'number'],
            [['name'], 'string', 'max' => 255],
            ['code', 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('one-time-service', 'ID'),
            'code'        => Yii::t('one-time-service', 'Code'),
            'name'        => Yii::t('one-time-service', 'Name'),
            'description' => Yii::t('one-time-service', 'Description'),
            'active'      => Yii::t('one-time-service', 'Active'),
            'price'       => Yii::t('one-time-service', 'Price'),
            'currency_id' => Yii::t('one-time-service', 'Currency ID'),
            'created_at'  => Yii::t('one-time-service', 'Created At'),
            'updated_at'  => Yii::t('one-time-service', 'Updated At'),
            'created_by'  => Yii::t('one-time-service', 'Created By'),
            'updated_by'  => Yii::t('one-time-service', 'Updated By'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_INSERT | self::OP_UPDATE,
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert
            || (isset($changedAttributes['price']) && $this->price != $changedAttributes['price'])
            || (isset($changedAttributes['currency_id']) && $this->currency_id != $changedAttributes['currency_id'])
        ) {
            PriceHistory::addOneTimeServicePriceChange($this->id, $this->price, $this->currency_id);
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        $module = Yii::$app->getModule('common-tenant/tariff');

        return $this->hasOne($module->currencyClass, ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentForOneTimeServices()
    {
        return $this->hasMany(PaymentForOneTimeService::className(), ['service_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return OneTimeServiceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OneTimeServiceQuery(get_called_class());
    }
}
