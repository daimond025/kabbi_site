<?php

namespace common\modules\tenant\modules\tariff\models;

/**
 * This is the ActiveQuery class for [[PriceHistory]].
 *
 * @see PriceHistory
 */
class PriceHistoryQuery extends \yii\db\ActiveQuery
{
    public function byOptionId($optionId)
    {
        $this->andWhere(['[[option_id]]' => $optionId]);

        return $this;
    }

    public function byServiceId($serviceId)
    {
        $this->andWhere(['[[service_id]]' => $serviceId]);

        return $this;
    }

    public function byTariffId($tariffId)
    {
        $this->andWhere(['[[tariff_id]]' => $tariffId]);

        return $this;
    }

    public function active()
    {
        $this->andWhere(['[[deactivated_at]]' => null]);

        return $this;
    }

    public function byDate($date)
    {
        $this->andWhere('([[activated_at]] <= :date and ([[deactivated_at]] >= :date or [[deactivated_at]] is null))',
            compact('date'));

        return $this;
    }

    /**
     * @inheritdoc
     * @return PriceHistory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PriceHistory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}