<?php

namespace common\modules\tenant\modules\tariff\models;


/**
 * This is the ActiveQuery class for [[TenantAdditionalOption]].
 *
 * @see TenantAdditionalOption
 */
class TenantAdditionalOptionQuery extends \yii\db\ActiveQuery
{
    public function active($alias = null)
    {
        $field = $alias ? $alias . '.active' : 'active';
        $this->andWhere([$field => 1]);

        return $this;
    }

    public function byTenantId($tenantId)
    {
        $this->andWhere(['[[tenant_id]]' => $tenantId]);

        return $this;
    }

    public function byDate($date)
    {
        $this->andWhere(['<=', 'started_at', $date])
            ->andWhere(['>=', 'expiry_date', $date]);

        return $this;
    }

    /**
     * @inheritdoc
     * @return TenantAdditionalOption[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TenantAdditionalOption|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}