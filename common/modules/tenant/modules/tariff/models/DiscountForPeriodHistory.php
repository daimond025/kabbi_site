<?php

namespace common\modules\tenant\modules\tariff\models;

use common\modules\tenant\modules\tariff\components\behaviors\DatetimeBehavior;
use Yii;
use yii\base\Exception;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%discount_for_period_history}}".
 *
 * @property integer $id
 * @property integer $period
 * @property integer $percent
 * @property integer $activated_at
 * @property integer $deactivated_at
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 */
class DiscountForPeriodHistory extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%discount_for_period_history}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['period', 'percent', 'activated_at'], 'required'],
            ['period', 'integer', 'min' => 1, 'max' => 999],
            ['percent', 'integer', 'min' => 0, 'max' => 99999],
            [
                [
                    'activated_at',
                    'deactivated_at',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                ],
                'integer',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'             => Yii::t('discount-for-period', 'ID'),
            'period'         => Yii::t('discount-for-period', 'Period'),
            'percent'        => Yii::t('discount-for-period', 'Percent'),
            'activated_at'   => Yii::t('discount-for-period', 'Activated At'),
            'activatedAt'    => Yii::t('discount-for-period', 'Activated At'),
            'deactivatedAt'  => Yii::t('discount-for-period', 'Deactivated At'),
            'deactivated_at' => Yii::t('discount-for-period', 'Deactivated At'),
            'created_by'     => Yii::t('app', 'Created By'),
            'created_at'     => Yii::t('app', 'Created At'),
            'updated_by'     => Yii::t('app', 'Updated By'),
            'updated_at'     => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
            [
                'class'   => DatetimeBehavior::className(),
                'options' => [
                    'activatedAt'   => [
                        'field'  => 'activated_at',
                        'format' => 'd.m.Y',
                    ],
                    'deactivatedAt' => [
                        'field'  => 'deactivated_at',
                        'format' => 'd.m.Y',
                    ],
                ],
            ],
        ];
    }

    public static function changeDiscount($period, $discount)
    {
        $lastDiscount = static::find()->byPeriod($period)->active()->one();

        $deactivatedAt = time();
        if (!empty($lastDiscount)) {
            $lastDiscount->deactivated_at = $deactivatedAt;
            $lastDiscount->save();
        }

        $actualDiscount = new static([
            'period'       => $period,
            'percent'      => $discount,
            'activated_at' => strtotime('+1 sec', $deactivatedAt),
        ]);

        if (!$actualDiscount->save()) {
            throw new Exception(implode("\n", $actualDiscount->getFirstErrors()));
        }
    }

    /**
     * @inheritdoc
     * @return DiscountForPeriodHistoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DiscountForPeriodHistoryQuery(get_called_class());
    }
}
