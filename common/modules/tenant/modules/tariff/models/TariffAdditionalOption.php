<?php

namespace common\modules\tenant\modules\tariff\models;

use common\components\behaviors\CacheInvalidateBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%tariff_additional_option}}".
 *
 * @property integer                      $id
 * @property integer                      $code
 * @property string                       $name
 * @property string                       $description
 * @property float                        $price
 * @property integer                      $currency_id
 * @property integer                      $active
 * @property integer                      $created_at
 * @property integer                      $updated_at
 * @property integer                      $created_by
 * @property integer                      $updated_by
 *
 * @property PaymentForAdditionalOption[] $paymentForAdditionalOptions
 * @property TenantAdditionalOption[]     $tenantAdditionalOptions
 * @property Currency                     $currency
 */
class TariffAdditionalOption extends \yii\db\ActiveRecord
{
    const CACHE_TAG = 'tariff-additional-options';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tariff_additional_option}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name', 'price', 'currency_id'], 'required'],
            [['description'], 'string'],
            [['code', 'active', 'currency_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 255],
            ['price', 'number'],
            ['code', 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('tariff-additional-option', 'ID'),
            'code'        => Yii::t('tariff-additional-option', 'Code'),
            'name'        => Yii::t('tariff-additional-option', 'Name'),
            'description' => Yii::t('tariff-additional-option', 'Description'),
            'price'       => Yii::t('tariff-additional-option', 'Price'),
            'currency_id' => Yii::t('tariff-additional-option', 'Currency ID'),
            'active'      => Yii::t('tariff-additional-option', 'Active'),
            'created_at'  => Yii::t('tariff-additional-option', 'Created At'),
            'updated_at'  => Yii::t('tariff-additional-option', 'Updated At'),
            'created_by'  => Yii::t('tariff-additional-option', 'Created By'),
            'updated_by'  => Yii::t('tariff-additional-option', 'Updated By'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
            [
                'class' => CacheInvalidateBehavior::className(),
                'tags'  => [
                    self::CACHE_TAG,
                    function ($model) {
                        return self::CACHE_TAG . ' id=' . $model->id;
                    },
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_INSERT | self::OP_UPDATE,
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert
            || (isset($changedAttributes['price']) && $this->price != $changedAttributes['price'])
            || (isset($changedAttributes['currency_id']) && $this->currency_id != $changedAttributes['currency_id'])
        ) {
            PriceHistory::addAdditionalOptionPriceChange($this->id, $this->price, $this->currency_id);
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        $module = Yii::$app->getModule('common-tenant/tariff');

        return $this->hasOne($module->currencyClass, ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentForAdditionalOptions()
    {
        return $this->hasMany(PaymentForAdditionalOption::className(), ['option_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantAdditionalOptions()
    {
        return $this->hasMany(TenantAdditionalOption::className(), ['option_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return TariffAdditionalOptionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TariffAdditionalOptionQuery(get_called_class());
    }
}
