<?php

namespace common\modules\tenant\modules\tariff\models;

use common\components\behaviors\CacheInvalidateBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\caching\TagDependency;
use common\modules\tenant\modules\tariff\models\TariffPermissionHistory;

/**
 * This is the model class for table "{{%tariff_permission}}".
 *
 * @property integer                   $id
 * @property integer                   $tariff_id
 * @property integer                   $permission_id
 * @property integer                   $value
 * @property integer                   $active
 * @property integer                   $created_at
 * @property integer                   $updated_at
 * @property integer                   $created_by
 * @property integer                   $updated_by
 *
 * @property Permission                $permission
 * @property Tariff                    $tariff
 * @property TariffPermissionHistory[] $tariffPermissionHistories
 * @property TenantPermission[]        $tenantPermissions
 */
class TariffPermission extends \yii\db\ActiveRecord
{
    const CACHE_TAG = 'tariff-permissions';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tariff_permission}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id', 'permission_id', 'value'], 'required'],
            [
                [
                    'tariff_id',
                    'value',
                    'active',
                    'created_at',
                    'updated_at',
                    'created_by',
                    'updated_by',
                    'permission_id',
                ],
                'integer',
            ],
            [
                'permission_id',
                'unique',
                'targetAttribute' => ['tariff_id', 'permission_id'],
                'message'         => t('tariff', 'Permission of tariff already exists'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => Yii::t('tariff-permission', 'ID'),
            'tariff_id'     => Yii::t('tariff-permission', 'Tariff ID'),
            'permission_id' => Yii::t('tariff-permission', 'Permission ID'),
            'value'         => Yii::t('tariff-permission', 'Value'),
            'active'        => Yii::t('tariff-permission', 'Active'),
            'created_at'    => Yii::t('app', 'Created At'),
            'updated_at'    => Yii::t('app', 'Updated At'),
            'created_by'    => Yii::t('app', 'Created By'),
            'updated_by'    => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
            [
                'class' => CacheInvalidateBehavior::className(),
                'tags'  => [
                    self::CACHE_TAG,
                    function ($model) {
                        return self::CACHE_TAG . ' id=' . $model->id;
                    },
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_INSERT | self::OP_UPDATE,
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert
            || (isset($changedAttributes['value']) && $this->value != $changedAttributes['value'])
        ) {
            TariffPermissionHistory::addTariffPermissionChange($this->id, $this->value);
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPermission()
    {
        return $this->hasOne(Permission::className(), ['id' => 'permission_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(Tariff::className(), ['id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffPermissionHistories()
    {
        return $this->hasMany(TariffPermissionHistory::className(), ['permission_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantPermissions()
    {
        return $this->hasMany(TenantPermission::className(), ['permission_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return TariffPermissionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TariffPermissionQuery(get_called_class());
    }
}
