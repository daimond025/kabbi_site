<?php

namespace common\modules\tenant\modules\tariff\models;

use app\modules\setting\models\TenantHasCity;
use common\components\billing\Billing;
use common\modules\tenant\modules\tariff\components\behaviors\DatetimeBehavior;
use Yii;
use yii\base\Exception;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%payment}}".
 *
 * @property integer                      $id
 * @property integer                      $tenant_id
 * @property integer                      $payment_number
 * @property string                       $description
 * @property integer                      $purchased_at
 * @property string                       $payment_sum
 * @property string                       $discount
 * @property integer                      $fixed_discount
 * @property integer                      $currency_id
 * @property integer                      $confirmed_at
 * @property integer                      $created_at
 * @property integer                      $updated_at
 * @property integer                      $created_by
 * @property integer                      $updated_by
 *
 * @property Currency                     $currency
 * @property Tenant                       $tenant
 * @property PaymentForAdditionalOption[] $paymentForAdditionalOptions
 * @property PaymentForOneTimeService[]   $paymentForOneTimeServices
 * @property PaymentForTariff[]           $paymentForTariffs
 */
class Payment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'payment_number', 'purchased_at', 'currency_id'], 'required'],
            [
                [
                    'tenant_id',
                    'currency_id',
                    'created_at',
                    'updated_at',
                    'created_by',
                    'updated_by',
                    'fixed_discount',
                ],
                'integer',
            ],
            [['payment_number', 'description'], 'string'],
            [['payment_sum', 'discount'], 'number'],
            [['purchasedAt', 'confirmedAt'], 'date', 'format' => 'php:d.m.Y'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'             => Yii::t('payment', 'ID'),
            'tenant_id'      => Yii::t('payment', 'Tenant ID'),
            'payment_number' => Yii::t('payment', 'Payment Number'),
            'description'    => Yii::t('payment', 'Description'),
            'purchased_at'   => Yii::t('payment', 'Purchased At'),
            'purchasedAt'    => Yii::t('payment', 'Purchased At'),
            'payment_sum'    => Yii::t('payment', 'Payment Sum'),
            'discount'       => Yii::t('payment', 'Discount'),
            'fixed_discount' => Yii::t('payment', 'Fixed Discount'),
            'currency_id'    => Yii::t('payment', 'Currency ID'),
            'created_at'     => Yii::t('app', 'Created At'),
            'updated_at'     => Yii::t('app', 'Updated At'),
            'created_by'     => Yii::t('app', 'Created By'),
            'updated_by'     => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
            [
                'class'   => DatetimeBehavior::className(),
                'options' => [
                    'purchasedAt' => [
                        'field'  => 'purchased_at',
                        'format' => 'd.m.Y',
                    ],
                    'confirmedAt' => [
                        'field'  => 'confirmed_at',
                        'format' => 'd.m.Y',
                    ],
                ],
            ],
        ];
    }

    public static function getCurrencyClass()
    {
        $module = Yii::$app->getModule('common-tenant/tariff');

        return $module->currencyClass;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(static::getCurrencyClass(), ['currency_id' => 'currency_id']);
    }

    public static function getTenantClass()
    {
        $module = \Yii::$app->getModule('common-tenant/tariff');

        return $module->tenantClass;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(static::getTenantClass(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentForAdditionalOptions()
    {
        return $this->hasMany(PaymentForAdditionalOption::className(), ['payment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentForOneTimeServices()
    {
        return $this->hasMany(PaymentForOneTimeService::className(), ['payment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentForTariffs()
    {
        return $this->hasMany(PaymentForTariff::className(), ['payment_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return PaymentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PaymentQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (isset($this->confirmed_at) && !$this->isAttributeChanged('confirmed_at')) {
            $this->addError(null, t('payment', 'You can not edit a confirmed payment'));

            return false;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (isset($this->confirmed_at)) {
            $this->addError(null, t('payment', 'Confirmed payment can not be removed'));

            return false;
        }

        return parent::beforeDelete();
    }


    /**
     * Confirm payment
     *
     * @param integer $deposit
     *
     * @return bool
     */
    public function confirm($deposit = 0)
    {
        if (isset($this->confirmed_at)) {
            $this->addError(null, t('payment', 'The payment already has been confirmed'));

            return false;
        }

        if ($deposit) {

        }

        $transaction = app()->db->beginTransaction();
        try {
            $this->confirmed_at = time();
            $items              = [];
            if ($this->save()) {
                if (!empty($this->paymentForTariffs)) {
                    foreach ($this->paymentForTariffs as $paymentForTariff) {
                        $items[]      = $paymentForTariff->tariff->name
                            . ' (' . \Yii::t('gootax_modules', $paymentForTariff->tariff->module->name, null, 'ru-RU') . ')';
                        $tenantTariff = new TenantTariff([
                            'tenant_id'             => $this->tenant_id,
                            'tariff_id'             => $paymentForTariff->tariff_id,
                            'started_at'            => $paymentForTariff->started_at,
                            'expiry_date'           => strtotime('+ ' . $paymentForTariff->period . ' month -1day',
                                $paymentForTariff->started_at),
                            'payment_for_tariff_id' => $paymentForTariff->id,
                        ]);
                        if (!$tenantTariff->save()) {
                            $this->addError(null, implode('; ', $tenantTariff->getFirstErrors()));
                            $transaction->rollback();

                            return false;
                        };
                        $tenantTariff->refresh();
                        if (!empty($paymentForTariff->tariff->tariffPermissions)) {
                            foreach ($paymentForTariff->tariff->tariffPermissions as $permission) {
                                if ($permission->active) {
                                    $tenantPermission = new TenantPermission([
                                        'permission_id'    => $permission->id,
                                        'tenant_tariff_id' => $tenantTariff->id,
                                        'value'            => $permission->value,
                                    ]);
                                    if (!$tenantPermission->save()) {
                                        $this->addError(null, implode('; ', $tenantPermission->getFirstErrors()));
                                        $transaction->rollback();

                                        return false;
                                    };
                                }
                            }
                        }
                    }
                }
                if (!empty($this->paymentForAdditionalOptions)) {
                    foreach ($this->paymentForAdditionalOptions as $paymentForAdditionalOption) {
                        $items[]                = $paymentForAdditionalOption->additionalOption->name;
                        $tenantAdditionalOption = new TenantAdditionalOption([
                            'tenant_id'             => $this->tenant_id,
                            'option_id'             => $paymentForAdditionalOption->option_id,
                            'started_at'            => $paymentForAdditionalOption->started_at,
                            'expiry_date'           => strtotime('+ ' . $paymentForAdditionalOption->period . ' month - 1day',
                                $paymentForAdditionalOption->started_at),
                            'payment_for_option_id' => $paymentForAdditionalOption->id,
                        ]);
                        if (!$tenantAdditionalOption->save()) {
                            $this->addError(null, implode('; ', $tenantAdditionalOption->getFirstErrors()));
                            $transaction->rollback();

                            return false;
                        };
                    }
                }

                $billing = new Billing(['gearman' => app()->gearman]);

                if ($deposit) {
                    if (!$billing->depositTenantBalance(
                        $this->tenant_id,
                        $this->payment_sum - $this->discount,
                        $this->currency_id,
                        null
                    )
                    ) {
                        $this->addError(null, 'Произошла ошибка при пополнении счета арендатора');
                        $transaction->rollback();

                        return false;
                    }
                };

                if (!$billing->registerGootaxPayment(
                    $this->tenant_id,
                    $this->id,
                    $this->payment_sum - $this->discount,
                    $this->currency_id,
                    implode(', ', $items)
                )
                ) {
                    $this->addError(null, 'Произошла ошибка при проведении счета на оплату');
                    $transaction->rollback();

                    return false;
                }

                $transaction->commit();

                return true;
            }
        } catch (\Exception $ex) {
            Yii::error($ex, 'tenant-tariff');
            $this->addError(null, t('payment', 'An error occurred during the confirmation of payment'));
        }
        $transaction->rollback();

        return false;
    }

    /**
     * Cancel payment
     * @return bool
     * @throws \yii\db\Exception
     */
    public function cancel()
    {
        if (!isset($this->confirmed_at)) {
            $this->addError(null, t('payment', 'The payment is not confirmed'));

            return false;
        }

        $transaction = app()->db->beginTransaction();
        try {
            $this->confirmed_at = null;
            if ($this->save()) {
                if (!empty($this->paymentForTariffs)) {
                    foreach ($this->paymentForTariffs as $paymentForTariff) {
                        if ($paymentForTariff->tenantTariff !== null
                            && !$paymentForTariff->tenantTariff->delete()) {
                            $this->addError(null, t('payment', 'An error occurred while deleting tenant tariff'));
                            $transaction->rollBack();

                            return false;
                        }
                    }
                }
                if (!empty($this->paymentForAdditionalOptions)) {
                    foreach ($this->paymentForAdditionalOptions as $paymentForAdditionalOption) {
                        if ($paymentForAdditionalOption->tenantAdditionalOption !== null
                            && !$paymentForAdditionalOption->tenantAdditionalOption->delete()) {
                            $this->addError(t('payment', 'An error occurred while deleting additional option'));
                            $transaction->rollBack();

                            return false;
                        }
                    }
                }

                $billing = new Billing(['gearman' => app()->gearman]);
                if (!$billing->refundGootaxPayment(
                    $this->tenant_id,
                    $this->id,
                    $this->payment_sum - $this->discount,
                    $this->currency_id,
                    'Refund payment'
                )
                ) {
                    $this->addError(null, 'Ошибка биллинга');
                    $transaction->rollBack();

                    return false;
                }

                $transaction->commit();

                return true;
            }
        } catch (\Exception $ex) {
            Yii::error($ex, 'tenant-tariff');
            $this->addError(null, t('payment', 'An error occurred while canceling confirmation of payment'));
        }
        $transaction->rollBack();

        return false;
    }

}
