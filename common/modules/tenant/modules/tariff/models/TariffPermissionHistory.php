<?php

namespace common\modules\tenant\modules\tariff\models;

use common\modules\tenant\modules\tariff\components\behaviors\DatetimeBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%tariff_permission_history}}".
 *
 * @property integer          $id
 * @property integer          $permission_id
 * @property integer          $value
 * @property integer          $activated_at
 * @property integer          $deactivated_at
 * @property integer          $created_at
 * @property integer          $updated_at
 * @property integer          $created_by
 * @property integer          $updated_by
 *
 * @property TariffPermission $tariffPermission
 */
class TariffPermissionHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tariff_permission_history}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['permission_id', 'value', 'activated_at'], 'required'],
            [
                [
                    'permission_id',
                    'value',
                    'created_at',
                    'updated_at',
                    'created_by',
                    'updated_by',
                ],
                'integer',
            ],
            [['activatedAt', 'deactivatedAt'], 'date', 'format' => 'php:d.m.Y'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'             => Yii::t('tariff-permission-history', 'ID'),
            'permission_id'  => Yii::t('tariff-permission-history', 'Permission ID'),
            'value'          => Yii::t('tariff-permission-history', 'Value'),
            'activated_at'   => Yii::t('tariff-permission-history', 'Activated At'),
            'activatedAt'    => Yii::t('tariff-permission-history', 'Activated At'),
            'deactivated_at' => Yii::t('tariff-permission-history', 'Deactivated At'),
            'deactivatedAt'  => Yii::t('tariff-permission-history', 'Deactivated At'),
            'created_at'     => Yii::t('app', 'Created At'),
            'updated_at'     => Yii::t('app', 'Updated At'),
            'created_by'     => Yii::t('app', 'Created By'),
            'updated_by'     => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
            [
                'class'   => DatetimeBehavior::className(),
                'options' => [
                    'activatedAt'   => [
                        'field'  => 'activated_at',
                        'format' => 'd.m.Y',
                    ],
                    'deactivatedAt' => [
                        'field'  => 'deactivated_at',
                        'format' => 'd.m.Y',
                    ],
                ],
            ],
        ];
    }

    /**
     * Saving price changes history
     *
     * @param $tariffPermissionId
     * @param $value
     *
     * @throws InvalidParamException|Exception
     */
    public static function addTariffPermissionChange($tariffPermissionId, $value)
    {
        $last = static::find()->active()->byTariffPermissionId($tariffPermissionId)->one();

        $deactivatedAt = time();
        if (!empty($last)) {
            $last->deactivated_at = $deactivatedAt;
            $last->save();
        }

        $actual = new static([
            'permission_id' => $tariffPermissionId,
            'value'         => $value,
            'activated_at'  => strtotime('+1 sec', $deactivatedAt),
        ]);

        if (!$actual->save()) {
            throw new Exception(implode("\n", $actual->getFirstErrors()));
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffPermission()
    {
        return $this->hasOne(TariffPermission::className(), ['id' => 'permission_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPermission()
    {
        return $this->hasOne(Permission::className(), ['id' => 'permission_id'])
            ->via('tariffPermission');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(Tariff::className(), ['id' => 'tariff_id'])
            ->via('tariffPermission');
    }

    /**
     * @inheritdoc
     * @return TariffPermissionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TariffPermissionHistoryQuery(get_called_class());
    }
}
