<?php

namespace common\modules\tenant\modules\tariff\models;

use common\components\behaviors\CacheInvalidateBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%tariff}}".
 *
 * @property integer            $id
 * @property string             $name
 * @property string             $description
 * @property integer            $module_id
 * @property float              $price
 * @property integer            $currency_id
 * @property integer            $active
 * @property integer            $default
 * @property integer            $created_at
 * @property integer            $updated_at
 * @property integer            $created_by
 * @property integer            $updated_by
 *
 * @property PaymentForTariff[] $paymentForTariffs
 * @property Module             $module
 * @property Currency           $currency
 * @property TariffPermission[] $tariffPermissions
 * @property TenantTariff[]     $tenantTariffs
 * @property DemoTariff         $demoTariff
 */
class Tariff extends \yii\db\ActiveRecord
{
    const CACHE_TAG = 'tariffs';
    const CACHE_TAG_DEFAULT = 'tenant-tariff-defaults';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tariff}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'module_id', 'price', 'currency_id'], 'required'],
            [['description'], 'string'],
            [
                [
                    'module_id',
                    'active',
                    'default',
                    'currency_id',
                    'created_at',
                    'updated_at',
                    'created_by',
                    'updated_by',
                ],
                'integer',
            ],
            [['name'], 'string', 'max' => 255],
            ['price', 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('tariff', 'ID'),
            'name'        => Yii::t('tariff', 'Name'),
            'description' => Yii::t('tariff', 'Description'),
            'module_id'   => Yii::t('tariff', 'Module ID'),
            'price'       => Yii::t('tariff', 'Price'),
            'currency_id' => Yii::t('tariff', 'Currency ID'),
            'active'      => Yii::t('tariff', 'Active'),
            'default'     => Yii::t('tariff', 'Default'),
            'created_at'  => Yii::t('app', 'Created At'),
            'updated_at'  => Yii::t('app', 'Updated At'),
            'created_by'  => Yii::t('app', 'Created By'),
            'updated_by'  => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
            [
                'class' => CacheInvalidateBehavior::className(),
                'tags'  => [
                    self::CACHE_TAG,
                    function ($model) {
                        return self::CACHE_TAG . ' id=' . $model->id;
                    },
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_INSERT | self::OP_UPDATE,
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert
            || (isset($changedAttributes['price']) && $this->price != $changedAttributes['price'])
            || (isset($changedAttributes['currency_id']) && $this->currency_id != $changedAttributes['currency_id'])
        ) {
            PriceHistory::addTariffPriceChange($this->id, $this->price, $this->currency_id);
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        $module = Yii::$app->getModule('common-tenant/tariff');

        return $this->hasOne($module->currencyClass, ['currency_id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentForTariffs()
    {
        return $this->hasMany(PaymentForTariff::className(), ['tariff_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModule()
    {
        return $this->hasOne(Module::className(), ['id' => 'module_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffPermissions()
    {
        return $this->hasMany(TariffPermission::className(), ['tariff_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantTariffs()
    {
        return $this->hasMany(TenantTariff::className(), ['tariff_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDemoTariff()
    {
        return $this->hasOne(DemoTariff::className(), ['tariff_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return TariffQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TariffQuery(get_called_class());
    }

    /**
     * Getting price by date
     *
     * @param integer $date
     *
     * @return float
     */
    public function getPriceByDate($date)
    {
        return PriceHistory::find()->select('price')->byTariffId($this->id)->byDate($date)->scalar();
    }

    /**
     * Getting permission value
     *
     * @param integer $permissionCode
     *
     * @return integer|null
     */
    public function getPermissionValue($permissionCode)
    {
        return static::find()
            ->innerJoinWith([
                'tariffPermissions t',
                'tariffPermissions.permission p' => function ($query) use ($permissionCode) {
                    $query->andWhere(['p.code' => $permissionCode]);
                },
            ])
            ->select('t.value')
            ->scalar();
    }

    /**
     * Getting default tariff by module
     *
     * @param $moduleCode
     *
     * @return Tariff|null
     */
    public static function getDefaultTariff($moduleCode)
    {
        $moduleId = Module::find()->select('id')->byCode($moduleCode)->scalar();

        return Tariff::find()
            ->byModuleId($moduleId)
            ->byDefault(true)
            ->active()
            ->one();
    }

}
