<?php

namespace common\modules\tenant\modules\tariff\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "tbl_tariff_entity_type".
 *
 * @property integer $id
 * @property string  $name
 * @property string  $description
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $code
 */
class TariffEntityType extends \yii\db\ActiveRecord
{
    const CODE_TARIFF = 1;
    const CODE_ADDITIONAL_OPTION = 2;
    const CODE_ONE_TIME_SERVICE = 3;

    /**
     * @param $entityType
     */
    public static function getEntityClass($entityType)
    {
        switch ($entityType) {
            case self::CODE_TARIFF:
                return Tariff::className();
            case self::CODE_ADDITIONAL_OPTION:
                return TariffAdditionalOption::className();
            case self::CODE_ONE_TIME_SERVICE:
                return OneTimeService::className();
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_tariff_entity_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code'], 'required'],
            [['description'], 'string'],
            [['created_at', 'updated_at', 'created_by', 'updated_by', 'code'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'name'        => 'Name',
            'description' => 'Description',
            'created_at'  => 'Created At',
            'updated_at'  => 'Updated At',
            'created_by'  => 'Created By',
            'updated_by'  => 'Updated By',
            'code'        => 'Code',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     * @return TariffEntityTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TariffEntityTypeQuery(get_called_class());
    }
}
