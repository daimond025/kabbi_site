<?php

namespace common\modules\tenant\modules\tariff\models;

/**
 * This is the ActiveQuery class for [[TariffAdditionalOption]].
 *
 * @see TariffAdditionalOption
 */
class TariffAdditionalOptionQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        $this->andWhere('[[active]]=1');
        return $this;
    }

    /**
     * @inheritdoc
     * @return TariffAdditionalOption[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TariffAdditionalOption|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}