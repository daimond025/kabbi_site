<?php

namespace common\modules\tenant\modules\tariff\models;

use common\components\behaviors\CacheInvalidateBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%module}}".
 *
 * @property integer $id
 * @property integer $code
 * @property string $name
 * @property string $description
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Tariff[] $tariffs
 */
class Module extends \yii\db\ActiveRecord
{
    const CACHE_TAG = 'modules';

    const CODE_TAXI = 1;

    /**
     * Const set by code.
     */
    const TAXI_MODULE = 1;
    const TRACK_TAXI_MODULE = 2;
    const COURIER_MODULE = 3;
    const SERVICE_MODULE = 4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%module}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name'], 'required'],
            [['description'], 'string'],
            [['code', 'active', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 255],
            ['code', 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('module', 'ID'),
            'code'        => Yii::t('module', 'Code'),
            'name'        => Yii::t('module', 'Name'),
            'description' => Yii::t('module', 'Description'),
            'active'      => Yii::t('module', 'Active'),
            'created_at'  => Yii::t('app', 'Created At'),
            'updated_at'  => Yii::t('app', 'Updated At'),
            'created_by'  => Yii::t('app', 'Created By'),
            'updated_by'  => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
            [
                'class' => CacheInvalidateBehavior::className(),
                'tags'  => [
                    self::CACHE_TAG,
                    function ($model) {
                        return self::CACHE_TAG . ' id=' . $model->id;
                    },
                ],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffs()
    {
        return $this->hasMany(Tariff::className(), ['module_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return ModuleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ModuleQuery(get_called_class());
    }
}
