<?php

namespace common\modules\tenant\modules\tariff\models;

/**
 * This is the ActiveQuery class for [[TariffEntityType]].
 *
 * @see TariffEntityType
 */
class TariffEntityTypeQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return TariffEntityType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TariffEntityType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}