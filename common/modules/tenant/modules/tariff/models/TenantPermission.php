<?php

namespace common\modules\tenant\modules\tariff\models;

use common\components\behaviors\CacheInvalidateBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%tenant_permission}}".
 *
 * @property integer          $id
 * @property integer          $permission_id
 * @property integer          $value
 * @property integer          $created_at
 * @property integer          $updated_at
 * @property integer          $created_by
 * @property integer          $updated_by
 * @property integer          $tenant_tariff_id
 *
 * @property TariffPermission $tariffPermission
 * @property TenantTariff     $tenantTariff
 */
class TenantPermission extends \yii\db\ActiveRecord
{
    const CACHE_TAG = 'tenant-permissions';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_permission}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['permission_id', 'value', 'tenant_tariff_id'], 'required'],
            [
                ['permission_id', 'value', 'created_at', 'updated_at', 'created_by', 'updated_by', 'tenant_tariff_id'],
                'integer',
            ],
            [
                'permission_id',
                'unique',
                'targetAttribute' => ['tenant_tariff_id', 'permission_id'],
                'message'         => t('tariff', 'Permission of tariff already exists'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'               => Yii::t('tenant-permission', 'ID'),
            'permission_id'    => Yii::t('tenant-permission', 'Permission ID'),
            'value'            => Yii::t('tenant-permission', 'Value'),
            'created_at'       => Yii::t('app', 'Created At'),
            'updated_at'       => Yii::t('app', 'Updated At'),
            'created_by'       => Yii::t('app', 'Created By'),
            'updated_by'       => Yii::t('app', 'Updated By'),
            'tenant_tariff_id' => Yii::t('tenant-permission', 'Tenant Tariff ID'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
            [
                'class' => CacheInvalidateBehavior::className(),
                'tags'  => [
                    self::CACHE_TAG,
                    function ($model) {
                        return self::CACHE_TAG . ' id=' . $model->id;
                    },
                ],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffPermission()
    {
        return $this->hasOne(TariffPermission::className(), ['id' => 'permission_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantTariff()
    {
        return $this->hasOne(TenantTariff::className(), ['id' => 'tenant_tariff_id']);
    }

    /**
     * @inheritdoc
     * @return TenantPermissionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TenantPermissionQuery(get_called_class());
    }
}
