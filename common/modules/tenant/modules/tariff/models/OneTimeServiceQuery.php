<?php

namespace common\modules\tenant\modules\tariff\models;

/**
 * This is the ActiveQuery class for [[OneTimeService]].
 *
 * @see OneTimeService
 */
class OneTimeServiceQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        $this->andWhere('[[active]]=1');
        return $this;
    }

    /**
     * @inheritdoc
     * @return OneTimeService[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return OneTimeService|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}