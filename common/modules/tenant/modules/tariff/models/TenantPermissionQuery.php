<?php

namespace common\modules\tenant\modules\tariff\models;

/**
 * This is the ActiveQuery class for [[TenantPermission]].
 *
 * @see TenantPermission
 */
class TenantPermissionQuery extends \yii\db\ActiveQuery
{
    public function byTenantTariffId($tenantTariffId)
    {
        $this->andWhere(['[[tenant_tariff_id]]' => $tenantTariffId]);

        return $this;
    }

    public function byPermissionId($permissionId)
    {
        $this->andWhere([
            'in',
            'permission_id',
            TariffPermission::find()->select('id')->byPermissionId($permissionId),
        ]);

        return $this;
    }

    /**
     * @inheritdoc
     * @return TenantPermission[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TenantPermission|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}