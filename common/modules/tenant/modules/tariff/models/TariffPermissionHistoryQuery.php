<?php

namespace common\modules\tenant\modules\tariff\models;

/**
 * This is the ActiveQuery class for [[TariffPermissionHistory]].
 *
 * @see TariffPermissionHistory
 */
class TariffPermissionHistoryQuery extends \yii\db\ActiveQuery
{
    public function byTariffPermissionId($id)
    {
        $this->andWhere(['[[permission_id]]' => $id]);
        return $this;
    }

    public function active()
    {
        $this->andWhere(['[[deactivated_at]]' => null]);
        return $this;
    }

    /**
     * @inheritdoc
     * @return TariffPermissionHistory[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TariffPermissionHistory|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}