<?php

namespace common\modules\tenant\modules\tariff\components;

use common\modules\tenant\modules\tariff\models\Permission;
use common\modules\tenant\modules\tariff\models\Tariff;
use common\modules\tenant\modules\tariff\models\TenantTariff;
use yii\base\Object;

/**
 * Class TenantTariffService
 * @package common\modules\tenant\modules\tariff\components
 */
class TenantTariffService extends Object
{

    /**
     * Getting free tariffs
     * @return array
     */
    private function getFreeTariffs()
    {
        $tariffs = Tariff::find()
            ->alias('t')
            ->joinWith(['module m', 'tariffPermissions', 'tariffPermissions.permission'])
            ->byDefault(true)
            ->active('t')
            ->groupBy('m.code')
            ->orderBy(['m.code' => SORT_ASC, 't.id' => SORT_ASC])
            ->asArray()
            ->all();

        $freeTariffs = [];
        foreach ($tariffs as $tariff) {
            if (empty($freeTariffs[$tariff['module']['code']])) {
                $freeTariffs[$tariff['module']['code']]['name'] =
                    $tariff['name'] . ' (' . \Yii::t('gootax_modules', $tariff['module']['name'], null, 'ru-RU') . ')';

                foreach ($tariff['tariffPermissions'] as $permission) {
                    switch ($permission['permission']['code']) {
                        case Permission::CODE_WORKERS_ONLINE:
                            $freeTariffs[$tariff['module']['code']]['workers_online'] = $permission['value'];
                            break;
                        case Permission::CODE_USERS_ONLINE:
                            $freeTariffs[$tariff['module']['code']]['employees_online'] = $permission['value'];
                            break;
                    }
                }
            }
        }

        return $freeTariffs;
    }

    /**
     * Getting data for notifications
     *
     * @param       $date
     * @param int[] $notificationDays
     *
     * @return array
     */
    public function getNotificationData($date, array $notificationDays)
    {
        $date      = strtotime(date('d.m.Y', $date));
        $dayBefore = strtotime(date('d.m.Y', $date) . ' -1day');

        $freeTariffs = $this->getFreeTariffs();

        $tariffs = TenantTariff::find()
            ->alias('tt')
            ->joinWith([
                'tenant',
                'tariff',
                'tariff.module m',
                'tenantPermissions',
                'tenantPermissions.tariffPermission',
                'tenantPermissions.tariffPermission.permission',
            ])
            ->active('tt')
            ->andWhere([
                'or',
                [
                    'and',
                    ['<=', 'tt.started_at', $date],
                    ['>=', 'tt.expiry_date', $date],
                ],
                ['=', 'expiry_date', $dayBefore],
            ])
            ->asArray()
            ->orderBy(['tt.tenant_id' => SORT_ASC, 'm.code' => SORT_ASC])
            ->all();

        $result = [];
        foreach ($tariffs as $tariff) {
            $interval = date_diff(
                date_create(date('d.m.Y', strtotime('+1 day', $tariff['expiry_date']))),
                date_create(date('d.m.Y', $date))
            );

            if (in_array($interval->days, $notificationDays)) {
                $isNextPaid = TenantTariff::find()
                    ->byModuleId($tariff['tariff']['module']['id'])
                    ->byTenantId($tariff['tenant_id'])
                    ->byDate(strtotime('+1 day', $tariff['expiry_date']))
                    ->active()
                    ->exists();

                if (!$isNextPaid) {
                    if (empty($result[$tariff['tenant_id']])) {
                        $result[$tariff['tenant_id']] = [
                            'tenant_id'           => $tariff['tenant']['tenant_id'],
                            'domain'              => $tariff['tenant']['domain'],
                            'company_name'        => $tariff['tenant']['company_name'],
                            'contact_name'        => $tariff['tenant']['contact_name'],
                            'contact_second_name' => $tariff['tenant']['contact_second_name'],
                            'contact_last_name'   => $tariff['tenant']['contact_last_name'],
                            'contact_email'       => $tariff['tenant']['contact_email'],
                            'contact_phone'       => $tariff['tenant']['contact_phone'],
                            'tariffs'             => [],
                        ];
                    }

                    $freeTariff = $freeTariffs[$tariff['tariff']['module']['code']];

                    $result[$tariff['tenant_id']]['tariffs'][$tariff['id']] = [
                        'name'        => $tariff['tariff']['name'] . ' (' . \Yii::t('gootax_modules',
                                $tariff['tariff']['module']['name'], null, 'ru-RU') . ')',
                        'expiry_date' => $tariff['expiry_date'],
                        'days_left'   => $interval->days,
                        'free_tariff' => $freeTariff['name'],
                    ];

                    if (isset($freeTariff['workers_online'])) {
                        $result[$tariff['tenant_id']]['tariffs'][$tariff['id']]['workers_online'] = $freeTariff['workers_online'];
                    }
                    if (isset($freeTariff['employees_online'])) {
                        $result[$tariff['tenant_id']]['tariffs'][$tariff['id']]['employees_online'] = $freeTariff['employees_online'];
                    }
                }
            }
        }

        return $result;
    }
}