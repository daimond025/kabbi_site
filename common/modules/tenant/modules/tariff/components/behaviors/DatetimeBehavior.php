<?php

namespace common\modules\tenant\modules\tariff\components\behaviors;

use yii\base\Behavior;

class DatetimeBehavior extends Behavior
{
    /**
     * Array with data in this format
     * [
     *      'field_date' => [
     *          'field' => 'field_timestamp',
     *          'format' => 'datetime format'
     *      ],
     *  ]
     * @var array
     */
    public $options;

    public function canGetProperty($name, $checkVars = true)
    {
        if (isset($this->options[$name])) {
            return true;
        } else {
            return parent::canGetProperty($name, $checkVars);
        }
    }

    public function canSetProperty($name, $checkVars = true)
    {
        if (isset($this->options[$name])) {
            return true;
        } else {
            return parent::canSetProperty($name, $checkVars);
        }
    }

    public function __get($name)
    {
        if (isset($this->options[$name])) {
            $field  = $this->options[$name]['field'];
            $format = $this->options[$name]['format'];

            return empty($this->owner->$field)
                ? null : date($format, $this->owner->$field);
        } else {
            return parent::__get($name);
        }
    }

    public function __set($name, $value)
    {
        if (isset($this->options[$name])) {
            $field               = $this->options[$name]['field'];
            $time = strtotime($value);
            $this->owner->$field = $time === false ? null : $time;
        } else {
            parent::__set($name, $value);
        }
    }

}


