<?php

namespace common\modules\tenant\modules\tariff;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'common\modules\tenant\modules\tariff\controllers';

    public $currencyClass;
    public $tenantClass;

    public $freeTimeOffset = 12;

    public function init()
    {
        parent::init();

    }
}
