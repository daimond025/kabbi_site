<?php

namespace common\modules\tenant\helpers;

class SmsTemplateParamsHelper
{
    const TAXI_NAME = '#TAXI_NAME#';
    const ADDRESS = '#ADDRESS#';
    const COMPANY_NAME = '#COMPANY_NAME#';
    const COMPANY_PHONE = '#COMPANY_PHONE#';
    const BRAND = '#BRAND#';
    const MODEL = '#MODEL#';
    const PHONE = '#PHONE#';
    const COLOR = '#PHONE#';
    const NUMBER = '#NUMBER#';
    const TIME = '#TIME#';
    const PRICE = '#PRICE#';
    const CURRENCY = '#CURRENCY#';
    const CODE = '#CODE#';
    const DATETIME = '#DATETIME#';

    public static function getParamsList()
    {
        return [
            self::TAXI_NAME,
            self::ADDRESS,
            self::COMPANY_NAME,
            self::COMPANY_PHONE,
            self::BRAND,
            self::MODEL,
            self::PHONE,
            self::COLOR,
            self::NUMBER,
            self::TIME,
            self::PRICE,
            self::CURRENCY,
            self::CODE,
            self::DATETIME,
        ];
    }

    public static function getParamsMap()
    {
        $params = [];

        $paramsList = self::getParamsList();

        if (is_array($paramsList)) {
            foreach ($paramsList as $param) {
                $params[$param] = $param . ' - ' . t('template', $param, [], 'ru');
            }
        }


        return $params;
    }
}
