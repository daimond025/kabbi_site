<?php

namespace common\modules\billing\models;

use Yii;

/**
 * This is the model class for table "{{%account_kind}}".
 *
 * @property integer $kind_id
 * @property string  $name
 */
class AccountKind extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%account_kind}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kind_id' => Yii::t('app', 'Kind ID'),
            'name'    => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @inheritdoc
     * @return AccountKindQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AccountKindQuery(get_called_class());
    }
}
