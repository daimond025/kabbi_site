<?php

namespace common\modules\billing\models;

use Yii;

/**
 * This is the model class for table "{{%transaction}}".
 *
 * @property integer $transaction_id
 * @property integer $type_id
 * @property integer $sender_acc_id
 * @property integer $receiver_acc_id
 * @property double  $sum
 * @property string  $payment_method
 * @property string  $date
 * @property string  $comment
 * @property integer $user_created
 * @property integer $city_id
 * @property string  $terminal_transact_number
 * @property string  $terminal_transact_date
 * @property integer $order_id
 */
class Transaction extends \yii\db\ActiveRecord
{
    const TERMINAL_ELECSNET_PAYMENT = 'TERMINAL_ELECSNET';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transaction}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'sender_acc_id', 'receiver_acc_id', 'user_created', 'city_id', 'order_id'], 'integer'],
            [['receiver_acc_id', 'sum'], 'required'],
            [['sum'], 'number'],
            [['date'], 'safe'],
            [
                ['payment_method', 'comment', 'terminal_transact_number', 'terminal_transact_date'],
                'string',
                'max' => 255,
            ],
            [
                ['payment_method'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => PaymentMethod::className(),
                'targetAttribute' => ['payment_method' => 'payment'],
            ],
            [
                ['city_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => City::className(),
                'targetAttribute' => ['city_id' => 'city_id'],
            ],
            [
                ['receiver_acc_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Account::className(),
                'targetAttribute' => ['receiver_acc_id' => 'account_id'],
            ],
            [
                ['sender_acc_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Account::className(),
                'targetAttribute' => ['sender_acc_id' => 'account_id'],
            ],
            [
                ['type_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => TransactionType::className(),
                'targetAttribute' => ['type_id' => 'type_id'],
            ],
            [
                ['user_created'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['user_created' => 'user_id'],
            ],
            [
                ['order_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Order::className(),
                'targetAttribute' => ['order_id' => 'order_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'transaction_id'           => Yii::t('app', 'Transaction ID'),
            'type_id'                  => Yii::t('app', 'Type ID'),
            'sender_acc_id'            => Yii::t('app', 'Sender Acc ID'),
            'receiver_acc_id'          => Yii::t('app', 'Receiver Acc ID'),
            'sum'                      => Yii::t('app', 'Sum'),
            'payment_method'           => Yii::t('app', 'Payment Method'),
            'date'                     => Yii::t('app', 'Date'),
            'comment'                  => Yii::t('app', 'Comment'),
            'user_created'             => Yii::t('app', 'User Created'),
            'city_id'                  => Yii::t('app', 'City ID'),
            'terminal_transact_number' => Yii::t('app', 'Terminal Transact Number'),
            'terminal_transact_date'   => Yii::t('app', 'Terminal Transact Date'),
            'order_id'                 => Yii::t('app', 'Order ID'),
        ];
    }

    /**
     * @inheritdoc
     * @return TransactionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TransactionQuery(get_called_class());
    }
}
