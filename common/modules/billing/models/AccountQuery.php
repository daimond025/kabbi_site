<?php

namespace common\modules\billing\models;

/**
 * This is the ActiveQuery class for [[Account]].
 *
 * @see Account
 */
class AccountQuery extends \yii\db\ActiveQuery
{
    public function tenantAccounts()
    {
        $this->andWhere([
            'acc_kind_id' => Account::KIND_ID_TENANT,
            'acc_type_id' => Account::TYPE_ID_PASSIVE,
        ]);

        return $this;
    }

    public function byTypeId($typeId)
    {
        $this->andWhere(['acc_type_id' => $typeId]);

        return $this;
    }

    public function byKindId($kindId)
    {
        $this->andWhere(['acc_kind_id' => $kindId]);

        return $this;
    }

    public function byTenantId($tenantId)
    {
        $this->andWhere(['tenant_id' => $tenantId]);

        return $this;
    }

    public function byCurrencyId($currencyId)
    {
        $this->andWhere(['currency_id' => $currencyId]);

        return $this;
    }

    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Account[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Account|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
