<?php

namespace common\modules\billing\models;

use common\modules\employee\models\worker\Worker;
use Yii;

/**
 * This is the model class for table "{{%account}}".
 *
 * @property integer $account_id
 * @property integer $acc_kind_id
 * @property integer $acc_type_id
 * @property integer $owner_id
 * @property integer $currency_id
 * @property integer $tenant_id
 * @property double  $balance
 * @property Worker  $worker
 */
class Account extends \yii\db\ActiveRecord
{
    const KIND_ID_TENANT = 1;

    const TYPE_ID_ACTIVE = 1;
    const TYPE_ID_PASSIVE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%account}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['acc_kind_id', 'acc_type_id', 'currency_id'], 'required'],
            [['acc_kind_id', 'acc_type_id', 'owner_id', 'currency_id', 'tenant_id'], 'integer'],
            [['balance'], 'number'],
            //            [['owner_id', 'acc_kind_id', 'acc_type_id', 'tenant_id', 'currency_id'], 'unique', 'targetAttribute' => ['owner_id', 'acc_kind_id', 'acc_type_id', 'tenant_id', 'currency_id'], 'message' => 'The combination of Acc Kind ID, Acc Type ID, Owner ID, Currency ID and Tenant ID has already been taken.'],
            //            [['acc_kind_id'], 'exist', 'skipOnError' => true, 'targetClass' => AccountKind::className(), 'targetAttribute' => ['acc_kind_id' => 'kind_id']],
            //            [['acc_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => AccountType::className(), 'targetAttribute' => ['acc_type_id' => 'type_id']],
            //            [['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency_id' => 'currency_id']],
            //            [['tenant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tenant::className(), 'targetAttribute' => ['tenant_id' => 'tenant_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'account_id'  => Yii::t('app', 'Account ID'),
            'acc_kind_id' => Yii::t('app', 'Acc Kind ID'),
            'acc_type_id' => Yii::t('app', 'Acc Type ID'),
            'owner_id'    => Yii::t('app', 'Owner ID'),
            'currency_id' => Yii::t('app', 'Currency ID'),
            'tenant_id'   => Yii::t('app', 'Tenant ID'),
            'balance'     => Yii::t('app', 'Balance'),
        ];
    }

    /**
     * @inheritdoc
     * @return AccountQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AccountQuery(get_called_class());
    }

    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'owner_id']);
    }
}
