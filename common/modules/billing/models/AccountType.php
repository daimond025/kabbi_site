<?php

namespace common\modules\billing\models;

use Yii;

/**
 * This is the model class for table "{{%account_type}}".
 *
 * @property integer $type_id
 * @property string  $name
 */
class AccountType extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%account_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type_id' => Yii::t('app', 'Type ID'),
            'name'    => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @inheritdoc
     * @return AccountTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AccountTypeQuery(get_called_class());
    }
}
