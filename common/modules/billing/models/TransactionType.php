<?php

namespace common\modules\billing\models;

use Yii;

/**
 * This is the model class for table "{{%transaction_type}}".
 *
 * @property integer $type_id
 * @property string $name
 */
class TransactionType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transaction_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type_id' => Yii::t('app', 'Type ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @inheritdoc
     * @return TransactionTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TransactionTypeQuery(get_called_class());
    }
}
