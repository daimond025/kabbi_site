<?php

namespace common\modules\billing\models;

/**
 * This is the ActiveQuery class for [[AccountKind]].
 *
 * @see AccountKind
 */
class AccountKindQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return AccountKind[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return AccountKind|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
