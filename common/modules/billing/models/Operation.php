<?php

namespace common\modules\billing\models;

use Yii;

/**
 * This is the model class for table "{{%operation}}".
 *
 * @property integer       $operation_id
 * @property integer       $type_id
 * @property integer       $account_id
 * @property integer       $transaction_id
 * @property double        $sum
 * @property double        $saldo
 * @property string        $comment
 * @property string        $date
 * @property string        $owner_name
 *
 * @property Account       $account
 * @property Transaction   $transaction
 * @property OperationType $type
 */
class Operation extends \yii\db\ActiveRecord
{
    const TYPE_ID_INCOME = 1;
    const TYPE_ID_EXPENSES = 2;
    const TYPE_ID_CASH_OUT = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%operation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'account_id', 'transaction_id', 'saldo'], 'required'],
            [['type_id', 'account_id', 'transaction_id'], 'integer'],
            [['sum', 'saldo'], 'number'],
            [['date'], 'safe'],
            [['comment', 'owner_name'], 'string', 'max' => 255],
            //            [
            //                ['account_id'],
            //                'exist',
            //                'skipOnError'     => true,
            //                'targetClass'     => Account::className(),
            //                'targetAttribute' => ['account_id' => 'account_id'],
            //            ],
            //            [
            //                ['transaction_id'],
            //                'exist',
            //                'skipOnError'     => true,
            //                'targetClass'     => Transaction::className(),
            //                'targetAttribute' => ['transaction_id' => 'transaction_id'],
            //            ],
            //            [
            //                ['type_id'],
            //                'exist',
            //                'skipOnError'     => true,
            //                'targetClass'     => OperationType::className(),
            //                'targetAttribute' => ['type_id' => 'type_id'],
            //            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'operation_id'   => Yii::t('app', 'Operation ID'),
            'type_id'        => Yii::t('app', 'Type ID'),
            'account_id'     => Yii::t('app', 'Account ID'),
            'transaction_id' => Yii::t('app', 'Transaction ID'),
            'sum'            => Yii::t('app', 'Sum'),
            'saldo'          => Yii::t('app', 'Saldo'),
            'comment'        => Yii::t('app', 'Comment'),
            'date'           => Yii::t('app', 'Date'),
            'owner_name'     => Yii::t('app', 'Owner Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Account::className(), ['account_id' => 'account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(Transaction::className(), ['transaction_id' => 'transaction_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(OperationType::className(), ['type_id' => 'type_id']);
    }

    /**
     * @inheritdoc
     * @return OperationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OperationQuery(get_called_class());
    }
}
