<?php

namespace common\modules\car\helpers;

use common\modules\car\models\CarClass;
use yii\helpers\ArrayHelper;
use Yii;

class CarClassHelper
{
    /**
     * @return array
     */
    public static function createCarsMap()
    {
        return ArrayHelper::map(CarClass::getAllCars(), 'class_id', function ($cars) {
            return Yii::t('car', $cars->class);
        });
    }
}