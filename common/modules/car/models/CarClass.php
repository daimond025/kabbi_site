<?php

namespace common\modules\car\models;

use app\modules\setting\models\ClientCompany;
use app\modules\setting\models\ClientCompanyHasTariff;
use app\modules\setting\models\TaxiTariff;
use common\modules\car\models\Car;
use common\modules\car\models\transport\TransportType;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%car_class}}".
 *
 * @property integer $class_id
 * @property string $class
 * @property integer $type_id
 *
 * @property Car[] $cars
 * @property ClientCompanyHasTariff[] $clientCompanyHasTariffs
 * @property ClientCompany[] $companies
 * @property TaxiTariff[] $taxiTariffs
 */
class CarClass extends \yii\db\ActiveRecord
{
    const SOURCE_CATEGORY = 'car';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%car_class}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['class', 'type_id'], 'required'],
            [['class'], 'string', 'max' => 50],
            [['class'], 'unique'],
            [['type_id', 'sort'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'class_id' => 'ID',
            'class'    => 'Класс авто',
            'sort' => 'Сортировка',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientCompanyHasTariffs()
    {
        return $this->hasMany(ClientCompanyHasTariff::className(), ['tariff_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(ClientCompany::className(),
            ['company_id' => 'company_id'])->viaTable('{{%client_company_has_tariff}}', ['tariff_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxiTariffs()
    {
        return $this->hasMany(TaxiTariff::className(), ['class_id' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(TransportType::className(), ['type_id' => 'type_id']);
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getAllCars()
    {
        return self::find()->all();
    }

}