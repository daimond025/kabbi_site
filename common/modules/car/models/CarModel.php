<?php

namespace common\modules\car\models;

use common\modules\car\models\CarBrand;
use common\modules\car\models\transport\TransportType;
use Yii;

/**
 * This is the model class for table "{{%car_model}}".
 *
 * @property string $model_id
 * @property integer $brand_id
 * @property string $name
 * @property integer $type_id
 *
 * @property CarBrand $brand
 */
class CarModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%car_model}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brand_id', 'name'], 'required'],
            [['brand_id', 'type_id'], 'integer'],
            [['name'], 'string', 'max' => 45],
            [
                ['brand_id', 'name'],
                'unique',
                'targetAttribute' => ['brand_id', 'name'],
                'message'         => 'The combination of Brand ID and Name has already been taken.',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'model_id' => Yii::t('car', 'Model ID'),
            'brand_id' => Yii::t('car', 'Brand ID'),
            'name'     => Yii::t('car', 'Name'),
            'type_id'  => Yii::t('car', 'Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(CarBrand::className(), ['brand_id' => 'brand_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(TransportType::className(), ['type_id' => 'type_id']);
    }
}
