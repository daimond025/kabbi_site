<?php

namespace common\modules\car\models;

use app\modules\tariff\models\AdditionalOption;
use common\modules\car\models\transport\TransportType;
use common\modules\tenant\models\TenantHasCarOption;
use Yii;

/**
 * This is the model class for table "{{%car_option}}".
 *
 * @property integer $option_id
 * @property string $name
 * @property integer $type_id
 *
 * @property AdditionalOption[] $additionalOptions
 * @property CarHasOption[] $carHasOptions
 * @property Car[] $cars
 * @property TenantHasCarOption[] $tenantHasCarOptions
 */
class CarOption extends \yii\db\ActiveRecord
{
    const SOURCE_CATEGORY = 'car-options';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%car_option}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type_id', 'sort'], 'required'],
            [['name'], 'string', 'max' => 45],
            [['type_id'], 'integer'],
            [['name'], 'unique'],
            [['sort'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'option_id' => 'ID',
            'name'      => 'Название',
            'type_id'   => 'Тип транспорта',
            'sort'      => 'Сортировка',
            'type.name' => 'Тип транспорта',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdditionalOptions()
    {
        return $this->hasMany(AdditionalOption::className(), ['additional_option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarHasOptions()
    {
        return $this->hasMany(CarHasOption::className(), ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::className(), ['car_id' => 'car_id'])->viaTable('{{%car_has_option}}',
            ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantHasCarOptions()
    {
        return $this->hasMany(TenantHasCarOption::className(), ['option_id' => 'option_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(TransportType::className(), ['type_id' => 'type_id']);
    }
}
