<?php

namespace common\modules\car\models;

use common\modules\car\models\Car;
use Yii;
use yii\caching\TagDependency;

/**
 * This is the model class for table "{{%car_color}}".
 *
 * @property integer $color_id
 * @property string $name
 *
 * @property Car[] $cars
 */
class CarColor extends \yii\db\ActiveRecord
{
    const SOURCE_CATEGORY = 'car';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%car_color}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'color_id' => 'Color ID',
            'name'     => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::className(), ['color' => 'color_id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->deleteCache();
        parent::afterSave($insert, $changedAttributes);
    }

    public function afterDelete()
    {
        $this->deleteCache();
        parent::afterDelete();
    }

    public function deleteCache()
    {
        TagDependency::invalidate(Yii::$app->cache, \frontend\modules\car\models\CarColor::CACHE_KEY);
    }

}
