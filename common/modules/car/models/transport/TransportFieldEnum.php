<?php

namespace common\modules\car\models\transport;

use Yii;

/**
 * This is the model class for table "{{%transport_field_enum}}".
 *
 * @property integer $enum_id
 * @property integer $type_has_field_id
 * @property string $value
 *
 * @property TransportTypeHasField $typeHasField
 */
class TransportFieldEnum extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transport_field_enum}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_has_field_id', 'value'], 'required'],
            [['type_has_field_id'], 'integer'],
            [['value'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'enum_id'           => 'Enum ID',
            'type_has_field_id' => 'Type Has Field ID',
            'value'             => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeHasField()
    {
        return $this->hasOne(TransportTypeHasField::className(), ['id' => 'type_has_field_id']);
    }

    public static function batchInsert(array $arValue, $linkId)
    {
        $insertValue = [];
        $connection = Yii::$app->db;

        foreach ($arValue as $item) {
            $insertValue[] = [$linkId, $item];
        }

        if (!empty($insertValue)) {
            return $connection->createCommand()->batchInsert(self::tableName(), ['type_has_field_id', 'value'],
                $insertValue)->execute();
        }

        return false;
    }
}
