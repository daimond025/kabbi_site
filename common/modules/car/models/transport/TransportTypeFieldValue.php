<?php

namespace common\modules\car\models\transport;

use common\modules\car\models\Car;
use Yii;

/**
 * This is the model class for table "{{%transport_type_field_value}}".
 *
 * @property integer $id
 * @property integer $has_field_id
 * @property integer $car_id
 * @property integer $value_int
 * @property string $value_string
 * @property string $value_text
 *
 * @property Car $car
 * @property TransportTypeHasField $hasField
 */
class TransportTypeFieldValue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transport_type_field_value}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['has_field_id'], 'required'],
            [['has_field_id', 'car_id', 'value_int'], 'integer'],
            [['value_text'], 'string'],
            [['value_string'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'has_field_id' => 'Has Field ID',
            'car_id'       => 'Car ID',
            'value_int'    => 'Value Int',
            'value_string' => 'Value String',
            'value_text'   => 'Value Text',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::className(), ['car_id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHasField()
    {
        return $this->hasOne(TransportTypeHasField::className(), ['id' => 'has_field_id']);
    }

    public static function getFieldNameByType($type)
    {
        return $type == TransportField::ENUM_TYPE ? 'value_int' : 'value_' . $type;
    }
}
