<?php

namespace common\modules\car\models\transport;

use common\modules\car\models\transport\TransportFieldEnum;
use common\modules\car\models\transport\TransportTypeHasField;
use Yii;

/**
 * This is the model class for table "{{%transport_field}}".
 *
 * @property integer $field_id
 * @property string $name
 * @property string $type
 *
 * @property TransportFieldEnum[] $transportFieldEnums
 * @property TransportTypeHasField[] $transportTypeHasFields
 */
class TransportField extends \yii\db\ActiveRecord
{
    const INTEGER_TYPE = 'int';
    const STRING_TYPE = 'string';
    const TEXT_TYPE = 'text';
    const ENUM_TYPE = 'enum';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transport_field}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'name'], 'required'],
            [['type'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'field_id' => 'Field ID',
            'name'     => 'Name',
            'type'     => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransportFieldEnums()
    {
        return $this->hasMany(TransportFieldEnum::className(), ['field_id' => 'field_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransportTypeHasFields()
    {
        return $this->hasMany(TransportTypeHasField::className(), ['field_id' => 'field_id']);
    }
}
