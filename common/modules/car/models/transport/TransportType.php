<?php

namespace common\modules\car\models\transport;

use common\modules\car\models\CarModel;
use common\modules\car\models\transport\TransportTypeHasField;
use Yii;

/**
 * This is the model class for table "{{%transport_type}}".
 *
 * @property integer $type_id
 * @property string $name
 *
 * @property \common\modules\car\models\CarModel[] $carModels
 * @property TransportTypeHasField[] $transportTypeHasFields
 */
class TransportType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transport_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type_id' => 'Type ID',
            'name' => 'Тип транспорта',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModels()
    {
        return $this->hasMany(CarModel::className(), ['type_id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransportTypeHasFields()
    {
        return $this->hasMany(TransportTypeHasField::className(), ['type_id' => 'type_id']);
    }
}
