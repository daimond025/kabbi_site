<?php

namespace common\modules\order\models;

/**
 * This is the model class for table "{{%order_status}}".
 *
 * @property integer $status_id
 * @property string  $name
 * @property string  $status_group
 * @property integer $dispatcher_sees
 *
 * @property Order[] $orders
 */
class OrderStatus extends \yii\db\ActiveRecord
{

    const STATUS_GROUP_0 = 'new';
    const STATUS_GROUP_1 = 'car_assigned';
    const STATUS_GROUP_2 = 'car_at_place';
    const STATUS_GROUP_3 = 'executing';
    const STATUS_GROUP_4 = 'completed';
    const STATUS_GROUP_5 = 'rejected';
    const STATUS_GROUP_6 = 'pre_order';
    const STATUS_GROUP_7 = 'warning';
    const STATUS_GROUP_8 = 'works';

    const STATUS_GROUP_NEW = 'new';
    const STATUS_GROUP_PRE_ORDER = 'pre_order';
    const STATUS_GROUP_EXECUTING = 'executing';
    const STATUS_GROUP_COMPLETED = 'completed';
    const STATUS_GROUP_REJECTED = 'rejected';
    const STATUS_GROUP_CAR_ASSIGNED = 'car_assigned';
    const STATUS_GROUP_CAR_AT_PLACE = 'car_at_place';

    const STATUS_NEW = 1;
    const STATUS_OFFER_ORDER = 2;
    const STATUS_WORKER_REFUSED = 3;
    const STATUS_FREE = 4;
    const STATUS_NOPARKING = 5;
    const STATUS_PRE = 6;
    const STATUS_PRE_GET_WORKER = 7;
    const STATUS_PRE_REFUSE_WORKER = 10;
    const STATUS_PRE_NOPARKING = 16;
    const STATUS_GET_WORKER = 17;
    const STATUS_WORKER_WAITING = 26;
    const STATUS_CLIENT_IS_NOT_COMING_OUT = 27;
    const STATUS_FREE_AWAITING = 29;
    const STATUS_NONE_FREE_AWAITING = 30;
    const STATUS_EXECUTING = 36;
    const STATUS_COMPLETED_PAID = 37;
    const STATUS_COMPLETED_NOT_PAID = 38;
    const STATUS_REJECTED = 39;
    const STATUS_NO_CARS = 40;
    const STATUS_OVERDUE = 52;
    const STATUS_WORKER_LATE = 54;
    const STATUS_EXECUTION_PRE = 55;
    const STATUS_PAYMENT_CONFIRM = 106;
    const STATUS_NO_CARS_BY_TIMER = 107;
    const STATUS_MANUAL_MODE = 108;
    const STATUS_DRIVER_IGNORE_ORDER_OFFER = 109;
    const STATUS_WAITING_FOR_PAYMENT = 110;
    const STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_SOFT = 111;
    const STATUS_WORKER_ASSIGNED_AT_PRE_ORDER_HARD = 112;
    const STATUS_WORKER_ASSIGNED_AT_ORDER_SOFT = 113;
    const STATUS_WORKER_ASSIGNED_AT_ORDER_HARD = 114;
    const STATUS_REFUSED_ORDER_ASSIGN = 115;
    const STATUS_CANCELED_BY_WORKER = 120;

    const CACHE_KEY = 'tbl_order_status';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_status}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['dispatcher_sees'], 'integer'],
            [['name', 'status_group'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'status_id'       => 'Status ID',
            'name'            => 'Name',
            'status_group'    => 'Status Group',
            'dispatcher_sees' => 'Dispatcher Sees',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['status_id' => 'status_id']);
    }

}
