<?php

namespace common\modules\order\models\ar;

use yii\db\ActiveRecord;


/**
 * This is the model class for table "{{%order_track}}".
 *
 * @property integer $track
 * @property string  $tracking
 * @property integer $order_id
 * @property integer $time
 *
 */
class OrderTrack extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_track}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tracking', 'order_id'], 'required'],
            [['tracking'], 'string'],
            [['order_id', 'time'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'track'    => 'Track',
            'tracking' => 'Tracking',
            'order_id' => 'Order ID',
            'time'     => 'Time',
        ];
    }

}
