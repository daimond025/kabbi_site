<?php

namespace common\modules\order\models;

use common\modules\order\models\ar\mongodb\OrderTrack;

/**
 * Class OrderTrackService
 * @package common\modules\order\models
 */
class OrderTrackService
{
    /**
     * Getting order track
     *
     * @param int $orderId
     *
     * @return array
     */
    public function getTrack($orderId)
    {
        $record = OrderTrack::find()
            ->where(['order_id' => $orderId])
            ->asArray()
            ->one();

        return empty($record['tracking']) ? [] : $record['tracking'];
    }
}