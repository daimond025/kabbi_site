<?php

namespace common\services\user\access;


use frontend\modules\tenant\models\UserDefaultRight;
use yii\rbac\ManagerInterface;
use yii\web\IdentityInterface;
use Yii;

class PermissionService
{

    protected $authManager;

    public function __construct(ManagerInterface $authManager)
    {
        $this->authManager = $authManager;
    }


    public function appointmentPermissionsByPositions(IdentityInterface $user, $positionId)
    {
        $permissions = UserDefaultRight::getDefaultRightSettingByPosition($positionId);

        $this->assignPermissions($user, $permissions);
    }


    public function assignPermissions($user, $permissions)
    {
        if (!empty($permissions) && is_array($permissions)) {

            foreach ($permissions as $name => $permission) {
                if ($permission == 'off') {
                    continue;
                }

                $permissionName = $permission == 'read' ? $permission . '_' . $name : $name;
                $permissionRbac = $this->authManager->getPermission($permissionName);
                if ($permissionRbac) {
                    $this->authManager->assign($permissionRbac, $user->user_id);
                } else {
                    Yii::error('Cant find permission RBAC by name: ' . $permissionName . '. For user: ' . $user->user_id);
                }

            }
        }
    }
}