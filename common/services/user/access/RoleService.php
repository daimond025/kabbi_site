<?php

namespace common\services\user\access;


use yii\di\Instance;
use yii\rbac\DbManager;
use yii\web\IdentityInterface;

class RoleService
{


    const ADMIN = 7;
    const BRANCH_DIRECTOR = 11;
    const POSITION_OPERATOR = 6;
    const POSITION_MAIN_OPERATOR = 8;
    const POSITION_PARTNER_EMPLOYEE = 13;
    const POSITION_EXTRINSIC_DISPATCHER = 14;

    const USER_ROLE_1 = 'top_manager';
    const USER_ROLE_2 = 'branch_director';
    const USER_ROLE_3 = 'staff';
    const USER_ROLE_4 = 'staff_company';
    const USER_ROLE_5 = 'extrinsic_dispatcher';

    protected $authManager;
    protected $permissionService;


    public function __construct()
    {
        $this->authManager = Instance::ensure([
            'class'           => DbManager::className(),
            'itemTable'       => '{{%auth_item}}',
            'itemChildTable'  => '{{%auth_item_child}}',
            'assignmentTable' => '{{%auth_assignment}}',
            'ruleTable'       => '{{%auth_rule}}',
        ]);

        $this->permissionService = new PermissionService($this->authManager);

    }


    public function setRoleByPosition(IdentityInterface $user, $positionId)
    {

        $this->authManager->revokeAll($user->user_id);

        $roleName = $this->getUserRoleByPosition($positionId);
        $role = $this->authManager->getRole($roleName);
        $this->authManager->assign($role, $user->user_id);

        $this->permissionService->appointmentPermissionsByPositions($user, $positionId);

    }



    public function getUserRoleByPosition($position_id)
    {
        if (in_array($position_id, self::getTopManagerPositionIdList())) {
            return self::USER_ROLE_1;
        } elseif ($position_id == self::BRANCH_DIRECTOR) {
            return self::USER_ROLE_2;
        } elseif (in_array($position_id, self::getStaffCompanyPositionIdList())) {
            return self::USER_ROLE_4;
        } elseif (in_array($position_id, self::getExtrinsicDispatcherPositionIdList())) {
            return self::USER_ROLE_5;
        } else {
            return self::USER_ROLE_3;
        }
    }

    public function getTopManagerPositionIdList()
    {
        return [1, 2, 3, 4, self::ADMIN];
    }

    public function getStaffPositionIdList()
    {
        return [5, 6, 8, 9, 10];
    }

    public function getStaffCompanyPositionIdList()
    {
        return [13];
    }

    public function getExtrinsicDispatcherPositionIdList()
    {
        return [14];
    }
}