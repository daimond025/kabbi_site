<?php


namespace common\services;


use common\models\Module;

class ModuleService
{
    /**
     * @return array
     */
    public function getAll()
    {
        $modules = Module::find()->select(['id', 'name', 'description', 'active'])->asArray()->all();

        return $this->getResult($modules);
    }

    /**
     * @param $active 1|0
     * @return array
     */
    public function getAllByActive($active)
    {
        $modules = Module::find()->active($active)->select(['id', 'name', 'description', 'active'])->asArray()->all();

        return $this->getResult($modules);
    }

    /**
     * @param array $modules
     * @return array
     */
    private function getResult(array $modules)
    {
        return array_map(function ($item) {
            $item['name'] = t('gootax_modules', $item['name']);

            return $item;
        }, $modules);
    }
}