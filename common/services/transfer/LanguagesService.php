<?php

namespace common\services\transfer;


class LanguagesService
{

    public function getSupportedLanguages()
    {
        $languages = app()->params['supportedLanguages'];
        array_walk($languages, function (&$value, $key) {
            $value = t('language', $value, null, $key);
        });

        return $languages;
    }


}