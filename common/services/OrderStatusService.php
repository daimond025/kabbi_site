<?php

namespace common\services;

use yii\db\Query;

/**
 * Class OrderStatusService
 * @package common\services
 */
class OrderStatusService
{
    const TRANSLATE_CATEGORY = 'order_status';

    private static $statuses;

    /**
     * Getting order statuses
     * @return array
     */
    private static function getStatuses()
    {
        $statuses = (new Query())
            ->from('tbl_order_status')
            ->select('name')
            ->indexBy('status_id')
            ->column();

        return empty($statuses) ? [] : $statuses;
    }

    /**
     * Translate status
     *
     * @param int    $statusId
     * @param int    $positionId
     * @param string $language
     *
     * @return string
     */
    public static function translate($statusId, $positionId = null, $language = null)
    {
        if (self::$statuses === null) {
            self::$statuses = self::getStatuses();
        }

        if (!isset(self::$statuses[$statusId])) {
            return "Unknown order status (status={$statusId})";
        }

        return self::translateByName(self::$statuses[$statusId], $positionId, $language);
    }

    /**
     * Translate status by name
     *
     * @param string $name
     * @param int    $positionId
     * @param string $language
     *
     * @return string
     */
    public static function translateByName($name, $positionId = null, $language = null)
    {
        return \Yii::t(
            self::TRANSLATE_CATEGORY,
            $name, // implode(':', array_filter([$name, $positionId])),
            null,
            $language
        );
    }
}
