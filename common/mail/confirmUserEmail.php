<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user app\modules\user\models\User */
?>

<?php

$text = 'Dear {name},<br>';
$text .= 'Thank you for registering.<br>';
$text .= 'Below you will find your activation link that you can use to activate your account. Please click on the Activation Link {link}<br>';
$text .= 'Then, you will be able to log in and begin using your account.<br>';
$text .= 'Your password: {password}<br>';

?>

<p><?= t('email',
        $text,
        [
            'name'     => Html::encode($data['NAME']),
            'link'     => Html::a($data['URL'], $data['URL']),
            'password' => isset($data['PASSWORD']) ? $data['PASSWORD'] : null,
        ], $data['LANGUAGE']) ?></p>