<?php

use yii\helpers\Html;
?>

<p><?= t('email', 'Hello', NULL, $data['LANGUAGE']) ?>!</p>

<p><?= t('email', 'You have a new unread ticket!', NULL, $data['LANGUAGE']) ?>
    <br>
    <?= t('email', 'Please, ', NULL, $data['LANGUAGE']) ?><a href="<?= $data['LINK'] ?>"><?= t('email', 'click here ', NULL, $data['LANGUAGE']) ?></a><?= t('email', 'to read and give an answer', NULL, $data['LANGUAGE']) ?></p>
