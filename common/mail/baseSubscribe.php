<?php

use yii\helpers\Html;

?>

<p><?=t('email', 'Hello', NULL, $data['LANGUAGE'])?>, <?= Html::encode($data['NAME']) ?>!</p>

<p><?= Html::encode($data['MESSAGE']); ?></p>

<p><?= Html::a($data['LINK-TITLE'], $data['URL']) ?></p>
