<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
?>
<p><?= t('email', 'Hello', NULL, $data['LANGUAGE']) ?>, <?= Html::encode($data['NAME']) ?>!</p>
<p><?= t('email', 'Thank you for your registration in Gootax', NULL, $data['LANGUAGE']) ?>.</p>
<p><?= t('email', 'Your gootax', NULL, $data['LANGUAGE']) ?> - <?= Html::a($data['URL'], $data['URL']) ?></p>
<p><?= t('email', 'Login', NULL, $data['LANGUAGE']) ?>: <?= $data['LOGIN'] ?></p>
<p><?= t('email', 'Password', NULL, $data['LANGUAGE']) ?>: <?= $data['PASSWORD'] ?></p>
<p><?= t('email', 'You have questions? Please contact us:', NULL, $data['LANGUAGE']) ?> info@gootax.pro</p>
<p><?= t('email', 'Enjoy!', NULL, $data['LANGUAGE']) ?></p>