<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */

$contactPhone = $this->params['contact-phone'];
$contactEmail = $this->params['contact-email'];
$language     = $this->params['language'];

?>

<?php $this->beginPage() ?>

    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html>

    <head>
        <title><?= Html::encode($this->title) ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <!--[if !mso]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <!--<![endif]-->
        <style type="text/css">
            .ReadMsgBody {
                width: 100%;
                background-color: #f9f7f7;
            }

            .ExternalClass {
                width: 100%;
                background-color: #f9f7f7;
            }

            body {
                width: 100%;
                background-color: #f9f7f7;
                margin: 0;
                padding: 10px 0;
                -webkit-font-smoothing: antialiased;
                font-family: Arial, Times, serif
            }

            table {
                border-collapse: collapse !important;
                mso-table-lspace: 0pt;
                mso-table-rspace: 0pt;
            }

            a {
                color: #1867a3;
            }

            @-ms-viewport {
                width: device-width;
            }

            @media only screen and (max-width: 639px) {
                .wrapper {
                    width: 100% !important;
                    padding: 0 !important;
                }
            }

            @media only screen and (max-width: 480px) {
                body {
                    width: 640px !important;
                    min-width: 640px !important;
                }
            }
        </style>
        <?php $this->head() ?>
    </head>

    <body marginwidth="0" marginheight="0" leftmargin="0" topmargin="0"
          style="background-color:#f9f7f7;  font-family:Arial,serif; margin:0; padding:0 0; min-width: 100%; -webkit-text-size-adjust:none; -ms-text-size-adjust:none;">
    <!--[if !mso]><!-- -->
    <img style="min-width:640px; display:block; margin:0; padding:0" class="mobileOff" width="640" height="1"
         src="http://foto.gootax.ru/img/spacer.gif">
    <!--<![endif]-->
    <?php $this->beginBody() ?>
    <table class="container" width="100%" cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="#f9f7f7"
           st-sortable="body1" style="background-color:#f9f7f7;">
        <tr>
            <td height="20" style="font-size:10px; line-height:10px;"></td>
            <!-- Spacer -->
        </tr>
        <tr>
            <td width="100%" valign="top" align="center">
                <table width="640" class="container" cellpadding="0" cellspacing="0" border="0" align="center"
                       st-sortable="body" style="border-width: 1px; border-color: #e0dede; border-style: solid;">
                    <tr>
                        <td width="100%" valign="top" align="center">
                            <!-- START HEADER -->
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center"
                                   st-sortable="header">
                                <tr>
                                    <td width="100%" valign="top" align="center">
                                        <!-- Start Wrapper  -->
                                        <table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper"
                                               bgcolor="#FFFFFF">
                                            <tr>
                                                <td align="center">
                                                    <!-- Start Container  -->
                                                    <table width="640" cellpadding="0" cellspacing="0" border="0"
                                                           class="container">
                                                        <tr>
                                                            <td height="20"
                                                                style="font-size:10px; line-height:10px;"></td>
                                                            <!-- Spacer -->
                                                        </tr>
                                                        <tr>
                                                            <td width="320" class="mobile" style="padding-left:15px;">
                                                                <img src="http://foto.gootax.ru/img/logo.png"
                                                                     width="200" height="50"
                                                                     style="margin:0; padding:0; border:none; display:block;"
                                                                     border="0" class="centerClass" alt=""
                                                                     st-image="image"/>
                                                            </td>
                                                            <td height="10" style="font-size:10px; line-height:10px;"
                                                                class="mobileOn"></td>
                                                            <td width="320" class="mobile"
                                                                style="font-family:arial; font-size:14px; line-height:18px;"
                                                                align="left">
                                                                <table width="320" class="container" cellpadding="0"
                                                                       cellspacing="0" border="0" align="center">
                                                                    <tr>
                                                                        <td align="left"
                                                                            style="font-family:Verdana, Arial, sans serif; font-size: 14px; color: #4d4d4d; line-height:18px; padding:0 20px;">
                                                                            <?= Html::encode(Yii::t('email',
                                                                                'Cloud system for automatization a taxi, couriers and services',
                                                                                null, $language)) ?>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="20"
                                                                style="font-size:10px; line-height:10px;"></td>
                                                            <!-- Spacer -->
                                                        </tr>
                                                    </table>
                                                    <!-- Start Container  -->
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- End Wrapper  -->
                                    </td>
                                </tr>
                            </table>
                            <!-- END HEADER -->

                            <?= $content ?>

                            <!-- START FOOTER -->
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center"
                                   st-sortable="footer">
                                <tr>
                                    <td width="100%" valign="top" align="center">
                                        <!-- Start Wrapper  -->
                                        <table width="640" cellpadding="0" cellspacing="0" border="0" class="wrapper"
                                               bgcolor="#ffffff">
                                            <tr>
                                                <td align="center">
                                                    <!-- Start Container  -->
                                                    <table width="640" cellpadding="0" cellspacing="0" border="0"
                                                           class="container" align="center">
                                                        <tr>
                                                            <td height="20"
                                                                style="font-size:10px; line-height:10px;"></td>
                                                            <!-- Spacer -->
                                                        </tr>
                                                        <tr>
                                                            <td width="65" class="mobile"
                                                                style="font-family:arial; font-size:14px; line-height:18px; color:#000000; padding-left:20px;"
                                                                st-content="footer">
                                                                <img src="http://foto.gootax.ru/img/logo_footer.png"
                                                                     width="27" height="27"
                                                                     style="margin:0; padding:0; border:none; display:block;"
                                                                     border="0" class="centerClass" alt=""
                                                                     st-image="image"/>
                                                            </td>
                                                            <td height="10" style="font-size:10px; line-height:10px;"
                                                                class="mobileOn"></td>
                                                            <td width="85" class="mobile"
                                                                style="font-family:arial; font-size:14px; line-height:18px; color:#000000; padding-right:20px;"
                                                                align="left" st-content="footer">
                                                                <a href="https://gootax.pro/" target="_blank" alias=""
                                                                   style="font-size:14px; line-height:18px; color:#1867a3; text-decoration:underline;"><nobr>gootax.pro</nobr></a>
                                                            </td>
                                                            <td height="10" style="font-size:10px; line-height:10px;"
                                                                class="mobileOn"></td>
                                                            <td width="160" class="mobile"
                                                                style="font-family:arial; font-size:14px; line-height:18px; color:#000000; padding-right:20px;"
                                                                align="left" st-content="footer">
                                                                <a href="mailto:<?= $contactEmail ?> target=" _blank"
                                                                alias=""
                                                                style="font-size:14px; line-height:18px; color:#1867a3;
                                                                text-decoration:underline;"><nobr><?= $contactEmail ?></nobr></a>
                                                            </td>
                                                            <td height="10" style="font-size:10px; line-height:10px;"
                                                                class="mobileOn"></td>
                                                            <td width="170" class="mobile"
                                                                style="font-family:arial; font-size:16px; line-height:18px; color:#000000; padding-right:20px;"
                                                                align="left" st-content="footer">
                                                                <nobr><?= $contactPhone ?></nobr>
                                                            </td>
                                                            <td height="10" style="font-size:10px; line-height:10px;"
                                                                class="mobileOn"></td>
                                                            <td width="160" class="mobile"
                                                                style="font-family:arial; font-size:16px; line-height:18px; color:#000000; padding-right:20px;"
                                                                align="left" st-content="footer">
                                                                <table width="160" cellpadding="0" cellspacing="0"
                                                                       border="0" class="container icons">
                                                                    <tr>
                                                                        <td width="40"
                                                                            style="font-family:arial; font-size:12px; line-height:18px;"
                                                                            align="center">
                                                                            <a href="https://www.facebook.com/gootax.pro"
                                                                               target="_blank" alias=""
                                                                               style="font-size:25px; line-height:25px; color:#1867a3; text-decoration:none;">
                                                                                <img
                                                                                    src="http://foto.gootax.ru/img/fb_icon.png"
                                                                                    width="25" height="25"
                                                                                    style="margin:0; padding:0; border:none; display:block;"
                                                                                    border="0" class="centerClass"
                                                                                    alt="" st-image="image"/>
                                                                            </a>
                                                                        </td>
                                                                        <td width="40"
                                                                            style="font-family:arial; font-size:12px; line-height:18px;"
                                                                            align="center">
                                                                            <a href="https://www.linkedin.com/company/gootax"
                                                                               target="_blank" alias=""
                                                                               style="font-size:25px; line-height:25px; color:#1867a3; text-decoration:none;">
                                                                                <img
                                                                                    src="http://foto.gootax.ru/img/li_icon.png"
                                                                                    width="25" height="25"
                                                                                    style="margin:0; padding:0; border:none; display:block;"
                                                                                    border="0" class="centerClass"
                                                                                    alt="" st-image="image"/>
                                                                            </a>
                                                                        </td>
                                                                        <td width="40"
                                                                            style="font-family:arial; font-size:12px; line-height:18px;"
                                                                            align="center">
                                                                            <a href="https://twitter.com/GootaxPro"
                                                                               target="_blank" alias=""
                                                                               style="font-size:25px; line-height:25px; color:#1867a3; text-decoration:none;">
                                                                                <img
                                                                                    src="http://foto.gootax.ru/img/tw_icon.png"
                                                                                    width="25" height="25"
                                                                                    style="margin:0; padding:0; border:none; display:block;"
                                                                                    border="0" class="centerClass"
                                                                                    alt="" st-image="image"/>
                                                                            </a>
                                                                        </td>
                                                                        <td width="40"
                                                                            style="font-family:arial; font-size:12px; line-height:18px;"
                                                                            align="center">
                                                                            <a href="https://vk.com/gootax"
                                                                               target="_blank" alias=""
                                                                               style="font-size:25px; line-height:25px; color:#1867a3; text-decoration:none;">
                                                                                <img
                                                                                    src="http://foto.gootax.ru/img/vk_icon.png"
                                                                                    width="25" height="25"
                                                                                    style="margin:0; padding:0; border:none; display:block;"
                                                                                    border="0" class="centerClass"
                                                                                    alt="" st-image="image"/>
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="20"
                                                                style="font-size:10px; line-height:10px;"></td>
                                                            <!-- Spacer -->
                                                        </tr>
                                                    </table>
                                                    <!-- Start Container  -->
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- End Wrapper  -->
                                    </td>
                                </tr>
                            </table>
                            <!-- END FOOTER -->
                        </td>
                    </tr>
                </table>
                <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" st-sortable="full-text">
                    <tr>
                        <td width="100%" valign="top" align="center">
                            <!-- Start Wrapper -->
                            <table width="640" cellpadding="0" cellspacing="0" align="center" border="0"
                                   class="wrapper">
                                <tbody>
                                <tr>
                                    <td align="center">
                                        <!-- Start Container -->
                                        <table width="640" cellpadding="0" cellspacing="0" align="center" border="0"
                                               class="container">
                                            <tr>
                                                <td height="20" style="font-size:20px; line-height:20px;"></td>
                                                <!-- Spacer -->
                                            </tr>
                                            <tr>
                                                <td align="center"
                                                    style="font-family:Verdana, Arial, sans serif; font-size: 12px; color: #808080; line-height:14px; padding:0 15px;"
                                                    st-content="full-text">
                                                    <?= HtmlPurifier::process(Yii::t('email',
                                                        'You received this message because you are subscribed to the newsletter.<br/>If you do not want to receive the newsletter, please contact us.',
                                                        null, $language)); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="20" style="font-size:20px; line-height:20px;"></td>
                                                <!-- Spacer -->
                                            </tr>
                                        </table>
                                        <!-- End Container -->
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <!-- End Wrapper -->
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- END FULL-TEXT -->

    <?php $this->endBody() ?>
    </body>

    </html>

<?php $this->endPage() ?>