<?php
use yii\helpers\Html;

echo Html::tag('h1', Html::encode($data['TITLE']));

if (!empty($data['FIELDS'])) {
    foreach ($data['FIELDS'] as $field) {
        echo Html::tag('p', $field);
    }
}
