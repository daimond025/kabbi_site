<?php
$data = array( 'message' => 'Hello World! Hello World!Hello World!' );

// The recipient registration tokens for this notification
// https://developer.android.com/google/gcm/
$ids = array( "cwxBx2hKz4Y:APA91bG6-Vzy4R68CtYXSvHfiOh6-7Ax5ZaOmU7fCesnK-G-MpU7SPnMmbDmQBQiCm8YUQCE4Z3fyJmcnS1KAE4w3jKnF--SlP4Yul7_rMCGSEIXlSN2gl4MS832bw6kRiOeA0mXWocb");


        // Insert real GCM API key from the Google APIs Console
        // https://code.google.com/apis/console/        
$apiKey = "AIzaSyCZWyU4wRJBuPHxOOtSV9--jQm0Hy57N7A";

        // Set POST request body payload
$post = array(
    'registration_ids'  => $ids,
    'data'              => $data,
);

        // Set CURL request headers
        $headers = array(
            'Authorization: key=' . $apiKey,
            'Content-Type: application/json'
        );

        // Initialize curl handle
        $ch = curl_init();

        // Set URL to GCM push endpoint
        curl_setopt( $ch, CURLOPT_URL, 'https://gcm-http.googleapis.com/gcm/send' );

        // Set request method to POST
        curl_setopt( $ch, CURLOPT_POST, true );

        // Set custom request headers
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

        // Get the response back as string instead of printing it
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

        // Set JSON post data
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $post ) );

        // Actually send the push
        $result = curl_exec( $ch );

        // Error handling
        if ( curl_errno( $ch ) )
        {
            echo 'GCM error: ' . curl_error( $ch );
        }

        // Close curl handle
        curl_close( $ch );

        // Debug GCM response
        var_dump( $result);