<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

class MapsAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $jsOptions = ['position' => \yii\web\View::POS_END];
    public $css = [
    ];
    public $js = [
        '//api-maps.yandex.ru/2.0/?load=package.map&lang=ru-RU',
        '//maps.google.com/maps/api/js?v=3&key=AIzaSyCDAe-oozmoTkrxFBzuoANCHSB_aOwckUE',
    ];
    public $depends = [
    ];

}
