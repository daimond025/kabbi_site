<?php

namespace backend\assets;

use yii\web\AssetBundle;

class DateRangePickerAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/daterangepicker/daterangepicker-bs3.css',
    ];
    public $js = [
        'js/plugins/daterangepicker/daterangepicker.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}