<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use app\models\MillionLog;
use app\modules\setting\models\MillionErrorCode;
use app\models\MillionLogSearch;

/**
 * Site controller
 */
class MillionLogController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function actionIndex($start = null, $end = null) {
        
        $errorCode = new ActiveDataProvider([
            'query' => MillionErrorCode::find(),
            
        ]);
        
        $errorLogFilter = new MillionLog();
        
        $params = Yii::$app->request->get();

        $errorLog = $errorLogFilter->search($params,$start,$end);

        return $this->render('index', [
                    'errorCode' => $errorCode,
                    'errorLog' => $errorLog,
                    'errorLogFilter' => $errorLogFilter,
        ]);
    }
    
}
