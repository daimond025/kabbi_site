<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use app\models\ElecsnetLog;
use app\modules\setting\models\ElecsnetErrorCode;
use app\models\ElecsnetLogSearch;

/**
 * Site controller
 */
class ElecsnetLogController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function actionIndex($start = null, $end = null) {
        
        $errorCode = new ActiveDataProvider([
            'query' => ElecsnetErrorCode::find(),
        ]);
        
        $errorLogFilter = new ElecsnetLog();
        
        $params = Yii::$app->request->get();

        $errorLog = $errorLogFilter->search($params,$start,$end);

        return $this->render('index', [
                    'errorCode' => $errorCode,
                    'errorLog' => $errorLog,
                    'errorLogFilter' => $errorLogFilter,
        ]);
    }

}
