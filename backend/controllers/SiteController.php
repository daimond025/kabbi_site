<?php

namespace backend\controllers;

use app\models\MainPage;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use backend\models\LoginForm;
use yii\filters\VerbFilter;
use yii\base\ErrorHandler;
use app\models\MainPageAdmin;


/**
 * Site controller
 */
class SiteController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (404 == app()->errorHandler->exception->statusCode || $this->action->id == 'error') {
            $this->layout = 'error';
            return $this->render('error', ['message' => app()->errorHandler->exception->statusCode]);
        }
        return parent::beforeAction($action);
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $izhTimeOffset = +4 * 3600;

        $begin = mktime(0, 0, 0) + $izhTimeOffset;
        $end = mktime(24, 0, 0) + $izhTimeOffset;

        $mainPage = new MainPageAdmin($begin, $end);

        $demoDownloaders = $mainPage->getDemoDownload();

        $planDownloaders = $mainPage->getPlanDownload();

        $tenants = $mainPage->getTenants();

        $tickets = $mainPage->getTickets();

        $payments = $mainPage->getPayments();

        return $this->render('index', [
            'tickets' => $tickets,
            'tenants' => $tenants,
            'payments' => $payments,
            'plan_downloaders' => $planDownloaders,
            'demo_downloaders' => $demoDownloaders
        ]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'login';
        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
