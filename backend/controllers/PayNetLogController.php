<?php

namespace backend\controllers;

use app\models\PayNetLogSearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * PayNetLog controller
 */
class PayNetLogController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function actionIndex($start = null, $end = null)
    {
        $params = Yii::$app->request->get();
        $searchModel = new PayNetLogSearch();
        $dataProvider = $searchModel->search($params, $start, $end);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

}
