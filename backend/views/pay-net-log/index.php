<?php

use yii\helpers\Html;
use yii\grid\GridView;
/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $searchModel \app\models\PayNetLogSearch
 **/

use app\modules\setting\assets\SettingAsset;
use yii\helpers\ArrayHelper;
use app\models\PayNetLogSearch;

$bundle = SettingAsset::register($this);

$this->registerJsFile($bundle->baseUrl . '/log-widgets.js', ['depends' => 'backend\assets\DateRangePickerAsset']);
$this->title                   = 'Лог PayNet';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="input-group">
    <button type="button" class="btn btn-default pull-right" id="daterange">
        <i class="fa fa-calendar"></i> Выбрать период
        <i class="fa fa-caret-down"></i>
    </button>
</div>



<div class="elecsnet-log-index">
    <h1><?= Html::encode($this->title) ?></h1>
</div>

<?php yii\widgets\Pjax::begin(); ?>
<section id="payments">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'class'         => 'yii\grid\SerialColumn',
                'headerOptions' => ['style' => 'width:80px'],
            ],
            [
                'attribute'     => 'log_id',
                'headerOptions' => ['style' => 'width:80px'],
            ],
            [
                'attribute'     => 'serviceId',
                'headerOptions' => ['style' => 'width:100px'],
            ],
            [
                'attribute'     => 'type',
                'filter'        => PayNetLogSearch::getTypeList(),
                'headerOptions' => ['style' => 'width:200px'],
                'content'       => function (PayNetLogSearch $model) {
                    return ArrayHelper::getValue(PayNetLogSearch::getTypeList(), $model->type);
                }
            ],
            [
                'attribute' => 'data',
                'content'   => function (PayNetLogSearch $model) {
                    return $this->render('_data', ['model' => $model]);
                },
            ],
            [
                'attribute'     => 'result',
                'filter'        => PayNetLogSearch::getResultList(),
                'headerOptions' => ['style' => 'width:300px'],
                'content'       => function (PayNetLogSearch $model) {
                    return ArrayHelper::getValue(PayNetLogSearch::getResultList(), $model->result);
                }
            ],
            [
                'attribute'     => 'create_time',
                'headerOptions' => ['style' => 'width:200px'],
                'content'       => function (PayNetLogSearch $model) {
                    return app()->formatter->asDatetime($model->create_time);
                },
            ],
        ],
    ]);?>
</section>

<?php yii\widgets\Pjax::end(); ?>

