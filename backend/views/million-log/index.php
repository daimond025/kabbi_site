<?php

use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
use app\modules\setting\assets\SettingAsset;

$bundle = SettingAsset::register($this);

$this->registerJsFile($bundle->baseUrl . '/log-widgets.js', ['depends' => 'backend\assets\DateRangePickerAsset']);
$this->title = 'Лог Миллиона';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="input-group">
    <button type="button" class="btn btn-default pull-right" id="daterange">
        <i class="fa fa-calendar"></i> Выбрать период
        <i class="fa fa-caret-down"></i>
    </button>

    <button type="button" class="btn btn-default pull-right" id="error_code">
        <i class="fa fa-th-list"></i> Коды ошибок
    </button>
</div>

<div id="helper" style="display: none">
        <?=
        GridView::widget([
            'dataProvider' => $errorCode,
            'columns' => [
                'code',
                'description',
            ],
        ]);
        ?>
</div>
                
<div class="elecsnet-log-index">
    <h1><?= Html::encode($this->title) ?></h1>
</div>

<?php yii\widgets\Pjax::begin(); ?>
<section id="payments">
    <?= GridView::widget([
        'dataProvider' => $errorLog,
        'filterModel' => $errorLogFilter,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'type',
                'filter' => [
                    'check' => 'Check',
                    'process' => 'Process',
                    'status' => 'Status',
                    'underfined' => 'Underfined'
                ],
                'options' => [
                    'style' => 'width:100px',
                ],
            ],
            [
                'attribute' => 'result',
                'options' => [
                    'style' => 'width:100px',
                ],
            ],
            [
                'attribute' => 'apikey',
                'options' => [
                    'style' => 'width:150px',
                ],
            ],
            'callsign',
            'phone',
            'sum',
            [
                'attribute' => 'data',
                'filter' => false,
                'options' => [
                    'style' => 'width:240px',
                ],
            ],
            [
                'attribute' => 'create_time',
                'format' => 'datetime',
                'filter' => false,
            ]
        ],
    ]);?>
</section>
    
<?php yii\widgets\Pjax::end(); ?>

