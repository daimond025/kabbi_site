<?php

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<link rel="shortcut icon" href="/img/favicon.ico">
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<body class="skin-blue">
<!-- header logo: style can be found in header.less -->
<header class="header">
    <a href="<?= app()->homeUrl ?>" class="logo">
        <!-- Add the class icon to your logo image or logo icon to add the margining -->
        Gootax
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-user"></i>
                        <span><?= user()->name ?> <i class="caret"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <?= Html::a('Профиль', ['/user/user/view', 'id' => user()->id],
                                    ['class' => 'btn btn-default btn-flat']) ?>
                            </div>
                            <div class="pull-right">
                                <?= Html::a('Выйти', '@web/site/logout', ['class' => 'btn btn-default btn-flat']) ?>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <?=
            backend\widgets\Menu::widget([
                'options' => ['class' => 'sidebar-menu'],
                'labelTemplate' => '<a href="#">{icon}<span>{label}</span>{right-icon}{badge}</a>',
                'linkTemplate' => '<a href="{url}">{icon}<span>{label}</span>{right-icon}{badge}</a>',
                'submenuTemplate' => "\n<ul class=\"treeview-menu\">\n{items}\n</ul>\n",
                'activateParents' => true,
                'items' => [
                    [
                        'label' => 'Пользователи',
                        'icon' => '<i class="fa fa-users"></i>',
                        'options' => ['class' => 'treeview'],
                        'items' => [
                            ['label' => 'Список', 'url' => ['/user/user/']],
                            ['label' => 'Rbac', 'url' => ['/rbac'], 'visible' => Yii::$app->user->can('users')],
                        ],
                        'visible' => Yii::$app->user->can('usersRead'),
                    ],
                    [
                        'label' => 'Продающий сайт',
                        'icon' => '<i class="fa fa-users"></i>',
                        'options' => ['class' => 'treeview'],
                        'items' => [
                            [
                                'label' => 'Редактирование страниц',
                                'url' => ['/landing/page-content'],
                                'visible' => Yii::$app->user->can('landingPageEdit'),
                            ],
                            [
                                'label' => 'Скачавшие бизнес-план',
                                'url' => ['/landing/business-plan'],
                                'visible' => Yii::$app->user->can('landingDownLoads'),
                            ],
                            [
                                'label' => 'Скачавшие пакет документов',
                                'url' => ['/landing/docpack'],
                                'visible' => Yii::$app->user->can('landingDownLoads'),
                            ],
                            [
                                'label' => 'Заказали демонстрацию',
                                'url' => ['/landing/conversion'],
                                'visible' => Yii::$app->user->can('landingDownLoads'),
                            ],
                            [
                                'label' => 'Сообщество',
                                'icon' => '<i class="fa fa-angle-double-right"></i>',
                                'options' => ['class' => 'treeview'],
                                'items' => [
                                    [
                                        'label' => 'Пользователи',
                                        'url' => ['/landing/community/community-user'],
                                    ],
                                    [
                                        'label' => 'Посты',
                                        'url' => ['/landing/community/post'],
                                    ],
                                    [
                                        'label' => 'Разделы',
                                        'url' => ['/landing/community/section'],
                                    ],
                                    [
                                        'label' => 'Теги',
                                        'url' => ['/landing/community/tag'],
                                    ],
                                ],
                                'visible' => Yii::$app->user->can('landingCommunityRead'),
                            ],
                        ],
                        'visible' => Yii::$app->user->can('landingPageEdit') || Yii::$app->user->can('landingDownLoads') ||
                            Yii::$app->user->can('landingCommunityRead'),
                    ],
                    [
                        'label' => 'Арендаторы',
                        'icon' => '<i class="fa fa-user"></i>',
                        'options' => ['class' => 'treeview'],
                        'items' => [
                            [
                                'label' => 'Данные',
                                'url' => ['/setting/tenant'],
                                'visible' => Yii::$app->user->can('tenantInfoRead'),
                            ],
                            [
                                'label' => t('module', 'Modules'),
                                'url' => ['/tenant/tariff/module'],
                                'visible' => Yii::$app->user->can('tenantRead'),
                            ],
                            [
                                'label' => t('tariff', 'Tariffs'),
                                'options' => ['class' => 'treeview'],
                                'items' => [
                                    [
                                        'label' => t('tariff', 'Tariffs'),
                                        'url' => ['/tenant/tariff/tariff'],
                                    ],
                                    [
                                        'label' => t('permission', 'Permissions'),
                                        'url' => ['/tenant/tariff/permission'],
                                    ],
                                    [
                                        'label' => t('tariff-additional-option', 'Tariff Additional Options'),
                                        'url' => ['/tenant/tariff/tariff-additional-option'],
                                    ],

                                    [
                                        'label' => t('one-time-service', 'One Time Services'),
                                        'url' => ['/tenant/tariff/one-time-service'],
                                    ],
                                    [
                                        'label' => t('discount', 'Discounts'),
                                        'url' => ['/tenant/tariff/discount'],
                                    ],
                                ],
                                'visible' => Yii::$app->user->can('tenantRead'),
                            ],

                            [
                                'label' => t('tenant-tariff', 'Tenant Tariffs'),
                                'url' => ['/tenant/tariff/tenant-tariff'],
                                'visible' => Yii::$app->user->can('tenantRead'),
                            ],
                            [
                                'label' => t('tenant-additional-option', 'Tenant Additional Options'),
                                'url' => ['/tenant/tariff/tenant-additional-option'],
                                'visible' => Yii::$app->user->can('tenantRead'),
                            ],
                            [
                                'label' => 'Счета на оплату',
                                'url' => ['/tenant/tariff/payment'],
                                'visible' => Yii::$app->user->can('tenantRead'),
                            ],
                            [
                                'label' => t('tenant', 'Tenant Account'),
                                'url' => ['/tenant/balance'],
                                'visible' => Yii::$app->user->can('tenantRead'),
                            ],

                            [
                                'label' => 'Заказы',
                                'url' => ['/setting/order-search'],
                                'visible' => Yii::$app->user->can('tenantOrders'),
                            ],
                        ],
                    ],
                    [
                        'label' => 'Логи',
                        'icon' => '<i class="fa fa-file-text-o"></i>',
                        'options' => ['class' => 'treeview'],
                        'items' => [
                            ['label' => 'Элекснет', 'url' => ['/elecsnet-log/index']],
                            ['label' => 'Миллион', 'url' => ['/million-log/index']],
                            ['label' => 'PayNet', 'url' => ['/pay-net-log/index']],
                            ['label' => 'Транзакции', 'url' => ['/log/transaction-log/index']],
                            ['label' => 'СМС', 'url' => ['/setting/sms-log']],
                            [
                                'label' => t('bank-card', 'Bank card'),
                                'url' => ['/log/card-payment/index'],
                            ],
                            [
                                'label' => t('tenant-notification', 'Tenant notifications'),
                                'url' => ['/log/tenant-notification/index'],
                            ],
                        ],
                        'visible' => Yii::$app->user->can('logs'),
                    ],
                    [
                        'label' => 'Статистика',
                        'icon' => '<i class="fa fa-bar-chart-o"></i>',
                        'options' => ['class' => 'treeview'],
                        'items' => [
                            ['label' => 'Кол-во регистраций', 'url' => ['/statistic/by-days']],
                            ['label' => 'Скачавшие бизнес-план', 'url' => ['/statistic/business-plan']],
                            ['label' => 'Скачавшие пакет документов', 'url' => ['/statistic/docpack']],
                            ['label' => 'Заказали демонстрацию', 'url' => ['/statistic/conversion']],
                            ['label' => 'Отчет по оплатам', 'url' => ['/statistic/payments-report']],
                            ['label' => 'Отчет по тарифам', 'url' => ['/statistic/tariff-report']],
                            ['label' => 'Отчет по продлению', 'url' => ['/statistic/renewal-report']],
                        ],
                        'visible' => Yii::$app->user->can('statistics'),
                    ],
                    [
                        'label' => 'Техподдержка',
                        'icon' => '<i class="fa fa-support"></i>',
                        'options' => ['class' => 'treeview'],
                        'items' =>
                            [
                                [
                                    'label' => 'Активные заявки',
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    'options' => ['class' => 'treeview'],
                                    'items' => [
                                        [
                                            'label' => 'Важные',
                                            'url' => ['/support/support/show-active', 'prior' => 'HIGHT'],
                                        ],
                                        [
                                            'label' => 'Средние',
                                            'url' => ['/support/support/show-active', 'prior' => 'MEDIUM'],
                                        ],
                                        [
                                            'label' => 'Низкие',
                                            'url' => ['/support/support/show-active', 'prior' => 'LOW'],
                                        ],
                                    ],
                                ],
                                [
                                    'label' => 'Закрытые заявки',
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    'url' => ['/support/support/show-closed'],
                                ],
                            ],
                        'visible' => Yii::$app->user->can('supportRead'),
                    ],
                    [
                        'label' => 'Служебные команды',
                        'icon' => '<i class="fa fa-ambulance"></i>',
                        'options' => ['class' => 'treeview'],
                        'items' => [
                            [
                                'label' => 'Снять водителя с заказа',
                                'url' => ['/support/service-command/remove-worker-from-order'],
                            ],
                            [
                                'label' => 'Сменить статус исполнителя',
                                'url' => ['/support/service-command/change-worker-status'],
                            ],
                        ],
                        'visible' => Yii::$app->user->can('serviceCommands'),
                    ],
                    [
                        'label' => 'Настройки',
                        'icon' => '<i class="fa fa-cogs"></i>',
                        'options' => ['class' => 'treeview'],
                        'items' =>
                            [
                                [
                                    'label' => 'Переводы',
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    'options' => ['class' => 'treeview'],
                                    'items' => [
                                        ['label' => 'Таблица фраз', 'url' => ['/setting/source-message']],
                                        ['label' => 'Таблица переводов', 'url' => ['/setting/translation']],
                                    ],
                                    'visible' => Yii::$app->user->can('translationsRead'),
                                ],
                                [
                                    'label' => 'Права должностей',
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    'url' => ['/setting/default-right'],
                                    'visible' => Yii::$app->user->can('tenantRightsRead'),
                                ],
                                [
                                    'label' => 'Гео база',
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    'options' => ['class' => 'treeview'],
                                    'items' => [
                                        ['label' => 'Страны', 'url' => ['/setting/country']],
                                        ['label' => 'Республики', 'url' => ['/setting/republic']],
                                        ['label' => 'Часовые пояса', 'url' => ['/setting/time']],
                                        ['label' => 'Города', 'url' => ['/setting/city']],
                                    ],
                                    'visible' => Yii::$app->user->can('geoRead'),
                                ],
                                [
                                    'label' => 'Исполнители',
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    'options' => ['class' => 'treeview'],
                                    'items' => [
                                        [
                                            'label' => 'Должности исполнителей',
                                            'url' => ['/setting/position'],
                                        ],
                                        [
                                            'label' => 'Поля',
                                            'url' => ['/setting/position-field'],
                                        ],
                                        [
                                            'label' => 'Поля должностей',
                                            'url' => ['/setting/position-has-field'],
                                        ],
                                        [
                                            'label' => 'Документы должностей',
                                            'url' => ['/setting/position-has-document'],
                                        ],
                                        [
                                            'label' => 'Документы',
                                            'url' => ['/setting/document'],
                                        ],
                                    ],
                                    'visible' => Yii::$app->user->can('employeeRead'),
                                ],
                                [
                                    'label' => 'Транспорт',
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    'options' => ['class' => 'treeview'],
                                    'items' => [
                                        [
                                            'label' => 'Тип',
                                            'icon' => '<i class="fa fa-angle-double-right"></i>',
                                            'options' => ['class' => 'treeview'],
                                            'items' => [
                                                ['label' => 'Список', 'url' => ['/setting/transport-type']],
                                                ['label' => 'Поля', 'url' => ['/setting/transport-field']],
                                                [
                                                    'label' => 'Привязка полей к типу',
                                                    'url' => ['/setting/transport-type-has-field'],
                                                ],
                                            ],
                                        ],
                                        ['label' => 'Марки', 'url' => ['/setting/car-brand/']],
                                        ['label' => 'Модели', 'url' => ['/setting/car-model']],
                                        ['label' => 'Классы', 'url' => ['/setting/car-class']],
                                        ['label' => 'Цвета', 'url' => ['/setting/car-color']],
                                        ['label' => 'Опции', 'url' => ['/setting/car-option']],
                                    ],
                                    'visible' => Yii::$app->user->can('transportRead'),
                                ],
                                [
                                    'label' => 'СМС серевер',
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    'options' => ['class' => 'treeview'],
                                    'items' => [
                                        ['label' => 'Общий список', 'url' => ['/setting/sms-server/']],
                                        ['label' => 'В работе', 'url' => ['/setting/admin-sms-server/index']],
                                    ],
                                    'visible' => Yii::$app->user->can('smsRead'),
                                ],

                                [
                                    'label' => 'Email',
                                    'icon'  => '<i class="fa fa-angle-double-right"></i>',
                                    'options' => ['class' => 'treeview'],
                                    'items' => [
                                        [
                                            'label' => 'Система -> Арендатор',
                                            'url'   => ['/setting/email/system'],
                                        ],
                                        [
                                            'label' => 'Арендатор -> Сотрудник',
                                            'url'   => ['/setting/email/tenant'],
                                        ],
                                ],
                                    'visible' => Yii::$app->user->can('emailSettingsRead'),
                                ],

                                [
                                    'label' => 'Телефония',
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    'options' => ['class' => 'treeview'],
                                    'items' => [
                                        ['label' => 'Телефонные линии', 'url' => ['/setting/phone-line']],
                                        ['label' => 'Исх. маршрутизация', 'url' => ['/setting/outbound-routing']],
                                    ],
                                    'visible' => Yii::$app->user->can('phoneLinesRead'),
                                ],
                                [
                                    'label' => 'Валюты',
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    'url' => ['/setting/currency/'],
                                    'visible' => Yii::$app->user->can('currencyRead'),
                                ],
                                [
                                    'label' => 'Уведомления клиентам',
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    'url' => ['/setting/default-client-status-event/'],
                                    'visible' => Yii::$app->user->can('clientStatusEventRead'),
                                ],
                                [
                                    'label' => 'Шаблоны PUSH-уведомлений исполнителю',
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    'url' => ['/setting/default-worker-push-notification/'],
                                    'visible' => Yii::$app->user->can('messageTemplatesRead'),
                                ],
                                [
                                    'label' => 'Шаблоны PUSH-уведомлений клиенту',
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    'url' => ['/setting/default-client-push-notifications/'],
                                    'visible' => Yii::$app->user->can('messageTemplatesRead'),
                                ],
                                [
                                    'label' => 'Шаблоны PUSH-уведомлений клиенту (курьеры)',
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    'url' => ['/setting/default-client-push-notifications-courier/'],
                                    'visible' => Yii::$app->user->can('messageTemplatesRead'),
                                ],
                                [
                                    'label' => 'Шаблоны смс получателю',
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    'url' => ['/setting/default-recipient-sms-template'],
                                    'visible' => Yii::$app->user->can('messageTemplatesRead'),
                                ],
                                [
                                    'label' => 'Шаблоны смс',
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    'url' => ['/setting/sms-template'],
                                    'visible' => Yii::$app->user->can('messageTemplatesRead'),
                                ],
                                [
                                    'label' => 'Шаблоны смс (курьеры)',
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    'url' => ['/setting/sms-template-courier'],
                                    'visible' => Yii::$app->user->can('messageTemplatesRead'),
                                ],
                                [
                                    'label' => 'Выходные, праздники',
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    'url' => ['/setting/holiday'],
                                    'visible' => Yii::$app->user->can('holidaysRead'),
                                ],
                                [
                                    'label' => 'Статусы заказа',
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    'url' => ['/setting/order-status'],
                                    'visible' => Yii::$app->user->can('orderStatusRead'),
                                ],
                                [
                                    'label' => 'Стандартные настройки',
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    'url' => ['/setting/default-settings'],
                                    'visible' => Yii::$app->user->can('defaultSettingsRead'),
                                ],
                                [
                                    'label' => 'Настройки заказчика',
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    'url' => ['/setting/tenant-setting'],
                                    'visible' => Yii::$app->user->can('tenantSettingsRead'),
                                ],
                                [
                                    'label' => 'Geo-service access',
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    'url' => ['/setting/elastic-access'],
                                    'visible' => Yii::$app->user->can('geoElasticRead'),
                                ],
                                [
                                    'label' => 'Geo-service provider',
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    'url' => ['/setting/geo-service'],
                                    'visible' => Yii::$app->user->can('geoElasticRead'),
                                ],
                                [
                                    'label' => 'Внешние диспетчера',
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                    'url' => ['/setting/extrinsic-dispatcher'],
                                    'visible' => Yii::$app->user->can('extrinsicDispatcherRead'),
                                ],
                            ],
                    ],
                ],
            ])
            ?>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?= $this->title ?>
                <small></small>
            </h1>
            <?=
            Breadcrumbs::widget([
                'tag' => 'ol',
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ])
            ?>
        </section>

        <!-- Main content -->
        <section class="content">
            <?= backend\widgets\Alert::widget() ?>
            <?= $content ?>
        </section><!-- /.content -->
    </aside><!-- /.right-side -->
</div><!-- ./wrapper -->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
