<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

$this->title = 'Административная панель';
?>
<div style="width: 98%;" class="unresolved-tickets">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Новые заказчики за сегодня</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body" style="display: block;">
            <div class="table-responsive">
                <table class="table no-margin">
                    <? if (!$tenants == []): ?>
                        <thead>
                        <tr>
                            <th>ID заказчика</th>
                            <th>Домен</th>
                            <th>Точное время регистрации</th>
                            <th>Название компании</th>
                            <th>ФИО Контактного лица</th>
                            <th>Номер телефона контактного лица</th>
                            <th>Эл. почта контактного лица</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? foreach ($tenants as $tenant): ?>
                            <tr>
                                <td><?= Html::encode($tenant['tenant_id']) ?></td>
                                <td><?= Html::encode($tenant['domain']) ?></td>
                                <td><?= Html::encode($tenant['create_time']) ?></td>
                                <td><?= Html::encode($tenant['company_name']) ?></td>
                                <td><?= Html::encode($tenant['contact_last_name'] . ' ' . $tenant['contact_name'] . ' ' . $tenant['contact_second_name']) ?></td>
                                <td><?= Html::encode($tenant['contact_phone']) ?></td>
                                <td><?= Html::encode($tenant['contact_email']) ?></td>
                            </tr>
                        <? endforeach; ?>
                        </tbody>
                    <? else: ?>
                        <?= 'За сегодня нет ни одного нового заказчика' ?>
                    <? endif; ?>
                </table>
            </div><!-- /.table-responsive -->
        </div><!-- /.box-body -->
        <div class="box-footer clearfix" style="display: block;">
            <a href="/setting/tenant" class="btn btn-sm btn-info btn-flat pull-left">Просмотреть всех заказчиков</a>
        </div><!-- /.box-footer -->
    </div>
</div>

<div style="width: 98%;" >
    <div class="box box-info">
        <div class="box-header with-border">
            <div class="box-header with-border">
                <h3 class="box-title">Скачавшие бизнес-план сегодня</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body" style="display: block;">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <thead>
                        <th>ID</th>
                        <th>Имя и фамилия</th>
                        <th>Эл.почта</th>
                        <th>Телефон</th>
                        <th>Зарегистрирован?</th>
                        <th>Время отправки</th>
                        </thead>
                        <?php
                        foreach ($plan_downloaders as $plan_downloader) {
                            ?>
                            <tbody>
                            <tr>
                                <td><?= Html::encode($plan_downloader["id"]) ?></td>
                                <td><?= Html::encode($plan_downloader["name"]) ?></td>
                                <td><?= Html::encode($plan_downloader["email"]) ?></td>
                                <td><?= Html::encode($plan_downloader["phone"]) ?></td>
                                <td><?= Html::encode($plan_downloader["is_registered"]) ?></td>
                                <td><?= Html::encode($plan_downloader["create_time"]) ?></td>
                            </tr>
                            </tbody>
                            <?php
                        }
                        ?>
                    </table>
                </div><!-- /.table-responsive -->
            </div><!-- /.box-body -->
        </div><!-- /.box-body -->
        <div class="box-footer clearfix" style="display: block;">
            <a href="/landing/business-plan" class="btn btn-sm btn-info btn-flat pull-left">Просмотреть всех скачавших</a>
        </div><!-- /.box-footer -->
    </div>
</div>

<div style="width: 98%;" >
    <div class="box box-info">
        <div class="box-header with-border">
            <div class="box-header with-border">
                <h3 class="box-title">Заказали демонстрацию сегодня</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body" style="display: block;">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <thead>
                        <th>ID</th>
                        <th>Действие</th>
                        <th>Имя</th>
                        <th>Город</th>
                        <th>Телефон или скайп</th>
                        <th>Эл.почта</th>
                        <th>Время создания</th>
                        </thead>
                        <?php
                        foreach ($demo_downloaders as $demo_downloader) {
                            ?>
                            <tbody>
                            <tr>
                                <td><?= Html::encode($demo_downloader["conversion_id"]) ?></td>
                                <td><?= Html::encode($demo_downloader["action"]) ?></td>
                                <td><?= Html::encode($demo_downloader["name"]) ?></td>
                                <td><?= Html::encode($demo_downloader["city"]) ?></td>
                                <td><?= Html::encode($demo_downloader["contact"]) ?></td>
                                <td><?= Html::encode($demo_downloader["email"]) ?></td>
                                <td><?= Html::encode($demo_downloader["create_time"]) ?></td>
                            </tr>
                            </tbody>
                            <?php
                        }
                        ?>
                    </table>
                </div><!-- /.table-responsive -->
            </div><!-- /.box-body -->
        </div><!-- /.box-body -->
        <div class="box-footer clearfix" style="display: block;">
            <a href="/landing/conversion" class="btn btn-sm btn-info btn-flat pull-left">Просмотреть всех скачавших</a>
        </div><!-- /.box-footer -->
    </div>
</div>

<div style="width: 98%; display: inline-block;" class="latest-client">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Непрочитанные обращения в тех поддержку</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body" style="display: block;">
            <div class="table-responsive">
                <table class="table no-margin">
                    <? if (!$tickets == []): ?>
                        <thead>
                        <tr>
                            <th>ID обращения</th>
                            <th>Важность</th>
                            <th>Время</th>
                            <th>Сообщение</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? foreach ($tickets as $ticket): ?>
                            <tr>
                                <td><?= '<a href="/support/support/update/' . Html::encode($ticket['support_id']) . '"> #' . Html::encode($ticket['support_id']) . '</a>' ?></td>
                                <td><?= Html::encode($ticket['priority']) ?></td>
                                <td><?= Html::encode($ticket['create_time']) ?></td>
                                <td><?= HtmlPurifier::process($ticket['message']) ?></td>

                            </tr>
                        <? endforeach; ?>
                        </tbody>
                    <? else: ?>
                        <?= 'Нет активных обращений' ?>
                    <? endif; ?>
                </table>
            </div><!-- /.table-responsive -->
        </div><!-- /.box-body -->
    </div>
</div>
<div style="width: 98%; display" class="latest-elecsnet">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Финансовые операции Элекснет за сегодня</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body" style="display: block;">
            <div class="table-responsive">
                <table class="table no-margin">
                    <? if (!$payments == []): ?>
                        <thead>
                        <tr>
                            <th> ID запроса</th>
                            <th>Сумма</th>
                            <th>Дата</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? foreach ($payments as $payment): ?>
                            <tr>
                                <td><?= Html::encode($payment['reqid']) ?></td>
                                <td><?= Html::encode($payment['amount']) / 100 ?></td>
                                <td><?= date('Y-m-d H:i:s', Html::encode($payment['create_time'])) ?></td>
                            </tr>
                        <? endforeach; ?>
                        </tbody>
                    <? else: ?>
                        <?= 'За сегодня нет ни одной финансовой операции' ?>
                    <? endif; ?>
                </table>
            </div><!-- /.table-responsive -->
        </div><!-- /.box-body -->
        <div class="box-footer clearfix" style="display: block;">
            <a href="/elecsnet-log/" class="btn btn-sm btn-info btn-flat pull-left">Просмотреть все логи
                элекснет</a>
        </div><!-- /.box-footer -->
    </div>
</div>