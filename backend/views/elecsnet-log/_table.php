<?php
use yii\grid\GridView;

?>

<?=
    GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $errorLogDataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'type',
            'reqid',
            'amount',
            'auth_code',
            'currency',
            'result',
            'date',
            'create_time:datetime',
        ],
    ]);
    ?>