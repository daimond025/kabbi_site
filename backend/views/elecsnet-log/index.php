<?php

use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
use app\modules\setting\assets\SettingAsset;

$bundle = SettingAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/js/bootstrap-datepicker.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile($bundle->baseUrl . '/locales/bootstrap-datepicker.ru.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile($bundle->baseUrl . '/log-widgets.js', ['depends' => 'backend\assets\DateRangePickerAsset']);

$this->title = 'Лог Элекснета';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="input-group">
    <button type="button" class="btn btn-default pull-right" id="daterange">
        <i class="fa fa-calendar"></i> Выбрать период
        <i class="fa fa-caret-down"></i>
    </button>

    <button type="button" class="btn btn-default pull-right" id="error_code">
        <i class="fa fa-th-list"></i> Коды ошибок
    </button>
</div>

<div id="helper" style="display: none">
        <?=
        GridView::widget([
            'dataProvider' => $errorCode,
            'columns' => [
                'code',
                'description',
            ],
        ]);
        ?>
</div>

<div class="elecsnet-log-index">
    <h1><?= Html::encode($this->title) ?></h1>
</div>

<?php yii\widgets\Pjax::begin(); ?>
<section id="payments">
    <?=
    GridView::widget([
        'dataProvider' => $errorLog,
        'filterModel' => $errorLogFilter,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'type',
                'filter'=>[1=>1,2=>2],
            ],
            'reqid',
            'amount',
            'auth_code',
            'currency',
            'result',
            'date',
            'create_time:datetime',
        ],
    ]);
    ?>
</section>
    <?php yii\widgets\Pjax::begin(); ?>
</div>
