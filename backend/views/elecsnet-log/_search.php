<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ElecsnetLogSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="elecsnet-log-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'log_id') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'reqid') ?>

    <?= $form->field($model, 'amount') ?>

    <?= $form->field($model, 'auth_code') ?>

    <?php // echo $form->field($model, 'currency') ?>

    <?php // echo $form->field($model, 'result') ?>

    <?php // echo $form->field($model, 'date') ?>

    <?php // echo $form->field($model, 'create_time') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
