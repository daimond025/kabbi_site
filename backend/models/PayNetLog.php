<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%paynet_log}}".
 *
 * @property integer $log_id
 * @property string $type
 * @property string $username
 * @property integer $serviceId
 * @property string $data
 * @property string $result
 * @property integer $create_time
 */
class PayNetLog extends ActiveRecord
{

    const TYPE_GET_INFORMATION = 'get information';
    const TYPE_PERFORM_TRANSACTION = 'perform transaction';
    const TYPE_CHECK_TRANSACTION = 'check transaction';
    const TYPE_CANCEL_TRANSACTION = 'cancel transaction';
    const TYPE_GET_STATEMENT = 'get statement';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%paynet_log}}';
    }
}
