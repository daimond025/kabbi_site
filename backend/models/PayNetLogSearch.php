<?php

namespace app\models;

use yii\data\ActiveDataProvider;

class PayNetLogSearch extends PayNetLog
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'username', 'serviceId', 'result'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'log_id'      => 'Log ID',
            'type'        => 'Тип',
            'username'    => 'Логин',
            'serviceId'   => 'Арендатор',
            'data'        => 'Вх. данные',
            'result'      => 'Результат',
            'create_time' => 'Время',
        ];
    }

    public function search($params, $startDate = null, $endDate = null)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->filterWhere([
            'serviceId' => $this->serviceId,
            'type'      => $this->type,
            'username'  => $this->username,
            'result'    => $this->result,
        ]);

        if ($startDate && $endDate) {
            $start = explode('-', $startDate);
            $end = explode('-', $endDate);
            $timeStart = mktime(0, 0, 0, $start[1], $start[2], $start[0]);
            $timeEnd = mktime(23, 59, 59, $end[1], $end[2], $end[0]);

            $query->andWhere('create_time >= :start', ['start' => $timeStart])
                ->andWhere('create_time <= :end', ['end' => $timeEnd]);

        }

        return $dataProvider;
    }


    public static function getTypeList()
    {
            return [
                self::TYPE_CHECK_TRANSACTION   => self::TYPE_CHECK_TRANSACTION,
                self::TYPE_PERFORM_TRANSACTION => self::TYPE_PERFORM_TRANSACTION,
                self::TYPE_GET_INFORMATION     => self::TYPE_GET_INFORMATION,
                self::TYPE_GET_STATEMENT       => self::TYPE_GET_STATEMENT,
                self::TYPE_CANCEL_TRANSACTION  => self::TYPE_CANCEL_TRANSACTION,
            ];
    }

    public static function getResultList()
    {
        return [
            0   => 'Проведено успешно',
            77  => 'Недостаточно средств на счету клиента для отмены платежа',
            100 => 'Услуга временно не поддерживается',
            101 => 'Квота исчерпана',
            102 => 'Системная ошибка',
            103 => 'Неизвестная ошибка',
            201 => 'Транзакция уже существует',
            202 => 'Транзакция уже отменена',
            301 => 'Номер не существует',
            302 => 'Клиент не найден',
            304 => 'Товар не найден',
            305 => 'Услуга не найдена',
            401 => 'Ошибка валидации параметра 1',
            402 => 'Ошибка валидации параметра 2',
            403 => 'Ошибка валидации параметра 3',
            404 => 'Ошибка валидации параметра 4',
            405 => 'Ошибка валидации параметра 5',
            406 => 'Ошибка валидации параметра 6',
            407 => 'Ошибка валидации параметра 7',
            408 => 'Ошибка валидации параметра 8',
            409 => 'Ошибка валидации параметра 9',
            410 => 'Ошибка валидации параметра 10',
            411 => 'Не заданы один или несколько обязательных параметров',
            412 => 'Неверный логин',
            413 => 'Неверная сумма',
            414 => 'Неверный формат даты и времени',
            501 => 'Транзакции запрещены для данного плательщика',
            601 => 'Доступ запрещен',
            603 => 'Неправильный код команды',
        ];
    }
}
