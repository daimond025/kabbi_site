<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%elecsnet_log}}".
 *
 * @property integer $log_id
 * @property string $type
 * @property string $reqid
 * @property string $amount
 * @property string $auth_code
 * @property string $currency
 * @property string $result
 * @property string $date
 * @property integer $create_time
 */
class ElecsnetLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%elecsnet_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'reqid', 'amount', 'auth_code', 'currency', 'result'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'log_id' => 'Log ID',
            'type' => 'Type',
            'reqid' => 'Reqid',
            'amount' => 'Amount',
            'auth_code' => 'Auth Code',
            'currency' => 'Currency',
            'result' => 'Result',
            'date' => 'Date',
            'create_time' => 'Create Time',
        ];
    }
    
    public function search($params, $start = null, $end = null) 
    {
        $query = ElecsnetLog::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'create_time' => SORT_DESC
                ],
            ],
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if($start && $end){
            $start = explode('-',$start);
            $end = explode('-',$end);
            $timeStart = mktime(0,0,0,$start[1],$start[2],$start[0]);
            $timeEnd = mktime(23,59,59,$end[1],$end[2],$end[0]);
            
            $query->andWhere('create_time >= :start',['start' => $timeStart])
                  ->andWhere('create_time <= :end',['end' => $timeEnd]);
                  
        }
        $query->andFilterWhere([
            'log_id' => $this->log_id,
            'type' => $this->type,
        ]);
        
        $query->andFilterWhere(['like', 'reqid', $this->reqid])
            ->andFilterWhere(['like', 'amount', $this->amount])
            ->andFilterWhere(['like', 'auth_code', $this->auth_code])
            ->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['like', 'result', $this->result]);

        return $dataProvider;
    }
}
