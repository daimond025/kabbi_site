<?php

namespace app\models;

use landing\models\Conversion;
use landing\models\BusinessPlanSearch;
use app\modules\setting\models\Tenant;
use app\modules\support\models\SupportMessage;

/**
 * Class MainPage
 * @package app\models
 */
class MainPageAdmin
{
    public $begin;
    public $end;

    public function __construct($begin,$end){
        $this->begin = $begin;
        $this->end = $end;
    }

    public function getDemoDownload()
    {
        return Conversion::find()
            ->where([
                'between',
                'create_time',
                date("Y-m-d h:i:s", $this->begin),
                date("Y-m-d h:i:s", $this->end)
            ])
            ->all();
    }

    public function getPlanDownload()
    {
        return BusinessPlanSearch::find()
            ->where([
                'between',
                'create_time',
                date("Y-m-d h:i:s", $this->begin),
                date("Y-m-d h:i:s", $this->end)
            ])
            ->all();
    }

    public function getTenants()
    {
        return Tenant::find()
            ->where([
                'between',
                'create_time',
                date("Y-m-d h:i:s", $this->begin),
                date("Y-m-d h:i:s", $this->end)
            ])
            ->orderBy(['create_time' => SORT_DESC])
            ->all();
    }

    public function getTickets()
    {
        return SupportMessage::find()
            ->alias('sm')
            ->select(['sm.support_id', 'sm.message', 'sm.create_time', 's.priority as priority'])
            ->joinWith(['support s'])
            ->andWhere(['status' => 'OPEN'])
            ->asArray()
            ->orderBy(['sm.create_time' => SORT_DESC])
            ->all();
    }

    public function getPayments()
    {
        return ElecsnetLog::find()
            ->where([
                'between',
                'create_time',
                $this->begin,
                $this->end,
            ])
            ->andWhere(['like', 'result', 'ans_code=00'])
            ->orderBy(['create_time' => SORT_DESC])
            ->all();
    }
}