<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%million_log}}".
 *
 * @property integer $log_id
 * @property string $type
 * @property string $data
 * @property string $apikey
 * @property string $result
 * @property integer $create_time
 * @property integer $callsign
 * @property string $phone
 * @property string $sum
 */
class MillionLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%million_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['create_time'], 'integer'],
            [['type', 'apikey', 'data', 'result', 'sum'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 15],
            [['callsign'], 'string', 'max' => 8],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'log_id' => 'Log ID',
            'type' => 'Тип',
            'apikey' => 'ИД клиента',
            'data' => 'Данные',
            'result' => 'Результат',
            'create_time' => 'Время',
            'phone' => 'Телефон',
            'callsign' => 'Позывной водителя',
            'sum' => 'Сумма',
        ];
    }
    
    
    public function search($params, $start = null, $end = null) 
    {
        $query = MillionLog::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'create_time' => SORT_DESC
                ],
            ],
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        if($start && $end){
            $start = explode('-',$start);
            $end = explode('-',$end);
            $timeStart = mktime(0,0,0,$start[1],$start[2],$start[0]);
            $timeEnd = mktime(23,59,59,$end[1],$end[2],$end[0]);
            
            $query->andWhere('create_time >= :start',['start' => $timeStart])
                  ->andWhere('create_time <= :end',['end' => $timeEnd]);
                  
        }
        $query->andFilterWhere([
            'log_id' => $this->log_id,
            'create_time' => $this->create_time,
            'type' => $this->type,
        ]);
        
        $query->andFilterWhere(['like', 'apikey', $this->apikey])
            ->andFilterWhere(['like', 'result', $this->result])
            ->andFilterWhere(['like', 'sum', $this->sum])
            ->andFilterWhere(['like', 'callsign', $this->callsign])
            ->andFilterWhere(['like', 'phone', $this->phone]);

        return $dataProvider;
    }
}
