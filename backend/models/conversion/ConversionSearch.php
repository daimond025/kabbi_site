<?php

namespace backend\models\conversion;

use yii\data\ActiveDataProvider;

class ConversionSearch extends Conversion
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action', 'name', 'city', 'contact', 'email','comment'], 'safe'],
        ];
    }

    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'create_time' => SORT_DESC
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'action', $this->action])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'contact', $this->contact])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}