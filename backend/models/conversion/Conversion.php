<?php

namespace backend\models\conversion;

use yii\db\ActiveRecord;

/**
 * Class Conversion
 * @package landing\models
 *
 * @property int    $conversion_id
 * @property string $action
 * @property string $name
 * @property string $city
 * @property string $contact
 * @property string $email
 * @property string $create_time
 * @property string $comment
 */
class Conversion extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%conversion}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'conversion_id' => 'ID',
            'action'        => 'Действие',
            'name'          => 'Имя',
            'city'          => 'Город',
            'contact'       => 'Телефон или скайп',
            'email'         => 'Эл. почта',
            'create_time'   => 'Время создания',
            'comment' => 'Комментарий',
        ];
    }
}