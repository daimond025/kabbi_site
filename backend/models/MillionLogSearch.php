<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MillionLog;

/**
 * ElecsnetLogSearch represents the model behind the search form about `app\models\ElecsnetLog`.
 */
class MillionLogSearch extends MillionLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['log_id', 'create_time'], 'integer'],
            [['type', 'apikey', 'data', 'result'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MillionLog::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'log_id' => $this->log_id,
            'create_time' => $this->create_time,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'apikey', $this->apikey])
            ->andFilterWhere(['like', 'data', $this->data])
            ->andFilterWhere(['like', 'result', $this->result]);
        $query->orderBy(['create_time' => SORT_DESC]);
        return $dataProvider;
    }
}
