<?php

namespace backend\models\RenewalReport;

use yii\db\ActiveRecord;
use common\modules\tenant\models\Tenant;
use common\modules\tenant\modules\tariff\models\PaymentForAdditionalOption;
use common\modules\tenant\modules\tariff\models\PaymentForOneTimeService;
use common\modules\tenant\modules\tariff\models\PaymentForTariff;

/**
 * Class PaymentsReport
 * @package backend\models\PaymentsReport
 */
class RenewalReport extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_tariff}}';
    }

    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    public function getPaymentForTariffs()
    {
        return $this->hasMany(PaymentForTariff::className(), ['payment_id' => 'id']);
    }

    public function getPaymentForAdditionalOptions()
    {
        return $this->hasMany(PaymentForAdditionalOption::className(), ['payment_id' => 'id']);
    }

    public function getPaymentForOneTimeServices()
    {
        return $this->hasMany(PaymentForOneTimeService::className(), ['payment_id' => 'id']);
    }


}