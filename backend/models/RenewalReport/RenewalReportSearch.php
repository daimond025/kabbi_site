<?php

namespace backend\models\RenewalReport;

use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * Class PaymentsReportSearch
 * @package backend\models
 */
class RenewalReportSearch extends RenewalReport
{
    public $company_name;
    public $domain;
    public $leaseEnd;
    public $payment_id;

    const DEMO_TARIFF_ID = 5;


    public function rules()
    {
        return [
            [['leaseEnd', 'company_name', 'domain'], 'safe'],
        ];
    }

    public function search($params)
    {

        $subquery = self::find()->alias('tt')->select(['t.domain', 'MAX(tt.expiry_date) as expiry_date'])
            ->joinWith(['tenant t'])
            ->groupBy(['domain'])
            ->asArray()
            ->all();

        $query = self::find()->alias('tt')->select([

            'tt.expiry_date as expiry_date',
            'IF(
                DATEDIFF(DATE_FORMAT(FROM_UNIXTIME(tt.expiry_date), "%Y-%m-%d"), 
                DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(NOW())),"%Y-%m-%d")) > 0, 
                DATEDIFF(DATE_FORMAT(FROM_UNIXTIME(tt.expiry_date), "%Y-%m-%d"), 
                DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(NOW())),"%Y-%m-%d")), 
                CONCAT("просрочен на ", 20 - (DATEDIFF(DATE_FORMAT(FROM_UNIXTIME(tt.expiry_date), "%Y-%m-%d"), 
                DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(NOW())),"%Y-%m-%d")) + 20), " дней")) as leaseEnd',
            't.domain as domain',
            't.company_name as company_name',
            'tt.payment_for_tariff_id as payment_id',
            'tt.tariff_id as tariff_id',
            'pftp.confirmed_at as confirmed_at',
        ])
            ->groupBy(['domain','company_name','leaseEnd'])
            ->having('leaseEnd >= 0 AND IF(LOCATE("дней",leaseEnd) = 0, leaseEnd <= 15, REPLACE(leaseEnd, "просрочен на ", "") <= 20) AND confirmed_at IS NULL AND tariff_id !='.self::DEMO_TARIFF_ID)
            ->andHaving(['IN',['expiry_date','domain'], $subquery]);

        $query->joinWith(['paymentForTariffs.payment pftp', 'tenant t']);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'sort'       => [
                'attributes'   => [
                    'company_name',
                    'domain',
                    'leaseEnd'
                ],
            ],
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if(!$params){
            $query->orderBy(new \yii\db\Expression('IF(LOCATE("п",leaseEnd) = 0, REPLACE(leaseEnd, "просрочен на ", "")+0, null) ASC, IF(LOCATE("п",leaseEnd) > 0, REPLACE(leaseEnd, "просрочен на ", "")+0, null) DESC'));
        }

        if($params['sort'] === 'leaseEnd'){
            $query->orderBy(new \yii\db\Expression('IF(LOCATE("п",leaseEnd) = 0, REPLACE(leaseEnd, "просрочен на ", "")+0, null) DESC, IF(LOCATE("п",leaseEnd) > 0, REPLACE(leaseEnd, "просрочен на ", "")+0, null) ASC'));
        }

        if($params['sort'] === '-leaseEnd'){
            $query->orderBy(new \yii\db\Expression('IF(LOCATE("п",leaseEnd) = 0, REPLACE(leaseEnd, "просрочен на ", "")+0, null) ASC, IF(LOCATE("п",leaseEnd) > 0, REPLACE(leaseEnd, "просрочен на ", "")+0, null) DESC'));
        }


        $query->andFilterWhere(['like', 't.company_name', $this->company_name]);

        $query->andFilterWhere(['like', 't.domain', $this->domain]);

        $query->andFilterHaving(['like', 'leaseEnd', $this->leaseEnd]);


        return $dataProvider;

    }

}