<?php

namespace backend\models\TariffReport;

use common\modules\tenant\modules\tariff\models\Payment;
use yii\db\ActiveRecord;
use common\modules\tenant\models\Tenant;
use common\modules\tenant\modules\tariff\models\Tariff;
use common\modules\tenant\modules\tariff\models\PaymentForTariff;

/**
 * Class TariffReport
 * @package backend\models\TariffReport
 */
class TariffReport extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment_for_tariff}}';
    }


    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }


    public function getTariff()
    {
        return $this->hasOne(Tariff::className(), ['id' => 'tariff_id']);
    }

    public function getPaymentForTariffs()
    {
        return $this->hasMany(PaymentForTariff::className(), ['payment_id' => 'payment_for_tariff_id']);
    }

    public function getPayment()
    {
        return $this->hasMany(Payment::className(), ['id' => 'payment_id']);
    }


}