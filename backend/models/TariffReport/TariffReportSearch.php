<?php

namespace backend\models\TariffReport;

use yii\data\ActiveDataProvider;

/**
 * Class TariffReportSearch
 * @package backend\models\TariffReport
 */
class TariffReportSearch extends TariffReport
{

    public $numTariffTenants;
    public $tariffName;

    public function rules()
    {
        return [
            ['tariffName', 'safe'],
        ];
    }

    public function search($params)
    {
        $query = self::find();
        if (!isset($params['start'])) {
            $query->select(['COUNT(tr.id) as numTariffTenants', 'tr.tariff_id', 't.name as tariffName']);
        }
        $query->alias('tr')
            ->joinWith(['tariff t'])
            ->joinWith(['payment pp'])
            ->where('pp.confirmed_at IS NOT NULL')
            ->groupBy('tr.tariff_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'attributes' => [
                    'tariffName',
                    'numTariffTenants',
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 't.name', $this->tariffName]);

        if (isset($params['start'])) {

            $start = strtotime($params['start']);

            $end = strtotime($params['end']);

            $end += 24 * 3600 - 1;

            $query->select([
                't.name as tariffName',
                'COUNT(IF(pp.confirmed_at BETWEEN ' . $start . ' AND ' . $end . ',1,null)) as numTariffTenants',
            ]);

        }

        return $dataProvider;

    }


}