<?php

namespace backend\models\TariffReport;

use yii\data\ActiveDataProvider;

/**
 * Class OneTimeServiceReportSearch
 * @package backend\models\TariffReport
 */
class OneTimeServiceReportSearch extends OneTimeServiceReport
{

    public $numOneTimeTenants;

    public function rules()
    {
        return [
            ['name', 'safe'],
        ];
    }

    public function attributeLabels()
    {

        return ['optionName' => 'Название опции', 'numOneTimeTenants' => 'Количество клиентов'];

    }

    public function search($params)
    {
        $query = self::find();
        if (!isset($params['start'])) {
            $query->select(['COUNT(t.tenant_id) as numOneTimeTenants', 'name']);
        };
        $query->joinWith(['paymentsForOneTimeService.payment.tenant t'])
            ->joinWith(['paymentsForOneTimeService.payment pp'])
            ->where('pp.payment_sum>0 && pp.confirmed_at IS NOT NULL')
            ->groupBy('name');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'attributes' => [
                    'name',
                    'numOneTimeTenants',
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name]);

        if ($params['start']) {

            $start = strtotime($params['start']);

            $end = strtotime($params['end']);

            $end += 24 * 3600 - 1;

            $query->select([
                'name',
                'COUNT(IF(pp.confirmed_at BETWEEN ' . $start . ' AND ' . $end . ',1,null)) as numOneTimeTenants',
            ]);

        }

        return $dataProvider;

    }

}