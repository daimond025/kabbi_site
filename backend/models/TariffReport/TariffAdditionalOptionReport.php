<?php

namespace backend\models\TariffReport;

use yii\db\ActiveRecord;
use common\modules\tenant\models\Tenant;
use common\modules\tenant\modules\tariff\models\TariffAdditionalOption;
use common\modules\tenant\modules\tariff\models\PaymentForAdditionalOption;
use common\modules\tenant\modules\tariff\models\Payment;

/**
 * Class TariffAdditionalOptionReport
 * @package backend\models\TariffReport
 */
class TariffAdditionalOptionReport extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment_for_additional_option}}';
    }


    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }


    public function getTariffAdditionalOptions()
    {
        return $this->hasMany(TariffAdditionalOption::className(), ['id' => 'option_id']);
    }

    public function getPaymentForAdditionalOptions()
    {
        return $this->hasMany(PaymentForAdditionalOption::className(), ['payment_id' => 'payment_for_option_id']);
    }

    public function getPayment()
    {
        return $this->hasMany(Payment::className(), ['id' => 'payment_id']);
    }

}