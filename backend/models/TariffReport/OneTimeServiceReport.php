<?php

namespace backend\models\TariffReport;

use common\modules\tenant\modules\tariff\models\PaymentForOneTimeService;
use yii\db\ActiveRecord;

/**
 * Class OneTimeServiceReport
 * @package backend\models\TariffReport
 */
class OneTimeServiceReport extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%one_time_service}}';
    }

    public function getPaymentsForOneTimeService()
    {
        return $this->hasOne(PaymentForOneTimeService::className(), ['service_id' => 'id']);
    }
}