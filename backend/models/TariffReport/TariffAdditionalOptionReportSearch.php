<?php

namespace backend\models\TariffReport;

use yii\data\ActiveDataProvider;


/**
 * Class TariffAdditionalOptionReportSearch
 * @package backend\models\TariffReport
 */
class TariffAdditionalOptionReportSearch extends TariffAdditionalOptionReport
{
    public $numOptionTenants;
    public $optionName;

    public function rules()
    {
        return [
            [['optionName', 'numOptionTenants'], 'safe'],
        ];
    }

    public function attributeLabels()
    {

        return ['optionName' => 'Название опции', 'numOptionTenants' => 'Количество клиентов'];

    }

    public function search($params)
    {
        $query = self::find()
            ->alias('a')
            ->select(['o.name as optionName, COUNT(a.id) as numOptionTenants, pp.confirmed_at as conf'])
            ->joinWith(['tariffAdditionalOptions o'])
            ->joinWith(['payment pp'])
            ->where('pp.confirmed_at IS NOT NULL')
            ->groupBy('a.option_id');


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'attributes' => [
                    'optionName',
                    'numOptionTenants',
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'o.name', $this->optionName]);

        if (isset($params['start'])) {

            $start = strtotime($params['start']);

            $end = strtotime($params['end']);

            $end += 24 * 3600 - 1;

            $query->select([
                'o.name as optionName',
                'COUNT(IF(pp.confirmed_at BETWEEN ' . $start . ' AND ' . $end . ',1,null)) as numOptionTenants',
            ]);

        }

        return $dataProvider;

    }

}