<?php

namespace backend\models\PaymentsReport;

use yii\data\ArrayDataProvider;

/**
 * Class PaymentsReportSearch
 * @package backend\models
 */
class PaymentsReportSearch extends PaymentsReport
{
    public $company_name;
    public $userPaymentsPerPeriod;
    public $userTotalPayments;
    public $domain;
    public $sumAvgPayments;
    public $sumPayments;
    public $countMonths;
    public $averageMonthsPerPeriod;
    public $averagePaymentsPerPeriod;


    public function rules()
    {
        return [
            [['countMonths', 'company_name', 'domain', 'sumPayments', 'created_at', 'sumAvgPayments'], 'safe'],
        ];
    }

    public function search($params)
    {

        if ($this->load($params)) {

            if (isset($params[$this->formName()])) {

                foreach ($params[$this->formName()] as $key => $val) {
                    if ($val !== '') {
                        $key    = $key === 'company_name' ? 't.company_name' : $key;
                        $gets[] = $key . ' LIKE ' . '\'%' . $val . '%\'';
                    }
                }
                if (isset($gets)) {
                    $gets = implode(' AND ', $gets);
                }

            }

        }

        if (!isset($gets)) {
            $gets = '1=1';
        };

        if (isset($params['start'])) {

            $start = strtotime($params['start']);

            $end = strtotime($params['end']) + 24 * 3600 - 1;

            $query = \Yii::$app->db->createCommand('
                        SELECT SUM(res1.sumAvgPayments)/SUM(res1.period) as sumAvgPayments,SUM(res1.sumPayments) as sumPayments,t.*,t.domain as domain,SUM(res1.period) as countMonths 
                        FROM(SELECT AVG((p.payment_sum) - (IFNULL(discount,0))) as sumAvgPayments,SUM(DISTINCT IF(ao.period IS NOT NULL,ao.period, pft.period)) as period,SUM(DISTINCT p.payment_sum) - IFNULL(discount,0) as sumPayments,tenant_id FROM tbl_payment as p LEFT JOIN tbl_payment_for_tariff as pft ON p.id = pft.payment_id LEFT JOIN tbl_payment_for_additional_option as ao ON p.id = ao.payment_id WHERE p.confirmed_at IS NOT NULL AND p.confirmed_at >= ' . $start . ' AND p.confirmed_at <= ' . $end . ' GROUP BY p.id) as res1 
                        INNER JOIN tbl_tenant as t ON t.tenant_id=res1.tenant_id WHERE ' . $gets . ' GROUP BY t.tenant_id 
                    ');

        } else {

            $query = \Yii::$app->db->createCommand('
                        SELECT SUM(res1.sumAvgPayments)/SUM(res1.period) as sumAvgPayments,SUM(res1.sumPayments) as sumPayments,t.*,t.domain as domain,SUM(res1.period) as countMonths 
                        FROM(SELECT AVG((p.payment_sum) - (IFNULL(discount,0))) as sumAvgPayments,SUM(DISTINCT IF(ao.period IS NOT NULL,ao.period, pft.period)) as period,SUM(DISTINCT p.payment_sum) - IFNULL(discount,0) as sumPayments,tenant_id FROM tbl_payment as p LEFT JOIN tbl_payment_for_tariff as pft ON p.id = pft.payment_id LEFT JOIN tbl_payment_for_additional_option as ao ON p.id = ao.payment_id WHERE p.confirmed_at IS NOT NULL GROUP BY p.id) as res1 
                        INNER JOIN tbl_tenant as t ON t.tenant_id=res1.tenant_id WHERE ' . $gets . ' GROUP BY t.tenant_id 
                    ');
        }


        $array = $query->queryAll();

        $dataProvider = new ArrayDataProvider([
            'allModels'  => $array,
            'sort'       => [
                'attributes' => [
                    'company_name',
                    'domain',
                    'sumPayments',
                    'countMonths',
                    'sumAvgPayments',
                ],
            ],
            'pagination' => false,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        foreach ($array as $val) {
            $sumPayments[] = $val['sumPayments'];
            $avgMonths[]   = $val['countMonths'];
            $avgPayments[] = $val['sumAvgPayments'];
        }

        if (isset($sumPayments)) {
            $this->userPaymentsPerPeriod = round(array_sum($sumPayments));
        }

        if (isset($avgMonths)) {
            $this->averageMonthsPerPeriod = round(array_sum($avgMonths) / count($avgMonths), 2);
        }

        if (isset($avgPayments)) {
            $this->averagePaymentsPerPeriod = round(array_sum($avgPayments) / count($avgPayments));
        }

        return $dataProvider;

    }

}