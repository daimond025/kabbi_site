<?php

namespace app\modules\user\models;

use app\modules\support\models\SupportFeedback;
use app\modules\support\models\SupportMessage;
use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "{{%admin_user}}".
 *
 * @property string $user_id
 * @property string $login
 * @property string $password
 * @property string $name
 * @property string $create_time
 * @property integer $active
 * @property string $auth_key
 * @property string access_token
 *
 * @property SupportFeedback[] $supportFeedbacks
 * @property SupportMessage[] $supportMessages
 */
class User extends ActiveRecord implements IdentityInterface
{
    public $password_repeat;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%admin_user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'password', 'name', 'password_repeat'], 'required'],
            [['active'], 'integer'],
            [['active'], 'default', 'value' => 1],
            [['login', 'name'], 'string', 'max' => 45],
            ['password_repeat', 'compare', 'compareAttribute' => 'password'],
            [['password'], 'string', 'min' => 6],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id'         => 'User ID',
            'login'           => 'Логин',
            'password'        => 'Пароль',
            'password_repeat' => 'Повторите пароль',
            'name'            => 'Имя',
            'create_time'     => 'Время создания',
            'active'          => 'Активность',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupportFeedbacks()
    {
        return $this->hasMany(SupportFeedback::className(), ['support_user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupportMessages()
    {
        return $this->hasMany(SupportMessage::className(), ['support_user_id' => 'user_id']);
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    public static function findIdentity($id)
    {
        return static::findOne(['user_id' => $id]);
    }

    public function setAccessToken()
    {
        $security = Yii::$app->security;
        $this->access_token = $security->generatePasswordHash($security->generateRandomString());
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return self::find()->where(['access_token' => $token])->one();
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public static function getUsername($id)
    {
        return self::find()->select('name')->where(['user_id'=>$id])->scalar();
    }

    /**
     * @return bool
     */
    public static function isActive()
    {
        return self::find()->where(['user_id' => user()->user_id, 'active' => 1])->exists();
    }

    public static function checkActivity()
    {
        if (!self::isActive()) {
            Yii::$app->user->logout();
        }
    }
}
