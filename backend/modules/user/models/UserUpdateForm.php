<?php


namespace app\modules\user\models;


use yii\base\Model;

class UserUpdateForm extends Model
{
    public $login;
    public $password;
    public $username;
    public $active;

    public function rules()
    {
        return [
            [['login', 'username'], 'required'],
            [['active'], 'boolean'],
            [['login', 'name'], 'string', 'max' => 45],
            [['password'], 'string', 'min' => 6],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'login'    => 'Логин',
            'password' => 'Пароль',
            'username' => 'Имя',
            'active'   => 'Активность',
        ];
    }
}