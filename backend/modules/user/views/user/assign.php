<?php


/* @var $this yii\web\View */
/* @var $user_id int */

use dektrium\rbac\widgets\Assignments;
use yii\helpers\Html;

$this->title = 'Редактирование прав';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование прав';

echo $this->render('_nav', ['user_id' => $user_id]);
?>

<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= Assignments::widget([
        'userId' => $user_id,
    ]); ?>

</div>

