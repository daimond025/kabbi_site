<?php
/* @var $user_id int */

use yii\bootstrap\Nav;

echo Nav::widget([
    'items'   => [
        [
            'label' => 'Редактировать пользователя',
            'url'   => ['/user/user/update', 'id' => $user_id],
        ],
        [
            'label' => 'Редактировать права',
            'url'   => ['/user/user/assign', 'id' => $user_id],
        ],
    ],
    'options' => ['class' => 'nav-tabs'],
]);