<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\UserUpdateForm */
/* @var $user_id int */

$this->title = 'Редактировать пользователя: ' . ' ' . Html::encode($model->username);
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Html::encode($model->username), 'url' => ['view', 'id' => $user_id]];
$this->params['breadcrumbs'][] = 'Редактировать';

echo $this->render('_nav', ['user_id' => $user_id]);
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_updateForm', [
        'model' => $model,
    ]) ?>

</div>
