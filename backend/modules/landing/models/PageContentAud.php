<?php

namespace backend\modules\landing\models;

use Yii;
use backend\modules\landing\models\PageContent;

/**
 * This is the model class for table "{{%page_content_aud}}".
 *
 * @property integer $id
 * @property integer $page_content_id
 * @property string $page
 * @property string $content
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property integer $created_at
 * @property integer $updated_at
 */
class PageContentAud extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_content_aud}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_content_id', 'page', 'title', 'description'], 'required'],
            [['content'], 'string'],
            [['page', 'title', 'description', 'keywords'], 'string', 'max' => 255],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_content_id' => 'Page Content Id',
            'page' => 'Page',
            'content' => 'Content',
            'title' => 'Title',
            'description' => 'Description',
            'keywords' => 'Keywords',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function behaviors() {
        return [
            \yii\behaviors\TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageContent()
    {
        return $this->hasOne(PageContent::className(), ['id' => 'page_content_id']);
    }

    public function restorePage()
    {
        $pageContent = PageContent::findone($this->page_content_id);
        $pageContent->page = $this->page;
        $pageContent->title = $this->title;
        $pageContent->description = $this->description;
        $pageContent->keywords = $this->keywords;
        $pageContent->content = $this->content;

        if ($pageContent->save()) {
            return $pageContent;
        }
        return false;
    }

    public static function find() {
        return new PageContentAudQuery(get_called_class());
    }

}

class PageContentAudQuery extends \yii\db\ActiveQuery
{
    public function byPage($page)
    {
        return $this->andWhere(['page' => $page]);
    }

    public function lastModifiedShowFirst()
    {
        return $this->addOrderBy(['created_at' => SORT_DESC]);
    }
}
