<?php
namespace app\modules\landing\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class PageForm extends Model
{
    const LANDING_VIEW_PATH = '@landing/views/site/pages/';
    
    public $page;
    public $html;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['html'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'page' => 'Страница',
            'html' => 'Контент',
        ];
    }
    
    public function getPagePath()
    {
        return \yii\helpers\Url::toRoute(self::LANDING_VIEW_PATH . $this->page) . '.php';
    }
    
    public function save()
    {
        if(!$this->validate())
            return false;
        
        return file_put_contents($this->getPagePath(), $this->html);
    }
}
