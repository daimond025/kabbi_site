<?php

namespace backend\modules\landing\models;

/**
 * This is the model class for table "{{%page_content}}".
 *
 * @property integer $id
 * @property string $page
 * @property string $content
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property integer $created_at
 * @property integer $updated_at
 */
class PageContent extends \yii\db\ActiveRecord
{
    const PAGE_KEY = 'page';
    const META_TAGS_KEY = 'meta-tags';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_content}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page', 'title', 'description'], 'required'],
            [['content'], 'string'],
            [['page', 'title', 'description', 'keywords'], 'string', 'max' => 255],
            [['page'], 'unique'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page' => 'Page',
            'content' => 'Content',
            'title' => 'Title',
            'description' => 'Description',
            'keywords' => 'Keywords',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function behaviors() {
        return [
            \yii\behaviors\TimestampBehavior::className(),
        ];
    }

    private function resetCache()
    {
        app()->cache->delete(self::PAGE_KEY . '.' . $this->page);
        app()->cache->delete(self::PAGE_KEY . '.' . $this->getOldAttribute('page'));
        app()->cache->delete(self::META_TAGS_KEY . '.' . $this->page);
        app()->cache->delete(self::META_TAGS_KEY . '.' . $this->getOldAttribute('page'));
    }

    public function beforeSave($insert)
    {
        $this->resetCache();

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        $pageContentAud = new PageContentAud;
        $pageContentAud->page_content_id = $this->id;
        $pageContentAud->page = $this->page;
        $pageContentAud->title = $this->title;
        $pageContentAud->content = $this->content;
        $pageContentAud->description = $this->description;
        $pageContentAud->keywords = $this->keywords;
        $pageContentAud->save();

        parent::afterSave($insert, $changedAttributes);
    }
    
    public static function hasShowMenu($page)
    {
        switch ($page) {
            case 'business-plan':
//            case 'business-plan2':
                return false;
        }
        
        return true;
    }

    public static function getPageContent($page)
    {
        $cacheKey = self::PAGE_KEY . '.' . $page;
        $cachedValue = app()->cache->get($cacheKey);

        if (!empty($cachedValue)) {
            return $cachedValue;
        }

        $content = PageContent::find()
                ->select('content')->byPage($page)->scalar();
        app()->cache->set($cacheKey, $content, 0);

        return $content;
    }
    
    public static function getMetaTags($page)
    {
        $cacheKey = self::META_TAGS_KEY . '.' . $page;
        $cachedValue = app()->cache->get($cacheKey);

        if (!empty($cachedValue)) {
            return $cachedValue;
        }

        $metaTags = PageContent::find()
                ->select(['title', 'description', 'keywords'])
                ->byPage($page)->asArray()->one();
        app()->cache->set($cacheKey, $metaTags, 0);

        return $metaTags;
    }

    public static function find() {
        return new PageContentQuery(get_called_class());
    }

}

class PageContentQuery extends \yii\db\ActiveQuery
{
    public function byPage($page)
    {
        return $this->andWhere(['page' => $page]);
    }
}
