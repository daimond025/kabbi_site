<?php
namespace backend\modules\landing\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_meta_tags".
 *
 * @property integer $id
 * @property string $description
 * @property string $title
 * @property string $key
 */
class MetaTags extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%meta_tags}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['description', 'title', 'keywords', 'page'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'title' => 'Title',
            'keyword' => 'Keywords',
        ];
    }
    
    public static function cachedMetaTags()
    {
        if(app()->cache->get('metaTags') == FALSE)
        {
            $query = MetaTags::find()->asArray()->all();
            $query = ArrayHelper::index($query, 'page');
            app()->cache->set('metaTags', $query, 0);
            return app()->cache->get('metaTags');
        }
        return app()->cache->get('metaTags');
    }
    
    public static function deleteMetaTags()
    {
        app()->cache->delete('metaTags');
    }
    
    public function afterSave($insert, $changedAttributes) {
        self::deleteMetaTags();
        self::cachedMetaTags();
        parent::afterSave($insert, $changedAttributes);
    }
    
    public function afterDelete() {
        self::deleteMetaTags();
        self::cachedMetaTags();
        parent::afterDelete();
    }
    
}
