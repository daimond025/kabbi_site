<?php

namespace app\modules\landing\controllers;

use Yii;
use backend\modules\landing\models\PageContentAud;
use backend\modules\landing\models\PageContentAudSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\modules\landing\models\PageContent;

/**
 * PageContentAudController implements the CRUD actions for PageContentAud model.
 */
class PageContentAudController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['landingPageEdit'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all PageContentAud models.
     * @return mixed
     */
    public function actionIndex($page_content_id = null)
    {
        $searchModel = new PageContentAudSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->lastModifiedShowFirst();

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PageContentAud model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PageContentAud model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PageContentAud();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PageContentAud model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PageContentAud model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $page_content_id = $model->page_content_id;
        $model->delete();

        return $this->redirect(['index', 'page_content_id' => $page_content_id]);
    }

    public function actionRestore($id)
    {
        $model = $this->findModel($id);

        if ($restoredPage = $model->restorePage()) {
            session()->setFlash('info', 'Page content was restore.');

            return $this->redirect(['page-content/index', 'id' => $restoredPage->id]);
        } else {
            session()->setFlash('error', 'Page content wasn\'t restore.');

            return $this->redirect(['view', 'id' => $model->id]);
        }
    }

    /**
     * Finds the PageContentAud model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PageContentAud the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PageContentAud::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
