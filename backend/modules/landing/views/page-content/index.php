<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\landing\models\PageContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Page Contents';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-content-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::a('Create Page Content', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'page',
            'title',
            'description',
            'created_at:datetime',
            [
                'class'          => 'yii\grid\ActionColumn',
            ],
        ],
]); ?>

</div>
