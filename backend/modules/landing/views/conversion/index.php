<?php
/** @var $this \yii\web\View */
/** @var $dataProvider \yii\data\ActiveDataProvider */
/** @var $searchModel \backend\models\conversion\ConversionSearch */

use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title                   = 'Заказали демонстрацию';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1>Заказали демонстрацию</h1>
<?php Pjax::begin(); ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel'  => $searchModel,
    'columns'      => [
        ['class' => 'yii\grid\SerialColumn'],
        'action',
        'name',
        'city',
        'contact',
        'email',
        'comment',
        'create_time',
        'utm_source',
        'utm_medium',
        'utm_campaign',
        'utm_content',
        'utm_term',
        'client_id_go',
        'client_id_ya',
        'ip',
        [
            'class' => 'yii\grid\ActionColumn',
            'header'=>'Действия',
            'template' => '{delete}',
        ],
    ],
]); ?>

<?php Pjax::end(); ?>