<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\landing\models\PageContentAudSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Page Content Auds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-content-aud-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                'page',
                'title',
                'created_at:datetime',
                [
                    'class'          => 'yii\grid\ActionColumn',
                    'visibleButtons' => [
                        'update' => \Yii::$app->user->can('landing'),
                        'delete' => \Yii::$app->user->can('landing'),
                    ],
                ],
            ],
        ]); ?>

</div>
