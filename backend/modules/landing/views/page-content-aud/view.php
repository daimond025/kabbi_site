<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\landing\models\PageContentAud */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Page Content Auds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-content-aud-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->user->can('landing')): ?>
        <p>
            <?= Html::a('Restore page content', ['restore', 'id' => $model->id], [
                'class' => 'btn btn-primary',
                'data'  => [
                    'confirm' => 'Are you sure you want to restore this page?',
                    'method'  => 'post',
                ],
            ]) ?>
        </p>
    <?php endif ?>

    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            'page',
            'created_at:datetime',
            'updated_at:datetime',
            'title',
            'description',
            'keywords',
            'content:html',
        ],
    ]) ?>

</div>
