<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\landing\models\PageContentAud */

$this->title = 'Update Page Content Aud: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Page Content Auds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="page-content-aud-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
