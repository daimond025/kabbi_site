<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\landing\models\PageContentAud */

$this->title = 'Create Page Content Aud';
$this->params['breadcrumbs'][] = ['label' => 'Page Content Auds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-content-aud-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
