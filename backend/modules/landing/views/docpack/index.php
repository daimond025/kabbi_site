<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'Скачавшие пакет документов';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1>Скачавшие пакет документов</h1>
<?php Pjax::begin(); ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'name',
        'email',
        'phone',
        'utm_source',
        'utm_medium',
        'utm_campaign',
        'utm_content',
        'utm_term',
        'client_id_go',
        'client_id_ya',
        'ip',
        [
            'attribute' => 'is_site',
            'filter' => false,
            'value' => function($data){
                return Html::encode($data->getIsSite());
            },
        ],
        'create_time'
    ],
]); ?>

<?php Pjax::end(); ?>