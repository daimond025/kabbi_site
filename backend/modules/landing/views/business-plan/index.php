<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use landing\models\BusinessPlanSearch;

$this->title = 'Скачавшие бизнес-план';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1>Скачавшие бизнес-план</h1>
<?php Pjax::begin(); ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'name',
        'email',
        'phone',
        'utm_source',
        'utm_medium',
        'utm_campaign',
        'utm_content',
        'utm_term',
        'client_id_go',
        'client_id_ya',
        'ip',
        [
            'attribute' => 'is_registered',
            'filter' => Html::activeDropDownList($searchModel,'is_registered', BusinessPlanSearch::getArrayFilters(), ['class'=>'form-control','prompt' => 'все']),
            'value' => function($data){
                return $data->getRegister();
            },
        ],
        'create_time'
    ],
]); ?>
<?php Pjax::end(); ?>

