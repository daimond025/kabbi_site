<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Продающий сайт';

?>

<h1>Редактирование страниц</h1>

<p>
    <?= Html::a('Create Page', ['create'], ['class' => 'btn btn-success']) ?>
</p>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'page',
        'title',
        [
            'class'          => 'yii\grid\ActionColumn',
            'visibleButtons' => [
                'update' => \Yii::$app->user->can('landing'),
                'delete' => \Yii::$app->user->can('landing'),
            ],
        ],
    ],
]); ?>