<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel landing\models\MetaTagsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Meta Tags';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meta-tags-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php if (Yii::$app->user->can('landing')): ?>
        <p>
            <?= Html::a('Create Meta Tags', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'description',
            'title',
            'keywords',
            'page',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
