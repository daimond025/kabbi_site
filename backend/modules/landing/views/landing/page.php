<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\landing\models\PageForm */
/* @var $form ActiveForm */
$this->title = 'Редактирование страницы: ' . Html::encode($model->page);
?>
<div class="index">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'html')->textarea(['rows' => '50']) ?>
     
        <?= $form->field($metaTagsModel, 'title')->textInput(['maxlength' => 255]) ?>

        <?= $form->field($metaTagsModel, 'keywords')->textInput(['maxlength' => 255]) ?>
    
        <?= $form->field($metaTagsModel, 'description')->textArea(['rows' => 3]) ?>
        
        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- index -->