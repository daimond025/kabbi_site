<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model landing\modules\community\models\OfferInfo */

$this->render('/post/_postBreadcrumbs', ['post' => $model->post]);

$this->title = 'Update OfferInfo: ' . ' ' . $model->label;
$this->params['breadcrumbs'][] = $model->label;
//$this->params['breadcrumbs'][] = 'Update';

?>

<div class="section-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
