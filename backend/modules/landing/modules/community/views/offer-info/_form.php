<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use landing\modules\community\models\Post;
use landing\modules\community\models\OfferInfo;

/* @var $this yii\web\View */
/* @var $model landing\modules\community\models\OfferInfo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="section-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'post_id')
            ->dropDownList(ArrayHelper::map(Post::find()->all(), 'post_id', 'label'), [
                'disabled' => true,
            ]); ?>
    <?= $form->field($model, 'status')->dropDownList(OfferInfo::$statusLabels); ?>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'plus')->textInput(['disabled' => true]); ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'minus')->textInput(['disabled' => true]); ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'rating')->textInput(['disabled' => true]); ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
