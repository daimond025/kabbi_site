<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model landing\modules\community\models\Comment */

$this->render('/post/_postBreadcrumbs', ['post' => $model->post]);

$this->title = 'Update Comment: ' . $model->label;
$this->params['breadcrumbs'][] = [
    'label' => 'Comments',
    'url' => ['index', 'post_id' => $model->post_id],
];
$this->params['breadcrumbs'][] = [
    'label' => $model->label,
    'url' => ['view', 'id' => $model->comment_id],
];
$this->params['breadcrumbs'][] = 'Update';

?>

<div class="comment-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
