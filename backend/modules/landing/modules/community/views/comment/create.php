<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model landing\modules\community\models\Comment */

$this->render('/post/_postBreadcrumbs', ['post' => $model->post]);

$this->title = 'Create Comment';
$this->params['breadcrumbs'][] = [
    'label' => 'Comments',
    'url' => ['index', 'post_id' => $model->post_id],
];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="comment-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
