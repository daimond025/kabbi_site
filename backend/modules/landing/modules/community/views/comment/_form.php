<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use landing\modules\community\models\Post;
use landing\modules\community\models\user\CommunityUser;
use kartik\widgets\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model landing\modules\community\models\Comment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'post_id')->dropDownList(
            ArrayHelper::map(Post::find()->all(), 'post_id', 'label'), ['disabled' => true]);
    ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]); ?>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'author_id')->dropDownList(
                    ArrayHelper::map(CommunityUser::find()->orderBy([
                                'lastname' => SORT_ASC,
                                'firstname' => SORT_ASC
                            ])->all(), 'id', 'label')
                );
            ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'publishedDate')->widget(DateTimePicker::className(), [
                    'type' => DateTimePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd-mm-yyyy hh:ii',
                    ],
                ]);
            ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', [
            'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
