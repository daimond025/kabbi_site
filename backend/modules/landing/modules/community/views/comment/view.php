<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model landing\modules\community\models\Comment */

$this->render('/post/_postBreadcrumbs', ['post' => $model->post]);

$this->title = $model->label;
$this->params['breadcrumbs'][] =
        ['label' => 'Comments', 'url' => ['index', 'post_id' => $model->post_id]];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="comment-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('landingCommunity')): ?>
    <p>
        <?= Html::a('Update',
            ['update', 'id' => $model->comment_id],
            ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', [
                'delete',
                'id' => $model->comment_id,
                'post_id' => $model->post_id,
            ], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]);
        ?>
    </p>
    <?php endif; ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'comment_id',
            [
                'attribute' => 'post_id',
                'format' => 'raw',
                'value' => Html::a($model->post->label,
                        ['post/view', 'id' => $model->post_id]),
            ],
            'comment:ntext',
            [
                'attribute' => 'author_id',
                'format' => 'raw',
                'value' => Html::a($model->author->label,
                        ['community-user/view', 'id' => $model->author_id]),
            ],
            'publication_date:datetime',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]) ?>

</div>
