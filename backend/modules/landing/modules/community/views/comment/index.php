<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\landing\models\CommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->render('/post/_postBreadcrumbs', ['post' => $post]);

$this->title = 'Comments';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="comment-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('landingCommunity')): ?>
    <p>
        <?= Html::a('Create Comment',
            ['create', 'post_id' => $post->post_id],
            ['class' => 'btn btn-success']) ?>
    </p>
    <?php endif ?>
    <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'comment_id',
                [
                    'attribute' => 'post_id',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Html::a($model->post->label,
                            ['post/view', 'id' => $model->post_id]);
                    }
                ],
                'comment:html',
                [
                    'attribute' => 'author',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return Html::a($model->author->fullName,
                                ['community-user/view', 'id' => $model->author_id]);
                    }
                ],
                'publication_date:datetime',
                'created_at:datetime',
                'updated_at:datetime',
                [
                    'class'          => 'yii\grid\ActionColumn',
                    'visibleButtons' => [
                        'update' => \Yii::$app->user->can('landingCommunity'),
                        'delete' => \Yii::$app->user->can('landingCommunity'),
                    ],
                ],
            ],
        ]);
        ?>
    <?php Pjax::end(); ?>

</div>
