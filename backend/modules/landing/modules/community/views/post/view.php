<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use landing\modules\community\models\Post;
use landing\modules\community\models\Section;

/* @var $this yii\web\View */
/* @var $model landing\modules\community\models\Post */

$this->title = $model->label;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if (Yii::$app->user->can('landingCommunity')): ?>
            <?= Html::a('Update', ['update', 'id' => $model->post_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->post_id], [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method'  => 'post',
                ],
            ]) ?>
        <?php endif ?>
        <?= Html::a('Comments (' . $model->count_comments . ')',
            ['comment/index', 'post_id' => $model->post_id],
            ['class' => 'btn btn-default']); ?>
        <?php if ($model->section_id === Section::OFFERS) {
            echo Html::a($model->offerInfo->label,
                    ['offer-info/update', 'id' => $model->offerInfo->offer_info_id],
                    ['class' => 'btn btn-default']);
        } ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'post_id',
            [
                'attribute' => 'section_id',
                'format' => 'raw',
                'value' => Html::a($model->section->label,
                        ['section/view', 'id' => $model->section_id]),
            ],
            'title',
            'anons:html',
            'content:html',
            'meta_description',
            'publication_date:datetime',
            [
                'attribute' => 'status',
                'value' => Post::$statusLabels[$model->status],
            ],
            [
                'attribute' => 'author_id',
                'format' => 'raw',
                'value' => Html::a($model->author->label,
                        ['community-user/view', 'id' => $model->author_id]),
            ],
            'created_at:datetime',
            'updated_at:datetime',
            [
                'attribute' => 'tags',
                'label' => 'Теги',
                'value' => implode(', ', ArrayHelper::getColumn($model->tags, 'name')),
            ],
        ],
    ]);
    ?>

</div>
