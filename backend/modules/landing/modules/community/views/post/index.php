<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use landing\modules\community\models\Post;
use landing\modules\community\models\Section;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\landing\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Posts';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="post-index">

    <h1><?= Html::encode($this->title); ?></h1>
    <?php if (Yii::$app->user->can('landingCommunity')): ?>
        <p>
            <?= Html::a('Create Post', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif ?>
    <?php yii\widgets\Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'post_id',
                [
                    'attribute' => 'section_id',
                    'format' => 'raw',
                    'value' => function ($model, $key, $index, $column) {
                        return Html::a($model->section->label,
                                ['section/view', 'id' => $model->section_id]);
                    },
                    'filter' => ArrayHelper::map(Section::find()->all(), 'section_id', function ($model) {
                        return $model->label;
                    }),
                 ],
                 'title',
                 'publication_date:datetime',
                 [
                    'attribute' => 'status',
                    'label' => 'Статус',
                    'value' => function ($model, $key, $index, $column) {
                        return Post::$statusLabels[$model->status];
                    },
                    'filter' => Post::$statusLabels,
                 ],
                 [
                    'attribute' => 'author',
                    'label' => 'Автор',
                    'format' => 'raw',
                    'value' => function ($model, $key, $index, $column) {
                        return Html::a($model->author->fullName,
                            ['community-user/view', 'id' => $model->author_id]);
                    },
                ],
                 'created_at:datetime',
                [
                    'class'          => 'yii\grid\ActionColumn',
                    'visibleButtons' => [
                        'update' => \Yii::$app->user->can('landingCommunity'),
                        'delete' => \Yii::$app->user->can('landingCommunity'),
                    ],
                ],
            ],
        ]); ?>
    <?php yii\widgets\Pjax::end(); ?>

</div>
