<?php

use yii\helpers\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DateTimePicker;
use landing\modules\community\models\Section;
use landing\modules\community\models\Post;
use landing\modules\community\models\user\CommunityUser;


/* @var $this yii\web\View */
/* @var $model landing\modules\community\models\Post */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="post-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-3">
            <?=
            $form->field($model, 'section_id')->dropDownList(
                    ArrayHelper::map(Section::find()->orderBy('name')->all(), 'section_id', 'name'), [
                'disabled' => !$model->isNewRecord,
            ]);
            ?>
        </div>
        <div class="col-sm-9">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <?php
    $clientOptions = [
        'lang' => 'ru',
        'cleanSpaces' => false,
        'buttonsHide' => ['file'],
        'imageUpload' => ['/landing/community/post/upload-image'],
        'imageManagerJson' => false,
        'imageAllowExtensions' => ['jpg', 'png', 'gif'],
        'uploadImageFields' => [
            app()->request->csrfParam => app()->request->getCsrfToken(),
        ],
        'minHeight' => 140,
        'plugins' => ['fullscreen', 'imagemanager'],
        'initCallback' => new JsExpression('function () {
                var undo = this.button.addFirst("undo", "' . t('community', 'Undo') . '");
                var redo = this.button.addAfter("undo", "redo", "' . t('community', 'Redo') . '");

                this.button.addCallback(undo, this.buffer.undo);
                this.button.addCallback(redo, this.buffer.redo);
            }'),
        'pasteCallback' => new JsExpression('function (html) {'
                . 'return html.replace(/<(\/)?(?=h1|h2|h3|h4|h5|h6|p|cite|quote|blockquote|pre)[^>]*>/, "<$1p>")'
                . '.replace(/<(\/)?(?!p|img)[^>]*>/gi, " "); }'),
    ];

    echo $form->field($model, 'anons')->widget(\yii\redactor\widgets\Redactor::className(), [
        'clientOptions' => $clientOptions,
    ]);

    echo $form->field($model, 'content')->widget(\yii\redactor\widgets\Redactor::className(), [
        'clientOptions' => $clientOptions,
    ]);
    ?>

    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'meta_description')->textarea(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-3">
            <?=
            $form->field($model, 'publishedDate')->widget(DateTimePicker::className(), [
                'type' => DateTimePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'dd-mm-yyyy hh:ii',
                ],
            ]);
            ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'status')->dropDownList(Post::$statusLabels); ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'author_id')->dropDownList(
                    ArrayHelper::map(CommunityUser::find()->orderBy([
                                'lastname' => SORT_ASC,
                                'firstname' => SORT_ASC,
                            ])->all(), 'id', 'label'));
            ?>
        </div>
    </div>

    <?= $form->field($model, 'choosedTags')
        ->checkboxList(ArrayHelper::map($tags, 'tag_id', 'name'), [
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::tag('div', Html::checkbox($name, $checked, [
                        'value' => $value,
                        'label' => Html::encode($label),
                    ]), ['class' => 'col-sm-3']);
            },
            'tag' => 'div',
            'class' => 'row',
        ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
