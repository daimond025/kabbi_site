<?php

$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['post/index']];
$this->params['breadcrumbs'][] = [
    'label' => $post->label,
    'url' => ['post/view', 'id' => $post->post_id],
];