<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model landing\modules\community\models\user\CommunityUser */

$this->title = 'Update Community User: ' . ' ' . $model->label;
$this->params['breadcrumbs'][] = ['label' => 'Community Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->label, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';

?>

<div class="community-user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
