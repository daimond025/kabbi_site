<?php

use yii\helpers\Html;
use yii\grid\GridView;
use landing\modules\community\models\user\CommunityUser;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\landing\models\CommunityUserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Community Users';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="community-user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                'firstname',
                'lastname',
                'email:email',
                [
                    'attribute' => 'status',
                    'value' => function ($model, $key, $index, $column) {
                        return CommunityUser::$statusLabels[$model->status];
                    },
                    'filter' => CommunityUser::$statusLabels,
                ],
                'user_id',
                'created_at:datetime',
                [
                    'class'          => 'yii\grid\ActionColumn',
                    'visibleButtons' => [
                        'update' => \Yii::$app->user->can('landingCommunity'),
                        'delete' => \Yii::$app->user->can('landingCommunity'),
                    ],
                ],
            ],
        ]);
    ?>

</div>
