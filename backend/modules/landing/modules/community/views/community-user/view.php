<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use landing\modules\community\models\user\CommunityUser;

/* @var $this yii\web\View */
/* @var $model landing\modules\community\models\user\CommunityUser */

$this->title = $model->label;
$this->params['breadcrumbs'][] = ['label' => 'Community Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="community-user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->user->can('landingCommunity')): ?>
        <p>
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method'  => 'post',
                ],
            ]) ?>
        </p>
    <?php endif ?>

    <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'firstname',
                'lastname',
                'email:email',
                [
                    'attribute' => 'status',
                    'value' => CommunityUser::$statusLabels[$model->status],
                ],
                'user_id',
                'created_at:datetime',
                'updated_at:datetime',
            ],
        ])
    ?>

</div>
