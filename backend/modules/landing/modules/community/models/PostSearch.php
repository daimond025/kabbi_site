<?php

namespace app\modules\landing\modules\community\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use landing\modules\community\models\Post;

/**
 * PostSearch represents the model behind the search form about `landing\modules\community\models\Post`.
 */
class PostSearch extends Post
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'section_id', 'author_id', 'count_comments'], 'integer'],
            [['title', 'anons', 'content', 'status', 'author', 'section'], 'safe'],
        ];
    }

    public function attributes() {
        return array_merge(parent::attributes(), ['author']);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Post::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'post_id' => $this->post_id,
            'author_id' => $this->author_id,
            'count_comments' => $this->count_comments,
            Post::tableName() . '.section_id' => $this->section_id,
            Post::tableName() . '.status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'anons', $this->anons])
            ->andFilterWhere(['like', 'content', $this->content]);

        $query->andFilterWhere([
            'or',
            ['like', 'lastname', $this->getAttribute('author')],
            ['like', 'firstname', $this->getAttribute('author')]
        ]);

        return $dataProvider;
    }
}
