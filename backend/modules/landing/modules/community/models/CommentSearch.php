<?php

namespace app\modules\landing\modules\community\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use landing\modules\community\models\Comment;

/**
 * CommentSearch represents the model behind the search form about `landing\modules\community\models\Comment`.
 */
class CommentSearch extends Comment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comment_id', 'post_id', 'author_id', 'publication_date', 'created_at', 'updated_at'], 'integer'],
            [['comment', 'author'], 'safe'],
        ];
    }

    public function attributes() {
        return array_merge(parent::attributes(), ['author']);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Comment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'comment_id' => $this->comment_id,
            'post_id' => $this->post_id,
            'author_id' => $this->author_id,
            'publication_date' => $this->publication_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);
        $query->andFilterWhere([
            'or',
            ['like', 'lastname', $this->getAttribute('author')],
            ['like', 'firstname', $this->getAttribute('author')]
        ]);

        return $dataProvider;
    }
}
