<?php

namespace app\modules\landing\modules\community\controllers;

use Yii;
use landing\modules\community\models\Post;
use landing\modules\community\models\Tag;
use app\modules\landing\modules\community\models\PostSearch;
use app\modules\landing\modules\community\models\CommentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actions() {
        return [
            'upload-image' => [
                'class' => \landing\components\actions\ImageUploadAction::className(),
                'web' => app()->params['landingUrl'],
                'webroot' => '@landing/web',
                'path' => app()->params['upload'] . '/post/images/',
        ]];
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->query->innerJoinWith(['author'])->innerJoinWith('section');

        $dataProvider->sort->attributes['author'] = [
            'asc' => ['lastname' => SORT_ASC, 'firstname' => SORT_ASC],
            'desc' => ['lastname' => SORT_DESC, 'firstname' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['section'] = [
            'asc' => ['name' => SORT_ASC],
            'desc' => ['name' => SORT_DESC],
        ];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Post model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $searchModel = new CommentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->query->byPostId($id)->joinWith(['author']);
        $dataProvider->sort->attributes['author'] = [
            'asc' => ['lastname' => SORT_ASC, 'firstname' => SORT_ASC],
            'desc' => ['lastname' => SORT_DESC, 'firstname' => SORT_DESC],
        ];

        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Post();
        $tags = Tag::find()->orderBy('name')->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->post_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'tags' => $tags,
            ]);
        }
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $tags = Tag::find()->orderBy('name')->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->post_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'tags' => $tags,
            ]);
        }
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::find()
                ->joinWith('section')
                ->joinWith('author')
                ->with('offerInfo')
                ->where(['post_id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
