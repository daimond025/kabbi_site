<?php

namespace app\modules\landing\modules\community\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use landing\modules\community\models\OfferInfo;

/**
 * OfferInfoController implements the CRUD actions for OfferInfo model.
 */
class OfferInfoController extends Controller
{

    public function init() {
        app()->subscribeManager->registerSubscribeOffer();

        parent::init();
    }

    /**
     * Updates an existing OfferInfo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['post/view', 'id' => $model->post_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the OfferInfo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OfferInfo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OfferInfo::find()->with('post')
                ->where(['offer_info_id' => $id])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
