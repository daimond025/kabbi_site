<?php

namespace app\modules\landing\modules\community;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\landing\modules\community\controllers';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['landingCommunityRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['landingCommunity'],
                    ],
                ],
            ],
        ];
    }
}
