<?php

use common\modules\billing\models\Operation;
use common\modules\billing\models\OperationType;
use frontend\modules\tenant\models\Currency;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this \yii\web\View */
/* @var $accountForm \backend\modules\tenant\models\AccountForm */

if (isset($accountForm)): ?>
    <hr>

    <? $form = ActiveForm::begin([
        'options' => [
            'data' => [
                'pjax' => true,
            ],
        ],
    ]) ?>
    <div>
        <p>
            <?= t('balance', 'Current balance') ?>:
            <b>
                <?= app()->formatter->asMoney(
                    $accountForm->balance, Currency::getCurrencySymbol($accountForm->currency_id)) ?>
            </b>
        </p>
    </div>

    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($accountForm, 'operationTypeId')->dropDownList(
                ArrayHelper::map(OperationType::find()->where([
                    'in',
                    'type_id',
                    [Operation::TYPE_ID_INCOME, Operation::TYPE_ID_EXPENSES],
                ])
                    ->all(), 'type_id', function ($model) {
                    return t('balance', $model->name);
                })) ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($accountForm, 'sum')->textInput([
                'placeholder' => '0',
            ])->label(t('balance', 'Sum')
                . ' (' . Currency::getCurrencySymbol($accountForm->currency_id) . ')') ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($accountForm, 'comment')->textarea(['rows' => 1]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitInput(t('app', 'Run'), ['class' => 'btn btn-success']) ?>
    </div>

    <? ActiveForm::end() ?>

<? endif ?>