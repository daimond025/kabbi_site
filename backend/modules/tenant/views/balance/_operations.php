<?php

use common\modules\billing\models\Operation;
use common\modules\billing\models\Transaction;
use frontend\modules\tenant\models\Currency;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $operations Operation[] */

if (!empty($operations)): ?>

    <hr>

    <h3><?= t('balance', 'History') ?></h3>

    <?

    $debit          = [];
    $credit         = [];
    $lastIndex      = count($operations);
    $currencySymbol = null;
    $formatter      = app()->formatter;

    echo GridView::widget([
        'dataProvider'     => new \yii\data\ArrayDataProvider([
            'allModels' => $operations,
        ]),
        'columns'          => [
            [
                'attribute' => 'date',
                'label'     => t('balance', 'Operation date'),
                'footer'    => t('balance', 'Total for the period'),
            ],
            [
                'format' => 'raw',
                'value'  => function ($model) {
                    if (!empty($model->transaction->terminal_transact_number) && $model->transaction->payment_method == Transaction::TERMINAL_ELECSNET_PAYMENT) {
                        $comment = Html::encode(t('balance', 'Terminal №{terminal} Check №{check}', [
                            'terminal' => substr($model->transaction->terminal_transact_number, 0, 8),
                            'check'    => substr($model->transaction->terminal_transact_number, 8, 4),
                        ]));
                    } else {
                        $comment = $model->transaction->comment;
                    }

                    return t('balance', $model->type->name)
                    . (empty($comment) ? '' : '<br>' . t('balance', 'Comment') . ': ' . Html::encode(t('balance',
                            $comment)));
                },
                'label'  => t('balance', 'Operation'),
            ],
            'debit'  => [
                'value' => function ($model) use ($formatter, &$currencySymbol) {
                    return $model->type_id == Operation::TYPE_ID_INCOME
                        ? $formatter->asMoney(+$model->sum, $currencySymbol) : '';
                },
                'label' => t('balance', 'Debet'),
            ],
            'credit' => [
                'value' => function ($model) use ($formatter, &$currencySymbol) {
                    return $model->type_id == Operation::TYPE_ID_EXPENSES
                        ? $formatter->asMoney(+$model->sum, $currencySymbol) : '';
                },
                'label' => t('balance', 'Credit'),
            ],
        ],
        'beforeRow'        => function ($model) use (&$currencySymbol, &$debit, &$credit) {
            $currencySymbol = Currency::getCurrencySymbol($model->account->currency_id);
            switch ($model->type_id) {
                case Operation::TYPE_ID_INCOME:
                    $debit[$currencySymbol] += $model->sum;
                    break;
                case Operation::TYPE_ID_EXPENSES:
                    $credit[$currencySymbol] += $model->sum;
                    break;
            }
        },
        'afterRow'         => function ($model, $key, $index, $grid) use ($formatter, $lastIndex, &$debit, &$credit) {
            //            if ($index == $lastIndex - 1) {
            $items = [];
            foreach ($debit as $key => $value) {
                $items[] = $formatter->asMoney($value, $key);
            }
            $grid->columns['debit']->footer = implode(", ", $items);

            $items = [];
            foreach ($credit as $key => $value) {
                $items[] = $formatter->asMoney($value, $key);
            }
            $grid->columns['credit']->footer = implode(", ", $items);
            //            }
        },
        'tableOptions'     => [
            'class' => 'table table-striped table-bordered',
        ],
        'showOnEmpty'      => false,
        'showFooter'       => true,
        'footerRowOptions' => ['style' => 'font-weight:bold'],
        'layout'           => "{sorter}\n{items}\n{pager}",
    ]); ?>

<? endif ?>