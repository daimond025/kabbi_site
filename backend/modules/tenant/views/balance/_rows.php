<?php

use common\modules\billing\models\Operation;
use frontend\modules\tenant\models\Currency;

/* @var $this \yii\web\View */
/* @var $operations Operation[]|null */


?>

<?
$debit         = [];
$credit        = [];
$lastOperation = null;
$formatter     = app()->formatter;

foreach ($operations as $operation):
    $lastOperation = $operation; ?>
    <tr>
        <td><span style="font-size: 14px"><?= $formatter->asDateTime($operation->date) ?></span></td>
        <td><?= 1//$operation->getTextOperation()     ?></td>
        <?
        $currencySymbol = Currency::getCurrencySymbol($operation->account->currency_id);
        $debitStr       = null;
        $creditStr      = null;

        switch ($operation->type_id) {
            case Operation::TYPE_ID_INCOME:
                $debit[$currencySymbol] += $operation->sum;
                $debitStr = $formatter->asMoney(+$operation->sum, $currencySymbol);
                break;
            case Operation::TYPE_ID_EXPENSES:
                $credit[$currencySymbol] += $operation->sum;
                $creditStr = $formatter->asMoney(+$operation->sum, $currencySymbol);
                break;
        }
        ?>
        <td style="text-align: right;"><?= $debitStr ?></td>
        <td style="text-align: right;"><?= $creditStr ?></td>
    </tr>
<? endforeach ?>

<tr class="mt_spec">
    <td><?= $formatter->asDate($lastOperation->date) ?></td>
    <td class="mt_itog"><b><?= t('balance', 'Total for the period') ?></b></td>
    <?
    array_walk($debit, function (&$item, $key) use ($formatter) {
        $item = $formatter->asMoney($item, $key);
    });
    $debitStr = implode(", ", $debit);

    array_walk($credit, function (&$item, $key) use ($formatter) {
        $item = $formatter->asMoney($item, $key);
    });
    $creditStr = implode(", ", $credit);
    ?>
    <td style="text-align: right;"><b><?= $debitStr ?></b></td>
    <td style="text-align: right;"><b><?= $creditStr ?></b></td>
</tr>
