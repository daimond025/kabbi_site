<?php

use backend\modules\tenant\models\TenantSearch;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel TenantSearch */

$this->title = t('tenant', 'Tenant accounts');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="tenant-tariff-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <? \yii\widgets\Pjax::begin() ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'domain',
            'create_time',
            'company_name',
            'full_company_name',
            'contact_name',
            'contact_second_name',
            'contact_last_name',
            'contact_phone',
            'contact_email:email',
            [
                'class'          => 'yii\grid\ActionColumn',
                'template'       => '{view}',
                'buttons'        => [
                    'view' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                            Url::to(['view', 'id' => $model->tenant_id]));
                    },
                ],
                'visibleButtons' => [
                    'view' => \Yii::$app->user->can('tenant'),
                ],
            ],
        ],
    ]); ?>
    <? \yii\widgets\Pjax::end() ?>

</div>
