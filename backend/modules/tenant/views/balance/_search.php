<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $tenantSearch \backend\modules\tenant\models\TenantSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-one-time-job-search">

    <?php $form = ActiveForm::begin([
        'action'  => ['index'],
        'method'  => 'get',
        'options' => [
            'data' => [
                'pjax' => true,
            ],
        ],
    ]); ?>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($tenantSearch, 'tenantId')->dropDownList(
                $tenantSearch->getTenants(),
                ['prompt' => t('tenant', 'Choose tenant...')]
            ) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
