<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $accountForm \backend\modules\tenant\models\AccountForm */
/* @var $operations \common\modules\billing\models\Operation[] */

$this->title                   = t('tenant', 'Tenant account') . ': ' . $accountForm->tenant_id;
$this->params['breadcrumbs'][] = ['label' => t('tenant', 'Tenant accounts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenant-tariff-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <? Pjax::begin() ?>

    <?= $this->render('_account', compact('accountForm')) ?>

    <?= $this->render('_operations', compact('operations')) ?>

    <? Pjax::end() ?>
</div>
