<?php

namespace backend\modules\tenant\controllers;

use app\modules\setting\models\TenantSearch;
use backend\modules\tenant\models\AccountForm;
use backend\modules\tenant\models\TenantAccountSearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * BalanceController implements the CRUD actions for Module model.
 */
class BalanceController extends Controller
{
    const FILTER_TODAY = 'today';
    const FILTER_MONTH = 'month';
    const FILTER_PERIOD = 'period';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index'],
                        'roles'   => ['tenantRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['tenant'],
                    ],
                ],
            ],
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Getting filtered dates
     *
     * @param $data
     *
     * @return array
     */
    private function getFilteredDates($data = null)
    {
        $filter = isset($data['filter']) ? $data['filter'] : null;
        switch ($filter) {
            case self::FILTER_MONTH:
                $dateFrom = strtotime(date('1.m.Y'));

                return [$dateFrom, strtotime('+1month -1second', $dateFrom)];
            case self::FILTER_PERIOD:
                $dateFrom = isset($data['first_date']) ? $data['first_date'] : null;
                $dateTo   = isset($data['second_date']) ? $data['second_date'] : null;

                return [strtotime($dateFrom), strtotime('+1day -1second', strtotime($dateTo))];
            case self::FILTER_TODAY:
            default:
                $dateFrom = strtotime(date('d.m.Y'));

                return [$dateFrom, strtotime('+1day -1second', $dateFrom)];
        }
    }

    /**
     * Getting payment history
     *
     * @param boolean $withoutSearch
     *
     * @return string
     */
    //    public function actionPaymentHistory($withoutSearch = null)
    //    {
    //        $paymentHistory = new PaymentHistory();
    //
    //        list($dateFrom, $dateTo) = $this->getFilteredDates(post());
    //        $operations = $paymentHistory->getPaymentTransactions($dateFrom, $dateTo);
    //
    //        if (empty($withoutSearch)) {
    //            return $this->renderAjax('paymentHistory', compact('operations'));
    //        } else {
    //            return $this->renderAjax('_grid', compact('operations'));
    //        }
    //    }

    /**
     * Lists all Tenant models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new TenantSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->sort = ['defaultOrder' => ['domain' => SORT_ASC]];

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel'  => $searchModel,
        ]);
    }

    /**
     * @param integer $id
     *
     * @return string|\yii\web\Response
     */
    public function actionView($id)
    {
        $accountForm = AccountForm::find()
            ->tenantAccounts()
            ->byTenantId($id)
            ->one();

        if (!empty($id) && empty($accountForm)) {
            $accountForm = new AccountForm(['tenant_id' => $id]);
        }

        if (!empty($accountForm)) {
            if (app()->request->isPost && $accountForm->load(post())) {
                if ($accountForm->validate()) {
                    if ($accountForm->doOperation()) {
                        session()->setFlash('success',
                            t('transaction', 'The operation was completed successfully'));
                        $accountForm->sum = 0;
                    } else {
                        session()->setFlash('error',
                            t('error', 'Service is temporarily unavailable. Notify to administrator, please.'));
                    }
                } else {
                    session()->setFlash('error', implode('<br>', $accountForm->getFirstErrors()));
                }

                return $this->refresh();
            }

            $dateFrom   = null;
            $dateTo     = null;
            $operations = $accountForm->getOperationsByPeriod($dateFrom, $dateTo);
        }

        return $this->render('view', compact('accountForm', 'operations'));
    }
}
