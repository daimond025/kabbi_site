/* global jQuery */
(function ($) {
    'use strict';

    var app = {
        init: function () {
            this.registerTenantChangeEvent();
        },

        registerTenantChangeEvent: function () {
            $(document).on('change', '#tenantsearch-tenantid', function (e) {
                $(e.target).closest('form').submit();
            });
        }
    };

    app.init();
})(jQuery);