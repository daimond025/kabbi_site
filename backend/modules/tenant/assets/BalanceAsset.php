<?php


namespace backend\modules\tenant\assets;

use yii\web\AssetBundle;

/**
 * Class BalanceAsset
 * @package backend\modules\tenant\assets
 */
class BalanceAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/tenant/assets/';
    public $publishOptions = [
        'forceCopy' => true,
        'only'      => [
            'js/balance.js',
        ],
    ];
    public $css = [
    ];
    public $js = [
        'js/balance.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

}
