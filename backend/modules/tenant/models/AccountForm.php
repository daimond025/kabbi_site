<?php

namespace backend\modules\tenant\models;

use common\components\billing\Billing;
use common\components\billing\models\Transaction;
use common\components\serviceApi\EmailTypes;
use common\components\serviceApi\ServiceApi;
use common\modules\billing\models\Account;
use common\modules\billing\models\Operation;
use common\modules\tenant\models\Tenant;
use frontend\modules\tenant\models\Currency;
use Yii;

/**
 * Class AccountForm
 * @package backend\modules\tenant\models
 *
 * @property integer $operationTypeId
 * @property float   $sum
 * @property string  $comment
 */
class AccountForm extends Account
{
    public $operationTypeId;
    public $sum;
    public $comment;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->acc_kind_id = isset($this->acc_kind_id) ? $this->acc_kind_id : Account::KIND_ID_TENANT;
        $this->acc_type_id = isset($this->acc_type_id) ? $this->acc_type_id : Account::TYPE_ID_PASSIVE;
        $this->balance     = isset($this->balance) ? $this->balance : 0;
        $this->currency_id = isset($this->currency_id) ? $this->currency_id : Currency::CURRENCY_ID_RUS_RUBLE;

        return parent::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['operationTypeId', 'sum'], 'required'],
            ['operationTypeId', 'integer'],
            [
                'sum',
                'number',
                'min' => -999999999.99,
                'max' => 999999999.99,
            ],
            ['comment', 'string'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'operationTypeId' => t('balance', 'Operation'),
            'sum'             => t('balance', 'Sum'),
            'comment'         => t('balance', 'Comment'),
        ]);
    }

    /**
     * Sending email
     *
     * @param integer $tenantId
     * @param float   $sum
     * @param integer $currencyId
     *
     * @throws \yii\base\InvalidConfigException
     */
    private function sendEmail($tenantId, $sum, $currencyId)
    {
        /** @var ServiceApi $serviceApi */
        $serviceApi = app()->get('serviceApi');
        $tenant     = Tenant::findOne($tenantId);
        $money      = app()->formatter->asMoney($sum, Currency::getCurrencySymbol($currencyId));

        $serviceApi->sendEmail($tenantId, null, EmailTypes::PAYMENT_NOTICE, null,
            $tenant->contact_email, [
                'user_name' => implode(' ', [$tenant->contact_name, $tenant->contact_second_name]),
                'payment'   => $money,
            ]);
    }

    /**
     * Do billing operation
     * @return boolean
     */
    public function doOperation()
    {
        $billing = Yii::createObject([
            'class'   => Billing::className(),
            'gearman' => app()->gearman,
        ]);

        switch ($this->operationTypeId) {
            case Operation::TYPE_ID_INCOME:
                $result = $billing->depositTenantBalance($this->tenant_id, $this->sum,
                    $this->currency_id, $this->comment);
                if ($result) {
                    $this->sendEmail($this->tenant_id, $this->sum, $this->currency_id);
                }

                return $result;
            case Operation::TYPE_ID_EXPENSES:
                return $billing->withdrawalTenantBalance($this->tenant_id, $this->sum,
                    $this->currency_id, $this->comment);
        }
    }

    public function getOperationsByPeriod($dateFrom = null, $dateTo = null)
    {
        $dateFrom = isset($dateFrom) ? date('Y-m-d H:i:s', $dateFrom) : null;
        $dateTo   = isset($dateTo) ? date('Y-m-d H:i:s', $dateTo) : null;

        return Operation::find()
            ->alias('o')
            ->innerJoinWith(['transaction t', 'account a'], true)
            ->where([
                'a.acc_kind_id' => Account::KIND_ID_TENANT,
                'a.acc_type_id' => Account::TYPE_ID_PASSIVE,
                'a.tenant_id'   => $this->tenant_id,
            ])
            ->andWhere([
                'or',
                ['t.type_id' => Transaction::TYPE_ID_GOOTAX_PAYMENT],
                ['!=', 't.payment_method', Transaction::PAYMENT_METHOD_TERMINAL_MILLION],
            ])
            ->andFilterWhere(['>=', 'o.date', $dateFrom])
            ->andFilterWhere(['<=', 'o.date', $dateTo])
            ->orderBy(['o.date' => SORT_DESC])
            ->all();
    }
}