<?php

namespace backend\modules\tenant;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\tenant\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
