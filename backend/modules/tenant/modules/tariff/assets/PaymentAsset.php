<?php


namespace backend\modules\tenant\modules\tariff\assets;

use yii\web\AssetBundle;

class PaymentAsset extends AssetBundle
{

    public $sourcePath = '@app/modules/tenant/modules/tariff/assets/';
    public $publishOptions = [
        'forceCopy' => true,
        'only'      => [
            'js/payment.js',
        ],
    ];
    public $css = [
    ];
    public $js = [
        'js/payment.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'kartik\date\DatePickerAsset',
    ];

}
