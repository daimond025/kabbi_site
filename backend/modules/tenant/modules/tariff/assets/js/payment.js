/* global jQuery */
(function ($) {
    var app = {

        init: function () {
            this.registerChangeEvent();
            this.registerIfChangedEvents();
            this.registerFixedDiscountEvent();
            this.recalcPaymentSum();
        },

        registerChangeEvent: function () {
            var _this = this;
            $('#paymentform-period, #paymentform-discount').on('change', function () {
                _this.recalcPaymentSum();
            });
        },

        registerIfChangedEvents: function () {
            var _this = this;
            $('[name="PaymentForm[tariffs][]"], [name="PaymentForm[options][]"], [name="PaymentForm[services][]"]')
                .on('ifChanged', function () {
                    _this.recalcPaymentSum();
                });
            $('[name="PaymentForm[tariffs][]"]')
                .on('ifChecked', function (e) {
                    var module = $(e.target).data('module');
                    $('[name="PaymentForm[tariffs][]"][data-module=' + module + ']')
                        .not($(e.target))
                        .iCheck('uncheck');
                });
        },

        registerFixedDiscountEvent: function () {
            var _this = this;
            $('#paymentform-fixed_discount')
                .on('ifChanged', function (e) {
                    $('#paymentform-discount').prop('disabled', !$(this).prop('checked'));
                    _this.recalcPaymentSum();
                });
        },

        recalcPaymentSum: function () {
            var paymentSum = 0;
            var paymentServiceSum = 0;
            var sumDiscount = 0;
            var period = +$('#paymentform-period').val();

            $('[name="PaymentForm[tariffs][]"], [name="PaymentForm[options][]"]').each(function () {
                var price = +$(this).data('price');
                if ($(this).prop('checked') && price) {
                    paymentSum += period * price;
                }
            });
            $('[name="PaymentForm[services][]"]').each(function () {
                var price = +$(this).data('price');
                if ($(this).prop('checked') && price) {
                    paymentServiceSum += price;
                }
            });

            if ($('#paymentform-fixed_discount').prop('checked')) {
                sumDiscount = $('#paymentform-discount').val();
                $('#paymentform-payment_sum').val(paymentSum + paymentServiceSum);
                $('#paymentform-summary').val(paymentSum + paymentServiceSum - sumDiscount);
            } else {
                $.get('/tenant/tariff/discount/get-discount?period=' + period)
                    .done(function (data) {
                        if (data) {
                            sumDiscount = Math.round(paymentSum * data) / 100;
                        }
                    })
                    .fail(function (data) {
                        console.log('fail', data);
                    })
                    .always(function () {
                        $('#paymentform-payment_sum').val(paymentSum + paymentServiceSum);
                        $('#paymentform-discount').val(sumDiscount);
                        $('#paymentform-summary').val(paymentSum + paymentServiceSum - sumDiscount);
                    });
            }
        }
    };

    app.init()
})(jQuery);
