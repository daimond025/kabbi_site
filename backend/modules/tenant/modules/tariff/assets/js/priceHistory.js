/* global jQuery */
(function ($) {
    var app = {
        init: function () {
            this.registerEntityTypeOnChangeEvent();
        },

        registerEntityTypeOnChangeEvent: function () {
            $('#pricehistory-entity_type_id').on('change', function () {
                $.get(
                    '/tenant/tariff/price-history/get-entity-list',
                    { entityTypeId: $(this).val() }
                )
                    .done(function (data) {
                        $('#pricehistory-entity_id').html(data);
                    })
                    .fail(function (data) {
                        console.log('fail', data);
                    });
            });
        }
    };

    app.init();
})(jQuery);
