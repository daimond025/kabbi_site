<?php


namespace backend\modules\tenant\modules\tariff\assets;

use yii\web\AssetBundle;

class PriceHistoryAsset extends AssetBundle
{

    public $sourcePath = '@app/modules/tenant/modules/tariff/assets/';
    public $publishOptions = [
        'forceCopy' => true,
        'only'      => [
            'js/priceHistory.js',
        ],
    ];
    public $css = [
    ];
    public $js = [
        'js/priceHistory.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

}
