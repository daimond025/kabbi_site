<?php

namespace backend\modules\tenant\modules\tariff;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\tenant\modules\tariff\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
