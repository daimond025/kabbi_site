<?php

namespace backend\modules\tenant\modules\tariff\models;

use app\modules\setting\models\Tenant;
use common\modules\tenant\modules\tariff\models\Tariff;
use common\modules\tenant\modules\tariff\models\TenantTariff;
use Yii;
use yii\data\ActiveDataProvider;

class TenantTariffSearch extends TenantTariff
{
    public $domain;
    public $tariff;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['domain', 'tariff'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TenantTariff::find()->alias('tt')->joinWIth('tariff t')->joinWith('tariff.module tm');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->domain)) {
            $query->andWhere([
                'in',
                'tenant_id',
                Tenant::find()->select('tenant_id')->andWhere(['like', 'domain', $this->domain]),
            ]);
        }

        if (!empty($this->tariff)) {
            $query->andWhere(['like', 'CONCAT(t.name," ","(",tm.name,")")', $this->tariff]);
        }

        return $dataProvider;
    }
}
