<?php

namespace backend\modules\tenant\modules\tariff\models;

use Yii;
use common\modules\tenant\modules\tariff\models\DemoTariff;
use common\modules\tenant\modules\tariff\models\Tariff;

class TariffForm extends Tariff
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['demo', 'safe'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'demo' => Yii::t('tariff', 'Demo'),
        ]);
    }

    public function getDemo()
    {
        return isset($this->demoTariff) ? 1 : 0;
    }

    public function setDemo($value)
    {
        if (empty($value)) {
            if (!empty($this->demoTariff)) {
                $this->demoTariff->delete();
            }
        } elseif (empty($this->demoTariff)) {
            $demoTariff = new DemoTariff([
                'tariff_id' => $this->id,
            ]);
            $demoTariff->save();
        }
    }

}