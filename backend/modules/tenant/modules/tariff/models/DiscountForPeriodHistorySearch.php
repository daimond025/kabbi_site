<?php

namespace backend\modules\tenant\modules\tariff\models;

use common\modules\tenant\modules\tariff\models\DiscountForPeriodHistory;
use Yii;
use yii\data\ActiveDataProvider;

class DiscountForPeriodHistorySearch extends DiscountForPeriodHistory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            [['period', 'percent'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DiscountForPeriodHistory::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params, '');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['period' => $this->period]);
        $query->andFilterWhere(['percent' => $this->percent]);

        return $dataProvider;
    }
}
