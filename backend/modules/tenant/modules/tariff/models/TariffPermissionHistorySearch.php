<?php

namespace backend\modules\tenant\modules\tariff\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\modules\tenant\modules\tariff\models\TariffPermissionHistory;

class TariffPermissionHistorySearch extends TariffPermissionHistory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            ['permission_id', 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TariffPermissionHistory::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params, '');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['permission_id' => $this->permission_id]);

        return $dataProvider;
    }
}
