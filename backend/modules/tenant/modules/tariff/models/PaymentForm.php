<?php

namespace backend\modules\tenant\modules\tariff\models;

use backend\modules\tenant\modules\tariff\components\DiscountService;
use common\modules\tenant\modules\tariff\models\PaymentForAdditionalOption;
use common\modules\tenant\modules\tariff\models\PaymentForOneTimeService;
use frontend\modules\tenant\models\Currency;
use Yii;
use common\modules\tenant\modules\tariff\components\behaviors\DatetimeBehavior;
use common\modules\tenant\modules\tariff\models\OneTimeService;
use common\modules\tenant\modules\tariff\models\Payment;
use common\modules\tenant\modules\tariff\models\PaymentForTariff;
use common\modules\tenant\modules\tariff\models\Tariff;
use common\modules\tenant\modules\tariff\models\TariffAdditionalOption;
use yii\base\ErrorException;
use yii\helpers\ArrayHelper;

class PaymentForm extends Payment
{
    public $domain;

    private $summary;

    public $tenant;

    /**
     * @var integer[]
     */
    protected $tariffs;

    /**
     * @var integer[]
     */
    protected $options;

    /**
     * @var integer[]
     */
    protected $services;

    /**
     * @var integer
     */
    public $started_at;

    /**
     * @var integer
     */
    public $period;

    /**
     * @inheritdoc
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['tariffs', 'options', 'services'], 'safe'],
            [['started_at', 'period'], 'required'],
            ['period', 'integer', 'min' => 1],
            ['startedAt', 'date', 'format' => 'php:d.m.Y'],
            ['domain','safe']
        ]);
    }

    public function checkRequiredPeriod($model)
    {
        return !empty($model->tariffs) || !empty($model->options);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'tariffs'    => Yii::t('tariff', 'Tariffs'),
            'options'    => Yii::t('tariff-additional-option', 'Additional Options'),
            'services'   => Yii::t('one-time-service', 'One-time Services'),
            'started_at' => Yii::t('payment-for-tariff', 'Started At'),
            'startedAt'  => Yii::t('payment-for-tariff', 'Started At'),
            'period'     => Yii::t('payment-for-tariff', 'Period'),
            'summary'    => Yii::t('payment-for-tariff', 'Summary'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            [
                'class'   => DatetimeBehavior::className(),
                'options' => [
                    'startedAt' => [
                        'field'  => 'started_at',
                        'format' => 'd.m.Y',
                    ],
                ],
            ],
        ]);
    }

    public function setTariffs($tariffIds)
    {
        $this->paymentForTariffs = (array)$tariffIds;
    }

    public function getTariffs()
    {
        if (isset($this->tariffs)) {
            return $this->tariffs;
        } else {
            return ArrayHelper::getColumn($this->paymentForTariffs, 'tariff_id');
        }
    }

    public function setOptions($optionIds)
    {
        $this->options = (array)$optionIds;
    }

    public function getOptions()
    {
        if (isset($this->options)) {
            return $this->options;
        } else {
            return ArrayHelper::getColumn($this->paymentForAdditionalOptions, 'option_id');
        }
    }

    public function setServices($serviceIds)
    {
        $this->services = (array)$serviceIds;
    }

    public function getServices()
    {
        if (isset($this->services)) {
            return $this->services;
        } else {
            return ArrayHelper::getColumn($this->paymentForOneTimeServices, 'service_id');
        }
    }

    public function getSummary()
    {
        return $this->payment_sum - $this->discount;
    }

    /**
     * Getting available tariffs
     * @return array|\common\modules\tenant\modules\tariff\models\Tariff[]
     */
    public function getAvailableTariffs()
    {
        return Tariff::find()
            ->alias('t')
            ->joinWith('module m', true)
            ->active('t')
            ->byDefault(false)
            ->byDemo(false, 't')
            ->orderBy(['m.name' => SORT_ASC])
            ->indexBy('id')
            ->all();
    }

    /**
     * Getting available additional options
     * @return array|\common\modules\tenant\modules\tariff\models\TariffAdditionalOption[]
     */
    public function getAvailableOptions()
    {
        return TariffAdditionalOption::find()
            ->active()
            ->indexBy('id')
            ->all();
    }

    /**
     * Getting available one-time services
     * @return array|\common\modules\tenant\modules\tariff\models\TariffAdditionalOption[]
     */
    public function getAvailableServices()
    {
        return OneTimeService::find()
            ->active()
            ->indexBy('id')
            ->all();
    }

    /**
     * Saving tariffs
     * @throws ErrorException
     */
    private function saveTariffs()
    {
        PaymentForTariff::deleteAll(['payment_id' => $this->id]);

        $ids = is_array($this->tariffs)
            ? $this->tariffs : [$this->tariffs];

        if (!empty($ids)) {
            foreach ($ids as $id) {
                $tariff = Tariff::findone($id);
                if (empty($tariff)) {
                    continue;
                }

                //                $price = $tariff->getPriceByDate($this->purchased_at);
                //                if (!$price) {
                //                    $price = 0;
                //                }

                $paymentSum = $tariff->price * $this->period;

                $model             = new PaymentForTariff([
                    'payment_id'  => $this->id,
                    'tariff_id'   => $id,
                    'started_at'  => $this->started_at,
                    'period'      => $this->period,
                    'price'       => $tariff->price,
                    'payment_sum' => $paymentSum,
                    'currency_id' => $this->currency_id,
                ]);
                $this->payment_sum += $paymentSum;

                if (!$model->save()) {
                    throw new ErrorException(implode("; ", $model->getFirstErrors()));
                }
            }
        }
    }

    /**
     * Saving additional option
     * @throws ErrorException
     */
    private function saveOptions()
    {
        PaymentForAdditionalOption::deleteAll(['payment_id' => $this->id]);

        $ids = is_array($this->options)
            ? $this->options : [$this->options];

        if (!empty($ids)) {
            foreach ($ids as $id) {
                $additionalOption = TariffAdditionalOption::findone($id);
                if (empty($additionalOption)) {
                    continue;
                }

                $paymentSum = $additionalOption->price * $this->period;

                $model             = new PaymentForAdditionalOption([
                    'payment_id'  => $this->id,
                    'option_id'   => $id,
                    'started_at'  => $this->started_at,
                    'period'      => $this->period,
                    'price'       => $additionalOption->price,
                    'payment_sum' => $paymentSum,
                    'currency_id' => $this->currency_id,
                ]);
                $this->payment_sum += $paymentSum;

                if (!$model->save()) {
                    throw new ErrorException(implode("; ", $model->getFirstErrors()));
                }
            }
        }
    }

    /**
     * Saving one-time services
     * @throws ErrorException
     */
    private function saveServices()
    {
        PaymentForOneTimeService::deleteAll(['payment_id' => $this->id]);

        $ids = is_array($this->services)
            ? $this->services : [$this->services];

        if (!empty($ids)) {
            foreach ($ids as $id) {
                $service = OneTimeService::findone($id);
                if (empty($service)) {
                    continue;
                }

                $model             = new PaymentForOneTimeService([
                    'payment_id'  => $this->id,
                    'service_id'  => $id,
                    'price'       => $service->price,
                    'payment_sum' => $service->price,
                    'currency_id' => $this->currency_id,
                ]);
                $this->payment_sum += $service->price;

                if (!$model->save()) {
                    throw new ErrorException(implode("; ", $model->getFirstErrors()));
                }
            }
        }
    }

    /**
     * Getting result of discount validation
     *
     * @param float $paymentSum
     *
     * @return bool
     */
    private function validateDiscount($paymentSum)
    {
        if ($this->discount < 0) {
            $this->addError('discount',
                Yii::t('yii', '{attribute} must be greater than or equal to "{compareValueOrAttribute}".', [
                    'attribute'               => $this->attributeLabels()['discount'],
                    'compareValueOrAttribute' => 0,
                ]));

            return false;
        }

        if ($this->discount > $paymentSum) {
            $this->addError('discount',
                Yii::t('yii', '{attribute} must be less than or equal to "{compareValueOrAttribute}".', [
                    'attribute'               => $this->attributeLabels()['discount'],
                    'compareValueOrAttribute' => $paymentSum,
                ]));

            return false;
        }

        return true;
    }

    public function beforeSave($insert)
    {
        $paymentSum        = 0;
        $paymentServiceSum = 0;


        $paymentSum += +Tariff::find()
                ->andWhere(['in', 'id', empty($this->tariffs) ? [] : $this->tariffs])->sum('price') * $this->period;

        $paymentSum += +TariffAdditionalOption::find()
                ->andWhere(['in', 'id', empty($this->options) ? [] : $this->options])->sum('price') * $this->period;

        $paymentServiceSum += +OneTimeService::find()
            ->andWhere(['in', 'id', empty($this->services) ? [] : $this->services])->sum('price');


        $this->discount = empty($this->discount) ? 0 : $this->discount;
        if (!$this->fixed_discount) {
            /* @var DiscountService */
            $discountService = \Yii::createObject(DiscountService::className());
            $discount        = $discountService->getDiscountByPeriod($this->period);
            $this->discount  = round($paymentSum * ($discount) / 100, 2);
        }

        $this->payment_sum = $paymentSum + $paymentServiceSum;

        if (!$this->validateDiscount($paymentSum)) {
            return false;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        $this->saveTariffs();
        $this->saveOptions();
        $this->saveServices();

        //        $this->save(false, ['payment_sum']);

        parent::afterSave($insert, $changedAttributes);
    }


    /**
     * Getting tariff period
     * @return integer
     */
    public function getTariffPeriod()
    {
        if (isset($this->paymentForTariffs[0])) {
            return $this->paymentForTariffs[0]->period;
        } elseif (isset($this->paymentForAdditionalOptions[0])) {
            return $this->paymentForAdditionalOptions[0]->period;
        };
    }

    /**
     * Getting tariff period
     * @return integer
     */
    public function getTariffStartedAt()
    {
        if (isset($this->paymentForTariffs[0])) {
            return $this->paymentForTariffs[0]->started_at;
        } elseif (isset($this->paymentForAdditionalOptions[0])) {
            return $this->paymentForAdditionalOptions[0]->started_at;
        };
    }

}