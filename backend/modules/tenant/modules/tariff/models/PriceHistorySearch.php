<?php

namespace backend\modules\tenant\modules\tariff\models;

use Yii;
use yii\data\ActiveDataProvider;
use common\modules\tenant\modules\tariff\models\PriceHistory;

class PriceHistorySearch extends PriceHistory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            [['entity_type_id', 'option_id', 'service_id', 'tariff_id'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PriceHistory::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params, '');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['entity_type_id' => $this->entity_type_id]);
        $query->andFilterWhere(['option_id' => $this->option_id]);
        $query->andFilterWhere(['service_id' => $this->service_id]);
        $query->andFilterWhere(['tariff_id' => $this->tariff_id]);

        return $dataProvider;
    }
}
