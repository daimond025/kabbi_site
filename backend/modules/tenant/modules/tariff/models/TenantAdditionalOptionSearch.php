<?php

namespace backend\modules\tenant\modules\tariff\models;

use common\modules\tenant\modules\tariff\models\TenantAdditionalOption;
use common\modules\tenant\modules\tariff\models\TariffAdditionalOption;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class TenantAdditionalOptionSearch extends TenantAdditionalOption
{
    public $domain;
    public $tariff;
    public $tenant_domain;
    public $name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_domain', 'option_id'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->innerJoinWith(['tenant t', 'additionalOption'])
            ->andFilterWhere(['like', 't.domain', $this->tenant_domain])
            ->andFilterWhere(['like', 'option_id', $this->option_id]);

        return $dataProvider;
    }

    public function getOptions(){
        return ArrayHelper::map(TariffAdditionalOption::find()->all(), 'id', 'name');
    }

}
