<?php

namespace backend\modules\tenant\modules\tariff\models;

use app\modules\setting\models\Currency;
use app\modules\setting\models\Tenant;
use common\modules\tenant\modules\tariff\models\Payment;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class PaymentSearch extends Payment
{
    public $domain;
    public $summary;

    public function rules()
    {
        return [
            [['currency_id','summary','discount','domain', 'payment_number', 'description', 'purchased_at', 'payment_sum'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->domain)) {
            $query->andWhere([
                'in',
                'tenant_id',
                Tenant::find()
                    ->select('tenant_id')
                    ->andWhere(['like', 'domain', $this->domain]),
            ]);
        }

        if (!empty($params['start']) && !empty($params['end'])) {
            $query->andWhere([
                'between',
                'purchased_at',
                strtotime($params['start']),
                strtotime($params['end'] . ' +1day - 1sec'),
            ]);
        }

        $query->joinWith(['currency c'])->andFilterWhere([
            'payment_number' => $this->payment_number,
            'description' => $this->description,
            'payment_sum' => $this->payment_sum,
            'discount' => $this->discount,
            'c.currency_id' => $this->currency_id,
        ]);

        $query->andFilterWhere(['(payment_sum) - (discount)'=>$this->summary]);

        return $dataProvider;
    }

    public function getCurrencies(){

        foreach(Currency::find()->all() as $val){
            $data[$val->currency_id] = $val->name.' '.'('.$val->code.')';
        }

        return $data;
    }
}
