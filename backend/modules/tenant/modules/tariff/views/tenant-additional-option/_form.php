<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use common\modules\tenant\modules\tariff\models\TariffAdditionalOption;
use common\modules\tenant\modules\tariff\models\TenantAdditionalOption;

$tenantClass = TenantAdditionalOption::getTenantClass();

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\TenantAdditionalOption */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tenant-additional-option-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'tenant_id')->dropDownList(
                    ArrayHelper::map($tenantClass::find()->orderBy('domain')->all(), 'tenant_id', 'domain'),
                    ['prompt' => t('tenant', 'Choose tenant...')]
                )->label(t('tenant', 'Tenant')) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'option_id')->dropDownList(
                    ArrayHelper::map(TariffAdditionalOption::find()->orderBy('name')->all(), 'id', 'name'),
                    ['prompt' => t('tariff-additional-option', 'Choose option...')]
                )->label(t('tenant-additional-option', 'Additional option')) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'startedAt')->widget(DatePicker::className(), [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format'    => 'dd.mm.yyyy',
                    ],
                ]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'expiryDate')->widget(DatePicker::className(), [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format'    => 'dd.mm.yyyy',
                    ],
                ]) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? t('app', 'Create') : t('app', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
