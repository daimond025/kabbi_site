<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = t('tenant-additional-option', 'Tenant Additional Options');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenant-additional-option-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('tenant')): ?>
        <p>
            <?= Html::a(t('tenant-additional-option', 'Create Tenant Additional Option'), ['create'],
                ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>
    <? \yii\widgets\Pjax::begin() ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'tenant_domain',
                'label' => t('tenant', 'Tenant'),
                'value' => 'tenant.domain',
            ],
            [
                'attribute' => 'option_id',
                'label' => t('tenant-additional-option', 'Additional option'),
                'value' => 'additionalOption.name',
                'filter' => $searchModel->getOptions(),
            ],
            [
                'attribute' => 'started_at',
                'format' => 'date',

            ],
            [
                'attribute' => 'expiry_date',
                'format' => 'date',

            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('tenant'),
                    'delete' => \Yii::$app->user->can('tenant'),
                ],
            ],
        ],
    ]); ?>
    <? \yii\widgets\Pjax::end() ?>

</div>
