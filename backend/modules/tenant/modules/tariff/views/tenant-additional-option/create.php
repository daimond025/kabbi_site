<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\TenantAdditionalOption */

$this->title = t('tenant-additional-option', 'Create Tenant Additional Option');
$this->params['breadcrumbs'][] = [
    'label' => t('tenant-addition-option', t('tenant-additional-option', 'Tenant Additional Options')),
    'url'   => ['index'],
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenant-additional-option-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
