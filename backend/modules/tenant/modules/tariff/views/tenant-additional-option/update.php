<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\TenantAdditionalOption */

$this->title = t('tenant-additional-option', 'Update Tenant Additional Option: ') . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => t('tenant-additional-option', 'Tenant Additional Options'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = t('tenant-additional-option', 'Update');
?>
<div class="tenant-additional-option-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
