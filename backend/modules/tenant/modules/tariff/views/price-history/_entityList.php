<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\modules\tenant\modules\tariff\models\TariffEntityType;

/* @var $this \yii\web\View */
/* @var $model \common\modules\tenant\modules\tariff\models\PriceHistory */

$class      = TariffEntityType::getEntityClass($entityTypeId);
$entityList = [];
if (isset($class)) {
    $entityList = ArrayHelper::map($class::find()->all(), 'id', 'name');
}

$options[] = Html::tag('option', 'Choose entity...');
foreach ($entityList as $key => $value) {
    $options[] = Html::tag('option', $value, [
        'value' => $key,
    ]);
}

echo implode("\n", $options);