<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\modules\tenant\modules\tariff\models\TariffEntityType;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = t('price-history', 'Price Histories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-history-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <? // Html::a('Create Price History', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'tariffEntityType.name',
                'label'     => t('price-history', 'Entity Type'),
                value       => function ($model) {
                    return t('tariff-entity-type', $model->tariffEntityType->name);
                },
            ],
            [
                'label' => t('price-history', 'Entity'),
                'value' => function ($model) {
                    switch ($model->entity_type_id) {
                        case TariffEntityType::CODE_ADDITIONAL_OPTION:
                            return $model->additionalOption->name;
                        case TariffEntityType::CODE_ONE_TIME_SERVICE:
                            return $model->oneTimeService->name;
                        case TariffEntityType::CODE_TARIFF:
                            return $model->tariff->name . ' (' . $model->tariff->module->name . ')';
                    }
                },
            ],
            [
                'label' => t('price-history', 'Price'),
                'value' => function ($model) {
                    return $model->price . ' ' . $model->currency->symbol;
                },
            ],
            'activated_at:datetime',
            'deactivated_at:datetime',

            //            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
