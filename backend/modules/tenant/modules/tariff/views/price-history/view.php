<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\user\models\User;

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\PriceHistory */

$this->title                   = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Price Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-history-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => t('yii', 'Are you sure you want to delete this item?'),
                'method'  => 'post',
            ],
        ]) ?>
    </p>

    <? \yii\widgets\Pjax::begin() ?>
    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'tariffEntityType.name',
                'label'     => 'Entity type',
            ],
            [
                'attribute' => 'entity.name',
                'label'     => 'Entity',
            ],
            'price',
            [
                'label' => 'Currency',
                'value' => $model->currency->name . ' (' . $model->currency->code . ')',
            ],
            'activated_at:date',
            'deactivated_at:date',
            'created_at:datetime',
            'updated_at:datetime',
            [
                'attribute' => 'created_by',
                'format'    => 'raw',
                'value'     => Html::a(
                    User::find($model->created_by)->select('name')->scalar(),
                    ['/user/user/view', 'id' => $model->created_by]),
            ],
            [
                'attribute' => 'updated_by',
                'format'    => 'raw',
                'value'     => Html::a(
                    User::find($model->updated_by)->select('name')->scalar(),
                    ['/user/user/view', 'id' => $model->updated_by]),
            ],
        ],
    ]) ?>
    <? \yii\widgets\Pjax::end() ?>

</div>
