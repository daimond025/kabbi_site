<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use common\modules\tenant\modules\tariff\models\TariffEntityType;
use common\modules\tenant\modules\tariff\models\PriceHistory;
use backend\modules\tenant\modules\tariff\assets\PriceHistoryAsset;

PriceHistoryAsset::register($this);

$currencyClass = PriceHistory::getCurrencyClass();

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\PriceHistory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="price-history-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'entity_type_id')->dropDownList(
                    ArrayHelper::map(TariffEntityType::find()->all(), 'id', 'name'),
                    ['prompt' => 'Choose entity type...']
                ) ?>
            </div>
            <div class="col-sm-6">
                <?
                $class      = TariffEntityType::getEntityClass($model->entity_type_id);
                $entityList = [];
                if (isset($class)) {
                    $entityList = ArrayHelper::map($class::find()->all(), 'id', 'name');
                }
                ?>

                <?= $form->field($model, 'entity_id')->dropDownList($entityList, [
                    'prompt' => 'Choose entity...',
                ]) ?>
            </div>
        </div>

    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'currency_id')->dropDownList(
                    ArrayHelper::map($currencyClass::find()->all(), 'currency_id', function ($value) {
                        return t('currency', $value->name) . ' (' . $value->code . ')';
                    })
                ) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'activatedAt')->widget(DatePicker::className(), [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format'    => 'dd.mm.yyyy',
                    ],
                ]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'deactivatedAt')->widget(DatePicker::className(), [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format'    => 'dd.mm.yyyy',
                    ],
                ]) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? t('app', 'Create') : t('app', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
