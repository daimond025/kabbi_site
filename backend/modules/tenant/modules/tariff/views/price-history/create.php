<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\PriceHistory */

$this->title = 'Create Price History';
$this->params['breadcrumbs'][] = ['label' => 'Price Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-history-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
