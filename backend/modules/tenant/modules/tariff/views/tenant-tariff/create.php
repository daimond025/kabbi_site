<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\TenantTariff */

$this->title                   = t('tenant-tariff', 'Create Tenant Tariff');
$this->params['breadcrumbs'][] = ['label' => t('tenant-tariff', 'Tenant Tariffs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenant-tariff-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
