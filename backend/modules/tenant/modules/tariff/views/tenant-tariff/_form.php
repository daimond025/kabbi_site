<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use common\modules\tenant\modules\tariff\models\TenantTariff;
use common\modules\tenant\modules\tariff\models\Tariff;

$tenantClass = TenantTariff::getTenantClass();

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\TenantTariff */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tenant-tariff-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'tenant_id')->dropDownList(
                    ArrayHelper::map($tenantClass::find()->orderBy('domain')->all(), 'tenant_id', 'domain'),
                    ['prompt' => t('tenant', 'Choose tenant...')]
                )->label(t('tenant-tariff', 'Tenant')) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'tariff_id')->dropDownList(
                    ArrayHelper::map(
                        Tariff::find()
                            ->byDefault(false)
                            ->orderBy('name')
                            ->all(),
                        'id', function ($value) {
                            return $value->name . ' (' . $value->module->name . ')';
                        }),
                    ['prompt' => t('tariff', 'Choose tariff...')]
                )->label(t('tenant-tariff', 'Tariff')) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'startedAt')->widget(DatePicker::className(), [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format'    => 'dd.mm.yyyy',
                    ],
                ]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'expiryDate')->widget(DatePicker::className(), [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format'    => 'dd.mm.yyyy',
                    ],
                ]) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? t('app', 'Create') : t('app', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
