<?php

/* @var $this \yii\web\View */
/* @var $tariff \common\modules\tenant\modules\tariff\models\TenantTariff */

if (isset($tenantTariff)) {
    $this->params['breadcrumbs'][] = [
        'label' => t('tenant-tariff', 'Tenant Tariffs'),
        'url'   => ['/tenant/tariff/tenant-tariff/index'],
    ];
    $this->params['breadcrumbs'][] = [
        'label' => $tenantTariff->tariff->name . ' (' . $tenantTariff->tariff->module->name . ')',
        'url'   => ['/tenant/tariff/tenant-tariff/view', 'id' => $tenantTariff->id],
    ];
}