<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\TenantTariff */

$this->title = t('tenant-tariff', 'Update Tenant Tariff: ') . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => t('tenant-tariff', 'Tenant Tariffs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = t('app', 'Update');
?>
<div class="tenant-tariff-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
