<?php

use app\modules\setting\models\Tenant;
use backend\modules\tenant\modules\tariff\models\TenantTariffSearch;
use common\modules\tenant\modules\tariff\models\Tariff;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel TenantTariffSearch */

$this->title                   = t('tenant-tariff', 'Tenant Tariffs');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="tenant-tariff-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('tenant')): ?>
    <p>
        <?= Html::a(t('tenant-tariff', 'Create Tenant Tariff'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php endif; ?>
    <? \yii\widgets\Pjax::begin() ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'tenant.domain',
                'label'     => t('tenant-tariff', 'Tenant'),
                'filter'    => Html::activeTextInput($searchModel, 'domain',
                    ['class' => 'form-control']),
            ],
            [
                'label'  => t('tenant-tariff', 'Tariff'),
                'value'  => function ($model) {
                    return $model->tariff->name . ' (' . $model->tariff->module->name . ')';
                },
                'filter' => Html::activeTextInput($searchModel, 'tariff',
                    ['class' => 'form-control']),
            ],
            'started_at:date',
            'expiry_date:date',

            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('tenant'),
                    'delete' => \Yii::$app->user->can('tenant'),
                ],
            ],
        ],
    ]); ?>
    <? \yii\widgets\Pjax::end() ?>

</div>
