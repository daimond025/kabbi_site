<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\tenant\modules\tariff\models\TenantTariff;
use common\modules\tenant\modules\tariff\models\TariffPermission;

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\TenantPermission */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tenant-permission-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'tenant_tariff_id')->dropDownList(
                    ArrayHelper::map(
                        TenantTariff::find()
                            ->joinWith(['tenant', 'tariff t', 'tariff.module'],
                                true)->orderBy('t.name')->all(),
                        'id',
                        function ($model) {
                            $tariff = $model->tariff;

                            return $model->tenant->domain
                            . ' - ' . $tariff->name
                            . ' (' . $tariff->module->name . ')';
                        }
                    ),
                    [
                        'prompt'   => t('tariff', 'Choose tariff...'),
                        'disabled' => isset($tenantTariff),
                    ]
                )->label(t('tenant-tariff', 'Tariff')) ?>
            </div>

            <div class="col-sm-4">
                <?= $form->field($model, 'permission_id')->dropDownList(
                    ArrayHelper::map(
                        TariffPermission::find()
                            ->joinWith('permission p', true)
                            ->andFilterWhere(['tariff_id' => $tenantTariff->tariff_id])
                            ->orderBy('name')
                            ->all(),
                        'id', 'permission.name'
                    ),
                    ['prompt' => t('permission', 'Choose permission...')]
                )->label(t('permission', 'Permission')) ?>
            </div>

            <div class="col-sm-4">
                <?= $form->field($model, 'value')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? t('app', 'Create') : t('app', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
