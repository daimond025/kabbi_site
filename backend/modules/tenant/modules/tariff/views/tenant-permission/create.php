<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\TenantPermission */

$this->render('/tenant-tariff/_tenantTariffBreadcrumbs', [
    'tenantTariff' => $tenantTariff,
]);

$this->title                   = t('tenant-permission', 'Create Tenant Permission');
$this->params['breadcrumbs'][] = ['label' => t('tenant-permission', 'Tenant Permissions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenant-permission-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', compact('model', 'tenantTariff')) ?>

</div>
