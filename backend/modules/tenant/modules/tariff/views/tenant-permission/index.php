<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->render('/tenant-tariff/_tenantTariffBreadcrumbs', [
    'tenantTariff' => $tenantTariff,
]);

$this->title                   = t('tenant-permission', 'Tenant Permissions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenant-permission-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->user->can('tenant')): ?>
        <p>
            <?
            $url = isset($tenantTariff) ? ['create', 'tenant_tariff_id' => $tenantTariff->id] : ['create'];
            echo Html::a(t('tenant-permission', 'Create Tenant Permission'), $url, ['class' => 'btn btn-success'])
            ?>
        </p>
    <?php endif; ?>
    <? \yii\widgets\Pjax::begin() ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'tenantTariff.tenant.domain',
                'label'     => t('tenant', 'Tenant'),
            ],
            [
                'label' => t('tariff', 'Tariff'),
                'value' => function ($model) {
                    $tariff = $model->tenantTariff->tariff;

                    return $tariff->name . ' (' . $tariff->module->name . ')';
                },
            ],
            [
                'attribute' => 'tariffPermission.permission.name',
                'label'     => t('permission', 'Permission'),
            ],
            'value',

            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('tenant'),
                    'delete' => \Yii::$app->user->can('tenant'),
                ],
            ],
        ],
    ]); ?>
    <? \yii\widgets\Pjax::end() ?>

</div>
