<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\user\models\User;

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\TenantPermission */

$this->render('/tenant-tariff/_tenantTariffBreadcrumbs', [
    'tenantTariff' => $tenantTariff,
]);

$this->title                   = $model->id;
$url                           = isset($tenantTariff) ? Url::to([
    'index',
    'tenant_tariff_id' => $tenant_tariff->id,
]) : Url::to(['index']);
$this->params['breadcrumbs'][] = ['label' => t('tenant-permission', 'Tenant Permissions'), 'url' => $url];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="tenant-permission-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('tenant')): ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method'  => 'post',
            ],
        ]) ?>
    </p>
    <?php endif; ?>
    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'tenantTariff.tenant.domain',
                'label'     => 'Tenant',
            ],
            [
                'label' => 'Tariff',
                'value' => $model->tenantTariff->tariff->name
                    . ' (' . $model->tenantTariff->tariff->module->name . ')',
            ],
            [
                'attribute' => 'tariffPermission.permission.name',
                'label'     => 'Permission',
            ],
            'value',
            'created_at:datetime',
            'updated_at:datetime',
            [
                'attribute' => 'created_by',
                'format'    => 'raw',
                'value'     => Html::a(
                    User::find($model->created_by)->select('name')->scalar(),
                    ['/user/user/view', 'id' => $model->created_by]),
            ],
            [
                'attribute' => 'updated_by',
                'format'    => 'raw',
                'value'     => Html::a(
                    User::find($model->updated_by)->select('name')->scalar(),
                    ['/user/user/view', 'id' => $model->updated_by]),
            ],
        ],
    ]) ?>

</div>
