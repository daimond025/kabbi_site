<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\TenantPermission */

$this->render('/tenant-tariff/_tenantTariffBreadcrumbs', [
    'tenantTariff' => $tenantTariff,
]);

$this->title = t('tenant-permission', 'Update Tenant Permission: ') . ' ' . $model->id;

$url = ['index'];
if (isset($tenantTariff)) {
    $url['tenant_tariff_id'] = $tenantTariff->id;
}
$this->params['breadcrumbs'][] = ['label' => t('tenant-permission', 'Tenant Permissions'), 'url' => $url];

$url = ['view', 'id' => $model->id];
if (isset($tenantTariff)) {
    $url['tenant_tariff_id'] = $tenantTariff->id;
}
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => $url];
$this->params['breadcrumbs'][] = t('app', 'Update');
?>
<div class="tenant-permission-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', compact('model', 'tenantTariff')) ?>

</div>
