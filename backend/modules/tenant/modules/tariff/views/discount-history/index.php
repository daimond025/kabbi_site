<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = t('discount-history', 'Discount Histories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discount-history-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'period',
            'percent',
            'activated_at:datetime',
            'deactivated_at:datetime',
        ],
    ]); ?>

</div>
