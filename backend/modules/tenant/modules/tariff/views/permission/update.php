<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\Permission */

$this->title = t('permission', 'Update Permission: ') . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => t('permission', 'Permissions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = t('app', 'Update');
?>
<div class="permission-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
