<?php

use yii\helpers\ArrayHelper;
use kartik\widgets\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\tenant\modules\tariff\models\Payment;
use common\modules\tenant\modules\tariff\models\OneTimeService;
use common\modules\tenant\modules\tariff\models\PaymentForOneTimeService;

$currencyClass = PaymentForOneTimeService::getCurrencyClass();

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\PaymentForOneTimeService */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-for-one-time-service-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'payment_id')->dropDownList(
                    ArrayHelper::map(
                        Payment::find()->joinWith('tenant t', true)->orderBy('t.domain')->all(),
                        'id',
                        function ($model) {
                            return '#' . $model->payment_number
                            . ' ' . date('d.m.Y', $model->purchased_at)
                            . ' (' . $model->tenant->domain . ')';
                        }),
                    ['prompt' => 'Choose payment...']
                ) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'service_id')->dropDownList(
                    ArrayHelper::map(OneTimeService::find()->orderBy('name')->all(), 'id', 'name'),
                    ['prompt' => 'Choose one-time service...']
                ) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'payment_sum')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'currency_id')->dropDownList(
                    ArrayHelper::map($currencyClass::find()->all(), 'currency_id', function ($value) {
                        return $value->name . ' (' . $value->code . ')';
                    })
                ) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'executedAt')->widget(DatePicker::className(), [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format'    => 'dd.mm.yyyy',
                    ],
                ]) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
