<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\user\models\User;

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\PaymentForOneTimeService */

$this->title                   = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Payment For One Time Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-for-one-time-service-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method'  => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            [
                'label' => 'Payment',
                'value' => '#' . $model->payment->payment_number
                    . ' ' . date('d.m.Y', $model->payment->purchased_at)
                    . ' (' . $model->payment->tenant->domain . ')',
            ],
            [
                'attribute' => 'service.name',
                'label'     => 'One-time Service',
            ],
            'price',
            'payment_sum',
            [
                'label' => 'Currency',
                'value' => $model->currency->name . ' (' . $model->currency->code . ')',
            ],
            'executed_at:date',
            'created_at:datetime',
            'updated_at:datetime',
            [
                'attribute' => 'created_by',
                'format'    => 'raw',
                'value'     => Html::a(
                    User::find($model->created_by)->select('name')->scalar(),
                    ['/user/user/view', 'id' => $model->created_by]),
            ],
            [
                'attribute' => 'updated_by',
                'format'    => 'raw',
                'value'     => Html::a(
                    User::find($model->updated_by)->select('name')->scalar(),
                    ['/user/user/view', 'id' => $model->updated_by]),
            ],
        ],
    ]) ?>

</div>
