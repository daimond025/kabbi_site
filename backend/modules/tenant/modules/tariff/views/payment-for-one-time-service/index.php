<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Payment For One Time Services';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-for-one-time-service-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Payment For One Time Service', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'payment.tenant.domain',
                'label'     => 'Tenant',
            ],
            [
                'label' => 'Payment',
                'value' => function ($model) {
                    return '#' . $model->payment->payment_number
                    . ' ' . date('d.m.Y', $model->payment->purchased_at);
                },
            ],
            [
                'attribute' => 'service.name',
                'label'     => 'One-time Service',
            ],
            'price',
            'payment_sum',
            [
                'label' => 'Currency',
                'value' => function ($model) {
                    return $model->currency->name . ' (' . $model->currency->code . ')';
                },
            ],
            'executed_at:date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
