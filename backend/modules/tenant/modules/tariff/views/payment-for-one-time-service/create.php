<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\PaymentForOneTimeService */

$this->title = 'Create Payment For One Time Service';
$this->params['breadcrumbs'][] = ['label' => 'Payment For One Time Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-for-one-time-service-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
