<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\TariffPermissionHistory */

$this->title = t('tariff-permission-history', 'Create Tariff Permission History');
$this->params['breadcrumbs'][] = [
    'label' => t('tariff-permission-history', 'Tariff Permission Histories'),
    'url'   => ['index'],
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tariff-permission-history-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
