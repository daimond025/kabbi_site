<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use common\modules\tenant\modules\tariff\models\TariffPermission;

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\TariffPermissionHistory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tariff-permission-history-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-5">
                <?= $form->field($model, 'permission_id')->dropDownList(
                    ArrayHelper::map(TariffPermission::find()
                        ->joinWith('permission', true)
                        ->joinWith('tariff', true)
                        ->all(),
                        'id', function ($value) {
                            return $value->permission->name
                            . ' (' . $value->tariff->name . ')';
                        })
                )->label(t('tariff-permission-history', 'Permission')) ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'value')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'activatedAt')->widget(DatePicker::className(), [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format'    => 'dd.mm.yyyy',
                    ],
                ]) ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'deactivatedAt')->widget(DatePicker::className(), [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format'    => 'dd.mm.yyyy',
                    ],
                ]) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? t('app', 'Create') : t('app', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
