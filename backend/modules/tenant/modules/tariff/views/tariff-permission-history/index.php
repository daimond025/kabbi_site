<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

if (isset($permission)) {
    $this->render('/tariff-permission/_tariffPermissionBreadcrumbs', [
        'permission' => $permission,
    ]);
}

$this->title                   = t('tariff-permission-history', 'Tariff Permission Histories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tariff-permission-history-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <? // Html::a(t('tariff-permission-history', 'Create Tariff Permission History'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <? \yii\widgets\Pjax::begin() ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label'  => t('tariff-permission-history', 'Permission'),
                'format' => 'raw',
                'value'  => function ($model, $key, $index, $column) {
                    return Html::a(Html::encode($model->permission->name), [
                        '/tenant/tariff/tariff-permission/view',
                        'id' => $model->permission_id,
                    ]);
                },
            ],
            'value',
            'activated_at:datetime',
            'deactivated_at:datetime',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <? \yii\widgets\Pjax::end() ?>

</div>
