<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\TariffPermissionHistory */

$this->title = t('tariff-permission-history', 'Update Tariff Permission History: ') . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tariff Permission Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = t('app', 'Update');
?>
<div class="tariff-permission-history-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
