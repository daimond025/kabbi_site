<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\user\models\User;

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\Module */

$this->title                   = $model->name;
$this->params['breadcrumbs'][] = ['label' => t('module', 'Modules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="module-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('tenant')): ?>
    <p>
        <?= Html::a(t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => t('yii', 'Are you sure you want to delete this item?'),
                'method'  => 'post',
            ],
        ]) ?>
    </p>
    <?php endif; ?>
    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            'code',
            'name',
            'description:ntext',
            'active',
            'created_at:datetime',
            'updated_at:datetime',
            [
                'attribute' => 'created_by',
                'format'    => 'raw',
                'value'     => Html::a(
                    User::find($model->created_by)->select('name')->scalar(),
                    ['/user/user/view', 'id' => $model->created_by]),
            ],
            [
                'attribute' => 'updated_by',
                'format'    => 'raw',
                'value'     => Html::a(
                    User::find($model->updated_by)->select('name')->scalar(),
                    ['/user/user/view', 'id' => $model->updated_by]),
            ],
        ],
    ]) ?>

</div>
