<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\Module */

$this->title = t('module', 'Update Module: ') . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => t('module', 'Modules'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = t('module', 'Update');
?>
<div class="module-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
