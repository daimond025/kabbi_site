<?php

/* @var $this \yii\web\View */
/* @var $tariff \common\modules\tenant\modules\tariff\models\TariffPermission */

if (isset($permission)) {
    $this->render('/tariff/_tariffBreadcrumbs', [
        'tariff' => $permission->tariff,
    ]);

    $this->params['breadcrumbs'][] = [
        'label' => t('tariff-permission', 'Tariff Permissions'),
        'url'   => [
            '/tenant/tariff/tariff-permission/index',
            'tariff_id' => $permission->tariff->id,
        ],
    ];
    $this->params['breadcrumbs'][] = [
        'label' => $permission->permission->name,
        'url'   => [
            '/tenant/tariff/tariff-permission/view',
            'id'        => $permission->id,
            'tariff_id' => $permission->tariff->id,
        ],
    ];
}