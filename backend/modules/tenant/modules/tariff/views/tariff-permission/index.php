<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->render('/tariff/_tariffBreadcrumbs', [
    'tariff' => $tariff,
]);

$this->title = t('tariff-permission', 'Tariff Permissions');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="tariff-permission-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('tenant')): ?>
        <p>
            <?
            $url = isset($tariff)
                ? Url::to(['create', 'tariff_id' => $tariff->id]) : Url::to(['create']);
            echo Html::a(t('tariff-permission', 'Create Tariff Permission'), $url, ['class' => 'btn btn-success'])
            ?>
        </p>
    <?php endif; ?>
    <? \yii\widgets\Pjax::begin() ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label'  => t('tariff-permission', 'Tariff'),
                'format' => 'raw',
                'value'  => function ($model, $key, $index, $column) {
                    return Html::a(
                        Html::encode($model->tariff->name . ' (' . $model->tariff->module->name . ')'), [
                        '/tenant/tariff/tariff/view',
                        'id' => $model->tariff_id,
                    ]);
                },
            ],
            [
                'label'  => t('tariff-permission', 'Permission'),
                'format' => 'raw',
                'value'  => function ($model, $key, $index, $column) {
                    return Html::a(Html::encode($model->permission->name), [
                        '/tenant/tariff/permission/view',
                        'id' => $model->permission_id,
                    ]);
                },
            ],
            'value',
            'active',
            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('tenant'),
                    'delete' => \Yii::$app->user->can('tenant'),
                ],
                'buttons'        => [
                    'view'   => function ($url, $model, $key) use ($tariff) {
                        $url = ['view', 'id' => $key];
                        if (isset($tariff)) {
                            $url['tariff_id'] = $tariff->id;
                        }

                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title'      => Yii::t('yii', 'View'),
                            'aria-label' => Yii::t('yii', 'View'),
                            'data-pjax'  => '0',
                        ]);
                    },
                    'update' => function ($url, $model, $key) use ($tariff) {
                        $url = ['update', 'id' => $key];
                        if (isset($tariff)) {
                            $url['tariff_id'] = $tariff->id;
                        }

                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                            'title'      => Yii::t('yii', 'Update'),
                            'aria-label' => Yii::t('yii', 'Update'),
                            'data-pjax'  => '0',
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>
    <? \yii\widgets\Pjax::end() ?>

</div>
