<?php

use yii\helpers\Url;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\TariffPermission */


$this->render('/tariff/_tariffBreadcrumbs', [
    'tariff' => $tariff,
]);

$this->title                   = t('tariff-permission', 'Create Tariff Permission');
$url                           = isset($tariff) ? Url::to(['index', 'tariff_id' => $tariff->id]) : Url::to(['index']);
$this->params['breadcrumbs'][] = ['label' => t('tariff-permission', 'Tariff Permissions'), 'url' => $url];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="tariff-permission-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'  => $model,
        'tariff' => $tariff,
    ]) ?>

</div>
