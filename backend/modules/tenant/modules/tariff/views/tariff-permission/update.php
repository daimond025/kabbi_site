<?php

use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\TariffPermission */

$this->render('/tariff/_tariffBreadcrumbs', [
    'tariff' => $tariff,
]);

$this->title = t('tariff-permission', 'Update Tariff Permission: ') . ' ' . $model->id;

$url = ['index'];
if (isset($tariff)) {
    $url['tariff_id'] = $tariff->id;
}
$this->params['breadcrumbs'][] = ['label' => t('tariff-permission', 'Tariff Permissions'), 'url' => $url];

$url = ['view', 'id' => $model->id];
if (isset($tariff)) {
    $url['tariff_id'] = $tariff->id;
}
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => $url];
$this->params['breadcrumbs'][] = t('app', 'Update');
?>
<div class="tariff-permission-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'  => $model,
        'tariff' => $tariff,
    ]) ?>

</div>
