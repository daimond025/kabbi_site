<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\user\models\User;

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\TariffPermission */

$this->render('/tariff/_tariffBreadcrumbs', [
    'tariff' => $tariff,
]);

$this->title                   = $model->id;
$url                           = isset($tariff) ? Url::to(['index', 'tariff_id' => $tariff->id]) : Url::to(['index']);
$this->params['breadcrumbs'][] = ['label' => t('tariff-permission', 'Tariff Permissions'), 'url' => $url];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tariff-permission-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if (Yii::$app->user->can('tenant')): ?>
            <?= Html::a(t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => t('yii', 'Are you sure you want to delete this item?'),
                    'method'  => 'post',
                ],
            ]) ?>
        <?php endif; ?>
        <?= Html::a(t('tariff-permission', 'History'),
            ['/tenant/tariff/tariff-permission-history', 'permission_id' => $model->id],
            ['class' => 'btn btn-default']) ?>
    </p>

    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            [
                'label'  => t('tariff-permission', 'Tariff'),
                'format' => 'raw',
                'value'  => Html::a(Html::encode($model->tariff->name), [
                    '/tenant/tariff/tariff/view',
                    'id' => $model->tariff_id,
                ]),
            ],
            [
                'label'  => t('tariff-permission', 'Permission'),
                'format' => 'raw',
                'value'  => Html::a(Html::encode($model->permission->name), [
                    '/tenant/tariff/permission/view',
                    'id' => $model->permission_id,
                ]),
            ],
            'value',
            'active',
            'created_at:datetime',
            'updated_at:datetime',
            [
                'attribute' => 'created_by',
                'format'    => 'raw',
                'value'     => Html::a(
                    User::find($model->created_by)->select('name')->scalar(),
                    ['/user/user/view', 'id' => $model->created_by]),
            ],
            [
                'attribute' => 'updated_by',
                'format'    => 'raw',
                'value'     => Html::a(
                    User::find($model->updated_by)->select('name')->scalar(),
                    ['/user/user/view', 'id' => $model->updated_by]),
            ],
        ],
    ]) ?>

</div>
