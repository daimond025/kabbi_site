<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\modules\tenant\modules\tariff\models\Tariff;
use common\modules\tenant\modules\tariff\models\Permission;

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\TariffPermission */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tariff-permission-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-3">
                <?= $form->field($model, 'tariff_id')->dropDownList(
                    ArrayHelper::map(Tariff::find()->all(), 'id', function ($model) {
                        return $model->name . ' (' . $model->module->name . ')';
                    }),
                    ['disabled' => isset($tariff)]
                )->label(t('tariff-permission', 'Tariff')) ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'permission_id')->dropDownList(
                    ArrayHelper::map(Permission::find()->all(), 'id', 'name')
                )->label(t('tariff-permission', 'Permission')) ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'value')->textInput() ?>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'active')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? t('app', 'Create') : t('app', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
