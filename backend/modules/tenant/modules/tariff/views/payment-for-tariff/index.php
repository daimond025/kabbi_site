<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->render('/payment/_paymentBreadcrumbs', [
    'payment' => $payment,
]);

$this->title                   = t('payment-for-tariff', 'Payment For Tariffs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-for-tariff-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('tenant')): ?>
    <p>
        <?
        $url = isset($payment) ? ['create', 'payment_id' => $payment->id] : ['create'];
        echo Html::a(t('payment-for-tariff', 'Create Payment For Tariff'), $url, ['class' => 'btn btn-success'])
        ?>
    </p>
    <?php endif; ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'payment.tenant.domain',
                'label'     => t('tenant', 'Tenant'),
            ],
            [
                'label' => t('payment', 'Payment'),
                'value' => function ($model) {
                    return '#' . $model->payment->payment_number
                    . ' ' . date('d.m.Y', $model->payment->purchased_at);
                },
            ],
            [
                'label' => t('tariff', 'Tariff'),
                'value' => function ($model) {
                    return $model->tariff->name . ' (' . $model->tariff->module->name . ')';
                },
            ],
            'period',
            'price',
            'payment_sum',
            [
                'label' => t('currency', 'Currency'),
                'value' => function ($model) {
                    return t('currency', $model->currency->name) . ' (' . $model->currency->code . ')';
                },
            ],

            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('tenant'),
                    'delete' => \Yii::$app->user->can('tenant'),
                ],
            ],
        ],
    ]); ?>

</div>
