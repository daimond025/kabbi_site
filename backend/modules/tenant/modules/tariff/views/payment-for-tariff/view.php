<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\user\models\User;

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\PaymentForTariff */

$this->render('/payment/_paymentBreadcrumbs', [
    'payment' => $payment,
]);

$this->title                   = $model->id;
$url                           = isset($payment) ? ['index', 'payment_id' => $payment->id] : ['index'];
$this->params['breadcrumbs'][] = ['label' => t('payment-for-tariff', 'Payment For Tariffs'), 'url' => $url];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-for-tariff-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('tenant')): ?>
    <p>
        <?= Html::a(t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => t('yii', 'Are you sure you want to delete this item?'),
                'method'  => 'post',
            ],
        ]) ?>
    </p>
    <?php endif; ?>
    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            [
                'label' => t('payment', 'Payment'),
                'value' => '#' . $model->payment->payment_number
                    . ' ' . date('d.m.Y', $model->payment->purchased_at)
                    . ' (' . $model->payment->tenant->domain . ')',
            ],
            [
                'label' => t('tariff', 'Tariff'),
                'value' => $model->tariff->name . ' (' . $model->tariff->module->name . ')',
            ],
            'period',
            'price',
            'payment_sum',
            [
                'label' => t('currency', 'Currency'),
                'value' => t('currency', $model->currency->name) . ' (' . $model->currency->code . ')',
            ],
            'created_at:datetime',
            'updated_at:datetime',
            [
                'attribute' => 'created_by',
                'format'    => 'raw',
                'value'     => Html::a(
                    User::find($model->created_by)->select('name')->scalar(),
                    ['/user/user/view', 'id' => $model->created_by]),
            ],
            [
                'attribute' => 'updated_by',
                'format'    => 'raw',
                'value'     => Html::a(
                    User::find($model->updated_by)->select('name')->scalar(),
                    ['/user/user/view', 'id' => $model->updated_by]),
            ],
        ],
    ]) ?>

</div>
