<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use common\modules\tenant\modules\tariff\models\Payment;
use common\modules\tenant\modules\tariff\models\Tariff;
use common\modules\tenant\modules\tariff\models\PaymentForTariff;

$currencyClass = PaymentForTariff::getCurrencyClass();

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\PaymentForTariff */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-for-tariff-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-3">
                <?= $form->field($model, 'payment_id')->dropDownList(
                    ArrayHelper::map(
                        Payment::find()->joinWith('tenant t', true)->orderBy('t.domain')->all(),
                        'id',
                        function ($model) {
                            return '#' . $model->payment_number
                            . ' ' . date('d.m.Y', $model->purchased_at)
                            . ' (' . $model->tenant->domain . ')';
                        }),
                    [
                        'prompt' => t('payment', 'Choose payment...'),
                        'disabled' => isset($payment) ? 'disabled' : false,
                    ]
                )->label(t('payment', 'Payment')) ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'tariff_id')->dropDownList(
                    ArrayHelper::map(Tariff::find()->orderBy('name')->all(), 'id', function ($value) {
                        return $value->name . ' (' . $value->module->name . ')';
                    }),
                    ['prompt' => t('tariff', 'Choose tariff...')]
                )->label(t('tariff', 'Tariff')) ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'startedAt')->widget(DatePicker::className(), [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format'    => 'dd.mm.yyyy',
                    ],
                ]) ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'period')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-3">
                <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'payment_sum')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'currency_id')->dropDownList(
                    ArrayHelper::map($currencyClass::find()->all(), 'currency_id', function ($value) {
                        return t('currency', $value->name) . ' (' . $value->code . ')';
                    })
                )->label(t('currency', 'Currency')) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? t('app', 'Create') : t('app', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
