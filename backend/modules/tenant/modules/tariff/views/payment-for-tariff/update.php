<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\PaymentForTariff */

$this->render('/payment/_paymentBreadcrumbs', [
    'payment' => $payment,
]);

$this->title = t('payment-for-tariff', 'Update Payment For Tariff: ') . ' ' . $model->id;

$url = ['index'];
if (isset($payment)) {
    $url['payment_id'] = $payment->id;
}
$this->params['breadcrumbs'][] = ['label' => t('payment-for-tariff', 'Payment For Tariffs'), 'url' => $url];

$url = ['view', 'id' => $model->id];
if (isset($payment)) {
    $url['payment_id'] = $payment->id;
}
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => $url];
$this->params['breadcrumbs'][] = t('app', 'Update');
?>
<div class="payment-for-tariff-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
