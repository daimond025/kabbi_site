<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\PaymentForTariff */

$this->render('/payment/_paymentBreadcrumbs', [
    'payment' => $payment,
]);

$this->title                   = t('payment-for-tariff', 'Create Payment For Tariff');
$url                           = isset($payment) ? ['index', 'payment_id' => $payment->id,] : ['index'];
$this->params['breadcrumbs'][] = ['label' => t('payment-for-tariff', 'Payment For Tariffs'), 'url' => $url];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-for-tariff-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'   => $model,
        'payment' => $payment,
    ]) ?>

</div>
