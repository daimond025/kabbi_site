<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = t('tariff-additional-option', 'Tariff Additional Options');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tariff-additional-option-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('tenant')): ?>
    <p>
        <?= Html::a(t('tariff-additional-option', 'Create Tariff Additional Option'), ['create'],
            ['class' => 'btn btn-success']) ?>
    </p>
    <?php endif; ?>
    <? \yii\widgets\Pjax::begin() ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'code',
            'name',
            [
                'label' => t('tariff-additional-option', 'Price'),
                'value' => function ($model) {
                    return $model->price . ' ' . $model->currency->symbol;
                }
            ],
            'description:ntext',
            'active',

            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('tenant'),
                    'delete' => \Yii::$app->user->can('tenant'),
                ],
            ],
        ],
    ]); ?>
    <? \yii\widgets\Pjax::end() ?>

</div>
