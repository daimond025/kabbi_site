<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\TariffAdditionalOption */

$this->title = t('tariff-additional-option', 'Create Tariff Additional Option');
$this->params['breadcrumbs'][] = [
    'label' => t('tariff-additional-option', 'Tariff Additional Options'),
    'url'   => ['index'],
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tariff-additional-option-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
