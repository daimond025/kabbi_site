<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\TariffAdditionalOption */

$this->title = t('tariff-additional-option',
        'Update Tariff Additional Option: ') . ' ' . $model->name;

$this->params['breadcrumbs'][] = [
    'label' => t('tariff-additional-option', 'Tariff Additional Options'),
    'url'   => ['index'],
];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = t('app', 'Update');
?>
<div class="tariff-additional-option-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
