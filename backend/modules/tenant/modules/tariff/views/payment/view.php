<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\user\models\User;

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\Payment */

$this->title                   = $model->id;
$this->params['breadcrumbs'][] = ['label' => t('payment', 'Payments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if (Yii::$app->user->can('tenant')): ?>
            <?= Html::a(t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => t('yii', 'Are you sure you want to delete this item?'),
                    'method'  => 'post',
                ],
            ]) ?>
        <?php endif; ?>
        <?= Html::a(t('tariff', 'Tariffs'), [
            '/tenant/tariff/payment-for-tariff',
            'payment_id' => $model->id,
        ],
            ['class' => 'btn btn-default']) ?>

        <?= Html::a(t('tariff-additional-option', 'Additional Options'), [
            '/tenant/tariff/payment-for-additional-option',
            'payment_id' => $model->id,
        ],
            ['class' => 'btn btn-default']) ?>
        <?php if (Yii::$app->user->can('tenant')): ?>
        <? if (!isset($model->confirmed_at)): ?>
            <button class="btn btn-success" data-toggle="modal" data-target="#deposit-modal">
                <?= t('payment', 'Confirm payment') ?>
            </button>
        <? endif ?>

        <? if (isset($model->confirmed_at)) {
            echo Html::a(t('payment', 'Cancel payment'), [
                '/tenant/tariff/payment/cancel-payment',
                'payment_id' => $model->id,
            ],
                ['class' => 'btn btn-danger']);
        } ?>
        <?php endif; ?>
    </p>

    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'domain',
                'label'     => t('tenant', 'Tenant'),
            ],
            'payment_number',
            'description:ntext',
            'purchased_at:date',
            'payment_sum:decimal',
            'discount:decimal',
            'summary:decimal',
            [
                'label' => t('currency', 'Currency'),
                'value' => t('currency', $model->currency->name) . ' (' . $model->currency->code . ')',
            ],
            'created_at:datetime',
            'updated_at:datetime',
            [
                'attribute' => 'created_by',
                'format'    => 'raw',
                'value'     => Html::a(
                    User::getUsername($model->created_by),
                    ['/user/user/view', 'id' => $model->created_by]),
            ],
            [
                'attribute' => 'updated_by',
                'format'    => 'raw',
                'value'     => Html::a(
                    User::getUsername($model->updated_by),
                    ['/user/user/view', 'id' => $model->updated_by]),
            ],
        ],
    ]) ?>

    <?= $this->render('_depositModal', compact('model')) ?>

</div>
