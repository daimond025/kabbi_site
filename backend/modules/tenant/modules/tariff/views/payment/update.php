<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\Payment */

$this->title                   = t('payment', 'Update Payment: ') . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => t('payment', 'Payments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = t('payment', 'Update');
?>
<div class="payment-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
