<?php

use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\tenant\modules\tariff\models\Payment;
use backend\modules\tenant\modules\tariff\assets\PaymentAsset;


PaymentAsset::register($this);

$currencyClass = Payment::getCurrencyClass();
$tenantClass   = Payment::getTenantClass();

/* @var $this yii\web\View */
/* @var $model \backend\modules\tenant\modules\tariff\models\PaymentForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-3">
                <?= $form->field($model, 'domain')->widget(\yii\jui\AutoComplete::classname(), [
                        'clientOptions' => [
                            'source' => ArrayHelper::getColumn($tenantClass::find()->orderBy('domain')->all(), 'domain'),
                        ],
                    ])->label(t('tenant', 'Tenant'))->textInput() ?>

            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'payment_number')->textInput() ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($model, 'purchasedAt')->widget(DatePicker::className(), [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format'    => 'dd.mm.yyyy',
                    ],
                ]) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-3">
                <?= $form->field($model, 'startedAt')->widget(DatePicker::className(), [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format'    => 'dd.mm.yyyy',
                    ],
                ]) ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'period',
                    ['enableClientValidation' => false])->textInput(['maxlength' => true]) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-2">
                <?= $form->field($model, 'payment_sum')->textInput([
                    'maxlength' => true,
                    'disabled'  => 'disabled',
                ]) ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'discount', [
                    'enableClientValidation' => false,
                ])->textInput([
                    'maxlength' => true,
                    'disabled'  => empty($model->fixed_discount),
                ]) ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'summary')->textInput([
                    'maxlength' => true,
                    'disabled'  => 'disabled',
                ]) ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'currency_id')->dropDownList(
                    ArrayHelper::map($currencyClass::find()->all(), 'currency_id', function ($value) {
                        return t('currency', $value->name) . ' (' . $value->code . ')';
                    }),
                    ['disabled' => 'disabled']
                )->label(t('currency', 'Currency')) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?= $form->field($model, 'fixed_discount')->checkbox() ?>
            </div>
        </div>
    </div>

    <?= $this->render('_content', compact('form', 'model')) ?>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-12">
                <?= $form->field($model, 'description')->textarea(['rows' => 2]) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? t('app', 'Create') : t('app', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
