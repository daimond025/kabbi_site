<?php

use backend\modules\tenant\modules\tariff\models\PaymentForm;
use yii\helpers\Html;

/* @var $model PaymentForm */

?>

<div class="modal" id="deposit-modal" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?= t('app', 'Confirmation') ?></h4>
            </div>
            <div class="modal-body">
                Пополнить счет арендатора на сумму платежа?
            </div>
            <div class="modal-footer">
                <?= Html::a(t('payment', 'Пополнить счет'), [
                    '/tenant/tariff/payment/confirm-payment',
                    'payment_id' => $model->id,
                    'deposit'    => 1,
                ],
                    ['class' => 'btn btn-success']); ?>
                <?= Html::a(t('payment', 'Не пополнять счет'), [
                    '/tenant/tariff/payment/confirm-payment',
                    'payment_id' => $model->id,
                ],
                    ['class' => 'btn btn-default']); ?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
            </div>
        </div>
    </div>
</div>
