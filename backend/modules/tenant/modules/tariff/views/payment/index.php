<?php
use app\modules\setting\assets\SettingAsset;
use yii\helpers\Html;
use yii\grid\GridView;

$bundle = SettingAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/log-widgets.js',
    ['depends' => 'backend\assets\DateRangePickerAsset']);

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @va $searchModel PaymentSearch */

$this->title                   = t('payment', 'Payments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('tenant')): ?>
    <p>
        <?= Html::a(t('payment', 'Create Payment'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php endif; ?>
    <div class="input-group">
        <button type="button" class="btn btn-default pull-right" id="daterange">
            <i class="fa fa-calendar"></i> <?= t('tenant-notification', 'Choose period') ?>
            <i class="fa fa-caret-down"></i>
        </button>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'tenant.domain',
                'label'     => t('tenant', 'Tenant'),
                'filter'    => Html::activeTextInput($searchModel, 'domain',
                    ['class' => 'form-control']),
            ],
            'payment_number',
            [
                'attribute' => 'purchased_at',
                'value'     => function ($model) {
                    return app()->formatter->asDate($model->purchased_at);
                },
                'filter'    => false,
            ],
            [
                'attribute' => 'payment_sum',
                'format'    => 'decimal',
            ],
            [
                'attribute' => 'discount',
                'format'    => 'decimal',
            ],
            [
                'attribute' => 'summary',
                'value'     => function ($model) {
                    return $model->payment_sum - $model->discount;
                },
                'format'    => 'decimal',
                'label'     => t('payment-for-tariff', 'Summary'),


            ],
            [
                'attribute' => 'currency_id',
                'label' => t('currency', 'Currency'),
                'value' => function ($model) {
                    return t('currency_id', $model->currency->name) . ' (' . $model->currency->code . ')';
                },
                'filter' => $searchModel->getCurrencies(),

            ],
            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('tenant'),
                    'delete' => \Yii::$app->user->can('tenant'),
                ],
            ],
        ],
    ]); ?>

</div>
