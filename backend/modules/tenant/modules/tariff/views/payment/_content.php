<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;


/* @var $form \yii\widgets\ActiveForm */
/* @var $model \backend\modules\tenant\modules\tariff\models\PaymentForm */

?>

<div class="row">
    <div class="col-sm-4">
        <?
        $currentModule = null;
        $availableTariffs = $model->getAvailableTariffs();
        echo $form->field($model, 'tariffs')->checkboxList(
            ArrayHelper::map($availableTariffs, 'id',
                function ($model) {
                    return $model->name . ' (' . $model->price . ' ' . $model->currency->symbol . ')';
                }
            ),
            [
                'item' => function ($index, $label, $name, $checked, $value) use ($availableTariffs, &$currentModule) {
                    if ($currentModule != $availableTariffs[$value]->module->code) {
                        $header = Html::tag('h4', $availableTariffs[$value]->module->name);
                        $currentModule = $availableTariffs[$value]->module->code;
                    }
                    $checkbox = Html::checkbox($name, $checked, [
                        'value' => $value,
                        'label' => Html::encode($label),
                        'data'  => [
                            'price'  => $availableTariffs[$value]->price,
                            'module' => $availableTariffs[$value]->module->code,
                        ],
                    ]);
                    return $header . Html::tag('div', $checkbox);
                },
            ]) ?>
    </div>
    <div class="col-sm-4">
        <?
        $availableOptions = $model->getAvailableOptions();
        echo $form->field($model, 'options')->checkboxList(
            ArrayHelper::map($availableOptions, 'id',
                function ($model) {
                    return $model->name . ' (' . $model->price . ' ' . $model->currency->symbol . ')';
                }),
            [
                'item' => function ($index, $label, $name, $checked, $value) use ($availableOptions) {
                    return Html::checkbox($name, $checked, [
                        'value' => $value,
                        'label' => Html::encode($label),
                        'data'  => [
                            'price' => $availableOptions[$value]->price,
                        ],
                    ]);
                },
            ]) ?>
    </div>
    <div class="col-sm-4">
        <?
        $availableServices = $model->getAvailableServices();
        echo $form->field($model, 'services')->checkboxList(
            ArrayHelper::map($model->getAvailableServices(), 'id',
                function ($model) {
                    return $model->name . ' (' . $model->price . ' ' . $model->currency->symbol . ')';
                }),
            [
                'item' => function ($index, $label, $name, $checked, $value) use ($availableServices) {
                    return Html::checkbox($name, $checked, [
                        'value' => $value,
                        'label' => Html::encode($label),
                        'data'  => [
                            'price' => $availableServices[$value]->price,
                        ],
                    ]);
                },
            ]) ?>
    </div>
</div>