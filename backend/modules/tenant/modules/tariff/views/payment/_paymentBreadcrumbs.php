<?php

/* @var $this \yii\web\View */
/* @var $tariff \common\modules\tenant\modules\tariff\models\Tariff */

if (isset($payment)) {
    $this->params['breadcrumbs'][] = ['label' => t('payment', 'Payments'), 'url' => ['/tenant/tariff/payment/index']];
    $this->params['breadcrumbs'][] = [
        'label' => '#' . $payment->payment_number . ' ' . date('d.m.Y',
                $payment->purchased_at) . ' (' . $payment->tenant->domain . ')',
        'url'   => ['/tenant/tariff/payment/view', 'id' => $payment->id],
    ];
}