<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$module        = app()->getModule('common-tenant/tariff');
$currencyClass = $module->currencyClass;

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\OneTimeService */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="one-time-service-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-2">
                <?= $form->field($model, 'code')->textInput() ?>
            </div>
            <div class="col-sm-5">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-2">
                <?= $form->field($model, 'price')->textInput() ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'currency_id')->dropDownList(
                    ArrayHelper::map($currencyClass::find()->all(), 'currency_id', function ($value) {
                        return t('currency', $value->name) . ' (' . $value->code . ')';
                    }),
                    [
                        'prompt' => t('one-time-service', 'Choose currency...'),
                    ]
                )->label(t('one-time-service', 'Currency')) ?>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'active')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? t('app', 'Create') : t('app', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
