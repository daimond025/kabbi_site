<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\OneTimeService */

$this->title = t('one-time-service', 'Create One Time Service');
$this->params['breadcrumbs'][] = ['label' => t('one-time-service', 'One Time Services'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="one-time-service-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
