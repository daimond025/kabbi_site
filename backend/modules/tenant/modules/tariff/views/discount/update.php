<?php

use common\modules\tenant\modules\tariff\models\DiscountForPeriod;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model DiscountForPeriod */

$this->title                   = t('discount', 'Update discount') . ': ' . $model->period;
$this->params['breadcrumbs'][] = ['label' => t('discount', 'Discounts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->period, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = t('app', 'Update');
?>
<div class="discount-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
