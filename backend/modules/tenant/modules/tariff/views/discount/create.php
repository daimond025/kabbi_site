<?php

use common\modules\tenant\modules\tariff\models\DiscountForPeriod;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model DiscountForPeriod */

$this->title                   = t('discount', 'Create discount');
$this->params['breadcrumbs'][] = ['label' => t('discount', 'Discounts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discount-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
