<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = t('tariff', 'Tariffs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tariff-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('tenant')): ?>
    <p>
        <?= Html::a(t('tariff', 'Create Tariff'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php endif; ?>
    <? \yii\widgets\Pjax::begin() ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label'  => t('tariff', 'Module'),
                'format' => 'raw',
                'value'  => function ($model, $key, $index, $column) {
                    return Html::a(Html::encode($model->module->name), [
                        '/tenant/tariff/module/view',
                        'id' => $model->module_id,
                    ]);
                },
            ],
            'name',
            [
                'label' => t('tariff', 'Price'),
                'value' => function ($model) {
                    return $model->price . ' ' . $model->currency->symbol;
                },
            ],
            'description:ntext',
            'active',
            'default',

            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('tenant'),
                    'delete' => \Yii::$app->user->can('tenant'),
                ],
            ],
        ],
    ]); ?>
    <? \yii\widgets\Pjax::end(); ?>

</div>
