<?php

/* @var $this \yii\web\View */
/* @var $tariff \common\modules\tenant\modules\tariff\models\Tariff */

if (isset($tariff)) {
    $this->params['breadcrumbs'][] = ['label' => t('tariff', 'Tariffs'), 'url' => ['/tenant/tariff/tariff/index']];
    $this->params['breadcrumbs'][] = [
        'label' => $tariff->name,
        'url'   => ['/tenant/tariff/tariff/view', 'id' => $tariff->id],
    ];
}