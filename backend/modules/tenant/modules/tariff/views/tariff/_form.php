<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\tenant\modules\tariff\models\Module;

$module        = app()->getModule('common-tenant/tariff');
$currencyClass = $module->currencyClass;

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\Tariff */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tariff-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($model, 'module_id')->dropDownList(ArrayHelper::map(Module::find()->all(), 'id',
                    'name'))->label(t('tariff', 'Module')); ?>
            </div>
            <div class="col-sm-8">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-2">
                <?= $form->field($model, 'price')->textInput() ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'currency_id')->dropDownList(
                    ArrayHelper::map($currencyClass::find()->all(), 'currency_id', function ($value) {
                        return t('currency', $value->name) . ' (' . $value->code . ')';
                    }),
                    [
                        'prompt' => t('tariff', 'Choose currency...'),
                    ]
                )->label(t('tariff', 'Currency')) ?>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-2">
                <?= $form->field($model, 'active')->checkbox() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'default')->checkbox() ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'demo')->checkbox() ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? t('app', 'Create') : t('app', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
