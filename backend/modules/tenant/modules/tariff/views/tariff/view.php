<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\user\models\User;
use common\modules\tenant\modules\tariff\models\TariffEntityType;

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\Tariff */

$this->title                   = $model->name;
$this->params['breadcrumbs'][] = ['label' => t('tariff', 'Tariffs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tariff-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if (Yii::$app->user->can('tenant')): ?>
            <?= Html::a(t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => t('yii', 'Are you sure you want to delete this item?'),
                    'method'  => 'post',
                ],
            ]) ?>
        <?php endif; ?>
        <?= Html::a(t('tariff', 'Permissions'), [
            '/tenant/tariff/tariff-permission',
            'tariff_id' => $model->id,
        ],
            ['class' => 'btn btn-default']) ?>
        <?= Html::a(t('tariff', 'History'), [
            '/tenant/tariff/price-history',
            'entity_type_id' => TariffEntityType::CODE_TARIFF,
            'tariff_id'      => $model->id,
        ],
            ['class' => 'btn btn-default']) ?>
    </p>

    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            [
                'label'  => t('tariff', 'Module'),
                'format' => 'raw',
                'value'  => Html::a(Html::encode($model->module->name), [
                    '/tenant/tariff/module/view',
                    'id' => $model->module_id,
                ]),
            ],
            'name',
            [
                'attribute' => t('tariff', 'Price'),
                'format'    => 'raw',
                'value'     => $model->price . ' ' . $model->currency->symbol,
            ],
            'description:ntext',
            'active',
            'default',
            'created_at:datetime',
            'updated_at:datetime',
            [
                'attribute' => 'created_by',
                'format'    => 'raw',
                'value'     => Html::a(
                    User::find($model->created_by)->select('name')->scalar(),
                    ['/user/user/view', 'id' => $model->created_by]),
            ],
            [
                'attribute' => 'updated_by',
                'format'    => 'raw',
                'value'     => Html::a(
                    User::find($model->updated_by)->select('name')->scalar(),
                    ['/user/user/view', 'id' => $model->updated_by]),
            ],
        ],
    ]) ?>

</div>
