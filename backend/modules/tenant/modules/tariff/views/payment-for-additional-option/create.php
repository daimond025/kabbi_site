<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\PaymentForAdditionalOption */


$this->render('/payment/_paymentBreadcrumbs', [
    'payment' => $payment,
]);

$this->title                   = t('payment-for-additional-option', 'Create Payment For Additional Option');
$url                           = isset($payment) ? ['index', 'payment_id' => $payment->id,] : ['index'];
$this->params['breadcrumbs'][] = [
    'label' => t('payment-for-additional-option', 'Payment For Additional Options'),
    'url'   => $url,
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-for-additional-option-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
