<?php

use kartik\widgets\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\modules\tenant\modules\tariff\models\Payment;
use common\modules\tenant\modules\tariff\models\TariffAdditionalOption;
use common\modules\tenant\modules\tariff\models\PaymentForAdditionalOption;

$currencyClass = PaymentForAdditionalOption::getCurrencyClass();

/* @var $this yii\web\View */
/* @var $model common\modules\tenant\modules\tariff\models\PaymentForAdditionalOption */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-for-additional-option-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-3">
                <?= $form->field($model, 'payment_id')->dropDownList(
                    ArrayHelper::map(
                        Payment::find()->joinWith('tenant t', true)->orderBy('t.domain')->all(),
                        'id',
                        function ($model) {
                            return '#' . $model->payment_number
                            . ' ' . date('d.m.Y', $model->purchased_at)
                            . ' (' . $model->tenant->domain . ')';
                        }),
                    [
                        'prompt'   => t('payment', 'Choose payment...'),
                        'disabled' => isset($payment) ? 'disabled' : '',
                    ]
                )->label(t('payment', 'Payment')); ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'option_id')->dropDownList(
                    ArrayHelper::map(TariffAdditionalOption::find()->orderBy('name')->all(), 'id', 'name'),
                    ['prompt' => t('tariff-additional-option', 'Choose option...')]
                )->label(t('tenant-additional-option', 'Additional option')); ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'startedAt')->widget(DatePicker::className(), [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format'    => 'dd.mm.yyyy',
                    ],
                ]) ?>
            </div>
            <div class="col-sm-2">
                <?= $form->field($model, 'period')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-sm-3">
                <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'payment_sum')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'currency_id')->dropDownList(
                    ArrayHelper::map($currencyClass::find()->all(), 'currency_id', function ($value) {
                        return t('currency', $value->name) . ' (' . $value->code . ')';
                    })
                )->label(t('currency', 'Currency')) ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? t('app', 'Create') : t('app', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
