<?php

namespace backend\modules\tenant\modules\tariff\controllers;

use common\modules\tenant\modules\tariff\models\TariffPermission;
use Yii;
use backend\modules\tenant\modules\tariff\models\TariffPermissionHistorySearch;
use common\modules\tenant\modules\tariff\models\TariffPermissionHistory;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TariffPermissionHistoryController implements the CRUD actions for TariffPermissionHistory model.
 */
class TariffPermissionHistoryController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['tenantRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['tenant'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TariffPermissionHistory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new TariffPermissionHistorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $permissionId = get('permission_id');
        $permission   = TariffPermission::findone($permissionId);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'permission'   => $permission,
        ]);
    }

    /**
     * Displays a single TariffPermissionHistory model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TariffPermissionHistory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TariffPermissionHistory();
        $model->loadDefaultValues();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->addFlash('error', implode("; ", $model->getFirstErrors()));
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TariffPermissionHistory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                Yii::$app->session->addFlash('error', implode("; ", $model->getFirstErrors()));
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TariffPermissionHistory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TariffPermissionHistory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return TariffPermissionHistory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TariffPermissionHistory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
