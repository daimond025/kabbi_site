<?php

namespace backend\modules\tenant\modules\tariff\controllers;

use common\modules\tenant\modules\tariff\models\Tariff;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\tenant\modules\tariff\models\TariffPermissionSearch;
use common\modules\tenant\modules\tariff\models\TariffPermission;

/**
 * TariffPermissionController implements the CRUD actions for TariffPermission model.
 */
class TariffPermissionController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['tenantRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['tenant'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TariffPermission models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new TariffPermissionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $tariffId = get('tariff_id');
        $tariff   = Tariff::findone($tariffId);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'tariff'       => $tariff,
        ]);
    }

    /**
     * Displays a single TariffPermission model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model'  => $model,
            'tariff' => $model->tariff,
        ]);
    }

    /**
     * Creates a new TariffPermission model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $tariffId = get('tariff_id');
        $tariff   = Tariff::findone($tariffId);

        $model = new TariffPermission();
        $model->loadDefaultValues();
        $model->tariff_id = $tariffId;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                session()->addFlash('error', implode("; ", $model->getFirstErrors()));
            }
        }

        return $this->render('create', [
            'model'  => $model,
            'tariff' => $tariff,
        ]);
    }

    /**
     * Updates an existing TariffPermission model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                session()->addFlash('error', implode("; ", $model->getFirstErrors()));
            }
        }

        return $this->render('update', [
            'model'  => $model,
            'tariff' => $model->tariff,
        ]);
    }

    /**
     * Deletes an existing TariffPermission model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TariffPermission model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return TariffPermission the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TariffPermission::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
