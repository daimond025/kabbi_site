<?php

namespace backend\modules\tenant\modules\tariff\controllers;

use backend\modules\tenant\modules\tariff\models\PaymentForAdditionalOptionSearch;
use common\modules\tenant\modules\tariff\models\Payment;
use Yii;
use common\modules\tenant\modules\tariff\models\PaymentForAdditionalOption;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PaymentForAdditionalOptionController implements the CRUD actions for PaymentForAdditionalOption model.
 */
class PaymentForAdditionalOptionController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['tenantRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['tenant'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PaymentForAdditionalOption models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new PaymentForAdditionalOptionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $paymentId = get('payment_id');
        $payment   = Payment::findone($paymentId);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'payment'      => $payment,
        ]);
    }

    /**
     * Displays a single PaymentForAdditionalOption model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model'   => $model,
            'payment' => $model->payment,
        ]);
    }

    /**
     * Creates a new PaymentForAdditionalOption model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $paymentId = get('payment_id');
        $payment   = Payment::findone($paymentId);

        $model = new PaymentForAdditionalOption();
        $model->loadDefaultValues();
        $model->payment_id = $paymentId;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                session()->addFlash('error', implode("; ", $model->getFirstErrors()));
            }
        }

        return $this->render('create', [
            'model'   => $model,
            'payment' => $payment,
        ]);
    }

    /**
     * Updates an existing PaymentForAdditionalOption model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                session()->addFlash('error', implode("; ", $model->getFirstErrors()));
            }
        }

        return $this->render('update', [
            'model'   => $model,
            'payment' => $model->payment,
        ]);
    }

    /**
     * Deletes an existing PaymentForAdditionalOption model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PaymentForAdditionalOption model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return PaymentForAdditionalOption the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PaymentForAdditionalOption::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
