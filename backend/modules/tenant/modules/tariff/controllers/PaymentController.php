<?php

namespace backend\modules\tenant\modules\tariff\controllers;

use backend\modules\tenant\modules\tariff\models\PaymentForm;
use backend\modules\tenant\modules\tariff\models\PaymentSearch;
use common\modules\tenant\models\Tenant;
use common\modules\tenant\modules\tariff\models\Tariff;
use frontend\modules\tenant\models\Currency;
use Yii;
use common\modules\tenant\modules\tariff\models\Payment;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\Session;

/**
 * PaymentController implements the CRUD actions for Payment model.
 */
class PaymentController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['tenantRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['tenant'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Payment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new PaymentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->sort = [
            'defaultOrder' => [
                'purchased_at' => SORT_DESC,
            ],
        ];

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel'  => $searchModel,
        ]);
    }

    /**
     * Displays a single Payment model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {

        $model = $this->findModel($id);

        $tenant = Tenant::findOne($model->tenant_id);

        $model->domain = $tenant->domain;

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Payment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new PaymentForm();
        $model->loadDefaultValues();
        $model->payment_sum = 0;
        $model->currency_id = Currency::CURRENCY_ID_RUS_RUBLE;

        if ($model->load(Yii::$app->request->post())) {

            $model->tenant_id = Tenant::getIdByDomain($model->domain);

            try {
                if ($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    session()->addFlash('error', implode("; ", $model->getFirstErrors()));
                }
            } catch (\Exception $ex) {
                session()->addFlash('error', $ex->getMessage());
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Payment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model             = $this->findModel($id);
        $model->started_at = $model->tariffStartedAt;
        $model->period     = $model->tariffPeriod;

        if ($model->load(Yii::$app->request->post())) {

            $tenant = Tenant::findOne(['domain'=>$model->domain]);

            $model->tenant_id = $tenant->tenant_id;

            try {
                if ($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    session()->addFlash('error', implode("; ", $model->getFirstErrors()));
                }
            } catch (\Exception $ex) {
                session()->addFlash('error', $ex->getMessage());
            }
        }else{
            $domain = Tenant::getDomainName($model->tenant_id);

            $model->domain = $domain;
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Payment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if (!$model->delete()) {
            session()->addFlash('error', implode("; ", $model->getFirstErrors()));
        };

        return $this->redirect(['index']);
    }

    /**
     * Confirm payment
     *
     * @param $payment_id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionConfirmPayment($payment_id, $deposit = 0)
    {
        $model = Payment::findOne($payment_id);

        if (!$model->confirm($deposit)) {
            session()->addFlash('error', implode('; ', $model->getFirstErrors()));
        };

        return $this->redirect(['view', 'id' => $payment_id]);
    }

    /**
     * Cancel payment
     *
     * @param $payment_id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionCancelPayment($payment_id)
    {
        $model = Payment::findOne($payment_id);

        if (!$model->cancel()) {
            session()->addFlash('error', implode('; ', $model->getFirstErrors()));
        };

        return $this->redirect(['view', 'id' => $payment_id]);
    }

    /**
     * Finds the Payment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Payment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PaymentForm::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetTariffs($date = null)
    {
        app()->response->format = Response::FORMAT_JSON;

        return [
            'hello' => 1,
        ];
    }

}
