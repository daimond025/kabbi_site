<?php

namespace backend\modules\tenant\modules\tariff\controllers;

use backend\modules\tenant\modules\tariff\components\DiscountService;
use common\modules\tenant\modules\tariff\models\DiscountForPeriod;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * DiscountController implements the CRUD actions for DiscountForPeriod model.
 */
class DiscountController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['tenantRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['tenant'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all DiscountForPeriod models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => DiscountForPeriod::find()->orderBy('period'),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DiscountForPeriod model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DiscountForPeriod model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DiscountForPeriod();
        $model->loadDefaultValues();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                session()->addFlash('error', implode("; ", $model->getFirstErrors()));
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing DiscountForPeriod model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                session()->addFlash('error', implode("; ", $model->getFirstErrors()));
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing DiscountForPeriod model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
        } catch (\Exception $ex) {
            session()->addFlash('error', t('app', 'An error occurred while deleting a record'));
            Yii::error($ex->getMessage());
        }

        return $this->redirect(['index']);
    }

    /**
     * Getting discount by period
     *
     * @param integer $period
     *
     * @return integer
     * @throws \yii\base\InvalidConfigException
     */
    public function actionGetDiscount($period)
    {
        if (!app()->request->isAjax) {
            return;
        }

        app()->response->format = Response::FORMAT_JSON;

        /* @var DiscountService */
        $discountService = \Yii::createObject(DiscountService::className());

        return $discountService->getDiscountByPeriod($period);
    }

    /**
     * Finds the DiscountForPeriod model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return DiscountForPeriod the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DiscountForPeriod::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
