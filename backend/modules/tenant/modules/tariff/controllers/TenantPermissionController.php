<?php

namespace backend\modules\tenant\modules\tariff\controllers;

use common\modules\tenant\modules\tariff\models\TenantTariff;
use Yii;
use common\modules\tenant\modules\tariff\models\TenantPermission;
use backend\modules\tenant\modules\tariff\models\TenantPermissionSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TenantPermissionController implements the CRUD actions for TenantPermission model.
 */
class TenantPermissionController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['tenantRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['tenant'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TenantPermission models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new TenantPermissionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $tenantTariffId = get('tenant_tariff_id');
        $tenantTariff   = TenantTariff::findone($tenantTariffId);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'tenantTariff' => $tenantTariff,
        ]);
    }

    /**
     * Displays a single TenantPermission model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model'        => $model,
            'tenantTariff' => $model->tenantTariff,
        ]);
    }

    /**
     * Creates a new TenantPermission model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $tenantTariffId = get('tenant_tariff_id');
        $tenantTariff   = TenantTariff::findone($tenantTariffId);

        $model = new TenantPermission();
        $model->loadDefaultValues();
        $model->tenant_tariff_id = $tenantTariffId;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                session()->addFlash('error', implode("; ", $model->getFirstErrors()));
            }
        }

        return $this->render('create', [
            'model'        => $model,
            'tenantTariff' => $tenantTariff,
        ]);
    }

    /**
     * Updates an existing TenantPermission model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                session()->addFlash('error', implode("; ", $model->getFirstErrors()));
            }
        }

        return $this->render('update', [
            'model'        => $model,
            'tenantTariff' => $model->tenantTariff,
        ]);
    }

    /**
     * Deletes an existing TenantPermission model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TenantPermission model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return TenantPermission the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TenantPermission::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
