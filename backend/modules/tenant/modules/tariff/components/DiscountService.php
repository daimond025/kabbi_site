<?php

namespace backend\modules\tenant\modules\tariff\components;

use common\modules\tenant\modules\tariff\models\DiscountForPeriod;
use yii\base\Object;

/**
 * Class DiscountService
 * @package backend\modules\tenant\modules\tariff\components
 */
class DiscountService extends Object
{
    /**
     * @param integer $period
     *
     * @return integer
     */
    public function getDiscountByPeriod($period)
    {
        $discount = DiscountForPeriod::find()
            ->select('percent')
            ->where(['<=', 'period', $period])
            ->orderBy('period desc')
            ->scalar();

        return empty($discount) ? 0 : $discount;
    }
}