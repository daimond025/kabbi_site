<?php

namespace app\modules\setting\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\setting\models\Republic;

/**
 * RepublicSearch represents the model behind the search form about `app\modules\setting\models\Republic`.
 */
class RepublicSearch extends Republic
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['republic_id', 'country_id', 'timezone'], 'integer'],
            [
                [
                    'name',
                    'shortname',
                    'name_en',
                    'name_az',
                    'name_de',
                    'country_name',
                    'name_sr',
                    'shortname_en',
                    'shortname_az',
                    'shortname_de',
                    'shortname_sr',
                ],
                'safe',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Republic::find()->with('country');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'republic_id' => $this->republic_id,
            'country_id'  => $this->country_id,
            'timezone'    => $this->timezone,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'country_id', $this->country_id])
            ->andFilterWhere(['like', 'shortname', $this->shortname])
            ->andFilterWhere(['like', 'name_en', $this->name_en])
            ->andFilterWhere(['like', 'name_az', $this->name_az])
            ->andFilterWhere(['like', 'name_de', $this->name_de])
            ->andFilterWhere(['like', 'name_sr', $this->name_sr])
            ->andFilterWhere(['like', 'shortname_en', $this->shortname_en])
            ->andFilterWhere(['like', 'shortname_az', $this->shortname_az])
            ->andFilterWhere(['like', 'shortname_de', $this->shortname_de])
            ->andFilterWhere(['like', 'shortname_sr', $this->shortname_sr]);

        return $dataProvider;
    }
}
