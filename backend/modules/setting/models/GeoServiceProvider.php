<?php

namespace app\modules\setting\models;

use yii\data\ActiveDataProvider;

/**
 * Class GeoServiceProvider
 * @package app\modules\setting\models
 * @property $row_id
 * @property $tenant_id
 * @property $tenant_domain
 * @property $city_id
 * @property $service_type
 * @property $service_provider_id
 */
class GeoServiceProvider extends \yii\elasticsearch\ActiveRecord
{
    const AUTO_GEO_SERVICE_TYPE = 'auto_geo_service';
    const ROUTING_SERVICE_TYPE = 'routing_service';

    public static function primaryKey()
    {
        return ['row_id'];
    }

    public static function index()
    {
        return 'service_provider';
    }

    public static function type()
    {
        return 'row';
    }

    /**
     * @return array the list of attributes for this record
     */
    public function attributes()
    {
        return [
            'row_id',
            'tenant_id',
            'tenant_domain',
            'city_id',
            'service_type',
            'service_provider_id',
            'key_1',
            'key_2',
        ];
    }

    public function rules()
    {
        return [
            [
                [
                    'row_id',
                    'tenant_id',
                    'tenant_domain',
                    'city_id',
                    'service_type',
                    'service_provider_id',
                    'service_provider_id',
                ],
                'required',
            ],
            [['key_1', 'key_2'], 'string', 'max' => 50],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query        = GeoServiceProvider::find();
        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 30,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }

    public static function create($tenantId, $tenantDomain, $cityId, $serviceType, $serviceProviderId, $key1, $key2)
    {
        $row = new GeoServiceProvider([
            'row_id'              => $tenantId . '_' . $cityId . '_' . $serviceType,
            'tenant_id'           => $tenantId,
            'tenant_domain'       => $tenantDomain,
            'city_id'             => $cityId,
            'service_type'        => $serviceType,
            'service_provider_id' => $serviceProviderId,
            'key_1'               => $key1,
            'key_2'               => $key2,
        ]);

        return $row->save();
    }
}