<?php

namespace backend\modules\setting\models\outboundRouting;

use app\modules\setting\models\PhoneLine;
use app\modules\setting\models\TaxiTariff;
use app\modules\setting\models\TaxiTariffHasCity;
use app\modules\setting\models\Tenant;
use app\modules\setting\models\TenantHasCity;
use yii\helpers\ArrayHelper;

/**
 * Class PhoneLineOutboundRoutingModel
 * @package frontend\modules\setting\models\outboundRouting
 */
class PhoneLineOutboundRoutingModel extends PhoneLineOutboundRouting
{
    public $domain;
    public $taxiTariffList;

    public $lineOnNotify = [];
    public $sortOnNotify = [];
    public $timeoutOnNotify = [];
    public $distributionCountOnNotify = [];
    public $distributionDelayOnNotify = [];

    public $lineOnOperator = [];
    public $sortOnOperator = [];
    public $timeoutOnOperator = [];

    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    public function rules()
    {
        return [
            [['name', 'sort', 'active'], 'required'],
            [['tenant_id', 'city_id'], 'required', 'on' => self::SCENARIO_CREATE],

            ['tenant_id', 'exist', 'targetClass' => Tenant::className()],
            [
                'city_id',
                'exist',
                'targetClass'     => TenantHasCity::className(),
                'targetAttribute' => ['city_id', 'tenant_id'],
                'on'              => self::SCENARIO_CREATE,
            ],
            ['name', 'string', 'max' => 100],
            ['sort', 'integer', 'min' => 1, 'max' => 999999],
            ['active', 'in', 'range' => self::getActiveList()],

            [
                'taxiTariffList',
                'each',
                'rule' => [
                    'exist',
                    'targetClass'     => TaxiTariff::className(),
                    'filter'          => ['tenant_id' => $this->tenant_id],
                    'targetAttribute' => 'tariff_id',
                ],
                'on'   => self::SCENARIO_UPDATE,
            ],
            [
                'taxiTariffList',
                'each',
                'rule' => [
                    'exist',
                    'targetClass'     => TaxiTariffHasCity::className(),
                    'filter'          => ['city_id' => $this->city_id],
                    'targetAttribute' => 'tariff_id',
                ],
                'on'   => self::SCENARIO_UPDATE,
            ],

            [
                'taxiTariffList',
                'each',
                'rule' => [
                    'exist',
                    'targetClass'     => TaxiTariff::className(),
                    'targetAttribute' => ['taxiTariffList' => 'tariff_id', 'tenant_id'],
                ],
                'on'   => self::SCENARIO_CREATE,
            ],
            [
                'taxiTariffList',
                'each',
                'rule' => [
                    'exist',
                    'targetClass'     => TaxiTariffHasCity::className(),
                    'targetAttribute' => ['taxiTariffList' => 'tariff_id', 'city_id'],
                ],
                'on'   => self::SCENARIO_CREATE,
            ],

            [
                [
                    'lineOnNotify',
                    'sortOnNotify',
                    'timeoutOnNotify',
                    'distributionCountOnNotify',
                    'distributionDelayOnNotify',
                ],
                'safe',
            ],


            [
                ['lineOnNotify', 'lineOnOperator'],
                'each',
                'rule' => [
                    'exist',
                    'targetClass'     => PhoneLine::className(),
                    'targetAttribute' => 'line_id',
                    'filter'          => ['tenant_id' => $this->tenant_id, 'city_id' => $this->city_id],
                ],
                'on'   => self::SCENARIO_UPDATE,
            ],

            [
                'lineOnNotify',
                'each',
                'rule' => [
                    'exist',
                    'targetClass'     => PhoneLine::className(),
                    'targetAttribute' => ['lineOnNotify' => 'line_id', 'tenant_id', 'city_id'],
                ],
                'on'   => self::SCENARIO_CREATE,
            ],

            [
                'lineOnOperator',
                'each',
                'rule' => [
                    'exist',
                    'targetClass'     => PhoneLine::className(),
                    'targetAttribute' => ['lineOnOperator' => 'line_id', 'tenant_id', 'city_id'],
                ],
                'on'   => self::SCENARIO_CREATE,
            ],


            [
                ['sortOnNotify', 'sortOnOperator'],
                'each',
                'rule' => ['integer', 'min' => 1, 'max' => 999999, 'skipOnEmpty' => false],
            ],
            [
                ['timeoutOnNotify', 'distributionCountOnNotify', 'distributionDelayOnNotify', 'timeoutOnOperator'],
                'each',
                'rule' => ['integer', 'min' => 0, 'max' => 9999999999, 'skipOnEmpty' => false],
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name'           => 'Название',
            'domain'         => 'Домен',
            'tenant_id'      => 'ID арендатора',
            'city_id'        => 'Филиал',
            'taxiTariffList' => 'Список тарифов',
            'sort'           => 'Сортировка',
            'active'         => 'Активность',

            'lineOnNotify'              => 'Телефонная линия',
            'sortOnNotify'              => 'Сортировка',
            'timeoutOnNotify'           => 'Время ожидания ответа (сек.)',
            'distributionCountOnNotify' => 'Кол-во повторов',
            'distributionDelayOnNotify' => 'Задержка между повторами (сек.)',

            'lineOnOperator'    => 'Телефонная линия',
            'sortOnOperator'    => 'Сортировка',
            'timeoutOnOperator' => 'Время ожидания ответа (сек.)',
        ];
    }

    public function getActiveMap()
    {
        return [
            self::ACTIVE     => 'Да',
            self::NOT_ACTIVE => 'Нет',
        ];
    }

    public function getPhoneLineMap()
    {
        $phoneLines = PhoneLine::find()
            ->where([
                'tenant_id' => $this->tenant_id,
                'city_id'   => $this->city_id,
            ])
            ->all();

        return ArrayHelper::map($phoneLines, 'line_id', 'phone');
    }

    public function afterFind()
    {
        parent::afterFind(); // TODO: Change the autogenerated stub

        $this->domain = $this->tenant->domain;

        $this->taxiTariffList = ArrayHelper::getColumn($this->taxiTariff, 'tariff_id');

        if (is_array($this->onNotify)) {
            foreach ($this->onNotify as $model) {
                $this->lineOnNotify[]              = $model->line_id;
                $this->sortOnNotify[]              = $model->sort;
                $this->timeoutOnNotify[]           = $model->timeout;
                $this->distributionCountOnNotify[] = $model->distribution_count;
                $this->distributionDelayOnNotify[] = $model->distribution_delay;
            }
        }

        if (is_array($this->onOperator)) {
            foreach ($this->onOperator as $model) {
                $this->lineOnOperator[]    = $model->line_id;
                $this->sortOnOperator[]    = $model->sort;
                $this->timeoutOnOperator[] = $model->timeout;
            }
        }

    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub

        $this->saveTaxiTariffs();
        $this->savePhoneLinesOnNotify();
        $this->savePhoneLinesOnOperator();
    }

    protected function saveTaxiTariffs()
    {
        $name = ['routing_id', 'tariff_id'];
        $data = [];

        if (is_array($this->taxiTariffList)) {
            foreach ($this->taxiTariffList as $tariff_id) {
                $data[] = [$this->routing_id, $tariff_id];
            }
        }

        PhoneLineOutboundRoutingHasTaxiTariff::deleteAll(['routing_id' => $this->routing_id]);

        app()->db->createCommand()->batchInsert(PhoneLineOutboundRoutingHasTaxiTariff::tableName(), $name,
            $data)->execute();

    }

    protected function savePhoneLinesOnNotify()
    {

        $name = ['routing_id', 'line_id', 'sort', 'timeout', 'distribution_count', 'distribution_delay'];
        $data = [];

        foreach ($this->lineOnNotify as $key => $ignore) {
            $data[] = [
                $this->routing_id,
                ArrayHelper::getValue($this->lineOnNotify, $key),
                ArrayHelper::getValue($this->sortOnNotify, $key),
                ArrayHelper::getValue($this->timeoutOnNotify, $key),
                ArrayHelper::getValue($this->distributionCountOnNotify, $key),
                ArrayHelper::getValue($this->distributionDelayOnNotify, $key),
            ];
        }

        PhoneLineOutboundRoutingOnNotify::deleteAll(['routing_id' => $this->routing_id]);

        app()->db->createCommand()->batchInsert(PhoneLineOutboundRoutingOnNotify::tableName(), $name, $data)->execute();

    }

    protected function savePhoneLinesOnOperator()
    {

        $name = ['routing_id', 'line_id', 'sort', 'timeout'];
        $data = [];

        foreach ($this->lineOnOperator as $key => $ignore) {
            $data[] = [
                $this->routing_id,
                ArrayHelper::getValue($this->lineOnOperator, $key),
                ArrayHelper::getValue($this->sortOnOperator, $key),
                ArrayHelper::getValue($this->timeoutOnOperator, $key),
            ];
        }

        PhoneLineOutboundRoutingOnOperator::deleteAll(['routing_id' => $this->routing_id]);

        app()->db->createCommand()->batchInsert(PhoneLineOutboundRoutingOnOperator::tableName(), $name,
            $data)->execute();
    }

}