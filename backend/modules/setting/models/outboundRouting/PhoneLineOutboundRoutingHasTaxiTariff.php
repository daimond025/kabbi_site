<?php

namespace backend\modules\setting\models\outboundRouting;

use app\modules\setting\models\TaxiTariff;
use yii\db\ActiveRecord;

/**
 * Class PhoneLineOutboundRoutingHasTaxiTariff
 * @package frontend\modules\setting\models\outboundRouting
 *
 * @property integer                  $id
 * @property integer                  $routing_id
 * @property integer                  $tariff_id
 *
 * @property PhoneLineOutboundRouting $outboundRouting
 * @property TaxiTariff               $taxiTariff
 *
 */
class PhoneLineOutboundRoutingHasTaxiTariff extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%phone_line_outbound_routing_has_taxi_tariff}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['routing_id', 'tariff_id'], 'required'],
            ['routing_id', 'exist', 'targetClass' => PhoneLineOutboundRouting::className()],
            ['tariff_id', 'exist', 'targetClass' => TaxiTariff::className()],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutboundRouting()
    {
        return $this->hasOne(PhoneLineOutboundRouting::className(), ['routing_id' => 'routing_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxiTariff()
    {
        return $this->hasOne(TaxiTariff::className(), ['tariff_id' => 'tariff_id']);
    }
}