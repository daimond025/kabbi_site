<?php

namespace backend\modules\setting\models\outboundRouting;

use yii\data\ActiveDataProvider;

/**
 * Class PhoneLineOutboundRoutingSearch
 * @package backend\modules\setting\models\outboundRouting
 *
 * @property boolean $isActive
 */
class PhoneLineOutboundRoutingSearch extends PhoneLineOutboundRouting
{
    public $cityName;
    public $domain;

    const PAGE_SIZE = 50;

    public function rules()
    {
        return [
            [['domain', 'cityName', 'active', 'name'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'routing_id' => 'ID',
            'name'       => 'Название',
            'domain'     => 'Домен',
            'cityName'   => 'Филиал',
            'active'     => 'Активность',
            'sort'       => 'Сортировка',
            'tariffs'    => 'Тарифы',
        ];
    }

    public function search($params)
    {
        $query = self::find();
        $query->alias('r');

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'sort'       => [
                'attributes'   => [
                    'routing_id',
                    'name',
                    'active',
                    'sort',
                    'domain',
                ],
                'defaultOrder' => [
                    'sort' => SORT_ASC,
                ],
            ],
            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['tenant t', 'city c']);

        $query->filterWhere([
            'tenant_id' => $this->tenant_id,
            'active'    => $this->active,
        ]);

        $query->andFilterWhere(['like', 'r.name', $this->name]);
        $query->andFilterWhere(['like', 'domain', $this->domain]);
        $query->andFilterWhere(['like', 'c.name', $this->cityName]);

        return $dataProvider;
    }

    public function getActiveMap()
    {
        return [
            self::ACTIVE     => 'Да',
            self::NOT_ACTIVE => 'Нет',
        ];
    }

    public function getIsActive()
    {
        return (int)$this->active === self::ACTIVE;
    }
}