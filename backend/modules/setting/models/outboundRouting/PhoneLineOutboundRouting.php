<?php

namespace backend\modules\setting\models\outboundRouting;

use app\modules\setting\models\City;
use app\modules\setting\models\TaxiTariff;
use app\modules\setting\models\Tenant;
use app\modules\setting\models\TenantHasCity;
use yii\db\ActiveRecord;

/**
 * Class PhoneLineOutboundRouting
 * @package frontend\modules\setting\models\outboundRouting
 *
 * @property integer                              $routing_id
 * @property integer                              $tenant_id
 * @property integer                              $city_id
 * @property string                               $name
 * @property integer                              $sort
 * @property integer                              $active
 *
 * @property Tenant                               $tenant
 * @property City                                 $city
 * @property TaxiTariff[]                         $taxiTariff
 * @property PhoneLineOutboundRoutingOnNotify[]   $onNotify
 * @property PhoneLineOutboundRoutingOnOperator[] $onOperator
 *
 */
class PhoneLineOutboundRouting extends ActiveRecord
{
    const ACTIVE = 1;
    const NOT_ACTIVE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%phone_line_outbound_routing}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'city_id', 'name', 'active'], 'required'],

            ['tenant_id', 'exist', 'targetClass' => Tenant::className()],
            [
                'city_id',
                'exist',
                'targetClass' => TenantHasCity::className(),
                ['targetAttribute' => ['city_id', 'tenant_id']],
            ],
            ['name', 'string', 'max' => 100],
            ['active', 'in', 'range' => self::getActiveList()],
            ['sort', 'integer', 'min' => 1, 'max' => 999999],

        ];
    }

    /**
     * Список доступных значений атрибутов $active
     * @return integer[]
     */
    public static function getActiveList()
    {
        return [self::ACTIVE, self::NOT_ACTIVE];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxiTariff()
    {
        return $this->hasMany(TaxiTariff::className(), ['tariff_id' => 'tariff_id'])
            ->viaTable(PhoneLineOutboundRoutingHasTaxiTariff::tableName(), ['routing_id' => 'routing_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOnNotify()
    {
        return $this->hasMany(PhoneLineOutboundRoutingOnNotify::className(), ['routing_id' => 'routing_id'])
            ->orderBy('sort');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOnOperator()
    {
        return $this->hasMany(PhoneLineOutboundRoutingOnOperator::className(), ['routing_id' => 'routing_id'])
            ->orderBy('sort');
    }
}