<?php

namespace backend\modules\setting\models\outboundRouting;

use app\modules\setting\models\PhoneLine;
use yii\db\ActiveRecord;

/**
 * Class PhoneLineOutboundRoutingOnOperator
 * @package frontend\modules\setting\models\outboundRouting
 *
 * @property integer                  $id
 * @property integer                  $routing_id
 * @property integer                  $line_id
 * @property integer                  $sort
 * @property integer                  $timeout
 *
 * @property PhoneLineOutboundRouting $outboundRouting
 * @property PhoneLine                $phoneLine
 *
 */
class PhoneLineOutboundRoutingOnOperator extends ActiveRecord
{
    /**
     *
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%phone_line_outbound_routing_on_operator}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['routing_id', 'line_id', 'sort', 'timeout'], 'required'],
            ['routing_id', 'exist', 'targetClass' => PhoneLineOutboundRouting::className()],
            ['line_id', 'exist', 'targetClass' => PhoneLine::className()],
            ['sort', 'integer', 'min' => 1, 'max' => 999999],
            ['timeout', 'integer', 'min' => 0, 'max' => 9999999999],
        ];
    }

    public function attributeLabels()
    {
        return [
            'line_id'            => 'Телефонная линия',
            'sort'               => 'Сортировка',
            'timeout'            => 'Время ожидания ответа (сек.)',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutboundRouting()
    {
        return $this->hasOne(PhoneLineOutboundRouting::className(), ['routing_id' => 'routing_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoneLine()
    {
        return $this->hasone(PhoneLine::className(), ['line_id' => 'line_id']);
    }
}