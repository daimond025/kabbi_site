<?php

namespace backend\modules\setting\models;

use common\modules\tenant\models\Tenant;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%worker_balance_notification}}".
 *
 * @property integer $id
 * @property integer $tenant_id
 * @property string  $name
 * @property string  $url
 * @property string  $token
 * @property integer $writeoff_notify
 * @property integer $deposit_notify
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Tenant  $tenant
 */
class WorkerBalanceNotification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_balance_notification}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'name', 'url', 'token'], 'required'],
            [['tenant_id', 'writeoff_notify', 'deposit_notify', 'created_at', 'updated_at'], 'integer'],
            [['name', 'url', 'token'], 'string', 'max' => 255],
            [
                ['tenant_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Tenant::className(),
                'targetAttribute' => ['tenant_id' => 'tenant_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => t('worker-balance-notification', 'ID'),
            'tenant_id'       => t('worker-balance-notification', 'Tenant ID'),
            'name'            => t('worker-balance-notification', 'Name'),
            'url'             => t('worker-balance-notification', 'Url'),
            'token'           => t('worker-balance-notification', 'Token'),
            'writeoff_notify' => t('worker-balance-notification', 'Writeoff Notify'),
            'deposit_notify'  => t('worker-balance-notification', 'Deposit Notify'),
            'created_at'      => t('worker-balance-notification', 'Created At'),
            'updated_at'      => t('worker-balance-notification', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @inheritdoc
     * @return WorkerBalanceNotificationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new WorkerBalanceNotificationQuery(get_called_class());
    }
}
