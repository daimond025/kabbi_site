<?php

namespace app\modules\setting\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%taxi_tariff}}".
 *
 * @property City[] $city
 */
class TaxiTariff extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%taxi_tariff}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffHasCity()
    {
        return $this->hasOne(TaxiTariffHasCity::className(), ['tariff_id' => 'tariff_id']);
    }

}
