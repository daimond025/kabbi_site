<?php

namespace app\modules\setting\models;

use Yii;

/**
 * This is the model class for table "tbl_tenant_admin_tariff_changeset".
 *
 * @property integer $id
 * @property integer $tenant_id
 * @property integer $tariff_id_old
 * @property integer $tariff_id_new
 * @property integer $option_id_old
 * @property integer $option_id_new
 * @property string $create_date
 *
 * @property TenantAdminTariff $tariffIdNew
 * @property TenantAdminTariff $tariffIdOld
 */
class TenantAdminTariffChangeset extends \yii\db\ActiveRecord {

    public function behaviors() {
        return [
            [
                'class'              => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => 'create_date',
                'updatedAtAttribute' => 'create_date',
                'value'              => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'tbl_tenant_admin_tariff_changeset';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['tenant_id', 'tariff_id_old', 'tariff_id_new', 'option_id_old', 'option_id_new'], 'required'],
            [['tenant_id', 'tariff_id_old', 'tariff_id_new', 'option_id_old', 'option_id_new'], 'integer'],
            [['create_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id'            => 'ID',
            'tenant_id'     => 'ID арендатора',
            'tariff_id_old' => 'Старый тариф',
            'tariff_id_new' => 'Новый тариф',
            'option_id_old' => 'Старая опция',
            'option_id_new' => 'Новая опция',
            'create_date'   => 'Дата создания',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffIdNew() {
        return $this->hasOne(TenantAdminTariff::className(), ['tariff_id' => 'tariff_id_new']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffIdOld() {
        return $this->hasOne(TenantAdminTariff::className(), ['tariff_id' => 'tariff_id_old']);
    }

}
