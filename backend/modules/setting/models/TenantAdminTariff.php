<?php

namespace app\modules\setting\models;

use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "tbl_tenant_admin_tariff".
 *
 * @property integer $tariff_id
 * @property string $name
 * @property integer $block
 * @property string $create_date
 * @property integer $price
 * @property integer $stuff_quantity
 * @property integer $driver_quantity
 *
 * @property TenantAdminTariffChangeset[] $tenantAdminTariffChangesets
 * @property TenantAdminTariffChangeset[] $tenantAdminTariffChangesets0
 */
class TenantAdminTariff extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%tenant_admin_tariff}}';
    }

    public function behaviors() {
        return [
            [
                'class'              => TimestampBehavior::className(),
                'createdAtAttribute' => 'create_date',
                'updatedAtAttribute' => 'create_date',
                'value'              => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'price', 'stuff_quantity', 'driver_quantity'], 'required'],
            [['block', 'price', 'stuff_quantity', 'driver_quantity'], 'integer'],
            [['create_date'], 'safe'],
            [['name'], 'string', 'max' => 32]
        ];
    }
    
    public function subRules()
    {
        $rules = [
            [['name', 'price', 'stuff_quantitiy', 'driver_quantity', 'create_date'], 'filter', 'filter' => '\yii\helpers\HtmlPurifier::process']
        ];
        return array_merge(parent::rules(), $rules);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'tariff_id'       => 'ID тарифа',
            'name'            => 'Имя',
            'block'           => 'Заблокирована?',
            'create_date'     => 'Дата добавления',
            'price'           => 'Цена',
            'stuff_quantity'  => 'Кол-во сотрудников',
            'driver_quantity' => 'Кол-во водителей',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantAdminTariffChangesets() {
        return $this->hasMany(TenantAdminTariffChangeset::className(), ['tariff_id_new' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantAdminTariffChangesets0() {
        return $this->hasMany(TenantAdminTariffChangeset::className(), ['tariff_id_old' => 'tariff_id']);
    }

}
