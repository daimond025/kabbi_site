<?php

namespace app\modules\setting\models;

use Yii;

/**
 * This is the model class for table "tbl_million_error_code".
 *
 * @property integer $log_id
 * @property string $code
 * @property string $description
 */
class MillionErrorCode extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_million_error_code';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'description'], 'required'],
            [['description'], 'string'],
            [['code'], 'string', 'max' => 4]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'log_id' => 'Error ID',
            'code' => 'Код',
            'description' => 'Расшифровка',
        ];
    }
}
