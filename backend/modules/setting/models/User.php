<?php

namespace app\modules\setting\models;

use frontend\modules\tenant\models\UserDispetcher;
use Yii;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "tbl_user".
 *
 * @property integer                    $user_id
 * @property integer                    $tenant_id
 * @property integer                    $position_id
 * @property string                     $email
 * @property string                     $email_confirm
 * @property string                     $password
 * @property string                     $last_name
 * @property string                     $name
 * @property string                     $second_name
 * @property string                     $phone
 * @property string                     $photo
 * @property integer                    $active
 * @property string                     $birth
 * @property string                     $address
 * @property string                     $create_time
 * @property string                     $auth_key
 * @property string                     $password_reset_token
 * @property string                     $access_token
 * @property integer                    $active_time
 * @property string                     $ha1
 * @property string                     $ha1b
 * @property integer                    $auth_exp
 * @property string                     $lang
 *
 * @property CommunityUser[]            $communityUsers
 * @property OrderViews[]               $orderViews
 * @property PublicPlaceModeration[]    $publicPlaceModerations
 * @property PublicPlaceModerationAdd[] $publicPlaceModerationAdds
 * @property StreetModeration[]         $streetModerations
 * @property StreetModerationAdd[]      $streetModerationAdds
 * @property Support[]                  $supports
 * @property Support[]                  $supports0
 * @property Transaction[]              $transactions
 * @property Tenant                     $tenant
 * @property UserPosition               $position
 * @property UserDispetcher[]           $userDispetchers
 * @property UserHasCity[]              $userHasCities
 */
class User extends \yii\db\ActiveRecord
{
    public $cities = [];
    public $logo;
    public $logoLink;
    public $fullName;

    const POSITION_OPERATOR = 6;
    const POSITION_MAIN_OPERATOR = 8;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'position_id', 'email', 'email_confirm', 'password', 'auth_key', 'active_time'], 'required'],
            [['tenant_id', 'position_id', 'active', 'active_time', 'auth_exp'], 'integer'],
            [['birth', 'create_time'], 'safe'],
            [['email'], 'string', 'max' => 40],
            [['email_confirm', 'last_name', 'name', 'second_name'], 'string', 'max' => 45],
            [
                ['password', 'photo', 'address', 'auth_key', 'password_reset_token', 'access_token'],
                'string',
                'max' => 255,
            ],
            [['phone'], 'string', 'max' => 15],
            [['ha1', 'ha1b'], 'string', 'max' => 64],
            [['lang'], 'string', 'max' => 10],
            [['email'], 'unique'],
            [['cities', 'fullName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id'              => 'User ID',
            'tenant_id'            => 'Tenant ID',
            'position_id'          => 'Position ID',
            'email'                => 'Email',
            'email_confirm'        => 'Email Confirm',
            'password'             => 'Password',
            'last_name'            => 'Last Name',
            'name'                 => 'Name',
            'second_name'          => 'Second Name',
            'phone'                => 'Phone',
            'photo'                => 'Photo',
            'active'               => 'Active',
            'birth'                => 'Birth',
            'address'              => 'Address',
            'create_time'          => 'Create Time',
            'auth_key'             => 'Auth Key',
            'access_token'         => 'Access token',
            'password_reset_token' => 'Password Reset Token',
            'active_time'          => 'Active Time',
            'ha1'                  => 'Ha1',
            'ha1b'                 => 'Ha1b',
            'auth_exp'             => 'Auth Exp',
            'lang'                 => 'Lang',
            'cities'               => 'Филиалы',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommunityUsers()
    {
        return $this->hasMany(CommunityUser::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderViews()
    {
        return $this->hasMany(OrderViews::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicPlaceModerations()
    {
        return $this->hasMany(PublicPlaceModeration::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPublicPlaceModerationAdds()
    {
        return $this->hasMany(PublicPlaceModerationAdd::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStreetModerations()
    {
        return $this->hasMany(StreetModeration::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStreetModerationAdds()
    {
        return $this->hasMany(StreetModerationAdd::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupports()
    {
        return $this->hasMany(Support::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupports0()
    {
        return $this->hasMany(Support::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(Transaction::className(), ['user_created' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(UserPosition::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDispetchers()
    {
        return $this->hasMany(UserDispetcher::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserHasCities()
    {
        return $this->hasMany(UserHasCity::className(), ['user_id' => 'user_id']);
    }

    public static function getUserPositions()
    {
        return ArrayHelper::map(UserPosition::find()->all(), 'position_id', function ($model) {
            return Yii::t('company-roles', $model->name, [], 'ru');
        });
    }

    public function getCityList()
    {
        return implode(', ', $this->citiesName);
    }

    public function getUserIdCities()
    {
        return ArrayHelper::getColumn($this->userHasCities, 'city_id');
    }

    public function getUserCities()
    {
        $query = UserHasCity::find()->where(['user_id' => $this->user_id])->with('city')->all();

        return implode(', ', ArrayHelper::getColumn($query, 'city.name'));
    }

    public function getTenantCities()
    {
        $cityList = TenantHasCity::find()->with('city')->where(['tenant_id' => $this->tenant_id])->indexBy('city_id')->all();

        return ArrayHelper::getColumn($cityList, 'city.name');
    }

    public function clearCities()
    {
        UserHasCity::deleteAll('user_id =' . $this->user_id);
    }

    public function addCities()
    {
        if (is_array($this->cities)) {

            foreach ($this->cities as $city_id) {
                $data[] = [$this->user_id, $city_id];
            }

            if (isset($data)) {

                Yii::$app->db->createCommand()->batchInsert(UserHasCity::tableName(), ['user_id', 'city_id'],
                    $data)->execute();

            }
        }
    }

    public function getExplodeFullName($fullName)
    {
        return explode(' ', $fullName);
    }


    public function afterSave($insert, $changedAttributes)
    {

        $update = !$insert;

        if ($insert) {

            (new UserRightsInsert($this))->save();

        } elseif ($update) {

            if (isset($changedAttributes['position_id']) && (int)$changedAttributes['position_id'] !== (int)$this->position_id) {
                (new UserRightsUpdate($this))->save();
            }

        }

        parent::afterSave($insert, $changedAttributes);
    }


}
