<?php
/**
 * Created by PhpStorm.
 * User: zigfried123
 * Date: 22.12.2017
 * Time: 13:51
 */

namespace app\modules\setting\models;


use yii\db\Exception;

class UserRightsUpdate extends UserRightsSaveStrategy
{

    protected function delete()
    {
        $user_id = $this->user->user_id;

        try {
            \Yii::$app->db->createCommand()
                ->delete('{{%auth_assignment}}', "user_id=$user_id")
                ->execute();
        } catch (Exception $e) {
            return false;
        }

        return true;
    }


    public function save()
    {

        if (!$this->delete()) {
            return false;
        }


        if (!parent::save()) {
            return false;
        }

        return true;
    }

}