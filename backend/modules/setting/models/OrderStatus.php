<?php

namespace app\modules\setting\models;

use Yii;

/**
 * This is the model class for table "{{%order_status}}".
 *
 * @property string  $status_id
 * @property string  $name
 * @property string  $status_group
 * @property integer $dispatcher_sees
 */
class OrderStatus extends \yii\db\ActiveRecord
{
    const CACHE_KEY = 'tbl_order_status';

    const STATUS_GROUP_0 = 'new';
    const STATUS_GROUP_1 = 'car_assigned';
    const STATUS_GROUP_2 = 'car_at_place';
    const STATUS_GROUP_3 = 'executing';
    const STATUS_GROUP_4 = 'completed';
    const STATUS_GROUP_5 = 'rejected';
    const STATUS_GROUP_6 = 'pre_order';
    const STATUS_GROUP_7 = 'warning';
    const STATUS_GROUP_8 = 'works';
    const STATUS_NEW = 1;
    const STATUS_OFFER_ORDER = 2;
    const STATUS_DRIVER_REFUSED = 3;
    const STATUS_FREE = 4;
    const STATUS_NOPARKING = 5;
    const STATUS_PRE = 6;
    const STATUS_PRE_GET_WORKER = 7;
    const STATUS_PRE_REFUSE_WORKER = 10;
    const STATUS_PRE_NOPARKING = 16;
    const STATUS_GET_WORKER = 17;
    const STATUS_WORKER_WAITING = 26;
    const STATUS_REJECTED = 39;
    const STATUS_EXECUTING = 36;
    const STATUS_COMPLETED_PAID = 37;
    const STATUS_COMPLETED_NOT_PAID = 38;
    const STATUS_OVERDUE = 52;
    const STATUS_WORKER_LATE = 54;
    const STATUS_EXECUTION_PRE = 55;
    const STATUS_PAYMENT_CONFIRM = 106;
    const STATUS_NO_CARS_BY_DISPATCHER = 107;
    const STATUS_MANUAL_MODE = 108;
    const STATUS_DRIVER_IGNORE_ORDER_OFFER = 109;
    const STATUS_WAITING_FOR_PAYMENT = 110;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_status}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['dispatcher_sees'], 'integer'],
            [['name', 'status_group'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'status_id'       => 'ID',
            'name'            => 'Название',
            'status_group'    => 'Группа',
            'dispatcher_sees' => 'В списке диспетчера',

        ];
    }

    public function getGroupList()
    {
        return [
            'new'          => 'new',
            'pre_order'    => 'pre_order',
            'car_assigned' => 'car_assigned',
            'car_at_place' => 'car_at_place',
            'executing'    => 'executing',
            'completed'    => 'completed',
            'rejected'     => 'rejected',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['status_id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatusTimes()
    {
        return $this->hasMany(OrderStatusTime::className(), ['status_id' => 'status_id']);
    }

    public static function getStatusData()
    {
        $cache = Yii::$app->cache;
        $data  = $cache->get(self::CACHE_KEY);

        if ($data === false) {
            $data = self::find()->asArray()->all();
            $cache->set(self::CACHE_KEY, $data);
        }

        return $data;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $cache = Yii::$app->cache;
        $cache->delete(self::CACHE_KEY);
    }


    public static function getFinishedStatusId()
    {
        return array_merge(self::getCompletedStatusId(), self::getRejectedStatusId());
    }

    public static function getRejectedStatusId()
    {
        return [39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 107];
    }

    public static function getCompletedStatusId()
    {
        return [37, 38];
    }

    /**
     * Группы статусов при которых на карте отображается водитель.
     * @return array
     */
    public static function getStatusGroupsForMapDriverType()
    {
        return ['car_assigned', 'car_at_place', 'executing'];
    }

    /**
     * Группы статусов при которых на карте отображается маршрут заказа.
     * @return array
     */
    public static function getStatusGroupsForMapRouteType()
    {
        return ['completed'];
    }

}
