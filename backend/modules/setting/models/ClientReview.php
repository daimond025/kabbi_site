<?php

namespace app\modules\setting\models;

use Yii;

use app\modules\setting\models\Client;

/**
 * This is the model class for table "{{%client_review}}".
 *
 */
class ClientReview extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client_review}}';
    }
    
    
    /**
     * @return string;
     */
    public function getClientName() {
        $model = Client::findOne($this->client_id);
        
        if($model)
            return $model->getName();
        return null;
    }

}
