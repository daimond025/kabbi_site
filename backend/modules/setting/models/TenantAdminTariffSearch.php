<?php

namespace app\modules\setting\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\setting\models\TenantAdminTariff;

/**
 * TenantAdminTariffSearch represents the model behind the search form about `app\modules\setting\models\TenantAdminTariff`.
 */
class TenantAdminTariffSearch extends TenantAdminTariff
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tariff_id','block', 'price', 'driver_quantity', 'stuff_quantity'], 'integer'],
            [['name', 'create_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TenantAdminTariff::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'tariff_id' => $this->tariff_id,
            'block' => $this->block,
            'create_date' => $this->create_date,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
