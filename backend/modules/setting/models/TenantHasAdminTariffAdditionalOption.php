<?php

namespace app\modules\setting\models;

use Yii;

/**
 * This is the model class for table "tbl_tenant_has_admin_tariff_additional_option".
 *
 * @property integer $id
 * @property integer $tenant_id
 * @property integer $option_id
 */
class TenantHasAdminTariffAdditionalOption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_has_admin_tariff_additional_option}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'option_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tenant_id' =>  'ID арендатора',
            'option_id' => 'ID опции ',
        ];
    }
}
