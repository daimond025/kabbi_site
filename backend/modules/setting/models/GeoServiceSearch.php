<?php

namespace app\modules\setting\models;

use yii\data\ActiveDataProvider;

class GeoServiceSearch extends GeoServiceProvider
{

    /**
     * @return array the list of attributes for this record
     */
    public function attributes()
    {
        return [
            'row_id',
            'tenant_id',
            'tenant_domain',
            'city_id',
            'service_type',
            'service_provider_id',
            'key_1',
            'key_2',
        ];
    }

    public function rules()
    {
        return [
            [
                [
                    'row_id',
                    'tenant_id',
                    'tenant_domain',
                    'city_id',
                    'service_type',
                    'service_provider_id',
                    'service_provider_id',
                ],
                'safe',
            ],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query        = self::find();
        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 30,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'row_id'              => $this->row_id,
            'tenant_id'           => $this->tenant_id,
            'tenant_domain'       => $this->tenant_domain,
            'city_id'             => $this->city_id,
            'service_type'        => $this->service_type,
            'service_provider_id' => $this->service_provider_id,
        ]);

        return $dataProvider;
    }

}