<?php

namespace backend\modules\setting\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\modules\setting\models\Currency;

/**
 * This is the model class for table "tbl_currency_type".
 *
 * @property integer $type_id
 * @property string $name
 *
 * @property Currency[] $currencies
 */
class CurrencyType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_currency_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type_id' => 'Type ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrencies()
    {
        return $this->hasMany(Currency::className(), ['type_id' => 'type_id']);
    }
    
    public static function nameList ()
    {
        $type = self::find()
                ->all();
        
        return ArrayHelper::map($type, 'type_id', 'name');
    }
    
}
