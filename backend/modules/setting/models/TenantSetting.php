<?php

namespace app\modules\setting\models;

use backend\modules\setting\models\ElasticAccess;
use common\modules\tenant\models\DefaultSettings;
use common\modules\tenant\models\TenantSetting as BaseTenantSetting;

/**
 * This is the model class for table "tbl_tenant_setting".
 *
 * @property integer         $setting_id
 * @property integer         $tenant_id
 * @property string          $name
 * @property string          $value
 * @property string          $type
 * @property integer         $city_id
 * @property integer         $position_id
 *
 * @property DefaultSettings $defaultSettings
 * @property Tenant          $tenant
 */
class TenantSetting extends BaseTenantSetting
{

    const PHONE_LINE_SCENARIO = 'PHONE_LINE_SCENARIO';

    const TYPE_SYSTEM = 'system';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_setting}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'name'], 'required'],
            [['tenant_id'], 'integer'],
            [['value', 'type'], 'string'],
            [['name'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'setting_id' => 'ID параметра',
            'tenant_id'  => 'ID арендатора',
            'name'       => 'Название параметра',
            'value'      => 'Значение параметра',
            'type'       => 'Тип',
            'edit'       => 'Редактировать'
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (array_key_exists('value', $changedAttributes)) {
            if ($this->name == ElasticAccess::CLIENT_API_KEY_TENANT_SETTING || $this->name == ElasticAccess::WORKER_API_KEY_TENANT_SETTING) {
                $type_app   = ElasticAccess::getTypeAppByTenantSetting($this->name);
                $elasticRow = ElasticAccess::findOne($this->tenant_id . '_' . $type_app);

                if (!empty($elasticRow)) {
                    $elasticRow->api_key = $this->value;
                    $elasticRow->save(false, ['api_key']);
                }
            }
        }
    }

}
