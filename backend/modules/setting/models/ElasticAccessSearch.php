<?php

namespace backend\modules\setting\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CarOptionSearch represents the model behind the search form about `common\modules\car\models\CarOption`.
 */
class ElasticAccessSearch extends ElasticAccess
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['row_id','tenant_id','type_app','tenant_domain','api_key'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'row_id' => $this->row_id,
            'tenant_id' => $this->tenant_id,
            'type_app' => $this->type_app,
            'tenant_domain' => $this->tenant_domain,
            'api_key' => $this->api_key,
        ]);

        return $dataProvider;
    }
}
