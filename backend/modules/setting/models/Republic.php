<?php

namespace app\modules\setting\models;

use Yii;

/**
 * This is the model class for table "{{%tbl_republic}}".
 *
 * @property string     $republic_id
 * @property integer    $country_id
 * @property string     $name
 * @property string     $name_en
 * @property string     $name_az
 * @property string     $name_de
 * @property string     $name_sr
 * @property string     $name_uz
 * @property string     $name_fi
 * @property string     $name_fa
 * @property string     $name_bg
 * @property string     $timezone
 * @property string     $shortname
 *
 *
 * @property TblCity[]  $tblCities
 * @property TblCountry $country
 */
class Republic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%republic}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'name', 'shortname'], 'required'],
            [['country_id'], 'integer'],
            [['timezone'], 'number'],
            [
                ['name', 'name_en', 'name_az', 'name_de', 'name_sr', 'name_uz', 'name_fi', 'name_fa', 'name_bg'],
                'string',
                'max' => 100,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'republic_id' => 'ID республики',
            'country_id'  => 'Страна',
            'name'        => 'Название республики',
            'timezone'    => 'Часовой пояс',
            'shortname'   => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasMany(City::className(), ['republic_id' => 'republic_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['country_id' => 'country_id']);
    }

    public function getShortNameList()
    {
        return [
            'Респ'  => 'Респ',
            'край'  => 'край',
            'обл'   => 'обл',
            'АО'    => 'АО',
            'state' => 'Штат',
            'остан' => 'остан',
            'г'     => 'г',
        ];
    }
}
