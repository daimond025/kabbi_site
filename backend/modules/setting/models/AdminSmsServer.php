<?php

namespace app\modules\setting\models;


/**
 * This is the model class for table "{{%admin_sms_server}}".
 *
 * @property integer $id
 * @property integer $server_id
 * @property string $login
 * @property string $password
 * @property integer $active
 * @property string $sign
 *
 * @property SmsServer $server
 */
class AdminSmsServer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%admin_sms_server}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['server_id', 'login', 'password', 'active'], 'required'],
            [['server_id', 'active'], 'integer'],
            [['login', 'password'], 'string', 'max' => 45],
            [['sign'], 'string', 'max' => 255],
            ['active', 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'server_id' => 'Server ID',
            'login' => 'Login',
            'password' => 'Password',
            'active' => 'Активность',
            'sign' => 'Подпись',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServer()
    {
        return $this->hasOne(SmsServer::className(), ['server_id' => 'server_id']);
    }
}
