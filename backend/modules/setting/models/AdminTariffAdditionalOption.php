<?php

namespace app\modules\setting\models;

/**
 * This is the model class for table "tbl_admin_tariff_additional_option".
 *
 * @property integer $option_id
 * @property string $name
 * @property integer $price
 * @property integer $active
 * @property string $create_date
 */
class AdminTariffAdditionalOption extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%admin_tariff_additional_option}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'price', 'active', 'create_date'], 'required'],
            [['price', 'active'], 'integer'],
            [['create_date'], 'safe'],
            [['name'], 'string', 'max' => 48]
        ];
    }
    
    public function behaviors() {
        return [
            [
                'class'              => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => 'create_date',
                'updatedAtAttribute' => 'create_date',
                'value'              => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * 
     * @return array cached additional options
     */
    public static function cachedTable() {
        if (!app()->cache->get('adminTariffAddOptions')) {
            $tenantAdditionalOptions = AdminTariffAdditionalOption::find()->all();
            app()->cache->set('adminTariffAddOptions', $tenantAdditionalOptions, 0);
            return $tenantAdditionalOptions;
        }
        return app()->cache->get('adminTariffAddOptions');
    }

    public static function updateCache() {
        app()->cache->delete('adminTariffAddOptions');
        self::cachedTable();
    }
    
    public function afterDelete() {
        self::updateCache();
        parent::afterDelete();
    }

    public function afterSave($insert, $changedAttributes) {
        self::updateCache();
        parent::afterSave($insert, $changedAttributes);
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'option_id' => 'ID опции',
            'name' => 'Название',
            'price' => 'Цена',
            'active' => 'Активна?',
            'create_date' => 'Дата добавления',
        ];
    }

}
