<?php

namespace app\modules\setting\models;

use app\modules\setting\models\City;
use Yii;

/**
 * This is the model class for table "{{%tbl_user_has_city}}".
 *
 * @property string $city_id
 * @property string $user_id
 *
 * @property Order[] $tblOrders
 * @property OrderHistory[] $tblOrderHistories
 * @property User $user
 * @property City $city
 */
class UserHasCity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_has_city}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'user_id'], 'required'],
            [['city_id', 'user_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'city_id' => 'City ID',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderHistories()
    {
        return $this->hasMany(OrderHistory::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(\common\modules\city\models\City::className(), ['city_id' => 'city_id']);
    }

    /**
     * Allow to save multiple city
     * @param array $arCity
     * @param array $user_id
     * @return boolean
     */
    public static function manySave($arCity, $user_id)
    {
        if(is_array($arCity) && !empty($arCity))
        {
            $insertValue = [];
            $connection = app()->db;
            $arCity = array_unique($arCity);

            foreach($arCity as $city_id)
            {
                if($city_id > 0)
                    $insertValue[] = [$city_id, $user_id];
            }

            if(!empty($insertValue))
            {
                $connection->createCommand()->batchInsert(self::tableName(), ['city_id', 'user_id'], $insertValue)->execute();
                self::deleteUserCityListWithRepublic();
                return true;
            }
        }

        return false;
    }
    
    public static function deleteUserCityListWithRepublic()
    {
        app()->cache->delete('UserCityListWithRepublic_' . user()->tenant_id);
    }

    public static function getCityList()
    {
        return self::find()->
                        asArray()->
                        where(['user_id' => user()->user_id])->
                        select('city_id')->
                        with(['city' => function($query) {
                                $query->with(['republic' => function($query) {
                                $query->select(['republic_id', \common\modules\city\models\Republic::tableName() . '.name']);
                            }]);
                        }])->
                        all();
    }

}
