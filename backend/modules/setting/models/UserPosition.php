<?php

namespace app\modules\setting\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%user_position}}".
 *
 * @property integer $position_id
 * @property string $name
 *
 * @property User[] $users
 * @property UserDefaultRight[] $userDefaultRights
 */
class UserPosition extends \yii\db\ActiveRecord
{
    private $_position_map = [];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_position}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'position_id' => 'Position ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDefaultRights()
    {
        return $this->hasMany(UserDefaultRight::className(), ['position_id' => 'position_id']);
    }
    
    public static function getPositionMap()
    {
        return ArrayHelper::map(self::find()->all(), 'position_id', function ($item) {
            return t('company-roles', $item['name']);
        });
    }
    
    public function getPositionName($position_id)
    {
        if(empty($this->_position_map))
            $this->_position_map = self::getPositionMap();
        
        return isset($this->_position_map[$position_id]) ? $this->_position_map[$position_id] : null;
    }
}
