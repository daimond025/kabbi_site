<?php

namespace backend\modules\setting\models;

use Yii;

/**
 * This is the model class for table "{{%source_message}}".
 *
 * @property integer $id
 * @property string $category
 * @property string $message
 *
 * @property Message[] $messages
 */
class SourceMessage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%source_message}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category', 'message'], 'required'],
            [['message'], 'string'],
            [['category'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category' => 'Категория',
            'message' => 'Фраза для перевода',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslates()
    {
        return $this->hasMany(Message::className(), ['id' => 'id']);
    }

    public static function getMessageWithTranslates($message, $category)
    {
        return self::find()
                ->where(['message' => $message, 'category' => $category])
                ->with(['translates' => function($query) {
                    $query->indexBy('language');
                }])
                ->one();
    }

    /**
     * Deletes the table row corresponding to this active record.
     * @param string|array $condition the conditions that will be put in the WHERE part of the DELETE SQL.
     * @return integer|false the number of rows deleted, or false if the deletion is unsuccessful for some reason.
     */
    public static function deleteOne($condition)
    {
        $source_message = SourceMessage::findOne($condition);

        if (!empty($source_message)) {
            return $source_message->delete();
        }

        return false;
    }
}
