<?php

namespace app\modules\setting\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * TenantSettingSearch represents the model behind the search form about `app\modules\setting\models\TenantSetting`.
 */
class TenantSettingSearch extends TenantSetting
{
    public $edit;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['setting_id', 'tenant_id'], 'integer'],
            [['name', 'value', 'type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        
        $query = TenantSetting::find();
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
        
        $this->load($params);


        if(isset($params['tenant_id']))
        {
            $this->tenant_id = $params['tenant_id'];
        }

        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->andFilterWhere([
            'setting_id' => $this->setting_id,
            'tenant_id' => $this->tenant_id,
            'value'     => $this->value,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);


        return $dataProvider;
    }
}