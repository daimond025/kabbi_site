<?php

namespace app\modules\setting\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%taxi_tariff_has_city}}".
 *
 * @property string $tariff_id
 * @property string $city_id
 *
 */
class TaxiTariffHasCity extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%taxi_tariff_has_city}}';
    }
}
