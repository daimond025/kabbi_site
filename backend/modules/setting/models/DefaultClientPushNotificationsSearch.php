<?php

namespace app\modules\setting\models;

use common\modules\tenant\models\DefaultClientPushNotification;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * DefaultClientPushNotificationsSearch represents the model behind the search form about
 * `app\modules\setting\models\DefaultClientPushNotifications`.
 */
class DefaultClientPushNotificationsSearch extends DefaultClientPushNotification
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['push_id', 'position_id'], 'integer'],
            [['type'], 'string'],
            [['text', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DefaultClientPushNotification::find();

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'push_id'     => $this->push_id,
            'type'        => $this->type,
            'position_id' => $this->position_id,
        ]);

        $query->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
