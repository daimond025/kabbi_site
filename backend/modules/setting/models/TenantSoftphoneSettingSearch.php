<?php

namespace app\modules\setting\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\setting\models\TenantSetting;

/**
 * TenantSettingSearch represents the model behind the search form about `app\modules\setting\models\TenantSetting`.
 */
class TenantSoftphoneSettingSearch extends TenantSetting
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TenantSetting::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'setting_id' => $this->setting_id,
            'tenant_id' => $this->tenant_id,
        ]);

        $query->where(['name' => 'SOFTPHONE']);

        return $dataProvider;
    }
}