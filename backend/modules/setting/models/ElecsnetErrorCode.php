<?php

namespace app\modules\setting\models;

use Yii;

/**
 * This is the model class for table "tbl_elecsnet_error_code".
 *
 * @property integer $error_id
 * @property string $code
 * @property string $description
 */
class ElecsnetErrorCode extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_elecsnet_error_code';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'description'], 'required'],
            [['description'], 'string'],
            [['code'], 'string', 'max' => 4]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'error_id' => 'Error ID',
            'code' => 'Код',
            'description' => 'Расшифровка',
        ];
    }
}
