<?php


namespace app\modules\setting\models;


use common\modules\car\models\transport\TransportField;
use common\modules\car\models\transport\TransportFieldEnum;
use common\modules\car\models\transport\TransportTypeHasField as TransportTypeHasFieldBase;
use yii\helpers\ArrayHelper;

class TransportTypeHasField extends TransportTypeHasFieldBase
{
    public $enumList;

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [
                ['enumList'],
                'required',
                'when'       => function ($model) {
                    return $this->isEnumType();
                },
                'whenClient' => 'function(attr,value){
                    var fieldName = $("#' . strtolower($this->formName()) . '-field_id :selected").text();
                    return fieldName.indexOf("enum") > -1;
                }',
            ],
        ]);
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->enumList = $this->getEnumString();
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->isEnumType()) {
            $this->linkEnumVariants();
        }
    }

    private function linkEnumVariants()
    {
        $arEnum = $this->getEnumArray();
        TransportFieldEnum::deleteAll(['type_has_field_id' => $this->id]);

        return TransportFieldEnum::batchInsert($arEnum, $this->id);
    }

    private function isEnumType()
    {
        return $this->field->type == TransportField::ENUM_TYPE;
    }

    private function getEnumArray()
    {
        $arEnum = explode(';', $this->enumList);

        return array_map('trim', $arEnum);
    }

    private function getEnumString()
    {
        $arEnum = ArrayHelper::getColumn($this->enum, 'value');

        return implode('; ', $arEnum);
    }

    public function getFieldName()
    {
        return $this->field->name;
    }

    public function getFieldType()
    {
        return $this->field->type;
    }

    public function getTransportTypeName()
    {
        return $this->type->name;
    }
}