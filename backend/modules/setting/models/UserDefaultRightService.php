<?php
/**
 * Created by PhpStorm.
 * User: zigfried123
 * Date: 22.12.2017
 * Time: 11:21
 */

namespace app\modules\setting\models;

use yii\db\Expression;
use yii\db\Query;


class UserDefaultRightService
{
    private $user;

    /**
     * UserDefaultRightService constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_default_right}}';
    }

    /**
     * @return mixed
     */
    public function getUserDefaultRightsByPosition()
    {
        $arrayRights = $this->getArrayPermissions();

        return $this->extendArrayRights($arrayRights);
    }

    private function getArrayPermissions()
    {
        return (new Query())
            ->from(self::tableName())
            ->select(new Expression('if(rights = "read",CONCAT("read_",permission),permission) as permission'))
            ->where(['position_id' => $this->user->position_id])
            ->andWhere([
                'OR',
                ['rights' => 'read'],
                ['rights' => 'write'],
            ])
            ->all();
    }

    /**
     * @param $arrayRights
     *
     * @return mixed
     */
    private function extendArrayRights(array $arrayRights)
    {
        foreach ($arrayRights as &$right) {

            $right['user_id'] = $this->user->user_id;

            $right['created_at'] = time();

        }

        unset($right);

        return $arrayRights;
    }

}