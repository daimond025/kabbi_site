<?php
/**
 * Created by PhpStorm.
 * User: zigfried123
 * Date: 22.12.2017
 * Time: 13:48
 */

namespace app\modules\setting\models;

use Yii;
use yii\db\Exception;


class UserRightsSaveStrategy
{

    protected $user;

    public function __construct(User $user){
        $this->user = $user;
    }

    private function getArrayRights()
    {
        return (new UserDefaultRightService($this->user))->getUserDefaultRightsByPosition();
    }

    public function save()
    {
        $arrayRights = $this->getArrayRights();

        try {

            Yii::$app->db->createCommand()->batchInsert('{{%auth_assignment}}',
                ['item_name', 'user_id', 'created_at'],
                $arrayRights)->execute();

        }catch(Exception $e){
            return false;
        }

        return true;

    }


}