<?php

namespace app\modules\setting\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * TransportTypeHasFieldSearch represents the model behind the search form about `app\modules\setting\models\TransportTypeHasField`.
 */
class TransportTypeHasFieldSearch extends Model
{
    public $fieldName;
    public $transportTypeName;
    public $fieldType;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fieldName', 'transportTypeName', 'fieldType'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TransportTypeHasField::find()->with(['enum']);
        $query->joinWith([
            'field f',
            'type t',
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'fieldName' => [
                    'asc'  => ['f.name' => SORT_ASC],
                    'desc' => ['f.name' => SORT_DESC],
                ],
                'fieldType' => [
                    'asc'  => ['f.type' => SORT_ASC],
                    'desc' => ['f.type' => SORT_DESC],
                ],
                'transportTypeName' => [
                    'asc'  => ['t.name' => SORT_ASC],
                    'desc' => ['t.name' => SORT_DESC],
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions

        return $dataProvider;
    }
}
