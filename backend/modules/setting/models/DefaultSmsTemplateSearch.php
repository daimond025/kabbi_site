<?php


namespace app\modules\setting\models;

use common\modules\tenant\models\DefaultSmsTemplate;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class DefaultSmsTemplateSearch
 * @package app\modules\setting\models
 */
class DefaultSmsTemplateSearch extends DefaultSmsTemplate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['template_id', 'position_id'], 'integer'],
            [['type'], 'string'],
            [['text', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = parent::find();

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'template_id' => $this->template_id,
            'type'        => $this->type,
            'position_id' => $this->position_id,
        ]);

        $query->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}