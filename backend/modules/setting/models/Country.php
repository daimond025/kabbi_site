<?php

namespace app\modules\setting\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%tbl_country}}".
 *
 * @property integer $country_id
 * @property string $name
 * @property string $name_en
 * @property string $name_az
 * @property string $name_de
 * @property string $name_sr
 * @property string $name_ro
 * @property string $name_uz
 * @property string $name_fi
 * @property string $name_fa
 * @property string $name_bg
 *
 * @property TblRepublic[] $tblRepublics
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%country}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [
                [
                    'name',
                    'name_en',
                    'name_az',
                    'name_de',
                    'name_sr',
                    'name_ro',
                    'name_uz',
                    'name_fi',
                    'name_fa',
                    'name_bg',
                ],
                'string',
                'max' => 255,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'country_id' => 'Country ID',
            'name'       => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepublics()
    {
        return $this->hasMany(Republic::className(), ['country_id' => 'country_id']);
    }

    /**
     * @return array
     */
    public static function getCountryList()
    {
        return ArrayHelper::map(self::find()->all(), 'country_id', 'name');
    }
}
