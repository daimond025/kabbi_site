<?php

namespace backend\modules\setting\models\entities;


use backend\modules\setting\components\TenantServiceComponent;
use common\modules\tenant\models\Tenant;

class TenantForExtrinsicDispatcher extends Tenant
{

    public $domain;

    public function rules()
    {
        return [
            [['tenant_id', 'domain'], 'required'],
            [
                ['tenant_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Tenant::className(),
                'message' => 'Такого арендатора не существует.'
            ],
            [
                'tenant_id', function ($attribute) {
                    if ($this->tenant_id == (new TenantServiceComponent())->getTenantId($this->domain)) {
                        $this->addError($attribute, 'Этот арендатор уже выбран в поле "Головной арендатор".');
                    }
                }
            ]
        ];
    }

}