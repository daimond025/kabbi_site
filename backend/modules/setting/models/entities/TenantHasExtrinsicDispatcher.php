<?php

namespace backend\modules\setting\models\entities;

use app\modules\setting\models\Tenant;
use Yii;

/**
 * This is the model class for table "{{%tenant_has_extrinsic_dispatcher}}".
 *
 * @property integer                 $user_id
 * @property integer                 $tenant_id
 *
 * @property Tenant                  $tenant
 * @property UserExtrinsicDispatcher $user
 */
class TenantHasExtrinsicDispatcher extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_has_extrinsic_dispatcher}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'tenant_id'], 'required'],
            [['user_id', 'tenant_id'], 'integer'],
            [
                ['tenant_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Tenant::className(),
                'targetAttribute' => ['tenant_id' => 'tenant_id'],
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => UserExtrinsicDispatcher::className(),
                'targetAttribute' => ['user_id' => 'user_id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id'   => Yii::t('app', 'User ID'),
            'tenant_id' => Yii::t('app', 'Tenant ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserExtrinsicDispatcher::className(), ['user_id' => 'user_id']);
    }
}
