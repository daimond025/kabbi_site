<?php

namespace backend\modules\setting\models\entities;


use app\models\User;
use app\modules\setting\models\Tenant;
use backend\modules\setting\components\TenantServiceComponent;
use backend\modules\setting\models\query\ExtrinsicDispatcherQuery;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

class UserExtrinsicDispatcher extends User implements IdentityInterface
{

    const POSITION_EXTRINSIC_DISPATCHER = 14;
    const SCENARIO_UPDATE = 'update';

    public $password_repeat;
    public $password_for_update;
    public $tenants_relation;

    protected $domain;

    public static function find()
    {
        return new ExtrinsicDispatcherQuery(__CLASS__);
    }

    public function rules()
    {
        return [
            [
                [
                    'email',
                    'name',
                    'last_name',
                ],
                'required',
            ],

            ['access_token', 'string', 'max' => 255],
            [
                'access_token',
                'unique',
                'targetClass' => User::class,
                'targetAttribute' => 'access_token',
                'filter' => ['not', ['user_id' => $this->user_id]],
            ],

            ['active', 'in', 'range' => [1, 0]],

            [['password_repeat', 'domain', 'password'], 'required', 'except' => self::SCENARIO_UPDATE],
            ['password_for_update', 'safe'],
            [['active_time', 'tenant_id'], 'integer'],
            [['email'], 'string', 'max' => 255],
            [['last_name', 'name', 'second_name'], 'string', 'max' => 45],
            [['password', 'photo', 'address'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 15],

            ['lang', 'string', 'max' => 10],

            [
                'password_repeat',
                'compare',
                'compareAttribute' => 'password',
            ],

            [
                ['email'],
                'unique',
                'targetClass'     => User::className(),
                'except'          => self::SCENARIO_UPDATE,
            ],
            [
                ['domain'],
                'exist',
                'targetClass' => Tenant::className(),
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_id'              => 'User ID',
            'tenant_id'            => 'Tenant ID',
            'position_id'          => 'Position ID',
            'email'                => 'Email',
            'email_confirm'        => 'Email Confirm',
            'password'             => 'Пароль',
            'password_repeat'      => 'Повтор пароля',
            'password_for_update'  => 'Пароль',
            'last_name'            => 'Фамилия',
            'name'                 => 'Имя',
            'second_name'          => 'Отчество',
            'phone'                => 'Телефон',
            'photo'                => 'Photo',
            'active'               => 'Active',
            'birth'                => 'Birth',
            'address'              => 'Address',
            'create_time'          => 'Create Time',
            'auth_key'             => 'Auth Key',
            'password_reset_token' => 'Password Reset Token',
            'active_time'          => 'Active Time',
            'role'                 => 'Role',
            'lang'                 => 'Язык',
            'domain'               => 'Головной арендатор',
        ];
    }

    public function beforeValidate()
    {
        if ($this->domain) {
            $this->tenant_id = (new TenantServiceComponent())->getTenantId($this->domain);
        }

        return parent::beforeValidate();
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->active   = 1;
            $this->password = \Yii::$app->getSecurity()->generatePasswordHash($this->password);
            $this->setAccessToken();
        }

        if ($this->password_for_update) {
            $this->password = \Yii::$app->getSecurity()->generatePasswordHash($this->password_for_update);
            $this->setAccessToken();
        }


        $this->position_id = self::POSITION_EXTRINSIC_DISPATCHER;

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->updateTenantsRelation();
    }


    public static function findIdentity($id)
    {
        return self::findOne(['user_id' => $id]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return self::findOne(['access_token' => $token]);
    }

    public function getId()
    {
        $this->user_id;
    }

    public function getAuthKey()
    {
        $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function getDomain()
    {
        if ($this->domain) {
            return $this->domain;
        }

        return (new TenantServiceComponent())->getDomainId($this->tenant_id);
    }

    public function setDomain($value)
    {
        $this->domain = $value;
    }

    public function setAccessToken()
    {
        $security = \Yii::$app->security;
        $this->access_token = $security->generatePasswordHash($security->generateRandomString());
    }


    /**
     * @return ActiveQuery
     */
    public function getSavedTenant()
    {
        return $this->hasMany(TenantForExtrinsicDispatcher::className(), ['tenant_id' => 'tenant_id'])
            ->viaTable(TenantHasExtrinsicDispatcher::tableName(), ['user_id' => 'user_id']);
    }

    public function updateTenantsRelation()
    {
        $currentTenantIds = $this->getSavedTenant()->select('tenant_id')->column();
        $newTenantIds = ArrayHelper::getColumn($this->tenants_relation, 'tenant_id');


        $toInsert = [];
        foreach (array_filter(array_diff($newTenantIds, $currentTenantIds)) as $tenantId) {
            $toInsert[] = ['user_id' => $this->user_id, 'tenant_id' => $tenantId];
        }

        if ($toInsert) {
            TenantHasExtrinsicDispatcher::getDb()
                ->createCommand()
                ->batchInsert(
                    TenantHasExtrinsicDispatcher::tableName(),
                    ['user_id', 'tenant_id'],
                    $toInsert
                )
                ->execute();
        }

        if ($toRemove = array_filter(array_diff($currentTenantIds, $newTenantIds))) {
            TenantHasExtrinsicDispatcher::deleteAll(['user_id' => $this->user_id, 'tenant_id' => $toRemove]);
        }
    }

}