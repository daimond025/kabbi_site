<?php

namespace app\modules\setting\models;

use Yii;

/**
 * This is the model class for table "{{%client}}".
 *
 */
class Client extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client}}';
    }
    
    /**
     * @return string
     */
    public function getName()
    {
        if(is_null($this->last_name) && is_null($this->name) && is_null($this->second_name)) {
            return '';
        }
        
        return $this->last_name . ' ' . $this->name . ' ' . $this->second_name;
    }

}
