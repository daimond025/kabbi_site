<?php

namespace app\modules\setting\models;


/**
 * This is the model class for table "tbl_phone_line".
 *
 * @property integer    $line_id
 * @property integer    $tenant_id
 * @property integer    $tariff_id
 * @property integer    $city_id
 * @property string     $phone
 * @property string     $incoming_scenario
 * @property integer    $block
 * @property string     $r_username
 * @property string     $r_domain
 * @property string     $sip_server_local
 * @property string     $auth_proxy
 * @property integer    $port
 * @property string     $realm
 * @property string     $typeServer
 * @property integer    $draft
 *
 * @property City       $city
 * @property TaxiTariff $tariff
 * @property Tenant     $tenant
 * @property boolean    $isDraft
 * @property boolean    $isActive
 */
class PhoneLine extends \yii\db\ActiveRecord
{
    const YES = 1;
    const NO = 0;
    const ALL = 2;

    public $domain;
    public $password;

    const TYPE_SERVER_KAMAILIO = 'kamailio';
    const TYPE_SERVER_ASTERISK = 'asterisk';

    const DEFAULT_PORT = 5060;
    const DEFAULT_REALM = 11;
    const DEFAULT_SIP_SERVER_LOCAL = 'media1';

    const DRAFT = 1;
    const NOT_DRAFT = 0;

    const BLOCK = 1;
    const NOT_BLOCK = 0;

    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_UPDATE_FROM_DRAFT = 'updateFromDraft';

    const TYPE_CREATE = 'create';
    const TYPE_UPDATE = 'update';
    const TYPE_DELETE = 'delete';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_phone_line';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'line_id',
                    'tenant_id',
                    'tariff_id',
                    'city_id',
                    'phone',
                    'incoming_scenario',
                    'block',
                    'r_username',
                    'r_domain',
                    'password',
                    'sip_server_local',
                    'auth_proxy',
                    'port',
                    'realm',
                    'typeServer',
                    'draft',
                ],
                'trim',
            ],
            [['domain', 'tariff_id', 'phone', 'block', 'draft', 'incoming_scenario'], 'required'],

            [
                'tenant_id',
                'exist',
                'targetClass'     => \common\modules\tenant\models\Tenant::className(),
                'targetAttribute' => ['tenant_id', 'domain'],
                'skipOnEmpty'     => false,
                'on'              => self::SCENARIO_CREATE,
            ],

            [
                'tariff_id',
                'exist',
                'targetClass'     => TaxiTariff::className(),
                'targetAttribute' => ['tariff_id', 'tenant_id'],
            ],
            [
                'tariff_id',
                'exist',
                'targetClass'     => TaxiTariffHasCity::className(),
                'targetAttribute' => ['tariff_id', 'city_id'],
            ],

            [
                'city_id',
                'exist',
                'targetClass'     => TenantHasCity::className(),
                'targetAttribute' => ['city_id', 'tenant_id'],
                'skipOnEmpty'     => false,
                'on'              => self::SCENARIO_CREATE,
            ],

            [['phone'], 'string', 'max' => 20],
            [['phone'], 'unique'],

            ['block', 'in', 'range' => self::getBlockTypeList()],

            [
                'draft',
                'filter',
                'filter' => function ($value) {
                    $oldValue = $this->getOldAttribute('draft');

                    return (int)$oldValue === self::NOT_DRAFT ? self::NOT_DRAFT : $value;
                },
                'except' => self::SCENARIO_CREATE,
            ],

            ['draft', 'in', 'range' => self::getDraftTypeList()],

            [
                ['r_username', 'r_domain', 'realm', 'port', 'sip_server_local', 'auth_proxy', 'typeServer'],
                'required',
                'when'       => [$this, 'whenIsNotDraft'],
                'whenClient' => self::whenClientIsNotDraft(),
            ],

            [
                'password',
                'string',
                'min'         => 4,
                'on'          => [self::SCENARIO_UPDATE_FROM_DRAFT, self::SCENARIO_CREATE],
                'when'        => [$this, 'whenIsNotDraft'],
                'skipOnEmpty' => false,
                'whenClient'  => self::whenClientIsNotDraft(),
            ],

            [
                'password',
                'string',
                'min' => 4,
                'on'  => self::SCENARIO_UPDATE,
            ],

            [
                ['r_username', 'r_domain', 'realm', 'password'],
                'string',
                'max' => 50,
                //                'when'       => [$this, 'whenIsNotDraft'],
                //                'whenClient' => self::whenClientIsNotDraft(),
            ],


            [
                'port',
                'integer',
                'max' => 99999,
                //                'when'       => [$this, 'whenIsNotDraft'],
                //                'whenClient' => self::whenClientIsNotDraft(),
            ],

            [
                ['sip_server_local', 'auth_proxy'],
                'string',
                'max' => 255,
                //                'when'       => [$this, 'whenIsNotDraft'],
                //                'whenClient' => self::whenClientIsNotDraft(),
            ],

            [
                'typeServer',
                'in',
                'range' => self::getSupportedTypeServerList(),
                //                'when'       => [$this, 'whenIsNotDraft'],
                //                'whenClient' => self::whenClientIsNotDraft(),
            ],

        ];
    }


    public function whenIsDraft($model)
    {
        return (int)$model->draft === self::DRAFT;
    }

    public function whenIsNotDraft($model)
    {
        return (int)$model->draft === self::NOT_DRAFT;
    }

    public static function whenClientIsNotDraft()
    {
        return 'function(attr,value){
                    var model = attr.input.replace("-" + attr.name, "");
                    return $(model + "-draft").val() == ' . self::NOT_DRAFT . '
                }';

    }


    public function getIsDraft()
    {
        return (int)$this->draft === self::DRAFT;
    }

    public function setIsDraft($value)
    {
        $value       = (bool)$value;
        $this->draft = (int)$value;
    }


    public function getIsBlock()
    {
        return (int)$this->block === self::BLOCK;
    }

    public function setIsBlock($value)
    {
        $value       = (bool)$value;
        $this->block = (int)$value;
    }

    public function getIsFromDraft()
    {
        $oldDraft = $this->getOldAttribute('draft');

        return (int)$oldDraft != self::NOT_DRAFT && !$this->isDraft;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'line_id'           => 'ID',
            'tenant_id'         => 'Ид арендатора',
            'domain'            => 'Домен',
            'tariff_id'         => 'Тариф',
            'city_id'           => 'Филиал',
            'phone'             => 'Телефон',
            'incoming_scenario' => 'Сценарии',
            'block'             => 'Блокировка',
            'draft'             => 'Черновик',
        ];
    }

    public static function getDraftTypeList()
    {
        return array_keys(self::getDraftTypeMap());
    }

    public static function getBlockTypeList()
    {
        return array_keys(self::getBlockTypeMap());
    }

    public static function getDraftTypeMap()
    {
        return [
            self::DRAFT     => 'Да',
            self::NOT_DRAFT => 'Нет',
        ];
    }

    public static function getBlockTypeMap()
    {
        return [
            self::BLOCK     => 'Да',
            self::NOT_BLOCK => 'Нет',
        ];
    }

    public static function getSupportedTypeServerList()
    {
        return [
            self::TYPE_SERVER_ASTERISK,
            self::TYPE_SERVER_KAMAILIO,
        ];
    }

    public static function getTypeServerList()
    {
        return [
            self::TYPE_SERVER_ASTERISK => self::TYPE_SERVER_ASTERISK,
            self::TYPE_SERVER_KAMAILIO => self::TYPE_SERVER_KAMAILIO,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(TaxiTariff::className(), ['tariff_id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    public function unserializeIncomingScenario()
    {
        if ($incomingScenario = unserialize($this->incoming_scenario)) {
            $this->incoming_scenario = json_encode($incomingScenario);
        } else {
            $this->incoming_scenario = '';
        }
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->domain = $this->tenant->domain;
        $this->unserializeIncomingScenario();
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->incoming_scenario = serialize(json_decode($this->incoming_scenario, true));
            if (!$insert) {

                $diff           = array_diff($this->attributes, $this->oldAttributes);
                $diffAttributes = array_keys($diff);

                if (array_intersect($diffAttributes,
                    ['tenant_id', 'tariff_id', 'city_id', 'phone', 'incoming_scenario', 'block'])) {
                    $this->deleteInRedis();
                }
            }


            return true;
        }

        return false;
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->deleteInRedis();

            return true;
        }

        return false;
    }

    public function deleteInRedis()
    {
        \Yii::$app->get('redis_phone_scenario')->executeCommand('del', [$this->oldAttributes['phone']]);
    }

    public function getParamsByVoipApi($type)
    {
        switch ($type) {
            case self::TYPE_CREATE:
                return [
                    'r_username'     => $this->r_username,
                    'auth_password'  => $this->password,
                    'r_domain'       => $this->r_domain,
                    'sipServerLocal' => $this->sip_server_local,
                    'auth_proxy'     => $this->auth_proxy,
                    'port'           => $this->port,
                    'realm'          => $this->realm,
                    'phoneLine'      => $this->phone,
                    'typeServer'     => $this->typeServer,
                    'typeQuery'      => $type,
                    'tenantId'       => $this->tenant_id,
                    'phoneLineId'    => $this->line_id,
                ];

            case self::TYPE_UPDATE:
                $params = [
                    'r_username'     => $this->r_username,
                    'r_domain'       => $this->r_domain,
                    'sipServerLocal' => $this->sip_server_local,
                    'auth_proxy'     => $this->auth_proxy,
                    'port'           => $this->port,
                    'realm'          => $this->realm,
                    'phoneLine'      => $this->phone,
                    'typeServer'     => $this->typeServer,
                    'typeQuery'      => $type,
                    'tenantId'       => $this->tenant_id,
                    'phoneLineId'    => $this->line_id,
                ];
                if (!empty($this->password)) {
                    $params['auth_password'] = $this->password;
                }

                return $params;

            case self::TYPE_DELETE:
                return [
                    'phoneLineId'    => $this->line_id,
                    'sipServerLocal' => $this->sip_server_local,
                    'typeServer'     => $this->typeServer,
                ];
        }
    }
}
