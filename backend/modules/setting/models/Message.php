<?php

namespace backend\modules\setting\models;

use Yii;

/**
 * This is the model class for table "{{%message}}".
 *
 * @property integer $id
 * @property string $language
 * @property string $translation
 *
 * @property SourceMessage $id0
 */
class Message extends \yii\db\ActiveRecord
{
    public $message;
    public $category;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%message}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language', 'translation', 'message', 'category'], 'required'],
            [['translation', 'category', 'message'], 'string'],
            [['language'], 'string', 'max' => 16],
            ['language', 'unique', 'targetAttribute' => ['id', 'language'], 'message' => 'Для данного языка уже есть перевод текущей фразы'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'language'    => 'Язык',
            'translation' => 'Перевод',
            'message'     => 'Фраза для перевода',
            'category'    => 'Категория',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSourceMessage()
    {
        return $this->hasOne(SourceMessage::className(), ['id' => 'id']);
    }

    public static function getLanguageVariants()
    {
        $languages = app()->params['supportedLanguages'];
        array_walk($languages, function (&$value) {
            $value = t('language', $value);
        });
        return $languages;
    }
    

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->id = $this->getSourceMessageId();
        }

        return parent::beforeValidate();
    }

    public function beforeSave($insert)
    {
        if (empty($this->id)) {
            $this->setSourceMessageId();
        }
        return parent::beforeSave($insert);
    }

    private function getSourceMessageId()
    {
        return SourceMessage::find()->where(['category' => $this->category, 'message' => $this->message])->select('id')->scalar();
    }

    private function setSourceMessageId()
    {
        $sourse_message = new SourceMessage(['category' => $this->category, 'message' => $this->message]);
        $sourse_message->save(false);

        $this->id = $sourse_message->id;
    }

    /**
     * Allow to insert a lot of rows.
     * @param integer $source_message_id ID of translate phrase(tbl_source_message)
     * @param array $translate_map ['ru-RU' => 'Some translate text']
     * @return null|integer number of rows affected by the execution.
     * @throws Exception execution failed
     */
    public static function batchInsert($source_message_id, array $translate_map)
    {
        $arInsert = [];
        foreach ($translate_map as $key => $translate) {
            if (!empty($translate)) {
                $arInsert[] = [$source_message_id, $key, $translate];
            }
        }

        if (!empty($arInsert)) {
            return app()->db->createCommand()->batchInsert(self::tableName(), ['id', 'language', 'translation'], $arInsert)->execute();
        }

        return null;
    }

}
