<?php

namespace app\modules\setting\models;

use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "tbl_user".
 *
 * @property integer $user_id
 * @property integer $tenant_id
 * @property integer $position_id
 * @property string $email
 * @property string $email_confirm
 * @property string $password
 * @property string $last_name
 * @property string $name
 * @property string $second_name
 * @property string $phone
 * @property string $photo
 * @property integer $active
 * @property string $birth
 * @property string $address
 * @property string $create_time
 * @property string $auth_key
 * @property string $password_reset_token
 * @property integer $active_time
 * @property string $ha1
 * @property string $ha1b
 * @property integer $auth_exp
 * @property string $lang
 *
 * @property CommunityUser[] $communityUsers
 * @property OrderViews[] $orderViews
 * @property PublicPlaceModeration[] $publicPlaceModerations
 * @property PublicPlaceModerationAdd[] $publicPlaceModerationAdds
 * @property StreetModeration[] $streetModerations
 * @property StreetModerationAdd[] $streetModerationAdds
 * @property Support[] $supports
 * @property Support[] $supports0
 * @property Transaction[] $transactions
 * @property Tenant $tenant
 * @property UserPosition $position
 * @property UserDispetcher[] $userDispetchers
 * @property UserHasCity[] $userHasCities
 */
class UserSearch extends User
{

    const PAGE_SIZE = 50;
    public $cities;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'tenant_id' => 'Tenant ID',
            'position_id' => 'Position ID',
            'email' => 'Email',
            'email_confirm' => 'Email Confirm',
            'password' => 'Password',
            'last_name' => 'Last Name',
            'name' => 'Name',
            'second_name' => 'Second Name',
            'phone' => 'Phone',
            'photo' => 'Photo',
            'active' => 'Active',
            'birth' => 'Birth',
            'address' => 'Address',
            'create_time' => 'Create Time',
            'auth_key' => 'Auth Key',
            'password_reset_token' => 'Password Reset Token',
            'active_time' => 'Active Time',
            'ha1' => 'Ha1',
            'ha1b' => 'Ha1b',
            'auth_exp' => 'Auth Exp',
            'lang' => 'Lang',
        ];
    }

    public function search($params)
    {

        $query = self::find()->alias('u')->select(['u.*','GROUP_CONCAT(uc.name SEPARATOR ", ") as cities'])->joinWith([
            'position',
            'userHasCities.city uc'
        ])->groupBy('u.user_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        $this->load($params);
        if (isset($params['tenant_id'])) {
            $this->tenant_id = $params['tenant_id'];
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->filterWhere([
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere([
            'tenant_id' => $this->tenant_id,
        ]);

        return $dataProvider;
    }


}
