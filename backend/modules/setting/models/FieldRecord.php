<?php

namespace app\modules\setting\models;

use yii\db\ActiveRecord;

/**
 * Class FieldRecord
 * @package app\modules\tenant\models\field
 *
 * @property integer $field_id
 * @property integer $tenant_id
 * @property integer $city_id
 * @property integer $position_id
 * @property string  $name
 * @property integer $value
 */
class FieldRecord extends ActiveRecord
{
    const NAME_WORKER_BIRTHDAY = 'workerBirthday';
    const NAME_WORKER_EMAIL = 'workerEmail';
    const NAME_WORKER_CARD_NUMBER = 'workerCardNumber';
    const NAME_WORKER_YANDEX_ACCOUNT_NUMBER = 'workerYandexAccountNumber';
    const NAME_WORKER_DESCRIPTION = 'workerDescription';
    const NAME_WORKER_PASSPORT = 'workerPassport';
    const NAME_WORKER_SNILS = 'workerSNILS';
    const NAME_WORKER_INN = 'workerINN';
    const NAME_WORKER_OGRNIP = 'workerOGRNIP';

    const NAME_WORKER_POSITION_OSAGO = 'workerPositionOSAGO';
    const NAME_WORKER_POSITION_DRIVER_LICENSE = 'workerPositionDriverLicense';
    const NAME_WORKER_POSITION_PTS = 'workerPositionPTS';
    const NAME_WORKER_POSITION_MEDICAL_CERTIFICATE = 'workerPositionMedicalCertificate';

    const NAME_CAR_CALLSIGN = 'carCallsign';
    const NAME_CAR_LICENSE = 'carLicense';


    public static function tableName()
    {
        return '{{%tenant_show_field}}';
    }
}