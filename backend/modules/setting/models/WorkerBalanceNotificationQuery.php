<?php

namespace backend\modules\setting\models;

/**
 * This is the ActiveQuery class for [[WorkerBalanceNotification]].
 *
 * @see WorkerBalanceNotification
 */
class WorkerBalanceNotificationQuery extends \yii\db\ActiveQuery
{
    public function byTenantId($tenantId)
    {
        return $this->andWhere(['tenant_id' => $tenantId]);
    }
}
