<?php

namespace backend\modules\setting\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class WorkerBalanceNotificationSearch
 * @package backend\modules\setting\models
 */
class WorkerBalanceNotificationSearch extends Model
{
    /**
     * @param int $tenantId
     *
     * @return ActiveDataProvider
     */
    public function search($tenantId)
    {
        return new ActiveDataProvider([
            'query' => WorkerBalanceNotification::find()->byTenantId($tenantId),
        ]);
    }
}