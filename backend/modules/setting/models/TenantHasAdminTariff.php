<?php

namespace app\modules\setting\models;
use app\modules\setting\models\TenantAdminTariffChangeset;
use app\modules\setting\models\TenantAdminTariff;
use Yii;

/**
 * This is the model class for table "tbl_tenant_has_admin_tariff".
 *
 * @property integer $tariff_id
 * @property integer $tenant_id
 * @property string $create_date
 */
class TenantHasAdminTariff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tenant_has_admin_tariff}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id'], 'integer'],
            [['create_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tariff_id' => 'Tariff ID',
            'tenant_id' => 'Tenant ID',
            'create_date' => 'Create Date',
        ];
    }
}
