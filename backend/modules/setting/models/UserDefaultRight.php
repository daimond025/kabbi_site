<?php

namespace app\modules\setting\models;

use frontend\components\rbac\DbManager;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%user_default_right}}".
 *
 * @property integer      $id
 * @property integer      $position_id
 * @property string       $permission
 * @property string       $rights
 *
 * @property AuthItem     $permission0
 * @property UserPosition $position
 */
class UserDefaultRight extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_default_right}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['permission'],
                'filter',
                'filter' => function ($value) {
                    return strtolower(trim($value));
                },
            ],
            [['position_id', 'permission', 'rights'], 'required'],
            [['position_id'], 'integer'],
            [['rights'], 'string'],
            [['permission'], 'string', 'max' => 64],
            [
                'permission',
                'unique',
                'targetAttribute' => ['position_id', 'permission'],
                'message'         => 'Для данной должности разрешение уже добавлено',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'position_id' => 'Должность',
            'permission'  => 'Разрешение',
            'rights'      => 'Права',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPermission0()
    {
        return $this->hasOne(AuthItem::className(), ['name' => 'permission']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(UserPosition::className(), ['position_id' => 'position_id']);
    }

    /**
     * Сохраненние прав.
     *
     * @param array $arPositionId Массив из position_id
     * @param array $arRights ['cars' => 'all', 'orders' => 'read']
     *
     * @return boolean
     */
    public static function saveRights(array $arPositionId, array $arRights)
    {
        if (!empty($arPositionId) && !empty($arRights)) {
            $insertValue = [];
            $connection  = app()->db;

            foreach ($arPositionId as $position_id) {
                self::deleteAll(['position_id' => $position_id]);

                foreach ($arRights as $permission => $right) {
                    $right = empty($right) ? 'off' : $right;

                    $insertValue[] = [$position_id, $permission, $right];
                }
            }

            if (!empty($insertValue)) {
                $connection->createCommand()->batchInsert(self::tableName(), ['position_id', 'permission', 'rights'],
                    $insertValue)->execute();

                return true;
            }
        }

        return false;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->addRight();
            }

            return true;
        }

        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        //Add permission to users
        if ($insert) {
            $this->assignRightToUsers();
        }
    }

    /**
     * Add right to db
     */
    private function addRight()
    {
        /** @var DbManager $auth */
        $auth = Yii::$app->authManager;

        if ($auth->isRightExist($this->permission)) {
            return false;
        }

        $permission              = $auth->createPermission($this->permission);
        $permission->description = ucfirst($this->permission);
        $auth->add($permission);

        $readPermission              = $auth->createPermission('read_' . $this->permission);
        $readPermission->description = $permission->description . ' (read)';
        $auth->add($readPermission);

        return $auth->addChild($permission, $readPermission);
    }

    /**
     * @return string
     */
    private function getPermissionName()
    {
        return $this->rights == 'read' ? 'read_' . $this->permission : $this->permission;
    }

    private function assignRightToUsers()
    {
        if ($this->rights !== 'off') {
            $permissionName = $this->getPermissionName();
            /** @var DbManager $auth */
            $auth      = Yii::$app->authManager;
            $userQuery = User::find()->select(['user_id', 'position_id'])->where(['position_id' => $this->position_id]);
            $roleObj   = $auth->getPermission($permissionName);
            /** @var User $user */
            foreach ($userQuery->each(10) as $user) {
                $auth->assign($roleObj, $user->user_id);
            }
        }
    }

    public static function getRightsMap()
    {
        return [
            'write' => 'Редактирование',
            'read'  => 'Чтение',
            'off'   => 'Выключено',
        ];
    }

    public static function getRightNameByKey($key)
    {
        return ArrayHelper::getValue(self::getRightsMap(), $key);
    }


}
