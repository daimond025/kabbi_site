<?php

namespace backend\modules\setting\models;

use Yii;
use yii\data\ActiveDataProvider;

class ElasticAccess extends \yii\elasticsearch\ActiveRecord
{
    const CLIENT_API_KEY_TENANT_SETTING = 'CLIENT_API_KEY';
    const WORKER_API_KEY_TENANT_SETTING = 'WORKER_API_KEY';
    const CLIENT_TYPE_APP = 'client';
    const WORKER_TYPE_APP = 'worker';

    public static function primaryKey()
    {
        return ['row_id'];
    }

    //public $index = 'access';

    public static function index()
    {
        return 'access';
    }

    public static function type()
    {
        return 'row';
    }

    /**
     * @return array the list of attributes for this record
     */
    public function attributes()
    {
        // path mapping for '_id' is setup to field 'id'
        return ['row_id', 'tenant_id', 'type_app', 'tenant_domain', 'api_key'];
    }

    public function rules()
    {
        return [
            ['row_id', 'required'],
            ['tenant_id', 'required'],
            ['type_app', 'required'],
            ['tenant_domain', 'required'],
            ['api_key', 'required']
        ];
    }

    public static function createKey($rowId, $tenantId, $tenantDomain, $typeApp, $apiKey)
    {
        $row = new ElasticAccess([
            'row_id'        => $rowId,
            'tenant_id'     => $tenantId,
            'tenant_domain' => $tenantDomain,
            'type_app'      => $typeApp,
            'api_key'       => $apiKey,
        ]);

        return $row->save();
    }

    /**
     * @param string $settingKey Setting name of api key from tenant setting table
     * @return null|string
     */
    public static function getTypeAppByTenantSetting($settingKey)
    {
        switch ($settingKey) {
            case self::CLIENT_API_KEY_TENANT_SETTING:
                $typeApp = self::CLIENT_TYPE_APP;
                break;
            case self::WORKER_API_KEY_TENANT_SETTING;
                $typeApp = self::WORKER_TYPE_APP;
                break;
            default:
                $typeApp = null;
        }

        return $typeApp;
    }

}
