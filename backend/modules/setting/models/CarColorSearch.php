<?php

namespace app\modules\setting\models;

use common\modules\car\models\CarColor;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CarColorSearch represents the model behind the search form about `frontend\modules\car\models\CarColor`.
 */
class CarColorSearch extends CarColor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['color_id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CarColor::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'color_id' => $this->color_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
