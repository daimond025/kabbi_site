<?php

namespace app\modules\setting\models;

use common\modules\tenant\models\mobileApp\MobileApp as BaseMobileApp;
use common\modules\tenant\models\mobileApp\MobileAppHasCity;
use yii\helpers\ArrayHelper;

class MobileApp extends BaseMobileApp
{

    public $citiesName = [];
    public $cities = [];

    public function attributeLabels()
    {
        return [
            'name'                => 'Название',
            'sort'                => 'Сортировка',
            'active'              => 'Активность',
            'api_key'             => 'Ключ апи',
            'push_key_android'    => 'Пуш для Android',
            'push_key_ios'        => 'Пуш для iOS',
            'link_android'        => 'Ссылка для Android',
            'link_ios'            => 'Ссылка для iOS',
            'cities'              => 'Филиалы',
            'page_info'           => 'О нас',
            'confidential'        => 'Политика конфиденцальности',
            'city_autocompletion' => 'Автодополение городов',
            'demo'                => 'Демо',
        ];
    }

    public function getCityList()
    {
        return implode(', ', $this->citiesName);
    }

    public function afterFind()
    {
        parent::afterFind(); // TODO: Change the autogenerated stub

        $this->citiesName = ArrayHelper::map($this->hasCity, 'city_id', 'city.name');
        $this->cities     = array_keys($this->citiesName);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub

        $cities = ArrayHelper::getColumn($this->hasCity, 'city_id');
        $this->cities = is_array($this->cities) ? $this->cities : [];

        $delete = array_diff($cities, $this->cities);
        $add    = array_diff($this->cities, $cities);

        $this->deleteCities($delete);
        $this->addCities($add);
    }

    private function deleteCities($cities)
    {
        foreach ($this->hasCity as $city) {
            if (in_array($city->city_id, $cities, true)) {
                $city->delete();
            }
        }
    }

    private function addCities($cities = [])
    {
        foreach ($cities as $city) {
            $model             = new MobileAppHasCity();
            $model->attributes = [
                'app_id'  => $this->app_id,
                'city_id' => $city,
            ];
            $model->save();
        }
    }
}