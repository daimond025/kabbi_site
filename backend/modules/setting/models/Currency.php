<?php

namespace app\modules\setting\models;

use Yii;
use backend\modules\setting\models\CurrencyType;

/**
 * This is the model class for table "{{%currency}}".
 *
 */
class Currency extends \yii\db\ActiveRecord
{

    
    const SOURCE_CATEGORY = 'currency';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%currency}}';
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code', 'numeric_code'], 'unique'],
            [['name', 'code', 'symbol', 'numeric_code', 'minor_unit', 'type_id'], 'required'],
            [['name', 'symbol','code'], 'string', 'max' => 255],
            ['numeric_code', 'integer', 'max' => 999],
            ['minor_unit', 'integer', 'max' => 10],
            ['type_id', 'in', 'range' => array_keys( CurrencyType::nameList() ) ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'currency_id' => 'ID',
            'type_id' => 'Тип',
            'name' => 'Название',
            'code' => 'Трёхбуквенное обозначение',
            'symbol' => 'Аббревиатура',
            'numeric_code' => 'Код',
            'minor_unit' => 'Знаков после запятой',
        ];
    }
    
    public static function getCurrencySymbol($id)
    {
        $model = self::findOne($id);
        
        return $model->symbol;
    }
    
    public function getCurrencyType()
    {
        return $this->hasOne(CurrencyType::className(), ['type_id' => 'type_id']);
    }
    
    
    public function typeList ()
    {
        return CurrencyType::nameList();
    }
}
