<?php

namespace app\modules\setting\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\setting\models\City;
use app\modules\setting\models\Country;
use app\modules\setting\models\Republic;

/**
 * CitySearch represents the model behind the search form about `app\modules\setting\models\City`.
 */
class CitySearch extends City
{

    public $has_polygon;
    public $country_name;
    public $republic_name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'republic_id'], 'integer'],
            [['search'], 'boolean'],
            [
                [
                    'name',
                    'shortname',
                    'lat',
                    'lon',
                    'country_name',
                    'republic_name',
                    'fulladdress',
                    'has_polygon',
                    'fulladdress_reverse',
                    'sort',
                    'name_az',
                    'name_en',
                    'name_de',
                    'name_sr',
                    'fulladdress_az',
                    'fulladdress_en',
                    'fulladdress_de',
                    'fulladdress_sr',
                ],
                'safe',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()->alias('c')->joinWith(['republic.country as rc'])->joinWith(['republic as r']);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if(isset($this->has_polygon)) {

            if ($this->has_polygon == 0) {
                $query->andFilterWhere(['in', 'city_polygon', null]);
                $query->orWhere(['city_polygon' => null]);
            } elseif ($this->has_polygon == 1) {
                $query->andFilterWhere(['!=', 'city_polygon', null]);
                $query->andWhere(['!=', 'city_polygon', '']);
            }

        }

        if (!empty($this->country_name)) {
            $query->andWhere(['like', 'rc.name', $this->country_name]);
        }

        if (!empty($this->republic_name)) {
            $query->andWhere(['like', 'r.name', $this->republic_name]);
        }

        $query->andFilterWhere([
            'city_id'     => $this->city_id,
            'republic_id' => $this->republic_id,
        ]);
        
        $query->andFilterWhere(['like', 'c.name', $this->name])
            ->andFilterWhere(['like', 'c.shortname', $this->shortname])
            ->andFilterWhere(['like', 'lat', $this->lat])
            ->andFilterWhere(['like', 'lon', $this->lon])
            ->andFilterWhere(['like', 'fulladdress', $this->fulladdress])
            ->andFilterWhere(['like', 'fulladdress_reverse', $this->fulladdress_reverse])
            ->andFilterWhere(['like', 'sort', $this->sort])
            ->andFilterWhere(['like', 'name_az', $this->name_az])
            ->andFilterWhere(['like', 'name_en', $this->name_en])
            ->andFilterWhere(['like', 'name_de', $this->name_de])
            ->andFilterWhere(['like', 'name_sr', $this->name_sr])
            ->andFilterWhere(['like', 'fulladdress_az', $this->fulladdress_az])
            ->andFilterWhere(['like', 'fulladdress_en', $this->fulladdress_en])
            ->andFilterWhere(['like', 'fulladdress_de', $this->fulladdress_de])
            ->andFilterWhere(['like', 'fulladdress_sr', $this->fulladdress_sr])
            ->andFilterWhere(['like', 'search', $this->search]);

        return $dataProvider;
    }

    public function getPolygonValues()
    {
        if (empty($this->city_polygon)) {
            return 'Нет';
        } else {
            return 'Да';
        }
    }

}
