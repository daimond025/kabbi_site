<?php

namespace app\modules\setting\models;

/**
 * This is the model class for table "tbl_admin_one_time_job".
 *
 * @property integer $job_id
 * @property string $name
 * @property integer $price
 * @property integer $active
 * @property string $create_date
 */
class AdminOneTimeJob extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%admin_one_time_job}}';
    }
    
    public function behaviors() {
        return [
            [
                'class'              => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => 'create_date',
                'updatedAtAttribute' => 'create_date',
                'value'              => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'price'], 'required'],
            [['price', 'active'], 'integer'],
            [['create_date'], 'safe'],
            [['name'], 'string', 'max' => 48]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'job_id'      => 'ID услуги',
            'name'        => 'Название',
            'price'       => 'Цена',
            'active'      => 'Активна?',
            'create_date' => 'Дата добавления/изменения',
        ];
    }
    
}
