<?php

namespace app\modules\setting\models;

use common\modules\tenant\models\DefaultSettings;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class DefaultSettingsSearch
 * @package app\modules\setting\models
 */
class DefaultSettingsSearch extends DefaultSettings
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'value', 'type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DefaultSettings::find();

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'value', $this->value])
            ->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}