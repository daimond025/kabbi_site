<?php

namespace app\modules\setting\models;

use common\modules\car\models\CarBrand;
use common\modules\car\models\CarModel;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * CarModelSearch represents the model behind the search form about `common\modules\car\models\CarModel`.
 */
class CarModelSearch extends CarModel
{

    public $typeName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['model_id'], 'integer'],
            [['name', 'brand_id', 'typeName'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = parent::find();
        $query->joinWith(['type t']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'model_id',
                'brand_id',
                'name',
                'typeName' => [
                    'asc'  => ['t.name' => SORT_ASC],
                    'desc' => ['t.name' => SORT_DESC],
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if (!empty($this->brand_id)) {

            $brand_id = CarBrand::find()->where('name like "%' . $this->brand_id . '%"')->one()->brand_id;
            $query->where(['brand_id' => $brand_id]);
        }
        $query->andFilterWhere([
            'model_id' => $this->model_id,
        ]);
        $query->andFilterWhere(['like', parent::tableName() . '.name', $this->name]);
        $query->joinWith([
            'type t' => function ($q) {
                return $q->andFilterWhere(['t.name' => $this->typeName]);
            },
        ]);

        return $dataProvider;
    }

}
