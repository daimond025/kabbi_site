<?php

namespace app\modules\setting\models;

use Yii;

/**
 * This is the model class for table "{{%order_change_data}}".
 *
 * @property integer $change_id
 * @property integer $tenant_id
 * @property integer $order_id
 * @property string $change_field
 * @property string $change_object_id
 * @property string $change_object_type
 * @property string $change_subject
 * @property string $change_val
 * @property string $change_time
 *
 * @property Order $order
 * @property Tenant $tenant
 */
class OrderChangeData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_change_data}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'order_id'], 'required'],
            [['tenant_id', 'order_id'], 'integer'],
            [['change_field', 'change_object_id', 'change_object_type'], 'string', 'max' => 45],
            [['change_subject'], 'string', 'max' => 100],
            [['change_val'], 'string', 'max' => 300]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'change_id' => 'Change ID',
            'tenant_id' => 'Tenant ID',
            'order_id' => 'Order ID',
            'change_field' => 'Change Field',
            'change_object_id' => 'Change Object ID',
            'change_object_type' => 'Change Object Type',
            'change_subject' => 'Change Subject',
            'change_val' => 'Change Val',
            'change_time' => 'Change Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['order_id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(\common\modules\tenant\models\Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * Allow to log user order update.
     * @param integer $order_id
     * @param integer $tenant_id
     * @param integer $user_id
     * @param array $arChangeField Array of order change field.
     * @return boolean
     */
    public static function userLog($order_id, $tenant_id, $user_id, $arChangeField)
    {
        if(is_array($arChangeField) && !empty($arChangeField))
        {
            $insertValue = [];
            $connection = app()->db;

            foreach($arChangeField as $field => $value)
            {
                $insertValue[] = [$tenant_id, $order_id, $field, $order_id, 'order', "disputcher_$user_id", $value, time()];
            }

            if(!empty($insertValue))
            {
                $arFields = [
                    'tenant_id',
                    'order_id',
                    'change_field',
                    'change_object_id',
                    'change_object_type',
                    'change_subject',
                    'change_val',
                    'change_time'
                ];

                $connection->createCommand()->batchInsert(self::tableName(), $arFields, $insertValue)->execute();

                return true;
            }
        }

        return false;
    }
}
