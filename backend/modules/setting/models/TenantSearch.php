<?php

namespace app\modules\setting\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\setting\models\Tenant;

/**
 * TenantSearch represents the model behind the search form about `app\modules\setting\models\Tenant`.
 */
class TenantSearch extends Tenant
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id'], 'integer'],
            [['utm_source','utm_medium','utm_campaign','utm_content','utm_term','client_id_go','client_id_ya','ip','company_name', 'full_company_name', 'legal_address', 'post_address', 'contact_name', 'contact_second_name', 'contact_last_name', 'contact_phone', 'contact_email', 'company_phone', 'domain', 'email', 'inn', 'bookkeeper', 'kpp', 'ogrn', 'site', 'archive', 'logo', 'director', 'director_position', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tenant::find();


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'tenant_id' => $this->tenant_id,
            'archive' => $this->archive,
        ]);

        $query->andFilterWhere(['like', 'company_name', $this->company_name])
            ->andFilterWhere(['like', 'full_company_name', $this->full_company_name])
            ->andFilterWhere(['like', 'legal_address', $this->legal_address])
            ->andFilterWhere(['like', 'post_address', $this->post_address])
            ->andFilterWhere(['like', 'contact_name', $this->contact_name])
            ->andFilterWhere(['like', 'contact_second_name', $this->contact_second_name])
            ->andFilterWhere(['like', 'contact_last_name', $this->contact_last_name])
            ->andFilterWhere(['like', 'contact_phone', $this->contact_phone])
            ->andFilterWhere(['like', 'contact_email', $this->contact_email])
            ->andFilterWhere(['like', 'company_phone', $this->company_phone])
            ->andFilterWhere(['like', 'domain', $this->domain])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'inn', $this->inn])
            ->andFilterWhere(['like', 'bookkeeper', $this->bookkeeper])
            ->andFilterWhere(['like', 'kpp', $this->kpp])
            ->andFilterWhere(['like', 'ogrn', $this->ogrn])
            ->andFilterWhere(['like', 'site', $this->site])
            ->andFilterWhere(['like', 'logo', $this->logo])
            ->andFilterWhere(['like', 'director', $this->director])
            ->andFilterWhere(['like', 'director_position', $this->director_position])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'utm_source', $this->utm_source])
            ->andFilterWhere(['like', 'utm_medium', $this->utm_medium])
            ->andFilterWhere(['like', 'utm_campaign', $this->utm_campaign])
            ->andFilterWhere(['like', 'utm_content', $this->utm_content])
            ->andFilterWhere(['like', 'utm_term', $this->utm_term])
            ->andFilterWhere(['like', 'client_id_go', $this->client_id_go])
            ->andFilterWhere(['like', 'client_id_ya', $this->client_id_ya])
            ->andFilterWhere(['like', 'ip', $this->ip]);


        return $dataProvider;
    }


}
