<?php

namespace backend\modules\setting\models\search;


use backend\modules\setting\models\entities\UserExtrinsicDispatcher;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class UserExtrinsicDispatcherSearch extends UserExtrinsicDispatcher
{

    protected $fullName;

    public function setFullName($value)
    {
        $this->fullName = $value;
    }

    public function rules()
    {
        return [
            [['email'], 'string'],
            ['active', 'in', 'range' => [1, 0]],
            [['address'], 'string', 'max' => 255],
            [['fullName', 'create_time'], 'string', 'max' => 45],
            [['phone'], 'string', 'max' => 15],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'fullName' => [
                    'asc' => ['name' => SORT_ASC, 'last_name' => SORT_ASC],
                    'desc' => ['name' => SORT_DESC, 'last_name' => SORT_DESC],
                    'label' => 'Full Name',
                    'default' => SORT_ASC
                ],
                'create_time',
                'active',
                'email',
                'user_id',
            ]
        ]);


        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'active' => $this->active
        ]);

        $query->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'create_time', $this->create_time]);

        $query->andWhere('name LIKE "%' . $this->fullName . '%" '
            . 'OR last_name LIKE "%' . $this->fullName . '%"'
            . 'OR second_name LIKE "%' . $this->fullName . '%"'
        );


        return $dataProvider;
    }
}