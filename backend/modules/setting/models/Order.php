<?php

namespace app\modules\setting\models;

use common\modules\employee\models\worker\Worker;
use common\modules\car\models\Car;


class Order extends \yii\db\ActiveRecord
{


    const MAP_DATA_TYPE_ADDRESS = 'address';

    const MAP_DATA_TYPE_DRIVER = 'driver';

    const MAP_DATA_TYPE_ROUTE = 'route';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_number', 'tenant_id', 'create_time', 'status_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_number' => 'Номер заказа',
            'tenant_id'    => 'Домен',
            'create_time'  => 'Время создания',
            'status_id'    => 'Статус',
            'worker_id'    => 'Исполнитель',
            'city_id'      => 'Город',
            'tariff_id'    => 'Тариф',
            'comment'      => 'Пожелания',
            'predv_price'  => 'Предварительный расчет',
            'payment'      => 'Оплата',
            'order_time'   => 'Время заказа',
            'client_id'    => 'Клиент',
            'user_create'  => 'Кем создан',
            'car_id'       => 'Автомобиль',

        ];
    }

    /**
     * @return string;
     */
    public function getDomain()
    {
        $model = Tenant::findOne($this->tenant_id);

        if ($model) {
            return $model->domain;
        }

        return null;
    }

    /**
     * @return string;
     */
    public function getWorkerName()
    {
        $model = Worker::findOne($this->worker_id);

        if ($model) {
            return $model->getFullName();
        }

        return null;
    }


    /**
     * @return string;
     */
    public function getTaxiTariff()
    {
        $model = TaxiTariff::findOne($this->tariff_id);

        if ($model) {
            return $model->name;
        }

        return null;
    }

    /**
     * @return string
     */
    public function getPrice()
    {

        $model = Currency::findOne($this->currency_id);

        if ($model) {
            $this->predv_price .= ' ' . $model->symbol;
        }

        return $this->predv_price;
    }


    /**
     * @return integer;
     */
    public function getIdByDomain()
    {
        $model = Tenant::findOne(['domain' => $this->tenant_id]);

        return $model->tenant_id;
    }

    /**
     * @return string;
     */
    public function getStatus()
    {
        $model = OrderStatus::findOne($this->status_id);

        return $model->name;
    }

    /**
     * @return string;
     */
    public function getClientName()
    {
        $model = Client::findOne($this->client_id);

        if ($model) {
            return $model->getName();
        }

        return null;
    }

    public function getCar()
    {
        return $this->hasOne(Car::className(), ['car_id' => 'car_id']);
    }


    public function getAddressNotA()
    {
        $track = [];
        $address = unserialize($this->address);
        unset($address['A']);
        if (count($address) == 0) {
            return null;
        }

        foreach ($address as $key => $item) {
            $track[] = $this->getAddressByKey($key);
        }

        return implode('<br/>', $track);
    }

    public function getAddressByKey($key)
    {
        $track = [];
        $address = unserialize($this->address);
        if (!isset($address[$key])) {
            return null;
        }

        $span = '<span class="badge">' . $key . '</span> ';
        $track[] = 'Город ' . $address[$key]['city'];
        $track[] = $address[$key]['street'];
        $track[] = 'Дом ' . $address[$key]['house'];
        if (!empty($address[$key]['housing'])) {
            $track[] = 'Стр/корп. ' . $address[$key]['housing'];
        }
        if (!empty($address[$key]['porch'])) {
            $track[] = 'Подъезд ' . $address[$key]['porch'];
        }
        if (!empty($address[$key]['apt'])) {
            $track[] = 'Квартира ' . $address[$key]['apt'];
        }
        if (!empty($address[$key]['parking'])) {
            $track[] = 'Парковка: ' . $address[$key]['parking'];
        }


        return $span . implode(', ', $track);
    }


    public function getOrderOffset()
    {
        return City::getTimeOffset($this->city_id);
    }


    /**
     * Getting order address
     * @return array
     */
    public static function getOrderAddress($orderId)
    {
        return $order = Order::findone($orderId)->address;
    }


}
