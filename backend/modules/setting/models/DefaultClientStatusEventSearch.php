<?php

namespace app\modules\setting\models;

use common\modules\tenant\models\DefaultClientStatusEvent;
use yii\data\ActiveDataProvider;

/**
 * Class DefaultClientStatusEventSearch
 * @package app\modules\setting\models
 */
class DefaultClientStatusEventSearch extends DefaultClientStatusEvent
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'status_id', 'position_id'], 'integer'],
            [['group', 'notice'], 'safe'],
        ];
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DefaultClientStatusEvent::find();

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'event_id'    => $this->event_id,
            'status_id'   => $this->status_id,
            'position_id' => $this->position_id,
        ]);

        $query->andFilterWhere(['like', 'group', $this->group])
            ->andFilterWhere(['like', 'notice', $this->notice]);

        return $dataProvider;
    }
}
