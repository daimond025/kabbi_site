<?php

namespace app\modules\setting\models;

use DateTime;
use DateInterval;

/**
 * This is the model class for table "tbl_tenant_payment".
 *
 * @property integer $payment_id
 * @property integer $amount
 * @property integer $tenant_id
 * @property string $create_date
 * @property integer $period
 * @property string $payment_item
 * @property string $expiration_date
 * @property string $payment_activity_begin_date
 */
class TenantPayment extends \yii\db\ActiveRecord {

    public function behaviors() {
        return [
            [
                'class'              => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => 'create_date',
                'updatedAtAttribute' => 'create_date',
                'value'              => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%tenant_payment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['amount', 'tenant_id', 'period'], 'integer'],
            [['create_date', 'expiration_date', 'payment_activity_begin_date'], 'safe'],
            [['payment_item'], 'string', 'max' => 48]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'payment_id'                  => 'ID платежа',
            'amount'                      => 'Сумма',
            'tenant_id'                   => 'ID арендатора',
            'create_date'                 => 'Дата платежа',
            'period'                      => 'Период (в мес)',
            'payment_item'                => 'Назначение платежа',
            'payment_activity_begin_date' => 'Вступление платежа в силу',
            'expiration_date'             => 'Дата окончания действия платежа',
        ];
    }

    public static function getTotalCosts($dataProvider) {
        $sum = 0;
        $data = $dataProvider->getModels();
        foreach ($data as $rows) {
            $sum += $rows['amount'];
        }
        return $sum;
    }

    /**
     * 
     * @param dateTime $createDate
     * @param integer $period
     * @return dateTime
     * returns expiration date which computes by formula: date_create + period * month length
     */
    public function setExpirationDate($beginperiod, $period) {
        $createDate = DateTime::createFromFormat("Y-m-d", $date = !empty(post()['TenantPayment']['create_date']) ?
                        post()['TenantPayment']['create_date'] : $beginperiod);
        $interval = new DateInterval('P' . $period . 'M');
        $createDate->add($interval);
        return date_format($createDate, "Y-m-d") . ' 00:00:00';
    }

}
