<?php
namespace app\modules\setting\models;

use yii\data\ActiveDataProvider;

class MobileAppSearch extends MobileApp
{

    public function rules()
    {
        return [
            [['tenant_id'], 'safe'],
        ];
    }

    public function search($params)
    {

        $query = self::find();
        $query->orderBy('sort');

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'sort' => false,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        $this->load($params);
        if (isset($params['tenant_id'])) {
            $this->tenant_id = $params['tenant_id'];
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->andFilterWhere([
            'tenant_id' => $this->tenant_id,
        ]);

        return $dataProvider;
    }
}