<?php

namespace backend\modules\setting\models\query;


use backend\modules\setting\models\entities\UserExtrinsicDispatcher;
use yii\db\ActiveQuery;

class ExtrinsicDispatcherQuery extends ActiveQuery
{
    public function init()
    {
        parent::init();
        $this->andwhere(['position_id' => UserExtrinsicDispatcher::POSITION_EXTRINSIC_DISPATCHER]);

    }
}