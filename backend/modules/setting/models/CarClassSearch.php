<?php

namespace app\modules\setting\models;

use common\modules\car\models\CarClass;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use common\modules\car\models\transport\TransportType;

/**
 * CarColorSearch represents the model behind the search form about `frontend\modules\car\models\CarColor`.
 */
class CarClassSearch extends CarClass
{


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['class_id','class','sort','type_id'],'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()
            ->alias('c')
            ->innerJoinWith('type t');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'c.class_id' => $this->class_id,
            'c.class' => $this->class,
            'c.sort' => $this->sort,
            'c.type_id' => $this->type_id,
        ]);

        return $dataProvider;
    }

    public function getTransportType(){
        return ArrayHelper::map(TransportType::find()->all(), 'type_id', 'name');
    }

}
