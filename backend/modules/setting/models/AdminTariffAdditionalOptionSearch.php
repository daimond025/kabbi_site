<?php

namespace app\modules\setting\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\setting\models\AdminTariffAdditionalOption;

/**
 * AdminTariffAdditionalOptionSearch represents the model behind the search form about `app\modules\setting\models\AdminTariffAdditionalOption`.
 */
class AdminTariffAdditionalOptionSearch extends AdminTariffAdditionalOption
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['option_id', 'price', 'active'], 'integer'],
            [['name', 'create_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AdminTariffAdditionalOption::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'option_id' => $this->option_id,
            'price' => $this->price,
            'active' => $this->active,
            'create_date' => $this->create_date,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
