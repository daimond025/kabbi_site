<?php

namespace app\modules\setting\models;

use Yii;

/**
 * This is the model class for table "tbl_city".
 *
 * @property integer               $city_id
 * @property integer               $republic_id
 * @property string                $name
 * @property string                $shortname
 * @property string                $lat
 * @property string                $lon
 * @property string                $fulladdress
 * @property string                $fulladdress_reverse
 * @property string                $sort
 * @property string                $city_polygon
 * @property string                $name_az
 * @property string                $name_en
 * @property string                $name_de
 * @property string                $name_sr
 * @property string                $name_uz
 * @property string                $name_fi
 * @property string                $name_fa
 * @property string                $name_bg
 * @property string                $fulladdress_az
 * @property string                $fulladdress_en
 * @property string                $fulladdress_de
 * @property string                $fulladdress_sr
 * @property string                $fulladdress_uz
 * @property string                $fulladdress_fi
 * @property string                $fulladdress_fa
 * @property string                $fulladdress_bg
 *
 * @property ClientBonus[]         $clientBonuses
 * @property ClientCompany[]       $clientCompanies
 * @property OrderViews[]          $orderViews
 * @property PhoneLine[]           $phoneLines
 * @property TenantHasCity[]       $tenantHasCities
 * @property Tenant[]              $tenants
 * @property Transaction[]         $transactions
 */
class City extends \yii\db\ActiveRecord
{

    private static $_timeOffset = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['search'], 'boolean'],
            [['republic_id', 'name'], 'required'],
            [['republic_id'], 'integer'],
            [['city_polygon'], 'string'],
            [
                [
                    'name',
                    'shortname',
                    'lat',
                    'lon',
                    'name_az',
                    'name_en',
                    'name_de',
                    'name_sr',
                    'name_ro',
                    'name_uz',
                    'name_fi',
                    'name_fa',
                    'name_bg',
                    'fulladdress_az',
                    'fulladdress_en',
                    'fulladdress_de',
                    'fulladdress_sr',
                    'fulladdress_ro',
                    'fulladdress_uz',
                    'fulladdress_fi',
                    'fulladdress_fa',
                    'fulladdress_bg',

                ],
                'string',
                'max' => 100,
            ],
            [['fulladdress', 'fulladdress_reverse'], 'string', 'max' => 200],
            [['sort'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'city_id'             => 'ID',
            'republic_id'         => 'Республика',
            'name'                => 'Название',
            'shortname'           => 'Обозначение',
            'lat'                 => 'Широта',
            'search'              => 'Включить в поиск',
            'lon'                 => 'Долгота',
            'fulladdress'         => 'Полный адрес',
            'fulladdress_reverse' => 'Полный адрес (инверсия)',
            'sort'                => 'Сортировка',
            'city_polygon'        => 'Полигон',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepublic()
    {
        return $this->hasOne(Republic::className(), ['republic_id' => 'republic_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientCompanies()
    {
        return $this->hasMany(ClientCompany::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderViews()
    {
        return $this->hasMany(OrderViews::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoneLines()
    {
        return $this->hasMany(PhoneLine::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantHasCities()
    {
        return $this->hasMany(TenantHasCity::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenants()
    {
        return $this->hasMany(Tenant::className(), ['tenant_id' => 'tenant_id'])->viaTable('tbl_tenant_has_city',
            ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(Transaction::className(), ['city_id' => 'city_id']);
    }


    public static function getTimeOffset($city_id)
    {
        if (!isset(self::$_timeOffset[$city_id])) {
            $city = self::find()
                ->where(['city_id' => $city_id])
                ->with([
                    'republic' => function ($query) {
                        $query->select([
                            'republic_id',
                            'timezone',
                        ]);
                    },
                ])
                ->one();

            self::$_timeOffset[$city_id] = !empty($city) && !empty($city->republic->timezone) ? $city->republic->timezone * 3600 : 0;
        }

        return self::$_timeOffset[$city_id];
    }
}
