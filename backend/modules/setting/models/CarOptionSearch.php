<?php

namespace app\modules\setting\models;

use common\modules\car\models\transport\TransportType;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\car\models\CarOption;
use yii\helpers\ArrayHelper;

/**
 * CarOptionSearch represents the model behind the search form about `common\modules\car\models\CarOption`.
 */
class CarOptionSearch extends CarOption
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['option_id', 'type_id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->with('type')->andFilterWhere([
            'option_id' => $this->option_id,
            'type_id' => $this->type_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

    public function getTransportType()
    {
        return ArrayHelper::map(TransportType::find()->all(), 'type_id', 'name');
    }

}
