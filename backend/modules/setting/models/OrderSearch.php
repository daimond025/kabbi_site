<?php

namespace backend\modules\setting\models;

use app\modules\setting\models\Order;
use yii\data\ActiveDataProvider;

class OrderSearch extends Order
{
    public function rules()
    {
        return [
            [['order_id','order_number', 'tenant_id'], 'safe'],
        ];
    }

    public function search($params)
    {
        $query = Order::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'defaultOrder' => [
                    'order_number' => SORT_DESC,
                ],
            ],

        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($params['start']) && !empty($params['end'])) {
            $start = strtotime($params['start']);
            $end = strtotime($params['end']);

            $query->andWhere([
                'between',
                'create_time',
                strtotime(date('d.m.Y', $start)),
                strtotime(date('d.m.Y ', $end) . ' +1day - 1sec'),
            ]);
        }

        $query->andFilterWhere([
            'order_number' => $this->order_number,
            'tenant_id'    => $this->getIdByDomain(),
            'order_id'     => $this->order_id,
        ]);

        return $dataProvider;
    }
}