<?php

namespace backend\modules\setting\models\email;

use yii\db\ActiveRecord;

/**
 * Class DefaultEmailSetting
 * @package frontend\modules\tenant\models\email
 *
 * @property integer $setting_id
 * @property string  $sender_name
 * @property string  $sender_email
 * @property string  $sender_password
 * @property string $provider_server
 * @property integer $provider_port
 * @property string  $template
 * @property string  $type
 *
 */
class DefaultEmailSetting extends ActiveRecord
{
    const PARAMS_SEPARATOR = ';';

    const TYPE_SYSTEM = 'system';
    const TYPE_TENANT = 'tenant';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%default_email_settings}}';
    }
}