<?php

namespace app\modules\setting\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\setting\models\TenantPayment;

/**
 * TenantPaymentSearch represents the model behind the search form about `app\modules\setting\models\TenantPayment`.
 */
class TenantPaymentSearch extends TenantPayment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_id', 'amount', 'tenant_id', 'period'], 'integer'],
            [['create_date', 'payment_item', 'expiration_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TenantPayment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'payment_id' => $this->payment_id,
            'amount' => $this->amount,
            'tenant_id' => $this->tenant_id,
            'create_date' => $this->create_date,
            'period' => $this->period,
            'expiration_date' => $this->expiration_date,
        ]);

        $query->andFilterWhere(['like', 'payment_item', $this->payment_item]);

        return $dataProvider;
    }
}
