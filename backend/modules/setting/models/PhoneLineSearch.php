<?php

namespace app\modules\setting\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * OrderStatusSearch represents the model behind the search form about `app\modules\setting\models\OrderStatus`.
 */
class PhoneLineSearch extends PhoneLine
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['domain','line_id','tenant_id','tariff_id','city_id','phone','block'],'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()->alias('p');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'line_id',
                    'tenant_id',
                    'phone',
                    'block',
                    'city_id',
                    'tariff_id',
                    'domain',
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['p.line_id' => $this->line_id])
            ->andFilterWhere(['p.tenant_id' => $this->tenant_id])
            ->andFilterWhere(['like','tt.name',$this->tariff_id])->joinWith('tariff tt')
            ->andFilterWhere(['like','c.name',$this->city_id])->joinWith('city c')
            ->andFilterWhere(['like','p.phone',$this->phone])
            ->andFilterWhere(['like','p.block',$this->block])
            ->andFilterWhere(['like','t.domain',$this->domain])->joinWith('tenant t');

        return $dataProvider;
    }

    public function getBlock(){
        return ArrayHelper::map(self::find()->all(),'block','block');
    }
}
