<?php

namespace app\modules\setting\models;

use common\modules\car\models\Car;
use common\modules\car\models\CarOption;
use common\modules\employee\models\worker\Worker;
use common\modules\employee\models\worker\WorkerTariff;
use common\modules\tenant\models\Tenant as FrontendTenant;
use frontend\modules\tenant\models\UserDispetcher;
use backend\modules\setting\models\ElasticAccess;

/**
 * This is the model class for table "tbl_tenant".
 *
 * @property integer $tenant_id
 * @property string $company_name
 * @property string $full_company_name
 * @property string $legal_address
 * @property string $post_address
 * @property string $contact_name
 * @property string $contact_second_name
 * @property string $contact_last_name
 * @property string $contact_phone
 * @property string $contact_email
 * @property string $company_phone
 * @property string $domain
 * @property string $email
 * @property string $inn
 * @property string $create_time
 * @property string $bookkeeper
 * @property string $kpp
 * @property string $ogrn
 * @property string $site
 * @property string $archive
 * @property string $logo
 * @property string $director
 * @property string $director_position
 * @property string $status
 *
 * @property AutocallFile[] $autocallFiles
 * @property Car[] $cars
 * @property Client[] $clients
 * @property ClientCompany[] $clientCompanies
 * @property ClientStatusEvent[] $clientStatusEvents
 * @property MessageCity[] $messageCities
 * @property OrderViews[] $orderViews
 * @property Parking[] $parkings
 * @property PaymentTnDr[] $paymentTnDrs
 * @property PaymentTnSt[] $paymentTnSts
 * @property PaymentTnTn[] $paymentTnTns
 * @property PhoneLine[] $phoneLines
 * @property SmsTemplate[] $smsTemplates
 * @property Support[] $supports
 * @property TaxiTariff[] $taxiTariffs
 * @property TenantBalance[] $tenantBalances
 * @property TenantHasCarOption[] $tenantHasCarOptions
 * @property CarOption[] $options
 * @property TenantHasCity[] $tenantHasCities
 * @property City[] $cities
 * @property TenantHasSms[] $tenantHasSms
 * @property TenantSetting[] $tenantSettings
 * @property TenantTariff[] $tenantTariffs
 * @property User[] $users
 */
class Tenant extends \yii\db\ActiveRecord
{
    public $logoLink='';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_tenant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contact_phone', 'domain', 'email'], 'required'],
            [['create_time', 'archive'], 'safe'],
            [['status'], 'string'],
            [['company_name', 'full_company_name', 'legal_address', 'post_address', 'logo'], 'string', 'max' => 255],
            [['contact_name', 'contact_second_name', 'contact_last_name', 'contact_email', 'email', 'bookkeeper', 'director_position'], 'string', 'max' => 45],
            [['contact_phone', 'company_phone', 'inn', 'domain'], 'string', 'max' => 15],
            ['kpp', 'string', 'max' => 10],
            [['ogrn', 'site'], 'string', 'max' => 20],
            [['director'], 'string', 'max' => 50],
            [['domain'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'contact_full_name' => 'ФИО директора',
            'tenant_id' => 'ID арендатора',
            'company_name' => 'Название службы такси',
            'full_company_name' => 'Полное наименование организации',
            'legal_address' => 'Юридический адрес',
            'post_address' => 'Почтовый фактический адрес',
            'contact_name' => 'Имя',
            'contact_second_name' => 'Отчество',
            'contact_last_name' => 'Фамилия',
            'contact_phone' => 'Телефон контактного лица',
            'contact_email' => 'Эл. почта контактного лица',
            'company_phone' => 'Телефон организации',
            'email' => 'Эл. почта организации',
            'site' => 'Сайт организации',
            'domain' => 'Домен',
            'inn' => 'ИНН',
            'create_time' => 'Время/дата регистрации',
            'bookkeeper' => 'ФИО бухгалтера',
            'kpp' => 'КПП',
            'ogrn' => 'ОГРН или ОГРНИП',
            'archive' => 'Архив',
            'logo' => 'Логотип',
            'director' => 'ФИО директора',
            'director_position' => 'Должность директора',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutocallFiles()
    {
        return $this->hasMany(AutocallFile::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasMany(Client::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientCompanies()
    {
        return $this->hasMany(ClientCompany::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientStatusEvents()
    {
        return $this->hasMany(ClientStatusEvent::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessageCities()
    {
        return $this->hasMany(MessageCity::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderViews()
    {
        return $this->hasMany(OrderViews::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParkings()
    {
        return $this->hasMany(Parking::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentTnDrs()
    {
        return $this->hasMany(PaymentTnDr::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentTnSts()
    {
        return $this->hasMany(PaymentTnSt::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentTnTns()
    {
        return $this->hasMany(PaymentTnTn::className(), ['recipient' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoneLines()
    {
        return $this->hasMany(PhoneLine::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSmsTemplates()
    {
        return $this->hasMany(SmsTemplate::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupports()
    {
        return $this->hasMany(Support::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxiTariffs()
    {
        return $this->hasMany(TaxiTariff::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantBalances()
    {
        return $this->hasMany(TenantBalance::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantHasCarOptions()
    {
        return $this->hasMany(TenantHasCarOption::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOptions()
    {
        return $this->hasMany(CarOption::className(), ['option_id' => 'option_id'])->viaTable('tbl_tenant_has_car_option', ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantHasCities()
    {
        return $this->hasMany(TenantHasCity::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['city_id' => 'city_id'])->viaTable('tbl_tenant_has_city', ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantHasSms()
    {
        return $this->hasMany(TenantHasSms::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantSettings()
    {
        return $this->hasMany(TenantSetting::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantTariffs()
    {
        return $this->hasMany(TenantTariff::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['tenant_id' => 'tenant_id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (array_key_exists('domain', $changedAttributes)) {
            $elasticRows = ElasticAccess::findAll(['tenant_id' => $this->tenant_id]);

            foreach ($elasticRows as $row) {
                $row->tenant_domain = $this->domain;
                $row->save(false, ['tenant_domain']);
            }
        }
    }


    public function afterDelete()
    {
        parent::afterDelete();

        $db = app()->kamailio;
        $domain = FrontendTenant::getTenantHostInPhoneBase($this->domain);
        $db->createCommand()->delete('domain', "domain = '$domain'")->execute();
        $db->createCommand()->delete(UserDispetcher::KAMAILIO_USER_TABLE, "domain = '$domain'")->execute();
        $db->createCommand()->delete(UserDispetcher::KAMAILIO_USER_GROUP_TABLE, "domain = '$domain'")->execute();

        ElasticAccess::deleteAll(['tenant_id' => $this->tenant_id]);
    }
}
