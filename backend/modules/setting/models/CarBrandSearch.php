<?php

namespace app\modules\setting\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\car\models\CarBrand;

/**
 * CarBrandSearch represents the model behind the search form about `common\modules\car\models\CarBrand`.
 */
class CarBrandSearch extends \common\modules\car\models\CarBrand {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['brand_id'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {

        $query = (new \yii\db\Query())
                ->from([\common\modules\car\models\CarBrand::tableName()]);
        if (($this->load($params) && $this->validate())) {

            $query->filterWhere(['brand_id' => $this->brand_id, 'name' => $this->name]);
        }

        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $query->all(),
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
        return $dataProvider;
    }

}
