<?php

namespace app\modules\setting\models;

use Yii;

/**
 * This is the model class for table "{{%sms_server}}".
 *
 * @property integer          $server_id
 * @property string           $name
 * @property integer          $sort
 * @property string           $host
 *
 * @property AdminSmsServer[] $adminSmsServers
 * @property TenantHasSms[]   $tenantHasSms
 */
class SmsServer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sms_server}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'host'], 'string', 'max' => 45],
            [['sort'], 'integer', 'min' => -999, 'max' => 999],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'server_id' => 'ID',
            'name'      => 'Название',
            'host'      => 'Хост',
            'sort'      => 'Сортировка',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdminSmsServers()
    {
        return $this->hasMany(AdminSmsServer::className(), ['server_id' => 'server_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenantHasSms()
    {
        return $this->hasMany(TenantHasSms::className(), ['server_id' => 'server_id']);
    }
}
