<?php

namespace backend\modules\setting\models;

use yii\db\ActiveRecord;

/**
 * Class DefaultWorkerPushNotification
 * @package backend\modules\setting\models
 *
 * @property integer $push_id
 * @property string  $text
 * @property integer $type
 * @property integer $city_id
 * @property integer $tenant_id
 * @property integer $position_id
 */
class TenantWorkerPushNotification extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_push_notifications}}';
    }
}