<?php

namespace backend\modules\setting\forms;

use backend\modules\setting\helpers\NotificationHelper;
use backend\modules\setting\models\DefaultRecipientSmsTemplate;
use yii\base\Model;

class DefaultRecipientSmsTemplateForm extends Model
{
    public $templateId;
    public $text;
    public $type;
    public $params = [];
    public $positionId;

    public function __construct(DefaultRecipientSmsTemplate $model, array $config = [])
    {
        parent::__construct($config);

        $this->templateId = $model->template_id;
        $this->text       = $model->text;
        $this->type       = $model->type;
        $this->params     = $model->params;
        $this->positionId = $model->position_id;
    }

    public function rules()
    {
        return [
            ['text', 'required'],
            ['text', 'string', 'max' => 255],

            [
                'params',
                'filter',
                'filter' => function ($value) {
                    return empty($value) ? [] : (array)$value;
                },
            ],
            ['params', 'each', 'rule' => ['in', 'range' => NotificationHelper::getParamsList()]],
        ];
    }

    public function attributeLabels()
    {
        return [
            'text'       => t('recipient-sms-notices', 'Text', [], 'ru'),
            'type'       => t('recipient-sms-notices', 'Type', [], 'ru'),
            'params'     => t('recipient-sms-notices', 'Params', [], 'ru'),
            'positionId' => t('recipient-sms-notices', 'Position', [], 'ru'),
        ];
    }
}
