<?php

namespace backend\modules\setting\forms;

use backend\modules\setting\helpers\NotificationHelper;
use backend\modules\setting\models\DefaultWorkerPushNotification;
use common\modules\employee\models\position\Position;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class DefaultWorkerNotificationCreateForm extends Model
{
    public $pushId;
    public $text;
    public $type;
    public $params = [];
    public $positionId;

    public function rules()
    {
        return [
            ['text', 'required'],
            ['text', 'string', 'max' => 255],

            ['type', 'required'],
            ['type', 'in', 'range' => NotificationHelper::getStatusList()],
            [
                'type',
                'unique',
                'targetClass'     => DefaultWorkerPushNotification::className(),
                'targetAttribute' => ['type', 'positionId' => 'position_id'],
                'filter'          => ['not', ['push_id' => $this->pushId]],
                'message'         => 'Данный шаблон уже существцует',
            ],

            [
                'params',
                'filter',
                'filter' => function ($value) {
                    return empty($value) ? [] : (array)$value;
                },
            ],
            ['params', 'each', 'rule' => ['in', 'range' => NotificationHelper::getParamsList()]],

            ['positionId', 'required'],
            [
                'positionId',
                'exist',
                'targetClass'     => Position::className(),
                'targetAttribute' => ['positionId' => 'position_id'],
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'text'       => t('worker-push-notices', 'Text', [], 'ru'),
            'type'       => t('worker-push-notices', 'Type', [], 'ru'),
            'params'     => t('worker-push-notices', 'Params', [], 'ru'),
            'positionId' => t('worker-push-notices', 'Position', [], 'ru'),
        ];
    }


    public function getParamsAsString()
    {
        $params = NotificationHelper::getParamsMap();

        $result = [];

        if (ArrayHelper::isTraversable($params)) {
            foreach ($this->params as $param) {
                $result[] = Html::encode(ArrayHelper::getValue($params, $param));
            }
        }

        return implode('<br>', $result);
    }
}