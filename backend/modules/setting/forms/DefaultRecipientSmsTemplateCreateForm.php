<?php

namespace backend\modules\setting\forms;

use backend\modules\setting\helpers\NotificationHelper;
use backend\modules\setting\models\DefaultRecipientSmsTemplate;
use common\modules\employee\models\position\Position;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class DefaultRecipientSmsTemplateCreateForm extends Model
{
    public $templateId;
    public $text;
    public $type;
    public $params = [];
    public $positionId;

    public function rules()
    {
        return [
            ['text', 'required'],
            ['text', 'string', 'max' => 255],

            ['type', 'required'],
            ['type', 'in', 'range' => NotificationHelper::getStatusList()],
            [
                'type',
                'unique',
                'targetClass'     => DefaultRecipientSmsTemplate::className(),
                'targetAttribute' => ['type', 'positionId' => 'position_id'],
                'filter'          => ['not', ['template_id' => $this->templateId]],
                'message'         => 'Данный шаблон уже существцует',
            ],

            [
                'params',
                'filter',
                'filter' => function ($value) {
                    return empty($value) ? [] : (array)$value;
                },
            ],
            ['params', 'each', 'rule' => ['in', 'range' => NotificationHelper::getParamsList()]],

            ['positionId', 'required'],
            [
                'positionId',
                'exist',
                'targetClass'     => Position::className(),
                'targetAttribute' => ['positionId' => 'position_id'],
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'text'       => t('recipient-sms-notices', 'Text', [], 'ru'),
            'type'       => t('recipient-sms-notices', 'Type', [], 'ru'),
            'params'     => t('recipient-sms-notices', 'Params', [], 'ru'),
            'positionId' => t('recipient-sms-notices', 'Position', [], 'ru'),
        ];
    }
}
