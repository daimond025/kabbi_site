<?php

namespace backend\modules\setting\forms\search;

use backend\modules\setting\helpers\NotificationHelper;
use backend\modules\setting\models\DefaultRecipientSmsTemplate;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

class DefaultRecipientSmsTemplateSearch extends DefaultRecipientSmsTemplate
{
    const PAGE_SIZE = 50;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['type', 'in', 'range' => NotificationHelper::getStatusList()],
            ['position_id', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    public function attributeLabels()
    {
        return [
            'text'        => t('recipient-sms-notices', 'Text', [], 'ru'),
            'type'        => t('recipient-sms-notices', 'Type', [], 'ru'),
            'params'      => t('recipient-sms-notices', 'Params', [], 'ru'),
            'position_id' => t('recipient-sms-notices', 'Position', [], 'ru'),
        ];
    }

    public function search($params)
    {
        /** @var ActiveQuery $query */
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->filterWhere([
            'type'        => $this->type,
            'position_id' => $this->position_id,
        ]);

        return $dataProvider;
    }
}
