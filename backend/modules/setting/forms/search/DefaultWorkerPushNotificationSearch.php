<?php

namespace backend\modules\setting\forms\search;

use backend\modules\setting\helpers\NotificationHelper;
use backend\modules\setting\models\DefaultWorkerPushNotification;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

class DefaultWorkerPushNotificationSearch extends DefaultWorkerPushNotification
{
    const PAGE_SIZE = 50;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['type', 'in', 'range' => NotificationHelper::getStatusList()],
            ['position_id', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    public function attributeLabels()
    {
        return [
            'text'       => t('worker-push-notices', 'Text', [], 'ru'),
            'type'       => t('worker-push-notices', 'Type', [], 'ru'),
            'params'     => t('worker-push-notices', 'Params', [], 'ru'),
            'position_id' => t('worker-push-notices', 'Position', [], 'ru'),
        ];
    }

    public function search($params)
    {
        /** @var ActiveQuery $query */
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => self::PAGE_SIZE,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->filterWhere([
            'type'        => $this->type,
            'position_id' => $this->position_id,
        ]);

        return $dataProvider;
    }
}