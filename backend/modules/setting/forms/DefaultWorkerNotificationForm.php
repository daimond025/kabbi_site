<?php

namespace backend\modules\setting\forms;

use backend\modules\setting\helpers\NotificationHelper;
use backend\modules\setting\models\DefaultWorkerPushNotification;
use yii\base\Model;

class DefaultWorkerNotificationForm extends Model
{
    public $pushId;
    public $text;
    public $type;
    public $params = [];
    public $positionId;

    public function __construct(DefaultWorkerPushNotification $model, array $config = [])
    {
        parent::__construct($config);

        $this->pushId     = $model->push_id;
        $this->text       = $model->text;
        $this->type       = $model->type;
        $this->params     = $model->params;
        $this->positionId = $model->position_id;
    }

    public function rules()
    {
        return [
            ['text', 'required'],
            ['text', 'string', 'max' => 255],

            [
                'params',
                'filter',
                'filter' => function ($value) {
                    return empty($value) ? [] : (array)$value;
                },
            ],
            ['params', 'each', 'rule' => ['in', 'range' => NotificationHelper::getParamsList()]],
        ];
    }

    public function attributeLabels()
    {
        return [
            'text'       => t('worker-push-notices', 'Text', [], 'ru'),
            'type'       => t('worker-push-notices', 'Type', [], 'ru'),
            'params'     => t('worker-push-notices', 'Params', [], 'ru'),
            'positionId' => t('worker-push-notices', 'Position', [], 'ru'),
        ];
    }
}