<?php

namespace backend\modules\setting\forms;

use backend\modules\setting\helpers\EmailSettingHelper;
use backend\modules\setting\models\email\DefaultEmailSetting;
use yii\base\Model;

class DefaultEmailSettingForm extends Model
{
    public $provider;
    public $senderName;
    public $senderEmail;
    public $senderPassword;
    public $template;

    public function __construct(DefaultEmailSetting $model, array $config = [])
    {
        parent::__construct($config);
        $this->senderName     = $model->sender_name;
        $this->senderEmail    = $model->sender_email;
        $this->senderPassword = $model->sender_password;
        $this->template       = $model->template;
        $this->provider       = EmailSettingHelper::getProviderByServer($model->provider_server, $model->provider_port);
    }

    public function rules()
    {
        return [
            ['provider', 'required'],
            ['provider', 'in', 'range' => EmailSettingHelper::getProviderList()],

            ['senderName', 'required'],
            ['senderName', 'string', 'max' => 100],

            ['senderEmail', 'required'],
            ['senderEmail', 'email'],
            ['senderEmail', 'string', 'max' => 255],

            ['senderPassword', 'required'],
            ['senderPassword', 'string', 'max' => 100],

            ['template', 'required'],
            ['template', 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'provider'       => 'Провайдер',
            'senderName'     => 'Имя отправителя',
            'senderEmail'    => 'Почта отправителя',
            'senderPassword' => 'Пароль отправителя',
            'template'       => 'Шаблон',
        ];
    }
}