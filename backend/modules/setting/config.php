<?php

return [
    'components' => [
        'tenantService' => [
            'class' => \backend\modules\setting\components\TenantServiceComponent::className(),
        ],
        'taxiTariffService' => [
            'class' => \backend\modules\setting\components\TaxiTariffServiceComponent::className()
        ],
    ],
];