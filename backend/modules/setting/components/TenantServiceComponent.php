<?php

namespace backend\modules\setting\components;

use app\modules\setting\models\TenantSetting;
use common\modules\tenant\models\Tenant;
use common\modules\tenant\models\TenantHasCity;
use yii\base\Component;
use yii\helpers\ArrayHelper;

class TenantServiceComponent extends Component
{

    public function getTenantId($domain)
    {
        return Tenant::find()
            ->select('tenant_id')
            ->where(['domain' => $domain])
            ->scalar();
    }

    public function getDomainId($tenantId)
    {
        return Tenant::find()
            ->select('domain')
            ->where(['tenant_id' => $tenantId])
            ->scalar();
    }

    public function getCityList($tenantId, $onlyActive = true)
    {
        $query = TenantHasCity::find()
            ->alias('thc')
            ->joinWith('city c')
            ->where(['thc.tenant_id' => $tenantId])
            ->indexBy('city_id')
            ->orderBy('sort');

        if ($onlyActive) {
            $query->andWhere(['block' => 0]);
        }
        $models = $query->all();

        return ArrayHelper::getColumn($models, function ($model) {
            /** @var TenantHasCity $model */
            return $model->city->name;
        });
    }

    public function getPhoneLineScenario($tenantId)
    {
        $scenario = TenantSetting::find()
            ->select('value')
            ->where([
                'tenant_id' => $tenantId,
                'name'      => TenantSetting::PHONE_LINE_SCENARIO,
                'type'      => TenantSetting::TYPE_SYSTEM,
            ])
            ->scalar();

        if ($unserializeScenario = unserialize($scenario)) {
            return $unserializeScenario;
        }

        return [];
    }
}