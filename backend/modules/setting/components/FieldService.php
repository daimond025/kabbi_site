<?php

namespace backend\modules\setting\components;

use app\modules\setting\models\FieldRecord;
use common\modules\tenant\models\TenantCityHasPosition;
use yii\helpers\ArrayHelper;

class FieldService
{

    public function addDocumentByPosition($documentId, $positionId)
    {
        $tenantCityHasPositionModels = TenantCityHasPosition::find()
            ->where([
                'position_id' => $positionId,
            ])
            ->asArray()
            ->all();


        $data = [];
        foreach ($tenantCityHasPositionModels as $model) {
            $name = ArrayHelper::getValue($this->getDocumentByNameList(), $documentId);

            if ($name) {
                $data[] = [
                    $model['tenant_id'],
                    $model['city_id'],
                    $positionId,
                    'worker',
                    $name,
                    1,
                ];
            }
        }

        if (!empty($data)) {
            app()->db->createCommand()->batchInsert(FieldRecord::tableName(),
                ['tenant_id', 'city_id', 'position_id', 'type', 'name', 'value'], $data)->execute();
        }
    }

    public function deleteDocumentByPosition($documentId, $positionId)
    {
        $name = ArrayHelper::getValue($this->getDocumentByNameList(), $documentId);

        if ($name) {
            FieldRecord::deleteAll([
                'position_id' => $positionId,
                'name'        => $name,
            ]);
        }

    }

    public function getDocumentByNameList()
    {
        return [
            1 => FieldRecord::NAME_WORKER_PASSPORT,
            2 => FieldRecord::NAME_WORKER_INN,
            3 => FieldRecord::NAME_WORKER_OGRNIP,
            4 => FieldRecord::NAME_WORKER_SNILS,
            5 => FieldRecord::NAME_WORKER_POSITION_MEDICAL_CERTIFICATE,
            6 => FieldRecord::NAME_WORKER_POSITION_OSAGO,
            7 => FieldRecord::NAME_WORKER_POSITION_DRIVER_LICENSE,
            8 => FieldRecord::NAME_WORKER_POSITION_PTS,
        ];
    }


}