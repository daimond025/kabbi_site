<?php

namespace backend\modules\setting\components;

use app\modules\setting\models\TaxiTariff;
use yii\base\Component;
use yii\helpers\ArrayHelper;

class TaxiTariffServiceComponent extends Component
{

    public function getTariffList($tenantId, $cityId)
    {
        $models = TaxiTariff::find()
            ->alias('t')
            ->joinWith('tariffHasCity thc')
            ->where([
                't.tenant_id' => $tenantId,
                'thc.city_id'   => $cityId,
            ])
            ->indexBy('tariff_id')
            ->orderBy('sort')
            ->all();

        return ArrayHelper::getColumn($models, 'name');
    }
}