<?php

namespace backend\modules\setting\helpers;

use yii\helpers\ArrayHelper;

class EmailSettingHelper
{
    public static function getParamsMap()
    {
        return [
            '#CONTENT#' => t('email-template', '#CONTENT#', [], 'ru'),
        ];
    }

    public static function getProviderNames()
    {
        $providers = self::getProviders();
        if (is_array($providers)) {
            return ArrayHelper::getColumn($providers, 'name');
        }

        return null;
    }

    public static function getProviderByServer($server, $port)
    {
        $providers = self::getProviders();
        if (is_array($providers)) {
            foreach ($providers as $providerKey => $provider) {
                if (ArrayHelper::getValue($provider, 'server') === $server
                    && ArrayHelper::getValue($provider, 'port') === $port) {
                    return $providerKey;
                }
            }
        }

        return null;
    }

    public static function getProviderServer($provider)
    {
        $providers = self::getProviders();
        if (is_array($providers)) {
            return ArrayHelper::getValue($providers, "$provider.server");
        }

        return null;
    }

    public static function getProviderPort($provider)
    {
        $providers = self::getProviders();
        if (is_array($providers)) {
            return ArrayHelper::getValue($providers, "$provider.port");
        }

        return null;
    }

    public static function getProviderList()
    {
        $providers = self::getProviders();
        if (is_array($providers)) {
            return array_keys($providers);
        }

        return [];
    }

    private static function getProviders()
    {
        return [
            'yandex' => [
                'name'   => 'Yandex',
                'server' => 'smtp.yandex.ru',
                'port'   => 465,
            ],
            'gmail'  => [
                'name'   => 'Gmail',
                'server' => 'smtp.gmail.com',
                'port'   => 465,
            ],
        ];
    }
}