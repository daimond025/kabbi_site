<?php

namespace backend\modules\setting\helpers;

use app\modules\setting\models\OrderStatus;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class NotificationHelper
{
    const PARAM_ADDRESS = '#ADDRESS#';
    const PARAM_DATE = '#DATE#';
    const PARAM_TIME = '#TIME#';
    const PARAM_TARIFF = '#TARIFF#';
    const PARAM_PRICE = '#PRICE#';
    const PARAM_CURRENCY = '#CURRENCY#';
    const PARAM_WORKER_LAST_NAME = '#LAST_NAME#';
    const PARAM_WORKER_NAME = '#NAME#';
    const PARAM_WORKER_SECOND_NAME = '#SECOND_NAME#';
    const PARAM_WORKER_PHONE = '#PHONE#';
    const PARAM_CODE = '#CODE#';

    public static function getStatusList()
    {
        return [
            OrderStatus::STATUS_NEW,
            OrderStatus::STATUS_PRE,
            OrderStatus::STATUS_EXECUTING,
        ];
    }

    public static function getStatusMap()
    {
        $result = [];

        $statusList  = self::getStatusList();
        $statusNames = ArrayHelper::map(OrderStatus::getStatusData(), 'status_id', 'name');

        if (ArrayHelper::isTraversable($statusList)) {
            foreach ($statusList as $statusId) {
                $statusName = ArrayHelper::getValue($statusNames, $statusId);
                $result[$statusId] = "[$statusId] " . t('status_event', $statusName, [], 'ru');
            }
        }

        return $result;
    }

    public static function getParamsList()
    {
        return [
            self::PARAM_ADDRESS,
            self::PARAM_DATE,
            self::PARAM_TIME,
            self::PARAM_TARIFF,
            self::PARAM_PRICE,
            self::PARAM_CURRENCY,
            self::PARAM_WORKER_LAST_NAME,
            self::PARAM_WORKER_NAME,
            self::PARAM_WORKER_SECOND_NAME,
            self::PARAM_WORKER_PHONE,
            self::PARAM_CODE,
        ];
    }

    public static function getParamsMap()
    {
        $result = [];

        $params = self::getParamsList();

        if (ArrayHelper::isTraversable($params)) {
            foreach ($params as $param) {
                $result[$param] = $param . ' - ' . t('template', $param, [], 'ru');
            }
        }

        return $result;
    }

    public static function getParamsAsString($params)
    {
        $allParams = self::getParamsMap();

        $result = [];

        if (ArrayHelper::isTraversable($allParams)) {
            foreach ($params as $param) {
                $result[] = Html::encode(ArrayHelper::getValue($allParams, $param));
            }
        }

        return implode('<br>', $result);
    }
}