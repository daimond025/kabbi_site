<?php

namespace app\modules\setting\controllers;

use common\modules\car\models\transport\TransportType;
use Yii;
use common\modules\car\models\CarOption;
use app\modules\setting\models\CarOptionSearch;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\setting\models\SourceMessage;
use backend\modules\setting\models\Message;

/**
 * CarOptionController implements the CRUD actions for CarOption model.
 */
class CarOptionController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['transportRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['transport'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CarOption models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new CarOptionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CarOption model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    private function getTypeList()
    {
        return TransportType::find()->all();
    }

    private function getTypeMap()
    {
        return ArrayHelper::map($this->getTypeList(), 'type_id', 'name');
    }

    /**
     * Creates a new CarOption model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CarOption(['sort' => 100]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            session()->setFlash('success', 'Опция успешно добавлена');

            $transaction = app()->db->beginTransaction();
            try {
                $post_translate_map = post('translate');
                //Создаем фразу для перевода
                $source_message = new SourceMessage([
                    'category' => CarOption::SOURCE_CATEGORY,
                    'message'  => $model->name,
                ]);
                if (!$source_message->save()) {
                    throw new Exception;
                }

                //Добавляем переводы
                Message::batchInsert($source_message->id, $post_translate_map);

                $transaction->commit();
            } catch (Exception $exc) {
                session()->setFlash('error', 'Ошибка добавления переводов');
                $transaction->rollBack();
            }

            return $this->redirect(['view', 'id' => $model->option_id]);
        } else {
            return $this->render('create', [
                'model'   => $model,
                'typeMap' => $this->getTypeMap(),
            ]);
        }
    }

    /**
     * Updates an existing CarOption model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model          = $this->findModel($id);
        $source_message = SourceMessage::getMessageWithTranslates($model->name, CarOption::SOURCE_CATEGORY);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $old_class_attribute = $model->getOldAttribute('name');
            if ($model->save()) {
                $post_translate_map = post('translate');
                if (!empty($post_translate_map)) {
                    //Если изменилась фраза(цвет)
                    if ($old_class_attribute !== $model->name) {
                        $source_message->message = $model->name;
                        $source_message->save(true, ['message']);
                    }
                    //Обновление переводов
                    foreach ($post_translate_map as $language => $translation) {
                        if (!isset($source_message->translates[$language])) {
                            (new Message([
                                'id'          => $source_message->id,
                                'language'    => $language,
                                'translation' => $translation,
                                'message'     => $model->name,
                                'category'    => CarOption::SOURCE_CATEGORY,
                            ]))
                                ->save();
                        } elseif ($translation != $source_message->translates[$language]->language) {
                            $source_message->translates[$language]->translation = $translation;
                            $source_message->translates[$language]->save(true, ['translation']);
                        }
                    }
                }
                session()->setFlash('success', 'Данные обновлены.');

                return $this->refresh();
            }
        }

        return $this->render('update', [
            'model'      => $model,
            'translates' => $source_message->translates,
            'typeMap'    => $this->getTypeMap(),
        ]);
    }

    /**
     * Deletes an existing CarOption model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CarOption model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return CarOption the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CarOption::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
