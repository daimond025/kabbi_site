<?php

namespace app\modules\setting\controllers;

use backend\modules\setting\models\OrderSearch;
use common\modules\employee\models\worker\Worker;
use common\modules\order\models\OrderTrackService;
use common\services\OrderStatusService;
use frontend\modules\employee\components\worker\WorkerShiftService;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\Controller;
use yii\helpers\ArrayHelper;

use app\modules\setting\models\Order;
use app\modules\setting\models\OrderStatus;
use app\modules\setting\models\OrderChangeData;
use app\modules\setting\models\OrderDetailCost;
use app\modules\setting\models\ClientReview;
use app\modules\setting\models\City;
use app\modules\setting\models\Currency;
use app\modules\setting\models\User;


class OrderSearchController extends Controller
{


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['tenantOrders'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel  = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $model         = Order::findOne($id);
        $review        = ClientReview::find()
            ->where('order_id = :id', ['id' => $model->order_id])
            ->all();
        $raw_cacl_data = (new \yii\db\Query())
            ->select(['raw_cacl_data'])
            ->from('tbl_raw_order_calc')
            ->where(['order_id' => $id])
            ->scalar();
        $raw_cacl_data = !empty($raw_cacl_data) ? json_decode($raw_cacl_data, true) : null;

        $map = $this->getMap($model->status_id);

        return $this->render('view', [
            'order'         => $model,
            'review'        => $review,
            'event'         => $this->getEvents($model->order_id, $model->city_id),
            'currency'      => Currency::getCurrencySymbol($model->currency_id),
            'raw_cacl_data' => $raw_cacl_data,
            'map_data_type' => $map,
        ]);
    }


    protected function getMap($status_id)
    {
        $map_data_type = Order::MAP_DATA_TYPE_ADDRESS;
        $status_map    = ArrayHelper::map(OrderStatus::getStatusData(), 'status_id', 'status_group');

        if (isset($status_map[$status_id])) {
            if (in_array($status_map[$status_id], OrderStatus::getStatusGroupsForMapDriverType())) {
                $map_data_type = Order::MAP_DATA_TYPE_DRIVER;
            } elseif (in_array($status_map[$status_id], OrderStatus::getStatusGroupsForMapRouteType())) {
                $map_data_type = Order::MAP_DATA_TYPE_ROUTE;
            }
        }

        return $map_data_type;
    }


    /*
     * Обрабатываем события для отчета
    */

    protected function getEvents($order_id, $city_id)
    {

        //Результирующий массив
        $events = [];
        //Доп. информация по логу (Расчет стоимости, Отзыв)
        $info = [];
        //Получаем события из БД
        $db_events = OrderChangeData::find()->asArray()->where(['order_id' => $order_id])->all();
        //Смещение часового пояса
        $city_offset = City::getTimeOffset($city_id);
        //Берем из кеша все статусы, формируем карту
        $arStatus     = ArrayHelper::map(OrderStatus::getStatusData(), 'status_id', 'name');
        $lastStatusId = null;

        $order      = Order::findOne($order_id);
        $positionId = isset($order->position_id) ? $order->position_id : null;

        if (isset($db_events[0])) {
            if ($db_events[0]['change_object_type'] == 'order' && $db_events[0]['change_field'] == 'status_id') {
                $lastStatusId = $db_events[0]['change_val'];
                if (in_array($db_events[0]['change_val'], [
                    OrderStatus::STATUS_NEW,
                    OrderStatus::STATUS_FREE,
                    OrderStatus::STATUS_MANUAL_MODE,
                ])) {
                    $events[] = [
                        'TEXT' => 'Создание заказа',
                        'TIME' => Yii::$app->formatter->asDateTime($city_offset + $db_events[0]['change_time'],
                            'php:d.m.Y в H:i:s'),
                        'TYPE' => $db_events[0]['change_object_type'],
                        'INFO' => $info,
                    ];
                } elseif ($db_events[0]['change_val'] == OrderStatus::STATUS_EXECUTING) {
                    $events[] = [
                        'TEXT' => 'Заказ сделан с бордюра',
                        'TIME' => Yii::$app->formatter->asDateTime($city_offset + $db_events[0]['change_time'],
                            'php:d.m.Y в H:i:s'),
                        'TYPE' => $db_events[0]['change_object_type'],
                        'INFO' => $info,
                    ];
                }
            }
        }

        unset($db_events[0]);
        foreach ($db_events as $key => $item) {
            $text = '';
            $info = [];

            if ($item['change_object_type'] == 'order' && $item['change_field'] == 'status_id') {
                $lastStatusId = $item['change_val'];
            }

            switch ($item['change_object_type']) {
                //Изменения с заказом
                case 'order':
                    //Изменения диспетчера
                    if (strpos($item['change_subject'], 'disputcher') !== false) {
                        //Пропускаем служебные поля, которые не должен видеть пользователь
                        if ($item['change_field'] === 'client_id') {
                            continue;
                        }
                        $arUserPieces = explode('_', $item['change_subject']);
                        $user_id      = end($arUserPieces);

                        $user = User::find()->
                        where(['user_id' => $user_id])->
                        select(['last_name', 'name'])->
                        one();
                        $name = '<u>' . Html::encode($user->last_name . ' ' . $user->name) . '</u>';

                        $order = new Order();

                        $text = 'Диспетчер ' . $name . ' изменил заказ. Изменилось: ' . Html::encode($order->getAttributeLabel($item['change_field']));
                    } //Все остальные, связанные со сменой статуса
                    elseif ($item['change_field'] == 'status_id') {
                        if (in_array($item['change_val'], OrderStatus::getFinishedStatusId(), false)
                            || $item['change_val'] == OrderStatus::STATUS_EXECUTING
                        ) {
                            $order_finished = true;
                        }
                        //Статус завершенного заказа
                        if (in_array($item['change_val'], OrderStatus::getCompletedStatusId(), false)) {
                            $text = OrderStatusService::translate($item['change_val'], $positionId);

                            $info                       = OrderDetailCost::find()->asArray()->where(['order_id' => $item['order_id']])->one();
                            $item['change_object_type'] = 'finish';
                        } //Пропускаем статус "Предложение водителю", т.к. уже вывели эту инфу вместе с водилой.
                        elseif (in_array($item['change_val'], [
                            OrderStatus::STATUS_OFFER_ORDER,
                            OrderStatus::STATUS_DRIVER_REFUSED,
                            OrderStatus::STATUS_DRIVER_IGNORE_ORDER_OFFER,
                        ], false)) {
                            continue;
                        } //Все остальные статусы, название берется из карты статусов
                        else {
                            $text = OrderStatusService::translate($item['change_val'], $positionId);
                        }
                    }
                    break;

                //Изменения с водителем
                case 'worker':

                    $driver = Worker::findOne([
                        'callsign'  => $item['change_object_id'],
                        'tenant_id' => $item['tenant_id'],
                    ]);
                    $name   = '(' . $driver->callsign . ')' . '<u>' . Html::encode($driver->last_name . ' ' . $driver->name) . '</u>';


                    switch ($item['change_val']) {

                        case 'OFFER_ORDER':
                            $text = 'Заказ предложен водителю ' . $name;
                            break;

                        case 'FREE':
                            if (!$order_finished) {
                                switch ($lastStatusId) {
                                    case OrderStatus::STATUS_DRIVER_REFUSED:
                                        $message = 'Driver {name} refused';
                                        break;
                                    case OrderStatus::STATUS_DRIVER_IGNORE_ORDER_OFFER:
                                        $message = 'Driver {name} ignored order offer';
                                        break;
                                    default:
                                        $message = 'Driver {name} removed from the order';
                                }
                                $text = t('order', $message, ['name' => $name]);
                            }
                            break;

                        case 'BLOCKED':
                            $text = 'Водитель ' . $name . ' заблокирован';
                            break;

                        case 'ON_ORDER':
                            //Для получения времени подъезда берем следующий эл. массива
                            $nextAfterElement = $db_events[$key + 1];
                            $time             = $nextAfterElement['change_field'] == 'time_to_client' ? $nextAfterElement['change_val'] : 0;
                            $text             = 'Водитель ' . $name . ' взял заказ, подъедет через ' . $time . ' мин.';

                            //Удаляем элементы, т.к. мы их уже обработали
                            if (($db_events[$key + 2]['change_field'] == 'status_id')
                                && ($db_events[$key + 2]['change_val'] == OrderStatus::STATUS_GET_WORKER
                                    || $db_events[$key + 2]['change_val'] == OrderStatus::STATUS_EXECUTION_PRE)
                            ) {
                                unset($db_events[$key + 2]);
                            }
                            if ($db_events[$key + 1]['change_field'] == 'time_to_client') {
                                unset($db_events[$key + 1]);
                            }
                            break;

                    }
                    break;

                //Оставлен отзыв к заказу
                case 'review':
                    $clientReview = ClientReview::find()->
                    with([
                        'client' => function ($query) {
                            $query->select(['client_id', 'last_name', 'name']);
                        },
                    ])->
                    where(['review_id' => $item['change_object_id']])->
                    one();
                    $name         = Html::encode($clientReview->client->last_name . ' ' . $clientReview->client->name);
                    $text         = 'Клиент ' . $name . ' добавил отзыв о поездке:';
                    $info         = [
                        'REVIEW'  => $clientReview->text,
                        'RAITING' => $clientReview->rating,
                    ];
                    break;

            }

            //Формирование результирующего массива
            if (!empty($text)) {
                $events[] = [
                    'TEXT'         => $text,
                    'TIME'         => Yii::$app->formatter->asDateTime($city_offset + $item['change_time'],
                        'php:d.m.Y в H:i:s'),
                    'TYPE'         => $item['change_object_type'],
                    'CHANGE_FIELD' => $item['change_field'],
                    'INFO'         => $info,
                ];
            }

        }


        return $events;
    }


    /**
     * Getting current worker coords by id.
     *
     * @param integer $workerId
     *
     * @return json
     */
    public function actionGetWorkerCoordsById($workerId)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $json   = ['error' => t('order', 'No data for display')];
        $worker = Worker::findOne($workerId);

        if (!empty($worker)) {
            $workerShiftService = new WorkerShiftService();
            $location           = $workerShiftService->getWorkerLocation($worker->callsign);

            if (!empty($location)) {
                $json             = $location;
                $json['callsign'] = $worker->callsign;
                $json['error']    = '';
            }
        }

        return json_encode($json);
    }


    /**
     * Getting order address coords
     *
     * @param type $order_id
     *
     * @return json
     */
    public function actionGetOrderAddress($order_id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $address = Order::getOrderAddress($order_id);

        return $address ? unserialize($address) : ['error' => t('order', 'No data for display')];
    }


    /**
     * Getting current worker coords by callsing.
     *
     * @param integer $callsign
     *
     * @return json
     */
    public function actionGetWorkerCoordsByCallsign($callsign)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $workerShiftService = new WorkerShiftService();

        return json_encode($workerShiftService->getWorkerLocation($callsign));
    }

    /**
     * Getting order tracking coords.
     *
     * @param integer $order_id
     *
     * @return json
     */
    public function actionGetTracking($order_id)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        /* @var $service OrderTrackService */
        $service = \Yii::createObject(OrderTrackService::class);
        $track   = $service->getTrack($order_id);

        return json_encode([
            'order_route' => $track,
        ]);
    }


    /**
     * Finds the SmsLog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return SmsLog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
