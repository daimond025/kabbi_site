<?php

namespace app\modules\setting\controllers;

use common\modules\tenant\helpers\SmsTemplateParamsHelper;
use Yii;
use app\modules\setting\models\DefaultClientPushNotificationsCourier;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultClientPushNotificationsCourierController implements the CRUD actions for DefaultClientPushNotificationsCourier model.
 */
class DefaultClientPushNotificationsCourierController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DefaultClientPushNotificationsCourier models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => DefaultClientPushNotificationsCourier::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DefaultClientPushNotificationsCourier model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model'     => $this->findModel($id),
            'paramsMap' => SmsTemplateParamsHelper::getParamsMap(),
        ]);
    }

    /**
     * Creates a new DefaultClientPushNotificationsCourier model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DefaultClientPushNotificationsCourier(['params' => []]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->template_id]);
        } else {
            return $this->render('create', [
                'model'     => $model,
                'paramsMap' => SmsTemplateParamsHelper::getParamsMap(),
            ]);
        }
    }

    /**
     * Updates an existing DefaultClientPushNotificationsCourier model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->template_id]);
        } else {
            return $this->render('update', [
                'model'     => $model,
                'paramsMap' => SmsTemplateParamsHelper::getParamsMap(),
            ]);
        }
    }

    /**
     * Deletes an existing DefaultClientPushNotificationsCourier model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DefaultClientPushNotificationsCourier model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DefaultClientPushNotificationsCourier the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DefaultClientPushNotificationsCourier::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
