<?php

namespace app\modules\setting\controllers;

use app\modules\setting\models\Republic;
use Yii;
use yii\filters\AccessControl;

class TimeController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index'],
                        'roles'   => ['geoRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['geo'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        if (Yii::$app->user->can('geo') && isset($_POST['Republic'])) {
            $arRepublic = array_keys($_POST['Republic']);

            $arRepublicModels = Republic::find()->where(['republic_id' => $arRepublic])->all();

            foreach ($arRepublicModels as $republic) {
                $republic->timezone = $_POST['Republic'][$republic->republic_id];
                $republic->save(false);
            }

            session()->setFlash('success', 'Данные успешно сохранены.');

            return $this->refresh();
        }

        $arCountryModels = \app\modules\setting\models\Country::find()
            ->orderBy('name')
            ->with('republics')
            ->all();

        return $this->render('index', [
            'arCountryModels' => $arCountryModels,
        ]);
    }
}
