<?php

namespace app\modules\setting\controllers;

use app\modules\setting\models\DefaultClientStatusEventSearch;
use common\modules\tenant\models\DefaultClientStatusEvent;
use frontend\modules\employee\components\position\PositionService;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


/**
 * Class DefaultClientStatusEventController
 * @package app\modules\setting\controllers
 */
class DefaultClientStatusEventController extends Controller
{
    private $positionService;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['clientStatusEventRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['clientStatusEvent'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function __construct($id, Module $module, PositionService $positionService, array $config = [])
    {
        $this->positionService = $positionService;

        parent::__construct($id, $module, $config);
    }

    private function getPositionMap()
    {
        $positions = $this->positionService->getAllPositions();

        return empty($positions) ? [] : ArrayHelper::map($positions, 'position_id', 'name');
    }

    /**
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex()
    {
        $searchModel  = new DefaultClientStatusEventSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'positionMap'  => $this->getPositionMap(),
            'statusMap'    => DefaultClientStatusEvent::getStatusMap(),
            'groupMap'     => DefaultClientStatusEvent::getGroupMap(),
            'noticeMap'    => DefaultClientStatusEvent::getNoticeMap(),
        ]);
    }

    /**
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionCreate()
    {
        $model = new DefaultClientStatusEvent();

        if ($model->load(post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model'       => $model,
            'positionMap' => $this->getPositionMap(),
            'statusMap'   => DefaultClientStatusEvent::getStatusMap(),
            'groupMap'    => DefaultClientStatusEvent::getGroupMap(),
            'noticeMap'   => DefaultClientStatusEvent::getNoticeMap(),
        ]);
    }


    /**
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidParamException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model'       => $model,
            'positionMap' => $this->getPositionMap(),
            'statusMap'   => DefaultClientStatusEvent::getStatusMap(),
            'groupMap'    => DefaultClientStatusEvent::getGroupMap(),
            'noticeMap'   => DefaultClientStatusEvent::getNoticeMap(),
        ]);
    }


    /**
     * @param integer $id
     *
     * @return mixed
     * @throws \Exception
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * @param integer $id
     *
     * @return DefaultClientStatusEvent
     * @throws \yii\web\NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = DefaultClientStatusEvent::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
