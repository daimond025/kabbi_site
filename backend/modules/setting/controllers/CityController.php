<?php

namespace app\modules\setting\controllers;

use Yii;
use app\modules\setting\models\City;
use app\modules\setting\models\Republic;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\setting\models\CitySearch;

/**
 * CityController implements the CRUD actions for City model.
 */
class CityController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view', 'city-search'],
                        'roles'   => ['geoRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['geo'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all City models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CitySearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single City model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $parking = City::findOne(['city_id' => $id]);
        $model = $this->findModel($id);
        $cityCoords = $model->city_polygon;

        return $this->render('view', [
            'cityCoords' => $cityCoords,
            'model'      => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new City model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new City(['lat'=>0,'lon'=>0]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->city_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing City model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $cityCoords = $model->city_polygon;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->city_id]);
        } else {
            return $this->render('update', [
                'cityCoords' => $cityCoords,
                'model'      => $model,
            ]);
        }
    }

    public function actionUpdatePolygon()
    {
        $model = $this->findModel(post('city_id'));
        if (post('polygon') == 'undefined') {
            $model->city_polygon = null;
        } else {
            $model->city_polygon = post('polygon');
        }
        $model->search = post('search');
        $model->fulladdress = post('fulladdress');
        $model->fulladdress_reverse = post('address_reverse');
        $model->lat = post('lat');
        $model->lon = post('lon');
        $model->shortname = post('shortname');
        $model->republic_id = post('republic_id');
        $model->sort = post('sort');
        if ($model->save()) {
            return true;
        } else {
            dd($model->save());
        }
    }

    /**
     * Deletes an existing City model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the City model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return City the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = City::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCitySearch()
    {
        $arRepublic = [];
        if (!empty($_POST['city'])) {
            $country_id = post('country');
            $city = post('city');
            $tbl_republic = Republic::tableName();

            $arRepublic = Republic::find()->asArray()->
            filterWhere(['country_id' => $country_id])->
            select(["$tbl_republic.republic_id", "$tbl_republic.name", "$tbl_republic.shortname"])->
            joinWith([
                'city' => function ($query) use ($city) {
                    $tbl_city = City::tableName();
                    $query->andWhere([
                        'LIKE',
                        "$tbl_city.name",
                        "$city%",
                        false,
                    ])->orderBy(["$tbl_city.sort" => SORT_DESC]);
                },
            ])->
            all();
        }

        return $this->renderAjax('citySearch', ['arRepublic' => $arRepublic]);
    }

}
