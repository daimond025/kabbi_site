<?php

namespace app\modules\setting\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\setting\models\Currency;
use backend\modules\setting\models\CurrencyType;
use yii\data\ActiveDataProvider;
use backend\modules\setting\models\SourceMessage;
use backend\modules\setting\models\Message;

/**
 * CityController implements the CRUD actions for City model.
 */
class CurrencyController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['currencyRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['currency'],
                    ],
                ],
            ],
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all City models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        $dataProvider = new ActiveDataProvider([
            'query' => Currency::find()->with('currencyType'),
        ]);
        
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    
    public function actionCreate()
    {
        $model = new Currency();
        
        if ( $model->load( post() ) && $model->save()) {
            
            $transaction = app()->db->beginTransaction();
            try {
                $post_translate_map = post('translate');
                //Создаем фразу для перевода
                $source_message = new SourceMessage(['category' => Currency::SOURCE_CATEGORY, 'message' => $model->name]);
                if (!$source_message->save()) {
                    throw new Exception;
                }

                //Добавляем переводы
                Message::batchInsert($source_message->id, $post_translate_map);

                $transaction->commit();
            } catch (Exception $exc) {
                session()->setFlash('error', 'Ошибка добавления переводов');
                $transaction->rollBack();
            }
            
            return $this->redirect(['view', 'id' => $model->currency_id]);
        }
        
        return $this->render('create', ['model' => $model]);
    }
    
    
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $source_message = SourceMessage::getMessageWithTranslates($model->name, Currency::SOURCE_CATEGORY);
        $old_class_attribute = $model->name;

        if ( $model->load( post() ) && $model->save()) {
            
            $post_translate_map = post('translate');
            if (!empty($post_translate_map)) {
                //Если изменилась фраза
                if ($old_class_attribute !== $model->name) {
                    $source_message->message = $model->name;
                    $source_message->save(true, ['message']);
                }
                //Обновление переводов
                foreach ($post_translate_map as $language => $translation) {
                    if (!isset($source_message->translates[$language])) {
                        (new Message([
                            'id' => $source_message->id,
                            'language' => $language,
                            'translation' => $translation,
                            'message' => $model->name,
                            'category' => Currency::SOURCE_CATEGORY,
                        ]))
                        ->save();
                    } elseif ($translation != $source_message->translates[$language]->language) {
                        $source_message->translates[$language]->translation = $translation;
                        $source_message->translates[$language]->save(true, ['translation']);
                    }
                }
            }
            
            return $this->redirect(['view', 'id' => $model->currency_id]);
        }
        
        return $this->render('update', [
            'model' => $model,
            'translates' => $source_message->translates
        ]);
    }
    
    
    public function actionView($id)
    {
        $model = $this->findModel($id);
        
        return $this->render('view', ['model' => $model]);
    }
    
    
    protected function findModel($id)
    {
        if (($model = Currency::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
