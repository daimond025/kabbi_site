<?php

namespace app\modules\setting\controllers;

use common\modules\car\models\transport\TransportType;
use Yii;
use common\modules\car\models\CarClass;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\setting\models\SourceMessage;
use backend\modules\setting\models\Message;
use app\modules\setting\models\CarClassSearch;

/**
 * CarClassController implements the CRUD actions for CarClass model.
 */
class CarClassController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['transportRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['transport'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CarClass models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CarClassSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CarClass model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CarClass model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CarClass();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            session()->setFlash('success', 'Класс успешно добавлен');

            $transaction = app()->db->beginTransaction();
            try {
                $post_translate_map = post('translate');
                //Создаем фразу для перевода
                $source_message = new SourceMessage([
                    'category' => CarClass::SOURCE_CATEGORY,
                    'message'  => $model->class,
                ]);
                if (!$source_message->save()) {
                    throw new Exception;
                }

                //Добавляем переводы
                Message::batchInsert($source_message->id, $post_translate_map);

                $transaction->commit();
            } catch (Exception $exc) {
                session()->setFlash('error', 'Ошибка добавления переводов');
                $transaction->rollBack();
            }

            return $this->redirect('/setting/car-class');
        } else {
            return $this->render('create', [
                'model'   => $model,
                'typeMap' => $this->getTypeMap(),
            ]);
        }
    }

    private function getTypeList()
    {
        return TransportType::find()->all();
    }

    private function getTypeMap()
    {
        return ArrayHelper::map($this->getTypeList(), 'type_id', 'name');
    }

    /**
     * Updates an existing CarClass model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $source_message = SourceMessage::getMessageWithTranslates($model->class, CarClass::SOURCE_CATEGORY);

        if ($model->load(Yii::$app->request->post())) {
            $old_class_attribute = $model->getOldAttribute('class');
            if ($model->save()) {
                $post_translate_map = post('translate');
                if (!empty($post_translate_map)) {
                    //Если изменилась фраза(класс)
                    if ($old_class_attribute !== $model->class) {
                        $source_message->message = $model->class;
                        $source_message->save(true, ['message']);
                    }
                    //Обновление переводов
                    foreach ($post_translate_map as $language => $translation) {
                        if (!isset($source_message->translates[$language])) {
                            (new Message([
                                'id'          => $source_message->id,
                                'language'    => $language,
                                'translation' => $translation,
                                'message'     => $model->class,
                                'category'    => CarClass::SOURCE_CATEGORY,
                            ]))
                                ->save();
                        } elseif ($translation != $source_message->translates[$language]->language) {
                            $source_message->translates[$language]->translation = $translation;
                            $source_message->translates[$language]->save(true, ['translation']);
                        }
                    }
                }
                session()->setFlash('success', 'Данные обновлены.');

                return $this->refresh();
            }
        }

        return $this->render('update', [
            'model'      => $model,
            'translates' => $source_message->translates,
            'typeMap'    => $this->getTypeMap(),
        ]);
    }

    /**
     * Deletes an existing CarClass model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        //Удаляем фразу для перевода
        SourceMessage::deleteOne(['message' => $model->class, 'category' => CarClass::SOURCE_CATEGORY]);

        session()->setFlash('success', 'Класс успешно удален');

        return $this->redirect(['index']);
    }

    /**
     * Finds the CarClass model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \common\modules\car\models\CarClass the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CarClass::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}