<?php

namespace app\modules\setting\controllers;

use backend\modules\setting\components\FieldService;
use common\modules\employee\components\document\DocumentLayer;
use common\modules\employee\models\position\Position;
use common\modules\employee\models\position\PositionHasDocument;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * PositionHasDocumentController implements the CRUD actions for PositionHasDocument model.
 */
class PositionHasDocumentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['employeeRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['employee'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PositionHasDocument models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => PositionHasDocument::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PositionHasDocument model.
     * @param integer $position_id
     * @param integer $document_id
     * @return mixed
     */
    public function actionView($position_id, $document_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($position_id, $document_id),
        ]);
    }

    /**
     * Creates a new PositionHasDocument model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PositionHasDocument();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $fieldService = new FieldService();
            $fieldService->addDocumentByPosition($model->document_id,$model->position_id);

            return $this->redirect([
                'view',
                'position_id' => $model->position_id,
                'document_id' => $model->document_id,
            ]);
        } else {
            return $this->render('create', [
                'model'       => $model,
                'positionMap' => $this->getPositionMap(),
                'documentMap' => $this->getDocumentMap(),
            ]);
        }
    }

    private function getPositionMap()
    {
        $positions = Position::find()->select(['position_id', 'name'])->all();

        return ArrayHelper::map($positions, 'position_id', 'name');
    }

    private function getDocumentMap()
    {
        return ArrayHelper::map(DocumentLayer::getDocumentList(), 'document_id', 'name');
    }

    /**
     * Updates an existing PositionHasDocument model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $position_id
     * @param integer $document_id
     * @return mixed
     */
    public function actionUpdate($position_id, $document_id)
    {
        $model = $this->findModel($position_id, $document_id);

        $fieldService = new FieldService();
        $fieldService->addDocumentByPosition($model->document_id,$model->position_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'view',
                'position_id' => $model->position_id,
                'document_id' => $model->document_id,
            ]);
        } else {
            return $this->render('update', [
                'model'       => $model,
                'positionMap' => $this->getPositionMap(),
                'documentMap' => $this->getDocumentMap(),
            ]);
        }
    }

    /**
     * Deletes an existing PositionHasDocument model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $position_id
     * @param integer $document_id
     * @return mixed
     */
    public function actionDelete($position_id, $document_id)
    {
        $this->findModel($position_id, $document_id)->delete();

        $fieldService = new FieldService();
        $fieldService->deleteDocumentByPosition($document_id,$position_id);

        return $this->redirect(['index']);
    }

    /**
     * Finds the PositionHasDocument model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $position_id
     * @param integer $document_id
     * @return PositionHasDocument the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($position_id, $document_id)
    {
        if (($model = PositionHasDocument::findOne([
                'position_id' => $position_id,
                'document_id' => $document_id,
            ])) !== null
        ) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
