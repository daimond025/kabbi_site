<?php

namespace app\modules\setting\controllers;

use app\modules\setting\models\MobileAppSearch;
use backend\modules\setting\models\WorkerBalanceNotification;
use backend\modules\setting\models\WorkerBalanceNotificationSearch;
use common\modules\employee\models\worker\Worker;
use app\modules\setting\models\MobileApp;
use Yii;
use app\modules\setting\models\Tenant;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use app\modules\setting\models\TenantSettingSearch;
use app\modules\setting\models\TenantSetting;
use app\modules\setting\models\TenantAdminTariff;
use app\modules\setting\models\TenantHasAdminTariff;
use app\modules\setting\models\TenantHasAdminTariffAdditionalOption;
use app\modules\setting\models\AdminTariffAdditionalOption;
use app\modules\setting\models\TenantAdminTariffChangeset;
use app\modules\setting\models\TenantPayment;
use yii\helpers\ArrayHelper;
use common\modules\car\models\Car;
use app\modules\setting\models\TenantHasCity;
use app\modules\setting\models\UserSearch;
use frontend\components\pushApi\PushApi;
use app\modules\setting\models\User;

/**
 * TenantController implements the CRUD actions for Tenant model.
 */
class TenantController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['tenantInfoRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['tenantInfo'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tenant models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel                        = new \app\modules\setting\models\TenantSearch();
        $dataProvider                       = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 50;

        $dataProvider->sort = ['defaultOrder' => ['create_time' => SORT_DESC]];

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tenant model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        $driversQuantity            = Worker::find()->where(['tenant_id' => $id])->count();
        $usersQuantity              = \app\modules\support\models\User::quantityByTenant($id);
        $carsQuantity               = Car::find()->where(['tenant_id' => $id])->count();
        $city_list                  = TenantHasCity::find()->with('city')->where(['tenant_id' => $id])->all();
        $tenantPayments             = TenantPayment::find()->where(['tenant_id' => $id]);
        $tenantPaymentsDataProvider = new ActiveDataProvider([
            'query'      => $tenantPayments,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);
        $tenantPaymentsTotal        = TenantPayment::getTotalCosts($tenantPaymentsDataProvider);
        $changesets                 = TenantAdminTariffChangeset::find()->where(['tenant_id' => $id])->with('tariffIdNew')->with('tariffIdOld')->orderBy(['create_date' => SORT_DESC]);
        $changesetsDataProvider     = new ActiveDataProvider([
            'query' => $changesets,
        ]);
        $SettingsSearchModel        = new TenantSettingSearch();
        $UserSearch                 = new UserSearch();
        $tenant_has_admin_tariff    = TenantHasAdminTariff::findOne(['tenant_id' => $id])['tariff_id'];
        $settingModel               = new TenantSetting();
        $SettingsdataProvider       = $SettingsSearchModel->search(['tenant_id' => $id]);
        $UsersDataProvider          = $UserSearch->search(['tenant_id' => $id]);
        $model                      = Tenant::findOne($id);
        if(!isset($model)){
            throw new NotFoundHttpException();
        }
        $tariff                     = TenantAdminTariff::find(['block' => 0])->all();
        $tenantApps                 = new MobileAppSearch();
        $tenantAppDataProvider      = $tenantApps->search(['tenant_id' => $id]);

        $workerBalanceNotifications = (new WorkerBalanceNotificationSearch())->search($id);

        if (isset($model) && $model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->tenant_id]);
        } elseif ($settingModel->load(Yii::$app->request->post()) && $settingModel->save()) {
            return $this->redirect(['view', 'id' => $model->tenant_id]);
        } else {
            return $this->render('view', [
                'model'                      => $model,
                'SettingsDataProvider'       => $SettingsdataProvider,
                'UsersDataProvider'          => $UsersDataProvider,
                //'payments'                   => $payments,
                'tariff'                     => $tariff,
                'city_list'                  => $city_list,
                'carsQuantity'               => $carsQuantity,
                'driversQuantity'            => $driversQuantity,
                'usersQuantity'              => $usersQuantity,
                'tenantPaymentsTotal'        => $tenantPaymentsTotal,
                'tenantPaymentsDataProvider' => $tenantPaymentsDataProvider,
                'tenant_has_admin_tariff'    => $tenant_has_admin_tariff,
                'changesetsDataProvider'     => $changesetsDataProvider,
                'tenantAppDataProvider'      => $tenantAppDataProvider,
                'workerBalanceNotifications' => $workerBalanceNotifications,
            ]);
        }
    }

    /**
     *
     * @param type $tenant_id
     * @param type $amount
     *
     * @return string return an answer on success
     */
    public function actionAddPayment()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        $beginPeriod                        = post()['beginPeriod'];
        $title                              = post()['title'];
        $tenant_id                          = post()['tenant_id'];
        $amount                             = post()['amount'];
        $period                             = post()['period'];
        $model                              = new TenantPayment();
        $model->payment_item                = $title;
        $model->expiration_date             = $model->setExpirationDate($beginPeriod, $period);
        $model->amount                      = $amount;
        $model->tenant_id                   = $tenant_id;
        $model->period                      = $period;
        $model->create_date                 = new \yii\db\Expression('NOW()');
        $model->payment_activity_begin_date = $beginPeriod;
        if ($model->save()) {
            return self::actionUpdateTable($tenant_id);
        } else {
            dd($model->save());
        }
    }

    /**
     * Creates a new Tenant model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tenant();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->tenant_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     *
     * @param type $name
     * @param type $value
     * @param type $settingId
     *
     * @return string return an answer on success
     */
    public function actionUpdateSetting()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        $settingId    = post('settingId');
        $value        = post('value');
        $model        = TenantSetting::findOne(['setting_id' => $settingId]);
        $model->value = $value;
        if ($model->save()) {
            return 'Успешно';
        }
    }

    public function actionSetTenantAdminTariff()
    {
        if (Yii::$app->request->getIsAjax()) {
            $tariff        = TenantAdminTariff::findOne(['tariff_id' => post()['tariff_id']]);
            $currentTariff = TenantHasAdminTariff::findOne(['tenant_id' => post()['tenant_id']]);
            if ($currentTariff->tariff_id == post()['tariff_id'] && $currentTariff->tenant_id == post()['tenant_id']) {
                return self::updateChangesetsTable(post()['tenant_id'],
                    'Этот тариф уже установлен для выбранного арендатора');
            }
            if ($tariff->block == 1) {
                return self::updateChangesetsTable(post()['tenant_id'],
                    'Невозможно применить заблокированный тариф, разблокируйте его или же выберите другой');
            } elseif ($tariff->block == 0) {
                if (!is_null($currentTariff)) {
                    $changesetModel                = new TenantAdminTariffChangeset();
                    $changesetModel->tariff_id_old = TenantHasAdminTariff::find()->where(['tenant_id' => $currentTariff->tenant_id])->orderBy(['create_date' => SORT_DESC])->one()->tariff_id;
                    $changesetModel->tenant_id     = post()['tenant_id'];
                    $changesetModel->tariff_id_new = post()['tariff_id'];
                    $changesetModel->create_date   = new \yii\db\Expression('NOW()');
                    $changesetModel->save();
                    $currentTariff->delete();
                }
                $model              = new TenantHasAdminTariff();
                $model->tenant_id   = post()['tenant_id'];
                $model->tariff_id   = post()['tariff_id'];
                $model->create_date = new \yii\db\Expression('NOW()');
                $model->save();

                return self::updateChangesetsTable(post()['tenant_id'], 'Новый тариф успешно применен');
            } else {
                return 'Произошла ошибка, повторите еще раз';
            }
        }
    }

    public function actionUpdateTable($tenant_id = null)
    {
        $tenantPayments = TenantPayment::find()->where(['tenant_id' => !empty($tenant_id) ? $tenant_id : get()['tenant_id']]);
        if (!is_null(get()['beginPeriod']) && !is_null(get()['beginPeriod'])) {
            $beginPeriod = str_replace('.', '-', get()['beginPeriod']);
            $endPeriod   = str_replace('.', '-', get()['endPeriod']);
            $tenantPayments->andWhere('create_date >= "' . $beginPeriod . ' "' . ' AND create_date <= "' . $endPeriod . ' 23:59:59"');
        }
        $tenantPaymentsDataProvider = new ActiveDataProvider([
            'query'      => $tenantPayments,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);
        $totalCosts                 = TenantPayment::getTotalCosts($tenantPaymentsDataProvider);
        $view                       = $this->renderAjax('_payments_table', ['payments' => $tenantPaymentsDataProvider]);

        return json_encode(compact('view', 'totalCosts'));
    }

    /**
     * adding a new additional option
     * @return string on error or "view" on success
     */
    public function actionSetAdditionalOption()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        $tenant_id          = post()['tenant_id'];
        $option_id          = post()['option_id'];
        $model              = new TenantHasAdminTariffAdditionalOption();
        $model->option_id   = $option_id;
        $model->tenant_id   = $tenant_id;
        $model->create_date = new \yii\db\Expression('NOW()');
        if ($model->save()) {
            return self::updateAdditionalOptionTable($tenant_id);
        } else {
            return 'Произошла ошибка';
        }
    }

    public function updateChangesetsTable($tenant_id, $message)
    {
        $changesets             = TenantAdminTariffChangeset::find()->where(['tenant_id' => $tenant_id])->orderBy(['create_date' => SORT_DESC]);
        $changesetsDataProvider = new ActiveDataProvider([
            'query' => $changesets,
        ]);
        $view                   = $this->renderAjax('_table_tariff_changesets',
            ['changesets' => $changesetsDataProvider, 'message' => $message]);

        return json_encode(compact('view'));
    }

    public function updateAdditionalOptionTable($tenant_id)
    {
        $hasAdditionalOptions          = TenantHasAdminTariffAdditionalOption::find()->where(['tenant_id' => $tenant_id]);
        $additionalOptionsDataProvider = new ActiveDataProvider([
            'query'      => $hasAdditionalOptions,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
        $view                          = $this->renderAjax('_table_has_add_options',
            ['hasAdditionalOptions' => $additionalOptionsDataProvider]);

        return json_encode(compact('view'));
    }

    /**
     * Updates an existing Tenant model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $changesets                    = TenantAdminTariffChangeset::find()->with('tariffIdNew')->with('tariffIdOld')->where(['tenant_id' => $id])->orderBy(['create_date' => SORT_DESC]);
        $changesetsDataProvider        = new
        ActiveDataProvider([
            'query'      => $changesets,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
        $hasAdditionalOptions          = TenantHasAdminTariffAdditionalOption::find()->where(['tenant_id' => $id]);
        $additionalOptionsDataProvider = new ActiveDataProvider([
            'query'      => $hasAdditionalOptions,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        $tenantAdditionalOptions    = ArrayHelper::map(AdminTariffAdditionalOption::cachedTable(), 'option_id', 'name');
        $tenantPayments             = TenantPayment::find()->where(['tenant_id' => $id]);
        $tenantPaymentsDataProvider = new ActiveDataProvider([
            'query'      => $tenantPayments,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);
        $tenantPaymentsTotal        = TenantPayment::getTotalCosts($tenantPaymentsDataProvider);
        $SettingsSearchModel        = new TenantSettingSearch();


        if(!empty(get()['TenantSettingSearch']) && Yii::$app->request->isAjax){
            $SettingsdataProvider       = $SettingsSearchModel->search(get());
        }

        $UserSearch                 = new UserSearch();
        $UsersDataProvider          = $UserSearch->search(['tenant_id' => $id]);
        $tenant_has_admin_tariff    = TenantHasAdminTariff::findOne(['tenant_id' => $id])['tariff_id'];
        $settingModel               = new TenantSetting();
        $SettingsdataProvider       = $SettingsSearchModel->search(['tenant_id'=>$id]);
        $model                      = Tenant::findOne($id);
        if (!isset($model)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $tariff = TenantAdminTariff::find(['block' => 0])->all();
        $host   = $_SERVER['HTTP_HOST'];
        $host   = explode('.', $host);
        if ($_SERVER["HTTPS"] == "on") {
            $logoLink = 'https://';
        } else {
            $logoLink = 'http://';
        }
        //$logoLink .= $host[1] . '.' . $host[2] . '/upload/' . $id . '/' . $model->logo;

        if (isset($model) && $model->load(Yii::$app->request->post()) && $model->save()) {
            $model->create_time = new \yii\db\Expression('NOW()');
            return $this->redirect(['view', 'id' => $model->tenant_id]);
        } elseif ($settingModel->load(Yii::$app->request->post()) && $settingModel->save()) {
            return $this->redirect(['view', 'id' => $model->tenant_id]);
        }

        return $this->render('update', [
            'model'                               => $model,
            'SettingsDataProvider'                => $SettingsdataProvider,
            'UsersDataProvider'                   => $UsersDataProvider,
            'payments'                            => $payments,
            'tenantPaymentsTotal'                 => $tenantPaymentsTotal,
            'tenantPaymentsDataProvider'          => $tenantPaymentsDataProvider,
            'tariff'                              => $tariff,
            'tenant_has_admin_tariff'             => $tenant_has_admin_tariff,
            'changesets'                          => $changesetsDataProvider,
            'hasAdditionalOptions'                => $additionalOptionsDataProvider,
            'tenantAdditionalOptionsDataProvider' => $tenantAdditionalOptionsDataProvider,
            'additionalOptions'                   => $tenantAdditionalOptions,
            'settingSearchModel'                  => $SettingsSearchModel,
        ]);


    }

    public function actionDeletePayment()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        $tenant_id  = get()['tenant_id'];
        $payment_id = get()['payment_id'];
        \app\modules\setting\models\TenantPayment::findOne($payment_id)->delete();

        return self::actionUpdateTable($tenant_id);
    }

    public function actionDeleteAdditionalOption()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        $tenant_id = get()['tenant_id'];
        $option_id = get()['option_id'];
        \app\modules\setting\models\TenantHasAdminTariffAdditionalOption::findOne($option_id)->delete();

        return self::updateAdditionalOptionTable($tenant_id);
    }

    /**
     * Deletes an existing Tenant model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
            \Yii::$app->getSession()->setFlash('success', 'Арендатор успешно удален');
        } catch (yii\db\IntegrityException $exc) {
            \Yii::$app->getSession()->setFlash('error', 'Нельзя удалить запись. Есть дочерние связи.');
        }

        return $this->redirect(['index']);
    }


    public function actionViewPageInfo($id)
    {
        $model = MobileApp::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException();
        }

        echo (string)$model->page_info;
    }

    public function actionViewConfidential($id)
    {
        $model = MobileApp::findOne($id);

        if (!$model) {
            throw new NotFoundHttpException();
        }

        echo (string)$model->confidential;
    }

    public function actionViewMobileApp($id)
    {
        $model = MobileApp::findOne($id);

        if ($model) {
            return $this->render('mobile-app/view', ['model' => $model]);
        }

        throw new NotFoundHttpException();
    }

    public function actionUpdateMobileApp($id)
    {
        $model = MobileApp::findOne($id);


        $pushKeyIos = $model->push_key_ios;
        $pushKeyAndroid = $model->push_key_android;

        if ($model->load(post())) {
            $pushApi = new PushApi(['url'=>getenv('API_PUSH_URL')]);

            if($model->push_key_ios !== $pushKeyIos){
                $pushApi->destroyIosClientAppSenderService($id);
            }
            if($model->push_key_android !== $pushKeyAndroid){
                $pushApi->destroyAndroidClientAppSenderService($id);
            }
            if($model->save()) {
                $this->redirect(['view-mobile-app', 'id' => $id]);
            }
        }

        $cityList = TenantHasCity::find()->with('city')->where(['tenant_id' => $model->tenant_id])->indexBy('city_id')->all();

        $cityList = array_map(function ($item) {
            $name = Html::encode($item->city->name);

            return $item->block ? Html::tag('span', $name, ['class' => 'text-red']) : $name;
        }, $cityList);


        if ($model) {
            return $this->render('mobile-app/update',
                ['model' => $model, 'cityList' => $cityList]);
        }

        throw new NotFoundHttpException();
    }

    public function actionCreateMobileApp($id)
    {
        $model = new MobileApp(['tenant_id' => $id, 'active' => 0, 'sort' => 100]);

        if ($model->load(post()) && $model->save()) {
            $this->redirect(['view-mobile-app', 'id' => $model->app_id]);
        }

        $cityList = TenantHasCity::find()->with('city')->where(['tenant_id' => $model->tenant_id])->indexBy('city_id')->all();

        $cityList = array_map(function ($item) {
            $name = Html::encode($item->city->name);

            return $item->block ? Html::tag('span', $name, ['class' => 'text-red']) : $name;
        }, $cityList);

        if ($model) {
            return $this->render('mobile-app/create', ['model' => $model, 'cityList' => $cityList]);
        }

        throw new NotFoundHttpException();
    }

    public function actionCreateNotification($id)
    {
        $model = new WorkerBalanceNotification(['tenant_id' => $id]);

        if ($model->load(post()) && $model->save()) {
            $this->redirect(['update-notification', 'id' => $model->id]);
        }

        return $this->render('worker-balance-notification/create', ['model' => $model]);
    }

    public function actionUpdateNotification($id)
    {
        $model = WorkerBalanceNotification::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException();
        }

        if ($model->load(post()) && $model->save()) {
            $this->redirect(['update-notification', 'id' => $model->id]);
        }

        return $this->render('worker-balance-notification/update', ['model' => $model]);
    }

    public function actionDeleteNotification($id)
    {
        $model = WorkerBalanceNotification::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException();
        }

        $tenantId = $model->tenant_id;

        $model->delete();

        return $this->redirect(['/setting/tenant/view', 'id' => $tenantId]);
    }

    /**
     * Finds the Tenant model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Tenant the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    private function findModel($id)
    {
        if (($tenantModel = Tenant::findOne($id)) !== null) {
            return $tenantModel;
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUpdateUser($id)
    {

        $user = User::findOne($id);

        if(isset($user)) {

            $user->fullName = getFullName($user->last_name, $user->name, $user->second_name);

            $cityList = $user->getTenantCities();

            $user->cities = $user->getUserIdCities();

            if ($user->load(Yii::$app->request->post())) {

                $explodeFullName = $user->getExplodeFullName($user->fullName);

                $user->last_name = $explodeFullName[0];

                $user->name = $explodeFullName[1];

                $user->second_name = $explodeFullName[2];

                if($user->save()) {

                    $user->clearCities();

                    $user->addCities();

                    $this->redirect(['tenant/view-user/' . $id]);
                }
            }

            return $this->render('users-data/update', ['model' => $user, 'cityList' => $cityList]);

        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }

    }

    public function actionViewUser($id)
    {
        $model = User::findOne($id);

        if ($model) {
            return $this->render('users-data/view', ['model' => $model]);
        }

        throw new NotFoundHttpException();
    }

}
