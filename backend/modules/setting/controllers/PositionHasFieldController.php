<?php

namespace app\modules\setting\controllers;

use Yii;
use common\modules\employee\models\position\PositionHasField;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\modules\employee\models\position\Position;
use common\modules\employee\models\position\PositionField;
use yii\helpers\ArrayHelper;

/**
 * PositionHasFieldController implements the CRUD actions for PositionHasField model.
 */
class PositionHasFieldController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['employeeRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['employee'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PositionHasField models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => PositionHasField::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PositionHasField model.
     * @param integer $position_id
     * @param integer $field_id
     * @return mixed
     */
    public function actionView($position_id, $field_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($position_id, $field_id),
        ]);
    }

    /**
     * Creates a new PositionHasField model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PositionHasField();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'position_id' => $model->position_id, 'field_id' => $model->field_id]);
        } else {
            $positions = ArrayHelper::map(Position::find()->all(), 'position_id', 'name');
            $fields = ArrayHelper::map(PositionField::find()->all(), 'field_id', 'name');
            return $this->render('create', [
                'model'     => $model,
                'positions' => $positions,
                'fields'    => $fields,
            ]);
        }
    }

    /**
     * Updates an existing PositionHasField model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $position_id
     * @param integer $field_id
     * @return mixed
     */
    public function actionUpdate($position_id, $field_id)
    {
        $model = $this->findModel($position_id, $field_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'position_id' => $model->position_id, 'field_id' => $model->field_id]);
        } else {
            $positions = ArrayHelper::map(Position::find()->all(), 'position_id', 'name');
            $fields = ArrayHelper::map(PositionField::find()->all(), 'field_id', 'name');
            return $this->render('update', [
                'model'     => $model,
                'positions' => $positions,
                'fields'    => $fields,
            ]);
        }
    }

    /**
     * Deletes an existing PositionHasField model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $position_id
     * @param integer $field_id
     * @return mixed
     */
    public function actionDelete($position_id, $field_id)
    {
        $this->findModel($position_id, $field_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PositionHasField model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $position_id
     * @param integer $field_id
     * @return PositionHasField the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($position_id, $field_id)
    {
        if (($model = PositionHasField::findOne(['position_id' => $position_id, 'field_id' => $field_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
