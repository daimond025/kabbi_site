<?php

namespace app\modules\setting\controllers;

use backend\modules\setting\forms\DefaultWorkerNotificationCreateForm;
use backend\modules\setting\forms\DefaultWorkerNotificationForm;
use backend\modules\setting\forms\search\DefaultWorkerPushNotificationSearch;
use backend\modules\setting\helpers\NotificationHelper;
use backend\modules\setting\services\WorkerPushNotificationService;
use frontend\modules\employee\components\position\PositionService;
use landing\modules\community\components\filters\AccessControl;
use yii\base\Module;
use yii\filters\VerbFilter;
use yii\web\Controller;

class DefaultWorkerPushNotificationController extends Controller
{
    private $positionService;
    private $workerPushNotificationService;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['messageTemplatesRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['messageTemplates'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function __construct(
        $id,
        Module $module,
        PositionService $positionService,
        WorkerPushNotificationService $workerPushNotificationService,
        array $config = []
    ) {
        parent::__construct($id, $module, $config);

        $this->positionService               = $positionService;
        $this->workerPushNotificationService = $workerPushNotificationService;
    }

    public function actionIndex()
    {
        $searchModel  = new DefaultWorkerPushNotificationSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'typesMap'     => NotificationHelper::getStatusMap(),
            'paramsMap'    => NotificationHelper::getParamsMap(),
            'positionsMap' => $this->positionService->getPositionMap(),
        ]);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function actionCreate()
    {
        $formModel = new DefaultWorkerNotificationCreateForm();

        if ($formModel->load(post()) && $formModel->validate()) {
            $id = $this->workerPushNotificationService->createDefaultNotification($formModel);

            return $this->redirect(['view', 'id' => $id]);
        }

        return $this->render('create', [
            'formModel'    => $formModel,
            'typesMap'     => NotificationHelper::getStatusMap(),
            'paramsMap'    => NotificationHelper::getParamsMap(),
            'positionsMap' => $this->positionService->getPositionMap(),
        ]);
    }

    /**
     * @param integer $id
     *
     * @return string
     * @throws \Exception
     */
    public function actionUpdate($id)
    {
        $model     = $this->workerPushNotificationService->findDefaultNotification($id);
        $formModel = new DefaultWorkerNotificationForm($model);

        if ($formModel->load(post()) && $formModel->validate()) {
            $this->workerPushNotificationService->updateDefaultNotification($id, $formModel);

            return $this->redirect(['view', 'id' => $id]);
        }

        return $this->render('update', [
            'formModel'    => $formModel,
            'typesMap'     => NotificationHelper::getStatusMap(),
            'paramsMap'    => NotificationHelper::getParamsMap(),
            'positionsMap' => $this->positionService->getPositionMap(),
        ]);
    }

    public function actionView($id)
    {
        $model     = $this->workerPushNotificationService->findDefaultNotification($id);
        $formModel = new DefaultWorkerNotificationForm($model);

        return $this->render('view', [
            'formModel'    => $formModel,
            'typesMap'     => NotificationHelper::getStatusMap(),
            'paramsMap'    => NotificationHelper::getParamsMap(),
            'positionsMap' => $this->positionService->getPositionMap(),
        ]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \Exception
     */
    public function actionDelete($id)
    {
        if (!\Yii::$app->request->isPost) {
            return false;
        }

        $this->workerPushNotificationService->deleteDefaultNotification($id);
        session()->setFlash('success', 'Шаблон успешно удален');

        return $this->redirect(['index']);
    }
}