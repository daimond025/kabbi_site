<?php

namespace app\modules\setting\controllers;

use app\modules\setting\models\DefaultSmsTemplateSearch;
use common\modules\tenant\helpers\SmsTemplateParamsHelper;
use common\modules\tenant\models\DefaultSmsTemplate;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use frontend\modules\employee\components\position\PositionService;

/**
 * SmsTemplateController implements the CRUD actions for DefaultSmsTemplate model.
 */
class SmsTemplateController extends Controller
{
    private $positionService;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['messageTemplatesRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['messageTemplates'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function __construct($id, Module $module, PositionService $positionService, array $config = [])
    {
        $this->positionService = $positionService;

        parent::__construct($id, $module, $config);
    }

    /**
     * Lists all DefaultSmsTemplate models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new DefaultSmsTemplateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'positionMap'  => $this->getPositionMap(),
        ]);
    }

    private function getPositionMap()
    {
        $positions = $this->positionService->getAllPositions();

        return empty($positions) ? [] : ArrayHelper::map($positions, 'position_id', 'name');
    }

    /**
     * Displays a single DefaultSmsTemplate model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model'     => $this->findModel($id),
            'paramsMap' => SmsTemplateParamsHelper::getParamsMap(),
        ]);
    }

    /**
     * Creates a new DefaultSmsTemplate model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DefaultSmsTemplate(['params' => []]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            session()->setFlash('success', 'Шаблон успешно создан');

            return $this->redirect(['view', 'id' => $model->template_id]);
        } else {
            return $this->render('create', [
                'model'       => $model,
                'positionMap' => $this->getPositionMap(),
                'paramsMap'   => SmsTemplateParamsHelper::getParamsMap(),
            ]);
        }
    }

    /**
     * Updates an existing DefaultSmsTemplate model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            session()->setFlash('success', 'Шаблон успешно изменен');

            return $this->redirect(['view', 'id' => $model->template_id]);
        } else {
            return $this->render('update', [
                'model'       => $model,
                'positionMap' => $this->getPositionMap(),
                'paramsMap'   => SmsTemplateParamsHelper::getParamsMap(),
            ]);
        }
    }

    /**
     * Deletes an existing DefaultSmsTemplate model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        session()->setFlash('success', 'Шаблон успешно удален');

        return $this->redirect(['index']);
    }

    /**
     * Finds the DefaultSmsTemplate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return DefaultSmsTemplate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DefaultSmsTemplate::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
