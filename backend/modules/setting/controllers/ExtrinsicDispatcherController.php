<?php

namespace app\modules\setting\controllers;


use backend\modules\setting\models\entities\TenantForExtrinsicDispatcher;
use backend\modules\setting\models\entities\UserExtrinsicDispatcher;
use backend\modules\setting\models\search\UserExtrinsicDispatcherSearch;
use common\components\extensions\ModelMultiple;
use common\services\transfer\LanguagesService;
use common\services\user\access\RoleService;
use yii\base\Model;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;

class ExtrinsicDispatcherController extends Controller
{

    protected $roleService;
    protected $languagesService;

    public function __construct(
        $id,
        Module $module,
        RoleService $roleService,
        LanguagesService $languagesService,
        array $config = []
    )
    {
        $this->roleService = $roleService;
        $this->languagesService = $languagesService;

        parent::__construct($id, $module, $config);
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['extrinsicDispatcherRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['extrinsicDispatcher'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel  = new UserExtrinsicDispatcherSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $model = $this->getExtrinsicDispatcherModel($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $extrinsicDispatcher = $this->getExtrinsicDispatcherModel($id);
        $extrinsicDispatcher->scenario = UserExtrinsicDispatcher::SCENARIO_UPDATE;

        $tenants = $extrinsicDispatcher->getSavedTenant()->all();

        if (Yii::$app->request->isPost) {
            $extrinsicDispatcher->load(Yii::$app->request->post());
            $tenants = ModelMultiple::createMultiple(
                TenantForExtrinsicDispatcher::class, [],
                'tenant_id',
                    ['domain' => $extrinsicDispatcher->getDomain()]
            );
            Model::loadMultiple($tenants, Yii::$app->request->post());

            $valid = $extrinsicDispatcher->validate();
            $valid = Model::validateMultiple($tenants) && $valid;

            if ($valid) {
                $extrinsicDispatcher->tenants_relation = $tenants;
                $transaction = app()->db->beginTransaction();
                if ($extrinsicDispatcher->save(false)) {

                    $transaction->commit();
                    return $this->redirect(['view', 'id' => $extrinsicDispatcher->user_id]);
                } else {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('update', [
            'extrinsicDispatcher' => $extrinsicDispatcher,
            'tenants' => $tenants,
            'lang'  => $this->languagesService->getSupportedLanguages()
        ]);
    }

    public function actionCreate()
    {
        $extrinsicDispatcher = new UserExtrinsicDispatcher();
        $tenants = [new TenantForExtrinsicDispatcher()];


        if (Yii::$app->request->isPost) {
            $extrinsicDispatcher->load(Yii::$app->request->post());
            $tenants = ModelMultiple::createMultiple(
                TenantForExtrinsicDispatcher::class, [],
                'tenant_id',
                ['domain' => $extrinsicDispatcher->getDomain()]
            );
            Model::loadMultiple($tenants, Yii::$app->request->post());

            $valid = $extrinsicDispatcher->validate();
            $valid = Model::validateMultiple($tenants) && $valid;

            if ($valid) {
                $extrinsicDispatcher->tenants_relation = $tenants;
                $transaction = app()->db->beginTransaction();
                if ($extrinsicDispatcher->save(false)) {
                    $this->roleService->setRoleByPosition($extrinsicDispatcher,
                        UserExtrinsicDispatcher::POSITION_EXTRINSIC_DISPATCHER);

                    $transaction->commit();
                    return $this->redirect(['view', 'id' => $extrinsicDispatcher->user_id]);
                } else {
                    $transaction->rollBack();
                }
            }
        }

        return $this->render('create', [
            'extrinsicDispatcher' => $extrinsicDispatcher,
            'tenants' => $tenants,
            'lang'  => $this->languagesService->getSupportedLanguages()
        ]);
    }

    public function actionDelete($id)
    {
        $this->getExtrinsicDispatcherModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function getExtrinsicDispatcherModel($id)
    {
        $extrinsicDispatcher = UserExtrinsicDispatcher::findOne($id);
        if (!$extrinsicDispatcher instanceof UserExtrinsicDispatcher) {
            throw new NotFoundHttpException();
        }

        return $extrinsicDispatcher;

    }

}