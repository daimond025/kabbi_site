<?php

namespace app\modules\setting\controllers;

use backend\modules\setting\forms\DefaultRecipientSmsTemplateCreateForm;
use backend\modules\setting\forms\DefaultRecipientSmsTemplateForm;
use backend\modules\setting\forms\search\DefaultRecipientSmsTemplateSearch;
use backend\modules\setting\helpers\NotificationHelper;
use backend\modules\setting\services\RecipientSmsTemplateService;
use frontend\modules\employee\components\position\PositionService;
use landing\modules\community\components\filters\AccessControl;
use yii\base\Module;
use yii\filters\VerbFilter;
use yii\web\Controller;

class DefaultRecipientSmsTemplateController extends Controller
{
    private $positionService;
    private $recipientSmsTemplateService;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['messageTemplatesRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['messageTemplates'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function __construct(
        $id,
        Module $module,
        PositionService $positionService,
        RecipientSmsTemplateService $recipientSmsTemplateService,
        array $config = []
    ) {
        parent::__construct($id, $module, $config);

        $this->positionService             = $positionService;
        $this->recipientSmsTemplateService = $recipientSmsTemplateService;
    }

    public function actionIndex()
    {
        $searchModel  = new DefaultRecipientSmsTemplateSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'typesMap'     => NotificationHelper::getStatusMap(),
            'paramsMap'    => NotificationHelper::getParamsMap(),
            'positionsMap' => $this->positionService->getPositionMap(),
        ]);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function actionCreate()
    {
        $formModel = new DefaultRecipientSmsTemplateCreateForm();

        if ($formModel->load(post()) && $formModel->validate()) {
            $id = $this->recipientSmsTemplateService->createDefaultNotification($formModel);

            return $this->redirect(['view', 'id' => $id]);
        }

        return $this->render('create', [
            'formModel'    => $formModel,
            'typesMap'     => NotificationHelper::getStatusMap(),
            'paramsMap'    => NotificationHelper::getParamsMap(),
            'positionsMap' => $this->positionService->getPositionMap(),
        ]);
    }

    /**
     * @param integer $id
     *
     * @return string
     * @throws \Exception
     */
    public function actionUpdate($id)
    {
        $model     = $this->recipientSmsTemplateService->findDefaultNotification($id);
        $formModel = new DefaultRecipientSmsTemplateForm($model);

        if ($formModel->load(post()) && $formModel->validate()) {
            $this->recipientSmsTemplateService->updateDefaultNotification($id, $formModel);

            return $this->redirect(['view', 'id' => $id]);
        }

        return $this->render('update', [
            'formModel'    => $formModel,
            'typesMap'     => NotificationHelper::getStatusMap(),
            'paramsMap'    => NotificationHelper::getParamsMap(),
            'positionsMap' => $this->positionService->getPositionMap(),
        ]);
    }

    public function actionView($id)
    {
        $model     = $this->recipientSmsTemplateService->findDefaultNotification($id);
        $formModel = new DefaultRecipientSmsTemplateForm($model);

        return $this->render('view', [
            'formModel'    => $formModel,
            'typesMap'     => NotificationHelper::getStatusMap(),
            'paramsMap'    => NotificationHelper::getParamsMap(),
            'positionsMap' => $this->positionService->getPositionMap(),
        ]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws \Exception
     */
    public function actionDelete($id)
    {
        if (!\Yii::$app->request->isPost) {
            return null;
        }

        $this->recipientSmsTemplateService->deleteDefaultNotification($id);
        session()->setFlash('success', 'Шаблон успешно удален');

        return $this->redirect(['index']);
    }
}
