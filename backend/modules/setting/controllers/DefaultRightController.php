<?php

namespace app\modules\setting\controllers;

use app\modules\setting\models\UserDefaultRightSearch;
use Yii;
use app\modules\setting\models\UserDefaultRight;
use yii\base\Model;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * DefaultRightController implements the CRUD actions for UserDefaultRight model.
 */
class DefaultRightController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['tenantRightsRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['tenantRights'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all UserDefaultRight models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserDefaultRightSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 50;

        return $this->render('index', [
                'dataProvider' => $dataProvider,
                'searchModel'  => $searchModel,
            ]
        );
    }

    /**
     * Displays a single UserDefaultRight model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => UserDefaultRight::findOne(['id' => $id]),
        ]);
    }

    /**
     * Creates a new UserDefaultRight model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserDefaultRight();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing UserDefaultRight model.
     * @param integer $id ID должности
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $models = UserDefaultRight::find()->where(['position_id' => $id])->indexBy('id')->all();

        if (empty($models)) {
            throw new NotFoundHttpException('Нет прав с position_id: ' . $id);
        }

        if (Model::loadMultiple($models, Yii::$app->request->post()) && Model::validateMultiple($models)) {
            foreach ($models as $model) {
                $model->save(false);
            }

            session()->setFlash('success', 'Права успешно обновлены');

            return $this->refresh();
        }

        $model = current(array_slice($models, 0, 1));
        $positionName = $model->position->name;

        return $this->render('update', ['models' => $models, 'positionName' => $positionName]);
    }
}
