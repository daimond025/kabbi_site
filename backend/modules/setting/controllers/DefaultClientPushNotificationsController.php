<?php

namespace app\modules\setting\controllers;

use app\modules\setting\models\DefaultClientPushNotificationsSearch;
use common\modules\tenant\models\DefaultClientPushNotification;
use common\modules\tenant\helpers\SmsTemplateParamsHelper;
use frontend\modules\employee\components\position\PositionService;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * DefaultClientPushNotificationsController implements the CRUD actions for DefaultClientPushNotifications model.
 */
class DefaultClientPushNotificationsController extends Controller
{
    private $positionService;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['messageTemplatesRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['messageTemplates'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function __construct($id, Module $module, PositionService $positionService, array $config = [])
    {
        $this->positionService = $positionService;

        parent::__construct($id, $module, $config);
    }

    private function getPositionMap()
    {
        $positions = $this->positionService->getAllPositions();

        return empty($positions) ? [] : ArrayHelper::map($positions, 'position_id', 'name');
    }

    /**
     * Lists all DefaultClientPushNotifications models.
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionIndex()
    {
        $searchModel  = new DefaultClientPushNotificationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'positionMap'  => $this->getPositionMap(),
        ]);
    }

    /**
     * Displays a single DefaultClientPushNotifications model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\base\InvalidParamException
     */
    public function actionView($id)
    {

        return $this->render('view', [
            'model'     => $this->findModel($id),
            'paramsMap' => SmsTemplateParamsHelper::getParamsMap(),
        ]);
    }

    /**
     * Creates a new DefaultClientPushNotifications model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\base\InvalidParamException
     */
    public function actionCreate()
    {
        $model = new DefaultClientPushNotification(['params' => []]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            session()->setFlash('success', 'Шаблон push-уведомления успешно создан');

            return $this->redirect(['view', 'id' => $model->push_id]);
        } else {
            return $this->render('create', [
                'model'       => $model,
                'positionMap' => $this->getPositionMap(),
                'paramsMap'   => SmsTemplateParamsHelper::getParamsMap(),
            ]);
        }
    }

    /**
     * Updates an existing DefaultClientPushNotifications model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\base\InvalidParamException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            session()->setFlash('success', 'Шаблон push-уведомления успешно изменен');

            return $this->redirect(['view', 'id' => $model->push_id]);
        } else {
            return $this->render('update', [
                'model'       => $model,
                'positionMap' => $this->getPositionMap(),
                'paramsMap'   => SmsTemplateParamsHelper::getParamsMap(),
            ]);
        }
    }

    /**
     * Deletes an existing DefaultClientPushNotifications model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \yii\db\StaleObjectException
     * @throws \Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        session()->setFlash('success', 'Шаблон push-уведомления успешно удален');

        return $this->redirect(['index']);
    }

    /**
     * Finds the DefaultClientPushNotifications model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return DefaultClientPushNotification the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DefaultClientPushNotification::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
