<?php

namespace app\modules\setting\controllers;

use Yii;
use common\modules\car\models\CarColor;
use app\modules\setting\models\CarColorSearch;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\modules\setting\models\SourceMessage;
use backend\modules\setting\models\Message;

/**
 * CarColorController implements the CRUD actions for CarColor model.
 */
class CarColorController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['transportRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['transport'],
                    ],
                ],
            ],
            'verbs'  => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CarColor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CarColorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort = ['defaultOrder' => ['name' => SORT_ASC]];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CarColor model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CarColor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CarColor();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            session()->setFlash('success', 'Цвет успешно добавлен');

            $transaction = app()->db->beginTransaction();
            try {
                $post_translate_map = post('translate');
                //Создаем фразу для перевода
                $source_message = new SourceMessage(['category' => CarColor::SOURCE_CATEGORY, 'message' => $model->name]);
                if (!$source_message->save()) {
                    throw new Exception;
                }

                //Добавляем переводы
                Message::batchInsert($source_message->id, $post_translate_map);

                $transaction->commit();
            } catch (Exception $exc) {
                session()->setFlash('error', 'Ошибка добавления переводов');
                $transaction->rollBack();
            }

            return $this->redirect(['view', 'id' => $model->color_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CarColor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $source_message = SourceMessage::getMessageWithTranslates($model->name, CarColor::SOURCE_CATEGORY);

        if ($model->load(Yii::$app->request->post())) {
            $old_class_attribute = $model->getOldAttribute('name');
            if ($model->save()) {
                $post_translate_map = post('translate');
                if (!empty($post_translate_map)) {
                    //Если изменилась фраза(цвет)
                    if ($old_class_attribute !== $model->name) {
                        $source_message->message = $model->name;
                        $source_message->save(true, ['message']);
                    }
                    //Обновление переводов
                    foreach ($post_translate_map as $language => $translation) {
                        if (!isset($source_message->translates[$language])) {
                            (new Message([
                                'id' => $source_message->id,
                                'language' => $language,
                                'translation' => $translation,
                                'message' => $model->name,
                                'category' => CarColor::SOURCE_CATEGORY,
                            ]))
                            ->save();
                        } elseif ($translation != $source_message->translates[$language]->language) {
                            $source_message->translates[$language]->translation = $translation;
                            $source_message->translates[$language]->save(true, ['translation']);
                        }
                    }
                }
                session()->setFlash('success', 'Данные обновлены.');
                return $this->refresh();
            }
        }

        return $this->render('update', [
            'model' => $model,
            'translates' => $source_message->translates
        ]);
    }

    /**
     * Deletes an existing CarColor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        //Удаляем фразу для перевода
        SourceMessage::deleteOne(['message' => $model->name, 'category' => CarColor::SOURCE_CATEGORY]);

        session()->setFlash('success', 'Цвет успешно удален');

        return $this->redirect(['index']);
    }

    /**
     * Finds the CarColor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \common\modules\car\models\CarColor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CarColor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
