<?php

namespace app\modules\setting\controllers;

use backend\modules\setting\forms\DefaultEmailSettingForm;
use backend\modules\setting\helpers\EmailSettingHelper;
use backend\modules\setting\services\dto\EmailProviderData;
use backend\modules\setting\services\dto\EmailSenderData;
use backend\modules\setting\services\EmailSettingService;
use yii\filters\AccessControl;
use yii\web\Controller;

class EmailController extends Controller
{
    private $emailSettingService;

    public function __construct(
        $id,
        $module,
        EmailSettingService $emailSettingService,
        $config = []
    ) {
        parent::__construct($id, $module, $config);

        $this->emailSettingService  = $emailSettingService;
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index'],
                        'roles'   => ['emailSettingsRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['emailSettings'],
                    ],
                ],
            ],
        ];
    }


    public function actionSystem()
    {
        $model      = $this->emailSettingService->findFromSystem();
        $formModel  = new DefaultEmailSettingForm($model);
        $allowWrite = \Yii::$app->user->can('emailSettings');

        if ($allowWrite && $formModel->load(post()) && $formModel->validate()) {
            $this->emailSettingService->saveFromSystem(new EmailProviderData(EmailSettingHelper::getProviderServer($formModel->provider),
                EmailSettingHelper::getProviderPort($formModel->provider)),
                new EmailSenderData($formModel->senderName, $formModel->senderEmail, $formModel->senderPassword),
                $formModel->template);
            session()->setFlash('success', 'Данные успешно отредактирваны');
        }

        return $this->render('from-system', ['formModel' => $formModel, 'allowWrite' => $allowWrite]);
    }

    public function actionTenant()
    {
        $model      = $this->emailSettingService->findFromTenant();
        $formModel  = new DefaultEmailSettingForm($model);
        $allowWrite = \Yii::$app->user->can('emailSettings');

        if ($allowWrite && $formModel->load(post()) && $formModel->validate()) {
            $this->emailSettingService->saveFromTenant(new EmailProviderData(EmailSettingHelper::getProviderServer($formModel->provider),
                EmailSettingHelper::getProviderPort($formModel->provider)),
                new EmailSenderData($formModel->senderName, $formModel->senderEmail, $formModel->senderPassword),
                $formModel->template);
            session()->setFlash('success', 'Данные успешно отредактирваны');
        }

        return $this->render('from-tenant', ['formModel' => $formModel, 'allowWrite' => $allowWrite]);
    }

}