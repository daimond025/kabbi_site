<?php

namespace app\modules\setting\controllers;

use app\modules\setting\models\PhoneLine;
use backend\modules\setting\components\TaxiTariffServiceComponent;
use backend\modules\setting\components\TenantServiceComponent;
use backend\modules\setting\models\outboundRouting\PhoneLineOutboundRoutingModel;
use backend\modules\setting\models\outboundRouting\PhoneLineOutboundRoutingSearch;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class OutboundRoutingController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['phoneLinesRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['phoneLines'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel  = new PhoneLineOutboundRoutingSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model           = $this->createModel();
        $model->scenario = PhoneLineOutboundRoutingModel::SCENARIO_CREATE;
        if ($model->load(post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->routing_id]);
        }

        return $this->render('create', [
            'model'         => $model,
            'cityList'      => ['' => 'Не выбрано'],
            'tariffList'    => [],
            'phoneLineList' => ['' => 'Не выбрано'],
        ]);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);
        if (!$model) {
            throw new NotFoundHttpException();
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (!$model) {
            throw new NotFoundHttpException();
        }

        $model->scenario = PhoneLineOutboundRoutingModel::SCENARIO_UPDATE;

        if ($model->load(post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->routing_id]);
        }


        /** @var TenantServiceComponent $tenantService */
        $tenantService = $this->module->get('tenantService');
        $cityList      = $tenantService->getCityList($model->tenant_id, false);

        /** @var TaxiTariffServiceComponent $taxiTariffService */
        $taxiTariffService = $this->module->get('taxiTariffService');
        $tariffList        = $taxiTariffService->getTariffList($model->tenant_id,
            $model->city_id);

        return $this->render('update', [
            'model'         => $model,
            'cityList'      => $cityList,
            'tariffList'    => $tariffList,
            'phoneLineList' => $model->getPhoneLineMap(),
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if (!$model) {
            return $this->redirect(['index']);
        }

        if ($model->delete()) {
            return $this->redirect(['index']);
        }

        return $this->redirect(['view', 'id' => $model->line_id]);
    }


    public function actionGetTenantId()
    {
        app()->response->format = Response::FORMAT_JSON;

        $domain = get('domain');

        if (!\Yii::$app->request->isAjax || !$domain) {
            return [];
        }

        /** @var TenantServiceComponent $tenantService */
        $tenantService = $this->module->get('tenantService');

        return (string)$tenantService->getTenantId($domain);
    }

    public function actionGetCityList()
    {
        app()->response->format = Response::FORMAT_JSON;

        $tenantId = get('tenantId');

        if (!\Yii::$app->request->isAjax || !$tenantId) {
            return [];
        }

        /** @var TenantServiceComponent $tenantService */
        $tenantService = $this->module->get('tenantService');

        return ArrayHelper::htmlEncode($tenantService->getCityList($tenantId, false));
    }

    public function actionGetTariffList()
    {
        app()->response->format = Response::FORMAT_JSON;

        $tenantId = get('tenantId');
        $cityId   = get('cityId');

        if (!\Yii::$app->request->isAjax || !$tenantId || !$cityId) {
            return [];
        }

        /** @var TaxiTariffServiceComponent $taxiTariffService */
        $taxiTariffService = $this->module->get('taxiTariffService');

        return ArrayHelper::htmlEncode($taxiTariffService->getTariffList($tenantId, $cityId));
    }

    public function actionGetPhoneLineList()
    {
        app()->response->format = Response::FORMAT_JSON;

        $tenantId = get('tenantId');
        $cityId   = get('cityId');

        if (!\Yii::$app->request->isAjax || !$tenantId || !$cityId) {
            return [];
        }


        $phoneLines = PhoneLine::find()
            ->where([
                'tenant_id' => $tenantId,
                'city_id'   => $cityId,
            ])
            ->all();

        return ArrayHelper::map($phoneLines, 'line_id', 'phone');
    }


    /**
     * @param $id
     *
     * @return array|null|PhoneLineOutboundRoutingModel
     */
    protected function findModel($id)
    {
        return PhoneLineOutboundRoutingModel::find()->where(['routing_id' => $id])->one();
    }

    protected function createModel()
    {
        return new PhoneLineOutboundRoutingModel();
    }
}