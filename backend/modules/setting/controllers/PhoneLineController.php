<?php

namespace app\modules\setting\controllers;

use backend\modules\setting\components\TaxiTariffServiceComponent;
use backend\modules\setting\components\TenantServiceComponent;
use common\components\voipApi\VoipApi;
use Yii;
use app\modules\setting\models\PhoneLine;
use app\modules\setting\models\PhoneLineSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * PhoneLineController implements the CRUD actions for PhoneLine model.
 */
class PhoneLineController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['phoneLinesRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['phoneLines'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PhoneLine models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PhoneLineSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PhoneLine model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PhoneLine model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model           = new PhoneLine();
        $model->scenario = PhoneLine::SCENARIO_CREATE;
        $model->isDraft  = true;

        $model->attributes = [
            'port'             => PhoneLine::DEFAULT_PORT,
            'sip_server_local' => PhoneLine::DEFAULT_SIP_SERVER_LOCAL,
            'typeServer'       => PhoneLine::TYPE_SERVER_ASTERISK,
            'realm'            => PhoneLine::DEFAULT_REALM,
            'block'            => 0,
            'draft'            => 1,
        ];

        if ($model->load(Yii::$app->request->post())) {
            $transaction = app()->db->beginTransaction();

            if ($model->save()) {
                $resp = [];
                if (!$model->isDraft) {
                    $paramsByVoipApi = $model->getParamsByVoipApi($model::TYPE_CREATE);
                    $resp            = app()->get('voipApi')->setPhoneLine($paramsByVoipApi, VoipApi::TYPE_CREATE);
                }

                if ($resp['state'] === VoipApi::STATE_ERROR) {
                    session()->setFlash('error', $resp['message']);
                } else {
                    $transaction->commit();

                    return $this->redirect(['view', 'id' => $model->line_id]);
                }
            }

            $transaction->rollBack();
            $model->isNewRecord = true;
            $model->unserializeIncomingScenario();
        }

        $cityList   = [];
        $tariffList = [];

        return $this->render('create', [
            'model'      => $model,
            'cityList'   => $cityList,
            'tariffList' => $tariffList,
        ]);

    }

    /**
     * Updates an existing PhoneLine model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (!$model) {
            throw new NotFoundHttpException();
        }

        $model->scenario = $model->isDraft ? $model::SCENARIO_UPDATE_FROM_DRAFT : $model::SCENARIO_UPDATE;

        $oldTypeServer     = $model->typeServer;
        $oldSipServerLocal = $model->sip_server_local;

        if ($model->load(Yii::$app->request->post())) {

            $isFromDraft = $model->getIsFromDraft();

            $transaction = app()->db->beginTransaction();

            if ($model->save()) {
                $resp = [];
                if ($isFromDraft) {
                    $paramsByVoipApi = $model->getParamsByVoipApi($model::TYPE_CREATE);
                    $resp            = app()->get('voipApi')->setPhoneLine($paramsByVoipApi, VoipApi::TYPE_CREATE);
                } else {
                    if (!$model->isDraft) {
                        $paramsByVoipApi                      = $model->getParamsByVoipApi($model::TYPE_UPDATE);
                        $paramsByVoipApi['typeServerOld']     = $oldTypeServer;
                        $paramsByVoipApi['sipServerLocalOld'] = $oldSipServerLocal;
                        $resp                                 = app()->get('voipApi')->setPhoneLine($paramsByVoipApi,
                            VoipApi::TYPE_UPDATE);
                    }
                }

                if ($resp['state'] === VoipApi::STATE_ERROR) {
                    session()->setFlash('error', $resp['message']);
                } else {
                    $transaction->commit();

                    return $this->redirect(['view', 'id' => $model->line_id]);
                }
            }

            $transaction->rollBack();
            $model->unserializeIncomingScenario();

        }

        /** @var TenantServiceComponent $tenantService */
        $tenantService = $this->module->get('tenantService');
        $cityList      = ArrayHelper::htmlEncode($tenantService->getCityList($model->tenant_id, false));

        /** @var TaxiTariffServiceComponent $taxiTariffService */
        $taxiTariffService = $this->module->get('taxiTariffService');
        $tariffList        = ArrayHelper::htmlEncode($taxiTariffService->getTariffList($model->tenant_id,
            $model->city_id));

        return $this->render('update', [
            'model'      => $model,
            'cityList'   => $cityList,
            'tariffList' => $tariffList,
        ]);
    }

    /**
     * Deletes an existing PhoneLine model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if (!$model) {
            return $this->redirect(['index']);
        }


        $transaction     = app()->db->beginTransaction();
        $paramsByVoipApi = $model->getParamsByVoipApi($model::TYPE_DELETE);
        $isDraft         = $model->isDraft;

        if ($model->delete()) {
            if (!$isDraft) {
                $response = app()->get('voipApi')->setPhoneLine($paramsByVoipApi, VoipApi::TYPE_DELETE);
                if (ArrayHelper::getValue($response, 'state') === 'ok') {
                    $transaction->commit();

                    return $this->redirect(['index']);
                } else {
                    $model->tenant_id = null;
                    session()->setFlash('error', $response['message']);
                }
            } else {
                $transaction->commit();

                return $this->redirect(['index']);
            }
        }

        return $this->redirect(['view', 'id' => $model->line_id]);
    }


    public function actionGetTenantId()
    {
        app()->response->format = Response::FORMAT_JSON;

        $domain = get('domain');

        if (!Yii::$app->request->isAjax || !$domain) {
            return [];
        }

        /** @var TenantServiceComponent $tenantService */
        $tenantService = $this->module->get('tenantService');

        return (string)$tenantService->getTenantId($domain);
    }

    /**
     * Получить список филиалов арендатора
     * @return array
     */
    public function actionGetCityList()
    {
        app()->response->format = Response::FORMAT_JSON;

        $tenantId = get('tenantId');

        if (!Yii::$app->request->isAjax || !$tenantId) {
            return [];
        }

        /** @var TenantServiceComponent $tenantService */
        $tenantService = $this->module->get('tenantService');

        return ArrayHelper::htmlEncode($tenantService->getCityList($tenantId, false));
    }

    /**
     * Получить список клиентских тарифов в филиале
     * @return array
     */
    public function actionGetTariffList()
    {
        app()->response->format = Response::FORMAT_JSON;

        $tenantId = get('tenantId');
        $cityId   = get('cityId');

        if (!Yii::$app->request->isAjax || !$tenantId || !$cityId) {
            return [];
        }

        /** @var TaxiTariffServiceComponent $taxiTariffService */
        $taxiTariffService = $this->module->get('taxiTariffService');

        return ArrayHelper::htmlEncode($taxiTariffService->getTariffList($tenantId, $cityId));
    }

    /**
     * Получить сценарий телефонных линий арендатора
     * @return string
     */
    public function actionGetPhoneLineScenario()
    {
        app()->response->format = Response::FORMAT_JSON;

        $tenantId = get('tenantId');

        if (!Yii::$app->request->isAjax || !$tenantId) {
            return '';
        }

        /** @var TenantServiceComponent $tenantService */
        $tenantService = $this->module->get('tenantService');

        return json_encode(ArrayHelper::htmlEncode($tenantService->getPhoneLineScenario($tenantId)));

    }

    /**
     * Finds the PhoneLine model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return PhoneLine the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PhoneLine::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
