<?php

namespace app\modules\setting\controllers;

use app\modules\setting\models\TransportTypeHasFieldSearch;
use common\modules\car\models\transport\TransportField;
use common\modules\car\models\transport\TransportType;
use Yii;
use app\modules\setting\models\TransportTypeHasField;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TransportTypeHasFieldController implements the CRUD actions for TransportTypeHasField model.
 */
class TransportTypeHasFieldController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['transportRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['transport'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TransportTypeHasField models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TransportTypeHasFieldSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TransportTypeHasField model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TransportTypeHasField model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TransportTypeHasField();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model'    => $model,
                'typeMap'  => $this->getTypeMap(),
                'fieldMap' => $this->getFieldMap(),
            ]);
        }
    }

    private function getTypeList()
    {
        return TransportType::find()->all();
    }

    private function getTypeMap()
    {
        return ArrayHelper::map($this->getTypeList(), 'type_id', 'name');
    }

    private function getFieldList()
    {
        return TransportField::find()->all();
    }

    private function getFieldMap()
    {
        return ArrayHelper::map($this->getFieldList(), 'field_id', function ($item) {
            return $item['name'] . ' (' . $item['type'] . ')';
        });
    }

    /**
     * Updates an existing TransportTypeHasField model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model'    => $model,
                'typeMap'  => $this->getTypeMap(),
                'fieldMap' => $this->getFieldMap(),
            ]);
        }
    }

    /**
     * Deletes an existing TransportTypeHasField model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TransportTypeHasField model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return \common\modules\car\models\transport\TransportTypeHasField the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TransportTypeHasField::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
