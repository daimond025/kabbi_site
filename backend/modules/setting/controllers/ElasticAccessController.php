<?php

namespace app\modules\setting\controllers;

use app\modules\setting\models\Tenant;
use common\modules\tenant\models\DefaultSettings;
use common\modules\tenant\models\TenantSetting;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use backend\modules\setting\models\ElasticAccessSearch;
use yii\web\NotFoundHttpException;

/**
 * ElasticAccessController implements the CRUD actions for Elastic model.
 */
class ElasticAccessController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['geoElasticRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['geoElastic'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Elastic rows.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new ElasticAccessSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SourceMessage model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = ElasticAccess::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates a new ElasticAccess model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ElasticAccess();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->save();

            return $this->redirect(['view', 'id' => $model->row_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ElasticAccess model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->row_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionImportAll()
    {
        $tenantIdArr = Tenant::find()->asArray()->all();
        foreach ($tenantIdArr as $tenantData) {
            $tenantId     = $tenantData['tenant_id'];
            $tenantDomain = $tenantData['domain'];
            $clientData   = TenantSetting::find()->where([
                'tenant_id' => $tenantId,
                'name'      => 'CLIENT_API_KEY',
            ])->one();
            $clientApiKey = $clientData->value;
            ElasticAccess::createKey($tenantId . '_client', $tenantId, $tenantDomain, 'client', $clientApiKey);

            $workerData   = TenantSetting::find()->where([
                'tenant_id' => $tenantId,
                'name'      => 'WORKER_API_KEY',
            ])->one();
            $workerApiKey = $workerData->value;
            ElasticAccess::createKey($tenantId . '_driver', $tenantId, $tenantDomain, 'driver', $workerApiKey);
        }

        $frontendKey = DefaultSettings::getValue(DefaultSettings::SETTING_GEOSERVICE_API_KEY);
        ElasticAccess::createKey('frontend', 'frontend', 'frontend', 'frontend', $frontendKey);

        return $this->redirect(['index']);
    }

    public function actionDeleteAll()
    {
        ElasticAccess::deleteAll();

        return $this->redirect(['index']);
    }

}
