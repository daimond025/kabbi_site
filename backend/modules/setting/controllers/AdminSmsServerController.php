<?php

namespace app\modules\setting\controllers;

use Yii;
use app\modules\setting\models\AdminSmsServer;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use common\controllers\IqSmsController;
use common\controllers\IbateleSmsController;
use common\controllers\SapsanSmsController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AdminSmsServerController implements the CRUD actions for AdminSmsServer model.
 */
class AdminSmsServerController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['smsRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['sms'],
                    ],
                ],
            ],
            'verbs'  => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AdminSmsServer models.
     * @return mixed
     */
    public function actionIndex() {
        $serverList = AdminSmsServer::find()->with('server');
        $dataProvider = new ActiveDataProvider([
            'query' => $serverList,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);
        $serverList = ArrayHelper::map($serverList->all(), 'server_id', 'server.name');
        return $this->render('index', [
                    'serverList' => $serverList,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AdminSmsServer model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionBalance() {
        $server_id = post()['server_id'];
        $userData = AdminSmsServer::findOne(['server_id' => $server_id]);
        if ($server_id == 1) {
            $smsServer = new IqSmsController('iqsms',Yii::$app);
            return $smsServer->actionBalance($userData->login, $userData->password);
        }
        elseif($server_id == 2) {
            $smsServer = new IbateleSmsController('ibatele',Yii::$app);
            return $smsServer->actionRequestBalance($userData->login, $userData->password);
        }
        elseif($server_id == 3) {
            $smsServer = new SapsanSmsController('sapsan',Yii::$app);
            return $smsServer->actionRequestBalance($userData->login, $userData->password);
        }
    }

    /**
     * Creates a new AdminSmsServer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new AdminSmsServer();
        $count = AdminSmsServer::find()->count();

        if ($count >= 2)
            return $this->redirect('index');
        elseif ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AdminSmsServer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AdminSmsServer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AdminSmsServer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AdminSmsServer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = AdminSmsServer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
