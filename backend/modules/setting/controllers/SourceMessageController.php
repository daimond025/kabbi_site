<?php

namespace app\modules\setting\controllers;

use Yii;
use backend\modules\setting\models\SourceMessage;
use backend\modules\setting\models\SourceMessageSearch;
use backend\modules\setting\models\Message;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SourceMessageController implements the CRUD actions for SourceMessage model.
 */
class SourceMessageController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['index', 'view'],
                        'roles'   => ['translationsRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['translations'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        if (YII_ENV == 'prod' && in_array($action->id, ['index', 'create', 'update', 'delete'])) {
            //     return $this->redirect('/setting/source-message/uat-message');
        }

        return true;
    }


    /**
     * Lists all SourceMessage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SourceMessageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SourceMessage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SourceMessage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SourceMessage();

        if ($model->load(Yii::$app->request->post())) {
            $transaction = app()->db->beginTransaction();
            try {
                if ($model->save()) {
                    $post_translate_map = post('translate');

                    //Добавляем переводы
                    Message::batchInsert($model->id, $post_translate_map);

                    $transaction->commit();

                    return $this->redirect(['view', 'id' => $model->id]);
                }
            } catch (Exception $exc) {
                session()->setFlash('error', 'Ошибка добавления переводов');
            }
            $transaction->rollBack();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SourceMessage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = SourceMessage::find()
            ->where(['id' => $id])
            ->with([
                'translates' => function ($query) {
                    $query->indexBy('language');
                },
            ])
            ->one();

        if ($model->load(Yii::$app->request->post())) {
            $transaction = app()->db->beginTransaction();
            if ($model->save()) {
                $post_translate_map = post('translate');
                //Обновление переводов
                foreach ($post_translate_map as $language => $translation) {
                    if (!isset($model->translates[$language])) {
                        (new Message([
                            'id'          => $model->id,
                            'language'    => $language,
                            'translation' => $translation,
                            'message'     => $model->message,
                            'category'    => $model->category,
                        ]))
                            ->save();
                    } elseif ($translation != $model->translates[$language]->language) {
                        $model->translates[$language]->translation = $translation;
                        $model->translates[$language]->save(true, ['translation']);
                    }
                }
                $transaction->commit();

                return $this->redirect(['view', 'id' => $model->id]);
            }
            $transaction->rollBack();
        }

        return $this->render('update', [
            'model' => $model,
        ]);

    }

    /**
     * Deletes an existing SourceMessage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SourceMessage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SourceMessage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SourceMessage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUatMessage()
    {
        return $this->render('uat_message');
    }
}
