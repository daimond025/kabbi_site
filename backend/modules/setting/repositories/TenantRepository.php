<?php

namespace backend\modules\setting\repositories;

use app\modules\setting\models\TenantHasCity;

class TenantRepository
{
    /**
     * @return array
     */
    public function getTenantHasCityList()
    {
        /** @var TenantHasCity[] $models */
        $models = TenantHasCity::find()->all();

        $tenantHasCityList = [];

        foreach ($models as $model) {
            $tenantHasCityList[$model->tenant_id][] = $model->city_id;
        }

        return $tenantHasCityList;
    }
}