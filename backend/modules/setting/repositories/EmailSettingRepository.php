<?php

namespace backend\modules\setting\repositories;

use backend\modules\setting\models\email\DefaultEmailSetting;
use backend\modules\setting\services\dto\EmailProviderData;
use backend\modules\setting\services\dto\EmailSenderData;

class EmailSettingRepository
{
    /**
     * @return array|null|DefaultEmailSetting
     */
    public function findFromSystem()
    {
        if (!$model = DefaultEmailSetting::findOne(['type' => DefaultEmailSetting::TYPE_SYSTEM])) {
            throw new NotFoundException();
        }

        return $model;
    }

    /**
     * @return array|null|DefaultEmailSetting
     */
    public function findFromTenant()
    {
        if (!$model = DefaultEmailSetting::findOne(['type' => DefaultEmailSetting::TYPE_TENANT])) {
            throw new NotFoundException();
        }

        return $model;
    }

    public function saveFromSystem(EmailProviderData $providerData, EmailSenderData $senderData, $template)
    {
        $model = $this->findFromSystem();

        $model->provider_server = $providerData->server;
        $model->provider_port   = $providerData->port;

        $model->sender_name     = $senderData->name;
        $model->sender_email    = $senderData->email;
        $model->sender_password = $senderData->password;

        $model->template = $template;

        if (!$model->save(false)) {
            throw new \RuntimeException('Save model');
        }
    }

    public function saveFromTenant(EmailProviderData $providerData, EmailSenderData $senderData, $template)
    {
        $model = $this->findFromTenant();

        $model->provider_server = $providerData->server;
        $model->provider_port   = $providerData->port;

        $model->sender_name     = $senderData->name;
        $model->sender_email    = $senderData->email;
        $model->sender_password = $senderData->password;

        $model->template = $template;

        if (!$model->save(false)) {
            throw new \RuntimeException('Save model');
        }
    }
}