<?php

namespace backend\modules\setting\repositories;

use backend\modules\setting\models\DefaultWorkerPushNotification;
use backend\modules\setting\models\TenantWorkerPushNotification;
use yii\helpers\ArrayHelper;

class WorkerPushNotificationRepository
{
    /**
     * @param $pushId
     *
     * @return DefaultWorkerPushNotification
     */
    public function findDefaultNotification($pushId)
    {
        if (!$result = DefaultWorkerPushNotification::findOne($pushId)) {
            throw new NotFoundException();
        }

        return $result;
    }

    public function findAllDefaultNotification()
    {
        return DefaultWorkerPushNotification::find()->all();
    }

    /**
     * @param integer  $type
     * @param integer  $positionId
     * @param string   $text
     * @param string[] $params
     *
     * @return integer
     */
    public function createDefaultNotification($type, $positionId, $text, array $params)
    {
        $model = new DefaultWorkerPushNotification();

        $model->type        = $type;
        $model->position_id = $positionId;
        $model->text        = $text;
        $model->params      = $params;

        if (!$model->save(false)) {
            throw new \RuntimeException('Save model');
        }

        return $model->push_id;
    }

    /**
     * @param integer  $id
     * @param string   $text
     * @param string[] $params
     *
     * @throws \Exception
     */
    public function updateDefaultNotification($id, $text, array $params)
    {
        $model = $this->findDefaultNotification($id);

        $model->text   = $text;
        $model->params = $params;

        if (!$model->save(false)) {
            throw new \RuntimeException('Save model');
        }
    }

    /**
     * @param integer $id
     *
     * @throws \Exception
     */
    public function deleteDefaultNotification($id)
    {
        DefaultWorkerPushNotification::deleteAll(['push_id' => $id]);
    }

    public function deleteTenantNotifications($type, $positionId)
    {
        TenantWorkerPushNotification::deleteAll(['type' => $type, 'position_id' => $positionId]);
    }

    /**
     * @param       $type
     * @param       $positionId
     * @param       $text
     * @param array $tenantHasCityList
     *
     * @throws \yii\db\Exception
     */
    public function createNotifications($type, $positionId, $text, array $tenantHasCityList)
    {
        $insert = [];
        foreach ($tenantHasCityList as $tenantId => $cities) {
            if (ArrayHelper::isTraversable($cities)) {
                foreach ($cities as $cityId) {
                    $insert[] = [
                        'text'        => $text,
                        'type'        => $type,
                        'tenant_id'   => $tenantId,
                        'city_id'     => $cityId,
                        'position_id' => $positionId,
                    ];
                }
            }
        }

        if (!empty($insert)) {
            $columns     = array_keys(ArrayHelper::getValue($insert, 0, []));
            $chunkInsert = array_chunk($insert, 100);
            foreach ($chunkInsert as $item) {
                \Yii::$app->db->createCommand()->batchInsert(TenantWorkerPushNotification::tableName(), $columns,
                    $item)->execute();
            }
        }

    }
}