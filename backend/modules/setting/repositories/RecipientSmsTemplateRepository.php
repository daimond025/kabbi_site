<?php

namespace backend\modules\setting\repositories;

use backend\modules\setting\models\DefaultRecipientSmsTemplate;
use backend\modules\setting\models\RecipientSmsTemplate;
use yii\db\Exception;
use yii\helpers\ArrayHelper;

class RecipientSmsTemplateRepository
{
    /**
     * @param $id
     *
     * @return DefaultRecipientSmsTemplate
     */
    public function findDefaultNotification($id)
    {
        if (!$result = DefaultRecipientSmsTemplate::findOne($id)) {
            throw new NotFoundException();
        }

        return $result;
    }

    public function findAllDefaultNotification()
    {
        return DefaultRecipientSmsTemplate::find()->all();
    }

    /**
     * @param integer  $type
     * @param integer  $positionId
     * @param string   $text
     * @param string[] $params
     *
     * @return integer
     */
    public function createDefaultNotification($type, $positionId, $text, array $params)
    {
        $model = new DefaultRecipientSmsTemplate();

        $model->type        = $type;
        $model->position_id = $positionId;
        $model->text        = $text;
        $model->params      = $params;

        if (!$model->save(false)) {
            throw new \RuntimeException('Save ' . DefaultRecipientSmsTemplate::class . ' model');
        }

        return $model->template_id;
    }

    /**
     * @param integer  $id
     * @param string   $text
     * @param string[] $params
     *
     * @throws \Exception
     */
    public function updateDefaultNotification($id, $text, array $params)
    {
        $model = $this->findDefaultNotification($id);

        $model->text   = $text;
        $model->params = $params;

        if (!$model->save(false)) {
            throw new \RuntimeException('Save ' . DefaultRecipientSmsTemplate::class . ' model');
        }
    }

    /**
     * @param integer $id
     *
     * @throws \Exception
     */
    public function deleteDefaultNotification($id)
    {
        DefaultRecipientSmsTemplate::deleteAll(['template_id' => $id]);
    }

    public function deleteTenantNotifications($type, $positionId)
    {
        RecipientSmsTemplate::deleteAll(['type' => $type, 'position_id' => $positionId]);
    }

    /**
     * @param       $type
     * @param       $positionId
     * @param       $text
     * @param array $tenantHasCityList
     *
     * @throws Exception
     */
    public function createNotifications($type, $positionId, $text, array $tenantHasCityList)
    {
        $insert = [];
        foreach ($tenantHasCityList as $tenantId => $cities) {
            if (ArrayHelper::isTraversable($cities)) {
                foreach ($cities as $cityId) {
                    $insert[] = [
                        'text'        => $text,
                        'type'        => $type,
                        'tenant_id'   => $tenantId,
                        'city_id'     => $cityId,
                        'position_id' => $positionId,
                    ];
                }
            }
        }

        if (!empty($insert)) {
            $columns     = array_keys(ArrayHelper::getValue($insert, 0, []));
            $chunkInsert = array_chunk($insert, 100);
            foreach ($chunkInsert as $item) {
                \Yii::$app->db->createCommand()->batchInsert(
                    RecipientSmsTemplate::tableName(),
                    $columns,
                    $item
                )->execute();
            }
        }

    }
}
