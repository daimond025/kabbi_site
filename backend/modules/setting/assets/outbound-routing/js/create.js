/**
 * Created by artem on 21.08.17.
 */
;(function () {
    'use strict';

    var create = {

        domainSelector: ".js-domain",
        tenantIdSelector: ".js-tenant-id",
        ajaxUrlTenantId: "/setting/outbound-routing/get-tenant-id",

        cityIdSelector: ".js-city-id",
        ajaxUrlCityList: "/setting/outbound-routing/get-city-list",

        tariffIdSelector: ".js-tariff-id",
        ajaxUrlTariffList: "/setting/outbound-routing/get-tariff-list",

        lineOnNotifySelector: ".js-lineOnNotify",
        lineOnOperatorSelector: ".js-lineOnOperator",
        lineOnNotifyContainerSelector: ".js-container-on-notify",
        lineOnOperatorContainerSelector: ".js-container-on-operator",
        templateLineOnNotifyContainerSelector: ".js-container-on-notify-template",
        templateLineOnOperatorContainerSelector: ".js-container-on-operator-template",

        ajaxUrlPhoneLines: "/setting/outbound-routing/get-phone-line-list",


        init: function () {
            this.registerChangeDomain();
            this.registerChangeCityList();
        },

        registerChangeDomain: function () {
            this.onChangeDomain();
        },

        onChangeDomain: function () {
            var _this = this;
            $(document).on('change', this.domainSelector, function () {
                _this.changeTenantList();
            });
        },

        changeTenantList: function () {
            var domain = $(this.domainSelector).val();
            var _this = this;
            $.ajax({
                type: "GET",
                url: this.ajaxUrlTenantId,
                data: {domain: domain},
                dataType: "json",
                beforeSend: function () {
                    _this.clearTenantId();
                },
                success: function (response) {
                    $(_this.tenantIdSelector).val(response);
                    _this.fillCityList();
                }
            });
        },

        clearTenantId: function () {
            $(this.tenantIdSelector).val('');
            this.clearCityList();
        },


        registerChangeCityList: function () {
            this.onChangeCityList();
        },

        onChangeCityList: function () {
            var _this = this;
            $(document).on('change', this.cityIdSelector, function () {
                _this.changeCityList();
            });
        },

        changeCityList: function () {
            this.fillTariffList();
            this.fillPhoneLines();

        },

        clearCityList: function () {
            $(this.cityIdSelector).empty().append('<option value=""">Не выбрано</option>');
            this.clearTariffList();
            this.clearPhoneLines();
        },

        fillCityList: function () {
            var tenantId = $(this.tenantIdSelector).val();
            var _this = this;
            $.ajax({
                type: "GET",
                url: _this.ajaxUrlCityList,
                data: {tenantId: tenantId},
                dataType: "json",
                beforeSend: function () {
                    _this.clearCityList();
                },
                success: function (response) {
                    for (var key in response) {
                        $(_this.cityIdSelector).append('<option value="' + key + '">' + response[key] + '</option>');
                    }
                }
            });
        },


        clearTariffList: function () {
            $(this.tariffIdSelector).empty().append('');
        },

        fillTariffList: function () {
            var tenantId = $(this.tenantIdSelector).val();
            var cityId = $(this.cityIdSelector).val();
            var _this = this;
            $.ajax({
                type: "GET",
                url: _this.ajaxUrlTariffList,
                data: {tenantId: tenantId, cityId: cityId},
                dataType: "json",
                beforeSend: function () {
                    _this.clearTariffList();
                },
                success: function (response) {
                    for (var key in response) {
                        $(_this.tariffIdSelector).append('<option value="' + key + '">' + response[key] + '</option>');
                    }
                }
            });
        },

        fillPhoneLines: function () {
            var tenantId = $(this.tenantIdSelector).val();
            var cityId = $(this.cityIdSelector).val();
            var _this = this;
            $.ajax({
                type: "GET",
                url: _this.ajaxUrlPhoneLines,
                data: {tenantId: tenantId, cityId: cityId},
                dataType: "json",
                beforeSend: function () {
                    _this.clearPhoneLines();
                },
                success: function (response) {
                    for (var key in response) {
                        $(_this.lineOnNotifySelector).append('<option value="' + key + '">' + response[key] + '</option>');
                        $(_this.lineOnOperatorSelector).append('<option value="' + key + '">' + response[key] + '</option>');
                    }
                }
            });

        },

        clearPhoneLines: function () {
            $(this.lineOnNotifySelector).empty().append('<option value=""">Не выбрано</option>');
            $(this.lineOnOperatorSelector).empty().append('<option value=""">Не выбрано</option>');

            $(this.lineOnNotifyContainerSelector).not(this.templateLineOnNotifyContainerSelector).remove();
            $(this.lineOnOperatorContainerSelector).not(this.templateLineOnOperatorContainerSelector).remove();
        }
    };

    create.init();

})();