;(function () {
    'use strict';

    var app = {
        onNotifyContainerSelector: ".js-container-on-notify",
        addButtonOnNotifySelector: ".js-add-button-on-notify",
        templateOnNotifyClass: "js-container-on-notify-template",
        templateOnNotifySelector: ".js-container-on-notify-template",
        deleteButtonOnNotifySelector: ".js-delete-button-on-notify",

        onOperatorContainerSelector: ".js-container-on-operator",
        addButtonOnOperatorSelector: ".js-add-button-on-operator",
        templateOnOperatorClass: "js-container-on-operator-template",
        templateOnOperatorSelector: ".js-container-on-operator-template",
        deleteButtonOnOperatorSelector: ".js-delete-button-on-operator",

        init: function () {
            this.registerAddButtonOnNotify();
            this.registerDeleteButtonOnNotify();

            this.registerAddButtonOnOperator();
            this.registerDeleteButtonOnOperator();
        },

        registerAddButtonOnNotify: function () {
            var _this = this;
            $(document).on('click', this.addButtonOnNotifySelector, function () {
                _this.addNewOnNotifyForm();
            });
        },

        registerDeleteButtonOnNotify: function () {
            var _this = this;
            $(document).on('click', this.deleteButtonOnNotifySelector, function () {
                _this.deleteNewOnNotifyForm(this);
            });
        },

        addNewOnNotifyForm: function () {
            var _this = this;
            $(_this.templateOnNotifySelector).clone()
                .show()
                .insertBefore(_this.templateOnNotifySelector)
                .removeClass(_this.templateOnNotifyClass)
                .find('select,input')
                .attr('disabled', false);
        },

        deleteNewOnNotifyForm: function (element) {
            console.log($(element).parents(this.onNotifyContainerSelector).first().remove());
        },


        registerAddButtonOnOperator: function () {
            var _this = this;
            $(document).on('click', this.addButtonOnOperatorSelector, function () {
                _this.addNewOnOperatorForm();
            });
        },

        registerDeleteButtonOnOperator: function () {
            var _this = this;
            $(document).on('click', this.deleteButtonOnOperatorSelector, function () {
                _this.deleteNewOnOperatorForm(this);
            });
        },

        addNewOnOperatorForm: function () {
            var _this = this;
            $(_this.templateOnOperatorSelector).clone()
                .show()
                .insertBefore(_this.templateOnOperatorSelector)
                .removeClass(_this.templateOnOperatorClass)
                .find('select,input')
                .attr('disabled', false);
        },

        deleteNewOnOperatorForm: function (element) {
            console.log($(element).parents(this.onOperatorContainerSelector).first().remove());
        }
    };

    app.init();

})();