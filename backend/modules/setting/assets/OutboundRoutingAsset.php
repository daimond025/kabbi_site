<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\modules\setting\assets;

use yii\web\AssetBundle;

class OutboundRoutingAsset extends AssetBundle
{

    public $sourcePath = '@backend/modules/setting/assets/outbound-routing';
    public $publishOptions = [
        'forceCopy' => true,
    ];
    public $js = [
        'js/main.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];

}
