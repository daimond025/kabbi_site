$(document).ready(function () {
    var MIN_DATE = '2000-01-01';
    var MAX_DATE = '3000-01-01';

    var searchDates = getSearchDates();

    function getSearchDates() {
        var start, end, search = window.location.search;

        start = search.match(/start=[^&]*/gi);
        end = search.match(/end=[^&]*/gi);

        start = moment(Array.isArray(start) ? start[0] : MIN_DATE);
        end = moment(Array.isArray(end) ? end[0] : MAX_DATE);

        return {start: start, end: end};
    }


    $('#daterange').daterangepicker({
            locale: {
                applyLabel: 'Ok',
                cancelLabel: 'Отмена',
                fromLabel: 'От',
                toLabel: 'До',
                weekLabel: 'W',
                customRangeLabel: 'Задать самому',
                firstDay: 1,
                daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                monthNames: ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сент', 'Окт', 'Ноя', 'Дек'],
            },
            ranges: {
                'За весь период': [moment(MIN_DATE), moment(MAX_DATE)],
                'Сегодня': [moment(), moment()],
                'Вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'За 7 дней': [moment().subtract(6, 'days'), moment()],
                'За 30 дней': [moment().subtract(29, 'days'), moment()],
                'За этот месяц': [moment().startOf('month'), moment().endOf('month')],
                'За прошлый месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            },
            format: 'DD.MM.YYYY',
            startDate: searchDates.start,
            endDate: searchDates.end
        },
        function (start, end) {
            var searchParams = document.location.search.substr(1).split('&').filter(function (item) {
                return item && item.indexOf('start=') == -1 && item.indexOf('end=') == -1;
            });

            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

            if (start <= end && !start.isSame(MIN_DATE) && !end.isSame(MAX_DATE)) {
                searchParams.push('start=' + start.format('YYYY-MM-DD'));
                searchParams.push('end=' + end.format('YYYY-MM-DD'));
            }

            document.location.search = '?' + searchParams.join('&');
        }
    );

    $('#error_code').bind('click', function () {
        $('#helper').slideToggle('fast');
    });
});

