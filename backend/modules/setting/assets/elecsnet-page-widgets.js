$(document).ready(function () {
    $('#table-roller').html('Развернуть таблицу с расшифровкой кодов ошибок');
    $('#elecsnet-counter-roller').html('Развернуть форму подсчета');
    $('.elecsnet-payment-roller').html('Развернуть форму внесения платежей');
    $('#add_payment_roller').html('Развернуть форму добавление платежа');
});

$('#additional-fields-roller').click(function () {
    if ($('#helper').attr('style') == 'display: none')
    {
        $('#helper').attr('style', 'display: block');
        $('#table-roller').html('Свернуть таблицу с расшифровкой кодов ошибок');
    }
    else
    {
        $('#helper').attr('style', 'display: none');
        $('#table-roller').html('Развернуть таблицу с расшифровкой кодов ошибок');
    }
});

$('#add_payment_roller').click(function () {
    if ($('#add_payment').attr('style') == 'display: none')
    {
        $('#add_payment').attr('style', 'display: block');
        $('#add_payment_roller').html('Свернуть форму добавление платежа');
    }
    else
    {
        $('#add_payment').attr('style', 'display: none');
        $('#add_payment_roller').html('Развернуть форму добавление платежа');
    }
});


$('#table-roller').click(function () {
    if ($('#helper').attr('style') == 'display: none')
    {
        $('#helper').attr('style', 'display: block');
        $('#table-roller').html('Свернуть таблицу с расшифровкой кодов ошибок');
    }
    else
    {
        $('#helper').attr('style', 'display: none');
        $('#table-roller').html('Развернуть таблицу с расшифровкой кодов ошибок');
    }
});

$('#elecsnet-counter-roller').click(function () {
    if ($('#elecsnet-counter').attr('style') == 'display: none')
    {
        $('#elecsnet-counter').attr('style', 'display: block');
        $('#elecsnet-counter-roller').html('Свернуть форму подсчета');
    }
    else
    {
        $('#elecsnet-counter').attr('style', 'display: none');
        $('#elecsnet-counter-roller').html('Развернуть форму подсчета');
    }
});


$('#elecsnet-payment-roller').click(function () {
    if ($('#elecsnet-payment').attr('style') == 'display: none')
    {
        $('.elecsnet-payment').attr('style', 'display: block');
        $('.elecsnet-payment-roller').html('Свернуть форму внесения платежа');
    }
    else
    {
        $('.elecsnet-payment').attr('style', 'display: none');
        $('.elecsnet-payment-roller').html('Развернуть форму внесения платежа');
    }
});

$('.date').datepicker({
    clearBtn: true,
    language: "ru",
    format: 'yyyy-mm-dd',
    autoclose: true,
    todayHighlight: true,
});

$('#count').click(function () {
    var tenant_id = $('#tenant_id').val();
    var beginPeriod = $('#beginPeriod').val();
    var endPeriod = $('#endPeriod').val();
    $.ajax({
        type: "GET",
        url: "/setting/tenant/update-table",
        data: {
            tenant_id: tenant_id,
            beginPeriod: beginPeriod,
            endPeriod: endPeriod,
        },
        success: function (json) {
            var obj = JSON.parse(json);
            $('#payments').html('');
            $('#payments').html(obj.view);
            $('#payments').trigger('update');
            if (obj.totalCosts != undefined || obj.totalCosts != null)
            {
                $('#total').html('Итого за выбранный период: ' + obj.totalCosts);
            }
            delete obj;
        },
        error: function (json) {
            console.log(json);
        }
    });
});


$('#add_payment_button').click(function () {
    var amount = $('#amount').val();
    var title = $('#payment_titile').val()
    var tenant_id = $('#tenant_id').val();
    var beginPeriod = $('#paymentBeginPeriod').val();
    var period = $('#dropDown').val();
   if (amount != '' && isNaN(amount) == false)
    {
        $.ajax({
            type: "POST",
            url: "/setting/tenant/add-payment",
            data: {
                tenant_id: tenant_id,
                amount: amount,
                title: title,
                beginPeriod: beginPeriod,
                period: period,
            },
            success: function (json) {
                var obj = JSON.parse(json);
                $('#payments').html(obj.view);
                $('#payments').trigger('update');
                $('#total_sum').html('Общая сумма платежей арендатора за все время составляет: ' + obj.totalCosts);
                $('#total_sum').trigger('update');
            },
            error: function (json) {
                console.log(json);
            }
        });
    }
    else if (isNaN(amount) == true)
    {
        $('#payment_result').html('<label style="color: red">Введите числовое значение</label>');
    }
    else
    {
        $('#payment_result').html('<label style="color: red">Введите сумму</label>');
    }
});