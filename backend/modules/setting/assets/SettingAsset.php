<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\setting\assets;

use yii\web\AssetBundle;

class SettingAsset extends AssetBundle
{

    public $sourcePath = '@app/modules/setting/assets/';
    public $publishOptions = [
        'forceCopy' => true
    ];
    public $css = [
        'mapEditor/leaflet-0.7.3/leaflet.css',
    ];
    public $js = [

    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

}
