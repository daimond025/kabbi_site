$(document).ready(function () {
    var cityId = $('#cityId').val();
    var cityName = $('#cityName').val();
    var editableLayers;

    function showMapEditor(container, lat, lon) {
        var polygon = $('#polygon').val();

        var geojsonObject;
        try {
            geojsonObject = JSON.parse(polygon);
        } catch (err) {
            console.log('Error of parsing city polygon: ' + err.message);
            return;
        }

        var map = new L.Map(container, {center: new L.LatLng(lat, lon), zoom: 13, zoomAnimation: false});

        //Подключаем тайлы
        var osm = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

        //Добавляем слой osm
        map.addLayer(osm);
        //Добавляем control для переключения карт
        map.addControl(new L.Control.Layers({'OSM': osm}));

        editableLayers = L.geoJson(geojsonObject).addTo(map);
        map.addLayer(editableLayers);

        var options = function (editableLayers) {
            return {
                position: 'topright',
                draw: {
                    polygon: true,
                    polyline: false,
                    circle: false,
                    marker: false
                },
                edit: {
                    featureGroup: editableLayers, //REQUIRED!!
                    remove: true
                }
            };
        };
        var drawControl = new L.Control.Draw(options(editableLayers));
        map.addControl(drawControl);
        function drawnItemHandler(layer) {
            editableLayers.addLayer(layer);
        }

        map.on('draw:created', function (e) {
            drawnItemHandler(e.layer);
        });
        map.on('draw:edited', function (e) {
            e.layers.eachLayer(drawnItemHandler);
        });
        map.on('draw:deleted', function (e) {
            $('#city-city_polygon').empty();
        });
    }

    var cityLat = $('#cityLat').val();
    var cityLon = $('#cityLon').val();
    showMapEditor('editMap', cityLat, cityLon);

    function validateCommand(parkCount) {
        var message = null;
        if (parkCount === 0) {
            message = i18n('Create a parking on the map');
        }
        if (parkCount > 1) {
            message = i18n('On the map more than one parking');
        }
        return message;
    }

    $('#save').click(function () {
        if ($('.js-field-map').hasClass('active')) {
            $('#polygon').empty();
            var shapes = [];
            editableLayers.eachLayer(function (layer) {
                var shape = layer.toGeoJSON();
                var shape_for_db = JSON.stringify(shape);
                shapes.push(shape_for_db);
            });
            var parkCount = shapes.length;
            var errorMessage = validateCommand(parkCount);
            if (errorMessage !== null) {
                $('#polygon').text(errorMessage);
            } else {
                var polygon = shapes[0];
                $('#city-city_polygon').val(polygon);
            }
        }
    });
});