/*
 * Leaflet.draw assumes that you have already included the Leaflet library.
 */

L.drawVersion = '0.2.4-dev';

L.drawLocal = {
	draw: {
		toolbar: {
			actions: {
				title: 'Отменить рисование',
				text: 'Отмена'
			},
			undo: {
				title: 'Удалить последнюю нарисованную точку',
				text: 'Удалить последнюю точку'
			},
			buttons: {
				polyline: 'Нирисовать линию',
				polygon: 'Нарисовать полигон',
				rectangle: 'Нарисовать прямоугольник',
				circle: 'Нарисовать круг',
				marker: 'Нарисовать точку'
			}
		},
		handlers: {
			circle: {
				tooltip: {
					start: 'Кликните и растяните для отрисовки окружности'
				}
			},
			marker: {
				tooltip: {
					start: 'Для отрисовки точки кликните на карту'
				}
			},
			polygon: {
				tooltip: {
					start: 'Для построения полигона кликните на карту',
					cont: 'Кликните для продолжения построения',
					end: 'Кликните на первую точку для завершения построения'
				}
			},
			polyline: {
				error: '<strong>Error:</strong> shape edges cannot cross!',
				tooltip: {
					start: 'Click to start drawing line.',
					cont: 'Click to continue drawing line.',
					end: 'Click last point to finish line.'
				}
			},
			rectangle: {
				tooltip: {
					start: 'Click and drag to draw rectangle.'
				}
			},
			simpleshape: {
				tooltip: {
					end: 'Release mouse to finish drawing.'
				}
			}
		}
	},
	edit: {
		toolbar: {
			actions: {
				save: {
					title: 'Сохранить изменения',
					text: 'Сохранить'
				},
				cancel: {
					title: 'Прекратить рисование, отменить все изменения',
					text: 'Отмена'
				}
			},
			buttons: {
				edit: 'Редактирование парковки',
				editDisabled: 'Парковка для редактирования отсутствуют',
				remove: 'Удалить парковку',
				removeDisabled: 'Парковка для удаления отсутствует'
			}
		},
		handlers: {
			edit: {
				tooltip: {
					text: 'Для редактирования парковки перетащите вершины полигона',
					subtext: 'Для сброса изменений нажмите Отмена.'
				}
			},
			remove: {
				tooltip: {
					text: 'Для удаления парковки кликните на неё'
				}
			}
		}
	}
};
