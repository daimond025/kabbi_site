$(document).ready(function () {


    function showMapEditor(container, lat, lon) {
        map = new L.Map(container, {center: new L.LatLng(lat, lon), zoom: 13, zoomAnimation: false});
        map.scrollWheelZoom.disable();
        //Подключаем тайлы
        var osm = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
        var yndx = new L.Yandex();
        var googleLayer = new L.Google('ROADMAP');
        //Добавляем слой osm
        map.addLayer(osm);
        //Добавляем control для переключения карт
        map.addControl(new L.Control.Layers({'OSM': osm, "Yandex": yndx, "Google": googleLayer}));
        //Добавляем cintrol для рисования карт
        var drawnItems = new L.FeatureGroup();
        map.addLayer(drawnItems);
    }

    var cityLat = $('#cityLat').val();
    var cityLon = $('#cityLon').val();
    showMapEditor('mainMap', cityLat, cityLon);

    var layers = new Array();
    var layerCity = new Array();
    var layerOutCity = new Array();
    var layerAirport = new Array();
    var layerStation = new Array();
    var layerReseption = new Array();

    $('input[type=checkbox]').click(function () {
        if ($(this).is(':checked')) {
            var id = $(this).attr('id');
            var elemClass = $(this).attr('class');
            switch (id) {
                case 'cityParks':
                    showBlockParks('cityParks');
                    break
                case 'reseptionParking':
                    showBlockParks('reseptionParking');
                    break
                case 'outCityParks':
                    showBlockParks('outCityParks');
                    break
                case 'airportParks':
                    showBlockParks('airportParks');
                    break
                case 'stationParks':
                    showBlockParks('stationParks');
                    break
                default:
                    addPark(id, elemClass);
                    break
            }
        }
        if (!$(this).is(':checked')) {
            var id = $(this).attr('id');
            var elemClass = $(this).attr('class');
            switch (id) {
                case 'cityParks':
                    removeBlockParks('cityParks');
                    break
                case 'reseptionParking':
                    removeBlockParks('reseptionParking');
                    break
                case 'outCityParks':
                    removeBlockParks('outCityParks');
                    break
                case 'airportParks':
                    removeBlockParks('airportParks');
                    break
                case 'stationParks':
                    removeBlockParks('stationParks');
                    break
                default:
                    removePark(id, elemClass);
                    break
            }

        }
    });

    function removePark(id, elemClass) {
        var inGroup = $("#" + elemClass).prop("checked");
        if (inGroup) {
            switch (elemClass) {
                case 'cityParks':
                    map.removeLayer(layerCity[id]);
                    break
                case 'reseptionParking':
                    map.removeLayer(layerReseption[id]);
                    break
                case 'outCityParks':
                    map.removeLayer(layerOutCity[id]);
                    break
                case 'airportParks':
                    map.removeLayer(layerAirport[id]);
                    break
                case 'stationParks':
                    map.removeLayer(layerStation[id]);
                    break
            }
        } else {
            map.removeLayer(layers[id]);
        }

    }

    function  addPark(id, elemClass) {
        var geojsonString = $('#' + id).attr('data-coords');
        var geojsonObject = JSON.parse(geojsonString);
        var myLayer = L.geoJson().addTo(map);
        var inGroup = $("#" + elemClass).prop("checked");
        if (inGroup) {
            switch (elemClass) {
                case 'cityParks':
                    layerCity[id] = myLayer;
                    break
                case 'reseptionParking':
                    layerReseption[id] = myLayer;
                    break
                case 'outCityParks':
                    layerOutCity[id] = myLayer;
                    break
                case 'airportParks':
                    layerAirport[id] = myLayer;
                    break
                case 'stationParks':
                    layerStation[id] = myLayer;
                    break
            }
        } else {
            layers[id] = myLayer;
        }
        myLayer.addData(geojsonObject);

    }
    function showBlockParks(mainParkId) {
        var classOfParks = mainParkId;
        if (classOfParks === 'cityParks') {
            var geojsonString = $("#" + mainParkId).attr('data-coords');
            if (geojsonString !== '' && geojsonString !== undefined && geojsonString !== null) {
                var geojsonObject = JSON.parse(geojsonString);
                var myLayer = L.geoJson().addTo(map);
                layerCity['cityParks'] = myLayer;
                myLayer.addData(geojsonObject);
            } else {
                //var geojsonObject = JSON.parse(geojsonString);
                var myLayer = L.geoJson().addTo(map);
                layerCity['cityParks'] = myLayer;
                //myLayer.addData(geojsonObject);
            }

        }
        if (classOfParks === 'reseptionParking') {
            var geojsonString = $("#" + mainParkId).attr('data-coords');
            if (geojsonString !== '' && typeof geojsonString !== "undefined" && geojsonString !== null) {
                var geojsonObject = JSON.parse(geojsonString);
                var myLayer = L.geoJson().addTo(map);
                layerReseption['reseptionParking'] = myLayer;
                myLayer.addData(geojsonObject);
            } else {
                //var geojsonObject = JSON.parse(geojsonString);
                var myLayer = L.geoJson().addTo(map);
                layerReseption['reseptionParking'] = myLayer;
                //myLayer.addData(geojsonObject);
            }
        }
        $("." + classOfParks).each(function () {
            $(this).prop("checked", true);
            var id = $(this).attr('id');
            deleteParkFromMianLayer(id);
            var geojsonString = $(this).attr('data-coords');
            var geojsonObject = JSON.parse(geojsonString);
            var myLayer = L.geoJson().addTo(map);
            switch (mainParkId) {
                case 'cityParks':
                    layerCity[id] = myLayer;
                    break
                case 'reseptionParking':
                    layerReseption[id] = myLayer;
                    break
                case 'outCityParks':
                    layerOutCity[id] = myLayer;
                    break
                case 'airportParks':
                    layerAirport[id] = myLayer;
                    break
                case 'stationParks':
                    layerStation[id] = myLayer;
                    break
            }
            myLayer.addData(geojsonObject);
        });
    }

    function removeBlockParks(mainParkId) {
        var classOfParks = mainParkId;
        if (classOfParks == "reseptionParking") {
            map.removeLayer(layerReseption['reseptionParking']);
        }
        $("." + classOfParks).each(function () {
            var id = $(this).attr('id');
            $(this).prop("checked", false);
            for (var item in layers) {
                map.removeLayer(item);
            }
            switch (mainParkId) {
                case 'cityParks':
                    map.removeLayer(layerCity[id]);
                    map.removeLayer(layerCity['cityParks']);
                    break
                case 'reseptionParking':
                    map.removeLayer(layerReseption[id]);
                    map.removeLayer(layerReseption['reseptionParking']);
                    break
                case 'outCityParks':
                    map.removeLayer(layerOutCity[id]);
                    break
                case 'airportParks':
                    map.removeLayer(layerAirport[id]);
                    break
                case 'stationParks':
                    map.removeLayer(layerStation[id]);
                    break
            }

        });
    }

    function  deleteParkFromMianLayer(id) {
        if (layers[id] !== undefined) {
            map.removeLayer(layers[id]);
        }
    }

});