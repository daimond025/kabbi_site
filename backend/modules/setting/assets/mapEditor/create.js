$(document).ready(function () {
    var mainLayer = new Array();

    function showMapEditor(container, lat, lon) {
        map = new L.Map(container, {center: new L.LatLng(lat, lon), zoom: 5, zoomAnimation: false});
        map.scrollWheelZoom.enable();
        //Подключаем тайлы
        var osm = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
        //Добавляем слой osm
        map.addLayer(osm);
        //Добавляем cintrol для рисования карт
        drawnItems = new L.FeatureGroup();
        map.addLayer(drawnItems);
        var options = {
            position: 'topright',
            draw: {
                polygon: {
                    allowIntersection: false, // Restricts shapes to simple polygons
                    drawError: {
                        color: '#e1e100', // Color the shape will turn when intersects
                        message: '<strong>Ошибка!<strong>Не удалось отрисовать полигон!' // Message that will show when intersect
                    },
                    shapeOptions: {
                        color: '#1b3834',
                        weight: 1,
                        opacity: 0.9,
                        stroke: true,
                        fill: true,
                        clickable: true
                    }
                }
            },
            edit: {
                featureGroup: drawnItems, //REQUIRED!!
                remove: true,
                polyline: {
                    shapeOptions: {
                        color: '#1b3834',
                        weight: 1,
                        opacity: 0.9
                    }
                },
                polygon: {
                    allowIntersection: false, // Restricts shapes to simple polygons
                    drawError: {
                        color: '#e1e100', // Color the shape will turn when intersects
                        message: '<strong>Ошибка!<strong>Не удалось отрисовать полигон!' // Message that will show when intersect
                    },
                    shapeOptions: {
                        color: '#1b3834',
                        weight: 1,
                        opacity: 0.9,
                        stroke: true,
                        fill: true,
                        clickable: true
                    }
                }
            }
        };
        var drawControl = new L.Control.Draw(options);
        map.addControl(drawControl);
        map.on('draw:created', function (e) {
            var type = e.layerType,
                layer = e.layer;
            drawnItems.addLayer(layer);
        });

    }

    mainLayer = new Array();
    $('#showAll').click(function () {
        if ($(this).is(':checked')) {
            $.ajax({
                type: 'POST',
                cache: false,
                url: '/parking/parking/get-all-parks/?cityName=' + cityName,
                success: function (response) {
                    var parkArr = JSON.parse(response);
                    var length = parkArr.length;
                    var park = null;
                    for (var i = 0; i < length; i++) {
                        park = parkArr[i];
                        var id = park.parking_id;
                        var geojsonString = park.polygon;
                        var geojsonObject = JSON.parse(geojsonString);
                        var myLayer = L.geoJson().addTo(map);
                        mainLayer[id] = myLayer;
                        myLayer.addData(geojsonObject);
                    }
                }
            });

        }
    });
    var cityLat = $('#cityLat').val();
    var cityLon = $('#cityLon').val();
    showMapEditor('editMap', cityLat, cityLon);
    $('#showAll').click(function () {
        if ($(this).is(':checked')) {
            $.ajax({
                type: 'POST',
                cache: false,
                url: '/parking/parking/get-all-parks/?cityName=' + cityName,
                success: function (response) {
                    var parkArr = JSON.parse(response);
                    var length = parkArr.length;
                    var park = null;
                    for (var i = 0; i < length; i++) {
                        park = parkArr[i];
                        var id = park.parking_id;
                        var geojsonString = park.polygon;
                        var geojsonObject = JSON.parse(geojsonString);
                        var myLayer = L.geoJson().addTo(map);
                        mainLayer[id] = myLayer;
                        myLayer.addData(geojsonObject);
                    }
                }
            });

        }
        if (!$(this).is(':checked')) {
            for (var item in mainLayer) {
                map.removeLayer(mainLayer[item]);
            }
        }

    });
});


function validateCommand(type, name, parkCount) {
    var message = null;
    if (type === undefined || type === null || type === '') {
        message = "Не выбран тип";
    }
    if (name === undefined || name === null || name === '') {
        message = "Введите наименование";
    }
    if (parkCount === 0) {
        message = "Создайте полигон на карте";
    }
    if (parkCount > 1) {
        message = "На карте больше 1 полигона";
    }
    return message;
}


$('#save').click(function () {
    if ($('.js-field-map').hasClass('active')) {
        var shapes = [];
        drawnItems.eachLayer(function (layer) {
            var shape = layer.toGeoJSON();
            var shape_for_db = JSON.stringify(shape);
            $('#city-city_polygon').val(shape_for_db);
            //shapes.push(shape_for_db);

        });

        var city_id = $("#city_id").val();
        var polygon = shapes[0];
        var republic_id = $('#city-republic_id').val();
        var city_name = $('#city-name').val();
        var shortname = $('#city-shortname').val();
        var lat = $('#city-lat').val();
        var lon = $('#city-lon').val();
        var fulladdress = $('#city-fulladdress').val();
        var address_reverse = $('#city-fulladdress_reverse').val();
        var sort = $('#city-sort').val();
        $.ajax({
            type: 'POST',
            cache: false,
            url: '/setting/city/update-polygon',
            data: 'polygon=' + polygon + '&city_id=' + city_id + '&republic_id=' + republic_id + '&city_name=' + city_name +
            '&shortname=' + shortname + '&lat=' + lat + '&lon=' + lon + '&fulladdress=' + fulladdress + '&address_reverse=' + address_reverse +
            '&sort=' + sort,
            success: function (response) {
                location.reload();
            },
            error: function () {
            }
        });
    }
});

