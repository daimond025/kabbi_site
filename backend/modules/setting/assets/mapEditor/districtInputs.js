$('#in_district_select').on('change', function () {
    if ($('#in_district_select').val() == '%')
    {
        $('#in_district_select').attr('value', 'PERCENT');
    }
    else
    {
        $('#in_district_select').attr('value', 'MONEY');
    }

});

$(document).on('change', '#type', function () {
    var selected = $('#type').val();
    if (selected == 'basePolygon' || selected == "reseptionArea")
    {
        $('#name-type').hide();
        $('#name-section').hide();
        $('#district_section').hide();
        $('#in_district').attr('disabled', 'disabled');
        $('#in_district_select').attr('disabled', 'disabled');
        $('#out_district').attr('disabled', 'disabled');
        $('#out_district_select').attr('disabled', 'disabled');
        if (selected == 'basePolygon') {
            $('#name').val('Basic parking zone (outskirts)');
        }
        if (selected == 'reseptionArea') {
            $('#name').val('The reception area of orders');
        }

    }
    else
    {
        //$('#in_district').removeAttr('disabled');
        //$('#in_district_select').removeAttr('disabled');
//        $('#out_district').removeAttr('disabled');
//        $('#out_district_select').removeAttr('disabled');
        $('#name-type').show();
        $('#district_section').show();
        $('#name-section').show();
        $('#name').val('');
    }
});
