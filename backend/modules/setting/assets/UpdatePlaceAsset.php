<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\setting\assets;

use yii\web\AssetBundle;

class UpdatePlaceAsset extends AssetBundle
{

    public $sourcePath = '@app/modules/setting/assets/';
    public $publishOptions = [
        'forceCopy' => true,
    ];
    public $css = [
        'mapEditor/libs/leaflet.css',
        'mapEditor/dist/leaflet.draw.css',
        'mapEditor/leaflet-0.7.3/leaflet.css',
    ];
    public $js = [
        'mapEditor/leaflet-0.7.3/leaflet.js',
        '//api-maps.yandex.ru/2.0/?load=package.map&lang=ru-RU',
        '//maps.google.com/maps/api/js?v=3.2&sensor=false',
        'mapEditor/libs/leaflet-src.js',
        'mapEditor/src/Leaflet.draw.js',
        'mapEditor/src/edit/handler/Edit.Poly.js',
        'mapEditor/src/edit/handler/Edit.SimpleShape.js',
        'mapEditor/src/edit/handler/Edit.Circle.js',
        'mapEditor/src/edit/handler/Edit.Rectangle.js',
        'mapEditor/src/draw/handler/Draw.Feature.js',
        'mapEditor/src/draw/handler/Draw.Polyline.js',
        'mapEditor/src/draw/handler/Draw.Polygon.js',
        'mapEditor/src/draw/handler/Draw.SimpleShape.js',
        'mapEditor/src/draw/handler/Draw.Rectangle.js',
        'mapEditor/src/draw/handler/Draw.Circle.js',
        'mapEditor/src/draw/handler/Draw.Marker.js',
        'mapEditor/src/ext/LatLngUtil.js',
        'mapEditor/src/ext/GeometryUtil.js',
        'mapEditor/src/ext/LineUtil.Intersect.js',
        'mapEditor/src/ext/Polyline.Intersect.js',
        'mapEditor/src/ext/Polygon.Intersect.js',
        'mapEditor/src/Control.Draw.js',
        'mapEditor/src/Tooltip.js',
        'mapEditor/src/Toolbar.js',
        'mapEditor/src/draw/DrawToolbar.js',
        'mapEditor/src/edit/EditToolbar.js',
        'mapEditor/src/edit/handler/EditToolbar.Edit.js',
        'mapEditor/src/edit/handler/EditToolbar.Delete.js',
        'mapEditor/layer/tile/Google.js',
        'mapEditor/layer/tile/Yandex.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'backend\assets\MapsAsset',
    ];

}
