$('#langSwitcher').change(function () {
    var language = $('#langSwitcher').val();
    $.ajax({
        type: "GET",
        url: "/setting/hints/switchtable",
        data: {
            language: language,
        },
        success: function (json) {
            var obj = JSON.parse(json);
            $('#hints').html('');
            $('#hints').html(obj.view);
            $('#hints').trigger('update');
            $('.update').attr('href', $('.update').attr('href') + $('#language').val());
            $('.delete').attr('href', $('.update').attr('href') + $('#language').val());
            delete obj;
        },
        error: function (json) {
            console.log(json);
        }
    });
});

$('#saveHint').click(function () {
    var language = $('#lang').val();
    var text = $('#text').val();
    var action = $('#action').val();
    var hint_id = $('#hint_id').val();
    $.ajax({
        type: "POST",
        url: "/setting/hints/save-hint",
        data: {
            language: language,
            text: text,
            action: action,
            hint_id: hint_id,
        },
        success: function() {
            window.location.href = "/setting/hints";
        }
    });
});

function update(e) {
    var id = e.id;
    var language = $('#language').val();
    $.ajax({
        type: "POST",
        url: "/setting/tenant/update-setting",
        data: {
            name: name.val(),
            value: value.val(),
            settingId: settingId.val(),
        },
        success: function (json) {
            statusblock.attr('style', 'color: green;');
            tableValue.html(value.val());
            statusblock.html(json);
        },
        error: function () {
            statusblock.attr('style', 'color: red;');
            statusblock.html(json);
        }

    });
}

$('body').on('click', '.update', function () {
    update(this);
});