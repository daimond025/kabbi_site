$('#apply_sms_server').click(function () {
    var server_id = $('#server_id').val();
    $.ajax({
        type: "POST",
        url: "/setting/admin-sms-server/setserver",
        data: {
            server_id: server_id,
        },
        success: function (json) {
            $('#result').html(json);
        },
        error: function () {
            $('#result').html('Возникла ошибка, просмотрите консольные логи');
        }
    });
});


$('#checkBalance').click(function () {
    var server_id = $('#server_id').val();
    console.log(server_id);
    $.ajax({
        type: "POST",
        url: "/setting/admin-sms-server/balance",
        data: {
            server_id: server_id,
        },
        success: function (json) {
            var result = 0;
            if(server_id == 2 || server_id == 3)
            {
                var obj = JSON.parse(json);
                result = obj.money;
                
            }
            else
            {
                result = json;
            }
            $('#result').html('Баланс составляет: ' + result + ' ₽');
        },
        error: function () {
            $('#result').html('Возникла ошибка, просмотрите консольные логи');
        }
    });
});