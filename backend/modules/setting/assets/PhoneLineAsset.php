<?php

namespace backend\modules\setting\assets;

use yii\web\AssetBundle;

class PhoneLineAsset extends AssetBundle
{

    public $sourcePath = '@backend/modules/setting/assets/phoneLine';


    public $js = [
        'js/main.js',
        'js/function.js'
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];

    public $publishOptions = [
        'forceCopy' => true,
    ];


}