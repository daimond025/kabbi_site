var btnClassClick = function (e) {
    var statusblock = $('#status-block' + e.currentTarget.id);
    var value = $('#value' + e.currentTarget.id);
    var settingId = $('#settingid' + e.currentTarget.id);
    var tableValue = $('#table-value' + e.currentTarget.id);
    $.ajax({
        type: "POST",
        url: "/setting/tenant/update-setting",
        data: {
            value: value.val(),
            settingId: settingId.val(),
        },
        success: function (json) {
            statusblock.attr('style', 'color: green');
            $('tr[data-key="'+e.currentTarget.id+'"').children('td').eq(-2).html(value.val());
            statusblock.html(json);
        },
        error: function () {
            statusblock.attr('style', 'color: red');
            statusblock.html(json);
        }

    }
    );
}

$('.btnupdate').click(btnClassClick);


$('body').on('click', '.deleteOption', function () {
    del(this);
});

$('body').on('click', '.deletePayment', function () {
    deletePayment(this);
});

function deletePayment(e) {
    var payment_id = $('#' + e.id).attr('value');
    var tenant_id = $('#' + e.id).attr('tenant_id');
    $.ajax({
        type: "GET",
        url: "/setting/tenant/delete-payment",
        data: {
            payment_id: payment_id,
            tenant_id: tenant_id,
        },
        success: function (json) {
            var obj = JSON.parse(json);
            $('#payments').html(obj.view);
            $('#payments').trigger('update');
            $('#total_sum').html('Общая сумма платежей арендатора за все время составляет: ' + obj.totalCosts);
            $('#total_sum').trigger('update');
        },
        error: function (json) {
            console.log(json);
        },
    });
}

function del(e) {
    var add_option = $('#' + e.id).val();
    var tenant_id = $('#' + e.id).attr('tenant_id');
    $.ajax({
        type: "GET",
        url: "/setting/tenant/delete-additional-option",
        data: {
            option_id: add_option,
            tenant_id: tenant_id,
        },
        success: function (json) {
            var obj = JSON.parse(json);
            $('#additional_options').html('');
            $('#additional_options').html(obj.view);
            $('#additional_options').trigger('update');
        },
        error: function (json) {
            console.log(json);
        },
    });
}

$(document).on('pjax:end', function () {
    var btns = $("[data-toggle='popover-x']");
    if (btns.length) {
        btns.popoverButton();
    }
    $('.btnupdate').click(btnClassClick);
});