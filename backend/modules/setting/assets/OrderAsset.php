<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\setting\assets;

use yii\web\AssetBundle;

class OrderAsset extends AssetBundle
{

    
    public $sourcePath = '@app/modules/setting/assets/order';
    
    public $jsOptions = ['position' => \yii\web\View::POS_END];
    public $publishOptions = [
        'forceCopy' => true
    ];
    
    public $css = [
        'leaflet-0.7.3/leaflet.css',
        'main.css',
    ];
    public $js = [
        'leaflet-0.7.3/leaflet.js',
//        'leaflet-0.7.3/tile/Google.js',
//        'leaflet-0.7.3/tile/Yandex.js',
        'leaflet-0.7.3/Layer.Deferred.js',
        'leaflet-0.7.3/Marker.Rotate.js',
        'leaflet-0.7.3/functions.js',
    ];
    public $depends = [
    ];

}
