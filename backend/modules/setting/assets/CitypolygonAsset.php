<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\modules\setting\assets;

use yii\web\AssetBundle;

class CitypolygonAsset extends AssetBundle
{

    public $sourcePath = '@backend/modules/setting/assets/mapEditor';
    public $publishOptions = [
        'forceCopy' => true,
    ];
    public $css = [
        'leaflet.css',
    ];
    public $js = [
    ];
    public $depends = [
        'backend\assets\MapsAsset',
        'backend\modules\setting\assets\MapEditorAsset',
    ];

}
