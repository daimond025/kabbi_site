$(document).ready(function () {

    $('.js-domain').on('change', function () {
        var domain = $(this).val();
        fillTenantId(domain);
    });

    $('.js-tenant-id').on('change', function () {
        var tenantId = $(this).val();
        fillCityList(tenantId);
    });

    $('.js-city-id').on('change', function () {
        var tenantId = $('.js-tenant-id').val();
        var cityId = $(this).val();
        fillTariffList(tenantId, cityId);
    });

    $('.js-phone-line-button').on('click', function() {
        var tenantId = $('.js-tenant-id').val();
        fillPhoneLineScenario(tenantId);
    });

    changeExtendedForm();

    $('.js-click-is-extended-form').on('click', function() {
        var isExtended = $('.js-is-extended-form').val();

        $('.js-is-extended-form').val((Number(isExtended) + 1) % 2);

        changeExtendedForm();
    })

});
