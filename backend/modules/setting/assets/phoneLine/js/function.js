function fillTenantId(domain)
{
    $.ajax({
        type: "GET",
        url: "/setting/phone-line/get-tenant-id",
        data: {domain: domain},
        dataType: "json",
        beforeSend: function () {
            clearTenantId();
        },
        success: function (response) {
            $('.js-tenant-id').val(response);
            fillCityList(response);
        }
    });
}

function clearTenantId() {
    $('.js-tenant-id').val('');
    clearCityList();
}


function fillCityList(tenantId) {
    $.ajax({
        type: "GET",
        url: "/setting/phone-line/get-city-list",
        data: {tenantId: tenantId},
        dataType: "json",
        beforeSend: function () {
            clearCityList();
        },
        success: function (response) {
            for (var key in response) {
                $('.js-city-id').append('<option value="' + key + '">' + response[key] + '</option>');
            }
        }
    });
}

function clearCityList() {
    $('.js-city-id').empty().append('<option value=""">Не выбрано</option>');
    clearTariffList();
}

function fillTariffList(tenantId, cityId) {
    $.ajax({
        type: "GET",
        url: "/setting/phone-line/get-tariff-list",
        data: {tenantId: tenantId, cityId: cityId},
        dataType: "json",
        beforeSend: function () {
            clearTariffList();
        },
        success: function (response) {
            for (var key in response) {
                $('.js-tariff-id').append('<option value="' + key + '">' + response[key] + '</option>');
            }
        }
    });
}

function clearTariffList() {
    $('.js-tariff-id').empty().append('<option value=""">Не выбрано</option>');
}

function fillPhoneLineScenario(tenantId) {
    $.ajax({
        type: "GET",
        url: "/setting/phone-line/get-phone-line-scenario",
        data: {tenantId: tenantId},
        dataType: "json",
        success: function (response) {
            $('.js-phone-line').val(response);
        }
    });
}

function showExtendedForm() {
    $('.js-extended-form').show();
}

function hideExtendedForm() {
    $('.js-extended-form').hide();
}

function changeExtendedForm() {
    if ($('.js-is-extended-form').val() == 1) {
        showExtendedForm();
    } else {
        hideExtendedForm();
    }
}
