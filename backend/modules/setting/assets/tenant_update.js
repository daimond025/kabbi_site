$(document).ready(function () {
    
    var tenant_id = $('#tenant_id').val();
    
    $('#tariff_list').change(function () {
        $('#tariff_list').attr('value', $('#tariff_list').val());
    });

    $('#save_tariff').click(function () {
        var selected_tariff = $('#tariff_list').val();
        $.ajax({
            type: "POST",
            url: "/setting/tenant/set-tenant-admin-tariff",
            data: {
                tenant_id: tenant_id,
                tariff_id: selected_tariff,
            },
            success: function (json) {
                var obj = JSON.parse(json);
                $('#tariff_changesets').html(obj.view);
                $('#tariff_changesets').trigger('update');
            },
            error: function () {
            }

        });
    });


    $('#addOption').click(function () {
        var option_id = $('#addOptionsDropDown').val();
        $.ajax({
            type: "POST",
            url: "/setting/tenant/set-additional-option",
            data: {
                tenant_id: tenant_id,
                option_id: option_id,
            },
            success: function (json) {
                var obj = JSON.parse(json);
                $('#additional_options').html('');
                $('#additional_options').html(obj.view);
                $('#additional_options').trigger('update');
            },
            error: function (json) {
                console.log(json);
            }
        });
    });
});