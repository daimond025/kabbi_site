/*global generalMap*/
$(function () {

    _timerId = null;
    _updateDriverLocation = null;

    function getClientInfo() {
        var $phone = $('.mask_phone'),
            $cityId = $('#order-city_id');

        if ($phone.attr('is-valid')) {
            fillClientInfo($phone.val(), $cityId.val());
        } else {
            clearClientInfo();
        }
    }

    //Поиск клиента после ввода телефона
    $('body').on('blur', '.mask_phone', function () {
        getClientInfo();
        $(document).trigger('checkCanBonusPayment');
    });

    $('body').on('change', '.order-city_id', function () {
        getClientInfo();
    });

    //Оплата
    $(document).on('change', '#order_payment', function () {
        var company_id = $(this).find(':selected').data('company');
        if (company_id) {
            $('#order-company_id').val(company_id);
        } else {
            $('#order-company_id').val('');
        }
    });

    //Поиск водителя
    var search_val_old = '';

    $('body').on('keyup', '#driver_find', function (e) {
        var input_search = $(this);

        if (e.which != 13)
        {
            var city_id = $('#order-city_id').val();

            setTimeout(function () {

                var search = input_search.val();

                if (search_val_old == search)
                    return false;

                search_val_old = search;

                if (search.length > 0 && $.trim(search) != '')
                {
                    $.ajax({
                        type: "GET",
                        url: '/order/order/get-driver',
                        data: {input_search: input_search.val(), city_id: city_id},
                        dataType: "html",
                        beforeSend: function () {
                            input_search.addClass('input_preloader');
                        },
                        success: function (html) {
                            input_search.removeClass('input_preloader');
                            input_search.next().html(html).show();
                            $('body').addClass('driverSearch');
                        }
                    });
                }
            }, 700);
        }
    });

    //Клик по выбранному водителю
    $('body').on('click', '.ow_cols .driver_auto li:not(".no_select") a', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('#driver_find').val($(this).text());
        $('#order-driver_id').val($(this).data('driver_id'));
        $(this).parents('ul').hide();
    });

    //Сокрытие поиска
    $('body').on('click', function () {
        if ($(this).hasClass('driverSearch'))
        {
            $('ul.driver_auto').hide();
            $(this).removeClass('driverSearch');
        }
    });

    //-------------------------------------------------------------------------

    //Добавление нового адреса
    $('body').on('click', '.form_line a.add_direction', function (e) {
        e.preventDefault();
        var char = $(this).parents('.form_line').find('.od_d').text();
        var obj = $(this);

        $.post(
                '/order/order/get-new-address',
                {char: $.trim(char)},
        function (html) {
            var parent = obj.parents('div.wlc_right');
            parent.after(html);
            parent.find('a.close_red').hide();
            obj.hide();
            $.colorbox.resize();
        }
        );
    });

    //Сокрытие автокомплита при переходе табом
    $('body').on('keydown', '.fl_city input, .fl_street input', function (e) {
        if (e.keyCode == 9)
            $(this).next().hide();
        else if (e.keyCode == 13)
            return false;
    });

    //Автокомплит городов
    $('body').on('keyup', '.fl_city input', function (e) {

        var input_search = $(this);
        var autocomplete_list = input_search.next();

        input_search.closest('.fl_city').find('.js-cur-city_id').val('');

        //Выбор с помощью стрелок
        choose_point_by_arrow(autocomplete_list, e, 'city');

        setTimeout(function () {

            var search = input_search.val();

            if (search_val_old == search || e.keyCode < 48 || (e.keyCode > 90 && e.keyCode != 192))
                return false;

            search_val_old = search;

            if (search.length >= 3)
            {
                $.ajax({
                    type: "POST",
                    url: '/city/city/city-search',
                    data: {city: search},
                    dataType: "html",
                    beforeSend: function () {
                        input_search.addClass('input_preloader');
                    },
                    success: function (html) {
                        input_search.removeClass('input_preloader');
                        autocomplete_list.html(html).show();
                        $('body').addClass('driverSearch');
                    }
                });
            }
        }, 700);
    });

    //Автокомплит улиц
    $('body').on('keyup', '.fl_street input', function (e) {
        var MIN_LENGTH = 1;
        var input_search = $(this);
        var autocomplete_list = input_search.next();
        //Выбор с помощью стрелок
        choose_point_by_arrow(autocomplete_list, e, 'street');

        setTimeout(function () {

            var search = input_search.val();

            if (search_val_old == search || e.keyCode == 13 || e.keyCode == 38 || e.keyCode == 40)
                return false;

            search_val_old = search;

            if (search.length >= MIN_LENGTH)
            {
                var city_id = input_search.parent().prev().find('input[type="hidden"]').val();

                if (city_id != '')
                {
                    $.ajax({
                        type: "POST",
                        url: '/address/street/street-search?view_name=orderSearch&with_public_place=1',
                        data: {street: search, city: city_id},
                        dataType: "html",
                        beforeSend: function () {
                            input_search.addClass('input_preloader');
                        },
                        success: function (html) {
                            input_search.removeClass('input_preloader');
                            autocomplete_list.html(html).show();
                            $('body').addClass('driverSearch');
                        }
                    });
                }
            }
        }, 700);
    });

    //Клик по выбранному городу
    $('body').on('click', '.fl_city .driver_auto li a', function (e) {
        e.preventDefault();
        e.stopPropagation();
        choose_city($(this));
    });

    //Подгрузка парковок
    $('body').on('change', '.fl_mas .fl_minii:first-child input', function (e) {
        var parent = $(this).parents('.fl_inp');
        var city_input = parent.find('.fl_city input');
        var street_input = parent.find('.fl_street input');

        if (street_input.data('type') == 'public')
            return false;

        var city_id = $('#order-city_id').val();
        var street = street_input.val();
        var address = city_input.val() + ', ' + street + ', ' + $(this).val();
        var select = parent.find('.fl_dist .default_select');

        getParkings('/parking/parking/get-parking-by-address', {city_id: city_id, address: address}, select);
    });

    //Клик по выбранной улице
    $('body').on('click', '.fl_street .driver_auto li a', function (e) {
        e.preventDefault();
        e.stopPropagation();
        choose_street($(this));
    });

    $(document).on('change', '.fl_dist .default_select', function () {
        $(this).parent('.fl_dist').find('input[type="hidden"]').val($(this).find('option:selected').data('parking_id'));
    });

    //Сохранение заказа
    $('body').on('click', '.submit_form input', function (e) {
        e.preventDefault();
        if ($(this).hasClass('loading_button') || $(this).parents('.submit_form').find('p.parking_error').is(':visible')) {
            return false;
        }
        var error = false;
        var form = $(this).parents('form');
        form.find('.input_error').removeClass('input_error');
        form.find('select.not_empty').parent().prev('b').css({color: 'black'});

        form.find('.not_empty').each(function () {
            if ($(this).val() == '' || $(this).val() === null)
            {
                if ($(this).hasClass('default_select')) {
                    $(this).parent().prev('b').css({color: 'red'});
                } else {
                    $(this).addClass('input_error');
                }

                error = true;
            }
        });

        $phone = form.find('.mask_phone');
        if (!$phone.attr('is-valid')) {
            $phone.addClass('input_error');
            error = true;
        }

//        var company_id = $('#order-company_id').val();
//
//        if (company_id != '')
//        {
//            var tariff_id = $('#order-tariff_id').val();
//            $.ajax({
//                async: false,
//                type: "POST",
//                url: '/order/order/check-company-tariff',
//                data: {company_id: company_id, tariff_id: tariff_id},
//                dataType: 'json',
//                success: function (json) {
//                    if (json === 0)
//                    {
//                        alert('Выбранный тариф не доступен для текущего способа оплаты.');
//                        error = true;
//                    }
//                }
//            });
//        }

        if (error) {
            return false;
        }

        var firstAddress = $('.fl_inp:first');
        var parking_id = firstAddress.find('.fl_dist select option:selected').data('parking_id');

        $('#order-parking_id').val(parking_id);

        $.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize(),
            dataType: 'json',
            beforeSend: function () {
                $('.submit_form input').addClass('loading_button');
            },
            success: function (json) {
                var cur_page, controller;

                if (json == 2) {
                    $('.date_order_selector .b_sc').show();
                    $('.submit_form input').removeClass('loading_button');
                } else if (json == 3) {
                    alert('Нельзя редактировать время предварительного заказа, т.к. он забронирован водителем');
                    $('.submit_form input').removeClass('loading_button');
                } else {
                    cur_page = location.href;
                    controller = cur_page.search('/operator/') !== -1 ? 'operator' : 'order';
                    location.href = '/order/' + controller + '/index' + location.search;
                }
            },
            error: function () {
                $('.submit_form input').removeClass('loading_button');
            }
        });
    });

    //Смена филиала заказа
    $(document).on('change', '#order-city_id', function () {
        var city_id = $(this).find(':selected').val();
        var name = $(this).find(':selected').text();
        var firstAddress = $('.fl_inp:first .fl_city');
        var secondAddress = $('.fl_inp:eq(1) .fl_city');
        var firstCity = firstAddress.find('.routeAnalysis');
        var secondCity = secondAddress.find('.routeAnalysis');
        var cities;
        var option = '';
        if (firstCity.val() == '')
        {
            firstCity.val(name);
            firstAddress.find('input[type="hidden"]').val(city_id);
        }

        if (secondCity.val() == '')
        {
            secondCity.val(name);
            secondAddress.find('input[type="hidden"]').val(city_id);
        }

        getClientInfo();
        $.get(
            '/city/city/get-city-currency-symbol',
            {city_id: city_id},
            function(json){
                $('span.currency_symbol').text(json);
            },
            'json'
        );

        $.get(
            "/order/order/tariff-ajax-update",
            {city_id: city_id, cities: cities},
            function (json) {
                var firstElem = false;
                var $optionList = $();
                var $optionItem;
                var $comfortList = $();
                var $comfortItem, $comfortLabel, $comfortInput;
                var first_tariff_id = null;
                var hidden = '';

                for (var item in json)
                {
                    $optionItem = $('<option/>');
                    if (firstElem === false) {
                        $optionItem.attr('selected' , 'selected');
                        firstElem = true;
                        first_tariff_id = json[item]['tariff_id'];
                    }

                    $optionItem.val(json[item]['tariff_id']);
                    $optionItem.text(json[item]['name']);
                    $optionList = $optionList.add($optionItem);

                    for (var option_id in json[item]['comfort_list'])
                    {
                        hidden = first_tariff_id !== json[item]['tariff_id'] ? 'display: none' : '';
                        $comfortLabel = $('<label/>')
                                .text(json[item]['comfort_list'][option_id]);
                        $comfortInput = $('<input/>')
                                .attr('name', 'Order[additional_option][]')
                                .attr('type', 'checkbox')
                                .val(option_id);
                        $comfortItem = $('<li>').attr('style', hidden)
                                .data('tariff', json[item]['tariff_id']);
                        $comfortList = $comfortList.add($comfortItem
                                .append($comfortLabel.append($comfortInput)));
                    }
                }
                $('#order-tariff_id').html($optionList);
                $('#order-tariff_id').trigger('update');
                $('form ul.comfort_list').html($comfortList);
                $(document).trigger('checkCanBonusPayment');
            },
            'json'
        )
            .fail(function (data) {
                console.log('fail', data);
                $(document).trigger('checkCanBonusPayment');
            });
    });

    //Календарь на index.php
    $('.sdp_f').datepicker({
        //altField: $(this).prev('input'),
        gotoCurrent: true,
        onSelect: function (date) {
            setAttr('date', date);
        }
    });

    var urlVar = getUrlVar();
    var calendar_date = urlVar !== false && urlVar['date'] !== undefined ? urlVar['date'] : null;

    $('.sdp_f').datepicker('setDate', calendar_date);

    //Таймер
    setInterval(function () {

        var timer = new Timer();

        timer.colon($('.time_colon')); //Двоеточие

        //Местное время городов
        $('.order_cities li').each(function () {
            var clock_obj = timer.clock($(this).data('offset'));

            $(this).find('.hours').text(clock_obj.hours);
            $(this).find('.minutes').text(clock_obj.minutes);
        });

        //Счетчик заказов
        $('.order_table .timer').each(function () {
            timer.run($(this).find('.minutes'), $(this).find('.seconds'), $(this).data('type'));
        });

    }, 1000);

    //Клик на табы
    $(document).on('click', '.tnums ul li a', function () {
        var url = $(this).data('ajax');
        var div_id = $(this).attr('class');
        $(this).find('sup').text('');
        $('#' + div_id).load(url);
    });
    $('.order_cities li a.active').on('click', function (e) {
        e.preventDefault()
    });

    //Клик на табы
    $(document).on('click', '.tnums ul li a', function () {
        var url = $(this).data('ajax');
        var div_id = $(this).attr('class');
        $(this).find('sup').text('');
        $('#' + div_id).load(url);
    });

    //Показ селекта отмены заказа
    $(document).on('change', '#order-status_id', function () {
        var selected = $(this).find('option:selected');

        if (selected.hasClass('cancel'))
        {
            $('#rejected_reason').show();
        }
        else
        {
            $('#rejected_reason').hide();
        }

        $.colorbox.resize();
    });

    //Фильтр статусов
    $(document).on('click', 'ul.sortable input', function () {
        var orders = $(this).parents('div.active').find('.order_table tr').not(':first');
        var ul = $(this).parents('ul');
        var input_checked = ul.find('input:checked');

        orders.filter(':hidden').show();

        if ($(this).val() == '0')
        {
            input_checked.not(':first').removeAttr('checked');
        }
        else if (input_checked.size() > 0)
        {
            ul.find('input').filter(':first').removeAttr('checked');

            input_checked.each(function () {
                orders = orders.not('.status_' + $(this).val());
            });

            orders.hide();
        }
    });

    //Фильтр статусов из редиса
    $(document).on('click', 'ul.redis_sort input', function () {
        var ul = $(this).parents('ul');
        var input_checked = ul.find('input:checked');
        var arr_status_id = [];
        var tbody = ul.next('table').find('tbody');
        var sort_class = tbody.find('tr:first a span').attr('class');
        var sort = null;
        var filter = {status: null};

        if ($(this).val() == '0')
        {
            input_checked.not(':first').removeAttr('checked');
        }
        else if (input_checked.size() > 0)
        {
            ul.find('input').filter(':first').removeAttr('checked');

            input_checked.each(function () {
                arr_status_id.push($(this).val());
            });
        }

        sort = sort_class === 'pt_up' ? 'desc' : 'asc';
        filter.status = arr_status_id;

        $.post(
                $(this).parents('ul').data('url'),
                {sort: sort, filter: filter},
        function (html) {
            put_search(tbody, html);
        }
        );
    });

    //Сортировка таблиц
    $(document).on('click', 'table.sortable th a', function (e) {
        e.preventDefault();
        $(this).parents('tr').find('a').removeClass('sort');
        $(this).addClass('sort');
        sort_order_table($(this));
    });

    //Сортировка заказов на сегодня из БД
    $(document).on('click', 'table.redis_sort th a', function (e) {
        e.preventDefault();
        $(this).parents('tr').find('a').removeClass('sort');
        $(this).addClass('sort');
        var sort_span = $(this).find('span');
        var sort_class = sort_span.attr('class');
        $(this).parents('tr').find('a span').attr('class', '');
        var sort = null;
        var tbody = $(this).parents('tbody');
        var status_filter = $(this).parents('table').prev('ul').find('input:gt(0)').filter(':checked');
        var filter = {status: []};

        if (status_filter.size() > 0)
        {
            status_filter.each(function () {
                filter.status.push($(this).val());
            });
        }

        if (sort_class === 'pt_up') {
            sort = 'asc';
            sort_class = 'pt_down';
        }
        else {
            sort = 'desc';
            sort_class = 'pt_up';
        }

        $.post(
                $(this).attr('href'),
                {sort: sort, filter: filter},
        function (html) {
            put_search(tbody, html);
        }
        );

        sort_span.attr('class', sort_class);
    });

    //Расчет стоимости заказа
    $(document).on('getPrice', function () {
        if (check_streets_filled())
            getOrderPriceAnalyz($('#order_create'));
    });
    $(document).on('change', '#order-tariff_id, .comfort_list input, #order-city_id, select.parkings', function () {
        $(document).trigger('getPrice');
    });

    $(document).on('change', '#order-tariff_id', function () {
        var tariff_id = $(this).val();
        var comfort_list = $('.comfort_list li');
        comfort_list.each(function () {
            $(this).find('input').prop('checked', false);
            if ($(this).data('tariff') == tariff_id)
                $(this).show();
            else
                $(this).hide();
        });
        $(document).trigger('checkCanBonusPayment');
    });

    call_init();

    //Всплывающее окно водителей на парковках
    $('body').on('click', '.order_status>div', function (e) {
        if ($(this).find('div').css('display') == 'block') {
            $('.order_status>div div').hide();
        } else {
            $('.order_status>div div').hide();
            $(this).find('div').show();
            e.preventDefault();
            e.stopPropagation();
            $(this).find('div').on('click', function (e) {
                e.stopPropagation()
            });
            $('body').on('click', function () {
                $('.order_status>div div').hide();
            });
            var cur_div = $(this);
            $.get(
                    cur_div.data('href'),
                    {},
                    function (html) {
                        cur_div.find('div').html(html);
                    }
            );
        }
    });

    $(".open_map_win").colorbox({
        inline: true,
        width: "90%",
        height: "90%",
        overlayClose: false,
        onComplete: function () {
            window.generalMap && generalMap.initialize();
        },
        onClosed: function () {
            window.generalMap && generalMap.finalize();
        }
    });

    //Удаление блока нового адреса
    $(document).on('click', 'a.close_red', function(e){
        e.preventDefault();
        var parent = $(this).parents('div.wlc_right');
        var last_block_address = parent.prev();

        parent.remove();
        last_block_address.find('a.close_red, a.add_direction').show();
        $.colorbox.resize();
    });
});
