/*global orderMap*/
function fillClientInfo(phone, cityId)
{
     $.get(
        '/order/order/get-client',
        {phone: phone, city_id: cityId},
        function (json) {
            clearClientInfo();
            if (json.name !== undefined)
            {
                $('#client_name').text(json.name).attr('href', json.url);
                $('#order-client_id').val(json.client_id);
            }

            if (json.client_more_info) {
                $('.client_more_info').html(json.client_more_info);
            }

            if (json.payment_html_select !== undefined)
            {
                $('#order_payment').replaceWith(json.payment_html_select);
                $('#order_payment').trigger('update');
            }

            if (json.black_list == 1)
            {
                $('#order-phone').addClass("in_black_list");
            }
            $('#order-company_id').val('');
            $('#call_to_client').attr('href', 'call:' + phone);
            $.colorbox.resize();
    }, 'json');
}


function clearClientInfo() {
    $('#client_name').text('').next().val('');
    $('#order-client_id').val('');
    $('.client_more_info').html('');
    $('#order-bonus_payment').prop('checked', false);
    $('#order-phone').removeClass("in_black_list");
    $('#order-company_id').val('');
    $('#call_to_client').attr('href', 'call:');
    $.colorbox.resize();
}


function timer_init()
{
    _timerId = setInterval(function () {

        var timer = new Timer();

        timer.colon($('.time_colon')); //Двоеточие

        var timer_obj = $('form .timer');
        timer.run(timer_obj.find('.minutes'), timer_obj.find('.seconds'), timer_obj.data('type'));

    }, 1000);
}

function init_pac()
{
    select_init();
    phone_mask_init();
    timer_init();
    window.OrderDatepicker && OrderDatepicker.init();
}

function colorbox_init()
{
    $(function () {
        $(document).on('click', '.add_order, .ot_edit', function (e) {
            e.preventDefault();
            $.colorbox({
                href: $(this).attr('href'),
                overlayClose: false,
                width: "820px",
                onComplete: function () {
                    init_pac();
                    $.colorbox.resize();

                    window.orderMap && orderMap.initialize();
                },
                onClosed: function () {
                    window.orderMap && orderMap.finalize();

                    clearTimeout(_timerId);
                    clearInterval(_updateDriverLocation);
                }
            });
        });
    });
}

function getParkings(url, data, select)
{
    $.ajax({
        type: 'GET',
        url: url,
        data: data,
        dataType: "json",
        success: function (json) {
            var first_option = select.find('option:first');
            var selected = '';
            var $selectItem;

            first_option.removeAttr('selected');
            select.find('option:gt(0)').remove();

            if (json.parking) {
                for (var i = 0; i < json.parking.length; i++)
                {
                    selected = json.parking[i].parking_id == json.inside;
                    $selectItem = $('<option/>')
                            .data('parking_id', json.parking[i].parking_id)
                            .val(json.parking[i].name)
                            .text(json.parking[i].name);

                    select.append($selectItem);
                    if (selected) {
                        $selectItem.attr('selected', 'selected');
                        select.parent('.fl_dist').find('input[type="hidden"]').val(json.parking[i].parking_id);
                    }
                }
            }

            if (json.error !== null || json.inside === null)
            {
                first_option.attr('selected', 'selected');
            }

            select.trigger('update');
            getOrderPriceAnalyz(select.parents('form'));
        }
    });
}

function getOrderPriceAnalyz(form)
{
    $('input.lat, input.lon').not('.not_empty').val('');
    var parking_error_block = $('section.submit_form p.parking_error');
    parking_error_block.hide();

    $.ajax({
        type: "POST",
        url: '/order/order/route-analyzer',
        data: form.serialize(),
        dataType: "json",
        beforeSend: function () {
            $(document).trigger('disableMap');
        },
        complete: function () {
            $(document).trigger('enableMap');
        },
        success: function (data) {
            var analyzerBlock = $('#routeAnalyzer'),
                routePoints = [];

            if (data != '')
            {
                if (data.error != undefined)
                {
                    if (data.error == 100) {
                        parking_error_block.show();
                        $.colorbox.resize();
                    }

                    return;
                }

                analyzerBlock.find('input').val(Math.round(data.summaryCost));
                analyzerBlock.find('.distance').text(data.summaryDistance).css('color', '');
                $('#order-predv_distance').val(data.summaryDistance);

                var summaryTime = Math.round(data.summaryTime);
                analyzerBlock.find('.time').text(summaryTime).css('color', '');
                $('#order-predv_time').val(summaryTime);

                analyzerBlock.find('div').show();

                if (data.addressCoords !== undefined)
                {
                    var firstCityId, currentIndex = 0;

                    $('.form_line').each(function (index) {
                        var _this = $(this),
                            cityId = _this.find('.js-cur-city_id').val(),
                            street = _this.find('.fl_street input').val();

                        if (!firstCityId) {
                            firstCityId = cityId;
                        }

                        if (!(index && (!cityId || (!('' + street).trim().length && firstCityId === cityId)))
                                && data.addressCoords[currentIndex]) {
                            _this.find('.lat').val(data.addressCoords[currentIndex].lat);
                            _this.find('.lon').val(data.addressCoords[currentIndex].lon);

                            currentIndex++;
                        };
                    });
                }
                routePoints = data.routeLine;
            }
            else
            {
                analyzerBlock.find('.distance').css('color', 'red').text('--');
                analyzerBlock.find('.time').css('color', 'red').text('--');
                analyzerBlock.find('div').show();
            };
            $(document).trigger('updateMap', [routePoints]);
        },
    });
}

function sort_order_table(a)
{
    var column = a.data('column');
    var sort_span = a.find('span');
    var sort_class = sort_span.attr('class');
    var table = a.parents('table');
    var all_sort_span = table.find('th a span');
    var tr = table.find('tr').not(':first');
    var sortable = [];
    var dir = null;

    all_sort_span.attr('class', '');

    if (sort_class === 'pt_up') {
        dir = 1;
        sort_class = 'pt_down';
    }
    else {
        dir = -1;
        sort_class = 'pt_up';
    }

    tr.each(function () {
        var sort_data = $(this).find('td:eq(' + column + ')').data('sort');
        sortable.push([$(this).attr('id'), sort_data]);
    });

    sortable.sort(function (a, b) {
        if (a[1] < b[1])
            return (-1) * dir;
        else if (a[1] > b[1])
            return dir;
        else
            return 0;
    });

    for (var i = 0; i < sortable.length; i++)
    {
        $('#' + sortable[i][0]).appendTo(table);
    }
    sort_span.attr('class', sort_class);
}

function call_init()
{
    $('div.orders').on('click', '.pt_call', function () {

        var call_type = $(this).data('type');
        var caller_phone = $(this).data('call');

        if (caller_phone != '')
        {
            try {
                if (call_type == 'client')
                {
                    window.qt_handler.setDriverPhone(caller_phone);
                }
                else if (call_type == 'driver')
                {
                    window.qt_handler.setClientPhone(caller_phone);
                }
            } catch (e) {
                console.log('Ошибка');
            }
        }
    });
}

//Клик по выбранному городу
function choose_city(point_of_autocomplete_list)
{
    var city_id = point_of_autocomplete_list.data('city');
    var ul = point_of_autocomplete_list.parents('ul');
    var city_text = point_of_autocomplete_list.text();
    var $form_line = point_of_autocomplete_list.parents('.form_line'),
        lat = point_of_autocomplete_list.data('lat'),
        lon = point_of_autocomplete_list.data('lon');

    city_text = city_text.split(', ');
    ul.hide().prev('input').val($.trim(city_text[1]));
    ul.next('input').val(city_id);

    if (lat && lon) {
        $form_line.find('input.lat').val(lat).addClass('not_empty');
        $form_line.find('input.lon').val(lon).addClass('not_empty');
    } else {
        $form_line.find('input.lat').removeClass('not_empty');
        $form_line.find('input.lon').removeClass('not_empty');
    }

    getOrderPriceAnalyz(point_of_autocomplete_list.parents('form'));
}

//Клик по выбранной улице
function choose_street(point_of_autocomplete_list)
{
    var lon = point_of_autocomplete_list.data('lon');
    var street_type = lon != '' ? 'public' : 'street';
    var form_line = point_of_autocomplete_list.parents('div.form_line');

    point_of_autocomplete_list
            .parents('ul').hide()
            .prev('input')
                .data('type', street_type)
                .val(point_of_autocomplete_list.text());

    if (lon != '')
    {
        var parent = point_of_autocomplete_list.parents('.fl_inp');
        var city_input = parent.find('.fl_city input');
        var city_id = city_input.nextAll('input').val();
        var select = parent.find('.fl_dist .default_select');
        var lat = point_of_autocomplete_list.data('lat');

        form_line.find('input.lat').val(lat).addClass('not_empty');
        form_line.find('input.lon').val(lon).addClass('not_empty');

        getParkings('/parking/parking/get-parking-by-coords', {city_id: city_id, lat: lat, lon: lon}, select);

        if (check_streets_filled())
            getOrderPriceAnalyz(point_of_autocomplete_list.parents('form'));
    }
    else
    {
        form_line.find('input.lat').removeClass('not_empty');
        form_line.find('input.lon').removeClass('not_empty');
        var house_val = point_of_autocomplete_list.parents('.fl_inp').find('.fl_mas .routeAnalysis').val();
        if (house_val != '')
            getOrderPriceAnalyz(point_of_autocomplete_list.parents('form'));
    }

}

//Проверка заполнения двух улиц для запуска анализатора маршрута
function check_streets_filled()
{
    var streets = $('.fl_street input');
    var filled = true;

    streets.each(function () {
        if ($(this).val() == '') {
            filled = false;
        }
    });

    return filled;
}

//Выбор улиц с помощью стрелок и переключение при табуляции
function choose_point_by_arrow(autocomplete_list, event, type)
{
    if (autocomplete_list.is(':visible'))
    {
        var points_by_list = autocomplete_list.find('li a');

        //Вверх
        if (event.keyCode == 38)
        {
            if (points_by_list.hasClass('autocomplete_hover'))
            {
                var prev_point = points_by_list.filter('.autocomplete_hover').removeClass('autocomplete_hover').parent().prev();

                if (prev_point.size() > 0)
                    prev_point.find('a').addClass('autocomplete_hover');
                else
                    points_by_list.last().addClass('autocomplete_hover');
            }
        }
        //Вверх
        else if (event.keyCode == 40)
        {
            if (points_by_list.hasClass('autocomplete_hover'))
            {
                var next_point = points_by_list.filter('.autocomplete_hover').removeClass('autocomplete_hover').parent().next();

                if (next_point.size() > 0)
                    next_point.find('a').addClass('autocomplete_hover');
                else
                    points_by_list.eq(0).addClass('autocomplete_hover');
            }
            else
            {
                points_by_list.eq(0).addClass('autocomplete_hover');
            }
        }
        //Выбор
        else if (event.keyCode == 13 && points_by_list.hasClass('autocomplete_hover'))
        {
            if (type == 'city')
                choose_city(points_by_list.filter('.autocomplete_hover'));
            else if (type == 'street')
                choose_street(points_by_list.filter('.autocomplete_hover'));
        }
    }
}