/*global L, $*/
function init_map(center_coords, zoom, selector)
{
    var map, osmLayer;

    $('#' + selector).empty();
    map = L.map(selector).setView(center_coords, zoom);
    map.setMaxBounds(L.latLngBounds(L.latLng(85, -180), L.latLng(-85, 180)));

    // Bugfix for zoom buttons https://github.com/shramov/leaflet-plugins/issues/62
    L.polyline([[0, 0]]).addTo(map);


    osmLayer = new L.TileLayer('//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');



    map.addLayer(osmLayer);


    return map;
}
