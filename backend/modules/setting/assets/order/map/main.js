/*global L*/
var coordinatesLoader = {
    displayError: function (error) {
        $('#b03').html($('<p/>').text(error));
        $.colorbox.resize();
    },

    getAddressIcon: function (key) {
        var iconMap = {
                'A': 'map_icon_point_a',
                'B': 'map_icon_point_b',
                'C': 'map_icon_point_c',
                'D': 'map_icon_point_d',
                'E': 'map_icon_point_e'
            };

        return !iconMap[key]
            ? null : L.divIcon({
                            iconAnchor: [12, 28],
                            className: iconMap[key]
                        });
    },

    getAddressString: function (obj) {
        return [obj.city, obj.street, obj.house, obj.housing, obj.porch, obj.apt]
                .filter(function (value) {
                    return value;
                }).join(', ');
    },

    setCenter: function (map, points) {
        var bounds;

        if (!map || !Array.isArray(points)) {
            return;
        }
        
        bounds = new L.latLngBounds(points);
        map.fitBounds(bounds, { maxZoom: 13 });
    },

    getAddressMarkers: function (L, addressJson) {
        var key, marker, point, markers, icon, markerParams, template;

        markers = [];
        for (key in addressJson) {
            if (addressJson.hasOwnProperty(key)) {
                if (addressJson[key].lat && addressJson[key].lon) {
                    point = L.latLng(addressJson[key].lat, addressJson[key].lon);
                } else {
                    continue;
                }

                icon = this.getAddressIcon(key);
                markerParams = icon ? { icon: icon } : {};

                template =
                    '<div class="win_map_content">\
                        <div class="wmc_win">\
                            <div class="wmcw_content">\
                                <span>' + this.getAddressString(addressJson[key]) + '</span>\
                            </div>\
                        </div>\
                    </div>';
                marker = L.marker(point, markerParams).bindPopup(template);
                markers.push(marker);
            }
        }
        return markers;
    },

    addMarkersToMap: function (map, markers) {
        var i, l;
        for (i = 0, l = markers.length; i < l; i++) {
            markers[i].addTo(map);
        }
    },

    getMarkerPoints: function (markers) {
        var i, l, points = [];
        for (i = 0, l = markers.length; i < l; i++) {
            points.push(markers[i].getLatLng());
        }
        return points;
    },

    getDriverSpeedTemplate: function (speed) {
        var speedCaption = 'Скорость ' + speed + ' км/ч';

        return '<div class="win_map_content">\
                    <div class="wmc_win">\
                        <div class="wmcw_content">\
                            <span>' + speedCaption + '</span>\
                        </div>\
                    </div>\
                </div>';
    },

    getBusyDriverIcon: function () {
        return L.divIcon({
            iconAnchor: [18, 47],
            className: 'busy_car_icon'
        });
    },

    registerUpdateDriverInterval: function (L, callsign, marker) {
        'use strict';
        var _this = this;

        _updateDriverLocation = setInterval(function () {
            $.get(
                '/setting/order-search/get-worker-coords-by-callsign',
                {callsign: callsign},
                function (json) {
                    var driverCoords = L.latLng([json.lat, json.lon]);
                    marker.setLatLng(driverCoords).setPopupContent(_this.getDriverSpeedTemplate(json.speed));
                    marker.setIconAngle(json.iconAngle);
                },
                'json'
            );
        }, 3000);
    },

    loadTrackingCoords: function (L) {
        'use strict';
        var _this = this;
        $.get(
            '/setting/order-search/get-tracking',
            {order_id: $('#map').data('order')},
            function (json) {
                var icon, markerParams;

                if (json.error !== undefined)
                {
                    _this.displayError(json.error);
                    return;
                }

                var points = [];

                for (var i = 0; i < json.order_route.length; i++)
                {
                    var latlng = L.latLng([json.order_route[i].lat, json.order_route[i].lon]);
                    points.push(latlng);
                }

                if (!points.length) {
                    return;
                }

                var map = init_map(points[0], 13, 'map');

                L.polyline(points, {color: 'red'}).addTo(map);

                icon = _this.getAddressIcon('A');
                markerParams = icon ? { icon: icon } : {};
                L.marker(points[0], markerParams).addTo(map);

                icon = _this.getAddressIcon('B');
                markerParams = icon ? { icon: icon } : {};
                L.marker(points[points.length-1], markerParams).addTo(map);

                _this.setCenter(map, points);
            },
            'json'
        );
    },

    loadDriverCoords: function (L) {
        'use strict';
        var _this = this;

        $.when(
                $.ajax({
                    url: '/setting/order-search/get-worker-coords-by-id',
                    data: {workerId: $('#order-worker_id').val()},
                    dataType: 'json'
                }),
                $.ajax({
                    url: '/setting/order-search/get-order-address',
                    data: { order_id: $('#map').data('order') },
                    dataType: 'json'
                })
        )
                .done(function (workerResponse, addressResponse) {
                    var map, marker, markers, points, workerCoords,
                        workerJson = workerResponse[0],
                        addressJson = addressResponse[0];

                    if (!workerJson) {
                        return;
                    } else if (workerJson.error) {
                        _this.displayError(workerJson.error);
                    }

                    workerCoords = L.latLng([workerJson.lat, workerJson.lon]);
                    map = init_map(workerCoords, 16, 'map');

                    marker = L.marker(workerCoords, {
                        icon: _this.getBusyDriverIcon(),
                        iconAngle: workerJson.iconAngle
                    }).addTo(map).bindPopup(_this.getDriverSpeedTemplate(workerJson.error));

                    _this.registerUpdateDriverInterval(L, workerJson.callsign, marker);

                    if (!addressJson || addressJson.error) {
                        return;
                    }

                    markers = _this.getAddressMarkers(L, addressJson);
                    if (!markers || !markers.length) {
                        return;
                    }

                    _this.addMarkersToMap(map, markers);

                    points = _this.getMarkerPoints(markers);
                    points.push(workerCoords);

                    _this.setCenter(map, points);
                })
                .fail(function (data) {
                    console.log('fail', data);
                });
    },

    loadAddressCoords: function (L) {
        'use strict';
        var _this = this;
        $.get(
            '/setting/order-search/get-order-address',
            {order_id: $('#map').data('order')},
            function (json) {
                var map, markers;

                if (!json) {
                    return;
                } else if (json.error) {
                    _this.displayError(json.error);
                    return;
                }

                markers = _this.getAddressMarkers(L, json);
                if (!markers || !markers.length) {
                    return;
                }

                map = init_map(markers[0].getLatLng(), 13, 'map');
                _this.addMarkersToMap(map, markers);

                _this.setCenter(map, _this.getMarkerPoints(markers));
            },
            'json'
        );
    }
};

function init_map_data(map_data_type)
{
    if (coordinatesLoader) {
        switch (map_data_type) {
            case 'route':
                coordinatesLoader.loadTrackingCoords(L);
                break;
            case 'driver':
                coordinatesLoader.loadDriverCoords(L);
                break;
            case 'address':
                coordinatesLoader.loadAddressCoords(L);
                break;
        }
    }
}
