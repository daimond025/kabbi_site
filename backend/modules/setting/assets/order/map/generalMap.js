/*global jQuery, L*/
var generalMap = (function ($) {
    'use strict';

    function MarkerCollection() {
        this.markers = null;
        this.action = null;
        this.iconClass = null;
        this.filter = null;
        this.active = false;
    }
    MarkerCollection.prototype.addMarkersToMap = function (map) {
        var key;

        if (!map || !this.markers) {
            return;
        }
        for (key in this.markers) {
            if (this.markers.hasOwnProperty(key)) {
                map.addLayer(this.markers[key]);
            }
        }
    };
    MarkerCollection.prototype.removeMarkersFromMap = function (map) {
        var key;

        if (!map || !this.markers) {
            return;
        }
        for (key in this.markers) {
            if (this.markers.hasOwnProperty(key)) {
                map.removeLayer(this.markers[key]);
            }
        }
    };
    MarkerCollection.prototype.addMarkers = function (markers) {
        var _this = this, icon, latLng, iconAngle, key, markerKeys = [];

        icon = L.divIcon({
                iconAnchor: [18, 47],
                className: _this.iconClass
            });

        if (!this.markers) {
            this.markers = [];
        }
        markers.forEach(function (item) {
            if (!item.coords || !item.coords.lat || !item.coords.lon) {
                return;
            }
            latLng = L.latLng([ item.coords.lat, item.coords.lon ]);
            iconAngle = item.iconAngle ? item.iconAngle : 0;

            markerKeys.push(item.key_name);
            if (_this.markers[item.key_name]) {
                _this.markers[item.key_name].setLatLng(latLng);
                _this.markers[item.key_name].setIconAngle(iconAngle);
            } else {
                _this.markers[item.key_name] =
                        L.marker(latLng, { icon: icon, iconAngle: iconAngle });
                _this.markers[item.key_name].bindPopup(item.marker_info);
            }
        });

        for (key in _this.markers) {
            if (_this.markers.hasOwnProperty(key)
                    && markerKeys.indexOf(key) === -1) {
                delete _this.markers[key];
            }
        }
    };
    MarkerCollection.prototype.getRequest = function (cityId) {
        return $.post(
                '/order/general-map/' + this.action,
                {city_id: cityId}
        );
    };


    function OrderCollection() {
        MarkerCollection.apply(this, arguments);
        this.filter = 'orders';
        this.action = 'get-orders';
        this.iconClass = 'order_icon';
    }
    OrderCollection.prototype = Object.create(MarkerCollection.prototype);
    OrderCollection.prototype.constructor = OrderCollection;

    function FreeCarCollection() {
        MarkerCollection.apply(this, arguments);
        this.filter = 'free_cars';
        this.action = 'get-free-cars';
        this.iconClass = 'free_car_icon';
    }
    FreeCarCollection.prototype = Object.create(MarkerCollection.prototype);
    FreeCarCollection.prototype.constructor = FreeCarCollection;

    function BusyCarCollection() {
        MarkerCollection.apply(this, arguments);
        this.filter = 'busy_cars';
        this.action = 'get-busy-cars';
        this.iconClass = 'busy_car_icon';
    }
    BusyCarCollection.prototype = Object.create(MarkerCollection.prototype);
    BusyCarCollection.prototype.constructor = BusyCarCollection;

    function PauseCarCollection() {
        MarkerCollection.apply(this, arguments);
        this.filter = 'pause_cars';
        this.action = 'get-pause-cars';
        this.iconClass = 'pause_car_icon';
    }
    PauseCarCollection.prototype = Object.create(MarkerCollection.prototype);
    PauseCarCollection.prototype.constructor = PauseCarCollection;


    var app = {
        map: null,
        cityId: null,
        $filterElems: null,
        markerCollections: null,
        updateMapInterval: null,

        initialize: function () {
            var _this = this;
            _this.cityId = _this.getCityId();

            _this.$filterElems = $('.win_map_header input[type="checkbox"]');
            _this.registerFilterEvent();

            _this.initMap();
            _this.refreshMap(true);

            _this.updateMapInterval = setInterval(function () {
                _this.refreshMap();
            }, 5000);
        },

        finalize: function () {
            this.cityId = null;
            this.$filterElems = null;
            clearInterval(this.updateMapInterval);

            this.removeMap();
        },

        initMap: function () {
            // bugfix
            // When initializing the map html container must be visible
            var $mapContainer = $('#orders_map');
            $mapContainer.show();
            this.map = init_map([0, 0], 0, 'orders_map');
            $mapContainer.hide();

            this.markerCollections = [
                new OrderCollection(),
                new FreeCarCollection(),
                new BusyCarCollection(),
                new PauseCarCollection()
            ];
        },

        removeMap: function () {
            this.markerCollections = null;

            if (this.map) {
                this.map.remove();
                this.map = null;
            }
        },

        refreshMap: function (center) {
            var _this = this,
                filters = _this.getFilters(),
                markerCollections = [],
                requests = [],
                request;

            if (!Array.isArray(_this.markerCollections)) {
                return;
            }

            _this.markerCollections.forEach(function (item) {
                item.active = false;
                item.error = null;

                if (filters.indexOf(item.filter) >= 0) {
                    markerCollections.push(item);
                    request = item.getRequest();
                } else {
                    item.removeMarkersFromMap(_this.map);
                    request = null;
                }
                requests.push(request);
            });

            $.when.apply($, requests)
                    .done(function () {
                        var args = Array.prototype.slice.call(arguments),
                            countActiveCollections = 0,
                            $mapContainer, $errorContainer, errorMessage;

                        if (requests.length === 1) {
                            args = [args];
                        }

                        args.forEach(function (item, index) {
                            if (!item || !item[0] || item[1] !== 'success') {
                                return;
                            }

                            if (item[0].message) {
                                errorMessage = item[0].message;
                            } else if (Array.isArray(item[0].markers) && item[0].markers.length){
                                _this.markerCollections[index].active = true;
                                _this.markerCollections[index].addMarkers(item[0].markers);
                                _this.markerCollections[index].addMarkersToMap(_this.map);

                                countActiveCollections++;
                            }
                        });

                        $mapContainer = $(_this.map.getContainer());
                        $errorContainer = $mapContainer.parent().find('p');

                        if (!countActiveCollections) {
                            $mapContainer.hide();
                            $errorContainer.text(
                                    errorMessage ? errorMessage : $errorContainer.data('empty'));
                            $errorContainer.show();
                        } else {
                            $errorContainer.hide();
                            $mapContainer.show();
                            if (center) {
                                _this.setCenter();
                            }
                        }
                    })
                    .fail(function (data) {
                        console.log('fail', data);
                    });
        },

        setCenter: function () {
            var _this = this,
                points = [],
                bounds,
                i, l, key;

            if (!Array.isArray(_this.markerCollections)) {
                return;
            }

            for (i = 0, l = _this.markerCollections.length; i < l; i++) {
                for (key in _this.markerCollections[i].markers) {
                    if (_this.markerCollections[i].markers.hasOwnProperty(key)) {
                        points.push(_this.markerCollections[i].markers[key].getLatLng());
                    }
                }
            }

            if (!points) {
                return;
            }

            bounds = new L.latLngBounds(points);
            _this.map.fitBounds(bounds, { maxZoom: 13 });
        },

        getCityId: function () {
            var urlVars = getUrlVar();
            return urlVars && urlVars.city_id ? urlVars.city_id : null;
        },

        registerFilterEvent: function () {
            var _this = this;

            if (_this.$filterElems) {
                _this.$filterElems.on('change', function () {
                    _this.refreshMap(true);
                });
            }
        },

        getFilters: function () {
            var filters = [];

            this.$filterElems.each(function () {
                var $this = $(this);

                if ($this.prop('checked')) {
                    filters.push($this.val());
                }
            });
            return filters;
        }
    };

    return {
        initialize: function () {
            app.initialize();
        },
        finalize: function () {
            app.finalize();
        }
    };

})(jQuery);