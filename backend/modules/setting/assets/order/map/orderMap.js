/*global jQuery, L, getOrderPriceAnalyz*/
var orderMap = (function ($) {
    'use strict';

    var app = {
        map: null,
        markers: [],
        routePoints: [],
        routePolyline: null,

        initialize: function () {
            this.registerShowMapEvent();
            this.registerMapEvents();
        },

        finalize: function () {
            this.finalizeMap();
        },

        registerShowMapEvent: function () {
            var _this = this;

            $('.js-show-map').click(function () {
                var $elem = $('.js-order-map');
                $elem.toggle();

                if ($elem.not(':hidden').length) {
                    if (!_this.map) {
                        _this.initializeMap();
                    } else {
                        _this.refreshMarkers();
                        _this.refreshRoutes(_this.routePoints);
                    }
                }

                $.colorbox.resize();

                return false;
            });
        },

        registerMapEvents: function () {
            var _this = this;

            $(document).on('updateMap', function (e, routePoints) {
                _this.routePoints = routePoints;
                if (_this.map
                        && $('.js-order-map').not(':hidden').length) {
                    _this.refreshMarkers();
                    _this.refreshRoutes(routePoints);
                }
            });

            $(document).on('disableMap', function () {
                if (_this.map) {
                    _this.disableMap();
                }
            });

            $(document).on('enableMap', function () {
                if (_this.map) {
                    _this.enableMap();
                }
            });
        },

        finalizeMap: function () {
            if (this.map) {
                this.map.remove();
                this.map = null;
            }

            this.markers = [];
            this.routePolyline = null;
            this.routePoints = [];

            $('.js-order-map').empty();
        },

        initializeMap: function () {
            $('.js-order-map')
                    .append($('<div id="order-map" style="height: 500px"/>'));

            this.map = init_map(['0', '0'], 0, 'order-map');
            this.refreshMarkers();

            if (this.routePoints && this.routePoints.length) {
                this.refreshRoutes(this.routePoints);
            }
        },

        disableMap: function () {
            this.map.dragging.disable();
            this.map.touchZoom.disable();
            this.map.doubleClickZoom.disable();
            this.map.scrollWheelZoom.disable();
            this.map.boxZoom.disable();
            this.map.keyboard.disable();
            if (this.map.tap) {
                this.map.tap.disable();
            }
        },

        enableMap: function () {
            this.map.dragging.enable();
            this.map.touchZoom.enable();
            this.map.doubleClickZoom.enable();
            this.map.scrollWheelZoom.enable();
            this.map.boxZoom.enable();
            this.map.keyboard.enable();
            if (this.map.tap) {
                this.map.tap.enable();
            }
        },

        getRequestCityCoords: function (cityId) {
            return $.post(
                    '/city/city/get-city-coords',
                    { city_id: cityId}
                );
        },

        removeMarkers: function () {
            var i;

            for (i = 0; i < this.markers.length; i++) {
                this.map.removeLayer(this.markers[i]);
            }
            this.markers = [];
        },

        markerDragEndEvent: function () {
            var latLng = this.getLatLng();
            this.formElement.find('.lat')
                    .val(latLng.lat).addClass('not_empty');
            this.formElement.find('.lon')
                    .val(latLng.lng).addClass('not_empty');

            getOrderPriceAnalyz($(this.formElement).closest('form'));
        },

        getAddressIcon: function (key) {
            var iconMap = {
                    'A': 'map_icon_point_a',
                    'B': 'map_icon_point_b',
                    'C': 'map_icon_point_c',
                    'D': 'map_icon_point_d',
                    'E': 'map_icon_point_e'
                };

            return !iconMap[key]
                ? null : L.divIcon({
                                iconAnchor: [12, 28],
                                className: iconMap[key]
                            });
        },

        getAddressString: function ($elem) {
            return [
                $elem.find('.fl_city input[name$="[city]"]').val(),
                $elem.find('.fl_street input[name$="[street]"]').val(),
                $elem.find('.fl_mas input[name$="[house]"]').val(),
                $elem.find('.fl_mas input[name$="[housing]"]').val(),
                $elem.find('.fl_mas input[name$="[porch]"]').val(),
                $elem.find('.fl_mas input[name$="[apt]"]').val()
            ].filter(function (value) {
                return value;
            }).join(', ');
        },

        refreshMarkers: function () {
            var _this = this,
                template,
                deferredObjs = [
                    _this.getRequestCityCoords($('.js-city_id').val())
                ];

            _this.removeMarkers();

            $('.form_line').each(function (index) {
                var $elem = $(this),
                    icon,
                    marker,
                    markerParams,
                    title = $elem.find('.od_d').text(),
                    lat = $elem.find('.lat').val(),
                    lon = $elem.find('.lon').val(),
                    cityId = $elem.find('.js-cur-city_id').val(),
                    street = $elem.find('.fl_street input').val();

                if (index && (!cityId || (!('' + street).trim().length && _this.markers[0].cityId === cityId))) {
                    return;
                } else {
                    template =
                        '<div class="win_map_content">\
                            <div class="wmc_win">\
                                <div class="wmcw_content">\
                                    <span>' + _this.getAddressString($elem) + '</span>\
                                </div>\
                            </div>\
                        </div>';

                    markerParams = {
                        title: title,
                        draggable: true
                    };

                    icon = _this.getAddressIcon(title);
                    if (icon) {
                        markerParams.icon = icon;
                        markerParams.iconAnchor = ['12', '28'];
                    }

                    marker = L.marker([0, 0], markerParams)
                        .bindPopup(template)
                        .addTo(_this.map);

                    marker.cityId = cityId;
                    marker.formElement = $elem;
                    marker.on('dragend', _this.markerDragEndEvent);

                    _this.markers.push(marker);

                    if (lat && lon) {
                        deferredObjs.push([{ lat: lat, lon: lon }]);
                    } else {
                        deferredObjs.push(_this.getRequestCityCoords(cityId));
                    }
                }
            });

            $.when.apply($, deferredObjs)
                    .done(function () {
                        var i, markerCoords;
                        for (i = 0; i < _this.markers.length; i++) {
                            markerCoords = arguments[i+1][0] === 'error'
                                ? arguments[0][0] : arguments[i+1][0];
                            _this.markers[i].setLatLng(
                                    new L.LatLng(markerCoords.lat, markerCoords.lon));
                        }
                        _this.setCenter();
                    })
                    .fail(function (data) {
                        console.log('fail', data);
                    });
        },

        refreshRoutes: function (routePoints) {
            var latLngs = [], i, l;
            if (this.routePolyline) {
                this.map.removeLayer(this.routePolyline);
            }

            for (i = 0, l = routePoints.length; i < l; i++) {
                latLngs.push(new L.LatLng(routePoints[i][0], routePoints[i][1]));
            }

            this.routePolyline =
                    L.polyline(latLngs, { color: "blue" }).addTo(this.map);
        },

        setCenter: function () {
            var _this = this,
                points = [],
                bounds,
                i;

            for (i = 0; i < _this.markers.length; i++) {
                points.push(_this.markers[i].getLatLng());
            }

            if (!points) {
                return;
            }

            bounds = new L.latLngBounds(points);
            _this.map.fitBounds(bounds, { maxZoom: 13 });

            // Bugfix
            // After re-opening the modal window doesn't work 
            // centering map (invalidate map tiles and objects)
            setTimeout(function () {
                _this.map.panBy(new L.Point(1, 1));
            }, 0);
        }
    };

    return {
        initialize: function () {
            app.initialize();
        },
        finalize: function () {
            app.finalize();
        }
    };
})(jQuery);