function Timer()
{
    var that = this;

    this.clock = function(offset)
    {
        if(offset == undefined)
            offset = 0;

        var clock = {};

        clock.time = new Date();
        clock.hours = (clock.time.getUTCHours() + offset) % 24;
        clock.minutes = clock.time.getMinutes();

        if(clock.minutes < 10)
            clock.minutes = '0' + clock.minutes;

        if(clock.hours < 10)
            clock.hours = '0' + clock.hours;

        return clock;
    }

    this.colon = function(selector)
    {
        if(selector.css('opacity') == 0)
            selector.css('opacity', 1);
        else
            selector.css('opacity', 0);
    }

    this.countup = function(minutes_selector, seconds_selector)
    {
        var minutes = minutes_selector.text();
        var seconds = Number(seconds_selector.text());

        minutes = Number(minutes.replace(/-*/i, ''));

        seconds++;

        if(seconds > 59)
        {
            seconds = '00';
            minutes++;

            if(minutes < 10)
                minutes = '0' + minutes;

            minutes_selector.text('-' + minutes);
        }
        else if(seconds < 10)
            seconds = '0' + seconds;

        seconds_selector.text(seconds);
    }

    this.countdown = function(minutes_selector, seconds_selector)
    {
        var minutes = Number(minutes_selector.text());
        var seconds = Number(seconds_selector.text());

        if(minutes == 0 && seconds == 0)
        {
            minutes_selector.text('-00').parents('.timer').data('type', 'up').removeClass('ot_green').addClass('ot_red');
            seconds_selector.text('01');
            return false;
        }

        seconds--;

        if(seconds < 0)
        {
            seconds = 59;
            minutes--;

            if(minutes < 10)
                minutes = '0' + minutes;

            minutes_selector.text(minutes);
        }
        else if(seconds < 10)
            seconds = '0' + seconds;

        seconds_selector.text(seconds);
    }

    this.run = function(minutes_selector, seconds_selector, type)
    {
        if(type == 'down')
            that.countdown(minutes_selector, seconds_selector);
        else
            that.countup(minutes_selector, seconds_selector);
    }
}