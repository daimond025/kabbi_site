<?php

namespace backend\modules\setting\services;

use backend\modules\setting\forms\DefaultWorkerNotificationCreateForm;
use backend\modules\setting\forms\DefaultWorkerNotificationForm;
use backend\modules\setting\repositories\TenantRepository;
use backend\modules\setting\repositories\WorkerPushNotificationRepository;

class WorkerPushNotificationService
{
    private $workerPushNotificationRepository;
    private $tenantRepository;

    public function __construct(
        WorkerPushNotificationRepository $workerPushNotificationRepository,
        TenantRepository $tenantRepository
    ) {
        $this->workerPushNotificationRepository = $workerPushNotificationRepository;
        $this->tenantRepository                 = $tenantRepository;
    }


    public function findDefaultNotification($pushId)
    {
        return $this->workerPushNotificationRepository->findDefaultNotification($pushId);
    }

    public function findAllDefaultNotification()
    {
        return $this->workerPushNotificationRepository->findAllDefaultNotification();
    }

    /**
     * @param DefaultWorkerNotificationCreateForm $model
     *
     * @return int
     * @throws \Exception
     */
    public function createDefaultNotification(DefaultWorkerNotificationCreateForm $model)
    {
        $id = $this->workerPushNotificationRepository->createDefaultNotification($model->type, $model->positionId,
            $model->text, $model->params);

        $this->createTenantNotifications($model);

        return $id;
    }

    /**
     * @param integer $id
     * @param DefaultWorkerNotificationForm $model
     *
     * @throws \Exception
     */
    public function updateDefaultNotification($id, DefaultWorkerNotificationForm $model)
    {
        $this->workerPushNotificationRepository->updateDefaultNotification($id, $model->text, $model->params);
    }

    /**
     * @param integer $id
     *
     * @throws \Exception
     */
    public function deleteDefaultNotification($id)
    {
        $model = $this->findDefaultNotification($id);

        $this->workerPushNotificationRepository->deleteDefaultNotification($id);
        $this->workerPushNotificationRepository->deleteTenantNotifications($model->type, $model->position_id);
    }

    private function createTenantNotifications(DefaultWorkerNotificationCreateForm $model)
    {
        $tenantHasCityList = $this->tenantRepository->getTenantHasCityList();

        $this->workerPushNotificationRepository->createNotifications($model->type, $model->positionId, $model->text, $tenantHasCityList);
    }
}