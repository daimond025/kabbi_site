<?php

namespace backend\modules\setting\services;

use backend\modules\setting\forms\DefaultRecipientSmsTemplateCreateForm;
use backend\modules\setting\forms\DefaultRecipientSmsTemplateForm;
use backend\modules\setting\repositories\RecipientSmsTemplateRepository;
use backend\modules\setting\repositories\TenantRepository;

class RecipientSmsTemplateService
{
    private $recipientSmsTemplateRepository;
    private $tenantRepository;

    public function __construct(
        RecipientSmsTemplateRepository $recipientSmsTemplateRepository,
        TenantRepository $tenantRepository
    ) {
        $this->recipientSmsTemplateRepository = $recipientSmsTemplateRepository;
        $this->tenantRepository               = $tenantRepository;
    }


    public function findDefaultNotification($pushId)
    {
        return $this->recipientSmsTemplateRepository->findDefaultNotification($pushId);
    }

    public function findAllDefaultNotification()
    {
        return $this->recipientSmsTemplateRepository->findAllDefaultNotification();
    }

    /**
     * @param DefaultRecipientSmsTemplateCreateForm $model
     *
     * @return int
     * @throws \Exception
     */
    public function createDefaultNotification(DefaultRecipientSmsTemplateCreateForm $model)
    {
        $id = $this->recipientSmsTemplateRepository->createDefaultNotification(
            $model->type,
            $model->positionId,
            $model->text,
            $model->params
        );

        $this->createTenantNotifications($model);

        return $id;
    }

    /**
     * @param integer                         $id
     * @param DefaultRecipientSmsTemplateForm $model
     *
     * @throws \Exception
     */
    public function updateDefaultNotification($id, DefaultRecipientSmsTemplateForm $model)
    {
        $this->recipientSmsTemplateRepository->updateDefaultNotification($id, $model->text, $model->params);
    }

    /**
     * @param integer $id
     *
     * @throws \Exception
     */
    public function deleteDefaultNotification($id)
    {
        $model = $this->findDefaultNotification($id);

        $this->recipientSmsTemplateRepository->deleteDefaultNotification($id);
        $this->recipientSmsTemplateRepository->deleteTenantNotifications($model->type, $model->position_id);
    }

    /**
     * @param DefaultRecipientSmsTemplateCreateForm $model
     *
     * @throws \yii\db\Exception
     */
    private function createTenantNotifications(DefaultRecipientSmsTemplateCreateForm $model)
    {
        $tenantHasCityList = $this->tenantRepository->getTenantHasCityList();

        $this->recipientSmsTemplateRepository->createNotifications(
            $model->type,
            $model->positionId,
            $model->text,
            $tenantHasCityList
        );
    }
}
