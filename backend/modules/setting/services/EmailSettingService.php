<?php

namespace backend\modules\setting\services;

use backend\modules\setting\repositories\EmailSettingRepository;
use backend\modules\setting\services\dto\EmailProviderData;
use backend\modules\setting\services\dto\EmailSenderData;

class EmailSettingService
{
    private $emailSettingRepository;

    public function __construct(EmailSettingRepository $emailSettingRepository)
    {
        $this->emailSettingRepository = $emailSettingRepository;
    }

    public function findFromSystem()
    {
        return $this->emailSettingRepository->findFromSystem();
    }

    public function findFromTenant()
    {
        return $this->emailSettingRepository->findFromTenant();
    }

    public function saveFromSystem(EmailProviderData $providerData, EmailSenderData $senderData, $template)
    {
        $this->emailSettingRepository->saveFromSystem($providerData, $senderData, $template);
    }

    public function saveFromTenant(EmailProviderData $providerData, EmailSenderData $senderData, $template)
    {
        $this->emailSettingRepository->saveFromTenant($providerData, $senderData, $template);
    }
}