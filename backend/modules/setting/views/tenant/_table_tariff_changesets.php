<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\setting\assets\SettingAsset;

$bundle = SettingAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/tenant_update.js', ['depends' => 'yii\web\JqueryAsset']);
?>
<label><?=$message?></label>
<?php
\yii\widgets\Pjax::begin();
echo GridView::widget([
    'dataProvider' => $changesets,
    'showOnEmpty' => false,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'tariff_id_old',
            'value' => 'tariffIdOld.name',
        ],
        [
            'attribute' => 'tariff_id_new',
            'value' => 'tariffIdNew.name',
        ],
        'option_id_old',
        'option_id_new',
        'create_date',
    ],
]);
\yii\widgets\Pjax::end();
?>
