<?php

use yii\helpers\Html;
use yii\grid\GridView;
?>

<?php

\yii\widgets\Pjax::begin();
echo GridView::widget([
    'dataProvider' => $payments,
    'showOnEmpty' => false,
    'columns' => [
        'payment_id',
        'tenant_id',
        'amount',
        'payment_item',
        'create_date',
        'payment_activity_begin_date',
        'expiration_date',
        ['class' => 'yii\grid\ActionColumn',
            'template' => '{delete}',
            'buttons' => [
                'delete' => function ($url, $model, $key) {
                    return Html::a('', null, ['class' => 'btn btn-danger glyphicon glyphicon-trash deletePayment', 'id' => 'payment' . $model['payment_id'], 'tenant_id' => $model['tenant_id'], 'style' => 'cursor: pointer', 'value' => $model['payment_id']]);
                }
                    ],
                ],
            ],
        ]);
        \yii\widgets\Pjax::end();
        ?>