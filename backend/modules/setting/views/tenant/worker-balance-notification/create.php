<?php

use backend\modules\setting\models\WorkerBalanceNotification;
use yii\web\View;

/* @var $this View */
/* @var $model WorkerBalanceNotification */

$this->title = 'Интеграция';

$this->params['breadcrumbs'][] = ['label' => 'Арендаторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tenant_id, 'url' => ['tenant/view', 'id' => $model->tenant_id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div>
    <h1>Добавление</h1>
    <?= $this->render('_form', ['model' => $model]); ?>
</div>