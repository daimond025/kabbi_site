<?php

use app\modules\setting\models\Tenant;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model Tenant */
/* @var $workerBalanceNotifications ActiveDataProvider */

if (Yii::$app->user->can('tenantInfo')): ?>
    <p>
        <?= Html::a(t('app', 'Create'), ['create-notification', 'id' => $model->tenant_id],
            ['class' => 'btn btn-success']) ?>
    </p>
<?php endif;

echo GridView::widget([
    'dataProvider' => $workerBalanceNotifications,
    'columns'      => [
        'name',
        'url',
        'token',

        [
            'attribute' => 'writeoff_notify',
            'content'   => function ($model) {
                return (int)$model->writeoff_notify === 1 ? 'Да' : 'Нет';
            },
        ],
        [
            'attribute' => 'deposit_notify',
            'content'   => function ($model) {
                return (int)$model->writeoff_notify === 1 ? 'Да' : 'Нет';
            },
        ],
        [
            'class'          => 'yii\grid\ActionColumn',
            'urlCreator'     => function ($action, $model) {
                return Url::to(["/setting/tenant/{$action}-notification", 'id' => $model->id]);
            },
            'template'       => '{update}{delete}',
            'visibleButtons' => [
                'update' => \Yii::$app->user->can('tenantInfo'),
            ],
        ],
    ],
]);