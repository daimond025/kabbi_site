<?php

use backend\modules\setting\models\WorkerBalanceNotification;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model WorkerBalanceNotification */
?>

<div class="tenant-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 100]) ?>
    <?= $form->field($model, 'url')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'token')->textInput(['maxlength' => 255]) ?>

    <div class="row">
        <div class="col-xs-6 col-sm-3">
            <?= $form->field($model, 'writeoff_notify')->dropDownList(['1' => 'Да', '0' => 'Нет']) ?>
        </div>
        <div class="col-xs-6 col-sm-3">
            <?= $form->field($model, 'deposit_notify')->dropDownList(['1' => 'Да', '0' => 'Нет']) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? t('app', 'Create') : t('app', 'Save'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>