<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\setting\models */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Арендаторы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenant-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php if (Yii::$app->user->can('tenantInfo')): ?>
        <p>
            <?= Html::a('Добавить арендатора', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>
    <?php
    \yii\widgets\Pjax::begin();
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'tenant_id',
            'domain',
            'create_time',
            'company_name',
            'full_company_name',
            'contact_name',
            'contact_second_name',
            'contact_last_name',
            'contact_phone',
            'contact_email:email',
            'utm_source',
            'utm_medium',
            'utm_campaign',
            'utm_content',
            'utm_term',
            'client_id_go',
            'client_id_ya',
            'ip',
            // 'company_phone',
            // 'email:email',
            // 'inn',
            // 'bookkeeper',
            // 'kpp',
            // 'ogrn',
            // 'site',
            // 'archive',
            // 'logo',
            // 'director',
            // 'director_position',
            // 'status',

            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('tenantInfo'),
                    'delete' => \Yii::$app->user->can('tenantInfo'),
                ],
            ],
        ],
    ]);
    \yii\widgets\Pjax::end()
    ?>

</div>
