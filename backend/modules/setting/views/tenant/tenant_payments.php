<?php

use yii\helpers\Html;
use common\helpers\DateTimeHelper;
?>
<div style="cursor: pointer"><a id="elecsnet-counter-roller"></a></div>
<div style="cursor: pointer"><a id="add_payment_roller"></a></div>
<div id="elecsnet-counter" style="display: none">
    <span>
        <input type="hidden" id="tenant_id"value="<?= $tenant_id ?>">
        <div class="input-group date" style="display: inline-block; width: 24%">
            <input type="text" id="beginPeriod" class="form-control" value="<?= date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - 14, date("Y"))); ?>"><span class="input-group-addon">Начало периода</span>
        </div>
        <div class="input-group date" style="display: inline-block; width: 24%">
            <input type="text" id="endPeriod" class="form-control" value="<?= date('Y-m-d') ?>"><span class="input-group-addon">Конец периода</span>
        </div>
        <button type="button" id="count" style="width: 48%" class="btn btn-block btn-success btn-sm">Подсчитать</button>
    </span>
    <div style="cursor: pointer"><a id="additional-fields-roller"></a></div>
    <div id="additional-fields">
        <input type="hidden" style="width: 48%" type="text" id="phone" class="form-control" value="<?= $phone ?>">
    </div>
    <label id="result"></label>
</div>

<div id="add_payment" style="display: none">

    <span>
        <span>
            <label>Назначение платежа</label>
            <p>
                <input type="text" id="payment_titile">
            </p>
        </span>
        <span>
            <label>Сумма</label>
            <p>
                <input type="text" id="amount">
            </p>
        </span>
        <span>
            <label>Начало действия платежа</label>
            <p>
                <input type="text" id="paymentBeginPeriod" class="date">
            </p>
        </span>
        <span>
            <label>Период действия платежа</label>
            <p>
                <?= Html::dropDownList('period', 1, DateTimeHelper::getYearMonths(), ['class' => 'dropdown-toggle', 'id' => 'dropDown']) ?>
            </p>
        </span>
        <p>
            <label id="payment_result"></label>
        </p>
    </span>
    <button id="add_payment_button" class="btn btn-primary">Добавить платеж</button>
<!--    <input type="text" id="amount">-->
</div>

<h1>Все платежи</h1>
<?if(!empty($totalPayments)): ?>
<label id="total_sum">Общая сумма платежей арендатора за все время составляет: <?= $totalPayments ?></label>
<?endif;?>
<div id="payments">
    <?=$this->render('_payments_table', ['payments' => $payments, 'totalCosts' => $totalPayments]);?>
</div>