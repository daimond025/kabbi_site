<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tenant-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'tenant_id') ?>

    <?= $form->field($model, 'company_name') ?>

    <?= $form->field($model, 'full_company_name') ?>

    <?= $form->field($model, 'legal_address') ?>

    <?= $form->field($model, 'post_address') ?>

    <?php // echo $form->field($model, 'contact_name') ?>

    <?php // echo $form->field($model, 'contact_second_name') ?>

    <?php // echo $form->field($model, 'contact_last_name') ?>

    <?php // echo $form->field($model, 'contact_phone') ?>

    <?php // echo $form->field($model, 'contact_email') ?>

    <?php // echo $form->field($model, 'company_phone') ?>

    <?php // echo $form->field($model, 'domain') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'inn') ?>

    <?php // echo $form->field($model, 'create_time') ?>

    <?php // echo $form->field($model, 'bookkeeper') ?>

    <?php // echo $form->field($model, 'kpp') ?>

    <?php // echo $form->field($model, 'ogrn') ?>

    <?php // echo $form->field($model, 'site') ?>

    <?php // echo $form->field($model, 'archive') ?>

    <?php // echo $form->field($model, 'logo') ?>

    <?php // echo $form->field($model, 'director') ?>

    <?php // echo $form->field($model, 'director_position') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
