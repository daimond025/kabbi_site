<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use kartik\popover\PopoverX;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \app\modules\setting\models\TenantSettingSearch */

$this->title                   = 'Настройки арендатора';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenant-setting-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <? if (($dataProvider->getModels() == [])): ?>
        <?= 'У данного арендатора отсутствуют какие - либо настройки' ?>
    <? else: ?>

        <?php
        \yii\widgets\Pjax::begin();
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'showOnEmpty'  => false,
            'columns'      => [
                'setting_id',
                'name',
                'value',
                [
                    'attribute' => 'edit',
                    'format'    => 'raw',
                    'filter'    => false,
                    'value'     => function ($model) {


                        return PopoverX::widget([
                            'header'       => 'Изменить значение',
                            'type'         => PopoverX::TYPE_WARNING,
                            'placement'    => PopoverX::ALIGN_BOTTOM,
                            'size'         => PopoverX::SIZE_LARGE,
                            'content'      => $this->render('_settings', ['model' => $model]),
                            'toggleButton' => ['label' => '', 'class' => "btn btn-info glyphicon glyphicon-pencil"],
                        ]);

                    },
                ],
            ],
        ]);
        \yii\widgets\Pjax::end();
        ?>
    <? endif; ?>

</div>
