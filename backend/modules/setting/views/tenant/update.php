<?php

use yii\helpers\Html;
use app\modules\setting\assets\SettingAsset;
/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\Tenant */



$this->title = 'Редактировать профиль: ' . ' #' . $model->tenant_id;
$this->params['breadcrumbs'][] = ['label' => 'Арендаторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tenant_id, 'url' => ['view', 'id' => $model->tenant_id]];
$this->params['breadcrumbs'][] = 'Редактировать профиль';
$bundle = SettingAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/main.js', ['depends' => 'yii\web\JqueryAsset']);
?>

<div>
    <p>
        <?= Html::a('Просмотреть профиль', ['view', 'id' => $model->tenant_id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Удалить арендатора', ['delete', 'id' => $model->tenant_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Подтверждаете удаление?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>


    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">Информация</a></li>
            <li><a href="#tab_2" data-toggle="tab">Настройки</a></li>
            <li><a href="#tab_3" data-toggle="tab">Тариф, доп. опции и история изменений</a></li>
            <li><a href="#tab_4" data-toggle="tab">Платежи арендатора </a></li>
            <li><a href="#tab_6" data-toggle="tab">Статистика арендатора</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">
                <h1><?= Html::encode($this->title) ?></h1>
                <div class="tenant-update">
                    <?if(!empty($model->logo) && @getimagesize($model->logoLink)):?>
                        <img src="<?=$model['logoLink']?>">
                    <?endif?>
                    <?php
                        echo $this->render('_form', [
                            'model' => $model,
                        ]);
                    ?>

                </div>

            </div><!-- /.tab-pane -->

            <div class="tab-pane" id="tab_2">
                <?=
                $this->render('settings', [
                    'dataProvider' => $SettingsDataProvider,
                    'searchModel'  => $settingSearchModel
                ])
                ?>

            </div>

            <div class="tab-pane" id="tab_3">
                <?= $this->render('tariff', ['tariff' => $tariff, 'tenant_id' => $model->tenant_id, 
                    'tenant_has_admin_tariff' => $tenant_has_admin_tariff, 
                    'changesets' => $changesets,
                    'hasAdditionalOptions' => $hasAdditionalOptions,
                    'tenantAdditionalOptions' => $additionalOptions,
                    ]);
                    
                ?>
            </div>

            <div class="tab-pane" id="tab_4">
                <?=
                $this->render('tenant_payments', [
                    'totalPayments' => $tenantPaymentsTotal,
                    'payments' => $tenantPaymentsDataProvider,
                ])
                ?>
            </div>
            
            <div class="tab-pane" id="tab_5">
                <?= 
                $this->render('driver_payments', [
                    'payments' => $payments,
                    'tenant_id' => $model->tenant_id,
                ])
                ?>
            </div>
            
            <div class="tab-pane" id="tab_6">
                <?= 'В процессе разработки'
                ?>
            </div>

        </div>
    </div>
</div>



