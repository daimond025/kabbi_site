
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\setting\assets\SettingAsset;
$bundle_order = SettingAsset::register($this);
$this->registerJsFile($bundle_order->baseUrl . '/gridUpdate.js', ['depends' => 'yii\web\JqueryAsset']);
/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\Tenant */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tenant-form">

    <?php $form = ActiveForm::begin(); ?>
    <input id="<?='name' . $model->setting_id?>" class="setting-name" hidden value="<?=$model->name?>">
    <input id="<?='settingid' . $model->setting_id?>" class="setting-name" hidden value="<?=$model->setting_id?>">
    <?= $form->field($model, 'value')->textarea([ 'id' => 'value' . $model->setting_id]) ?>
    <label id="<?='status-block' . $model->setting_id?>"></label>
    <div class="form-group">
         <?= Html::button('Сохранить', ['id' => $model->setting_id, 'class' => 'btn btn-primary btnupdate']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

