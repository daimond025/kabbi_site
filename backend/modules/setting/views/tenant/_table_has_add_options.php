<?php

use yii\helpers\Html;
use yii\grid\GridView;
?>

<input type="hidden" id="tenant_id" value="">
<?php

\yii\widgets\Pjax::begin();
echo GridView::widget([
    'dataProvider' => $hasAdditionalOptions,
    'showOnEmpty' => false,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'tenant_id',
        'option_id',
        'create_date',
        ['class' => 'yii\grid\ActionColumn',
            'template' => '{delete}',
            'buttons' => [
                'delete' => function ($url, $model, $key) {
                    return Html::button('', ['class' => 'btn btn-danger glyphicon glyphicon-trash deleteOption', 'id' => $model['id'], 'tenant_id' => $model['tenant_id'], 'style' => 'cursor: pointer', 'value' => $model['id']]);
                }
                    ],
                ],
            ],
        ]);
        \yii\widgets\Pjax::end();
        ?>