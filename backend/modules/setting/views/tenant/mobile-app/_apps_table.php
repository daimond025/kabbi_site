<?php

use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $tenantAppDataProvider array */
/* @var $model \app\modules\setting\models\Tenant */

if (Yii::$app->user->can('tenantInfo')): ?>
    <p>
        <?=
        Html::a('Добавить', ['create-mobile-app', 'id' => $model->tenant_id], [
            'class' => 'btn btn-success',
        ])
        ?>
    </p>
<?php endif;

echo \yii\grid\GridView::widget([
    'dataProvider' => $tenantAppDataProvider,
    'showOnEmpty' => false,
    'columns' => [
        'app_id',
        'name',
        [
            'attribute' => 'demo',
            'content' => function ($model) {
                return $model->isDemo ? 'Да' : 'Нет';
            },
        ],
        [
            'attribute' => 'active',
            'content' => function ($model) {
                return $model->isActive ? 'Да' : 'Нет';
            },
        ],
        'sort',
        [
            'attribute' => 'cities',
            'content' => function ($model) {
                return $model->getCityList();
            },
        ],
        'api_key',
        [
            'class'          => 'yii\grid\ActionColumn',
            'urlCreator' => function ($action, $model) {
                return \yii\helpers\Url::to(["/setting/tenant/{$action}-mobile-app", 'id' => $model->app_id]);
            },
            'template' => '{update} {view}',
            'visibleButtons' => [
                'update' => \Yii::$app->user->can('tenantInfo'),
            ],
        ],
    ],
]);