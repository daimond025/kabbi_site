<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \app\modules\setting\models\MobileApp */

$this->title                   = 'Мобильные приложения';
$this->params['breadcrumbs'][] = ['label' => 'Арендаторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tenant_id, 'url' => ['tenant/view', 'id' => $model->tenant_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div>
    <?php if (Yii::$app->user->can('tenantInfo')): ?>
        <p>
            <?= Html::a('Редактировать', ['update-mobile-app', 'id' => $model->app_id],
                ['class' => 'btn btn-primary']) ?>
        </p>
    <?php endif; ?>

    <?=
    \yii\widgets\DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'app_id',
            'name',
            'sort',
            [
                'attribute' => 'demo',
                'value'     => $model->isDemo ? 'Да' : 'Нет',
            ],
            [
                'attribute' => 'active',
                'value'     => $model->isActive ? 'Да' : 'Нет',
            ],
            [
                'attribute' => 'city_autocompletion',
                'value'     => $model->isCityAutocompletion ? 'Да' : 'Нет',
            ],
            'api_key',
            'push_key_android',
            'push_key_ios',
            'link_android',
            'link_ios',
            [
                'attribute' => 'cities',
                'value'     => $model->getCityList(),
            ],
            [
                'attribute' => 'page_info',
                'value'     => $model->page_info,
                'label'     => Html::a($model->getAttributeLabel('page_info'),
                    ['view-page-info', 'id' => $model->app_id], ['target' => '_blank', 'rel' => 'nofollow noopener']),
            ],
            [
                'attribute' => 'confidential',
                'value'     => $model->confidential,
                'label'     => Html::a($model->getAttributeLabel('confidential'),
                    ['view-confidential', 'id' => $model->app_id], ['target' => '_blank']),
            ],
        ],
    ])
    ?>
</div>
