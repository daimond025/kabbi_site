<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \app\modules\setting\models\MobileApp */
/* @var $form yii\widgets\ActiveForm */
/* @var $cityList array */
?>

<div class="tenant-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 100]) ?>
    <?= $form->field($model, 'sort')->textInput(['type' => 'number']) ?>
    <?= $form->field($model, 'active')->dropDownList([ '1' => 'Да', '0' => 'Нет']) ?>
    <?= $form->field($model, 'city_autocompletion')->dropDownList([ '1' => 'Да', '0' => 'Нет']) ?>
    <?= $form->field($model, 'api_key')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'push_key_android')->textarea() ?>
    <?= $form->field($model, 'push_key_ios')->textarea() ?>
    <?= $form->field($model, 'link_android')->textInput(['maxlength' => 250]) ?>
    <?= $form->field($model, 'link_ios')->textInput(['maxlength' => 250]) ?>
    <?= $form->field($model, 'page_info')->textarea() ?>
    <?= $form->field($model, 'confidential')->textarea() ?>
    <?= $form->field($model, 'cities')->checkboxList($cityList,[
        'separator' => '<br/>',
        'encode' => false,
    ]) ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить новое' : 'Применить изменения', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>