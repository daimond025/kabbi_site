<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\Tenant */
/* @var $tenantAppDataProvider array */
/* @var $workerBalanceNotifications ActiveDataProvider */

$this->title = $model->tenant_id;
$this->params['breadcrumbs'][] = ['label' => 'Арендаторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div>
    <?php if (Yii::$app->user->can('tenantInfo')): ?>
        <p>
            <?= Html::a('Редактировать профиль', ['update', 'id' => $model->tenant_id], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a('Удалить арендатора', ['delete', 'id' => $model->tenant_id], [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => 'Подвтерждаете удаление?',
                    'method'  => 'post',
                ],
            ])
            ?>
        </p>
    <?php endif; ?>
    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">Информация</a></li>
            <li><a href="#tab_2" data-toggle="tab">Настройки</a></li>
            <li><a href="#tab_3" data-toggle="tab">Тариф, доп. опции и история изменений</a></li>
            <li><a href="#tab_4" data-toggle="tab">Платежи</a></li>
            <li><a href="#tab_5" data-toggle="tab">Статистика</a></li>
            <li><a href="#tab_6" data-toggle="tab">Мобильные приложения</a></li>
            <li><a href="#tab_7" data-toggle="tab">Сотрудники</a></li>
            <li><a href="#tab_8" data-toggle="tab">Интеграция</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">
                <h3>Города арендатора:</h3>
                <p>
                    <?if(!empty($city_list)):?>
                    <?$cities = '';?>
                        <?foreach($city_list as $rows):?>
                            <? $cities .= $rows->city->name . ', ' ?>
                        <?endforeach;?>
                        <?=substr($cities, 0, -2);?>
                    <?else:?>
                        <?='У текущего арендатора нет выбранных городов'?>
                    <?endif?>
                </p>
                <p>
                    <?='Количество сотрудников: ' . $usersQuantity?>
                </p>
                <p>
                    <?='Количество водителей: ' . $driversQuantity?>
                </p>
                <p>
                    <?='Количество авто: ' . $carsQuantity?>
                </p>

                <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'full_company_name',
                            'company_name',
                            'legal_address',
                            'post_address',
                            'director',
                            'director_position',
                            'bookkeeper',
                            'inn',
                            'kpp',
                            'ogrn',
                            'company_phone',
                            'email:email',
                            'site',
                            'contact_last_name',
                            'contact_name',
                            'contact_second_name',
                            'contact_phone',
                            'contact_email:email',
                        ],
                    ]);
                ?>
            </div>
            <div class="tab-pane" id="tab_2">
                <?if(($SettingsDataProvider->getModels()==[])):?>
                <?= 'У данного арендатора отсутствуют какие - либо настройки' ?>
                <?else:?>
                <?=
                GridView::widget([
                    'dataProvider' => $SettingsDataProvider,
                    'showOnEmpty' => false,
                    'columns' => [
                        'setting_id',
                        'tenant_id',
                        'name',
                        'value',
                        'city_id',
                    ],
                ]);
                ?>
                <?endif;?>
            </div>
            <div class="tab-pane" id="tab_3">
                <?php
                if (!is_null($tenant_has_admin_tariff) && $tariff[$tenant_has_admin_tariff - 1]->block == 0) {
                    echo 'Текущий тариф: ' . $tariff[$tenant_has_admin_tariff - 1]->name;
                } elseif (is_null($tenant_has_admin_tariff)) {
                    echo '<p>У данного арендатора не установлен тариф, перейдите на страницу редактирования, чтобы установить необходимый тариф</p>';
                } elseif ($tariff[$tenant_has_admin_tariff - 1]->block == 1) {
                    echo 'Арендатор использует заблокированный тариф: ' . $tariff[$tenant_has_admin_tariff - 1]->name;
                }
                ?>
                <p>
                    <?=
                    $this->render('_table_tariff_changesets', ['changesets' => $changesetsDataProvider]);
                    ?>
                </p>
            </div>
            <div class="tab-pane" id="tab_4">
                <?=
                $this->render('_payments_table', [
                    'totalPayments' => $tenantPaymentsTotal,
                    'payments' => $tenantPaymentsDataProvider,
                ])
                ?>
            </div>
            <div class="tab-pane" id="tab_5">
                <?= 'В процессе разработки'
                ?>
            </div>
            <div class="tab-pane" id="tab_6">
                <?=
                $this->render('mobile-app/_apps_table', [
                    'model' => $model,
                    'tenantAppDataProvider' => $tenantAppDataProvider,
                ]);
                ?>
            </div>
            <div class="tab-pane" id="tab_7">
                <?=
                $this->render('_users_data', [
                    'model' => $model,
                    'UsersDataProvider' => $UsersDataProvider,
                ]);
                ?>
            </div>
            <div class="tab-pane" id="tab_8">
                <?= $this->render('worker-balance-notification/index', [
                    'model'                      => $model,
                    'workerBalanceNotifications' => $workerBalanceNotifications,
                ]); ?>
            </div>
        </div>
    </div>
</div>
