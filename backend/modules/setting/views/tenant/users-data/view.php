<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \app\modules\setting\models\User */

$this->title                   = 'Сотрудники';
$this->params['breadcrumbs'][] = ['label' => 'Арендаторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tenant_id, 'url' => ['tenant/view', 'id' => $model->tenant_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div>
    <?php if (Yii::$app->user->can('tenantInfo')): ?>
        <p>
            <?= Html::a('Редактировать', ['update-user', 'id' => $model->user_id],
                ['class' => 'btn btn-primary']) ?>
        </p>
    <?php endif; ?>

    <?=
    \yii\widgets\DetailView::widget([
        'model'      => $model,
        'attributes' => [
            [
                'attribute' => 'name',
                'label'     => 'ФИО',
                'value'     => function ($user) {
                    return $user->last_name . ' ' . $user->name . ' ' . $user->second_name;
                },
            ],
            'email',
            [
                'attribute' => 'position_id',
                'label'     => 'Должность',
                'value'     => function ($model) {
                    return Yii::t('company-roles', $model->position->name, [], 'ru');
                }
            ],
            [
                'attribute' => 'cities',
                'label'     => 'Филиалы',
                'value'     => $model->getUserCities()
            ],
            [
                'attribute' => 'demo',
                'value'     => isset($model->isDemo) ? 'Да' : 'Нет',
            ],
            [
                'attribute' => 'active',
                'value'     => function ($user) {
                    return $user->active == 1 ? 'активен' : 'не активен';
                },
            ],
            [
                'attribute' => 'access_token',
                'value'     => $model->access_token
            ],

        ]
    ])

    ?>
</div>

