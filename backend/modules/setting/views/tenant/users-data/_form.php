<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\modules\setting\models\User;

/* @var $this yii\web\View */
/* @var $model \app\modules\setting\models\User */
/* @var $form yii\widgets\ActiveForm */
/* @var $cityList array */
?>

<div class="tenant-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fullName')->label('ФИО')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'position_id')->label('Должность')->dropDownList($model->getUserPositions()) ?>

    <?= $form->field($model, 'active')->dropDownList([0 => 'не активен', 1 => 'активен']) ?>

    <?= $form->field($model, 'access_token')->label('access_token')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'cities')->checkboxList($cityList, [
        'separator' => '<br/>',
        'encode'    => false,
    ]) ?>




    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить новое' : 'Применить изменения', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>