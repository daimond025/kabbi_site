<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \app\modules\setting\models\User */
/* @var $cityList array */

$this->title                   = 'Сотрудники';
$this->params['breadcrumbs'][] = ['label' => 'Арендаторы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tenant_id, 'url' => ['tenant/view', 'id' => $model->tenant_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div>
    <?= $this->render('_form', ['model' => $model, 'cityList'=>$cityList]); ?>
</div>