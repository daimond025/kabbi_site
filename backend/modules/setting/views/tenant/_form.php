<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\Tenant */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tenant-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'company_name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'full_company_name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'legal_address')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'post_address')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'contact_name')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'contact_second_name')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'contact_last_name')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'contact_phone')->textInput(['maxlength' => 15]) ?>

    <?= $form->field($model, 'contact_email')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'company_phone')->textInput(['maxlength' => 15]) ?>

    <?= $form->field($model, 'domain')->textInput(['maxlength' => 15]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'inn')->textInput(['maxlength' => 15]) ?>

    <?= $form->field($model, 'create_time')->textInput() ?>

    <?= $form->field($model, 'bookkeeper')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'kpp')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'ogrn')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'site')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'archive')->textInput() ?>

    <?= $form->field($model, 'logo')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'director')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'director_position')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'status')->dropDownList([ 'ACTIVE' => 'ACTIVE', 'BLOCKED' => 'BLOCKED', 'REMOVED' => 'REMOVED',], ['prompt' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить нового' : 'Применить изменения', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>



</div>
