<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\setting\assets\SettingAsset;

$bundle = SettingAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/tenant_update.js', ['depends' => 'yii\web\JqueryAsset']);
?>
<section>
    <input type="hidden" id="tenant_id" value="<?= $tenant_id ?>">
    <label>Текущий тариф арендатора</label><br>
    <?= Html::dropDownList('tariff_id', !empty($tenant_has_admin_tariff) ? $tenant_has_admin_tariff : 1, ArrayHelper::map($tariff, 'tariff_id', 'name'), ['class' => 'form-control dropdown-toggle', 'id' => 'tariff_list'])
    ?>
    <div class="result-block"></div>
    <br>

    <?= Html::button('Сохранить', ['id' => 'save_tariff', 'class' => 'btn btn-success']) ?>
</section>


<h1>История изменений</h1>
<div id="tariff_changesets">
    <?=
    $this->render('_table_tariff_changesets', ['changesets' => $changesets])
    ?>
</div>
<h1>Дополнительные опции</h1>

<div id="additional_options">
    <?= $this->render('_table_has_add_options', ['hasAdditionalOptions' => $hasAdditionalOptions]) ?>
</div>
<p>
    <?= Html::dropDownList('additionalOptionsDropDown', null, $tenantAdditionalOptions, ['id' => 'addOptionsDropDown']) ?>
</p>
<p>
    <?=
    Html::button('Добавить опцию', ['id' => 'addOption', 'class' => 'btn btn-success'])
    ?>
</p>