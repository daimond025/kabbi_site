<?php

use app\modules\setting\assets\SettingAsset;

$bundle = SettingAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/js/bootstrap-datepicker.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile($bundle->baseUrl . '/locales/bootstrap-datepicker.ru.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile($bundle->baseUrl . '/elecsnet-page-widgets.js', ['depends' => 'yii\web\JqueryAsset']);
?>
<?if($payments == []):?>
<?= 'Арендатор еще не производил никаких платежей' ?>
<?else:?>
<div style="cursor: pointer"><a class="elecsnet-counter-roller"></a></div>
<div class="elecsnet-counter" style="display: none">
    <span>
        <div class="input-group date" style="display: inline-block; width: 24%">
            <input type="text" id="beginPeriod" class="form-control" value="<?= date("d.m.Y", mktime(0, 0, 0, date("m"), date("d") - 14, date("Y"))); ?>"><span class="input-group-addon">Начало периода</span>
        </div>
        <div class="input-group date" style="display: inline-block; width: 24%">
            <input type="text" id="endPeriod" class="form-control" value="<?= date('d.m.Y') ?>"><span class="input-group-addon">Конец периода</span>
        </div>
        <button type="button" id="count" style="width: 48%" class="btn btn-block btn-success btn-sm">Подсчитать</button>
    </span>
    <div style="cursor: pointer"><a id="additional-fields-roller"></a></div>
    <div id="additional-fields">
        <input type="hidden" style="width: 48%" type="text" id="phone" class="form-control" value="<?=$phone?>">
    </div>
    <label id="sum"></label>
</div>


<h1>Все платежи</h1>
<div id="table">
    <table class="table table-striped table-bordered"><thead>
            <tr><th>#</th>
                <th><a data-sort="type">Тип</a></th>
                <th><a>Reqid</a></th><th><a data-sort="amount">Сумма</a></th>
                <th><a data-sort="auth_code">Auth Code</a></th>
                <th><a>Валюта</a></th><th><a data-sort="result">Результат</a></th>
                <th><a data-sort="create_time">Дата/время</a></th></tr>
            <tr class="filters">
<!--                <td><input type="text" class="form-control" name="ElecsnetLogSearch[type]"></td>
                <td><input type="text" class="form-control" name="ElecsnetLogSearch[reqid]"></td>
                <td><input type="text" class="form-control" name="ElecsnetLogSearch[amount]"></td>
                <td><input type="text" class="form-control" name="ElecsnetLogSearch[auth_code]"></td>
                <td><input type="text" class="form-control" name="ElecsnetLogSearch[currency]"></td>
                <td><input type="text" class="form-control" name="ElecsnetLogSearch[result]"></td>
                <td><input type="text" class="form-control" name="ElecsnetLogSearch[create_time]"></td></tr>-->
        </thead>
        <tbody>
            <?$counter = 1?>
            <?foreach($payments as $payment):?>
            <tr data-key="<?= $counter ?>">
                <td>#<?= $counter++ ?></td>
                <td><?= $payment['type'] ?></td>
                <td><?= $payment['reqid'] ?></td>
                <td><?= $payment['amount'] / 100 ?></td>
                <td><?= $payment['auth_code'] ?></td>
                <td><?= $payment['currency'] ?></td>
                <td><?= $payment['result'] ?></td>
                <td><?= date('d-m-Y H:i:s', $payment['create_time']) ?></td>
            </tr>
            <?endforeach;?>
        </tbody></table>
</div>
<?endif;?>
