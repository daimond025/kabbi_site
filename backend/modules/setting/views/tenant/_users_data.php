<?php
use yii\grid\GridView;

echo GridView::widget([
    'dataProvider' => $UsersDataProvider,
    'columns'      => [
        [
            'attribute' => 'name',
            'label'     => 'ФИО',
            'value'     => function ($user) {
                return $user->last_name . ' ' . $user->name . ' ' . $user->second_name;
            },
        ],
        'email',
        [
            'attribute' => 'position_id',
            'label'     => 'Должность',
            'value'     => function ($model) {
                return Yii::t('company-roles', $model->position->name, [], 'ru');
            }

        ],
        [
            'attribute' => 'cities',
            'label'     => 'Филиалы',
            'value'     => function ($model) {
                return $model->cities;
            },
        ],
        [
            'attribute' => 'active',
            'value'     => function ($user) {
                return $user->active == 1 ? 'активен' : 'не активен';
            },
        ],
        [
            'class'      => 'yii\grid\ActionColumn',
            'urlCreator' => function ($action, $model) {
                return \yii\helpers\Url::to(["/setting/tenant/{$action}-user", 'id' => $model->user_id]);
            },
            'template'   => '{update},{view}',
        ],
    ],
]);

?>
