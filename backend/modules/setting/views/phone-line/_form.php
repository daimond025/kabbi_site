<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View
 * @var $model \app\modules\setting\models\PhoneLine
 * @var $cityList array
 * @var $tariffList array
 * @var $form yii\widgets\ActiveForm
 */

?>

<div class="phone-line-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'phone')->textInput(['class' => 'form-control js-phone']); ?>

    <?= $form->field($model, 'domain')->textInput(['class' => 'form-control js-domain', 'disabled' => !$model->isNewRecord]); ?>


    <?= Html::label($model->getAttributeLabel('tenant_id')); ?>
    <?= Html::textInput('tenant-id', $model->tenant_id,
        ['class' => 'form-control js-tenant-id', 'disabled' => true]); ?>
    <?= $form->field($model, 'tenant_id')->hiddenInput(['class' => 'form-control js-tenant-id'])->label(false); ?>

    <?= $form->field($model, 'city_id')->dropDownList($cityList, ['class' => 'form-control js-city-id', 'disabled' => !$model->isNewRecord]); ?>

    <?= $form->field($model, 'tariff_id')->dropDownList($tariffList, ['class' => 'form-control js-tariff-id']); ?>

    <?= $form->field($model, 'incoming_scenario')->textarea(['rows' => 10, 'class' => 'form-control js-phone-line'])
        ->label($model->getAttributeLabel('incoming_scenario') . ' ' . Html::a('загрузить', null,
                ['class' => 'js-phone-line-button'])); ?>

    <?= $form->field($model, 'block')->dropDownList($model::getBlockTypeMap(),
        ['class' => 'form-control js-block']); ?>

    <?= $form->field($model, 'draft')->dropDownList($model::getDraftTypeMap(),
        ['class' => 'form-control js-draft', 'disabled' => !$model->isDraft && !$model->isNewRecord]); ?>

    <?= $form->field($model, 'r_username')->textInput(['class' => 'form-control']); ?>
    <?= $form->field($model, 'password')->textInput(['class' => 'form-control']); ?>
    <?= $form->field($model, 'r_domain')->textInput(['class' => 'form-control']); ?>
    <?= $form->field($model, 'sip_server_local')->textInput(['class' => 'form-control']); ?>
    <?= $form->field($model, 'auth_proxy')->textInput(['class' => 'form-control']); ?>
    <?= $form->field($model, 'port')->textInput(['class' => 'form-control']); ?>
    <?= $form->field($model, 'realm')->textInput(['class' => 'form-control']); ?>
    <?= $form->field($model, 'typeServer')->dropDownList($model::getTypeServerList(), ['class' => 'form-control']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
