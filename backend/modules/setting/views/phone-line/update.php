<?php

use yii\helpers\Html;

/* @var $this yii\web\View
 * @var $model \app\modules\setting\models\PhoneLine
 * @var $cityList array
 * @var $tariffList array
 * @var $form yii\widgets\ActiveForm
 */

\backend\modules\setting\assets\PhoneLineAsset::register($this);

$this->title = 'Редактирование телефонной линии ' . $model->phone;
$this->params['breadcrumbs'][] = ['label' => 'Телефонные линии', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->line_id, 'url' => ['view', 'id' => $model->line_id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="phone-line-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'      => $model,
        'cityList'   => $cityList,
        'tariffList' => $tariffList,
    ]) ?>

</div>
