<?php

use yii\helpers\Html;


/* @var $this yii\web\View
 * @var $model \app\modules\setting\models\PhoneLine
 * @var $cityList array
 * @var $tariffList array
 * @var $form yii\widgets\ActiveForm
 */

\backend\modules\setting\assets\PhoneLineAsset::register($this);

$this->title                   = 'Создание телефонной линии';
$this->params['breadcrumbs'][] = ['label' => 'Телефонные линии', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="phone-line-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'      => $model,
        'cityList'   => $cityList,
        'tariffList' => $tariffList,
    ]) ?>

</div>
