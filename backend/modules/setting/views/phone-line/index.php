<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\setting\models\PhoneLine;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Телефонные линии';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="phone-line-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('phoneLines')): ?>
        <p>
            <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>
    <?=
    GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
            'line_id',
            'tenant_id',
            [
                'attribute' => 'domain',
                'content'   => function ($model) {
                    return $model->tenant->domain ;
                },
            ],
            [
                'attribute' => 'tariff_id',
                'content'   => function ($model) {
                    return $model->tariff->name . ' (' . $model->tariff_id . ')';
                },
            ],
            [
                'attribute' => 'city_id',
                'content'   => function ($model) {
                    return $model->city->name;
                },
            ],
            'phone',
            [
                'attribute' => 'block',
                'filter' => Html::activeDropDownList($searchModel,'block',PhoneLine::getBlockTypeMap(),['prompt'=>'','class'=>'form-control']),
                'content'   => function ($model) {
                    return $model->block ? 'Да' : 'Нет';
                },
            ],
            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('phoneLines'),
                    'delete' => \Yii::$app->user->can('phoneLines'),
                ],
            ],
        ],
    ]);
    ?>

</div>
