<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\VarDumper;

/* @var $this yii\web\View */
/* @var $model \app\modules\setting\models\PhoneLine */

$this->title                   = $model->line_id;
$this->params['breadcrumbs'][] = ['label' => 'Телефонные линии', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="phone-line-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('phoneLines')): ?>
        <p>
            <?= Html::a('Редактировать', ['update', 'id' => $model->line_id], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a('Удалить', ['delete', 'id' => $model->line_id], [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => 'Вы точно хотите удалить элемент?',
                    'method'  => 'post',
                ],
            ])
            ?>
        </p>
    <?php endif; ?>
    <?=
    DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'phone',
            'line_id',
            [
                'attribute' => 'domain',
                'value'     => $model->tenant->domain,
            ],
            'tenant_id',
            [
                'attribute' => 'tariff_id',
                'value'     => $model->tariff->name . ' (' . $model->tariff_id . ')',
            ],
            [
                'attribute' => 'city_id',
                'value'     => $model->city->name,
            ],
            'phone',
            'incoming_scenario',
            [
                'attribute' => 'block',
                'value'     => $model->block ? 'Да' : 'Нет',
            ],
            'r_username',
            'r_domain',
            'sip_server_local',
            'auth_proxy',
            'port',
            'realm',
            [
                'attribute' => 'typeServer',
                'content'   => function ($model) {
                    /** @var \app\modules\setting\models\PhoneLine $model */
                    $array = (array)$model::getTypeServerList();

                    return \yii\helpers\ArrayHelper::getValue($array, 'typeServer');
                },
            ],
        ],
    ])
    ?>

</div>
