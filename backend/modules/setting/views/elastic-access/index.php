<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Create Elastic access row';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="phone-line-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('geoElastic')): ?>
    <p>
        <?= Html::a('Создать запись в Эластике', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Импортировать все ключи в Эластик', ['import-all'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Удалить все записи из Эластика', ['delete-all'], ['class' => 'btn btn-danger']) ?>
    </p>
    <?php endif; ?>
    <?=
    GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
            'row_id',
            'tenant_id',
            'type_app',
            'tenant_domain',
            'api_key',
            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('geoElastic'),
                    'delete' => \Yii::$app->user->can('geoElastic'),
                ],
            ],
        ],
    ]);
    ?>

</div>
