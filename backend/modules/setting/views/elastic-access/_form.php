<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\settings\models\ElasticAccess */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="phone-line-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'row_id')->textInput() ?>
    <?= $form->field($model, 'tenant_id')->textInput() ?>

    <?= $form->field($model, 'type_app')->textInput() ?>

    <?= $form->field($model, 'tenant_domain')->textInput() ?>

    <?= $form->field($model, 'api_key')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
