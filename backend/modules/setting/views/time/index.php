<?
/* @var $this yii\web\View */
/* @var $arCountryModels [] \app\modules\setting\models\Country */

use yii\widgets\ActiveForm;

$this->title = 'Настройка часовых поясов';
?>
<div class="box box-primary col-md-6">
    <?php $form = ActiveForm::begin(['validateOnSubmit' => false, 'enableClientValidation' => false]); ?>
        <?foreach ($arCountryModels as $country):
            if(empty($country->republics))
                continue;
        ?>
            <div class="box-header">
                <h3 class="box-title"><?= $country->name?></h3>
            </div>
            <div class="box-body">
                <?foreach ($country->republics as $republic):?>
                    <div class="form-group">
                        <label><?= $republic->name?></label>
                        <?= $form->field($republic, 'timezone', ['template' => '{input}'])->textInput(['name' => "Republic[$republic->republic_id]", 'value' => $republic->timezone]);?>
                    </div>
                <?endforeach;?>
            </div><!-- /.box-body -->
        <?endforeach;?>
    <?php if (Yii::$app->user->can('geo')): ?>
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>
    <?php endif; ?>
    <?php ActiveForm::end(); ?>
</div>
