<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\SmsServer */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Смс сервера', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-server-view">
    <?php if (Yii::$app->user->can('sms')): ?>
    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->server_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->server_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php endif; ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'server_id',
            'name',
            'sort',
            'host',
        ],
    ]) ?>

</div>
