<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Смс сервера';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-server-index">
    <?php if (Yii::$app->user->can('sms')): ?>
        <p>
            <?= Html::a('Добавить смс сервер', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'server_id',
            'name',
            'sort',
            'host',

            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('sms'),
                    'delete' => \Yii::$app->user->can('sms'),
                ],
            ],
        ],
    ]); ?>

</div>
