<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\SmsServer */

$this->title = 'Редактировать смс сервер: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Смс сервера', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->server_id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="sms-server-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
