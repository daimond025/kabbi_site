<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\SmsServer */

$this->title = 'Добавить смс сервер';
$this->params['breadcrumbs'][] = ['label' => 'Смс сервера', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-server-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
