<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\modules\car\models\CarColor */

$this->title = 'Добавить цвет';
$this->params['breadcrumbs'][] = ['label' => 'Цвета', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-color-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
