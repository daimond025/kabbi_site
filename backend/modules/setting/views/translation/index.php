<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\setting\models\MessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Таблица переводов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="message-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php if (Yii::$app->user->can('translations')): ?>
    <p>
        <?= Html::a('Добавить перевод', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php endif ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'language',
            [
                'attribute' => 'category',
                'value' => function($model){
                    return $model->sourceMessage->category;
                }
            ],
            [
                'attribute' => 'message',
                'format' => 'ntext',
                'value' => function($model){
                    return $model->sourceMessage->message;
                }
            ],
            'translation:ntext',
            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('translations'),
                    'delete' => \Yii::$app->user->can('translations'),
                ],
            ],
        ],
    ]); ?>

</div>
