<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\setting\models\Message;

/* @var $this yii\web\View */
/* @var $model backend\modules\setting\models\Message */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="message-form">

    <?php $form = ActiveForm::begin(); ?>
    <?if ($model->isNewRecord):?>
        <?= $form->field($model, 'language')->dropDownList(Message::getLanguageVariants()) ?>
        <?= $form->field($model, 'category')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'message')->textarea(['rows' => 6]) ?>
    <?else:?>
        <?= Html::activeLabel($model, 'language')?>
        <?=  Html::tag('p', $model->language)?>
        <?= Html::activeLabel($model, 'category')?>
        <?=  Html::tag('p', $model->sourceMessage->category)?>
        <?= Html::activeLabel($model, 'message')?>
        <?=  Html::tag('p', $model->sourceMessage->message)?>
    <?endif?>
    <?= $form->field($model, 'translation')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
