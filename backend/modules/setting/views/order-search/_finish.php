<?php

use common\helpers\DateTimeHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\Order */


$this->registerJs("
$('#raw_cacl_data_link').on('click', function () {
    $('#raw_cacl_data').slideToggle();
    return false;
});
");

?>

<table class="table table-condensed" style="width: 600px">
    <tbody>
        <tr>
            <th> </th>
            <th style="width: 60px">Стоимость</th>
        </tr>




        <?

            $isMixedCity    = $info['accrual_city'] == 'MIXED';
            $isMixedOut     = $info['accrual_out'] == 'MIXED';
            $isDistanceCity = $info['accrual_city'] == 'DISTANCE';
            $isDistanceOut  = $info['accrual_out'] == 'DISTANCE';
            $isTimeCity     = $info['accrual_city'] == 'TIME';
            $isTimeOut      = $info['accrual_out'] == 'TIME';

            $isFixed = $info['is_fix'];

            $timeUnit     = t('app', 'min.');
            $distanceUnit = t('app', 'km');

            $isUnknownUnit = true;
            if (!empty($info['city_next_cost_unit'])
                && !empty($info['out_next_cost_unit'])) {
                $isUnknownUnit = false;

                $unitMap = [
                        'minute' => 'min.',
                        'hour'   => 'h.',
                        'km'     => 'km'
                    ];

                list($value, $unit) = explode('_', $info['city_next_cost_unit']);
                $cityNextUnit  = t('app', $unitMap[$unit]);
                $cityNextValue = $value;

                list($value, $unit) = explode('_', $info['out_next_cost_unit']);
                $outNextUnit  = t('app', $unitMap[$unit]);
                $outNextValue = $value;
            }
        ?>

        <!--The cost of planting-->
        <?
        if (!empty(+$info['planting_price'])):
            $plantingPrice = !$isFixed
                ? +$info['planting_price']
                : $info['summary_cost'] - $info['before_time_wait_cost'];
            ?>
            <tr>
                <td><?= t('order', 'Submission car') ?></td>
                <td><b><?= Html::encode($plantingPrice) ?> <?= $currency ?></b></td>
            </tr>
        <? endif ?>

        <!--Included in planting -->
        <?
        $isStartInCity = $info['start_point_location'] == 'in';

        $isMixed    = $isStartInCity ? $isMixedCity : $isMixedOut;
        $isDistance = $isStartInCity ? $isDistanceCity : $isDistanceOut;
        $isTime     = $isStartInCity ? $isTimeCity : $isTimeOut;

        $plantingInclude = null;
        if ($isMixed || $isDistance) {
            $plantingInclude['distance'] = $info['planting_include'];
        }
        if ($isMixed || $isTime) {
            $plantingInclude['time'] = !$isMixed
                ? $info['planting_include'] : $info['planting_include_time'];
        }
        ?>
        <? if (!$isFixed && !empty(+$plantingInclude['distance'])): ?>
            <tr>
                <td>
                    <?= t('order', 'Price includes {value} {unit}', [
                            'value' => Html::encode($plantingInclude['distance']),
                            'unit'  => $distanceUnit,
                        ]) ?>
                </td>
                <td><b>0 <?= $currency ?></td>
            </tr>
        <? endif ?>
        <? if (!$isFixed && !empty(+$plantingInclude['time'])): ?>
            <tr>
                <td>
                    <?= t('order', 'Price includes {value} {unit}', [
                            'value' => Html::encode($plantingInclude['time']),
                            'unit'  => $timeUnit,
                        ]) ?>
                </td>
                <td><b>0 <?= $currencySymbol ?></b></td>
            </tr>
        <? endif ?>

        <!--In city information-->
        <? if (!$isFixed && !$isUnknownUnit):
            $cityCostInfo = null;
            if ($isMixedCity || $isDistanceCity) {
                $cityCostInfo['distance'] = [
                    'cost'  => $info['city_cost'],
                    'value' => $info['city_distance'],
                    'price' => $info['city_next_km_price'],
                ];
            }
            if ($isMixedCity || $isTimeCity) {
                $cityCostInfo['time'] = [
                    'cost'  => !$isMixedCity ? $info['city_cost'] : $info['city_cost_time'],
                    'value' => $info['city_time'],
                    'price' => !$isMixedCity ? $info['city_next_km_price'] : $info['city_next_km_price_time'],
                ];
            }
        ?>
            <? if (!empty(+$cityCostInfo['distance']['value'])): ?>
                <tr>
                    <td>
                        <?= t('order', 'City: {value} {unit} ({price} {currency}/{price-unit})', [
                                'value'      => Html::encode($cityCostInfo['distance']['value']),
                                'unit'       => $distanceUnit,
                                'price'      => Html::encode($cityCostInfo['distance']['price']),
                                'currency'   => $currencySymbol,
                                'price-unit' => $distanceUnit,
                            ]) ?>
                    </td>
                    <td><b><?= Html::encode($cityCostInfo['distance']['cost']); ?> <?= $currency ?></b></td>
                </tr>
            <? endif ?>
            <? if (!empty(+$cityCostInfo['time']['value'])): ?>
                <tr>
                    <td>
                        <?= t('order', 'City: {time} ({price} {currency}/{price-unit})', [
                                'time'       => Html::encode(DateTimeHelper::secondsToStr($cityCostInfo['time']['value'] * 60)),
                                'price'      => Html::encode($cityCostInfo['time']['price']),
                                'currency'   => $currencySymbol,
                                'price-unit' => $cityNextValue == 1
                                        ? $cityNextUnit : "$cityNextValue $cityNextUnit",
                            ]) ?>
                    </td>
                    <td><b><?= Html::encode($cityCostInfo['time']['cost']); ?> <?= $currency ?></b></td>
                </tr>
            <? endif ?>
        <? endif; ?>

        <!--Idle time in the city-->
        <? if (!$isFixed && !empty(+$info['city_time_wait'])): ?>
            <tr>
                <td>
                    <?= t('order', 'Downtime in city: {time} ({price} {currency}/{price-unit})', [
                            'time'       => Html::encode(DateTimeHelper::secondsToStr($info['city_time_wait'])),
                            'price'      => Html::encode($info['city_wait_price']),
                            'currency'   => $currencySymbol,
                            'price-unit' => $timeUnit,
                        ]) ?>
                </td>
                <td><b><?= Html::encode($info['city_wait_cost']); ?> <?= $currency ?></b></td>
            </tr>
        <? endif ?>

        <!--Out of city information-->
        <? if (!$isFixed && !$isUnknownUnit):
            $outCostInfo = null;
            if ($isMixedOut || $isDistanceOut) {
                $outCostInfo['distance'] = [
                    'cost'  => $info['out_city_cost'],
                    'value' => $info['out_city_distance'],
                    'price' => $info['out_next_km_price'],
                ];
            }
            if ($isMixedOut || $isTimeOut) {
                $outCostInfo['time'] = [
                    'cost'  => !$isMixedOut ? $info['out_city_cost'] : $info['out_city_cost_time'],
                    'value' => $info['out_city_time'],
                    'price' => !$isMixedOut ? $info['out_next_km_price'] : $info['out_next_km_price_time'],
                ];
            }
            ?>
            <? if (!empty(+$outCostInfo['distance']['value'])): ?>
                <tr>
                    <td>
                        <?= t('order', 'Outcity: {value} {unit} ({price} {currency}/{price-unit})', [
                                'value'      => Html::encode($outCostInfo['distance']['value']),
                                'unit'       => $distanceUnit,
                                'price'      => Html::encode($outCostInfo['distance']['price']),
                                'currency'   => $currencySymbol,
                                'price-unit' => $distanceUnit,
                            ]) ?>
                    </td>
                    <td><b><?= Html::encode($outCostInfo['distance']['cost']); ?> <?= $currency ?></b></td>
                </tr>
            <? endif ?>
            <? if (!empty(+$outCostInfo['time']['value'])): ?>
                <tr>
                    <td>
                        <?= t('order', 'Outcity: {time} ({price} {currency}/{price-unit})', [
                                'time'       => Html::encode(DateTimeHelper::secondsToStr($outCostInfo['time']['value'] * 60)),
                                'price'      => Html::encode($outCostInfo['time']['price']),
                                'currency'   => $currencySymbol,
                                'price-unit' => $outNextValue == 1
                                        ? $outNextUnit : "$outNextValue $outNextUnit",
                            ]) ?>
                    </td>
                    <td><b><?= Html::encode($outCostInfo['time']['cost']); ?> <?= $currency ?></b></td>
                </tr>
            <? endif ?>
        <? endif; ?>

        <!--Idle time out of city-->
        <? if (!$isFixed && !empty(+$info['out_time_wait'])): ?>
            <tr>
                <td>
                    <?= t('order', 'Downtime in outcity: {time} ({price} {currency}/{price-unit})', [
                            'time'       => Html::encode(DateTimeHelper::secondsToStr($info['out_time_wait'])),
                            'price'      => Html::encode($info['out_wait_price']),
                            'currency'   => $currencySymbol,
                            'price-unit' => $timeUnit,
                        ]) ?>
                </td>
                <td><b><?= Html::encode($info['out_wait_cost']); ?> <?= $currency ?></b></td>
            </tr>
        <? endif ?>

        <!--Waiting time-->
        <? if (!empty(+$info['before_time_wait'])): ?>
            <tr>
                <td>
                    <?= t('order', 'Time waiting before landing in the car: {time} ({price} {currency}/{price-unit})', [
                            'time'       => Html::encode(DateTimeHelper::secondsToStr($info['before_time_wait'] * 60)),
                            'price'      => Html::encode($info['start_point_location'] == 'in'
                                        ? $info['city_wait_price'] : $info['out_wait_price']),
                            'currency'   => $currencySymbol,
                            'price-unit' => $timeUnit,
                        ]) ?>
                </td>
                <td><b><?= Html::encode($info['before_time_wait_cost']); ?> <?= $currency ?></b></td>
            </tr>
        <? endif ?>

        <!--Additional options-->
        <? if (!$isFixed && !empty(+$info['additional_cost'])): ?>
            <tr>
                <td><?= t('order', 'Additional options') ?></td>
                <td><b><?= Html::encode($info['additional_cost']); ?> <?= $currency ?></b></td>
            </tr>
        <? endif ?>

        <!--Information of the distance for plant-->
        <? if (!$isFixed && !empty(+$info['supply_price'])): ?>
            <? if (!empty(+$info['distance_for_plant'])): ?>
                <tr>
                    <td>
                        <?= t('order', 'Distance for submission car: {distance} km. ({price} {currency}/km.)', [
                                'distance' => Html::encode($info['distance_for_plant']),
                                'price'    => Html::encode($info['supply_price']),
                                'currency' => $currencySymbol,
                            ]) ?>
                    </td>
                    <td><b><?= Html::encode($info['distance_for_plant_cost']); ?> <?= $currency ?></b></td>
                </tr>
            <? endif ?>
        <? endif ?>

        <!--Summary time-->
        <? //if (!empty(+$info['summary_time'])): ?>
            <tr>
                <td><?= t('order', 'Summary time') ?></td>
                <td><b><?= Html::encode(DateTimeHelper::secondsToStr($info['summary_time'] * 60)); ?></b></td>
            </tr>
        <? //endif ?>

        <!--Summary distance-->
        <? //if (!empty(+$info['summary_distance'])): ?>
            <tr>
                <td><?= t('order', 'Summary distance') ?></td>
                <td><b><?= Html::encode($info['summary_distance']); ?> <?= t('app', 'km') ?></b></td>
            </tr>
        <? //endif ?>

        <!--Bonus information-->
        <? if (!empty(+$info['bonus'])): ?>
            <tr>
                <td><?= t('order', 'Paid by bonus') ?></td>
                <td><b>
                    <?= Html::encode($info['bonus']); ?>
                    <?= Html::encode(t('currency', 'B') . '(' . $currency . ')'); ?>
                </b></td>
            </tr>
        <? endif; ?>
        <? if (!empty(+$info['refill_bonus'])): ?>
            <tr>
                <td><?= t('order', 'Refilled bonus') ?></td>
                <td><b>
                    <?= Html::encode($info['refill_bonus']); ?>
                    <?= Html::encode(t('currency', 'B') . '(' . $currency . ')'); ?>
                </b></td>
            </tr>
        <? endif; ?>

        <!--Summary cost-->
        <tr>
            <td><b><?= t('order', 'Total') ?></b></td>
            <td><b><?= Html::encode($info['summary_cost']); ?> <?= $currency ?></b></td>
        </tr>
        <tr>
            <td colspan=2>
                <a href="#" id="raw_cacl_data_link">Показать/скрыть сырые данные</a>
                <div style="display:none" id="raw_cacl_data">
                <? dump($raw_cacl_data); ?>
                </div>
            </td>
        </tr>

</tbody>
</table>


