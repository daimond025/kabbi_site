<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\Order */

?>

<div class="callout callout-info">
    <?= Html::encode($info['REVIEW']) ?>
    
    <p>рейтинг: <?= Html::encode($info['RAITING']) ?></p>
</div>
