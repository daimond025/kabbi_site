<?php

use app\modules\setting\assets\SettingAsset;
use yii\helpers\Html;
use yii\grid\GridView;

$bundle = SettingAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/log-widgets.js',
    ['depends' => 'backend\assets\DateRangePickerAsset']);

$this->title                   = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="input-group">
    <button type="button" class="btn btn-default pull-right" id="daterange">
        <i class="fa fa-calendar"></i> <?= t('tenant-notification', 'Choose period') ?>
        <i class="fa fa-caret-down"></i>
    </button>
</div>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel'  => $searchModel,
    'columns'      => [
        [
            'class' => 'yii\grid\SerialColumn',
        ],

        [
            'attribute' => 'order_id',
            'label' => 'ID заказа',
        ],

        [
            'attribute' => 'order_number',
            'options'   =>
                [
                    'width' => '100',
                ],
        ],
        [
            'attribute' => 'tenant_id',
            'value'     => function ($data) {
                return $data->getDomain();
            },
        ],
        [
            'attribute' => 'create_time',
            'value'     => function ($data) {
                return $data->create_time + $data->getOrderOffset();
            },
            'format'    => 'datetime',
            'filter'    => false,
        ],
        [
            'attribute' => 'status_id',
            'filter'    => false,
            'value'     => function ($data) {
                return $data->getStatus();
            },
        ],

        [
            'class'    => \yii\grid\ActionColumn::className(),
            'template' => '{view}',
        ],


    ],
]); ?>
