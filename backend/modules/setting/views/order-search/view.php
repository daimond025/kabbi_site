<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $order app\modules\setting\models\Order */

$bundle = \app\modules\setting\assets\OrderAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/map/main.js');

$this->registerJs("
$('#map-link').one('click', function () {
    $(init_map_data('$map_data_type'));
});
");

$this->title = $order->order_number;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div>

    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">Заказ</a></li>
            <li><a href="#tab_2" data-toggle="tab">События</a></li>
            <li><a href="#tab_3" id="map-link" data-toggle="tab">Маршрут на карте</a></li>
            <li><a href="#tab_4" data-toggle="tab">Отзывы</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">

                <?=
                DetailView::widget([
                    'model'      => $order,
                    'attributes' => [
                        [
                            'attribute' => 'tenant_id',
                            'value'     => $order->getDomain(),
                        ],
                        'order_number',
                        [
                            'attribute' => 'status_id',
                            'value'     => $order->getStatus(),
                        ],
                        [
                            'attribute' => 'client_id',
                            'value'     => $order->getClientName(),
                        ],
                        [
                            'attribute' => 'driver_id',
                            'value'     => $order->getWorkerName(),
                        ],
                        [
                            'attribute' => 'car_id',
                            'value'     => $order->car_id ? $order->car->name . ', ' . $order->car->gos_number . ', ' . t('car',
                                    $order->car->color->name) : $order->car_id,
                        ],
                        [
                            'attribute' => 'tariff_id',
                            'value'     => $order->getTaxiTariff(),
                        ],
                        [
                            'label'     => 'Откуда',
                            'attribute' => 'address',
                            'value'     => $order->getAddressByKey('A'),
                            'format'    => 'html',
                        ],
                        [
                            'label'     => 'Куда',
                            'attribute' => 'address',
                            'value'     => $order->getAddressNotA(),
                            'format'    => 'html',
                        ],
                        'comment',
                        [
                            'attribute' => 'predv_price',
                            'value'     => $order->getPrice(),
                        ],
                        'payment',
                        [
                            'attribute' => 'order_time',
                            'format'    => 'datetime',
                            'value'     => $order->order_time + $order->getOrderOffset(),
                        ],
                        [
                            'attribute' => 'create_time',
                            'format'    => 'datetime',
                            'value'     => $order->create_time + $order->getOrderOffset(),
                        ],

                    ],
                ]);
                ?>
            </div>
            <div class="tab-pane" id="tab_2">
                <dl class="dl-horizontal">

                    <? foreach ($event as $e) : ?>
                    <dt><?= $e['TIME'] ?></dt>
                    <dd>
                        <?= $e['TEXT'] ?>
                        <? switch ($e['TYPE']) {
                            case 'review':
                                echo $this->render('_review', ['info' => $e['INFO']]);
                                break;
                            case 'finish':
                                echo $this->render('_finish',
                                    ['info' => $e['INFO'], 'currency' => $currency, 'raw_cacl_data' => $raw_cacl_data]);
                                break;
                        } ?>
                    <dd>


                        <? endforeach; ?>

                </dl>
            </div>
            <div class="tab-pane" id="tab_3">
                <div id="map" style="height: 500px" data-order="<?= get('id') ?>">
                    <p>Пусто</p>
                </div>
            </div>
            <div class="tab-pane" id="tab_4">
                <dl class="dl-horizontal">

                    <? foreach ($review as $r): ?>
                    <dt><?= Html::encode($r->getClientName()); ?></dt>
                    <dd><?= Html::encode($r->text); ?>
                    <dd>

                        <? endforeach; ?>
            </div>
        </div>
    </div>
</div>
