<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Default Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="default-settings-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php// echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php if (Yii::$app->user->can('defaultSettings')): ?>
        <p>
            <?= Html::a('Create Default Settings', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'value:ntext',
            'type',
            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('defaultSettings'),
                    'delete' => \Yii::$app->user->can('defaultSettings'),
                ],
            ],
        ],
    ]); ?>

</div>
