<?php

use common\modules\tenant\models\DefaultSettingss;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\VarDumper;

/* @var $this yii\web\View */
/* @var $model DefaultSettingss */

$this->title                   = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Default Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="default-settings-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('defaultSettings')): ?>
        <p>
            <?= Html::a('Update', ['update', 'id' => $model->name], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a('Delete', ['delete', 'id' => $model->name], [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method'  => 'post',
                ],
            ])
            ?>

        </p>
    <?php endif; ?>

    <?=
    DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'name',
            'value:ntext',
            'type',
        ],
    ])
    ?>

    <?
    if ($model->name == "PHONE_LINE_SCENARIO") {
        $scenario = unserialize($model->value);
        VarDumper::dump($scenario, 10, true);
        echo '</br>';
        echo Html::a("Установить сценарий для всех", "/setting/default-settings/set-phone-line-scenario-for-all/",
            ["class" => "btn btn-primary"]);
    }
    ?>

</div>
