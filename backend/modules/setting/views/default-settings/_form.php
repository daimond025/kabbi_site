<?php

use common\modules\tenant\models\DefaultSettings;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model DefaultSettings */
/* @var $form ActiveForm */
?>

<div class="default-settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'value')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'type')->dropDownList([
        'general' => 'General',
        'orders'  => 'Orders',
        'drivers' => 'Drivers',
        'cars'    => 'Cars',
        'system'  => 'System',
    ], ['prompt' => '', 'disabled' => !$model->isNewRecord]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
