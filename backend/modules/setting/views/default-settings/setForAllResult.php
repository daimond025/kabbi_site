<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Установка сценария телефонного вызова для всех заказчиков';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="default-settings-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Количество обновленных записей : <?= $updateResult ?>
    </p>

</div>
