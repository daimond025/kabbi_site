<?php

use common\modules\tenant\models\DefaultSettings;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model DefaultSettings */

$this->title                   = 'Create Default Settings';
$this->params['breadcrumbs'][] = ['label' => 'Default Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="default-settings-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
