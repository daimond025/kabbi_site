<?php

use common\modules\tenant\models\DefaultSettings;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model DefaultSettings */

$this->title                   = 'Update Default Settings: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Default Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->name]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="default-settings-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
