<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\employee\models\position\PositionHasDocument */

$this->title = 'Update Position Has Document: ' . $model->position_id;
$this->params['breadcrumbs'][] = ['label' => 'Position Has Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = [
    'label' => $model->position_id,
    'url'   => [
        'view',
        'position_id' => $model->position_id,
        'document_id' => $model->document_id,
    ],
];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="position-has-document-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'       => $model,
        'positionMap' => $positionMap,
        'documentMap' => $documentMap,
    ]) ?>

</div>
