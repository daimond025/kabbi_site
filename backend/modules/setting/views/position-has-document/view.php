<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\employee\models\position\PositionHasDocument */

$this->title = $model->position_id;
$this->params['breadcrumbs'][] = ['label' => 'Position Has Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="position-has-document-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('employee')): ?>
    <p>
        <?= Html::a('Update', ['update', 'position_id' => $model->position_id, 'document_id' => $model->document_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'position_id' => $model->position_id, 'document_id' => $model->document_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php endif ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'position_id',
            'document_id',
        ],
    ]) ?>

</div>
