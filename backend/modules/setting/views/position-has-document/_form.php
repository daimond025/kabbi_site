<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\employee\models\position\PositionHasDocument */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="position-has-document-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'position_id')->dropDownList($positionMap) ?>

    <?= $form->field($model, 'document_id')->dropDownList($documentMap) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
