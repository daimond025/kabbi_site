<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Position Has Documents';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="position-has-document-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('employee')): ?>
    <p>
        <?= Html::a('Create Position Has Document', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php endif ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'position.name',
            'document.name',

            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('employee'),
                    'delete' => \Yii::$app->user->can('employee'),
                ],
            ],
        ],
    ]); ?>
</div>
