<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\employee\models\position\PositionHasDocument */

$this->title = 'Create Position Has Document';
$this->params['breadcrumbs'][] = ['label' => 'Position Has Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="position-has-document-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'       => $model,
        'positionMap' => $positionMap,
        'documentMap' => $documentMap,
    ]) ?>

</div>
