<?php

use common\modules\tenant\models\DefaultClientStatusEvent;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model DefaultClientStatusEvent */
/* @var array $positionMap */
/* @var array $statusMap */
/* @var array $groupMap */
/* @var array $noticeMap */

?>

<div class="default-client-status-event-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'position_id')->dropDownList($positionMap,
        ['disabled' => !$model->isNewRecord])->label('Профессия') ?>

    <?= $form->field($model, 'status_id')->dropDownList($statusMap,
        ['disabled' => !$model->isNewRecord])->label('Статус') ?>

    <?= $form->field($model, 'group')->dropDownList($groupMap, ['disabled' => !$model->isNewRecord])->label('Группа') ?>

    <?= $form->field($model, 'notice')->dropDownList($noticeMap)->label('Действие') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
