<?php

use app\modules\setting\models\DefaultClientStatusEvent;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model DefaultClientStatusEvent */
/* @var array $positionMap */
/* @var array $statusMap */
/* @var array $groupMap */
/* @var array $noticeMap */

$this->title                   = 'Добавить уведомление клиентам';
$this->params['breadcrumbs'][] = ['label' => 'Уведомления клиентам', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="default-client-status-event-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'       => $model,
        'positionMap' => $positionMap,
        'statusMap'   => $statusMap,
        'groupMap'    => $groupMap,
        'noticeMap'   => $noticeMap,
    ]) ?>

</div>
