<?php

use app\modules\setting\models\DefaultClientStatusEvent;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model DefaultClientStatusEvent */
/* @var array $positionMap */
/* @var array $statusMap */
/* @var array $groupMap */
/* @var array $noticeMap */


$this->title                   = 'Изменить уведомление клиентам: ' . ' ' . $model->event_id;
$this->params['breadcrumbs'][] = ['label' => 'Уведомление клиетам', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->event_id, 'url' => ['view', 'id' => $model->event_id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="default-client-status-event-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'       => $model,
        'positionMap' => $positionMap,
        'statusMap'   => $statusMap,
        'groupMap'    => $groupMap,
        'noticeMap'   => $noticeMap,
    ]) ?>

</div>
