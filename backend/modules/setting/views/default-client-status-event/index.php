<?php

use app\modules\setting\models\DefaultClientStatusEventSearch;
use common\services\OrderStatusService;
use yii\helpers\Html;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $searchModel DefaultClientStatusEventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var array $positionMap */
/* @var array $statusMap */
/* @var array $groupMap */
/* @var array $noticeMap */

$this->title                   = 'Уведомления клиентам';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="default-client-status-event-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <? if (Yii::$app->user->can('clientStatusEvent')): ?>
        <p>
            <?= Html::a('Добавить уведомление клиентам', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <? endif ?>

    <? Pjax::begin() ?>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'event_id',

            [
                'attribute' => 'position_id',
                'label'     => 'Профессия',
                'value'     => function ($model) use ($positionMap) {
                    return $positionMap[$model->position_id];
                },
                'filter'    => $positionMap,
            ],
            [
                'attribute' => 'status_id',
                'label'     => 'Статус',
                'value'     => function ($model) {
                    return OrderStatusService::translate($model->status_id, $model->position_id);
                },
                'filter'    => $statusMap,
            ],
            [
                'attribute' => 'group',
                'label'     => 'Группа',
                'value'     => function ($model) use ($groupMap) {
                    return $groupMap[$model->group];
                },
                'filter'    => $groupMap,
            ],
            [
                'attribute' => 'notice',
                'label'     => 'Действие',
                'value'     => function ($model) use ($noticeMap) {
                    return $noticeMap[$model->notice];
                },
                'filter'    => $noticeMap,
            ],
            [
                'class'          => 'yii\grid\ActionColumn',
                'template'       => '{update} {delete}',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('clientStatusEvent'),
                    'delete' => \Yii::$app->user->can('clientStatusEvent'),
                ],
            ],
        ],
    ]);
    ?>
    <? Pjax::end() ?>
</div>
