<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\setting\models\TenantAdminTariffSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Тарифы арендаторов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenant-admin-tariff-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить новый тариф', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    \yii\widgets\Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'tariff_id',
            'name',
            'price',
            'stuff_quantity',
            'driver_quantity',
            [
                'attribute' => 'block',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->block == 0) {
                        return 'Нет';
                    } else {
                        return 'Да';
                    }
                },
            ],
            'create_date',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    \yii\widgets\Pjax::end();
    ?>

</div>
