<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\TenantAdminTariff */

$this->title = 'Редактировать тариф арендатора: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Тарифы арендатора', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->tariff_id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="tenant-admin-tariff-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
