<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var $this \yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $searchModel \backend\modules\setting\models\outboundRouting\PhoneLineOutboundRoutingSearch
 */

use \backend\modules\setting\models\outboundRouting\PhoneLineOutboundRoutingSearch;
use \yii\helpers\ArrayHelper;

$this->title                   = 'Исх. маршрутизация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="outbound-routing-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('phoneLines')): ?>
        <p>
            <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>
    <?php
    \yii\widgets\Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'routing_id',
            'name',
            [
                'attribute' => 'domain',
                'content'   => function (PhoneLineOutboundRoutingSearch $model) {
                    return Html::encode($model->tenant->domain);
                },
            ],

            [
                'attribute' => 'cityName',
                'content'   => function (PhoneLineOutboundRoutingSearch $model) {

                    return Html::encode($model->city->name);
                },
            ],

            [
                'attribute' => 'active',
                'filter'    => $searchModel->getActiveMap(),
                'content'   => function (PhoneLineOutboundRoutingSearch $model) {
                    $activeMap = $model->getActiveMap();

                    return Html::encode(ArrayHelper::getValue($activeMap, $model->active));
                },
            ],

            'sort',

            [
                'attribute' => 'tariffs',
                'content'   => function (PhoneLineOutboundRoutingSearch $model) {

                    $tariffs = ArrayHelper::getColumn($model->taxiTariff, function ($model) {
                        return "{$model->name} ({$model->tariff_id})";
                    });

                    return Html::encode(implode(', ', $tariffs));
                },
            ],

            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('phoneLines'),
                    'delete' => \Yii::$app->user->can('phoneLines'),
                ],
            ],
        ],
    ]);
    \yii\widgets\Pjax::end();
    ?>

</div>