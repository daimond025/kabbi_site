<?php

/* @var $this yii\web\View
 * @var $model \backend\modules\setting\models\outboundRouting\PhoneLineOutboundRoutingModel
 * @var $key string
 * @var $isTemplate bool
 * @var $phoneLineList array
 * @var $form yii\widgets\ActiveForm
 */

?>

<div class="box <?= $model->isNewRecord || $isTemplate || empty($model->onOperator[$key]) ? 'box-success' : 'box-primary'; ?> js-container-on-operator <?= $isTemplate ? 'js-container-on-operator-template' : ''; ?>"
     data-key="<?= $key ?>" <?= $isTemplate ? 'style="display: none"' : ''; ?>>
    <div class="box-body">
        <?= $form->field($model, "lineOnOperator[{$key}]")->dropDownList($phoneLineList,
            ['class' => 'form-control js-lineOnOperator', 'disabled' => $isTemplate]); ?>
        <?= $form->field($model, "sortOnOperator[{$key}]")->textInput([
            'class'    => 'form-control js-sortOnOperator',
            'disabled' => $isTemplate,
        ]); ?>
        <?= $form->field($model, "timeoutOnOperator[{$key}]")->textInput([
            'class'    => 'form-control js-timeoutOnOperator',
            'disabled' => $isTemplate,
        ]); ?>
        <div class="form-group">
            <?= \yii\helpers\Html::button('Удалить', ['class' => 'btn btn-danger js-delete-button-on-operator']); ?>
        </div>
    </div>
</div>