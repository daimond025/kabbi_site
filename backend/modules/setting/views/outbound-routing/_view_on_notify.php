<?php

use yii\widgets\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \backend\modules\setting\models\outboundRouting\PhoneLineOutboundRoutingOnNotify */

?>
<?=
DetailView::widget([
    'model'      => $model,
    'attributes' => [
        [
            'attribute' => 'line_id',
            'value'     => Html::a(Html::encode($model->phoneLine->phone),
                ['/setting/phone-line/view', 'id' => $model->line_id]),
            'format'    => 'html',
        ],
        'sort',
        'timeout',
        'distribution_count',
        'distribution_delay',
    ],
])
?>