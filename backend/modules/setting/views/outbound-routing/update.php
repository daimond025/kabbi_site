<?php

use yii\helpers\Html;

/* @var $this yii\web\View
 * @var $model \backend\modules\setting\models\outboundRouting\PhoneLineOutboundRoutingModel
 * @var $cityList array
 * @var $tariffList array
 * @var $phoneLineList array
 * @var $form yii\widgets\ActiveForm
 */

\backend\modules\setting\assets\OutboundRoutingAsset::register($this);

$this->title                   = 'Редактирование исх. маршрутизации';
$this->params['breadcrumbs'][] = ['label' => 'Исх. маршрутизация', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->routing_id, 'url' => ['view', 'id' => $model->routing_id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="outbound-routing-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'         => $model,
        'cityList'      => $cityList,
        'tariffList'    => $tariffList,
        'phoneLineList' => $phoneLineList,
    ]) ?>

</div>
