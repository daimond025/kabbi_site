<?php

use yii\helpers\Html;

/* @var $this yii\web\View
 * @var $model \backend\modules\setting\models\outboundRouting\PhoneLineOutboundRoutingModel
 * @var $cityList array
 * @var $tariffList array
 * @var $phoneLineList array
 * @var $form yii\widgets\ActiveForm
 */

$asset = \backend\modules\setting\assets\OutboundRoutingAsset::register($this);
//dd($asset->baseUrl . '/js/create.js');
$this->registerJsFile($asset->baseUrl . '/js/create.js', ['depends' => $asset->depends]);

$this->title                   = 'Добавление исх. маршрутизации';
$this->params['breadcrumbs'][] = ['label' => 'Исх. маршрутизация', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Добавить';
?>
<div class="outbound-routing-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'         => $model,
        'cityList'      => $cityList,
        'tariffList'    => $tariffList,
        'phoneLineList' => $phoneLineList,
    ]) ?>

</div>
