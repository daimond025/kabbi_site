<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View
 * @var $model \backend\modules\setting\models\outboundRouting\PhoneLineOutboundRoutingModel
 * @var $cityList array
 * @var $tariffList array
 * @var $phoneLineList array
 * @var $form yii\widgets\ActiveForm
 */

?>

<div class="phone-line-form">

    <?php $form = ActiveForm::begin(); ?>

    <h3>Общее</h3>

    <?= $form->field($model, 'name')->textInput(['class' => 'form-control js-name']); ?>

    <?= $form->field($model, 'domain')->textInput([
        'class'    => 'form-control js-domain',
        'disabled' => !$model->isNewRecord,
    ]); ?>
    <?= Html::label($model->getAttributeLabel('tenant_id')); ?>
    <?= Html::textInput('tenant-id', $model->tenant_id, [
        'class'    => 'form-control js-tenant-id',
        'disabled' => true,
    ]); ?>
    <?= $form->field($model, 'tenant_id')->hiddenInput(['class' => 'form-control js-tenant-id'])->label(false); ?>

    <?= $form->field($model, 'city_id')->dropDownList($cityList, [
        'class'    => 'form-control js-city-id',
        'disabled' => !$model->isNewRecord,
    ]); ?>

    <?= $form->field($model, 'taxiTariffList')->dropDownList($tariffList,
        ['class' => 'form-control js-tariff-id', 'multiple' => true]); ?>

    <?= $form->field($model, 'sort')->textInput(['class' => 'form-control js-sort']); ?>
    <?= $form->field($model, 'active')->dropDownList($model->getActiveMap(), ['class' => 'form-control js-active']); ?>


    <h3>Для автоотзвонов</h3>

    <? foreach ($model->lineOnNotify as $key => $ignore): ?>

        <?= $this->render('_form_on_notify', [
            'form'          => $form,
            'model'         => $model,
            'key'           => $key,
            'phoneLineList' => $phoneLineList,
            'isTemplate'    => false,
        ]) ?>

    <? endforeach; ?>

    <?= $this->render('_form_on_notify', [
        'form'          => $form,
        'model'         => $model,
        'key'           => '',
        'phoneLineList' => $phoneLineList,
        'isTemplate'    => true,
    ]) ?>

    <div class="form-group">
        <?= Html::button('Добавить еще', ['class' => 'btn btn-primary js-add-button-on-notify']); ?>
    </div>


    <h3>Для операторов</h3>

    <? foreach ($model->lineOnOperator as $key => $ignore): ?>

        <?= $this->render('_form_on_operator', [
            'form'          => $form,
            'model'         => $model,
            'key'           => $key,
            'phoneLineList' => $phoneLineList,
            'isTemplate'    => false,
        ]) ?>

    <? endforeach; ?>

    <?= $this->render('_form_on_operator', [
        'form'          => $form,
        'model'         => $model,
        'key'           => '',
        'phoneLineList' => $phoneLineList,
        'isTemplate'    => true,
    ]) ?>

    <div class="form-group">
        <?= Html::button('Добавить еще', ['class' => 'btn btn-primary js-add-button-on-operator']); ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Редактировать',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
