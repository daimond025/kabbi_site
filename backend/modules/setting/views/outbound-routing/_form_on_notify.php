<?php

/* @var $this yii\web\View
 * @var $model \backend\modules\setting\models\outboundRouting\PhoneLineOutboundRoutingModel
 * @var $key string
 * @var $isTemplate bool
 * @var $phoneLineList array
 * @var $form yii\widgets\ActiveForm
 */

?>

<div class="box <?= $model->isNewRecord || $isTemplate || empty($model->onNotify[$key]) ? 'box-success' : 'box-primary'; ?> js-container-on-notify <?= $isTemplate ? 'js-container-on-notify-template' : ''; ?>"
     data-key="<?= $key ?>" <?= $isTemplate ? 'style="display: none"' : ''; ?>>
    <div class="box-body">
        <?= $form->field($model, "lineOnNotify[{$key}]")->dropDownList($phoneLineList,
            ['class' => 'form-control js-lineOnNotify', 'disabled' => $isTemplate]); ?>
        <?= $form->field($model, "sortOnNotify[{$key}]")->textInput([
            'class'    => 'form-control js-sortOnNotify',
            'disabled' => $isTemplate,
        ]); ?>
        <?= $form->field($model, "timeoutOnNotify[{$key}]")->textInput([
            'class'    => 'form-control js-timeoutOnNotify',
            'disabled' => $isTemplate,
        ]); ?>
        <?= $form->field($model, "distributionCountOnNotify[{$key}]")->textInput([
            'class'    => 'form-control js-distributionCountOnNotify',
            'disabled' => $isTemplate,
        ]); ?>
        <?= $form->field($model, "distributionDelayOnNotify[{$key}]")->textInput([
            'class'    => 'form-control js-distributionDelayOnNotify',
            'disabled' => $isTemplate,
        ]); ?>

        <div class="form-group">
            <?= \yii\helpers\Html::button('Удалить', ['class' => 'btn btn-danger js-delete-button-on-notify']); ?>
        </div>
    </div>
</div>