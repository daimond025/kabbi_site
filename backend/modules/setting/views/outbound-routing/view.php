<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \backend\modules\setting\models\outboundRouting\PhoneLineOutboundRoutingModel;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model \backend\modules\setting\models\outboundRouting\PhoneLineOutboundRoutingModel */

$this->title                   = $model->routing_id;
$this->params['breadcrumbs'][] = ['label' => 'Исх. маршрутизация', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="phone-line-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('phoneLines')): ?>
        <p>
            <?= Html::a('Редактировать', ['update', 'id' => $model->routing_id], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a('Удалить', ['delete', 'id' => $model->routing_id], [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => 'Вы точно хотите удалить элемент?',
                    'method'  => 'post',
                ],
            ])
            ?>
        </p>
    <?php endif; ?>
    <h3>Общее</h3>
    <?=
    DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'name',
            'domain',
            [
                'attribute' => 'city_id',
                'value'     => Html::encode($model->city->name),
            ],
            'tenant_id',
            [
                'attribute' => 'taxiTariffList',
                'value'     => function (PhoneLineOutboundRoutingModel $model) {
                    $tariffs = ArrayHelper::getColumn($model->taxiTariff, function ($model) {
                        return "{$model->name} ({$model->tariff_id})";
                    });

                    return implode(', ', $tariffs);
                },
            ],
            'sort',
            [
                'attribute' => 'active',
                'value'     => function (PhoneLineOutboundRoutingModel $model) {
                    $activeMap = $model->getActiveMap();

                    return Html::encode(ArrayHelper::getValue($activeMap, $model->active));
                },
            ],
        ],
    ])
    ?>

    <? if(!empty($model->onNotify)): ?>
        <h3>Для автоотзвонов</h3>
        <? foreach ($model->onNotify as $onNotify): ?>

            <?= $this->render('_view_on_notify', [
                'model' => $onNotify,
            ]); ?>
        <? endforeach; ?>
    <? endif; ?>

    <? if(!empty($model->onOperator)): ?>
        <h3>Для операторов</h3>
        <? foreach ($model->onOperator as $onOperator): ?>

            <?= $this->render('_view_on_operator', [
                'model' => $onOperator,
            ]); ?>
        <? endforeach; ?>
    <? endif; ?>



</div>
