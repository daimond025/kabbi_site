<?php

use backend\modules\setting\helpers\NotificationHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var \yii\web\View                                                $this
 * @var \backend\modules\setting\forms\DefaultWorkerNotificationForm $formModel
 * @var string[]                                                     $typesMap
 * @var string[]                                                     $paramsMap
 * @var string[]                                                     $positionsMap
 */

$this->title                   = $formModel->pushId;
$this->params['breadcrumbs'][] = ['label' => 'Шаблоны PUSH-уведомлений исполнителю', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="default-client-push-notifications-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $formModel->pushId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $formModel->pushId], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => 'Удалить этот шаблон?',
                'method'  => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model'      => $formModel,
        'attributes' => [
            [
                'attribute' => 'type',
                'value'     => ArrayHelper::getValue($typesMap, $formModel->type),
            ],
            [
                'attribute' => 'positionId',
                'value'     => ArrayHelper::getValue($positionsMap, $formModel->positionId),
            ],
            'text',
            [
                'attribute' => 'params',
                'format'    => 'html',
                'value'     => NotificationHelper::getParamsAsString($formModel->params),
            ],
        ],
    ]) ?>

</div>
