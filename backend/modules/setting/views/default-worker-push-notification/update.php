<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var \yii\web\View                                                $this
 * @var \backend\modules\setting\forms\DefaultWorkerNotificationForm $formModel
 * @var string[]                                                     $typesMap
 * @var string[]                                                     $paramsMap
 * @var string[]                                                     $positionsMap
 */

$this->title                   = 'Редактировать шаблон PUSH-уведомления исполнителю: ' . ' ' . $formModel->pushId;
$this->params['breadcrumbs'][] = ['label' => 'Шаблоны PUSH-уведомлений исполнителю', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактировать';
?>

<div class="default-client-push-notifications-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="default-client-push-notifications-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($formModel, 'positionId')->dropDownList($positionsMap, ['disabled' => true]) ?>
        <?= $form->field($formModel, 'type')->dropDownList($typesMap, ['disabled' => true]) ?>
        <?= $form->field($formModel, 'text')->textarea(['rows' => 6]) ?>
        <?= $form->field($formModel, 'params')->checkboxList($paramsMap, [
            'separator' => '<br/>',
            'encode'    => false,
        ]) ?>

        <div class="form-group">
            <?= Html::submitButton('Редактировать', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
