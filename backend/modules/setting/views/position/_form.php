<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\modules\employee\models\position\Position */
/* @var $modules array */
/* @var $typeList array */
/* @var $parentPositions array */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="position-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'module_id')->dropDownList($modules) ?>

    <?= $form->field($model, 'has_car')->checkbox() ?>

    <?= $form->field($model, 'transport_type_id')->dropDownList($typeList, ['prompt' => 'None']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
