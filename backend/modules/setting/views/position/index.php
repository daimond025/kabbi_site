<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Positions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="position-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('employee')): ?>
        <p>
            <?= Html::a('Create Position', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'position_id',
            'name',
            [
                'attribute' => 'module.name',
                'label'     => 'Module',
            ],
            [
                'attribute' => 'has_car',
                'value'     => function ($model) {
                    return $model->has_car ? 'Да' : 'Нет';
                },
            ],
            [
                'attribute' => 'transportType.name',
                'label'     => 'Тип транспорта',
            ],
            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('employee'),
                    'delete' => \Yii::$app->user->can('employee'),
                ],
            ],
        ],
    ]); ?>
</div>
