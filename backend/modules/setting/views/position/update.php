<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \common\modules\employee\models\position\Position */
/* @var array $modules */
/* @var array $typeList */

$this->title = 'Update Position: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Positions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->position_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="position-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'           => $model,
        'modules'         => $modules,
        'typeList'        => $typeList,
    ]) ?>

</div>
