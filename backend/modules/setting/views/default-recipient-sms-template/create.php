<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var \yii\web\View                                                  $this
 * @var \backend\modules\setting\forms\DefaultRecipientSmsTemplateForm $formModel
 * @var string[]                                                       $typesMap
 * @var string[]                                                       $paramsMap
 * @var string[]                                                       $positionsMap
 */

$this->title                   = 'Создать шаблон SMS-уведомления получателю';
$this->params['breadcrumbs'][] = ['label' => 'Шаблоны SMS-уведомлений получателю', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Создать';
?>

<div class="default-client-push-notifications-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="default-client-push-notifications-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($formModel, 'positionId')->dropDownList($positionsMap) ?>
        <?= $form->field($formModel, 'type')->dropDownList($typesMap) ?>
        <?= $form->field($formModel, 'text')->textarea(['rows' => 6]) ?>
        <?= $form->field($formModel, 'params')->checkboxList($paramsMap, [
            'separator' => '<br/>',
            'encode'    => false,
        ]) ?>

        <div class="form-group">
            <?= Html::submitButton('Создать', ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>