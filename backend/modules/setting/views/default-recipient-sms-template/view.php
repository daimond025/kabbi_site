<?php

use backend\modules\setting\helpers\NotificationHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var \yii\web\View                                                  $this
 * @var \backend\modules\setting\forms\DefaultRecipientSmsTemplateForm $formModel
 * @var string[]                                                       $typesMap
 * @var string[]                                                       $paramsMap
 * @var string[]                                                       $positionsMap
 */

$this->title                   = $formModel->templateId;
$this->params['breadcrumbs'][] = ['label' => 'Шаблоны SMS-уведомлений получателю', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="default-client-push-notifications-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $formModel->templateId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $formModel->templateId], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => 'Удалить этот шаблон?',
                'method'  => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model'      => $formModel,
        'attributes' => [
            [
                'attribute' => 'type',
                'value'     => ArrayHelper::getValue($typesMap, $formModel->type),
            ],
            [
                'attribute' => 'positionId',
                'value'     => ArrayHelper::getValue($positionsMap, $formModel->positionId),
            ],
            'text',
            [
                'attribute' => 'params',
                'format'    => 'html',
                'value'     => NotificationHelper::getParamsAsString($formModel->params),
            ],
        ],
    ]) ?>

</div>
