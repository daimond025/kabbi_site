<?php

use backend\modules\setting\forms\search\DefaultRecipientSmsTemplateSearch;
use backend\modules\setting\helpers\NotificationHelper;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**
 * @var \yii\web\View                     $this
 * @var DefaultRecipientSmsTemplateSearch $searchModel
 * @var \yii\data\ActiveDataProvider      $dataProvider
 * @var string[]                          $typesMap
 * @var string[]                          $paramsMap
 * @var string[]                          $positionsMap
 *
 */

$this->title                   = 'Шаблоны SMS-уведомлений получателю';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="default-worker-push-notifications-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('messageTemplates')): ?>
        <p>
            <?= Html::a('Добавить шаблон SMS-уведомления получателю', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>

    <? Pjax::begin() ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'attribute' => 'type',
                'content'   => function ($model, $key, $index, $column) use ($typesMap) {
                    /** @var DefaultRecipientSmsTemplateSearch $model */
                    return ArrayHelper::getValue($typesMap, $model->type);
                },
                'filter'    => $typesMap,
            ],
            [
                'attribute' => 'position_id',
                'content'   => function ($model, $key, $index, $column) use ($positionsMap) {
                    /** @var DefaultRecipientSmsTemplateSearch $model */
                    return ArrayHelper::getValue($positionsMap, $model->position_id);
                },
                'filter'    => $positionsMap,
            ],
            'text',
            [
                'attribute' => 'params',
                'content'   => function ($model, $key, $index, $column) use ($positionsMap) {
                    /** @var DefaultRecipientSmsTemplateSearch $model */
                    return NotificationHelper::getParamsAsString($model->params);
                },
            ],

            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('messageTemplates'),
                    'delete' => \Yii::$app->user->can('messageTemplates'),
                ],
            ],
        ],
    ]); ?>
    <? Pjax::end() ?>
</div>