<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\TenantPayment */

$this->title = 'Редактировать платеж арендатора: ' . ' ' . $model->payment_id;
$this->params['breadcrumbs'][] = ['label' => 'Платежи арендаторов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->payment_id, 'url' => ['view', 'id' => $model->payment_id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="tenant-payment-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
