<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\setting\assets\SettingAsset;
use common\helpers\DateTimeHelper;

$bundle = SettingAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/js/bootstrap-datepicker.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile($bundle->baseUrl . '/locales/bootstrap-datepicker.ru.min.js', ['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile($bundle->baseUrl . '/elecsnet-page-widgets.js', ['depends' => 'yii\web\JqueryAsset']);


/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\TenantPayment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tenant-payment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'amount')->textInput() ?>

    <?= $form->field($model, 'payment_activity_begin_date')->textInput(['class' => 'form-control date'])?>

    <?= $form->field($model, 'tenant_id')->textInput() ?>

    <?= $form->field($model, 'period')->dropDownList(DateTimeHelper::getYearMonths()); ?>

    <?= $form->field($model, 'payment_item')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
