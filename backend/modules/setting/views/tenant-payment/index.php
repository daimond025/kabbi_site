<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\setting\models\TenantPaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Платежи арендаторов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenant-payment-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить платеж', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php
    \yii\widgets\Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'payment_id',
            'amount',
            'tenant_id',
            'create_date',
            'period',
            'payment_activity_begin_date',
            'expiration_date',
             'payment_item',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); 
    \yii\widgets\Pjax::end();
    ?>

</div>
