<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\TenantPaymentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tenant-payment-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'payment_id') ?>

    <?= $form->field($model, 'amount') ?>

    <?= $form->field($model, 'tenant_id') ?>

    <?= $form->field($model, 'create_date') ?>

    <?= $form->field($model, 'period') ?>

    <?php // echo $form->field($model, 'payment_item') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
