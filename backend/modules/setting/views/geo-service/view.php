<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\VarDumper;

/* @var $this yii\web\View */
/* @var $model app\modules\settings\models\GeoServiceProvider */

$this->title = $model->row_id;
$this->params['breadcrumbs'][] = ['label' => 'Elastc geo-service row', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="phone-line-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('geoElastic')): ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->row_id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->row_id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method'  => 'post',
            ],
        ])
        ?>
    </p>
    <?php endif; ?>
    <?=
    DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'row_id', 'tenant_id', 'tenant_domain', 'city_id', 'service_type', 'service_provider_id', 'key_1', 'key_2',
        ],
    ])
    ?>
</div>
