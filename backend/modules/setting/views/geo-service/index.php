<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider \app\modules\setting\models\GeoServiceProvider */
/* @var $searchModel \app\modules\setting\models\GeoServiceProvider */

$this->title                   = 'Create Elastic geo-service row';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="phone-line-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('geoElastic')): ?>
        <p>
            <?= Html::a('Создать запись в Эластике', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>
    <?=
    GridView::widget([
        'filterModel'  => $searchModel,
        'dataProvider' => $dataProvider,
        'columns'      => [
            ['class' => \yii\grid\SerialColumn::className()],
            'row_id',
            'tenant_id',
            'tenant_domain',
            'city_id',
            'service_type',
            'service_provider_id',
            'key_1',
            'key_2',
            [
                'class'          => \yii\grid\ActionColumn::className(),
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('geoElastic'),
                    'delete' => \Yii::$app->user->can('geoElastic'),
                ],
            ],
        ],
    ]);
    ?>

</div>
