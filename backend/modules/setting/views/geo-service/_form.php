<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\settings\models\GeoServiceProvider */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="phone-line-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'row_id')->textInput() ?>

    <?= $form->field($model, 'tenant_id')->textInput() ?>

    <?= $form->field($model, 'tenant_domain')->textInput() ?>

    <?= $form->field($model, 'city_id')->textInput() ?>

    <?= $form->field($model, 'service_type')->textInput() ?>

    <?= $form->field($model, 'service_provider_id')->textInput() ?>

    <?= $form->field($model, 'key_1')->textInput() ?>

    <?= $form->field($model, 'key_2')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
