<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use backend\modules\setting\models\Message;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\Holiday */
/* @var $form yii\widgets\ActiveForm */
?>

<?php Pjax::begin(); ?>

<div class="holiday-form">

    <?php $form = ActiveForm::begin([
            'options' => ['data-pjax' => '']
        ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
    
    <?$languageVariants = Message::getLanguageVariants();?>
    <? foreach ($languageVariants as $language => $translate): ?>
        <div class="form-group">
            <?= Html::label($translate) ?>
            <?$value = isset($_POST['translate'][$language]) ? $_POST['translate'][$language] : (isset($translates[$language]) ? $translates[$language]->translation : '')?>
            <?= Html::textInput('translate[' . $language . ']', $value, ['class' => 'form-control']) ?>
        </div>
    <? endforeach; ?>
    
    <br/>
    
    <?= $form->field($model, 'code')->textInput(['maxlength' => 3]) ?> 
    
    <?= $form->field($model, 'symbol')->textInput(['maxlength' => 255]) ?> 
    
    <?= $form->field($model, 'numeric_code')->textInput(['maxlength' => 3]) ?>
    
    <?= $form->field($model, 'minor_unit')->textInput(['maxlength' => 2]) ?>
    
    <?= $form->field($model, 'type_id')->dropDownList($model->typeList()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Редактировать', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php Pjax::end(); ?>
