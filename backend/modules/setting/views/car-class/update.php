<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \frontend\modules\car\models\CarClass */

$this->title = 'Редактирование класса: ' . ' ' . $model->class;
$this->params['breadcrumbs'][] = ['label' => 'Классы авто', 'url' => ['index']];
$this->params['breadcrumbs'][] = Html::encode($model->class);
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="car-class-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', compact('model', 'translates', 'typeMap')) ?>

</div>