<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model \common\modules\car\models\CarClass */

$this->title = 'Добавить класс';
$this->params['breadcrumbs'][] = ['label' => 'Классы авто', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-class-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'   => $model,
        'typeMap' => $typeMap,
    ]) ?>

</div>