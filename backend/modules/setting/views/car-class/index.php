<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Классы авто';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-class-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('transport')): ?>
    <p>
        <?= Html::a('Добавить класс', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php endif; ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'class_id',
            'class',
            'sort',
            [
                'attribute' => 'type_id',
                'label' => 'Тип транспорта',
                'value' => 'type.name',
                'filter' => $searchModel->getTransportType(),
            ],
            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('transport'),
                    'delete' => \Yii::$app->user->can('transport'),
                ],
                'template'       => '{update} {delete}',
            ],
        ],
    ]); ?>

</div>