<?php
use yii\helpers\Html;
use app\modules\setting\assets\UpdatePlaceAsset;
use yii\widgets\Menu;

$bundle = UpdatePlaceAsset::register($this);

$this->registerJsFile($bundle->baseUrl . '/mapEditor/update.js', ['depends' => 'yii\web\JqueryAsset']);
?>
<div class="city-index">
    <h1><?= t('parking', 'Parkings') ?></h1>
    <input id="cityLat" type="text" value="<?= Html::encode($model->lat) ?>" style="display:none"/>
    <input id="cityLon" type="text" value="<?= Html::encode($model->lon) ?>" style="display:none"/>
    <input id="polygon" type="text" value='<?= Html::encode($cityCoords) ?>' style="display:none" />
    <div id="editMap" style="height: 590px; background: #ccc; margin-bottom: 30px;"></div>
</div>
