<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\setting\models\Republic;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\City */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="city-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'republic_id')->dropDownList(ArrayHelper::map(Republic::find()->select([
        'republic_id',
        'name',
    ])->all(), 'republic_id', 'name')) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'shortname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lat')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lon')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fulladdress')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fulladdress_reverse')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sort')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'search')->checkBox() ?>

    <?= $form->field($model, 'name_az')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name_de')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name_sr')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name_ro')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name_uz')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name_fi')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name_fa')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name_bg')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fulladdress_az')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'fulladdress_en')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'fulladdress_de')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'fulladdress_sr')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'fulladdress_ro')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'fulladdress_uz')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'fulladdress_fi')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'fulladdress_fa')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'fulladdress_bg')->textInput(['maxlength' => true]) ?>

    <h1>Полигон</h1>

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active js-field-map"><a href="#tab_map" data-toggle="tab">Карта</a></li>
            <li><a href="#tab_field" data-toggle="tab">Форма</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_map">
                <input type="hidden" id="city_id" value="<?= $model->city_id ?>">
                <input id="cityLat" type="text" value="<?= $model->lat ?>" style="display:none"/>
                <input id="cityLon" type="text" value="<?= $model->lon ?>" style="display:none"/>
                <input id="polygon" type="text" value='<?= $model->city_polygon ?>' style="display:none"/>
                <section id="editParkBlock">
                    <div id="editMap" style="height: 590px; background: #ccc; margin-bottom: 30px;"></div>
                </section>
            </div>
            <div class="tab-pane" id="tab_field">
                <?= $form->field($model, 'city_polygon')->textarea(['rows' => 6])->label('') ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить',
            ['id'=>'save','class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
