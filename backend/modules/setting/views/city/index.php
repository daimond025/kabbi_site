<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Города';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('geo')): ?>
    <p>
        <?= Html::a('Добавить город', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php endif; ?>
    <?php
    \yii\widgets\Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'has_polygon',
                'label'     => 'Есть ли полигон?',
                'filter'    => Html::activeDropDownList($searchModel, 'has_polygon',['2'=>'Все','0' => 'Нет', '1' => 'Да'], ['class'=>'form-control']),
                'value'     => function ($model) {
                    return $model->getPolygonValues();
                },
            ],
            'city_id',
            'name',
            'shortname',
            'lat',
            'lon',
            [
                'attribute' => 'country_name',
                'label'     => 'Страна',
                'value'     => function ($model) {
                    return $model->republic->country->name;
                },
            ],
            [
                'attribute' => 'republic_name',
                'label'     => 'Республика',
                'value' => function ($model) {
                    return $model->republic->name;
                },
            ],
            'search',
            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('geo'),
                    'delete' => \Yii::$app->user->can('geo'),
                ],
            ],
        ],
    ]);
    \yii\widgets\Pjax::end();
    ?>

</div>
