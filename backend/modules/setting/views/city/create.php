<?php

use yii\helpers\Html;
use backend\modules\setting\assets\CitypolygonAsset;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\City */

$bundle = CitypolygonAsset::register($this);
$file = (!empty($cityCoords) && ($cityCoords != 'undefined')) ? 'update' : 'create';
$this->registerJsFile($bundle->baseUrl . '/' . $file . '.js', ['depends' => 'yii\web\JqueryAsset']);

$this->title = 'Добавить город';
$this->params['breadcrumbs'][] = ['label' => 'Города', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    
</div>
