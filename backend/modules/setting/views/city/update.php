<?php

use yii\helpers\Html;
use backend\modules\setting\assets\CitypolygonAsset;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\City */

$this->title = 'Редактировать: ' . ' ' . $model->name;
$bundle = CitypolygonAsset::register($this);
$file = (!empty($cityCoords) && ($cityCoords != 'undefined')) ? 'update' : 'create';
$this->registerJsFile($bundle->baseUrl . '/' . $file . '.js', ['depends' => 'yii\web\JqueryAsset']);

$this->params['breadcrumbs'][] = ['label' => 'Города', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->city_id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="tab-content">
    <div class="city-update">

        <h1><?= Html::encode($this->title) ?></h1>

        <?=
        $this->render('_form', [
            'model' => $model,
        ])
        ?>

    </div>
    </div>
