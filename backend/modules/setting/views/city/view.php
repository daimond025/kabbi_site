<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\setting\assets\UpdatePlaceAsset;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\City */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Города', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$bundle = UpdatePlaceAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/mapEditor/update.js', ['depends' => 'yii\web\JqueryAsset']);
?>
<div class="tab-content">
    <div class="city-view">

        <h1><?= Html::encode($this->title) ?></h1>
        <?php if (Yii::$app->user->can('geo')): ?>
        <p>
            <?= Html::a('Редактировать', ['update', 'id' => $model->city_id], ['class' => 'btn btn-primary']) ?>
            <?=
            Html::a('Удалить', ['delete', 'id' => $model->city_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Подтверждаете удаление?',
                    'method' => 'post',
                ],
            ])
            ?>
        </p>
        <?php endif; ?>
        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                'city_id',
                'republic_id',
                'name',
                'shortname',
                'lat',
                'lon',
                'fulladdress',
                'fulladdress_reverse',
                'sort',
                'city_polygon',
            ],
        ])
        ?>
    </div>
    <?if(!empty($cityCoords)):?>
    <div class="parking-index">
        <h1>Полигон</h1>
        <input id="cityLat" type="text" value="<?= $model->lat ?>" style="display:none"/>
        <input id="cityLon" type="text" value="<?= $model->lon ?>" style="display:none"/>
        <input id="polygon" type="text" value='<?= $cityCoords ?>' style="display:none" />
        <div id="editMap" style="height: 590px; background: #ccc; margin-bottom: 30px;"></div>
        <?else:?>
        <?= 'У города не указан полигон' ?>
        <?endif;?>
    </div>
</div>

