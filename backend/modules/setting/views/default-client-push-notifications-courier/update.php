<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\DefaultClientPushNotificationsCourier */
/* @var $paramsMap string[] */

$this->title                   = 'Update Default Client Push Notifications Courier: ' . $model->template_id;
$this->params['breadcrumbs'][] = ['label' => 'Default Client Push Notifications Couriers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->template_id, 'url' => ['view', 'id' => $model->template_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="default-client-push-notifications-courier-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'     => $model,
        'paramsMap' => $paramsMap,
    ]) ?>

</div>
