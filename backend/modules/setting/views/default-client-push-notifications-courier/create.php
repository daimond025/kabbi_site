<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\DefaultClientPushNotificationsCourier */
/* @var $paramsMap string[] */

$this->title                   = 'Create Default Client Push Notifications Courier';
$this->params['breadcrumbs'][] = ['label' => 'Default Client Push Notifications Couriers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="default-client-push-notifications-courier-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'     => $model,
        'paramsMap' => $paramsMap,
    ]) ?>

</div>
