<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Default Client Push Notifications Couriers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="default-client-push-notifications-courier-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Default Client Push Notifications Courier', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            'template_id',
            'type',
            'text:ntext',
            [
                'attribute' => 'params',
                'content' => function ($model) {
                    return implode('<br>', $model->params);
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
