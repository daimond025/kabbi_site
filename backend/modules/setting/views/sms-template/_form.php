<?php

use common\modules\tenant\models\DefaultSmsTemplate;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model DefaultSmsTemplate */
/* @var $form yii\widgets\ActiveForm */
/* @var $paramsMap string[] */
?>

<div class="default-sms-template-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'position_id')->dropDownList($positionMap, ['disabled' => !$model->isNewRecord]) ?>

    <?= $form->field($model, 'type')->textInput([
        'maxlength' => 45,
        'disabled'  => !$model->isNewRecord,
    ]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'params')->checkboxList($paramsMap,[
        'separator' => '<br/>',
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
