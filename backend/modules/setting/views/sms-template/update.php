<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\DefaultSmsTemplate */
/* @var array $positionMap */
/* @var $paramsMap string[] */

$this->title = 'Редактировать';
$this->params['breadcrumbs'][] = ['label' => 'Шаблоны смс', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->template_id, 'url' => ['view', 'id' => $model->template_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="default-sms-template-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'       => $model,
        'positionMap' => $positionMap,
        'paramsMap'   => $paramsMap,
    ]) ?>

</div>
