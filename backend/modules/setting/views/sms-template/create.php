<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\DefaultSmsTemplate */
/* @var array $positionMap */
/* @var $paramsMap string[] */

$this->title = 'Добавить шаблон';
$this->params['breadcrumbs'][] = ['label' => 'Шаблоны смс', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="default-sms-template-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'       => $model,
        'positionMap' => $positionMap,
        'paramsMap'   => $paramsMap,
    ]) ?>

</div>
