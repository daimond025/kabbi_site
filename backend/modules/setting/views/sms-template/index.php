<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel \app\modules\setting\models\DefaultSmsTemplateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var array $positionMap */

$this->title                   = 'Шаблоны смс';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="default-sms-template-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('messageTemplates')): ?>
        <p>
            <?= Html::a('Добавить шаблон', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>
    <? Pjax::begin() ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [

            'template_id',
            [
                'attribute' => 'position_id',
                'label'     => 'Профессия',
                'value'     => function ($model) use ($positionMap) {
                    return isset($positionMap[$model->position_id]) ? $positionMap[$model->position_id] : null;
                },
                'filter'    => $positionMap,
            ],
            'type:ntext',
            'text',
            [
                'attribute' => 'params',
                'content' => function ($model) {
                    return implode('<br>', $model->params);
                },
            ],

            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('messageTemplates'),
                    'delete' => \Yii::$app->user->can('messageTemplates'),
                ],
            ],
        ],
    ]); ?>
    <? Pjax::end() ?>
</div>
