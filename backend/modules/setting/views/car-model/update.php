<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\CarModel */
/* @var array $carBrandList */
/* @var array $typeList */

$this->title = 'Редактировать модель: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Модели автомобилей', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->model_id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="car-model-update">

    <?= $this->render('_form', [
        'model' => $model,
        'carBrandList' => $carBrandList,
        'typeList'     => $typeList,
    ]) ?>

</div>
