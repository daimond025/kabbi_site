<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\CarModel */

$this->title = 'Модель: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Модели автомобилей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-model-view">
    <?php if (Yii::$app->user->can('transport')): ?>
    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->model_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->model_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php endif; ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'model_id',
            'brand.name',
            'name',
            [
                'attribute' => 'type.name',
                'label'     => 'Тип',
            ],
        ],
    ]) ?>

</div>
