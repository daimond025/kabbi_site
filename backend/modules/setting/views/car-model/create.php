<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\CarModel */
/* @var array $carBrandList */
/* @var array $typeList */

$this->title = 'Добавить модель';
$this->params['breadcrumbs'][] = ['label' => 'Модели автомобилей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-model-create">

    <?= $this->render('_form', [
        'model'        => $model,
        'carBrandList' => $carBrandList,
        'typeList'     => $typeList,
    ]) ?>

</div>
