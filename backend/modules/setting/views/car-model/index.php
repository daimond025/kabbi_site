<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\setting\models\CarModelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
//dd($dataProvider);
$this->title = 'Модели автомобилей';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-model-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php if (Yii::$app->user->can('transport')): ?>
    <p>
        <?= Html::a('Добавить модель', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php endif; ?>
    <?php Pjax::begin(); ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
            'model_id',
            [
                'attribute' => 'brand_id',
                'label'     => 'Марка',
                'value'     => function ($model) {
                    return $model->brand->name;
                },
            ],
            'name',
            [
                'attribute' => 'typeName',
                'label'     => 'Тип',
                'value'     => function ($model) {
                    return $model->type->name;
                },
            ],
            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('transport'),
                    'delete' => \Yii::$app->user->can('transport'),
                ],
            ],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>
