<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\DefaultClientPushNotifications */
/* @var array $positionMap */
/* @var $paramsMap string[] */

$this->title = 'Добавить шаблон PUSH-уведомления клиенту';
$this->params['breadcrumbs'][] = ['label' => 'Шаблоны PUSH-уведомлений клиенту', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="default-client-push-notifications-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'       => $model,
        'positionMap' => $positionMap,
        'paramsMap'   => $paramsMap,
    ]) ?>

</div>
