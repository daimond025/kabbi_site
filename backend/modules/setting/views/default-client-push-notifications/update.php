<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\DefaultClientPushNotifications */
/* @var array $positionMap */
/* @var $paramsMap string[] */

$this->title = 'Изменить шаблон PUSH-уведомления клиенту: ' . ' ' . $model->push_id;
$this->params['breadcrumbs'][] = ['label' => 'Шаблоны PUSH-уведомлений клиенту', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->push_id, 'url' => ['view', 'id' => $model->push_id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="default-client-push-notifications-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'       => $model,
        'positionMap' => $positionMap,
        'paramsMap'   => $paramsMap,
    ]) ?>

</div>
