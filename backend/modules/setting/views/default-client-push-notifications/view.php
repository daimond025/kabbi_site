<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\DefaultClientPushNotifications */
/* @var $paramsMap string[] */

$this->title = $model->push_id;
$this->params['breadcrumbs'][] = ['label' => 'Шаблоны PUSH-уведомлений клиенту', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="default-client-push-notifications-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->push_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->push_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Удалить этот шаблон?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'push_id',
            'text',
            'type',
            [
                'attribute' => 'params',
                'format' => 'html',
                'value' => function ($model) use ($paramsMap) {
                    $params = array_intersect_key($paramsMap, array_flip($model->params));
                    return implode('<br>', $params);
                },
            ],
        ],
    ]) ?>

</div>
