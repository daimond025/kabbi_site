<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\setting\models\DefaultClientPushNotificationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var array $positionMap */

$this->title                   = 'Шаблоны PUSH-уведомлений клиенту';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="default-client-push-notifications-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php if (Yii::$app->user->can('messageTemplates')): ?>
        <p>
            <?= Html::a('Добавить шаблон PUSH-уведомления клиенту', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>

    <? Pjax::begin() ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [

            'push_id',
            [
                'attribute' => 'position_id',
                'label'     => 'Профессия',
                'value'     => function ($model) use ($positionMap) {
                    return isset($positionMap[$model->position_id]) ? $positionMap[$model->position_id] : null;
                },
                'filter'    => $positionMap,
            ],
            'type:ntext',
            'text',
            [
                'attribute' => 'params',
                'content' => function ($model) {
                    return implode('<br>', $model->params);
                },
            ],

            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('messageTemplates'),
                    'delete' => \Yii::$app->user->can('messageTemplates'),
                ],
            ],
        ],
    ]); ?>
    <? Pjax::end() ?>
</div>
