<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\modules\tenant\models\DefaultClientPushNotification */
/* @var $form yii\widgets\ActiveForm */
/* @var array $positionMap */
/* @var $paramsMap string[] */
?>

<div class="default-client-push-notifications-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'position_id')->dropDownList($positionMap, ['disabled' => !$model->isNewRecord]) ?>

    <?= $form->field($model, 'type')->textInput(['disabled' => !$model->isNewRecord]) ?>

    <?= $form->field($model, 'text')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'params')->checkboxList($paramsMap,[
        'separator' => '<br/>',
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Изменить',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
