<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\DefaultSmsTemplateCourier */
/* @var $paramsMap string[] */

$this->title = 'Create Default Sms Template Courier';
$this->params['breadcrumbs'][] = ['label' => 'Default Sms Template Couriers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="default-sms-template-courier-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'     => $model,
        'paramsMap' => $paramsMap,
    ]) ?>

</div>
