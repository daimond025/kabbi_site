<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\DefaultSmsTemplateCourier */
/* @var $paramsMap string[] */

$this->title = 'Update Default Sms Template Courier: ' . $model->template_id;
$this->params['breadcrumbs'][] = ['label' => 'Default Sms Template Couriers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->template_id, 'url' => ['view', 'id' => $model->template_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="default-sms-template-courier-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'     => $model,
        'paramsMap' => $paramsMap,
    ]) ?>

</div>
