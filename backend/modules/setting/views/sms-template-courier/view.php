<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\DefaultSmsTemplateCourier */
/* @var $paramsMap string[] */

$this->title = $model->template_id;
$this->params['breadcrumbs'][] = ['label' => 'Default Sms Template Couriers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="default-sms-template-courier-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->template_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->template_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'template_id',
            'type',
            'text:ntext',
            [
                'attribute' => 'params',
                'format' => 'html',
                'value' => function ($model) use ($paramsMap) {
                    $params = array_intersect_key($paramsMap, array_flip($model->params));
                    return implode('<br>', $params);
                },
            ],
        ],
    ]) ?>

</div>
