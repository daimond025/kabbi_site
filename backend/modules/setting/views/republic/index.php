<?php

use app\modules\setting\models\Country;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \app\modules\setting\models\RepublicSearch */

$this->title = 'Республики';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="republic-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('geo')): ?>
    <p>
        <?= Html::a('Добавить республику', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php endif; ?>
    <?php
    \yii\widgets\Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'republic_id',
            [
                'attribute' => 'country_id',
                'label'     => 'Страна',
                'value'     => function ($model) {
                    return $model->country->name;
                },
                'filter'    => Country::getCountryList(),
            ],
            'name',
            'shortname',
            'timezone',

            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('geo'),
                    'delete' => \Yii::$app->user->can('geo'),
                ],
            ],
        ],
    ]);
    \yii\widgets\Pjax::end();
    ?>

</div>