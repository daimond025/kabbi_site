<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model \common\modules\city\models\Republic */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Республики', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="republic-view">

    <h1><?= Html::encode($this->title) ?></h1>
<?php if (Yii::$app->user->can('geo')): ?>
    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->republic_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->republic_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Подтверждаете удаление?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
<?php endif; ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'republic_id',
            'country_id',
            'name',
            'shortname',
            'timezone',
        ],
    ]) ?>