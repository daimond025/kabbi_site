<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model \common\modules\city\models\Republic */

$this->title = 'Create Republic';
$this->params['breadcrumbs'][] = ['label' => 'Republics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="republic-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>