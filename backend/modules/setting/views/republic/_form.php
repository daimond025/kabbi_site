<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\modules\city\models\Republic */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="republic-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'country_id')->dropDownList(\app\modules\setting\models\Country::getCountryList()) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 100]) ?>
    <?= $form->field($model, 'name_en')->textInput(['maxlength' => 100]) ?>
    <?= $form->field($model, 'name_az')->textInput(['maxlength' => 100]) ?>
    <?= $form->field($model, 'name_de')->textInput(['maxlength' => 100]) ?>
    <?= $form->field($model, 'name_sr')->textInput(['maxlength' => 100]) ?>
    <?= $form->field($model, 'name_uz')->textInput(['maxlength' => 100]) ?>
    <?= $form->field($model, 'name_fi')->textInput(['maxlength' => 100]) ?>
    <?= $form->field($model, 'name_fa')->textInput(['maxlength' => 100]) ?>
    <?= $form->field($model, 'name_bg')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'shortname')->dropDownList($model->getShortNameList()) ?>

    <?= $form->field($model, 'timezone')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Редактировать',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>