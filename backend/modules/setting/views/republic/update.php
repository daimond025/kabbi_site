<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \common\modules\city\models\Republic */

$this->title = 'Редактировать республику: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Республики', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->republic_id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="republic-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>