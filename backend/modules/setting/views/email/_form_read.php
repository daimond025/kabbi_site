<?php

use backend\modules\setting\helpers\EmailSettingHelper;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;

/**
 * @var \yii\web\View                                          $this
 * @var \backend\modules\setting\forms\DefaultEmailSettingForm $formModel
 */

?>

<div class="email-setting-form">

    <?= DetailView::widget([
        'model'      => $formModel,
        'attributes' => [
            [
                'attribute' => 'provider',
                'value'     => ArrayHelper::getValue(EmailSettingHelper::getProviderNames(), $formModel->provider),
            ],
            'senderName',
            'senderEmail',
            'senderPassword',
            'template',
        ],
    ]); ?>

</div>