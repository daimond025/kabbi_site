<?php

use backend\modules\setting\helpers\EmailSettingHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var \yii\web\View                                          $this
 * @var \backend\modules\setting\forms\DefaultEmailSettingForm $formModel
 */

?>

<div class="email-setting-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($formModel, 'provider')->dropDownList(EmailSettingHelper::getProviderNames()); ?>
    <?= $form->field($formModel, 'senderName')->textInput(['class' => 'form-control']); ?>
    <?= $form->field($formModel, 'senderEmail')->textInput(['class' => 'form-control']); ?>
    <?= $form->field($formModel, 'senderPassword')->textInput(['class' => 'form-control']); ?>
    <?= $form->field($formModel, 'template')->textarea(['class' => 'form-control', 'rows' => 18]); ?>

    <?php
    $params = [];
    foreach (EmailSettingHelper::getParamsMap() as $key => $value) {
        $params[] = Html::encode($key) . ' - ' . Html::encode($value);
    }
    ?>

    <div class="form-group">
        <?= implode('<br>', $params) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Редактировать', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>