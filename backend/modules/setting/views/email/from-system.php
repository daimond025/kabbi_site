<?php

use yii\helpers\Html;

/**
 * @var \yii\web\View                                          $this
 * @var \backend\modules\setting\forms\DefaultEmailSettingForm $formModel
 * @var boolean                                                $allowWrite
 */

$this->title                   = 'Редактирование настройки Email. Система -> Арендатор';
$this->params['breadcrumbs'][] = 'Настройки Email';
?>
<div class="email-setting-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render($allowWrite ? '_form' : '_form_read', compact('formModel')) ?>

</div>