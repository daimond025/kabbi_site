<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model \common\modules\car\models\transport\TransportType */

$this->title = 'Добавить тип';
$this->params['breadcrumbs'][] = ['label' => 'Тип транспорта', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transport-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
