<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \common\modules\car\models\transport\TransportType */

$this->title = 'Редактировать тип: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Тип транспорта', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->type_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="transport-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
