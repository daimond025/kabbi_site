<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model \common\modules\employee\models\position\PositionHasField */

$this->title = $model->position_id;
$this->params['breadcrumbs'][] = ['label' => 'Position Has Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="position-has-field-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'position_id' => $model->position_id, 'field_id' => $model->field_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'position_id' => $model->position_id, 'field_id' => $model->field_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'position_id',
            'field_id',
        ],
    ]) ?>

</div>
