<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \common\modules\employee\models\position\PositionHasField */

$this->title = 'Update Position Has Field: ' . $model->position_id;
$this->params['breadcrumbs'][] = ['label' => 'Position Has Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->position_id, 'url' => ['view', 'position_id' => $model->position_id, 'field_id' => $model->field_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="position-has-field-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'positions' => $positions,
        'fields'    => $fields,
    ]) ?>

</div>
