<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\modules\employee\models\position\PositionHasField */
/* @var $positions array */
/* @var $fields array */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="position-has-field-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'position_id')->dropDownList($positions) ?>

    <?= $form->field($model, 'field_id')->dropDownList($fields) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
