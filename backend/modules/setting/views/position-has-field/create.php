<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model \common\modules\employee\models\position\PositionHasField */

$this->title = 'Create Position Has Field';
$this->params['breadcrumbs'][] = ['label' => 'Position Has Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="position-has-field-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'positions' => $positions,
        'fields'    => $fields,
    ]) ?>

</div>
