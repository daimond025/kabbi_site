<?php

use app\modules\setting\models\UserDefaultRight;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $models app\modules\setting\models\UserDefaultRight[] */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-default-right-form">

    <?php
    $form = ActiveForm::begin();

    foreach ($models as $index => $model) {
        echo $form->field($model, "[$index]rights")->label($model->permission)->dropDownList(UserDefaultRight::getRightsMap());
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
