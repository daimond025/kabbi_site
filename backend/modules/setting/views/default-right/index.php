<?php

use app\modules\setting\models\UserDefaultRight;
use app\modules\setting\models\UserDefaultRightSearch;
use app\modules\setting\models\UserPosition;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel UserDefaultRightSearch */

$this->title = 'Права должностей';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if (Yii::$app->user->can('tenantRights')): ?>
    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php endif; ?>
<?php Pjax::begin(); ?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel'  => $searchModel,
    'columns'      => [
        'id',
        [
            'attribute' => 'position_id',
            'content'   => function ($model, $key, $index, $column) {
                return t('company-roles', $model->position->name);
            },
            'filter'    => UserPosition::getPositionMap(),
        ],
        'permission',
        [
            'attribute' => 'rights',
            'content'   => function ($model, $key, $index, $column) {
                return UserDefaultRight::getRightNameByKey($model->rights);
            },
            'filter'    => UserDefaultRight::getRightsMap(),
        ],
        [
            'class'          => 'yii\grid\ActionColumn',
            'template'       => '{view} {update}',
            'urlCreator'     => function ($action, $model, $key, $index, $actionColumn) {
                if ($action == 'update') {
                    return Url::to(['/setting/default-right/update', 'id' => $model->position_id]);
                }

                return Url::to(['/setting/default-right/' . $action, 'id' => $model->id]);
            },
            'visibleButtons' => [
                'update' => \Yii::$app->user->can('tenantRights'),
                'delete' => \Yii::$app->user->can('tenantRights'),
            ],
        ],
    ],
]); ?>
<?php Pjax::end(); ?>