<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\UserDefaultRight */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Права должностей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-default-right-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('tenantRights')): ?>
    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->position_id], ['class' => 'btn btn-primary']) ?>
    </p>
    <?php endif; ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'position.name',
            'permission',
            'rights',
        ],
    ]) ?>

</div>
