<?php

use app\modules\setting\models\UserPosition;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\UserDefaultRight */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Права должностей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-default-right-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="user-default-right-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'position_id')->dropDownList(UserPosition::getPositionMap()) ?>

        <?= $form->field($model, 'permission')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'rights')->dropDownList(['read' => 'Read', 'write' => 'Write', 'off' => 'Off',],
            ['prompt' => 'Выберите права']) ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить',
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
