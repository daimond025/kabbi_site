<?php

use yii\helpers\Html;
use app\modules\setting\models\UserPosition;

/* @var $this yii\web\View */
/* @var $positionName string */
/* @var $models app\modules\setting\models\UserDefaultRight[] */

$this->title = "Редактирование прав: $positionName";
$this->params['breadcrumbs'][] = ['label' => 'Права должностей', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="user-default-right-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'models' => $models,
    ]) ?>

</div>
