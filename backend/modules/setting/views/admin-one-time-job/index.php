<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\setting\models\AdminOneTimeJobSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Одноразовые работы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-one-time-job-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить одноразовую работу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php 
    \yii\widgets\Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'job_id',
            'name',
            'price',
            'active',
            'create_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); 
    \yii\widgets\Pjax::end();
    ?>

</div>
