<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\AdminOneTimeJob */

$this->title = 'Добавить одноразовую работу';
$this->params['breadcrumbs'][] = ['label' => 'Одноразовые работы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-one-time-job-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
