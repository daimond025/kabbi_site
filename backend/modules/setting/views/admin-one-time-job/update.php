<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\AdminOneTimeJob */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Одноразовые работы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->job_id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="admin-one-time-job-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
