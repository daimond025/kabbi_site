<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\setting\models\Tenant;

/* @var $this yii\web\View */
/* @var $extrinsicDispatcher app\modules\user\models\User */
/* @var $form yii\widgets\ActiveForm */
/* @var $lang array */
/* @var $tenants \common\modules\tenant\models\Tenant[] */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'dynamic-form'
        ]
    ]); ?>

    <?= $form->field($extrinsicDispatcher, 'password')->passwordInput(['maxlength' => 255]) ?>
    <?= $form->field($extrinsicDispatcher, 'password_repeat')->passwordInput(['maxlength' => 255]) ?>

    <?= $form->field($extrinsicDispatcher, 'name')->textInput(['maxlength' => 45]) ?>
    <?= $form->field($extrinsicDispatcher, 'last_name')->textInput(['maxlength' => 45]) ?>
    <?= $form->field($extrinsicDispatcher, 'second_name')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($extrinsicDispatcher, 'email')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($extrinsicDispatcher, 'phone')->textInput(['maxlength' => 15]) ?>
    <?= $form->field($extrinsicDispatcher, 'lang')->dropDownList($lang) ?>

    <?= $form->field($extrinsicDispatcher, 'domain')->widget(\yii\jui\AutoComplete::classname(), [
        'clientOptions' => [
            'source' => ArrayHelper::getColumn(Tenant::find()->orderBy('domain')->all(), 'domain'),
        ],
    ])->textInput() ?>


    <?= $this->render('_form_tenant_values', [
        'form' => $form,
        'tenants' => $tenants
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => $extrinsicDispatcher->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
