<?php

/* @var $extrinsicDispatcher app\modules\user\models\User */
/* @var $form yii\widgets\ActiveForm */
/* @var $tenants \common\modules\tenant\models\Tenant[] */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\JuiAsset;
use yii\web\JsExpression;
use kartik\widgets\FileInput;
use wbraganca\dynamicform\DynamicFormWidget;
use wbraganca\dynamicform\DynamicFormAsset;
use yii\helpers\ArrayHelper;
use app\modules\setting\models\Tenant;
?>

    <div id="panel-option-values" class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Связанные арендаторы</h3>
        </div>

        <?php DynamicFormWidget::begin([
            'widgetContainer' => 'dynamicform_wrapper',
            'widgetBody' => '.form-options-body',
            'widgetItem' => '.form-options-item',
            'min' => 1,
            'insertButton' => '.add-item',
            'deleteButton' => '.delete-item',
            'model' => $tenants[0],
            'formId' => 'dynamic-form',
            'formFields' => [
                'tenant_id'
            ]
        ]);?>

        <table class="table table-bordered table-striped margin-b-none">
            <thead>
            <tr>
                <th class="required">ID арендатора</th>
                <th style="width: 90px; text-align: center">Действия</th>
            </tr>
            </thead>
            <tbody class="form-options-body">
            <?php foreach ($tenants as $index => $tenant): ?>
                <tr class="form-options-item">
                    <td class="vcenter">
                        <?= $form->field($tenant, "[{$index}]tenant_id")->label(false)->textInput(['maxlength' => 128]); ?>
                    </td>
                    <td class="text-center vcenter">
                        <button type="button" class="delete-item btn btn-danger btn-xs"><i class="fa fa-minus"></i></button>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="1"></td>
                <td><button type="button" class="add-item btn btn-success btn-sm"><span class="fa fa-plus"></span> New</button></td>
            </tr>
            </tfoot>
        </table>
        <?php DynamicFormWidget::end(); ?>
    </div>

<?php

DynamicFormAsset::register($this);