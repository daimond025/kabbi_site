<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $extrinsicDispatcher app\modules\user\models\User */
/* @var $lang array */
/* @var $tenants \common\modules\tenant\models\Tenant[] */

$this->title = 'Добавить внешнего диспетчера';
$this->params['breadcrumbs'][] = ['label' => 'Внешние диспетчеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'extrinsicDispatcher' => $extrinsicDispatcher,
        'lang'  => $lang,
        'tenants' => $tenants
    ]); ?>

</div>
