<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $extrinsicDispatcher \backend\modules\setting\models\entities\UserExtrinsicDispatcher */
/* @var $tenants \app\modules\setting\models\Tenant[] */
/* @var $lang array */

$this->title = 'Редактировать внешнего диспетчера: ' . ' ' . Html::encode($extrinsicDispatcher->name);
$this->params['breadcrumbs'][] = ['label' => 'Внешние диспетчеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Html::encode($extrinsicDispatcher->name), 'url' => ['view', 'id' => $extrinsicDispatcher->user_id]];
$this->params['breadcrumbs'][] = 'Редактировать';

//echo $this->render('_nav', ['user_id' => $extrinsicDispatcher->user_id]);
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_updateForm', [
        'extrinsicDispatcher' => $extrinsicDispatcher,
        'tenants' => $tenants,
        'lang' => $lang,
    ]) ?>

</div>
