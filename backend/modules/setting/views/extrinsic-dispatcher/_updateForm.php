<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\modules\tenant\models\Tenant;

/* @var $this yii\web\View */
/* @var $extrinsicDispatcher \backend\modules\setting\models\entities\UserExtrinsicDispatcher */
/* @var $form yii\widgets\ActiveForm */
/* @var $tenants \app\modules\setting\models\Tenant[] */
/* @var $lang array */

?>

<div class="user-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'dynamic-form'
        ]
    ]); ?>

    <?= $form->field($extrinsicDispatcher, 'password_for_update')->passwordInput(['maxlength' => 255]) ?>

    <?= $form->field($extrinsicDispatcher, 'name')->textInput(['maxlength' => 45]) ?>
    <?= $form->field($extrinsicDispatcher, 'last_name')->textInput(['maxlength' => 45]) ?>
    <?= $form->field($extrinsicDispatcher, 'second_name')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($extrinsicDispatcher, 'email')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($extrinsicDispatcher, 'phone')->textInput(['maxlength' => 15]) ?>
    <?= $form->field($extrinsicDispatcher, 'lang')->dropDownList($lang) ?>

    <?= $form->field($extrinsicDispatcher, 'access_token')->textInput() ?>

    <?= $form->field($extrinsicDispatcher, 'domain')->widget(\yii\jui\AutoComplete::classname(), [
        'clientOptions' => [
            'source' => ArrayHelper::getColumn(Tenant::find()->orderBy('domain')->all(), 'domain'),
        ],
    ])->textInput() ?>


    <?= $this->render('_form_tenant_values', [
        'form' => $form,
        'tenants' => $tenants
    ]) ?>

    <?= $form->field($extrinsicDispatcher, 'active')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
