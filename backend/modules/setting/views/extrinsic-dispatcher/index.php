<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel \backend\modules\setting\models\search\UserExtrinsicDispatcherSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Внешние диспетчера';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php if (Yii::$app->user->can('extrinsicDispatcher')): ?>
        <p>
            <?= Html::a('Добавить внешнего диспетчера', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'user_id',
            'fullName',
            'email',
            'active:boolean',
            'create_time',

            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('extrinsicDispatcher'),
                    'delete' => \Yii::$app->user->can('extrinsicDispatcher'),
                ],
            ],
        ],
    ]); ?>

</div>
