<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SmsLog */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'СМС логи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-log-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'text',
            'status',
            'login',
            'phone',
            'create_time',
            'sender',
            'signature',
            'server_id',
        ],
    ]) ?>

</div>
