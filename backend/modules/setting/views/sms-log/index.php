<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SmsLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'СМС логи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sms-log-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'tenant_id',
            'text',
            'status',
            'login',
            'phone',
            'create_time',
            'signature',
            [
                'attribute' => 'response',
                'value'     => function ($model) {
                    if ($model->server_id == 1) {
                        $response = unserialize($model->response);

                        if ($response === false) {
                            return $model->response;
                        }

                        return !empty($response['description']) ? $response['description'] : $response;
                    } else {
                        return $model->response;
                    }
                },
            ],
            [
                'attribute' => 'server_name',
                'label'     => 'Название СМС сервера',
                'value'     => function ($model) {
                    return $model->server->name;
                },
            ],
            [
                'attribute' => 'tenant_domain',
                'label'     => 'Домен',
                'value'     => function ($model) {
                    return $model->tenant->domain;
                },
            ],
        ],
    ]);
    ?>

</div>
