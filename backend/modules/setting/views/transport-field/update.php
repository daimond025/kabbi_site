<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \common\modules\car\models\transport\TransportField */

$this->title = 'Редактировать поле: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Transport Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->field_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="transport-field-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
