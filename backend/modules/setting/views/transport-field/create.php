<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model \common\modules\car\models\transport\TransportField */

$this->title = 'Добавить поле';
$this->params['breadcrumbs'][] = ['label' => 'Transport Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transport-field-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
