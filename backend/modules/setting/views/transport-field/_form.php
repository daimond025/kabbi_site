<?php

use common\modules\car\models\transport\TransportField;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model TransportField */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transport-field-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type')->dropDownList([
        TransportField::INTEGER_TYPE => 'Int',
        TransportField::STRING_TYPE  => 'String',
        TransportField::TEXT_TYPE    => 'Text',
        TransportField::ENUM_TYPE    => 'Enum',
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
