<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\Holiday */

$this->title = 'Update Holiday: ' . ' ' . $model->holiday_id;
$this->params['breadcrumbs'][] = ['label' => 'Holidays', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->holiday_id, 'url' => ['view', 'id' => $model->holiday_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="holiday-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
