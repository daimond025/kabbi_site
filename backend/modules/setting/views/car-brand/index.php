<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\setting\models\CarBrandSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Марки автомобилей';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-brand-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php if (Yii::$app->user->can('transport')): ?>
        <p>
            <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
            'brand_id',
            'name',

            [
                'class'          => 'yii\grid\ActionColumn',
                'template'       => '{view}{update}{delete}',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('transport'),
                    'delete' => \Yii::$app->user->can('transport'),
                ],
                'buttons'        => [
                    'view'   => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::to(['view', 'id' => $model[brand_id]]);

                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
                    },
                    'update' => function ($url, $model, $key) {

                        $url = \yii\helpers\Url::to(['update', 'id' => $model[brand_id]]);

                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                    },
                    'delete' => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::to(['delete', 'id' => $model[brand_id]]);

                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [

                            'data' => [
                                'confirm' => 'Вы уверены, что хотите удалить?',
                                'method'  => 'post',
                            ],
                        ]);
                    },
                ],
            ],
        ],
    ]);
    ?>

</div>
