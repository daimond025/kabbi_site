<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \common\modules\car\models\CarBrand */

$this->title = 'Редактировать марку: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Марки автомобилей', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->brand_id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="car-brand-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
