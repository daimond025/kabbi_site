<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model \common\modules\car\models\CarBrand */

$this->title = 'Марка: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Марки автомобилей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-brand-view">
    <?php if (Yii::$app->user->can('transport')): ?>
    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->brand_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->brand_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php endif; ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'brand_id',
            'name',
        ],
    ]) ?>

</div>
