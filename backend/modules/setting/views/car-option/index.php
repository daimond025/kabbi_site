<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\setting\models\CarOptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Опции';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-option-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('transport')): ?>
        <p>
            <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
            'option_id',
            'name',
            [
                'attribute' => 'type_id',
                'value'     => 'type.name',
                'filter'    => Html::activeDropDownList($searchModel, 'type_id', $searchModel->getTransportType(),
                    ['class' => 'form-control', 'prompt' => 'All']),
            ],
            'sort',
            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('transport'),
                    'delete' => \Yii::$app->user->can('transport'),
                ],
            ],
        ],
    ]); ?>

</div>
