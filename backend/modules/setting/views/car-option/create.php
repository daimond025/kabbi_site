<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model \common\modules\car\models\CarOption */

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Опции', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-option-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'typeMap'  => $typeMap,
    ]) ?>

</div>
