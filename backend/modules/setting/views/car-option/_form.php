<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\modules\setting\models\Message;

/* @var $this yii\web\View */
/* @var $model \common\modules\car\models\CarOption */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-option-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?$languageVariants = Message::getLanguageVariants();?>
    <? foreach ($languageVariants as $language => $translate): ?>
        <div class="form-group">
            <?= Html::label($translate) ?>
            <?$value = isset($_POST['translate'][$language]) ? $_POST['translate'][$language] : (isset($translates[$language]) ? $translates[$language]->translation : '')?>
            <?= Html::textInput('translate[' . $language . ']', $value, ['class' => 'form-control']) ?>
        </div>
    <? endforeach; ?>

    <?= $form->field($model, 'type_id')->dropDownList($typeMap) ?>
    <?= $form->field($model, 'sort')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
