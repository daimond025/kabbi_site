<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \common\modules\car\models\CarOption */

$this->title = 'Редактировать: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Опции', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->option_id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="car-option-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'translates' => $translates,
        'typeMap'  => $typeMap,
    ]) ?>

</div>
