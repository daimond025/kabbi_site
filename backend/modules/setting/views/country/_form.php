<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\modules\city\models\Country */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="country-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'name_en')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'name_az')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'name_de')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'name_sr')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'name_ro')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'name_uz')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'name_fi')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'name_fa')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'name_bg')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>