<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tenant Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tenant-setting-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_search', ['model' => $searchModel]); ?>
    <?php if (Yii::$app->user->can('tenantSettings')): ?>
        <p>
            <?= Html::a('Create Tenant Setting', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'setting_id',
            'tenant_id',
            'name',
            'value:ntext',
            'type',

            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('tenantSettings'),
                    'delete' => \Yii::$app->user->can('tenantSettings'),
                ],
            ],
        ],
    ]); ?>

</div>
