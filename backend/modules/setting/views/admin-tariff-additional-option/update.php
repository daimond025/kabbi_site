<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\AdminTariffAdditionalOption */

$this->title = 'Редактировать доп. опцию: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Доп. опции', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->option_id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="admin-tariff-additional-option-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
