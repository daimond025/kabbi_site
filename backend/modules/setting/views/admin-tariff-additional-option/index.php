<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\setting\models\AdminTariffAdditionalOptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Доп. опции';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-tariff-additional-option-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить доп. опцию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php 
    \yii\widgets\Pjax::begin();
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'option_id',
            'name',
            'price',
            'active',
            'create_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); 
    \yii\widgets\Pjax::end();
    ?>

</div>
