<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\AdminTariffAdditionalOption */

$this->title = 'Добавить доп. опцию';
$this->params['breadcrumbs'][] = ['label' => 'Доп. опции', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-tariff-additional-option-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
