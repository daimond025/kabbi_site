<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\AdminTariffAdditionalOption */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Доп. опции', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-tariff-additional-option-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->option_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->option_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Подтверждаете удаление?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'option_id',
            'name',
            'price',
            'active',
            'create_date',
        ],
    ]) ?>

</div>
