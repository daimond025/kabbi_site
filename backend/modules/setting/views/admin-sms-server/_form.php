<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\setting\models\SmsServer;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\AdminSmsServer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-sms-server-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'server_id')->dropDownList(ArrayHelper::map(SmsServer::find()->all(), 'server_id', 'name')) ?>

    <?= $form->field($model, 'login')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'sign')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'active')->dropDownList([1 => 'Да', 0 => 'Нет']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
