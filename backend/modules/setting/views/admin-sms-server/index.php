<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
use app\modules\setting\assets\SettingAsset;
$bundle = SettingAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/smsServer.js', ['depends' => 'yii\web\JqueryAsset']);
$this->title = 'В работе';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="balance_roller">
    <h3>Проверить баланс</h3>
    <div>
        <p>
            <?=
            Html::dropDownList('tariff_id', 1, $serverList, ['class' => 'form-control dropdown-toggle', 'id' => 'server_id', 'style' => 'width: 20%', 'selected' => 1])
            ?>
        </p>
        <div style="display: inline-block; width: 40%">
            <button type="button" id="checkBalance" style="display: inline; width: 49%" class="btn btn-success btn-sm">Текущий баланс</button>
        </div>
        <p>
            <label id="result"></label>
        </p>
    </div>


</div>

<div class="admin-sms-server-index">
    <?if($dataProvider->count < 2):?>
    <p>
        <?= Html::a('Добавить в работу', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?endif?>
    <?php yii\widgets\Pjax::begin(); ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'server_id',
            'login',
            'password',
            'sign',
            'active',
            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('sms'),
                    'delete' => \Yii::$app->user->can('sms'),
                ],
            ],
        ],
    ]);
    ?>
<?php yii\widgets\Pjax::end(); ?>
</div>
