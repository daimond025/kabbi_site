<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\AdminSmsServer */

$this->title = 'Добавить в работу';
$this->params['breadcrumbs'][] = ['label' => 'В работе', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-sms-server-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
