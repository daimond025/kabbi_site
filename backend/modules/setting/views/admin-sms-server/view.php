<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\AdminSmsServer */

$this->title = 'Сервер: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'В работе', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-sms-server-view">
    <?php if (Yii::$app->user->can('sms')): ?>
    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php endif; ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'server_id',
            'login',
            'password',
            'active',
        ],
    ]) ?>

</div>
