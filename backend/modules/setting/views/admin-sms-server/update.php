<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\setting\models\AdminSmsServer */

$this->title = 'Редактировать сервер:' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'В работе', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="admin-sms-server-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
