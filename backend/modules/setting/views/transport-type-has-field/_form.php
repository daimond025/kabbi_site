<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\modules\car\models\transport\TransportTypeHasField */
/* @var $form yii\widgets\ActiveForm */
/* @var array $fieldMap */
/* @var array $typeMap */
?>

<div class="transport-type-has-field-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type_id')->dropDownList($typeMap) ?>

    <?= $form->field($model, 'field_id')->dropDownList($fieldMap) ?>

    <?= $form->field($model, 'enumList')->textInput(['placeholder' => 'до 1,5 тонн; до 3 тонн; до 5 тонн'])->label('Варианты выбора') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
