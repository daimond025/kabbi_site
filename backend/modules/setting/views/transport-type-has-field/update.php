<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \common\modules\car\models\transport\TransportTypeHasField */
/* @var array $fieldMap */
/* @var array $typeMap */

$this->title = 'Редактировать привязку к типу: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Transport Type Has Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="transport-type-has-field-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'typeMap'  => $typeMap,
        'fieldMap' => $fieldMap,
    ]) ?>

</div>
