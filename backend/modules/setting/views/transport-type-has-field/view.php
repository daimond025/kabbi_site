<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model \common\modules\car\models\transport\TransportTypeHasField */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Transport Type Has Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transport-type-has-field-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('transport')): ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php endif; ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'field.name',
                'label'     => 'Поле',
            ],
            [
                'attribute' => 'field.type',
                'label'     => 'Тип поля',
            ],
            [
                'attribute' => 'enumList',
                'label'     => 'Варинты значений',
            ],
        ],
    ]) ?>

</div>
