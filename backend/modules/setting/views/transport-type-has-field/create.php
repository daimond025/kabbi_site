<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model \common\modules\car\models\transport\TransportTypeHasField */
/* @var array $fieldMap */
/* @var array $typeMap */

$this->title = 'Добавить привязку поля к типу';
$this->params['breadcrumbs'][] = ['label' => 'Привязка полей к типу', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transport-type-has-field-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'    => $model,
        'typeMap'  => $typeMap,
        'fieldMap' => $fieldMap,
    ]) ?>

</div>
