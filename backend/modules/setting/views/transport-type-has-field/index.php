<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Привязка полей к типу';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transport-type-has-field-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('transport')): ?>
    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php endif; ?>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'transportTypeName',
                'label'     => 'Тип транспорта',
            ],
            [
                'attribute' => 'fieldName',
                'label'     => 'Поле',
            ],
            [
                'attribute' => 'fieldType',
                'label'     => 'Тип поля',
            ],
            [
                'attribute' => 'enumList',
                'label'     => 'Варинты значений',
            ],
            [
                'class'          => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => \Yii::$app->user->can('transport'),
                    'delete' => \Yii::$app->user->can('transport'),
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
