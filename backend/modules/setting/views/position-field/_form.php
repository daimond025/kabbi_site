<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\employee\models\position\PositionField;

/* @var $this yii\web\View */
/* @var $model \common\modules\employee\models\position\PositionField */
/* @var $form yii\widgets\ActiveForm */
$type_list = PositionField::getTypeList();
?>

<div class="position-field-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'type')->dropDownList(array_combine($type_list, $type_list)) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
