<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model \common\modules\employee\models\position\PositionField */

$this->title = 'Create Position Field';
$this->params['breadcrumbs'][] = ['label' => 'Position Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="position-field-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
