<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model \common\modules\employee\models\position\PositionField */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Position Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="position-field-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('employee')): ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->field_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->field_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php endif ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'field_id',
            'name',
            'type',
        ],
    ]) ?>

</div>
