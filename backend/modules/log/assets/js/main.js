;(function () {
    'use strict';

    var app = {

        changeStatusSelector: '.js-status',
        oldStatusAttribute: 'js-old-status',

        init: function () {

            this.registerChangeStatus();

        },

        registerChangeStatus: function () {

            this.onChangeStatus();

        },

        onChangeStatus: function () {

            var _this = this;

            $(document).on('change', this.changeStatusSelector, function () {

                var transactionId = $(this).data('transactionId');
                var status = $(this).val();

                _this.changeStatus(transactionId, status, this);
            });
        },

        changeStatus: function (transactionId, status, element) {

            var _this = this;

            $.ajax({
                type: "POST",
                url: "update-status-pay-net-cancel-transaction",
                data: {
                    transactionId: transactionId,
                    status: status
                },
                beforeSend: function () {
                    $(element).attr('disabled', true);
                },
                success: function (response) {

                    var newStatus = response ? response : $(element).attr(_this.oldStatusAttribute);
                    $(element).val(newStatus);
                    $(element).attr(_this.oldStatusAttribute, newStatus);

                },
                complete: function () {
                    $(element).attr('disabled', false);
                }
            });
        },
    };

    app.init();

})();