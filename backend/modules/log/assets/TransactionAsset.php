<?php

namespace backend\modules\log\assets;

use yii\web\AssetBundle;

class TransactionAsset extends AssetBundle
{
    public function init()
    {
        parent::init();

        $this->sourcePath     = '@backend/modules/log/assets';
        $this->publishOptions = ['forceCopy' => true];
        $this->js             = ['js/main.js'];
        $this->depends        = ['yii\web\JqueryAsset'];
    }
}