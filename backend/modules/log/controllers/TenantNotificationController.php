<?php

namespace backend\modules\log\controllers;

use backend\modules\log\models\TenantNotificationLog;
use backend\modules\log\models\TenantNotificationLogSearch;
use Yii;
use yii\web\Controller;

/**
 * Tenant notification controller
 */
class TenantNotificationController extends Controller
{

    public function actionIndex()
    {
        $searchModel  = new TenantNotificationLogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->sort = [
            'defaultOrder' => [
                'created_at' => SORT_DESC,
            ],
        ];

        return $this->render('index',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
    }

    public function actionView($id)
    {
        $model = TenantNotificationLog::findone($id);

        return $this->render('view', compact('model'));
    }
}
