<?php

namespace backend\modules\log\controllers;

use backend\modules\log\models\transaction\PayNetCancelTransaction;
use backend\modules\log\models\transaction\TransactionSearch;
use backend\modules\log\models\transaction\TransactionService;
use yii\filters\AccessControl;
use yii\web\Controller;

class TransactionLogController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex($start = null, $end = null)
    {

        $paymentList  = TransactionService::getPaymentList();
        $searchModel  = new TransactionSearch();
        $dataProvider = $searchModel->search(app()->request->get(), $start, $end);

        return $this->render('index', compact('paymentList', 'searchModel', 'dataProvider'));
    }

    public function actionUpdateStatusPayNetCancelTransaction()
    {

        if (!app()->request->isAjax) {
            return false;
        }

        $transactionId = post('transactionId');
        $status        = post('status');

        /** @var PayNetCancelTransaction $payNetCancelTransaction */
        $payNetCancelTransaction = PayNetCancelTransaction::find()->where(['transaction_id' => $transactionId])->one();

        if (!$payNetCancelTransaction) {
            return false;
        }

        if ($status != $payNetCancelTransaction->status) {
            $payNetCancelTransaction->status = $status;
            return $payNetCancelTransaction->save() ? $status : false;
        }

        return $status;
    }
}
