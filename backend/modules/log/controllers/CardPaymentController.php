<?php

namespace backend\modules\log\controllers;

use Yii;
use yii\web\Controller;
use backend\modules\log\models\CardPaymentLog;
use backend\modules\log\models\CardPaymentLogSearch;

/**
 * Card payment controller
 */
class CardPaymentController extends Controller
{

    public function actionIndex()
    {
        $searchModel  = new CardPaymentLogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->sort = [
            'defaultOrder' => [
                'created_at' => SORT_DESC
            ],
        ];

        return $this->render('index',
                [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $model = CardPaymentLog::findone($id);

        return $this->render('view', compact('model'));
    }
}
