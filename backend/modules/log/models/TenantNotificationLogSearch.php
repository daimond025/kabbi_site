<?php

namespace backend\modules\log\models;

use Yii;
use yii\data\ActiveDataProvider;
use app\modules\setting\models\Tenant;

/**
 * TenantNotificationLogSearch represents the model behind the search form about
 * `backend\modules\log\models\TenantNotificationLog`.
 */
class TenantNotificationLogSearch extends TenantNotificationLog
{

    public $domain;

    public function rules()
    {
        return [
            [['tenant_id'], 'integer'],
            [['type', 'reason', 'content'], 'string'],
            [['to', 'domain'], 'string', 'max' => 255],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TenantNotificationLog::find();
        $query->with(['tenant']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0 = 1');
            return $dataProvider;
        }

        if (!empty($this->domain)) {
            $query->andWhere([
                'in',
                'tenant_id',
                Tenant::find()->select('tenant_id')
                    ->where(['like', 'domain', $this->domain]),
            ]);
        }

        if (!empty($params['start']) && !empty($params['end'])) {
            $query->andWhere([
                'between',
                'created_at',
                strtotime($params['start']),
                strtotime($params['end'] . ' +1day - 1second'),
            ]);
        }

        $query->andFilterWhere([
            'type'   => $this->type,
            'reason' => $this->reason,
        ]);

        $query->andFilterWhere(['like', 'to', $this->to]);
        $query->andFilterWhere(['like', 'content', $this->content]);

        return $dataProvider;
    }
}
