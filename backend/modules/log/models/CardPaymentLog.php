<?php

namespace backend\modules\log\models;

use Yii;
use app\modules\setting\models\Tenant;

/**
 * This is the model class for table "{{%card_payment_log}}".
 *
 * @property integer $id
 * @property integer $tenant_id
 * @property integer $client_id
 * @property integer $worker_id
 * @property integer $order_id
 * @property string $profile
 * @property string $type
 * @property string $params
 * @property string $response
 * @property integer $result
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Order $order
 */
class CardPaymentLog extends \yii\db\ActiveRecord
{

    const TYPE_PAY = 'PAY';
    const TYPE_REFUND = 'REFUND';
    const TYPE_ADD_CARD = 'ADD CARD';
    const TYPE_CHECK_CARD = 'CHECK CARD';
    const TYPE_REMOVE_CARD = 'REMOVE CARD';

    const RESULT_OK = 1;
    const RESULT_ERROR = 0;

    /**
     * Return types of log
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_PAY,
            self::TYPE_REFUND,
            self::TYPE_ADD_CARD,
            self::TYPE_CHECK_CARD,
            self::TYPE_REMOVE_CARD,
        ];
    }

    /**
     * Getting names of result
     * @return type
     */
    public static function getResultNames()
    {
        return [
            self::RESULT_ERROR => 'Error',
            self::RESULT_OK    => 'OK',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%card_payment_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'type', 'result'], 'required'],
            [['tenant_id', 'client_id', 'worker_id', 'order_id', 'result'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['profile', 'type', 'params', 'response'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'tenant_id'  => 'Tenant ID',
            'client_id'  => 'Client ID',
            'worker_id'  => 'Worker ID',
            'order_id'   => 'Order ID',
            'type'       => 'Type',
            'profile'    => 'Profile',
            'params'     => 'Params',
            'response'   => 'Response',
            'result'     => 'Result',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }
}
