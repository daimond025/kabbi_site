<?php

namespace backend\modules\log\models;

use Yii;
use yii\data\ActiveDataProvider;
use backend\modules\log\models\CardPaymentLog;
use app\modules\setting\models\Tenant;

/**
 * CardPaymentLogSearch represents the model behind the search form about
 * `backend\modules\landing\models\CardPaymentLog`.
 */
class CardPaymentLogSearch extends CardPaymentLog
{

    public $domain;

    public function rules()
    {
        return [
            [['id', 'client_id', 'worker_id', 'order_id', 'result'], 'integer'],
            [['domain', 'type'], 'string'],
            [['params', 'response'], 'string'],
            [['domain', 'params', 'response'], 'trim'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CardPaymentLog::find();
        $query->with(['tenant']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        //        dd($this->toArray());
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!empty($this->domain)) {
            $query->andWhere([
                'in',
                'tenant_id',
                Tenant::find()->select('tenant_id')
                    ->where(['like', 'domain', $this->domain]),
            ]);
        }


        if (!empty($params['start']) && !empty($params['end'])) {
            $query->andWhere([
                'between',
                'created_at',
                strtotime($params['start']),
                strtotime($params['end']),
            ]);
        }

        $query->andFilterWhere([
            'id'        => $this->id,
            'client_id' => $this->client_id,
            'worker_id' => $this->worker_id,
            'order_id'  => $this->order_id,
            'type'      => CardPaymentLog::getTypes()[$this->type],
            'result'    => $this->result,
        ]);

        $query->andFilterWhere(['like', 'params', $this->params]);
        $query->andFilterWhere(['like', 'response', $this->response]);

        return $dataProvider;
    }
}
