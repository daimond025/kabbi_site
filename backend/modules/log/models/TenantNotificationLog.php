<?php

namespace backend\modules\log\models;

use app\modules\setting\models\Tenant;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tbl_tenant_notification_log".
 *
 * @property integer $id
 * @property integer $tenant_id
 * @property string  $type
 * @property string  $reason
 * @property string  $to
 * @property string  $content
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Tenant  $tenant
 */
class TenantNotificationLog extends ActiveRecord
{
    const TYPE_EMAIL = 'EMAIL';
    const TYPE_SMS = 'SMS';

    const REASON_ADVANCE_NOTICE = 'ADVANCE_NOTICE';
    const REASON_TARIFF_EXPIRED_NOTICE = 'TARIFF_EXPIRED_NOTICE';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_tenant_notification_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'created_at', 'updated_at'], 'integer'],
            [['type', 'reason'], 'required'],
            [['type', 'reason', 'content'], 'string'],
            [['to'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'tenant_id'  => 'Tenant ID',
            'type'       => t('tenant-notification', 'Type'),
            'reason'     => t('tenant-notification', 'Reason'),
            'to'         => t('tenant-notification', 'To'),
            'content'    => t('tenant-notification', 'Content'),
            'created_at' => t('tenant-notification', 'Created At'),
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_EMAIL => 'Email',
            self::TYPE_SMS   => 'SMS',
        ];
    }

    /**
     * @return array
     */
    public static function getReasons()
    {
        return [
            self::REASON_ADVANCE_NOTICE        => t('tenant-notification', 'Advance notice'),
            self::REASON_TARIFF_EXPIRED_NOTICE => t('tenant-notification', 'Tariff expired'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }
}
