<?php

namespace backend\modules\log\models\transaction;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%payment_method}}".
 *
 * @property string $payment
 * @property string $name
 *
 */
class PaymentMethodRecord extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment_method}}';
    }
}
