<?php

namespace backend\modules\log\models\transaction;

use yii\data\ActiveDataProvider;

class TransactionSearch extends Transaction
{

    public $status;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_method', 'terminal_transact_number', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'payment_method'           => 'Терминал',
            'cityName'                 => 'Филиал',
            'sum'                      => 'Сумма',
            'status'                   => 'Статус',
            'date'                     => 'Дата',
            'sender'                   => 'Объект',
            'terminal_transact_number' => 'Номер транзакции',
        ];
    }

    public function search($params, $start = null, $end = null)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'defaultOrder' => [
                    'date' => SORT_DESC,
                ],
            ],
        ]);
        $query->alias('t');
        $query->joinWith(['payNetCancelTransaction pn']);

        $query->where([
            'payment_method' => PaymentMethod::getTerminalPaymentList(),
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($start && $end) {
            $endDate = "$end 23:59:59";
            $query->andWhere('date >= :start', ['start' => $start])
                ->andWhere('date <= :end', ['end' => $endDate]);
        }

        $query->andFilterWhere([
            't.payment_method' => $this->payment_method,
            'pn.status'        => $this->status,
        ]);

        $query->andFilterWhere(['like', 't.terminal_transact_number', $this->terminal_transact_number, false]);

        return $dataProvider;
    }
}