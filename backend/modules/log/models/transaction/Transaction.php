<?php

namespace backend\modules\log\models\transaction;

use common\modules\billing\models\Account;
use common\modules\city\models\City;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%transaction}}".
 *
 * @property integer                 $transaction_id
 * @property integer                 $type_id
 * @property integer                 $sender_acc_id
 * @property integer                 $receiver_acc_id
 * @property string                  $payment_method
 * @property string                  $date
 * @property string                  $comment
 * @property integer                 $user_created
 * @property integer                 $city_id
 * @property string                  $terminal_transact_number
 * @property string                  $terminal_transact_date
 * @property integer                 $order_id
 * @property float                   $sum
 *
 * @property City                    $city
 * @property PayNetCancelTransaction $payNetCancelTransaction
 * @property Account                 $account
 */
class Transaction extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transaction}}';
    }

    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    public function getPayNetCancelTransaction()
    {
        return $this->hasOne(PayNetCancelTransaction::className(), ['transaction_id' => 'terminal_transact_number']);
    }

    public function getAccount()
    {
        return $this->hasOne(Account::className(), ['account_id' => 'sender_acc_id']);
    }
}
