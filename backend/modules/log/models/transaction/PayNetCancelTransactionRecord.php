<?php

namespace backend\modules\log\models\transaction;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%paynet_cancel_transaction}}".
 *
 * @property integer $id
 * @property integer $tenant_id
 * @property string  $transaction_id
 * @property integer $status
 * @property integer $create_time
 *
 */
class PayNetCancelTransactionRecord extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%paynet_cancel_transaction}}';
    }
}
