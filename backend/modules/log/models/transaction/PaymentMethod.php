<?php

namespace backend\modules\log\models\transaction;


class PaymentMethod extends PaymentMethodRecord
{
    const TERMINAL_ELECSNET = 'TERMINAL_ELECSNET';
    const TERMINAL_MILLION = 'TERMINAL_MILLION';
    const TERMINAL_PAYNET = 'TERMINAL_PAYNET';

    public static function getTerminalPaymentList()
    {
        return [
            static::TERMINAL_ELECSNET,
            static::TERMINAL_MILLION,
            static::TERMINAL_PAYNET,
        ];
    }

    public static function findAllTerminal()
    {
        $model = self::find()
            ->where([
                'payment' => self::getTerminalPaymentList(),
            ])
            ->all();

        return $model;
    }
}
