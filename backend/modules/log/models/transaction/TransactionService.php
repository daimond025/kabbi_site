<?php
namespace backend\modules\log\models\transaction;

use yii\helpers\ArrayHelper;

class TransactionService
{

    public static function getPaymentList()
    {
        $models = PaymentMethod::findAllTerminal();

        return ArrayHelper::map($models, 'payment', 'name');
    }
}