<?php

namespace backend\modules\log\models\transaction;


class PayNetCancelTransaction extends PayNetCancelTransactionRecord
{
    const STATUS_NEW = 0;
    const STATUS_DONE = 1;

    public static function getStatusList()
    {
        return array_keys(static::getStatusMap());
    }

    public static function getStatusMap()
    {
        return [
            static::STATUS_NEW  => 'Ожидает отмены',
            static::STATUS_DONE => 'Отменен',
        ];
    }
}
