<?php

use backend\modules\log\models\TenantNotificationLog;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model TenantNotificationLog */

$this->title                   = t('tenant-notification', 'Tenant notification ({id})', ['id' => $model->id]);
$this->params['breadcrumbs'][] = ['label' => t('tenant-notification', 'Tenant notificaions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            'created_at:datetime',
            [
                'attribute' => 'type',
                'value'     => Html::encode(TenantNotificationLog::getTypes()[$model->type]),
            ],
            [
                'label'     => t('tenant', 'Tenant'),
                'attribute' => 'domain',
                'format'    => 'raw',
                'value'     => Html::a(Html::encode($model->tenant->domain),
                    [
                        '/setting/tenant/view',
                        'id' => $model->tenant_id,
                    ]),
            ],
            [
                'attribute' => 'reason',
                'value'     => Html::encode(TenantNotificationLog::getReasons()[$model->reason]),
            ],
            'to',
            [
                'attribute' => 'content',
                'format'    => 'raw',
                'value'     => Html::tag('pre', Html::encode($model->content)),
            ],
        ],
    ]);

    ?>

</div>
