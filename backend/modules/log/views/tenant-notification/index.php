<?php

use app\modules\setting\assets\SettingAsset;
use backend\modules\log\models\TenantNotificationLog;
use backend\modules\log\models\TenantNotificationLogSearch;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;

$bundle = SettingAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/log-widgets.js',
    ['depends' => 'backend\assets\DateRangePickerAsset']);

/* @var $this yii\web\View */
/* @var $searchModel TenantNotificationLogSearch */
/* @var $dataProvider ActiveDataProvider */

$this->title                   = t('tenant-notification', 'Tenant notifications');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="tenant-notification-index">

    <div class="input-group">
        <button type="button" class="btn btn-default pull-right" id="daterange">
            <i class="fa fa-calendar"></i> <?= t('tenant-notification', 'Choose period') ?>
            <i class="fa fa-caret-down"></i>
        </button>
    </div>

    <h1><?= Html::encode($this->title) ?></h1>

    <?php yii\widgets\Pjax::begin(); ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
            'created_at:datetime',
            [
                'attribute' => 'type',
                'filter'    => TenantNotificationLog::getTypes(),
                'value'     => function ($model, $key, $index, $column) {
                    return Html::encode(TenantNotificationLog::getTypes()[$model->type]);
                },
            ],
            [
                'label'     => t('tenant', 'Tenant'),
                'attribute' => 'domain',
                'format'    => 'raw',
                'value'     => function ($model, $key, $index, $column) {
                    return Html::a(Html::encode($model->tenant->domain),
                        ['/setting/tenant/view', 'id' => $model->tenant_id]);
                },
            ],
            [
                'attribute' => 'reason',
                'filter'    => TenantNotificationLog::getReasons(),
                'value'     => function ($model, $key, $index, $column) {
                    return Html::encode(TenantNotificationLog::getReasons()[$model->reason]);
                },
            ],
            'to',
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]);

    ?>
    <?php yii\widgets\Pjax::end(); ?>

</div>
