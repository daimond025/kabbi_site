<?php
/**
 * @var \backend\modules\log\models\transaction\TransactionSearch $model
 */

use yii\helpers\Html;

if ($model->account) {

    switch ($model->account->acc_kind_id) {
        case 2:
            if ($model->account->worker) {
                echo Html::encode($model->account->worker->getFullName());
                echo " ({$model->account->worker->callsign})";
            }
    }
}