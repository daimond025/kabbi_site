<?php

use yii\helpers\Html;
use yii\grid\GridView;
/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $searchModel \backend\modules\log\models\transaction\TransactionSearch
 * @var $paymentList array
 **/

use app\modules\setting\assets\SettingAsset;
use yii\helpers\ArrayHelper;
use backend\modules\log\models\transaction\TransactionSearch;
use backend\modules\log\models\transaction\PayNetCancelTransaction;
use backend\modules\log\assets\TransactionAsset;

TransactionAsset::register($this);
$bundle = SettingAsset::register($this);

$this->registerJsFile($bundle->baseUrl . '/log-widgets.js', ['depends' => 'backend\assets\DateRangePickerAsset']);
$this->title                   = 'Транзакции';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="input-group">
    <button type="button" class="btn btn-default pull-right" id="daterange">
        <i class="fa fa-calendar"></i> Выбрать период
        <i class="fa fa-caret-down"></i>
    </button>
</div>


<div class="elecsnet-log-index">
    <h1><?= Html::encode($this->title) ?></h1>
</div>

<?php yii\widgets\Pjax::begin(); ?>
<section id="payments">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            [
                'class'         => 'yii\grid\SerialColumn',
                'headerOptions' => ['style' => 'width:60px'],
            ],
            [
                'attribute' => 'terminal_transact_number',
            ],
            [
                'attribute' => 'payment_method',
                'headerOptions' => ['style' => 'width:110px'],
                'filter'    => $paymentList,
                'content'   => function (TransactionSearch $model) use ($paymentList) {
                    return ArrayHelper::getValue($paymentList, $model->payment_method);
                },
            ],
            [
                'attribute' => 'sum',
                'headerOptions' => ['style' => 'width:130px'],
            ],
            [
                'attribute' => 'sender',
                'content'   => function (TransactionSearch $model) {
                    return $this->render('_sender', compact('model'));
                },
            ],
            [
                'attribute' => 'status',
                'filter'    => PayNetCancelTransaction::getStatusMap(),
                'content'   => function (TransactionSearch $model) {
                    return $this->render('_status', compact('model'));
                },
            ],
            [
                'attribute' => 'date',
            ],
        ],
    ]); ?>
</section>

<?php yii\widgets\Pjax::end(); ?>

