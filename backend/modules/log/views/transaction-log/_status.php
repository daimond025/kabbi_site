<?php
/**
 * @var \backend\modules\log\models\transaction\TransactionSearch $model
 */

use backend\modules\log\models\transaction\PaymentMethod;
use yii\helpers\Html;
use backend\modules\log\models\transaction\PayNetCancelTransaction;

switch ($model->payment_method) {
    case PaymentMethod::TERMINAL_PAYNET:
        if ($model->payNetCancelTransaction) {
            echo Html::dropDownList('status', $model->payNetCancelTransaction->status,
                PayNetCancelTransaction::getStatusMap(),
                [
                    'class'               => 'js-status',
                    'data-transaction-id' => $model->terminal_transact_number,
                    'js-old-status'       => $model->payNetCancelTransaction->status,
                ]);

        }
        break;


}