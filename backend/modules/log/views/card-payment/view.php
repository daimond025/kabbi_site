<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use backend\modules\log\models\CardPaymentLog;

/* @var $this yii\web\View */
/* @var $model backend\modules\log\models\CardPaymentLog */

$this->title                   = "Card log (" . $model->id . ")";
$this->params['breadcrumbs'][] = ['label' => 'Card payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            'created_at:datetime',
            'type',
            [
                'label'     => 'Tenant',
                'attribute' => 'domain',
                'format'    => 'raw',
                'value'     => Html::a(Html::encode($model->tenant->domain),
                    [
                    '/setting/tenant/view', 'id' => $model->tenant_id,
                ])
            ],
            'profile',
            'client_id',
            'worker_id',
            'order_id',
            [
                'label'     => 'Result',
                'attribute' => 'result',
                'value'     => Html::encode(CardPaymentLog::getResultNames()[$model->result]),
            ],
            [
                'label'     => 'Parameters',
                'attribute' => 'params',
                'format'    => 'raw',
                'value'     => Html::tag('pre', Html::encode($model->params)),
            ],
            [
                'label'     => 'Response',
                'attribute' => 'response',
                'format'    => 'raw',
                'value'     => Html::tag('pre', Html::encode($model->response)),
            ],
        ],
    ]);

    ?>

</div>
