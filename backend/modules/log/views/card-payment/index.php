<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use app\modules\setting\models\Tenant;
use app\modules\setting\assets\SettingAsset;
use backend\modules\log\models\CardPaymentLog;
use backend\modules\log\models\CardPaymentLogSearch;

$bundle = SettingAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/log-widgets.js',
    ['depends' => 'backend\assets\DateRangePickerAsset']);

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\log\models\CardPaymentLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Card payments';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="page-content-index">

    <div class="input-group">
        <button type="button" class="btn btn-default pull-right" id="daterange">
            <i class="fa fa-calendar"></i> Выбрать период
            <i class="fa fa-caret-down"></i>
        </button>
    </div>

    <h1><?= Html::encode($this->title) ?></h1>

    <?php yii\widgets\Pjax::begin(); ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'created_at:datetime',
            [
                'label'     => 'Type',
                'attribute' => 'type',
                'filter'    => CardPaymentLog::getTypes(),
            ],
            [
                'label'     => 'Tenant',
                'attribute' => 'domain',
                'format'    => 'raw',
                'value'     => function ($model, $key, $index, $column) {
                    return Html::a(Html::encode($model->tenant->domain),
                        ['/setting/tenant/view', 'id' => $model->tenant_id]);
                },
            ],
            'profile',
            'client_id',
            'worker_id',
            'order_id',
            [
                'label'     => 'Result',
                'attribute' => 'result',
                'filter'    => CardPaymentLog::getResultNames(),
                'value'     => function ($model, $key, $index, $column) {
                    return Html::encode(CardPaymentLog::getResultNames()[$model->result]);
                },
            ],
            [
                'class'    => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]);

    ?>
    <?php yii\widgets\Pjax::end(); ?>

</div>
