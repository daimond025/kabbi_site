<?php

namespace app\modules\support\forms;

use Yii;
use yii\base\Model;

class RemoveWorkerFromOrderForm extends Model
{
    public $tenantDomain;
    public $orderNumber;
    public $workerCallsing;

    public function rules()
    {
        return [
            [['tenantDomain', 'orderNumber', 'workerCallsing'], 'required'],
            [['orderNumber', 'workerCallsing'], 'integer'],
        ];
    }
}