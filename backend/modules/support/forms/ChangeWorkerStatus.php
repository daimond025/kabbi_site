<?php

namespace app\modules\support\forms;

use Yii;
use yii\base\Model;

class ChangeWorkerStatus extends Model
{
    public $tenantDomain;
    public $workerCallsign;
    public $workerStatus;

    public function rules()
    {
        return [
            [['tenantDomain', 'workerStatus', 'workerCallsign'], 'required'],
            [['workerCallsign'], 'integer'],
        ];
    }
}