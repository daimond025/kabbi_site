<?php

namespace app\modules\support\controllers;

use yii\web\Controller;
use yii\filters\AccessControl;
use Yii;
use app\modules\support\models\ServiceCommandService;
use app\modules\support\forms\RemoveWorkerFromOrderForm;
use app\modules\support\forms\ChangeWorkerStatus;

class ServiceCommandController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['serviceCommands'],
                    ],
                ],
            ],
        ];
    }

    public function actionRemoveWorkerFromOrder()
    {
        $model = new RemoveWorkerFromOrderForm();
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $result = ServiceCommandService::removeWorkerFromOrder($model->tenantDomain, $model->orderNumber, $model->workerCallsing);
                if (isset($result['error'])) {
                    session()->setFlash('error', 'Ошибка выполнения команды:');
                    session()->setFlash('error', $result['error']);
                } else {
                    session()->setFlash('info', 'Команда выполненна успешно!');
                }
            } else {
                session()->setFlash('error', $model->errors);
            }
        }

        return $this->render('removeWorkerFromOrder', ['model' => $model]);
    }

    public function actionChangeWorkerStatus()
    {
        $workerStatusList = ['FREE' => 'FREE', 'ON_ORDER' => 'ON_ORDER', 'ON_BREAK' => 'ON_BREAK'];
        $model = new ChangeWorkerStatus();
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $result = ServiceCommandService::changeWorkerStatus($model->tenantDomain, $model->workerCallsign, $model->workerStatus);
                if (isset($result['error'])) {
                    session()->setFlash('error', 'Ошибка выполнения команды:');
                    session()->setFlash('error', $result['error']);
                } else {
                    session()->setFlash('info', 'Статус исполнителя изменен');
                }
            } else {
                session()->setFlash('error', $model->errors);
            }
        }

        return $this->render('changeWorkerStatus', ['model' => $model, 'workerStatusList' => $workerStatusList]);
    }
}