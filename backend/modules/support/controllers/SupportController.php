<?php

namespace app\modules\support\controllers;


use common\components\helpers\UserHelper;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use app\modules\support\models\Support;
use app\modules\support\models\SupportMessage;
use app\modules\support\models\SupportMessagePhoto;
use app\modules\support\models\SupportFeedback;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

class SupportController extends Controller
{

    public $image_path_arr;
    public $image_arr;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['show-active', 'show-closed'],
                        'roles'   => ['supportRead'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['support'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionShowActive($prior)
    {
        $searchModel                        = new \app\modules\support\models\SupportSearch();
        $dataProvider                       = $searchModel->search(Yii::$app->request->queryParams, 'active', $prior);
        $dataProvider->pagination->pageSize = 50;

        return $this->render('active', [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    public function actionShowClosed()
    {
        $searchModel                        = new \app\modules\support\models\SupportSearch();
        $dataProvider                       = $searchModel->search(Yii::$app->request->queryParams, 'closed');
        $dataProvider->pagination->pageSize = 50;

        return $this->render('active', [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
                'pages'        => $pages,
            ]
        );
    }

    public function actionUpdate($id)
    {
        $modelSupport = $this->findModel($id);
        $editHref     = "";
        $user         = \app\modules\support\models\User::findOne(['user_id' => $modelSupport->user_id]);

        $modelSupportMessage      = new SupportMessage();
        $modelSupportMessagePhoto = new SupportMessagePhoto();

        //Вся переписка
        $modelSupportMessageArr = SupportMessage::find()
            ->where(["support_id" => $modelSupport->support_id])
            ->all();

        // Массив фотографий
        $imgArr = [];
        foreach ($modelSupportMessageArr as $message) {
            $message_id          = $message->message_id;
            $SupportMessagePhoto = SupportMessagePhoto::find()
                ->where([
                    'message_id' => $message_id,
                ])
                ->all();
            foreach ($SupportMessagePhoto as $photo) {
                $image                 = $photo->image;
                $imgData['name']       = $image;
                $imgData['path']       = $modelSupportMessagePhoto->getPicturePath($image, $modelSupport->tenant_id);
                $imgArr[$message_id][] = $imgData;
            }
        }
        $this->image_arr = $imgArr;

        //Если сабмит формы
        if ($modelSupportMessage->load(Yii::$app->request->post())) {
            $modelSupportMessagePhoto->load(Yii::$app->request->post());
            $modelSupportMessage->support_id      = $id;
            $modelSupportMessage->support_user_id = user()->user_id;
            $transaction                          = app()->db->beginTransaction();
            //Если сохранилась фото
            if ($modelSupportMessage->validate() && $modelSupportMessage->save()) {
                //Сохраняем фотографии
                $modelSupportMessagePhoto->manySave($modelSupportMessage->message_id,
                    $modelSupportMessagePhoto->saveFiles($modelSupport->tenant_id));
                $transaction->commit();
                // Обновляем переписку
                $modelSupportMessageArr = SupportMessage::find()
                    ->where(["support_id" => $modelSupport->support_id])
                    ->all();

                // Обновляем фотки
                $imgArr = [];
                foreach ($modelSupportMessageArr as $message) {
                    $message_id          = $message->message_id;
                    $SupportMessagePhoto = SupportMessagePhoto::find()
                        ->where([
                            'message_id' => $message_id,
                        ])
                        ->all();
                    foreach ($SupportMessagePhoto as $photo) {
                        $image                 = $photo->image;
                        $imgData['name']       = $image;
                        $imgData['path']       = $modelSupportMessagePhoto->getPicturePath($image,
                            $modelSupport->tenant_id);
                        $imgArr[$message_id][] = $imgData;
                    }
                }
                $this->image_arr = $imgArr;
                // Всё ок обновляем статус и рендерим модель
                $modelSupport->status = 'ANSWER_RECIVED';
                $modelSupport->save();

                $modelSupportFeedback = SupportFeedback::find()
                    ->where([
                        'support_id' => $modelSupport->support_id,
                    ])
                    ->one();
                if (isset($modelSupportFeedback)) {
                    $modelSupportFeedback->delete();
                }

                $modelSupportMessage->refresh();
                $userName   = UserHelper::getNameUser($user);
                $ticketUrl  = Url::to(['/support/update', 'id' => $modelSupport->support_id], true);
                $ticketUrl  = str_replace('admin', $user->tenant->domain, $ticketUrl);
                $createTime = strtotime($modelSupportMessage->create_time);
                $answerTime = app()->formatter->asDate($createTime, 'short')
                    . ' ' . t('app', 'at') . ' ' . app()->formatter->asTime($createTime, 'short');

                $this->sendEmailToUser(
                    $ticketUrl,
                    $modelSupport->support_id,
                    $modelSupport->title,
                    $modelSupport->email,
                    $userName,
                    user()->name,
                    $user->lang,
                    str_replace(PHP_EOL, '<br/>', $modelSupportMessage->message),
                    $answerTime);

                $modelSupportMessage = new SupportMessage();
                session()->setFlash('success', t('support', 'Message send'));

                return $this->render('update', [
                    'modelSupport'             => $modelSupport,
                    'modelSupportMessageArr'   => $modelSupportMessageArr,
                    'modelSupportMessage'      => $modelSupportMessage,
                    'modelSupportMessagePhoto' => $modelSupportMessagePhoto,
                    'image_arr'                => $this->image_arr,
                    'editHref'                 => $editHref,
                ]);
            } else {
                displayErrors([$modelSupport, $modelSupportMessage]);
                $transaction->rollBack();
            }
        }

        $tenant               = \app\modules\setting\models\Tenant::findOne(['tenant_id' => $user->tenant_id]);
        $modelSupport['name'] = $user->last_name . ' ' . $user->name . ' ' . $user->second_name;
        $host                 = $_SERVER['HTTP_HOST'];
        $host                 = explode('.', $host);
        if (empty($user->photo) && empty($tenant->logo)) {
            $modelSupport['picture'] = '';
        } elseif (!empty($user->photo) || !empty($tenant->logo)) {
            if ($_SERVER["HTTPS"] == "on") {
                $modelSupport['picture'] = 'https://';
            } else {
                $modelSupport['picture'] = 'http://';
            }
            $modelSupport['picture'] .= $host[1] . '.' . $host[2] . '/upload/' . $tenant->tenant_id . '/';
            $modelSupport['picture'] .= !empty($user->photo) ? 'thumb_' . $user->photo : !empty($tenant->logo);
        }
        $modelSupport['tenant_info'] = $tenant;

        return $this->render('update', [
            'modelSupport'             => $modelSupport,
            'modelSupportMessageArr'   => $modelSupportMessageArr,
            'modelSupportMessage'      => $modelSupportMessage,
            'modelSupportMessagePhoto' => $modelSupportMessagePhoto,
            'image_arr'                => $this->image_arr,
            'editHref'                 => $editHref,
        ]);
    }

    protected function sendEmailToUser(
        $ticketUrl,
        $ticketId,
        $ticketTitle,
        $email,
        $user,
        $supportUser,
        $lang,
        $answer,
        $answerTime
    ) {
        $params = [
            'LAYOUT'   => 'layouts/base',
            'TEMPLATE' => 'sendSupportAnswer',
            'TO'       => $email,
            'SUBJECT'  => t('email', 'Reply received from the technical support', null, $lang),
            'DATA'     => [
                'PHONE'        => app()->params['supportPhone'],
                'EMAIL'        => app()->params['supportEmail'],
                'USER'         => $user,
                'SUPPORT_USER' => $supportUser,
                'TITLE'        => t('support', $ticketTitle, null, $lang),
                'TICKET_URL'   => $ticketUrl,
                'TICKET_ID'    => $ticketId,
                'ANSWER'       => $answer,
                'ANSWER_TIME'  => $answerTime,
                'LANGUAGE'     => $lang,
            ],
        ];

        sendMail($params);
    }

    protected function findModel($id)
    {
        if (($model = Support::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCloseTicket($id)
    {
        $model         = $this->findModel($id);
        $model->status = 'CLOSE';
        if ($model->validate() && $model->save()) {
            return $this->render('_ok_close', [
                'model' => $model,
            ]);
        }

        return $this->render('_no_ok_close', [
            'model' => $model,
        ]);
    }

    public function actionOpenTicket($id)
    {
        $model         = $this->findModel($id);
        $model->status = 'OPEN';
        if ($model->validate() && $model->save()) {
            $modelSupportFeedback = SupportFeedback::find()
                ->where([
                    'support_id' => $id,
                ])
                ->one();
            if (isset($modelSupportFeedback)) {
                $modelSupportFeedback->delete();
            }

            return $this->render('_ok_open', [
                'model' => $model,
            ]);
        }

        return $this->render('_no_ok_open', [
            'model' => $model,
        ]);
    }

}
