<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use app\modules\support\assets\SupportAsset;

$bundle = SupportAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/supportinit.js', ['depends' => 'yii\web\JqueryAsset']);
$this->title = Yii::t('app', 'Support');
$this->params['breadcrumbs'][] = $this->title;
?>
<h1>Обращения</h1>
<div class="tabs_content">
    <?php
    echo $this->render('_message_list', [
        'dataProvider'  => $dataProvider,
        'searchModel' => $searchModel,
        'pages'            => $pages,
        'registerLinkTags' => false
    ]);
    ?>
</div>


