<?php
use yii\grid\GridView;
use yii\helpers\Html;

?>
<?php
\yii\widgets\Pjax::begin();
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel'  => $searchModel,
    'columns'      => [
        'support_id',
        [
            'attribute' => 'domain',
            'label'     => 'Домен',
            'value'     => function ($model) {
                return $model->tenant->domain;
            },
        ],
        [
            'attribute' => 'last_name',
            'value'     => 'user.last_name',
        ],
        [
            'attribute' => 'name',
            'value'     => 'user.name',
        ],
        [
            'attribute' => 'second_name',
            'value'     => 'user.second_name',
        ],
        'create_time',
        'priority',
        'user_id',
        'title',
        'status',
        'email',
        [
            'attribute' => '',
            'format'    => 'raw',
            'value'     => function ($model) {
                return Html::a('Ответить', '/support/support/update/' . $model->support_id);
            },
            'visible'   => Yii::$app->user->can('support'),
        ],
    ],
]);
\yii\widgets\Pjax::end();
?>