<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

?>
<div class="box box-success">
    <div class="box-header ui-sortable-handle" style="cursor: move;">
        <i class="fa fa-comments-o"></i>
        <h3 class="box-title">Чат</h3>
    </div>

    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: auto;">
        <div class="box-body chat" id="chat-box" style="overflow: hidden; width: auto; height: auto;">
            <!-- chat item -->
            <?
            $i = 0;
            foreach ($modelSupportMessage as $model) {
                $str = '';
                if (isset($image_arr[$model->message_id])) {
                    $imageArr = $image_arr[$model->message_id];
                    foreach ($imageArr as $image) {
                        $name = Html::encode($image['name']);
                        $str = $str . "<a rel=\"nofollow noopener\" target='_blank' href='$image[path]'>$name</a></br>";
                    }
                }
                $date = app()->formatter->asDate($model->create_time, 'medium');
                $time = app()->formatter->asTime($model->create_time, 'short');

            ?>
            <? if(!empty($model->support_user_id)):?>

            <div class="item">
                <img src="<?=$modelSupport->picture?>" alt="user image" class="online">
                <p class="message">
                    <a href="#" class="name">
                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> <?= $date . ' в ' . $time ?></small>
                        Support User#<?= $model->support_user_id ?>
                    </a>

                    <?= nl2br(HtmlPurifier::process($model->message))?>
                </p>
                <div class="attachment">
                    <h4>Прикрепленные файлы:</h4>
                    <?=$str?>
                </div>
            </div>
             <?else:?>
            <div class="item">
                <img src="<?=$modelSupport->picture?>" alt="user image" class="online">
                <p class="message">
                    <a href="#" class="name">
                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> <?= $date . ' в ' . $time ?></small>
                        <?= Html::encode($modelSupport->name) ?>
                    </a>
                    <span style="width: 50%; word-wrap: break-word; white-space: pre-wrap;"><?= HtmlPurifier::process($model->message)?></span>
                </p>
                <div class="attachment">
                    <h4>Прикрепленные файлы:</h4>
                    <?= $str ?>
                </div>
            </div>
            <?$i++;
            ?>
            <?endif;?>
            <?}?>
        </div>
        <div class="slimScrollBar" style="width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 0px; z-index: 99; right: 1px; height: 184.91124260355px; background: rgb(0, 0, 0);">
        </div>
        <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 0px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(51, 51, 51);">
        </div>
    </div>
</div>



