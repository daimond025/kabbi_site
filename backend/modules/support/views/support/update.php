<?php

use yii\helpers\Html;
use app\modules\tenant\models\User;
use \yii\widgets\Pjax;
use app\modules\support\assets\SupportAsset;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;
$bundle = SupportAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/supportupdate.js', ['depends' => 'yii\web\JqueryAsset']);
$this->title = Yii::t('app', 'Support');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['show-active?prior=' . $modelSupport->priority]];
?>
<p id="ticketId" style="display: none"><?= Html::encode($modelSupport->support_id) ?></p>
<div class="ticket_detail">
    <img src="">
    <h3>Тема: <?= Html::encode(t('support', $modelSupport->title)) ?></h3>
    <p>
        <?='Домен: ' . Html::encode($modelSupport['tenant_info']->domain) ?>
    </p>
    <p>
        <?='Название службы такси: ' . Html::encode($modelSupport['tenant_info']->company_name) ?>
    </p>
    <?php if ($modelSupport->status <> "CLOSE"): ?>
    <div style="text-align: left">
        <p><?= 'ФИО пользователя: ' . Html::encode($modelSupport['name']) ?></p>
        <?= !empty($modelSupport->email) ? 'Эл. почта: ' . '<a href="mailto: '.$modelSupport->email  .'">'.Html::encode($modelSupport->email).'</a>' : ''?>
    </div>
        <div style="text-align: right">
            <a href="/support/support/close-ticket/<?= Html::encode($modelSupport->support_id) ?>" id="close_ticket" class="btn btn-danger btn-lg" >Закрыть Заявку</a>
        </div>
    <?php endif; ?>
    <?php if ($modelSupport->status == "CLOSE"): ?>
        <div style="text-align: right">
            <a href="/support/support/open-ticket/<?= Html::encode($modelSupport->support_id) ?>" id="open_ticket" class="btn btn-primary btn-lg" >Открыть Заявку</a>
        </div>
    <?php endif; ?>
    <?php
    
    if (!empty($editHref)) {
        echo Html::encode($editHref);
    }
    ?>
    <div class="td_answers">
        <?php
        echo $this->render('_chat_list', [
            'modelSupportMessage' => $modelSupportMessageArr,
            'image_arr' => $image_arr,
            'modelSupport' => $modelSupport,
        ]);
        ?>
    </div>
</div>
<?php
$form = ActiveForm::begin([
            'id' => 'open_ticket',
            'errorCssClass' => 'input_error',
            'successCssClass' => 'input_success',
            'enableClientValidation' => true,
            'options' => ['enctype' => 'multipart/form-data']
        ]);
?>
<div class="tf_content">
    <div><label>Сообщение</label></div>
    <div style="text-align: center">
        <p><?= Html::activeTextarea($modelSupportMessage, 'message', ['rows' => '6', 'style' => 'width:100%;white-space: pre-wrap;']); ?></p>
    </div>
    <div ><label>Прикрепить файл</label></div>
    <div style="text-align: left">
        <a>
            <?php
            echo FileInput::widget([
                'model' => $modelSupportMessagePhoto,
                'attribute' => 'image',
                'options' => [
                    'multiple' => true,
                    'name' => 'SupportMessagePhoto[image][]',
                    'accept' => 'image/*',
                ],
                'pluginOptions' => [
                    'allowedFileExtensions' => ['jpg', 'gif', 'png'],
                    'showUpload' => false,
                    'showRemove' => false,
                    'showCaption' => false,
                    'maxFileCount' => 7,
                ]
            ]);
            ?>
        </a>
    </div>
</div>

<div style="text-align: right"><input type="submit" class="btn btn-success btn-lg" value="Отправить"/></div>
    <?php ActiveForm::end(); ?>

