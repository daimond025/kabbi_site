<?
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin([
    'id' => 'service-command-form',
]); ?>
<h1>Сменить статус исполнителя на...</h1>
<?= $form->field($model, 'tenantDomain', ['enableLabel' => true])->textInput(array('placeholder' => 'Домен арендатора', 'class' => 'form-control text-left')); ?>
<?= $form->field($model, 'workerCallsign', ['enableLabel' => true])->textInput(array('placeholder' => 'Позывной исполнителя', 'class' => 'form-control text-left')); ?>
<?= $form->field($model, 'workerStatus', ['enableLabel' => true])->dropDownList($workerStatusList, ['prompt' => 'Выберите новый статус']); ?>
<div class="form-group">
    <?= Html::submitButton('Отправить') ?>
</div>
<?php ActiveForm::end(); ?>

