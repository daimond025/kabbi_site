<?
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin([
    'id' => 'service-command-form',
]); ?>
<h1>Снятие водителя с заказа</h1>
<?= $form->field($model, 'tenantDomain', ['enableLabel' => true])->textInput(array('placeholder' => 'Домен арендатора', 'class' => 'form-control text-left')); ?>
<?= $form->field($model, 'orderNumber', ['enableLabel' => true])->textInput(array('placeholder' => 'Номер заказа', 'class' => 'form-control text-left')); ?>
<?= $form->field($model, 'workerCallsing', ['enableLabel' => true])->textInput(array('placeholder' => 'Позывной водителя', 'class' => 'form-control text-left')); ?>
<div class="form-group">
    <?= Html::submitButton('Отправить') ?>
</div>
<?php ActiveForm::end(); ?>
<p>*Отправляет запрос в движок(engine), тот в свою очередь создает сообщение в очереди водителя(rabbitmq). Фактически
    водитель не снимается с заказа в системе а лишь закрывает карточку заказа в приложении.</p>
