<?php

namespace app\modules\support\models;

use Yii;

/**
 * This is the model class for table "tbl_city".
 *
 * @property string   $city_id
 * @property string   $republic_id
 * @property string   $name
 * @property string   $shortname
 * @property string   $lat
 * @property string   $lon
 * @property string   $fulladdress
 * @property string   $fulladdress_reverse
 *
 * @property Republic $republic
 * @property Order[]  $orders
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['republic_id', 'name'], 'required'],
            [['republic_id'], 'integer'],
            [['name', 'shortname', 'lat', 'lon'], 'string', 'max' => 100],
            [['fulladdress', 'fulladdress_reverse'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'city_id'             => 'City ID',
            'republic_id'         => 'Republic ID',
            'name'                => 'Name',
            'shortname'           => 'Shortname',
            'lat'                 => 'Lat',
            'lon'                 => 'Lon',
            'fulladdress'         => 'Fulladdress',
            'fulladdress_reverse' => 'Fulladdress Reverse',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepublic()
    {
        return $this->hasOne(Republic::className(), ['republic_id' => 'republic_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['city_id' => 'city_id']);
    }

}
