<?php

namespace app\modules\support\models;

use common\components\helpers\UserHelper;
use yii\web\IdentityInterface;
use frontend\components\behavior\file\FileBehavior;
use Yii;
use app\modules\tenant\models\UserPosition;
use yii\helpers\ArrayHelper;
use common\modules\city\models\City;
use common\modules\tenant\models\Tenant;
use frontend\modules\tenant\models\UserDispetcher;
use yii\base\NotSupportedException;
use frontend\modules\tenant\models\UserDefaultRight;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property string $user_id
 * @property string $tenant_id
 * @property string $position_id
 * @property string $email
 * @property string $email_confirm
 * @property string $password
 * @property string $last_name
 * @property string $name
 * @property string $second_name
 * @property string $phone
 * @property string $photo
 * @property integer $active
 * @property string $birth
 * @property string $address
 * @property string $create_time
 * @property string $auth_key
 * @property string $password_reset_token
 * @property timestamp $active_time
 * @property string $ha1
 * @property string $ha1b
 * @property integer $auth_exp
 *
 * @property Order[] $Orders
 * @property OrderHistory[] $OrderHistories
 * @property Support[] $Supports
 * @property SupportFeedback[] $SupportFeedbacks
 * @property \common\modules\tenant\models\Tenant $tenant
 * @property UserPosition $position
 * @property UserHasCity[] $UserHasCities
 * @property \common\modules\city\models\City[] $cities
 * @property UserSetting[] $UserSettings
 * @property UserWorkingTime[] $UserWorkingTimes
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface {

    /**
     * position_id должности администратора
     */
    const ADMIN = 7;
    const BRANCH_DIRECTOR = 11;
    const POSITION_OPERATOR = 6;
    const POSITION_MAIN_OPERATOR = 8;
    const USER_ROLE_1 = 'top_manager';
    const USER_ROLE_2 = 'branch_director';
    const USER_ROLE_3 = 'staff';

    public $password_repeat;
    public $city_list = [];
    public $position_list = [];
    public $isRightsChange = false;
    public $rights;

    /**
     * Flag of block user
     * @var bool
     */
    public $block;

    /**
     * Is user online
     * @var string on|off The value immediately substituted in name of css class
     */
    public $online = 'off';

    /**
     * Vars for user birth
     */
    public $birth_day;
    public $birth_month;
    public $birth_year;
    public $permissions = null;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['password', 'name', 'last_name'], 'required', 'on' => 'insert'],
            [['email'], 'required', 'message' => t('app', 'Email cannot be blank')],
            [['email'], 'string', 'max' => 45],
            [['email'], 'unique'],
            [['email'], 'email'],
            [['phone'], 'filter', 'filter' => function($value) {
            return preg_replace("/[^0-9]/", '', $value);
        }],
            [['tenant_id', 'position_id', 'active'], 'integer'],
            [['password_repeat', 'city_list', 'block', 'isRightsChange', 'rights'], 'safe'],
            [['address'], 'string', 'max' => 255],
            [['last_name', 'name', 'second_name'], 'string', 'max' => 45],
            [['phone'], 'string', 'max' => 15],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'on' => 'update'],
            ['password', 'string', 'min' => 6],
            ['photo', 'file', 'extensions' => ['png', 'jpeg', 'jpg'], 'maxSize' => getUploadMaxFileSize(),
                'tooBig' => t('file', 'The file size must be less than {file_size}Mb', [
                    'file_size' => getUploadMaxFileSize(true)])],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'password'        => t('user', 'Password'),
            'password_repeat' => t('user', 'Repeat password'),
            'address'         => t('user', 'Address'),
            'birth'           => t('user', 'Birth'),
            'name'            => t('user', 'Name'),
            'last_name'       => t('user', 'Last name'),
            'second_name'     => t('user', 'Second name'),
            'create_time'     => t('user', 'Registration date'),
            'position_id'     => t('user', 'Position'),
            'position.name'   => t('user', 'Position'),
            'city'            => t('user', 'City'),
            'email'           => t('app', 'Username'),
            'phone'           => t('user', 'Mobile phone'),
            'active'          => t('user', 'Active user'),
            'photo'           => t('user', 'Photo'),
            'contact_name'    => t('user', 'Contact name'),
            'active_time'     => 'Online',
            'city_list'       => t('user', 'Working in cities')
        );
    }

    /**
     * Get user position list
     * @return array [position_id => name]
     */
    public function getPositionList() {
        $edit_user_role = self::getUserRoleByPosition($this->position_id);
        $cur_user_role = self::getUserRoleByPosition(user()->position_id);

        if ($cur_user_role == self::USER_ROLE_1) {
            $position_list = $edit_user_role == self::USER_ROLE_1 ? self::getTopManagerPositionIdList() : null;
        } elseif ($cur_user_role == self::USER_ROLE_2) {
            $position_list = $edit_user_role == self::USER_ROLE_2 ? [$this->position_id] : self::getStaffPositionIdList();
        } else {
            $position_list = self::getStaffPositionIdList();
        }

        return ArrayHelper::map(UserPosition::find()->andFilterwhere(['position_id' => $position_list])->all(), 'position_id', 'name');
    }

    public static function quantityByTenant($tenant_id) {
        $usersQuantity = new \yii\db\Query();
        $usersQuantity->from(self::tableName());
        $usersQuantity->where(['tenant_id' => $tenant_id]);
        return $usersQuantity->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders() {
        return $this->hasMany(Order::className(), ['user_modifed' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderHistories() {
        return $this->hasMany(OrderHistory::className(), ['user_modifed' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupports() {
        return $this->hasMany(Support::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupportFeedbacks() {
        return $this->hasMany(SupportFeedback::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant() {
        return $this->hasOne(\common\modules\tenant\models\Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition() {
        return $this->hasOne(UserPosition::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserHasCities() {
        return $this->hasMany(UserHasCity::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities() {
        return $this->hasMany(\common\modules\city\models\City::className(), ['city_id' => 'city_id'])->viaTable('{{%user_has_city}}', ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserSettings() {
        return $this->hasMany(UserSetting::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDispetchers() {
        return $this->hasMany(UserDispetcher::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserWorkingTimes() {
        return $this->hasMany(UserWorkingTime::className(), ['user_id' => 'user_id']);
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId() {
        return $this->getPrimaryKey();
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password) {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password) {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    public static function findIdentity($id) {
        return self::findOne(['user_id' => $id]);
    }

    public static function findIdentityByEmail($email, $domain = null) {
        $user = self::findByEmail($email, $domain);

        if (is_null($user) ||
                ($user->position_id != self::ADMIN && $user->tenant->status == \common\modules\tenant\models\Tenant::BLOCKED)
        ) {
            return false;
        }

        return $user;
    }

    public static function findByEmail($email, $domain = null) {
        return self::find()
                        ->where([
                            static::tableName() . '.email' => $email,
                            'active'                       => 1
                        ])
                        ->joinWith(['tenant' => function($query) use($domain) {
                                $domain = empty($domain) ? current(explode('.', $_SERVER['HTTP_HOST'])) : $domain;
                                $query->andWhere("status != 'REMOVED' AND domain = '$domain'");
                            }
                                ], false)
                        ->one();
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public static function findIdentityByAuthKey($user_id, $auth_key) {
        return self::find()->where(['user_id' => $user_id, 'auth_key' => $auth_key])->andWhere(time() . '<= auth_exp')->one();
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey() {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    public function getAuthKey() {
        return $this->auth_key;
    }

    //---------------------------------------------------------
    //Recovery password

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken() {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken() {
        $this->password_reset_token = null;
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token) {
        if (!self::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
                    'password_reset_token' => $token,
                    'active'               => 1,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token) {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    //------------------------------------------------------------------------------

    public function behaviors() {
        return [
            'fileBehavior' => [
                'class'     => FileBehavior::className(),
                'fileField' => ['photo'],
                'upload_dir' => 'upload'
            ],
        ];
    }

    public function setEmailToken() {
        $this->email_confirm = md5($this->email . time());
    }

    /**
     * Format array of user models for table.
     * @param array $users Array of \app\modules\tenant\models\User
     * @param bool $city_list Return result with city list from all users.
     * @param bool $position_list Return result with position list from all users.
     * @return array
     */
    public static function getGridData($users, $city_list = true, $position_list = true) {

        $arUsers['USERS'] = [];
        $keyCityId = self::getKeyCityId($users);
        if ($city_list)
            $arCity = [];

        if ($position_list)
            $arPosition = [];
        foreach ($users as $user) {
            $arUserCity = [];

            foreach ($user->cities as $city) {
                $arUserCity[] = $city->name;

                if ($city_list && !array_key_exists($city->city_id, $arCity))
                    $arCity[$city->city_id] = $city->name;
            }

            if ($position_list && !array_key_exists($user['position']['position_id'], $arPosition))
                $arPosition[$user['position']['position_id']] = $user['position']['name'];

            $arUsers['USERS'][] = [
                'ID'       => $user->user_id,
                'CITY_ID'  => $keyCityId,
                'CITIES'   => implode(', ', $arUserCity),
                'PHOTO'    => $user->getPictureHtml($user->photo),
                'ONLINE'   => self::isUserOnline($user['active_time']) ? 'on' : 'off',
                'NAME'     => trim($user->last_name . ' ' . $user->name . ' ' . $user->second_name),
                'POSITION' => $user->position->name
            ];
        }



        if ($city_list) {
            asort($arCity);
            $arUsers['CITI_LIST'] = $arCity;
        }

        if ($position_list) {
            asort($arPosition);
            $arUsers['POSITION_LIST'] = $arPosition;
        }
        return $arUsers;
    }

    public static function getKeyCityId($users) {
        if (!app()->cache->get(app()->user->getId() . 'chat_key_city')) {
            foreach ($users as $rows) {
                if ($rows->user_id == app()->user->getId()) {
                    app()->cache->add(app()->user->getId() . 'chat_key_city', $rows->cities[0]->city_id, 0);
                    return $rows->cities[0]->city_id;
                }
            }
        }
        return app()->cache->get(app()->user->getId() . 'chat_key_city');
    }

    public static function isUserOnline($active_time) {
        return time() - $active_time < app()->params['user.online.ttlSec'];
    }

    /**
     * Check user invite status
     * @return bool
     */
    public function isUserInvited() {
        return $this->email_confirm != 1 && $this->active == 0;
    }

    /**
     *
     * @return array ['city_id => 'name']
     */
    public function getUserCityList() {
        return ArrayHelper::map($this->cities, 'city_id', 'name');
    }

    /**
     * Города, где работает пользователь с республиками и временным смещением
     * @return array
     */
    public function getUserCityListWithRepublic() {
        return \common\helpers\CacheHelper::getFromCache('UserCityListWithRepublic_' . $this->tenant_id, function() {
            return \common\modules\city\models\City::find()->
            joinWith(['userHasCities' => function($query) {
                $query->where(['user_id' => $this->user_id]);
            }], false)->
            with(['republic' => function($query) {
                $query->select(['republic_id', 'timezone']);
            }])->
            select([City::tableName() . '.city_id', 'name', 'republic_id'])->
            asArray()->
            all();
        });
    }

    public function getFullName() {
        return trim($this->last_name . ' ' . $this->name . ' ' . $this->second_name);
    }

    /**
     * Варианты прав для разрешений.
     * @return array
     */
    public static function getRightsVariantsOfPermission() {
        return [
            'read'  => t('rights', 'Read'),
            'write' => t('rights', 'Write'),
            'off'   => t('rights', 'Off'),
        ];
    }

    /**
     * Определение прав пользователя на конкретное разрешение.
     * @param string $permision
     * @return string all|read|write|off
     */
    public function getUserRightsByPermission($permision) {
        if (is_null($this->permissions))
            $this->permissions = $this->getPermissions();
    }

    public function beforeSave($insert) {
        $cities = $this->cities;
        foreach ($cities as $city) {
            app()->cache->delete('user_list_' . $this->tenant_id . '_' . $city['city_id']);
        }
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes) {
        //Сохранение прав
        if ($this->isRightsChange || $insert) {
            if (empty($this->rights) && user()->getUserRole() == self::USER_ROLE_3) {
                $this->rights = UserDefaultRight::getDefaultRightSettingByPosition($this->position_id);
            }
            $this->assignRights();
        }

        return $rights;
    }

    public function assignRights() {
        if (!empty($this->rights) && is_array($this->rights)) {
            $auth = Yii::$app->authManager;
            $auth->revokeAll($this->id);

            foreach ($this->rights as $name => $rights) {
                if ($rights == 'off') {
                    continue;
                }

                $permission_name = $rights == 'read' ? $rights . '_' . $name : $name;
                $permission = $auth->getPermission($permission_name);
                $auth->assign($permission, $this->id);
            }

            $role_name = self::getUserRoleByPosition($this->position_id);
            $role = $auth->getRole($role_name);
            $auth->assign($role, $this->id);
        }
    }

    public function beforeDelete() {
        //Удаление из базы операторов
        if (in_array($this->position_id, self::getOperatorPositions()))
            UserDispetcher::deleteOperatorFromPhoneBase($this->user_id);

        return parent::beforeDelete();
    }

    public function afterDelete() {
        parent::afterDelete();
        Yii::$app->authManager->revokeAll($this->id);
    }
    /**
     * Getting operator position list.
     * @return array
     */
    public static function getOperatorPositions() {
        return [
            self::POSITION_OPERATOR,
            self::POSITION_MAIN_OPERATOR
        ];
    }

    /**
     * Checking - is user dispatcher.
     * @return bool
     */
    public function is_dispatcher() {
        return in_array($this->position_id, self::getOperatorPositions());
    }

    /**
     * Getting position name of user.
     * @return string
     */
    public function getPositionName() {
        $position_map = self::getPositionMap();
        return getValue($position_map[$this->position_id]);
    }

    public static function getPositionMap() {
        return \common\helpers\CacheHelper::getFromCache('position_name' . __CLASS__, function() {
            $res = (new \yii\db\Query)
                    ->from(UserPosition::tableName())
                    ->all();
            return ArrayHelper::map($res, 'position_id', 'name');
        });
    }

    /**
     * Getting statistic of online users.
     * @return array ['dispatchers' => 0, 'others' => 11]
     */
    public static function getOnlineStat() {
        $users = self::find()->where(['tenant_id' => user()->tenant_id])->select('active_time, position_id')->all();
        $arStat = [
            'dispatchers' => 0,
            'others'      => 0
        ];

        foreach ($users as $user) {
            if (self::isUserOnline($user->active_time)) {
                if ($user->is_dispatcher()) {
                    $arStat['dispatchers'] ++;
                } else {
                    $arStat['others'] ++;
                }
            }
        }

        return $arStat;
    }

    /**
     * Update active user time for identity online.
     */
    public function updateUserActiveTime() {
        app()->db->createCommand('UPDATE ' . User::tableName() . ' SET active_time=' . time() . ' WHERE user_id=' . $this->id)->execute();
    }

    public static function getCurUserCityIds() {
        return ArrayHelper::getColumn(user()->userHasCities, 'city_id');
    }

    /**
     * Getting string user role name by position.
     * @param integer $position_id
     * @return string
     */
    public static function getUserRoleByPosition($position_id) {
        if (in_array($position_id, self::getTopManagerPositionIdList())) {
            return self::USER_ROLE_1;
        } elseif ($position_id == self::BRANCH_DIRECTOR) {
            return self::USER_ROLE_2;
        } else {
            return self::USER_ROLE_3;
        }
    }

    /**
     * Getting role name of current user.
     * @return string
     */
    public function getUserRole() {
        return self::getUserRoleByPosition($this->position_id);
    }

    public static function getTopManagerPositionIdList() {
        return [1, 2, 3, 4, self::ADMIN];
    }

    public static function getStaffPositionIdList() {
        return [5, 6, 8, 9, 10];
    }

}
