<?php

namespace app\modules\support\models;

use Yii;

/**
 * This is the model class for table "tbl_support_message".
 *
 * @property string $message_id
 * @property string $support_id
 * @property string $support_user_id
 * @property string $message
 * @property string $create_time
 *
 * @property AdminUser $supportUser
 * @property Support $support
 * @property SupportMessagePhoto[] $supportMessagePhotos
 */
class SupportMessage extends \yii\db\ActiveRecord
{

    public $photo;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_support_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['support_id', 'message'], 'required', 'on' => 'insert'],
            [['support_id', 'support_user_id'], 'integer'],
            [['message'], 'string'],
            [['create_time'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'message_id' => 'Message ID',
            'support_id' => 'Support ID',
            'support_user_id' => 'Support User ID',
            'message' => 'Message',
            'create_time' => 'Create Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupportUser()
    {
        return $this->hasOne(AdminUser::className(), ['user_id' => 'support_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupport()
    {
        return $this->hasOne(Support::className(), ['support_id' => 'support_id', 'create_time' => 'create_time']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupportMessagePhotos()
    {
        return $this->hasMany(SupportMessagePhoto::className(), ['message_id' => 'message_id']);
    }

    public function getFirstMessage($supportId)
    {
        return SupportMessage::find()
            ->where(['support_id' => $supportId])
            ->asArray()
            ->one();
    }

}
