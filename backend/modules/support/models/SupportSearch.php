<?php

namespace app\modules\support\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\support\models\Support;
use app\modules\setting\models\Tenant;
/**
 * SupportSearch represents the model behind the search form about `app\modules\support\models\Support`.
 */
class SupportSearch extends Support {

    public $name;
    public $last_name;
    public $second_name;
    public $domain;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['support_id', 'tenant_id', 'user_id'], 'integer'],
            [['priority', 'title', 'status', 'email', 'domain', 'name', 'last_name', 'second_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $ticketType = '', $prior = '') {
        $this->load($params);
        $query = Support::find()->alias('s')->joinWith('tenant t')->joinWith('user u');


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'support_id'  => $this->support_id,
            'tenant_id'   => $this->tenant_id,
            's.user_id'     => $this->user_id,
        ]);


        $query->andFilterWhere(['like', 'priority', $this->priority])
                ->andFilterWhere(['like', 'title', $this->title])
                ->andFilterWhere(['like', 's.status', $this->status])
                ->andFilterWhere(['like', 's.email', $this->email])
                ->andFilterWhere(['like', 'u.name', $this->name])
                ->andFilterWhere(['like', 'u.last_name', $this->last_name])
                ->andFilterWhere(['like', 'u.second_name', $this->second_name])
                ->andFilterWhere(['like', 't.domain', $this->domain]);

        if(empty($params)) {
            $query->orderBy(['support_id' => SORT_ASC]);
        }
        
        return $dataProvider;
    }

}
