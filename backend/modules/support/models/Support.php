<?php

namespace app\modules\support\models;

use app\modules\setting\models\Tenant;
use Yii;

/**
 * This is the model class for table "tbl_support".
 *
 * @property string $support_id
 * @property string $tenant_id
 * @property string $user_id
 * @property string $create_time
 * @property string $priority
 * @property string $title
 * @property string $status
 * @property string $email
 *
 * @property Tenant $tenant
 * @property User $user
 * @property SupportFeedback[] $supportFeedbacks
 * @property SupportMessage[] $supportMessages
 */
class Support extends \yii\db\ActiveRecord
{

    public $name;
    public $photo;
    public $picture = '';
    public $tenant_info;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_support';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'user_id', 'priority', 'title'], 'required'],
            [['tenant_id', 'user_id'], 'integer'],
            [['create_time'], 'safe'],
            [['priority', 'email'], 'string', 'max' => 45],
            [['title'], 'string', 'max' => 100],
            [['status'], 'string', 'max' => 25]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'support_id' => 'Support ID',
            'tenant_id' => 'Tenant ID',
            'user_id' => 'User ID',
            'create_time' => 'Create Time',
            'priority' => 'Priority',
            'title' => 'Title',
            'status' => 'Status',
            'email' => 'Email',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupportFeedbacks()
    {
        return $this->hasMany(SupportFeedback::className(), ['support_id' => 'support_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupportMessages()
    {
        return $this->hasMany(SupportMessage::className(), ['support_id' => 'support_id']);
    }
}
