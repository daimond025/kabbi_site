<?php

namespace app\modules\support\models;

use Yii;

/**
 * This is the model class for table "tbl_support_feedback".
 *
 * @property string $feedback_id
 * @property string $support_id
 * @property string $create_time
 * @property integer $raiting
 *
 * @property Support $support
 */
class SupportFeedback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_support_feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['support_id', 'raiting'], 'required'],
            [['support_id', 'raiting'], 'integer'],
            [['create_time'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'feedback_id' => 'Feedback ID',
            'support_id' => 'Support ID',
            'create_time' => 'Create Time',
            'raiting' => 'Raiting',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupport()
    {
        return $this->hasOne(Support::className(), ['support_id' => 'support_id']);
    }
}
