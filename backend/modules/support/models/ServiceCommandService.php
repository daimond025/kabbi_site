<?php

namespace app\modules\support\models;

use frontend\components\consul\ConsulService;
use yii\helpers\Json;
use frontend\components\serviceEngine\ServiceEngine;


class ServiceCommandService
{

    /**
     * Remove worker form order
     *  - send request to engine and then engine sends amqp message to worker queue
     * @param $tenantDomain
     * @param $orderNumber
     * @param $workerCallsing
     * @return array|mixed
     */
    public static function removeWorkerFromOrder($tenantDomain, $orderNumber, $workerCallsing)
    {
        $tenantDomain   = strtolower($tenantDomain);
        $orderNumber    = (int)$orderNumber;
        $workerCallsing = (int)$workerCallsing;
        $params         = [
            'tenant_domain'   => $tenantDomain,
            'worker_callsign' => $workerCallsing,
            'order_number'    => $orderNumber
        ];
        $method         = 'send_reject_order_to_worker';
        $consul         = \Yii::createObject([
            'class'           => ConsulService::className(),
            'consulAgentHost' => getenv('CONSUL_MAIN_HOST'),
            'consulAgentPort' => getenv('CONSUL_MAIN_PORT'),
        ]);
        $url            = $consul->createMethodUrl(ServiceEngine::CONSUL_SERVICE_NAME, $method);
        $curl           = app()->curl;
        $response       = $curl->get($url, $params);
        $statusCode     = $response->headers['Status-Code'];

        if ($statusCode != 200) {
            $error = $curl->error();
            return ['error' => $error ? $error : $statusCode];
        }
        try {
            return Json::decode($response->body);
        } catch (\Exception $exc) {
            return ['error' => $exc];
        }
    }

    public static function changeWorkerStatus($tenantDomain, $workerCallsign, $workerStatus)
    {
        $tenantDomain   = strtolower($tenantDomain);
        $workerCallsign = (int)$workerCallsign;
        $workerStatus   = strtolower($workerStatus);
        $params         = [
            'tenant_domain'   => $tenantDomain,
            'worker_callsign' => $workerCallsign,
            'worker_status'   => $workerStatus
        ];
        $method         = 'change_worker_status';
        $consul         = \Yii::createObject([
            'class'           => ConsulService::className(),
            'consulAgentHost' => getenv('CONSUL_MAIN_HOST'),
            'consulAgentPort' => getenv('CONSUL_MAIN_PORT'),
        ]);
        $url            = $consul->createMethodUrl(ServiceEngine::CONSUL_SERVICE_NAME, $method);
        $curl           = app()->curl;
        $response       = $curl->get($url, $params);
        $statusCode     = $response->headers['Status-Code'];

        if ($statusCode != 200) {
            $error = $curl->error();

            return ['error' => $error ? $error : $statusCode];
        }
        try {
            return Json::decode($response->body);
        } catch (\Exception $exc) {
            return ['error' => $exc];
        }
    }

}