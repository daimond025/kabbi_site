<?php

namespace app\modules\support;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\support\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
}
