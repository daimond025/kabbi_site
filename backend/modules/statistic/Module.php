<?php

namespace app\modules\statistic;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\statistic\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['statistics'],
                    ],
                ],
            ],
        ];
    }
}
