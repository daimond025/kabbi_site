<?php

use yii\web\View;
use app\modules\statistic\assets\StatisticAsset;

$this->title = 'Скачавшие пакет документов';
$this->params['breadcrumbs'][] = $this->title;

$bundle = StatisticAsset::register($this);

?>

<div class="input-group">
    <button type="button" class="btn btn-default pull-right" id="daterange" data-update_link='/statistic/docpack/get-data'>
        <i class="fa fa-calendar"></i> Выбрать период
        <i class="fa fa-caret-down"></i>
    </button>
</div>

<div id="myfirstchart" style="height: 250px;"></div>

Итого: <span id='total_regs' data-label="Скачавших"></span>
