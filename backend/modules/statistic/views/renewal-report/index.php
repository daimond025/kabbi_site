<?php
use yii\grid\GridView;
use app\modules\setting\assets\SettingAsset;

$bundle = SettingAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/log-widgets.js',
    ['depends' => 'backend\assets\DateRangePickerAsset']);

$this->title = 'Отчет по продлению';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="page-content-index">

    <?php
    echo GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'showFooter'=>true,
        'footerRowOptions'=>['style'=>'font-weight:bold'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'company_name',
                'label'     => 'Клиент',
            ],
            [
                'attribute' => 'domain',
                'label'     => 'Домен',
            ],
            [
                'attribute' => 'leaseEnd',
                'label' => 'Завершение аренды, дней',
            ],

        ]
    ]);

    ?>

</div>
