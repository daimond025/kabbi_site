<?php
use yii\grid\GridView;
use app\modules\setting\assets\SettingAsset;

$bundle = SettingAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/log-widgets.js',
    ['depends' => 'backend\assets\DateRangePickerAsset']);

$this->title = 'Отчет по оплатам';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="page-content-index">

    <div class="input-group">
        <button type="button" class="btn btn-default pull-right" id="daterange">
            <i class="fa fa-calendar"></i> Выбрать период
            <i class="fa fa-caret-down"></i>
        </button>
    </div>

    <?php
    echo GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'showFooter'=>TRUE,
        'footerRowOptions'=>['style'=>'font-weight:bold'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'company_name',
                'label' => 'Клиент',

            ],

            [
                'attribute' => 'domain',
                'label' => 'Домен',

            ],

            [
                'attribute' => 'sumPayments',
                'label' => 'Сумма всех платежей клиента',
                'format' => 'integer',
                'filter' => false,
                'footer' => 'Сумма: '.$searchModel->userPaymentsPerPeriod
            ],

            [
                'attribute' => 'countMonths',
                'label' => 'Кол-во месяцев (сумма по месяцам в счетах)',
                'format' => 'integer',
                'filter' => false,
                'footer' => 'Среднее: '.$searchModel->averageMonthsPerPeriod
            ],

            [
                'attribute' => 'sumAvgPayments',
                'label' => 'Средний платеж (сумма / кол-во)',
                'format' => 'integer',
                'filter' => false,
                'footer' => 'Среднее: '.$searchModel->averagePaymentsPerPeriod
            ],

        ]
    ]);

    ?>

</div>
