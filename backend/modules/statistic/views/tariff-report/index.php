<?php

use app\modules\statistic\assets\StatisticAsset;
use yii\grid\GridView;
use backend\models\PaymentsReport\TariffReportSearch;
use yii\jui\DatePicker;
use app\modules\setting\assets\SettingAsset;

$bundle = SettingAsset::register($this);
$this->registerJsFile($bundle->baseUrl . '/log-widgets.js',
    ['depends' => 'backend\assets\DateRangePickerAsset']);

$this->title = 'Отчет по тарифам';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="page-content-index">

    <div class="input-group">
        <button type="button" class="btn btn-default pull-right" id="daterange">
            <i class="fa fa-calendar"></i> Выбрать период
            <i class="fa fa-caret-down"></i>
        </button>
    </div>


    <h3>Тарифы</h3>
    <?php
    echo GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'columns' => [

            [
                'attribute' => 'tariffName',
                'label' => 'Название тарифа',
                'value' => function($model) {
                    return $model->tariffName;
                }
            ],
            [
                'attribute' => 'numTariffTenants',
                'label' => 'Количество клиентов',
                'filter' => false,
            ],
        ]
    ]);

    ?>
    <h3>Опции</h3>
    <?php

    echo GridView::widget([
        'filterModel' => $searchModel2,
        'dataProvider' => $dataProvider2,
        'columns' => [
            'optionName',
            [
                'attribute' => 'numOptionTenants',
                'label' => 'Количество клиентов',
                'filter' => false,
            ],
        ]
    ]);

    ?>
    <h3>Разовые услуги</h3>
    <?php

    echo GridView::widget([
        'filterModel' => $searchModel3,
        'dataProvider' => $dataProvider3,
        'columns' => [
            [
                'attribute' => 'name',
                'label' => 'Название услуги',
                'contentOptions' => ['style' => 'width:50%'],
            ],
            [
                'attribute' => 'numOneTimeTenants',
                'label' => 'Количество клиентов',
                'filter' => false,
            ],
        ]
    ]);

    ?>

</div>
