<?php
namespace app\modules\statistic\controllers;

use yii\web\Controller;
use backend\models\PaymentsReport\PaymentsReportSearch;
use Yii;


class PaymentsReportController extends Controller
{

    public function actionIndex()
    {

        $searchModel = new PaymentsReportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

}