<?php

namespace app\modules\statistic\controllers;

use Yii;
use yii\web\Controller;

use app\modules\setting\models\Tenant;


class ByDaysController extends Controller {


    public function behaviors() {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    
    public function actionGetData($start, $end)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        
        app()->response->format = \yii\web\Response::FORMAT_JSON;
        
        if($end < 0) $end = 0;
        if($start < $end + 5) $start = $end + 5;
        
        //Шаг
        $step = ceil( ($start - $end) / 30 );
        
        $startDay = date( "Y-m-d" , time() - 3600 * 24 * $start );
        $endDay = date( "Y-m-d" , time() - 3600 * 24 * ($end - 1) );

       $q = Tenant::find()
           ->select('create_time')
           ->where('create_time > :start',['start' => $startDay])
           ->andWhere('create_time < :end',['end' => $endDay])
           ->asArray();

        $model = $q->all();

        $total = count($model);
        
        $regCount = [];
        foreach($model as $item){
            $day = substr($item['create_time'],0,10);
            $regCount[ $day ]++;
        }    
        
        $data = [];    
        for($i = $start; $i >= $end; $i = $i -$step){
            
            $count = 0;
            for($k = $i; $k > $i - $step; $k--){
                $day = date( "Y-m-d" , time() - 3600 * 24 * $k );
                $count += $regCount[ $day ];
            }
            $startStep = date( "d.m.Y" , time() - 3600 * 24 * $i );
            $endStep = date( "d.m.Y" , time() - 3600 * 24 * ($i - $step + 1) );
            $data[] = [
                'id' => $start - $i,
                'name' => ($startStep == $endStep) ? $startStep : $startStep . ' - ' . $endStep,
                'value' => $count,
                'total' => $total,
            ];
        }
        
        return $data;
    }

    public function actionIndex($start = 30, $end = 0) {
        return $this->render('index');
    }


}
