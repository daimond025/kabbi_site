<?php
namespace app\modules\statistic\controllers;

use yii\web\Controller;
use backend\models\RenewalReport\RenewalReportSearch;
use Yii;


class RenewalReportController extends Controller
{

    public function actionIndex()
    {

        $searchModel = new RenewalReportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

}