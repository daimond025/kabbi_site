<?php

namespace app\modules\statistic\controllers;

use backend\models\TariffReport\TariffAdditionalOptionReportSearch;
use yii\web\Controller;
use backend\models\TariffReport\TariffReportSearch;
use Yii;
use backend\models\TariffReport\OneTimeServiceReportSearch;


class TariffReportController extends Controller
{

    public function actionIndex()
    {

        $searchModel = new TariffReportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $searchModel2 = new TariffAdditionalOptionReportSearch();
        $dataProvider2 = $searchModel2->search(Yii::$app->request->queryParams);

        $searchModel3 = new OneTimeServiceReportSearch();
        $dataProvider3 = $searchModel3->search(Yii::$app->request->queryParams);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'searchModel2' => $searchModel2,
            'dataProvider2' => $dataProvider2,
            'searchModel3' => $searchModel3,
            'dataProvider3' => $dataProvider3,
        ]);
    }

}