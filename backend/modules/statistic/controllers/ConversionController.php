<?php

namespace app\modules\statistic\controllers;

use backend\models\conversion\Conversion;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;


class ConversionController extends Controller {


    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    
    public function actionGetData($start, $end)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        
        app()->response->format = Response::FORMAT_JSON;
        
        if($end < 0) $end = 0;
        if($start < $end + 5) $start = $end + 5;
        
        //Шаг
        $step = ceil( ($start - $end) / 30 );
        
        $startDay = date( "Y-m-d" , time() - 3600 * 24 * $start );
        $endDay = date( "Y-m-d" , time() - 3600 * 24 * ($end - 1) );

        $q = Conversion::find()
            ->select('create_time')
            ->where('create_time > :start',['start' => $startDay])
            ->andWhere('create_time < :end',['end' => $endDay])
            ->asArray();

        $model = $q->all();

        $total = count($model);
        
        $regCount = [];
        foreach($model as $item){
            $day = substr($item['create_time'],0,10);
            $regCount[ $day ]++;
        }    
        
        $data = [];    
        for($i = $start; $i >= $end; $i = $i -$step){
            
            $count = 0;
            for($k = $i; $k > $i - $step; $k--){
                $day = date( "Y-m-d" , time() - 3600 * 24 * $k );
                $count += $regCount[ $day ];
            }
            $start_step = date( "d.m.Y" , time() - 3600 * 24 * $i );
            $end_step = date( "d.m.Y" , time() - 3600 * 24 * ($i - $step + 1) );
            $data[] = [
                'id' => $start - $i,
                'name' => ($start_step == $end_step) ? $start_step : $start_step . ' - ' . $end_step,
                'value' => $count,
                'total' => $total,
            ];
        }
        
        return $data;
    }

    public function actionIndex() {
        return $this->render('index');
    }

}






