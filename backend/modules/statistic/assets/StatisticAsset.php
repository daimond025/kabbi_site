<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\statistic\assets;

use yii\web\AssetBundle;

class StatisticAsset extends AssetBundle
{
    public $publishOptions = [
        'forceCopy' => true
    ];

    public $sourcePath = '@app/modules/statistic/assets/';

    public $css = [
        '//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css',
        'css/daterangepicker-bs3.css',
    ];
    
    public $js = [
        '//morrisjs.github.io/morris.js/js/libs/raphael-2.0.2.min.js',
        'js/morris/morris.min.js',
        'js/daterangepicker/daterangepicker.js',
        'main.js',
    ];
    public $depends = [
        'backend\assets\AppAsset',
    ];



}
