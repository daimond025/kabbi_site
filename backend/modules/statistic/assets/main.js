$(function()
{

    var dateRangePicker = $('#daterange');
    var myAreaChart;
    var data = [];
    var update_link = $('#daterange').data('update_link');


    function printToDate(key){
        for(var item in data){
            if(data[item].id == key){
                if(data[item].day1 == data[item].day2)
                    return data[item].day1;
                else
                    return data[item].day1 + ' - ' + data[item].day2;
            }
        }
        return key;
    }

    function updateMorris (start, end) {
        $.ajax({
            type: "GET",
            url: update_link,
            data: {start: start, end: end},
            success: function (json) {
                myAreaChart.setData(json);
                data = json;
                var label = $('#total_regs').data('label');
                $('#total_regs').text(json[0].total + ' ' + label[0].toLowerCase() + label.slice(1));
            },
            error: function () {
                console.log('Не смог обновить графику');
            }

        });
    }

    // Подключаем календарь
    dateRangePicker.daterangepicker({
        locale: {
            applyLabel: 'Ok',
            cancelLabel: 'Отмена',
            fromLabel: 'От',
            toLabel: 'До',
            weekLabel: 'W',
            customRangeLabel: 'Задать самому',
            firstDay: 1,
            daysOfWeek: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
            monthNames: ['Янв','Фев','Март','Апр','Май','Июнь','Июль','Авг','Сент','Окт','Ноя','Дек'],
        },
        ranges: {
            'За 7 дней': [moment().subtract(6, 'days'), moment()],
            'За 30 дней': [moment().subtract(29, 'days'), moment()],
            'За этот месяц': [moment().startOf('month'), moment()],
            'За прошлый месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            'За этот год': [moment().startOf('year'), moment()],
        },
        format: 'DD.MM.YYYY',
        startDate: moment().subtract(29, 'days'),
        endDate: moment().subtract(0, 'days'),
        opens: 'right',
        },
        function (start, end) {
            var startDays = Math.floor( (moment() - start ) / 3600 / 24 / 1000 );
            var endDays = Math.floor( (moment() - end ) / 3600 / 24 / 1000 );

            updateMorris(startDays, endDays);
        }
    );
    // Подключаем графику

    myAreaChart = new Morris.Area({
        element: 'myfirstchart',
        // the chart.
        data: [],
        xkey: 'id',
        ykeys: ['value'],
        labels: [$('#total_regs').data('label')],
        xLabelFormat: function(key){
          return '';
        },
        dateFormat: function(key){
            for(var item in this.data){
            if(this.data[item].id == key){
                return this.data[item].name;
            }
        }
        return key;
        },
    });

    updateMorris(29,0);
    console.log(data);
});

