<?php

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/params.php'
);

$logVars = array_merge(
    ['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION', '_SERVER'],
    require __DIR__ . '/../../common/config/logVarsFilter.php'
);

$config = [
    'id'                  => 'app-backend',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'backend\controllers',
    'on beforeRequest'    => ['app\modules\user\models\User', 'checkActivity'],
    'aliases'             => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components'          => [
        // Yii2 components
        'assetManager' => [
            'basePath' => '@webroot/assets',
            'baseUrl'  => '@web/assets',
        ],

        'authManager' => [
            'class'           => 'backend\components\rbac\DbManager',
            'itemTable'       => '{{%admin_auth_item}}',
            'itemChildTable'  => '{{%admin_auth_item_child}}',
            'assignmentTable' => '{{%admin_auth_assignment}}',
            'ruleTable'       => '{{%admin_auth_rule}}',
            'cacheKey'        => 'admin_rbac',
        ],

        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'log' => [
            'traceLevel' => 3,
            'targets'    => [
                [
                    'class'   => 'yii\log\FileTarget',
                    'levels'  => ['error', 'warning'],
                    'logVars' => $logVars,
                ],
                [
                    'class'   => 'yii\log\EmailTarget',
                    'mailer'  => 'mailer_log',
                    'levels'  => ['error', 'warning'],
                    'logVars' => $logVars,
                    'message' => [
                        'from'    => [getenv('MAIL_SUPPORT_USERNAME')],
                        'to'      => [getenv('MAIL_SUPPORT_USERNAME')],
                        'subject' => getenv('MAIL_SUPPORT_SUBJECT_BACKEND'),
                    ],
                ],
            ],
        ],

        'request' => [
            'baseUrl'             => '',
            'cookieValidationKey' => getenv('BACKEND_COOKIE_VALIDATION_KEY'),
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'normalizer'      => [
                'class'  => 'yii\web\UrlNormalizer',
                'action' => yii\web\UrlNormalizer::ACTION_REDIRECT_TEMPORARY,
                // use temporary redirection instead of permanent
            ],
            'rules'           => require __DIR__ . '/routes.php',
        ],

        'user'             => [
            'identityClass'   => 'app\modules\user\models\User',
            'enableAutoLogin' => true,
        ],

        // Other components
        'subscribeManager' => 'landing\modules\community\components\SubscribeManager',
    ],

    'modules' => [
        'common-tenant' => [
            'class'   => 'common\modules\tenant\Module',
            'modules' => [
                'tariff' => [
                    'class'         => 'common\modules\tenant\modules\tariff\Module',
                    'currencyClass' => \frontend\modules\tenant\models\Currency::className(),
                    'tenantClass'   => \common\modules\tenant\models\Tenant::className(),
                ],
            ],
        ],
        'employee'      => 'common\modules\employee\Module',
        'landing'       => [
            'class'   => 'app\modules\landing\Module',
            'modules' => [
                'community' => 'app\modules\landing\modules\community\Module',
            ],
        ],
        'log'           => 'app\modules\log\Module',
        'rbac'          => [
            'class'           => 'dektrium\rbac\RbacWebModule',
            'adminPermission' => 'users',
        ],
        'redactor'      => 'yii\redactor\RedactorModule',
        'setting'       => 'app\modules\setting\Module',
        'statistic'     => 'app\modules\statistic\Module',
        'support'       => 'app\modules\support\Module',
        'tenant'        => [
            'class'   => 'backend\modules\tenant\Module',
            'modules' => [
                'tariff' => 'backend\modules\tenant\modules\tariff\Module',
            ],
        ],
        'user'          => 'app\modules\user\Module',
        'utility'       => 'c006\utility\migration\Module',
    ],

    'params' => $params,
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class'      => 'yii\debug\Module',
        'allowedIPs' => ['192.168.1.*', '127.0.0.1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class'      => 'yii\gii\Module',
        'allowedIPs' => ['192.168.1.*', '127.0.0.1'],
    ];
}

return $config;