<?php

return [
    ''                                                               => 'site/index',
    'landing/community/post/<post_id:\d+>/comment/<action>/<id:\d+>' => 'landing/community/comment/<action>',
    'landing/community/post/<post_id:\d+>/comment/<action>'          => 'landing/community/comment/<action>',
    'landing/community/post/<post_id:\d+>/comment'                   => 'landing/community/comment/index',
    '<submodule>/<module>/<controller>/<action>/<id:\d+>'            => '<submodule>/<module>/<controller>/<action>',
    '<module>/<controller>/<action>/<id:\d+>'                        => '<module>/<controller>/<action>',
];