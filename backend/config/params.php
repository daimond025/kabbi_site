<?php

$backendHost  = $_SERVER['HTTP_HOST'];
$frontendHost = str_replace('admin.', 'www.', $backendHost);

return [
    'upload'    => getenv('BACKEND_UPLOAD_DIR'),
    'frontHost' => $frontendHost,
];
