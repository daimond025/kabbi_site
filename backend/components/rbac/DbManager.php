<?php


namespace backend\components\rbac;

use yii\db\Query;

class DbManager extends \dektrium\rbac\components\DbManager
{
    protected $assignments = [];

    public function checkAccess($userId, $permissionName, $params = [])
    {
        if (!isset($this->assignments[$userId])) {
            $this->assignments[$userId] = $this->getAssignments($userId);
        }

        $this->loadFromCache();
        if ($this->items !== null) {
            return $this->checkAccessFromCache($userId, $permissionName, $params, $this->assignments[$userId]);
        } else {
            return $this->checkAccessRecursive($userId, $permissionName, $params, $this->assignments[$userId]);
        }
    }

    /**
     * @param string $right
     * @return bool
     */
    public function isRightExist($right)
    {
        return (new Query())->from($this->itemTable)->where(['name' => $right])->exists();
    }

    /**
     * @param string $roleName
     * @param string $userId
     * @return bool|null
     */
    public function isAssignment($roleName, $userId)
    {
        if (empty($userId)) {
            return null;
        }

        return (new Query)->from($this->assignmentTable)
            ->where(['user_id' => (string) $userId, 'item_name' => $roleName])
            ->exists();
    }

    public function revokeByPermission($permission, $userId)
    {
        if (empty($userId)) {
            return false;
        }

        return $this->db->createCommand()
                ->delete($this->assignmentTable, ['user_id' => (string) $userId, 'item_name' => $permission])
                ->execute() > 0;
    }
}