<?php
/**
 * This behavior for upload files.
 */
namespace app\components\behavior;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

class FileSupportBehavior extends FileBaseBehavior
{
    /**
     * @var array $old_value contains old file path value
     */
    protected $old_value = [];

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
            ActiveRecord::EVENT_AFTER_FIND    => 'afterFind',
        ];
    }

    public function beforeDelete($event)
    {
        foreach ($this->fileField as $fileField) {
            $this->deleteDocument($this->owner->$fileField);
        }
    }

    public function afterFind($event)
    {
        foreach ($this->fileField as $fileField) {
            $this->old_value[$fileField] = $this->owner->$fileField;
        }
    }

    public function beforeSave($event)
    {
        foreach ($this->fileField as $fileField) {
            if ($file = UploadedFile::getInstance($this->owner, $fileField))
                $this->saveFile($file, $fileField);
            elseif (isset($this->old_value[$fileField]))
                $this->owner->$fileField = $this->old_value[$fileField];
        }
    }

    protected function saveFile(UploadedFile $file, $fileField)
    {
        if (isset($this->old_value[$fileField]))
            $this->deleteDocument($this->old_value[$fileField]);

        $extension = strtolower($file->extension);
        $filename = $this->getRandomFileName($extension);
        $basename = $filename . '.' . $extension;
        $this->owner->$fileField = $basename;
        $uploadDir = $this->uploadDir();

        if (!is_dir($uploadDir))
            mkdir($uploadDir);

        $path = $uploadDir . $basename;

        if (!$file->saveAs($path))
            $this->resize($path, $path);
        else
            Yii::error('Ошибка сохраенения фото!');
    }

    public function saveFiles($tenantId)
    {
        $arFiles = [];
        $form_name = $this->owner->formName();

        foreach ($_FILES[$form_name]['name'][$this->fileField[0]] as $key => $filname) {
            if (!empty($filname)) {
                $filnameArr = explode('.', $filname);
                $extension = strtolower(end($filnameArr));
                $filename = $this->getRandomFileName($extension, $tenantId);
                $basename = $filename . '.' . $extension;
                $arFiles[] = $basename;
                $uploadDir = $this->uploadDir($tenantId);
                if (!is_dir($uploadDir))
                    mkdir($uploadDir);
                $path = $uploadDir . $basename;
                if (move_uploaded_file($_FILES[$form_name]['tmp_name'][$this->fileField[0]][$key], $path)) {
                    $this->resize($path, $path);
                }
            }
        }

        return $arFiles;
    }

}
