<?php
/**
 * This behavior for upload files.
 */

namespace app\components\behavior;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

class FileBaseBehavior extends Behavior
{
    /**
     * @var array $fileField contains model file fields
     */
    public $fileField = [];

    public function getRandomFileName($extension, $tenantId)
    {
        do {
            $name = uniqid();
            $file = $this->uploadDir($tenantId) . $name . '.' . $extension;
        } while (file_exists($file));

        return $name;
    }

    public function getPicturePath($filename, $tenantId)
    {
        $uploadDir = (string)app()->params['frontHost'] . app()->params['upload'] . $tenantId . DIRECTORY_SEPARATOR . $filename;

        return $uploadDir;
    }

    public function getPictureHtml($filename, $style = '')
    {
        return is_file($this->uploadDir() . $filename) ? \yii\helpers\Html::img($this->getPicturePath($filename),
            ['style' => $style]) : '';
    }

    public function resize($file_input, $file_output, $w_o = 800, $h_o = 0, $percent = false)
    {
        list($w_i, $h_i, $type) = getimagesize($file_input);

        if ($w_i < 800) {
            return false;
        }
        $typeExp = explode('/', image_type_to_mime_type($type));
        $ext     = end($typeExp);

        $func = 'imagecreatefrom' . $ext;

        if (!is_callable($func)) {
            return false;
        }

        $img = $func($file_input);

        if ($percent) {
            $w_o *= $w_i / 100;
            $h_o *= $h_i / 100;
        }

        if (!$h_o) {
            $h_o = $w_o / ($w_i / $h_i);
        }
        if (!$w_o) {
            $w_o = $h_o / ($h_i / $w_i);
        }

        $img_o = imagecreatetruecolor($w_o, $h_o);
        imagecopyresampled($img_o, $img, 0, 0, 0, 0, $w_o, $h_o, $w_i, $h_i);

        $func = 'image' . $ext;

        return $func($img_o, $file_output);
    }

    public function deleteDocument($file)
    {
        $documentPath = $this->uploadDir() . $file;

        if (is_file($documentPath)) {
            unlink($documentPath);
        }
    }

    public function uploadDir($tenantId)
    {
        return $_SERVER['DOCUMENT_ROOT'] . '/frontend/web/' . app()->params['upload'] . $tenantId . DIRECTORY_SEPARATOR;
    }
}
