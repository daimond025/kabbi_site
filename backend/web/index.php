<?php

use yii\helpers\ArrayHelper;

require __DIR__ . '/../../vendor/autoload.php';

$env = new Dotenv\Dotenv(dirname(dirname(__DIR__)));
$env->load();

//defined('YII_ENV') or define('YII_ENV', getenv('YII_ENV'));
//defined('YII_DEBUG') or define('YII_DEBUG', getenv('YII_DEBUG'));

require __DIR__ . '/../../vendor/yiisoft/yii2/Yii.php';
require __DIR__ . '/../../common/config/bootstrap.php';
require __DIR__ . '/../../common/include/functions.php';
require __DIR__ . '/../config/bootstrap.php';

$config = ArrayHelper::merge(
    require __DIR__ . '/../../common/config/main.php',
    require __DIR__ . '/../config/main.php'
);

$application = new yii\web\Application($config);

$application->run();