<?php

namespace landing\models;

use Yii;
use frontend\modules\tenant\models\Tenant;

/**
 * This is the model class for table "tbl_admin_business_plan".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property integer $is_registered
 * @property string $create_time
 */
class Docpack extends \yii\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%admin_docpack}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['create_time'], 'safe', 'on' => 'create'],
            [['utm_source','utm_medium','utm_campaign','utm_content','utm_term','client_id_go','client_id_ya','ip'],'safe'],
            [['name','email','phone'],'required'],
            [['name'], 'string', 'max' => 255],
            [['email'],'email'],
            [['phone'], 'string', 'max' => 15],
            [['is_site'],'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя и фамилия',
            'email' => 'Эл. почта',
            'phone' => 'Телефон',
            'create_time' => 'Время отправки',
            'is_site' => 'С сайтом',
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \common\components\behaviors\FormattedError::className(),
        ];
    }
    
    public function response()
    {
        $errors = [];
        $status = 1;
        $attributes = ['name','phone','email'];
        foreach ($attributes as $item) {
            if( $this->hasErrors($item) ) {
                $errors[ $item ] = 1;
                $status = 0;
            } else {
                $errors[ $item ] = 0;
            }
        }
        
        return [
            'status' => $status,
            'errors' => $errors
        ];
    }
    
}
