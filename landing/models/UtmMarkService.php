<?php

namespace landing\models;

use Yii;
use \yii\web\Cookie;

class UtmMarkService
{
    private $marks = [];

    private $listMarks = [
        'utm_source',
        'utm_medium',
        'utm_campaign',
        'utm_content',
        'utm_term',
        'ip',
    ];

    public function __get($name)
    {
        if (array_key_exists($name, $this->marks)) {
            return $this->marks[$name];
        }

        return null;
    }

    public function __set($name, $val)
    {
        $this->marks[$name] = $val;
    }

    public function __isset($name)
    {
        return isset($this->marks[$name]);
    }

    /**
     * @param yii\web\CookieCollection $cookiesReq
     * @param yii\web\CookieCollection $cookiesRes
     * @param array|mixed $get
     */
    public function saveMarksToCookie($cookiesReq, $cookiesRes, $get)
    {
        foreach ($this->listMarks as $val) {
            if (isset($get[$val])) {
                if (!isset($cookiesReq[$val])) {
                    $cookiesRes->add(new Cookie([
                        'name'  => $val,
                        'value' => $get[$val],
                    ]));

                    $this->$val = $get[$val];

                }
            }

        }
    }

    /**
     * @param yii\web\CookieCollection $cookiesReq
     */
    public function setMarksFromCookie($cookiesReq)
    {
        if (isset($cookiesReq['_ym_yld'])) {
            $this->client_id_ya = $cookiesReq['_ym_yld'];
        }

        if (isset($cookiesReq['_ga'])) {
            $this->client_id_go = explode(' ', $cookiesReq['_ga'])[1];
        }

        foreach ($this->listMarks as $val) {
            if (isset($cookiesReq[$val])) {
                $this->$val = $cookiesReq[$val];
            }
        }

    }


}