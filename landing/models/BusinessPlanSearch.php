<?php

namespace landing\models;

class BusinessPlanSearch extends \landing\models\BusinessPlan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'phone', 'is_registered'], 'safe'],
        ];
    }

    public function search($params)
    {

        $query = self::find();

        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'create_time' => SORT_DESC
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            //$query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'is_registered', $this->is_registered]);

        return $dataProvider;
    }

    public function getRegister()
    {
        return $this->is_registered == 1 ? "Да" : "Нет";
    }

    public static function getArrayFilters(){
        return [1=>'да',0=>'нет'];
    }
}

