<?php

namespace landing\models;

use common\modules\tenant\models\Tenant;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    /**
     * Время(сек.) через которое не будет возможна авторизация на сайте диспетчерской.
     */
    const AUTH_EXP = 5;
    const SCENARIO_CAPTCHA = 'with captcha';

    public $email;
    public $password;
    public $domain;
    public $verifyCode;

    protected $tenantId = null;

    private $_user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // email and password are both required
            [['email', 'password', 'domain'], 'required', 'message' => 'Не заполнено'],
            ['email', 'email', 'message' => 'Неправильное значение'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
            ['verifyCode', 'required', 'on' => self::SCENARIO_CAPTCHA],
            ['verifyCode', 'captcha', 'on' => self::SCENARIO_CAPTCHA],
        ];
    }

    public function behaviors()
    {
        return [
            \common\components\behaviors\FormattedError::className(),
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array  $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Неправильный логин или пароль!');
            }
        }
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findIdentityByEmail($this->email, $this->domain);
        }

        return $this->_user;
    }

    public function attributeLabels()
    {
        return [
            'email'      => 'Эл. почта',
            'password'   => 'Пароль',
            'domain'     => 'Домен',
            'verifyCode' => 'Проверочный код',
        ];
    }

    public function getUserId()
    {
        return $this->getUser()->user_id;
    }

    public function getAuthUrl($auth_key, $user_id = null)
    {
        $user_id = empty($user_id) ? $this->getUserId() : $user_id;

        return app()->params['site.protocol'] . '://' . $this->domain . '.' . app()->params['site.domain'] . '/tenant/user/cross-login?auth_key=' . $auth_key . '&user_id=' . $user_id;
    }
}
