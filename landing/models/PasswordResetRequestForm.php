<?php
namespace landing\models;

use common\components\serviceApi\EmailTypes;
use common\components\serviceApi\ServiceApi;
use common\modules\tenant\models\Tenant;
use yii\base\Model;
use yii\helpers\Url;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;
    public $domain;

    private $_user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            [['email', 'domain'], 'required'],
            ['email', 'email'],
            ['email', 'validateDomain']
        ];
    }

    /**
     * Validates the domain.
     * This method serves as the inline validation for domain.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateDomain($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user) {
                $this->addError($attribute, 'Несуществующая Эл. почта!');
            }
        }
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false)
            $this->_user = User::findIdentityByEmail($this->email, $this->domain);

        return $this->_user;
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     * @throws \yii\base\InvalidConfigException
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = $this->getUser();

        if ($user)
        {
            if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
                $user->save(false, ['password_reset_token']);
            }

            /** @var ServiceApi $serviceApi */
            $serviceApi = app()->get('serviceApi');

            $result = $serviceApi->sendEmail(null, null, EmailTypes::PASSWORD_RESET_TOKEN, $user->lang,
                $this->email, [
                    'user_name' => $user->last_name . ' ' . $user->name,
                    'url'       => Tenant::getTenantUrl($this->domain)
                        . Url::to(['/tenant/user/reset-password', 'token' => $user->password_reset_token]),
                ]);

            return $result;
        }

        return false;
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Эл. почта',
            'domain' => 'Домен'
        ];
    }
}
