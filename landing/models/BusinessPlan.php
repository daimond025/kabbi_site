<?php

namespace landing\models;

use common\modules\tenant\models\Tenant;
use Yii;

/**
 * This is the model class for table "tbl_admin_business_plan".
 *
 * @property integer $id
 * @property string  $name
 * @property string  $email
 * @property string  $phone
 * @property integer $is_registered
 * @property string  $create_time
 */
class BusinessPlan extends \yii\db\ActiveRecord
{
    const NAME_DEFAULT = '-';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%admin_business_plan}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['utm_source','utm_medium','utm_campaign','utm_content','utm_term','client_id_go','client_id_ya','ip'],'safe'],
            [['name', 'email', 'phone'], 'required', 'message' => 'Не заполнено'],
            [['name'], 'string', 'max' => 255],
            [['email'], 'email', 'message' => 'Некорректный емайл'],
            [['phone'], 'string', 'max' => 15],
            [['is_registered'], 'integer', 'min' => 0, 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'name'          => 'Имя и фамилия',
            'email'         => 'Эл. почта',
            'phone'         => 'Телефон',
            'create_time'   => 'Время отправки',
            'is_registered' => 'Зарегистирован?',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \common\components\behaviors\FormattedError::className(),
        ];
    }

    /**
     * Метод находит пользователя по его почте
     * @return frontend\modules\tenant\models\Tenant
     */
    public function getTenantByEmail()
    {
        if (empty($this->email)) //Не ищем, если почта не указана
        {
            return null;
        }
        $tenant = Tenant::find()
            ->where('contact_email = :email', ['email' => $this->email])
            ->one();

        return $tenant;
    }

    /**
     * Метод находит пользователя по его телефону
     * @return frontend\modules\tenant\models\Tenant
     */
    public function getTenantByPhone()
    {
        if (empty($this->phone)) //Не ищем, если телефон не указан
        {
            return null;
        }
        $tenant = Tenant::find()
            ->where('contact_phone = :phone', ['phone' => $this->phone])
            ->one();

        return $tenant;
    }

    /**
     * Метод находит пользователя по его почте или телефону
     * @return boolean
     */
    public function getTenant()
    {
        $tenant = $this->getTenantByEmail();

        return is_null($tenant) ? $this->getTenantByPhone() : $tenant;
    }

    /**
     * @return bool
     */
    public function isNeedCreateLead()
    {
        return $this->name !== self::NAME_DEFAULT;
    }

    /**
     * Отправка почты
     */
    public function sendEmail()
    {
        $params = [
            'LAYOUT'   => 'layouts/base',
            'TEMPLATE' => 'downloadPlan',
            'TO'       => $this->email,
            'SUBJECT'  => 'Бизнес-план "Как открыть службу такси"',
            'DATA'     => [
                'NAME'     => $this->name,
                'URL'      => 'https://gootax.pro/?utm_source=mail&utm_medium=delivery&utm_campaign=business_plan_delivery_1',
                'MESSAGE'  => 'Бизнес-план прикреплен к письму. Осталось скачать и сохранить его себе на компьютер.',
                'LANGUAGE' => app()->language,
                'FILES'    => ['landing/business_plan.pdf'],
            ],
        ];

        sendMail($params);
    }
}
