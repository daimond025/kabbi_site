<?php

namespace landing\models;

class DocpackSearch extends \landing\models\Docpack
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','email', 'phone'], 'safe']
        ];
    }
    
    
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'create_time' => SORT_DESC
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone]);

        return $dataProvider;
    }
    
    public function getIsSite()
    {
        return $this->is_site ? 'Да' : 'Нет';
    }
}

