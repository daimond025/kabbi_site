<?php

namespace landing\models;

use common\modules\tenant\models\Tenant;
use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $user_id
 * @property integer $tenant_id
 * @property integer $position_id
 * @property string  $email
 * @property string  $email_confirm
 * @property string  $password
 * @property string  $last_name
 * @property string  $name
 * @property string  $second_name
 * @property string  $phone
 * @property string  $photo
 * @property integer $active
 * @property string  $birth
 * @property string  $address
 * @property string  $create_time
 * @property string  $auth_key
 * @property string  access_token
 * @property string  $password_reset_token
 * @property integer $active_time
 * @property string  $ha1
 * @property string  $ha1b
 * @property integer $auth_exp
 * @property string  $lang
 *
 * @property ClientCompanyTransaction[] $clientCompanyTransactions
 * @property ClientTransaction[] $clientTransactions
 * @property OrderViews[] $orderViews
 * @property Support[] $supports
 * @property Tenant $tenant
 * @property UserPosition $position
 * @property UserDispetcher[] $userDispetchers
 * @property UserHasCity[] $userHasCities
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    const ADMIN = 7;
    const BRANCH_DIRECTOR = 11;
    const POSITION_OPERATOR = 6;
    const POSITION_MAIN_OPERATOR = 8;
    const USER_ROLE_1 = 'top_manager';
    const USER_ROLE_2 = 'branch_director';
    const USER_ROLE_3 = 'staff';

    public $rights;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'position_id', 'email', 'email_confirm', 'password', 'auth_key', 'password_reset_token', 'active_time'], 'required'],
            [['tenant_id', 'position_id', 'active', 'active_time'], 'integer'],
            [['birth', 'create_time'], 'safe'],
            [['email'], 'string', 'max' => 40],
            [['email_confirm', 'last_name', 'name', 'second_name'], 'string', 'max' => 45],
            [['password', 'photo', 'address', 'auth_key', 'password_reset_token'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 15],
            [['ha1', 'ha1b'], 'string', 'max' => 64],
            [['email'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'tenant_id' => 'Tenant ID',
            'position_id' => 'Position ID',
            'email' => 'Email',
            'email_confirm' => 'Email Confirm',
            'password' => 'Password',
            'last_name' => 'Last Name',
            'name' => 'Name',
            'second_name' => 'Second Name',
            'phone' => 'Phone',
            'photo' => 'Photo',
            'active' => 'Active',
            'birth' => 'Birth',
            'address' => 'Address',
            'create_time' => 'Create Time',
            'auth_key' => 'Auth Key',
            'password_reset_token' => 'Password Reset Token',
            'active_time' => 'Active Time',
            'ha1' => 'Ha1',
            'ha1b' => 'Ha1b',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientCompanyTransactions()
    {
        return $this->hasMany(ClientCompanyTransaction::className(), ['user_create' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientTransactions()
    {
        return $this->hasMany(ClientTransaction::className(), ['user_create' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderViews()
    {
        return $this->hasMany(OrderViews::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupports()
    {
        return $this->hasMany(Support::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTenant()
    {
        return $this->hasOne(Tenant::className(), ['tenant_id' => 'tenant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(\app\modules\setting\models\UserPosition::className(), ['position_id' => 'position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDispetchers()
    {
        return $this->hasMany(UserDispetcher::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserHasCities()
    {
        return $this->hasMany(UserHasCity::className(), ['user_id' => 'user_id']);
    }

    public static function findIdentityByEmail($email, $domain)
    {
        $user = self::find()
                ->where([
                    static::tableName() . '.email'     => $email,
                    static::tableName() . '.tenant_id' => Tenant::getIdByDomain($domain),
                    'active'                           => 1,
                ])
                ->joinWith(['tenant' => function($query) use($domain) {
                        $query->andWhere("status != 'REMOVED' AND domain = '$domain'");
                    }
                ], false)
                ->one();

        if (is_null($user) ||
            ($user->position_id != self::ADMIN && $user->tenant->status == Tenant::BLOCKED)
        )
        {
            return false;
        }

        return $user;
    }

    public static function findIdentity($id)
    {
        return static::findOne(['user_id' => $id]);
    }

    public function setAccessToken()
    {
        $security = Yii::$app->security;
        $this->access_token = $security->generatePasswordHash($security->generateRandomString());
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return self::find()->where(['access_token' => $token])->one();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function assignRights()
    {
        if (!empty($this->rights) && is_array($this->rights)) {
            $auth = Yii::$app->authManager;

            foreach ($this->rights as $name => $rights) {
                if($rights == 'off') {
                    continue;
                }

                $permission_name = $rights == 'read' ? $rights . '_' . $name : $name;
                $permission = $auth->getPermission($permission_name);
                $auth->assign($permission, $this->id);
            }

            $role = $auth->getRole(self::USER_ROLE_1);
            $auth->assign($role, $this->id);
        }
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }
}