<?php

namespace landing\models;

use common\components\gearman\Gearman;

/**
 * Class Presentation
 * @package landing\models
 */
class Presentation
{
    private function getFilename($saleSoft)
    {
        return $saleSoft ? 'landing/Gootax-presantation-rus.pdf' : 'landing/gootax_presentation.pdf';
    }

    /**
     * Отправка почты
     */
    public function sendEmail($name, $email, $saleSoft)
    {
        $params = [
            'LAYOUT'   => 'layouts/base',
            'TEMPLATE' => 'downloadPlan',
            'TO'       => $email,
            'SUBJECT'  => 'Презентация Гутакса',
            'DATA'     => [
                'NAME'     => $name,
                'URL'      => 'https://gootax.pro',
                'MESSAGE'  => 'Презентация прикреплена к письму. Осталось скачать и сохранить файл себе на компьютер.',
                'LANGUAGE' => app()->language,
                'FILES'    => [$this->getFilename($saleSoft)],
            ],
        ];

        sendMail($params);
    }
}
