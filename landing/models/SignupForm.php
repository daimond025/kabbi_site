<?php

namespace landing\models;

use common\components\serviceApi\EmailTypes;
use common\components\serviceApi\ServiceApi;
use common\modules\tenant\models\mobileApp\MobileApp;
use common\modules\tenant\models\mobileApp\MobileAppDemo;
use common\modules\tenant\models\Tenant;
use common\modules\tenant\modules\tariff\models\Tariff;
use common\modules\tenant\modules\tariff\models\TenantPermission;
use common\modules\tenant\modules\tariff\models\TenantTariff;
use frontend\modules\tenant\models\UserDefaultRight;
use frontend\modules\tenant\models\UserDispetcher;
use lead\Module;
use Yii;
use yii\base\Model;

/**
 * Signup form
 */
class SignupForm extends Model
{
    const AUTH_EXP = 5;

    public $ip;
    public $utm_source;
    public $utm_medium;
    public $utm_campaign;
    public $utm_content;
    public $utm_term;
    public $client_id_go;
    public $client_id_ya;

    public $email;
    public $domain;
    public $name;
    public $last_name;
    public $second_name;
    public $phone;
    public $position_id = 7;
    public $agree = 0;

    private $_user = null;
    private $_password = null;
    private $tenantId;
    private $demoMobileApp;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'utm_source',
                    'utm_medium',
                    'utm_campaign',
                    'utm_content',
                    'utm_term',
                    'client_id_go',
                    'client_id_ya',
                    'ip',
                ],
                'safe',
            ],
            [
                ['email', 'domain', 'name', 'last_name', 'phone', 'agree'],
                'required',
                'message' => t('validator', 'It is not filled'),
            ],
            [
                'domain',
                'string',
                'min'      => 4,
                'max'      => 15,
                'tooShort' => t('validator', 'The name must not be less than {characters} characters.',
                    ['characters' => 4]),
                'tooLong'  => t('validator', 'The name must not be more than {characters} characters.',
                    ['characters' => 15]),
            ],
            ['domain', 'match', 'pattern' => '/^[a-z]+[a-z0-9\-]*$/'],
            ['email', 'email', 'message' => t('validator', '"{attribute}" is not a valid email address.')],
            [['name', 'last_name', 'second_name'], 'filter', 'filter' => 'trim'],
            [
                'domain',
                'unique',
                'targetClass' => Tenant::className(),
                'message'     => t('validator', 'Name is occupied, try to come up with more.'),
            ],
            [
                ['phone'],
                'filter',
                'filter' => function ($value) {
                    return preg_replace("/[^0-9]/", '', $value);
                },
            ],
        ];
    }

    /**
     * Setup demo tariff
     *
     * @param integer $tenantId
     */
    private function setupDemoTariff($tenantId)
    {
        $validity = app()->params['demoTariffValidity'];

        $demoTariff = Tariff::find()->byDemo(true)->one();

        $startedAt    = strtotime(date('d.m.Y'));
        $tenantTariff = new TenantTariff([
            'tenant_id'   => $tenantId,
            'started_at'  => $startedAt,
            'expiry_date' => strtotime('+' . $validity . 'day', $startedAt),
            'tariff_id'   => $demoTariff->id,
        ]);
        if ($tenantTariff->save()) {
            foreach ($demoTariff->tariffPermissions as $tariffPermission) {
                $tenantPermission = new TenantPermission([
                    'tenant_tariff_id' => $tenantTariff->id,
                    'permission_id'    => $tariffPermission->id,
                    'value'            => $tariffPermission->value,
                ]);
                if (!$tenantPermission->save()) {
                    \Yii::error(implode('\n', $tenantTariff->getFirstErrors()), 'tenant-tariff');
                }
            }
        } else {
            \Yii::error(implode('\n', $tenantTariff->getFirstErrors()), 'tenant-tariff');
        }
    }

    /**
     * Setup demo mobile app
     *
     * @param integer $tenantId
     *
     * @return int|null
     */
    private function setupDemoMobileApp($tenantId)
    {
        $demo = MobileAppDemo::find()->one();
        if ($demo) {
            $attributes = $demo->attributes;
            unset($attributes['demo_id']);

            $mobileApp             = new MobileApp(['tenant_id' => $tenantId, 'isDemo' => true]);
            $mobileApp->attributes = $attributes;
            if (!$mobileApp->save()) {
                \Yii::error(implode('\n', $mobileApp->getFirstErrors()), 'mobile-app');
            }

            return $mobileApp->app_id;
        } else {
            \Yii::error('Not found demo mobile app', 'mobile-app');
        }

        return null;
    }

    /**
     * Create lead
     *
     * @param int    $tenantId
     * @param string $domain
     * @param string $lastName
     * @param string $name
     * @param string $secondName
     * @param string $phone
     * @param string $email
     *
     * @throws \yii\base\InvalidConfigException
     */
    private function createLead($tenantId, $domain, $lastName, $name, $secondName, $phone, $email)
    {
        /* @var $lead Lead */
        $lead = \Yii::createObject(Lead::className());

        $lead->createLead($lastName, $name, $secondName, $tenantId, Tenant::getTenantUrl($domain), $phone, $email, $this->ip,Module::SOURCE_REGISTER);
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {

        //Заполнение тенанта
        $tenant                      = new \common\modules\tenant\models\Tenant;
        $tenant->domain              = $this->domain;
        $tenant->contact_name        = $this->name;
        $tenant->contact_last_name   = $this->last_name;
        $tenant->contact_second_name = $this->second_name;
        $tenant->contact_phone       = $this->phone;
        $tenant->email               = $this->email;
        $tenant->contact_email       = $this->email;
        $tenant->ip                  = $this->ip;
        $tenant->utm_source          = $this->utm_source;
        $tenant->full_company_name   = 'Service taxi';
        $tenant->company_name        = 'Service taxi';
        $tenant->utm_medium          = $this->utm_medium;
        $tenant->utm_campaign        = $this->utm_campaign;
        $tenant->utm_content         = $this->utm_content;
        $tenant->utm_term            = $this->utm_term;
        $tenant->client_id_go        = $this->client_id_go;
        $tenant->client_id_ya        = $this->client_id_ya;


        $transaction = app()->db->beginTransaction();

        if ($tenant->save(false)) {
            $this->setupDemoTariff($tenant->tenant_id);
            $mobileApp = $this->setupDemoMobileApp($tenant->tenant_id);

//            if (YII_ENV_PROD) {
//                $this->createLead(
//                    $tenant->tenant_id,
//                    $tenant->domain,
//                    $tenant->contact_last_name,
//                    $tenant->contact_name,
//                    $tenant->contact_second_name,
//                    $tenant->contact_phone,
//                    $tenant->contact_email
//                );
//            }

            //Заполнение пользователя
            $user                = new User();
            $user->position_id   = $this->position_id;
            $user->name          = $this->name;
            $user->last_name     = $this->last_name;
            $user->second_name   = $this->second_name;
            $user->email         = $this->email;
            $user->email_confirm = 1;
            $user->tenant_id     = $tenant->tenant_id;
            $user->active        = 1;
            $user->setPassword($this->getPassword());
            $user->generateAuthKey();
            $user->setAccessToken();
            $user->auth_exp    = time() + self::AUTH_EXP;
            $preferredLanguage = Yii::$app->bootstrapLanguage->getPreferredLanguage();
            $user->lang        = $preferredLanguage == 'en-US' ? $preferredLanguage : mb_substr($preferredLanguage, 0,
                2);

            if ($user->save(false)) {
                //Добавление прав
                $user->rights = UserDefaultRight::getDefaultRightSettingByPosition($user->position_id);

                if (is_array($user->rights) && !empty($user->rights)) {
                    $user->assignRights();
                }

                //----------------------------------------------------

                $this->_user = $user;
                $tenant->afterSignup();

                $transactionKamailio = app()->kamailio->beginTransaction();
                //Добавляем в БД телефонии пользователя
                $tenant->addDomainToPhoneBase();
                //Добавляем в БД телефонии пользователя
                $operator_name = 'notify';
                $password      = rand_str();
                $ha1           = UserDispetcher::generateHa1Hash($operator_name, $password, $tenant->domain);
                $ha1b          = UserDispetcher::generateHa1bHash($operator_name, $password, $tenant->domain);
                $md5_password  = UserDispetcher::getOperatorPasswordHash($operator_name, $password);
                UserDispetcher::addOperatorToPhoneBase($ha1, $ha1b, $md5_password, null, $tenant->domain);

                $transaction->commit();
                $transactionKamailio->commit();

                $this->tenantId      = $tenant->tenant_id;
                $this->demoMobileApp = $mobileApp;

                $this->sendEmail($tenant);

                return true;
            }

            $transaction->rollBack();
        }

        return false;
    }

    /**
     * @param Tenant $tenant
     *
     * @return void
     * @throws \yii\base\InvalidConfigException
     */
    private function sendEmail(Tenant $tenant)
    {
        /** @var ServiceApi $serviceApi */
        $serviceApi = app()->get('serviceApi');

        $serviceApi->sendEmail($tenant->tenant_id, null, EmailTypes::REGISTER, null,
            $this->email, [
                'user_name'     => $tenant->contact_last_name . ' ' . $tenant->contact_name,
                'user_email'    => $this->email,
                'user_password' => $this->getPassword(),
                'sales_phone'   => app()->params['salesPhone'],
                'sales_email'   => app()->params['salesEmail'],
                'url'           => Tenant::getTenantUrl($this->domain),
            ]);
    }

    private function getPassword()
    {
        if (is_null($this->_password)) {
            $this->_password = Yii::$app->security->generateRandomString(10);
        }

        return $this->_password;
    }

    public function attributeLabels()
    {
        return [
            'email'           => t('app', 'E-mail'),
            'password'        => t('app', 'Password'),
            'password_repeat' => t('app', 'Password confirm'),
            'domain'          => t('app', 'Domain'),
            'name'            => t('app', 'Name'),
            'last_name'       => t('app', 'Last name'),
            'second_name'     => t('app', 'Second name'),
            'phone'           => t('app', 'Phone'),
            'agree'           => '',
        ];
    }

    public function getAuthUrl()
    {
        $loginForm = new LoginForm(['domain' => $this->domain]);

        return $loginForm->getAuthUrl($this->_user->auth_key, $this->_user->user_id);
    }

    public function getTenantId()
    {
        return $this->tenantId;
    }

    public function getDemoMobileApp()
    {
        return $this->demoMobileApp;
    }
}
