<?php

namespace landing\models;

use Yii;
use yii\base\Model;
use common\components\gearman\Gearman;

class RegisterDistributerForm extends Model
{

    public $company;
    public $city;
    public $site;
    public $activity_type;
    public $contact_fio;
    public $contact_position;
    public $contact_phone;
    public $contact_email;
    public $source_type;
    public $direction_type;
    public $comment;

    /**
     * Getting activity types
     * @result array
     */
    public function getActivityTypes()
    {
        return [
            1  => 'Консалтинговая компания',
            2  => 'Системный интегратор',
            3  => 'Бизнес-консультант',
            4  => 'Обучающий центр',
            5  => 'Веб-студия',
            6  => 'Продажа софта',
            7  => 'Фрилансер',
            8  => 'Маркетинговые исследования',
            9  => 'Реклама и продвижение сайтов',
            10 => 'ИТ-аутсорсинг',
            11 => 'Другое',
        ];
    }

    /**
     * Getting source types
     * @result array
     */
    public function getSourceTypes()
    {
        return [
            1 => 'Через интернет',
            2 => 'От знакомых',
        ];
    }

    /**
     * Getting direction types
     * @result array
     */
    public function getDirectionTypes()
    {
        return [
            1 => 'Служба такси',
            2 => 'Грузоперевозки',
            3 => 'Служба доставки, курьеры',
            4 => 'Различные услуги',
        ];
    }

    public function behaviors()
    {
        return [
            \common\components\behaviors\FormattedError::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contact_fio', 'contact_position', 'contact_phone', 'contact_email'],
                'required', 'message' => 'Не заполнено'],
            [['company', 'city', 'contact_fio', 'contact_position',
                'contact_phone', 'comment'], 'string', 'max' => 255],
            ['contact_email', 'email', 'message' => 'Не заполнено'],
            ['activity_type', 'in', 'range'   => array_keys($this->getActivityTypes()),
                'message' => 'Не заполнено'],
            ['source_type', 'in', 'range'   => array_keys($this->getSourceTypes()),
                'message' => 'Не заполнено'],
            ['direction_type', 'in', 'range'   => array_keys($this->getDirectionTypes()),
                'message' => 'Не заполнено'],
            [['contact_phone'], 'filter', 'filter' => function($value) {
                return preg_replace("/[^0-9]/", '', $value);
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company'          => 'Название компании',
            'city'             => 'Страна, город',
            'site'             => 'Сайт',
            'activity_type'    => 'Вид деятельности компании',
            'contact_fio'      => 'ФИО контактного лица',
            'contact_position' => 'Должность',
            'contact_phone'    => 'Телефон',
            'contact_email'    => 'Эл. почта',
            'source_type'      => 'Как вы узнали о сервисе',
            'direction_type'   => 'Интересующее направление',
            'comment'          => 'Напишите почему вы хотите стать дистрибьютером Гутакса',
        ];
    }

    public function sendEmail($email)
    {
        $fields = [];
        foreach ($this->attributeLabels() as $key => $label) {
            switch ($key) {
                case 'activity_type':
                    $value = $this->getActivityTypes()[$this[$key]];
                    break;
                case 'source_type':
                    $value = $this->getSourceTypes()[$this[$key]];
                    break;
                case 'direction_type':
                    $value = $this->getDirectionTypes()[$this[$key]];
                    break;
                default:
                    $value = $this[$key];
            }
            $fields[] = "{$label}: \"$value\"";
        }

        $params = [
            'TEMPLATE' => 'registerDistributer',
            'TO'       => $email,
            'SUBJECT'  => 'Заявка на партнерство',
            'DATA'     => [
                'TITLE'  => 'Введенная информация',
                'FIELDS' => $fields,
            ],
        ];

        sendMail($params);
    }
}
