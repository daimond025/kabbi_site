<?php

namespace landing\models;

use common\components\behaviors\FormattedError;
use lead\Module;
use yii\db\ActiveRecord;

/**
 * Class Conversion
 * @package landing\models
 *
 * @property int $conversion_id
 * @property string $action
 * @property string $name
 * @property string $city
 * @property string $contact
 * @property string $email
 * @property string $utm_source
 * @property string $utm_medium
 * @property string $utm_campaign
 * @property string $utm_content
 * @property string $utm_term
 * @property string $client_id_go
 * @property string $client_id_ya
 * @property string $ip
 */
class Conversion extends ActiveRecord
{
    const TYPE_PRESENTATION = 'presentation';
    const ACTION_SALE_SOFT__PRESENTATION = 'sale-soft/request-presentation';
    const ACTION_POPUP__CALLBACK = 'popup/callback';
    const ACTION_INDEX__REQUEST_DEMONSTRATION = 'index/request-demonstration';
    const ACTION_BUSINESS_GUIDE__REQUEST_GUIDE = 'business-guide/request-guide';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%conversion}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['utm_source', 'utm_medium', 'utm_campaign', 'utm_content', 'utm_term', 'client_id_go', 'client_id_ya', 'ip'], 'safe'],
            [['name', 'city', 'contact'], 'required'],
            [['name', 'city', 'contact'], 'string', 'max' => 100],
            [['action'], 'string', 'max' => 255],
            [['email'], 'email', 'message' => 'Некорректный емайл'],
            ['comment', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'conversion_id' => 'ID',
            'action'        => 'Действие',
            'name'          => 'Имя',
            'city'          => 'Город',
            'contact'       => 'Телефон или скайп',
            'email'         => 'Эл. почта',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            FormattedError::className(),
        ];
    }

    private function sendPresentation($saleSoft)
    {
        $presentation = new Presentation();
        $presentation->sendEmail($this->name, $this->email, $saleSoft);
    }

    private function sendBusinessGuide()
    {
        $guide = new BusinessGuide();
        $guide->sendEmail($this->name, $this->email);
    }

    private function createLead($leadSource)
    {
        /* @var $lead Lead */
        $lead = \Yii::createObject(Lead::className());

        $lead->createLead(null, $this->name, null, null, null, $this->contact, $this->email, $this->ip, $leadSource);
    }

    /**
     * @return null|string
     */
    private function getLeadSource()
    {
        switch ($this->action) {
            case self::ACTION_POPUP__CALLBACK:
                return Module::SOURCE_CALLBACK;
            case self::ACTION_INDEX__REQUEST_DEMONSTRATION:
                return Module::SOURCE_DEMONSTRATION;
            case self::ACTION_BUSINESS_GUIDE__REQUEST_GUIDE:
                return Module::SOURCE_GUIDE;
            default:
                return null;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (mb_stripos($this->action, self::TYPE_PRESENTATION) !== false) {
            $this->sendPresentation($this->action === self::ACTION_SALE_SOFT__PRESENTATION);
        } elseif ($this->action === self::ACTION_BUSINESS_GUIDE__REQUEST_GUIDE) {
            $this->sendBusinessGuide();
        }

        if (YII_ENV_PROD) {
            try {
                $leadSource = $this->getLeadSource();
                if ($leadSource !== null) {
                    $this->createLead($leadSource);
                }
            } catch (\Exception $e) {
                \Yii::error("Create landing lead exception (name={$this->name}, phone={$this->contact}, email={$this->email}). Error={$e->getMessage()}");
            }
        }
    }
}