<?php

namespace landing\models;

use landing\components\address\SypexGeo;
use yii\base\InvalidConfigException;
use yii\base\Object;

/**
 * Class Lead
 * @package landing\models
 */
class Lead extends Object
{
    /**
     * Getting user address
     *
     * @return array
     * @throws InvalidConfigException
     */
    private function getUserAddress()
    {
        $sypexGeo = \Yii::createObject(SypexGeo::className());
        try {
            $address = $sypexGeo->getAddressByIp(app()->request->userIP);
        } catch (\Exception $ex) {
            $address = null;
        }

        $country = isset($address['country']['name_ru']) ? $address['country']['name_ru'] : null;
        $region = isset($address['region']['name_ru']) ? $address['region']['name_ru'] : null;
        $city = isset($address['city']['name_ru']) ? $address['city']['name_ru'] : null;

        return [$country, $region, $city];
    }

    /**
     * Creating lead
     *
     * @param string $lastName
     * @param string $name
     * @param string $secondName
     * @param int $tenantId
     * @param string $url
     * @param string $phone
     * @param string $email
     * @param string $ip
     * @param string $source
     */
    public function createLead($lastName, $name, $secondName, $tenantId, $url, $phone, $email, $ip, $source)
    {
        /* @var $module \lead\Module */
        $module = \Yii::$app->getModule('lead');

        try {
            $lead = $module->getLeadModel($module::TYPE_BITRIX24);

            list($country, $region, $city) = $this->getUserAddress();

            $lead->createLead([
                'id'          => $tenantId,
                'url'         => $url,
                'last_name'   => $lastName,
                'name'        => $name,
                'second_name' => $secondName,
                'phone'       => $phone,
                'email'       => $email,
                'ip'          => $ip,
                'country'     => $country,
                'region'      => $region,
                'city'        => $city,
                'source'      => $source,
            ]);
        } catch (\Exception $ex) {
            \Yii::error($ex, 'create lead');
        }
    }
}