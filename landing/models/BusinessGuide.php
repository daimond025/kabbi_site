<?php

namespace landing\models;

use common\components\gearman\Gearman;

/**
 * Class BusinessGuide
 * @package landing\models
 */
class BusinessGuide
{
    /**
     * Отправка почты
     */
    public function sendEmail($name, $email)
    {
        $params = [
            'LAYOUT'   => 'layouts/base',
            'TEMPLATE' => 'downloadPlan',
            'TO'       => $email,
            'SUBJECT'  => 'Руководство по открытию бизнеса такси',
            'DATA'     => [
                'NAME'     => $name,
                'URL'      => 'https://gootax.pro',
                'MESSAGE'  => 'Руководство прикреплено к письму. Осталось скачать и сохранить файл себе на компьютер.',
                'LANGUAGE' => app()->language,
                'FILES'    => ['landing/Gootax-for-business.pdf'],
            ],
        ];

        sendMail($params);
    }
}
