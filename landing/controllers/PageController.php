<?php

namespace landing\controllers;

use backend\modules\landing\models\PageContent;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use landing\models\UtmMarkService;
use landing\models\SignupForm;
use Yii;

class PageController extends BaseController
{
    public function actionIndex($view = null)
    {
        if (!isset($view)) {
            $view = $this->defaultAction;
        }
        switch ($view) {
            case 'business-plan':
                $showMenu = false;
                break;
            default:
                $showMenu = true;
        }

        $content = PageContent::getPageContent($view);

        if (!empty($content)) {

            return $this->renderContent('<div data-client-id-ya="' . $this->utmMarkService->client_id_ya . '" data-client-id-go="' . $this->utmMarkService->client_id_go . '" data-utm-term="' . $this->utmMarkService->utm_term . '" data-utm-content="' . $this->utmMarkService->utm_content . '" data-utm-campaign="' . $this->utmMarkService->utm_campaign . '" data-utm-medium="' . $this->utmMarkService->utm_medium . '" data-utm-source="' . $this->utmMarkService->utm_source . '" data-ip="' . $_SERVER['REMOTE_ADDR'] . '">' . $content . '</div>');
        }
        throw new NotFoundHttpException(t('yii', 'Page not found.'));

    }
}
