<?php

namespace landing\controllers;

use Yii;
use yii\web\Controller;

use landing\models\SignupForm;
use common\components\gearman\Gearman;
use \yii\web\Response;

class TenantController extends Controller
{

    /**
     * Ajax signup validation
     * @return json
     */
    public function actionSignupValidate()
    {
        if (Yii::$app->request->isAjax) {
            $model = new SignupForm;
            $model->load(post());
            Yii::$app->response->format = Response::FORMAT_JSON;

            return \yii\widgets\ActiveForm::validate($model);
        }
    }

    /**
     * Tenant registration
     * @return mixed
     */
    public function actionSignup()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $model = new SignupForm();

        if ($model->load(post()) && $model->signup()) {
            return json_encode([
                'auth_url'        => $model->getAuthUrl(),
                'domain'          => $model->domain,
                'tenant_id'       => $model->getTenantId(),
                'demo_mobile_app' => $model->getDemoMobileApp(),
            ]);
        }

        return json_encode(['auth_url' => null]);
    }

    public function actionSendSmsCode($phone)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $test = !YII_ENV_PROD && $phone == '79001234567';

        if (!$test) {
            $data['server']  = \common\models\AdminSmsServer::find()->asArray()->where('active=1')->all();
            $data['phone']   = $phone;
            $data['text']    = rand(1000, 9999);
            $data['expires'] = time() + app()->params['sms_code_exp'];
        } else {
            $data['text'] = 4785;
        }

        $session              = session();
        $session['sms_code']  = $data['text'];
        $session['sms_phone'] = $phone;
        $session['sms_exp']   = time() + app()->params['sms_code_exp'];

        if (!$test) {
            Yii::$app->gearman->doBackground(Gearman::SMS_TASK, $data);
        }
    }

    /**
     * Tenant confirm phone
     */
    public function actionConfirmSmsCode($code)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $result = ['error' => null];

        if (!(session()->get('sms_code') == $code && $this->checkSmsCodeExpire())) {
            $result['error'] = t('validator', 'Invalid sms code');
        }

        return json_encode($result);
    }

    /**
     * Check expire sms code
     */
    protected function checkSmsCodeExpire()
    {
        return session()->get('sms_exp') > time();
    }
}
