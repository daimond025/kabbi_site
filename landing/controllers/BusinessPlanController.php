<?php

namespace landing\controllers;

use common\modules\tenant\models\Tenant;
use landing\models\Lead;
use lead\Module;
use Yii;
use yii\web\Controller;

use landing\models\BusinessPlan;
use common\components\gearman\Gearman;

class BusinessPlanController extends Controller
{
    public function actionSend()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }
        app()->response->format = \yii\web\Response::FORMAT_JSON;


        $model = new BusinessPlan();

        if ($model->load(app()->request->post())) {
            if ($model->validate()) {
                $model->sendEmail();
                $tenant = $model->getTenant();
                if (!is_null($tenant)) {
                    $model->is_registered = 1;
                }

                if (YII_ENV_PROD && $model->isNeedCreateLead()) {
                    $this->createLead(
                        $model->name,
                        empty($tenant) ? null : $tenant->tenant_id,
                        empty($tenant) ? null : $tenant->domain,
                        $model->phone,
                        $model->email,
                        $model->ip
                    );
                }

                $model->save();

                return ['status' => 'success'];
            }

            return ['status' => 'error'];
        }
    }

    /**
     * Create lead
     *
     * @param string $name
     * @param int    $tenantId
     * @param string $domain
     * @param string $phone
     * @param string $email
     *
     * @throws \yii\base\InvalidConfigException
     */
    private function createLead($name, $tenantId, $domain, $phone, $email, $ip)
    {
        /* @var $lead Lead */
        $lead = \Yii::createObject(Lead::className());
        $url  = empty($domain) ? '' : Tenant::getTenantUrl($domain);

        $lead->createLead(null, $name, null, $tenantId, $url, $phone, $email, $ip, Module::SOURCE_BUSINESS_PLAN);
    }

    public function actionTest()
    {
    }


    /**
     * Ajax signup validation
     * @return json
     */
    public function actionValidate()
    {
        if (Yii::$app->request->isAjax) {
            $model = new BusinessPlan;
            $model->load(post());
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            return \yii\widgets\ActiveForm::validate($model);
        }
    }

    public function actionSendSmsCode($phone)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $test = !YII_ENV_PROD && $phone == '79001234567';

        if (!$test) {
            $data['server']  = \common\models\AdminSmsServer::find()->asArray()->where('active=1')->all();
            $data['phone']   = $phone;
            $data['text']    = rand(1000, 9999);
            $data['expires'] = time() + app()->params['sms_code_exp'];
        } else {
            $data['text'] = 4785;
        }

        $session              = session();
        $session['sms_code']  = $data['text'];
        $session['sms_phone'] = $phone;
        $session['sms_exp']   = time() + app()->params['sms_code_exp'];

        if (!$test) {
            Yii::$app->gearman->doBackground(Gearman::SMS_TASK, $data);
        }
    }

    /**
     * Tenant confirm phone
     */
    public function actionConfirmSmsCode($code)
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $result = ['error' => null];

        if (!(session()->get('sms_code') == $code && $this->checkSmsCodeExpire())) {
            $result['error'] = t('validator', 'Invalid sms code');
        }

        return json_encode($result);
    }

    /**
     * Check expire sms code
     */
    protected function checkSmsCodeExpire()
    {
        return session()->get('sms_exp') > time();
    }
}
