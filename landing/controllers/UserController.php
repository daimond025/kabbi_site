<?php

namespace landing\controllers;


use landing\models\LoginForm;
use landing\models\PasswordResetRequestForm;

class UserController extends BaseController
{

    public $layout = 'user';


    public function actionSignup()
    {

        return $this->render('signup', [
            'domain' => app()->params['site.domain'],
            'model' => $this->view->params['signupForm'],
        ]);
    }


    public function actionLogin()
    {
//        if (!\Yii::$app->user->isGuest) {
//            return json_encode([
//                'first_name' => user()->firstname,
//                'last_name'  => user()->lastname
//            ]);
//        }


        return $this->render('login', [
            'modelLogin' => new LoginForm([
                'scenario' => LoginForm::SCENARIO_CAPTCHA,
            ]),
            'modelRecoveryPassword' => new PasswordResetRequestForm(),
            'domain' => app()->params['site.domain'],
        ]);
    }
}