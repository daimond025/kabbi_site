<?php

namespace landing\controllers;

use landing\models\SignupForm;
use yii\web\Controller;
use landing\models\UtmMarkService;
use Yii;

class BaseController extends Controller
{

    protected $utmMarkService;

    public function __construct($id, $module, UtmMarkService $utmMarkService)
    {
        parent::__construct($id, $module);

        $this->utmMarkService = $utmMarkService;
        $this->utmMarkService->setMarksFromCookie(Yii::$app->request->cookies);
        $this->utmMarkService->saveMarksToCookie(Yii::$app->request->cookies, Yii::$app->response->cookies, get());

        $this->view->params['signupForm'] = new SignupForm(['ip'           => $this->utmMarkService->ip,
                                                            'utm_source'   => urldecode($this->utmMarkService->utm_source),
                                                            'utm_medium'   => urldecode($this->utmMarkService->utm_medium),
                                                            'utm_campaign' => urldecode($this->utmMarkService->utm_campaign),
                                                            'utm_content'  => urldecode($this->utmMarkService->utm_content),
                                                            'utm_term'     => urldecode($this->utmMarkService->utm_term),
                                                            'client_id_go' => urldecode($this->utmMarkService->client_id_go),
                                                            'client_id_ya' => urldecode($this->utmMarkService->client_id_ya),
        ]);
    }

}
