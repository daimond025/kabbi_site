<?php

namespace landing\controllers;

use app\components\actions\CaptchaAction;
use common\components\gearman\Gearman;
use landing\components\actions\ErrorAction;
use landing\models\Conversion;
use landing\models\Docpack;
use landing\models\LoginForm;
use landing\models\Presentation;
use landing\modules\community\models\user\CommunityLoginForm;
use landing\modules\community\models\user\CommunityUser;
use landing\modules\community\models\user\ConfirmEmailForm;
use landing\modules\community\models\user\PasswordResetRequestForm;
use landing\modules\community\models\user\ResetPasswordForm;
use landing\modules\community\models\user\SignupForm;
use Yii;
use yii\redis\Connection;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

class SiteController extends \yii\web\Controller
{

    const COUNTER_SIGNUPS = 'counter signups';
    const COUNTER_LOGINS = 'counter logins';
    const COUNTER_COMMUNITY_LOGINS = 'counter community logins';
    const MAX_ATTEMPTS = 5;

    public $showMenu = true;

    public function actions()
    {
        return [
            'error'   => ErrorAction::className(),
            'captcha' => [
                'class'           => CaptchaAction::className(),
                'foreColor'       => 0xFF8B0F,
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only'  => ['community-logout', 'community-signup'],
                'rules' => [
                    [
                        'actions' => ['community-signup'],
                        'allow'   => true,
                        'roles'   => ['?'],
                    ],
                    [
                        'actions' => ['community-logout'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'community-logout'  => ['post'],
                    'create-conversion' => ['post'],
                    'send-presentation' => ['post'],
                ],
            ],
        ];
    }



    public function actionContactForm()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        $json = 0;

        if (isset($_POST['Contact'])) {
            $params = [
                'TEMPLATE' => 'contactForm',
                'TO'       => app()->params['contacts.email'],
                'SUBJECT'  => 'Сообщение с формы обратной связи сайта ' . app()->name,
                'DATA'     => [
                    'NAME'     => $_POST['Contact']['name'],
                    'PHONE'    => $_POST['Contact']['phone'],
                    'EMAIL'    => $_POST['Contact']['email'],
                    'MESSAGE'  => $_POST['Contact']['message'],
                    'LANGUAGE' => app()->language,
                ],
            ];

            sendMail($params);
            $json = 1;
        }

        return json_encode($json);
    }

    public function actionLogin()
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException();
        }

        app()->response->format = \yii\web\Response::FORMAT_JSON;

//        if (!app()->user->isGuest) {
//            return 'success';
//        }

        $scenario = LoginForm::SCENARIO_DEFAULT;

        $counter = session()->get(self::COUNTER_LOGINS);
        if ($counter >= self::MAX_ATTEMPTS) {
            $scenario = LoginForm::SCENARIO_CAPTCHA;
        }

        $model = new LoginForm([
            'scenario' => $scenario,
        ]);

        if ($model->load(app()->request->post())) {
            if ($model->validate()) {

                $gootaxUser    = $model->getUser();
                $communityUser = CommunityUser::find()
                    ->where([
                        'user_id' => $gootaxUser->user_id,
                        'status'  => CommunityUser::STATUS_ACTIVE,
                    ])->one();

                if (!$communityUser) {
                    //$communityUser = \landing\components\SynchManager::registerGootaxClient($gootaxUser);

                    $communityUser = SignupForm::signupFromGootax($gootaxUser);
                }

                if (!$communityUser || !app()->user->login($communityUser)) {
                    session()->setFlash('error', 'Ошибка входа, cообщите администратору.');
                }

                return 'success';
            } else {
                $errors = $model->getFormattedError();

                if ($counter >= self::MAX_ATTEMPTS - 1) {
                    $errors['showCaptcha'] = true;
                }
                session()->set(self::COUNTER_LOGINS, (++$counter));

                return $errors;
            }
        }
    }

    public function actionRecoveryPassword()
    {
        app()->response->format = \yii\web\Response::FORMAT_JSON;

        $model = new \landing\models\PasswordResetRequestForm;

        if ($model->load(app()->request->post())) {
            if ($model->validate()) {
                if ($model->sendEmail()) {
                    $message = 'На Ваш email отправлены инструкции по восстановлению пароля.';
                } else {
                    $message = 'Возникла ошибка при отправке письма на Ваш email.';
                }
                app()->session->setFlash('success', $message);

                return 'success';
            }
        }

        app()->response->format = \yii\web\Response::FORMAT_JSON;

        return ActiveForm::validate($model);
    }

    public function actionDeleteCache()
    {
        $cache = app()->cache;
        echo $cache->flush() ? 'Good' : 'Error';
    }

    public function actionCommunitySignup()
    {
        app()->response->format = \yii\web\Response::FORMAT_JSON;

        $scenario = SignupForm::SCENARIO_DEFAULT;

        $counter = session()->get(self::COUNTER_SIGNUPS);
        if ($counter >= self::MAX_ATTEMPTS) {
            $scenario = SignupForm::SCENARIO_CAPTCHA;
        }

        $model = new SignupForm([
            'scenario' => $scenario,
        ]);

        if ($model->load(app()->request->post())) {
            if ($model->signup()) {

                app()->session->setFlash('success', 'На Ваш email отправленно письмо подтверждения регистрации.');

                return 'success';
            } else {
                $errors = $model->getFormattedError();

                if ($counter >= self::MAX_ATTEMPTS - 1) {
                    $errors['showCaptcha'] = true;
                }
                session()->set(self::COUNTER_SIGNUPS, (++$counter));

                return $errors;
            }
        }
    }

    public function actionGotoGootax()
    {
        $user = \landing\models\User::findone(user()->user_id);

        $user->generateAuthKey();
        $user->auth_exp = time() + \landing\models\LoginForm::AUTH_EXP;
        $user->save(false, ['auth_key', 'auth_exp']);

        $this->redirect(
            app()->params['site.protocol'] . '://' . $user->tenant->domain . '.' . app()->params['site.domain'] . '/tenant/user/cross-login?auth_key=' . $user->auth_key . '&user_id=' . $user->user_id);
    }

    public function actionCommunityLogin()
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException();
        }

        if (!app()->user->isGuest) {
            return $this->goHome();
        }

        app()->response->format = \yii\web\Response::FORMAT_JSON;

        $scenario = CommunityLoginForm::SCENARIO_DEFAULT;

        $counter = session()->get(self::COUNTER_COMMUNITY_LOGINS);
        if ($counter >= self::MAX_ATTEMPTS) {
            $scenario = CommunityLoginForm::SCENARIO_CAPTCHA;
        }

        $model = new CommunityLoginForm([
            'scenario' => $scenario,
        ]);

        if ($model->load(app()->request->post())) {
            if ($model->login()) {
                return 'success';
            } else {
                $errors = $model->getFormattedError();

                if ($counter >= self::MAX_ATTEMPTS - 1) {
                    $errors['showCaptcha'] = true;
                }
                app()->session->set(self::COUNTER_COMMUNITY_LOGINS, (++$counter));

                return $errors;
            }
        }
    }

    public function actionCommunityLogout()
    {
        app()->user->logout();
        $this->goHome();
    }

    public function actionCommunityConfirmEmail($token)
    {
        try {
            $model = new ConfirmEmailForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->confirmEmail()) {
            app()->session->setFlash('success', 'Ваш email успешно подтвержден.');
        } else {
            app()->session->setFlash('error', 'Ошибка подтверждения Вашего email.');
        }

        return $this->goHome();
    }

    public function actionCommunityRequestResetPassword()
    {
        app()->response->format = Response::FORMAT_JSON;

        $model = new PasswordResetRequestForm();

        if ($model->load(app()->request->post())) {
            if ($model->validate()) {
                if ($model->sendEmail()) {
                    $message = 'На Ваш email отправлены инструкции по восстановлению пароля.';
                } else {
                    $message = 'Возникла ошибка при отправке письма на Ваш email.';
                }
                app()->session->setFlash('success', $message);

                return 'success';
            }
        }

        return ActiveForm::validate($model);
    }

    public function actionCommunityResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (\yii\base\InvalidParamException $ex) {
            throw new \yii\web\BadRequestHttpException($ex->getMessage());
        }

        if ($model->load(app()->request->post()) && $model->validate() && $model->resetPassword()) {
            app()->session->setFlash('success', 'Пароль изменен.');

            return $this->goHome();
        }

        $this->layout = '@app/modules/community/views/layouts/main';

        return $this->render('user/resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionRegisterDistributer()
    {
        app()->response->format = Response::FORMAT_JSON;

        $model = new \landing\models\RegisterDistributerForm();

        if ($model->load(app()->request->post())) {
            if ($model->validate()) {
                $model->sendEmail(app()->params['contacts.partnersEmail']);

                return 'success';
            } else {
                $errors = $model->getFormattedError();

                return $errors;
            }
        }
    }

    public function actionRegisterDocpack()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }

        app()->response->format = Response::FORMAT_JSON;

        $post = post();

        $model             = new Docpack();
        $model->attributes = [
            'name'    => isset($post['name']) ? $post['name'] : '',
            'phone'   => isset($post['phone']) ? $post['phone'] : '',
            'email'   => isset($post['email']) ? $post['email'] : '',
            'is_site' => isset($post['is_site']) ? true : false,
        ];

        $model->save();

        return $model->response();
    }

    public function actionCreateConversion()
    {
        if (!Yii::$app->request->isAjax) {
            return false;
        }


        app()->response->format = Response::FORMAT_JSON;

        $model = $this->createConversionModel();

        $model->attributes = [
            'action'       => post('action'),
            'name'         => post('name'),
            'city'         => post('city'),
            'contact'      => post('contact'),
            'email'        => post('email'),
            'ip'           => post('ip'),
            'comment' => post('comment'),
            'utm_source'   => post('utm_source'),
            'utm_medium'   => post('utm_medium'),
            'utm_campaign' => post('utm_campaign'),
            'utm_content'  => post('utm_content'),
            'utm_term'     => post('utm_term'),
            'client_id_go' => post('client_id_go'),
            'client_id_ya' => post('client_id_ya'),
        ];

        if ($model->save()) {
            return ['result' => 1];
        }

        return [
            'result' => 0,
            'errors' => $model->firstErrors,
        ];

    }

    protected function createConversionModel()
    {
        $model = new Conversion();

        return $model;
    }

    /**
     * @param $popupCount
     *
     * @return array|Response
     */
    public function actionGetPopupIndex($popupCount)
    {
        if (!app()->request->isAjax) {
            return $this->goHome();
        }

        app()->response->format = Response::FORMAT_JSON;

        /* @var $db Connection */
        $db  = app()->redis_check_status;
        $key = 'landing.requestPopupId';

        if ($popupCount <= 0) {
            return ['index' => 1];
        }

        try {
            return ['index' => ((int)$db->incr($key) % (int)$popupCount) + 1];
        } catch (\Exception $ex) {
            $db->del($key);

            return ['index' => 1];
        }
    }
}
