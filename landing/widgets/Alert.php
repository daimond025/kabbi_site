<?php

namespace landing\widgets;

use \yii\helpers\Html;


class Alert extends \yii\base\Widget
{
    public $alertTypes = [
        'error'   => 'hn_red',
        'danger'  => 'hn_red',
        'success' => '',
        'info'    => '',
        'warning' => 'hn_red'
    ];

    public function init()
    {
        parent::init();

        $session = \Yii::$app->getSession();
        $flashes = $session->getAllFlashes();

        foreach ($flashes as $type => $data) {
            if (isset($this->alertTypes[$type])) {
                $data = (array) $data;
                foreach ($data as $message) {
                    $content = Html::tag('div',
                            $message . ' <a class="hn_close"></a>',
                            ['class' => 'content']);

                    echo Html::tag('header', $content, [
                        'class' => 'header_not ' . $this->alertTypes[$type],
                    ]);
                }

                $session->removeFlash($type);
            }
        }
    }
}
