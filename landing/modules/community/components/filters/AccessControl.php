<?php

namespace landing\modules\community\components\filters;

class AccessControl extends \yii\filters\AccessControl
{
    public function init() {
        $this->denyCallback = function ($rule, $action) {
            app()->session->addFlash('info',
                    'Для продолжения работы необходимо авторизоваться');
            $action->controller->redirect(['/community/community/index']);
        };

        parent::init();
    }

}