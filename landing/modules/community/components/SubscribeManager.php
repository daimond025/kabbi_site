<?php

namespace landing\modules\community\components;

use Yii;
use yii\helpers\Url;
use yii\base\Event;
use landing\modules\community\models\Comment;
use landing\modules\community\models\OfferInfo;
use landing\modules\community\models\SubscribeComment;
use landing\modules\community\models\SubscribeOffer;
use landing\modules\community\models\user\CommunityUser;
use landing\modules\community\models\Section;
use common\components\gearman\Gearman;

class SubscribeManager extends \yii\base\Component
{
    public function registerSubscribeOffer()
    {
        Event::on(OfferInfo::className(), OfferInfo::EVENT_BEFORE_UPDATE, function ($event) {
            if (!$event->sender->isAttributeChanged('status')) {
                return;
            }

            $subscribers = CommunityUser::find()
                    ->where(['in', 'id',
                        SubscribeOffer::find()
                                ->select('community_user_id')
                                ->byOfferInfoId($event->sender->offer_info_id),
                    ])->all();

            try {
                foreach ($subscribers as $subscriber) {
                    $params = [
                        'TEMPLATE' => 'baseSubscribe',
                        'TO' => $subscriber->email,
                        'SUBJECT' => \Yii::$app->name . '. Изменился статус предложения',
                        'DATA' => [
                            'URL' => app()->params['landingUrl'] . Url::to(['/community/'
                                . Section::$controllers[$event->sender->post->section_id] . '/view',
                                'id' => $event->sender->post_id,
                            ]),
                            'NAME' => $subscriber->fullName,
                            'MESSAGE' => 'Изменился статус предложения',
                            'LINK-TITLE' => 'перейти к предложению',
                            'LANGUAGE' => app()->language,
                        ],
                    ];
                    sendMail($params);
                }
            } catch (\yii\base\ErrorException $exc) {
                Yii::error('Ошибка рассылки (ключ поста (предложения) ' . $event->sender->post_id . ').', 'community');
            }
        });
    }

    public function registerSubscribeComment()
    {
        Event::on(Comment::className(), Comment::EVENT_AFTER_INSERT, function ($event) {
            $subscribers = CommunityUser::find()
                    ->where(['in', 'id',
                        SubscribeComment::find()
                                ->select('community_user_id')
                                ->byPostId($event->sender->post_id),
                    ])->all();

            try {
                foreach ($subscribers as $subscriber) {
                    if ($event->sender->author_id != $subscriber->id) {
                        $params = [
                            'TEMPLATE' => 'baseSubscribe',
                            'TO' => $subscriber->email,
                            'SUBJECT' => \Yii::$app->name . '. Новый комментарий',
                            'DATA' => [
                                'URL' => app()->params['landingUrl'] . Url::to(['/community/'
                                    . Section::$controllers[$event->sender->post->section_id] . '/view',
                                    'id' => $event->sender->post_id,
                                    '#' => 'comment' . $event->sender->comment_id,
                                ]),
                                'NAME' => $subscriber->fullName,
                                'MESSAGE' => 'Добавлен новый комментарий',
                                'LINK-TITLE' => 'перейти к комментарию',
                                'LANGUAGE' => app()->language,
                            ],
                        ];
                        sendMail($params);
                    }
                }
            } catch (\yii\base\ErrorException $exc) {
                Yii::error('Ошибка рассылки (ключ комментария ' . $event->sender->id . ').', 'community');
            }
        });
    }
}
