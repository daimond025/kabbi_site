<?php

namespace landing\modules\community\components\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;

class ClearCacheBehavior extends Behavior
{
    public $tags;

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'clearCache',
            ActiveRecord::EVENT_AFTER_UPDATE => 'clearCache',
            ActiveRecord::EVENT_AFTER_DELETE => 'clearCache',
        ];
    }

    public function clearCache($event)
    {
        foreach ($this->tags as $tag) {
            \yii\caching\TagDependency::invalidate(app()->cache, $tag);
        }
    }

}

