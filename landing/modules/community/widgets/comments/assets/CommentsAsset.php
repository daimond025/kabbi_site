<?php

namespace app\modules\community\widgets\comments\assets;

use yii\web\AssetBundle;

class CommentsAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/community/widgets/comments/assets/';
    public $css = [
    ];
    public $js = [
        'js/comments.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
