(function ($, document) {
    'use strict';

    var app = {

        init: function () {
            this.registerBeforeSubmitForm();
            this.registerLoginFromComment();
            this.registerSubscribeEvents();
        },

        registerBeforeSubmitForm: function () {
            $('#commentsform').on('beforeSubmit', function (e) {
                var $form = $(this),
                    $submit = $form.find('[type=submit]');

                if ($form.data('process')) {
                    return false;
                }

                $form.data('process', true);
                $submit.attr('disabled', true);
                $.post(
                        $form.attr('action'),
                        $form.serialize()
                        )
                        .done(function (data) {
                            if (data === 'success') {
                                $form.trigger('reset');
                                document.cookie = 'addedComment=true';
                                $.pjax.reload({container: '#comments-list'});
                            } else {
                                $form.yiiActiveForm('updateMessages', data, true);
                            }
                            $form.data('process', false);
                            $submit.attr('disabled', false);
                        })
                        .fail(function (data) {
                            $form.data('process', false);
                            $submit.attr('disabled', false);
                            console.log('fail', data);
                        });
                return false;
            });
        },

        registerLoginFromComment: function (e) {
            $('#login-from-comment').on('click', function () {
                document.cookie = 'comment=' + $('#comment-comment').val();
            });
        },

        registerSubscribeEvents: function () {
            var $checkbox = $('#subscribe-comment'),
                $form = $('#subscribe-comment-form');

            $checkbox.click(function () {
                if ($form.attr('processing')) {
                    return false;
                }
                $form.submit();
            });

            $form.submit(function (e) {
                if ($form.attr('processing')) {
                    return false;
                }

                $form.attr('processing', true);
                $.post(
                        $form.attr('action'),
                        $form.serialize())

                        .done(function (data) {
                            $form.attr('processing', null);
                            if (data === 'error') {
                                $checkbox.attr('checked', !!$checkbox.attr('checked'));
                            }
                        })
                        .fail(function (data) {
                            $form.attr('processing', null);
                            $checkbox.attr('checked', !!$checkbox.attr('checked'));
                            console.log('fail', data);
                        });
                return false;
            });
        }
    };

    app.init();
})(jQuery, document);