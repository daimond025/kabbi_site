<?php

namespace app\modules\community\widgets\comments;

use Yii;
use landing\modules\community\models\Comment;
use landing\modules\community\models\user\CommunityUser;

class Comments extends \yii\base\Widget {

    public $post;

    public function run() {

        $comment = new Comment;
        $comment->post_id = $this->post->post_id;

        $postId = $this->post->post_id;

        $queryCommand = Comment::find()->select('max(comment_id)')
                ->byPostId($postId)->createCommand();

        $comments = app()->db->cache(function () use ($postId) {
            return Comment::find()
                            ->joinWith('author')
                            ->byPostId($postId)
                            ->all();
        }, 60 * 60, new \yii\caching\ChainedDependency([
            'dependencies' => [
                new \yii\caching\DbDependency([
                    'sql' => $queryCommand->sql,
                    'params' => $queryCommand->params,
                        ]),
                new \yii\caching\TagDependency([
                    'tags' => [CommunityUser::className()],
                        ]),
            ],
        ]));

        // write comment from cookie after login from commentForm
        $comment->comment = \yii\helpers\Html::encode(
                filter_input(INPUT_COOKIE, 'comment'));
        setcookie('comment', null);

        return $this->render('index', [
            'post' => $this->post,
            'comments' => $comments,
            'comment' => $comment,
            'subscribeComment' => isset($this->post->userSubscribeComment),
        ]);
    }
}
