<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$fieldConfig = Yii::$app->params['fieldOptions'];
$fieldConfig['template'] = "{input}\n{error}";
$fieldConfig['options']['class'] = '';

?>

<div class="add_comment app_form">
    <h4>Ваш комментарий</h4>
    <?php if (!app()->user->isGuest): ?>
        <?= Html::beginForm('/community/comment/subscribe', 'post', [
            'id' => 'subscribe-comment-form',
        ]); ?>

        <label>
            <?= Html::hiddenInput('post_id', $comment->post_id); ?>

            <?= Html::checkbox('subscribe', $subscribeComment, [
                'id' => 'subscribe-comment',
                'value' => 1,
            ]); ?>
            Подписаться на комментарии
        </label>

        <?= Html::endForm(); ?>
    <?php endif; ?>

    <?php $form = ActiveForm::begin([
                'id' => 'commentsform',
                'action' => '/community/comment/add',
                'errorCssClass' => 'error',
                'fieldConfig' => $fieldConfig,
                'validateOnBlur' => false,
                'validateOnChange' => false,
    ]); ?>

        <?= $form->field($comment, 'comment')->textarea([
            'rows' => 3,
            'placeholder' => 'Текст комментария',
        ]); ?>

        <?= $form->field($comment, 'post_id')->hiddenInput(); ?>

        <div class="submit-comment">
            <?= Html::submitInput('Отправить', [
                'class' => 'button',
                Yii::$app->user->isGuest ? 'disabled' : '' => '',
            ]); ?>
            <?php if (Yii::$app->user->isGuest): ?>
            <a id="login-from-comment" data-remodal-target="login">Авторизуйтесь, пожалуйста</a>
            <?php endif; ?>
        </div>

    <?php ActiveForm::end(); ?>
</div>