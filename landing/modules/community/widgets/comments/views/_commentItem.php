<?php

use yii\helpers\Url;
use yii\helpers\Html;

?>

<div class="comment" id="comment<?= $comment->comment_id; ?>">
    <div class="bmi_first_line">
        <span class="bmi_time"><?= $comment->getPublishedAt(); ?></span>

        <a href="<?= Url::to(['/community/profile/view', 'id' => $comment->author_id]); ?>" class="bmi_man">
            <?= Html::encode($comment->author->lastname . ' ' . $comment->author->firstname); ?>
        </a>
    </div>
    <div>
        <?= $comment->comment; ?>
    </div>
</div>