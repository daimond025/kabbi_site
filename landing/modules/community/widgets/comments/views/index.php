<?php

use yii\widgets\Pjax;
use app\modules\community\widgets\comments\assets\CommentsAsset;

CommentsAsset::register($this);

?>

<div class="comments" id="comments">
    <?php Pjax::begin([
        'id' => 'comments-list',
    ]); ?>

    <?php if (!empty($comments)): ?>
        <div class="c_header">
            <a class="bmi_comm"><span><?= intval($post->count_comments); ?></span><i></i></a>
            <h4>Комментарии</h4>
        </div>

        <?= $this->render('_commentList', [
            'comments' => $comments,
        ]); ?>
    <?php endif; ?>

    <?php Pjax::end(); ?>

    <?= $this->render('_addComment', [
        'comment' => $comment,
        'subscribeComment' => $subscribeComment,
    ]); ?>

</div>