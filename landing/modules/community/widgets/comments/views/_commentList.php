<?php

use yii\helpers\Html;

?>

<?php
    if (!empty($comments)) {
        foreach ($comments as $comment) {
            echo $this->render('_commentItem', [
                'comment' => $comment,
            ]);
        }
    } else {
        echo Html::tag('div', t('community', 'Empty'), ['class' => 'comment']);
    }