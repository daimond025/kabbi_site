<?php

namespace app\modules\community\widgets;

use yii\helpers\Html;

/**
 * The block contains a set of links
 */
class LinkBlock extends \yii\base\Widget
{

    public $cssClass;
    public $title;
    public $links;

    public function run()
    {
        return $this->renderContent();
    }

    public function renderContent()
    {

        $lines = [];
        if (empty($this->links)) {
            return;
        }
        foreach ($this->links as $link) {
            if (!is_array($link)) {
                $lines[] = Html::tag('li', $link);
            } else {
                $lines[] = Html::tag('li', $link['html'], $link['isActive'] ? ['class' => 'active'] : []);
            }

        }

        return Html::tag('div',
            Html::tag('h3', $this->title)
            . Html::tag('ul', implode("\n", $lines)),
            ['class' => $this->cssClass]);
    }
}
