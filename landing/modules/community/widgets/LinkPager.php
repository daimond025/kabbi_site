<?php

namespace app\modules\community\widgets;

use yii\helpers\Html;


class LinkPager extends \yii\widgets\LinkPager
{
    public function init() {
        $this->nextPageLabel = 'Ранее';
        $this->prevPageLabel = 'Позднее';

        parent::init();
    }

    protected function renderPageButton($label, $page, $class, $disabled, $active) {
        $tag = Html::tag('span',
            ($class === $this->prevPageCssClass ? Html::tag('i') : '')
                . Html::tag('span', $label)
                . ($class === $this->nextPageCssClass ? Html::tag('i') : ''),
            [ 'class' => $class ]
            );

        if ($disabled) {
            return $tag;
        }
        $linkOptions = $this->linkOptions;
        $linkOptions['data-page'] = $page;

        return Html::a($tag, $this->pagination->createUrl($page), $linkOptions);
    }

    protected function renderPageButtons() {
        $pageCount = $this->pagination->getPageCount();
        if ($pageCount < 2 && $this->hideOnSinglePage) {
            return '';
        }

        $buttons = [];
        $currentPage = $this->pagination->getPage();

        // prev page
        if ($this->prevPageLabel !== false) {
            if (($page = $currentPage - 1) < 0) {
                $page = 0;
            }
            $buttons[] = $this->renderPageButton($this->prevPageLabel, $page, $this->prevPageCssClass, $currentPage <= 0, false);
        }

        // next page
        if ($this->nextPageLabel !== false) {
            if (($page = $currentPage + 1) >= $pageCount - 1) {
                $page = $pageCount - 1;
            }
            $buttons[] = $this->renderPageButton($this->nextPageLabel, $page, $this->nextPageCssClass, $currentPage >= $pageCount - 1, false);
        }

        return Html::tag('div', implode("\n", $buttons), $this->options);
    }

}
