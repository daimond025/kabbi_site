<?php

use yii\helpers\Html;

?>

<div class="cmi_rating js-rating
    <?= !$offerInfo->canVote() ? 'rat_disabled' : ''; ?>">

    <b class="js-rating-value"><?= $offerInfo->plus - $offerInfo->minus; ?></b>

    <?= Html::a('',
            ['/community/offers/plus', 'id' => $offerInfo->offer_info_id],
            ['class' => 'cmi_plus js-plus ' . $activePlus]); ?>

    <?= Html::a('',
            ['/community/offers/minus', 'id' => $offerInfo->offer_info_id],
            ['class' => 'cmi_minus js-minus ' . $activeMinus]); ?>
</div>