<?php

use yii\helpers\Html;

?>

<div class="big_cm_rating js-rating
    <?= !$offerInfo->canVote() ? 'rat_disabled' : ''; ?>">

    <div class="cmr_fitst">
        <b>Голосование за идею</b>
        <?= Html::a('<i></i>' . Html::tag('span', intval($offerInfo->plus), ['class' => 'js-plus-value']),
                    ['/community/offers/plus', 'id' => $offerInfo->offer_info_id],
                    ['class' => 'cmr_plus js-plus ' . $activePlus]); ?>

        <?= Html::a('<i></i>' . Html::tag('span', intval($offerInfo->minus), ['class' => 'js-minus-value']),
                    ['/community/offers/minus', 'id' => $offerInfo->offer_info_id],
                    ['class' => 'cmr_minus js-minus ' . $activeMinus]); ?>
    </div>

    <div class="cmr_sec">
        <b>Рейтинг</b>
        <span class="js-rating-value"><?= $offerInfo->plus - $offerInfo->minus; ?></span>
    </div>
</div>