<?php

use landing\modules\community\models\Vote;
use landing\modules\community\widgets\vote\assets\VoteAsset;

VoteAsset::register($this);


$vote = $offerInfo->userVote->vote;

if ($vote === Vote::VOTE_PLUS) {
    $activePlus = 'active';
}

if ($vote === Vote::VOTE_MINUS) {
    $activeMinus = 'active';
}

$params = [
    'offerInfo' => $offerInfo,
    'urlPlus' => $urlPlus,
    'urlMinus' => $urlMinus,
    'activePlus' => $activePlus,
    'activeMinus' => $activeMinus,
];

if ($small) {
    echo $this->render('_small', $params);
} else {
    echo $this->render('_normal', $params);
}