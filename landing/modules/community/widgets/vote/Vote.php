<?php

namespace app\modules\community\widgets\vote;


class Vote extends \yii\base\Widget {

    public $offerInfo;
    public $small;

    public function run() {
        return $this->render('index', [
            'offerInfo' => $this->offerInfo,
            'small' => !empty($this->small),
        ]);
    }
}
