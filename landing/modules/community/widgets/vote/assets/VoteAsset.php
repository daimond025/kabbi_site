<?php

namespace landing\modules\community\widgets\vote\assets;

use yii\web\AssetBundle;

class VoteAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/community/widgets/vote/assets/';
    public $publishOptions = [
        'forceCopy' => true,
    ];
    public $css = [
    ];
    public $js = [
        'js/main.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}