(function ($) {
    'use strict';

    var app = {

        init: function () {
            this.registerVoteEvents();
        },

        registerVoteEvents: function () {
            $('.js-rating .js-plus, .js-rating .js-minus').click(function () {
                var $this = $(this),
                    $rating = $this.closest('.js-rating');

                if ($rating.hasClass('rat_disabled')
                        || $rating.attr('processing')
                        || $this.hasClass('active')) {
                    return false;
                }

                $rating.attr('processing', true);
                $.post($this.attr('href'))
                        .done(function (data) {
                            if (data) {
                                if (data.needAuthorization) {
                                    $('#login-modal').remodal().open();
                                    $rating.attr('processing', null);
                                    return;
                                }

                                if (data.activePlus) {
                                    $rating.find('.js-plus').addClass('active');
                                } else {
                                    $rating.find('.js-plus').removeClass('active');
                                }

                                if (data.activeMinus) {
                                    $rating.find('.js-minus').addClass('active');
                                } else {
                                    $rating.find('.js-minus').removeClass('active');
                                }

                                $rating.find('.js-rating-value').text(data.ratingValue);
                                $rating.find('.js-plus-value').text(data.plusValue);
                                $rating.find('.js-minus-value').text(data.minusValue);

                                $rating.attr('processing', null);
                            }
                        })
                        .fail(function (data) {
                            $rating.attr('processing', null);
                            console.log('fail', data);
                        });

                return false;
            });
        }
    };

    app.init();

})(jQuery);