<?php

namespace app\modules\community\widgets;

use yii\helpers\Html;
use landing\modules\community\models\OfferInfo;


class OfferStatusTabs extends \yii\base\Widget {

    const OFFER_STATUSES_CACHE = 'offer statuses';

    public function run() {
        return $this->renderContent();
    }

    public function renderContent() {

        $currentStatus = get('status') ? get('status') : OfferInfo::DEFAULT_STATUS;

        $statuses = app()->cache->get(OfferStatusTabs::OFFER_STATUSES_CACHE);
        if (!$statuses) {
            $statuses = [];
            foreach (OfferInfo::$statuses as $status) {
                $statuses[] = [
                    'status' => $status,
                    'label' => OfferInfo::$statusLabels[$status],
                    'count' => OfferInfo::find()->byStatus($status)
                        ->publishedPosts()->count(),
                ];
            }
            app()->cache->set(OfferStatusTabs::OFFER_STATUSES_CACHE, $statuses,
                    60 * 60, new \yii\caching\TagDependency([
                        'tags' => OfferInfo::CHANGED_STATUS_CACHE_TAG,
                    ]));
        }

        $items = [];
        foreach ($statuses as $status) {
            $items[] = Html::tag('li', Html::a($status['label'], [
                    '/community/offers/index', 'status' => $status['status'],
                ]) . Html::tag('sup', $status['count']), [
                    'class' => $currentStatus === $status['status'] ? 'active' : '',
                ]);
        }

        return Html::tag('div', Html::tag('ul', implode("\n", $items)), [
                    'class' => 'community_menu',
        ]);
    }

}