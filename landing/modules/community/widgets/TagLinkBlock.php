<?php

namespace app\modules\community\widgets;

use yii\helpers\Url;
use yii\helpers\Html;
use landing\modules\community\models\Tag;
use landing\modules\community\models\Post;

/*
 * The block contains set of tag links
 */

class TagLinkBlock extends LinkBlock
{
    const TAG_LIMIT = 4;

    private function getLinks()
    {
        $tags = app()->db->cache(function () {
            return Tag::find()
                ->choosedTags()
                ->orderBy('name')
                ->limit(self::TAG_LIMIT)->all();
        }, 60 * 60, new \yii\caching\TagDependency([
            'tags' => [Tag::className(), Post::className()],
        ]));

        $links   = [];
        $links[] = [
            'html'     => Html::a('Все', Url::to(['/community/blog'])),
            'isActive' => empty(get('tag_id')),
        ];

        foreach ($tags as $tag) {
            $links[] = [
                'html'     => Html::a(Html::encode($tag->name),
                    Url::to(['/community/community/search', 'tag_id' => $tag->tag_id])),
                'isActive' => $tag->tag_id == get('tag_id'),
            ];
        }

        return $links;
    }

    public function init()
    {
        $this->title = 'Рубрики';
        $this->links = $this->getLinks();

        parent::init();
    }

}