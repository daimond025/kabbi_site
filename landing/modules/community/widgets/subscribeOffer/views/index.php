<?php

use yii\helpers\Html;
use landing\modules\community\widgets\subscribeOffer\assets\SubscribeAsset;

SubscribeAsset::register($this);

?>

<?= Html::beginForm('/community/offers/subscribe', 'post', [
    'id' => 'subscribe-offer-form',
]); ?>

<label>
    <?= Html::hiddenInput('offer_info_id', $offerInfo->offer_info_id); ?>
    <?= Html::checkbox('subscribe', isset($offerInfo->userSubscribeOffer), [
        'id' => 'subscribe-offer',
        'value' => 1,
    ]); ?>
    Получать информацию об изменениях на эл.почту
</label>

<?= Html::endForm(); ?>