<?php

namespace landing\modules\community\widgets\subscribeOffer\assets;

use yii\web\AssetBundle;

class SubscribeAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/community/widgets/subscribeOffer/assets/';
    public $publishOptions = [
        'forceCopy' => true,
    ];
    public $css = [
    ];
    public $js = [
        'js/main.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}