(function ($) {
    'use strict';

    var module = {
        init: function () {
            this.registerSubscribeEvents();
        },
        registerSubscribeEvents: function () {
            var $checkbox = $('#subscribe-offer'),
                $form = $('#subscribe-offer-form');

            $checkbox.click(function () {
                if ($form.attr('processing')) {
                    return false;
                }
                $form.submit();
            });

            $form.submit(function (e) {
                if ($form.attr('processing')) {
                    return false;
                }

                $form.attr('processing', true);
                $.post(
                        $form.attr('action'),
                        $form.serialize())

                        .done(function (data) {
                            $form.attr('processing', null);
                            if (data === 'error') {
                                $checkbox.attr('checked', !!$checkbox.attr('checked'));
                            }
                        })
                        .fail(function (data) {
                            $form.attr('processing', null);
                            $checkbox.attr('checked', !!$checkbox.attr('checked'));
                            console.log('fail', data);
                        });
                return false;
            });
        }
    };

    module.init();
})(jQuery);