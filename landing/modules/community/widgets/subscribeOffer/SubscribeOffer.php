<?php

namespace app\modules\community\widgets\subscribeOffer;

class SubscribeOffer extends \yii\base\Widget {

    public $offerInfo;

    public function run() {
        return $this->render('index', [
            'offerInfo' => $this->offerInfo,
        ]);
    }
}
