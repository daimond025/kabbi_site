<?php

namespace app\modules\community\widgets;

use yii\db\Query;
use yii\helpers\Url;
use yii\helpers\Html;
use landing\modules\community\models\user\CommunityUser;
use landing\modules\community\models\Post;

/*
 * The block contains set of author links
 */
class AuthorLinkBlock extends LinkBlock
{
    const AUTHOR_LIMIT = 4;

    private function getLinks()
    {
        $authors = app()->db->cache(function () {
            return CommunityUser::find()
                            ->postAuthors()
                            ->orderBy('lastname, firstname')
                            ->limit(self::AUTHOR_LIMIT)->all();
        }, 60 * 60, new \yii\caching\TagDependency([
            'tags' => [CommunityUser::className(), Post::className()]
                ]));

        $links = [];
        foreach ($authors as $author) {
            $links[] = Html::a(Html::encode($author->lastname . ' ' . $author->firstname),
                    Url::to(['/community/profile/view', 'id' => $author->id]));
        }

        return $links;
    }

    public function init() {
        $this->title = 'Авторы';
        $this->links = $this->getLinks();

        parent::init();
    }

}