(function ($) {
    'use strict';

    var app = {

        init: function () {
            this.registerSelectFileEvent();
            this.registerDeleteLogoClickEvent();
            this.registerFileInputChangeEvent();
        },

        registerSelectFileEvent: function () {
            $('.il_select_file').click(function () {
                $('#userprofile-image').click();
                return false;
            });
        },

        registerDeleteLogoClickEvent: function () {
            $('.il_delete_file').click(function () {
                var $fileinput = $('#userprofile-image');

                $(this).hide();
                $fileinput.replaceWith($fileinput.val('').clone(true));
                $('#userprofile-photo').val('sadf');
                $('.il_image img').attr('src', '/images/no_image.png');

                return false;
            });
        },

        registerFileInputChangeEvent: function () {
            if (window.File && window.FileReader && window.FileList) {
                $('#userprofile-image').change(function () {
                    var files = this.files,
                        reader;

                    $('#logo_error').hide();

                    if (!files.length) {
                        return false;
                    }

                    if (files[0].type !== 'image/png'
                            && files[0].type !== 'image/jpg'
                            && files[0].type !== 'image/jpeg') {
                        $('#logo_error').text('Логотип должен быть jpeg, либо png!').show();
                        return false;
                    }

                    reader = new FileReader();
                    reader.onload = (function (file) {
                        return function (e) {
                            var thumb = new Image();

                            thumb.src = e.target.result;
                            thumb.onload = function () {
                                if (this.width < 300 || this.height < 300) {
                                    $('#logo_error').text('Размер логотипа должен быть больше 300x300 px!').show();
                                    return false;
                                }

                                $('.il_image img').attr('src', e.target.result);
                                $('#logo_orig').attr('src', e.target.result);
                                $('#logo_preview').attr('src', e.target.result);

                                var imgAreaSelect = null;

                                $.colorbox({
                                    inline: true,
                                    href: '#logo_resize',
                                    onClosed: function(){
                                        imgAreaSelect.cancelSelection();
                                        $('.il_delete_file').show();
                                    },
                                    onComplete: function(){
                                        imgAreaSelect = $('#logo_orig').imgAreaSelect({
                                            x1: 0,
                                            y1: 0,
                                            x2: 300,
                                            y2: 300,
                                            minWidth: 300,
                                            minHeight: 300,
                                            aspectRatio: '1:1',
                                            instance: true,
                                            imageHeight: thumb.height,
                                            imageWidth: thumb.width,
                                            persistent: true,
                                            onSelectEnd: function (img, selection) {
                                                $('input[name="Crop[x]"]').val(selection.x1);
                                                $('input[name="Crop[y]"]').val(selection.y1);
                                                $('input[name="Crop[h]"]').val(selection.height);
                                                $('input[name="Crop[w]"]').val(selection.width);
                                            },
                                            onSelectChange: function (img, selection) {
                                                var scaleX = 100 / (selection.width || 1);
                                                var scaleY = 100 / (selection.height || 1);

                                                $('#logo_preview').css({
                                                    width: Math.round(scaleX * thumb.width) + 'px',
                                                    height: Math.round(scaleY * thumb.height) + 'px',
                                                    marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px',
                                                    marginTop: '-' + Math.round(scaleY * selection.y1) + 'px'
                                                });
                                            }
                                        });
                                    }
                                });
                            };
                        };
                    })(files[0]);
                    reader.readAsDataURL(files[0]);
                });
            }

            $('#choose_size').on('click', function(){
                $.colorbox.close();
            });
        }
    };

    app.init();
})(jQuery);