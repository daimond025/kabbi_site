<?php

namespace landing\modules\community\widgets\logo\assets;

use yii\web\AssetBundle;

class LogoAsset extends AssetBundle
{
    public $sourcePath = '@landing/modules/community/widgets/logo/assets/';

    public $publishOptions = [
        'forceCopy' => true
    ];

    public $css = [
        'css/imgareaselect-default.css'
    ];
    public $js = [
        'js/jquery.imgareaselect.pack.js',
        'js/main.js'
    ];
    public $depends = [
        'landing\assets\AppAsset',
    ];
}