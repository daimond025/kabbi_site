<?php

namespace landing\modules\community\widgets\logo;

use landing\modules\community\widgets\logo\assets\LogoAsset;

class Logo extends \yii\base\Widget
{

    /**
     * @var object yii\widgets\ActiveForm
     */
    public $form;
    public $model;
    public $field;
    public $image;

    public function init()
    {
        parent::init();

        LogoAsset::register($this->getView());
    }

    public function run()
    {
        return $this->render('index', [
                    'form' => $this->form,
                    'model' => $this->model,
                    'field' => $this->field,
                    'image' => $this->image,
        ]);
    }

}
