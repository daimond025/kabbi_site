<?php

use yii\helpers\Html;

?>


<div class="images_loader">
    
    <div class="il_image">
        <?php
            $filename = $model->getThumbFilename($model->$field);
            $showLogo = is_file($filename) && file_exists($filename);
            if ($showLogo) {
                echo '<a class="logo profile-logo" href="' . $model->getFilelink($model->$field) .'">'
                        . Html::img($model->getThumbFilelink($model->$field), ['alt' => 'photo'])
                        . '</a>';
            } else {
                echo Html::img('/images/no_image.png', ['alt' => 'no image']);
            }
        ?>
    </div>
    <?= $form->field($model, $field, ['options' => ['style' => 'display:none']])
                ->hiddenInput()->label(''); ?>
    <div class="il_links">
        <?= $form->field($model, $image)->begin(); ?>
            <a class="il_select_file">Загрузить фото</a>
            <a class="il_delete_file" style="<?= $showLogo ? '' : 'display: none'; ?>">Удалить</a>
            <p id="logo_error" style="color: red; text-align: center; display: none"></p>
            <?= Html::activeFileInput($model, $image, [
                'style' => 'display: none',
                'accept' => '.jpg, .png, .jpeg',
            ]); ?>
        <?= $form->field($model, $image)->end(); ?>

        <?= Html::hiddenInput('Crop[x]', 0); ?>
        <?= Html::hiddenInput('Crop[y]', 0); ?>
        <?= Html::hiddenInput('Crop[w]', 300); ?>
        <?= Html::hiddenInput('Crop[h]', 300); ?>

        <div style="display: none">
            <div id="logo_resize">
                <h3>Выделите нужный фрагмент</h3>
                <div>
                    <img id="logo_orig" src="" alt="" title="" style="max-width: 800px; max-height: 800px;" />
                    <div class="preview">
                        <img id="logo_preview" style="position: relative;" src="" alt="" title="" />
                    </div>
                </div>
                <button id="choose_size" style="margin-top: 10px">Выбрать</button>
            </div>
        </div>
    </div>
</div>