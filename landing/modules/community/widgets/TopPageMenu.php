<?php

namespace app\modules\community\widgets;

use yii\helpers\Html;


class TopPageMenu extends \yii\base\Widget {

    public $links;

    public function init() {
        if (!isset($this->links)) {
            $this->links = [
                [
                    'title' => 'Блог',
                    'controller' => 'blog',
                ],
                [
                    'title' => 'Предложения',
                    'controller' => 'offers',
                ],
                [
                    'title' => 'Общение',
                    'controller' => 'communion',
                ],
            ];
        }
        parent::init();
    }

    public function run() {
        return $this->renderContent();
    }

    public function renderContent() {

        $lines = [];
        if (empty($this->links)) {
            return;
        }
        foreach ($this->links as $link) {
            $url = ['/community/' . $link['controller'] . '/index'];
            $lines[] = Html::tag('li', Html::a($link['title'], $url), [
                'class' => app()->controller->id === $link['controller'] ? 'active' : '',
            ]);
        }

        return Html::tag('ul', implode("\n", $lines), ['class' => 'com_head_menu']);
    }
}
