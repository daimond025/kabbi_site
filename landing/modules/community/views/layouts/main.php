<?php

use app\modules\community\widgets\TagLinkBlock;


$title = 'Блог Гутакса';
if ($this->context->id == 'blog') {
    $title = 'Блог Гутакса';
}

?>

<?php $this->beginContent('@app/views/layouts/main.php'); ?>

<header class="page_header">
    <div class="content">
        <h1><?= $title; ?></h1>
        <? /* TopPageMenu::widget();*/ ?>
    </div>
</header>

<section class="blogs">
    <div class="content">
        <div class="right_side">
            <? /*
            <?= Html::a(Html::tag('i') . 'RSS Подписка',
                Url::to(['/community/rss'], true),
                ['class' => 'subscribe', 'target' => '_blank', 'rel' => 'nofollow noopener']); ?>

            <?php if (isset($this->params['controlPanel'])): ?>
                <div class="post_actions">
                    <h3>Управление</h3>
                    <ul><?= implode("\n", $this->params['controlPanel']); ?></ul>
                </div>
            <?php endif; ?>
                <?= AuthorLinkBlock::widget(['cssClass' => 'right_block']); ?>
            */?>
            <?= $this->render('@app/modules/community/views/community/_searchForm'); ?>
            <?= TagLinkBlock::widget(['cssClass' => 'right_block']); ?>

        </div>
        <div class="left_side">
            <?= $content; ?>
        </div>

    </div>
    <div style="clear: both"></div>
</section>

<?php $this->endContent(); ?>