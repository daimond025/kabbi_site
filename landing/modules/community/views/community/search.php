<?php

use yii\helpers\Html;
use landing\modules\community\models\Section;
use app\modules\community\widgets\LinkPager;

$this->params['topPageTitle'] = 'Сообщество Гутакса';

?>
<div class="search_line">
    Результаты поиска
</div>
<div class="list-view">
<?php
$models = $dataProvider->getModels();
if (!empty($models)) {
    $currentSectionId = 1;

    foreach ($models as $model) {
        if ($currentSectionId == $model->section_id) {
            $controller = Section::$controllers[$model->section_id];

            $view = $model->section_id === Section::OFFERS
                ? '/offers/_postItem' : '/post/_postItem';
            echo $this->render($view, ['model' => $model, 'controller' => $controller]);
        }
    }
} else {
    echo Html::tag('div', t('community', 'Empty'), ['class' => 'blog_mini_item']);
}
?>
</div>

<?= LinkPager::widget([
        'pagination' => $dataProvider->pagination,
    ]); ?>
