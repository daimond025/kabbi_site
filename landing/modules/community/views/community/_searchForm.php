<?php

use yii\helpers\Url;
use yii\helpers\Html;

?>

<div class="big_search">
    <?php
    echo Html::beginForm(Url::to(['/community/community/search']), 'GET');
    echo Html::textInput('query', get('query'), ['placeholder' => 'Поиск']);
    echo Html::submitButton('Искать', ['class' => 'button']);
    echo Html::endForm();
    ?>
</div>
