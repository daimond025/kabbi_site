<?php

use app\modules\community\widgets\LinkBlock;

$this->title = 'Сообщество';

$this->params['topPageTitle'] = 'Сообщество';
?>

<b style="display: block; color: #ff8b0f">Новое в разделах</b>

<?php
foreach ($data as $item) {
    echo LinkBlock::widget([
        'cssClass' => 'links_block',
        'title' => $item['title'],
        'links' => $item['links'],
    ]);
    break;
}
?>
