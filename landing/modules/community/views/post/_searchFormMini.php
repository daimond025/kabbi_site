<?php

use yii\helpers\Url;
use yii\helpers\Html;

?>

<?php $this->beginBlock('searchFormMini'); ?>
<div class="search_mini_form">
    <?php
    echo Html::beginForm(Url::to('/community/community/search'), 'GET');
    echo Html::textInput('query', get('query'), ['placeholder' => 'Поиск']);
    echo Html::submitInput();
    echo Html::endForm();
    ?>
</div>
<?php $this->endBlock(); ?>