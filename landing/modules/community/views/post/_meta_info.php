<?php

use yii\helpers\Url;
use yii\helpers\ArrayHelper;

preg_match("/<img[^>]+src\s*=\s*[\"']\/?([^\"']+)[\"'][^>]*\>/",
    $model->anons, $matches);
$image = isset($matches[1]) ? $matches[1] : '';

app()->params['customMetaTags'] = [
    'title'        => $model->title,
    'description'  => $model->meta_description,
    'keywords'     => implode(', ', ArrayHelper::getColumn($model->tags, 'name')),
    'og:site_name' => 'Сообщество Гутакс',
    'og:type'      => 'article',
    'og:title'     => $model->title,
    'og:url'       => Url::to('', true),
    'og:image'     => $image,
];