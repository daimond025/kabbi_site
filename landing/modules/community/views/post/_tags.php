<?php

use yii\helpers\Html;

?>

<ul class="tags">
    <?php foreach ($tags as $tag): ?>
        <li>
            <?= Html::a(Html::encode($tag->name),
                ['/community/community/search', 'tag_id' => $tag->tag_id]); ?>
        </li>
    <?php endforeach; ?>
</ul>