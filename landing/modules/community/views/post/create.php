<?php

use yii\helpers\Html;

$this->title = 'Написать пост или вопрос';
$this->params['topPageTitle'] = Html::a(Html::encode($section->name), [
        $this->context->id . '/index',
    ]);

?>

<?= $this->render('_searchFormMini'); ?>

<h2><?= Html::encode($this->title); ?></h2>

<?= $this->render('_form', ['model' => $model, 'tags' => $tags]); ?>