<?php

use yii\helpers\Url;
use yii\helpers\Html;

?>
<hr class="small_hr">
<div class="blog_mini_item blog-item">

    <h3>

        <?= Html::a(Html::encode($model->title, false), [
                $controller . '/view',
                'id' => $model->post_id
            ]); ?>
    </h3>

    <div class="bmi_first_line">
        


        <?/*
        <a href="<?= Url::to(['/community/profile/view', 'id' => $model->author_id]); ?>" class="bmi_man">
            <?= $model->author->fullName; ?>
        </a>*/?>
    </div>

    <div class="inline">
        <?= $model->anons; ?>
    </div>
    <div class="bmi_info">
        <?/*<a class="bmi_comm" href="<?= Url::to([$controller . '/view', 'id' => $model->post_id, '#' => 'comments']); ?>">
            <span><?= intval($model->count_comments); ?></span><i></i>
        </a>*/?>
        <span class="bmi_time"><?= $model->getPublicationDate(); ?></span>
        <?= $this->render('_tags', ['tags' => $model->tags]); ?>
    </div>




</div>
