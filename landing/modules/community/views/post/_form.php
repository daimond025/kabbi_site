<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;


$fieldConfig = app()->params['fieldOptions'];
$fieldConfig['template'] = "{label}\n{input}\n{error}";
$fieldConfig['options']['class'] = '';

?>

<div class="add_post_page">
    <?php $form = ActiveForm::begin([
        'errorCssClass' => 'error',
        'fieldConfig' => $fieldConfig,
    ]); ?>

    <div class="app_form">
        <?= $form->field($model, 'title')->textInput()->label('Тема'); ?>

        <?php
        $clientOptions = [
            'lang' => 'ru',
            'cleanSpaces' => false,
            'buttonsHide' => ['file'],
            'imageUpload' => ['/community/post/upload-image'],
            'imageManagerJson' => false,
            'imageAllowExtensions'=>['jpg','png','gif'],
            'uploadImageFields' => [
                app()->request->csrfParam => app()->request->getCsrfToken(),
            ],
            'minHeight' => 140,
            'plugins' => ['fullscreen', 'imagemanager'],
            'initCallback' => new JsExpression('function () {
                var undo = this.button.addFirst("undo", "' . t('community', 'Undo') . '");
                var redo = this.button.addAfter("undo", "redo", "' . t('community', 'Redo') . '");
                    
                this.button.addCallback(undo, this.buffer.undo);
                this.button.addCallback(redo, this.buffer.redo);
            }'),
            'pasteCallback' => new JsExpression('function (html) {'
                    . 'return html.replace(/<(\/)?(?=h1|h2|h3|h4|h5|h6|p|cite|quote|blockquote|pre)[^>]*>/, "<$1p>")'
                    . '.replace(/<(\/)?(?!p|img)[^>]*>/gi, " "); }'),
        ];

        echo $form->field($model, 'anons')->widget(\yii\redactor\widgets\Redactor::className(), [
            'clientOptions' => $clientOptions,
        ]);

        echo $form->field($model, 'content')->widget(\yii\redactor\widgets\Redactor::className(), [
            'clientOptions' => $clientOptions,
        ]);
        ?>

    </div>

<!--    <div class="load_files">
        <h3>Загрузить</h3>
        <div style="overflow: hidden">
            <a style="margin-left: 0;" class="load_file">Картинку</a>
            <a class="load_file">Видео</a>
            <a class="load_file">Файл</a>
        </div>
        <ul>
            <li>
                <b><a>Вставить в текст</a></b><i></i><span><a>pobeda.jpg</a></span>
            </li>
            <li>
                <b><a>Вставить в текст</a></b><i></i><span><a>pobeda.jpg</a></span>
            </li>
            <li>
                <b><a>Вставить в текст</a></b><i></i><span><a>pobeda.jpg</a></span>
            </li>
        </ul> 
    </div>-->

    <h3>Теги</h3>
    <?= $form->field($model, 'choosedTags')
        ->checkboxList(ArrayHelper::map($tags, 'tag_id', 'name'), [
            'item' => function ($index, $label, $name, $checked, $value) {
                return Html::tag('li', Html::checkbox($name, $checked, [
                    'value' => $value,
                    'label' => Html::encode($label),
                ]));
            },
            'tag' => 'ul',
            'class' => 'app_tags',
        ])->label(''); ?>

    <div class="app_submit">
        <?= Html::submitButton(!$model->isNewRecord ? 'Сохранить' : 'Написать', [
            'class' => 'button',
        ]); ?>
        <p>Все посты и вопросы проходят модерацию, рекомендуем ознакомиться с нашими <a href="#">правилами</a></p>  
    </div>
    <?php ActiveForm::end(); ?>
</div>