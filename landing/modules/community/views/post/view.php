<?php

use yii\helpers\Url;
use yii\helpers\Html;

use landing\modules\community\models\Section;
use app\modules\community\widgets\comments\Comments;
use landing\modules\community\models\Post;


$this->params['topPageTitle'] = Html::a(Html::encode($section->name), [
    $this->context->id . '/index',
]);


/* @var $model Post */

?>

<?php
if (Section::isPublicSection($section->section_id)
        && $model->isAuthor(user()->id)) {
    $this->params['controlPanel'][] = Html::tag('li',
            Html::a('Редактировать', [$this->context->id . '/update', 'id' => $model->post_id]));

    $this->params['controlPanel'][] = Html::tag('li', Html::a('Удалить', [
                $this->context->id . '/delete',
                'id' => $model->post_id,
            ], [
                'class' => 'pa_del',
                'data' => [
                    'confirm' => t('yii', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                    ]
            ]));
}
?>

<? $this->render('/post/_meta_info', ['model' => $model]); ?>

<?= $this->render('_searchFormMini'); ?>

<div class="blog_mini_item blog_item">
    <div class="full_item_go_back">
        <a href="/community/blog/"><i></i> Вернуться в блог</a>
    </div>
    <h1><?= Html::encode($model->title, false) ?></h1>



    <div class="inline">
        <?= $model->anons . $model->content; ?>
    </div>
    <div class="bmi_info">

        <span class="bmi_time">
            <?= $model->getPublicationDate(); ?>
        </span>
        <?= $this->render('_tags', ['tags' => $model->tags]); ?>


    </div>

</div>

<?/*
<div class="blog_item_comments">
    <h3>Комментарии</h3>
    <ul>
        <li>
            <div class="comment_header">
                <a class="ch_reply">Ответить</a>
                <i><img src="/images/avatar.png" alt="avatar"></i>
                <div>
                    <b>Мария</b>
                    <span>24.05.2016 в 16:13</span>
                </div>
            </div>
            <div class="comment_content">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </div>
        </li>
        <li>
            <div class="comment_header">
                <a class="ch_reply">Ответить</a>
                <i><img src="/images/avatar.png" alt="avatar"></i>
                <div>
                    <b>Мария</b>
                    <span>24.05.2016 в 16:13</span>
                </div>
            </div>
            <div class="comment_content">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </div>
        </li>
    </ul>
</div>
<div class="add_comment">
    <b>Ваш комментарий</b>
    <div class="add_comment_header">
        <div class="lf_input"><input type="text" placeholder="Имя"></div>
        <div class="lf_input"><input type="text" placeholder="E-mail"></div>
    </div>
    <div class="add_comment_content lf_input">
        <textarea rows="5"></textarea>
    </div>
    <input type="submit" value="Отправить" class="button">
</div>*/?>


<? Comments::widget(['post' => $model]); ?>