<?php

use yii\helpers\Url;
use yii\helpers\Html;
use app\modules\community\widgets\comments\Comments;
use landing\modules\community\models\Section;
use landing\modules\community\models\OfferInfo;
use app\modules\community\widgets\subscribeOffer\SubscribeOffer;
use app\modules\community\widgets\vote\Vote;

$this->params['topPageTitle'] = Html::a(Html::encode($section->name), [
    $this->context->id . '/index',
]);

?>

<? $this->render('/post/_meta_info', ['model' => $model]); ?>

<?php
if (Section::isPublicSection($section->section_id)
        && $model->isAuthor(user()->id)
        && (!$model->offerInfo || $model->offerInfo->status === OfferInfo::STATUS_VOTING)) {
    $this->params['controlPanel'][] = Html::tag('li', Html::a('Редактировать', [
                        $this->context->id . '/update',
                        'id' => $model->post_id,
    ]));
    $this->params['controlPanel'][] = Html::tag('li', Html::a('Удалить', [
                $this->context->id . '/delete',
                'id' => $model->post_id,
            ], [
                'class' => 'pa_del',
                'data' => [
                    'confirm' => 'Вы действительно хотите удалить запись?',
                    'method' => 'post',
                    ]
                ]));
}
?>

<?= $this->render('/post/_searchFormMini'); ?>

<div class="blog_mini_item blog_item">
    <h3><?= Html::encode($model->title, false); ?></h3>

    <div class="bmi_first_line">
        <?= $this->render('_currentOfferStatus', ['offerInfo' => $model->offerInfo]); ?>

        <span class="bmi_time">
            <?= $model->getPublicationDate(); ?>
        </span>

        <a href="<?= Url::to(['/community/profile/view', 'id' => $model->author_id]); ?>" class="bmi_man">
            <?= $model->author->fullName; ?>
        </a>
    </div>

    <div class="inline"> 
        <?= $model->anons . $model->content; ?>
        <div class="rating_big_block">
            <?= Vote::widget(['offerInfo' => $model->offerInfo]); ?>

            <?php if (!app()->user->isGuest && isset($model->offerInfo)) {
                echo SubscribeOffer::widget([
                    'offerInfo' => $model->offerInfo,
                ]);
            } ?>
        </div>
    </div>

    <?= $this->render('/post/_tags', ['tags' => $model->tags]); ?>
</div>

<div class="shadow"></div>

<?= Comments::widget(['post' => $model]); ?>