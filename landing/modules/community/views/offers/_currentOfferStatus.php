<?php

use yii\helpers\Html;
use landing\modules\community\models\OfferInfo;

?>

<?php if (isset($offerInfo)): ?>
<b class="bmi_ready">
    <?= $offerInfo->status === OfferInfo::STATUS_INTRODUCED ? Html::tag('i') : ''; ?>
    <?= OfferInfo::$statusLabels[$offerInfo->status]; ?>
</b>
<?php endif; ?>