<?php

use yii\helpers\Url;
use yii\helpers\Html;
use app\modules\community\widgets\vote\Vote;
use landing\modules\community\models\OfferInfo;

?>

<div class="blog_mini_item <?= $this->context->id === 'profile' ? ' profile_com': ''?>">

    <?= Vote::widget(['offerInfo' => $model->offerInfo, 'small' => true]); ?>

    <div class="cmi_content">

        <h3>
            <a class="bmi_comm" href="<?= Url::to([$controller . '/view', 'id' => $model->post_id, '#' => 'comments']); ?>">
                <span><?= intval($model->count_comments); ?></span><i></i>
            </a>
            <?= Html::a(Html::encode($model->title, false), [
                    $controller . '/view',
                    'id' => $model->post_id
                ]); ?>
        </h3>

        <div class="bmi_first_line">
            

            <?php if ($this->context->id === 'profile'): ?>
                <b class="bmi_ready">
                    <?= $model->offerInfo->status === OfferInfo::STATUS_INTRODUCED ? Html::tag('i') : ''; ?>
                    <?= OfferInfo::$statusLabels[$model->offerInfo->status]; ?>
                </b>
            <?php endif; ?>

            <span class="bmi_time">
                <?= $model->getPublicationDate(); ?>
            </span>

            <a href="<?= Url::to(['/community/profile/view', 'id' => $model->author_id]); ?>" class="bmi_man">
                <?= $model->author->fullName; ?>
            </a>
        </div>

        <div class="inline">
            <?= $model->anons; ?>
        </div>

        <?= $this->render('/post/_tags', ['tags' => $model->tags]); ?>

    </div>
</div>
