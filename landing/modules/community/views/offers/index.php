<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use landing\modules\community\models\Section;
use app\modules\community\widgets\LinkPager;
use app\modules\community\widgets\OfferStatusTabs;

$this->params['topPageTitle'] = Html::encode($section->name);

?>

<?php
if (Section::isPublicSection($section->section_id) && !app()->user->isGuest) {
    $this->params['controlPanel'][] = Html::tag('li',
            Html::a(t('community', 'To suggest an idea'), [$this->context->id . '/create']));
}
?>

<?= $this->render('/post/_searchFormMini'); ?>

<?= OfferStatusTabs::widget(); ?>

<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => function ($model) {
        return $this->render('_postItem', ['model' => $model, 'controller' => $this->context->id]);
    },
    'layout' => '{items}',
    'emptyText' => t('community', 'Empty'),
    'emptyTextOptions' => ['class' => 'blog_mini_item'],
]); ?>

<div class="shadow"></div>

<?= LinkPager::widget([
        'pagination' => $dataProvider->pagination,
    ]); ?>