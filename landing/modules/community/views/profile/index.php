<?php

use yii\widgets\ListView;
use landing\modules\community\models\Section;
use app\modules\community\widgets\LinkPager;

$this->params['topPageTitle'] = 'Профиль';

?>

<?= $this->render('/post/_searchFormMini'); ?>

<?= $this->render('_profileTabs', ['userId' => $userId]); ?>

<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => function ($model) {
        $view = $model->section_id == Section::OFFERS
                ? '/offers/_postItem' : '/post/_postItem';
        return $this->render($view, [
                    'model' => $model,
                    'controller' => Section::$controllers[$model->section_id],
        ]);
    },
    'layout' => '{items}',
    'emptyText' => t('community', 'Empty'),
    'emptyTextOptions' => ['class' => 'blog_mini_item'],
]); ?>

<div class="shadow"></div>

<?= LinkPager::widget([
        'pagination' => $dataProvider->pagination,
    ]); ?>