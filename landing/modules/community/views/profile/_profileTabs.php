<?php

use yii\helpers\Html;

?>

<ul class="profile_menu">
    <?php
        $currentRoute = '/' . app()->requestedRoute;

        $items = [
            ['url' => '/community/profile/update', 'label' => 'Профиль'],
            ['url' => '/community/profile/offers', 'label' => 'Предложения'],
            ['url' => '/community/profile/communion', 'label' => 'Общение'],
        ];

        if (isset($userId)) {
            $items[0]['url'] = '/community/profile/view';
        }

        foreach($items as $item) {
            echo Html::tag('li', Html::a($item['label'], [$item['url'], 'id' => $userId]), [
                'class' => $item['url'] === $currentRoute ? 'active' : '',
                ]);
        }
    ?>
</ul>