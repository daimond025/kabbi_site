<?php

use yii\helpers\Html;

$this->params['topPageTitle'] = 'Профиль';

?>

<?= $this->render('_profileTabs', ['userId' => $userId]); ?>

<div class="edit_profile">
    <div class="images_loader">
        <div class="il_image">
            <?php
                $userProfile = $user->userProfile;
                if ($userProfile) {
                    $filename = $userProfile->getThumbFilename($userProfile->photo);
                }

                if (is_file($filename) && file_exists($filename)) {
                    echo '<a class="logo profile-logo" href="' . $userProfile->getFilelink($userProfile->photo) . '">'
                            . Html::img($userProfile->getThumbFilelink($userProfile->photo))
                            . '</a>';
                } else {
                    echo Html::img('/images/no_image.png', ['alt' => $user->fullName]);
                }
            ?>
        </div>
    </div>
    <?= Html::encode($user->lastname . ' ' . $user->firstname . ' ' . $user->secondname); ?>
</div>