<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->params['topPageTitle'] = 'Профиль';

$fieldConfig = Yii::$app->params['fieldOptions'];
$fieldConfig['template'] = "{label}\n{input}\n{error}";

$isGootaxClient = isset($user->user_id);

if ($isGootaxClient) {
    $placeholder = t('community', 'Fill in Gootax');
}

?>

<?= $this->render('_profileTabs', ['userId' => $userId]); ?>

<?php $form = ActiveForm::begin([
        'fieldConfig' => $fieldConfig,
        'enableClientValidation' => false,
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>

<div class="edit_profile">

    <?=
        \landing\modules\community\widgets\logo\Logo::widget([
            'form' => $form,
            'model' => $profile,
            'field' => 'photo',
            'image' => 'image',
        ]);
    ?>

    <?= $form->field($user, 'email')->textInput(['disabled' => $isGootaxClient])->label('Эл.почта*'); ?>
    <div class="ep_cols">
        <div class="ep_col">
            <?= $form->field($user, 'lastname')->textInput([
                    'disabled' => $isGootaxClient,
                    'placeholder' => $placeholder,
                ])->label('Фамилия*'); ?>
        </div>
        <div class="ep_col">
            <?= $form->field($user, 'firstname')->textInput([
                    'disabled' => $isGootaxClient,
                    'placeholder' => $placeholder,
                ])->label('Имя*'); ?>
        </div>
        <div class="ep_col">
            <?= $form->field($user, 'secondname')->textInput([
                    'disabled' => $isGootaxClient,
                    'placeholder' => $placeholder,
                ])->label('Отчество'); ?>
        </div>
    </div>

    <?php if (!$isGootaxClient) {
        echo $form->field($resetPasswordForm, 'password')->passwordInput()->label('Изменить пароль');
        echo $form->field($resetPasswordForm, 'password_repeat')->passwordInput()->label('Повторите пароль');
    } ?>

    <h3>Дополнительная информация</h3>
    <?= $form->field($profile, 'company')->textInput([
                'disabled' => $isGootaxClient,
                'placeholder' => $placeholder,
            ])
            ->label('Компания, в которой вы работаете'); ?>
    <?= $form->field($profile, 'position')->textInput([
            'disabled' => $isGootaxClient,
            'placeholder' => $placeholder,
        ])->label('Должность'); ?>
    <?= $form->field($profile, 'phone')->textInput([
            'class' => 'mask_phone',
            'disabled' => $isGootaxClient,
            'placeholder' => $placeholder,
        ])->label('Ваш телефон'); ?>
    <?= $form->field($profile, 'automation_program')->textInput()
            ->label('Какой программой автоматизации пользуется ваша компания?'); ?>
    <?= $form->field($profile, 'community_participation')->textarea(['rows' => 5])
            ->label('Расскажите почему вам интересно участвовать в сообществе Гутакса'); ?>

    <div class="app_submit">
        <?= Html::submitInput('Сохранить', [
            'class' => 'button',
            'disabled' => !$confirmPersonalDataProcessing
        ]); ?>

        <p style="padding-top: 10px;">
            <label class="btn_trigger">
                <input type="checkbox" name="confirmPersonalDataProcessing"
                    <?= $confirmPersonalDataProcessing ? 'checked' : ''; ?>/>
                Соглашаюсь на обработку персональных данных
            </label>
        </p>
    </div>
</div>

<?php activeForm::end(); ?>