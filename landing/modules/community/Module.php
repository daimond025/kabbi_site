<?php

namespace app\modules\community;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\community\controllers';

    public $defaultRoute = 'community';

    public $layout = 'main';

    public function init()
    {
        parent::init();
    }

}
