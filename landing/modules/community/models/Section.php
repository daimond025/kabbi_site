<?php

namespace landing\modules\community\models;

use Yii;
use yii\helpers\Html;
use landing\modules\community\components\behaviors\ClearCacheBehavior;


/**
 * This is the model class for table "{{%section}}".
 *
 * @property integer $section_id
 * @property string $code
 * @property string $name
 *
 * @property Post[] $posts
 */
class Section extends \yii\db\ActiveRecord
{
    const BLOG = 1;
    const OFFERS = 2;
    const COMMUNION = 3;
    const DOCS = 4;

    public static $controllers = [
        self::BLOG => 'blog',
        self::OFFERS => 'offers',
        self::COMMUNION => 'communion',
        self::DOCS => 'docs',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%section}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'section_id' => 'Ключ',
            'name' => 'Название',
        ];
    }

    public function behaviors() {
        return [
            'clearCacheBehavior' => [
                'class' => ClearCacheBehavior::className(),
                'tags' => [Section::className()],
            ]
        ];
    }

    public function getLabel()
    {
        return Html::encode($this->section_id . ' (' . $this->name . ')');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['section_id' => 'section_id']);
    }

    public static function isPublicSection($sectionId)
    {
        return in_array($sectionId, [self::COMMUNION, self::OFFERS]);
    }

    public static function getCachedSection($sectionId)
    {
        return app()->db->cache(function () use ($sectionId) {
                    return Section::find()
                            ->andWhere(['section_id' => $sectionId])->one();
                }, 0, new \yii\caching\TagDependency(['tags' => Section::className()]));
    }

}
