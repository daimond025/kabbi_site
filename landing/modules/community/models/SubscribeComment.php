<?php

namespace landing\modules\community\models;

use Yii;

/**
 * This is the model class for table "{{%subscribe_comment}}".
 *
 * @property integer $id
 * @property integer $community_user_id
 * @property integer $post_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property CommunityUser $communityUser
 * @property Post $post
 */
class SubscribeComment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%subscribe_comment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['community_user_id', 'post_id'], 'required'],
            [['community_user_id', 'post_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }

    public function behaviors() {
        return [
            \yii\behaviors\TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'community_user_id' => 'Community User ID',
            'post_id' => 'Post ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommunityUser()
    {
        return $this->hasOne(CommunityUser::className(), ['id' => 'community_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['post_id' => 'post_id']);
    }

    public static function find() {
        return new SubscribeCommentQuery(get_called_class());
    }

}

class SubscribeCommentQuery extends \yii\db\ActiveQuery
{
    public function byPostId($postId)
    {
        return $this->andWhere(['post_id' => $postId]);
    }
}
