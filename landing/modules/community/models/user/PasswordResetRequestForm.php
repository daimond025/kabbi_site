<?php

namespace landing\modules\community\models\user;

use common\components\serviceApi\EmailTypes;
use common\components\serviceApi\ServiceApi;
use Yii;
use yii\helpers\Url;
use yii\base\Model;

class PasswordResetRequestForm extends Model
{
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required', 'message' => 'Не заполнено'],
            ['email', 'email', 'message' => 'Неправильное значение'],
            ['email', 'validateEmail']
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'Эл. почта'
        ];
    }

    public function validateEmail($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = CommunityUser::findByEmail($this[$attribute]);
            if (!$user) {
                $this->addError($attribute, 'Несуществующий адрес эл. почты!');
            }
        }
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function sendEmail()
    {
        /* @var $user CommunityUser */
        $user = CommunityUser::findOne([
            'status' => CommunityUser::STATUS_ACTIVE,
            'user_id' => null,
            'email' => $this->email,
        ]);

        if ($user) {
            if (!CommunityUser::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
            }

            if ($user->save()) {
                /** @var ServiceApi $serviceApi */
                $serviceApi = app()->get('serviceApi');

                $result = $serviceApi->sendEmail(null, null, EmailTypes::PASSWORD_RESET_TOKEN, null,
                    $this->email, [
                        'user_name' => $user->lastname . ' ' . $user->firstname,
                        'url'       => Url::to([
                            '/site/community-reset-password',
                            'token' => $user->password_reset_token
                        ], true),
                    ]);

                return $result;
            }
        }

        return false;
    }

}