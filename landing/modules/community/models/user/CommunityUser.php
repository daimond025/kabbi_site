<?php

namespace landing\modules\community\models\user;

use Yii;
use yii\db\Query;
use yii\helpers\Html;
use landing\modules\community\models\Post;
use landing\modules\community\models\UserProfile;
use landing\modules\community\components\behaviors\ClearCacheBehavior;

/**
 * This is the model class for table "{{%community_user}}".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $email_confirm_token
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property CommunityAuth[] $communityAuths
 */
class CommunityUser extends \yii\db\ActiveRecord
    implements \yii\web\IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    const STATUS_INACTIVE = 20;

    public static $statuses = [
        self::STATUS_DELETED,
        self::STATUS_INACTIVE,
        self::STATUS_ACTIVE,
    ];

    public static $statusLabels = [
        self::STATUS_DELETED => 'Заблокирован',
        self::STATUS_INACTIVE => 'Неактивный',
        self::STATUS_ACTIVE => 'Активный',
    ];

    const SCENARIO_GOOTAX_CLIENT = 'gootax client';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%community_user}}';
    }

    public function scenarios() {
        return array_merge(parent::scenarios(), [
            self::SCENARIO_GOOTAX_CLIENT => [],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'auth_key', 'password_hash', 'email'], 'required'],
            ['status', 'in', 'range' => self::$statuses],
            ['status', 'default', 'value' => self::STATUS_INACTIVE],
            ['email', 'email'],
            [['firstname', 'lastname', 'secondname',
                'password_hash', 'password_reset_token',
                'email_confirm_token'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                   => 'Ключ',
            'firstname'            => 'Имя',
            'lastname'             => 'Фамилия',
            'secondname'           => 'Отчество',
            'auth_key'             => 'Auth Key',
            'password_hash'        => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email'                => 'Эл. почта',
            'status'               => 'Статус',
            'created_at'           => 'Дата создания',
            'updated_at'           => 'Дата редактирования',
        ];
    }

    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            'clearCacheBehavior' => [
                'class' => ClearCacheBehavior::className(),
                'tags' => [CommunityUser::className()],
            ]
        ];
    }

    public function getFullName()
    {
        return Html::encode($this->lastname . ' ' . $this->firstname);
    }

    public function getLabel()
    {
        return Html::encode($this->id
            . ' (' . $this->lastname . ' ' . $this->firstname
            . ' [' . $this->email . '])');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommunityAuths()
    {
        return $this->hasMany(CommunityAuth::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserProfile()
    {
        return $this->hasOne(UserProfile::className(), ['community_user_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public static function findByEmail($email)
    {
        return static::findOne([
            'email' => $email,
            'status' => self::STATUS_ACTIVE,
            'user_id' => null,
            ]);
    }

    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public static function findByEmailConfirmToken($token)
    {
        return static::findOne([
            'email_confirm_token' => $token,
            'status' => self::STATUS_INACTIVE
        ]);
    }

    public function generateEmailConfirmToken()
    {
        $this->email_confirm_token = Yii::$app->security->generateRandomString();
    }

    public function removeEmailConfirmToken()
    {
        $this->email_confirm_token = null;
    }

    public static function find() {
        return new CommunityUserQuery(get_called_class());
    }

}

class CommunityUserQuery extends \yii\db\ActiveQuery
{
    public function postAuthors()
    {
        return $this->andWhere(['in', 'id',
            Post::find()->select('author_id')->publishedPosts()]);
    }

    public function byGootaxUserId($userId)
    {
        return $this->andWhere(['user_id' => $userId]);
    }

    public function activeUsers()
    {
        return $this->andWhere(['status' => CommunityUser::STATUS_ACTIVE]);
    }
}