<?php

namespace landing\modules\community\models\user;

use Yii;
use yii\base\Model;


class CommunityLoginForm extends Model
{
    const SCENARIO_CAPTCHA = 'with captcha';

    public $email;
    public $password;
    public $rememberMe = true;
    public $verifyCode;

    private $_user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password'], 'required', 'message' => 'Не заполнено'],
            ['email', 'email', 'message' => 'Неправильное значение'],
            ['password', 'validatePassword'],
            ['verifyCode', 'required', 'on' => self::SCENARIO_CAPTCHA],
            ['verifyCode', 'captcha', 'on' => self::SCENARIO_CAPTCHA],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email'      => 'Эл. почта',
            'password'   => 'Пароль',
            'verifyCode' => 'Проверочный код',
        ];
    }

    public function behaviors() {
        return [
            \common\components\behaviors\FormattedError::className(),
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Адрес эл. почты или пароль введен неверно.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(),
                $this->rememberMe ? Yii::$app->params['rememberMe'] : 0);
        } else {
            return false;
        }
    }

    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = CommunityUser::findByEmail($this->email);
        }
        return $this->_user;
    }

}