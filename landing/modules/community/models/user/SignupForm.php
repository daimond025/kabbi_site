<?php

namespace landing\modules\community\models\user;

use common\components\serviceApi\EmailTypes;
use common\components\serviceApi\ServiceApi;
use landing\modules\community\models\UserProfile;
use Yii;
use yii\base\Model;
use yii\helpers\Url;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $email;
    public $password;
    public $password_repeat;
    public $firstname;
    public $lastname;
    public $verifyCode;

    const SCENARIO_CAPTCHA = 'with captcha';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'firstname', 'lastname'], 'filter', 'filter' => 'trim'],
            [['email', 'password', 'password_repeat', 'firstname', 'lastname'], 'required', 'message' => 'Не заполнено'],
            ['email', 'email', 'message' => 'Неправильное значение'],
            [['firstname', 'lastname'], 'string'],
            ['password', 'string', 'min' => 6,
                'tooShort' => 'Должен быть не менее {min} символов'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password',
                'message' => 'Пароли не совпадают'],
            ['verifyCode', 'required', 'on' => self::SCENARIO_CAPTCHA],
            ['verifyCode', 'captcha', 'on' => self::SCENARIO_CAPTCHA],
        ];
    }

    public function behaviors() {
        return [
            \common\components\behaviors\FormattedError::className(),
        ];
    }

    public function attributeLabels()
    {
        return [
            'email'           => 'Эл. почта',
            'firstname'       => 'Имя',
            'lastname'        => 'Фамилия',
            'password'        => 'Пароль',
            'password_repeat' => 'Повторите пароль',
            'verifyCode'      => 'Проверочный код',
        ];
    }

    /**
     * @param CommunityUser $user
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    private function sendEmailConfirmToken(CommunityUser $user)
    {
        /** @var ServiceApi $serviceApi */
        $serviceApi = app()->get('serviceApi');
        $result     = $serviceApi->sendEmail(null, null, EmailTypes::EMAIL_USER_CONFIRM, null,
            $this->email, [
                'user_name'  => $user->lastname . ' ' . $user->firstname,
                'user_email' => $this->email,
                'url'        => Url::to([
                    '/site/community-confirm-email',
                    'token' => $user->email_confirm_token,
                ], true),
            ]);

        return $result;
    }

    public function signup()
    {
        if ($this->validate()) {
            $user = new CommunityUser();


            $user->email     = $this->email;
            $user->firstname = $this->firstname;
            $user->lastname  = $this->lastname;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->generateEmailConfirmToken();

            return $user->save() && $this->sendEmailConfirmToken($user);
        }

        return false;
    }

    public static function signupFromGootax($gootaxUser)
    {
        $transaction = app()->db->beginTransaction();
        try {
            $user = new CommunityUser();

            $user->email                = $gootaxUser->email;
            $user->firstname            = $gootaxUser->name;
            $user->lastname             = $gootaxUser->last_name;
            $user->secondname           = $gootaxUser->second_name;
            $user->password             = $gootaxUser->password;
            $user->password_reset_token = $gootaxUser->password_reset_token;
            $user->user_id              = $gootaxUser->user_id;
            $user->status               = CommunityUser::STATUS_ACTIVE;
            $user->generateAuthKey();
            $userSaved = $user->save();

            $profile                    = new UserProfile;
            $profile->community_user_id = $user->id;
            $profile->company           = $gootaxUser->tenant->full_company_name;
            //dd($gootaxUser->getUserPosition());
            //$profile->position = $gootaxUser->position->name;
            $profile->phone = $gootaxUser->phone;

            if ($userSaved && $profile->save()) {
                $transaction->commit();

                return $user;
            } else {
                return false;
            }
        } catch (yii\base\Exception $ex) {
            $transaction->rollback();

            return false;
        }
    }

    public function checkUniqueEmail()
    {
        if (!empty(CommunityUser::findByEmail($this->email))) {
            $this->addError('email', 'Уже существует');
        }
    }

    public function afterValidate() {
        if (!$this->hasErrors()) {
            $this->checkUniqueEmail();
        }
        parent::afterValidate();
    }
}