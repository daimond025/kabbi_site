<?php

namespace landing\modules\community\models\user;

use yii\base\InvalidParamException;
use yii\base\Model;
use Yii;

class ConfirmEmailForm extends Model
{
    private $_user;

    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Ошибочная ссылка подтверждения эл. почты.');
        }
        $this->_user = CommunityUser::findByEmailConfirmToken($token);
        if (!$this->_user) {
            throw new InvalidParamException('Ошибочная ссылка подтверждения эл. почты.');
        }
        parent::__construct($config);
    }

    public function confirmEmail()
    {
        $user = $this->_user;
        $user->status = CommunityUser::STATUS_ACTIVE;
        $user->removeEmailConfirmToken();
        Yii::$app->user->login($user);

        return $user->save();
    }

}