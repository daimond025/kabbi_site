<?php

namespace landing\modules\community\models\user;

use yii\base\InvalidParamException;
use yii\base\Model;
use Yii;

class ResetPasswordForm extends Model
{
    const SCENARIO_CHANGE_PASSWORD_IN_PROFILE = 'change password in profile';
    const SCENARIO_GOOTAX_CLIENT = 'gootax client';

    public $password;
    public $password_repeat;

    private $_user;

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_GOOTAX_CLIENT] = [];

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['password', 'required', 'message' => 'Не заполнено',
                'on' => self::SCENARIO_DEFAULT],
            ['password', 'string', 'min' => 6,
                'tooShort' => 'Должен быть не менее {min} символов',
                'on' => [
                    self::SCENARIO_DEFAULT,
                    self::SCENARIO_CHANGE_PASSWORD_IN_PROFILE
                ]],
            ['password_repeat', 'required', 'message' => 'Не заполнено',
                'when' => function ($model) {
                    return $model->password;
                }],
            ['password_repeat', 'compare', 'compareAttribute' => 'password',
                'message' => 'Пароли не совпадают',
                'when' => function ($model) {
                    return $model->password;
                }],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
        ];
    }

    /**
     * Creates a form model given a token.
     *
     * @param  string                          $token
     * @param  array                           $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $userId = null, $config = [])
    {
        if (!empty($userId)) {
            $this->_user = CommunityUser::findIdentity($userId);
        } else {
            if (empty($token) || !is_string($token)) {
                throw new InvalidParamException('Пароль обязателен к заполнению.');
            }
            $this->_user = CommunityUser::findByPasswordResetToken($token);
            if (!$this->_user) {
                throw new InvalidParamException('Неверный токен восстановления пароля.');
            }
        }

        parent::__construct($config);
    }

    /**
     * Resets password.
     *
     * @return boolean if password was reset.
     */
    public function resetPassword()
    {
        $user = $this->_user;
        $user->setPassword($this->password);
        $user->removePasswordResetToken();
        return $user->save(false);
    }

}