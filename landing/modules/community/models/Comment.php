<?php

namespace landing\modules\community\models;

use Yii;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use landing\modules\community\components\behaviors\ClearCacheBehavior;

/**
 * This is the model class for table "{{%comment}}".
 *
 * @property integer $comment_id
 * @property integer $post_id
 * @property string $comment
 * @property integer $author_id
 * @property integer $publication_date
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Post $post
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%comment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'comment'], 'required'],
            [['post_id', 'author_id'], 'integer'],
            [['comment'], 'string'],
            [['publishedDate', 'publication_date', 'created_at', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'comment_id' => 'Ключ',
            'post_id' => 'Ключ поста',
            'comment' => 'Комментарий',
            'author_id' => 'Ключ автора',
            'publication_date' => 'Дата публикации',
            'publishedDate' => 'Дата публикации',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
        ];
    }

    public function behaviors() {
        return [
            \yii\behaviors\TimestampBehavior::className(),
            \common\components\behaviors\FormattedError::className(),
            'clearCacheBehavior' => [
                'class' => ClearCacheBehavior::className(),
                'tags' => [Comment::className()],
            ],
        ];
    }

    public function getPublishedAt()
    {
        return date('d.m.Y в H:i', $this->publication_date);
    }

    public function getPublishedDate()
    {
        return $this->publication_date ? date('d.m.Y H:i', $this->publication_date) : null;
    }

    public function setPublishedDate($value)
    {
        if (!empty($value)) {
            $this->publication_date = strtotime($value);
        }
    }

    public function getLabel()
    {
        return strip_tags($this->comment_id . ' ('
            . StringHelper::truncateWords($this->comment, 5) .')');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['post_id' => 'post_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(user\CommunityUser::className(), ['id' => 'author_id']);
    }

    public function beforeSave($insert) {
        if ($insert) {
            if (!$this->publication_date) {
                $this->publication_date = time();
            }
            if (!isset($this->author_id)) {
                $this->author_id = user()->id;
            }
        }
        $this->comment = strip_tags(mb_ereg_replace("\n", '<br>', $this->comment), '<br><br/>');

        return parent::beforeSave($insert);
    }

    protected function refreshCountComments()
    {
        $this->post->count_comments = Comment::find()
                    ->byPostId($this->post->post_id)->count();
        $this->post->save();
    }

    public function afterSave($insert, $changedAttributes) {
        if ($insert) {
            $this->refreshCountComments();
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function afterDelete() {
        $this->refreshCountComments();

        parent::afterDelete();
    }

    public static function find() {
        return new CommentQuery(get_called_class());
    }

}

class CommentQuery extends \yii\db\ActiveQuery
{
    public function byPostId($postId)
    {
        return $this->andWhere([Comment::tableName() . '.post_id' => $postId])
                ->addOrderBy(['comment_id' => SORT_ASC]);
    }

}