<?php

namespace landing\modules\community\models;

use Yii;

/**
 * This is the model class for table "{{%subscribe_offer}}".
 *
 * @property integer $id
 * @property integer $community_user_id
 * @property integer $offer_info_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property OfferInfo $offerInfo
 * @property CommunityUser $communityUser
 */
class SubscribeOffer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%subscribe_offer}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['community_user_id', 'offer_info_id'], 'required'],
            [['community_user_id', 'offer_info_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }

    public function behaviors() {
        return [
            \yii\behaviors\TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'community_user_id' => 'Community User ID',
            'offer_info_id' => 'Offer Info ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferInfo()
    {
        return $this->hasOne(OfferInfo::className(), ['offer_info_id' => 'offer_info_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommunityUser()
    {
        return $this->hasOne(CommunityUser::className(), ['id' => 'community_user_id']);
    }

    public static function find() {
        return new SubscribeOfferQuery(get_called_class());
    }

}

class SubscribeOfferQuery extends \yii\db\ActiveQuery
{
    public function byOfferInfoId($offerInfoId)
    {
        return $this->andWhere(['offer_info_id' => $offerInfoId]);
    }
}



