<?php

namespace landing\modules\community\models;

use Yii;
use yii\helpers\Html;
use landing\modules\community\models\Post;
use landing\modules\community\components\behaviors\ClearCacheBehavior;

/**
 * This is the model class for table "{{%offer_info}}".
 *
 * @property integer $offer_info_id
 * @property integer $post_id
 * @property string $status
 * @property integer $plus
 * @property integer $minus
 *
 * @property Post $post
 */
class OfferInfo extends \yii\db\ActiveRecord
{
    const STATUS_VOTING = 'voting';
    const STATUS_IN_WORK = 'in work';
    const STATUS_INTRODUCED = 'introduced';
    const STATUS_DEFERRED = 'deferred';

    const DEFAULT_STATUS = OfferInfo::STATUS_VOTING;

    const CHANGED_STATUS_CACHE_TAG = 'offer info changed status';

    public static $statuses = [
        OfferInfo::STATUS_VOTING, OfferInfo::STATUS_IN_WORK,
        OfferInfo::STATUS_INTRODUCED, OfferInfo::STATUS_DEFERRED
    ];

    public static $statusLabels = [
        OfferInfo::STATUS_VOTING => 'Голосование',
        OfferInfo::STATUS_IN_WORK => 'В работе',
        OfferInfo::STATUS_INTRODUCED => 'Внедрено',
        OfferInfo::STATUS_DEFERRED => 'Под вопросом',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%offer_info}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id'], 'required'],
            [['post_id', 'plus', 'minus'], 'integer'],
            ['status', 'in', 'range' => OfferInfo::$statuses],
            ['status', 'default', 'value' => OfferInfo::DEFAULT_STATUS],
            [['plus', 'minus'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'offer_info_id' => 'Ключ',
            'post_id' => 'Ключ поста',
            'status' => 'Статус',
            'plus' => 'За',
            'minus' => 'Против',
            'rating' => 'Рейтинг',
        ];
    }

    public function behaviors() {
        return [
            'clearCacheBehavior' => [
                'class' => ClearCacheBehavior::className(),
                'tags' => [OfferInfo::className()],
            ]
        ];
    }

    public function getLabel()
    {
        return Html::encode('Offer (' . self::$statusLabels[$this->status]
                . ($this->rating > 0 ? ' +' : ' ') . $this->rating . ')');
    }

    public function getRating()
    {
        return $this->plus - $this->minus;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['post_id' => 'post_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVote()
    {
        return $this->hasMany(Vote::className(), ['offer_info_id' => 'offer_info_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserVote()
    {
        return $this->hasOne(Vote::className(), ['offer_info_id' => 'offer_info_id'])
                    ->onCondition([Vote::tableName() . '.user_id' => user()->id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserSubscribeOffer()
    {
        return $this->hasOne(SubscribeOffer::className(), ['offer_info_id' => 'offer_info_id'])
                    ->onCondition([SubscribeOffer::tableName() . '.community_user_id' => user()->id]);
    }

    public function afterSave($insert, $changedAttributes) {
        if ($insert || array_key_exists('status', $changedAttributes)) {
            \yii\caching\TagDependency::invalidate(app()->cache,
                    self::CHANGED_STATUS_CACHE_TAG);
        }
        parent::afterSave($insert, $changedAttributes);
    }

    public function afterDelete() {
        \yii\caching\TagDependency::invalidate(app()->cache,
                self::CHANGED_STATUS_CACHE_TAG);
        parent::afterDelete();
    }

    public function canVote()
    {
        return $this->status === OfferInfo::STATUS_VOTING;
    }

    public function doVote($vote, $userId)
    {
        if (!$this->canVote()) {
            throw new \yii\web\MethodNotAllowedHttpException(
                    t('yii', 'You are not allowed to perform this action.'));
        }

        Vote::deleteAll([
            'offer_info_id' => $this->offer_info_id,
            'user_id' => $userId,
        ]);

        $newVote = new Vote;
        $newVote->offer_info_id = $this->offer_info_id;
        $newVote->user_id = $userId;
        $newVote->vote = $vote;
        $newVote->save();

        $this->plus = Vote::find()
                ->byOfferInfoId($this->offer_info_id)
                ->byVote(Vote::VOTE_PLUS)->count();

        $this->minus = Vote::find()
                ->byOfferInfoId($this->offer_info_id)
                ->byVote(Vote::VOTE_MINUS)->count();

        $this->save();
    }

    public static function find() {
        return new OfferInfoQuery(get_called_class());
    }

}


class OfferInfoQuery extends \yii\db\ActiveQuery
{
    public function byStatus($status)
    {
        return $this->andWhere([OfferInfo::tableName() . '.status' => $status]);
    }

    public function publishedPosts()
    {
        return $this->andWhere(['in', 'post_id',
            Post::find()->select('post_id')->publishedPosts()]);
    }
}