<?php

namespace landing\modules\community\models;

use Yii;
use yii\db\Query;
use landing\modules\community\components\behaviors\ClearCacheBehavior;

/**
 * This is the model class for table "{{%tag}}".
 *
 * @property integer $tag_id
 * @property string $name
 *
 * @property PostTag[] $postTags
 */
class Tag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tag}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tag_id' => 'Ключ',
            'name' => 'Название',
        ];
    }

    public function behaviors() {
        return [
            'clearCacheBehavior' => [
                'class' => ClearCacheBehavior::className(),
                'tags' => [Tag::className()],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostTags()
    {
        return $this->hasMany(PostTag::className(), ['tag_id' => 'tag_id']);
    }

    public static function find() {
        return new TagQuery(get_called_class());
    }

    public static function getCachedTags()
    {
        return app()->db->cache(function () {
                    return Tag::find()->orderBy('name')->all();
                }, 0, new \yii\caching\TagDependency(['tags' => [Tag::className()]]));
    }

}

class TagQuery extends \yii\db\ActiveQuery
{
    public function choosedTags()
    {
        return $this->andWhere(['in', 'tag_id',
            Post::find()->innerJoinWith('postTags')
                ->select(PostTag::tableName() . '.tag_id')
                ->publishedPosts()]);
    }
}