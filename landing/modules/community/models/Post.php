<?php

namespace landing\modules\community\models;

use Yii;
use yii\db\ActiveQuery;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\HtmlPurifier;
use yii\helpers\StringHelper;
use yii\behaviors\TimestampBehavior;
use landing\modules\community\models\SubscribeComment;
use landing\modules\community\components\behaviors\ClearCacheBehavior;

/**
 * This is the model class for table "{{%post}}".
 *
 * @property integer $post_id
 * @property integer $section_id
 * @property string $title
 * @property string $anons
 * @property string $content
 * @property string $publication_date
 * @property string $status
 * @property integer $author_id
 * @property integer $count_comments
 * @property string $meta_description
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property OfferInfo[] $offerInfo
 * @property Section $section
 * @property PostTag[] $postTags
 */
class Post extends \yii\db\ActiveRecord
{
    const STATUS_DRAFT = 'draft';
    const STATUS_PUBLISHED = 'published';
    const STATUS_IN_ARCHIVE = 'in archive';

    public static $statuses = [
        self::STATUS_DRAFT,
        self::STATUS_PUBLISHED,
        self::STATUS_IN_ARCHIVE
    ];

    public static $statusLabels = [
        self::STATUS_DRAFT => 'Не опубликована',
        self::STATUS_PUBLISHED => 'Опубликована',
        self::STATUS_IN_ARCHIVE => 'В архиве',
    ];

    private $choosed_tags;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%post}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['section_id', 'title', 'anons'], 'required'],
            [['title', 'meta_description'], 'string', 'max' => 255],
            [['anons', 'content'], 'string'],
            ['status', 'in', 'range' => self::$statuses],
            ['count_comments', 'default', 'value' => 0],
            [['publication_date', 'author_id', 'created_at', 'updated_at',
                'choosedTags', 'publishedDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'post_id'          => 'Ключ',
            'section_id'       => 'Ключ раздела',
            'title'            => 'Заголовок',
            'anons'            => 'Анонс',
            'content'          => 'Подробно',
            'publication_date' => 'Дата публикации',
            'status'           => 'Статус',
            'author_id'        => 'Ключ автора',
            'count_comments'   => 'Комментарии',
            'meta_description' => 'Описание страницы (метаинформация)',
            'created_at'       => 'Дата создания',
            'updated_at'       => 'Дата редактирования',
            'choosedTags'      => 'Теги',
            'publishedDate'    => 'Дата публикации',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
            'clearCacheBehavior' => [
                'class' => ClearCacheBehavior::className(),
                'tags' => [Post::className()],
            ]
        ];
    }

    public function getPublicationDate()
    {
        return date('d.m.Y', $this->publication_date);
    }

    public function getPublishedDate()
    {
        return $this->publication_date ? date('d.m.Y H:i', $this->publication_date) : null;
    }

    public function setPublishedDate($value)
    {
        if (!empty($value)) {
            $this->publication_date = strtotime($value);
        }
    }

    public function getLabel()
    {
        return Html::encode($this->post_id . ' ('
            . StringHelper::truncateWords($this->title, 5) .')', false);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferInfo()
    {
        return $this->hasOne(OfferInfo::className(), ['post_id' => 'post_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSection()
    {
        return $this->hasOne(Section::className(), ['section_id' => 'section_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(user\CommunityUser::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostTags()
    {
        return $this->hasMany(PostTag::className(), ['post_id' => 'post_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['tag_id' => 'tag_id'])
                ->via('postTags');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserSubscribeComment()
    {
        return $this->hasOne(SubscribeComment::className(), ['post_id' => 'post_id'])
                    ->onCondition([SubscribeComment::tableName() . '.community_user_id' => app()->user->id]);
    }

    /**
     * Getter and setter for field "choosed_tags"
     */
    public function setChoosedTags($tags)
    {
        $this->choosed_tags = (array) $tags;
    }
    public function getChoosedTags()
    {
        return ArrayHelper::getColumn(
            $this->getPostTags()->all(), 'tag_id'
        );
    }

    public function isAuthor($userId)
    {
        return $this->author_id === $userId;
    }

    protected function saveChoosedTags()
    {
        PostTag::deleteAll(['post_id' => $this->post_id]);

        if (is_array($this->choosed_tags)) {
            $values = [];
            foreach ($this->choosed_tags as $id) {
                if (!empty($id)) {
                    $values[] = [$this->post_id, $id];
                }
            }

            if (!empty($values)) {
                self::getDb()->createCommand()
                        ->batchInsert(PostTag::tableName(), ['post_id', 'tag_id'], $values)->execute();
            }
        }
    }

    protected function savePostContent()
    {
        $postContent = PostContent::find()->byPostId($this->post_id)->one();
        if (!$postContent) {
            $postContent = new PostContent();
        }

        $postContent->post_id = $this->post_id;
        $postContent->title = strip_tags($this->title);
        $postContent->anons = strip_tags($this->anons);
        $postContent->content = strip_tags($this->content);
        $postContent->author = strip_tags(
                $this->author->firstname . ' '
                . $this->author->lastname . ' '
                . $this->author->secondname);

        $postContent->save();
    }

    protected function createOfferInfo()
    {
        $operInfo = new OfferInfo();
        $operInfo->post_id = $this->post_id;
        $operInfo->save();
    }

    public function setDefaultStatus()
    {
        $this->status = Section::isPublicSection($this->section_id) ?
                self::STATUS_PUBLISHED : self::STATUS_DRAFT;
    }

    public function beforeSave($insert) {
        if ($insert) {
            if (!isset($this->status)) {
                $this->setDefaultStatus();
            }
            if (!isset($this->publication_date)) {
                $this->publication_date = time();
            }
            if (!isset($this->author_id)) {
                $this->author_id = user()->id;
            }
        }

        $isBlogSection = $this->section_id === Section::BLOG;
        $this->anons = HtmlPurifier::process($this->anons, function ($config) use ($isBlogSection) {
            $conf = $config->getHTMLDefinition(true);
            $conf->addAttribute('a', 'target', 'Enum#_blank,_self,_target,_top');
            if ($isBlogSection) {
                $conf->addAttribute('a', 'data-remodal-target', 'Enum#register');
                $conf->addAttribute('a', 'data-ga-event-type', 'CDATA');
            }
            $conf->addElement('noindex',  'Inline', 'Inline', 'Common');
        });
        $this->content = HtmlPurifier::process($this->content, function ($config) use ($isBlogSection) {
            $conf = $config->getHTMLDefinition(true);
            $conf->addAttribute('a', 'target', 'Enum#_blank,_self,_target,_top');
            if ($isBlogSection) {
                $conf->addAttribute('a', 'data-remodal-target', 'Enum#register');
                $conf->addAttribute('a', 'data-ga-event-type', 'CDATA');
            }
            $conf->addElement('noindex',  'Inline', 'Inline', 'Common');
        });

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes) {
        $this->saveChoosedTags();
        $this->savePostContent();

        if ($insert && $this->section_id == Section::OFFERS) {
            $this->createOfferInfo();
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function afterDelete() {
        PostContent::deleteAll(['post_id' => $this->post_id]);

        parent::afterDelete();
    }

    public static function find() {
        return new PostQuery(get_called_class());
    }

}


class PostQuery extends ActiveQuery
{
    public function bySectionId($sectionId)
    {
        return $this->andWhere([Post::tableName() . '.section_id' => $sectionId]);
    }

    public function byAuthorId($authorId)
    {
        return $this->andWhere([Post::tableName() . '.author_id' => $authorId]);
    }

    public function byOfferStatus($status)
    {
        return $this->andWhere([OfferInfo::tableName() . '.status' => $status]);
    }

    public function publishedPosts()
    {
        return $this->andWhere([Post::tableName() . '.status' => 'published']);
    }

    public function recentPosts()
    {
        return $this->addOrderBy([Post::tableName() . '.publication_date' => SORT_DESC]);
    }
}