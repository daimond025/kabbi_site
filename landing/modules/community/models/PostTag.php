<?php

namespace landing\modules\community\models;

use Yii;
use landing\modules\community\components\behaviors\ClearCacheBehavior;

/**
 * This is the model class for table "{{%post_tag}}".
 *
 * @property integer $post_tag_id
 * @property integer $post_id
 * @property integer $tag_id
 *
 * @property Post $post
 * @property Tag $tag
 */
class PostTag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%post_tag}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'tag_id'], 'required'],
            [['post_id', 'tag_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'post_tag_id' => 'Ключ',
            'post_id' => 'Ключ поста',
            'tag_id' => 'Ключ тега',
        ];
    }

    public function behaviors() {
        return [
            'clearCacheBehavior' => [
                'class' => ClearCacheBehavior::className(),
                'tags' => [PostTag::className()],
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['post_id' => 'post_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTag()
    {
        return $this->hasOne(Tag::className(), ['tag_id' => 'tag_id']);
    }
}
