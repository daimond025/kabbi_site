<?php

namespace landing\modules\community\models;

use Yii;
use landing\modules\community\components\behaviors\ClearCacheBehavior;

/**
 * This is the model class for table "{{%user_profile}}".
 *
 * @property integer $user_profile_id
 * @property integer $community_user_id
 * @property string $company
 * @property string $position
 * @property string $phone
 * @property string $photo
 * @property string $automation_program
 * @property string $community_participation
 *
 * @property CommunityUser $communityUser
 */
class UserProfile extends \yii\db\ActiveRecord
{
    const SCENARIO_GOOTAX_CLIENT = 'gootax client';

    public $image;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_profile}}';
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_GOOTAX_CLIENT] = [
                'photo','automation_program', 'community_participation'
            ];

        return $scenarios;
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['community_user_id'], 'required'],
            [['community_user_id'], 'integer'],
            [['company', 'position', 'photo', 'phone',
                'automation_program', 'community_participation'], 'string', 'max' => 255],
            ['image', 'file',
                'extensions' => ['png', 'jpeg', 'jpg'],
                'maxSize' => getUploadMaxFileSize(),
                'tooBig' => t('file', 'The file size must be less than {file_size}Mb', [
                    'file_size' => getUploadMaxFileSize(true)])],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_profile_id' => 'Ключ профиля',
            'community_user_id' => 'Ключ пользователя',
            'company' => 'Компания',
            'position' => 'Должность',
            'phone' => 'Телефон',
            'photo' => 'Фото',
            'automation_program' => 'Программа автоматизации',
            'community_participation' => 'Участвие в сообществе',
        ];
    }

    public function behaviors() {
        return [
            'fileBehavior' => [
                'class'   => \landing\components\behaviors\ImageUploadBehavior::className(),
                'files'   => [['filename' => 'photo', 'file' => 'image']],
                'web'     => app()->params['landingUrl'],
                'webroot' => '@landing/web/',
                'path'    => app()->params['upload'] . '/photo/',
                'maxSize' => 600,
            ],
            'clearCacheBehavior' => [
                'class' => ClearCacheBehavior::className(),
                'tags'  => [UserProfile::className()],
            ]
       ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommunityUser()
    {
        return $this->hasOne(CommunityUser::className(), ['id' => 'community_user_id']);
    }

    public static function find() {
        return new UserProfileQuery(get_called_class());
    }
}

class UserProfileQuery extends \yii\db\ActiveQuery
{
    public function byCommunityUserId($communityUserId)
    {
        return $this->andWhere(['community_user_id' => $communityUserId]);
    }
}