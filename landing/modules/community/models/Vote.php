<?php

namespace landing\modules\community\models;

use Yii;

/**
 * This is the model class for table "{{%vote}}".
 *
 * @property integer $vote_id
 * @property integer $offer_info_id
 * @property integer $user_id
 *
 * @property OfferInfo $offerInfo
 */
class Vote extends \yii\db\ActiveRecord
{
    const VOTE_PLUS = 'plus';
    const VOTE_MINUS = 'minus';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%vote}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['offer_info_id', 'user_id'], 'required'],
            [['offer_info_id', 'user_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'vote_id' => 'Ключ',
            'offer_info_id' => 'Ключ предложения',
            'user_id' => 'Пользователь',
            'vote' => 'Голос'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfferInfo()
    {
        return $this->hasOne(OfferInfo::className(), ['offer_info_id' => 'offer_info_id']);
    }

    public static function find() {
        return new VoteQuery(get_called_class());
    }

}

class VoteQuery extends \yii\db\ActiveQuery
{
    public function byOfferInfoId($offerInfoId)
    {
        return $this->andWhere(['offer_info_id' => $offerInfoId]);
    }

    public function byVote($vote)
    {
        return $this->andWhere(['vote' => $vote]);
    }

    public function byUserId($userId)
    {
        return $this->andWhere(['user_id' => $userId]);
    }
}
