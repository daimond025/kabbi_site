<?php

namespace landing\modules\community\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use landing\modules\community\models\Post;

/**
 * PostSearch represents the model behind the search form about `app\modules\community\models\Post`.
 */
class PostSearch extends Post
{
    public $tag_id;
    public $query;

    const MIN_LENGTH_SEARCH_WORD = 3;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'section_id', 'publication_date', 'author_id',
              'created_at', 'updated_at', 'tag_id'], 'integer'],
            ['query', 'required', 'when' => function ($model) {
                    return empty($model->tag_id) && empty($model->author_id);
                }],
            [['title', 'anons', 'content', 'status'], 'safe'],
        ];
    }

    /**
     * @param array
     * @return array
     */
    private function filterWordsByMinLength($words)
    {
        return array_map(function ($word) {
                if (mb_strlen($word) < self::MIN_LENGTH_SEARCH_WORD) {
                    return '';
                } else {
                    return $word . '*';
                }
            }, $words);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $q = null)
    {
        $query = Post::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params, '');

        if (!$this->validate()) {
            $query->where('0 = 1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'post_id' => $this->post_id,
            'section_id' => $this->section_id,
            'publication_date' => $this->publication_date,
            'author_id' => $this->author_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'anons', $this->anons])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'status', $this->status]);

        if (!empty($this->query)) {
            $searchWords = $this->filterWordsByMinLength(explode(' ', $this->query));

            $query->andWhere(['in', 'post_id',
                    PostContent::find()->select('post_id')
                        ->where('MATCH (title, anons, content, author) AGAINST (:query in boolean mode)',
                                    ['query' => implode(' ', $searchWords)])]);
        }

        if (!empty($this->tag_id)) {
            $query->andWhere(['in', 'post_id',
                PostTag::find()->select('post_id')
                        ->where(['tag_id' => $this->tag_id])])->all();
        }
        return $dataProvider;
    }
}
