<?php

namespace landing\modules\community\models;

use Yii;

/**
 * This is the model class for table "{{%post_content}}".
 * @property integer $post_content_id
 * @property integer $post_id
 * @property string $title
 * @property string $anons
 * @property string $content
 * @property string $author
 */
class PostContent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%post_content}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id'], 'integer'],
            [['title', 'anons', 'content', 'author'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'post_content_id' => 'Ключ',
            'post_id' => 'Ключ поста',
            'title' => 'Заголовок',
            'anons' => 'Анонс',
            'content' => 'Подробно',
            'author' => 'Автор',
        ];
    }

    /*
     * Advanced find method
     */
    public static function find() {
        return new PostContentQuery(get_called_class());
    }
}

class PostContentQuery extends \yii\db\ActiveQuery
{
    public function byPostId($postId)
    {
        return $this->andWhere(['post_id' => $postId]);
    }
}
