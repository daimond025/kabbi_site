<?php

namespace app\modules\community\controllers;

use landing\modules\community\models\Section;

/*
 * Communion community section
 */
class CommunionController extends PostController
{
    protected $sectionId = Section::COMMUNION;
}
