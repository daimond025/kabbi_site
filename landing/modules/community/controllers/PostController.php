<?php

namespace app\modules\community\controllers;

use landing\controllers\BaseController;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\web\MethodNotAllowedHttpException;
use yii\filters\VerbFilter;
use yii\base\ViewContextInterface;
use landing\modules\community\components\filters\AccessControl;
use landing\modules\community\models\Section;
use landing\modules\community\models\Tag;
use landing\modules\community\models\Post;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends BaseController
     implements ViewContextInterface
{
    /*
     * Community section setup in childs
     * @var integer
     */
    protected $sectionId;

    const DEFAULT_PAGE_SIZE = 20;

    /*
     * The path to the view
     * @return string/boolean
     */
    public function getViewPath() {
        return \Yii::getAlias('@app/modules/community/views/post');
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'uploadImage' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'delete', 'uploadImage'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete', 'uploadImage'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions() {
        return [
            'upload-image' => [
                'class' => \landing\components\actions\ImageUploadAction::className(),
                'web' => app()->params['landingUrl'],
                'webroot' => '@landing/web',
                'path' => app()->params['upload'] . '/post/images/',
        ]];
    }

    protected function getIndexQuery()
    {
        return Post::find()
                        ->with(['section', 'author', 'tags'])
                        ->bySectionId($this->sectionId)
                        ->publishedPosts()
                        ->recentPosts();
    }

    /**
     * Lists Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        $section = Section::getCachedSection($this->sectionId);

        $query = $this->getIndexQuery();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => self::DEFAULT_PAGE_SIZE,
            ],
        ]);

//        app()->db->cache(function () use ($dataProvider){
//            $dataProvider->prepare();
//        }, 60 * 60, new \yii\caching\TagDependency([
//            'tags' => [Post::className(), \landing\modules\community\models\user\CommunityUser::className
//        ]));

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'section' => $section,
        ]);
    }

    /**
     * Displays a single Post model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $section = Section::getCachedSection($this->sectionId);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'section' => $section,
        ]);
    }

    /*
     * Save post in transaction
     * @param Post model
     * @return boolean
     */
    private function saveModel($model)
    {
        $transaction = app()->db->beginTransaction();
        try {
            if ($model->save()) {
                $transaction->commit();
                return true;
            }
        } catch (\yii\db\Exception $ex) {
            session()->setFlash('error', 'Ошибка при сохранении статьи. Пожалуйста, cообщите администратору.');
            \Yii::error($ex->getMessage(), 'community');
            $transaction->rollback();
        }
        return false;
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $section = Section::getCachedSection($this->sectionId);
        $tags = Tag::getCachedTags();

        $model = new Post();
        $model->section_id = $this->sectionId;

        if ($model->load(app()->request->post()) && $this->saveModel($model)) {
            return $this->redirect(['view', 'id' => $model->post_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'section' => $section,
                'tags' => $tags,
            ]);
        }
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $section = Section::getCachedSection($this->sectionId);
        $tags = Tag::getCachedTags();

        $model = $this->findModel($id);

        if ($model->load(app()->request->post())) {
            if (!$model->isAuthor(user()->id)) {
                throw new MethodNotAllowedHttpException(
                        t('yii', 'You are not allowed to perform this action.'));
            }

            $model->setDefaultStatus();

            if ($this->saveModel($model)) {
                return $this->redirect(['view', 'id' => $model->post_id]);
            }
        }

        return $this->render('update', [
                'model' => $model,
                'section' => $section,
                'tags' => $tags,
            ]);
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->isAuthor(user()->id)) {
            $model->delete();
            return $this->redirect(['index']);
        } else {
            throw new MethodNotAllowedHttpException(
                    t('yii', 'You are not allowed to perform this action.'));
        }
    }

    protected function getModelQuery($id)
    {
        return Post::find()
                    ->with(['section', 'author', 'userSubscribeComment'])
                    ->where(['post_id' => $id])->one();
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model  = $this->getModelQuery($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(t('yii', 'Page not found.'));
        }
    }

}
