<?php

namespace app\modules\community\controllers;

use yii\filters\VerbFilter;
use landing\modules\community\components\filters\AccessControl;
use landing\modules\community\models\Section;
use landing\modules\community\models\OfferInfo;
use landing\modules\community\models\Vote;
use landing\modules\community\models\Post;
use landing\modules\community\models\SubscribeOffer;

/*
 * Offers community section
 */
class OffersController extends PostController
{
    protected $sectionId = Section::OFFERS;

    public function getViewPath() {
        return \Yii::getAlias('@app/modules/community/views/offers');
    }

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'plus' => ['post'],
                    'minus' => ['post'],
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'delete', 'subscribe'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete', 'subscribe'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function init() {
        app()->subscribeManager->registerSubscribeOffer();
        parent::init();
    }

    protected function getIndexQuery() {
        $status = get('status') ? get('status') : OfferInfo::DEFAULT_STATUS;

        return parent::getIndexQuery()
                        ->innerJoinWith(['offerInfo' => function ($query) use ($status) {
                            $query->andWhere([OfferInfo::tableName() . '.status' => $status])
                                    ->with('userVote');
                        }]);
    }

    protected function getModelQuery($id)
    {
        return Post::find()
                ->with(['section', 'author', 'offerInfo.userVote',
                    'offerInfo.userSubscribeOffer'])
                ->where(['post_id' => $id])->one();
    }

    private function setVote($id, $currentVote)
    {
        app()->response->format = \yii\web\Response::FORMAT_JSON;

        $offerInfo = OfferInfo::findOne($id);

        if (app()->user->isGuest) {
            return ['needAuthorization' => true];
        }

        if (!$offerInfo->canVote()) {
            return;
        }

        $transaction = app()->db->beginTransaction();
        try {
            $offerInfo->doVote($currentVote, user()->id);

            $transaction->commit();

            return [
                'activePlus' => $currentVote === Vote::VOTE_PLUS,
                'activeMinus' => $currentVote === Vote::VOTE_MINUS,
                'ratingValue' => $offerInfo->plus - $offerInfo->minus,
                'plusValue' => intval($offerInfo->plus),
                'minusValue' => intval($offerInfo->minus),
            ];
        } catch (\yii\base\Exception $ex) {
            $transaction->rollback();
        }
    }

    public function actionPlus($id)
    {
        return $this->setVote($id, Vote::VOTE_PLUS);
    }

    public function actionMinus($id)
    {
        return $this->setVote($id, Vote::VOTE_MINUS);
    }

    protected function subscribe($offerInfoId, $subscribe)
    {
        SubscribeOffer::deleteAll([
            'offer_info_id' => $offerInfoId,
            'community_user_id' => user()->id]);

        if ($subscribe) {
            $subscribeOffer= new SubscribeOffer;
            $subscribeOffer->offer_info_id = $offerInfoId;
            $subscribeOffer->community_user_id = user()->id;
            $subscribeOffer->save();
        }
    }

    public function actionSubscribe()
    {
        $data['offer_info_id'] = post('offer_info_id');
        $data['subscribe'] = post('subscribe');

        $model = \yii\base\DynamicModel::validateData($data,
          [['offer_info_id', 'required'],
           ['subscribe', 'safe']]);

        if ($model->hasErrors()) {
            return 'error';
        }

        $transaction = app()->db->beginTransaction();
        try {
            $this->subscribe($model->offer_info_id, $model->subscribe);
            $transaction->commit();
            return 'success';
        } catch (\yii\base\Exception $ex) {
            $transaction->rollback();
            return 'error' . $ex;
        }
    }

}

