<?php

namespace app\modules\community\controllers;

use Yii;
use landing\modules\community\models\Comment;
use landing\modules\community\models\SubscribeComment;
use landing\modules\community\components\filters\AccessControl;

class CommentController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['add', 'subscribe'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['add', 'subscribe'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function init() {
        app()->subscribeManager->registerSubscribeComment();
        parent::init();
    }

    public function actionAdd()
    {
        $model = new Comment;

        if ($model->load(app()->request->post()) && $model->save()) {
            return 'success';
        } else {
            app()->response->format = \yii\web\Response::FORMAT_JSON;
            return $model->getFormattedError();
        }
    }

    protected function subscribe($postId, $subscribe)
    {
        SubscribeComment::deleteAll([
            'post_id' => $postId,
            'community_user_id' => user()->id]);

        if ($subscribe) {
            $subscribeComment = new SubscribeComment;
            $subscribeComment->post_id = $postId;
            $subscribeComment->community_user_id = user()->id;
            $subscribeComment->save();
        }
    }

    public function actionSubscribe()
    {
        $data['post_id'] = post('post_id');
        $data['subscribe'] = post('subscribe');

        $model = \yii\base\DynamicModel::validateData($data,
          [['post_id', 'required'],
           ['subscribe', 'safe']]);

        if ($model->hasErrors()) {
            return 'error';
        }

        $transaction = app()->db->beginTransaction();
        try {
            $this->subscribe($model->post_id, $model->subscribe);
            $transaction->commit();
            return 'success';
        } catch (\yii\base\Exception $ex) {
            $transaction->rollback();
            return 'error' . $ex;
        }
    }

}
