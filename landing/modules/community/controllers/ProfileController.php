<?php

namespace app\modules\community\controllers;

use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use landing\modules\community\models\Section;
use landing\modules\community\models\Post;
use landing\modules\community\models\Tag;
use landing\modules\community\models\PostTag;
use landing\modules\community\models\OfferInfo;
use landing\modules\community\models\user\CommunityUser;
use landing\modules\community\models\UserProfile;
use landing\modules\community\models\user\ResetPasswordForm;
use landing\modules\community\components\filters\AccessControl;

class ProfileController extends \yii\web\Controller
{

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['update'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionView($id = null)
    {
        $userId = $id;
        if (!isset($id)) {
            $id = user()->id;
        }

        $user = $this->findModel($id);

        return $this->render('view', [
            'user' => $user,
            'userId' => $userId,
            ]);
    }

    public function actionUpdate($id = null)
    {
        $userId = $id;
        if (!isset($id)) {
            $id = user()->id;
        }

        if (user()->id != $id) {
            return $this->redirect(['view', 'id' => $userId]);
        }

        $user = $this->findModel($id);
        $profile = UserProfile::find()->byCommunityUserId($id)->one();
        $resetPasswordForm = new ResetPasswordForm(null, $id);

        if (!$profile) {
            $profile = new UserProfile;
            $profile->community_user_id = $id;
            $profile->save();
        }

        if (isset(user()->user_id)) {
            $user->scenario = CommunityUser::SCENARIO_GOOTAX_CLIENT;
            $profile->scenario = UserProfile::SCENARIO_GOOTAX_CLIENT;
            $resetPasswordForm->scenario = ResetPasswordForm::SCENARIO_GOOTAX_CLIENT;
        } else {
            $user->scenario = CommunityUser::SCENARIO_DEFAULT;
            $profile->scenario = UserProfile::SCENARIO_DEFAULT;
            $resetPasswordForm->scenario = ResetPasswordForm::SCENARIO_CHANGE_PASSWORD_IN_PROFILE;
        }

        $transaction = app()->db->beginTransaction();
        try {
            $loadedUser = $user->load(app()->request->post());
            $loadedProfile = $profile->load(app()->request->post());
            $loadedResetPasswordForm = $resetPasswordForm->load(app()->request->post());

            $isUserValid = $user->validate();
            $isProfileValid = $profile->validate();
            $isResetPasswordFormValid = $resetPasswordForm->validate();

            if (($loadedUser || $loadedProfile || $loadedResetPasswordForm)
                    && $isUserValid && $isProfileValid && $isResetPasswordFormValid) {

                $user->save(false);

                if ($user->save(false) && $profile->save(false)
                        && (!$resetPasswordForm->password || $resetPasswordForm->resetPassword())) {

                    $transaction->commit();
                    app()->session->setFlash('success', 'Ваш профиль обновлен.');

                    return $this->refresh();
                }
            }

            return $this->render('update', [
                'user' => $user,
                'profile' => $profile,
                'resetPasswordForm' => $resetPasswordForm,
                'confirmPersonalDataProcessing' => true,
                'userId' => $userId,
            ]);

        } catch (\yii\db\Exception $ex) {
            session()->setFlash('error', 'Ошибка при редактировании профиля пользователя. Пожалуйста, cообщите администратору.');
            \Yii::error($ex->getMessage(), 'community');
            $transaction->rollback();
            return $this->refresh();
        }
    }

    protected function renderPosts($userId, $sectionId)
    {
        $id = $userId;
        if (!isset($id)) {
            $id = user()->id;
        }

        $this->findModel($id);
        $section = Section::getCachedSection($sectionId);

        $query = Post::find()->with(['section', 'author', 'tags', 'offerInfo'])
            ->bySectionId($sectionId)
            ->byAuthorId($id)
            ->recentPosts();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'defaultPageSize' => PostController::DEFAULT_PAGE_SIZE,
            ],
        ]);

        app()->db->cache(function () use ($dataProvider) {
            return $dataProvider->prepare();
        }, 60 * 60, new \yii\caching\TagDependency([
            'tags' => [
                Section::className(), Post::className(),
                CommunityUser::className(), OfferInfo::className(),
                Tag::className(), PostTag::className(),
        ]]));

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'section' => $section,
            'userId' => $userId,
        ]);
    }

    public function actionOffers($id = null)
    {
        return $this->renderPosts($id, Section::OFFERS);
    }

    public function actionCommunion($id = null)
    {
        return $this->renderPosts($id, Section::COMMUNION);
    }

    protected function findModel($id)
    {
        $model = app()->db->cache(function () use ($id) {
            return CommunityUser::find()
                            ->with(['userProfile'])
                            ->where(['id' => $id])->one();
        }, 60 * 60, new \yii\caching\TagDependency([
            'tags' => [CommunityUser::className(), UserProfile::className()],
        ]));

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(t('yii', 'Page not found.'));
        }
    }
}
