<?php

namespace app\modules\community\controllers;

use landing\modules\community\models\Section;

/*
 * Docs community section
 */
class DocsController extends PostController
{
    protected $sectionId = Section::DOCS;
}
