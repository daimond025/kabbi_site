<?php

namespace app\modules\community\controllers;

use landing\controllers\BaseController;
use Yii;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\data\ActiveDataProvider;
use yii\caching\TagDependency;
use Zelenin\yii\extensions\Rss\RssView;
use landing\modules\community\models\Section;
use landing\modules\community\models\Post;
use landing\modules\community\models\PostSearch;

class CommunityController extends BaseController
{
    const RECENT_POST_LIMIT = 3;
    const RECENT_POSTS_CACHE = 'recent posts';

    public function getRecentPosts()
    {
        $result = app()->cache->get(self::RECENT_POSTS_CACHE);

        if (!$result) {
            $result = [];
            $sections = Section::find()->all();

            foreach ($sections as $section) {
                $title = Html::a(Html::encode($section->name),
                        ['/community/' . Section::$controllers[$section->section_id] . '/index']);

                $recentPosts = Post::find()
                                ->bySectionId($section->section_id)
                                ->publishedPosts()
                                ->recentPosts()
                                ->limit(self::RECENT_POST_LIMIT)->all();

                $links = [];
                foreach ($recentPosts as $post) {
                    $links[] = Html::a(Html::encode($post->title), [
                            '/community/' . Section::$controllers[$post->section_id] . '/view',
                            'id' => $post->post_id
                        ]);
                }

                $result[] = [
                    'title' => $title,
                    'links' => $links,
                ];
            }

            app()->cache->set(self::RECENT_POSTS_CACHE, $result, 60 * 60, (new TagDependency([
                'tags' => [Section::className(), Post::className()]
            ])));
        }

        return $result;
    }

    public function actionIndex()
    {
        return $this->render('index', [
            'data' => $this->getRecentPosts(),
        ]);
    }

    public function actionSearch()
    {
        $dataProvider = (new PostSearch())->search(get());

        $dataProvider->pagination->defaultPageSize = PostController::DEFAULT_PAGE_SIZE;

        $dataProvider->query
                ->with(['section', 'author', 'tags', 'offerInfo.userVote'])
                ->orderBy(Post::tableName() . '.section_id')
                ->publishedPosts()
                ->recentPosts();

        return $this->render('search', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRss()
    {
        $response = Yii::$app->getResponse();
        $response->format = \yii\web\Response::FORMAT_XML;

        $headers = $response->getHeaders();
        $headers->set('Content-Type', 'application/rss+xml; charset=utf-8');

        $content = app()->cache->get('rss');

        if (!$content) {
            $dataProvider = new ActiveDataProvider([
                'query' => Post::find()
                    ->with('section')
                    ->publishedPosts()
                    ->recentPosts(),
                'pagination' => [
                    'pageSize' => PostController::DEFAULT_PAGE_SIZE,
                ],
            ]);

            $content = RssView::widget([
                'dataProvider' => $dataProvider,
                'channel' => [
                    'title' => 'Сообщество Гутакса',
                    'link' => Url::to(['community/'], true),
                    'description' => 'Пишем о Гутаксе, интересных моментах в сфере такси и много другое',
                    'language' => Yii::$app->language,
                ],
                'items' => [
                    'title' => function ($model, $widget) {
                            return Html::encode($model->title);
                        },
                    'description' => function ($model) {
                            return '<![CDATA[' . StringHelper::truncateWords(Html::encode($model->anons), 50) . ']]>';
                        },
                    'link' => function ($model, $widget) {
                            return Url::to([
                                    Section::$controllers[$model->section_id] . '/view',
                                    'id' => $model->post_id
                                ], true);
                        },
                    'guid' => function ($model, $widget) {
                            return Url::to([
                                    Section::$controllers[$model->section_id] . '/view',
                                    'id' => $model->post_id
                                ], true);
                        },
                    'pubDate' => function ($model, $widget) {
                            $date = new \DateTime();
                            $date->setTimestamp($model->publication_date);
                            return $date->format(DATE_RSS);
                        },
                ],
            ]);
            app()->cache->set('rss', $content, 60 * 60);
        }
        $response->content = $content;
    }
}
