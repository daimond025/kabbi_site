<?php

namespace app\modules\community\controllers;

use landing\modules\community\models\Section;

/*
 * Blog community section
 */
class BlogController extends PostController
{
    protected $sectionId = Section::BLOG;
}
