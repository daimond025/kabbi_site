<?php

namespace lead\models;

use yii\base\Model;

/**
 * Class BaseLead
 * @package lead\models
 */
abstract class BaseLead extends Model
{
    /**
     * Creating new lead
     *
     * @param array
     */
    abstract public function createLead(array $leadData);
}