<?php

namespace lead\models;

use common\components\curl\Curl;
use lead\exceptions\LeadException;
use yii\base\InvalidConfigException;

/**
 * Class RedmineLead
 * @package lead\models
 */
class RedmineLead extends BaseLead
{
    const ACTION_CREATE_CONTACT = 'contacts.json';
    const PROJECT_ID = 'customers';
    const TYPE_CONTACT = 'Потенциальный';
    const TAG_NEW = 'NEW';
    const ASSIGNED_ID = 4;
    const VISIBILITY_PROJECT = '0';

    /**
     * @var string
     */
    public $protocol;

    /**
     * @var string
     */
    public $host;

    /**
     * @var string
     */
    public $user;

    /**
     * @var string
     */
    public $password;

    /**
     * Getting url
     *
     * @param $action
     *
     * @return string
     */
    public function getUrl($action)
    {
        return "{$this->protocol}://{$this->user}:{$this->password}@{$this->host}/{$action}";
    }

    /**
     * Getting lead data in json format
     *
     * @param array $data
     *
     * @return string
     */
    private function getJsonLeadData($data)
    {
        return json_encode([
            'contact' => [
                'project_id'         => self::PROJECT_ID,
                'is_company'         => true,
                'first_name'         => implode(' ',
                    array_filter([$data['last_name'], $data['name'], $data['second_name']])),
                'address_attributes' => [
                    'country' => $data['country'],
                    'region'  => $data['region'],
                    'city'    => $data['city'],
                ],
                'custom_fields'      => [
                    [
                        'id'    => 2,
                        'value' => $data['url'],
                    ],
                    [
                        'id'    => 12,
                        'value' => $data['source'],
                    ],
                    [
                        'id'    => 13,
                        'value' => self::TYPE_CONTACT,
                    ],
                ],
                'email'              => $data['email'],
                'phone'              => $data['phone'],
                'assigned_to_id'     => self::ASSIGNED_ID,
                'tag_list'           => self::TAG_NEW,
                'visibility'         => self::VISIBILITY_PROJECT,
            ],
        ]);
    }

    /**
     * @inheritdoc
     *
     * @throws InvalidConfigException
     * @throws LeadException
     */
    public function createLead(array $leadData)
    {
        /* @var $curl Curl */
        $curl = \Yii::createObject(Curl::class);

        $curl->headers = [
            'Content-Type' => 'application/json',
            'Accept'       => 'application/json',
        ];

        $response = $curl->post($this->getUrl(self::ACTION_CREATE_CONTACT),
            $this->getJsonLeadData($leadData));

        if ($response === false) {
            throw new LeadException($curl->error());
        } else {
            $statusCode = $response->headers['Status-Code'];

            if (!in_array($statusCode, [200, 201])) {
                $result  = json_decode($response->body, true);
                $message = !empty($result['errors'])
                    ? implode(', ', $result['errors']) : (!empty($response->body)
                        ? $response->body : $curl->error());

                throw new LeadException($message);
            }
        }
    }
}