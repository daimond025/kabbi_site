<?php

namespace lead\models;

use common\components\curl\Curl;
use common\components\curl\CurlResponse;
use lead\exceptions\LeadException;
use lead\Module;
use yii\base\InvalidConfigException;
use yii\base\Model;

/**
 * Class Bitrix24Lead
 * @package lead\models
 */
class Bitrix24Lead extends Model
{
    const ACTION_IMPORT_LEAD = '/crm/configs/import/lead.php';
    const STATUS_ID = 'ASSIGNED';
    const OPENED = 'Y';
    const ASSIGNED_BY_ID = 114;
    const CUSTOM_COLUMN_DOMAIN = 'UF_CRM_1468911037';
    const CUSTOM_COLUMN_TENANT_ID = 'UF_CRM_1470114655';

    const SOURCE_MAP = [
        Module::SOURCE_REGISTER      => 'SELF',
        Module::SOURCE_BUSINESS_PLAN => 1,
        Module::SOURCE_DEMONSTRATION => 'WEB',
        Module::SOURCE_CALLBACK      => 'WEB',
        Module::SOURCE_GUIDE         => 'WEB',
    ];

    /**
     * @var string
     */
    public $protocol;

    /**
     * @var string
     */
    public $host;

    /**
     * @var string
     */
    public $user;

    /**
     * @var string
     */
    public $password;

    /**
     * Getting url
     *
     * @return string
     */
    public function getUrl()
    {
        $action = self::ACTION_IMPORT_LEAD;

        return "{$this->protocol}://{$this->host}/{$action}";
    }

    private function getTitle($leadSource)
    {
        switch ($leadSource) {
            case Module::SOURCE_REGISTER:
                return 'Регистрация на сайте gootax.pro';
            case Module::SOURCE_BUSINESS_PLAN:
                return 'Бизнес-план';
            case Module::SOURCE_DEMONSTRATION:
                return 'Демонстрация';
            case Module::SOURCE_CALLBACK:
                return 'Обратный звонок';
            case Module::SOURCE_GUIDE:
                return 'Руководство по открытию бизнеса';
            default:
                return '-';
        }
    }

    /**
     * @inheritdoc
     *
     * @throws InvalidConfigException
     * @throws LeadException
     */
    public function createLead(array $leadData)
    {
        /* @var $curl Curl */
        $curl = \Yii::createObject(Curl::class);

        $address = implode(', ', array_filter([
            $leadData['country'],
            $leadData['region'],
            $leadData['city'],
        ]));

        $params = [
            'LOGIN'                       => $this->user,
            'PASSWORD'                    => $this->password,
            'TITLE'                       => $this->getTitle($leadData['source']),
            'NAME'                        => $leadData['name'],
            'SECOND_NAME'                 => $leadData['second_name'],
            'LAST_NAME'                   => $leadData['last_name'],
            'PHONE_MOBILE'                => $leadData['phone'],
            'EMAIL_WORK'                  => $leadData['email'],
            'ADDRESS'                     => $address,
            'STATUS_ID'                   => self::STATUS_ID,
            'OPENED'                      => self::OPENED,
            'ASSIGNED_BY_ID'              => self::ASSIGNED_BY_ID,
            'SOURCE_ID'                   => self::SOURCE_MAP[$leadData['source']],
            self::CUSTOM_COLUMN_DOMAIN    => $leadData['url'],
            self::CUSTOM_COLUMN_TENANT_ID => $leadData['id'],
        ];

        /** @var $response CurlResponse */
        $response = $curl->post($this->getUrl(), $params);

        if ($response === false) {
            throw new LeadException($curl->error());
        }

        $result     = json_decode(str_replace("'", "\"", $response->body), true);
        $statusCode = $response->headers['Status-Code'];

        if ($statusCode != 200 || $result['error'] != 201) {
            $message = $curl->error() . "\n" . $response->body;
            throw new LeadException($message);
        }
    }
}