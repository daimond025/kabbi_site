<?php

namespace lead\exceptions;

use yii\base\Exception;

/**
 * Class UnknownLeadTypeException
 * @package lead\exceptions
 */
class UnknownLeadTypeException extends Exception
{

}