<?php

namespace lead\exceptions;

use yii\base\Exception;

/**
 * Class LeadException
 * @package lead\exceptions
 */
class LeadException extends Exception
{

}