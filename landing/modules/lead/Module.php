<?php

namespace lead;

use lead\exceptions\UnknownLeadTypeException;
use lead\models\BaseLead;
use lead\models\Bitrix24Lead;
use lead\models\RedmineLead;

/**
 * lead module definition class
 */
class Module extends \yii\base\Module
{
    const TYPE_REDMINE = 'Redmine';
    const TYPE_BITRIX24 = 'Bitrix24';

    const SOURCE_REGISTER = 'register';
    const SOURCE_BUSINESS_PLAN = 'business-plan';
    const SOURCE_CALLBACK = 'callback';
    const SOURCE_DEMONSTRATION = 'demonstration';
    const SOURCE_GUIDE = 'guide';

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'lead\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        \Yii::configure($this, require(__DIR__ . '/config.php'));
    }

    /**
     * Getting lead model
     *
     * @param $type
     *
     * @return BaseLead
     * @throws UnknownLeadTypeException
     * @throws \yii\base\InvalidConfigException
     */
    public function getLeadModel($type)
    {
        switch ($type) {
            case self::TYPE_REDMINE:
                return \Yii::createObject([
                    'class'    => RedmineLead::className(),
                    'protocol' => $this->params['redmine']['protocol'],
                    'host'     => $this->params['redmine']['host'],
                    'user'     => $this->params['redmine']['user'],
                    'password' => $this->params['redmine']['password'],
                ]);
            case self::TYPE_BITRIX24:
                return \Yii::createObject([
                    'class'    => Bitrix24Lead::className(),
                    'protocol' => $this->params['bitrix24']['protocol'],
                    'host'     => $this->params['bitrix24']['host'],
                    'user'     => $this->params['bitrix24']['user'],
                    'password' => $this->params['bitrix24']['password'],
                ]);
            default:
                throw new UnknownLeadTypeException();
        }
    }

}