<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
use landing\controllers\SiteController;
use landing\views\user\assets\UserAsset;

UserAsset::register($this);
?>

<div class="login_form" id="login_tab">
    <h2><?=t('landing', 'Login')?></h2>
    <div class="lf">

        <?php $form = \yii\widgets\ActiveForm::begin([
            'id' => 'login-form',
            'action' => '/site/login',
            'errorCssClass' => 'error',
            'enableClientValidation' => false,
            'enableAjaxValidation' => true,
            'validateOnBlur' => false,
            'validateOnChange' => false,
            'fieldConfig' => app()->params['fieldOptions'],
        ]); ?>

        <div class="lf_adr">
            <b><?= '.' . $domain; ?></b>
            <?= $form->field($modelLogin, 'domain')->textInput([
                'maxLength' => 255,
                'placeholder' => t('landing', 'Address'),
            ]); ?>

        </div>

        <div class="lf_mail">
            <?= $form->field($modelLogin, 'email')->textInput([
                'maxLength' => 255,
                'placeholder' => t('landing', 'Email'),
            ]); ?>
        </div>

        <div class="lf_pass">
            <?= $form->field($modelLogin, 'password')->passwordInput([
                'maxLength' => 255,
                'placeholder' => t('landing', 'Password'),
            ]); ?>

        </div>

        <?php
        $showCaptcha = session()->get(SiteController::COUNTER_LOGINS) > SiteController::MAX_ATTEMPTS;
        ?>

        <div id="<?= $form->id . '-verifycode-w'; ?>"
             class="lf_mail captcha <?= $showCaptcha ? '' : 'hidden';?>">
            <?= $form->field($modelLogin, 'verifyCode')
                ->widget(Captcha::className(), [
                    'captchaAction' => '/site/captcha',
                ]) ?>
        </div>

        <div class="lf_rest">
            <a class=""><?= t('landing', 'Forgot your password?') ?></a>
        </div>

        <div class="lf_sub">
            <?= Html::submitInput(t('landing', 'To come in'), ['class' => 'button']); ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>

<div class="login_form" id="password_restore_tab" style="display: none;">
    <h2><?=t('landing', 'Forgot your password?')?></h2>
    <div class="lf">
        <?php $form = \yii\widgets\ActiveForm::begin([
            'errorCssClass' => 'error',
            'enableClientValidation' => false,
            'enableAjaxValidation' => true,
            'validateOnBlur' => false,
            'validateOnChange' => false,
            'validationUrl' => yii\helpers\Url::to('/site/recovery-password'),
            'fieldConfig' => app()->params['fieldOptions'],
            'action' => '/site/recovery-password',
        ]); ?>

        <div class="lf_adr">
            <b>.<?= $domain; ?></b>
            <div class="lf_input">
                <?= $form->field($modelRecoveryPassword, 'domain')->textInput([
                    'maxLength' => 255,
                    'placeholder' => t('landing', 'Address'),
                ]); ?>
            </div>
        </div>

        <div class="lf_mail">
            <?= $form->field($modelRecoveryPassword, 'email')->textInput([
                'maxLength' => 255,
                'placeholder' => t('landing', 'Email'),
            ]); ?>
        </div>

        <div class="lf_pass">
            <div class="lf_input">
                <input type="password" disabled placeholder="<?= t('landing', 'Password') ?>"/>
                <span class="er_text"><?= t('landing', 'Not filled out') ?></span>
            </div>
        </div>

        <div class="lf_rest">
            <a class=""><?= t('landing', 'Password found') ?></a>
        </div>

        <div class="lf_sub">
            <?= Html::submitInput(t('landing', 'Send password'), [
                'class' => 'button',
            ]); ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <div class="js-result" style="display: none; text-align: center;">
        <?=t('landing', 'Password restore success result text')?>
    </div>
</div>
