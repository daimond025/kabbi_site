$(document).ready(function () {

    var $b = $('body');
    var gootaxUrl = $b.hasClass('metric-enabled') ? 'https://www.gootax.pro/' : 'https://uatgootax.ru/';

    var metricManager = function (id, state, event, eventCategory, services) {

        if (!$b.hasClass('metric-enabled')) {
            console.log(arguments);
            return false;
        }

        dataLayer.push({
            event: event,
            eventCategory: eventCategory,
            eventAction: id,
            eventLabel: ''
        });
    };

    //LOGIN FORM

    $b.on('click', '.lf_rest a', function () {
        $('#login_tab').toggle();
        $('#password_restore_tab').toggle();
    });

    var legacyUrl = '', bitrix_id = null;

    //Login form
    $('#login-form .lf_sub input[type="submit"]').on('click', function (e) {
        e.preventDefault();

        var error = false;
        var form = $(this).parents('form');
        var $button = $(this);

        form.find('.required').each(function () {
            if (!$(this).hasClass('field-loginform-verifycode'))
                if ($(this).find('input').val() == '') {
                    error = true;
                    $(this).addClass('error');
                    $(this).find('input').next().text('Не заполнено');
                }
                else
                    $(this).removeClass('error');
            else if ($(this).find('input').val() == '' && !$(this).parent().hasClass('hidden')) {
                error = true;
                $(this).addClass('error');
                $(this).find('input').next().text('Не заполнено');
            }
            else
                $(this).removeClass('error');
        });
        if (error)
            return false;

        var fd = new FormData(form[0]);

        $button.attr('disabled', true).addClass('loading');

        $.ajax({
            url: legacyUrl + form.attr('action'),
            data: fd,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function (data) {
                if (data === 'success') {
                    window.top.location.href = window.location.origin + '/site/goto-gootax';
                } else {
                    $button.attr('disabled', false).removeClass('loading');

                    if (data !== null) {
                        var email = form.find('input[name="LoginForm[email]"]');

                        if (typeof(data['loginform-email']) !== 'undefined') {
                            email.parent().addClass('error');
                            email.next().text(data['loginform-email'][0]);
                        }
                        else if (typeof(data['loginform-password']) !== 'undefined') {
                            var password = form.find('input[name="LoginForm[password]"]');
                            password.parent().addClass('error');
                            password.next().text(data['loginform-password'][0]);

                            email.parent().addClass('error');
                            email.next().text('');
                        } else if (typeof(data['loginform-verifycode']) !== 'undefined') {
                            var verifycode = form.find('input[name="LoginForm[loginform-verifycode]"]');
                            verifycode.parent().addClass('error');
                            verifycode.next().text(data['loginform-verifycode'][0]);
                        }
                    }
                    return false;
                }
            },
            error: function (data) {
                $button.attr('disabled', false).removeClass('loading');
                console.log(data);
            }
        });
    });

    //Recovery password
    $('#password_restore_tab input[type="submit"]').on('click', function (e) {
        e.preventDefault();
        var error = false;
        var form = $(this).parents('form');

        form.find('.required').each(function () {
            if ($(this).find('input').val() == '') {
                error = true;
                $(this).parent().addClass('error');
                $(this).next().text('Не заполнено');
            }
            else
                $(this).parent().removeClass('error');
        });

        if (error)
            return false;

        $(this).attr('disabled', true).addClass('loading');
        var $button = $(this);

        var fd = new FormData(form[0]);

        $.ajax({
            url: legacyUrl + form.attr('action'),
            data: fd,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function (data) {
                if (data === 'success') {
                    $button.parents('.lf').hide();
                    $button.parents('.login_form').find('.js-result').show();
                } else {
                    if (data !== null) {
                        var email = form.find('input[name="PasswordResetRequestForm[email]"]');

                        email.parent().addClass('error');
                        email.next().text(data['passwordresetrequestform-email'][0]);
                    }
                }

                $button.attr('disabled', false).removeClass('loading');
            },
            error: function (data) {
                $button.attr('disabled', false).removeClass('loading');
                console.log(data);
            }
        });
    });

    //РЕГИСТРАЦИЯ

    //Функция отправки смс
    function send_sms(phone) {
        $.get(legacyUrl + '/tenant/send-sms-code', {phone: phone}).fail(function () {
            $('.er_text').text('Извините, произошла ошибка и SMS не отправлена. Сообщите нам об ошибке на support@gootax.pro');
            $('.er_text').attr("style", "display: block !important;color: red")
        });
        $('#send_sms_again').addClass('send_block');

        setTimeout(function () {
            $('#send_sms_again').removeClass('send_block');
        }, 15000);
    }

    //Запрет ввода кириллицы
    var old_domain = '';
    $('#signupform-domain').keyup(function (e) {
        var re = /^[a-z0-9\-]*$/,
            BTN_LEFT = 37,
            BTN_RIGHT = 39;

        if (e.keyCode == BTN_LEFT || e.keyCode == BTN_RIGHT) {
            return;
        }

        var domain = $(this).val().toLowerCase();
        if (re.test(domain)) {
            old_domain = domain;
        }
        $(this).val(old_domain);

    });

    //Галочка принятия лицензии
    $('#form-signup .reg_lin label').on('click', function () {
        if ($(this).find('input[type="checkbox"]').prop("checked")) {
            $(this).parents('form').find('input[type="submit"]').prop('disabled', false);
        }
        else {
            $(this).parents('form').find('input[type="submit"]').prop('disabled', true);
        }
    });

    //Кнопка "Далее"
    var old_phone = null; //При нажатии кнопки "Далее" отправки смски только при смене номера
    $('#step_1 .button').on('click', function (e) {
        e.preventDefault();
        var form = $(this).parents('form');
        var error = false;
        var step_1 = $('#step_1');
        var $button = $(this);

        //Валидация
        if (step_1.find('.error').length > 0)
            error = true;

        step_1.find('.required').each(function () {
            var input = $(this).find('input');
            if (input.val() == '') {
                error = true;
                input.parent().addClass('error');
                input.nextAll('.er_text, .help-block-error').text('Не заполнено');
            }
        });

        if (error)
            return false;

        $button.attr('disabled', true).addClass('loading');

        //-------------------------------------------------------

        //Переход на второй шаг, когда все поля заполнены.
        //Валидация при сабмите.
        $.post(
            legacyUrl + '/tenant/signup-validate',
            form.serialize(),
            function (json) {


                if (json == '') {
                    //Отправка смс кода
                    var phone = $('#signupform-phone').val();

                    if (phone != old_phone) {
                        old_phone = phone;
                        send_sms(phone);
                    }

                    var formData = new FormData(form[0]);
                    formData.append('completed', false);

                    if(bitrix_id !== null){
                        formData.append('bitrix_id', bitrix_id);
                        formData.append('update', 'true');
                    }

                    $.ajax({
                        url: gootaxUrl + 'forms/register.php',
                        type: 'POST',
                        data: formData,
                        headers: {
                            'Gootax': 'true'
                        },
                        processData: false,
                        contentType: false,
                        success: function (response) {

                            response = JSON.parse(response);

                            if (response.result == 1 && bitrix_id === null) {
                                bitrix_id = response.id
                            }

                            $button.attr('disabled', false).removeClass('loading');
                            form.find('#step_1').hide();
                            form.find('#step_2').show();
                        }, error: function (error) {
                            console.log(error);

                            $button.attr('disabled', false).removeClass('loading');
                            form.find('#step_1').hide();
                            form.find('#step_2').show();
                        }
                    });

                    metricManager('btnNext', '', 'eventTarget_low', 'target_low');

                }
            },
            'json'
        );
    });

    //Кнопка "Изменить данные"
    $('#go_to_step_1').on('click', function (e) {
        e.preventDefault();
        var form = $(this).parents('form');
        form.find('#step_2').hide();
        form.find('#step_1').show();
    });

    //Кнопка "Подтвердить"
    $('#step_2 .button').on('click', function (e) {
        e.preventDefault();

        var form = $(this).parents('form');
        var button = $(this);
        var sms_code = $('#sms_code');
        var code = sms_code.val();

        if (form.data('process')) {
            return false;
        }

        if (code == '') {
            sms_code.parent().addClass('error');
            sms_code.next().text('Не заполнено');
            return false;
        }

        sms_code.parent().removeClass('error');

        form.data('process', true);
        button.attr('disabled', true).addClass('loading');

        //Проверка смс кода
        $.get(
            legacyUrl + '/tenant/confirm-sms-code',
            {code: code},
            function (json) {
                if (json.error == null) {
                    //Телефон подтвержден, отправляем форму для регистрации
                    $.post(
                        '/tenant/signup',
                        form.serialize(),
                        function (json) {
                            if (json.auth_url != null) {

                                metricManager('btnApprove', '', 'eventTarget_low', 'target_low');
                                metricManager('submitRegister', 'Done', 'eventTarget', 'target');

                                if (bitrix_id !== null) {

                                    var formData = new FormData(form[0]);
                                    formData.append('completed', true);
                                    formData.append('bitrix_id', bitrix_id);
                                    formData.append('update', 'true');
                                    formData.append('tenant_id', json.tenant_id);
                                    formData.append('app_id', json.demo_mobile_app);

                                    $.ajax({
                                        url: gootaxUrl + 'forms/register.php',
                                        type: 'POST',
                                        data: formData,
                                        headers: {
                                            'Gootax': 'true'
                                        },
                                        processData: false,
                                        contentType: false,
                                        success: function () {
                                            window.top.location.href = json.auth_url;
                                        }, error: function (error) {
                                            console.log(error);

                                            window.top.location.href = json.auth_url;
                                        }
                                    });

                                } else {
                                    window.top.location.href = json.auth_url;
                                }

                            }
                        },
                        'json'
                    )
                        .done(function (data) {
                            form.data('process', false);
                            button.attr('disabled', false).removeClass('loading');
                        })
                        .fail(function (data) {
                            console.log('fail', data);
                            form.data('process', false);
                            button.attr('disabled', false).removeClass('loading');
                        });
                } else {
                    sms_code.parent().addClass('error');
                    sms_code.next().text(json.error);

                    form.data('process', false);
                    button.attr('disabled', false).removeClass('loading');
                }
            },
            'json'
        )
            .fail(function (data) {
                console.log('fail', data);
                form.data('process', false);
                button.attr('disabled', false).removeClass('loading');
            });
    });

    //Кнопка "Отправить заново"
    $('#send_sms_again').on('click', function (e) {
        e.preventDefault();

        if ($(this).hasClass('send_block'))
            return false;

        send_sms($('#signupform-phone').val());
    });

    //----------------------------------------------------------------------------------------

    //Телефонная маска 
    var maskList = phoneCodes;
    var maskOpts = {
        inputmask: {
            definitions: {
                '#': {
                    validator: "[0-9]",
                    cardinality: 1
                }
            },
            oncomplete: function () {
                $(this).attr('is-valid', 1);
            },
            onincomplete: function () {
                $(this).attr('is-valid', null);
            },
            showMaskOnHover: false,
            autoUnmask: true
        },
        match: /[0-9]/,
        replace: '#',
        list: maskList,
        listKey: "mask",
    };

    $('.mask_phone').inputmasks(maskOpts);
    $('.callback_phone').inputmasks(maskOpts);


});