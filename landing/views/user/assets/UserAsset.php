<?php

namespace landing\views\user\assets;

use yii\web\AssetBundle;

class UserAsset extends AssetBundle
{
    public $sourcePath = '@landing/views/user/assets/';
    public $publishOptions = [
        'forceCopy' => true
    ];
    public $css = [
        'https://fonts.googleapis.com/css?family=Roboto:300,400,500&amp;subset=cyrillic-ext',
        YII_ENV_PROD ? 'https://gootax.pro/local/templates/gootax/css/login.css' : 'https://uatgootax.ru/local/templates/gootax/css/login.css'
    ];
    public $js = [
        'js/resizerSettings.js',
        'js/iframeResizer.contentWindow.min.js',
        'js/phone-codes.js',
        'js/jquery.bind-first-0.1.min.js',
        'js/jquery.inputmask.js',
        'js/jquery.inputmask-multi.js',
        'js/app.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
