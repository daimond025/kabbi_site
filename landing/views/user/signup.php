<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use landing\views\user\assets\UserAsset;

UserAsset::register($this);
?>
<div class="register">
    <? $form = ActiveForm::begin([
        'id' => 'form-signup',
        'validateOnSubmit' => false,
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'errorCssClass' => 'error',
        'validationUrl' => \yii\helpers\Url::to('/tenant/signup-validate'),
        'action' => ['/tenant/signup'],
        'options' => [
            'onsubmit' => YII_ENV_PROD ? "yaCounter30670468.reachGoal('submitRegister'); ga('send', 'event', 'registrHeader','submit');" : false,
        ],
    ]);
    $activeFieldOptions = ['options' => ['class' => 'lf_input']];
    ?>
    <div id="step_1" class="reg_step_1">
        <div class="lf_adr">
            <b>.<?= $domain ?></b>
            <?= $form->field($model, 'domain', $activeFieldOptions)->begin(); ?>

            <?= Html::activeTextInput($model, 'domain', ['class' => 'red_site', 'autocomplete' => 'off', 'placeholder' => t('landing', 'Address')]); ?>


            <div class="lf_tooltip">
                <?= Html::error($model, 'domain', ['tag' => 'p', 'class' => 'help-block help-block-error']); ?>
                <p class="domain_info"><?= t('landing', 'Invent a memorable address.') ?></p>
                <p class="domain_ok"><?= t('landing', 'The name is free, you can use!') ?></p>
                <?= t('landing', 'The name should be written in Latin letters. You can use numbers and dashes. Number of characters from 4 to 15.') ?>
            </div>

            <?= $form->field($model, 'domain')->end(); ?>
        </div>

        <div class="cm_name">
            <?= $form->field($model, 'last_name', $activeFieldOptions)->begin(); ?>
            <?= Html::activeTextInput($model, 'last_name', ['placeholder' => t('landing', 'Surname')]); ?>
            <?= Html::error($model, 'last_name', ['tag' => 'span', 'class' => 'er_text help-block help-block-error']); ?>
            <?= $form->field($model, 'last_name')->end(); ?>

            <?= $form->field($model, 'name', $activeFieldOptions)->begin(); ?>
            <?= Html::activeTextInput($model, 'name', ['placeholder' => t('landing', 'Name')]); ?>
            <?= Html::error($model, 'name', ['tag' => 'span', 'class' => 'er_text help-block help-block-error']); ?>
            <?= $form->field($model, 'name')->end(); ?>

            <?= $form->field($model, 'second_name', $activeFieldOptions)->begin(); ?>
            <?= Html::activeTextInput($model, 'second_name', ['placeholder' => t('landing', 'Middle name')]); ?>
            <?= Html::error($model, 'second_name', ['tag' => 'span', 'class' => 'er_text help-block help-block-error']); ?>
            <?= $form->field($model, 'second_name')->end(); ?>
        </div>
        <div class="cm_phone">
            <?= $form->field($model, 'email', $activeFieldOptions)->begin(); ?>
            <?= Html::activeTextInput($model, 'email', ['placeholder' => t('landing', 'Email')]); ?>
            <?= Html::error($model, 'email', ['tag' => 'span', 'class' => 'er_text help-block help-block-error']); ?>
            <?= $form->field($model, 'email')->end(); ?>
        </div>
        <div class="reg_phone">
            <?= $form->field($model, 'phone', $activeFieldOptions)->begin(); ?>
            <?= Html::activeTextInput($model, 'phone', ['placeholder' => t('landing', 'Phone'), 'class' => 'mask_phone']); ?>
            <?= Html::error($model, 'phone', ['tag' => 'span', 'class' => 'er_text help-block help-block-error']); ?>
            <?= $form->field($model, 'phone')->end(); ?>
        </div>
        <div class="reg_lin">
            <label><?= Html::activeCheckbox($model, 'agree', ['label' => null]) ?>
                <?= t('landing', 'I accept {tag1} the license agreement {tag2} and {tag3} privacy policy {tag4}',
                    [
                        'tag1' => '<a rel="nofollow noopener" target="_blank" href="https://gootax.pro/files/licence_gootax_pro.pdf">',
                        'tag2' => '</a><br>',
                        'tag3' => '<a rel="nofollow noopener" target="_blank" href="https://gootax.pro/files/confidential_gootax_pro.pdf">',
                        'tag4' => '</a>',
                    ]
                ) ?></label>
        </div>
        <div class="lf_sub">
            <?= Html::submitInput(t('landing', 'Further'), [
                'class' => 'button',
                'disabled' => 'disabled',
            ]) ?>
        </div>
    </div>
    <div id="step_2" class="reg_step_2" style="display: none">
        <div class="lf_input">
            <input id="sms_code" name="sms_code" type="text" placeholder="<?= t('landing', 'Sms code') ?>" value=""/>
            <span class="er_text"><?= t('landing', 'Not filled out') ?></span>
        </div>
        <div class="reg_control">
            <a class="send_block" id="send_sms_again" href=""><?= t('landing', 'Resend') ?></a>
            <a id="go_to_step_1" href=""><?= t('landing', 'To change the data') ?></a>
        </div>
        <div class="lf_sub">
            <?= Html::submitInput(t('landing', 'Confirm'), [
                'class' => 'button',
            ]) ?>
        </div>
    </div>
    <?= $form->field($model, 'ip')->hiddenInput()->label(false); ?>

    <?= $form->field($model, 'utm_source')->hiddenInput()->label(false); ?>

    <?= $form->field($model, 'utm_medium')->hiddenInput()->label(false); ?>

    <?= $form->field($model, 'utm_campaign')->hiddenInput()->label(false); ?>

    <?= $form->field($model, 'utm_content')->hiddenInput()->label(false); ?>

    <?= $form->field($model, 'utm_term')->hiddenInput()->label(false); ?>

    <?= $form->field($model, 'client_id_go')->hiddenInput()->label(false); ?>

    <?= $form->field($model, 'client_id_ya')->hiddenInput()->label(false); ?>

    <?php ActiveForm::end(); ?>
</div>
