(function ($) {
    'use strict';
    var app = {
        init: function () {
            this.registerBeforeValidate($('#signupform'));
            this.registerBeforeValidate($('#communityloginform'));
            this.registerBeforeValidate($('#loginform'));
        },
        registerBeforeValidate: function ($form) {
            var $verifyCode = $form.find('#' + $form.attr('id') + '-verifycode'),
                $verifyImage = $form.find('#' + $form.attr('id') + '-verifycode-image'),
                $submit = $form.find('[type=submit]');

            $form.on('beforeValidate', function (e) {
                if ($form.data('process')) {
                    return false;
                }

                $form.data('process', true);
                $submit.attr('disabled', true);
                $.post(
                        $form.attr('action'),
                        $form.serialize()
                        )
                        .done(function (data) {
                            if (data === 'success') {
                                window.location.reload();
                            } else {
                                if (data['showCaptcha']) {
                                    $verifyCode.show();
                                }
                                $form.yiiActiveForm('updateMessages', data, true);
                                // reset captcha
                                $verifyImage.trigger('click');
                            }
                            $form.data('process', false);
                            $submit.attr('disabled', false);
                        })
                        .fail(function (data) {
                            $form.data('process', false);
                            $submit.attr('disabled', false);
                            console.log('fail', data);
                        });
                return false;
            });
        }
    };

    app.init();
})(jQuery);