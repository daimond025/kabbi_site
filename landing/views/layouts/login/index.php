<?php

use landing\models\LoginForm;
use landing\modules\community\models\user\CommunityLoginForm;
use landing\modules\community\models\user\SignupForm;
use landing\modules\community\models\user\PasswordResetRequestForm;
use app\views\layouts\login\assets\LoginAsset;

$domain       = app()->params['site.domain'];
$fieldOptions = app()->params['fieldOptions'];

LoginAsset::register($this);

?>

<div style="display: none">
    <div id="login-modal" class="remodal" data-remodal-id="login" data-remodal-options="hashTracking: false">
        <button data-remodal-action="close" class="remodal-close"></button>
        <div class="u_tabs" style="display: none;">
            <ul>
                <li><label><input class="utc1" type="radio" name="r01" checked/> Для клиентов Гутакса</label></li>
                <li><label><input class="utc2" type="radio" name="r01"/> Для всех, кто хочет участвовать в&nbsp;сообществе</label></li>
            </ul>
        </div>
        <div class="u_tabls_cont">
            <div id="utc1" style="display: block;">
                <div class="login_form" id="login_tab">
                    <h2>Вход</h2>
                    <div class="lf">
                        <?= $this->render('@app/views/site/user/login', [
                            'model' => new LoginForm([
                                'scenario' => LoginForm::SCENARIO_CAPTCHA,
                            ]),
                            'domain' => $domain,
                        ]); ?>
                    </div>
                </div>
                <div class="login_form" id="password_restore_tab" style="display: none;">
                    <h2>Забыли пароль?</h2>
                    <div class="lf">
                        <?= $this->render('@app/views/site/user/recoveryPassword', [
                            'model' => new landing\models\PasswordResetRequestForm(),
                            'domain' => $domain,
                        ]); ?>
                    </div>
                </div>
            </div>
            <?/*<div id="utc2" style="display: none;">
                <div class="login_form" id="login_tab2">
                    <h2><a rel="lt2_login" class="lt2_tab active">Вход</a> / <a rel="lt2_reg" class="lt2_tab">Регистрация</a></h2>
                    <div class="lf" id="lt2_login" style="display: block;">
                        <?= $this->render('@app/views/site/user/communityLogin', [
                            'model' => new CommunityLoginForm([
                                'scenario' => CommunityLoginForm::SCENARIO_CAPTCHA,
                            ]),
                        ]); ?>
                    </div>
                    <div class="lf" id="lt2_reg" style="display: none;">
                        <?= $this->render('@app/views/site/user/signup', [
                            'model' => new SignupForm([
                                'scenario' => SignupForm::SCENARIO_CAPTCHA,
                            ]),
                        ]); ?>
                    </div>
                </div>
                <div class="login_form" id="password_restore_tab2" style="display: none;">
                    <h2>Забыли пароль?</h2>
                    <div class="lf">
                        <?= $this->render('@app/views/site/user/requestPasswordResetToken', [
                            'model' => new PasswordResetRequestForm(),
                        ]); ?>
                    </div>
                </div>
            </div>*/?>
        </div>
    </div>
</div>