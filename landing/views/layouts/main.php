<?php

use backend\modules\landing\models\PageContent;
use landing\assets\AppAsset;
use landing\widgets\Menu;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);

$phone  = app()->params['contacts.phone'];
$mail   = app()->params['contacts.email'];
$domain = app()->params['site.domain'];
//$skype = app()->params['contacts.skype'];


if (app()->requestedRoute === 'page/index') {
    $page = get('view') ? get('view') : app()->controller->defaultAction;
} else {
    $page = app()->controller->module->id;
}

$page2 = app()->requestedRoute;
$showMenu = PageContent::hasShowMenu($page);
$metaTags = PageContent::getMetaTags($page);

$customMetaTags = app()->params['customMetaTags'];
$metaTags['title'] = empty($customMetaTags['title'])
    ? $metaTags['title'] : $customMetaTags['title'];
$metaTags['description'] = empty($customMetaTags['description'])
    ? $metaTags['description'] : $customMetaTags['description'];
$metaTags['keywords'] = empty($customMetaTags['keywords'])
    ? $metaTags['keywords'] : $customMetaTags['keywords'];

$openGraphMeta = [];
$openGraphMeta['og:site_name'] = empty($customMetaTags['og:site_name']) ? '' : $customMetaTags['og:site_name'];
$openGraphMeta['og:type']      = empty($customMetaTags['og:type']) ? '' : $customMetaTags['og:type'];
$openGraphMeta['og:title']     = empty($customMetaTags['og:title']) ? '' : $customMetaTags['og:title'];
$openGraphMeta['og:url']       = empty($customMetaTags['og:url']) ? '' : $customMetaTags['og:url'];
$openGraphMeta['og:image']     = empty($customMetaTags['og:image']) ? '' : $customMetaTags['og:image'];

$links = [
    'about' => [
        'label' => 'О проекте',
        'url'   => ['/page/index', 'view' => 'index'],
    ],
    'capabilities' => [
        'label' => 'Возможности',
        'url'   => ['/page/index', 'view' => 'capabilities'],
    ],
    'prices' => [
        'label' => 'Тарифы',
        'url'   => ['/page/index', 'view' => 'prices'],
    ],
    'partners' => [
        'label' => 'Партнёрам',
        'url'   => ['/page/index', 'view' => 'partners'],
    ],
    'support' => [
        'label' => 'Поддержка',
        'url'   => ['/page/index', 'view' => 'contacts'],
    ],
    'community' => [
        'label' => 'Сообщество',
        'url' => ['/community/community/index'],
    ],
    'blog' => [
        'label' => 'Блог',
        'url' => ['/community/blog/index'],
    ],
    'offers' => [
        'label' => 'Предложения',
        'url' => ['/community/offers/index'],
    ],
    'communion' => [
        'label' => 'Общение',
        'url' => ['/community/communion/index'],
    ],
    'docs' => [
        'label' => 'Документация',
        'url' => ['/community/docs/index'],
    ],
    'business-plan' => [
        'label' => 'Бизнес-план',
        'url'   => ['/page/index', 'view' => 'business-plan'],
    ],
    'job' => [
        'label' => 'Вакансии',
        'url'   => ['/page/index', 'view' => 'job'],
    ],
    'study'      => [
        'label' => 'Учебный центр',
        'url'   => ['/job#training'],
    ],
    'site'       => [
        'label' => 'Создание сайта для такси',
        'url'   => ['/site'],
    ],
    'mobile_app' => [
        'label' => 'Разработка мобильного приложения',
        'url'   => ['/capabilities/client-app'],
    ],
];
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($metaTags['title']); ?></title>
        <meta name="robots" content="index, follow" />
        <meta name="keywords" content= "<?= Html::encode($metaTags['keywords']); ?>" />
        <meta name="description" content="<?= Html::encode($metaTags['description']); ?>" />
        <meta name="google-site-verification" content="FjdiqHNmMuaCsizSVkZTgZlr0iCLnbOW0sLoJVClALI" />
        <meta name='yandex-verification' content='58f4ac2828798330' />

        <? if (!empty($openGraphMeta)):
            foreach ($openGraphMeta as $key => $value):
                if (!empty($value)): ?>
                <meta property="<?= $key ?>" content="<?= $value ?>">
            <? endif;
            endforeach;
        endif; ?>

        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500&amp;subset=cyrillic-ext" rel="stylesheet">

        <style>
            html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,center,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,embed,figure,figcaption,footer,header,hgroup,menu,nav,output,ruby,section,summary,time,mark,audio,video{margin:0;padding:0;border:0;-webkit-text-size-adjust:none;-webkit-appearance:none !important;font-family:'Roboto', sans-serif;font-weight:300}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}body{background:#fff;color:#000;font-size:18px;-webkit-text-size-adjust:none;-webkit-appearance:none;min-width:1000px}a{text-decoration:none;color:#15af82;cursor:pointer}a:hover{color:#057655}ul{list-style:none}h1{font-size:48px}h2{font-size:40px}h3{font-size:30px}h4{font-size:24px}.main_wrapper,.wrapper{position:relative}.button{display:inline-block;font-size:16px;border:0;outline:none;background:#2cbc92;color:#fff;line-height:16px;padding:6px 18px;font-weight:400;border-radius:4px;border:2px solid #2cbc92;font-family:'Roboto', sans-serif;transition:all .1s ease;cursor:pointer}.button:hover{color:#fff;background:#7bd9bd;border-color:#7bd9bd}.button:active{background:#189c75;border-color:#189c75;color:#fff}.login_button,.login_button:hover,.login_button:active{background:transparent;color:#6d6d6d}.button:disabled,.button:disabled:hover,.button:disabled:active{background:#959595;border-color:#959595;opacity:0.8;cursor:default}.styled li,.inline ul li{display:block;position:relative;padding-left:35px;margin-bottom:15px;line-height:1.6}.styled li:before,.inline ul li:before{content:'';display:block;width:14px;height:1px;background:#9b9b9b;position:absolute;left:0;top:13px}ul.styled li.without_marker:before,.inline ul li.without_marker:before{display:none}sup{color:#a5a5a5}.content{margin:0 auto;min-width:940px;padding:0 30px;max-width:1160px}.main_header{height:45px;padding-top:15px;background-color:#ddf4ec;transition:background-color .1s ease;width:auto !important}.main_header_wrapper{padding:15px 0;background-color:#ddf4ec}.login_block{float:right;position:relative}a.logotype{display:block;float:left;width:139px;height:26px;background:url("/images/logo.svg");background-size:100% 100%}.main_header menu{display:block;margin-left:190px}.main_header menu ul{display:block;list-style:none}.main_header menu li{display:inline-block;margin-right:15px;vertical-align:top;line-height:30px}.main_header menu a{font-weight:400;color:#606060}.main_header menu li.active a{font-weight:500}.main_header menu a:hover{color:#929292}.login_block .login_button{margin-right:8px}.page_header{padding:75px 0 90px 0;text-align:center}.index_header{background:#ddf4ec;padding:60px 0 80px 0}.page_header h1{margin-bottom:30px}.header_description{font-size:20px;line-height:35px}.try_gootax{text-align:center;padding-top:50px}.try_gootax .button{font-size:24px;font-weight:500;padding:18px 32px;position:relative;z-index:2}.try_gootax span{display:block;font-size:20px;margin-top:15px;color:#787878}.page_section{padding:90px 0}.index_app_section .right_col{padding-left:52%;position:relative}.index_app_section .col_icon{position:absolute;left:0;bottom:0;width:50%}.index_app_section .col_icon img{display:block;width:418px;height:659px;margin:-100px auto 0 auto}.index_app_section h2{margin-bottom:25px}.index_app_section .bottom_col{clear:left;padding-top:75px;text-align:center}.index_app_section .bottom_col p{display:block;font-size:16px;margin-bottom:30px;color:#7a7a7a}a.as,a.gp{display:inline-block;width:148px;height:49px;margin:0 10px;background-image:url("/images/app_links.png");background-size:100% 200%;transition:all .1s ease}a.as{background-position:0 0}a.gp{background-position:0 100%}a.as:hover,a.gp:hover{opacity:0.8}.tabs_section h2{text-align:center;margin-bottom:50px}.small_hr{display:block;margin:0 auto;border:0;height:1px;width:104px;background:#b7b7b7}.pluses_section h2{text-align:center;margin-bottom:40px}.pluses_section ul{text-align:center;margin-bottom:-50px}.pluses_section li{display:inline-block;width:30%;box-sizing:border-box;padding:0 20px;text-align:left;vertical-align:top;margin-bottom:50px}.pluses_section h4{margin-bottom:15px}.pluses_section p{line-height:1.6}.pluses_section i{display:block;height:110px;line-height:110px;text-align:center}.pluses_section i img{vertical-align:middle}.start_section{background:url("/images/index_bg_01.jpg") 50% 50%;background-size:cover;color:#fff;text-align:center}.start_section .try_gootax{padding-top:35px}.start_section .try_gootax span{color:#fff}.faq_section,.index_news_section{text-align:center}.faq_section h2{margin-bottom:50px}.index_news_section h2{margin-bottom:30px}.faq_section .content>ul>li.active,.faq_section .content>ul>li.active:hover{background:#fff}.faq_section .content>ul>li.active h4,.faq_section .content>ul>li.active:hover h4{color:#000}.index_news_section ul{margin:0 -30px}.index_news_section li{display:inline-block;width:300px;margin:0 14px;vertical-align:top}.index_news_section li a{display:block;padding:20px;text-align:left;line-height:1.6;cursor:default}.short_story_image{display:block;height:180px;background:#ddd;margin-bottom:25px;background-size:cover;background-position:50% 50%;background-repeat:no-repeat}.short_story_title{font-size:20px;display:block;margin-bottom:15px}.short_story_contnent{color:#000}.short_story_date{color:#7b7b7b;font-size:14px;display:block;margin-top:15px}.close_mobile_menu{position:absolute;z-index:19;width:100%;height:100%;display:none}a.mobile_menu_trigger{display:none;width:23px;height:20px;padding:20px;margin:-17px -20px 0 0;float:right}a.mobile_menu_trigger i{display:block;height:2px;margin-bottom:6px;background:#2cbc92;-moz-transition:all 0.3s ease;-webkit-transition:all 0.3s ease;transition:all 0.3s ease;position:relative;top:0}a.mobile_menu_trigger.active .mmt_3{-moz-transform:rotate(45deg);-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);-o-transform:rotate(45deg);transform:rotate(45deg);top:-8px}a.mobile_menu_trigger.active .mmt_2{opacity:0}a.mobile_menu_trigger.active .mmt_1{-moz-transform:rotate(-45deg);-webkit-transform:rotate(-45deg);-ms-transform:rotate(-45deg);-o-transform:rotate(-45deg);transform:rotate(-45deg);top:8px}.mobile_menu{position:absolute;right:-220px;width:220px;box-sizing:border-box;padding:25px 40px 0 40px;height:100%;z-index:20;background:#2cbc92;-moz-transition:right 0.3s ease;-webkit-transition:right 0.3s ease;transition:right 0.3s ease;display:none}.mobile_menu li{margin-bottom:12px;display:block}.mobile_menu li a{color:#fff}.mobile_menu hr{display:block;padding:0;border:0;margin:17px -40px 29px -40px;height:1px;background:#80d7be}.mobile_menu .button,.mobile_menu .button:hover,.mobile_menu .button:active{background:#fff;border-color:#fff;display:block;text-align:center;color:#2cbc92;margin-bottom:15px}.mobile_menu .login_button,.mobile_menu .login_button:hover,.mobile_menu .login_button:active{background:transparent;color:#fff}.it_header li a{display:inline-block}.it_header ul{text-align:center}.it_header li{display:inline-block;width:220px;text-align:center}.it_header li span,.itc_icon span{display:block;height:50px;line-height:50px;text-align:center;margin-bottom:10px}.it_header li a.active{color:#000}.it_header li a i,.itc_icon i{display:inline-block;background-size:100% 100%;vertical-align:middle}.it_icon01{width:33px;height:31px;background-image:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PHN2ZyB3aWR0aD0iMzNweCIgaGVpZ2h0PSIzMXB4IiB2aWV3Qm94PSIwIDAgMzMgMzEiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+ICAgIDx0aXRsZT5vcmRlcjwvdGl0bGU+ICAgIDxkZXNjPkNyZWF0ZWQgd2l0aCBTa2V0Y2guPC9kZXNjPiAgICA8ZGVmcz4gICAgICAgIDxyZWN0IGlkPSJwYXRoLTEiIHg9IjAiIHk9IjAiIHdpZHRoPSI3IiBoZWlnaHQ9IjciIHJ4PSIxIj48L3JlY3Q+ICAgICAgICA8bWFzayBpZD0ibWFzay0yIiBtYXNrQ29udGVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgbWFza1VuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgeD0iMCIgeT0iMCIgd2lkdGg9IjciIGhlaWdodD0iNyIgZmlsbD0id2hpdGUiPiAgICAgICAgICAgIDx1c2UgeGxpbms6aHJlZj0iI3BhdGgtMSI+PC91c2U+ICAgICAgICA8L21hc2s+ICAgICAgICA8cmVjdCBpZD0icGF0aC0zIiB4PSIwIiB5PSIxMiIgd2lkdGg9IjciIGhlaWdodD0iNyIgcng9IjEiPjwvcmVjdD4gICAgICAgIDxtYXNrIGlkPSJtYXNrLTQiIG1hc2tDb250ZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIiBtYXNrVW5pdHM9Im9iamVjdEJvdW5kaW5nQm94IiB4PSIwIiB5PSIwIiB3aWR0aD0iNyIgaGVpZ2h0PSI3IiBmaWxsPSJ3aGl0ZSI+ICAgICAgICAgICAgPHVzZSB4bGluazpocmVmPSIjcGF0aC0zIj48L3VzZT4gICAgICAgIDwvbWFzaz4gICAgICAgIDxyZWN0IGlkPSJwYXRoLTUiIHg9IjAiIHk9IjI0IiB3aWR0aD0iNyIgaGVpZ2h0PSI3IiByeD0iMSI+PC9yZWN0PiAgICAgICAgPG1hc2sgaWQ9Im1hc2stNiIgbWFza0NvbnRlbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIG1hc2tVbml0cz0ib2JqZWN0Qm91bmRpbmdCb3giIHg9IjAiIHk9IjAiIHdpZHRoPSI3IiBoZWlnaHQ9IjciIGZpbGw9IndoaXRlIj4gICAgICAgICAgICA8dXNlIHhsaW5rOmhyZWY9IiNwYXRoLTUiPjwvdXNlPiAgICAgICAgPC9tYXNrPiAgICA8L2RlZnM+ICAgIDxnIGlkPSLQk9C70LDQstC90LDRjyIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+ICAgICAgICA8ZyBpZD0ib3JkZXIiIHN0cm9rZT0iIzJjYmM5MiI+ICAgICAgICAgICAgPHVzZSBpZD0iUmVjdGFuZ2xlLTE2IiBtYXNrPSJ1cmwoI21hc2stMikiIHN0cm9rZS13aWR0aD0iMiIgeGxpbms6aHJlZj0iI3BhdGgtMSI+PC91c2U+ICAgICAgICAgICAgPHVzZSBpZD0iUmVjdGFuZ2xlLTE2LUNvcHkiIG1hc2s9InVybCgjbWFzay00KSIgc3Ryb2tlLXdpZHRoPSIyIiB4bGluazpocmVmPSIjcGF0aC0zIj48L3VzZT4gICAgICAgICAgICA8dXNlIGlkPSJSZWN0YW5nbGUtMTYtQ29weS0yIiBtYXNrPSJ1cmwoI21hc2stNikiIHN0cm9rZS13aWR0aD0iMiIgeGxpbms6aHJlZj0iI3BhdGgtNSI+PC91c2U+ICAgICAgICAgICAgPHBhdGggZD0iTTExLDMuNSBMMzMsMy41IiBpZD0iTGluZSI+PC9wYXRoPiAgICAgICAgICAgIDxwYXRoIGQ9Ik0xMSwxNS41IEwzMywxNS41IiBpZD0iTGluZS1Db3B5LTMiPjwvcGF0aD4gICAgICAgICAgICA8cGF0aCBkPSJNMTEsMjcuNSBMMzMsMjcuNSIgaWQ9IkxpbmUtQ29weS00Ij48L3BhdGg+ICAgICAgICA8L2c+ICAgIDwvZz48L3N2Zz4=)}.it_header li a:hover .it_icon01{background-image:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PHN2ZyB3aWR0aD0iMzNweCIgaGVpZ2h0PSIzMXB4IiB2aWV3Qm94PSIwIDAgMzMgMzEiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+ICAgIDx0aXRsZT5vcmRlcjwvdGl0bGU+ICAgIDxkZXNjPkNyZWF0ZWQgd2l0aCBTa2V0Y2guPC9kZXNjPiAgICA8ZGVmcz4gICAgICAgIDxyZWN0IGlkPSJwYXRoLTEiIHg9IjAiIHk9IjAiIHdpZHRoPSI3IiBoZWlnaHQ9IjciIHJ4PSIxIj48L3JlY3Q+ICAgICAgICA8bWFzayBpZD0ibWFzay0yIiBtYXNrQ29udGVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgbWFza1VuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgeD0iMCIgeT0iMCIgd2lkdGg9IjciIGhlaWdodD0iNyIgZmlsbD0id2hpdGUiPiAgICAgICAgICAgIDx1c2UgeGxpbms6aHJlZj0iI3BhdGgtMSI+PC91c2U+ICAgICAgICA8L21hc2s+ICAgICAgICA8cmVjdCBpZD0icGF0aC0zIiB4PSIwIiB5PSIxMiIgd2lkdGg9IjciIGhlaWdodD0iNyIgcng9IjEiPjwvcmVjdD4gICAgICAgIDxtYXNrIGlkPSJtYXNrLTQiIG1hc2tDb250ZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIiBtYXNrVW5pdHM9Im9iamVjdEJvdW5kaW5nQm94IiB4PSIwIiB5PSIwIiB3aWR0aD0iNyIgaGVpZ2h0PSI3IiBmaWxsPSJ3aGl0ZSI+ICAgICAgICAgICAgPHVzZSB4bGluazpocmVmPSIjcGF0aC0zIj48L3VzZT4gICAgICAgIDwvbWFzaz4gICAgICAgIDxyZWN0IGlkPSJwYXRoLTUiIHg9IjAiIHk9IjI0IiB3aWR0aD0iNyIgaGVpZ2h0PSI3IiByeD0iMSI+PC9yZWN0PiAgICAgICAgPG1hc2sgaWQ9Im1hc2stNiIgbWFza0NvbnRlbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIG1hc2tVbml0cz0ib2JqZWN0Qm91bmRpbmdCb3giIHg9IjAiIHk9IjAiIHdpZHRoPSI3IiBoZWlnaHQ9IjciIGZpbGw9IndoaXRlIj4gICAgICAgICAgICA8dXNlIHhsaW5rOmhyZWY9IiNwYXRoLTUiPjwvdXNlPiAgICAgICAgPC9tYXNrPiAgICA8L2RlZnM+ICAgIDxnIGlkPSLQk9C70LDQstC90LDRjyIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+ICAgICAgICA8ZyBpZD0ib3JkZXIiIHN0cm9rZT0iIzA1NzY1NSI+ICAgICAgICAgICAgPHVzZSBpZD0iUmVjdGFuZ2xlLTE2IiBtYXNrPSJ1cmwoI21hc2stMikiIHN0cm9rZS13aWR0aD0iMiIgeGxpbms6aHJlZj0iI3BhdGgtMSI+PC91c2U+ICAgICAgICAgICAgPHVzZSBpZD0iUmVjdGFuZ2xlLTE2LUNvcHkiIG1hc2s9InVybCgjbWFzay00KSIgc3Ryb2tlLXdpZHRoPSIyIiB4bGluazpocmVmPSIjcGF0aC0zIj48L3VzZT4gICAgICAgICAgICA8dXNlIGlkPSJSZWN0YW5nbGUtMTYtQ29weS0yIiBtYXNrPSJ1cmwoI21hc2stNikiIHN0cm9rZS13aWR0aD0iMiIgeGxpbms6aHJlZj0iI3BhdGgtNSI+PC91c2U+ICAgICAgICAgICAgPHBhdGggZD0iTTExLDMuNSBMMzMsMy41IiBpZD0iTGluZSI+PC9wYXRoPiAgICAgICAgICAgIDxwYXRoIGQ9Ik0xMSwxNS41IEwzMywxNS41IiBpZD0iTGluZS1Db3B5LTMiPjwvcGF0aD4gICAgICAgICAgICA8cGF0aCBkPSJNMTEsMjcuNSBMMzMsMjcuNSIgaWQ9IkxpbmUtQ29weS00Ij48L3BhdGg+ICAgICAgICA8L2c+ICAgIDwvZz48L3N2Zz4=)}.it_header li a.active .it_icon01,.itc_icon .it_icon01{background-image:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PHN2ZyB3aWR0aD0iMzNweCIgaGVpZ2h0PSIzMXB4IiB2aWV3Qm94PSIwIDAgMzMgMzEiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+ICAgIDx0aXRsZT5vcmRlcjwvdGl0bGU+ICAgIDxkZXNjPkNyZWF0ZWQgd2l0aCBTa2V0Y2guPC9kZXNjPiAgICA8ZGVmcz4gICAgICAgIDxyZWN0IGlkPSJwYXRoLTEiIHg9IjAiIHk9IjAiIHdpZHRoPSI3IiBoZWlnaHQ9IjciIHJ4PSIxIj48L3JlY3Q+ICAgICAgICA8bWFzayBpZD0ibWFzay0yIiBtYXNrQ29udGVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgbWFza1VuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgeD0iMCIgeT0iMCIgd2lkdGg9IjciIGhlaWdodD0iNyIgZmlsbD0id2hpdGUiPiAgICAgICAgICAgIDx1c2UgeGxpbms6aHJlZj0iI3BhdGgtMSI+PC91c2U+ICAgICAgICA8L21hc2s+ICAgICAgICA8cmVjdCBpZD0icGF0aC0zIiB4PSIwIiB5PSIxMiIgd2lkdGg9IjciIGhlaWdodD0iNyIgcng9IjEiPjwvcmVjdD4gICAgICAgIDxtYXNrIGlkPSJtYXNrLTQiIG1hc2tDb250ZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIiBtYXNrVW5pdHM9Im9iamVjdEJvdW5kaW5nQm94IiB4PSIwIiB5PSIwIiB3aWR0aD0iNyIgaGVpZ2h0PSI3IiBmaWxsPSJ3aGl0ZSI+ICAgICAgICAgICAgPHVzZSB4bGluazpocmVmPSIjcGF0aC0zIj48L3VzZT4gICAgICAgIDwvbWFzaz4gICAgICAgIDxyZWN0IGlkPSJwYXRoLTUiIHg9IjAiIHk9IjI0IiB3aWR0aD0iNyIgaGVpZ2h0PSI3IiByeD0iMSI+PC9yZWN0PiAgICAgICAgPG1hc2sgaWQ9Im1hc2stNiIgbWFza0NvbnRlbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIG1hc2tVbml0cz0ib2JqZWN0Qm91bmRpbmdCb3giIHg9IjAiIHk9IjAiIHdpZHRoPSI3IiBoZWlnaHQ9IjciIGZpbGw9IndoaXRlIj4gICAgICAgICAgICA8dXNlIHhsaW5rOmhyZWY9IiNwYXRoLTUiPjwvdXNlPiAgICAgICAgPC9tYXNrPiAgICA8L2RlZnM+ICAgIDxnIGlkPSLQk9C70LDQstC90LDRjyIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+ICAgICAgICA8ZyBpZD0ib3JkZXIiIHN0cm9rZT0iIzAzMDMwMyI+ICAgICAgICAgICAgPHVzZSBpZD0iUmVjdGFuZ2xlLTE2IiBtYXNrPSJ1cmwoI21hc2stMikiIHN0cm9rZS13aWR0aD0iMiIgeGxpbms6aHJlZj0iI3BhdGgtMSI+PC91c2U+ICAgICAgICAgICAgPHVzZSBpZD0iUmVjdGFuZ2xlLTE2LUNvcHkiIG1hc2s9InVybCgjbWFzay00KSIgc3Ryb2tlLXdpZHRoPSIyIiB4bGluazpocmVmPSIjcGF0aC0zIj48L3VzZT4gICAgICAgICAgICA8dXNlIGlkPSJSZWN0YW5nbGUtMTYtQ29weS0yIiBtYXNrPSJ1cmwoI21hc2stNikiIHN0cm9rZS13aWR0aD0iMiIgeGxpbms6aHJlZj0iI3BhdGgtNSI+PC91c2U+ICAgICAgICAgICAgPHBhdGggZD0iTTExLDMuNSBMMzMsMy41IiBpZD0iTGluZSI+PC9wYXRoPiAgICAgICAgICAgIDxwYXRoIGQ9Ik0xMSwxNS41IEwzMywxNS41IiBpZD0iTGluZS1Db3B5LTMiPjwvcGF0aD4gICAgICAgICAgICA8cGF0aCBkPSJNMTEsMjcuNSBMMzMsMjcuNSIgaWQ9IkxpbmUtQ29weS00Ij48L3BhdGg+ICAgICAgICA8L2c+ICAgIDwvZz48L3N2Zz4=)}.it_icon02{width:37px;height:37px;background-image:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PHN2ZyB3aWR0aD0iMzdweCIgaGVpZ2h0PSIzN3B4IiB2aWV3Qm94PSIwIDAgMzcgMzciIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+ICAgIDx0aXRsZT5hbmFsaXRpYzwvdGl0bGU+ICAgIDxkZXNjPkNyZWF0ZWQgd2l0aCBTa2V0Y2guPC9kZXNjPiAgICA8ZGVmcz48L2RlZnM+ICAgIDxnIGlkPSLQk9C70LDQstC90LDRjyIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+ICAgICAgICA8ZyBpZD0iYW5hbGl0aWMiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDEuMDAwMDAwLCAxLjAwMDAwMCkiIHN0cm9rZT0iIzJDQkM5MiI+ICAgICAgICAgICAgPHBhdGggZD0iTTE1LjUsMzUgQzI0LjA2MDQxMzYsMzUgMzEsMjguMDYwNDEzNiAzMSwxOS41IEwxNS41LDE5LjUgTDE1LjUsNCBDNi45Mzk1ODYzOCw0IDAsMTAuOTM5NTg2NCAwLDE5LjUgQzAsMjguMDYwNDEzNiA2LjkzOTU4NjM4LDM1IDE1LjUsMzUgWiIgaWQ9Ik92YWwtMyI+PC9wYXRoPiAgICAgICAgICAgIDxwYXRoIGQ9Ik0zNC41MjQwNjExLDE2LjQ5NTgxMDIgQzM0LjUyNDA2MTEsNy42NTkyNTQyMyAyNy4zNjA2MTcxLDAuNDk1ODEwMjMgMTguNTI0MDYxMSwwLjQ5NTgxMDIzIEMxOC41MjQwNjExLDAuNDk1ODEwMjMgMTguNTI0MDYxMSwxNi40OTU4MTAyIDE4LjUyNDA2MTEsMTYuNDk1ODEwMiBDMTguNTI0MDYxMSwxNi40OTU4MTAyIDM0LjUyNDA2MTEsMTYuNDk1ODEwMiAzNC41MjQwNjExLDE2LjQ5NTgxMDIgWiIgaWQ9Ik92YWwtNCI+PC9wYXRoPiAgICAgICAgPC9nPiAgICA8L2c+PC9zdmc+)}.it_header li a:hover .it_icon02{background-image:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PHN2ZyB3aWR0aD0iMzdweCIgaGVpZ2h0PSIzN3B4IiB2aWV3Qm94PSIwIDAgMzcgMzciIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+ICAgIDx0aXRsZT5hbmFsaXRpYzwvdGl0bGU+ICAgIDxkZXNjPkNyZWF0ZWQgd2l0aCBTa2V0Y2guPC9kZXNjPiAgICA8ZGVmcz48L2RlZnM+ICAgIDxnIGlkPSLQk9C70LDQstC90LDRjyIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+ICAgICAgICA8ZyBpZD0iYW5hbGl0aWMiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDEuMDAwMDAwLCAxLjAwMDAwMCkiIHN0cm9rZT0iIzA1NzY1NSI+ICAgICAgICAgICAgPHBhdGggZD0iTTE1LjUsMzUgQzI0LjA2MDQxMzYsMzUgMzEsMjguMDYwNDEzNiAzMSwxOS41IEwxNS41LDE5LjUgTDE1LjUsNCBDNi45Mzk1ODYzOCw0IDAsMTAuOTM5NTg2NCAwLDE5LjUgQzAsMjguMDYwNDEzNiA2LjkzOTU4NjM4LDM1IDE1LjUsMzUgWiIgaWQ9Ik92YWwtMyI+PC9wYXRoPiAgICAgICAgICAgIDxwYXRoIGQ9Ik0zNC41MjQwNjExLDE2LjQ5NTgxMDIgQzM0LjUyNDA2MTEsNy42NTkyNTQyMyAyNy4zNjA2MTcxLDAuNDk1ODEwMjMgMTguNTI0MDYxMSwwLjQ5NTgxMDIzIEMxOC41MjQwNjExLDAuNDk1ODEwMjMgMTguNTI0MDYxMSwxNi40OTU4MTAyIDE4LjUyNDA2MTEsMTYuNDk1ODEwMiBDMTguNTI0MDYxMSwxNi40OTU4MTAyIDM0LjUyNDA2MTEsMTYuNDk1ODEwMiAzNC41MjQwNjExLDE2LjQ5NTgxMDIgWiIgaWQ9Ik92YWwtNCI+PC9wYXRoPiAgICAgICAgPC9nPiAgICA8L2c+PC9zdmc+)}.it_header li a.active .it_icon02,.itc_icon .it_icon02{background-image:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PHN2ZyB3aWR0aD0iMzdweCIgaGVpZ2h0PSIzN3B4IiB2aWV3Qm94PSIwIDAgMzcgMzciIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+ICAgIDx0aXRsZT5hbmFsaXRpYzwvdGl0bGU+ICAgIDxkZXNjPkNyZWF0ZWQgd2l0aCBTa2V0Y2guPC9kZXNjPiAgICA8ZGVmcz48L2RlZnM+ICAgIDxnIGlkPSLQk9C70LDQstC90LDRjyIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+ICAgICAgICA8ZyBpZD0iYW5hbGl0aWMiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDEuMDAwMDAwLCAxLjAwMDAwMCkiIHN0cm9rZT0iIzA0MDQwNCI+ICAgICAgICAgICAgPHBhdGggZD0iTTE1LjUsMzUgQzI0LjA2MDQxMzYsMzUgMzEsMjguMDYwNDEzNiAzMSwxOS41IEwxNS41LDE5LjUgTDE1LjUsNCBDNi45Mzk1ODYzOCw0IDAsMTAuOTM5NTg2NCAwLDE5LjUgQzAsMjguMDYwNDEzNiA2LjkzOTU4NjM4LDM1IDE1LjUsMzUgWiIgaWQ9Ik92YWwtMyI+PC9wYXRoPiAgICAgICAgICAgIDxwYXRoIGQ9Ik0zNC41MjQwNjExLDE2LjQ5NTgxMDIgQzM0LjUyNDA2MTEsNy42NTkyNTQyMyAyNy4zNjA2MTcxLDAuNDk1ODEwMjMgMTguNTI0MDYxMSwwLjQ5NTgxMDIzIEMxOC41MjQwNjExLDAuNDk1ODEwMjMgMTguNTI0MDYxMSwxNi40OTU4MTAyIDE4LjUyNDA2MTEsMTYuNDk1ODEwMiBDMTguNTI0MDYxMSwxNi40OTU4MTAyIDM0LjUyNDA2MTEsMTYuNDk1ODEwMiAzNC41MjQwNjExLDE2LjQ5NTgxMDIgWiIgaWQ9Ik92YWwtNCI+PC9wYXRoPiAgICAgICAgPC9nPiAgICA8L2c+PC9zdmc+)}.it_icon03{width:35px;height:30px;background-image:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PHN2ZyB3aWR0aD0iMzVweCIgaGVpZ2h0PSIzMHB4IiB2aWV3Qm94PSIwIDAgMzUgMzAiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+ICAgIDx0aXRsZT5pbnRlZ3JhdGlvbjwvdGl0bGU+ICAgIDxkZXNjPkNyZWF0ZWQgd2l0aCBTa2V0Y2guPC9kZXNjPiAgICA8ZGVmcz4gICAgICAgIDxyZWN0IGlkPSJwYXRoLTEiIHg9IjI0IiB5PSIyMCIgd2lkdGg9IjExIiBoZWlnaHQ9IjEwIj48L3JlY3Q+ICAgICAgICA8bWFzayBpZD0ibWFzay0yIiBtYXNrQ29udGVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgbWFza1VuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgeD0iMCIgeT0iMCIgd2lkdGg9IjExIiBoZWlnaHQ9IjEwIiBmaWxsPSJ3aGl0ZSI+ICAgICAgICAgICAgPHVzZSB4bGluazpocmVmPSIjcGF0aC0xIj48L3VzZT4gICAgICAgIDwvbWFzaz4gICAgICAgIDxyZWN0IGlkPSJwYXRoLTMiIHg9IjAiIHk9IjIwIiB3aWR0aD0iMTEiIGhlaWdodD0iMTAiPjwvcmVjdD4gICAgICAgIDxtYXNrIGlkPSJtYXNrLTQiIG1hc2tDb250ZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIiBtYXNrVW5pdHM9Im9iamVjdEJvdW5kaW5nQm94IiB4PSIwIiB5PSIwIiB3aWR0aD0iMTEiIGhlaWdodD0iMTAiIGZpbGw9IndoaXRlIj4gICAgICAgICAgICA8dXNlIHhsaW5rOmhyZWY9IiNwYXRoLTMiPjwvdXNlPiAgICAgICAgPC9tYXNrPiAgICAgICAgPHJlY3QgaWQ9InBhdGgtNSIgeD0iMTIiIHk9IjAiIHdpZHRoPSIxMSIgaGVpZ2h0PSIxMCI+PC9yZWN0PiAgICAgICAgPG1hc2sgaWQ9Im1hc2stNiIgbWFza0NvbnRlbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIG1hc2tVbml0cz0ib2JqZWN0Qm91bmRpbmdCb3giIHg9IjAiIHk9IjAiIHdpZHRoPSIxMSIgaGVpZ2h0PSIxMCIgZmlsbD0id2hpdGUiPiAgICAgICAgICAgIDx1c2UgeGxpbms6aHJlZj0iI3BhdGgtNSI+PC91c2U+ICAgICAgICA8L21hc2s+ICAgIDwvZGVmcz4gICAgPGcgaWQ9ItCT0LvQsNCy0L3QsNGPIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj4gICAgICAgIDxnIGlkPSJpbnRlZ3JhdGlvbiIgc3Ryb2tlPSIjMkNCQzkyIj4gICAgICAgICAgICA8dXNlIGlkPSJSZWN0YW5nbGUtOSIgbWFzaz0idXJsKCNtYXNrLTIpIiBzdHJva2Utd2lkdGg9IjIiIHhsaW5rOmhyZWY9IiNwYXRoLTEiPjwvdXNlPiAgICAgICAgICAgIDx1c2UgaWQ9IlJlY3RhbmdsZS05LUNvcHkiIG1hc2s9InVybCgjbWFzay00KSIgc3Ryb2tlLXdpZHRoPSIyIiB4bGluazpocmVmPSIjcGF0aC0zIj48L3VzZT4gICAgICAgICAgICA8dXNlIGlkPSJSZWN0YW5nbGUtOS1Db3B5LTIiIG1hc2s9InVybCgjbWFzay02KSIgc3Ryb2tlLXdpZHRoPSIyIiB4bGluazpocmVmPSIjcGF0aC01Ij48L3VzZT4gICAgICAgICAgICA8cGF0aCBkPSJNMTcuNSw5LjUgTDE3LjUsMTUuNSIgaWQ9IkxpbmUiIHN0cm9rZS1saW5lY2FwPSJzcXVhcmUiPjwvcGF0aD4gICAgICAgICAgICA8cGF0aCBkPSJNMjkuNSwxNi41IEwyOS41LDE5LjUiIGlkPSJMaW5lLUNvcHkiIHN0cm9rZS1saW5lY2FwPSJzcXVhcmUiPjwvcGF0aD4gICAgICAgICAgICA8cGF0aCBkPSJNNS41LDE1LjUgTDUuNSwxOS41IiBpZD0iTGluZS1Db3B5LTIiIHN0cm9rZS1saW5lY2FwPSJzcXVhcmUiPjwvcGF0aD4gICAgICAgICAgICA8cGF0aCBkPSJNMjkuNSwxNS41IEw1LjUsMTUuNSIgaWQ9IkxpbmUiIHN0cm9rZS1saW5lY2FwPSJzcXVhcmUiPjwvcGF0aD4gICAgICAgIDwvZz4gICAgPC9nPjwvc3ZnPg==)}.it_header li a:hover .it_icon03{background-image:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PHN2ZyB3aWR0aD0iMzVweCIgaGVpZ2h0PSIzMHB4IiB2aWV3Qm94PSIwIDAgMzUgMzAiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+ICAgIDx0aXRsZT5pbnRlZ3JhdGlvbjwvdGl0bGU+ICAgIDxkZXNjPkNyZWF0ZWQgd2l0aCBTa2V0Y2guPC9kZXNjPiAgICA8ZGVmcz4gICAgICAgIDxyZWN0IGlkPSJwYXRoLTEiIHg9IjI0IiB5PSIyMCIgd2lkdGg9IjExIiBoZWlnaHQ9IjEwIj48L3JlY3Q+ICAgICAgICA8bWFzayBpZD0ibWFzay0yIiBtYXNrQ29udGVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgbWFza1VuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgeD0iMCIgeT0iMCIgd2lkdGg9IjExIiBoZWlnaHQ9IjEwIiBmaWxsPSJ3aGl0ZSI+ICAgICAgICAgICAgPHVzZSB4bGluazpocmVmPSIjcGF0aC0xIj48L3VzZT4gICAgICAgIDwvbWFzaz4gICAgICAgIDxyZWN0IGlkPSJwYXRoLTMiIHg9IjAiIHk9IjIwIiB3aWR0aD0iMTEiIGhlaWdodD0iMTAiPjwvcmVjdD4gICAgICAgIDxtYXNrIGlkPSJtYXNrLTQiIG1hc2tDb250ZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIiBtYXNrVW5pdHM9Im9iamVjdEJvdW5kaW5nQm94IiB4PSIwIiB5PSIwIiB3aWR0aD0iMTEiIGhlaWdodD0iMTAiIGZpbGw9IndoaXRlIj4gICAgICAgICAgICA8dXNlIHhsaW5rOmhyZWY9IiNwYXRoLTMiPjwvdXNlPiAgICAgICAgPC9tYXNrPiAgICAgICAgPHJlY3QgaWQ9InBhdGgtNSIgeD0iMTIiIHk9IjAiIHdpZHRoPSIxMSIgaGVpZ2h0PSIxMCI+PC9yZWN0PiAgICAgICAgPG1hc2sgaWQ9Im1hc2stNiIgbWFza0NvbnRlbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIG1hc2tVbml0cz0ib2JqZWN0Qm91bmRpbmdCb3giIHg9IjAiIHk9IjAiIHdpZHRoPSIxMSIgaGVpZ2h0PSIxMCIgZmlsbD0id2hpdGUiPiAgICAgICAgICAgIDx1c2UgeGxpbms6aHJlZj0iI3BhdGgtNSI+PC91c2U+ICAgICAgICA8L21hc2s+ICAgIDwvZGVmcz4gICAgPGcgaWQ9ItCT0LvQsNCy0L3QsNGPIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj4gICAgICAgIDxnIGlkPSJpbnRlZ3JhdGlvbiIgc3Ryb2tlPSIjMDU3NjU1Ij4gICAgICAgICAgICA8dXNlIGlkPSJSZWN0YW5nbGUtOSIgbWFzaz0idXJsKCNtYXNrLTIpIiBzdHJva2Utd2lkdGg9IjIiIHhsaW5rOmhyZWY9IiNwYXRoLTEiPjwvdXNlPiAgICAgICAgICAgIDx1c2UgaWQ9IlJlY3RhbmdsZS05LUNvcHkiIG1hc2s9InVybCgjbWFzay00KSIgc3Ryb2tlLXdpZHRoPSIyIiB4bGluazpocmVmPSIjcGF0aC0zIj48L3VzZT4gICAgICAgICAgICA8dXNlIGlkPSJSZWN0YW5nbGUtOS1Db3B5LTIiIG1hc2s9InVybCgjbWFzay02KSIgc3Ryb2tlLXdpZHRoPSIyIiB4bGluazpocmVmPSIjcGF0aC01Ij48L3VzZT4gICAgICAgICAgICA8cGF0aCBkPSJNMTcuNSw5LjUgTDE3LjUsMTUuNSIgaWQ9IkxpbmUiIHN0cm9rZS1saW5lY2FwPSJzcXVhcmUiPjwvcGF0aD4gICAgICAgICAgICA8cGF0aCBkPSJNMjkuNSwxNi41IEwyOS41LDE5LjUiIGlkPSJMaW5lLUNvcHkiIHN0cm9rZS1saW5lY2FwPSJzcXVhcmUiPjwvcGF0aD4gICAgICAgICAgICA8cGF0aCBkPSJNNS41LDE1LjUgTDUuNSwxOS41IiBpZD0iTGluZS1Db3B5LTIiIHN0cm9rZS1saW5lY2FwPSJzcXVhcmUiPjwvcGF0aD4gICAgICAgICAgICA8cGF0aCBkPSJNMjkuNSwxNS41IEw1LjUsMTUuNSIgaWQ9IkxpbmUiIHN0cm9rZS1saW5lY2FwPSJzcXVhcmUiPjwvcGF0aD4gICAgICAgIDwvZz4gICAgPC9nPjwvc3ZnPg==)}.it_header li a.active .it_icon03,.itc_icon .it_icon03{background-image:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PHN2ZyB3aWR0aD0iMzVweCIgaGVpZ2h0PSIzMHB4IiB2aWV3Qm94PSIwIDAgMzUgMzAiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+ICAgIDx0aXRsZT5pbnRlZ3JhdGlvbjwvdGl0bGU+ICAgIDxkZXNjPkNyZWF0ZWQgd2l0aCBTa2V0Y2guPC9kZXNjPiAgICA8ZGVmcz4gICAgICAgIDxyZWN0IGlkPSJwYXRoLTEiIHg9IjI0IiB5PSIyMCIgd2lkdGg9IjExIiBoZWlnaHQ9IjEwIj48L3JlY3Q+ICAgICAgICA8bWFzayBpZD0ibWFzay0yIiBtYXNrQ29udGVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgbWFza1VuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgeD0iMCIgeT0iMCIgd2lkdGg9IjExIiBoZWlnaHQ9IjEwIiBmaWxsPSJ3aGl0ZSI+ICAgICAgICAgICAgPHVzZSB4bGluazpocmVmPSIjcGF0aC0xIj48L3VzZT4gICAgICAgIDwvbWFzaz4gICAgICAgIDxyZWN0IGlkPSJwYXRoLTMiIHg9IjAiIHk9IjIwIiB3aWR0aD0iMTEiIGhlaWdodD0iMTAiPjwvcmVjdD4gICAgICAgIDxtYXNrIGlkPSJtYXNrLTQiIG1hc2tDb250ZW50VW5pdHM9InVzZXJTcGFjZU9uVXNlIiBtYXNrVW5pdHM9Im9iamVjdEJvdW5kaW5nQm94IiB4PSIwIiB5PSIwIiB3aWR0aD0iMTEiIGhlaWdodD0iMTAiIGZpbGw9IndoaXRlIj4gICAgICAgICAgICA8dXNlIHhsaW5rOmhyZWY9IiNwYXRoLTMiPjwvdXNlPiAgICAgICAgPC9tYXNrPiAgICAgICAgPHJlY3QgaWQ9InBhdGgtNSIgeD0iMTIiIHk9IjAiIHdpZHRoPSIxMSIgaGVpZ2h0PSIxMCI+PC9yZWN0PiAgICAgICAgPG1hc2sgaWQ9Im1hc2stNiIgbWFza0NvbnRlbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIG1hc2tVbml0cz0ib2JqZWN0Qm91bmRpbmdCb3giIHg9IjAiIHk9IjAiIHdpZHRoPSIxMSIgaGVpZ2h0PSIxMCIgZmlsbD0id2hpdGUiPiAgICAgICAgICAgIDx1c2UgeGxpbms6aHJlZj0iI3BhdGgtNSI+PC91c2U+ICAgICAgICA8L21hc2s+ICAgIDwvZGVmcz4gICAgPGcgaWQ9ItCT0LvQsNCy0L3QsNGPIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj4gICAgICAgIDxnIGlkPSJpbnRlZ3JhdGlvbiIgc3Ryb2tlPSIjMDQwNDA0Ij4gICAgICAgICAgICA8dXNlIGlkPSJSZWN0YW5nbGUtOSIgbWFzaz0idXJsKCNtYXNrLTIpIiBzdHJva2Utd2lkdGg9IjIiIHhsaW5rOmhyZWY9IiNwYXRoLTEiPjwvdXNlPiAgICAgICAgICAgIDx1c2UgaWQ9IlJlY3RhbmdsZS05LUNvcHkiIG1hc2s9InVybCgjbWFzay00KSIgc3Ryb2tlLXdpZHRoPSIyIiB4bGluazpocmVmPSIjcGF0aC0zIj48L3VzZT4gICAgICAgICAgICA8dXNlIGlkPSJSZWN0YW5nbGUtOS1Db3B5LTIiIG1hc2s9InVybCgjbWFzay02KSIgc3Ryb2tlLXdpZHRoPSIyIiB4bGluazpocmVmPSIjcGF0aC01Ij48L3VzZT4gICAgICAgICAgICA8cGF0aCBkPSJNMTcuNSw5LjUgTDE3LjUsMTUuNSIgaWQ9IkxpbmUiIHN0cm9rZS1saW5lY2FwPSJzcXVhcmUiPjwvcGF0aD4gICAgICAgICAgICA8cGF0aCBkPSJNMjkuNSwxNi41IEwyOS41LDE5LjUiIGlkPSJMaW5lLUNvcHkiIHN0cm9rZS1saW5lY2FwPSJzcXVhcmUiPjwvcGF0aD4gICAgICAgICAgICA8cGF0aCBkPSJNNS41LDE1LjUgTDUuNSwxOS41IiBpZD0iTGluZS1Db3B5LTIiIHN0cm9rZS1saW5lY2FwPSJzcXVhcmUiPjwvcGF0aD4gICAgICAgICAgICA8cGF0aCBkPSJNMjkuNSwxNS41IEw1LjUsMTUuNSIgaWQ9IkxpbmUiIHN0cm9rZS1saW5lY2FwPSJzcXVhcmUiPjwvcGF0aD4gICAgICAgIDwvZz4gICAgPC9nPjwvc3ZnPg==)}.it_icon04{width:24px;height:36px;background-image:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PHN2ZyB3aWR0aD0iMjRweCIgaGVpZ2h0PSIzNnB4IiB2aWV3Qm94PSIwIDAgMjQgMzYiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+ICAgIDx0aXRsZT5tb2JpbGU8L3RpdGxlPiAgICA8ZGVzYz5DcmVhdGVkIHdpdGggU2tldGNoLjwvZGVzYz4gICAgPGRlZnM+ICAgICAgICA8cmVjdCBpZD0icGF0aC0xIiB4PSIwIiB5PSIwIiB3aWR0aD0iMjQiIGhlaWdodD0iMzYiIHJ4PSI0Ij48L3JlY3Q+ICAgICAgICA8bWFzayBpZD0ibWFzay0yIiBtYXNrQ29udGVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgbWFza1VuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgeD0iMCIgeT0iMCIgd2lkdGg9IjI0IiBoZWlnaHQ9IjM2IiBmaWxsPSJ3aGl0ZSI+ICAgICAgICAgICAgPHVzZSB4bGluazpocmVmPSIjcGF0aC0xIj48L3VzZT4gICAgICAgIDwvbWFzaz4gICAgICAgIDxyZWN0IGlkPSJwYXRoLTMiIHg9IjQiIHk9IjQiIHdpZHRoPSIxNiIgaGVpZ2h0PSIyNSI+PC9yZWN0PiAgICAgICAgPG1hc2sgaWQ9Im1hc2stNCIgbWFza0NvbnRlbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIG1hc2tVbml0cz0ib2JqZWN0Qm91bmRpbmdCb3giIHg9IjAiIHk9IjAiIHdpZHRoPSIxNiIgaGVpZ2h0PSIyNSIgZmlsbD0id2hpdGUiPiAgICAgICAgICAgIDx1c2UgeGxpbms6aHJlZj0iI3BhdGgtMyI+PC91c2U+ICAgICAgICA8L21hc2s+ICAgIDwvZGVmcz4gICAgPGcgaWQ9ItCT0LvQsNCy0L3QsNGPIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj4gICAgICAgIDxnIGlkPSJtb2JpbGUiPiAgICAgICAgICAgIDx1c2UgaWQ9IlJlY3RhbmdsZS0yMCIgc3Ryb2tlPSIjMkNCQzkyIiBtYXNrPSJ1cmwoI21hc2stMikiIHN0cm9rZS13aWR0aD0iMiIgeGxpbms6aHJlZj0iI3BhdGgtMSI+PC91c2U+ICAgICAgICAgICAgPHVzZSBpZD0iUmVjdGFuZ2xlLTIwLUNvcHkiIHN0cm9rZT0iIzJDQkM5MiIgbWFzaz0idXJsKCNtYXNrLTQpIiBzdHJva2Utd2lkdGg9IjIiIHhsaW5rOmhyZWY9IiNwYXRoLTMiPjwvdXNlPiAgICAgICAgICAgIDxjaXJjbGUgaWQ9Ik92YWwtMjQiIGZpbGw9IiMyQ0JDOTIiIGN4PSIxMiIgY3k9IjMyIiByPSIxIj48L2NpcmNsZT4gICAgICAgIDwvZz4gICAgPC9nPjwvc3ZnPg==)}.it_header li a:hover .it_icon04{background-image:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PHN2ZyB3aWR0aD0iMjRweCIgaGVpZ2h0PSIzNnB4IiB2aWV3Qm94PSIwIDAgMjQgMzYiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+ICAgIDx0aXRsZT5tb2JpbGU8L3RpdGxlPiAgICA8ZGVzYz5DcmVhdGVkIHdpdGggU2tldGNoLjwvZGVzYz4gICAgPGRlZnM+ICAgICAgICA8cmVjdCBpZD0icGF0aC0xIiB4PSIwIiB5PSIwIiB3aWR0aD0iMjQiIGhlaWdodD0iMzYiIHJ4PSI0Ij48L3JlY3Q+ICAgICAgICA8bWFzayBpZD0ibWFzay0yIiBtYXNrQ29udGVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgbWFza1VuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgeD0iMCIgeT0iMCIgd2lkdGg9IjI0IiBoZWlnaHQ9IjM2IiBmaWxsPSJ3aGl0ZSI+ICAgICAgICAgICAgPHVzZSB4bGluazpocmVmPSIjcGF0aC0xIj48L3VzZT4gICAgICAgIDwvbWFzaz4gICAgICAgIDxyZWN0IGlkPSJwYXRoLTMiIHg9IjQiIHk9IjQiIHdpZHRoPSIxNiIgaGVpZ2h0PSIyNSI+PC9yZWN0PiAgICAgICAgPG1hc2sgaWQ9Im1hc2stNCIgbWFza0NvbnRlbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIG1hc2tVbml0cz0ib2JqZWN0Qm91bmRpbmdCb3giIHg9IjAiIHk9IjAiIHdpZHRoPSIxNiIgaGVpZ2h0PSIyNSIgZmlsbD0id2hpdGUiPiAgICAgICAgICAgIDx1c2UgeGxpbms6aHJlZj0iI3BhdGgtMyI+PC91c2U+ICAgICAgICA8L21hc2s+ICAgIDwvZGVmcz4gICAgPGcgaWQ9ItCT0LvQsNCy0L3QsNGPIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj4gICAgICAgIDxnIGlkPSJtb2JpbGUiPiAgICAgICAgICAgIDx1c2UgaWQ9IlJlY3RhbmdsZS0yMCIgc3Ryb2tlPSIjMDU3NjU1IiBtYXNrPSJ1cmwoI21hc2stMikiIHN0cm9rZS13aWR0aD0iMiIgeGxpbms6aHJlZj0iI3BhdGgtMSI+PC91c2U+ICAgICAgICAgICAgPHVzZSBpZD0iUmVjdGFuZ2xlLTIwLUNvcHkiIHN0cm9rZT0iIzA1NzY1NSIgbWFzaz0idXJsKCNtYXNrLTQpIiBzdHJva2Utd2lkdGg9IjIiIHhsaW5rOmhyZWY9IiNwYXRoLTMiPjwvdXNlPiAgICAgICAgICAgIDxjaXJjbGUgaWQ9Ik92YWwtMjQiIGZpbGw9IiMwNTc2NTUiIGN4PSIxMiIgY3k9IjMyIiByPSIxIj48L2NpcmNsZT4gICAgICAgIDwvZz4gICAgPC9nPjwvc3ZnPg==)}.it_header li a.active .it_icon04,.itc_icon .it_icon04{background-image:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PHN2ZyB3aWR0aD0iMjRweCIgaGVpZ2h0PSIzNnB4IiB2aWV3Qm94PSIwIDAgMjQgMzYiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+ICAgIDx0aXRsZT5tb2JpbGU8L3RpdGxlPiAgICA8ZGVzYz5DcmVhdGVkIHdpdGggU2tldGNoLjwvZGVzYz4gICAgPGRlZnM+ICAgICAgICA8cmVjdCBpZD0icGF0aC0xIiB4PSIwIiB5PSIwIiB3aWR0aD0iMjQiIGhlaWdodD0iMzYiIHJ4PSI0Ij48L3JlY3Q+ICAgICAgICA8bWFzayBpZD0ibWFzay0yIiBtYXNrQ29udGVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgbWFza1VuaXRzPSJvYmplY3RCb3VuZGluZ0JveCIgeD0iMCIgeT0iMCIgd2lkdGg9IjI0IiBoZWlnaHQ9IjM2IiBmaWxsPSJ3aGl0ZSI+ICAgICAgICAgICAgPHVzZSB4bGluazpocmVmPSIjcGF0aC0xIj48L3VzZT4gICAgICAgIDwvbWFzaz4gICAgICAgIDxyZWN0IGlkPSJwYXRoLTMiIHg9IjQiIHk9IjQiIHdpZHRoPSIxNiIgaGVpZ2h0PSIyNSI+PC9yZWN0PiAgICAgICAgPG1hc2sgaWQ9Im1hc2stNCIgbWFza0NvbnRlbnRVbml0cz0idXNlclNwYWNlT25Vc2UiIG1hc2tVbml0cz0ib2JqZWN0Qm91bmRpbmdCb3giIHg9IjAiIHk9IjAiIHdpZHRoPSIxNiIgaGVpZ2h0PSIyNSIgZmlsbD0id2hpdGUiPiAgICAgICAgICAgIDx1c2UgeGxpbms6aHJlZj0iI3BhdGgtMyI+PC91c2U+ICAgICAgICA8L21hc2s+ICAgIDwvZGVmcz4gICAgPGcgaWQ9ItCT0LvQsNCy0L3QsNGPIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj4gICAgICAgIDxnIGlkPSJtb2JpbGUiPiAgICAgICAgICAgIDx1c2UgaWQ9IlJlY3RhbmdsZS0yMCIgc3Ryb2tlPSIjMDQwNDA0IiBtYXNrPSJ1cmwoI21hc2stMikiIHN0cm9rZS13aWR0aD0iMiIgeGxpbms6aHJlZj0iI3BhdGgtMSI+PC91c2U+ICAgICAgICAgICAgPHVzZSBpZD0iUmVjdGFuZ2xlLTIwLUNvcHkiIHN0cm9rZT0iIzA0MDQwNCIgbWFzaz0idXJsKCNtYXNrLTQpIiBzdHJva2Utd2lkdGg9IjIiIHhsaW5rOmhyZWY9IiNwYXRoLTMiPjwvdXNlPiAgICAgICAgICAgIDxjaXJjbGUgaWQ9Ik92YWwtMjQiIGZpbGw9IiMwNDA0MDQiIGN4PSIxMiIgY3k9IjMyIiByPSIxIj48L2NpcmNsZT4gICAgICAgIDwvZz4gICAgPC9nPjwvc3ZnPg==)}.it_header{margin-bottom:40px}.it_content{overflow:hidden}.it_content .mobile_onn{overflow:hidden}.it_left{width:50%;margin-right:50%;padding:20px 20px 0 30px;box-sizing:border-box}.it_right{float:right;width:50%;position:relative}.itc_icon{display:none;text-align:center;margin-bottom:40px}.more_info i{display:inline-block;width:7px;height:13px;background-size:100% 100%;margin-left:5px}.faq_section i,.more_info i{background-image:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PHN2ZyB3aWR0aD0iMTFweCIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMTEgMjAiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+ICAgIDx0aXRsZT5nYWxvY2hrYTwvdGl0bGU+ICAgIDxkZXNjPkNyZWF0ZWQgd2l0aCBTa2V0Y2guPC9kZXNjPiAgICA8ZGVmcz48L2RlZnM+ICAgIDxnIGlkPSLQk9C70LDQstC90LDRjyIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+ICAgICAgICA8cG9seWxpbmUgaWQ9ImdhbG9jaGthIiBzdHJva2U9IiMyQ0JDOTIiIHBvaW50cz0iMSAwIDEwIDEwIDEgMjAiPjwvcG9seWxpbmU+ICAgIDwvZz48L3N2Zz4=)}.faq_section li:hover i,.more_info:hover i{background-image:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PHN2ZyB3aWR0aD0iMTFweCIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMTEgMjAiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+ICAgIDx0aXRsZT5nYWxvY2hrYTwvdGl0bGU+ICAgIDxkZXNjPkNyZWF0ZWQgd2l0aCBTa2V0Y2guPC9kZXNjPiAgICA8ZGVmcz48L2RlZnM+ICAgIDxnIGlkPSLQk9C70LDQstC90LDRjyIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+ICAgICAgICA8cG9seWxpbmUgaWQ9ImdhbG9jaGthIiBzdHJva2U9IiMwNTc2NTUiIHBvaW50cz0iMSAwIDEwIDEwIDEgMjAiPjwvcG9seWxpbmU+ICAgIDwvZz48L3N2Zz4=)}.faq_section i{display:block;width:9px;height:20px;background-size:100% 100%;position:absolute;right:35px;top:28px;transition:all .1s ease}.faq_section .active i{-webkit-transform:rotate(90deg);-moz-transform:rotate(90deg);-ms-transform:rotate(90deg);transform:rotate(90deg)}.remodal .u_tabs li{display:block;margin-bottom:10px}.lf_input{margin-bottom:20px}.lf_input input[type="text"],.lf_input input[type="email"],.lf_input input[type="password"],.lf_input select,.lf_input textarea{display:inline-block;width:100%;box-sizing:border-box;padding:12px 20px;font-family:'Roboto', sans-serif;font-weight:300;color:#000;background:#fff;border:1px solid #959595;border-radius:2px;font-size:18px;outline:none}.lf_input select{padding:12px 16px}.lf_input select option{font-weight:300}.lf_input textarea{resize:vertical}.lf_input input[type="text"]:focus,.lf_input input[type="email"]:focus,.lf_input input[type="password"]:focus,.lf_input select:focus,.lf_input textarea:focus{border-color:#2cbc92}.lf_input input[type="text"]:disabled,.lf_input input[type="email"]:disabled,.lf_input input[type="password"]:disabled,.lf_input textarea:disabled,.lf_input select:disabled{opacity:0.5}.er_text{display:none !important}.lf_adr{overflow:hidden}.lf_adr b{float:right;width:100px;text-align:right;line-height:47px;font-weight:400;overflow:hidden;text-overflow:ellipsis}.lf_adr>.lf_input{margin-right:110px}.login_form h2{text-align:center;margin-bottom:20px}#loginform-verifycode{overflow:hidden}#loginform-verifycode img{display:block;float:left}.lf_sub{margin-top:20px;text-align:center}#login_tab2 h2{color:#c7c7c7}#login_tab2 h2 a.active{color:#000}.reg_step_1 .lf_input .domain_ok,.reg_step_1 .lf_input.error .domain_info,.reg_step_1 .lf_input.has-success .domain_info{display:none}.lf_tooltip{margin-right:-110px;background:#e0f6f0;padding:20px;font-size:16px;position:relative;margin-top:14px;min-height:57px}.lf_tooltip::before{content:'';display:block;width:0;height:0;border:14px solid transparent;border-left-width:12px;border-right-width:12px;border-bottom-color:#e0f6f0;top:-28px;left:20px;position:absolute}.signup h2{text-align:center}.signup h3{font-size:18px;text-align:center;margin-bottom:20px}.reg_control{text-align:center}.reg_control a{display:inline-block;margin:0 10px}.it_content i{display:block}.it_content .icon01{width:799px;height:678px;background:url("/images/icons/index_screen01.jpg");background-size:100% 100%}.it_content .icon02{width:778px;height:538px;background:url("/images/icons/index_screen02.jpg");background-size:100% 100%}.it_content .icon03{width:465px;height:446px;background:url("/images/icons/index_screen03.svg");background-size:100% 100%}.it_content .icon04{width:490px;height:585px;background:url("/images/icons/index_screen04.jpg");background-size:100% 100%}.it_left ul{display:block;width:100%;min-width:440px;max-width:550px;float:right}.mobile_on{display:none}.user_menu.active{-webkit-box-shadow:0px 0px 7px 0px rgba(0,0,0,0.2);-moz-box-shadow:0px 0px 7px 0px rgba(0,0,0,0.2);box-shadow:0px 0px 7px 0px rgba(0,0,0,0.2);border-radius:3px;overflow:hidden}#download-plan-form h2,#download-plan-form p{text-align:center;margin-bottom:20px}.lf_subm{text-align:center}.faq_section p{margin-bottom:50px}.rem_part h5{font-size:18px;margin-bottom:15px}.rem_part h2{margin-bottom:20px;text-align:center}.rem_part hr{margin:35px auto}.zooming_image_wrap{position:fixed;width:100%;height:100%;opacity:0;z-index:101;transition:all .3s ease;top:0;left:0;cursor:pointer;background-color:rgba(0,0,0,0.8)}.zooming_image_wrap::before,.zooming_image_wrap::after{content:'';display:block;width:29px;height:3px;background:#838383;position:absolute;right:20px;top:30px}.zooming_image_wrap::before{-webkit-transform:rotate(45deg);-moz-transform:rotate(45deg);transform:rotate(45deg)}.zooming_image_wrap::after{-webkit-transform:rotate(-45deg);-moz-transform:rotate(-45deg);transform:rotate(-45deg)}.zooming_image_wrap:hover::before,.zooming_image_wrap:hover::after{background:#dbdbdb}.zooming_image_wrap div{width:80%;height:80%;left:10%;top:10%;position:absolute;background-position:50% 50%;background-repeat:no-repeat}.zooming_image_wrap.active{opacity:1}[data-tarif="start"]{display:none !important;visibility:hidden !important}.oc_sec_success.active{opacity:1}ol.styled,.inline ol{list-style:none;counter-reset:point}ol.styled li:before,.inline ol li:before{content:counter(point) ".";counter-increment:point 1;width:auto;height:auto;background:none;top:0;color:#9b9b9b}.main_footer{background:#f3f3f3;padding:110px 0 100px 0;font-size:16px;font-weight:400;margin-top:50px}.main_footer *{font-weight:400}.main_footer .content{overflow:hidden}.f_col{float:left;width:26%;box-sizing:border-box;padding-right:15px}.f_col_s{margin-left:80%;text-align:right}.f_col li{display:block;margin-bottom:20px}.main_footer hr{display:block;border:0;height:1px;width:100%;background:#ddd;margin:60px 0 45px 0;padding:0}.main_footer .left_col,.main_footer .right_col{float:left;width:50%}.main_footer .right_col{text-align:right}.main_footer .left_col span{display:inline-block;color:#8c8c8c;margin-right:30px}.main_footer .right_col span{display:inline-block;margin-right:40px}.f_col_s ul{white-space:nowrap}.f_col_s li{display:inline-block;margin-left:12px;width:39px;height:39px}.f_col_s li a{display:block;width:37px;height:37px;border:1px solid #626262;border-radius:50%}.f_col_s li a:hover{background:#626262}.f_col_s li a i{display:block;height:37px;background-image:url("/images/social.png")}.f_col_s .fb{background-position:0 0}.f_col_s a:hover .fb{background-position:-37px 0}.f_col_s .vk{background-position:0 -37px}.f_col_s a:hover .vk{background-position:-37px -37px}.f_col_s .li{background-position:0 -74px}.f_col_s a:hover .li{background-position:-37px -74px}.remodal,[data-remodal-id]{display:none}.remodal{position:relative;outline:none;text-size-adjust:100%}.remodal{box-sizing:border-box;width:100%;margin-bottom:10px;padding:30px 40px;transform:translate3d(0, 0, 0);text-align:left;color:#2b2e38;background:#fff}.remodal .button{font-size:20px;padding:16px 30px 14px 30px}.remodal,.remodal-wrapper:after{vertical-align:middle}.remodal-close{position:absolute;top:0;right:0;display:block;overflow:visible;width:60px;height:60px;margin:0;cursor:pointer;transition:color 0.2s;text-decoration:none;color:#95979c;border:0;outline:0;background:transparent}.remodal-close::before,.remodal-close::after{content:'';display:block;width:32px;height:2px;background:#2cbc92;position:absolute;left:14px;top:27px}.remodal-close::before{-moz-transform:rotate(45deg);-webkit-transform:rotate(45deg);-ms-transform:rotate(45deg);transform:rotate(45deg)}.remodal-close::after{-webkit-transform:rotate(135deg);-moz-transform:rotate(135deg);-ms-transform:rotate(135deg);transform:rotate(135deg)}.remodal-close:hover::before,.remodal-close:hover::after{background:#198263}.remodal-confirm::-moz-focus-inner,.remodal-cancel::-moz-focus-inner,.remodal-close::-moz-focus-inner{padding:0;border:0}.av_tab.active{display:block}a.av_tab_link.active{opacity:0.8}.cw_tab.active{display:block}.gootax-demo-form{background:#e1f8f1;padding:40px 30px 55px 30px;text-align:center;width:90%;box-sizing:border-box;margin-left:auto;margin-right:auto}.gootax-demo-form h2{margin-bottom:15px}.gootax-demo-form p{font-size:20px;line-height:1.8;margin-bottom:40px}.gootax-demo-form .cols{display:flex;justify-content:space-between}.gootax-demo-form .cols .col{width:25%}.gootax-demo-form .cols .col:last-child{width:18%}.gootax-demo-form .cols .col .button{display:block;height:48px;padding:0;line-height:48px;font-size:20px;width:100%}.our-clients h2{margin-bottom:55px;text-align:center}.our-clients .index-carousel{max-width:770px;margin-left:auto;margin-right:auto;position:relative;margin-bottom:100px}.our-clients__item{overflow:hidden}.our-clients__item i{display:block;width:140px;height:140px;border-radius:50%;overflow:hidden;background-size:cover;background-position:50% 50%;float:left}.our-clients__item div{margin-left:200px}.our-clients__item p{line-height:1.7;margin-bottom:8px;font-size:15px}.our-clients__item h3{margin-bottom:12px;font-size:24px}.how-to-start-taxi h2{text-align:center;margin-bottom:25px}.how-to-start-taxi .description{font-size:20px;margin-bottom:50px;text-align:center}.how-to-start-taxi ul{display:flex;justify-content:space-between;padding:20px 0;background:#e1f8f1;width:90%;margin-left:auto;margin-right:auto}.how-to-start-taxi ul li{display:block;width:25%;box-sizing:border-box;border-left:1px solid #bdd2cc;padding:10px 20px}.how-to-start-taxi ul li:first-child{border-left:0;border-top:0}.how-to-start-taxi ul li h3{font-size:22px;height:75px}.how-to-start-taxi ul li p{font-size:16px;line-height:1.6}.how-to-start-taxi ul li .reg{padding-top:25px;text-align:center}.owl-carousel{display:none;width:100%;-webkit-tap-highlight-color:transparent;position:relative;z-index:1}.owl-carousel .owl-nav.disabled,.owl-carousel .owl-dots.disabled{display:none}@media only screen and (min-width: 720px){.remodal{max-width:620px}}@media screen and (max-width: 720px){.main_header_wrapper{padding:0}h1{font-size:30px}h2{font-size:30px}h3{font-size:24px}h4{font-size:20px}.main_wrapper{width:100%;overflow-x:hidden}.wrapper{-moz-transition:right 0.3s ease;-webkit-transition:right 0.3s ease;transition:right 0.3s ease;right:0}.mobile_menu{display:block}a.mobile_menu_trigger{display:block}body{min-width:320px}.content{min-width:280px;padding:0 20px}.main_header{padding-top:17px;height:43px}.login_block{display:none}.main_header menu{display:none}a.logotype{width:106px;height:20px}.page_header{padding:50px 0}.index_header{padding:30px 0 85px 0}.page_header h1{margin-bottom:25px}.header_description{font-size:18px;line-height:30px}.styled li{margin-bottom:12px}.try_gootax,.price_header_h .try_gootax{padding-top:10px}.try_gootax .button{font-size:20px;padding:15px 18px}.page_section{padding:45px 0}.index_app_section .left_col{float:none;width:100%}.index_app_section .right_col{float:none;width:100%;padding:340px 0 0 0;margin:0}.index_app_section h2{text-align:center}.index_app_section .bottom_col{padding-top:40px;margin:0 -20px}a.as,a.gp{margin:0 6px}.pluses_section li{display:block;width:100%;padding:0;margin-bottom:35px;text-align:center}.faq_section h2{margin-bottom:40px}.faq_section i{right:15px;top:17px}.index_news_section li{display:none;margin:0}.index_news_section li:first-child{display:inline-block}.main_footer{padding:30px 0}.f_col{float:none;width:100%;margin-bottom:40px}.f_col_s{margin:0;text-align:left}.main_footer .content{overflow:visible}.main_footer hr{margin:30px -20px;width:auto}.main_footer .left_col,.main_footer .right_col{float:none;width:100%;text-align:left}.main_footer .left_col{margin-bottom:30px}.main_footer .left_col span,.main_footer .right_col span{display:block;margin:0 0 15px 0}.index_app_section .col_icon img{width:250px;height:auto;margin:0 auto}.index_app_section .col_icon{width:100%;bottom:auto;top:-90px}.itc_icon{display:block}.it_header{display:none}.it_left{width:100%;padding-right:20px}.it_right{float:none;width:100%}.mobile_onn{display:block !important;margin-bottom:40px}.it_content i{margin-left:auto;margin-right:auto}.it_left ul{float:none;min-width:0}.it_content .icon01{width:400px;height:320px}.it_content .icon02{width:389px;height:269px}.it_content .icon03{width:232px;height:223px}.it_content .icon04{width:240px;height:287px}.f_col_s li{margin:0 12px 0 0}.mobile_on{display:table-cell}.main_footer{margin-top:20px}}@media screen and (max-width: 720px){.gootax-demo-form{width:auto;padding:20px 20px 1px 20px;margin-left:-20px;margin-right:-20px}}@media screen and (max-width: 720px){.gootax-demo-form p{font-size:18px;margin-bottom:20px}}@media screen and (max-width: 720px){.gootax-demo-form .cols{display:block}}@media screen and (max-width: 720px){.gootax-demo-form .cols .col{width:100%;margin-bottom:20px}}@media screen and (max-width: 720px){.gootax-demo-form .cols .col:last-child{width:100%}}@media screen and (max-width: 720px){.gootax-demo-form .cols .col .button{max-width:200px;margin-left:auto;margin-right:auto}}@media screen and (max-width: 720px){.our-clients h2{margin-bottom:30px}}@media screen and (max-width: 720px){.our-clients .index-carousel{padding-left:25px;padding-right:25px;max-width:100%;box-sizing:border-box;margin-bottom:40px}}@media screen and (max-width: 720px){.our-clients__item i{float:none;margin:0 auto 20px auto}}@media screen and (max-width: 720px){.our-clients__item div{margin-left:0}}@media screen and (max-width: 720px){.how-to-start-taxi .description{margin-bottom:30px}}@media screen and (max-width: 720px){.how-to-start-taxi ul{display:block;width:auto;margin-left:-20px;margin-right:-20px;padding:0 20px}}@media screen and (max-width: 720px){.how-to-start-taxi ul li{width:100%;border-left:0;padding:20px;border-top:1px solid #bdd2cc}}@media screen and (max-width: 720px){.how-to-start-taxi ul li h3{height:auto;margin-bottom:10px}}
        </style>

        <link rel="preload" href="/css/main.css" as="style" onload="this.rel='stylesheet'">
        <noscript><link rel="stylesheet" href="/css/main.css"></noscript>
        <script>
            !function(e){"use strict";var t=function(t,n,r){function o(e){if(i.body)return e();setTimeout(function(){o(e)})}function a(){d.addEventListener&&d.removeEventListener("load",a),d.media=r||"all"}var l,i=e.document,d=i.createElement("link");if(n)l=n;else{var s=(i.body||i.getElementsByTagName("head")[0]).childNodes;l=s[s.length-1]}var u=i.styleSheets;d.rel="stylesheet",d.href=t,d.media="only x",o(function(){l.parentNode.insertBefore(d,n?l:l.nextSibling)});var f=function(e){for(var t=d.href,n=u.length;n--;)if(u[n].href===t)return e();setTimeout(function(){f(e)})};return d.addEventListener&&d.addEventListener("load",a),d.onloadcssdefined=f,f(a),d};"undefined"!=typeof exports?exports.loadCSS=t:e.loadCSS=t}("undefined"!=typeof global?global:this),function(e){if(e.loadCSS){var t=loadCSS.relpreload={};if(t.support=function(){try{return e.document.createElement("link").relList.supports("preload")}catch(e){return!1}},t.poly=function(){for(var t=e.document.getElementsByTagName("link"),n=0;n<t.length;n++){var r=t[n];"preload"===r.rel&&"style"===r.getAttribute("as")&&(e.loadCSS(r.href,r,r.getAttribute("media")),r.rel=null)}},!t.support()){t.poly();var n=e.setInterval(t.poly,300);e.addEventListener&&e.addEventListener("load",function(){t.poly(),e.clearInterval(n)}),e.attachEvent&&e.attachEvent("onload",function(){e.clearInterval(n)})}}}(this);
        </script>

        <link rel="apple-touch-icon" href="/images/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="/images/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="/images/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="/images/icon180.png" sizes="180x180">
        <link rel="shortcut icon" href="/images/favicon.ico">

        <?php $this->head() ?>

        <?php if (YII_ENV_PROD): ?>

        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-M7DSGCX');</script>
        <!-- End Google Tag Manager -->

        <?php endif;?>

    </head>

<?php $this->beginBody() ?>
    <body class="skin-blue <?php if (YII_ENV_PROD) echo 'metric-enabled';?>">

    <?php if (YII_ENV_PROD): ?>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M7DSGCX"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <?php endif;?>

    <section class="main_wrapper <? if ($page == 'docpack') echo 'docpack_page'; ?> <? if ($page == 'community') echo 'community_page'; ?> <? if ($page2 == 'community/blog/view') echo 'community_page_detail'; ?>">
        <div class="mobile_menu"></div>
        <section class="wrapper">
            <?= \landing\widgets\Alert::widget(); ?>
            <? if ($page != 'corporate' && $page != 'sale-soft' && $page != 'business-guide'): ?>
            <div class="close_mobile_menu"></div>
            <div class="main_header_wrapper">
            <header class="main_header">
                <div class="content">
                    <div class="login_block">
                        <?if(Yii::$app->user->isGuest):?>
                            <a data-remodal-target="login" class="button login_button">Вход</a>
                            <a data-ga-event-type="TopButtonClick" data-remodal-target="register" class="button">Регистрация</a>
                        <?else:?>
                            <?=$this->render('_profileMenu');?>
                        <?endif;?>
                    </div>
                    <a class="mobile_menu_trigger"><i class="mmt_1"></i><i class="mmt_2"></i><i class="mmt_3"></i></a>
                    <a href="/" class="logotype"></a>
                    <menu>
                        <?php
                        echo Menu::widget([
                            'options' => [],
                            'items'   => [
                                // Important: you need to specify url as 'controller/action',
                                // not just as 'controller' even if default action is used.
                                ['label' => $links['capabilities']['label'], 'url' => $links['capabilities']['url']],
                                ['label' => $links['prices']['label'], 'url' => $links['prices']['url']],
                                //['label' => $links['business-plan']['label'], 'url' => $links['business-plan']['url']],
                                ['label' => $links['support']['label'], 'url' => $links['support']['url']],
                                ['label' => $links['blog']['label'], 'url' => $links['blog']['url']],
                            ],
                        ]);
                        ?>
                    </menu>
                </div>
            </header></div>
            <?endif;?>

            <?=$content;?>

            <? if(Yii::$app->user->isGuest){
                echo $this->render('login/index');
            };?>

            <? if ($page == 'docpack' || $page == 'corporate' || $page == 'sale-soft' || $page == 'business-guide'): ?>
            <footer class="docpack_footer">
                <div class="content">
                    <span>&COPY; <?=  date('Y')?>, <?= app()->name?></span>
                    <div>
                        <div class="f_col_s">
                            <ul>
                                <li><a rel="nofollow noopener" target="_blank"
                                       href="https://www.facebook.com/gootax.pro"><i class="fb"></i></a></li>
                                <li><a rel="nofollow noopener" target="_blank" href="https://vk.com/gootax"><i
                                                class="vk"></i></a></li>
                                <li><a rel="nofollow noopener" target="_blank"
                                       href="https://www.linkedin.com/company/gootax"><i class="li"></i></a></li>
                            </ul>
                        </div>
                        <span style="padding-right: 10px;">с 8:00 до 20:00<sup>мск</sup></span>
                        <span><a class="tel_link" href="tel:88002501932">8-800-250-19-32</a></span>
                        <span><a href="mailto:sales@gootax.pro">sales@gootax.pro</a></span>
                    </div>
                </div>
            </footer>
            <? endif; ?>
            <?/*
            <hr class="small_hr">
            <section class="page_section index_news_section">
                <div class="content">
                    <h2>Новости</h2>
                    <ul>
                        <li>
                            <a href="#">
                                <span class="short_story_image"></span>
                                <b class="short_story_title">Облачные сервисы надёжны?</b>
                                <p class="short_story_contnent">Гутакс подходит для любого бизнеса для оказания услуг. Самые популярные можно разделить на 3 вида.</p>
                                <span class="short_story_date">24.05.2016</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="short_story_image"></span>
                                <b class="short_story_title">Облачные сервисы надёжны?</b>
                                <p class="short_story_contnent">Гутакс подходит для любого бизнеса для оказания услуг. Самые популярные можно разделить на 3 вида.</p>
                                <span class="short_story_date">24.05.2016</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="short_story_image"></span>
                                <b class="short_story_title">Облачные сервисы надёжны?</b>
                                <p class="short_story_contnent">Гутакс подходит для любого бизнеса для оказания услуг. Самые популярные можно разделить на 3 вида.</p>
                                <span class="short_story_date">24.05.2016</span>
                            </a>
                        </li>
                    </ul>
                    <div class="more_news">
                        <a class="more_info" href="#">Больше новостей <i></i></a>
                    </div>
                </div>
            </section>*/?>

            <?/*
            <?= $this->render('@app/views/site/registerDistributer', [
                'model' => new \landing\models\RegisterDistributerForm,
            ]); ?>

            <?= $this->render('@app/views/site/download-plan', [
                'model' => new \landing\models\BusinessPlan,
            ]); ?>*/?>



            <? if ($page != 'docpack' && $page != 'corporate' && $page != 'sale-soft' && $page != 'business-guide'): ?>
            <footer class="main_footer">
                <div class="content">
                    <div class="f_col">
                        <ul>
                            <li><?= Html::a(Html::encode($links['capabilities']['label']),
                                    $links['capabilities']['url']); ?></li>
                            <li><?= Html::a(Html::encode($links['prices']['label']),
                                    $links['prices']['url']); ?></li>
                            <li><?= Html::a(Html::encode($links['support']['label']),
                                    $links['support']['url']); ?></li>
                        </ul>
                    </div>

                    <div class="f_col">
                        <ul>
                            <li><?= Html::a(Html::encode($links['blog']['label']),
                                    $links['blog']['url']); ?></li>
                            <li><?= Html::a(Html::encode($links['job']['label']),
                                    $links['job']['url']); ?></li>
                            <li><?= Html::a(Html::encode($links['study']['label']),
                                    $links['study']['url']); ?></li>
                        </ul>
                    </div>
                    <div class="f_col">
                        <ul>
                            <li><?= Html::a(Html::encode($links['site']['label']),
                                    $links['site']['url']); ?></li>
                            <li><?= Html::a(Html::encode($links['mobile_app']['label']),
                                    $links['mobile_app']['url']); ?></li>
                            <li><a rel="noopener" target="_blank" href="/files/gootax_presentation.pdf">Скачать презентацию</a></li>
                        </ul>
                    </div>
                    <div class="f_col_s">
                        <ul>
                            <li><a rel="nofollow noopener" target="_blank" href="https://www.facebook.com/gootax.pro"><i
                                            class="fb"></i></a></li>
                            <li><a rel="nofollow noopener" target="_blank" href="https://vk.com/gootax"><i
                                            class="vk"></i></a></li>
                            <li><a rel="nofollow noopener" target="_blank"
                                   href="https://www.linkedin.com/company/gootax"><i class="li"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="content">
                    <hr>
                </div>
                <div class="content">
                    <div class="left_col">
                        <span>&COPY; <?=  date('Y')?>, <?= app()->name?></span>
                        <a rel="nofollow noopener" target="_blank" href="/files/licence_gootax_pro.pdf">Лицензионный
                            договор</a>
                    </div>
                    <div class="right_col">
                        <span><a style="color: #8c8c8c;" href="tel:+<?=phoneFilter($phone)?>"><?=$phone?></a></span>
                        <a href="mailto:<?=$mail?>"><?=$mail?></a>
                    </div>
                </div>
            </footer>
            <?endif;?>
        </section>
    </section>
    <div class="zooming_image_wrap" style="display: none;"><div></div></div>

    <div style="display: none">
        <div class="remodal signup" data-remodal-id="register"
             data-remodal-options="hashTracking: false"
             data-yandex-metrika="<?= YII_ENV_PROD ? 1 : 0; ?>"
             data-google-analytics="<?= YII_ENV_PROD ? 1 : 0; ?>">
            <button data-remodal-action="close" class="remodal-close"></button>
            <?php


            ?>

            <?= $this->render('@app/views/tenant/signup', [
                'domain' => $domain,
                'model' => $this->params['signupForm'],
            ]); ?>
        </div>

        <div class="remodal signup" data-remodal-id="site">
            <button data-remodal-action="close" class="remodal-close"></button>
            <h2>Оставить заявку</h2>
            <h3></h3>
            <div class="lf_input">
                <input type="text" placeholder="Как к вам обращаться?">
            </div>
            <div class="lf_input">
                <input type="text" placeholder="Телефон">
            </div>
            <div class="lf_input">
                <input type="text" placeholder="Электронная почта">
            </div>


            <input type="hidden" name="ip" >

            <input type="hidden" name="utm_source" >

            <input type="hidden" name="utm_medium">

            <input type="hidden" name="utm_campaign" >

            <input type="hidden" name="utm_content">

            <input type="hidden" name="utm_term" >
            <input type="hidden" name="client_id_go">
            <input type="hidden" name="client_id_ya">


            <div class="lf_sub">
                <input type="submit" class="button" value="Далее">
            </div>
        </div>

        <div class="remodal timer-popup" data-remodal-id="popup-timer" data-yandex-metrika="<?= YII_ENV_PROD ? 1 : 0; ?>" data-google-analytics="<?= YII_ENV_PROD ? 1 : 0; ?>" data-remodal-options="hashTracking: false">
            <button data-remodal-action="close" class="remodal-close"></button>
            <div data-popup="1" data-yandex-opened="presentationOpened" data-yandex-success="getPresentation" class="js-popup-timer" style="display: none;">
                <section class="get-presentation">
                    <div class="get-presentation__form">
                        <div class="js-form">
                            <i></i>
                            <h2>Получите презентацию Гутакса</h2>
                            <p>Заполните адрес электронной почты и мы пришлём вам презентацию нашей системы в течение 5 минут</p>
                            <input type="hidden" name="action" value="popup/request-presentation">
                            <input type="hidden" name="name" value="-">
                            <input type="hidden" name="city" value="-">
                            <input type="hidden" name="contact" value="-">
                            <div class="col lf_input">
                                <input type="email" placeholder="E-mail" name="email">
                                <div class="error"></div>
                            </div>

                            <input type="hidden" name="ip" >

                            <input type="hidden" name="utm_source" >

                            <input type="hidden" name="utm_medium">

                            <input type="hidden" name="utm_campaign" >

                            <input type="hidden" name="utm_content">

                            <input type="hidden" name="utm_term" >
                            <input type="hidden" name="client_id_go">
                            <input type="hidden" name="client_id_ya">


                            <a class="button">Получить презентацию</a>
                        </div>
                        <div class="js-success">
                            <h2>Заявка отправлена</h2>
                            <p>В скором времени с вами свяжутся.</p>
                        </div>
                    </div>
                </section>
            </div>
            <div data-popup="2" data-yandex-opened="callbackOpened" data-yandex-success="waitCall" class="js-popup-timer" style="display: none;">
                <div class="callback">
                    <div class="js-form">
                        <h2>Хотите,<br>мы перезвоним вам?</h2>
                        <p>Наш менеджер позвонит вам и проконсультирует<br>по всем вопросам</p>
                        <input type="hidden" name="action" value="popup/callback">
                        <input type="hidden" name="name" value="-">
                        <input type="hidden" name="city" value="-">
                        <input type="hidden" name="email" value="">
                        <div class="col lf_input">
                            <input type="tel" placeholder="Телефон" name="contact" class="callback_phone">
                            <div class="error"></div>
                        </div>

                        <input type="hidden" name="ip" >

                        <input type="hidden" name="utm_source" >

                        <input type="hidden" name="utm_medium">

                        <input type="hidden" name="utm_campaign" >

                        <input type="hidden" name="utm_content">

                        <input type="hidden" name="utm_term" >
                        <input type="hidden" name="client_id_go">
                        <input type="hidden" name="client_id_ya">

                        <a class="button">Жду звонка!</a>
                    </div>
                    <div class="js-success">
                        <h2>Заявка отправлена</h2>
                        <p>В скором времени с вами свяжутся.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->render('@app/views/site/registerDistributer', [
        'model' => new \landing\models\RegisterDistributerForm,
    ]); ?>

    <?= $this->render('@app/views/site/download-plan', [
        'model' => new \landing\models\BusinessPlan,
    ]); ?>

    <?php $this->endBody() ?>

    <? if ($page == 'configurator'): ?>
        <script type="text/javascript" src="/js/configurator_libs.js"></script>
        <script type="text/javascript" src="/js/configurator.js"></script>
    <? endif; ?>

    <?php if ($page !== 'sale-soft'): ?>
    <script data-skip-moving="true">
        (function(w,d,u,b){
            s=d.createElement('script');r=(Date.now()/1000|0);s.async=1;s.src=u+'?'+r;
            h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://cdn.bitrix24.ru/b1710865/crm/site_button/loader_6_3wwz5d.js');
    </script>
    <?php endif; ?>

    <?php if (YII_ENV_PROD): ?>
        <?/*<!-- Yandex.Metrika counter --><script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter30670468 = new Ya.Metrika({ id:30670468, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="https://mc.yandex.ru/watch/30670468" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->

        <!-- Google analytics -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-102285457-1', 'auto');
            ga('send', 'pageview');
        </script>*/?>
        <!-- Retargeting VK -->
        <? if ($page == 'index'): ?>
            <script async type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=P5qjxmjSjciWb*cU5Ftrx4fxuyduEkiOXVPLDdbn/LRbkDlCTaOcto4N3Vr0Ught5CC1v/4EXqygODc2o5roueifjAsSLGcgNE9A8OvSRhxpxxh*Qx2kwmi60nB/iK/TTxr8fHqZbBX7VpbaUT4mnpbmoxpsUan37G8cX2DXwnA-';</script>
        <? endif; ?>
        <?/*<script type="text/javascript">

            var google_conversion_id = 926692164;
            var google_custom_params = window.google_tag_params;
            var google_remarketing_only = true;

        </script>
        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
        </script>
        <noscript>
            <div style="display:inline;">
                <img height="1" width="1" style="border-style:none;" alt=""
                     src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/926692164/?guid=ON&amp;script=0"/>
            </div>
        </noscript>*/?>
    <?php endif; ?>
    </body>
</html><?php $this->endPage() ?>
