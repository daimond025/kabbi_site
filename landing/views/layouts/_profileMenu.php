<?php

use yii\helpers\Html;

$author = user()->fullName;

?>

<div class="user_menu">
    <div class="um_avatar" title="<?= $author; ?>">
        <?php
            $userProfile = user()->userProfile;

            if ($userProfile) {
                $filename = $userProfile->getThumbFilename($userProfile->photo);
            }

            if (is_file($filename) && file_exists($filename)) {
                echo Html::img($userProfile->getThumbFilelink($userProfile->photo), ['alt' => 'photo']);
            } else {
                echo Html::img('/images/no_photo.png', ['alt' => $author]);
            }
        ?> <?= $author; ?>
    </div>

    <div class="um_content">
        <ul>
            <?/*<li><?= Html::a('Перейти в профиль', [
                    '/community/profile/update',
                ]); ?></li>*/?>
            <?= user()->user_id ? Html::tag('li', Html::a('Перейти в Гутакс', ['/site/goto-gootax'])) : ''; ?>
            <li><?=
                Html::a('Выйти', ['/site/community-logout'], [
                    'data-method' => 'post'
                ]);
                ?></li>
        </ul>
    </div>
</div>