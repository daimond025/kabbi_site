<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<h2>Попробовать</h2>
<h3>&nbsp;</h3>
<div class="register">
    <? $form = ActiveForm::begin([
        'id'                     => 'form-signup',
        'validateOnSubmit'       => false,
        'enableClientValidation' => false,
        'enableAjaxValidation'   => true,
        'errorCssClass'          => 'error',
        'validationUrl'          => \yii\helpers\Url::to('/tenant/signup-validate'),
        'action'                 => ['/tenant/signup'],
        'options'                => [
            'onsubmit' => YII_ENV_PROD ? "yaCounter30670468.reachGoal('submitRegister'); ga('send', 'event', 'registrHeader','submit');" : false,
        ],
    ]);
    $activeFieldOptions = ['options' => ['class' => 'lf_input']];
    ?>
    <div id="step_1" class="reg_step_1">
        <div class="lf_adr">
            <b>.<?=$domain?></b>
            <?= $form->field($model, 'domain', $activeFieldOptions)->begin(); ?>

            <?= Html::activeTextInput($model, 'domain', ['class' => 'red_site', 'autocomplete' => 'off', 'placeholder' => 'Адрес']); ?>


            <div class="lf_tooltip">
                <?= Html::error($model, 'domain', ['tag' => 'p', 'class' => 'help-block help-block-error']); ?>
                <p class="domain_info">Придумайте запоминающийся адрес.</p>
                <p class="domain_ok">Название свободно, можно использовать!</p>
                Название должно быть написано латинскими буквами. Можно использовать цифры и тире. Количество знаков от 4-х до 15.
            </div>

            <?= $form->field($model, 'domain')->end(); ?>
        </div>

        <div class="cm_name">
                <?= $form->field($model, 'last_name', $activeFieldOptions)->begin(); ?>
                <?= Html::activeTextInput($model, 'last_name', ['placeholder' => 'Фамилия']); ?>
                <?= Html::error($model, 'last_name', ['tag' => 'span', 'class' => 'er_text help-block help-block-error']); ?>
                <?= $form->field($model, 'last_name')->end(); ?>

                <?= $form->field($model, 'name', $activeFieldOptions)->begin(); ?>
                <?= Html::activeTextInput($model, 'name', ['placeholder' => 'Имя']); ?>
                <?= Html::error($model, 'name', ['tag' => 'span', 'class' => 'er_text help-block help-block-error']); ?>
                <?= $form->field($model, 'name')->end(); ?>

                <?= $form->field($model, 'second_name', $activeFieldOptions)->begin(); ?>
                <?= Html::activeTextInput($model, 'second_name', ['placeholder' => 'Отчество']); ?>
                <?= Html::error($model, 'second_name', ['tag' => 'span', 'class' => 'er_text help-block help-block-error']); ?>
                <?= $form->field($model, 'second_name')->end(); ?>
        </div>
        <div class="cm_phone">
                <?= $form->field($model, 'email', $activeFieldOptions)->begin(); ?>
                <?= Html::activeTextInput($model, 'email', ['placeholder' => 'Эл. почта']); ?>
                <?= Html::error($model, 'email', ['tag' => 'span', 'class' => 'er_text help-block help-block-error']); ?>
                <?= $form->field($model, 'email')->end(); ?>
        </div>
        <div class="reg_phone">
                <?= $form->field($model, 'phone', $activeFieldOptions)->begin(); ?>
                <?= Html::activeTextInput($model, 'phone', ['placeholder' => 'Телефон', 'class' => 'mask_phone']); ?>
                <?= Html::error($model, 'phone', ['tag' => 'span', 'class' => 'er_text help-block help-block-error']); ?>
                <?= $form->field($model, 'phone')->end(); ?>
        </div>
        <div class="reg_lin">
            <label><?= Html::activeCheckbox($model, 'agree', ['label' => null]) ?> Принимаю <a
                    rel="nofollow noopener" target="_blank" href="/files/licence_gootax_pro.pdf">лицензионное
                соглашение</a><br>и <a rel="nofollow noopener" target="_blank" href="/files/confidential_gootax_pro.pdf">политику конфиденциальности</a></label>
        </div>
        <div class="lf_sub">
            <?= Html::submitInput('Далее', [
                'onclick'  => YII_ENV_PROD ? "yaCounter30670468.reachGoal('btnNext'); return true;" : false,
                'class'    => 'button',
                'disabled' => 'disabled',
            ]) ?>
        </div>
    </div>
    <div id="step_2" class="reg_step_2" style="display: none">
        <div class="lf_input">
            <input id="sms_code" name="sms_code" type="text" placeholder="Код в смс" value="" />
            <span class="er_text">Не заполнено</span>
        </div>
        <div class="reg_control">
            <a class="send_block" id="send_sms_again" href="">Отправить заново</a>
            <a id="go_to_step_1" href="">Изменить данные</a>
        </div>
        <div class="lf_sub">
            <?= Html::submitInput('Подтвердить', [
                'onclick' => YII_ENV_PROD ? "yaCounter30670468.reachGoal('btnApprove'); return true;" : false,
                'class'   => 'button',
            ]) ?>
        </div>
    </div>
    <?= $form->field($model, 'ip')->hiddenInput()->label(false); ?>

    <?= $form->field($model, 'utm_source')->hiddenInput()->label(false); ?>

    <?= $form->field($model, 'utm_medium')->hiddenInput()->label(false); ?>

    <?= $form->field($model, 'utm_campaign')->hiddenInput()->label(false); ?>

    <?= $form->field($model, 'utm_content')->hiddenInput()->label(false); ?>

    <?= $form->field($model, 'utm_term')->hiddenInput()->label(false); ?>

    <?= $form->field($model, 'client_id_go')->hiddenInput()->label(false); ?>

    <?= $form->field($model, 'client_id_ya')->hiddenInput()->label(false); ?>

    <?php ActiveForm::end(); ?>
</div>