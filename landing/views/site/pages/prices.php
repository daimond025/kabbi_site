<section class="top_page prices">
    <div class="content">
        <h1>Выгодные тарифы</h1>
        <h5>Мы экономим ваши деньги, т.к. вы платите только за тех водителей, которые вышли <br/>на смену.</h5>
        <div class="prices_sec">
            <div class="ps_col psl">
                <div class="pr_selector">
                    <h4>Кол-во месяцев</h4>
                    <a data-month="1" class="active"><span>1</span><i>мес.</i></a>
                    <a data-month="3"><span>3</span><i>мес.</i><b>-10%</b></a>
                    <a data-month="12"><span>12</span><i>мес.</i><b>-15%</b></a>
                </div>
                <ul class="ps_list">
                    <li data-prop="1">Кол-во водителей</li>
                    <li data-prop="3">Кол-во сотрудников</li>
                    <li data-prop="4">Водительское прил.<br/>Android и iOS</li>
                    <li data-prop="5">Моб. прил. для заказа<br/>такси Android и iOS</li>
                    <li data-prop="6">Обмен заказами (внутренний)</li>
                    <li data-prop="7">Обмен заказами (внешний)</li>
                    <li data-prop="8">Контакт-центр</li>
                    <li data-prop="9">Рабочее место<br/>операторов</li>
                    <li data-prop="10">СМС-провайдер</li>
                    <li data-prop="12">Время отклика<br/>тех.поддержки</li>
                </ul>
            </div>
            <div class="ps_col">
                <div class="ps_header">
                    <h3>Старт</h3>     
                    <h4 data-month="0">Бесплатно</h4>
                    <a data-remodal-target="register" class="button">Начать работу</a>
                </div>

                <div class="ps_content">
                    <ul>
                        <li data-prop="1">до 5</li>
                        <li data-prop="3">не более 2</li>
                        <li data-prop="4" class="twow"><i class="ps_yes"></i></li>
                        <li data-prop="5" class="twow">+5 000 ₽</li>
                        <li data-prop="6"><i class="ps_yes"></i></li>
                        <li data-prop="7"><i class="ps_yes"></i></li>
                        <li data-prop="8">SIP</li>
                        <li data-prop="9" class="twow"><i class="ps_yes"></i></li>
                        <li data-prop="10">СМС дисконт</li>
                        <li data-prop="12" class="twow">до 3-х дней</li>
                    </ul>
                </div>

                <div class="mobile_spoler">
                    <a>Подробнее</a>
                </div>
            </div>
            <div class="ps_col">
                <div class="ps_header">
                    <h3>Оптима</h3>     
                    <h4 data-month="1">10 000 ₽ /мес.</h4>
                    <h4 data-month="3" style="display: none;">9 000 ₽ /мес.</h4>
                    <h4 data-month="12" style="display: none;">8 500 ₽ /мес.</h4>
                    <a data-remodal-target="register" class="button">Попробовать</a>
                    <span>1 мес. бесплатно</span>
                </div>

                <div class="ps_content">
                    <ul>
                        <li data-prop="1">до 20</li>
                        <li data-prop="3">Неограниченно</li>
                        <li data-prop="4" class="twow"><i class="ps_yes"></i></li>
                        <li data-prop="5" class="twow">+5 000 ₽</li>
                        <li data-prop="6"><i class="ps_yes"></i></li>
                        <li data-prop="7"><i class="ps_yes"></i></li>
                        <li data-prop="8">SIP</li>
                        <li data-prop="9" class="twow"><i class="ps_yes"></i></li>
                        <li data-prop="10">СМС дисконт</li>
                        <li data-prop="12" class="twow">до 2-х дней</li>
                    </ul>
                </div>
                <div class="mobile_spoler">
                    <a>Подробнее</a>
                </div>
            </div>
            <div class="ps_col">
                <div class="ps_header">
                    <h3>Максима</h3>     
                    <h4 data-month="1">15 000 ₽ /мес.</h4>
                    <h4 data-month="3" style="display: none;">13 500 ₽ /мес.</h4>
                    <h4 data-month="12" style="display: none;">12 750 ₽ /мес.</h4>
                    <a data-remodal-target="register" class="button">Попробовать</a>
                    <span>1 мес. бесплатно</span>
                </div>
                <div class="ps_content">
                    <ul>
                        <li data-prop="1">до 100</li>
                        <li data-prop="3">Неограниченно</li>
                        <li data-prop="4" class="twow"><i class="ps_yes"></i></li>
                        <li data-prop="5" class="twow">+5 000 ₽</li>
                        <li data-prop="6"><i class="ps_yes"></i></li>
                        <li data-prop="7"><i class="ps_yes"></i></li>
                        <li data-prop="8">SIP</li>
                        <li data-prop="9" class="twow"><i class="ps_yes"></i></li>
                        <li data-prop="10">Любой СМС провайдер</li>
                        <li data-prop="12" class="twow">до 1-го дня</li>
                    </ul>
                </div>
                <div class="mobile_spoler">
                    <a>Подробнее</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="pluses prices_pluses">
    <div class="content">
        <h2>Преимущества наших тарифов</h2>
        <ul>
            <li>
                <i class="pl_i08"></i><h4>Можно попробовать бесплатно</h4><p>Вы не&nbsp;рискуете большими вложениями</p>
            </li>
            <li>
                <i class="pl_i07"></i><h4>Фиксированная плата за&nbsp;использование</h4><p>Вы можете точно прогнозировать ежемесячные вложения в&nbsp;программный комплекс</p>
            </li>
            <li>
                <i class="pl_i09"></i><h4>Бесплатно для&nbsp;небольшой компании</h4><p>Отличный вариант для&nbsp;начинающих предпринимателей. Можно работать бесплатно, если в&nbsp;штате не&nbsp;более 2-х сотрудников и&nbsp;5&nbsp;водителей</p>
            </li>

        </ul>     
    </div>
</section>