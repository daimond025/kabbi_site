<section class="top_page partners">
    <div class="content">
        <h1>Партнёры</h1>
        <p>Рекомендуем наших партнёров</p>
    </div>
</section>
<section class="partners_cont">
    <div class="content inline">
        <div class="pc_item">
            <div style="text-align: center">
                <i class="part_i02"></i>
            </div>
            <h3><a target="_blank" rel="nofollow noopener" href="http://www.taxi3c.ru">Студия «Три Цвета»</a></h3>
            <ul>
                <li>Разработка сайтов для такси с интеграцией Гутакса</li>
                <li>Изменение дизайна в клиентском приложении Гутакса</li>
            </ul>
        </div>
        <div class="pc_item">
            <div style="text-align: center">
                <i class="part_i01"></i>
            </div>
            <h3><a target="_blank" rel="nofollow noopener" href="http://www.elecsnet.ru/">«Элекснет»</a></h3>
            <ul>
                <li>Пополнение баланса водителей в автоматическом режиме</li>
            </ul>
        </div>

        <div class="pc_item">
            <div style="text-align: center">
                <i class="part_i03"></i>
            </div>
            <h3><a target="_blank" rel="nofollow noopener" href="http://www.iqsms.ru">«СМС дисконт»</a></h3>
            <ul>
                <li>Отправка СМС сообщений</li>
            </ul>
        </div>
    </div>
</section>