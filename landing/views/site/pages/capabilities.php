<?php

use common\modules\tenant\models\DefaultSettings;

?>

<section class="top_page capabilities">
    <div class="content">
        <h1>Возможности</h1>
        <h2>В Гутаксе есть всё для автоматизации работы таксопарка</h2>
        <div class="cap_frame">
            <i class="cf_01"></i>
            <i class="cf_02"></i>
            <i class="cf_03"></i>
            <a data-remodal-target="register" class="button">Попробовать бесплатно</a>
        </div>
    </div>
    <div class="scroll_cars"></div>
</section>
<section class="our_cap">
    <section class="content inline">
        <div class="oc_item oc_right">
            <i class="oc_i01"></i>
            <div>
                <h2>Автоматическая обработка заказов</h2>
                <ul>
                    <li>Заказ попадает в&nbsp;программу через мобильное приложение, сайт или&nbsp;звонок диспетчеру</li>
                    <li>Гутакс ищет ближайшего водителя и&nbsp;предлагает ему заказ</li>
                    <li>Если водитель отказывается, заказ предлагается следующему ближайшему водителю. Поиск
                        продолжается пока заказ не&nbsp;будет принят, либо не&nbsp;кончится допустимое время
                    </li>
                    <li>Если время принятия заказа вышло, он&nbsp;отправляется в&nbsp;биржу и&nbsp;ждёт покупки от&nbsp;партнёров</li>
                </ul>
            </div>
        </div>
        <div class="oc_item" id="driver-app">
            <i class="oc_i02 os_i_apps">
                <a class="g_play"
                   href="<?= DefaultSettings::getValue(DefaultSettings::SETTING_GOOGLE_PLAY_WORKER_APP_LINK) ?>"></a>
                <a class="a_store"
                   href="<?= DefaultSettings::getValue(DefaultSettings::SETTING_APP_STORE_WORKER_APP_LINK) ?>"></a>
            </i>
            <div>
                <h2>Водительское приложение</h2>
                <ul>
                    <li>Разработано для Android&nbsp;и&nbsp;iOS</li>
                    <li>Автоматическое распределение заказов. Водитель может просто следовать заказам, которые будет
                        предлагать Гутакс
                    </li>
                    <li>Таксометр, считающий стоимость поездки</li>
                    <li>Навигатор</li>
                    <li>История поездок</li>
                    <li>Сообщения от диспетчера (чат)</li>
                    <li>SOS</li>
                    <li>Режимы день/ночь</li>
                    <li>Обновления в соответствии с последними требованиями платформ</li>
                </ul>
            </div>
        </div>
        <div class="oc_item oc_right" id="client-app">
            <i class="oc_i03 os_i_apps2">
                <a class="g_play" href="#"></a>
                <a class="a_store" href="#"></a>
            </i>
            <div>
                <h2>Клиентское приложение для&nbsp;заказа такси</h2>
                <ul>
                    <li>Удобный вызов такси для клиентов</li>
                    <li>Выпускается индивидуально под вашу службу</li>
                    <li>Разработано для Android и iOS</li>
                    <li>Примерный расчёт стоимости и времени поездки</li>
                    <li>Инструмент для работы с клиентами</li>
                    <li>Обновление в соответствии последними требованиями платформ</li>
                </ul>
            </div>
        </div>
        <div class="oc_item">
            <i class="oc_i04"></i>
            <div>
                <h2>Несколько филиалов вашей службы</h2>
                <ul>
                    <li>1 аккаунт Гутакса для&nbsp;ваших филиалов в&nbsp;разных городах без&nbsp;дополнительной платы.
                        Единая точка контроля.
                    </li>
                    <li>Единые или&nbsp;разные настройки для&nbsp;филиалов</li>
                    <li>Общие сотрудники в&nbsp;разных филиалах</li>
                </ul>
            </div>
        </div>
        <div class="oc_item oc_right">
            <i class="oc_i05"></i>
            <div>
                <h2>Справочники</h2>
                <ul>
                    <li>Сотрудники</li>
                    <li>Автомобили</li>
                    <li>Группы водителей</li>
                    <li>Парковки (при их наличии)</li>
                    <li>Тарифы для водителей и клиентов</li>
                    <li>Улицы и общественные места</li>
                </ul>
            </div>
        </div>
        <div class="oc_item">
            <i class="oc_i06"></i>
            <div>
                <h2>Парковки вашего города</h2>
                <ul>
                    <li>Возможность распределения заказов по&nbsp;парковкам</li>
                    <li>Удобная работа с&nbsp;картой</li>
                    <li>Наценка на&nbsp;поездки в&nbsp;определенные районы города</li>
                </ul>
            </div>
        </div>
        <div class="oc_item oc_right">
            <i class="oc_i07"></i>
            <div>
                <h2>База улиц и&nbsp;общественных мест</h2>
                <ul>
                    <li>Добавление новых улиц (с&nbsp;модерацией)</li>
                    <li>Обновление общественных мест (с&nbsp;модерацией)</li>
                </ul>
            </div>
        </div>
        <div class="oc_item">
            <i class="oc_i08"></i>
            <div>
                <h2>Тарифы для клиентов</h2>
                <ul>
                    <li>Гибкая настройка тарифов: за расстояние или&nbsp;время, комбинированный или&nbsp;фиксированный
                        (по&nbsp;парковкам), город/загород, день/ночь.
                    </li>
                    <li>Отдельные тарифы в&nbsp;аэропорты и&nbsp;жд-вокзалы. Праздничные дни и&nbsp;исключения.</li>
                    <li>Дополнительные опции</li>
                </ul>
            </div>
        </div>
        <div class="oc_item oc_right">
            <i class="oc_i09"></i>
            <div>
                <h2>Тарифы для&nbsp;водителей</h2>
                <ul>
                    <li>Оплата за&nbsp;смену, за&nbsp;заказ или&nbsp;покупка абонемента</li>
                    <li>Гибкая настройка</li>
                </ul>
            </div>
        </div>
        <div class="oc_item" id="dispatcher-app">
            <i class="oc_i10"></i>
            <div>
                <h2>Контакт-центр, рабочее место диспетчера</h2>
                <ul>
                    <li>Быстрая интеграция с SIP-телефонией &mdash; облачной или&nbsp;стационарной</li>
                    <li>IVR&nbsp;&mdash; автоматическая обработка звонков</li>
                    <li>Запись звонков и хранение в&nbsp;течение 3-х&nbsp;месяцев</li>
                    <li>Рабочее место диспетчера на&nbsp;Windows</li>
                    <li>Софтфон — программа, через которую совершаются звонки</li>
                </ul>
            </div>
        </div>
        <div class="oc_item oc_right">
            <i class="oc_i11"></i>
            <div>
                <h2>Отчёты о&nbsp;работе службы такси</h2>
                <ul>
                    <li>Наглядная статистика о&nbsp;работе службы такси</li>
                    <li>Выявление проблем в&nbsp;работе</li>
                    <li>Прогнозирование заработка</li>
                </ul>
            </div>
        </div>
        <div class="oc_item">
            <i class="oc_i12"></i>
            <div>
                <h2>Пополнение баланса</h2>
                <ul>
                    <li>Для служб такси: банковский перевод, банковская карта</li>
                    <li>Для водителей: Элекснет, банковская карта</li>
                </ul>
            </div>
        </div>
        <div class="oc_item oc_right">
            <i class="oc_i13"></i>
            <div>
                <h2>Корпоративное обслуживание</h2>
                <ul>
                    <li>Обслуживание юридических лиц</li>
                    <li>Индивидуальные условия</li>
                    <li>Отчёты</li>
                </ul>
            </div>
        </div>
        <div class="oc_item">
            <i class="oc_i14"></i>
            <div>
                <h2>Сообщество</h2>
                <ul>
                    <li>Общение с&nbsp;коллегами и&nbsp;представителями Гутакса, тематический блог, обмен опытом и&nbsp;решение
                        вопросов
                    </li>
                    <li>Документация</li>
                    <li>Предложения по улучшению Гутакса с&nbsp;голосованием&nbsp;за очередность реализации</li>
                </ul>
            </div>
        </div>
    </section>
</section>