<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\views\site\assets\DownloadPlanAsset;

DownloadPlanAsset::register($this);

/* @var $model \landing\models\BusinessPlan */
/* @var $this \yii\web\View */

?>

<div style="display: none;">
    <div class="remodal dp_win" data-remodal-id="download_plan">
        <?php $form = \yii\widgets\ActiveForm::begin([
                'id'                     => 'download-plan-form',
                'action'                 => '/site/download-plan',
                'enableClientValidation' => false,
                'enableAjaxValidation'   => true,
                'validateOnSubmit'       => false,
                'validationUrl'          => \yii\helpers\Url::to('/business-plan/validate'),
                'errorCssClass'          => 'error',
                'fieldConfig'            => app()->params['fieldOptions'],
                'options'                => [
                    'data' => [
                        'google-analytics' => YII_ENV_PROD,
                        'yandex-metrika' => YII_ENV_PROD,
                    ],
                ],
        ]); ?>
        <button data-remodal-action="close" class="remodal-close"></button>
        <div id="download-plan-step1">
            <h2>Скачать бизнес-план</h2>
            <p>Заполните форму и мы отправим бизнес-план на вашу эл. почту.</p>
            <div class="lf_input">
                <?= $form->field($model, 'name')->textInput([
                    'maxLength'   => 255,
                    'placeholder' => $model->attributeLabels()['name'],
                ]); ?>
            </div>
            <div class="lf_input">
                <?= $form->field($model, 'email')->textInput([
                    'maxLength'   => 255,
                    'placeholder' => $model->attributeLabels()['email'],
                ]); ?>
            </div>
            <div class="lf_input">
                <?= $form->field($model, 'phone')->textInput([
                    'maxLength'   => 255,
                    'placeholder' => $model->attributeLabels()['phone'],
                    'class'       => 'mask_phone',
                    'options'     => [
                        'class' => 'mask_phone',
                    ],
                ]); ?>
            </div>
            <div class="lf_subm">
                <?= Html::submitInput('Далее',[
                        'class' => 'button',
                        'id'  => 'businessplan-submit',
                    ]); ?>
            </div>
        </div>

        <div id="download-plan-step2" class="reg_step_2" style="display: none">
            <h2>Подтвердите телефон</h2>
            <p>Введите код в SMS, который вам пришёл.</p>
            <div class="lf_input">
                <input id="download-plan-sms_code" name="sms_code" type="text" placeholder="Код в смс" value="" />
                <span class="er_text">Не заполнено</span>
            </div>
            <div class="reg_control">
                <a class="send_block" id="send_sms_again-download-plan" href="">Отправить заново</a>
                <a id="go_to_download-plan-step1" href="">Изменить данные</a>
            </div>
            <div class="lf_sub">
                <?= Html::submitInput('Подтвердить', ['class' => 'button']) ?>
            </div>
        </div>

        <div id="download-plan-step3" style="display: none">
            <div class="lf_success">
                <h2>Готово!</h2>
                <i></i>
                <p>Бизнес-план успешно отправлен на указанную вами электронную почту</p>
                <div class="lf_subm">
                    <?= Html::a('Узнайте о возможностях Гутакс',['/capabilities'],['class' => 'button']); ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
