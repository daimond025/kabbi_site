<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>

<section class="top_page tp_support">
    <div class="content">
        <h1><?= Html::encode($message) . ' (' . $exception->statusCode . ')'?></h1>
        <p><?= t('error', 'The above error occurred while the Web server was processing your request.'); ?></p>
    </div>
</section>