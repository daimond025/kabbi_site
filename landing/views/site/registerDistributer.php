<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\views\site\assets\RegisterDistributerAsset;

/* @var $model \landing\models\RegisterDistributerForm */
/* @var $this \yii\web\View */

RegisterDistributerAsset::register($this);

?>

<div style="display: none">
    <div class="remodal rem_part" data-remodal-id="partner"
         data-remodal-options="hashTracking: false">
        <button data-remodal-action="close" class="remodal-close"></button>
        <div id="register-distributer-step1" style="display: block">
            <h2>Стать партнером</h2>
            <?php
            $form = \yii\widgets\ActiveForm::begin([
                    'id'                     => 'register-distributer-form',
                    'action'                 => '/site/register-distributer',
                    'enableClientValidation' => false,
                    'enableAjaxValidation'   => true,
                    'validateOnBlur'         => false,
                    'validateOnChange'       => false,
                    'errorCssClass'          => 'error',
                    'fieldConfig'            => app()->params['fieldOptions'],
                    'options'                => [
                        'data' => [
                            'google-analytics' => YII_ENV_PROD,
                        ],
                    ],
            ]);

            ?>

            <h5>Контактное лицо</h5>

            <?=
            $form->field($model, 'contact_fio')->textInput([
                'maxLength'   => 255,
                'placeholder' => $model->attributeLabels()['contact_fio'],
            ]);

            ?>

            <?=
            $form->field($model, 'contact_position')->textInput([
                'maxLength'   => 255,
                'placeholder' => $model->attributeLabels()['contact_position'],
            ]);

            ?>

            <?=
            $form->field($model, 'contact_phone')->textInput([
                'maxLength'   => 255,
                'placeholder' => $model->attributeLabels()['contact_phone'],
                'class'       => 'mask_phone',
                'options'     => [
                    'class' => 'mask_phone',
                ]
            ]);

            ?>

            <?=
            $form->field($model, 'contact_email')->textInput([
                'maxLength'   => 255,
                'placeholder' => $model->attributeLabels()['contact_email'],
            ]);

            ?>
            <hr class="small_hr">


            <h5>Компания</h5>
            <?=
            $form->field($model, 'company')->textInput([
                'maxLength'   => 255,
                'placeholder' => $model->attributeLabels()['company'],
            ]);

            ?>

            <?=
            $form->field($model, 'city')->textInput([
                'maxLength'   => 255,
                'placeholder' => $model->attributeLabels()['city'],
            ]);

            ?>

            <div class="lf_input">
                <?=
                $form->field($model, 'site')->textInput([
                    'maxLength'   => 255,
                    'placeholder' => $model->attributeLabels()['site'],
                ]);

                ?>
            </div>

            <div class="lf_input">
                <?=
                $form->field($model, 'activity_type')->dropDownList(
                    $model->getActivityTypes(),
                    [
                    'prompt' => $model->attributeLabels()['activity_type'],
                ]);

                ?>
                <span class="er_text"></span>
            </div>

            <hr class="small_hr">
            <h5>Дополнительная информация</h5>

            <?=
            $form->field($model, 'source_type')->dropDownList(
                $model->getSourceTypes(),
                [
                'prompt' => $model->attributeLabels()['source_type'],
            ]);

            ?>

            <?=
            $form->field($model, 'direction_type')->dropDownList(
                $model->getDirectionTypes(),
                [
                'prompt' => $model->attributeLabels()['direction_type'],
            ]);

            ?>

            <?=
            $form->field($model, 'comment',
                [
                'options' => [
                    'class' => 'lf_input lf_text',
                ]
            ])->textarea([
                'class'       => 'lf_input lf_text',
                'maxLength'   => 255,
                'placeholder' => $model->attributeLabels()['comment'],
            ]);

            ?>

            <div class="lf_subm">
                <input class="button" type="submit" value="Отправить">
            </div>
            <? ActiveForm::end(); ?>
        </div>

        <div id="register-distributer-step2" style="display: none">
            <div class="lf_success">
                <h2>Анкета отправлена</h2>
                <p style="text-align: center; margin: 15px 0;">Спасибо за интерес к продукту. Мы скоро свяжемся с вами.</p>
                <i></i>
                <div class="lf_subm">
                    <?=
                    Html::submitInput('Продолжить работу',
                        [
                        'class' => 'button remodal-close',
                        'data'  => [
                            'remodal-action' => 'close',
                        ],
                    ]);

                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
