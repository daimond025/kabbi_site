<?php

use yii\helpers\Html;
use yii\captcha\Captcha;
use yii\widgets\ActiveForm;
use landing\controllers\SiteController;

?>

<?php $form = \yii\widgets\ActiveForm::begin([
        'id' => 'signupform',
        'action' => '/site/community-signup',
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'validateOnBlur' => false,
        'validateOnChange' => false,
        'errorCssClass' => 'error',
        'fieldConfig' => app()->params['fieldOptions'],
    ]);?>

<div class="lf_mail">
    <?= $form->field($model, 'email')->textInput([
            'maxLength' => 255,
            'placeholder' => 'Эл. почта',
        ]); ?>
</div>

<div class="lf_us_name">
    <?= $form->field($model, 'lastname')->textInput([
            'maxLength' => 255,
            'placeholder' => 'Фамилия',
        ]); ?>
    <?= $form->field($model, 'firstname')->textInput([
            'maxLength' => 255,
            'placeholder' => 'Имя'
        ]); ?>
</div>

<div class="lf_mail">
    <?= $form->field($model, 'password')->passwordInput([
            'maxLength' => 255,
            'placeholder' => 'Пароль',
        ]); ?>
</div>
<div class="lf_mail">
    <?= $form->field($model, 'password_repeat')->passwordInput([
            'maxLength' => 255,
            'placeholder' => 'Повторите пароль',
        ]); ?>
</div>

<?php
$showCaptcha = session()->get(SiteController::COUNTER_SIGNUPS) > SiteController::MAX_ATTEMPTS;
?>

<div id="<?= $form->id . '-verifycode'; ?>"
     class="lf_mail captcha" style="<?= $showCaptcha ? '' : 'display: none'; ?>">
    <?= $form->field($model, 'verifyCode')
            ->widget(Captcha::className(), [
                'captchaAction' => '/site/captcha',
                ]) ?>
</div>

<div class="lf_sub">
    <?= Html::submitInput('Зарегистрироваться', ['class' => 'button']); ?>
</div>

<?php ActiveForm::end(); ?>
