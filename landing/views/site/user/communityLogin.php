<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
use landing\controllers\SiteController;

?>

<?php $form = \yii\widgets\ActiveForm::begin([
        'id' => 'communityloginform',
        'action' => '/site/community-login',
        'errorCssClass' => 'error',
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'validateOnBlur' => false,
        'validateOnChange' => false,
        'fieldConfig' => app()->params['fieldOptions'],
    ]);?>

<div class="lf_mail">
    <?= $form->field($model, 'email')->textInput([
            'maxLength' => 255,
            'placeholder' => 'Эл. почта',
        ]); ?>
</div>

<div class="lf_pass">
    <?= $form->field($model, 'password')->passwordInput([
            'maxLength' => 255,
            'placeholder' => 'Пароль',
        ]); ?>
    <a class="">Забыли пароль?</a>
</div>

<?php
$showCaptcha = session()->get(SiteController::COUNTER_COMMUNITY_LOGINS) > SiteController::MAX_ATTEMPTS;
?>

<div id="<?= $form->id . '-verifycode'; ?>"
     class="lf_mail captcha" style="<?= $showCaptcha ? '' : 'display: none'; ?>">
    <?= $form->field($model, 'verifyCode')
            ->widget(Captcha::className(), [
                'captchaAction' => '/site/captcha',
                ]) ?>
</div>

<div class="lf_sub">
    <?= Html::submitInput('Войти', ['class' => 'button']); ?>
</div>

<?php ActiveForm::end(); ?>