<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = \yii\widgets\ActiveForm::begin([
        'errorCssClass' => 'error',
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'validateOnBlur' => false,
        'validateOnChange' => false,
        'validationUrl' => yii\helpers\Url::to('/site/recovery-password'),
        'fieldConfig' => app()->params['fieldOptions'],
    ]);?>

<div class="lf_adr">
    <b>.<?= $domain; ?></b>
    <div class="lf_input">
        <?= $form->field($model, 'domain')->textInput([
                'maxLength' => 255,
                'placeholder' => 'Адрес',
            ]); ?>
    </div>
</div>

<div class="lf_mail">
    <?= $form->field($model, 'email')->textInput([
            'maxLength' => 255,
            'placeholder' => 'Эл. почта',
        ]); ?>
</div>

<div class="lf_pass">
    <div class="lf_input">
        <input type="password" disabled placeholder="Пароль"/>
        <span class="er_text">Не заполнено</span>
    </div>
</div>

<div class="lf_rest">
    <a class="">Пароль найден</a>
</div>

<div class="lf_sub">
    <?= Html::submitInput('Выслать пароль', [
            'class' => 'button',
        ]); ?>
</div>

<?php ActiveForm::end(); ?>