<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = \yii\widgets\ActiveForm::begin([
        'errorCssClass' => 'error',
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'validateOnBlur' => false,
        'validateOnChange' => false,
        'validationUrl' => yii\helpers\Url::to('/site/community-request-reset-password'),
        'fieldConfig' => app()->params['fieldOptions'],
    ]);?>

<div class="lf_mail">
    <?= $form->field($model, 'email')->textInput([
            'maxLength' => 255,
            'placeholder' => 'Эл. почта',
        ]); ?>
</div>

<div class="lf_pass">
    <div class="lf_input">
        <input type="password" disabled placeholder="Пароль"/>
        <span class="er_text">Не заполнено</span>
    </div>
    <a>Пароль найден</a>
</div>

<div class="lf_sub">
    <?= Html::submitInput('Сбросить пароль', ['class' => 'button']); ?>
</div>

<?php ActiveForm::end(); ?>