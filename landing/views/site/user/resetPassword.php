<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->params['topPageTitle'] = 'Смена пароля';

$fieldConfig = Yii::$app->params['fieldOptions'];

$fieldConfig['template'] = "{label}\n{input}\n{error}";

?>

<div class="edit_profile">
    <?php $form = ActiveForm::begin([
        'fieldConfig' => $fieldConfig,
        'enableClientValidation' => false,
    ]); ?>
        <?= $form->field($model, 'password')->passwordInput()->label('Изменить пароль'); ?>
        <?= $form->field($model, 'password_repeat')->passwordInput()->label('Повторите пароль'); ?>
        <?= Html::submitInput('Сохранить', ['class' => 'button']); ?>
    <?php ActiveForm::end(); ?>
</div>