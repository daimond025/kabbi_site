<?php

namespace app\views\site\assets;

use yii\web\AssetBundle;

class DownloadPlanAsset extends AssetBundle
{
    public $sourcePath = '@app/views/site/assets/';
    public $publishOptions = [
        'forceCopy' => true
    ];
    public $css = [
    ];
    public $js = [
        'js/downloadPlan.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
