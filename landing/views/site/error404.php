<?php

use yii\helpers\Url;

?>

<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Gootax</title>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,400italic&subset=cyrillic' rel='stylesheet'
              type='text/css'>
        <link rel="stylesheet" href="/error/css/main.css"/>
    </head>
    <body>
    <div class="wrapper">
        <div class="error_text">
            <h1>:-(</h1>
            <div>
                <b><?= $name; ?></b><span><?= $message; ?></span>
                <h2><a href="<?= Url::to(['/']); ?>">Перейти на главную страницу Гутакса</a></h2>
            </div>
        </div>
    </div>
    </body>
</html>
