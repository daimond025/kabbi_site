<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace landing\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
          'js/app.min.js',
//        'js/libs.min.js',
//        'js/jquery.colorbox-min.js',
//        'js/inputmask/jquery.bind-first-0.1.min.js',
//        'js/inputmask/jquery.inputmask.js',
//        'js/inputmask/jquery.inputmask-multi.js',
//        'js/after_body.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
