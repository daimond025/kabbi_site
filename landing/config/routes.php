<?php

return [
    'signup'                                            => 'site/community-signup',
    'login'                                             => 'site/community-login',
    'logout'                                            => 'site/community-logout',
    'captcha'                                           => 'site/captcha',
    '<controller:(site|tenant|user)>/<action>'          => '<controller>/<action>',
    'confirm-email/<token>'                             => 'site/community-confirm-email/<token>',
    'request-reset-password'                            => 'site/community-request-reset-password',
    'community/'                                        => 'community/community/index',
    'community/search/author/<author_id:\d+>'           => 'community/community/search',
    'community/search/tag/<tag_id:\d+>'                 => 'community/community/search',
    'community/search'                                  => 'community/community/search',
    'community/rss'                                     => 'community/community/rss',
    'community/profile/<id:\d+>/offers'                 => 'community/profile/offers',
    'community/profile/<id:\d+>/communion'              => 'community/profile/communion',
    'community/<controller>'                            => 'community/<controller>/index',
    '<module>/<controller>/<action>/<id:\d+>'           => '<module>/<controller>/<action>',
    '<module>/<controller>/<action>'                    => '<module>/<controller>/<action>',
    '<view:[\w\-\/]{1,}>/'                              => 'page/index',
    '/'                                                 => 'page/index',
];