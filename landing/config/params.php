<?php

return [
    'upload' => getenv('LANDING_UPLOAD_DIR'),

    'contacts.skype'         => getenv('LANDING_CONTACTS_SKYPE'),
    'contacts.phone'         => getenv('LANDING_CONTACTS_PHONE'),
    'contacts.email'         => getenv('LANDING_CONTACTS_EMAIL'),
    'contacts.partnersEmail' => getenv('LANDING_CONTACTS_PARTNERS_EMAIL'),

    'sms_code_exp'       => 300,
    'fieldOptions'       => [
        'options'      => [
            'class' => 'lf_input',
        ],
        'errorOptions' => [
            'tag'   => 'span',
            'class' => 'er_text',
        ],
        'inputOptions' => [
            'class' => '',
        ],
        'template'     => "{input}\n{error}",
    ],
    'demoTariffValidity' => getenv('COMMON_DEMO_TARIFF_VALIDITY'),
    'supportedLanguages' => require __DIR__ . '/supportedLanguages.php',
];
