<?php

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/params.php'
);

$logVars = array_merge(
    ['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION', '_SERVER'],
    require __DIR__ . '/../../common/config/logVarsFilter.php'
);

$config = [
    'id'                  => 'app-landing',
    'aliases'             => [
        '@lead' => '@landing/modules/lead',
    ],
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log', 'bootstrapLanguage'],
    'controllerNamespace' => 'landing\controllers',

    'components' => [
        // Yii2 components
        'assetManager' => [
            'appendTimestamp' => true,
            'basePath'        => '@webroot/assets',
            'baseUrl'         => '@web/assets',
            'forceCopy'       => YII_ENV_TEST,
        ],

        'bootstrapLanguage' => [
            'class'              => 'landing\components\bootstrap\Language',
            'supportedLanguages' => array_keys($params['supportedLanguages']),
        ],

        'authManager' => '\yii\rbac\DbManager',

        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'log' => [
            'traceLevel' => 3,
            'targets'    => [
                [
                    'class'   => 'yii\log\FileTarget',
                    'levels'  => ['error', 'warning'],
                    'logVars' => $logVars,
                ],
                [
                    'class'   => 'yii\log\EmailTarget',
                    'mailer'  => 'mailer_log',
                    'levels'  => ['error', 'warning'],
                    'logVars' => $logVars,
                    'message' => [
                        'from'    => [getenv('MAIL_SUPPORT_USERNAME')],
                        'to'      => [getenv('MAIL_SUPPORT_USERNAME')],
                        'subject' => getenv('MAIL_SUPPORT_SUBJECT_LANDING'),
                    ],
                ],
            ],
        ],

        'request' => [
            'baseUrl'             => '',
            'cookieValidationKey' => getenv('LANDING_COOKIE_VALIDATION_KEY'),
            'csrfCookie'          => ['httpOnly' => true, 'secure' => YII_ENV_PROD],
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'normalizer'      => [
                'class'  => 'yii\web\UrlNormalizer',
                'action' => yii\web\UrlNormalizer::ACTION_REDIRECT_PERMANENT,
                // use temporary redirection instead of permanent
            ],
            'rules'           => require __DIR__ . '/routes.php',
        ],

        'user'             => [
            'identityClass'   => 'landing\modules\community\models\user\CommunityUser',
            'enableAutoLogin' => true,
        ],

        // Others
        'subscribeManager' => 'landing\modules\community\components\SubscribeManager',
    ],

    'modules' => [
        'community'     => 'app\modules\community\Module',
        'redactor'      => 'yii\redactor\RedactorModule',
        'lead'          => '\lead\Module',
        'tenant-tariff' => [
            'class'   => 'backend\modules\tenant\Module',
            'modules' => [
                'tariff' => 'backend\modules\tenant\modules\tariff\Module',
            ],
        ],
    ],

    'params' => $params,
];

if (YII_ENV_DEV) {
    $config['bootstrap'][]      = 'debug';
    $config['modules']['debug'] = [
        'class'      => 'yii\debug\Module',
        'allowedIPs' => ['192.168.1.*', '127.0.0.1'],
    ];

    $config['bootstrap'][]    = 'gii';
    $config['modules']['gii'] = [
        'class'      => 'yii\gii\Module',
        'allowedIPs' => ['192.168.1.*', '127.0.0.1'],
    ];
}

return $config;