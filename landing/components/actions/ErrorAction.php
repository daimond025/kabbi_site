<?php

namespace landing\components\actions;

use Yii;

class ErrorAction extends \yii\web\ErrorAction
{
    public function run() {
        $this->controller->layout = false;
        
        if (($exception = Yii::$app->getErrorHandler()->exception) === null) {
            // action has been invoked not from error handler, but by direct route, so we display '404 Not Found'
            $exception = new HttpException(404, Yii::t('yii', 'Page not found.'));
        }

        if ($exception instanceof HttpException) {
            $code = $exception->statusCode;
        } else {
            $code = $exception->getCode();
        }
        if ($exception instanceof Exception) {
            $name = $exception->getName();
        } else {
            $name = $this->defaultName ?: Yii::t('yii', 'Error');
        }
        if ($code) {
            $name .= " (#$code)";
        }

        if ($exception instanceof UserException) {
            $message = $exception->getMessage();
        } else {
            $message = $this->defaultMessage ?: Yii::t('yii', 'An internal server error occurred.');
        }

        if ($exception->statusCode == 404) {
            $name = 'Такой страницы не существует.';
            $message = 'Вероятно, у неё изменился путь или она была удалена с сервера';
        }

        if (Yii::$app->getRequest()->getIsAjax()) {
            return "$name: $message";
        } else {
            return $this->controller->render('error404', [
                'name' => $name,
                'message' => $message,
                'exception' => $exception,
            ]);
        }
    }
}