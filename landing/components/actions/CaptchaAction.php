<?php

namespace app\components\actions;

class CaptchaAction extends \yii\captcha\CaptchaAction
{
    public $length = 6;

    protected function generateVerifyCode() {
        $code = '';
        for ($i = 0; $i < $this->length; ++$i) {
            $code .= mt_rand(0, 9);
        }

        return $code;
    }
}