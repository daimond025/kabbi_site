<?php

namespace landing\components\actions;

class ImageUploadAction extends \landing\components\actions\FileUploadAction
{
    public function init() {
        $this->rules = [
            [
                'file', 'file',
                'extensions' => ['png', 'jpeg', 'jpg'],
                'maxSize' => getUploadMaxFileSize(),
                'tooBig' => t('file', 'The file size must be less than {file_size}Mb', [
                    'file_size' => getUploadMaxFileSize(true)])
            ]
        ];

        parent::init();
    }

}