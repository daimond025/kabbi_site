<?php

namespace landing\components\actions;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\Url;
use yii\helpers\FileHelper;
use yii\base\DynamicModel;
use yii\web\Response;

class FileUploadAction extends \yii\base\Action
{
    public $web = '@web';
    public $webroot = '@webroot';
    public $rules = [];
    public $path;

    public function run()
    {
        app()->response->format = Response::FORMAT_JSON;

        $file = UploadedFile::getInstanceByName('file');
        $model = DynamicModel::validateData(['file' => $file], $this->rules);

        $basename = uniqid() . '.' . $file->extension;
        $filename = FileHelper::normalizePath(
                Yii::getAlias($this->webroot) . '/' . $this->path . '/' . $basename);

        $link = preg_replace('/\/+/', '/', '/' . $this->path . '/' . $basename);
        $filelink = Url::to(Yii::getAlias($this->web) . $link, true);

        FileHelper::createDirectory(dirname($filename));

        if (!$model->hasErrors() && $file->saveAs($filename)) {
            return ['filelink' => $filelink];
        } else {
            return ['error' => 'An error occurred while loading a file'];
        }
    }

}