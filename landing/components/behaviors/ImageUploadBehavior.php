<?php

namespace landing\components\behaviors;

use yii\helpers\Url;
use yii\helpers\FileHelper;
use yii\db\ActiveRecord;
use yii\imagine\Image;
use Imagine\Image\Box;
use Imagine\Image\Point;

class ImageUploadBehavior extends FileUploadBehavior
{
    public $createThumb = true;
    public $thumbPrefix = 'thumb_';

    public $thumbWidth = 80;
    public $thumbHeight = 80;

    public $originQuality = 80;
    public $thumbQuality = 90;

    public $cropParams;

    public $maxSize;

    public function init() {
        if (post('Crop')) {
            $this->cropParams = post('Crop');
        }

        parent::init();
    }

    public function getThumbFilename($basename)
    {
        return parent::getFilename($this->thumbPrefix . $basename);
    }

    public function getThumbFilelink($basename)
    {
        return parent::getFilelink($this->thumbPrefix . $basename);
    }

    public function deleteFile($basename)
    {
        parent::deleteFile($basename);
        if ($this->createThumb) {
            parent::deleteFile($this->thumbPrefix . $basename);
        }
    }

    protected function cropImage($image)
    {
        return $image->crop(
                new Point($this->cropParams['x'], $this->cropParams['y']),
                new Box($this->cropParams['w'], $this->cropParams['h'])
        );
    }

    protected function resizeImage($image)
    {
        $size = $image->getSize();
        $ratio = $size->getWidth() / $size->getHeight();

        if ($ratio > 0) {
            $width = $this->maxSize;
            $height = round($width / $ratio);
        } else {
            $height = $this->maxSize;
            $width = round($height / $ratio);
        }

        return $image->resize(new Box($width, $height));
    }

    public function saveFile($filename, $fileInstance)
    {
        $basename = $this->generateBasename($fileInstance->extension);
        $this->owner->{$filename} = $basename;
        $file = $this->getFilename($basename);
        FileHelper::createDirectory(dirname($file));

        $image = Image::getImagine()->open($fileInstance->tempName);

        if ($this->cropParams) {
            $this->cropImage($image);
        }

        if ($this->maxSize) {
            $this->resizeImage($image);
        }

        $image->save($file, [$this->originQuality]);

        if ($this->createThumb) {
            $image->thumbnail(new Box($this->thumbWidth, $this->thumbHeight))
                    ->save($this->getThumbFilename($basename), [$this->thumbQuality]);
        }
    }

}