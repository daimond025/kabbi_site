<?php

namespace landing\components\behaviors;

use yii\helpers\Url;
use yii\helpers\FileHelper;
use yii\db\ActiveRecord;

class FileUploadBehavior extends \yii\base\Behavior
{
    public $web = '@web';
    public $webroot = '@webroot';

    public $path;

    /*
     * [
     *      'filename' => '',
     *      'file' => '',
     * ]
     * 
     * @var array
     */
    public $files;

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
        ];
    }

    public function getFilename($basename)
    {
        return FileHelper::normalizePath(
                \Yii::getAlias($this->webroot)
                    . '/' . $this->path . '/' . $basename);
    }

    public function getFilelink($basename)
    {
        $link = preg_replace('/\/+/', '/', '/' . $this->path . '/' . $basename);
        return Url::to(\Yii::getAlias($this->web) . $link, true);
    }

    public function generateBasename($extension)
    {
        do {
            $basename = uniqid() . '.' . $extension;
            $filename = $this->getFilename($basename);
        } while (file_exists($filename));

        return $basename;
    }

    public function deleteFile($basename)
    {
        $filename = $this->getFilename($basename);
        if (is_file($filename) && file_exists($filename)) {
            unlink($filename);
        }
    }

    public function deleteFiles()
    {
        foreach ($this->files as $file) {
            $this->deleteFile($file['filename']);
        }
    }

    public function saveFile($filename, $fileInstance)
    {
        $basename = $this->generateBasename($fileInstance->extension);
        $this->owner->{$filename} = $basename;
        $file = $this->getFilename($basename);

        FileHelper::createDirectory(dirname($file));
        $fileInstance->saveAs($file);
    }

    public function updateFiles()
    {
        foreach ($this->files as $file) {
            $oldBasename = $this->owner->oldAttributes[$file['filename']];
            $basename = $this->owner->{$file['filename']};
            $fileInstance = \yii\web\UploadedFile::getInstance($this->owner, $file['file']);

            if ($oldBasename && !$basename && !$fileInstance) {
                $this->deleteFile($oldBasename);
            } elseif ($fileInstance) {
                $this->deleteFile($oldBasename);
                $this->saveFile($file['filename'], $fileInstance);
            }
        }
    }

    public function beforeDelete($event)
    {
        $this->deleteFiles();
    }

    public function beforeSave($event)
    {
        $this->updateFiles();
    }

}
