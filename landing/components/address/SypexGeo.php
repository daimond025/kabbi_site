<?php

namespace landing\components\address;

use common\components\curl\Curl;
use common\components\curl\CurlResponse;

class SypexGeo extends \yii\base\Object
{

    public $host   = 'http://api.sypexgeo.net';
    public $format = 'json';

    /**
     * Getting address by ip
     * @param string $ip
     * @return array|null
     */
    public function getAddressByIp($ip)
    {
        /* @var $response CurlResponse */
        $curl = new Curl();
        $response = $curl->get(implode("/", [$this->host, $this->format, $ip]));
        return json_decode($response->body, true);
    }
}
