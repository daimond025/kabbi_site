<?php

namespace landing\components\bootstrap;


use yii\base\BootstrapInterface;

class Language implements BootstrapInterface
{


    /**
     * @var array
     */
    public $supportedLanguages = [];

    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        if (get('lang')) {
            $lang = get('lang');
        } else {
            $lang = $this->getPreferredLanguage();
        }

        $app->language = in_array($lang, $this->supportedLanguages) ? $lang : 'en';
    }

    /**
     * Getting preferred language
     * @return string
     */
    public function getPreferredLanguage()
    {
        return \Yii::$app->request->getPreferredLanguage($this->supportedLanguages);
    }
}