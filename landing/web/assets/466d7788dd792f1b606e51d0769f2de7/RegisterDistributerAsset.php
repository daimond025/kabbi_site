<?php

namespace app\views\site\assets;

use yii\web\AssetBundle;

class RegisterDistributerAsset extends AssetBundle
{
    public $sourcePath = '@app/views/site/assets/';
    public $publishOptions = [
        'forceCopy' => true
    ];
    public $css = [
    ];
    public $js = [
        'js/registerDistributer.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
