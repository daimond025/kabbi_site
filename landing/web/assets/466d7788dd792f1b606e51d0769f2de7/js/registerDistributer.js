/* global jQuery */
(function ($) {
    'use strict';
    var app = {
        $step1: $('#register-distributer-step1'),
        $step2: $('#register-distributer-step2'),
        init: function () {
            this.registerBeforeSave();
            this.registerRemodalClosedEvent();
        },
        registerRemodalClosedEvent: function () {
            var _this = this;
            $(document).on('closed', '.remodal', function (e) {
                if ($(e.target).hasClass('rem_part')) {
                    _this.$step2.hide();
                    _this.$step1.show();
                }
            });
        },
        registerBeforeSave: function () {
            var _this = this,
                    $form = $('#register-distributer-form'),
                    $submit = $form.find('[type=submit]');

            $form.on('beforeValidate', function () {
                if ($form.data('process')) {
                    return false;
                }

                $form.data('process', true);
                $submit.attr('disabled', true);
                $.post(
                        $form.attr('action'),
                        $form.serialize()
                        )
                        .done(function (data) {
                            var company;

                            $form.data('process', false);
                            $submit.attr('disabled', false);

                            if (data === 'success') {
                                if ($form.data('google-analytics')) {
                                    company = $('#registerdistributerform-company').val();
                                    _this.sendEventToGA(company);
                                }

                                $form.trigger('reset');
                                _this.$step1.hide();
                                _this.$step2.show();
                            } else {
                                try {
                                    $form.yiiActiveForm('updateMessages', data, true);
                                } catch (e) {
                                    console.log('fail', e.message);
                                }
                            }
                        })
                        .fail(function (data) {
                            $form.data('process', false);
                            $submit.attr('disabled', false);
                            console.log('fail', data);
                        });
                return false;
            });
        },
        sendEventToGA: function (company) {
            if (ga) {
                ga('send', 'event', 'DistributeRegistration', 'Done', company);
            }
        }
    };

    app.init();
})(jQuery);