function send_sms(phone)
    {
        $.get('/business-plan/send-sms-code', {phone: phone});
        $('#send_sms_again-download-plan').addClass('send_block');
        setTimeout(function(){
            $('#send_sms_again-download-plan').removeClass('send_block');
        }, 15000);
    }



    //Кнопка "Далее"
    var old_phone = null; //При нажатии кнопки "Далее" отправки смски только при смене номера
    $('#businessplan-submit').on('click',function(e){
        e.preventDefault();
        var form = $(this).parents('form');
        var error = false;
        var step_1 = $('#download-plan-step1');
        //Валидация
        if(step_1.find('.error').size() > 0)
            error = true;

        step_1.find('.required').each(function(){
            var input = $(this).find('input');
            if(input.val() == '')
            {
                error = true;
                input.parent().addClass('error');
                input.nextAll('.er_text, .help-block-error').text('Не заполнено');
            }
        });

        if(error)
            return false;

        //-------------------------------------------------------

        //Переход на второй шаг, когда все поля заполнены.
        //Валидация при сабмите.
        $.post(
            '/business-plan/validate',
            form.serialize(),
            function(json){
                if(json == '')
                {
                    //Отправка смс кода
                    var phone = $('#businessplan-phone').val();

                    if(phone != old_phone)
                    {
                        old_phone = phone;
                        send_sms(phone);
                    }

                    //Переход на следующий шаг
                    $('#download-plan-step1').hide();
                    $('#download-plan-step2').show();
                }
            },
            'json'
        );
    });

    //Кнопка "Изменить данные"
    $('#go_to_download-plan-step1').on('click', function(e){
        e.preventDefault();
        $('#download-plan-step2').hide();
        $('#download-plan-step1').show();
    });

    //Кнопка "Подтвердить"
    $('#download-plan-step2 .button').on('click',function(e){
        e.preventDefault();

        var form = $(this).parents('form');
        var button = $(this);
        var sms_code = $('#download-plan-sms_code');
        var code = sms_code.val();

        if (form.data('process')) {
            return false;
        }

        if(code == '')
        {
            sms_code.parent().addClass('error');
            sms_code.next().text('Не заполнено');
            return false;
        }

        sms_code.parent().removeClass('error');

        form.data('process', true);
        button.attr('disabled', true);

        //Проверка смс кода
        $.get(
            '/business-plan/confirm-sms-code',
            {code: code},
            function(json){
                if(json.error == null){
                    //Телефон подтвержден, отправляем форму 
                    $.post(
                        '/business-plan/send',
                        form.serialize(),
                        function(json){
                            if(json['status'] === 'success') {
                                
                                $('#download-plan-step2').hide();
                                $('#download-plan-step3').show();
                                // Yandex metrika
                                if (form.data('yandex-metrika')) {
                                    yaCounter30670468.reachGoal('BusinessPlanRegistration');
                                }

                                // Google analytics
                                if (form.data('google-analytics')) {
                                    ga('send', 'event', 'BusinessPlanRegistration', 'Done');
                                }
                            }
                            else {
                                $('#download-plan-step2').hide();
                                $('#download-plan-step1').show();
                            }
                        },
                        'json'
                    )
                        .done(function (data) {
                            form.data('process', false);
                            button.attr('disabled', false);
                        })
                        .fail(function (data) {
                            console.log('fail', data);
                            form.data('process', false);
                            button.attr('disabled', false);
                        });
                } else {
                    sms_code.parent().addClass('error');
                    sms_code.next().text(json.error);

                    form.data('process', false);
                    button.attr('disabled', false);
                }
            },
            'json'
        )
        .fail(function (data) {
            console.log('fail', data);
            form.data('process', false);
            button.attr('disabled', false);
        });
    });

    //Кнопка "Отправить заново"
    $('#send_sms_again-download-plan').on('click', function(e){
        e.preventDefault();

        if($(this).hasClass('send_block'))
            return false;

        send_sms($('#businessplan-phone').val());
    });