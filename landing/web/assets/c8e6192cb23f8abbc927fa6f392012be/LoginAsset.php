<?php

namespace app\views\layouts\login\assets;

use yii\web\AssetBundle;

class LoginAsset extends AssetBundle
{
    public $sourcePath = '@app/views/layouts/login/assets/';
    public $css = [
    ];
    public $js = [
        'js/login.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
