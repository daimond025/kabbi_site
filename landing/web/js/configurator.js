//Change it!
var preloader_src = 'images/loader.svg';
var iphone_screens_src = '/content/config_iphone_screens.html';
var android_screens_src = '/content/config_android_screens.html';
////////////


var changeColor, api;

$(document).ready(function () {
    if ($(window).width() > 700) {
        $('.load_false').remove();

        var f = $.farbtastic('#picker');
        var tempRgb;
        $('.colorpicker_window').hide();



        var current_colors = '{"accent_bg": "#2aba90", "accent_text": "#ffffff", "menu_bg": "#3a3d3c", "menu_text": "#ffffff", "menu_stroke": "#ffffff", "content_bg": "#ffffff", "content_text": "#000000", "content_stroke": "#000000", "content_icon_bg": "#2aba90", "content_icon_stroke": "#2aba90", "map_marker_bg": "#3a3d3c", "map_marker_bg_stroke": "#3a3d3c", "map_marker_text": "#ffffff", "map_car_bg": "#656a68"}'
        current_colors = JSON.parse(current_colors);
        var temp_colors = '{"accent_bg": "#2aba90", "accent_text": "#ffffff", "menu_bg": "#3a3d3c", "menu_text": "#ffffff", "menu_stroke": "#ffffff", "content_bg": "#ffffff", "content_text": "#000000", "content_stroke": "#000000", "content_icon_bg": "#2aba90", "content_icon_stroke": "#2aba90", "map_marker_bg": "#3a3d3c", "map_marker_bg_stroke": "#3a3d3c", "map_marker_text": "#ffffff", "map_car_bg": "#656a68"}'
        temp_colors = JSON.parse(temp_colors);






        $('body').on('click', '.open_cw', function () {
            if (!$(this).parents('.acc_item').hasClass('active_acc')) {
                $('.active_acc').removeClass('active_acc');
                var win = $(this).parents('.acc_item').addClass('active_acc').find('.colorpicker_window');
                $('.colorpicker_window').hide().removeClass('opened_cw');
                $(win).show();
                setTimeout(function () {
                    $(win).addClass('opened_cw');
                }, 1);
                if ($(win).find('.cw_tab.active').hasClass('with_picker')) {
                    $('#picker').appendTo($(win).find('.cw_tab.active').find('.mp_picker'));
                    f.linkTo($(win).find('.cw_tab.active .colorwell'));
                    $(win).find('.cw_tab.active .colorwell').trigger('change');
                };

            };
        })

        $('.colorwell').on('change', function () {
            if ($(this).val() !== '') {
                tempRgb = hexToRgb($(this).val());
                $(this).parent().find('.c_r').attr('value', tempRgb.r).val(tempRgb.r);
                $(this).parent().find('.c_g').attr('value', tempRgb.g).val(tempRgb.g);
                $(this).parent().find('.c_b').attr('value', tempRgb.b).val(tempRgb.b);
            };
        })

        $('.c_r, .c_g, .c_b').on('keyup', function () {
            if (Number($(this).val()) > 255) $(this).val(255).attr('value', 255);
            if (Number($(this).val()) < 0) $(this).val(0).attr('value', 0);
            var hex = rgbToHex(Number($(this).parent().find('.c_r').val()), Number($(this).parent().find('.c_g').val()), Number($(this).parent().find('.c_b').val()));
            $(this).parent().find('.colorwell').attr('value', hex).val(hex).trigger('keyup');
        })

        function hexToRgb(hex) {
            var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
            hex = hex.replace(shorthandRegex, function (m, r, g, b) {
                return r + r + g + g + b + b;
            });

            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            return result ? {
                r: parseInt(result[1], 16),
                g: parseInt(result[2], 16),
                b: parseInt(result[3], 16)
            } : null;
        };

        function componentToHex(c) {
            var hex = c.toString(16);
            return hex.length == 1 ? "0" + hex : hex;
        };

        function rgbToHex(r, g, b) {
            return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
        };

        $('body').on('click', '.color_apply a', function () {
            var cw = $(this).parents('.acc_item').removeClass('active_acc').find('.colorpicker_window').removeClass('opened_cw');
            setTimeout(function () {
                $(cw).hide();
            }, 200);
            if ($(this).hasClass('c_apply')) {
                for (var key in current_colors) {
                    current_colors[key] = temp_colors[key];
                };
                changeColorProcess2(current_colors);
            } else {
                appendColors(current_colors);
                $(this).parents('.colorpicker_window').find('.tp_text_color a.active').removeClass('active');
                var cl = $(this).parents('.colorpicker_window').find('.tp_text_color input[type="hidden"]').val();
                $(this).parents('.colorpicker_window').find('.tp_text_color a[data-text-color="' + cl + '"]').addClass('active');
            };

        })

        $('body').on('click', '.tp_text_color a', function () {
            var item = $(this).parents('.colorpicker_window').attr('data-cw');
            var par = $(this).parents('.acc_item');
            $(this).parent().find('a').removeClass('active');
            $(this).addClass('active');
            var color = $(this).attr('data-text-color');
            $(this).parent().find('input[type="hidden"]').attr('value', color).val(color);
            changeColorProcess(temp_colors);
        })

        changeColor = function (color) {
            $('#picker').parents('.colorpicker_window').find('.cw_tab.active .colorwell').trigger('change');
            $('#picker').parents('.colorpicker_window').find('.cw_tab.active .current_color').css('background', color);
            changeColorProcess(temp_colors);
        }

        function changeColorProcess(ob) {
            for (var key in ob) {
                if (key.indexOf('stroke') == -1) {
                    ob[key] = $('#' + key).val();
                    $('.' + key).attr('fill', ob[key]);
                }
                else {
                    ob[key] = $('[data-st-' + key + ']').val();
                    $('.' + key).attr('stroke', ob[key]);
                }
            }
        }

        function changeColorProcess2(ob) {
            for (var key in ob) {
                if (key.indexOf('stroke') == -1) {
                    $('.' + key).attr('fill', ob[key]);
                }
                else {
                    $('.' + key).attr('stroke', ob[key]);
                }
            }
        }

        $('body').on('click', '.cw_tabs a', function () {
            $(this).parent().find('[data-cwtab].active').removeClass('active');
            $(this).addClass('active');
            $(this).parents('.colorpicker_window').find('.cw_tabs_content .cw_tab.active').removeClass('active');
            $(this).parents('.colorpicker_window').find('.cw_tab[data-cwtab="' + $(this).attr('data-cwtab') + '"]').addClass('active');
            var seli = $(this).parents('.colorpicker_window').find('.cw_tabs_content .active');
            if ($(seli).hasClass('with_picker')) {
                $('#picker').appendTo($(seli).find('.mp_picker'));
                f.linkTo($(seli).find('.colorwell'));
            }
        });

        $(function () {
            var element = $('.scroll_content').jScrollPane();
            api = element.data('jsp');
        });

        $('body').on('click', '.av_tab_link', function () {
            $('.av_tabs').find('.active').removeClass('active');
            $(this).addClass('active');
            var trg = $(this).attr('data-avt');
            if ($('.av_tab.active').attr('data-avt') != trg) {
                $('.load_false, .avtc_preloader').animate({ "opacity": 0 }, 100, function () { $(this).remove(); });
                $('.av_tab.active').animate({ 'left': -30, "opacity": 0 }, 200, function () {
                    $(this).removeClass('active');
                    $('.av_tab[data-avt="' + trg + '"]').addClass('active').css({ "opacity": 0, "left": -30 }).animate({ "opacity": 1, 'left': 0 });
                    if (!$('.av_tab[data-avt="' + trg + '"]').hasClass('loaded')) {
                        var addr = android_screens_src;
                        if (trg == 'a1') arrd = iphone_screens_src;
                        $.ajax({
                            url: addr,
                            cache: true,
                            beforeSend: function () {
                                $('.load_false').animate({ "opacity": 0 }, 100, function () { $(this).remove(); });
                                $('.app_view').append('<img src="' + preloader_src + '" alt="preloader" class="avtc_preloader"/>');
                            },
                            success: function (html) {
                                $('.avtc_preloader').animate({ "opacity": 0 }, 200);
                                setTimeout(function () {
                                    $('.avtc_preloader').remove();
                                    $('.av_tab[data-avt="a1"]').html(html);
                                    $('.screen_item').animate({ "opacity": 1 }, 200);
                                    $('.av_tab[data-avt="' + trg + '"]').addClass('loaded');
                                    api.reinitialise();
                                }, 200)
                            },
                            error: function () {
                                $('.avtc_preloader').animate({ "opacity": 0 }, 200, function () { $(this).remove(); });
                                $('.app_view').append('<i class="load_false">:(</i>');
                            }
                        });
                    }
                    api.reinitialise();
                });
            }
        });

        $('body').on('click', '.default_theme', function () {
            var theme = $(this).attr('data-colors');
            temp_colors = JSON.parse(theme);
            var cw = $('.colorpicker_window.opened_cw').removeClass('opened_cw');
            $(cw).parents('.acc_item').removeClass('active_acc');
            setTimeout(function () {
                $(cw).hide();
            }, 200);
            appendColors(temp_colors);
            for (var key in current_colors) {
                current_colors[key] = temp_colors[key];
            };
        })

        function appendColors(theme) {
            for (var key in theme) {
                if (key.indexOf('stroke') == -1) {
                    if (key.indexOf('text') != -1) {
                        $('#' + key).parent().find('.active').removeClass('active');
                        $('#' + key).parent().find('[data-text-color="' + theme[key] + '"]').addClass('active');
                    }
                    $('.' + key).attr('fill', theme[key]);
                    $('#' + key).attr('value', theme[key]).val(theme[key]);
                }
                else $('.' + key).attr('stroke', theme[key]);
            }
        }



        $('body').on('click', '.generate_theme a', function () {

            var adds = {
                splash_bg: '#ffffff',
                accent_bg_tariff: '#AA' + current_colors.accent_bg.substr(1, 6)

            };

            $.extend(adds, current_colors);

            var colors_json = JSON.stringify(adds);
            $('.generate_theme').find('textarea').html(colors_json);
        });

        $('body').on('click', '.generate_theme textarea', function () {
            $(this).select();
        });




        $(window).load(function () {
            $.ajax({
                url: iphone_screens_src,
                cache: true,
                beforeSend: function () {
                    $('.app_view').append('<img src="' + preloader_src + '" alt="preloader" class="avtc_preloader"/>');
                    $('.load_false').animate({ "opacity": 0 }, 100, function () { $(this).remove(); });
                },
                success: function (html) {
                    $('.avtc_preloader').animate({ "opacity": 0 }, 200);
                    setTimeout(function () {
                        $('.avtc_preloader').remove();
                        $('.av_tab[data-avt="a1"]').addClass('loaded').html(html);
                        $('.screen_item').animate({ "opacity": 1 }, 200);
                        api.reinitialise();
                    }, 200);
                },
                error: function () {
                    $('.avtc_preloader').animate({ "opacity": 0 }, 200, function () { $(this).remove(); });
                    $('.app_view').append('<i class="load_false">:(</i>');
                }
            });
        })


        $(window).resize(function () {
            api.reinitialise();
        })

    };
});

