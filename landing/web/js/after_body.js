var $b = $('body');

var metricManager = function (id, state, event, eventCategory) {

    console.log({
        event: event,
        eventCategory: eventCategory,
        eventAction: id,
        eventLabel: ''
    });

    dataLayer.push({
        event: event,
        eventCategory: eventCategory,
        eventAction: id,
        eventLabel: ''
    });
};

(function () {

    //docs

    $b
		.on('click', '.docs_content menu>ul>li>a', function () {
        $(this).parent().find('ul').toggle(100);
        $(this).parent().toggleClass('opened');
    })
		.on('click', '.docs_content menu>ul>li>a', function () {
        $(this).parent().find('ul').toggle(100);
        $(this).parent().toggleClass('opened');
    });


    $b.on('click', '.main_header .um_avatar', function (e) {
        var content = $(this).parent().find('.um_content');
        var state = $(content).css('display');
        var source = $(this);
        $('.user_menu').removeClass('active');
        $(content).removeClass('opened').hide();

        if (state !== 'block') {
            $(content).show();
            $(this).addClass('active');
            setTimeout(function () {
                $(content).addClass('opened');
            }, 2);
            $(this).parent().addClass('active');
        } else {
            $(content).removeClass('opened');
            setTimeout(function () {
                $(content).hide();
                $(source).removeClass('active');
                $(source).parent().removeClass('active');
            }, 200)
        }
        e.preventDefault();
        e.stopPropagation();
    });

    $b.on('click', function (e) {
        if (!$('.main_header .um_content').is(e.target) && $('.main_header .um_content').has(e.target).length === 0) {
            $('.um_content').removeClass('opened');
            $('.um_content').hide();
            $('.user_menu').removeClass('active');
        }
    });


	//Zooming

    $b.on('click', '.zoom_image', function () {
        if($(window).width()>720){
            var t_src=$(this).attr('data-zooming-image');
            var img = new Image;
            img.src=t_src;
            $('.zooming_image_wrap').show();
            $('.zooming_image_wrap div').css('background-image', 'url('+t_src+')');
            var t_w=img.naturalWidth;
            var t_h=img.naturalHeight;
            if (t_w>$(window).width()*0.8 || t_h>$(window).height()*0.8) $('.zooming_image_wrap div').css('background-size', 'contain');
            else {
                if(!$('.price_section').hasClass('site_price'))
                    $('.zooming_image_wrap div').css('background-size', 'auto');
                else $('.zooming_image_wrap div').css('background-size', 'contain');
            }
            setTimeout(function () {
                $('.zooming_image_wrap').addClass('active');
            }, 10);
            delete img;
        }
    });

    $b.on('click', '.zooming_image_wrap', function () {
        $('.zooming_image_wrap').removeClass('active');
        setTimeout(function () {
            $('.zooming_image_wrap').hide();
        }, 310);
    });


	//вкладки

    var button_cap = 'main', cuurent_page = '';

    var change_button = function (button) {
        $('.sub_menu').find('.active').removeClass('active');
        $('.sub_menu [data-href="' + button + '"]').parent().addClass('active');
    };

    var show_sup = function (hash) {
        hash = hash.substr(1);
        var page = 'main.html';

        switch (hash) {
            case 'main':
                page = 'main.html';
                button_cap = 'main';
                break;
            case 'faq':
                page = 'faq.html';
                button_cap = 'faq';
                break;
            case 'docs':
                page = 'docs.html';
                button_cap = 'docs';
                break;
            default:
                page = 'main.html';
                button_cap = 'main';
        }

        change_button(button_cap);

        return page;
    };

    var show_vak = function (hash) {
        hash = hash.substr(1);
        var page = 'main.html';

        switch (hash) {
            case 'main':
                page = 'main.html';
                button_cap = 'main';
                break;
            case 'training':
                page = 'training.html';
                button_cap = 'training';
                break;
            default:
                page = 'main.html';
                button_cap = 'main';
        }

        change_button(button_cap);

        return page;
    };

    var load_show_sup = function (page, button_cap) {
        if (button_cap != cuurent_page) {
            $('.page_content_sup').hide();
            $('#'+button_cap).show();
            cuurent_page = button_cap;
        }
    };

    var load_show_vak = function (page, button_cap) {
        if (button_cap != cuurent_page) {
            $('.page_content_vak').hide();
            $('#'+button_cap).show();
            cuurent_page = button_cap;
        }
    };

    if(window.location.pathname.indexOf('job') != -1){
        var onload_page = window.location.hash;
        var page = show_vak(onload_page);
        load_show_vak(page, button_cap);

        $(window).on('hashchange', function() {
            var onload_page = window.location.hash;
            var page = show_vak(onload_page);
            load_show_vak(page, button_cap);
        });

    }


    if(window.location.pathname.indexOf('contacts') !== -1){
        var onload_page = window.location.hash;
        var page = show_sup(onload_page);
        load_show_sup(page, button_cap);

        $(window).on('hashchange', function() {
            var onload_page = window.location.hash;
            var page = show_sup(onload_page);
            load_show_sup(page, button_cap);
        });

    }



    //Dockpack form


    $('.docpack_form').on('submit', function (e) {
        e.preventDefault();
        var form = $(this);
        $.post(
            '/site/register-docpack',
            form.serialize(),
            function(json){
                console.log(json);
                $('.error').removeClass('error');
                if(json.status != 1) {
                    for(var key in json.errors){
                        if (json.errors[key]==1){
                            $('.dp_'+key).parent().addClass('error');
                        }
                    }
                } else {
                    $('.oc_sec_form').css('opacity', 0);
                    setTimeout(function(){
                        $('.oc_sec_success').show().addClass('active');
                    }, 200);
                    ga('send', 'event', 'DocPackRegistration', 'Send');
                }
            },
            'json'
        );
    });


    //DOCPACK PRICE

    $b.on('click', '.doc_price_action input', function () {
        if ($(this).is(':checked')) {
            $(this).parent().find('i').addClass('active');
            $('.doc_price span').html($(this).attr('data-checked-on'));
        } else{
            $(this).parent().find('i').removeClass('active');
            $('.doc_price span').html($(this).attr('data-checked-off'));
        }
    });


    //тарифы

    $b.on('click', '.discount_selector a', function (e) {
        var content = $(this).parent().find('ul');
        var state = $(content).css('display');
        var source = $(this);
        $('.discount_selector a').removeClass('active');
        $('.discount_selector ul').removeClass('opened').hide();

        if (state != 'block') {
            $(content).show();
            $(this).addClass('active');
            setTimeout(function () {
                $(content).addClass('opened');
            }, 2);
            $(this).parent().addClass('active');
        } else {
            $(content).removeClass('opened');
            setTimeout(function () {
                $(content).hide();
                $(source).removeClass('active');
                $(source).parent().removeClass('active');
            }, 200)
        }
        e.preventDefault();
        e.stopPropagation();
    });

    $b.on('click', function (e) {
        if (!$('.discount_selector ul').is(e.target) && $('.discount_selector ul').has(e.target).length === 0) {
            $('.discount_selector ul').removeClass('opened');
            $('.discount_selector ul').hide();
            $('.discount_selector a').removeClass('active');
        }
    });

    $b.on('click', '.discount_selector li', function () {
        $(this).addClass('active');
        $('.discount_selector a').html($(this).html() + '<i></i>');

        $('.discount_selector ul').removeClass('opened');
        $('.discount_selector ul').hide();
        $('.discount_selector a').removeClass('active');
        $('.discount_selector a').parent().removeClass('active');

        $('[data-discount-cont]').hide();
        $('[data-discount-cont="' + $(this).attr('data-discount') + '"]').show();
    });


    $b.on('click', '.mobile_tarif_selector li', function () {
        $('.mobile_tarif_selector').find('.active').removeClass('active');
        $(this).addClass('active');
        $('[data-tarif]').hide();
        $('[data-tarif="' + $(this).attr('data-tarif-id') + '"]').css('display', 'table-cell');
    });

    $(window).resize(function () {
        if($(window).width()>720) $('[data-tarif]').removeAttr('style');
    });


    //табы на главной

    $b.on('click', '.it_header a', function () {
        $('.it_header').find('.active').removeClass('active');
        $('[data-it-cont]').hide();
        $('[data-it-cont="'+$(this).attr('data-it-id')+'"]').show();
        $(this).addClass('active');
    });


    $b.on('click', '.partner_q li', function(){
        $(this).toggleClass('active');
    });

    $b.on('click', '.partners_menu a', function(){
        $('body, html').animate({scrollTop: $($(this).attr('href')).offset().top-60});
        return false;
    });

    $b.on('click', '.tarif_tool a', function (e) {
        if ($(this).parent().find('.tr_tooltip').css('display') == 'block') {
            $(this).parent().find('.tr_tooltip').stop().animate({
                "opacity": 0
            }, 100, function () { $(this).hide(); });
            $(this).parent().removeClass('opened');
        }
        else {
            $(this).parent().find('.tr_tooltip').show().animate({
                "opacity": 1
            }, 100);
            $(this).parent().addClass('opened');
        }
        e.stopPropagation();
    });


    $b.on('click', function (e) {
        var choose_city_ul = $('.tr_tooltip');
        if (!$(choose_city_ul).is(e.target) && $(choose_city_ul).has(e.target).length === 0) {
            $('.tr_tooltip').stop().animate({
                "opacity": 0
            }, 100, function () { $(this).hide(); });
            $('.tarif_tool').removeClass('opened');
        };
    });

    $b.on('click', '.hc_line .um_avatar', function (e) {
        if ($('.hc_line .um_content').css('display') == 'block') {
            $('.hc_line .um_content').stop().animate({
                "opacity": 0
            }, 100, function () { $(this).hide(); });
            $(this).removeClass('opened');
        }
        else {
            $('.hc_line .um_content').show().animate({
                "opacity": 1
            }, 100);
            $(this).addClass('opened');
        }
        e.stopPropagation();
    });


    $b.on('click', function (e) {
        var choose_city_ul = $('.hc_line .user_menu');
        if (!$(choose_city_ul).is(e.target) && $(choose_city_ul).has(e.target).length === 0) {
            $('.hc_line .um_content').stop().animate({
                "opacity": 0
            }, 100, function () { $(this).hide(); });
            $('.hc_line .um_avatar').removeClass('opened');
        }
    });

    $b.on('click', '.faq_section li', function () {
        $(this).toggleClass('active');
        $(this).find('div').toggle(100);
    });

    $b.on('click', '.faq_section ul li ul li', function (e) {
        e.stopPropagation();
    });


    //mobile menu

    if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
        var msViewportStyle = document.createElement("style");
        msViewportStyle.appendChild(
            document.createTextNode(
                "@-ms-viewport{width:auto!important}"
            )
        );
        document.getElementsByTagName("head")[0].appendChild(msViewportStyle);
    }

    $('.mobile_menu').html(
        $('.main_header menu').html() + '<hr>' + $('.login_block').html()
    );

    var flag = true;
    $('.mobile_menu_trigger').on('click', function () {
        if (flag) {
            $('.mobile_menu').animate({'right': 0}, 0);
            $('.main_wrapper').addClass('locked');
            $('.wrapper').animate({'right': 220}, 0);
            $('body').addClass('locked');
            $(this).addClass('active');
            flag = false;
            $('.close_mobile_menu').fadeIn(100);
        }
        else {
            $('.mobile_menu').animate({'right': -220}, 0);
            $('.wrapper').animate({'right': 0}, 0, function () {
                $('.main_wrapper').removeClass('locked');
                $('body').removeClass('locked');
                $('.mobile_menu_trigger').removeClass('active');
            });
            flag = true;
            $('.close_mobile_menu').fadeOut(100);
        }
    })


    $('.close_mobile_menu').on('click', function () {
        if (!flag) {
            $('.mobile_menu').animate({'right': -220}, 0);
            $('.wrapper').animate({'right': 0}, 0, function () {
                $('.main_wrapper').removeClass('locked');
                $('body').removeClass('locked');
                $('.mobile_menu_trigger').removeClass('active');
            });
            flag = true;
            $(this).fadeOut(300);
        }
    });

    //MOBILE PRICES

    $b.on('click', '.mobile_spoler a', function () {
        $(this).parents('.ps_col').find('.ps_content').toggle(150);
        if ($(this).html() == 'Подробнее') $(this).html('Свернуть');
        else $(this).html('Подробнее');
    });
    var thind = 0;
    var thhtml = '';
    $('.ps_list li').each(function () {
        thind = $(this).data('prop');
        thhtml = $(this);
        $('.ps_content [data-prop="' + thind + '"]').each(function () {
            $(this).html('<b>' + $(thhtml).html() + '</b>' + $(this).html());
        })
    });
    var mnt = '';

    $b.on('click', '.pr_selector a', function () {
        mnt = $(this).data('month');
        $('[data-month-cont]').each(function(){
            if($(this).data('month-cont')!=mnt && $(this).data('month-cont') != '0') $(this).hide();
            else $(this).show();
        })
        $('.pr_selector .active').removeClass('active');
        $(this).addClass('active');
    });



    //LOGIN FORM

    $b.on('click', '#utc1 .login_form .lf_rest a', function () {
        $('#login_tab').toggle();
        $('#password_restore_tab').toggle();
    });

    //CALL ME

    $b.on('click', '.call_me input[type="submit"]', function () {
        $('.call_me').hide();
        $('.call_me_suc').show();
    });

})();


$(document).ready(function () {

	$('.main_header').liFixar({
		side: 'top',
		position: 0,
		fix: function(el, side){
			$(el).addClass('fixed');
			el.liFixar('update')
		},
		unfix: function(el, side){
			$(el).removeClass('fixed');
			el.liFixar('update')
		}
	});

	if ($(window).width()<720) $('.main_header').liFixar('disable');
	$(window).resize(function () {
		if ($(window).width()>720) $('.main_header').liFixar('enable');
		else $('.main_header').liFixar('disable');
	});

    $('input#loginform-verifycode').attr('placeholder', 'Проверочный код');


    $("input[name='ip']").val($('[data-ip]').attr('data-ip'));

    $("input[name='utm_source']").val($('[data-utm-source]').attr('data-utm-source'));

    $("input[name='utm_medium']").val($('[data-utm-medium]').attr('data-utm-medium'));

    $("input[name='utm_campaign']").val($('[data-utm-campaign]').attr('data-utm-campaign'));

    $("input[name='utm_content']").val($('[data-utm-content]').attr('data-utm-content'));

    $("input[name='utm_term']").val($('[data-utm-term]').attr('data-utm-term'));

    $("input[name='client_id_go']").val($('[data-client-id-go]').attr('data-client-id-go'));

    $("input[name='client_id_ya']").val($('[data-client-id-ya]').attr('data-client-id-ya'));



    $('.inline table').wrap('<div class="inline-table"></div>');

    $('.ps_list li').each(function(){
        var maxh=0;
        $('[data-prop="'+$(this).attr('data-prop')+'"]').each(function(){
            if($(this).height()>maxh) maxh=$(this).height();
        }).css('height', maxh);
    });

    //SEARCH TYPE FIX

    $('.search_items_types').each(function () {
        $(this).liFixar({
            side: 'top',                 // "top" (по умолчанию), "bottom", "all"  -
            position: 0               // любое целое число - расстояние от элемента до края экрана,
        });
    });
});










$(document).ready(function () {

    //Contact form
    $('#contact-form').submit(function(e){
        e.preventDefault();
        var error = false;
        var form = $(this);

        form.find('.required').each(function(){
            if($(this).val() == '')
            {
                error = true;
                $(this).parent().addClass('error');
            }
            else
                $(this).parent().removeClass('error');
        });

        if(error)
            return false;

        $.post(
           form.attr('action'),
           form.serialize(),
           function(json){

               if(json == 1)
                   form.prev('.message').css('color', 'green').html('Сообщение успешно отправлено!');
               else
                   form.prev('.message').css('color', 'red').html('Ошибка отправки!');
           },
           'json'
        );
    });

    //Login form
    $('#login-form').submit(function(e){
        e.preventDefault();
        var error = false;
        var form = $(this);

        form.find('.required').each(function(){
            if($(this).val() == '')
            {
                error = true;
                $(this).parent().addClass('error');
                $(this).next().text('Не заполнено');
            }
            else
                $(this).parent().removeClass('error');
        });

        if(error)
            return false;

        $.post(
           form.attr('action'),
           form.serialize(),
           function(json){
              if(json.error !== null)
              {
                  var email = form.find('input[name="LoginForm[email]"]');

                  if(json.error.email !== undefined)
                  {
                      email.parent().addClass('error');
                      email.next().text(json.error.email[0]);
                  }
                  else if(json.error.password !== undefined)
                  {
                      var password = form.find('input[name="LoginForm[password]"]');
                      password.parent().addClass('error');
                      password.next().text(json.error.password[0]);

                      email.parent().addClass('error');
                      email.next().text('');
                  }
              }
              else
              {
                  location.href = json.response;
              }
           },
           'json'
        );
    });

    //Recovery password
    $('#recovery-form').submit(function(e){
        e.preventDefault();
        var error = false;
        var form = $(this);

        form.find('.required').each(function(){
            if($(this).val() == '')
            {
                error = true;
                $(this).parent().addClass('error');
                $(this).next().text('Не заполнено');
            }
            else
                $(this).parent().removeClass('error');
        });

        if(error)
            return false;

        $.post(
           form.attr('action'),
           form.serialize(),
           function(json){
              if(json.error !== null)
              {
                  var email = form.find('input[name="PasswordResetRequestForm[email]"]');

                  email.parent().addClass('error');
                  email.next().text(json.error[0]);
              }
              else
              {
                  form.prev('.message').css('color', 'green').html('Вам выслана информация по восстановлению пароля!');
              }
           },
           'json'
        );
    });

    //РЕГИСТРАЦИЯ

    //Функция отправки смс
    function send_sms(phone) {
        $.get('/tenant/send-sms-code', {phone: phone}).fail(function () {
                $('.er_text').text('Извините, произошла ошибка и SMS не отправлена. Сообщите нам об                 ошибке на support@gootax.pro');
                $('.er_text').attr("style", "display: block !important;color: red")
            });
        $('#send_sms_again').addClass('send_block');

        setTimeout(function () {
            $('#send_sms_again').removeClass('send_block');
        }, 15000);
    }

    //Запрет ввода кириллицы
    var old_domain = '';
    $('#signupform-domain').keyup(function (e) {
        var re = /^[a-z0-9\-]*$/,
            BTN_LEFT = 37,
            BTN_RIGHT = 39;

        if (e.keyCode == BTN_LEFT || e.keyCode == BTN_RIGHT) {
            return;
        }

        var domain = $(this).val().toLowerCase();
        if (re.test(domain)) {
            old_domain = domain;
        }
        $(this).val(old_domain);

    });

    //Галочка принятия лицензии
    $('#form-signup .reg_lin label').on('click',function(){
        if ($(this).find('input[type="checkbox"]').prop("checked")) {
            $(this).parents('form').find('input[type="submit"]').prop('disabled', false);
        }
        else {
            $(this).parents('form').find('input[type="submit"]').prop('disabled', true);
        }
    });

    //Кнопка "Далее"
    var old_phone = null; //При нажатии кнопки "Далее" отправки смски только при смене номера
    $('#step_1 .button').on('click',function(e){
        e.preventDefault();
        var form = $(this).parents('form');
        var error = false;
        var step_1 = $('#step_1');

        //Валидация
        if(step_1.find('.error').size() > 0)
            error = true;

        step_1.find('.required').each(function(){
            var input = $(this).find('input');
            if(input.val() == '')
            {
                error = true;
                input.parent().addClass('error');
                input.nextAll('.er_text, .help-block-error').text('Не заполнено');
            }
        });

        if(error)
            return false;

        //-------------------------------------------------------

        //Переход на второй шаг, когда все поля заполнены.
        //Валидация при сабмите.
        $.post(
            '/tenant/signup-validate',
            form.serialize(),
            function(json){
                if(json == '')
                {
                    //Отправка смс кода
                    var phone = $('#signupform-phone').val();

                    if(phone != old_phone)
                    {
                        old_phone = phone;
                        send_sms(phone);
                    }

                    //Переход на следующий шаг
                    form.find('#step_1').hide();
                    form.find('#step_2').show();

                    metricManager('btnNext', '', 'eventTarget_low', 'target_low');

                }
            },
            'json'
        );
    });

    //Кнопка "Изменить данные"
    $('#go_to_step_1').on('click', function(e){
        e.preventDefault();
        var form = $(this).parents('form');
        form.find('#step_2').hide();
        form.find('#step_1').show();
    });

    //Кнопка "Подтвердить"
    $('#step_2 .button').on('click',function(e){
        e.preventDefault();

        var form = $(this).parents('form');
        var button = $(this);
        var sms_code = $('#sms_code');
        var code = sms_code.val();

        if (form.data('process')) {
            return false;
        }

        if(code == '')
        {
            sms_code.parent().addClass('error');
            sms_code.next().text('Не заполнено');
            return false;
        }

        sms_code.parent().removeClass('error');

        form.data('process', true);
        button.attr('disabled', true);

        //Проверка смс кода
        $.get(
            '/tenant/confirm-sms-code',
            {code: code},
            function(json){
                if(json.error == null){
                    //Телефон подтвержден, отправляем форму для регистрации
                    $.post(
                        '/tenant/signup',
                        form.serialize(),
                        function(json){
                            if(json.auth_url != null) {

                                metricManager('btnApprove', '', 'eventTarget_low', 'target_low');
                                metricManager('submitRegister', 'Done', 'eventTarget', 'target');

                                location.href = json.auth_url;
                            }
                        },
                        'json'
                    )
                        .done(function (data) {
                            form.data('process', false);
                            button.attr('disabled', false);
                        })
                        .fail(function (data) {
                            console.log('fail', data);
                            form.data('process', false);
                            button.attr('disabled', false);
                        });
                } else {
                    sms_code.parent().addClass('error');
                    sms_code.next().text(json.error);

                    form.data('process', false);
                    button.attr('disabled', false);
                }
            },
            'json'
        )
        .fail(function (data) {
            console.log('fail', data);
            form.data('process', false);
            button.attr('disabled', false);
        });
    });

    //Кнопка "Отправить заново"
    $('#send_sms_again').on('click', function(e){
        e.preventDefault();

        if($(this).hasClass('send_block'))
            return false;

        send_sms($('#signupform-phone').val());
    });

    //----------------------------------------------------------------------------------------

    //Телефонная маска
		var maskList = $.masksSort($.masksLoad("/js/inputmask/phone-codes.json"), ['#'], /[0-9]|#/, "mask");
		var maskOpts = {
			inputmask: {
				definitions: {
					'#': {
						validator: "[0-9]",
						cardinality: 1
					}
				},
				oncomplete: function () {
					$(this).attr('is-valid', 1);
				},
				onincomplete: function () {
					$(this).attr('is-valid', null);
				},
				showMaskOnHover: false,
				autoUnmask: true
			},
			match: /[0-9]/,
			replace: '#',
			list: maskList,
			listKey: "mask",
		};

		console.log($('.mask_phone'));
		$('.mask_phone').inputmasks(maskOpts);
		$('.callback_phone').inputmasks(maskOpts);


});




//NEW TABS
$b.on('click', '.u_tabs label', function () {
    $('.u_tabls_cont>div').hide();
    $('.u_tabls_cont').find('#' + $(this).find('input').attr('class')).show();
});


$b.on('click', '#utc2 .login_form .lf_pass a', function () {
    $('#password_restore_tab2, #login_tab2').toggle();
});

$b.on('click', '.lt2_tab', function () {
    $(this).parent().find('.active').removeClass('active');
    $(this).addClass('active');
    $('#lt2_login, #lt2_reg').hide();
    $('#' + $(this).attr('rel')).show();
});


//notification close
$b.on('click', '.hn_close', function () {
    $(this).parents('.header_not').hide(150);
});

//video play
$b.on('click', '.play_video', function () {
    //if (($(window).width() < 700) && (isMobile.iOS()==null || isMobile.Windows())) {
    //    $('#video1').addClass('mobile_opened');
    //    $('.close_mobile_video').css('display', 'block');
    //}
    $('.play_video').animate({ "opacity": 0 }, 200, function () {
        $(this).hide();
        $('#video1').prop('controls', true);
        document.getElementById("video1").play();
    });
});

$('#register-distributer-form select [value=""]')
        .attr('selected', 'selected')
        .attr('disabled', 'disabled');

//BTN TRIGGER

$b.on('click', '.btn_trigger', function () {
    if ($(this).find('input[type="checkbox"]').is(':checked')) $(this).parents('.app_submit').find('input[type="submit"]').prop('disabled', false)
    else $(this).parents('.app_submit').find('input[type="submit"]').prop('disabled', true);
});

$('a.profile-logo').colorbox();

$('img.js-show-preview').click(function() {
    $(this).colorbox({
        href: $(this).attr('src'),
        maxWidth: '80%',
        maxHeight: '80%'
    });
});


var RegistrationModule = {
    gaEventType: null
};

$(document).on('click', '[data-remodal-target="register"]', function () {
    RegistrationModule.gaEventType = $(this).data('ga-event-type');
});


$(document).on('opened', '.remodal.signup', function (e) {

    metricManager('register', RegistrationModule.gaEventType, 'eventTarget_low', 'target_low');

});


/* Fixed vulnerability of html anchors with target=_blank */
$('.content').on('click', '.blogs a[target="_blank"]', function (e) {
    var otherWindow = window.open();
    otherWindow.opener = null;
    if (e.target.hasAttribute('href')) {
        otherWindow.location = e.target.getAttribute('href');
    }
});

$b.on('click', '.js-tab-selector', function () {

    var tabs = $(this).attr('data-tabs');
    var tab = $(this).attr('data-tab');

    $('.js-tab-selector[data-tabs="'+tabs+'"], .js-tab-content[data-tabs="'+tabs+'"]').removeClass('active');
    $('.js-tab-selector[data-tabs="'+tabs+'"][data-tab="'+tab+'"], .js-tab-content[data-tabs="'+tabs+'"][data-tab="'+tab+'"]').addClass('active');

});


$('.index-carousel').owlCarousel({
    loop:true,
    margin:0,
    nav:true,
	items: 1,
    navText: ['', '']
});
$('.site-carousel').owlCarousel({
    loop:true,
    center: true,
    margin:0,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        720:{
            items:3
        }
    },
    navText: ['', '']
});

$b.on('click', '.js-gootax-demo-form .button', function(e){
    var errors = false;
    var $form = $(this).parents('.js-gootax-demo-form');
    $form.find('input').each(function () {
        if(($(this).val()=='') && $(this).attr('type') !== 'hidden'){
            errors = true;
            $(this).addClass('error').parent().find('div.error').text('Не заполнено');
        } else{
            $(this).removeClass('error').parent().find('div.error').text('');
        }
    });
    if(!errors){
        e.preventDefault();
        var fd = new FormData(),
            fields = ['action', 'name', 'city', 'contact', 'email', 'ip', 'utm_source', 'utm_medium', 'utm_campaign', 'utm_content', 'utm_term', 'client_id_go', 'client_id_ya'];

        fields.forEach(function (field) {
            var value = $form.find('input[name="' + field + '"]').val();
            if (value || field !== 'email') {
                fd.append(field, value || '-');
            }
        });

        $.ajax({
            url: '/site/create-conversion',
            data: fd,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function (data) {
                if(data.result === 1){
                    $form.find('.gootax-demo-form__step').hide();
                    $form.find('.gootax-demo-form__step--success').show();

                    metricManager($($form).attr('data-form-type'), 'submit', 'eventTarget', 'target');

                }else{
                    for(var key in data.errors){
                        $form.find('input[name="'+key+'"]').addClass('error').parent().find('div.error').text(data.errors[key]);
                    }
                }
            },
            error: function (data) {
                alert('Ошибка при отправке данных!');
                console.log(data);
            }
        });
    }
});

$b.on('click', '.js-form .button', function (e) {
    e.preventDefault();

    var errors = false;
    var $form = $(this).parents('.js-form');
    $form.find('input').each(function () {
        if($(this).val()=='' && $(this).attr('type')!=='hidden'){
            errors = true;
            $(this).addClass('error').parent().find('div.error').text('Не заполнено');
        } else{
            $(this).removeClass('error').parent().find('div.error').text('');
        }
    });

    if(!errors){
        var fd = new FormData();

    	if($form.attr('data-type')!=='business-plan'){
            var fields = ['action', 'name', 'city', 'contact', 'email', 'ip', 'utm_source', 'utm_medium', 'utm_campaign', 'utm_content', 'utm_term', 'client_id_go', 'client_id_ya'];
            fields.forEach(function (field) {
                var value = $form.find('input[name="' + field + '"]').val();
                if (value || field !== 'email') {
                    fd.append(field, value || '-');
                }
            });

            $.ajax({
                url: '/site/create-conversion',
                data: fd,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function (data) {
                    if(data.result === 1){
                        $form.addClass('hidden');
                        $form.parent().find('.js-success').addClass('active');
                        if($form.parents('.js-popup-timer').length>0){

                            metricManager($form.parents('.js-popup-timer').attr('data-yandex-success'), 'submit', 'eventTarget', 'target');

						} else{
                            metricManager($($form).attr('data-form-type'), 'submit', 'eventTarget', 'target');
						}

                        var date = new Date(new Date().getTime() + 60 * 24 * 60 *  60 * 1000);
                        document.cookie = "popup-timer-filled=true; expires=" + date.toUTCString();

                    }else{
                        for(var key in data.errors){
                            $form.find('input[name="'+key+'"]').addClass('error').parent().find('div.error').text(data.errors[key]);
                        }
                    }
                },
                error: function (data) {
                    alert('Ошибка при отправке данных!');
                    console.log(data);
                }
            });
		} else{
            fd.append('BusinessPlan[name]', '-');
            fd.append('BusinessPlan[phone]', '-');
            fd.append('BusinessPlan[ip]', $form.find('input[name="ip"]').val() || '-');
            fd.append('BusinessPlan[utm_source]', $form.find('input[name="utm_source"]').val() || '-');
            fd.append('BusinessPlan[utm_medium]', $form.find('input[name="utm_medium"]').val() || '-');
            fd.append('BusinessPlan[utm_campaign]', $form.find('input[name="utm_campaign"]').val() || '-');
            fd.append('BusinessPlan[utm_content]', $form.find('input[name="utm_content"]').val() || '-');
            fd.append('BusinessPlan[utm_term]', $form.find('input[name="utm_term"]').val() || '-');
            fd.append('BusinessPlan[client_id_go]', $form.find('input[name="client_id_go"]').val() || '-');
            fd.append('BusinessPlan[client_id_ya]', $form.find('input[name="client_id_ya"]').val() || '-');
            fd.append('BusinessPlan[email]', $form.find('input[name="email"]').val() || '-');
            $.ajax({
                url: '/business-plan/send',
                data: fd,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function (data) {
                    if(data.status === 'success'){
                        $form.addClass('hidden');
                        $form.parent().find('.js-success').addClass('active');
                        //ga('send', 'event', $($form).attr('data-form-type'),'submit');

						metricManager($($form).attr('data-form-type'), 'submit', 'eventTarget7', 'target');

                    }else{
                        $form.find('input[name="email"]').addClass('error').parent().find('div.error').text('Неправильный e-mail');
                    }
                },
                error: function (data) {
                    alert('Ошибка при отправке данных!');
                    console.log(data);
                }
            });
		}
	}
});

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

$(document).ready(function(){
	setTimeout(function(){
		$('.js-video').each(function(){
			var height = $('.col--video').height();
			$('.col--video').css({height: height});
		});
	}, 100);

    $('.js-conf').on('click', function(){
        if(!$(this).find('input').is(':checked')){
            $(this).parents('.js-gootax-demo-form').find('.button').attr('disabled', 'disabled');
        } else{
            $(this).parents('.js-gootax-demo-form').find('.button').removeAttr('disabled');
        }
    });



    function isExistsOpenForm() {
        return $('.remodal-is-opened, .remodal-is-opening').length > 0;
    }

    if(getCookie('popup-timer-session')!=='true' && getCookie('popup-timer-filled')!=='true'){

        var popupId = 2;

        $.ajax({
            url: '/site/get-popup-index?popupCount=3',
            contentType: false,
            processData: false,
            type: 'GET',
            success: function (data) {
                popupId = data.index;
            }
        });

        setTimeout(function(){
            if(!isExistsOpenForm() && getCookie('popup-timer-session')!=='true' && getCookie('popup-timer-filled')!=='true') {
            	if(popupId!==3){
                    var $winVariant = $('.js-popup-timer[data-popup="' + popupId + '"]');
                    $winVariant.show();

                    var win = $('[data-remodal-id="popup-timer"]').remodal();
                    win.open();


                    metricManager($winVariant.attr('data-yandex-opened'), 'submit', 'eventTarget_low', 'target_low');

				} else{
					$('[data-remodal-target="register"]').click();

                    metricManager('tryOpened', '', 'eventTarget_low', 'target_low');

				}


                document.cookie = "popup-timer-session=true";
            }

        }, 25000);

	}


});