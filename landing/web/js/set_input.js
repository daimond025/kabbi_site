$(function () {

    $("input[name='ip']").val($('[data-ip]').attr('data-ip'));

    $("input[name='utm_source']").val($('[data-utm-source]').attr('data-utm-source'));

    $("input[name='utm_medium']").val($('[data-utm-medium]').attr('data-utm-medium'));

    $("input[name='utm_campaign']").val($('[data-utm-campaign]').attr('data-utm-campaign'));

    $("input[name='utm_content']").val($('[data-utm-content]').attr('data-utm-content'));

    $("input[name='utm_term']").val($('[data-utm-term]').attr('data-utm-term'));

    $("input[name='client_id_go']").val($('[data-client-id-go]').attr('data-client-id-go'));

    $("input[name='client_id_ya']").val($('[data-client-id-ya]').attr('data-client-id-ya'));

})